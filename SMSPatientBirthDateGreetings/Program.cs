﻿using JSLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;

namespace SMSPatientBirthDateGreetings
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic appSettings = getAppSettings();

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd;

            Console.WriteLine("Vet Cloud SMS Patient Birth Day Greeting Sender Service run as of {0}", DateTime.Now.ToString("MM/dd/yyyy ddd hh:mm tt"));

            try
            {

                sendSMS(appSettings);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            dateEnd = DateTime.Now;

            System.TimeSpan diff2 = dateEnd - dateStart;

            Console.WriteLine("Done.. Duration - {0}", diff2.ToString("hh\\:mm\\:ss"));
            Thread.Sleep(5000);
        }

        public static void sendSMS(dynamic appSettings)
        {
            string connectonString = appSettings.ConnectionString;
            string apiCode = appSettings.ITexMoAPICode;
            string apiPassword = appSettings.ITexMoAPIPassword;

            List<int> soapPlan = new List<int>();

            var dbAccess = new DBCollection(connectonString);

            var DetailView = dbAccess.GetDynamicObject("EXEC dbo.pGetForSendPatientBirthDate", true);

            var records = DetailView.records as List<dynamic>;

            if (records != null)
            {

                foreach (var d in records)
                {
                
                    try
                    {
                        var iTextMo_Status = "";

                        iTextMo_Status = JSLibrary.Common.Utility.sendTextMessage(d.ContactNumber_Client, d.Message, apiCode, apiPassword);

                        var _result = new DBCollection(connectonString).GetDynamicObject($"EXEC dbo.pLog_BirthDateSMSGreeting @ID_Client = {d.ID_Client}, @ContactNumber = '{d.ContactNumber_Client}',  @ID_Patient = {d.ID_Patient},  @iTextMo_Status = {iTextMo_Status}, @Message = '{d.Message}'", true);

                        Console.WriteLine("Status {0} - {1} - {2} - {3} - {4}",
                            iTextMo_Status,
                            d.Name_Company,
                            d.Name_Client,
                            d.Name_Patient,
                            d.ContactNumber_Client
                         );

                        Thread.Sleep(5000);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        System.Console.ReadKey();
                    }

                }


            }

        }
        public static Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }
        public static dynamic getAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader(@$"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }
    }
}

