import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserAuthenticationService } from '../core/UserAuthentication.service';
import { ToastService } from '../shared/toast.service';

@Component({
  selector: 'app-login-url',
  templateUrl: './login-url.component.html',
  styleUrls: ['./login-url.component.less']
})
export class LoginURLComponent implements OnInit {

  private _subscribeActiveRoute: any;

  constructor(protected activeRoute: ActivatedRoute, private userAuth: UserAuthenticationService, private toastService: ToastService) { }

  async ngOnInit(): Promise<void> {
    var _ = this;

    this._subscribeActiveRoute = this.activeRoute.params.subscribe(
      async (routeParams) => {

        var params: string = this.activeRoute.snapshot.params['text'];

        await this.userAuth.LogInByUrl(params)
        .then((promise) => {

          window.location.href = "./Starter"
        })
        .catch((rej) => {

          this.toastService.danger(rej.error);
          console.log(rej);
        });


      }
    );
  }

}
