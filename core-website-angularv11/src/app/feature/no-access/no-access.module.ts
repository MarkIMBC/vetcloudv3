import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoAccessRoutingModule } from './no-access-routing.module';
import { NoAccessComponent } from './no-access.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [NoAccessComponent],
  imports: [
    CommonModule,
    SharedModule,
    NoAccessRoutingModule
  ]
})
export class NoAccessModule { }
