import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyDetailRoutingModule } from './company-detail-routing.module';
import { CompanyDetailComponent } from './company-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';

@NgModule({
  declarations: [CompanyDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CompanyDetailRoutingModule,
  ]
})
export class CompanyDetailModule { }
