import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from '../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  ConfimentItemServiceModeEnum,
  FilingStatusEnum,
  IControlModelArg,
  ItemTypeEnum,
  User_UserRoles_DTO,
  PropertyTypeEnum,
} from '../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'user-roles-table',
  templateUrl: './user-roles-table.component.html',
  styleUrls: ['./user-roles-table.component.less'],
})
export class UserRolesTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;
  MedicationRouteList: any[] = [];

  _userRoles: User_UserRoles_DTO[] = [];

  @Input() set userRoles(objs: User_UserRoles_DTO[]) {
    this.loadMenuItems();

    if (!objs) return;

    this._userRoles = objs;
  }

  get userRoles() {
    return this._userRoles;
  }

  async loadMedicationRouteList(): Promise<void> {
    this.MedicationRouteList = [];

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tMedicationRoute
    `
    );

    var _MedicationRouteList: any[] = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      _MedicationRouteList.push(obj.Name);
    });

    console.log(_MedicationRouteList);
    this.MedicationRouteList = _MedicationRouteList;
  }

  @Input() ItemServiceMode: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  @Output() onModelChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() onMenuItemClick: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [];
  tempID: number = 0;

  loadMenuByCurrentObject(CurrentObject: any) {
    this.menuItems = [];

    var menuUserRole_Add = {
      label: 'Add Roles',
      icon: 'fa fa-user',
      visible: true,
    };

    this.menuItems.push(menuUserRole_Add);
  }

  loadMenuItems() {
    var menuItem_AddService = {
      label: 'Add Roles',
      icon: 'fa fa-user',
      visible: true,
    };

    this.menuItems = [];

    this.menuItems.push(menuItem_AddService);
  }

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    public toastService: ToastService,
    protected cs: CrypterService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Roles') {
      this.doAddRole();
    }

    this.onMenuItemClick.emit(arg);
  }

  async BtnDeleteUserRole_OnClick(userRoles: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this._userRoles,
      'ID',
      userRoles.ID + ''
    );

    this._userRoles.splice(index, 1);
  }

  async doAddRole() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              IsActive
            FROM dbo.vActiveUserRole
            /*encryptsqlend*/
          `,
      columns: [
        {
          name: 'Name',
          caption: 'User Role',
          propertyType: PropertyTypeEnum.String,
        },
      ],
      orderByString: 'Name DESC',
    });

    var _ = this;
    obj.rows.forEach((record: any) => {
      _.tempID--;

      var role: any = {
        ID: _.tempID,
        ID_UserRole: record.ID,
        Name_UserRole: record.Name,
        IsActive: true,
      };
      if (this._userRoles == null) this._userRoles = [];
      this._userRoles.push(role);
    });
  }

  userRole_onModelChanged(userRole: any, e: IControlModelArg) {
    userRole[e.name] = e.value;

    this.onModelChanged.emit();
  }
}
