import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyPaymentsSmallBoxComponent } from './daily-payments-small-box.component';

describe('DailyPaymentsSmallBoxComponent', () => {
  let component: DailyPaymentsSmallBoxComponent;
  let fixture: ComponentFixture<DailyPaymentsSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyPaymentsSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyPaymentsSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
