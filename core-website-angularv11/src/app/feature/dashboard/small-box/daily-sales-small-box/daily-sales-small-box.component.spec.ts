import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailySalesSmallBoxComponent } from './daily-sales-small-box.component';

describe('DailySalesSmallBoxComponent', () => {
  let component: DailySalesSmallBoxComponent;
  let fixture: ComponentFixture<DailySalesSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailySalesSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailySalesSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
