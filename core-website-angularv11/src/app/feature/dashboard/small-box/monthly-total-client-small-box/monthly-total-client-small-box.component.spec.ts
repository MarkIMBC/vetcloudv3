import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyTotalClientSmallBoxComponent } from './monthly-total-client-small-box.component';

describe('MonthlyTotalClientSmallBoxComponent', () => {
  let component: MonthlyTotalClientSmallBoxComponent;
  let fixture: ComponentFixture<MonthlyTotalClientSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyTotalClientSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyTotalClientSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
