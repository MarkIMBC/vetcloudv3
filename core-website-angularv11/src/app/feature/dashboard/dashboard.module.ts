import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardSalesChartComponent } from './dashboard-sales-chart/dashboard-sales-chart.component';
import { DashboardServicesChartComponent } from './dashboard-services-chart/dashboard-services-chart.component';
import { DashboardMonthlyClientChartComponent } from './dashboard-monthlyclient-chart/dashboard-monthlyclient-chart.component';
import { FormsModule } from '@angular/forms';
import { DailySalesSmallBoxComponent } from './small-box/daily-sales-small-box/daily-sales-small-box.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { DailyPaymentsSmallBoxComponent } from './small-box/daily-payments-small-box/daily-payments-small-box.component';
import { DailyClientCountSmallBoxComponent } from './small-box/daily-client-count-small-box/daily-client-count-small-box.component';
import { MonthlySalesSmallBoxComponent } from './small-box/monthly-sales-small-box/monthly-sales-small-box.component';
import { MonthlyPaymentsSmallBoxComponent } from './small-box/monthly-payments-small-box/monthly-payments-small-box.component';
import { MonthlyTotalClientSmallBoxComponent } from './small-box/monthly-total-client-small-box/monthly-total-client-small-box.component';
import { DashboardTopSellingItemsChartComponent } from './dashboard-top-selling-items-chart/dashboard-top-selling-items-chart.component';
import { DashboardRevisitClientChartComponent } from './dashboard-revisit-client-chart/dashboard-revisit-client-chart.component';
import { DashboardTopBilledClientComponent } from './dashboard-top-billed-client/dashboard-top-billed-client.component';
@NgModule({
  declarations: [DashboardComponent,DashboardSalesChartComponent,DashboardServicesChartComponent,DashboardMonthlyClientChartComponent, DailySalesSmallBoxComponent, DailyPaymentsSmallBoxComponent, DailyClientCountSmallBoxComponent, MonthlySalesSmallBoxComponent, MonthlyPaymentsSmallBoxComponent, MonthlyTotalClientSmallBoxComponent, DashboardTopSellingItemsChartComponent, DashboardRevisitClientChartComponent, DashboardTopBilledClientComponent],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    FormsModule,
    NgbModule,
    ModalModule,
  ],
  exports:[DashboardSalesChartComponent,DashboardServicesChartComponent,DashboardMonthlyClientChartComponent]
})
export class DashboardModule { }

