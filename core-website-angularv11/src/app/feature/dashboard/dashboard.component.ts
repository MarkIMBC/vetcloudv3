import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { DashboardSalesChartComponent } from './dashboard-sales-chart/dashboard-sales-chart.component';
import { DashboardMonthlyClientChartComponent } from './dashboard-monthlyclient-chart/dashboard-monthlyclient-chart.component';
import { DashboardServicesChartComponent } from './dashboard-services-chart/dashboard-services-chart.component';
import { DailySalesSmallBoxComponent } from './small-box/daily-sales-small-box/daily-sales-small-box.component';
import { DailyPaymentsSmallBoxComponent } from './small-box/daily-payments-small-box/daily-payments-small-box.component';
import { DailyClientCountSmallBoxComponent } from './small-box/daily-client-count-small-box/daily-client-count-small-box.component';
import { MonthlySalesSmallBoxComponent } from './small-box/monthly-sales-small-box/monthly-sales-small-box.component';
import { MonthlyPaymentsSmallBoxComponent } from './small-box/monthly-payments-small-box/monthly-payments-small-box.component';
import { DashboardTopSellingItemsChartComponent } from './dashboard-top-selling-items-chart/dashboard-top-selling-items-chart.component';
import { DashboardTopBilledClientComponent } from './dashboard-top-billed-client/dashboard-top-billed-client.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {

  @ViewChild('dailysalessmallbox') dailysalessmallbox: DailySalesSmallBoxComponent | undefined;
  @ViewChild('dailypaymentssmallbox') dailypaymentssmallbox: DailyPaymentsSmallBoxComponent | undefined;
  @ViewChild('dailyclientcountsmallbox') dailyclientcountsmallbox: DailyClientCountSmallBoxComponent | undefined;
  @ViewChild('monthlysalessmallbox') monthlysalessmallbox: MonthlySalesSmallBoxComponent | undefined;
  @ViewChild('monthlypaymentssmallbox') monthlypaymentssmallbox: MonthlyPaymentsSmallBoxComponent | undefined;
  @ViewChild('monthlytotalclientsmallbox') monthlytotalclientsmallbox: MonthlyPaymentsSmallBoxComponent | undefined;

  @ViewChild('dashboardsaleschart') dashboardsaleschart: DashboardSalesChartComponent | undefined;
  @ViewChild('dashboardserviceschart') dashboardserviceschart: DashboardServicesChartComponent | undefined;
  @ViewChild('dashboardmonthlyclientchart') dashboardmonthlyclientchart: DashboardMonthlyClientChartComponent | undefined;
  @ViewChild('dashboardrevisitclientchart') dashboardrevisitclientchart: DashboardTopSellingItemsChartComponent | undefined;
  @ViewChild('dashboardtopsellingitemschart') dashboardtopsellingitemschart: DashboardTopSellingItemsChartComponent | undefined;
  @ViewChild('dashboardtopbilledclient') dashboardtopbilledclient: DashboardTopBilledClientComponent | undefined;


  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardInfo: any = {
    ClientDailyCount: 0,
    ClientMonthlyCount: 0,
    SalesDailyAmount: 0,
    SalesMonthlyAmount: 0,
    PaymentDailyAmount: 0,
    PaymentMonthlyAmount: 0,
    FormattedSalesDailyAmount: '',
    FormattedSalesMonthlyAmount: '',
    FormattedPaymentDailyAmount: '',
    FormattedPaymentMonthlyAmount: '',
  };

  loading: boolean = false;
  isShowComponent: boolean = false;

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {
    var _ = this;
    /*Check and Log out by Server User Session Status*/
    if ((await this.userAuth.CheckAndLogOutByUserSessionStatus()) != true) {
      return;
    }

    this.isShowComponent = true;
    this.loadMenuItem();
  }

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItem() {
    this.menuItems = [];

    this.menuItems.push({
      label: 'Reset',
      icon: 'fas fa-sync',
      class: 'text-default',
      visible: true,
    });
  }

  menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == 'Reset') {

      if(this.dailysalessmallbox) this.dailysalessmallbox.reset();
      if(this.dailypaymentssmallbox) this.dailypaymentssmallbox.reset();
      if(this.dailyclientcountsmallbox) this.dailyclientcountsmallbox.reset();
      if(this.monthlysalessmallbox) this.monthlysalessmallbox.reset();
      if(this.monthlypaymentssmallbox) this.monthlypaymentssmallbox.reset();
      if(this.monthlytotalclientsmallbox) this.monthlytotalclientsmallbox.reset();

      if(this.dashboardsaleschart) this.dashboardsaleschart.reset();
      if(this.dashboardserviceschart) this.dashboardserviceschart.reset();
      if(this.dashboardmonthlyclientchart) this.dashboardmonthlyclientchart.reset();
      if(this.dashboardrevisitclientchart) this.dashboardrevisitclientchart.reset();
      if(this.dashboardtopsellingitemschart) this.dashboardtopsellingitemschart.reset();
      if(this.dashboardtopbilledclient) this.dashboardtopbilledclient.reset();
    }
  }
}
