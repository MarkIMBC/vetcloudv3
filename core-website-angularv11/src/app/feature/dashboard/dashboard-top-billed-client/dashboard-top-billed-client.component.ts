import { Component, OnInit, ChangeDetectionStrategy, Input, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import * as moment from 'moment';
import { IControlModelArg } from 'src/shared/APP_HELPER';
import { AdminLTEDateTimeboxComponent } from 'src/app/shared/control/admin-lte-datetimebox/admin-lte-datetimebox.component';

@Component({
  selector: 'dashboard-top-billed-client',
  templateUrl: './dashboard-top-billed-client.component.html',
  styleUrls: ['./dashboard-top-billed-client.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardTopBilledClientComponent implements OnInit {

  @ViewChild('adminltedateboxDateStart') adminltedateboxDateStart: AdminLTEDateTimeboxComponent | undefined
  @ViewChild('adminltedateboxDateEnd') adminltedateboxDateEnd: AdminLTEDateTimeboxComponent | undefined

  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: Chart | undefined;
  dataSource: any;
  Items: Array<string> = [];
  TotalAmounts: Array<number> = [];
  year = moment().year();
  label: string = "";
  CurrentObject: any = {

  }

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {

    this.CurrentObject.DateStart = moment(`${this.year.toString()}-01-01 00:00:00`).toDate();
    this.CurrentObject.DateEnd = moment().toDate();

    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;


  async ngOnInit() {

    this.loadRecords();
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    this.loadRecords();
  }

  async loadRecords() {

    var obj = await this.ds.execSP(
      "pGetDashboardTopBilledClientDataSourceByDateCoverage",
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateStart: moment(this.CurrentObject.DateStart).format("yyyy-MM-DD"),
        DateEnd: moment(this.CurrentObject.DateEnd).format("yyyy-MM-DD"),
      },
      {
        isReturnObject: true,
      }
    );

    this.label = obj.Label;
    this.dataSource = obj.DataSource;

    this.TotalAmounts = [];
    this.Items = [];

    this.dataSource.forEach((obj: { TotalAmount: number; }) => {
      this.TotalAmounts.push(obj.TotalAmount)
    });
    this.dataSource.forEach((obj: { Name: string; }) => {
      this.Items.push(obj.Name)
    });

    this.loadChart();
  }

  loadChart() {

    if (this.myChart) this.myChart.destroy();
    var _ = this;

    this.myChart = new Chart("TopBilledClientsChart", {
      type: 'bar',
      data: {
        labels: this.Items,
        datasets: [{
          label: _.label,
          data: this.TotalAmounts,
          backgroundColor: [
            '#82C272',
            '#00A88F',
            '#0087AC',
            '#005FAA',
            '#323B81',
            '#62BEB6',
            '#034D44',
            '#62BEB6',
            '#034D44',
          ],
          borderColor: [
            '#82C272',
            '#00A88F',
            '#0087AC',
            '#005FAA',
            '#323B81',
            '#62BEB6',
            '#034D44',
            '#62BEB6',
            '#034D44',
          ],
          borderWidth: 1
        }]
      },


      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                  return '₱ ' + value.toLocaleString();
                },
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, chart) {
              var yLabel: any = tooltipItem.yLabel;
              var value = 0;

              if (!yLabel) {
                yLabel = '0';
              }

              value = parseInt(yLabel);

              return ' ₱ ' + value.toLocaleString();
            },
          },
        },
      },
    });

    this.myChart.update()
  }

  reset() {

    this.CurrentObject.DateStart = moment(`${this.year.toString()}-01-01 00:00:00`).toDate();
    this.CurrentObject.DateEnd = moment().toDate();

    if (this.adminltedateboxDateStart) this.adminltedateboxDateStart.setValue(this.CurrentObject.DateStart);
    if (this.adminltedateboxDateEnd) this.adminltedateboxDateEnd.setValue(this.CurrentObject.DateEnd);

    this.loadRecords();
  }
}
