import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Employee_DTO, PagingOption } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './employee-list.component.less'
  ],
})
export class EmployeeListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'EmployeeListComponent';

  headerTitle: string = 'Employee';
  groupColumnNames: string[] = ['Name_Position'];
  groupDataSource: any = {};
  groupColumnPropertyNames: string[] = [];

  OrderByString: string = 'Name_Position ASC, DateModified DESC';

  pagingOption: PagingOption = {
    PageNumber: 1,
    DisplayCount: 100,
    TotalRecord: 0,
    TotalPageNumber: 0,
  }

  InitCurrentObject: any = {
    Name: '',
    Name_Position: ''
  }

  dataSource: any[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Employee',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Name,
                    Name_Position
            FROM dbo.[vNonSystemUseEmployeeTest]
            /*encryptsqlend*/
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = ""
    if (this.CurrentObject.ContactNumber == null) this.CurrentObject.ContactNumber = ""

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ContactNumber.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Position LIKE '%${this.CurrentObject.ContactNumber}%'`;
    }


    return filterString;
  }

  dataSource_InitLoad(obj: any) {

    var _ = this;

    _.groupDataSource = {};

    this.dataSource.forEach(data => {

      _.groupColumnNames.forEach(groupColumnName => {

        var value = data[groupColumnName];

        if(!_.groupDataSource.hasOwnProperty(value)){
          _.groupDataSource[value] = [];
        }
        _.groupDataSource[value].push(data);
      });
    });

    this.groupColumnPropertyNames = Object.keys(this.groupDataSource);

    console.log(this.groupDataSource);
    console.log(this.groupColumnPropertyNames);
  }

  Row_OnClick(rowData: Employee_DTO) {

    this.customNavigate(['Employee', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Employee', -1], {});
  }
}
