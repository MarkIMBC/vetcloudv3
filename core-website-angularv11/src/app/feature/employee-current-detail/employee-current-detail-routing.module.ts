import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeCurrentDetailComponent } from './employee-current-detail.component';

const routes: Routes = [{ path: '', component: EmployeeCurrentDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeCurrentDetailRoutingModule { }
