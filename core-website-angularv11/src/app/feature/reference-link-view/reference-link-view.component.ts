import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-reference-link-view',
  templateUrl: './reference-link-view.component.html',
  styleUrls: ['./reference-link-view.component.less'],
})
export class ReferenceLinkViewComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected elRef: ElementRef,
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {
    var Code = this.route.snapshot.params['Code'];

    var objReferenceMenuItems = await this.ds.execSP(
      'pGetReferenceLinkMenuItem',
      { Name: Code },
      {
        isReturnObject: true,
      }
    );

    setTimeout(function () {
      if (objReferenceMenuItems) {
        var obj = objReferenceMenuItems.MenuItems[0];

        document.location.href = obj.URLlink;
      }
    }, 3000);
  }
}
