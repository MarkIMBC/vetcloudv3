import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  forwardRef,
} from '@angular/core';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-passwordbox',
  templateUrl: './admin-lte-passwordbox.component.html',
  styleUrls: ['./admin-lte-passwordbox.component.less'],
  providers: [
    {
      provide: AdminLTEBaseControlComponent,
      useExisting: forwardRef(() => AdminLTEPasswordboxComponent),
    },
  ],
})
export class AdminLTEPasswordboxComponent extends AdminLTEBaseControlComponent {}
