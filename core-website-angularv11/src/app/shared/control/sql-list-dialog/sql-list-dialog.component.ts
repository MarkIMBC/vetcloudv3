import { AdminLTEListOption } from './../admin-lte-list/admin-lte-list.component';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import {
  AdminLTEListComponent,
  AdminLTEListRowSelectedArg,
} from '../admin-lte-list/admin-lte-list.component';

@Component({
  selector: 'sql-list-dialog',
  templateUrl: './sql-list-dialog.component.html',
  styleUrls: ['./sql-list-dialog.component.less'],
})
export class SQLListDialogComponent implements OnInit {
  @ViewChild('listview') listview: AdminLTEListComponent | undefined;
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  @Input() sql: string = '';
  @Input() ismultiple: boolean = true;

  @Output() onRowSelected: EventEmitter<AdminLTEListRowSelectedArg> =
    new EventEmitter<AdminLTEListRowSelectedArg>();

  constructor() {}

  ngOnInit(): void {}

  displayValue: string = '';
  listviewOption!: AdminLTEListOption;
  arg: AdminLTEListRowSelectedArg | undefined;

  async open<AdminLTEListRowSelectedArg>(
    option: AdminLTEListOption
  ): Promise<AdminLTEListRowSelectedArg> {
    var modalDialog = this.modalDialog;
    var listviewOption = this.listviewOption;
    var listview = this.listview;

    this.arg = {
      rows: [],
      values: [],
    };

    var promise = new Promise<any>(async (resolve, reject) => {
      if (!listview) {
        reject();
      } else {
        listviewOption = option;
        listview.selectedValues = [];

        listview.pagingOption.PageNumber = 1;
        listview.loadRecords(listviewOption);

        await modalDialog?.open();
      }

      if (this.arg == undefined) {
        reject();
      } else {
        if (this.arg.values.length > 0) {
          var arg = this.arg;

          resolve(arg);
        } else {
          reject();
        }
      }
    });

    return promise;
  }

  listview_onRowSelected(e: AdminLTEListRowSelectedArg) {
    this.arg = e;

    if (this.listview) {
      this.listview.selectedValues = [];
    }

    this.modalDialog?.close();
  }

  listview_onDataSourceLoaded(e: any) {
    var records = e.records;
  }
}
