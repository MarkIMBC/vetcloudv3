import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SOAPRecordSummaryViewComponent } from './soaprecord-summary-view.component';

describe('SOAPRecordSummaryViewComponent', () => {
  let component: SOAPRecordSummaryViewComponent;
  let fixture: ComponentFixture<SOAPRecordSummaryViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SOAPRecordSummaryViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SOAPRecordSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
