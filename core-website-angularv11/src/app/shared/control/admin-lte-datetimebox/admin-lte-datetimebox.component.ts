import { Component, forwardRef, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-datetimebox',
  templateUrl: './admin-lte-datetimebox.component.html',
  styleUrls: [
    './../admin-lte-base-control/admin-lte-base-control.component.less',
    './admin-lte-datetimebox.component.less'
  ],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEDateTimeboxComponent) }]
})
export class AdminLTEDateTimeboxComponent extends AdminLTEBaseControlComponent{

  @Input() format: string = 'MM-dd-yyyy';
  @Input() valueformat: string = 'yyyy-MM-DDTHH:mm';

  _min: string = '';
  @Input() set min(val: string) {

    if(val == null || val == '') return;

    this._min = moment(val).format(this.valueformat);
  }

  @Input() set initialValue(val: any) {

    if(val == null || val == '') return;

    this.setValue(val);
  }

  setValue(val: any){

    this.value = moment(val).format(this.valueformat);
  }
}
