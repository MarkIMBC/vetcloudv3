import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import {
  FilingStatusEnum,
  PatientWaitingList_DTO,
  Patient_DTO,
} from 'src/shared/APP_HELPER';

@Component({
  selector: 'btn-change-waiting-status',
  templateUrl: './btn-change-waiting-status.component.html',
  styleUrls: ['./btn-change-waiting-status.component.less'],
})
export class BtnChangeWaitingStatusComponent implements OnInit {
  selectedRecord: PatientWaitingList_DTO | undefined;
  currentUser: TokenSessionFields = new TokenSessionFields();
  menuItems: any[] = [];
  selectedMenuItem: any;

  defaultMenuItems: any[] = [
    {
      ID: FilingStatusEnum.Waiting,
      Name: 'Waiting',
    },
    {
      ID: FilingStatusEnum.Ongoing,
      Name: 'Ongoing',
    },
    {
      ID: FilingStatusEnum.Done,
      Name: 'Done',
    },
    {
      ID: FilingStatusEnum.Cancelled,
      Name: 'Cancelled',
    },
  ];

  @Input() set Value(val: PatientWaitingList_DTO) {
    if (val == null || val == undefined) return;

    this.selectedRecord = val;

    this.loadMenuItems();
  }

  @Output() onUpdatingPatientWaitingStatus: EventEmitter<any> =
    new EventEmitter<any>();
  @Output() onUpdatedPatientWaitingStatus: EventEmitter<any> =
    new EventEmitter<any>();

  loadMenuItems() {
    this.selectedMenuItem = null;

    this.menuItems = [];

    this.defaultMenuItems.forEach((menuItem) => {
      if (this.selectedRecord?.WaitingStatus_ID_FilingStatus != menuItem.ID) {
        this.menuItems.push(menuItem);
      } else {
        this.selectedMenuItem = menuItem;
      }
    });
  }

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  ngOnInit(): void {}

  menuItem_onClick(menuItem: any) {
    this.UpdatePatientWaitingStatus(menuItem);
  }

  async UpdatePatientWaitingStatus(menuItem: any): Promise<any> {
    var WaitingStatus_ID_FilingStatus: number = 0;

    if (this.selectedRecord != undefined) {
      if (this.selectedRecord.ID)
        WaitingStatus_ID_FilingStatus = this.selectedRecord.ID;
    }

    this.onUpdatingPatientWaitingStatus.emit({
      loading: true,
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pUpdatePatientToWaitingListStatus',
        {
          IDs_PatientWaitingList: [WaitingStatus_ID_FilingStatus],
          WaitingStatus_ID_FilingStatus: menuItem.ID,
          DateReschedule: null,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.onUpdatedPatientWaitingStatus.emit({
          Success: obj.Success,
          message: `${this.selectedRecord?.Name_Patient} has been "${this.selectedMenuItem.Name}" to waiting list.`,
        });

        this.onUpdatingPatientWaitingStatus.emit({
          loading: false,
        });
        res(obj);
      } else {
        this.onUpdatedPatientWaitingStatus.emit({
          Success: obj.Success,
          message: obj.message,
        });

        this.onUpdatingPatientWaitingStatus.emit({
          loading: false,
        });

        rej(obj);
      }
    });
  }
}
