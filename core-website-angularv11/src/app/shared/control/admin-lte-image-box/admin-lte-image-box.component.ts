import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg, IFile } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'admin-lte-image-box',
  templateUrl: './admin-lte-image-box.component.html',
  styleUrls: ['./admin-lte-image-box.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEImageBoxComponent) }]
})
export class AdminLTEImageBoxComponent extends AdminLTEBaseControlComponent {

  file: any = null;
  imagelocation: any;

  @Input() set initialValue(val: any) {

    if (val == null || val == '') val = "";

    var url: string = DataService.API_URL;

    this.value = val;

    if(this.value == null) this.value = "";
    if(this.value.length > 0)
    {
      this.imagelocation = `${url}Image/GetThumbnail/${this.value}`;
    }
  }

  readURL(event: any) {

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.imagelocation = event.target.result;
      }

      reader.readAsDataURL(event.target.files[0]);

      this.value = event.target.files[0].name;
      this.file = event.target.files[0];
    }

    this.emitModelChanged({
      name: this.name,
      value: this.value
    });
  }

  btnClose_onClick() {

    this.removeValue()
  }

  removeValue() {

    this.file = null;
    this.value = null;
    this.imagelocation = null;

    this.emitModelChanged({
      name: this.name,
      value: this.value
    });
  }

  Image_onClick(event: any) {

    var url = DataService.API_URL + `Image/GetImage/${this.value}`;
    var win = window.open(url, '_blank');
    if (win) {

      win.focus();
    }

  }

  getFile(): IFile {

    return {
      dataField: this.name,
      file: this.file,
      isImage: true,
    }
  }

  private emitModelChanged(arg: IControlModelArg) {

    this.onModelChanged.emit({
      name: arg.name,
      value: arg.value
    });
  }
}
