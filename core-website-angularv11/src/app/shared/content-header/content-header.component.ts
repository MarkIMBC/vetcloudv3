import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';

@Component({
  selector: 'content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.less'],
})
export class ContentHeaderComponent implements OnInit {
  @Input() customTitleTemplate!: TemplateRef<any>;
  @Input() breadCrumbItems!: BreadCrumbItem[];
  @Output() onBreadCrumbClicked: EventEmitter<BreadCrumbItem> =
    new EventEmitter();

  @Input()
  title: string = '';

  constructor() {}

  ngOnInit(): void {}

  breadCrumb_onClicked(breadCrumbItem: BreadCrumbItem): void {
    this.onBreadCrumbClicked.emit(breadCrumbItem);
  }
}

export class BreadCrumbItem {
  label: string = '';
  icon?: string = '';
  class?: string = '';
  visible: boolean = true;
  routerLink?: any = '';
  name?: string = '';
  isActive?: boolean = false;
}
