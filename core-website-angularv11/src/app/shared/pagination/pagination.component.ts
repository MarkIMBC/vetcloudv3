import { PagingOption } from 'src/shared/APP_HELPER';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent implements OnInit {

   _TotalPageNumber: number = 0;
  pagesCount: number[] = []

  @Input() currentPageNumber: number = 1;

  @Input() set TotalPageNumber(value: number) {

    this._TotalPageNumber = value;

    this.pagesCount = [];

    for (let i = 1; i <= this._TotalPageNumber; i++) {

      this.pagesCount.push(i);
    }
  }

  @Output() pageNumber_OnClick: EventEmitter<any> = new EventEmitter();

  _pageNumber_OnClick(e: number){

    this.pageNumber_OnClick.emit({
      page: e
    });
  }


  btnFistPage_onClicked() {

    this.currentPageNumber = 1

    this.pageNumber_OnClick.emit({
      page: this.currentPageNumber
    });
  }

  btnLastPage_onClicked() {

    this.currentPageNumber = this._TotalPageNumber;

    this.pageNumber_OnClick.emit({
      page: this.currentPageNumber
    });
  }

  btnPrev_onClicked() {

    if(this.currentPageNumber > 1) this.currentPageNumber--;

    this.pageNumber_OnClick.emit({
      page: this.currentPageNumber
    });
  }

  btnNext_onClicked() {

    if(this.currentPageNumber < this._TotalPageNumber) this.currentPageNumber++;

    this.pageNumber_OnClick.emit({
      page: this.currentPageNumber
    });
  }

  _selectBox_ModelChange() {
    this.pageNumber_OnClick.emit({
      page: this.currentPageNumber
    });
  }

  constructor() { }

  ngOnInit(): void {


  }

}
