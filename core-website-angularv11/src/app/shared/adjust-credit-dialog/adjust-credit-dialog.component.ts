import { AdminLTEDataLookupboxComponent } from './../control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  UserAuthenticationService,
  TokenSessionFields,
} from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  ClientCredit,
  FilingStatusEnum,
  IControlModelArg,
  IFormValidation,
  PropertyTypeEnum,
  ReceiveInventory,
} from 'src/shared/APP_HELPER';
import { APP_MODEL } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from '../control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'adjust-credit-dialog',
  templateUrl: './adjust-credit-dialog.component.html',
  styleUrls: ['./adjust-credit-dialog.component.less'],
})
export class AdjustCreditDialogComponent implements OnInit {
  @ViewChild('modalDialog1') modalDialog: ModalComponent | undefined;
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  loading: boolean = false;
  IsDeposit: boolean = true;
  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {};
  Caption: string = 'Deposit';

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  private _ID_Client: number = 0;
  private _ID_Patient_Confinement: number = 0;

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(ID_Client: number): Promise<any> {
    var modalDialog = this.modalDialog;

    this._ID_Client = ID_Client;
    this._ID_Patient_Confinement = 0;
    this.loading = true;

    this.loadRecord();

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        this.loading = false;
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  async showByPatientConfinement(ID_Patient_Confinement: number): Promise<any> {
    var modalDialog = this.modalDialog;

    if (this.ID_Patient_Lookupbox) {
      this.ID_Patient_Lookupbox.displayValue = '';
    }

    this._ID_Patient_Confinement = ID_Patient_Confinement;
    var obj = await this.loadPatient_Confinement();

    this._ID_Client = obj.ID_Client;

    this.loading = true;

    this.loadRecord();

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        this.loading = false;
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  async loadRecord(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pGetClient',
        {
          ID: this._ID_Client,
          ID_Session: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
        }
      );

      this.CurrentObject = obj;
      this.CurrentObject.CreditAmount = 0;
      this.CurrentObject.Comment = "N/A"

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                          SELECT
                                ID,
                                Name,
                                Name_Client
                          FROM dbo.vPatient
                          WHERE
                                ID_Company = ${this.currentUser.ID_Company} AND
                                ID_Client = ${this.CurrentObject.ID}
                          /*encryptsqlend*/`;
    });
  }

  async loadPatient_Confinement(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pGetPatient_Confinement',
        {
          ID: this._ID_Patient_Confinement,
          ID_Session: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
        }
      );

      res(obj);
    });
  }

  btnDepositWithdraw_onClick() {
    if (this._ID_Patient_Confinement > 0) return;

    this.IsDeposit = this.IsDeposit != true;

    this.Caption = this.IsDeposit ? 'Deposit' : 'Withdraw';
  }

  CreditAmount_onModelChanged(e: IControlModelArg) {
    this.CurrentObject.CreditAmount = e.value;
  }

  Comment_onModelChanged(e: IControlModelArg) {
    this.CurrentObject.Comment = e.value;
  }

  ID_Patient_Lookupbox_onModelChanged(e: IControlModelArg) {
    this.CurrentObject.ID_Patient = e.value;
  }

  private async validateRecord() {
    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if (this.IsDeposit) {
      if (this.CurrentObject.CreditAmount == 0) {
        validationsAppForm.push({
          message: 'Credit Amount is required.',
        });
      }
    }

    if (this.IsDeposit != true) {
      if (this.CurrentObject.CreditAmount < 1) {
        validationsAppForm.push({
          message: 'Credit Amount is must be greater than zero.',
        });
      }
    }

    if (this.CurrentObject.ID_Patient == 0 || this.CurrentObject.ID_Patient == null) {
      validationsAppForm.push({
        message: 'Patient is required.',
      });
    }

    if (this.CurrentObject.Comment.length == 0) {
      validationsAppForm.push({
        message: 'Note is required.',
      });
    }

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  async btnAdjust_onClick() {
    var isvalid;

    if (!this.CurrentObject['CreditAmount'])
      this.CurrentObject.CreditAmount = 0;
    if (!this.CurrentObject['Comment']) this.CurrentObject.Comment = '';

    this.CurrentObject.CreditAmount = Math.abs(this.CurrentObject.CreditAmount);

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    this.loading = true;

    try {
      if (this.IsDeposit) {
        await this.depositClientCredit();
      } else {
        await this.withdrawClientCredit();
      }
    } catch (error) {}

    this.loading = false;

    if (this.modalDialog != undefined) {
      this.modalDialog.close();
    }
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  async depositClientCredit(): Promise<any> {
    var clientCredits: ClientCredit[] = [];
    var creditAmount: number = this.CurrentObject.CreditAmount;

    creditAmount = this.IsDeposit
      ? this.CurrentObject.CreditAmount
      : 0 - this.CurrentObject.CreditAmount;

    clientCredits.push({
      ID_Client: this._ID_Client,
      Date: moment().format('MM-DD-YYYY HH:mm:ss'),
      CreditAmount: creditAmount,
      Code: `${this.Caption}  Credit`,
      Comment: this.CurrentObject.Comment,
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj;
      var ID_ClientDeposit;

      ID_ClientDeposit = await this.saveClientDeposit();
      obj = await this.approveClientDeposit(ID_ClientDeposit);

      if (obj.Success) {
        this.toastService.success(`${this.Caption}  successfully.`);
        res(obj);
      } else {
        this.toastService.danger(obj.message);
        rej(obj);
      }
    });
  }

  async saveClientDeposit(): Promise<number> {
    var currentObject: any;
    var previousObject: any;

    currentObject = await this.ds.execSP(
      `pGetClientDeposit`,
      {},
      {
        isReturnObject: true,
      }
    );

    previousObject = this.cloneObject(currentObject);
    currentObject['ID_Company'] = this.currentUser.ID_Company;
    currentObject['ID_Client'] = this.CurrentObject.ID;
    currentObject['ID_Patient'] = this.CurrentObject.ID_Patient;
    currentObject['Date'] = new Date();
    currentObject['DepositAmount'] = this.CurrentObject.CreditAmount;
    currentObject['Comment'] = this.CurrentObject.Comment;
    currentObject['ID_Patient_Confinement'] = this._ID_Patient_Confinement;

    console.log(currentObject);

    var r = await this.ds.saveObject(
      APP_MODEL.CLIENTDEPOSIT,
      currentObject,
      previousObject,
      [],
      this.currentUser
    );
    var id = (r.key + '').replace("'", '');
    var ID_CurrentObject = parseInt(id);

    return ID_CurrentObject;
  }

  async approveClientDeposit(ID_ClientDeposit: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pApproveClientDeposit',
        {
          IDs_ClientDeposit: [ID_ClientDeposit],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        rej(obj);
      }
    });
  }

  async withdrawClientCredit(): Promise<any> {
    var clientCredits: ClientCredit[] = [];
    var creditAmount: number = this.CurrentObject.CreditAmount;

    creditAmount = this.IsDeposit
      ? this.CurrentObject.CreditAmount
      : 0 - this.CurrentObject.CreditAmount;

    clientCredits.push({
      ID_Client: this._ID_Client,
      Date: moment().format('MM-DD-YYYY HH:mm:ss'),
      CreditAmount: creditAmount,
      Code: `${this.Caption}  Credit`,
      Comment: this.CurrentObject.Comment,
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj: any = {
        Success: false,
        message: '',
      };
      var ID_ClientWithdraw;

      try {
        ID_ClientWithdraw = await this.saveClientWithdraw();
        obj = await this.approveClientWithdraw(ID_ClientWithdraw);
      } catch (error) {
        obj = error;
      }

      if (obj.Success) {
        this.toastService.success(`${this.Caption}  successfully.`);
        res(obj);
      } else {
        this.toastService.danger(obj.message);
        rej(obj);
      }
    });
  }

  async saveClientWithdraw(): Promise<number> {
    var currentObject: any;
    var previousObject: any;

    currentObject = await this.ds.execSP(
      `pGetClientWithdraw`,
      {},
      {
        isReturnObject: true,
      }
    );

    previousObject = this.cloneObject(currentObject);
    currentObject['ID_Company'] = this.currentUser.ID_Company;
    currentObject['ID_Client'] = this.CurrentObject.ID;
    currentObject['Date'] = new Date();
    currentObject['WithdrawAmount'] = this.CurrentObject.CreditAmount;
    currentObject['Comment'] = this.CurrentObject.Comment;
    currentObject['ID_Patient_Confinement'] = this._ID_Patient_Confinement;

    var r = await this.ds.saveObject(
      APP_MODEL.CLIENTWITHDRAW,
      currentObject,
      previousObject,
      [],
      this.currentUser
    );
    var id = (r.key + '').replace("'", '');
    var ID_CurrentObject = parseInt(id);

    return ID_CurrentObject;
  }

  async approveClientWithdraw(ID_ClientWithdraw: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pApproveClientWithdraw',
        {
          IDs_ClientWithdraw: [ID_ClientWithdraw],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        rej(obj);
      }
    });
  }

  cloneObject = (inObject: any) => {
    var outObject: any, value, key;

    outObject;

    if (typeof inObject !== 'object' || inObject === null) {
      return inObject; // Return the value if inObject is not an object
    }

    // Create an array or object to hold the values
    outObject = Array.isArray(inObject) ? [] : {};

    for (key in inObject) {
      value = inObject[key];

      if (outObject[key] != undefined) {
        // Recursively (deep) copy for nested objects, including arrays
        outObject[key] = this.cloneObject(value);
      }
    }

    return outObject;
  };
}
