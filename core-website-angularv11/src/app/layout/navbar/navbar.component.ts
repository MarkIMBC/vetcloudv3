import {
  TokenSessionFields,
  UserAuthenticationService,
} from './../../core/UserAuthentication.service';
import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/data.service';
import { CrypterService } from 'src/app/core/crypter.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less'],
})
export class NavbarComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  IsShowPaymentWarningLabel: boolean = false;

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    protected cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.ds.loadConfig();

    this.currentUser = await this.userAuth.getDecodedToken();

    var queryString = this.cs.encrypt(`
                      SELECT
                        Name, IsShowPaymentWarningLabel
                      FROM vCompany
                      WHERE
                        ID IN (${this.currentUser.ID_Company})
    `);

    var records = await this.ds.query<any>(queryString);
    this.IsShowPaymentWarningLabel = records[0]['IsShowPaymentWarningLabel'];
  }

  async menuItem_onClick() {
    this.userAuth.LogOut();

    setTimeout(() => {
      window.localStorage.clear();
      window.location.href = './Login';
    }, 5000);
  }
}
