import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseLIstComponent } from './base-list.component';

describe('BaseLIstComponent', () => {
  let component: BaseLIstComponent;
  let fixture: ComponentFixture<BaseLIstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseLIstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseLIstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
