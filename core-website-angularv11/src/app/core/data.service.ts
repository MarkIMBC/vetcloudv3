import { Guid } from 'guid-typescript';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { IUserSession, TokenSessionFields, UserAuthenticationService } from './UserAuthentication.service';
import { CrypterService } from './crypter.service';
import { IFile } from 'src/shared/APP_HELPER';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public appVersion: string | undefined;
  public static API_URL: string = 'localurl';
  public static IsLoadedConfig: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private http: HttpClient, public crypter: CrypterService) {}

  loadObject<T>(
    Oid_Model: string,
    ID_CurrentObject: string,
    params?: any
  ): Promise<T> {
    if (ID_CurrentObject == undefined) ID_CurrentObject = '-1';
    if (params == undefined || params == null) params = {};
    params.ID_CurrentObject = ID_CurrentObject;
    return this.post<T>(`Model/LoadObject/${ID_CurrentObject}`, {
      Oid_Model: Oid_Model,
      Params: params ? JSON.stringify(params) : null,
    });
  }

  query<T>(sql: string): Promise<T[]> {
    return this.GetPagingList<T[]>('Model/Query3', {
      SQL: sql,
    });
  }

  isNullOrUndefined<T>(obj: T | null | undefined): obj is null | undefined {
    return typeof obj === 'undefined' || obj === null;
  }

  execSP(spName: string, params?: any, options?: any): Promise<any> {
    var isReturnObject: boolean = false;

    if (this.isNullOrUndefined(options) != true) {
      if (this.isNullOrUndefined(options) == true) {
        isReturnObject = false;
      } else {
        isReturnObject = options.isReturnObject;
      }
    }

    var isTransaction: boolean = false;

    if (this.isNullOrUndefined(options) != true) {
      if (this.isNullOrUndefined(options) == true) {
        isTransaction = false;
      } else {
        isTransaction = options.isTransaction;
      }
    }

    var _arg: string = '';

    _arg = this.isNullOrUndefined(params) != true ? JSON.stringify(params) : '';
    _arg = this.crypter.encrypt(_arg);

    return this.postAsync(`Model/ExecuteSP2`, {
      args: _arg,
      isReturnObject: isReturnObject,
      isTransaction: isTransaction,
      spName: this.crypter.encrypt(spName),
    });
  }

  sendtext(CurrentObject: any): Promise<any> {
    var form = new FormData();

    var strdata = JSON.stringify({
      CellPhoneNumbers: CurrentObject.CellPhoneNumbers,
      Message: CurrentObject.Message,
    });

    form.append('data', JSON.stringify(strdata));

    return this.post(`Model/SendText`, form);
  }

  private _convertObjectPropDateToString(obj: any): void {
    var dateFormat = [moment.ISO_8601, 'YYYY-MM-DD HH:mm:ss'];

    for (let key in obj) {
      let value = obj[key];

      if (moment(value, dateFormat, true).isValid()) {
        var _val = moment(value).format('YYYY-MM-DD HH:mm:ss');

        obj[key] = _val;
      } else if (Object.prototype.toString.call(value) == '[object Object]') {
        this._convertObjectPropDateToString(value);
      } else if (Object.prototype.toString.call(value) == '[object Array]') {
        value.forEach((_val: any, i: number) => {
          this._convertObjectPropDateToString(_val);
        });
      }
    }
  }

  private async getModelDateColumns(Oid_Model: any): Promise<any> {
    var obj = await this.execSP(
      'pGetModelDateColumns',
      {
        Oid_Model: Oid_Model,
      },
      {
        isReturnObject: true,
      }
    );

    return obj;
  }

  private async formatObjectDateValue(Oid_Model: any) {
    var obj: any = await this.getModelDateColumns(Oid_Model);

    var propertyList: any[] = [];
    var dateColumns: any[] = [];

    if (obj['PropertyList']) {
      obj.PropertyList.forEach((Obj: any) => {
        propertyList.push(Obj.ParentName);
      });
    }

    if (obj['DateColumns']) {
      obj.DateColumns.forEach((Obj: any) => {
        dateColumns.push(Obj.DateColumnName);
      });
    }

    var result = {
      propertyList: propertyList,
      dateColumns: dateColumns,
    };

    return result;
  }

  convertDateToString = (formatObj: any, Obj: any): any => {
    var propertyList: any[] = formatObj.propertyList;
    var dateColumns: any[] = formatObj.dateColumns;

    for (const property in Obj) {
      var hasOnPRopertyList = propertyList.includes(property);

      if (hasOnPRopertyList == true) {
        if (Obj[property] == null) Obj[property] = [];

        Obj[property].forEach((detail: any) => {
          this.convertDateToString(formatObj, detail);
        });
      } else {
        if (dateColumns.includes(property)) {
          var val = Obj[property];

          if (val != null && val != undefined) {
            if (val.replace != undefined) {
              val = val.replace('T', ' ');
            }
          }

          var _val = moment(val).format('YYYY-MM-DD HH:mm:ss');

          if (_val != 'Invalid date') Obj[property] = _val;
        }
      }
    }

    return Obj;
  };

  async saveObject(
    Oid_Model: any,
    CurrentObject: any,
    TempObject?: any,
    files?: IFile[],
    currentUser?: IUserSession
  ): Promise<any> {
    var form = new FormData();

    var _CurrentObject = Object.assign({}, CurrentObject);
    var _TempObject = Object.assign({}, TempObject);
    var FileDetails: any[] = [];

    var formatDateObj = await this.formatObjectDateValue(Oid_Model);

    _CurrentObject = this.convertDateToString(formatDateObj, _CurrentObject);
    _TempObject = this.convertDateToString(formatDateObj, _TempObject);

    if (files) {
      files.forEach((f, i: number) => {
        var fileId = Guid.create().toString();
        var fileName = f.file.name;
        form.append('files', f.file, `${fileId}`);
        FileDetails.push({
          FileName: fileName,
          DataField: f.dataField,
          FileId: fileId,
          IsImage: f.isImage,
        });
      });
    }

    var obj = {
      CurrentObject: JSON.stringify(_CurrentObject),
      PreviousObject: _TempObject
        ? JSON.stringify(_TempObject)
        : JSON.stringify({}),
      UploadFileDetails: JSON.stringify(FileDetails),
      CurrentUser: JSON.stringify(currentUser),
    };

    form.append('param', this.crypter.encrypt(JSON.stringify(obj)));

    return this.post(`Model/Save2/${Oid_Model}`, form);
  }

  UploadFiles(Oid_Model: any, files?: IFile[]): Promise<any> {
    var form = new FormData();
    var FileDetails: any[] = [];

    var _CurrentObject: any = Object.assign({}, { ID: 1 });
    var _TempObject: any = Object.assign({}, { ID: 1 });

    this._convertObjectPropDateToString(_CurrentObject);
    this._convertObjectPropDateToString(_TempObject);

    form.append('CurrentObject', JSON.stringify(_CurrentObject));
    form.append(
      'PreviousObject',
      _TempObject != null ? JSON.stringify(_TempObject) : ''
    );

    if (files) {
      files.forEach((f, i: number) => {
        var fileId = Guid.create().toString();
        var fileName = f.file.name;
        form.append('files', f.file, `${fileId}`);
        FileDetails.push({
          FileName: fileName,
          DataField: f.dataField,
          FileId: fileId,
          IsImage: f.isImage,
        });
      });
    }
    form.append('UploadFileDetails', JSON.stringify(FileDetails));
    return this.post(`Model/UploadFiles/${Oid_Model}`, form);
  }

  GetPagingList<T>(url: string, opt: IPagingOption): Promise<any> {
    return this.post<T>(url, opt);
  }

  loadConfig(): Promise<any> {
    const promise = this.http
      .get('/assets/app.config.json?q=' + new Date().getTime())
      .toPromise()
      .then((data: any) => {
        DataService.API_URL = data.apiUrl;
        DataService.IsLoadedConfig = true;
        return data;
      });
    return promise;
  }

  async post<T>(url: string, opt: any): Promise<T> {
    if (DataService.API_URL == 'localurl') {
      console.log('loading config');
      await this.loadConfig();
    }

    var _ = this;
    var result;

    var _url = DataService.API_URL + url;
    result = await this.http.post<T>(_url, opt).toPromise<T>();

    return result;
  }

  async postAsync<T>(url: string, opt: any): Promise<T> {

    if (DataService.API_URL == 'localurl') {

      if(!DataService.IsLoadedConfig) await this.loadConfig();
    }

    var _ = this;
    var result;

    var _url = DataService.API_URL + url;
    result = this.http.post<T>(_url, opt).toPromise<T>();

    return result;
  }

  async get<T>(url: string): Promise<T> {
    if (DataService.API_URL == 'localurl') {
      console.log('loading config');
      await this.loadConfig();
    }

    const headers = new HttpHeaders()
      .set('Cache-Control', 'no-cache')
      .set('Pragma', 'no-cache');

    var _ = this;
    var result: any;
    try {
      var _url = DataService.API_URL + url;
      result = await this.http.get<T>(_url, { headers }).toPromise<T>();
    } catch (error) {
      var fxTimeOut = await setTimeout(async () => {
        console.log('reloading configration');

        clearTimeout(fxTimeOut);
        result = await _.get(url);
      }, 3000);
    }

    return result;
  }
}

export class IPagingOption {
  Skip?: number = 0;
  Take?: number = 20;
  PrimaryKey?: string = 'ID';
  SQL?: string = '';
  Value?: string;
  FilterOption?: string;
  SummaryOption?: string;
  SortingOption?: string;
  GroupOptions?: string;
  SearchValue?: string;
  ActiveColumns?: string[];
  filterFormValue?: string;
}

export class ISaveObjectParam {
  CurrentObject: any;
  PreviousObject: any;
}
