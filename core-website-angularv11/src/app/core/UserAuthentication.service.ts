import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';
import { CrypterService } from './crypter.service';
import { DataService } from './data.service';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { promise } from 'protractor';
@Injectable({
  providedIn: 'root',
})
export class UserAuthenticationService {
  constructor(
    private dataService: DataService,
    private ck: CrypterService,
    private router: Router
  ) {}

  isNullOrUndefined<T>(obj: T | null | undefined): obj is null | undefined {
    return typeof obj === 'undefined' || obj === null;
  }

  LogIn(username: string, password: string): Promise<IUserSession> {
    if (password) {
      if (password.length > 0) {
        password = this.ck.encrypt(password);
      }
    }

    return new Promise<IUserSession>((res, rej) => {
      this.dataService
        .post('Login/Token', {
          Username: username,
          Password: password,
        })
        .then((r: any) => {
          window.localStorage.setItem('STORTKN', r.Token);
          window.localStorage.setItem('STORDNTL', r.EncryptToken);
          res(r);

          return Promise.resolve(r);
        })
        .catch((err) => {
          rej(err);
          return Promise.reject(err);
        });
    });
  }

  LogInByUrl(text: string): Promise<IUserSession> {

    return new Promise<IUserSession>((res, rej) => {
      this.dataService
        .post(`Login/LoginByUrl/`, {
          text: text
        })
        .then((r: any) => {
          window.localStorage.setItem('STORTKN', r.Token);
          window.localStorage.setItem('STORDNTL', r.EncryptToken);
          res(r);

          return Promise.resolve(r);
        })
        .catch((err) => {
          rej(err);
          return Promise.reject(err);
        });
    });
  }

  async LogOut(): Promise<IUserSession> {
    await this.dataService.loadConfig();

    var session = this.getDecodedToken();
    var ID_Session = session.ID_UserSession;

    return new Promise<IUserSession>((res, rej) => {
      this.dataService
        .get('Login/LogOut/' + ID_Session)
        .then((r) => {
          window.localStorage.clear();
          window.location.href = './Login';
          return Promise.resolve(res);
        })
        .catch((err) => {
          return Promise.resolve(err);
        });
    });
  }

  async CheckSession(): Promise<ICheckSession> {
    var session = this.getDecodedToken();

    var res: ICheckSession = {
      ID: -1,
      ID_Warehouse: -1,
      IsValid: false,
    };
    if (session == null) {
      return Promise.resolve(res);
    }

    return new Promise<ICheckSession>((resolve, reject) => {
      this.dataService.get(`Login/CheckSession`).then(
        (d) => {
          return Promise.resolve(res);
        },
        (r) => {
          return Promise.reject(res);
        }
      );
    });
  }

  async CheckSessionByPost(): Promise<ICheckSession> {
    return await this.dataService.post(`Login/CheckSession`, {});
  }

  getCurrentSession(): IUserSession {
    var str: any = '';
    str = window.localStorage.getItem('STORDNTL');

    if (this.isNullOrUndefined(str) == true) return new IUserSession();
    var user = JSON.parse(str) as IUserSession;

    return user;
  }

  getDecodedToken(): TokenSessionFields {
    var str: string = '';

    var STORDNTL = window.localStorage.getItem('STORDNTL');

    if (STORDNTL != null) {
      str = STORDNTL;
    }
    str = this.ck.decrypt(str);

    try {
      var obj: TokenSessionFields = JSON.parse(str);

      obj.ID_UserSession = obj.ID_Session;

      return obj;
    } catch (Error) {
      return new TokenSessionFields();
    }
  }
  async CheckAndLogOutByUserSessionStatus(): Promise<boolean> {
    await this.dataService.loadConfig();

    var isActive = true;
    var currentUser: TokenSessionFields = this.getDecodedToken();

    var params: any = {
      ID_UserSession: currentUser.ID_UserSession,
    };
    var _arg: string = '';

    _arg = this.isNullOrUndefined(params) != true ? JSON.stringify(params) : '';
    _arg = this.ck.encrypt(_arg);

    try {

      var obj: any = await this.dataService.post(`Model/ExecuteSP2`, {
        args: _arg,
        isReturnObject: true,
        isTransaction: false,
        spName: this.ck.encrypt('pGetLoggedUserSession'),
      });

      if (
        currentUser.ID_User == undefined ||
        currentUser.ID_User == null ||
        obj['IsActive'] != true
      ) {
        await this.LogOut();
        isActive = false;
      }

      if (isActive) {
        isActive = await this.validateCurrentRouteLink();
      }
    }
    catch (error) {
      isActive = true;
    }

    return isActive;
  }

  private async validateCurrentRouteLink(): Promise<boolean> {
    var url = this.router.url;
    var isActive = true;
    var weblinkParts = url.split('/');
    var currentRouteLink = '';
    var currentUser: TokenSessionFields = this.getDecodedToken();

    if (weblinkParts.length == 0) return isActive;

    if (weblinkParts[1]) {
      currentRouteLink = weblinkParts[1];
    }
    console.log(weblinkParts);

    if (currentRouteLink.length == 0) return true;
    if (currentRouteLink.length > 2) return true;

    var params: any = {
      RouteLink: currentRouteLink,
      ID_UserSession: currentUser.ID_UserSession,
    };
    var _arg: string = '';
    _arg = this.isNullOrUndefined(params) != true ? JSON.stringify(params) : '';
    _arg = this.ck.encrypt(_arg);

    var obj: any = await this.dataService.post(`Model/ExecuteSP2`, {
      args: _arg,
      isReturnObject: true,
      isTransaction: false,
      spName: this.ck.encrypt('pCheckModuleAccessByRouterLink'),
    });

    if (
      currentUser.ID_User == undefined ||
      currentUser.ID_User == null ||
      obj['IsValid'] != true
    ) {
      this.router.navigate(['./NoAccess']);
      isActive = false;
    }

    return isActive;
  }
}

export class IUserSession {
  ID?: number;
  ID_Employee?: number;
  FirstName?: string;
  LastName?: string;
  ID_UserSession?: number;
  Token?: string;
}

export class TokenSessionFields {
  ID_Company?: number;
  ID_User?: number;
  ID_UserGroup?: number;
  ID_Employee?: number;
  LastName?: string;
  FirstName?: string;
  Name_Employee?: string;
  ID_Position?: number;
  ID_Session?: number;
  ID_UserSession?: number;
  Password?: string;
}

export class ICheckSession {
  ID?: number;
  ID_Warehouse?: number;
  IsValid?: boolean;
}
