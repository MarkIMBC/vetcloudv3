﻿using JSLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;

namespace SMSSender
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic appSettings = getAppSettings();

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd;

            Console.WriteLine("Vet Cloud SMS Sender Service run as of {0}", DateTime.Now.ToString("MM/dd/yyyy ddd hh:mm tt"));

            try
            {

                sendSMS(appSettings);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            dateEnd = DateTime.Now;

            System.TimeSpan diff2 = dateEnd - dateStart;

            Console.WriteLine("Done.. Duration - {0}", diff2.ToString("hh\\:mm\\:ss"));
            Thread.Sleep(5000);
        }

        public static void sendSMS(dynamic appSettings)
        {
            string connectonString = appSettings.SystemManagementConnectionString;
            string apiCode = appSettings.ITexMoAPICode;
            string apiPassword = appSettings.ITexMoAPIPassword;
            string sendYesterdaySOAPPlanStoredProcedure = appSettings.SendYesterdaySOAPPlanStoredProcedure;

            List<int> soapPlan = new List<int>();

            var dbAccess = new DBCollection(connectonString);

            var DetailView = dbAccess.GetDynamicObject(sendYesterdaySOAPPlanStoredProcedure, true);

            var records = DetailView.records as List<dynamic>;

            if (records != null)
            {
                foreach (var d in records)
                {
                    var isAllowedObj = dbAccess.GetDynamicObject("EXEC dbo.pCheckCompanySMSSending " + d.ID_Company + ", '" + d.DateSending + "'", true);
                    var result = "";

                    if (isAllowedObj.IsAllowedToSendSMS)
                    {
                        result = JSLibrary.Common.Utility.sendTextMessage(d.ContactNumber_Client, d.Message, apiCode, apiPassword);
                    }
                    else
                    {
                        result = "-1";
                    }

                    Console.WriteLine("Status {0} - {1} - {2} - {3} TotaSMSCount: {4}; MaxSMSCountPerDay: {5}; Date Sent: {6}", result, d.Name_Client, d.ContactNumber_Client, d.Name_Company, isAllowedObj.TotalSMSCount, isAllowedObj.MaxSMSCountPerDay, d.DateSending);

                    var _result = new DBCollection(connectonString).GetDynamicObject("EXEC dbo.pNoteSOAPPlanAsSend @ID_Reference = " + d.ID_Reference + ", @Oid_Model = '" + d.Oid_Model + "', @iTextMo_Status = " + result + "", true);
                }
            }
        }

        public static Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }

        public static dynamic getAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader(@$"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }
    }
}

