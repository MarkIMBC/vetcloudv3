import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { CurrentObjectOnValueChangeArg } from 'src/app/layout/base-detail-view/base-detail-view.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  ReceiveInventory,
  FilingStatusEnum,
  IControlModelArg,
  IFormValidation,
} from 'src/shared/APP_HELPER';

@Component({
  selector: 'password-user-prompt',
  templateUrl: './password-user-prompt.component.html',
  styleUrls: ['./password-user-prompt.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PasswordUserPromptComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {
    Code_Company: '',
    Username: '',
    NewPassword: '',
    RetypeNewPassword: '',
  };

  private _ID_Employee: number = 0;

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }
  IsValidated: boolean = false;

  async ngOnInit(): Promise<void> {}

  async show(model: any, ID_CurrentObject: number): Promise<any> {
    this.IsValidated = false;

    this.CurrentObject = {
      Oid_Model: model.Oid,
      ID_CurrentObject: ID_CurrentObject,
      Password: '',
    };

    var modalDialog = this.modalDialog;

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  async close(): Promise<any> {
    var modalDialog = this.modalDialog;

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.close();
        resolve(null);
      }
    });

    return promise;
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }
  }

  async validation(): Promise<boolean> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Password.length == 0) {
      validations.push({
        message: 'Password is required.',
      });
    }

    if (validations.length > 0) {
      this.toastService.warning(validations[0].message);
    }

    return validations.length == 0;
  }

  async updateUserInfo() {
    var params = {
      Oid_Model: this.CurrentObject.Oid_Model,
      ID_CurrentObject: this.CurrentObject.ID_CurrentObject,
      Password: this.CurrentObject.Password,
      ID_UserSession: this.currentUser.ID_UserSession,
    };
    var obj = await this.ds.execSP(`pValidateUserByPassword`, params, {
      isReturnObject: true,
    });

    if (obj.Success) {
      if (this.modalDialog != undefined) {
        this.IsValidated = true;
        this.modalDialog.close();
      }
    } else {
      this.toastService.danger(obj.Message);
    }
  }

  async btnChange_onClick() {
    var isValid = await this.validation();
    if (isValid) {
      await this.updateUserInfo();
    }
  }

  btnClose_onClick() {
    if (this.modalDialog != undefined) {
      this.modalDialog.close();
    }
  }
}
