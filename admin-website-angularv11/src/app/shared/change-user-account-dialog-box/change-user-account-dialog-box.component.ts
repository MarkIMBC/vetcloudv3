import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { CurrentObjectOnValueChangeArg } from 'src/app/layout/base-detail-view/base-detail-view.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  ReceiveInventory,
  FilingStatusEnum,
  IControlModelArg,
  IFormValidation,
} from 'src/shared/APP_HELPER';

@Component({
  selector: 'change-user-account-dialog-box',
  templateUrl: './change-user-account-dialog-box.component.html',
  styleUrls: ['./change-user-account-dialog-box.component.less'],
})
export class ChangeUserAccountDialogBoxComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {
    Code_Company: '',
    Username: '',
    NewPassword: '',
    RetypeNewPassword: '',
  };

  private _ID_Employee: number = 0;

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {}

  async show(ID_Employee: number): Promise<any> {
    this.CurrentObject = {
      Code_Company: '',
      Username: '',
      NewPassword: '',
      RetypeNewPassword: '',
    };

    var modalDialog = this.modalDialog;

    this._ID_Employee = ID_Employee;

    await this.loadRecord(ID_Employee);

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  async loadRecord(ID_Employee: number) {
    var params = {
      ID_Employee: ID_Employee,
      ID_UserSession: this.currentUser.ID_UserSession,
    };
    var obj = await this.ds.execSP(`pGeEmployeetUserInfo`, params, {
      isReturnObject: true,
    });

    Object.keys(this.CurrentObject).forEach((key: string) => {
      if (obj[key]) {
        this.CurrentObject[key] = obj[key];
      }
    });

    // this.CurrentObject.Username = this.CurrentObject.Username.replace(
    //   this.CurrentObject.Code_Company + '-',
    //   ''
    // );

    // console.log(this.CurrentObject, obj);
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }
  }

  async validation(): Promise<boolean> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.ID_Client == null) this.CurrentObject.ID_Client = 0;
    if (this.CurrentObject.ID_Patient == null)
      this.CurrentObject.ID_Patient = 0;

    if (this.CurrentObject.Username.length == 0) {
      validations.push({
        message: 'Username is required.',
      });
    }

    if (this.CurrentObject.NewPassword.length == 0) {
      validations.push({
        message: 'New Password is required.',
      });
    }

    if (
      this.CurrentObject.RetypeNewPassword != this.CurrentObject.NewPassword
    ) {
      validations.push({
        message: 'Password Mismatch.',
      });
    }

    if (validations.length > 0) {
      this.toastService.warning(validations[0].message);
    }

    return validations.length == 0;
  }

  async updateUserInfo() {
    // var username =
    //   this.CurrentObject.Code_Company + '-' + this.CurrentObject.Username;

    var username = this.CurrentObject.Username;

    var params = {
      EmployeeID: this._ID_Employee,
      Username: username,
      Password: this.CurrentObject.NewPassword,
      ID_UserSession: this.currentUser.ID_UserSession,
    };
    var obj = await this.ds.execSP(`pUpdateEmployeetUserInfo`, params, {
      isReturnObject: true,
    });

    if (obj.Success) {
      this.toastService.success(`User Account has been updated.`);

      if (this.modalDialog != undefined) {
        this.modalDialog.close();
      }
    } else {
      this.toastService.danger(obj.Message);
    }
  }

  async btnChange_onClick() {
    var isValid = await this.validation();
    if (isValid) {
      await this.updateUserInfo();
    }
  }

  btnClose_onClick() {
    if (this.modalDialog != undefined) {
      this.modalDialog.close();
    }
  }
}
