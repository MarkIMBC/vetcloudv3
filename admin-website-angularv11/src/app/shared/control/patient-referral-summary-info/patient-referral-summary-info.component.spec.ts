import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralSummaryInfoComponent } from './patient-referral-summary-info.component';

describe('PatientReferralSummaryInfoComponent', () => {
  let component: PatientReferralSummaryInfoComponent;
  let fixture: ComponentFixture<PatientReferralSummaryInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralSummaryInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralSummaryInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
