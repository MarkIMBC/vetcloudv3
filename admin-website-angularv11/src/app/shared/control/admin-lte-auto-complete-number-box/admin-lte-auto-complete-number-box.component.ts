import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-admin-lte-auto-complete-number-box',
  templateUrl: './admin-lte-auto-complete-number-box.component.html',
  styleUrls: ['./admin-lte-auto-complete-number-box.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEAutoCompleteNumberBoxComponent) }]
})
export class AdminLTEAutoCompleteNumberBoxComponent extends AdminLTEBaseControlComponent {

  @Input()
  dataList: string[] = [];

  afterLoad(): void {

    if (this.input) {

      $(this.input.nativeElement).attr("list", this.idTag);
    }
  }
}
