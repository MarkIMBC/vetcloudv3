import { Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';
import { AdminLTEListComponent, AdminLTEListOption, AdminLTEListRowSelectedArg } from '../admin-lte-list/admin-lte-list.component';

@Component({
  selector: 'admin-lte-data-lookupbox',
  templateUrl: './admin-lte-data-lookupbox.component.html',
  styleUrls: ['./admin-lte-data-lookupbox.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEDataLookupboxComponent) }]
})
export class AdminLTEDataLookupboxComponent extends AdminLTEBaseControlComponent {

  @ViewChild('listview') listview: AdminLTEListComponent | undefined;
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  @Input()isHide : boolean = false

  @Input() set initialValue(val: any) {

    if(val == null || val == '') val = null;
    this.value = val;

    if(this.value == null){
      this.displayValue = ''
    }

  }

  @Input() set initialDisplayValue(val: any) {
    console.log(val)
    if(val == null || val == '') val = '';

    this.displayValue = val;
  }


  displayValue: string = '';

  @Input()
  option!: AdminLTEDataLookupboxOption;

  btnDelete_onClick() {

    this.value = null;
    this.displayValue = '';

    this.onModelChanged.emit({
      name: this.name,
      value: this.value,
      displayName: this.displayName,
      displayValue: this.displayValue
    });
  }

  btnShow_onClick() {

    if(!this.listview) return;

    this.modalDialog?.open();
    this.listview.option = this.option.listviewOption;
    this.listview.loadRecords();
  }

  listview_onRowSelected(e: AdminLTEListRowSelectedArg){

    if (e.values.length == 0) return;
    var selectedValue = e.values[0];

    this.value = selectedValue.value;
    this.displayValue = selectedValue.displayValue;

    this.onModelChanged.emit({
      name: this.name,
      value: this.value,
      displayName: this.displayName,
      displayValue: this.displayValue,
      data: e.rows[0]
    });

    this.modalDialog?.close();
  }

  removeValue(){

    this.value = null;
    this.displayValue = '';

    this.onModelChanged.emit({
      name: this.name,
      value: this.value,
      displayName: this.displayName,
      displayValue: this.displayValue
    });
  }
}

export class AdminLTEDataLookupboxOption {

  listviewOption: AdminLTEListOption | undefined;
}
