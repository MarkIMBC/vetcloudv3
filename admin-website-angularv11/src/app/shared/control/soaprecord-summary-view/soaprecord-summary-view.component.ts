import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Enumerable } from 'linq-typescript';
import * as moment from 'moment';
import { UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { FilingStatusEnum, PatientSOAPRecordInfoEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'soaprecord-summary-view',
  templateUrl: './soaprecord-summary-view.component.html',
  styleUrls: ['./soaprecord-summary-view.component.less']
})
export class SOAPRecordSummaryViewComponent implements OnInit {
  SOAPRecords: any;
  _record: any;
  _ID_Patient_SOAP?: number;
  title: string = "";

  CurrentRecord: any = {
    Code: "",
    Date: ""
  }

  constructor(
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    private cs: CrypterService,
    private router: Router
  ) { }

  @Input() set ID_Patient_SOAP(val: any) {
    if (val == null || val == undefined) return;

    this._ID_Patient_SOAP = val;

    var sql = `
        /*encryptsqlstart*/
        SELECT  *
        FROM dbo.vPatient_SOAP_ListView
        WHERE
          ID = ${this._ID_Patient_SOAP}
        ORDER BY Date DESC
        /*encryptsqlend*/
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {

      this._record = obj[0];

      this.CurrentRecord.Code = this._record.Code;

      console.log( obj[0]);

      this.title = "" +
        "Date: " + moment(this._record.Date).format('MM/DD/YYYY') + "\r\n" +
        "Status: " + this._record.Name_FilingStatus + "\r\n" +
        "Attending Personnel: " + this._record.AttendingPhysician_Name_Employee

      this.loadSOAPRecords();
    });

  }

  @Input() set record(val: any) {
    if (val == null || val == undefined) return;

    this._record = val;

    this.loadSOAPRecords();
  }

  ngOnInit(): void {
  }

  getCustomNavigateCommands(routelink: any, config?: any) {
    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    return routelink;
  }

  Code_onClicked(){

    var config: any[] = this.getCustomNavigateCommands(
      ['Patient_SOAP', this._record.ID],
      {}
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }

  async loadSOAPRecords() {

    this.SOAPRecords = [];
    var ID_Patient_SOAP = this._record.ID;

    this.initRecordValues([
      "History",
      "ClinicalExamination",
      "Interpretation",
      "Diagnosis",
      "ClientCommunication",
    ]);

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.PrimaryComplaintOrHistory,
      label: 'Primary Complaint / History',
      value: this._record.History,
      hasValue: this._record.History.length > 0,
      isLoading: false,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.ClinicalExam,
      label: 'Clinical Exam',
      value: this._record.ClinicalExamination,
      hasValue: this._record.ClinicalExamination.length > 0,
      isLoading: false,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation,
      label: 'Laboratory / Interpretation',
      value: this._record.Interpretation,
      hasValue: false,
      isLoading: true,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.Plan,
      label: 'Plan',
      value: '',
      hasValue: false,
      isLoading: true,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.Diagnosis,
      label: 'Diagnosis',
      value: this._record.Diagnosis,
      hasValue: this._record.Diagnosis.length > 0,
      isLoading: false,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.Treatment,
      label: 'Treatment',
      value: this._record.Treatment,
      hasValue: false,
      isLoading: true,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.Prescription,
      label: 'Prescription',
      value: this._record.Prescription,
      hasValue: false,
      isLoading: true,
    });

    this.SOAPRecords.push({
      id: PatientSOAPRecordInfoEnum.ClientCommunication,
      label: 'Client Communication',
      value: this._record.ClientCommunication,
      hasValue: this._record.ClientCommunication.length > 0,
      isLoading: false,
    });

    console.log("SOAPID-" + this._record.ID + " - starting load");
    this.load_Patient_SOAP_RecordInfo(ID_Patient_SOAP, {
      id: PatientSOAPRecordInfoEnum.Plan,
    });

    this.load_Patient_SOAP_RecordInfo(ID_Patient_SOAP, {
      id: PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation,
    });

    this.load_Patient_SOAP_RecordInfo(ID_Patient_SOAP, {
      id: PatientSOAPRecordInfoEnum.Prescription,
    });

    this.load_Patient_SOAP_RecordInfo(ID_Patient_SOAP, {
      id: PatientSOAPRecordInfoEnum.Treatment,
    });
    console.log("SOAPID-" + this._record.ID + " - ending loading");
  }

  public initRecordValues(propertyNames: any[]) {

    var _ = this;

    propertyNames.forEach(function (propertyName) {

      if (_._record[propertyName] == undefined) _._record[propertyName]
    });
  }

  async load_Patient_SOAP_RecordInfo(ID_Patient_SOAP: any, options: any) {

    var id = -1;
    var sp = '';

    if (options) {
      if (options['id']) {
        id = options['id'];
      }
    }

    switch (id) {
      case PatientSOAPRecordInfoEnum.Plan:
        sp = 'pGet_Patient_SOAP_RecordInfo_Plan';
        break;
      case PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation:
        sp = 'pGet_Patient_SOAP_RecordInfo_LaboratoryImages';
        break;
      case PatientSOAPRecordInfoEnum.Prescription:
        sp = 'pGet_Patient_SOAP_RecordInfo_Prescription';
        break;
      case PatientSOAPRecordInfoEnum.Treatment:
        sp = 'pGet_Patient_SOAP_RecordInfo_Treatment';
        break;
    }

    this.ds
      .execSP(
        sp,
        {
          IDs_Patient_SOAP: [ID_Patient_SOAP],
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (result: any) => {

        if (result.Details == null) result.Details = [];

        this.SOAPRecords.forEach((SOAPRecord: any) => {

          if (SOAPRecord.id == id) {
            result.Details.forEach((detail: any) => {

              detail.TagString = detail.TagString.replaceAll(
                '/*break*/ ',
                '\n'
              );

              if (detail.TagString == null || detail.TagString == undefined)
                detail.TagString = '';

              SOAPRecord.value = detail.TagString;
              SOAPRecord.hasValue = detail.TagString.length;


              SOAPRecord.isLoading = false;
            });
          }
        });

      });
  }

}
