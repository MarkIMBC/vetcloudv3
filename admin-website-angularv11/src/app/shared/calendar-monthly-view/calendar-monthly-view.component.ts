import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'calendar-monthly-view',
  templateUrl: './calendar-monthly-view.component.html',
  styleUrls: ['./calendar-monthly-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarMonthlyViewComponent implements OnInit {
  currentDate: Date = new Date();
  @Input() year: number = 0;
  @Input() month: number = 0;
  monthNames: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  dayHeaders: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  daysInMonth: (Date | null)[] = [];
  tasks: { [key: string]: CalendarDayTaskItem[] } = {};

  @Output() onChangedMonthYear = new EventEmitter<{ year: number, month: number }>();
  @Output() onTaskClicked = new EventEmitter<CalendarDayTaskItem>();

  constructor(protected cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.year = this.currentDate.getFullYear();
    this.month = this.currentDate.getMonth();
    this.generateMonth();
  }

  previousMonth() {
    if (this.month === 0) {
      this.year--;
      this.month = 11;
    } else {
      this.month--;
    }
    this.generateMonth();
  }

  nextMonth() {
    if (this.month === 11) {
      this.year++;
      this.month = 0;
    } else {
      this.month++;
    }
    this.generateMonth();
  }

  todayMonth() {

    this.currentDate = new Date();

    this.year = this.currentDate.getFullYear();
    this.month = this.currentDate.getMonth();

    this.generateMonth();
  }

  public removeAllTask() {
    Object.keys(this.tasks).forEach(key => {
      this.tasks[key] = [];
    });
  }

  generateMonth(): void {
    const firstDayOfMonth = new Date(this.year, this.month, 1);
    const startingDay = firstDayOfMonth.getDay(); // 0 is Sunday, 1 is Monday, etc.
    const numDays = new Date(this.year, this.month + 1, 0).getDate();
    const daysArray: (Date | null)[] = [];

    this.tasks = {};

    // Add null placeholders for days before the first day of the month
    for (let i = 0; i < startingDay; i++) {
      daysArray.push(null);
    }

    // Add days of the month
    for (let i = 1; i <= numDays; i++) {
      const currentDate = new Date(this.year, this.month, i);

      const dateString = this.getDateString(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate()
      );

      daysArray.push(currentDate);
      this.tasks[dateString] = [];
    }

    this.daysInMonth = daysArray;

    var _ = this;
    this.onChangedMonthYear.emit(
      {
        year: _.year,
        month: _.month
      }
    );


    setTimeout(() => {
      this.cdr.detectChanges(); // Trigger change detection
    }, 1000);

  }

  getDateString(year: number, month: number, day: number): string {
    return year + '-' + this.pad(month + 1) + '-' + this.pad(day);
  }

  pad(num: number): string {
    return num < 10 ? '0' + num : num.toString();
  }

  public refresh() {
    setTimeout(() => {
      this.cdr.detectChanges(); // Trigger change detection
    }, 500);
  }

  public addTaskItem(item: CalendarDayTaskItem): void {
    if (item.title) {
      if(this.tasks[item.dateString] == null) this.tasks[item.dateString] = [];
      this.tasks[item.dateString].push(item);
    }
  }

  public tack_onClick(item: CalendarDayTaskItem): void {

    this.onTaskClicked.emit(item);
  }
}

export class CalendarDayTaskItem {
  dateString: string = "";
  title: string = "";
  tooltip: string = "";
  className: string = "";
  referenceObj: any = null;
}
