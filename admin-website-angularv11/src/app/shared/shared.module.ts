import { AdminLTEDateTimeboxComponent } from './control/admin-lte-datetimebox/admin-lte-datetimebox.component';
import { DataService } from 'src/app/core/data.service';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContentHeaderComponent } from './content-header/content-header.component';
import { PaginationComponent } from './pagination/pagination.component';
import { AdminLTEBaseControlComponent } from './control/admin-lte-base-control/admin-lte-base-control.component';
import { AdminLTETextboxComponent } from './control/admin-lte-textbox/admin-lte-textbox.component';
import { MenubarComponent } from './menubar/menubar.component';
import { CardRefreshComponent } from './card-refresh/card-refresh.component';
import { AdminLTEListComponent } from './control/admin-lte-list/admin-lte-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminLTEDataLookupboxComponent } from './control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { ModalModule } from './modal/modal.module';
import { AdminLTEDateboxComponent } from './control/admin-lte-datebox/admin-lte-datebox.component';
import { AdminLTENumberboxComponent } from './control/admin-lte-numberbox/admin-lte-numberbox.component';
import { AdminLTETextareaComponent } from './control/admin-lte-textarea/admin-lte-textarea.component';
import { SQLListDialogComponent } from './control/sql-list-dialog/sql-list-dialog.component';
import { ToastsContainer } from './toasts-container/toasts-container.component';
import { DropDownButtonComponent } from './drop-down-button/drop-down-button.component';
import { ReportViewerComponent } from './report-viewer/report-viewer.component';
import { AdminLTEImageBoxComponent } from './control/admin-lte-image-box/admin-lte-image-box.component';
import { AdminLTECheckboxComponent } from './control/admin-lte-checkbox/admin-lte-checkbox.component';
import { AdjustCreditDialogComponent } from './adjust-credit-dialog/adjust-credit-dialog.component';
import { AdminLTEAutocompleteboxComponent } from './control/admin-lte-autocompletebox/admin-lte-autocompletebox.component';
import { AdminLTEListDataFormat } from './Pipes/AdminLTEListDataFormat.pipe';
import { AdminLTEListColumnAlign } from './Pipes/AdminLTEListColumnAlign.pipe';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { CardContainerComponent } from './card-container/card-container.component';
import { AdminLTETimeboxComponent } from './control/admin-lte-timebox/admin-lte-timebox.component';
import { AdminLTESmallBoxComponent } from './admin-lte-small-box/admin-lte-small-box.component';
import { BtnChangeWaitingStatusComponent } from './control/btn-change-waiting-status/btn-change-waiting-status.component';
import { AdminLTERadioButtonListComponent } from './control/admin-lte-radio-button-list/admin-lte-radio-button-list.component';
import { AdminLTEPasswordboxComponent } from './control/admin-lte-passwordbox/admin-lte-passwordbox.component';
import { ChangeUserAccountDialogBoxComponent } from './change-user-account-dialog-box/change-user-account-dialog-box.component';
import { PasswordUserPromptComponent } from './password-user-prompt/password-user-prompt.component';
import { CalendarMonthlyViewComponent } from './calendar-monthly-view/calendar-monthly-view.component';
import { SOAPRecordSummaryViewComponent } from './control/soaprecord-summary-view/soaprecord-summary-view.component';
import { AdminLTEAutoCompleteNumberBoxComponent } from './control/admin-lte-auto-complete-number-box/admin-lte-auto-complete-number-box.component';
import { PatientReferralSummaryInfoComponent } from './control/patient-referral-summary-info/patient-referral-summary-info.component';

@NgModule({
  declarations: [
    ContentHeaderComponent,
    PaginationComponent,
    AdminLTEBaseControlComponent,
    AdminLTETextboxComponent,
    MenubarComponent,
    CardRefreshComponent,
    AdminLTEListComponent,
    AdminLTEDataLookupboxComponent,
    AdminLTEDateboxComponent,
    AdminLTENumberboxComponent,
    AdminLTETextareaComponent,
    AdminLTEDateTimeboxComponent,
    SQLListDialogComponent,
    ToastsContainer,
    DropDownButtonComponent,
    ReportViewerComponent,
    AdminLTEImageBoxComponent,
    AdminLTECheckboxComponent,
    AdjustCreditDialogComponent,
    AdminLTEAutocompleteboxComponent,
    AdminLTEListDataFormat,
    AdminLTEListColumnAlign,
    MessageDialogComponent,
    CardContainerComponent,
    AdminLTETimeboxComponent,
    AdminLTESmallBoxComponent,
    BtnChangeWaitingStatusComponent,
    AdminLTERadioButtonListComponent,
    AdminLTEPasswordboxComponent,
    ChangeUserAccountDialogBoxComponent,
    PasswordUserPromptComponent,
    CalendarMonthlyViewComponent,
    SOAPRecordSummaryViewComponent,
    AdminLTEAutoCompleteNumberBoxComponent,
    PatientReferralSummaryInfoComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ModalModule,
  ],
  exports: [
    CommonModule,
    RouterModule,
    ContentHeaderComponent,
    MenubarComponent,
    PaginationComponent,
    CardRefreshComponent,
    AdminLTETextboxComponent,
    AdminLTETextareaComponent,
    AdminLTEDateboxComponent,
    AdminLTENumberboxComponent,
    AdminLTEListComponent,
    AdminLTEDataLookupboxComponent,
    AdminLTEDateTimeboxComponent,
    AdminLTEImageBoxComponent,
    AdminLTECheckboxComponent,
    SQLListDialogComponent,
    ToastsContainer,
    DropDownButtonComponent,
    ReportViewerComponent,
    AdjustCreditDialogComponent,
    AdminLTEAutocompleteboxComponent,
    MessageDialogComponent,
    CardContainerComponent,
    AdminLTETimeboxComponent,
    AdminLTESmallBoxComponent,
    BtnChangeWaitingStatusComponent,
    AdminLTERadioButtonListComponent,
    AdminLTEPasswordboxComponent,
    ChangeUserAccountDialogBoxComponent,
    PasswordUserPromptComponent,
    CalendarMonthlyViewComponent,
    SOAPRecordSummaryViewComponent,
    AdminLTEAutoCompleteNumberBoxComponent
  ],
  providers: [DataService, DecimalPipe, DatePipe],
  bootstrap: [AdminLTEListComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SharedModule {}
