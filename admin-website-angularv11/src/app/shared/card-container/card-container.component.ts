import { Component, OnInit, ChangeDetectionStrategy, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardContainerComponent implements OnInit {

  @Input() customBodyTemplate!: TemplateRef<any>;

  @Input() loading: boolean = false;
  @Input() IsShowHeader: boolean = true;
  @Input() title: string = '';
  @Input() bodyStyle: any = '';

  constructor() { }

  ngOnInit(): void {
  }

}
