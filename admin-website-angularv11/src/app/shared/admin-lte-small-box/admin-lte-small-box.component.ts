import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'admin-lte-small-box',
  templateUrl: './admin-lte-small-box.component.html',
  styleUrls: ['./admin-lte-small-box.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminLTESmallBoxComponent implements OnInit {

  @Input() title: string = '';
  @Input() subtitle: string = '';
  @Input() iconClass: string = '';
  @Input() bgClass: string = 'bg-info';

  constructor() { }

  ngOnInit(): void {
  }

}
