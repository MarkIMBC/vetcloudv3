import { ToastService } from './../shared/toast.service';
import { DataService } from './../core/data.service';
import { Component, OnInit } from '@angular/core';
import { UserAuthenticationService } from '../core/UserAuthentication.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  username: string = '';
  password: string = '';

  isInit: boolean = false;
  loading: boolean = false;

  constructor(private userAuth: UserAuthenticationService, private ds: DataService, private toastService: ToastService) { }

  async ngOnInit(): Promise<void> {

    var currentUser = this.userAuth.getDecodedToken();
    if(currentUser.ID_User != undefined && currentUser.ID_User != null ){

      window.location.href = './Starter'
      return;
    }
  }

  onKeyUp(event: any) {

    if(event.keyCode == 13){

      this.login();
    }
  }

  async login() {

  this.loading = true;

    await this.userAuth.LogIn(this.username, this.password)
      .then((promise) => {

        window.location.href = "./Starter"
      })
      .catch((rej) => {

        this.toastService.danger(rej.error);
        console.log(rej);
      });

    this.loading = false;
  }

  async ngAfterViewInit(): Promise<void> {

    this.isInit = true;
  }

}
