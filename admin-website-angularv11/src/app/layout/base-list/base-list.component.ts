import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEListComponent, AdminLTEListOption, AdminLTEListRowSelectedArg } from 'src/app/shared/control/admin-lte-list/admin-lte-list.component';
import { ToastService } from 'src/app/shared/toast.service';
import { PagingOption } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-base-list',
  templateUrl: './base-list.component.html',
  styleUrls: ['./base-list.component.less']
})
export class BaseListComponent implements OnInit {

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'New',
      icon: 'fas fa-file',
      visible: true,
    },
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
    },
  ];

  public pagingOption: PagingOption = new PagingOption();

  @ViewChild('listview') listview: AdminLTEListComponent | undefined;

  @Output() onRowSelected: EventEmitter<AdminLTEListRowSelectedArg> = new EventEmitter<AdminLTEListRowSelectedArg>();
  @Output() onDataSourceLoaded: EventEmitter<any> = new EventEmitter<any>();

  currentUser: TokenSessionFields = new TokenSessionFields();

  loading: boolean = false;

  listviewOption!: AdminLTEListOption;

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  ngOnInit(): void {

  }

  refreshRecord(){


  }

  menubar_OnClick(e: any) {

    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == "Refresh") {

      this.refreshRecord();
    } else if (menuItem.label == "New") {

      this.menuItem_New_onClick();
    }
  }

  listview_onRowSelected(e: AdminLTEListRowSelectedArg) {

    this.onRowSelected.emit(e);
    this.Row_OnClick(e.rows);
  }

  listview_onDataSourceLoaded(e: any){

    this.pagingOption = e.pagingOption;

    this.onDataSourceLoaded.emit(e);
  }

  menuItem_New_onClick() {

  }

  Row_OnClick(rows: any[]) {

  }

}
