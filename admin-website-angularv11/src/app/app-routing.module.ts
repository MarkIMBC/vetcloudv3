import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './defaultpage/page-not-found/page-not-found.component';

import { LoginURLComponent } from './login-url/login-url.component';

const routes: Routes = [

  { path: '', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'Login', component: LoginComponent},

  { path: 'LoginUrl/:text', component: LoginURLComponent},

  { path: 'Starter', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'CompanyDetail', loadChildren: () => import('./feature/company-detail/company-detail.module').then(m => m.CompanyDetailModule) },

  { path: 'Employee/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'PageNotFound', component: PageNotFoundComponent },

  { path: 'Report/:configOptions', loadChildren: () => import('./feature/report/report.module').then(m => m.ReportModule) },

  { path: 'EmployeeDetail', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'EmployeeList', loadChildren: () => import('./feature/employee-list/employee-list.module').then(m => m.EmployeeListModule) },


  { path: 'Dashboard', loadChildren: () => import('./feature/dashboard/dashboard.module').then(m => m.DashboardModule) },

  { path: 'CurrentEmployeeInfo', loadChildren: () => import('./feature/employee-current-detail/employee-current-detail.module').then(m => m.EmployeeCurrentDetailModule) },

  { path: 'ReferenceLink/:Code', loadChildren: () => import('./feature/reference-link-view/reference-link-view.module').then(m => m.ReferenceLinkViewModule) },

  { path: 'NoAccess', loadChildren: () => import('./feature/no-access/no-access.module').then(m => m.NoAccessModule) },

  { path: 'User/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/user-detail/user-detail.module').then(m => m.UserDetailModule) },

  { path: 'CompanyMasterList', loadChildren: () => import('./feature/company-master-list/company-master-list.module').then(m => m.CompanyMasterListModule) },

  { path: 'CompanyMasterDetail/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/company-master-detail/company-master-detail.module').then(m => m.CompanyMasterDetailModule) },

  { path: 'ServerList', loadChildren: () => import('./feature/server-list/server-list.module').then(m => m.ServerListModule) },

  { path: 'Server/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/server-detail/server-detail.module').then(m => m.ServerDetailModule) },

  /******************************************************************  Additional Modules and Components ******************************************************************/

  { path: '**', component: PageNotFoundComponent },

];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
