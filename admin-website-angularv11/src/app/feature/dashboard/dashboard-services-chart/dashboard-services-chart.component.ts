import { Component, OnInit, ChangeDetectionStrategy, Input, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import * as moment from 'moment';
import { IControlModelArg } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEDateTimeboxComponent } from 'src/app/shared/control/admin-lte-datetimebox/admin-lte-datetimebox.component';





@Component({
  selector: 'dashboard-services-chart',
  templateUrl: './dashboard-services-chart.component.html',
  styleUrls: ['./dashboard-services-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardServicesChartComponent implements OnInit {

  @ViewChild('adminltedateboxDateStart') adminltedateboxDateStart: AdminLTEDateTimeboxComponent | undefined
  @ViewChild('adminltedateboxDateEnd') adminltedateboxDateEnd: AdminLTEDateTimeboxComponent | undefined

  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: Chart | undefined;
  dataSource: any;
  ServiceCategories: Array<string> = [];
  ServiceCount: Array<number> = [];
  year = moment().year();
  label: string = "";
  CurrentObject: any = {

  }

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {

    this.CurrentObject.DateStart = moment(`${this.year.toString()}-01-01 00:00:00`).toDate();
    this.CurrentObject.DateEnd = moment().toDate();

    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;


  async ngOnInit() {

    this.loadRecords();
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    this.loadRecords();
  }

  async loadRecords() {

    var obj = await this.ds.execSP(
      "pGetDashboardServicesDataSourceByDateCoverage",
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateStart: moment(this.CurrentObject.DateStart).format("yyyy-MM-DD"),
        DateEnd: moment(this.CurrentObject.DateEnd).format("yyyy-MM-DD"),
      },
      {
        isReturnObject: true,
      }
    );

    this.label = obj.Label;
    this.dataSource = obj.DataSource;

    this.ServiceCount = [];
    this.ServiceCategories = [];

    this.dataSource.forEach((obj: { Count: number; }) => {
      this.ServiceCount.push(obj.Count)
    });
    this.dataSource.forEach((obj: { Name: string; }) => {
      this.ServiceCategories.push(obj.Name)
    });

    this.loadChart();
  }

  loadChart() {

    if(this.myChart) this.myChart.destroy();

    this.myChart = new Chart("ServicesChart", {
      type: 'pie',
      data: {
        labels: this.ServiceCategories,
        datasets: [{
          label: 'Monthly Sales',
          data: this.ServiceCount,
          backgroundColor: [
            '#82C272',
            '#00A88F',
            '#0087AC',
            '#005FAA',
            '#323B81',
            '#62BEB6',
            '#034D44',
            '#62BEB6',
            '#034D44',
          ],
          borderColor: [
            '#82C272',
            '#00A88F',
            '#0087AC',
            '#005FAA',
            '#323B81',
            '#62BEB6',
            '#034D44',
            '#62BEB6',
            '#034D44',
          ],
          borderWidth: 1
        }]
      },


      options: {
        title: {
          display: true,
          text: this.label
        },
        scales: {
        }
      }
    });

    this.myChart.update()
  }

  reset(){

    this.CurrentObject.DateStart = moment(`${this.year.toString()}-01-01 00:00:00`).toDate();
    this.CurrentObject.DateEnd = moment().toDate();

    if(this.adminltedateboxDateStart) this.adminltedateboxDateStart.setValue(this.CurrentObject.DateStart);
    if(this.adminltedateboxDateEnd) this.adminltedateboxDateEnd.setValue(this.CurrentObject.DateEnd);

    this.loadRecords();
  }
}
