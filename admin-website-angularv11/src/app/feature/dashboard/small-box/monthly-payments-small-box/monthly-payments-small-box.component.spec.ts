import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyPaymentsSmallBoxComponent } from './monthly-payments-small-box.component';

describe('MonthlyPaymentsSmallBoxComponent', () => {
  let component: MonthlyPaymentsSmallBoxComponent;
  let fixture: ComponentFixture<MonthlyPaymentsSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyPaymentsSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyPaymentsSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
