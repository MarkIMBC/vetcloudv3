import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlySalesSmallBoxComponent } from './monthly-sales-small-box.component';

describe('MonthlySalesSmallBoxComponent', () => {
  let component: MonthlySalesSmallBoxComponent;
  let fixture: ComponentFixture<MonthlySalesSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlySalesSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlySalesSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
