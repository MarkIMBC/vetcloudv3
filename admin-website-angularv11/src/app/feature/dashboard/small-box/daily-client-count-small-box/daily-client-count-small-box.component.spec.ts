import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyClientCountSmallBoxComponent } from './daily-client-count-small-box.component';

describe('DailyClientCountSmallBoxComponent', () => {
  let component: DailyClientCountSmallBoxComponent;
  let fixture: ComponentFixture<DailyClientCountSmallBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyClientCountSmallBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyClientCountSmallBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
