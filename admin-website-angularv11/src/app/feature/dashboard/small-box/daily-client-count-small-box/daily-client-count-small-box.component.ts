import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EventContentArg } from '@fullcalendar/angular';
import * as moment from 'moment';
import { title } from 'process';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { AdminLTEDateTimeboxComponent } from 'src/app/shared/control/admin-lte-datetimebox/admin-lte-datetimebox.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { IControlModelArg } from 'src/shared/APP_HELPER';

@Component({
  selector: 'daily-client-count-small-box',
  templateUrl: './daily-client-count-small-box.component.html',
  styleUrls: ['./daily-client-count-small-box.component.less']
})
export class DailyClientCountSmallBoxComponent implements OnInit {

  @ViewChild('adminltedateboxDate') adminltedateboxDate: AdminLTEDateTimeboxComponent | undefined
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();

  CurrentObject: any = {
    title: "",
    subtitle: "",
    Date: new Date()
  }

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  ngOnInit(): void {

    this.loadRecord();
  }

  iconContainer_OnClicked() {

    if (this.modalDialog) this.modalDialog.open();
  }

  btnReset_OnClicked() {

    this.reset();

    if (this.modalDialog) this.modalDialog.close();
  }

  btnFilter_OnClicked() {

    this.loadRecord();

    if (this.modalDialog) this.modalDialog.close();
  }

  control_onModelChanged(e: IControlModelArg) {

    this.CurrentObject.Date = e.value;

    // this.loadRecord();
    // if (this.modalDialog) this.modalDialog.close();
  }
  async loadRecord() {

    var obj = await this.ds.execSP(
      "pGetDashboardSmallBoxDailyClientCount",
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        Date: moment(this.CurrentObject.Date).format("yyyy-MM-DD"),
      },
      {
        isReturnObject: true,
      }
    );

    this.CurrentObject.title = obj.FormattedTotalCount;
    this.CurrentObject.subtitle = obj.Subtitle;
  }

  reset() {

    this.CurrentObject.Date = moment().toDate();

    if (this.adminltedateboxDate) this.adminltedateboxDate.setValue(this.CurrentObject.Date);

    this.loadRecord();
  }

}
