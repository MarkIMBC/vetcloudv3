import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  TemplateRef,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import * as moment from 'moment';

@Component({
  selector: 'dashboard-sales-chart',
  templateUrl: './dashboard-sales-chart.component.html',
  styleUrls: ['./dashboard-sales-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardSalesChartComponent implements OnInit {
  @ViewChild('selectboxYear') selectboxYear: ElementRef | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: Chart | undefined;
  chartLabel: string = '';
  dataSource: any;
  value:number =  0;
  years: any[] = [];

  CurrentObject: any = {
    Year: 0
  }

  MonthlySales: Array<number> = [];
  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();

    var currentYear = moment().year();
    this.CurrentObject.Year = currentYear;

    for (var counter = currentYear; counter >= 2020; counter--) {
      this.years.push(counter);
    }
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    await this.loadRecords();

  }

  loadChart(){

    var _chartLabel = this.chartLabel;

    if(this.myChart) this.myChart.destroy();

    this.myChart = new Chart('myChart', {
      type: 'bar',

      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        datasets: [
          {
            label: this.chartLabel,
            data: this.MonthlySales,
            backgroundColor: [
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderColor: [
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderWidth: 1,
          },
        ],
      },

      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                  return '₱ ' + value.toLocaleString();
                },
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, chart) {
              var yLabel: any = tooltipItem.yLabel;
              var value = 0;

              if (!yLabel) {
                yLabel = '0';
              }

              value = parseInt(yLabel);

              return _chartLabel + ' ₱ ' + value.toLocaleString();
            },
          },
        },
      },
    });

    this.myChart.update();
  }


  _onModelChanged(event: any) {

    this.CurrentObject.Year = event;
    this.loadRecords();
  }

  async loadRecords() {

    var obj = await this.ds.execSP(
      'pGetDashboardYearlySalesDataSource',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: this.CurrentObject.Year,
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.MonthlySales = [];

    this.dataSource.forEach((obj: { TotalSales: number }) => {
      this.MonthlySales.push(obj.TotalSales);
    });

    this.loadChart();
  }

  reset(){

    var currentYear = moment().year();

    if(this.selectboxYear) this.selectboxYear.nativeElement.value = currentYear;
    this.CurrentObject.Year = currentYear;

    this.loadRecords();
  }
}
