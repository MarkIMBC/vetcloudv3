import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReferenceLinkViewComponent } from './reference-link-view.component';

const routes: Routes = [{ path: '', component: ReferenceLinkViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferenceLinkViewRoutingModule { }
