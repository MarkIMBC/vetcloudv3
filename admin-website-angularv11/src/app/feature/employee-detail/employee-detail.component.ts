import {
  IFormValidation,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
} from './../../../shared/APP_HELPER';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import { ChangeUserAccountDialogBoxComponent } from '../../shared/change-user-account-dialog-box/change-user-account-dialog-box.component';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './employee-detail.component.less',
  ],
})
export class EmployeeDetailComponent extends BaseDetailViewComponent {
  @ViewChild('changeuseraccountdialogbox') changeuseraccountdialogbox:
    | ChangeUserAccountDialogBoxComponent
    | undefined;
  ModelName: string = 'Employee';
  headerTitle = 'Employee';

  displayMember: string = 'Name';

  ID_Position_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tPosition',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);

      if (
        this.CurrentObject.ID != this.currentUser.ID_Employee &&
        this.CurrentObject.IsAdministrator
      ) {
        this.menuItems.push(this._menuItem_Delete);
      }
    }
  }

  loadMenuItems() {

    console.log(this.CurrentObject);
    if (this.CurrentObject.ID < 1) return;
    if (this.CurrentObject.IsActive != true) return;

    var menuItemChangeUserAccount = {
      label: 'Change User Account',
      icon: 'fa fa-user',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCreateUserAccount = {
      label: 'Create User Account',
      icon: 'fa fa-user',
      class: 'text-primary',
      visible: true,
    };

    var menuItemModifyUserAccount = {
      label: 'Modify Access Modules',
      icon: 'fa fa-edit',
      class: 'text-primary',
      visible: true,
    };

    if (
      this.CurrentObject.ID == this.currentUser.ID_Employee ||
      (this.CurrentObject.IsAdministrator &&
        this.CurrentObject.UserAccount_ID_User)
    ) {
      this.menuItems.push(menuItemChangeUserAccount);
    }

    if (
      this.CurrentObject.IsAdministrator &&
      !this.CurrentObject.UserAccount_ID_User
    ) {
      this.menuItems.push(menuItemCreateUserAccount);
    }

    if (
      this.CurrentObject.IsAdministrator &&
      this.CurrentObject.UserAccount_ID_User
    ) {
      this.menuItems.push(menuItemModifyUserAccount);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Create User Account') {
      this.createUser();
    } else if (e.item.label == 'Change User Account') {
      if (this.changeuseraccountdialogbox != undefined) {
        await this.changeuseraccountdialogbox.show(this.CurrentObject.ID);
      }
    } else if (e.item.label == 'Modify Access Modules') {
      this.customNavigate(['User', this.CurrentObject.UserAccount_ID_User], {});
    }
  }

  async createUser() {
    // var username =
    //   this.CurrentObject.Code_Company + '-' + this.CurrentObject.Username;

    var username = this.CurrentObject.Username;

    var params = {
      CompanyID: this.CurrentObject.ID_Company,
      ID_Employee: this.CurrentObject.ID,
      Username: this.CurrentObject.FirstName.toLowerCase().replace(/ /g, '_'),
    };
    var obj = await this._dataServices.execSP(`pDoCreateUser`, params, {
      isReturnObject: true,
    });

    if (obj.Success) {
      this.toastService.success(`User Account has been Initialized.`);
      await this.loadRecord();

      if (this.changeuseraccountdialogbox != undefined) {
        await this.changeuseraccountdialogbox.show(this.CurrentObject.ID);
      }
    } else {
      this.toastService.danger(obj.Message);
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (
      this.CurrentObject['LastName'] == undefined ||
      this.CurrentObject['LastName'] == null
    )
      this.CurrentObject['LastName'] = '';
    if (
      this.CurrentObject['FirstName'] == undefined ||
      this.CurrentObject['FirstName'] == null
    )
      this.CurrentObject['FirstName'] = '';
    if (
      this.CurrentObject['MiddleName'] == undefined ||
      this.CurrentObject['MiddleName'] == null
    )
      this.CurrentObject['MiddleName'] = '';

    this.CurrentObject.Name =
      this.CurrentObject['LastName'] +
      ', ' +
      this.CurrentObject['FirstName'] +
      ' ' +
      this.CurrentObject['MiddleName'];
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var LastName = this.CurrentObject.LastName;
    var FirstName = this.CurrentObject.FirstName;
    var MiddleName = this.CurrentObject.MiddleName;
    var ID_Position = this.CurrentObject.ID_Position;

    if (LastName == undefined || LastName == null) LastName = '';
    if (FirstName == undefined || FirstName == null) FirstName = '';
    if (MiddleName == undefined || MiddleName == null) MiddleName = '';
    if (ID_Position == undefined || ID_Position == null) ID_Position = 0;

    if (LastName.length == 0) {
      validations.push({
        message: 'Last Name is required.',
      });
    }

    if (FirstName.length == 0) {
      validations.push({
        message: 'First Name is required.',
      });
    }

    if (ID_Position == 0) {
      validations.push({
        message: 'Position is required.',
      });
    }

    return Promise.resolve(validations);
  }
}
