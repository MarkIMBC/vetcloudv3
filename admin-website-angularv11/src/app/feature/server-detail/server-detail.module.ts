import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerDetailRoutingModule } from './server-detail-routing.module';
import { ServerDetailComponent } from './server-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ServerDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ServerDetailRoutingModule
  ]
})
export class ServerDetailModule { }
