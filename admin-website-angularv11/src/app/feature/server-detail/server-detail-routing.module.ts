import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServerDetailComponent } from './server-detail.component';

const routes: Routes = [{ path: '', component: ServerDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServerDetailRoutingModule { }
