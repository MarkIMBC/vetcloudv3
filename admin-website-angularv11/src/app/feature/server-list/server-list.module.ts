import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerListRoutingModule } from './server-list-routing.module';
import { ServerListComponent } from './server-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ServerListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ServerListRoutingModule
  ]
})
export class ServerListModule { }
