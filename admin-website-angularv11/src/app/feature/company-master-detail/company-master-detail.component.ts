import { IFormValidation, ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-company-master-detail',
  templateUrl: './company-master-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './company-master-detail.component.less'
  ]
})
export class CompanyMasterDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'CompanyMasterList'
  headerTitle: string = 'Company'

  routerFeatureName: string = 'CompanyMasterDetail';
  displayMember: string = "Name";

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
      // this.menuItems.push(this._menuItem_Delete);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    this.CurrentObject.Name = this.CurrentObject.Name.trimStart();
    this.CurrentObject.Name = this.CurrentObject.Name.trimEnd();


    if (this.CurrentObject.ServerAddress == null) this.CurrentObject.ServerAddress = '';
    this.CurrentObject.ServerAddress = this.CurrentObject.ServerAddress.trimStart();
    this.CurrentObject.ServerAddress = this.CurrentObject.ServerAddress.trimEnd();

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    if (this.CurrentObject.ServerAddress.length == 0) {
      validations.push({
        message: `Server Address is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  DetailView_onLoad() {

  }

}
