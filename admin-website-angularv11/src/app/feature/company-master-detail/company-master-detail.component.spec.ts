import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyMasterDetailComponent } from './company-master-detail.component';

describe('CompanyMasterDetailComponent', () => {
  let component: CompanyMasterDetailComponent;
  let fixture: ComponentFixture<CompanyMasterDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyMasterDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyMasterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
