import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyMasterDetailRoutingModule } from './company-master-detail-routing.module';
import { CompanyMasterDetailComponent } from './company-master-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CompanyMasterDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CompanyMasterDetailRoutingModule
  ]
})
export class CompanyMasterDetailModule { }
