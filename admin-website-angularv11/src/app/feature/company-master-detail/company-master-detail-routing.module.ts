import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyMasterDetailComponent } from './company-master-detail.component';

const routes: Routes = [{ path: '', component: CompanyMasterDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyMasterDetailRoutingModule { }
