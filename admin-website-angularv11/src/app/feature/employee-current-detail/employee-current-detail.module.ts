import { ModalModule } from './../../shared/modal/modal.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeCurrentDetailRoutingModule } from './employee-current-detail-routing.module';
import { EmployeeCurrentDetailComponent } from './employee-current-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [EmployeeCurrentDetailComponent],
  imports: [
    CommonModule,
    ModalModule,
    SharedModule,
    EmployeeCurrentDetailRoutingModule
  ]
})
export class EmployeeCurrentDetailModule { }
