import { Patient_Grooming_DTO, PropertyTypeEnum } from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'app-company-master-list',
  templateUrl: './company-master-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './company-master-list.component.less'
  ],
})
export class CompanyMasterListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'CompanyMasterListComponent';

  headerTitle: string = 'Company Master List';

  OrderByString: string = 'Name ASC';

  InitCurrentObject: any = {
    Name: '',
    Name_Server: ''
  };

  dataSource: any[] = [];

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
    {
      label: 'Options',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Sync From Servers',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        }
      ],
    }
  ];

  ID_Server_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        Select
         DISTINCT ID_Server ID, Name_Server Name
        FROM vActiveCompanyMasterList
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vActiveCompanyMasterList
            WHERE
              1 = 1
              ${filterString}
            /*encryptsqlend*/
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_Server']) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(ID_Server, 0) = ${this.CurrentObject['ID_Server']} `;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_Grooming_DTO) {
    this.customNavigate(['CompanyMasterDetail', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['CompanyMasterDetail', -1], {});
  }
}
