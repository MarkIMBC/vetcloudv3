import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyMasterListRoutingModule } from './company-master-list-routing.module';
import { CompanyMasterListComponent } from './company-master-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CompanyMasterListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CompanyMasterListRoutingModule
  ]
})
export class CompanyMasterListModule { }
