import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyMasterListComponent } from './company-master-list.component';

const routes: Routes = [{ path: '', component: CompanyMasterListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyMasterListRoutingModule { }
