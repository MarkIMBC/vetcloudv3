import { ReceivingReport } from '../../../shared/APP_MODELS';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingInvoice, User } from 'src/shared/APP_MODELS';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './user-list.component.less',
  ],
})
export class UserListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'UserListComponent';

  headerTitle: string = 'User';

  InitCurrentObject: any = {
    Name: '',
    ContactNumber: '',
  };

  dataSource: User[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'User',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Username,
                    Name
            FROM dbo.vUser
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: User) {
    this.customNavigate(['User', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['User', -1], {});
  }
}
