import { Router } from '@angular/router';

import { Injectable } from '@angular/core';
import { CrypterService } from './crypter.service';
import { MonthInfo } from 'src/shared/APP_HELPER';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class GeneralfxService {
  static isNullOrUndefined<T>(
    obj: T | null | undefined
  ): obj is null | undefined {
    return typeof obj === 'undefined' || obj === null;
  }

  static findIndexByKeyValue(_array: any[], key: string, value: any) {
    for (var i = 0; i < _array.length; i++) {
      if (_array[i][key] == value) {
        //console.log(_array[i], _array[i][key], value);
        return i;
      }
    }

    return -1000000;
  }

  static deleteFromArray(_array: any[], key: string, value: any) {
    var index: any = GeneralfxService.findIndexByKeyValue(_array, key, value);

    if (!index) index = -1000000;

    _array.splice(index, 1);
    return index;
  }

  static roundOffDecimal(value: number, decimalPlaces?: number): number {
    if (GeneralfxService.isNullOrUndefined(decimalPlaces)) decimalPlaces = 2;
    if (GeneralfxService.isNullOrUndefined(value)) value = 0;

    var strresult = value.toFixed(decimalPlaces);

    return parseFloat(strresult);
  }

  static customNavigate(
    router: Router,
    cs: CrypterService,
    routelink: any,
    config: any
  ) {
    var configJSON = JSON.stringify(config);

    configJSON = cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    router.navigate(routelink);
  }

  static removeHistoricalFilter() {
    var localstorageItems: any[] = this.getAllLocalStorageItems();

    localstorageItems.forEach((item: any) => {
      var name = item.name;

      if (name.includes('_ListViewFilter')) {
        localStorage.removeItem(name);
      }
    });
  }

  static getAllLocalStorageItems() {
    var archive = [],
      keys = Object.keys(localStorage),
      i = 0,
      key;

    for (; (key = keys[i]); i++) {
      archive.push({
        name: key,
        value: localStorage.getItem(key),
      });
    }

    return archive;
  }

  static getMonthStartAndEnd(year: number, month: number): { startDate: Date, endDate: Date } {

    const startDate = new Date(year, month, 1);
    const endDate = new Date(year, month + 1, 0);

    return { startDate, endDate };
  }

  static getFormatDateString(date: Date) {
    const currentDate = date;

    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  static getFormatDateTimeString(date: Date, format: string) {
    var datepipe: DatePipe = new DatePipe('en-US')
    let formattedDate = datepipe.transform(date, format)

    return formattedDate
  }

  static getMonthList(): MonthInfo[] {

    var months: MonthInfo[] = [];

    for (let i = 1; i <= 12; i++) {

      const monthName = this.getMonthName(i);
      const month: MonthInfo = { Number: i, Name: monthName };

      months.push(month);
    }

    return months;
  }

  static getMonthName(monthNumber: number): string {
    // Create an array of month names or use a function to get the name based on the number
    const monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    return monthNames[monthNumber - 1]; // Adjust for 0-based index
  }

  static getYearList(): number[]
  {
    var currentYear = moment().year();
    var years: number[] = [];

    for (var counter = currentYear; counter >= 2020; counter--) {

      years.push(counter);
    }

    return years;
  }
}
