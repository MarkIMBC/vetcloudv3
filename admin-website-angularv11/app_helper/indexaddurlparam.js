var fs = require('fs');
var dateTime = require('node-datetime');
var dt = dateTime.create();
var formatted = dt.format('YmdHMS');

var path = '..\\build\\dist\\admin-website-angularv11\\index.html';

fs.readFile(path,'utf-8', function (err, data) {

  if (err) throw err;

  var newValue = data;

  var i;
  for (i = 0; i < 100; i++) {

    newValue = newValue.replace('.css"', `.css?v=${formatted}"`);
    newValue = newValue.replace('.js"', `.js?v=${formatted}"`);
  }

  console.log(newValue);

  fs.writeFile(path, newValue, 'utf-8', function (err, data) {
    if (err) throw err;
    console.log('Done!');
  })
})
