const sql = require('mssql');
var fs = require('fs');
const prettier = require("prettier");
var colors = require('colors');
const { query } = require('@angular/animations');
const { resolve } = require('path');
require('linqjs');


var  CryptoJS = require('crypto-js');
const CrypterKey = "sql123$%^abc123$";

function encrypt(str) {
    var key = CryptoJS.enc.Utf8.parse(CrypterKey);
    var iv = CryptoJS.enc.Utf8.parse(CrypterKey);

    let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(str), key,
        {
            keySize: 128 / 2,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
    return encrypted.toString();
  }


const config = {
    user: 'backup',
    password: 'sql123',
    server: 'DESKTOP-E9HPC98',
    database: 'db_vetcloudv3_oldserver',
    options: {
        port: 1433,
        encrypt: false, // Use this if you're on Windows Azure
        //instanceName: '.'
    }
};

sql.on('error', err => {
    console.log('On ERR ->' + err);
})

async function getDetailView(ID_DetailView) {
    return new Promise((resolve, reject) => {
        sql.connect(config).then(function(conn) {
            var request = new sql.Request(conn);
            request.input('Oid', sql.UniqueIdentifier, ID_DetailView);
            request.execute('_pGetDetailViewWithDetails').then(function(data) {
                //console.log('done');
                resolve(data);
            }).catch(function(err) {
                reject(err);
                console.log(err);
            });
        });
    });
}

async function getListView(ID_ListView) {
    return new Promise((resolve, reject) => {
        sql.connect(config).then(function(conn) {
            var request = new sql.Request(conn);
            request.input('Oid', sql.UniqueIdentifier, ID_ListView);
            request.execute('_pGetListViewWithDetails').then(function(data) {
                resolve(data);
            }).catch(function(err) {
                console.log(err);
            });
        });
    });
}

async function Query(qry) {
    return new Promise((resolve, reject) => {
        sql.connect(config).then(pool => {
            return pool.request().query(qry)
        }).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};
const {promisify} = require("util")
var writeFile = promisify(fs.writeFile);
(async() => {
    var DetailViews = await Query("SELECT * FROM _tDetailView");
    var ListViews = await Query("SELECT * FROM _tListView");
    var DetailViewList = {};
    var ListViewList = {};
   // console.log(DetailViews);
    DetailViews.recordset.forEach(async(dv) => {
        var Oid_DetailView = dv.Oid;
        DetailViewList[dv.Oid] = dv.Name;
        //console.log("getting-->", dv.Name);
        var data = await getDetailView(Oid_DetailView);
        //console.log("getting done -->", dv.Name);
        var objectSchema = data.recordset[0];
        var detailView = data.recordsets[1][0];
        //var detailViewControls = data.recordsets[1];
        //console.dir(detailView);
        var properties = Object.keys(objectSchema);
        //console.log('dvName--->',properties.length);
        if (properties.length > 1) {
            for (var i = 1; i < properties.length; i++) {
                var det = data.recordsets[i + 1];
                det.forEach((d) => {
                    var keys = Object.keys(d);
                    keys.forEach((k) => {
                        if (Array.isArray(d[k]) == true) {
                            var value = null;
                            d[k].forEach((val) => {
                                if (value != null || value != undefined) return;
                                value = val;
                            });
                            d[k] = value;
                        }
                        if (["DataSource", "DataSourceKey"].includes(k) == true && (d[k] != null && d[k] !== undefined)) {
                            var ds = d[k];
                            d[k] = encrypt(ds);
                        }
                    });

                });
                detailView[properties[i]] = det;
            }
        }
        try {
            var str = JSON.stringify(detailView, null, 4);
            //console.log(dv.Name)
            await writeFile(`src/assets/detailview/${dv.Name}.json`, str);
        } catch (err) {
            console.log(err);
        }
    });
    ListViews.recordset.forEach(async(lv) => {
        var Oid_ListView = lv.Oid;
        ListViewList[Oid_ListView] = lv.Name;
        //console.log(dv);
        var data = await getListView(Oid_ListView);
        var objectSchema = data.recordset[0];
        var listView = data.recordsets[1][0];
        //var detailViewControls = data.recordsets[1];
        //console.dir(detailView);
        var ds = listView.DataSource;
        var properties = Object.keys(objectSchema);
        if (properties.length > 1) {
            for (var i = 1; i < properties.length; i++) {
                var det = data.recordsets[i + 1];
                det.forEach((d) => {
                    var keys = Object.keys(d);
                    keys.forEach((k) => {
                        if (Array.isArray(d[k]) == true) {
                            var value = null;
                            d[k].forEach((val) => {
                                if (value != null || value != undefined) return;
                                value = val;
                            });
                            d[k] = value;
                        }
                        if (["DataSource", "DataSourceKey"].includes(k) == true && (d[k] != null && d[k] !== undefined )) {
                            var ds = d[k];
                            d[k] = encrypt(ds);
                        }
                    });

                });
                listView[properties[i]] = det;
            }
        }
        try {
            var str = JSON.stringify(listView, null, 4);
             await writeFile(`src/assets/listview/${lv.Name}.json`, str);
        } catch (err) {
            console.log(err);
        }
    });
    await writeFile(`src/assets/detailview/_Index.json`, JSON.stringify(DetailViewList), null, 4);
    await writeFile(`src/assets/listview/_Index.json`, JSON.stringify(ListViewList), null, 4);
    //console.log('done wrting dapat -->', Object.keys(DetailViewList).length);
    // setTimeout((function() {
    //     return process.exit(-1);
    // }), 1000);
})();
