import {
  Schedule,
  ToothSurface,
  Employee,
  Patient,
  Patient_History,
  Patient_SOAP,
  Patient_SOAP_Plan,
  Patient_SOAP_Prescription,
  Message,
  Tooth,
  ToothStatus,
  ToothInfo,
  Patient_DentalExamination,
  Patient_DentalExamination_Image,
  Patient_DentalExamination_ToothInfo,
  Patient_DentalExamination_MedicalHistory,
  Item,
  PurchaseOrder,
  PurchaseOrder_Detail,
  BillingInvoice_Detail,
  BillingInvoice,
  ReceivingReport_Detail,
  ReceivingReport,
  InventoryTrail,
  PatientAppointment,
  AppointmentSchedule,
  PaymentTransaction,
  User,
  User_Roles,
  UserRole,
  UserRole_Detail,
  UserRole_Reports,
  Client,
  Patient_Confinement_ItemsServices,
  PatientWaitingList,
  Patient_Vaccination,
  Patient_SOAP_Treatment,
  BillingInvoice_Patient,
  Patient_Wellness,
  Patient_Wellness_Detail,
  Patient_Lodging,
  VeterinaryHealthCertificate,
  Patient_Confinement_Patient,
  Patient_Confinement,
  Payable,
  PayablePayment,
  Payable_Detail,
  Patient_Grooming,
  Patient_Grooming_Detail,
  PatientReferral,
} from './APP_MODELS';

export enum FilterCriteriaType {
  Exclude = '0',
  Equal = '1',
  NotEqual = '2',
  Contains = '3',
  NotContains = '4',
  Like = '5',
  NotLike = '6',
  StartWith = '7',
  EndWith = '8',
  GreaterThan = '9',
  LessThan = '10',
  GreaterThanEqual = '11',
  LessThanEqual = '12',
  Between = '13',
}

export interface IControlModelArg {
  name: string;
  value: any;
  displayName?: string;
  displayValue?: any;
  data?: any;
  file?: any;
}

export interface IDetailViewAfterSavedArg {
  CurrentObject: any;
  Model: any;
}
export interface IFilterFormValue {
  alias?: string;
  dataField?: string;
  filterCriteriaType?: FilterCriteriaType;
  value?: any;
  propertyType?: number;
}

export class PagingOption {
  PageNumber: number = 1;
  DisplayCount: number = 25;
  TotalRecord: number = 0;
  TotalPageNumber: number = 0;
}

export enum PropertyTypeEnum {
  String = 1,
  Int = 2,
  Decimal = 3,
  Bit = 4,
  DateTime = 5,
  Date = 6,
  Time = 7,
  Uniqueidentifier = 8,
  Object = 9,
  List = 10,
  Color = 11,
  Image = 12,
}

export enum ItemCategoryEnum {
  Service = 1,
  Inventoriable = 2,
}

export enum OrderArrangeEnum {
  ASC = 1,
  DESC = 0,
}

export enum ScheduleColorEnum {
  AlreadyFillSlot = '#a90505',
  HasVacantSlot = '#116fbf',
  PatientSOAP = '#116fbf',
  PatientAppointment = 'rgb(218 72 3)',
}

export enum QuestionTypeEnum {
  YesOrNo = 1,
  Specific = 2,
}

export enum ItemTypeEnum {
  Service = 1,
  Inventoriable = 2,
}

export enum PaymentMethodEnum {
  Cash = 1,
  Check = 2,
  GCash = 3,
  DebitCredit = 4,
}

export enum FilingStatusEnum {
  Filed = 1,
  Pending = 2,
  Approved = 3,
  Cancelled = 4,
  PartiallyServed = 5,
  FullyServed = 6,
  Waiting = 8,
  Ongoing = 9,
  Payment = 10,
  PartiallyPaid = 11,
  FullyPaid = 12,
  Done = 13,
  Confined = 14,
  Discharged = 15,
  ForBilling = 16,
  ForApprove = 20,
  Reschedule = 21,
}

export enum TaxSchemeEnum {
  TaxExclusive = 1,
  TaxInclusive = 2,
  ZeroRated = 3,
}

export enum ControlTypeEnum {
  AutoComplete = 17,
  CheckBox = 4,
  ColorPicker = 10,
  DatePicker = 6,
  DateTimePicker = 5,
  FileUploader = 16,
  ImageBox = 15,
  ListBox = 9,
  ListView = 11,
  NumberBox = 3,
  RichTextBox = 14,
  Section = 13,
  SelectBox = 8,
  Tab = 12,
  TextArea = 2,
  TextBox = 1,
  TimePicker = 7,
  MultipleListBox = 18,
  MultipleSelectBox = 19,
}

export class DetailViewModel {
  Oid?: string;
  Code?: string;
  Name?: string;
  IsActive?: boolean;
  Comment?: String;
  ID_Model?: string;
  Caption?: string;
  Details?: DetailView_Detail[];
  ModelName?: string;
  SelectBoxOptions?: any;
}

export class DetailView_Detail {
  SeqNo?: number;
  ID_PropertyType?: PropertyTypeEnum;
  GroupIndex?: number;
  DataSource?: string;
  DataSourceKey?: string;
  DisplayProperty?: string;
  Oid?: string;
  Code?: string;
  Name?: string;
  IsActive?: boolean;
  Comment?: string;
  Caption?: string;
  ID_ModelProperty?: string;
  ID_DetailView?: string;
  ID_Tab?: string;
  ID_Section?: string;
  ID_ControlType?: ControlTypeEnum;
  Format?: string;
  IsLoadData?: boolean;
  ColCount?: number;
  ColSpan?: number;
  IsDisabled?: boolean;
  Height?: number;
  ID_ListView?: string;
  IsReadOnly?: boolean;
  ID_LabelLocation?: LabelLocaltionEnum;
  IsShowLabel?: boolean;
  IsRequired?: boolean;
  ID_PropertyModel?: string;
  PropertyName?: boolean;
  PropertyModel?: string;
  IsSearchEnabled?: boolean;
  ID_PropertyModel_ID_DetailView?: string;
  // , DisplayExpr, ID_LookUp_ListView, LookUp_ListView_Caption, LookUp_ListView_DataSource, GroupIndex1, SearchExpr, Precision, ID_Parent_Grid, IsShowClearButton, IsSearchEnabled, InputMask, , , ID_PropertyModel_DetailView, ID_PropertyModel_DetailView_Height, ID_PropertyModel_DetailView_Width, PropertyModel_PrimaryKey, PropertyModel_Caption, ID_PropertyModel_ID_DetailView, LookUpDataSource, LookUp_ListView_PK, LookUp_ListView_Model, ID_ModelProperty_Key, ModelProperty_Key, IsAllowAdd, ListViewDetailName
  Childs?: DetailView_Detail[] = [];
}

export class ListViewModel {
  Oid?: string;
  Name?: string;
  ID_Model?: string;
  DataSource?: string;
  Comment?: string;
  ID_DetailView?: string;
  Caption?: string;
  Details?: ListViewModelDetail[];
  Model?: string;
  PrimaryKey?: string;
}

export class ListViewModelDetail {
  ID_PropertyType?: PropertyTypeEnum;
  Precision?: number;
  DataSource?: string;
  DisplayProperty?: string;
  Oid?: string;
  Code?: string;
  Name?: string;
  IsActive?: boolean;
  Comment?: string;
  Caption?: string;
  ID_ModelProperty?: string;
  ID_ListView?: string;
  Format?: string;
  Width?: number;
  Fixed?: boolean;
  VisibleIndex?: number;
  IsAllowEdit?: boolean;
  ID_ControlType?: number;
  ID_ColumnAlignment?: ColumnAlignmentEnum;
  IsVisible?: boolean;
  FixedPosition?: string;
  IsRequired?: boolean;
  ID_SummaryType?: number;
  IsAddModelClass?: boolean;
  ID_PropertyModel?: string;
  PropertyModel?: string;
  SummaryType?: string;
  GroupIndex?: number;
  IsFilter?: boolean;
  ID_FilterControlType?: number;
}

export enum ColumnAlignmentEnum {
  Left = 1,
  Right = 3,
  Top = 2,
}

export enum AlignEnum {
  left = 0,
  center = 1,
  right = 2,
}

export enum LabelLocaltionEnum {
  Left = 1,
  Right = 3,
  Top = 2,
}

export function DEBUG(fn: Function): void {
  fn();
}

export function isFloat(n: any): boolean {
  return Number(n) === n && n % 1 !== 0;
}

export interface IFile {
  dataField: string;
  file: File;
  isImage: boolean;
}

export class LabImage {
  RowIndex?: number;
  ImageNo?: string;
  File?: File;
  FilePath?: boolean;
  Remarks?: string;
}

export enum ViewTypeEnum {
  ListView = 1,
  DetailView = 2,
  ReportView = 3,
  Custom = 4,
}

export enum PositionEnum {
  Dentist = 10,
  Receptionist = 11,
  Inventory = 12,
  Veterinary = 10,
}

export enum UserGroupEnum {
  Dentist = 7,
  ProjectManager = 3,
  ProjectManagerSpecialist = 4,
  QualityAssurance = 5,
  Receptionist = 6,
  System = 1,
  SystemDeveloper = 2,
  Standard = 9,
}

export interface ChartModel {
  data: [];
  label: string;
}
export interface MsgThread {
  message?: string;
  position?: string;
}

export enum ToothSurfaceEnum {
  Distal = 1,
  Misial = 2,
  Buccal = 3,
  Occlusal = 4,
  Lingual = 5,
  Incisal = 6,
  Labial = 7,
}

export enum PositionEnum {
  Top = 0,
  Left = 1,
  Middle = 2,
  Bottom = 3,
  Right = 4,
}

export enum SOAPTypeEnum {
  Consultation = 1,
  Confinement = 2,
  Emergency = 3,
  WalkIn = 4,
}

export class Schedule_DTO extends Schedule {
  ScheduleType?: string;
  Doctor?: string;
  AvailableCount?: number;
  FormattedDateStart?: string;
  FormattedDateEnd?: string;
}

export enum ToothStatusTypeEnum {
  Caries = 1,
  Endo = 2,
  General = 3,
  Implant = 4,
  Preventive = 5,
  Prostho = 6,
  Resto = 7,
  Surgery = 8,
}

export enum DentitionEnum {
  Mixed = 3,
  Permanent = 1,
  Primary = 2,
}
export enum InventoryStatusEnum {
  High = 4,
  Medium = 3,
  Low = 2,
  NoStock = 1,
}

export enum TeethQuadrantEnum {
  Quandrant1 = 1,
  Quandrant2 = 2,
  Quandrant3 = 3,
  Quandrant4 = 4,
  Quandrant5 = 5,
  Quandrant6 = 6,
  Quandrant7 = 7,
  Quandrant8 = 8,
}

export class Employee_DTO extends Employee {
  Name_Position?: string;
}

export class Dentist_DTO extends Employee {}

export class Client_DTO extends Client {
  ContactNumbers?: string;
  Name_Gender?: string;
  PhoneCode_Country?: string;
  CreatedBy_Name_User?: string;
  LastModifiedBy_Name_User?: string;
  Patient_History_DTO?: Patient_History_DTO[] = [];
}

export class Patient_DTO extends Patient {
  Name_Client?: string;
  Name_Gender?: string;
  PhoneCode_Country?: string;
  CreatedBy_Name_User?: string;
  LastModifiedBy_Name_User?: string;
  Patient_History_DTO?: Patient_History_DTO[] = [];
  LabelActionQueue?: Patient_History_DTO[] = [];
  WaitingStatus_Name_FilingStatus?: string;
}
export class Patient_History_DTO extends Patient_History {
  Name_Doctor?: string;
  Name_FilingStatus?: string;
  DateString?: string;
}
export class Patient_SOAP_DTO extends Patient_SOAP {
  Name_SOAPType: string = '';
  Name_Client: string = '';
  Name_Patient: string = '';
  Name_FilingStatus: string = '';

  Patient_SOAP_Plan: Patient_SOAP_Plan_DTO[] = [];
  Patient_SOAP_Prescription: Patient_SOAP_Prescription_DTO[] = [];
  LabImages: LabImage[] = [];
}
export class Patient_SOAP_Plan_DTO extends Patient_SOAP_Plan {
  DateSent?: Date;
  Name_Item: string = '';
}
export class Patient_SOAP_Prescription_DTO extends Patient_SOAP_Prescription {
  Name_Item: string = '';
}

export class Patient_SOAP_Treatment_DTO extends Patient_SOAP_Treatment {
  Name_Item: string = '';
}

export class Patient_Vaccination_DTO extends Patient_Vaccination {
  Name_Item: string = '';
  Name_Client: string = '';
  Name_Patient: string = '';
  Name_FilingStatus: string = '';
}

export class Patient_Lodging_DTO extends Patient_Lodging {
  Name_Company: string = '';
  Name_CreatedBy: string = '';
  Name_LastModifiedBy: string = '';
  Name_Client: string = '';
  Name_Patient: string = '';
  Name_FilingStatus: string = '';
}

export class Message_DTO extends Message {
  Recipient_Name_User?: string;
}

export class MessageThread {
  ID?: number;
  Recipient_ID_User?: number;
  Sender_ID_User?: number;
  Recipient_Name_User?: number;
  Sender_Name_User?: number;
  Message?: string;
  DateSent?: Date;
  DateRead?: Date;
  IsRead?: boolean;
}

export class MessagRecipient {
  Sender_ID_User?: number;
  Recipient_ID_User?: number;
  Recipient_Name_User?: string;
  TotalReadCount?: number;
  LastDateSent?: Date;
  IsRead?: boolean;

  MessageThreads?: MessageThread[] = [];
}
export class Tooth_DTO extends Tooth {
  Left_Name_ToothSurface?: string;
  Top_Name_ToothSurface?: string;
  Bottom_Name_ToothSurface?: string;
  Right_Name_ToothSurface?: string;
  Middle_Name_ToothSurface?: string;
  IDs_ToothSurface?: string = '';
  ToothStatuses?: ToothStatus_DTO[] = [];
}

export class ToothStatus_DTO extends ToothStatus {
  __isShowCloseBtn?: boolean;
}

export class ToothInfo_DTO extends ToothInfo {
  ToothNumber_Tooth?: number;
  Location_Tooth?: string;
  Code_ToothStatus?: string;
  Name_ToothStatus?: string;
  ID_Dentitiion?: number;
}

export class Doctor extends Employee {}

export interface IToothInfoArg {
  component: any;
  toothInfo: ToothInfo_DTO;
}

export interface ITooth_ToothStatusArg {
  tooth: Tooth_DTO;
  toothStatus: ToothStatus_DTO;
}

export class Patient_DentalExamination_DTO extends Patient_DentalExamination {
  DateString?: string;
  Patient?: number;
  Doctor?: string;
  ToothInfos?: Patient_DentalExamination_ToothInfo_DTO[] = [];
  MedicalHistories?: Patient_DentalExamination_MedicalHistory_DTO[] = [];
  Images?: Patient_DentalExamination_Image[] = [];
  IsDelete?: boolean = false;
  Name_FilingStatus?: string;
  _deletedItems?: any;
}

export class Patient_DentalExamination_ToothInfo_DTO extends Patient_DentalExamination_ToothInfo {
  ToothNumber_Tooth?: number;
  Location_Tooth?: string;
  Name_ToothStatus?: string;
  Code_ToothStatus?: string;
  ToothSurfaces: ToothSurface[] = [];
}

export class Patient_DentalExamination_MedicalHistory_DTO extends Patient_DentalExamination_MedicalHistory {
  Parent_ID_MedicalHistoryQuestionnaire?: number;
  Parent_Comment_MedicalHistoryQuestionnaire?: string;
  IsParent?: boolean;
  ID_QuestionType?: number;
}

export class SavingPatientDentalExamToothInfo extends Patient_DentalExamination_ToothInfo {
  ID_Patient_DentalExamination?: number;
  Date?: Date;
  ID_Patient?: number;
  ID_Doctor?: number;
  ID_Patient_DentalExamination_ToothInfo?: number;
  ID_Tooth?: number;
  IDs_ToothSurface?: string;
  ID_ToothStatus?: number;
  Comment?: string;
  IsDelete?: boolean;
  ID_Dentition?: number;
  Prescription?: string;
}

export class Item_DTO extends Item {
  Name_InventoryStatus?: string;
  Name_ItemCategory?: string;
  Displayed_Name_ItemCategory?: string;
}

export class Item_Supplier_DTO extends Item {
  Name_Supplier?: string;
  Name_Item?: string;
}

export class PurchaseOrder_DTO extends PurchaseOrder {
  Name_Supplier?: string;
  Name_TaxScheme?: string;
  Name_FilingStatus?: string;
  ServingStatus_Name_FilingStatus?: string;
  CreatedBy_Name_User?: string;
  LastModifiedBy_Name_User?: string;
  ApprovedBy_Name_User?: string;
  CanceledBy_Name_User?: string;
  PurchaseOrder_Detail?: PurchaseOrder_Detail_DTO[];
}

export class PurchaseOrder_Detail_DTO extends PurchaseOrder_Detail {
  UOM?: string;
  Name_Item?: string;
}

export class BillingInvoice_Detail_DTO extends BillingInvoice_Detail {
  Name_Item?: string;
  isDirty?: boolean = false;
}

export class BillingInvoice_DTO extends BillingInvoice {
  Status?: string;
  AttendingPhysician_Name_Employee?: string;
  Name_Client?: string;
  Name_Patient?: string;
  Name_TaxScheme?: string;
  Name_FilingStatus?: string;
  Name_SOAPType?: string;
  Payment_Name_FilingStatus?: string;
  CreatedBy_Name_User?: string;
  LastModifiedBy_Name_User?: string;
  ApprovedBy_Name_User?: string;
  CanceledBy_Name_User?: string;
  BillingInvoice_Detail: BillingInvoice_Detail_DTO[] = [];
  BillingInvoice_Patient: BillingInvoice_Patient_DTO[] = [];
  TooltipText?: string = '';
}
export class BillingInvoice_Patient_DTO extends BillingInvoice_Patient {
  Name_Patient?: string;
}

export class ReceivingReport_Detail_DTO extends ReceivingReport_Detail {
  Name_Item?: string;
  isDirty?: boolean = false;
}
export class ReceivingReport_DTO extends ReceivingReport {
  Code_PurchaseOrder?: string;
  Name_Supplier?: string;
  Name_FilingStatus?: string;
  Name_TaxScheme?: string;
  CreatedBy_Name_User?: string;
  LastModifiedBy_Name_User?: string;
  ApprovedBy_Name_User?: string;
  CanceledBy_Name_User?: string;
  ReceivingReport_Detail?: ReceivingReport_Detail_DTO[] = [];
}

export class Inventory {
  ID_Item?: number;
  Name_Item?: string;
  Quantity?: number;
  ID_InventoryStatus?: number;
  Name_InventoryStatus?: number;
  colorStatus?: string = '';
}

export class InventoryTrail_DTO extends InventoryTrail {
  Name_Item?: string;
  Name_FilingStatus?: string;
  DateString?: string;
}
export class AppointmentEvent {
  UniqueID?: string;
  Oid_Model?: string;
  Name_Model?: string;
  ID_Company?: number;
  ID_CurrentObject?: number;
  DateStart?: Date;
  DateEnd?: Date;
  FormattedDateStart?: string;
  FormattedDateEnd?: string;
  FormattedDateStartTime?: string;
  FormattedDateEndTime?: string;
  ReferenceCode?: string;
  Paticular?: string;
  Description?: string;
}
export class PatientAppointment_DTO extends PatientAppointment {}

export class AppointmentSchedule_DTO extends AppointmentSchedule {
  ScheduleType?: string;
  Name_ServiceType?: string;
  AppointmentStatus_Name_FilingStatus?: string;
}

export interface IFormValidation {
  message: string;
}
export class ReceiveInventory {
  IsAddInventory: boolean = false;
  ID_Item?: number;
  Quantity?: number;
  UnitPrice?: number;
  DateExpired?: Date;
  BatchNo?: number;
  ID_FilingStatus?: number;
  ID_Company?: number;
  Comment?: string;
}

export class MergePet {
  ID_Patient_Source?: number;
  ID_Patient_Destination?: number;
  Name_Patient_Destination?: string;
}

export class MergeClient {
  ID_Client_Source?: number;
  ID_Client_Destination?: number;
  Name_Client_Destination?: string;
}

export class BulkTextMessage {
  _strCellPhoneNumbers?: string;
  ID: number = -1;
  CellPhoneNumbers?: string[];
  Message?: string;
  Log?: string = '';
}

export class PaymentTransaction_DTO extends PaymentTransaction {
  DateString?: string;
  Code_BillingInvoice?: string;
  BillingInvoice_Detail?: BillingInvoice_Detail_DTO[] = [];
  Name_Client?: string;
  Name_Patient?: string;
  Name_FilingStatus?: string;
  Name_PaymentMethod?: string;
  PatientNames?: string;
}
export class User_DTO extends User {
  Name_Employee?: string;
}
export class User_Role_DTO extends User_Roles {
  Name_UserRole?: string;
}

export class UserRole_DTO extends UserRole {
  Name_Model?: string;
  UserRole_Detail?: UserRole_Detail_DTO[];
  UserRole_Reports?: UserRole_Report_DTO[];
}

export class UserRole_Detail_DTO extends UserRole_Detail {
  Name_Model?: string;
}

export class UserRole_Report_DTO extends UserRole_Reports {
  Name_Report?: string;
}

export class Patient_Confinement_DTO extends Patient_Confinement {
  Patient_Confinement_Patient?: Patient_Confinement_Patient_DTO[] = [];
}

export class Patient_Confinement_Patient_DTO extends Patient_Confinement_Patient {
  Name_Patient?: string;
}

export class Patient_Confinement_ItemsServices_DTO extends Patient_Confinement_ItemsServices {
  Name_Item?: string;
  ID_ItemType?: number;
}

export class User_UserRoles_DTO extends User_Roles {
  Name_UserRole?: string;
  ID_UserRole?: number;
  IsActive?: boolean;
}

export class PatientWaitingList_DTO extends PatientWaitingList {
  Name_Client?: string;
  Name_Patient?: string;
  WaitingStatus_Name_FilingStatus?: string;
  BillingInvoice_Name_FilingStatus?: string;
  LabelActionQueue?: string;
}

export class ClientCredit {
  ID_Client?: number;
  Date?: string;
  CreditAmount?: number;
  Code?: string;
  Comment?: string;
}

export enum ConfimentItemServiceModeEnum {
  All = 1,
  Item = 2,
  Service = 3,
}

export enum PatientSOAPRecordInfoEnum {
	PrimaryComplaintOrHistory,
	ClinicalExam,
	LaboratoryOrInterpretation,
	Plan,
	Diagnosis,
	Treatment,
	Prescription,
	ClientCommunication,
}

export enum PatientReferralRecordInfoEnum {
	History,
  Treatment,
  Concerns
}


export class Patient_Wellness_DTO extends Patient_Wellness {
  Name_Client?: string;
  Name_Patient?: string;
  Name_FilingStatus?: string;
}

export class Patient_Wellness_Detail_DTO extends Patient_Wellness_Detail {
  Name_Item?: string;
}
export class Dashboard {
  ID_LastModifiedBy?: number;
  Name?: string;
  Comment?: string;
  ID?: number;
  IsActive?: boolean;
  Code?: string;
  ID_Company?: number;
  ID_CreatedBy?: number;
  DateCreated?: Date;
  DateModified?: Date;
  ClientDailyCount?: number;
  ClientMonthlyCount?: number;
  SalesDailyAmount?: number;
  SalesMonthlyAmount?: number;
}

export class Dashboard_DTO extends Dashboard {
  ClientDailyCount?: number = 0;
  ClientMonthlyCount?: number = 0;
  SalesDailyAmount?: number = 0;
  SalesMonthlyAmount?: number = 0;
}

export class VeterinaryHealthCertificate_DTO extends VeterinaryHealthCertificate {
  Name_Client?: string;
  Name_Patient?: string;
  Name_FilingStatus?: string;
  AttendingPhysician_Name_Employee?: string;
  Name_Item?: string;
}

export class Payable_DTO extends Payable {}

export class Payable_Detail_DTO extends Payable_Detail {
  Name_ExpenseCategory: string = '';
}
export class PayablePayment_DTO extends PayablePayment {
  RemaningAmount?: number;
  Name_Payable_Detail?: string;
}

export class Patient_Grooming_DTO extends Patient_Grooming {
  Name_Client?: string;
  Name_Patient?: string;
  Name_FilingStatus?: string;
  AttendingPhysician_Name_Employee?: string;
  Name_Item?: string;
}

export class Patient_Grooming_Detail_DTO extends Patient_Grooming_Detail {
  Name_Item?: string;
}

export class CompanyGUID {
  static PetstacularVeterinaryClinic: string = "85792A0D-81B7-49E0-B6E5-4F00791A2254";
  static PetstacularVeterinaryClinicCuyapo: string = "2E4A20E0-CA32-4473-BE4B-0921CD3B4ACF";
}

export class MonthInfo {
  Number: number = 0;
  Name: string = "";
}

export class PatientReferral_DTO extends PatientReferral {
  Name_Client?: string;
  Name_Patient?: string;
  AttendingPhysician_Name_Employee?: string;
  Name_FilingStatus?: string;
  SummaryInfos: any[] = [];
}

export class ReferralType_DTO extends PatientReferral {
  Name: string=  "";
  CheckBoxName: string=  "";
}

export class ReferralTypeCheckBoxObject{

  IsReferralType_1_Checked: boolean = false;
  IsReferralType_2_Checked: boolean = false;
  IsReferralType_3_Checked: boolean = false;
  IsReferralType_4_Checked: boolean = false;
  IsReferralType_5_Checked: boolean = false;
  IsReferralType_6_Checked: boolean = false;
}
