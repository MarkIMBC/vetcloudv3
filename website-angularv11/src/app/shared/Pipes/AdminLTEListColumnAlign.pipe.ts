import { Pipe, PipeTransform } from "@angular/core";
import { AlignEnum } from "src/shared/APP_HELPER";
import { AdminLTEListColumn } from "../AdminLTEListColumn";

@Pipe({name: 'adminltelistcolumnalign'})
export class AdminLTEListColumnAlign implements PipeTransform {
  transform(column: AdminLTEListColumn): any{

    var result = '';

    if(column.align == AlignEnum.center){

      result = 'center'
    }else if(column.align == AlignEnum.right){

      result = 'right'
    }else{

      result = 'left';
    }

    return result;
  }
}
