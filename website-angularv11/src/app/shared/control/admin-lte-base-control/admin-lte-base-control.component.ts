import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { DataService } from 'src/app/core/data.service';
import { IControlModelArg } from 'src/shared/APP_HELPER';

@Component({
  selector: 'admin-lte-base-control',
  templateUrl: './admin-lte-base-control.component.html',
  styleUrls: ['./admin-lte-base-control.component.less'],
})
export class AdminLTEBaseControlComponent{

  @ViewChild('input')
  input: ElementRef | undefined;

  id: string = '';

  @Output() onEnterKeyUp: EventEmitter<IControlModelArg> = new EventEmitter();
  @Output() onModelChanged: EventEmitter<IControlModelArg> = new EventEmitter();

  @Input() name: string = '';
  @Input() displayName: string = '';
  @Input() caption: string = '';
  @Input() format: string = '';
  @Input() helptext: string = '';
  idTag: string = '';

  @Input() set initialValue(val: any) {

    this.value = val;
  }
  value: any = '';

  @Input() readonly: boolean = false;
  @Input() placeholder: string = '';

  constructor(protected ds: DataService) {

    this.idTag = Math.floor(Math.random() * 99999) + moment().format('yyyyMMDDTHHmmss');
  }

  ngOnInit(): void {

  }

  _onModelChanged(event: any) {

    this.onModelChanged.emit({
      name: this.name,
      value: this.value
    });
  }

  onKey(event: any) {

    if(event.keyCode == 13){

      this.onEnterKeyUp.emit({
        name: this.name,
        value: this.value
      });
    }
  }

  ngAfterViewInit(){
    this.id = this.name;

    this.afterLoad();
  }

  afterLoad(): void { }

}
