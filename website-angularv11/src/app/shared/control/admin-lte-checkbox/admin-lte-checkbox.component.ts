import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'admin-lte-checkbox',
  templateUrl: './admin-lte-checkbox.component.html',
  styleUrls: ['./admin-lte-checkbox.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTECheckboxComponent) }]
})
export class AdminLTECheckboxComponent extends AdminLTEBaseControlComponent {



}
