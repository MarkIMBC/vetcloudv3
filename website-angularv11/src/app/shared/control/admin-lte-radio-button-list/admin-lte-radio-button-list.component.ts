import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg } from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'admin-lte-radio-button-list',
  templateUrl: './admin-lte-radio-button-list.component.html',
  styleUrls: ['./admin-lte-radio-button-list.component.less'],
  providers: [
    {
      provide: AdminLTEBaseControlComponent,
      useExisting: forwardRef(() => AdminLTERadioButtonListComponent),
    },
  ],
})
export class AdminLTERadioButtonListComponent extends AdminLTEBaseControlComponent {
  @Input()
  listDateSource: AdminLTERadioButtonListDateSourceItem[] = [];
}

export class AdminLTERadioButtonListDateSourceItem {
  value: any;
  label: string = '';
}
