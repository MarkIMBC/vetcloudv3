import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLTEAutoCompleteNumberBoxComponent } from './admin-lte-auto-complete-number-box.component';

describe('AdminLTEAutoCompleteNumberBoxComponent', () => {
  let component: AdminLTEAutoCompleteNumberBoxComponent;
  let fixture: ComponentFixture<AdminLTEAutoCompleteNumberBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminLTEAutoCompleteNumberBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLTEAutoCompleteNumberBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
