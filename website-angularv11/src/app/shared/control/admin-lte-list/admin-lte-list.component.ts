import { DataService } from 'src/app/core/data.service';
import { PagingOption } from 'src/shared/APP_HELPER';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { AdminLTEListColumn } from '../../AdminLTEListColumn';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'admin-lte-list',
  templateUrl: './admin-lte-list.component.html',
  styleUrls: ['./admin-lte-list.component.less'],
})
export class AdminLTEListComponent implements OnInit {
  @Input() option: AdminLTEListOption | undefined;
  @Output() onRowSelected: EventEmitter<AdminLTEListRowSelectedArg> =
    new EventEmitter<AdminLTEListRowSelectedArg>();
  @Output() onDataSourceLoaded: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('ngbpagination') ngbpagination: NgbPagination | undefined;

  @Input() ismultiple: boolean = false;
  @Input() valueMember: string = 'ID';
  @Input() displayMember: string = 'Name';
  @Input() IsShowFilter: boolean = true;
  @Input() IsShowMenuBar: boolean = true;

  selectedValues: any[] = [];

  TotalRecordCount: number = 0;
  LoadedRecordCount: number = 0;

  OrderByString: string = '';

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Select',
      icon: 'fa fa-plus',
      name: 'Select',
      visible: true,
    },
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      name: 'Refresh',
      visible: true,
    },
  ];

  filterColumnValue: any = null;
  filterValue: any = null;

  pagingOption: PagingOption = new PagingOption();
  dataSource: any[] = [];

  cardBodyHeight: string = '400px';
  gridHeight: string = '400px';

  loading: boolean = false;

  constructor(private ds: DataService) {}

  async ngOnInit(): Promise<void> {
    this.pagingOption.DisplayCount = 10;

    window.onresize = () => {
      setTimeout(() => {
        this.resize();
      }, 100);
    };
  }

  async ngAfterViewInit() {
    this.showHideMenuSelect();

    setTimeout(() => {
      this.resize();
    }, 2000);
  }

  resize() {
    var height = window.innerHeight;
    var cardBodyHeight = 0;
    var gridHeight = 0;

    cardBodyHeight = height - 318;
    gridHeight = height - 330;

    if (cardBodyHeight < 350) {
      cardBodyHeight = 400;
      gridHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
    this.gridHeight = gridHeight.toString() + 'px';
  }

  onPageChange(arg: any) {
    this.pagingOption.PageNumber = arg;
    this.loadRecords();
  }

  async loadRecords(option?: AdminLTEListOption) {
    if (this.ngbpagination != undefined) {
      this.ngbpagination.selectPage(this.pagingOption.PageNumber);
    }

    if (option != undefined) this.option = option;

    if (this.option == undefined) return;

    if (this.filterColumnValue == null)
      this.filterColumnValue = this.option.columns[0].name;
    if (this.option.sql == undefined) return;

    await this.getRecordPaging(this.option.sql);

    //this.selectedValues = [];
    this.showHideMenuSelect();
  }

  async getRecordPaging(query: string) {
    var _query = query.replace(/(\r\n|\n|\r)/gm, ' ');
    var orderByString: string = '';

    if (this.option?.orderByString) {
      orderByString = this.option.orderByString;
    }

    if (this.filterValue == null) this.filterValue = '';
    if (this.filterValue.length > 0) {
      _query += _query.includes(' WHERE ') ? ' AND ' : ` WHERE `;
      _query += ` ${this.filterColumnValue} LIKE '%${this.filterValue}%' `;
    }

    this.loading = true;
    console.log(_query)
    var obj = await this.ds.execSP(
      'pGetRecordPaging',
      {
        sql: _query,
        PageNumber: this.pagingOption.PageNumber,
        DisplayCount: this.pagingOption.DisplayCount,
        OrderByString: orderByString,
      },
      {
        isReturnObject: true,
      }
    );

    this.loading = false;

    if (obj == null || obj == true || obj == undefined) {
      obj = {};
      obj.Records = [];
      obj.TotalRecord = 0;
      obj.TotalPageNumber = 0;
    }

    if (obj.Records == null || obj.Records == undefined) obj.Records = [];

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;
    this.dataSource = obj.Records;

    this.dataSource.forEach((record) => {
      record.IsSelected = false;
    });

    this.selectedValues.forEach((selected) => {
      this.dataSource.forEach((record) => {
        if (selected['value']['value'] == record[this.valueMember]) {
          record.IsSelected = true;
        }
      });
    });

    this.TotalRecordCount = obj.TotalRecord;

    this.LoadedRecordCount =
      this.dataSource.length < this.pagingOption.DisplayCount
        ? this.dataSource.length
        : this.pagingOption.DisplayCount;
    this.LoadedRecordCount =
      this.pagingOption.DisplayCount * (this.pagingOption.PageNumber - 1) +
      this.dataSource.length;

    this.onDataSourceLoaded.emit({
      pagination: this.pagingOption,
      records: this.dataSource,
      LoadedRecordCount: this.LoadedRecordCount,
    });
  }

  selectColumn_onModelChanged(event: any) {
    this.pagingOption.PageNumber = 1;
    this.loadRecords();
  }

  txtFilterValue_onKey(event: any) {
    if (event.keyCode == 13) {
      this.pagingOption.PageNumber = 1;
      this.loadRecords();
    }
  }

  menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.name == 'Refresh') {
      this.loadRecords();
    } else if (menuItem.name == 'Select') {
      this.menuItemSelect_onClick();
    }
  }

  btnSearch_OnClick() {
    this.loadRecords();
  }

  menuItemSelect_onClick() {
    var arg: AdminLTEListRowSelectedArg = {
      rows: [],
      values: [],
    };

    this.selectedValues.forEach((obj) => {
      arg.rows.push(obj.rowData);
      arg.values.push(obj.value);
    });

    this.onRowSelected.emit(arg);
  }

  row_onClick(rowData: any) {
    if (!this.option) return;
    if (this.ismultiple == true) return;

    var arg: AdminLTEListRowSelectedArg = {
      rows: [rowData],
      values: [
        {
          value: rowData[this.valueMember],
          displayValue: rowData[this.displayMember],
        },
      ],
    };

    this.onRowSelected.emit(arg);
  }

  selectCheckbox_onChange(rowData: any, event: any) {
    var obj: any = {};
    var value: any = event.value;

    if (event.checked) {
      obj[this.valueMember] = value;
      obj['rowData'] = rowData;
      obj['value'] = {
        value: rowData[this.valueMember],
        displayValue: rowData[this.displayMember],
      };

      this.selectedValues.push(obj);
    } else {
      var index = GeneralfxService.findIndexByKeyValue(
        this.selectedValues,
        this.valueMember,
        value
      );
      this.selectedValues.splice(index, 1);
    }

    this.showHideMenuSelect();
  }

  showHideMenuSelect() {
    var index = GeneralfxService.findIndexByKeyValue(
      this.menuItems,
      'name',
      'Select'
    );

    this.menuItems[index].label = `Select (${this.selectedValues.length})`;
    this.menuItems[index].visible = this.selectedValues.length > 0;
  }
}

export class AdminLTEListOption {
  sql?: string = '';
  orderByString?: string = '';
  columns: AdminLTEListColumn[] = [];
  valueMember?: string = 'ID';
  displayMember?: string = 'Name';
}
export interface AdminLTEListRowSelectedArg {
  rows: any[];
  values: any[];
}
