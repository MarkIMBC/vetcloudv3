import {
  Component,
  Input,
  OnInit,
  ViewChild,
  TemplateRef,
} from '@angular/core';
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'modal-dialog',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less'],
})
export class ModalComponent implements OnInit {
  closeResult = '';

  @ViewChild('content') content: TemplateRef<any> | undefined;
  @ViewChild('modal') modal: TemplateRef<any> | undefined;

  @Input() title: string = '';
  @Input() footerTemplate!: TemplateRef<any>;
  @Input() size: string = 'lg';

  activeInstance: any = null;
  modalRefs: NgbModalRef[] = [];
  modalRef: NgbModalRef | undefined;

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {
    this.modalService.activeInstances.subscribe((list) => {
      this.modalRef = list[list.length - 1];
    });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open<T>(): Promise<any[]> {
    var modalService = this.modalService;
    var content = this.content;

    var promise = new Promise<T[]>(async (resolve, reject) => {
      modalService.open(content, { size: this.size }).result.then(
        (result) => {
          resolve(result);
        },
        (reason) => {
          resolve(reason);
        }
      );
    });

    return promise;
  }

  close() {
    if (this.modalRef) {
      this.modalRef.close();
    }
  }

  header_loadstart(modal: any) {}
}
