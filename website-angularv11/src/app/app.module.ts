import { FormsModule } from '@angular/forms';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { PageNotFoundComponent } from './defaultpage/page-not-found/page-not-found.component';
import { DataService } from './core/data.service';
import { CrypterService } from './core/crypter.service';
import { BaseListViewComponent } from './layout/base-list-view/base-list-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BaseDetailViewComponent } from './layout/base-detail-view/base-detail-view.component';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './login/login.component';
import { LoaderInterceptor } from './core/LoaderInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseListComponent } from './layout/base-list/base-list.component';
import { LoginURLComponent } from './login-url/login-url.component';


export function appInit(dataService: DataService) {

  return () => dataService.loadConfig();
}
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    BaseListViewComponent,
    BaseDetailViewComponent,
    LoginComponent,
    BaseListComponent,
    LoginURLComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    LayoutModule,
    AppRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    DataService,
    CrypterService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
