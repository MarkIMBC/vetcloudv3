import { FilingStatusEnum } from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingInvoice_DTO } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import * as moment from 'moment';

@Component({
  selector: 'app-billing-invoice-list',
  templateUrl: './billing-invoice-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './billing-invoice-list.component.less',
  ],
})
export class BillingInvoiceListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'BillingInvoiceListComponent';

  headerTitle: string = 'Billing Invoice';

  InitCurrentObject: any = {
    DateStart_BillingInvoice: null,
    DateEnd_BillingInvoice: null,
    Code: '',
    Name_Client: '',
    Name_Patient: '',
    AttendingPhysician_Name_Employee: '',
    Name_FilingStatus: '',
    OtherReferenceNumber: '',
    Comment: '',
    Status: '',
    Name_SOAPType: '',
  };

  dataSource: BillingInvoice_DTO[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Billing Invoice',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  FilingStatus_Approved: FilingStatusEnum = FilingStatusEnum.Approved;

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
          SELECT
            *
        FROM dbo.vBillingInvoice_ListView
        /*encryptsqlend*/
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.AttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `AttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.AttendingPhysician_Name_Employee}%'`;
    }

    if (this.CurrentObject.Status.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Status LIKE '%${this.CurrentObject.Status}%'`;
    }

    if (this.CurrentObject.OtherReferenceNumber.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `OtherReferenceNumber LIKE '%${this.CurrentObject.OtherReferenceNumber}%'`;
    }

    if (this.CurrentObject.Comment.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Comment LIKE '%${this.CurrentObject.Comment}%'`;
    }

    if (this.CurrentObject.Name_SOAPType.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_SOAPType LIKE '%${this.CurrentObject.Name_SOAPType}%'`;
    }

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (this.CurrentObject['DateStart_BillingInvoice']) {
      var _dateStart = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {
        var _dateEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        if (filterString.length > 0) filterString = filterString + ' AND ';

        if (_dateStart != null && _dateEnd == null) {
          filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
            'YYYY-MM-DD'
          )}'`;
        } else {
          filterString += `
              CONVERT(DATE, Date) BETWEEN
                 '${moment(_dateStart).format('YYYY-MM-DD')}' AND
                 '${moment(_dateEnd).format('YYYY-MM-DD')}'
          `;
        }
      }
    }

    if (!hasAssignedFilter) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_FilingStatus NOT IN (4)`;
    }

    return filterString;
  }

  Row_OnClick(rowData: BillingInvoice_DTO) {
    this.customNavigate(['BillingInvoice', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['BillingInvoice', -1], {});
  }
}
