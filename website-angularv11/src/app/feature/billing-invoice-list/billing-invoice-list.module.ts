import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { BillingInvoiceListRoutingModule } from './billing-invoice-list-routing.module';
import { BillingInvoiceListComponent } from './billing-invoice-list.component';

@NgModule({
  declarations: [BillingInvoiceListComponent],
  imports: [
    CommonModule,
    SharedModule,
    BillingInvoiceListRoutingModule
  ]
})
export class BillingInvoiceListModule { }
