import { APP_MODEL, BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Enumerable } from 'linq-typescript';

@Component({
  selector: 'app-for-billing-list',
  templateUrl: './for-billing-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './for-billing-list.component.less',
  ],
})
export class ForBillingListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'ForBillingListComponent';

  headerTitle: string = 'For Billing';

  FILINGSTATUS_FORBILLING: number = FilingStatusEnum.ForBilling;
  FILINGSTATUS_PENDING: number = FilingStatusEnum.Pending;
  FILINGSTATUS_PARTIALLYPAID: number = FilingStatusEnum.PartiallyPaid;
  FILINGSTATUS_FULLYPAID: number = FilingStatusEnum.FullyPaid;

  InitCurrentObject: any = {
    RefNo: '',
    Name_Client: '',
    Name_Patient: '',
    BillingInvoice_Name_FilingStatus: '',
  };

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();
    this.OrderByString = "Date DESC"
    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
               *
            FROM dbo.vForBilling_ListView_temp
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString} AND
              1 = 1
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.RefNo.length > 0) {
      filterString += `RefNo LIKE '%${this.CurrentObject.RefNo}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.BillingInvoice_Name_FilingStatus.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `BillingInvoice_Name_FilingStatus LIKE '%${this.CurrentObject.BillingInvoice_Name_FilingStatus}%'`;
    }

    return filterString;
  }

  async dataSource_InitLoad(obj: any) {

    obj.Records.forEach((record: any) => {

      record.BillingInvoices = [];
    });

    this.loadBillingRecords_By_Patient_SOAP(obj);
    this.loadBillingRecords_By_Patient_Wellness(obj);
    this.loadBillingRecords_By_Patient_Grooming(obj);
  }

  async loadBillingRecords_By_Patient_SOAP(obj: any){

    var IDs_Patient_SOAP = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_SOAP';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  var IDs_Patient_Confinement = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_Confinement';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  await this.ds
    .execSP(
      'pGetForBillingBillingInvoiceRecords_Patient_SOAP',
      {
        IDs_Patient_SOAP: IDs_Patient_SOAP,
        IDs_Patient_Confinement: IDs_Patient_Confinement,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
    .then(async (result) => {
      obj.Records.forEach((record: any) => {

        result.BillingInvoices.forEach((refbi: any) => {
          if (
            record.Oid_Model == refbi.Oid_Model &&
            record.ID_CurrentObject == refbi.ID_CurrentObject
          ) {
            for (let i = 1; i <= 10; i++) {
              var infoBilling = refbi[i + ''];

              if (infoBilling) {
                var infos = infoBilling.split('|');

                record.BillingInvoices.push({
                  ID: infos[0],
                  Code: infos[1],
                  Status: infos[2],
                });
              }
            }
          }
        });
      });
    });
  }

  async loadBillingRecords_By_Patient_Wellness(obj: any){

    var IDs_Patient_Wellness = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_Wellness';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  var IDs_Patient_Confinement = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_Confinement';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  await this.ds
    .execSP(
      'pGetForBillingBillingInvoiceRecords_Patient_Wellness',
      {
        IDs_Patient_Wellness: IDs_Patient_Wellness,
        IDs_Patient_Confinement: IDs_Patient_Confinement,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
    .then(async (result) => {
      obj.Records.forEach((record: any) => {

        result.BillingInvoices.forEach((refbi: any) => {
          if (
            record.Oid_Model == refbi.Oid_Model &&
            record.ID_CurrentObject == refbi.ID_CurrentObject
          ) {
            for (let i = 1; i <= 10; i++) {
              var infoBilling = refbi[i + ''];

              if (infoBilling) {
                var infos = infoBilling.split('|');

                record.BillingInvoices.push({
                  ID: infos[0],
                  Code: infos[1],
                  Status: infos[2],
                });
              }
            }
          }
        });
      });
    });
  }

  async loadBillingRecords_By_Patient_Grooming(obj: any){

    var IDs_Patient_Grooming = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_Grooming';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  var IDs_Patient_Confinement = Enumerable.fromSource(obj.Records)
    .where((r: any) => {
      return r['Name_Model'] == 'Patient_Confinement';
    })
    .select((r: any) => r['ID_CurrentObject'])
    .toArray();

  await this.ds
    .execSP(
      'pGetForBillingBillingInvoiceRecords_Patient_Grooming',
      {
        IDs_Patient_Grooming: IDs_Patient_Grooming,
        IDs_Patient_Confinement: IDs_Patient_Confinement,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
    .then(async (result) => {
      obj.Records.forEach((record: any) => {

        result.BillingInvoices.forEach((refbi: any) => {
          if (
            record.Oid_Model == refbi.Oid_Model &&
            record.ID_CurrentObject == refbi.ID_CurrentObject
          ) {
            for (let i = 1; i <= 10; i++) {
              var infoBilling = refbi[i + ''];

              if (infoBilling) {
                var infos = infoBilling.split('|');

                record.BillingInvoices.push({
                  ID: infos[0],
                  Code: infos[1],
                  Status: infos[2],
                });
              }
            }
          }
        });
      });
    });
  }

  Row_OnClick(rowData: any) {
    var routerLink = '';
    var ID_CurrentObject = rowData.ID_CurrentObject;

    if (
      rowData.Oid_Model.toLowerCase() ==
      APP_MODEL.PATIENT_CONFINEMENT.toLowerCase()
    ) {
      routerLink = 'Confinement';
    } else if (
      rowData.Oid_Model.toLowerCase() == APP_MODEL.PATIENT_SOAP.toLowerCase()
    ) {
      routerLink = 'Patient_SOAP';
    }else if (
      rowData.Name_Model == 'Patient_Wellness'
    ) {
      routerLink = 'Patient_Wellness';
    }else if (
      rowData.Name_Model == 'Patient_Grooming'
    ) {
      routerLink = 'Patient_Grooming';
    }

    this.customNavigate([routerLink, ID_CurrentObject], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Patient_SOAP', -1], {});
  }

  colAction_CreateBillingInvoice_onClick(record: any) {
    var routeLink: any[] = [];

    var config: any = {};

    config['BackRouteLink'] = ['ForBillingList'];

    if (
      record.Oid_Model.toLowerCase() ==
      APP_MODEL.PATIENT_CONFINEMENT.toLowerCase()
    ) {
      config['ID_Patient_Confinement'] = record.ID_CurrentObject;

      routeLink = ['BillingInvoice', -1];
      GeneralfxService.customNavigate(this.router, this.cs, routeLink, config);
    } else if (
      record.Oid_Model.toLowerCase() == APP_MODEL.PATIENT_SOAP.toLowerCase()
    ) {
      config['ID_Patient_SOAP'] = record.ID_CurrentObject;
      routeLink = ['BillingInvoice', -1];
    }else if (
      record.Name_Model == 'Patient_Wellness'
    ) {
      config['ID_Patient_Wellness'] = record.ID_CurrentObject;
      routeLink = ['BillingInvoice', -1];
    }else if (
      record.Name_Model == 'Patient_Grooming'
    ) {
      config['ID_Patient_Grooming'] = record.ID_CurrentObject;
      routeLink = ['BillingInvoice', -1];
    }

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, config);
  }

  colAction_ViewBillingInvoice_onClick(bi: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: ['ForBillingList'],
    };

    routeLink = ['BillingInvoice', bi.ID];
    GeneralfxService.customNavigate(this.router, this.cs, routeLink, config);
  }
}
