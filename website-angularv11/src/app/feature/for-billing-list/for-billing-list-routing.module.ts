import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForBillingListComponent } from './for-billing-list.component';

const routes: Routes = [{ path: '', component: ForBillingListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForBillingListRoutingModule { }
