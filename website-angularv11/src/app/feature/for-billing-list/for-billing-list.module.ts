
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ForBillingListRoutingModule } from './for-billing-list-routing.module';
import { ForBillingListComponent } from './for-billing-list.component';

@NgModule({
  declarations: [ForBillingListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ForBillingListRoutingModule
  ]
})
export class ForBillingListModule { }
