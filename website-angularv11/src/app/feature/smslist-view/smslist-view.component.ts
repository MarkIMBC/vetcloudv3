import { Item_DTO, PropertyTypeEnum } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { APP_MODEL, Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import * as moment from 'moment';

@Component({
  selector: 'app-item-service-list',
  templateUrl: './smslist-view.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './smslist-view.component.less',
  ],
})
export class SMSListViewComponent extends BaseListViewComponent {
  CustomComponentName: string = 'SMSListViewComponent';

  headerTitle: string = 'SMS';

  InitCurrentObject: any = {
    Name_Client: '',
    Name_Patient: '',
    DateStart_Date: '',
    DateEnd_Date: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'SMS List',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM vSMSList
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name_Client.length > 0) {
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject['DateStart_Date']) {
      var _dateStart = this.CurrentObject['DateStart_Date'];
      var _dateEnd = this.CurrentObject['DateEnd_Date'];

      if (filterString.length > 0) filterString = filterString + ' AND ';

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          'YYYY-MM-DD'
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateSending) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    }

    /*Default Record*/
    if (!this.CurrentObject['DateStart_Date'] && filterString.length == 0) {
      if (filterString.length > 0) filterString = filterString + ' AND ';

      filterString += `
        CONVERT(DATE, DateSending) BETWEEN
          '${moment(_dateStart).format('YYYY-MM-DD')}' AND
          '${moment(_dateEnd).format('YYYY-MM-DD')}'
      `;
    }
    console.log(filterString);
    return filterString;
  }

  Row_OnClick(record: any) {
    var subRoute = '';

    if (record.Oid_Model == APP_MODEL.PATIENT_SOAP_PLAN.toLowerCase()) {
      subRoute = 'Patient_SOAP';
    } else if (
      record.Oid_Model == APP_MODEL.PATIENT_VACCINATION_SCHEDULE.toLowerCase()
    ) {
      subRoute = 'Patient_Vaccination';
    } else if (
      record.Oid_Model == APP_MODEL.PATIENT_WELLNESS_SCHEDULE.toLowerCase()
    ) {
      subRoute = 'Patient_Wellness';
    }

    this.customNavigate([subRoute, record.Parent_ID_Reference], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['SMSListview', -1], {});
  }
}
