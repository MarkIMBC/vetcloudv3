import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SMSListViewRoutingModule } from './smslist-view-routing.module';
import { SMSListViewComponent } from './smslist-view.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SMSListViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    SMSListViewRoutingModule
  ]
})
export class SMSListViewModule { }
