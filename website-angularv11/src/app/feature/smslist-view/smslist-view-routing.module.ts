import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SMSListViewComponent } from './smslist-view.component';

const routes: Routes = [{ path: '', component: SMSListViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SMSListViewRoutingModule { }
