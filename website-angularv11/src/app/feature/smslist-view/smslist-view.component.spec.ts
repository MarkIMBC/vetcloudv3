import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SMSListViewComponent } from './smslist-view.component';

describe('SMSListViewComponent', () => {
  let component: SMSListViewComponent;
  let fixture: ComponentFixture<SMSListViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SMSListViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SMSListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
