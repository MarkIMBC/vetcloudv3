import { ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { ItemSupplierListComponent } from './item-supplier-list/item-supplier-list.component';

@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './supplier-detail.component.less',
  ],
})
export class SupplierDetailComponent extends BaseDetailViewComponent {
  @ViewChild('itemsupplierlist') itemsupplierlist:
    | ItemSupplierListComponent
    | undefined;

  ModelName: string = 'Supplier';
  headerTitle: string = 'Supplier';

  displayMember: string = 'Name';

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {
    if (this.itemsupplierlist != undefined) {
      this.itemsupplierlist.LoadBySupplier(this.CurrentObject.ID);
    }
  }
}
