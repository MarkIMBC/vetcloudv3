import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  ItemTypeEnum,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';

@Component({
  selector: 'item-supplier-list',
  templateUrl: './item-supplier-list.component.html',
  styleUrls: [
    './../../../layout/base-list-view/base-list-view.component.less',
    './item-supplier-list.component.less',
  ],
})
export class ItemSupplierListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  InitCurrentObject: any = {
    Name_Item: '',
    ID_InventoryStatus: 0,
    Name_InventoryStatus: '',
  };
  CurrentObject: any;
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID_Supplier: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {
    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurrentObject));
  }

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItems() {
    var menuItem_SendSMSNotification = {
      label: 'Sent SMS Notification',
      icon: 'fa fa-plus',
      visible: true,
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true,
    };

    this.menuItems = [];

    if (this.CurrentObject.ID < 0) return;
    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_Refresh);
  }

  ID_InventoryStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `SELECT ID, Name FROM tInventoryStatus`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create') {
      this.doCreate();
    } else if (menuItem.label == 'Refresh') {
      this.doRefresh();
    }
  }

  doCreate() {
    if (this.IsDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      ['Patient_Wellness', -1],
      {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        AttendingPhysician_ID_Employee:
          this.CurrentObject.AttendingPhysician_ID_Employee,
        ID_Supplier: this.CurrentObject.ID,
      }
    );
  }

  doRefresh() {
    this.LoadBySupplier(this.ID_Supplier);
  }

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async LoadBySupplier(ID_Supplier: number) {
    this.menuItems = [];

    this.loadMenuItems();

    this.ID_Supplier = ID_Supplier;

    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    var sql = `SELECT *
               FROM dbo.vSupplier_Item_List
               WHERE
                ID_Supplier = ${this.ID_Supplier}
                ${filterString}

    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;

      this.loadMenuItems();
    });
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name_Item == null) this.CurrentObject.Name_Item = '';

    if (this.CurrentObject.ID_InventoryStatus == null)
      this.CurrentObject.ID_InventoryStatus = 0;

    if (this.CurrentObject.Name_Item.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Item LIKE '%${this.CurrentObject.Name_Item}%'`;
    }

    if (this.CurrentObject.ID_InventoryStatus != 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_InventoryStatus = ${this.CurrentObject.ID_InventoryStatus}`;
    }

    return filterString;
  }

  onRowClick(rowData: any) {
    if (this.IsDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      ['Patient_Wellness', rowData.ID_Patient_Wellness],
      {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        AttendingPhysician_ID_Employee:
          this.CurrentObject.AttendingPhysician_ID_Employee,
        ID_Supplier: this.CurrentObject.ID,
      }
    );
  }

  FilterField_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }
  }

  FilterField_onEnterKeyUp(e: IControlModelArg) {
    this.LoadBySupplier(this.ID_Supplier);
  }

  btnFilterRefresh_OnClick(event: any) {
    this.LoadBySupplier(this.ID_Supplier);
  }

  BtnClearFilter_OnClick(event: any) {
    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurrentObject));
    this.LoadBySupplier(this.ID_Supplier);
  }
}
