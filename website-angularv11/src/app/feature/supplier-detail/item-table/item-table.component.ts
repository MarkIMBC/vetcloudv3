import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  Item_DTO,
  Item_Supplier_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'item-table',
  templateUrl: './item-table.component.html',
  styleUrls: ['./item-table.component.less'],
})
export class ItemTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() Items: Item_Supplier_DTO[] = [];
  @Input() ID_Supplier: number = 0;

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  loadByID(ID_Item: number): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }


  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {
      this.doAddItem();
    }
  }

  async BtnDeleteItem_OnClick(Item_Item: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.Items,
      'ID',
      Item_Item.ID + ''
    );

    this.Items.splice(index, 1);
  }

  async doAddItem() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT *
            FROM dbo.vSupplier_Item_List
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
      };

      if (this.Items == null) this.Items = [];
      this.Items.push(item);
    });
  }

  Item_onModelChanged(Item_Item: any, e: IControlModelArg) {
    Item_Item[e.name] = e.value;
  }
}
