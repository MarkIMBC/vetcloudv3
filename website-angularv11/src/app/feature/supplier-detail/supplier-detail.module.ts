import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { SupplierDetailRoutingModule } from './supplier-detail-routing.module';
import { SupplierDetailComponent } from './supplier-detail.component';
import { ItemTableComponent } from './item-table/item-table.component';
import { ItemSupplierListComponent } from './item-supplier-list/item-supplier-list.component';

@NgModule({
  declarations: [SupplierDetailComponent, ItemTableComponent, ItemSupplierListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    SupplierDetailRoutingModule
  ]
})
export class SupplierDetailModule { }
