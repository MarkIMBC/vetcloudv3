import { FormsModule } from '@angular/forms';
import { ModalModule } from './../../shared/modal/modal.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClientPatientRecordsRoutingModule } from './client-patient-records-routing.module';
import { ClientPatientRecordsComponent } from './client-patient-records.component';
import { MedicalRecordListComponent } from './medical-record-list/medical-record-list.component';
import { BillingInvoiceListComponent } from './billing-invoice-list/billing-invoice-list.component';
import { ClientDetailDialogComponent } from './client-detail-dialog/client-detail-dialog.component';
import { VaccinationListComponent } from './vaccination-list/vaccination-list.component';
import { ConfinementListComponent } from './confinement-list/confinement-list.component';
import { ClientFormComponent } from './client-form/client-form.component';
@NgModule({
  declarations: [
      ClientPatientRecordsComponent,
      MedicalRecordListComponent,
      BillingInvoiceListComponent,
      ClientDetailDialogComponent,
      VaccinationListComponent,
      ConfinementListComponent,
      ClientFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ClientPatientRecordsRoutingModule
  ]
})
export class ClientPatientRecordsModule { }
