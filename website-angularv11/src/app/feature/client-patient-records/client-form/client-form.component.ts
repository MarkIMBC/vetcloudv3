import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../../../layout/base-detail-view/base-detail-view.component';
import { PatientDetailComponent } from '../../patient-detail/patient-detail.component';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: [
    './../../../layout/base-detail-view/base-detail-view.component.less',
    './client-form.component.less',
  ],
})
export class ClientFormComponent extends BaseDetailViewComponent {
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  ModelName: string = 'Client';
  headerTitle: string = 'Client';
  displayMember: string = 'Name';

  PatientName = '';

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  rightDropDown_onMainButtonClick() {}

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    this.menuItems.push({
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    });
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      this.onCancel.emit({});
    }
  }

  async _menuItem_New_onClick() {
    this.LoadCurrentObjectRecord(-1);
  }

  DetailView_onLoad() {
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;
    }

    if (!this.CurrentObject.Name_Patient) this.CurrentObject.Name_Patient = '';
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    this.CurrentObject.Name = this.CurrentObject.Name.trimStart();
    this.CurrentObject.Name = this.CurrentObject.Name.trimEnd();

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    if (this.PatientName.length == 0) {
      validations.push({
        message: `Patient is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {}

  CurrentObject_onBeforeSaving() {
    this.PatientName = this.CurrentObject.Name_Patient;
  }

  public async CurrentObject_onAfterSaving() {
    this.loading = true;

    await this.SavingPatient();

    this.loading = false;
  }

  async SavingPatient() {
    var patientComponent: PatientDetailComponent = new PatientDetailComponent(
      this.router,
      this.route,
      this.elRef,
      this._dataServices,
      this.toastService,
      this.userAuth,
      this.cs,
      this.validationService,
      this.activeRoute,
      this.location
    );

    await patientComponent.LoadCurrentObjectRecord(-1);
    patientComponent.CurrentObject.Name = this.PatientName;
    patientComponent.CurrentObject.ID_Client = this.CurrentObject.ID;

    await patientComponent.save();
    await patientComponent.loadRecord();

    this.CurrentObject.ID_Patient = patientComponent.CurrentObject.ID;
    this.CurrentObject.Name_Patient = patientComponent.CurrentObject.Name;
  }
}
