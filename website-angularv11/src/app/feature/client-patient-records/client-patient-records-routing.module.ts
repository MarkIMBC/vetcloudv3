import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientPatientRecordsComponent } from './client-patient-records.component';

const routes: Routes = [{ path: '', component: ClientPatientRecordsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientPatientRecordsRoutingModule { }
