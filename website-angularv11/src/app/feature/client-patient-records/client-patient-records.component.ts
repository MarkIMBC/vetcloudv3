import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ItemTypeEnum, FilingStatusEnum, PaymentMethodEnum, FilterCriteriaType, IFormValidation, AlignEnum, IDetailViewAfterSavedArg } from './../../../shared/APP_HELPER';
import { SQLListDialogComponent } from './../../shared/control/sql-list-dialog/sql-list-dialog.component';
import { CurrentObjectOnValueChangeArg } from './../../layout/base-detail-view/base-detail-view.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxComponent, AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum, TaxSchemeEnum, BillingInvoice_Detail_DTO } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { MedicalRecordListComponent } from './medical-record-list/medical-record-list.component';
import { BillingInvoiceListComponent } from './billing-invoice-list/billing-invoice-list.component';
import { AdminLTEListRowSelectedArg } from 'src/app/shared/control/admin-lte-list/admin-lte-list.component';
import { VaccinationListComponent } from './vaccination-list/vaccination-list.component';
import { ConfinementListComponent } from './confinement-list/confinement-list.component';
import { ClientFormComponent } from './client-form/client-form.component';

@Component({
  selector: 'app-client-patient-records',
  templateUrl: './client-patient-records.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './client-patient-records.component.less'
  ]
})
export class ClientPatientRecordsComponent extends BaseDetailViewComponent {

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox: AdminLTEDataLookupboxComponent | undefined
  @ViewChild('MedicalRecordList') MedicalRecordList: MedicalRecordListComponent | undefined;
  @ViewChild('BillingInvoiceList') BillingInvoiceList: BillingInvoiceListComponent | undefined;
  @ViewChild('VaccinationList') VaccinationList: VaccinationListComponent | undefined;
  @ViewChild('ConfinementList') ConfinementList: ConfinementListComponent | undefined;
  @ViewChild('clientform') clientform: ClientFormComponent | undefined;

  ModelName: string = 'Patient'
  headerTitle: string = 'Client Patient'
  IsShowClientForm: boolean = false;

  PatientSOAPCount: number = 0;
  BillingInvoiceCount: number = 0;
  VaccinationCount: number = 0;
  ConfinementCount: number = 0;

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  loadMenuItem() {

    this.menuItems = [];

    this.menuItems.push({
      label: 'New Client',
      icon: 'fas fa-plus',
      class: 'text-default',
      visible: true
    })

    this.menuItems.push({
      label: 'New Patient',
      icon: 'fas fa-plus',
      class: 'text-default',
      visible: true
    })
  }

  DetailView_onLoad() {

    var jsonString = window.localStorage.getItem("ClientPatientRecords_FilterCurrentObject");
    if (jsonString) {
      this.CurrentObject = JSON.parse(jsonString);
    }

    this.loadMenuItem();
    this.loadAllListView();

    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.clientform) {

        this.clientform.LoadCurrentObjectRecord(this.CurrentObject.ID_Client);
      }
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (e.name == 'ID_Client') {

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();

      if (this.clientform) {

        this.clientform.LoadCurrentObjectRecord(this.CurrentObject.ID_Client);
      }
    }

    this.loadAllListView();

    this.setFilterCurrentObjectLocalStorage()
  }

  setFilterCurrentObjectLocalStorage() {

    var filterJSONObject = JSON.stringify(this.CurrentObject);
    window.localStorage.setItem("ClientPatientRecords_FilterCurrentObject", filterJSONObject);
  }

  loadAllListView() {

    if (this.MedicalRecordList != undefined) {
      this.MedicalRecordList.load(this.CurrentObject.ID_Client, this.CurrentObject.ID_Patient);
    }

    if (this.BillingInvoiceList != undefined) {
      this.BillingInvoiceList.load(this.CurrentObject.ID_Client, this.CurrentObject.ID_Patient);
    }

    if (this.VaccinationList != undefined) {
      this.VaccinationList.load(this.CurrentObject.ID_Client, this.CurrentObject.ID_Patient);
    }

    if (this.ConfinementList != undefined) {
      this.ConfinementList.load(this.CurrentObject.ID_Client, this.CurrentObject.ID_Patient);
    }
  }

  MedicalRecordList_onDataSourceLoaded(e: any) {

    this.PatientSOAPCount = e.pagination.TotalRecord;
  }

  ConfinementList_onDataSourceLoaded(e: any) {

    this.ConfinementCount = e.pagination.TotalRecord;
  }

  BillingInvoiceList_onDataSourceLoaded(e: any) {

    this.BillingInvoiceCount = e.pagination.TotalRecord;
  }

  VaccinationList_onDataSourceLoaded(e: any) {

    this.VaccinationCount = e.pagination.TotalRecord;
  }

  menu_OnClick(e: any) {

    if (e.item.label == 'New Client') {

      if (this.clientform) {

        this.IsShowClientForm = true;
        this.clientform.LoadCurrentObjectRecord(-1);
      }
    } else if (e.item.label == 'New Patient') {

      if (this.clientform) {

        this.IsShowClientForm = true;
        this.clientform.LoadCurrentObjectRecord(this.CurrentObject.ID_Client);
      }
    }
  }

  clientform_onAfterSaved(e: IDetailViewAfterSavedArg) {

    this.CurrentObject.ID_Client = e.CurrentObject.ID;
    this.CurrentObject.Name_Client = e.CurrentObject.Name;

    this.CurrentObject.ID_Patient = e.CurrentObject.ID_Patient;
    this.CurrentObject.Name_Patient = e.CurrentObject.Name_Patient;
    this.IsShowClientForm = false

    this.setFilterCurrentObjectLocalStorage();
    this.loadAllListView();
  }

  clientform_onCancel(e: any) {

    this.IsShowClientForm = false
  }
}
