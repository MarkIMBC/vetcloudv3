import { Component } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { BaseListComponent } from 'src/app/layout/base-list/base-list.component';
import { AlignEnum, FilingStatusEnum, Patient_SOAP_DTO, PropertyTypeEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'vaccination-list',
  templateUrl: './vaccination-list.component.html',

  styleUrls: [
    './../../../layout/base-list-view/base-list-view.component.less',
    './vaccination-list.component.less'
  ],
})
export class VaccinationListComponent extends BaseListComponent {

  private _ID_Client: number = 0;
  private _ID_Patient: number = 0;

  load(ID_Client: number, ID_Patient: number) {

    this._ID_Client = ID_Client;
    this._ID_Patient = ID_Patient;

    var filterString = `ID_FilingStatus NOT IN (${FilingStatusEnum.Cancelled}) `;

    if (ID_Client) {

      if (filterString.length > 0) filterString += " AND "
      filterString += `ID_Client = ${ID_Client}`;
    }

    if (ID_Patient > 0) {

      if (filterString.length > 0) filterString += " AND "
      filterString += `ID_Patient = ${ID_Patient}`;
    }

    if (this.currentUser.ID_Company) {

      if (filterString.length > 0) filterString += " AND "
      filterString += `ID_Company = ${this.currentUser.ID_Company}`;
    }

    if (this.listview) {

      this.listview.loadRecords({
        sql: `/*encryptsqlstart*/
              SELECT *
              FROM dbo.vPatient_Vaccination
              WHERE
                ${filterString}
              /*encryptsqlend*/
        `,
        orderByString: 'Date DESC',
        columns: [
          {
            name: 'Date',
            caption: 'Date',
            propertyType: PropertyTypeEnum.Date,
            align: AlignEnum.center,
            format: 'MM/dd/Y',
            style: {
              'width': '100px'
            }
          },
          {
            name: 'Code',
            caption: 'Code',
            align: AlignEnum.center,
            propertyType: PropertyTypeEnum.String,
            style: {
              'width': '100px'
            }
          },
          {
            name: 'Name_Client',
            caption: 'Client',
            propertyType: PropertyTypeEnum.String
          },
          {
            name: 'Name_Patient',
            caption: 'Patient',
            propertyType: PropertyTypeEnum.String
          },
          {
            name: 'Name_FilingStatus',
            caption: 'Status',
            align: AlignEnum.center,
            propertyType: PropertyTypeEnum.String,
            style: {
              'width': '100px'
            }
          }
        ]
      });
    }
  }

  refreshRecord() {

    this.load(this._ID_Client, this._ID_Patient)
  }

  menuItem_New_onClick() {

    GeneralfxService.customNavigate(this.router, this.cs, ['Patient_Vaccination', -1], {
      'ID_Client': this._ID_Client,
      'ID_Patient': this._ID_Patient
    });
  }

  Row_OnClick(rows: any[]) {

    if (rows.length != 1) return;

    var rowData = rows[0];

    GeneralfxService.customNavigate(this.router, this.cs, ['Patient_Vaccination', rowData.ID], {
      'ID_Client': this._ID_Client,
      'ID_Patient': this._ID_Patient
    });
  }
}
