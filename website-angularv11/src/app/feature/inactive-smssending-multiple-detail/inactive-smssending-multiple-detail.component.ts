import { ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-inactive-smssending-multiple-detail',
  templateUrl: './inactive-smssending-multiple-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './inactive-smssending-multiple-detail.component.less',
  ],
})
export class InactiveSMSSendingMultipleDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'Company';
  headerTitle: string = 'Inactive SMS Sending';

  routerFeatureName: string = 'InactiveSMSSendingMultiple';
  displayMember: string = 'Name';

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {
    if (
      this.CurrentObject['InactiveSMSSending'] == undefined ||
      this.CurrentObject['InactiveSMSSending'] == null ||
      this.CurrentObject['InactiveSMSSending'] == ''
    ) {
      this.CurrentObject['InactiveSMSSending'] = [];
    }
  }
}
