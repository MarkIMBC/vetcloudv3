import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveSMSSendingMultipleDetailComponent } from './inactive-smssending-multiple-detail.component';

describe('InactiveSMSSendingMultipleDetailComponent', () => {
  let component: InactiveSMSSendingMultipleDetailComponent;
  let fixture: ComponentFixture<InactiveSMSSendingMultipleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveSMSSendingMultipleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InactiveSMSSendingMultipleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
