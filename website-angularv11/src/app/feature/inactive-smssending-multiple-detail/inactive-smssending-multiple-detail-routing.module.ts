import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InactiveSMSSendingMultipleDetailComponent } from './inactive-smssending-multiple-detail.component';

const routes: Routes = [{ path: '', component: InactiveSMSSendingMultipleDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InactiveSMSSendingMultipleDetailRoutingModule { }
