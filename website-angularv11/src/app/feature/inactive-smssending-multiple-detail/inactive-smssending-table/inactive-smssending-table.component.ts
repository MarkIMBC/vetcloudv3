import { InactiveSMSSending } from './../../../../shared/APP_MODELS';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Patient_Grooming_Detail } from 'src/shared/APP_MODELS';

@Component({
  selector: 'inactive-smssending-table',
  templateUrl: './inactive-smssending-table.component.html',
  styleUrls: ['./inactive-smssending-table.component.less'],
})
export class InactiveSMSSendingTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() Details: InactiveSMSSending[] = [];

  tempID: number = 0;

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Detail',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Detail') {
      this.tempID--;

      var item = {
        ID: this.tempID,
      };

      if (this.Details == null) this.Details = [];
      this.Details.push(item);
    }
  }

  async BtnDeleteDetail_OnClick(InactiveSMSSendingTable: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.Details,
      'ID',
      InactiveSMSSendingTable.ID + ''
    );

    this.Details.splice(index, 1);
  }

  detail_onModelChanged(InactiveSMSSendingTable: any, e: IControlModelArg) {
    InactiveSMSSendingTable[e.name] = e.value;
  }
}
