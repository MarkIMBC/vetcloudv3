import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveSMSSendingTableComponent } from './inactive-smssending-table.component';

describe('InactiveSMSSendingTableComponent', () => {
  let component: InactiveSMSSendingTableComponent;
  let fixture: ComponentFixture<InactiveSMSSendingTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveSMSSendingTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InactiveSMSSendingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
