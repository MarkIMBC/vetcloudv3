import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InactiveSMSSendingMultipleDetailRoutingModule } from './inactive-smssending-multiple-detail-routing.module';
import { InactiveSMSSendingMultipleDetailComponent } from './inactive-smssending-multiple-detail.component';
import { InactiveSMSSendingTableComponent } from './inactive-smssending-table/inactive-smssending-table.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    InactiveSMSSendingMultipleDetailComponent,
    InactiveSMSSendingTableComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    InactiveSMSSendingMultipleDetailRoutingModule,
  ],
})
export class InactiveSMSSendingMultipleDetailModule {}
