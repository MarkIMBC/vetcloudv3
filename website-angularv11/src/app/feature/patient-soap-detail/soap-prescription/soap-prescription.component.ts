import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  Patient_SOAP_Prescription_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'soap-prescription',
  templateUrl: './soap-prescription.component.html',
  styleUrls: ['./soap-prescription.component.less'],
})
export class SOAPPrescriptionComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() SOAPPrescriptions: Patient_SOAP_Prescription_DTO[] = [];
  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {
      this.doAddItemInventoriable();
    }
  }

  async BtnDeleteSOAPPresciption_OnClick(SOAPPresciption: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.SOAPPrescriptions,
      'ID',
      SOAPPresciption.ID + ''
    );

    this.SOAPPrescriptions.splice(index, 1);

    this.onChanged.emit();
  }

  async doAddItemInventoriable() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              FormattedCurrentInventoryCount,
              RemainingBeforeExpired
            FROM dbo.vItemInventoriableForBillingLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Medication',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'RemainingBeforeExpired',
          caption: 'Rem. Before Expire',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.center,
        },
        {
          name: 'FormattedCurrentInventoryCount',
          caption: 'Available Count',
          propertyType: PropertyTypeEnum.Int,
          align: AlignEnum.center,
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        IsCharged: true,
      };

      if (this.SOAPPrescriptions == null) this.SOAPPrescriptions = [];
      this.SOAPPrescriptions.push(item);
    });

    console.log('SOAPPrescriptions', this.SOAPPrescriptions);

    this.onChanged.emit();
  }

  SOAPPresciption_onModelChanged(SOAPPresciption: any, e: IControlModelArg) {
    SOAPPresciption[e.name] = e.value;

    this.onChanged.emit();
  }
}
