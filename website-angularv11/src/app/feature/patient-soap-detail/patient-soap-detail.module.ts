import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientSOAPDetailRoutingModule } from './patient-soap-detail-routing.module';
import { PatientSOAPDetailComponent } from './patient-soap-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SOAPPlanComponent } from './soap-plan/soap-plan.component';
import { LaboratoryImagesComponent } from './laboratory-images/laboratory-images.component';
import { SOAPPrescriptionComponent } from './soap-prescription/soap-prescription.component';
import { PatientVaccinationListComponent } from './patient-vaccination-list/patient-vaccination-list.component';
import { PatientTreatmentComponent } from './patient-treatment/patient-treatment.component';
import { PatientWellnessListComponent } from './patient-wellness-list/patient-wellness-list.component';

@NgModule({
  declarations: [
    PatientSOAPDetailComponent,
    SOAPPlanComponent,
    LaboratoryImagesComponent,
    SOAPPrescriptionComponent,
    PatientVaccinationListComponent,
    PatientTreatmentComponent,
    PatientWellnessListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientSOAPDetailRoutingModule
  ]
})
export class PatientSoapDetailModule { }
