import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'soap-plan',
  templateUrl: './soap-plan.component.html',
  styleUrls: ['./soap-plan.component.less'],
})
export class SOAPPlanComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() soapPlans: Patient_SOAP_Plan_DTO[] = [];
  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Service') {
      this.doAddService();
    }
  }

  async BtnDeleteSOAPPlan_OnClick(soapPlan: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.soapPlans,
      'ID',
      soapPlan.ID + ''
    );

    this.soapPlans.splice(index, 1);
    this.onChanged.emit();
  }

  async doAddService() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vActiveItemService
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;
    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
      };

      if (this.soapPlans == null) this.soapPlans = [];
      this.soapPlans.push(item);
    });

    this.onChanged.emit();
  }

  soapPlan_onModelChanged(soapPlan: any, e: IControlModelArg) {
    soapPlan[e.name] = e.value;
    this.onChanged.emit();

    soapPlan.IsSentSMS = false;
  }
}
