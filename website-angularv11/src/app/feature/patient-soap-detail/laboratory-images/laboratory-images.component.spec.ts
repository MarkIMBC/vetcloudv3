import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaboratoryImagesComponent } from './laboratory-images.component';

describe('LaboratoryImagesComponent', () => {
  let component: LaboratoryImagesComponent;
  let fixture: ComponentFixture<LaboratoryImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaboratoryImagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaboratoryImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
