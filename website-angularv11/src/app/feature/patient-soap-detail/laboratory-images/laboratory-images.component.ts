import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEImageBoxComponent } from 'src/app/shared/control/admin-lte-image-box/admin-lte-image-box.component';
import { IControlModelArg, IFile, LabImage } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'laboratory-images',
  templateUrl: './laboratory-images.component.html',
  styleUrls: ['./laboratory-images.component.less'],
})
export class LaboratoryImagesComponent implements OnInit {
  @ViewChildren(AdminLTEImageBoxComponent)
  adminLTEImages!: QueryList<AdminLTEImageBoxComponent>;

  @Output() onModelChanged: EventEmitter<IControlModelArg> = new EventEmitter();
  @Input() CurrentObject: any;
  currentUser: TokenSessionFields = new TokenSessionFields();

  _LabImageRowIndex: number = 0;

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Image') {
      this.doAddLaboratoryImage();
    }
  }

  control_onModelChanged(e: IControlModelArg) {
    this.onModelChanged.emit(e);
  }

  doAddLaboratoryImage() {
    var i = 1;
    var isNotFind = false;

    if (this.CurrentObject.LabImages == null) this.CurrentObject.LabImages;

    if (this.CurrentObject.LabImages.length > 0) {
      this._LabImageRowIndex = this.CurrentObject.LabImages[0].ImageRowIndex;
    }

    this._LabImageRowIndex++;

    if (this.CurrentObject.LabImages.length == 0) {
      this.CurrentObject.LabImages.unshift({
        RowIndex: this._LabImageRowIndex,
        ImageNo: '01',
      });

      this.CurrentObject['LabImageRowIndex01'] = this._LabImageRowIndex;
    } else {
      for (i = 1; i <= 15; i++) {
        if (isNotFind == false) {
          var _imgNo = i.toString();
          if (_imgNo.length == 1) _imgNo = '0' + _imgNo;

          var index = GeneralfxService.findIndexByKeyValue(
            this.CurrentObject.LabImages,
            'ImageNo',
            _imgNo
          );

          if (index < 0) {
            var obj = {
              RowIndex: this._LabImageRowIndex,
              ImageNo: _imgNo,
            };

            this.CurrentObject['LabImageRowIndex' + _imgNo] = obj.RowIndex;
            this.CurrentObject.LabImages.unshift(obj);

            isNotFind = true;
          }
        }
      }
    }
  }

  btnfillTemplate_OnClick(labImage: LabImage) {
    var name = 'LabImageRemark' + labImage.ImageNo;
    var value = this.CurrentObject.LaboratoryTemplate;

    this.CurrentObject[name] = value;

    var arg: CurrentObjectOnValueChangeArg = {
      name: name,
      value: value,
      data: [],
    };

    this.onModelChanged.emit(arg);
  }

  btnLabImageRemove(labImage: LabImage) {
    var labImageColumns: string[] = [
      'LabImageFilePath',
      'LabImageRemark',
      'LabImageRowIndex',
    ];

    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject.LabImages,
      'ImageNo',
      labImage.ImageNo
    );
    this.CurrentObject.LabImages.splice(index, 1);

    labImageColumns.forEach((labImageColumn: string) => {
      var name = labImageColumn + labImage.ImageNo;
      var value = labImageColumn == 'LabImageRowIndex' ? null : '';

      this.onModelChanged.emit({
        name: name,
        value: value,
      });
    });
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Image',
      icon: 'fa fa-plus',
      visible: true,
    },
  ];

  getFiles(): IFile[] {
    var currentFiles: IFile[] = [];

    this.adminLTEImages.forEach((i: any) => {
      if (i['file']) {
        currentFiles.push({
          file: i['file'],
          dataField: i.name,
          isImage: true,
        });
      }
    });

    return currentFiles;
  }
}
