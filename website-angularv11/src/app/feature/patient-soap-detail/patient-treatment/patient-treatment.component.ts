import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  Patient_SOAP_Treatment_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'patient-treatment',
  templateUrl: './patient-treatment.component.html',
  styleUrls: ['./patient-treatment.component.less'],
})
export class PatientTreatmentComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;
  @Output() onChanged = new EventEmitter<any>();

  @Input() SOAPTreatments: Patient_SOAP_Treatment_DTO[] = [];

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Medication',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
    {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Medication') {
      this.doAddItemInventoriable();
    } else if (menuItem.label == 'Add Service') {
      this.doAddItemService();
    }
  }

  async BtnDeleteSOAPTreatment_OnClick(SOAPTreatment: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.SOAPTreatments,
      'ID',
      SOAPTreatment.ID + ''
    );

    this.SOAPTreatments.splice(index, 1);
    this.onChanged.emit();
  }

  async doAddItemInventoriable() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              FormattedCurrentInventoryCount,
              RemainingBeforeExpired
            FROM dbo.vItemInventoriableForBillingLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Medication',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'RemainingBeforeExpired',
          caption: 'Rem. Before Expire',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.center,
        },
        {
          name: 'FormattedCurrentInventoryCount',
          caption: 'Available Count',
          propertyType: PropertyTypeEnum.Int,
          align: AlignEnum.center,
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
      };

      if (this.SOAPTreatments == null) this.SOAPTreatments = [];
      this.SOAPTreatments.push(item);
    });
    this.onChanged.emit();
  }

  async doAddItemService() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vItemService
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Service',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;
    _.tempID--;

    obj.rows.forEach((record: any) => {
      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
      };

      if (this.SOAPTreatments == null) this.SOAPTreatments = [];
      this.SOAPTreatments.push(item);
    });
    this.onChanged.emit();
  }

  SOAPTreatment_onModelChanged(SOAPTreatment: any, e: IControlModelArg) {
    SOAPTreatment[e.name] = e.value;
    this.onChanged.emit();
  }
}
