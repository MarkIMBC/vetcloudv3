import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReceivingReportDetailRoutingModule } from './receiving-report-detail-routing.module';
import { ReceivingReportDetailComponent } from './receiving-report-detail.component';
import { ReceivingReportDetailTableComponent } from './receiving-report-detail-table/receiving-report-detail-table.component';

@NgModule({
  declarations: [ReceivingReportDetailComponent, ReceivingReportDetailTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ReceivingReportDetailRoutingModule
  ]
})
export class ReceivingReportDetailModule { }
