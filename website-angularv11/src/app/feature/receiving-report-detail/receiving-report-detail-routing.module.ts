import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceivingReportDetailComponent } from './receiving-report-detail.component';

const routes: Routes = [{ path: '', component: ReceivingReportDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceivingReportDetailRoutingModule { }
