import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  FilingStatusEnum,
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  Patient_SOAP_Prescription_DTO,
  PropertyTypeEnum,
  ReceivingReport_DTO,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Enumerable } from 'linq-typescript';

@Component({
  selector: 'receiving-report-detail-table',
  templateUrl: './receiving-report-detail-table.component.html',
  styleUrls: [
    './../../../layout/base-detail-view/base-detail-view.component.less',
    './receiving-report-detail-table.component.less',
  ],
})
export class ReceivingReportDetailTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() CurrentObject: ReceivingReport_DTO = {};
  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-plus',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {
      this.doAddPurchaseOrderDetail();
    }
  }

  async BtnDeleteReceivingReportDetail_OnClick(detail: any) {
    if (!this.CurrentObject.ReceivingReport_Detail) return;

    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject.ReceivingReport_Detail,
      'ID',
      detail.ID + ''
    );

    this.CurrentObject.ReceivingReport_Detail.splice(index, 1);

    this.onChanged.emit();
  }

  async doAddPurchaseOrderDetail() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;

    var strQuery_IDs_PurchaseOrder_Detail = '';

    if (this.CurrentObject.ReceivingReport_Detail) {
      strQuery_IDs_PurchaseOrder_Detail = Enumerable.fromSource(
        this.CurrentObject.ReceivingReport_Detail
      )
        .select((r) => r.ID_PurchaseOrder_Detail)
        .distinct()
        .toArray()
        .toString();
    }

    if (strQuery_IDs_PurchaseOrder_Detail.length > 0)
      strQuery_IDs_PurchaseOrder_Detail = `AND ID NOT IN (${strQuery_IDs_PurchaseOrder_Detail})`;

    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT ID,
                  Code_Item,
                  Name_Item,
                  ID_Item,
                  Quantity,
                  RemainingQuantity,
                  ServingStatus_Name_FilingStatus,
                  UnitPrice
           FROM vRemainingQuantityPurchaseOrderDetail
           WHERE
             ID_Company = ${this.currentUser.ID_Company} AND
             ID_PurchaseOrder = ${this.CurrentObject.ID_PurchaseOrder} AND
             ID_FilingStatus = ${FilingStatusEnum.Approved} AND
             ISNULL(RemainingQuantity, 0) > 0
             ${strQuery_IDs_PurchaseOrder_Detail}
            `,
      columns: [
        {
          name: 'Name_Item',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code_Item',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Quantity',
          caption: 'Quantity',
          align: AlignEnum.center,
          propertyType: PropertyTypeEnum.Int,
        },
        {
          name: 'RemainingQuantity',
          caption: 'Rem. Quantity',
          align: AlignEnum.center,
          propertyType: PropertyTypeEnum.Int,
        },
        {
          name: 'ServingStatus_Name_FilingStatus',
          caption: 'Serve Status',
          align: AlignEnum.center,
          propertyType: PropertyTypeEnum.String,
        },
      ],
    });

    var _ = this;
    if (!this.CurrentObject.ReceivingReport_Detail)
      this.CurrentObject.ReceivingReport_Detail = [];

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID_Item,
        Name_Item: record.Name_Item,
        ID_PurchaseOrder_Detail: record.ID,
        Quantity: record.RemainingQuantity,
        UnitPrice: record.UnitPrice,
      };

      if (this.CurrentObject.ReceivingReport_Detail == null)
        this.CurrentObject.ReceivingReport_Detail = [];
      this.CurrentObject.ReceivingReport_Detail.push(item);
    });

    this.onChanged.emit();
  }

  ReceivingReportDetail_onModelChanged(
    ReceivingReportDetail: any,
    e: IControlModelArg
  ) {
    ReceivingReportDetail[e.name] = e.value;

    this.onChanged.emit();
  }

  ReceivingReport_onModelChanged(fieldName: string, e: any) {
    if (fieldName == 'DiscountRate') {
      this.CurrentObject.IsComputeDiscountRate = true;
    } else if (fieldName == 'DiscountAmount') {
      this.CurrentObject.IsComputeDiscountRate = false;
    }

    this.onChanged.emit();
  }
}
