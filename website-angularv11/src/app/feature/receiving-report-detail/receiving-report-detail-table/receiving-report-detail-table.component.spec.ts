import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivingReportDetailTableComponent } from './receiving-report-detail-table.component';

describe('ReceivingReportDetailTableComponent', () => {
  let component: ReceivingReportDetailTableComponent;
  let fixture: ComponentFixture<ReceivingReportDetailTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceivingReportDetailTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivingReportDetailTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
