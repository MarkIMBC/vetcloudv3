import { ReceivingReport_Detail } from './../../../shared/APP_MODELS';
import {
  AlignEnum,
  FilingStatusEnum,
  FilterCriteriaType,
  IFormValidation,
  ItemTypeEnum,
  TaxSchemeEnum,
} from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-receiving-report-detail',
  templateUrl: './receiving-report-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './receiving-report-detail.component.less',
  ],
})
export class ReceivingReportDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'ReceivingReport';
  headerTitle: string = 'Receiving Report';

  displayMember: string = 'Code';

  ID_PurchaseOrder_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: ``,
      columns: [
        {
          name: 'Name_Supplier',
          caption: 'Supplier',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'PO #',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'Name_FilingStatus',
          caption: 'Status',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'ServingStatus_Name_FilingStatus',
          caption: 'Serving Status',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
      ],
    },
  };

  DetailView_onLoad() {
    if (this.CurrentObject.ReceivingReport_Detail == null) {
      this.CurrentObject.ReceivingReport_Detail = [];
    }

    var listViewOptionID_PurchaseOrder =
      this.ID_PurchaseOrder_LookupboxOption.listviewOption;

    if (listViewOptionID_PurchaseOrder != undefined) {
      listViewOptionID_PurchaseOrder.sql = `/*encryptsqlstart*/
                                            Select ID,
                                                  Code,
                                                  ID_Supplier,
                                                  Name_Supplier,
                                                  Name_FilingStatus,
                                                  ServingStatus_Name_FilingStatus
                                            FROM vPurchaseOrder
                                            WHERE
                                              ID_FilingStatus = ${FilingStatusEnum.Approved} AND
                                              ID_Company = ${this.currentUser.ID_Company} AND
                                              ServingStatus_ID_FilingStatus IN (
                                                                                  ${FilingStatusEnum.Pending},
                                                                                  ${FilingStatusEnum.PartiallyServed}
                                                                              )
                                     /*encryptsqlend*/`;
    }

    console.log(this.CurrentObject);
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuItemApprove = {
      label: 'Approve',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    };

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'RR Report',
          name: 'rrreport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
      ],
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID < 1) return;

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(menuItemApprove);
        this.addMenuItem(menuItemCancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        this.addMenuItem(menuItemCancel);
      }

      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Approve') {
      this.doApprove();
    } else if (e.item.label == 'Cancel') {
      this.doCancel();
    } else if (e.item.name == 'rrreport') {
      this.customNavigate(['Report'], {
        ReportName: 'ReceivingReport',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }

  async doApprove() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pApproveReceivingReport',
        {
          IDs_ReceivingReport: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been approved successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to approve ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before canceling ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pCancelReceivingReport',
        {
          IDs_ReceivingReport: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Supplier = this.CurrentObject.ID_Supplier;
    var ID_PurchaseOrder = this.CurrentObject.ID_PurchaseOrder;

    if (
      this.CurrentObject.ID_Supplier == undefined ||
      this.CurrentObject.ID_Supplier == null
    )
      ID_Supplier = 0;

    if (
      this.CurrentObject.ID_PurchaseOrder == undefined ||
      this.CurrentObject.ID_PurchaseOrder == null
    )
      ID_PurchaseOrder = 0;

    if (ID_PurchaseOrder == 0) {
      validations.push({
        message: 'PO # is required.',
      });
    }

    if (ID_Supplier == 0) {
      validations.push({
        message: 'Supplier is required.',
      });
    }

    if (!this.CurrentObject.Date) {
      validations.push({
        message: 'Date is required.',
      });
    }

    if (this.CurrentObject.ReceivingReport_Detail.length == 0) {
      validations.push({
        message: 'Add purchase Item atleast one.',
      });
    }

    return Promise.resolve(validations);
  }

  compute() {
    var subTotal = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;

    discountRate = GeneralfxService.roundOffDecimal(discountRate);
    discountAmount = GeneralfxService.roundOffDecimal(discountAmount);

    this.CurrentObject.ReceivingReport_Detail.forEach(
      (detail: {
        Amount: number;
        Quantity: number;
        UnitPrice: number;
        ID_ReceivingReport: any;
      }) => {
        detail.Amount = detail.Quantity * detail.UnitPrice;
        detail.ID_ReceivingReport = this.CurrentObject.ID;

        subTotal += detail.Amount;
      }
    );

    totalAmount = GeneralfxService.roundOffDecimal(subTotal);

    if (this.CurrentObject.IsComputeDiscountRate) {
      discountAmount = totalAmount * (discountRate / 100);
    } else {
      if (discountAmount > 0) {
        discountRate = (discountAmount / totalAmount) * 100;
        discountRate = GeneralfxService.roundOffDecimal(discountRate);
      } else {
        discountRate = 0;
      }
    }

    totalAmount = totalAmount - discountAmount;
    totalAmount = GeneralfxService.roundOffDecimal(totalAmount);

    grossAmount = totalAmount;

    vatAmount = (grossAmount / 1.12) * 0.12;
    vatAmount = GeneralfxService.roundOffDecimal(vatAmount);

    if (
      this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
      this.CurrentObject.ID_TaxScheme == 0
    ) {
      vatAmount = 0;
      netAmount = grossAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive) {
      netAmount = grossAmount + vatAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive) {
      netAmount = grossAmount - vatAmount;
    }

    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;

    this.isDirty = true;
  }

  ReceivingReportDetailTable_onChanged(e: any) {
    this.compute();
  }

  public CurrentObject_onBeforeSaving() {
    this.compute();
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_PurchaseOrder') {
      if (e.data) {
        var data: any = e.data;
        this.CurrentObject.Name_Supplier = data['Name_Supplier'];
        this.CurrentObject.ID_Supplier = data['ID_Supplier'];
        this.CurrentObject.Code_PurchaseOrder = data['Code'];
      }

      this.CurrentObject.ReceivingReport_Detail = [];
      this.compute();
    }
  }
}
