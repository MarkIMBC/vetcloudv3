import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { PurchaseOrderListRoutingModule } from './purchase-order-list-routing.module';
import { PurchaseOrderListComponent } from './purchase-order-list.component';

@NgModule({
  declarations: [PurchaseOrderListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PurchaseOrderListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PurchaseOrderListModule { }
