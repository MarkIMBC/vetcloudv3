import { Item } from './../../../shared/APP_MODELS';
import { Item_DTO, PurchaseOrder_DTO } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './purchase-order-list.component.less'
  ],
})
export class PurchaseOrderListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'PurchaseOrderListComponent';

  headerTitle: string = 'Purchase Order';

  InitCurrentObject: any = {
    Code: '',
  }

  dataSource: PurchaseOrder_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'PurchaseOrder',
      visible: true,
      isActive: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM vPurchaseOrder_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['PurchaseOrder', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['PurchaseOrder', -1], {});
  }
}
