import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientGroomingListRoutingModule } from './patient-grooming-list-routing.module';
import { PatientGroomingListComponent } from './patient-grooming-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [PatientGroomingListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientGroomingListRoutingModule
  ]
})
export class PatientGroomingListModule { }
