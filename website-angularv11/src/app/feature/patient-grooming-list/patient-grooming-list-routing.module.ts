import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientGroomingListComponent } from './patient-grooming-list.component';

const routes: Routes = [{ path: '', component: PatientGroomingListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientGroomingListRoutingModule { }
