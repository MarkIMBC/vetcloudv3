import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VeterinaryHealthCertificateDetailComponent } from './veterinary-health-certificate-detail.component';

const routes: Routes = [{ path: '', component: VeterinaryHealthCertificateDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeterinaryHealthCertificateDetailRoutingModule { }
