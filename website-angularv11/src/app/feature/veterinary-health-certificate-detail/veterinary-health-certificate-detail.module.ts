import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VeterinaryHealthCertificateDetailRoutingModule } from './veterinary-health-certificate-detail-routing.module';
import { VeterinaryHealthCertificateDetailComponent } from './veterinary-health-certificate-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [VeterinaryHealthCertificateDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    VeterinaryHealthCertificateDetailRoutingModule
  ]
})
export class VeterinaryHealthCertificateDetailModule { }
