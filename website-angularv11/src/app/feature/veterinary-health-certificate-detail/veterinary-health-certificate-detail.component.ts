import {
  FilingStatusEnum,
  FilterCriteriaType,
  IFormValidation,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
} from './../../../shared/APP_HELPER';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  AdminLTERadioButtonListComponent,
  AdminLTERadioButtonListDateSourceItem,
} from 'src/app/shared/control/admin-lte-radio-button-list/admin-lte-radio-button-list.component';

@Component({
  selector: 'app-veterinary-health-certificate-detail',
  templateUrl: './veterinary-health-certificate-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './veterinary-health-certificate-detail.component.less',
  ],
})
export class VeterinaryHealthCertificateDetailComponent extends BaseDetailViewComponent {
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  ModelName: string = 'VeterinaryHealthCertificate';
  headerTitle: string = 'Veterinary Health Certificate';
  displayMember: string = 'Code';

  VaccinationOption_ListDataSource: AdminLTERadioButtonListDateSourceItem[] =
    [];

  CaseTypeList: string[] = [];

  async loadCaseTypeList(): Promise<void> {
    this.CaseTypeList = [];

    var sql = this.cs.encrypt(
      `SELECT
                ID,
                Name
          FROM dbo.tCaseType
      `
    );

    var _CaseTypeList: string[] = [];

    var objs = await this._dataServices.query<any>(sql);

    objs.forEach(function (obj) {
      _CaseTypeList.push(obj.Name);
    });

    this.CaseTypeList = _CaseTypeList;
  }

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  ID_Item_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  _menuItem_Cancel: AdminLTEMenuItem = {
    label: 'Cancel',
    icon: 'fa fa-times',
    class: 'text-danger',
    visible: true,
  };

  async loadVaccinationOptionListDataSource(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT ID, Comment FROM dbo.tVaccinationOption
      `
    );

    var _VaccinationOption_ListDataSource: AdminLTERadioButtonListDateSourceItem[] =
      [];
    var objs = await this._dataServices.query<any>(sql);

    this.VaccinationOption_ListDataSource = [];

    objs.forEach(function (obj) {
      var item: AdminLTERadioButtonListDateSourceItem =
        new AdminLTERadioButtonListDateSourceItem();

      item.value = obj.ID;
      item.label = obj.Comment;

      _VaccinationOption_ListDataSource.push(item);
    });

    this.VaccinationOption_ListDataSource = _VaccinationOption_ListDataSource;
  }

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client}`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.Name_Patient != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Patient}`,
        visible: true,
        name: 'viewpatient',
      });
    }

    if (this.CurrentObject.ID > 0) {
      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice',
      });
    }
  }

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'viewclient') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before navigate client info.`
        );
        return;
      }

      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'viewpatient') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before navigate patient info.`
        );
        return;
      }

      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {});
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      this.doCancel();
    } else if (e.item.name == 'VeterinaryHealthCertificateReport') {
      this.customNavigate(['Report'], {
        ReportName: 'VeterinaryHealthClinic',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Veterinary Certificate',
          name: 'VeterinaryHealthCertificateReport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
      ],
    };

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._menuItem_Cancel);
      this.addMenuItem(menuReport);
    }
  }

  doCancel() {
    // if (!this.isDirty) {

    //   this.toastService.warning(`Please save first before canceling ${this.CurrentObject.Code}.`);
    //   return;
    // }

    this.loading = true;

    this.execSP(
      'pCancelVeterinaryHealthCertificate',
      {
        IDs_VeterinaryHealthCertificate: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async DetailView_Init() {
    await this.loadCaseTypeList();
    await this.loadVaccinationOptionListDataSource();
  }

  DetailView_onLoad() {
    var listViewOptionAttendingPhysician_ID_Employee =
      this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionID_ITem = this.ID_Item_LookupboxOption.listviewOption;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }

    if (listViewOptionID_ITem != undefined) {
      listViewOptionID_ITem.sql = `/*encryptsqlstart*/
                                    SELECT
                                      ID,
                                      Code,
                                      Name,
                                      UnitPrice,
                                      CurrentInventoryCount,
                                      UnitCost,
                                      RemainingBeforeExpired
                                    FROM dbo.vActiveItem
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/
                                    `;
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;
    var ID_Item = this.CurrentObject.ID_Item;

    if (
      this.CurrentObject.ID_Client == undefined ||
      this.CurrentObject.ID_Client == null
    )
      ID_Client = 0;
    if (
      this.CurrentObject.ID_Patient == undefined ||
      this.CurrentObject.ID_Patient == null
    )
      ID_Patient = 0;
    if (
      this.CurrentObject.ID_Item == undefined ||
      this.CurrentObject.ID_Item == null
    )
      ID_Item = 0;

    if (ID_Client == 0) {
      validations.push({
        message: 'Client is required.',
      });
    }

    if (ID_Patient == 0) {
      validations.push({
        message: 'Patient is required.',
      });
    }

    if (!this.CurrentObject.Date) {
      validations.push({
        message: `Date is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = [
      'ID_Client',
      'ID_Patient',
      'ID_Item',
      'AttendingPhysician_ID_Employee',
      'ID_Patient_SOAP',
    ];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
