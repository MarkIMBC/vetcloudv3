import { ScheduleCalendarRoutingModule } from './../../../schedule-calendar/schedule-calendar-routing.module';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EventContentArg } from '@fullcalendar/angular';
import * as moment from 'moment';
import { title } from 'process';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDateTimeboxComponent } from 'src/app/shared/control/admin-lte-datetimebox/admin-lte-datetimebox.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { IControlModelArg, MonthInfo } from 'src/shared/APP_HELPER';

@Component({
  selector: 'monthly-payments-small-box',
  templateUrl: './monthly-payments-small-box.component.html',
  styleUrls: ['./monthly-payments-small-box.component.less']
})
export class MonthlyPaymentsSmallBoxComponent implements OnInit {

  @ViewChild('selectboxMonth') selectboxMonth: ElementRef | undefined;
  @ViewChild('selectboxYear') selectboxYear: ElementRef | undefined;
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();
  years: number[] = [];
  months: MonthInfo[] = [];

  CurrentObject: any = {
    title: "",
    subtitle: "",
    Month: 1,
    Year: 1,
  }

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {

    this.currentUser = this.userAuth.getDecodedToken();

    this.months = GeneralfxService.getMonthList();
    this.years = GeneralfxService.getYearList();

    console.log(this.months);
  }

  ngOnInit(): void {

    this.reset();
  }

  iconContainer_OnClicked() {

    if (this.modalDialog) this.modalDialog.open();
  }

  btnReset_OnClicked() {

    this.reset();

    if (this.modalDialog) this.modalDialog.close();
  }

  btnFilter_OnClicked() {

    this.loadRecord();

    if (this.modalDialog) this.modalDialog.close();
  }

  control_onModelChanged(e: IControlModelArg) {

    this.CurrentObject[e.name] = e.value;

    // this.loadRecord();
    // if (this.modalDialog) this.modalDialog.close();
  }
  async loadRecord() {

    var obj = await this.ds.execSP(
      "pGetDashboardSmallBoxMonthlyPayments",
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        Month: this.CurrentObject.Month,
        Year: this.CurrentObject.Year,
      },
      {
        isReturnObject: true,
      }
    );

    this.CurrentObject.title = obj.FormattedTotalAmount;
    this.CurrentObject.subtitle = obj.Subtitle;
  }

  reset() {

    var currentYear = moment().year();
    var currentMonth =  moment().format("M");

    this.CurrentObject.Year = currentYear;
    this.CurrentObject.Month = parseInt(currentMonth);

    if(this.selectboxYear) this.selectboxYear.nativeElement.value = currentYear;
    if(this.selectboxMonth) this.selectboxMonth.nativeElement.value = currentMonth;

    this.loadRecord();
  }

}
