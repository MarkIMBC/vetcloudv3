import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  TemplateRef,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import * as moment from 'moment';

@Component({
  selector: 'dashboard-monthlyclient-chart',
  templateUrl: './dashboard-monthlyclient-chart.component.html',
  styleUrls: ['./dashboard-monthlyclient-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardMonthlyClientChartComponent implements OnInit {

  @ViewChild('selectboxYear') selectboxYear: ElementRef | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: Chart | undefined;
  dataSource: any;
  chartLabel: string = '';
  MonthlyClient: Array<number> = [];
  value:number =  0;
  years: any[] = [];

  CurrentObject: any = {
    Year: 0
  }

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();

    var currentYear = moment().year();
    this.CurrentObject.Year = currentYear;

    for (var counter = currentYear; counter >= 2020; counter--) {
      this.years.push(counter);
    }
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    await this.loadRecords();

  }

  _onModelChanged(event: any) {

    this.CurrentObject.Year = event;
    this.loadRecords();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardYearlyClientDataSource',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: this.CurrentObject.Year,
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.MonthlyClient = [];


    this.dataSource.forEach((obj: { TotalCount: number }) => {
      this.MonthlyClient.push(obj.TotalCount);
    });


    this.loadChart();
  }


  loadChart(){

    if(this.myChart) this.myChart.destroy();

    const data = {
      labels: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      datasets: [
        {
          label: this.chartLabel,
          data: this.MonthlyClient,
          fill: false,
          borderColor: '#62BEB6',
          backgroundColor: '#62BEB6',
        },
      ],
    };

    this.myChart = new Chart('MonthlyClientChart', {
      type: 'line',
      data: data,
      options: {
        responsive: true,
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                  return value.toLocaleString();
                },
              },
            },
          ],
        },
        plugins: {
          legend: {
            position: 'top',
          },

          title: {
            display: true,
          },
        },
      },
    });

    this.myChart.update();
  }

  reset(){

    var currentYear = moment().year();

    if(this.selectboxYear) this.selectboxYear.nativeElement.value = currentYear;
    this.CurrentObject.Year = currentYear;

    this.loadRecords();
  }
}
