import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-inventory-detail',
  templateUrl: './report-inventory-detail.component.html',
  styleUrls: ['./report-inventory-detail.component.less'],
})
export class ReportInventoryDetailComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'INVENTORYDETAILREPORT',
  };

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['DateStart_DateTrail']) {
      var value = this.CurrentObject['DateStart_DateTrail'];

      if (this.CurrentObject['DateEnd_DateTrail']) {
        var valueEnd = this.CurrentObject['DateEnd_DateTrail'];

        filterValues.push({
          dataField: 'DateTrail',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [value, valueEnd],
        });
      } else {
        filterValues.push({
          dataField: 'DateTrail',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [value, value],
        });
      }
    }

    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }

    /**Header Caption**/
    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(
      filterValues,
      'dataField',
      'DateTrail'
    );
    if (index > -1) {
      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Date Trail From ${moment(dateStart).format(
        'MM/DD/YYYY'
      )} To ${moment(dateEnd).format('MM/DD/YYYY')}<br/>`;
    }

    if (caption.length > 0) {
      filterValues.push({
        dataField: 'Header_CustomCaption',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }
}
