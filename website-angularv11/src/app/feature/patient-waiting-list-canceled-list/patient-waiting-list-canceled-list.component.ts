import {
  Patient_Vaccination_DTO,
  FilingStatusEnum,
} from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import * as moment from 'moment';

@Component({
  selector: 'app-patient-waiting-list-canceled-list',
  templateUrl: './patient-waiting-list-canceled-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-waiting-list-canceled-list.component.less',
  ],
})
export class PatientWaitingListCanceledListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'PatientWaitingListCanceledListComponent';

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  WaitingStatus_Cancelled = FilingStatusEnum.Cancelled;
  WaitingStatus_Reschedule = FilingStatusEnum.Reschedule;

  headerTitle: string = 'Missed Appointment (Waiting List)';

  OrderByString: string = 'DateStart ASC, Name_Client ASC, Name_Patient ASC';

  InitCurrentObject: any = {
    Code: '',
    Name_Client: '',
    Name_Patient: '',
    Name_FilingStatus: '',
    DateStart: null,
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vPatientWaitingCanceledList_ListView
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%'`;
    }

    var _dateStart = this.CurrentObject['DateStart'];
    var _dateEnd = this.CurrentObject['DateStart'];

    if (!this.CurrentObject['DateStart']) {
      if (filterString.length > 0) filterString += ' AND ';

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, DateStart) = '${moment().format(
          'YYYY-MM-DD'
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateCreated) BETWEEN
               '${moment().format('YYYY-MM-DD')}' AND
               '${moment().format('YYYY-MM-DD')}'
        `;
      }
    } else {
      if (filterString.length > 0) filterString += ' AND ';

      filterString += `
          CONVERT(DATE, DateCreated) BETWEEN
            '${moment(_dateStart).format('YYYY-MM-DD')}' AND
            '${moment(_dateEnd).format('YYYY-MM-DD')}'
      `;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_Vaccination_DTO) {
    this.customNavigate(['Patient_Vaccination', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Patient_Vaccination', -1], {});
  }
}
