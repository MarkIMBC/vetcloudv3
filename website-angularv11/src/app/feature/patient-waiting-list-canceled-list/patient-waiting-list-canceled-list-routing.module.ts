import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientWaitingListCanceledListComponent } from './patient-waiting-list-canceled-list.component';

const routes: Routes = [{ path: '', component: PatientWaitingListCanceledListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientWaitingListCanceledListRoutingModule { }
