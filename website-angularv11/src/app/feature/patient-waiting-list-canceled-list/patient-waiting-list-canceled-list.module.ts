import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CrypterService } from "src/app/core/crypter.service";
import { DataService } from "src/app/core/data.service";
import { SharedModule } from "src/app/shared/shared.module";
import { PatientWaitingListCanceledListRoutingModule } from "./patient-waiting-list-canceled-list-routing.module";
import { PatientWaitingListCanceledListComponent } from "./patient-waiting-list-canceled-list.component";

@NgModule({
  declarations: [PatientWaitingListCanceledListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientWaitingListCanceledListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientWaitingListCanceledListModule { }
