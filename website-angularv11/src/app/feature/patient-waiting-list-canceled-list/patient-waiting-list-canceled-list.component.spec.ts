import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientWaitingListCanceledListComponent } from './patient-waiting-list-canceled-list.component';

describe('PatientWaitingListCanceledListComponent', () => {
  let component: PatientWaitingListCanceledListComponent;
  let fixture: ComponentFixture<PatientWaitingListCanceledListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientWaitingListCanceledListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientWaitingListCanceledListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
