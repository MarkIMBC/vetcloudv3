import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientDetailRoutingModule } from './patient-detail-routing.module';
import { PatientDetailComponent } from './patient-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PatientSOAPTableComponent } from './patient-soap-table/patient-soap-table.component';
import { PatientVaccinationListComponent } from './patient-vaccination-list/patient-vaccination-list.component';
import { PatientWellnessListComponent } from './patient-wellness-list/patient-wellness-list.component';
import { PatientGroomingListComponent } from './patient-grooming-list/patient-grooming-list.component';


@NgModule({
  declarations: [PatientDetailComponent, PatientSOAPTableComponent, PatientVaccinationListComponent, PatientWellnessListComponent, PatientGroomingListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientDetailRoutingModule
  ]
})
export class PatientDetailModule { }
