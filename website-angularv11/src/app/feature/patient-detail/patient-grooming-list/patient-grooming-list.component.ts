import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'patient-grooming-list',
  templateUrl: './patient-grooming-list.component.html',
  styleUrls: [
    './../../../layout/base-list-view/base-list-view.component.less',
    './patient-grooming-list.component.less',
  ],
})
export class PatientGroomingListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any = {};
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID_Patient: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItems() {
    var menuItem_New = {
      label: 'Create',
      icon: 'fa fa-plus',
      visible: true,
    };

    var menuItem_View = {
      label: 'View',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true,
    };

    this.menuItems = [];

    if (
      this.CurrentObject.ID == 0 ||
      this.CurrentObject.ID == -1 ||
      this.CurrentObject.ID == null
    )
      return;
    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_New);
    this.menuItems.push(menuItem_Refresh);
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create') {
      this.doCreate();
    } else if (menuItem.label == 'Refresh') {
      this.doRefresh();
    }
  }

  doCreate() {
    if (this.IsDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      ['Patient_Grooming', -1],
      {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      }
    );
  }

  doRefresh() {
    this.Load(this.ID_Patient);
  }

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async Load(ID_Patient: number) {
    var _ = this;
    this.ID_Patient = ID_Patient;

    var sql = `SELECT *
               FROM dbo.vPatient_Grooming_Detail_Listview
               WHERE
                ID_Patient = ${this.ID_Patient} AND
                ID_FilingStatus IN (${FilingStatusEnum.Filed})
               ORDER BY
                Date DESC, ID_Patient_Grooming
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj: any) => {
      var mainParentID: number = 0;

      var parentRecords: any[] = [];

      obj.forEach((record: any) => {
        if (mainParentID != record.ID_Patient_Grooming) {
          parentRecords.push({
            Date: record.Date,
            Code: record.Code,
            ID_Patient_Grooming: record.ID_Patient_Grooming,
            AttendingPhysician_Name_Employee:
              record.AttendingPhysician_Name_Employee,
            Details: [],
          });

          mainParentID = record.ID_Patient_Grooming;
        }
      });

      obj.forEach((record: any) => {
        parentRecords.forEach((parentRecord: any) => {
          if (parentRecord.ID_Patient_Grooming == record.ID_Patient_Grooming) {
            parentRecord.Details.push({
              Name_Item: record.Name_Item,
              Comment: record.Comment,
            });
          }
        });
      });

      this.dataSource = parentRecords;
      this.loadMenuItems();
    });
  }

  onRowClick(rowData: any) {
    if (this.IsDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      ['Patient_Grooming', rowData.ID_Patient_Grooming],
      {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
        AttendingPhysician_ID_Employee:
          this.CurrentObject.AttendingPhysician_ID_Employee,
      }
    );
  }
}
