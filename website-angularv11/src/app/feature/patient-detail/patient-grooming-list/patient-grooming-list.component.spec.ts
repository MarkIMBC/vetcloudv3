import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientGroomingListComponent } from './patient-grooming-list.component';

describe('PatientGroomingListComponent', () => {
  let component: PatientGroomingListComponent;
  let fixture: ComponentFixture<PatientGroomingListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientGroomingListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientGroomingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
