import { Report } from './../../../shared/APP_MODELS';
import { PatientSOAPTableComponent } from './patient-soap-table/patient-soap-table.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFormValidation,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { PatientVaccinationListComponent } from './patient-vaccination-list/patient-vaccination-list.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { PatientWellnessListComponent } from './patient-wellness-list/patient-wellness-list.component';
import { PatientGroomingListComponent } from './patient-grooming-list/patient-grooming-list.component';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-detail.component.less',
  ],
})
export class PatientDetailComponent extends BaseDetailViewComponent {
  @ViewChild('patientsoaptable') patientsoaptable:
    | PatientSOAPTableComponent
    | undefined;
  @ViewChild('patientvaccinationlist') patientvaccinationlist:
    | PatientVaccinationListComponent
    | undefined;
  @ViewChild('patientwellnesslist') patientwellnesslist:
    | PatientWellnessListComponent
    | undefined;
  @ViewChild('patientgroominglist') patientgroominglist:
    | PatientGroomingListComponent
    | undefined;

  ModelName: string = 'Patient';
  headerTitle: string = 'Patient';
  displayMember: string = 'Name';
  BreedSpecieList: string[] = [];

  async loadBreedSpecieList(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tBreedSpecie
    `
    );

    var _BreedSpecieList: string[] = [];

    var objs = await this._dataServices.query<any>(sql);

    this.BreedSpecieList = [];
    objs.forEach(function (obj) {
      _BreedSpecieList.push(obj.Name);
    });

    this.BreedSpecieList = _BreedSpecieList;
  }

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client}`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.ID > 0) {
      this.rightDropDownItems.push({
        label: 'Create Appointment',
        visible: true,
        name: 'createappointment',
      });

      this.rightDropDownItems.push({
        label: 'Create Medical Record',
        visible: true,
        name: 'createsoap',
      });

      this.rightDropDownItems.push({
        label: 'Create Veterinary Health Certificate',
        visible: true,
        name: 'CreateVeterinaryHealthCertificate',
      });

      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice',
      });

      this.rightDropDownItems.push({
        label: 'Create Pet Grooming',
        visible: true,
        name: 'CreatePetGrooming',
      });
    }
  }

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'createsoap') {
      this.customNavigate(['Patient_SOAP', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'createappointment') {
      this.customNavigate(['PatientAppointment', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'createbillinginvoice') {
      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'viewclient') {
      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'CreateVeterinaryHealthCertificate') {
      this.customNavigate(['VeterinaryHealthCertificate', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'CreatePetGrooming') {
      this.customNavigate(['Patient_Grooming', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID,
      });
    }
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  loadMenuItems() {
    this.loadModelReports();
  }

  async loadModelReports() {
    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    this._dataServices
      .execSP(
        'pGetModelReports',
        {
          CompanyID: this.currentUser.ID_Company,
          Oid_Model: this.model.Oid,
        },
        {
          isReturnObject: true,
        }
      )
      .then((obj: any) => {
        if (!obj.Reports) obj.Reports = [];

        if (obj.Reports.length > 0) {
          obj.Reports.forEach((modelReport: any) => {
            var objReport: AdminLTEMenuItem = {
              label: modelReport.Name_ModelReport,
              name: modelReport.Name_Report + '-ModelReport',
              icon: 'far fa-file',
              class: 'text-primary',
              visible: true,
            };
            if (menuReport.items != undefined) {
              menuReport.items.push(objReport);
            }
          });
        } else {
          var acknowledgementMenuItem = {
            label: 'Acknowledgement',
            name: APP_REPORTVIEW.ACKNOWLEDGEMENTREPORT,
            icon: 'far fa-file',
            class: 'text-primary',
            visible: true,
          };

          var admissionReportMenuItem = {
            label: 'Admission',
            name: APP_REPORTVIEW.ADMISSIONREPORT,
            icon: 'far fa-file',
            class: 'text-primary',
            visible: true,
          };

          var agreementforConfinementMenuItem = {
            label: 'Agreement For Confinement',
            name: APP_REPORTVIEW.AGREEMENTFORCONFINEMENT,
            icon: 'far fa-file',
            class: 'text-primary',
            visible: true,
          };

          var concenttooperationMenuItem = {
            label: 'Consent To Operation',
            name: APP_REPORTVIEW.CONCENTTOOPERATION,
            icon: 'far fa-file',
            class: 'text-primary',
            visible: true,
          };

          var euthanasiaauthorizationMenuItem = {
            label: 'Euthanasia Authorization',
            name: APP_REPORTVIEW.EUTHANASIAAUTHORIZATION,
            icon: 'far fa-file',
            class: 'text-primary',
            visible: true,
          };

          if (menuReport.items) {
            menuReport.items.push(acknowledgementMenuItem);
            menuReport.items.push(admissionReportMenuItem);
            menuReport.items.push(agreementforConfinementMenuItem);
            menuReport.items.push(concenttooperationMenuItem);
            menuReport.items.push(euthanasiaauthorizationMenuItem);
          }
        }
      });

    if (this.CurrentObject.IsActive != true) return;

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name.includes('-ModelReport')) {
      var Name_Report = e.item.name.replace('-ModelReport', '');

      this.customNavigate(['Report'], {
        ReportName: Name_Report,
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }

    if (e.item.name == APP_REPORTVIEW.ACKNOWLEDGEMENTREPORT) {
      this.customNavigate(['Report'], {
        ReportName: 'ACKNOWLEDGEMENTREPORT',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == APP_REPORTVIEW.ADMISSIONREPORT) {
      this.customNavigate(['Report'], {
        ReportName: 'ADMISSIONREPORT',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == APP_REPORTVIEW.AGREEMENTFORCONFINEMENT) {
      this.customNavigate(['Report'], {
        ReportName: 'AGREEMENTFORCONFINEMENT',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == APP_REPORTVIEW.CONCENTTOOPERATION) {
      this.customNavigate(['Report'], {
        ReportName: 'CONCENTTOOPERATION',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == APP_REPORTVIEW.EUTHANASIAAUTHORIZATION) {
      this.customNavigate(['Report'], {
        ReportName: 'EUTHANASIAAUTHORIZATION',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == APP_REPORTVIEW.VETERINARYHEALTHCLINIC) {
      this.customNavigate(['Report'], {
        ReportName: 'VETERINARYHEALTHCLINIC',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  DetailView_onLoad() {
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;
    }

    this.loadBreedSpecieList();

    if (this.patientsoaptable != undefined) {
      this.patientsoaptable.load(this.CurrentObject.ID);
    }

    if (this.patientwellnesslist)
      this.patientwellnesslist.Load(this.CurrentObject.ID);

    if (this.patientgroominglist)
      this.patientgroominglist.Load(this.CurrentObject.ID);
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Client'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    this.CurrentObject.Name = this.CurrentObject.Name.trimStart();
    this.CurrentObject.Name = this.CurrentObject.Name.trimEnd();

    if (this.CurrentObject.ID_Client == null) this.CurrentObject.ID_Client = 0;

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    if (this.CurrentObject.ID_Client == 0) {
      validations.push({
        message: `Client is required.`,
      });
    }
    return Promise.resolve(validations);
  }
}
