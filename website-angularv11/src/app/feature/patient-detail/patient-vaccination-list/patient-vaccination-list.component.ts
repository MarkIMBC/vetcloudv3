import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'patient-vaccination-list',
  templateUrl: './patient-vaccination-list.component.html',
  styleUrls: ['./patient-vaccination-list.component.less']
})
export class PatientVaccinationListComponent implements OnInit {

  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any = {};
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID_Patient: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {

  }

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItems() {

    var menuItem_New = {
      label: 'Create',
      icon: 'fa fa-plus',
      visible: true
    };

    var menuItem_View = {
      label: 'View',
      icon: 'fa fa-stethoscope',
      visible: true
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true
    };

    this.menuItems = [];

    if (this.CurrentObject.ID < 0) return;
    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_New);
    this.menuItems.push(menuItem_Refresh);
  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create') {

      this.doCreate()
    } else if (menuItem.label == 'Refresh') {

      this.doRefresh()
    }
  }

  doCreate() {

    if (this.IsDirty) {

      this.toastService.warning(`Please save first before approving ${this.CurrentObject.Code}.`);
      return;
    }

    GeneralfxService.customNavigate(this.router, this.cs, ['Patient_Vaccination', -1], {
      ID_Client: this.CurrentObject.ID_Client,
      ID_Patient: this.CurrentObject.ID,
      AttendingPhysician_ID_Employee: this.CurrentObject.AttendingPhysician_ID_Employee,
    });
  }

  doRefresh() {

    this.Load(this.ID_Patient);
  }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async Load(ID_Patient: number) {

    this.menuItems = [];

    this.ID_Patient = ID_Patient;

    var sql = `SELECT *
               FROM dbo.vPatient_Vaccination_Listview
               WHERE
                ID_Patient = ${this.ID_Patient} AND
                ID_FilingStatus IN (${FilingStatusEnum.Filed})
               Order BY
                Date DESC
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql)
    .then((obj) => {

        this.dataSource = obj;

        this.loadMenuItems();
     });
  }

  onRowClick(rowData: any) {

    if (this.IsDirty) {

      this.toastService.warning(`Please save first before approving ${this.CurrentObject.Code}.`);
      return;
    }

    GeneralfxService.customNavigate(this.router, this.cs, ['Patient_Vaccination', rowData.ID], {
      ID_Client: this.CurrentObject.ID_Client,
      ID_Patient: this.CurrentObject.ID,
      AttendingPhysician_ID_Employee: this.CurrentObject.AttendingPhysician_ID_Employee
    });
  }

}
