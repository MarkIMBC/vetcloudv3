import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { CompanyGUID, FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'patient-soap-table',
  templateUrl: './patient-soap-table.component.html',
  styleUrls: ['./patient-soap-table.component.less'],
})
export class PatientSOAPTableComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any = {};
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  isShowSOAPSummary: boolean = false;
  menuItems: AdminLTEMenuItem[] = [];

  private ID_Patient: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {

    this.currentUser = await this.userAuth.getDecodedToken();
  }


  loadMenuItems() {
    var menuItem_New = {
      label: 'Create',
      icon: 'fa fa-plus',
      visible: true,
    };

    var menuItem_View = {
      label: 'View',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true,
    };

    this.menuItems = [];

    if (
      this.ID_Patient == 0 ||
      this.ID_Patient == -1 ||
      this.ID_Patient == null
    )
      return;

    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_New);
    this.menuItems.push(menuItem_Refresh);
  }

  load(ID_Patient: number) {
    this.ID_Patient = ID_Patient;

    var sql = `
        /*encryptsqlstart*/
        SELECT  *
        FROM dbo.vPatient_SOAP_ListView
        WHERE
          ID_Patient = ${this.ID_Patient} AND
          ID_Patient > 0  AND
          ID_FilingStatus IN (${FilingStatusEnum.Filed}, ${FilingStatusEnum.Approved}, ${FilingStatusEnum.Done})
        ORDER BY Date DESC
        /*encryptsqlend*/
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;
    });

    this.loadMenuItems();
  }

  onRowClick(rowData: any) {
    var routeLink = ['Patient_SOAP', rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {});
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (
      this.CurrentObject.ID == 0 ||
      this.CurrentObject.ID == -1 ||
      this.CurrentObject.ID == null
    )
      return;

    if (menuItem.label == 'Create') {
      GeneralfxService.customNavigate(
        this.router,
        this.cs,
        ['Patient_SOAP', -1],
        {
          ID_Client: this.CurrentObject.ID_Client,
          ID_Patient: this.CurrentObject.ID,
        }
      );
    } else if (menuItem.label == 'Refresh') {
      this.load(this.CurrentObject.ID);
    }
  }
}
