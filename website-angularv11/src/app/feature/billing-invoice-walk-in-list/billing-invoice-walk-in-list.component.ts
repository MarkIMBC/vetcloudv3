import {
  FilingStatusEnum,
  FilterCriteriaType,
  PropertyTypeEnum,
} from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingInvoice_DTO } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import * as moment from 'moment';

@Component({
  selector: 'app-billing-invoice-walk-in-list',
  templateUrl: './billing-invoice-walk-in-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './billing-invoice-walk-in-list.component.less',
  ],
})
export class BillingInvoiceWalkInListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'BillingInvoiceWalkInListComponent';

  headerTitle: string = 'Billing Invoice (Walk In)';

  InitCurrentObject: any = {
    DateStart_BillingInvoice: null,
    DateEnd_BillingInvoice: null,
    Code: '',
    WalkInCustomerName: '',
    Name_FilingStatus: '',
    OtherReferenceNumber: '',
    Comment: '',
    Status: '',
    Name_SOAPType: '',
  };

  dataSource: BillingInvoice_DTO[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Billing Invoice',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  FilingStatus_Approved: FilingStatusEnum = FilingStatusEnum.Approved;

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
          SELECT
            *
        FROM dbo.vBillingInvoice_Walkin_ListView
        /*encryptsqlend*/
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Status == null) this.CurrentObject.Status = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.WalkInCustomerName.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `WalkInCustomerName LIKE '%${this.CurrentObject.WalkInCustomerName}%'`;
    }

    if (this.CurrentObject.Status.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Status LIKE '%${this.CurrentObject.Status}%'`;
    }

    if (this.CurrentObject.OtherReferenceNumber.length > 0) {
      filterString += `OtherReferenceNumber LIKE '%${this.CurrentObject.OtherReferenceNumber}%'`;
    }

    if (this.CurrentObject.Comment.length > 0) {
      filterString += `Comment LIKE '%${this.CurrentObject.Comment}%'`;
    }

    if (this.CurrentObject.Name_SOAPType.length > 0) {
      filterString += `Name_SOAPType LIKE '%${this.CurrentObject.Name_SOAPType}%'`;
    }

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (this.CurrentObject['DateStart_BillingInvoice']) {
      var _dateStart = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {
        var _dateEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        if (filterString.length > 0) filterString = filterString + ' AND ';

        if (_dateStart != null && _dateEnd == null) {
          filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
            'YYYY-MM-DD'
          )}'`;
        } else {
          filterString += `
              CONVERT(DATE, Date) BETWEEN
                 '${moment(_dateStart).format('YYYY-MM-DD')}' AND
                 '${moment(_dateEnd).format('YYYY-MM-DD')}'
          `;
        }
      }
    }

    if (!hasAssignedFilter) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_FilingStatus NOT IN (4)`;
    }

    return filterString;
  }

  Row_OnClick(rowData: BillingInvoice_DTO) {
    this.customNavigate(['BillingInvoice', rowData.ID], {
      IsWalkIn: true,
    });
  }

  menuItem_New_onClick() {
    this.customNavigate(['BillingInvoice', -1], {
      IsWalkIn: true,
    });
  }
}
