
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { BillingInvoiceWalkInListRoutingModule } from './billing-invoice-walk-in-list-routing.module';
import { BillingInvoiceWalkInListComponent } from './billing-invoice-walk-in-list.component';


@NgModule({
  declarations: [BillingInvoiceWalkInListComponent],
  imports: [
    CommonModule,
    SharedModule,
    BillingInvoiceWalkInListRoutingModule
  ]
})
export class BillingInvoiceWalkInListModule { }
