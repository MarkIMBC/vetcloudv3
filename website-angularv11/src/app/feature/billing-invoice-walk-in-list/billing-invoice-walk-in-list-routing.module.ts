import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingInvoiceWalkInListComponent } from './billing-invoice-walk-in-list.component';

const routes: Routes = [{ path: '', component: BillingInvoiceWalkInListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingInvoiceWalkInListRoutingModule { }
