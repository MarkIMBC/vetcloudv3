import { IControlModelArg, ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import * as moment from 'moment';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './company-detail.component.less',
  ],
})
export class CompanyDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'Company';
  headerTitle: string = 'Company';

  displayMember: string = 'Name';
  routerFeatureName: string = 'CompanyDetail';

  SOAPPlanSMSMessageHTML: string = '';
  SOAPPlanSMSMessageStringCount = 0;

  SMSFormatBillingInvoiceNotificationHTML: string = '';
  SMSFormatBillingInvoiceNotificationStringCount: number = 0;

  SMSTemplateSampleData: any = {};

  smsSetting: any = {};

  protected async _getDefault__ID_CurrentObject() {
    var id: number = 0;
    if (this.currentUser != undefined) {
      if (this.currentUser.ID_Company != undefined) {
        id = this.currentUser.ID_Company;
      }
    }

    return id;
  }

  DetailView_Init() {
    this.formatSMSTemplateSampleData();
  }

  DetailView_onLoad() {
    this.SMSTemplateSampleData = {
      Client: 'Client C. Client',
      Pet: 'Pet Name',
      Service: 'Services Applied',
      DateReturn: '1/01/2000 Mon',
      CompanyName: this.CurrentObject.Name,
      ContactNumber: '09123456789',
    };

    if (
      this.CurrentObject.Company_SMSSetting == null ||
      this.CurrentObject.Company_SMSSetting == undefined
    ) {
      this.CurrentObject.Company_SMSSetting = [];
    }

    if (this.CurrentObject.Company_SMSSetting.length == 0) {
      this.CurrentObject.Company_SMSSetting.push({
        ID: this._tempID,
        ID_Company: this.currentUser.ID_Company,
      });
    }

    this.smsSetting = this.CurrentObject.Company_SMSSetting[0];

    this.formatSMSTemplateSampleData();
  }

  loadInitMenuItem() {
    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Save);

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  CurrentObject_onValueChange(e: IControlModelArg) {
    if (e.name == 'SOAPPlanSMSMessage') {
      this.formatSMSTemplateSampleData();
    }
  }

  smsSetting_onModelChanged(e: IControlModelArg) {
    this.smsSetting[e.name] = e.value;

    if (e.displayName != undefined) {
      this.smsSetting[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }

    if (e.name == 'SMSFormatBillingInvoiceNotification') {
      this.formatSMSTemplateSampleData();
    }

    this.isDirty = true;
  }

  formatSMSTemplateSampleData() {
    var _ = this;
    if (
      this.CurrentObject['SOAPPlanSMSMessage'] == null ||
      this.CurrentObject['SOAPPlanSMSMessage'] == undefined
    )
      this.CurrentObject['SOAPPlanSMSMessage'] = '';

    if (
      this.smsSetting['SMSFormatBillingInvoiceNotification'] == null ||
      this.smsSetting['SMSFormatBillingInvoiceNotification'] == undefined
    )
      this.smsSetting['SMSFormatBillingInvoiceNotification'] = '';

    this.SOAPPlanSMSMessageHTML = this.CurrentObject['SOAPPlanSMSMessage'];
    this.SOAPPlanSMSMessageStringCount = this.SOAPPlanSMSMessageHTML.length;
    this.SOAPPlanSMSMessageHTML = this.SOAPPlanSMSMessageHTML.replace(
      /(?:\r\n|\r|\n)/g,
      '<br>'
    );

    this.SMSFormatBillingInvoiceNotificationHTML =
      this.smsSetting['SMSFormatBillingInvoiceNotification'];
    this.SMSFormatBillingInvoiceNotificationStringCount =
      this.SMSFormatBillingInvoiceNotificationHTML.length;
    this.SMSFormatBillingInvoiceNotificationHTML =
      this.SMSFormatBillingInvoiceNotificationHTML.replace(
        /(?:\r\n|\r|\n)/g,
        '<br>'
      );

    Object.keys(this.SMSTemplateSampleData).forEach((key) => {
      this.SMSFormatBillingInvoiceNotificationHTML =
        this.SMSFormatBillingInvoiceNotificationHTML.toString().replace(
          `/*${key}*/`,
          `<b class="text-danger">${_.SMSTemplateSampleData[key]}</b>`
        );
    });
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName];
    this.customNavigate(routeLink);
  }
}
