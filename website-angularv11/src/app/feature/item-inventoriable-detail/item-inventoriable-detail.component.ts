import { Item_Supplier } from './../../../shared/APP_MODELS';
import { AdjustInventoryDialogComponent } from './adjust-inventory-dialog/adjust-inventory-dialog.component';
import { ItemTypeEnum, Item_DTO } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-item-inventoriable-detail',
  templateUrl: './item-inventoriable-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './item-inventoriable-detail.component.less',
  ],
})
export class ItemInventoriableDetailComponent extends BaseDetailViewComponent {
  @ViewChild('adjustinventorydialog') adjustinventorydialog:
    | AdjustInventoryDialogComponent
    | undefined;

  ModelName: string = 'Item';
  headerTitle: string = 'Item';

  routerFeatureName: string = 'ItemInventoriable';
  displayMember: string = 'Name';

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `SELECT ID, Name FROM tItemCategory WHERE ID_ItemType = ${ItemTypeEnum.Inventoriable}`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.IsActive != true) return;

    var menuAdjustInventory = {
      label: 'Adjust Quantity',
      icon: 'fas fa-cubes',
      class: 'text-primary',
      visible: true,
    };

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(menuAdjustInventory);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Adjust Quantity') {
      if (this.adjustinventorydialog != undefined) {
        await this.adjustinventorydialog.show(this.CurrentObject.ID, true);
        this.loadRecord();
      }
    }
  }

  DetailView_onLoad() {
    this.CurrentObject.ID_ItemType = ItemTypeEnum.Inventoriable;

    if (this.CurrentObject.Item_Supplier == null) {
      this.CurrentObject.Item_Supplier = [];
    }
  }

  async btnAdjustInventoryAdd_onClick() {
    if (this.adjustinventorydialog != undefined) {
      await this.adjustinventorydialog.show(this.CurrentObject.ID, true);

      this.loadRecord();
    }
  }
  async btnAdjustInventoryDeduct_onClick() {
    if (this.adjustinventorydialog != undefined) {
      await this.adjustinventorydialog.show(this.CurrentObject.ID, false);

      this.loadRecord();
    }
  }

  async btnRefreshInventory_OnClick() {

   this.refreshItemInventory();
  }

  async refreshItemInventory() {

    this.loading = true;

    this.execSP(
      'pReUpdateItemCurrentInventory',
      {
        ID_Item: this.CurrentObject.ID
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        this.loadRecord();
      })
      .catch(() => {
        this.loadRecord();
      });
  }

  public CurrentObject_onBeforeSaving() {
    var item: any = this.CurrentObject;
    var fieldNames: string[] = [
      'UnitCost',
      'UnitPrice',
      'MinInventoryCount',
      'MaxInventoryCount',
    ];

    fieldNames.forEach((fieldName) => {
      if (!item[fieldName]) item[fieldName] = 0;
      item[fieldName] = Math.abs(item[fieldName]);
    });
  }
}
