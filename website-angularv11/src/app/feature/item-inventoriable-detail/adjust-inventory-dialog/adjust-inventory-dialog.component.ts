import { Component, OnInit, ViewChild } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { UserAuthenticationService, TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum, IControlModelArg, IFormValidation, ReceiveInventory } from 'src/shared/APP_HELPER';

@Component({
  selector: 'adjust-inventory-dialog',
  templateUrl: './adjust-inventory-dialog.component.html',
  styleUrls: ['./adjust-inventory-dialog.component.less']
})
export class AdjustInventoryDialogComponent implements OnInit {

  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: ReceiveInventory = new ReceiveInventory();

  private ID_Item: number = 0;

  constructor(private ds: DataService, private userAuth: UserAuthenticationService,
    public toastService: ToastService, private cs: CrypterService) { }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(ID_Item: number, IsAddInventory: boolean): Promise<any> {

    this.CurrentObject = new ReceiveInventory();

    this.CurrentObject.ID_Item = ID_Item;
    this.CurrentObject.Quantity = 1;
    this.CurrentObject.IsAddInventory = IsAddInventory
    this.CurrentObject.ID_FilingStatus = FilingStatusEnum.Approved;
    this.CurrentObject.Comment = 'N/A';

    var modalDialog = this.modalDialog;

    var promise = new Promise<any>(async (resolve, reject) => {

      if (modalDialog == undefined) {

        reject('no modal instantiate..');
      } else {

        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  btnAdd_onClick() {

    this.CurrentObject.IsAddInventory = false;
  }

  btnDeduct_onClick() {

    this.CurrentObject.IsAddInventory = true;
  }

  Quantity_onModelChanged(e: IControlModelArg) {

    this.CurrentObject.Quantity = e.value;
  }

  Comment_onModelChanged(e: IControlModelArg) {

    this.CurrentObject.Comment = e.value;
  }






  async btnAdjust_onClick() {

    this.loading = true;

    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject != undefined) {

      var qty = this.CurrentObject.Quantity;
      var comment = this.CurrentObject.Comment;

      if (qty == undefined) qty = 0;
      if (comment == undefined) comment = '';

      if (qty == 0)
        validationsAppForm.push({ message: "Quantity is required." });

      if (comment.replace(/\s/g, "").length == 0)
        validationsAppForm.push({ message: "Note is required." });

      if (validationsAppForm.length == 0 && this.CurrentObject.IsAddInventory != true) {
        var currentInventoryCount = 0;
        var itemInventories = [];
        var sql = `SELECT CurrentInventoryCount
          FROM dbo.tItem
          WHERE ID = ${this.CurrentObject.ID_Item}
        `;

        sql = this.cs.encrypt(sql);
        itemInventories = await this.ds.query<any>(sql);

        if (itemInventories == null) itemInventories = [];

        if (itemInventories.length == 1)
          currentInventoryCount = itemInventories[0].CurrentInventoryCount;

        if (qty > currentInventoryCount)
          validationsAppForm.push({ message: `Insufficient Inventory to deduct for the Current Inventory: ${currentInventoryCount}` });

      }
    }

    validationsAppForm.forEach((validation) => {

      this.toastService.warning(validation.message);
    });

    this.loading = false;

    if(validationsAppForm.length > 0) return;

    this.receiveInventory().then(() => {

      if(this.modalDialog != undefined){

        this.modalDialog.close();
      }
    });
  }


  async receiveInventory(): Promise<any> {

    var item = this.CurrentObject;

    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {

      var obj = await this.ds.execSP(
        "pReceiveInventory",
        {
          record: [this.CurrentObject],
          ID_UserSession: this.currentUser.ID_UserSession
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(
          `Item has been ${this.CurrentObject.IsAddInventory ? 'Add' : 'Deduct'} successfully.`
        );
        res(obj);
      } else {
        this.toastService.danger(obj.message);
        rej(obj);
      }

      this.loading = false;
    });
  }

  btnClose_onClick() {

    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

}
