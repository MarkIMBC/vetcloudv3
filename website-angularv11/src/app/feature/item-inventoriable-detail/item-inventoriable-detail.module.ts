import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemInventoriableDetailRoutingModule } from './item-inventoriable-detail-routing.module';
import { ItemInventoriableDetailComponent } from './item-inventoriable-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdjustInventoryDialogComponent } from './adjust-inventory-dialog/adjust-inventory-dialog.component';
import { SupplierComponent } from './supplier/supplier.component';


@NgModule({
  declarations: [ItemInventoriableDetailComponent, AdjustInventoryDialogComponent, SupplierComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ItemInventoriableDetailRoutingModule
  ]
})
export class ItemInventoriableDetailModule { }
