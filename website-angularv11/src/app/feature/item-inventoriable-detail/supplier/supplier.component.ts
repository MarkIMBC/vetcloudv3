import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  Item_Supplier_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Item_Supplier } from 'src/shared/APP_MODELS';

@Component({
  selector: 'supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.less'],
})
export class SupplierComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() ItemSuppliers: Item_Supplier_DTO[] = [];

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Supplier',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Supplier') {
      this.doAddItem_Supplier();
    }
  }

  async BtnDeleteItem_Supplier_OnClick(Item_Supplier: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.ItemSuppliers,
      'ID',
      Item_Supplier.ID + ''
    );

    this.ItemSuppliers.splice(index, 1);
  }

  async doAddItem_Supplier() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name
            FROM dbo.tSupplier
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Supplier: record.ID,
        Name_Supplier: record.Name,
      };

      if (this.ItemSuppliers == null) this.ItemSuppliers = [];
      this.ItemSuppliers.push(item);
    });
  }

  Item_Supplier_onModelChanged(Item_Supplier: any, e: IControlModelArg) {

    Item_Supplier[e.name] = e.value;
  }
}
