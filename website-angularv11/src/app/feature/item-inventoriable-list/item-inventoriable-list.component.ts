import { Item } from './../../../shared/APP_MODELS';
import {
  InventoryStatusEnum,
  Item_DTO,
  PropertyTypeEnum,
} from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import * as moment from 'moment';

@Component({
  selector: 'app-item-inventoriable-list',
  templateUrl: './item-inventoriable-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './item-inventoriable-list.component.less',
  ],
})
export class ItemInventoriableListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'ItemInventoriableListComponent';

  headerTitle: string = 'Item';

  InitCurrentObject: any = {
    Code: '',
    Name: '',
    Name_ItemCategory: '',
    ID_InventoryStatus: 0,
    Name_InventoryStatus: '',
    RemainingBeforeExpired: '',
    OtherInfo_DateExpiration:  null,
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ItemInventoriable',
      visible: true,
      isActive: true,
      command: () => {
        return true;
      },
    },
  ];

  ID_InventoryStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        SELECT '-1' ID, 'Available Stock' Name
        UNION ALL
        Select
          ID, Name
        FROM tInventoryStatus
        WHERE
          ID IN (1, 2)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `

        Select
          ID, Name
        FROM tItemCategory
        WHERE
          ID_ItemType IN (2)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vItemInventoriable_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_InventoryStatus']) {
      if (this.CurrentObject['ID_InventoryStatus'] == -1) {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) IN (
          ${InventoryStatusEnum.High},
          ${InventoryStatusEnum.Medium}
        )`;
      } else {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) = ${this.CurrentObject['ID_InventoryStatus']} `;
      }
    }

    if (this.CurrentObject['ID_ItemCategory']) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(ID_ItemCategory, 0) = ${this.CurrentObject['ID_ItemCategory']} `;
    }

    if (this.CurrentObject['DateStart_OtherInfo_DateExpiration']) {
      var _dateStart = this.CurrentObject['DateStart_OtherInfo_DateExpiration'];

      if (this.CurrentObject['DateEnd_OtherInfo_DateExpiration']) {
        var _dateEnd = this.CurrentObject['DateEnd_OtherInfo_DateExpiration'];

        if (filterString.length > 0) filterString = filterString + ' AND ';

        if (_dateStart != null && _dateEnd == null) {
          filterString += `CONVERT(DATE, OtherInfo_DateExpiration) = '${moment(
            _dateStart
          ).format('YYYY-MM-DD')}'`;
        } else {
          filterString += `
              CONVERT(DATE, OtherInfo_DateExpiration) BETWEEN
                 '${moment(_dateStart).format('YYYY-MM-DD')}' AND
                 '${moment(_dateEnd).format('YYYY-MM-DD')}'
          `;
        }
      }
    }

    return filterString;
  }

  dataSource_InitLoad(obj: any) {
    obj.Records.forEach((record: any) => {
      record['Displayed_Name_ItemCategory'] = '';

      if (record.Name_ItemCategory.length > 23) {
        record['Displayed_Name_ItemCategory'] =
          record.Name_ItemCategory.substring(0, 23) + '...';
      } else {
        record['Displayed_Name_ItemCategory'] = record.Name_ItemCategory;
      }
    });
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['ItemInventoriable', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['ItemInventoriable', -1], {});
  }
}
