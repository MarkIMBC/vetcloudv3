import { Item_DTO, PropertyTypeEnum } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'app-item-service-list',
  templateUrl: './item-service-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './item-service-list.component.less',
  ],
})
export class ItemServiceListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'ItemServiceListComponent';

  headerTitle: string = 'Service';

  InitCurrentObject: any = {
    Code: '',
    Name: '',
    Name_ItemCategory: '',
  };

  dataSource: Item_DTO[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ItemService',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `

        Select
          ID, Name
        FROM tItemCategory
        WHERE
          ID_ItemType IN (1)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM vItemService_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_ItemCategory']) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(ID_ItemCategory, 0) = ${this.CurrentObject['ID_ItemCategory']} `;
    }

    return filterString;
  }

  dataSource_InitLoad(obj: any) {
    obj.Records.forEach((record: any) => {
      record['Displayed_Name_ItemCategory'] = '';

      if (record.Name_ItemCategory.length > 23) {
        record['Displayed_Name_ItemCategory'] =
          record.Name_ItemCategory.substring(0, 23) + '...';
      } else {
        record['Displayed_Name_ItemCategory'] = record.Name_ItemCategory;
      }
    });
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['ItemService', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['ItemService', -1], {});
  }
}
