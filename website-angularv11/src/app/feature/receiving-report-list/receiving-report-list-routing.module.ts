import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceivingReportListComponent } from './receiving-report-list.component';

const routes: Routes = [{ path: '', component: ReceivingReportListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceivingReportListRoutingModule { }
