import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ReceivingReportListRoutingModule } from './receiving-report-list-routing.module';
import { ReceivingReportListComponent } from './receiving-report-list.component';

@NgModule({
  declarations: [ReceivingReportListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReceivingReportListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ReceivingReportListModule { }
