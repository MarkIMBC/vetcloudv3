import { Item } from './../../../shared/APP_MODELS';
import { Item_DTO, ReceivingReport_DTO } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-receiving-report-list',
  templateUrl: './receiving-report-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './receiving-report-list.component.less',
  ],
})
export class ReceivingReportListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'ReceivingReportListComponent';

  headerTitle: string = 'Receiving Report';

  InitCurrentObject: any = {
    Code: '',
    Code_PurchaseOrder: '',
    Name_Supplier: '',
  };

  dataSource: ReceivingReport_DTO[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ReceivingReport',
      visible: true,
      isActive: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM vReceivingReport_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }
    else if (this.CurrentObject.Code_PurchaseOrder.length > 0) {
      filterString += `Code_PurchaseOrder LIKE '%${this.CurrentObject.Code_PurchaseOrder}%'`;
    }
    else if (this.CurrentObject.Name_Supplier.length > 0) {
      filterString += `Name_Supplier LIKE '%${this.CurrentObject.Name_Supplier}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['ReceivingReport', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['ReceivingReport', -1], {});
  }
}
