import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ReportViewerComponent } from 'src/app/shared/report-viewer/report-viewer.component';
import {
  FilterCriteriaType,
  IControlModelArg,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { CurrentObjectOnValueChangeArg } from './../../layout/base-detail-view/base-detail-view.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.less'],
})
export class ReportComponent implements OnInit {
  @ViewChild('reportviewer') reportviewer: ReportViewerComponent | undefined;
  filterValues: IFilterFormValue[] = [];

  currentUser: TokenSessionFields = new TokenSessionFields();
  ID_Report: string = '';
  configOptions: any = {};
  CurrentObject: any = {};

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService,
    protected location: Location
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
    this.loadConfigOption();
  }

  async ngAfterViewInit() {
    await this.loadReportInfo();
    this.load();
  }

  async loadReportInfo() {
    return new Promise<any>(async (resolve, reject) => {
      this.ds
        .execSP(
          'pGetReportByName',
          {
            ReportName: this.configOptions['ReportName'],
            ID_Session: this.currentUser.ID_UserSession,
          },
          { isReturnObject: true }
        )
        .then((obj: any) => {
          this.ID_Report = obj.Oid;

          resolve(obj);
        })
        .catch((obj: any) => {
          reject();
        });
    });
  }

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params['configOptions'];

    if (configOptionsString == undefined || configOptionsString == null) return;
    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  protected getDefaultFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    filterValues.push({
      dataField: 'ID_Company',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return filterValues;
  }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    return filterValues;
  }

  load() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.load(this.ID_Report, filterValues);
  }

  exportPdf() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.exportPDF(this.ID_Report, filterValues);
  }

  exportExcel() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.exportExcel(this.ID_Report, filterValues);
  }

  protected onLoad() {}

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }
  }

  onbtnSaveClick() {
    this.load();
  }

  onbtnClearClick() {
    this.CurrentObject = {};

    this.load();
  }

  onbtnExportPDFClick() {
    this.exportPdf();
  }

  onbtnExportExcelClick() {
    this.exportExcel();
  }

  ononbtnBackButtonClick() {
    this.location.back();
  }

  onbtnRightClick(event: any) {
    if (event.item.name == 'exporttoexcel') {
      this.exportExcel();
    } else if (event.item.name == 'exporttopdf') {
      this.exportPdf();
    }
  }

  onbtnBackClick(event: any) {
    this.location.back();
  }
}
