import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientReferralListComponent } from './patient-referral-list.component';

const routes: Routes = [{ path: '', component: PatientReferralListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientReferralListRoutingModule { }
