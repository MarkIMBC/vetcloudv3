import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';


import { PatientReferralListRoutingModule } from './patient-referral-list-routing.module';
import { PatientReferralListComponent } from './patient-referral-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [PatientReferralListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientReferralListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientReferralListModule { }
