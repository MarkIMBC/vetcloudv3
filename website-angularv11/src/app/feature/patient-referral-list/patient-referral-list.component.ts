
import { Item } from './../../../shared/APP_MODELS';
import { PatientReferral_DTO, PatientReferralRecordInfoEnum } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-patient-referral-list',
  templateUrl: './patient-referral-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-referral-list.component.less'
  ],
})
export class PatientReferralListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'PatientReferralListComponent';

  headerTitle: string = 'Patient Referral';

  InitCurrentObject: any = {
    Name_Client: '',
    Name_Patient: ''
  }

  dataSource: PatientReferral_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'PatientReferral',
      visible: true,
      isActive: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vPatientReferral_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name_Client.length > 0) {

      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (!hasAssignedFilter) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_FilingStatus NOT IN (4)`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['PatientReferral', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['PatientReferral', -1], {});
  }

  dataSource_InitLoad(obj: any) {

    obj.Records.forEach((record: any) => {
      console.log(record)
      record['SummaryInfos'] = [];

      record.SummaryInfos.push({
        id: PatientReferralRecordInfoEnum.History,
        label: 'History',
        value: record.History,
        hasValue: record.Count_History > 0,
        isLoading: false,
      });

      record.SummaryInfos.push({
        id: PatientReferralRecordInfoEnum.Treatment,
        label: 'Treatment',
        value: record.Treatment,
        hasValue: record.Count_Treatment > 0,
        isLoading: false,
      });

      record.SummaryInfos.push({
        id: PatientReferralRecordInfoEnum.Concerns,
        label: 'Concerns',
        value: record.Concerns,
        hasValue: record.Count_Concerns > 0,
        isLoading: false,
      });

    });
  }

}
