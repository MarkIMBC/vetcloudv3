import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralListComponent } from './patient-referral-list.component';

describe('PatientReferralListComponent', () => {
  let component: PatientReferralListComponent;
  let fixture: ComponentFixture<PatientReferralListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
