import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-access',
  templateUrl: './no-access.component.html',
  styleUrls: ['./no-access.component.less'],
})
export class NoAccessComponent implements OnInit {
  cardBodyHeight: string = '';

  constructor() {
    this.resize();
  }

  ngOnInit(): void {}
  resize() {
    var height = window.innerHeight;
    var cardBodyHeight = 0;

    cardBodyHeight = height - 100;

    if (cardBodyHeight < 350) {
      cardBodyHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
  }
}
