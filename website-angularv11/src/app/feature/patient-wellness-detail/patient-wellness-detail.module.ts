import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PatientWellnessDetailRoutingModule } from './patient-wellness-detail-routing.module';
import { PatientWellnessDetailComponent } from './patient-wellness-detail.component';
import { ScheduleListComponent } from './schedule-list/schedule-list.component';
import { DetailListComponent } from './detail-list/detail-list.component';

@NgModule({
  declarations: [PatientWellnessDetailComponent, ScheduleListComponent, DetailListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientWellnessDetailRoutingModule
  ]
})
export class PatientWellnessDetailModule { }
