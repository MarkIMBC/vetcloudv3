import {
  FilingStatusEnum,
  IFormValidation,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
} from './../../../shared/APP_HELPER';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Patient_Wellness_Schedule } from 'src/shared/APP_MODELS';
import * as moment from 'moment';

@Component({
  selector: 'app-patient-wellness-detail',
  templateUrl: './patient-wellness-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-wellness-detail.component.less',
  ],
})
export class PatientWellnessDetailComponent extends BaseDetailViewComponent {
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  ModelName: string = 'Patient_Wellness';
  headerTitle: string = 'Patient Wellness';
  displayMember: string = 'Code';

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  ID_Item_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  _menuItem_ForBilling: AdminLTEMenuItem = {
    label: 'For Billing',
    icon: 'fa fa-file',
    class: 'text-primary',
    visible: true,
  };

  _menuItem_UndoAsForBilling: AdminLTEMenuItem = {
    label: 'Undo For Billing',
    icon: 'fa fa-file red-text',
    class: 'text-primary',
    visible: true,
  };

  _menuItem_Cancel: AdminLTEMenuItem = {
    label: 'Cancel',
    icon: 'fa fa-times',
    class: 'text-danger',
    visible: true,
  };

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client}`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.Name_Patient != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Patient}`,
        visible: true,
        name: 'viewpatient',
      });
    }

    if (this.CurrentObject.ID > 0) {
      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice',
      });
    }
  }

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'viewclient') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before navigate client info.`
        );
        return;
      }

      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'viewpatient') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before navigate patient info.`
        );
        return;
      }

      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {});
    } else if (event.item.name == 'createbillinginvoice') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before Create Billing Invoice.`
        );
        return;
      }

      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        AttendingPhysician_ID_Employee:
          this.CurrentObject.AttendingPhysician_ID_Employee,
        ID_Patient_Wellness: this.CurrentObject.ID,
      });
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      this.doCancel();
    }else if (e.item.label == 'For Billing') {
      this.updateBillStatus(FilingStatusEnum.ForBilling);
    } else if (e.item.label == 'Undo For Billing') {
      this.updateBillStatus(null);
    }
  }

  async updateBillStatus(filingStatus: any): Promise<any> {

    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {

      var obj = await this.execSP(
        'pUpdatePatientWellnessBillStatus',
        {
          IDs_Patient_Wellness: [this.CurrentObject.ID],
          BillingInvoice_ID_FilingStatus: filingStatus,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(`Bill Status has been change successfully.`);

        this.loading = false;

        this.loadRecord();

        res(obj);
      } else {
        this.loading = false;

        this.toastService.warning(`Failed to Change Bill Status.`);

        rej(obj);
      }
    });
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._menuItem_Cancel);
    }

    if (
      [
        FilingStatusEnum.Filed,
        FilingStatusEnum.Approved,
        FilingStatusEnum.Done,
      ].includes(this.CurrentObject.ID_FilingStatus)
    ) {
      if (
        this.CurrentObject.BillingInvoice_ID_FilingStatus !=
        FilingStatusEnum.ForBilling
      ) {
        this.addMenuItem(this._menuItem_ForBilling);
      } else {
        this.addMenuItem(this._menuItem_UndoAsForBilling);
      }
    }
  }

  doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before canceling ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
      'pCancelPatient_Wellness',
      {
        IDs_Patient_Wellness: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  DetailView_onLoad() {
    var listViewOptionAttendingPhysician_ID_Employee =
      this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionID_ITem = this.ID_Item_LookupboxOption.listviewOption;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (this.CurrentObject.Patient_Wellness_Schedule == null) {
      this.CurrentObject.Patient_Wellness_Schedule = [];
    }

    if (this.CurrentObject.Patient_Wellness_Detail == null) {
      this.CurrentObject.Patient_Wellness_Detail = [];
    }

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }

    if (listViewOptionID_ITem != undefined) {
      listViewOptionID_ITem.sql = `/*encryptsqlstart*/
                                    SELECT
                                      ID,
                                      Name,
                                      UnitPrice,
                                      CurrentInventoryCount,
                                      UnitCost,
                                      RemainingBeforeExpired
                                    FROM dbo.vActiveItem
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/
                                    `;
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.vActiveClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vActivePatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vActivePatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;
    var ID_Item = this.CurrentObject.ID_Item;
    var AttendingPhysician_ID_Employee =
      this.CurrentObject.AttendingPhysician_ID_Employee;

    if (
      this.CurrentObject.ID_Client == undefined ||
      this.CurrentObject.ID_Client == null
    )
      ID_Client = 0;
    if (
      this.CurrentObject.ID_Patient == undefined ||
      this.CurrentObject.ID_Patient == null
    )
      ID_Patient = 0;
    if (
      this.CurrentObject.ID_Item == undefined ||
      this.CurrentObject.ID_Item == null
    )
      ID_Item = 0;
    if (
      this.CurrentObject.AttendingPhysician_ID_Employee == undefined ||
      this.CurrentObject.AttendingPhysician_ID_Employee == null
    )
      AttendingPhysician_ID_Employee = 0;

    if (ID_Client == 0) {
      validations.push({
        message: 'Client is required.',
      });
    }

    if (ID_Patient == 0) {
      validations.push({
        message: 'Patient is required.',
      });
    }

    if (AttendingPhysician_ID_Employee == 0) {
      validations.push({
        message: 'Attnd. Veterinarian is required.',
      });
    }

    if (!this.CurrentObject.Date) {
      validations.push({
        message: `Date is required.`,
      });
    }

    if (this.CurrentObject.Patient_Wellness_Detail.length == 0) {
      validations.push({
        message: `Detail is required.`,
      });
    }

    this.CurrentObject.Patient_Wellness_Schedule.forEach(
      (schedule: Patient_Wellness_Schedule) => {
        if (!schedule.Date) {
          validations.push({
            message: `Valid Date Schedule is required.`,
          });
        }
      }
    );

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = [
      'ID_Client',
      'ID_Patient',
      'ID_Item',
      'AttendingPhysician_ID_Employee',
      'ID_Patient_SOAP',
    ];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
