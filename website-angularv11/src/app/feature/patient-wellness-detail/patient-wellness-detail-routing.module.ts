import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientWellnessDetailComponent } from './patient-wellness-detail.component';

const routes: Routes = [{ path: '', component: PatientWellnessDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientWellnessDetailRoutingModule { }
