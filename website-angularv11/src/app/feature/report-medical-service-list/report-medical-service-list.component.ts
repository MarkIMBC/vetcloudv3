import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilingStatusEnum,
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-medical-service-list',
  templateUrl: './report-medical-service-list.component.html',
  styleUrls: ['./report-medical-service-list.component.less'],
})
export class ReportMedicalServiceListComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'MedicalServiceListReport',
  };

  ID_SOAPType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        Select
          ID, Name
        FROM tSOAPType

      `,
      orderByString: 'Name ASC',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_FilingStatus_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name FROM tFilingStatus WHERE ID IN (
        ${FilingStatusEnum.Filed},
        ${FilingStatusEnum.Done},
        ${FilingStatusEnum.Approved},
        ${FilingStatusEnum.Cancelled}
      )`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  onLoad() { }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {

    if (this.CurrentObject['DateStart']) {
      var value = this.CurrentObject['DateStart'];

      if (this.CurrentObject['DateEnd']) {
        var valueEnd = this.CurrentObject['DateEnd'];

        filterValues.push({
          alias: 'schema1',
          dataField: 'Date',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [value, valueEnd],
        });
      } else {
        filterValues.push({
          alias: 'schema1',
          dataField: 'Date',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [value, value],
        });
      }
    } else {
      filterValues.push({
        alias: 'schema1',
        dataField: 'Date',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
      });
    }



    if (this.CurrentObject['Name_Client']) {

      filterValues.push({
        dataField: "Name_Client",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Client']
      });
    }

    if (this.CurrentObject['Name_Patient']) {

      filterValues.push({
        dataField: "Name_Patient",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Patient']
      });
    }


    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }

    if (this.CurrentObject['Name_SOAPType']) {
      filterValues.push({
        dataField: 'Name_SOAPType',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_SOAPType'],
      });
    }

    if (this.CurrentObject['ID_FilingStatus']) {

      filterValues.push({
        dataField: 'ID_FilingStatus',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject["ID_FilingStatus"],
      });
    }
    else {

      filterValues.push({
        dataField: 'ID_FilingStatus',
        filterCriteriaType: FilterCriteriaType.NotEqual,
        propertyType: PropertyTypeEnum.Int,
        value: FilingStatusEnum.Cancelled,
      });
    }

    return filterValues;
  }
}
