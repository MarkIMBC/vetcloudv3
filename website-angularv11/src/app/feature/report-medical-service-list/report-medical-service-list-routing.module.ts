import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportMedicalServiceListComponent } from './report-medical-service-list.component';

const routes: Routes = [{ path: '', component: ReportMedicalServiceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportMedicalServiceListRoutingModule { }
