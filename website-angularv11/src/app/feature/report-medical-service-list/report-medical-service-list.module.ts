import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportMedicalServiceListRoutingModule } from './report-medical-service-list-routing.module';
import { ReportMedicalServiceListComponent } from './report-medical-service-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportMedicalServiceListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportMedicalServiceListRoutingModule
  ]
})
export class ReportMedicalServiceListModule { }
