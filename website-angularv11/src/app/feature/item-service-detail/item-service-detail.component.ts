import { ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-item-service-detail',
  templateUrl: './item-service-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './item-service-detail.component.less'
  ]
})
export class ItemServiceDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Item'
  headerTitle: string = 'Service'

  routerFeatureName: string = 'ItemService';
  displayMember: string = "Name";

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name FROM tItemCategory WHERE ID_ItemType = ${ItemTypeEnum.Service}`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }
  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {

    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;
  }

}
