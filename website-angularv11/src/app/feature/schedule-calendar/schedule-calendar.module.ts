import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ScheduleCalendarRoutingModule } from './schedule-calendar-routing.module';
import { ScheduleCalendarComponent } from './schedule-calendar.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ScheduleCalendarComponent],
  imports: [
    CommonModule,
    FullCalendarModule,
    SharedModule,
    ScheduleCalendarRoutingModule,
  ]
})
export class ScheduleCalendarModule { }
