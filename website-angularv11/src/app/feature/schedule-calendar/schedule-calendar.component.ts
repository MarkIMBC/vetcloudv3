import { CalendarMonthlyViewComponent, CalendarDayTaskItem } from './../../shared/calendar-monthly-view/calendar-monthly-view.component';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CalendarOptions, EventClickArg } from '@fullcalendar/core'; // useful for typechecking
import dayGridPlugin from '@fullcalendar/daygrid';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import {
  Calendar,
  DateRange,
  DatesSetArg,
  EventChangeArg,
  EventInput,
  EventMountArg,
  FullCalendarComponent,
} from '@fullcalendar/angular';
import * as moment from 'moment';
import { APP_MODEL, APP_MODELNAME } from 'src/shared/APP_MODELS';
import tippy, { Tippy } from 'tippy.js';
import { Dictionary } from 'linq-typescript';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Router } from '@angular/router';
import { UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-schedule-calendar',
  templateUrl: './schedule-calendar.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './schedule-calendar.component.less',
  ],
})
export class ScheduleCalendarComponent extends BaseListViewComponent {
  CustomComponentName: string = 'ScheduleCalendarComponent';
  @ViewChild('calendarmonthlyview') calendarmonthlyview: CalendarMonthlyViewComponent | undefined;

  InitCurrentObject: any = {
    ReferenceCode: '',
    Name_Client: '',
    Name_Patient: '',
    AttendingPhysician_Name_Employee: '',
    DateCurrentDateStart: new Date(),
    CurrentYear: 0,
    CurrentMonth: 0,
  };

  headerTitle: string = 'Schedule Calendar';
  showFilter: boolean = true;
  isLazyloading: boolean = false;

  onAfterViewInit() {

    if (this.calendarmonthlyview)
    {
      this.loadRecords();

      if(this.calendarmonthlyview) this.calendarmonthlyview?.refresh();
    }
  }

  async ngAfterViewInit() {
    var _: any = this;

    this.resetCurrentObject();
    this.resetHistoricalFilter();
    var filterObj = this.getLastFilterObj();

    var keyNameFilterObj: any[] = [
      'CurrentObject',
      //"pagingOption",
      'OrderByString',
    ];
    keyNameFilterObj.forEach((key: string) => {
      if (filterObj[key]) {
        _[key] = filterObj[key];
      }
    });

    this.onAfterViewInit();

    setTimeout(() => {
      this.resize();

      this.ListView_OnAfterView();
    }, 2000);

    this.isInitiate = true;
  }

  btnfilter_OnClick() {
    this.showFilter = this.showFilter == false;

    var _ = this;

    setTimeout(() => {
      if (_.calendarmonthlyview) {

      }
    }, 500);
  }

  onResize() {
    var _ = this;

    setTimeout(() => {
      if (_.calendarmonthlyview) {

      }
    }, 500);
  }

  async calendarOptions_handleEventClick(clickInfo: EventClickArg) {
    var eventID = clickInfo.event.id;

    var routeLink: string[] = [];
    var config = {};
    var Oid_Model = '';
    var ID_CurrentObject = 0;
    var ID_Client = 0;
    var splitted = eventID.split('|');

    Oid_Model = splitted[0];
    ID_CurrentObject = splitted[1] as unknown as number;
    ID_Client = splitted[2] as unknown as number;

    config = {
      BackRouteLink: [`/Main`, 'Schedule'],
      ID_Client: ID_Client,
    };

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        routeLink = ['Patient_SOAP', ID_CurrentObject + ''];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = ['PatientAppointment', ID_CurrentObject + ''];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = ['Patient_Wellness', ID_CurrentObject + ''];
        break;
    }

    this.customNavigate(routeLink, {});
  }

  getFilterString(): string
  {
    var filterString = '';
    var dateCoverage = GeneralfxService.getMonthStartAndEnd(this.CurrentObject.CurrentYear, this.CurrentObject.CurrentMonth);

    var dateStartString = GeneralfxService.getFormatDateString(dateCoverage.startDate);
    var dateEndString = GeneralfxService.getFormatDateString(dateCoverage.endDate);

    filterString += `CONVERT(DATE, DateStart) BETWEEN '${dateStartString}' AND '${dateEndString}'`;

    if(this.CurrentObject.ReferenceCode == null) this.CurrentObject.ReferenceCode = "";
    if(this.CurrentObject.Name_Client == null) this.CurrentObject.Name_Client = "";
    if(this.CurrentObject.Name_Patient == null) this.CurrentObject.Name_Patient = "";
    if(this.CurrentObject.AttendingPhysician_Name_Employee == null) this.CurrentObject.AttendingPhysician_Name_Employee = "";
    this.CurrentObject.CompanyID = this.currentUser.ID_Company;

    if (this.CurrentObject.ReferenceCode.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ReferenceCode LIKE '%${this.CurrentObject.ReferenceCode}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.AttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `AttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.AttendingPhysician_Name_Employee}%'`;
    }


    return filterString;
  }

  resetCurrentObject() {

    if(!this.calendarmonthlyview) return;

    var month = this.calendarmonthlyview.month;
    var year = this.calendarmonthlyview.year;

    this.InitCurrentObject.CurrentMonth = month;
    this.InitCurrentObject.CurrentYear = year;

    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurrentObject));
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    this.OrderByString = 'DateStart ASC';
    this.pagingOption.DisplayCount = 5000;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vAppointmentEvent WITH (NOLOCK)
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  dataSource_InitLoad(obj: any)
  {
    if(this.calendarmonthlyview) this.calendarmonthlyview.removeAllTask();

    obj.Records.forEach((record: any) => {

      var dateStartFormat = moment(record.DateStart).format('MMMM DD, yyyy dddd');

      var title = '';
      title += record.FormattedDateStartTime + " - ";
      title += record.Paticular + " - ";
      title += record.Description;

      var tooltip = ""
      tooltip += dateStartFormat + ', ' + record.FormattedDateStartTime + '\n';
      tooltip += 'Client: '+ record.Name_Client + '\n';
      tooltip += 'Patient: '+ record.Name_Patient + '\n\r';
      tooltip += 'Ref. Code: '+ record.ReferenceCode + '\n';
      tooltip += 'Description: '+ record.Description + '\n';

      var task: CalendarDayTaskItem = new CalendarDayTaskItem();

      task.dateString = record.FormattedDateStart;
      task.title = title;
      task.tooltip = tooltip;
      task.className = this.getClassByModel(record.Name_Model);
      task.referenceObj = record;

      if(this.calendarmonthlyview) this.calendarmonthlyview.addTaskItem(task);
    });

    if(this.calendarmonthlyview) this.calendarmonthlyview?.refresh();
  }

  getClassByModel(Name_Model: string) {
    var _class;

    switch (Name_Model) {
      case APP_MODELNAME.PATIENT_SOAP:
        _class = 'fc-daygrid-event-patientsoap';
        break;
      case APP_MODELNAME.PATIENTAPPOINTMENT:
        _class = 'fc-daygrid-event-patientappointment';
        break;
      case APP_MODELNAME.PATIENT_WELLNESS:
        _class = 'fc-daygrid-event-patientwellness';
        break;

      default:
        _class = 'fc-daygrid-event-patientsoap';
        break;
    }
    return _class;
  }

  menuItem_New_onClick() {
    this.customNavigate(['PatientAppointment', -1], {});
  }

  calendarmonthlyview_onChangedMonthYear(obj: any)
  {
    var currentYear = obj.year;
    var currentMonth = obj.month;

    this.CurrentObject.CurrentYear = currentYear;
    this.CurrentObject.CurrentMonth = currentMonth;

    this.loadRecords();
  }

  calendarmonthlyview_onTaskClicked(task: CalendarDayTaskItem)
  {
    var referenceObj = task.referenceObj;
    if(!referenceObj) return;

    var routeLink: string[] = [];
    var Oid_Model = referenceObj.Oid_Model;
    var ID_CurrentObject = referenceObj.ID_CurrentObject;
    var ID_Client = 0;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        routeLink = ['Patient_SOAP', ID_CurrentObject + ''];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = ['PatientAppointment', ID_CurrentObject + ''];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = ['Patient_Wellness', ID_CurrentObject + ''];
        break;
    }

    this.customNavigate(routeLink, {});

  }
}
