import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveSMSSendingListComponent } from './inactive-smssending-list.component';

describe('InactiveSMSSendingListComponent', () => {
  let component: InactiveSMSSendingListComponent;
  let fixture: ComponentFixture<InactiveSMSSendingListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveSMSSendingListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InactiveSMSSendingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
