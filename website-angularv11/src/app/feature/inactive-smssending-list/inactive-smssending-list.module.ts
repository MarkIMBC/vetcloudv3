import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InactiveSMSSendingListRoutingModule } from './inactive-smssending-list-routing.module';
import { InactiveSMSSendingListComponent } from './inactive-smssending-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [InactiveSMSSendingListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    InactiveSMSSendingListRoutingModule,
  ],
})
export class InactiveSMSSendingListModule {}
