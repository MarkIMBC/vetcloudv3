import { Item_DTO, PropertyTypeEnum } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'app-inactive-smssending-list',
  templateUrl: './inactive-smssending-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './inactive-smssending-list.component.less',
  ],
})
export class InactiveSMSSendingListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'InactiveSMSSendingListComponent';

  headerTitle: string = 'Inactive SMS Sending';

  InitCurrentObject: any = {
    Code: '',
    Name: '',
    Name_ItemCategory: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'InactiveSMSSending',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `

        Select
          ID, Name
        FROM tItemCategory
        WHERE
          ID_ItemType IN (1)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  OrderByString: string = 'Date ASC';

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM vInactiveSMSSending_Listview
            /*encryptsqlend*/
            WHERE
              (ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1 )
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_ItemCategory']) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(ID_ItemCategory, 0) = ${this.CurrentObject['ID_ItemCategory']} `;
    }

    return filterString;
  }

  dataSource_InitLoad(obj: any) {
    obj.Records.forEach((record: any) => {
      record['Displayed_Name_ItemCategory'] = '';

      if (record.Name_ItemCategory.length > 23) {
        record['Displayed_Name_ItemCategory'] =
          record.Name_ItemCategory.substring(0, 23) + '...';
      } else {
        record['Displayed_Name_ItemCategory'] = record.Name_ItemCategory;
      }
    });
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['InactiveSMSSending', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['InactiveSMSSending', -1], {});
  }

  openNewTab(rowData: any) {
    var config: any[] = this.getCustomNavigateCommands(
      ['InactiveSMSSending', rowData.ID],
      {}
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }
}
