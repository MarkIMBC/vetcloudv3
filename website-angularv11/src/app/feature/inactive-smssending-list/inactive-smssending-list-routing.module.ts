import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InactiveSMSSendingListComponent } from './inactive-smssending-list.component';

const routes: Routes = [{ path: '', component: InactiveSMSSendingListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InactiveSMSSendingListRoutingModule { }
