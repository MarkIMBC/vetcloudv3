import { EmployeeDetailComponent } from './../employee-detail/employee-detail.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'employee-current-detail',
  templateUrl: './employee-current-detail.component.html',
  styleUrls: ['./../employee-detail/employee-detail.component.less'],
})
export class EmployeeCurrentDetailComponent extends EmployeeDetailComponent {

  protected async _getDefault__ID_CurrentObject() {
    var id: number = 0;
    if (this.currentUser != undefined) {
      if (this.currentUser.ID_Employee != undefined) {
        id = this.currentUser.ID_Employee;
      }
    }

    return id;
  }

}
