import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdjustCreditDialogComponent } from 'src/app/shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'credit-trail-list',
  templateUrl: './credit-trail-list.component.html',
  styleUrls: ['./credit-trail-list.component.less'],
})
export class CreditTrailListComponent implements OnInit {
  @Input() CurrentObject: any;
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  TotalCreditAmount: number = 0;

  @ViewChild('adjustcreditdialog') adjustcreditdialog:
    | AdjustCreditDialogComponent
    | undefined;

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Withdraw',
      icon: 'fa fa-minus',
      class: 'text-danger',
      visible: true,
    },
  ];

  private ID_Client: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async initmenubar_OnClick(e: any) {
    if (e.item.label == 'Withdraw') {
      var ID_Client = this.ID_Client;

      if (this.adjustcreditdialog) {
        this.adjustcreditdialog.IsDeposit = false;
        await this.adjustcreditdialog.show(ID_Client);
        this.load(this.ID_Client);
      }
    }
  }

  load(ID_Client: number) {
    this.ID_Client = ID_Client;

    var sql = `
      SELECT *
      FROM dbo.vClientCredit_ListView
      WHERE
        ID_Client = ${this.ID_Client}
      ORder by Date ASC
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;

      this.dataSource.forEach((record: any) => {
        this.TotalCreditAmount += record['CreditAmount'];
      });
    });
  }

  onRowClick(rowData: any) {
    if (rowData['ID_Patient_Confinement']) {
      var routeLink = ['Confinement', rowData.ID_Patient_Confinement];

      GeneralfxService.customNavigate(this.router, this.cs, routeLink, {});
    }
  }
}
