import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditTrailListComponent } from './credit-trail-list.component';

describe('CreditTrailListComponent', () => {
  let component: CreditTrailListComponent;
  let fixture: ComponentFixture<CreditTrailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditTrailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditTrailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
