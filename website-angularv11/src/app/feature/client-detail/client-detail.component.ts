import { ClientBillingInvoiceTableComponent } from './client-billing-invoice-table/client-billing-invoice-table.component';
import { ClientPatientTableComponent } from './client-patient-table/client-patient-table.component';
import { Component, ViewChild } from '@angular/core';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { AdjustCreditDialogComponent } from '../../shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { IFormValidation } from 'src/shared/APP_HELPER';
import { CreditTrailListComponent } from './credit-trail-list/credit-trail-list.component';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './client-detail.component.less',
  ],
})
export class ClientDetailComponent extends BaseDetailViewComponent {
  @ViewChild('clientpatienttable') clientpatienttable:
    | ClientPatientTableComponent
    | undefined;
  @ViewChild('clientbillinginvoicetable') clientbillinginvoicetable:
    | ClientBillingInvoiceTableComponent
    | undefined;
  @ViewChild('credittraillist') credittraillist:
    | CreditTrailListComponent
    | undefined;
  @ViewChild('adjustcreditdialog') adjustcreditdialog:
    | AdjustCreditDialogComponent
    | undefined;

  ModelName: string = 'Client';
  headerTitle: string = 'Client';
  displayMember: string = 'Name';

  _menuItem_AdjustCredit: AdminLTEMenuItem = {
    label: 'Adjust Credit',
    icon: 'fa fa-save',
    class: 'text-info',
    visible: true,
  };

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.ID > 0) {
      this.rightDropDownItems.push({
        label: 'Create Patient',
        visible: true,
        name: 'createpatient',
      });

      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice',
      });
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Adjust Credit') {
      var ID_Client = this.CurrentObject.ID;

      if (this.adjustcreditdialog) {
        await this.adjustcreditdialog.show(ID_Client);
        this.loadRecord();
      }
    }
  }

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (
      this.CurrentObject.ID == 0 ||
      this.CurrentObject.ID == -1 ||
      this.CurrentObject.ID == null
    )
      return;

    if (event.item.name == 'createpatient') {
      this.customNavigate(['Patient', -1], {
        ID_Client: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'createbillinginvoice') {
      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID,
      });
    }
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.ID > 0) {
      //this.menuItems.push(this._menuItem_AdjustCredit);
    }
  }

  DetailView_onLoad() {
    if (this.clientpatienttable != undefined) {
      this.clientpatienttable.load(this.CurrentObject.ID);
    }

    if (this.clientbillinginvoicetable != undefined) {
      this.clientbillinginvoicetable.load(this.CurrentObject.ID);
    }

    if (this.credittraillist != undefined) {
      this.credittraillist.load(this.CurrentObject.ID);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];
    var ContactNumber = this.CurrentObject.ContactNumber;

    if (ContactNumber == null || ContactNumber == undefined) ContactNumber = '';

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    this.CurrentObject.Name = this.CurrentObject.Name.trimStart();
    this.CurrentObject.Name = this.CurrentObject.Name.trimEnd();

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    if (ContactNumber.length > 0) {
      if (!this.validationService.validatePHMobileNumber(ContactNumber)) {
        validations.push({ message: 'Invalid Mobile Number' });
      }
    }

    var validateObj = await this.execSP(
      'pValidateClient',
      {
        ID_Client: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true && validateObj.isWarning != true) {
      validations.push({ message: validateObj.message });
    }

    return Promise.resolve(validations);
  }
}
