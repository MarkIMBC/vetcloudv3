import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'client-billing-invoice-table',
  templateUrl: './client-billing-invoice-table.component.html',
  styleUrls: ['./client-billing-invoice-table.component.less'],
})
export class ClientBillingInvoiceTableComponent implements OnInit {
  @Input() CurrentObject: any;
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Create',
      icon: 'fa fa-plus',
      class: 'text-default',
      visible: true,
    },
  ];

  TotalRemainingAmount: number = 0;
  TotalBillingAmount: number = 0;
  BillingCount: number = 0;

  private ID_Client: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  initmenubar_OnClick(e: any) {
    if (this.ID_Client == 0 || this.ID_Client == -1 || this.ID_Client == null)
      return;

    if (e.item.label == 'Create') {
      var routeLink = ['BillingInvoice', -1];
      var configOptions = {
        ID_Client: this.ID_Client,
      };

      GeneralfxService.customNavigate(
        this.router,
        this.cs,
        routeLink,
        configOptions
      );
    }
  }

  load(ID_Client: number) {
    this.ID_Client = ID_Client;

    var sql = `
        SELECT  ID,
            Date,
            Code,
            NetAmount,
            RemainingAmount,
            Name_FilingStatus,
            Status,
            Payment_Name_FilingStatus
        FROM dbo.vBillingInvoice
        WHERE
          ID_Client = ${this.ID_Client} AND
          ID_Company = ${this.currentUser.ID_Company} AND
          ID_FilingStatus IN (${FilingStatusEnum.Filed}, ${FilingStatusEnum.Approved})
        ORDER BY
          Date DESC
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;

      this.dataSource.forEach((record: any) => {
        this.TotalBillingAmount += record['NetAmount'];
        this.TotalRemainingAmount += record['RemainingAmount'];
      });

      this.BillingCount = this.dataSource.length;
    });
  }

  onRowClick(rowData: any) {
    var routeLink = ['BillingInvoice', rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {});
  }
}
