import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'client-patient-table',
  templateUrl: './client-patient-table.component.html',
  styleUrls: ['./client-patient-table.component.less'],
})
export class ClientPatientTableComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Patient',
      icon: 'fa fa-plus',
      class: 'text-default',
      visible: true,
    },
  ];

  private ID_Client: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  load(ID_Client: number) {
    this.ID_Client = ID_Client;

    var sql = `SELECT
                    ID,
                    Name,
                    Name_Gender,
                    Species
                FROM dbo.vPatient
                WHERE
                  ID_Client = ${this.ID_Client} AND
                  IsActive = 1
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;
    });
  }

  onRowClick(rowData: any) {
    var routeLink = ['Patient', rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {});
  }

  initmenubar_OnClick(e: any) {
    if (this.ID_Client == 0 || this.ID_Client == -1 || this.ID_Client == null)
      return;

    if (e.item.label == 'Add Patient') {
      var routeLink = ['Patient', -1];

      GeneralfxService.customNavigate(this.router, this.cs, routeLink, {
        ID_Client: this.ID_Client,
      });
    }
  }
}
