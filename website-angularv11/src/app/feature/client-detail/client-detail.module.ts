import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientDetailRoutingModule } from './client-detail-routing.module';
import { ClientDetailComponent } from './client-detail.component';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClientPatientTableComponent } from './client-patient-table/client-patient-table.component';
import { ClientBillingInvoiceTableComponent } from './client-billing-invoice-table/client-billing-invoice-table.component';
import { AdjustCreditDialogComponent } from '../../shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { CreditTrailListComponent } from './credit-trail-list/credit-trail-list.component';


@NgModule({
  declarations: [ClientDetailComponent, ClientPatientTableComponent, ClientBillingInvoiceTableComponent, CreditTrailListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ClientDetailRoutingModule
  ],
})
export class ClientDetailModule { }
