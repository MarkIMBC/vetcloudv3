import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferenceLinkViewRoutingModule } from './reference-link-view-routing.module';
import { ReferenceLinkViewComponent } from './reference-link-view.component';


@NgModule({
  declarations: [ReferenceLinkViewComponent],
  imports: [
    CommonModule,
    ReferenceLinkViewRoutingModule
  ]
})
export class ReferenceLinkViewModule { }
