import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientWellnessListComponent } from './patient-wellness-list.component';

const routes: Routes = [{ path: '', component: PatientWellnessListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientWellnessListRoutingModule { }
