import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientWellnessListRoutingModule } from './patient-wellness-list-routing.module';
import { PatientWellnessListComponent } from './patient-wellness-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [PatientWellnessListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientWellnessListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientWellnessListModule { }
