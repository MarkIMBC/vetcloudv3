import { FilingStatusEnum, Item_DTO, PropertyTypeEnum } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'app-patient-confinement-list',
  templateUrl: './patient-confinement-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-confinement-list.component.less'
  ],
})
export class PatientConfinementListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'PatientConfinementListComponent';

  headerTitle: string = 'Confinement';

  InitCurrentObject: any = {
    Name: '',
    ID_FilingStatus: FilingStatusEnum.Confined,
    Name_FilingStatus: 'Confined'
  }

  dataSource: any[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Confinement',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  ID_FilingStatus_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name FROM tFilingStatus WHERE ID IN (
        ${FilingStatusEnum.Pending},
        ${FilingStatusEnum.Confined},
        ${FilingStatusEnum.Discharged},
        ${FilingStatusEnum.Cancelled}
      )`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `
          SELECT *
          FROM vPatient_Confinement_Listview
          WHERE
            ID_Company = ${this.currentUser.ID_Company}
            ${filterString} AND
            1 = 1
       `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject['Code']) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject['Name_Client']) {

      if (filterString.length > 0) filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject['Name_Patient']) {

      if (filterString.length > 0) filterString += ' AND '
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject['ID_FilingStatus']) {

      if (this.CurrentObject['ID_FilingStatus'] == FilingStatusEnum.Pending) {

        if (filterString.length > 0) filterString += " AND ";

        filterString +=  " (";
        filterString += `(ISNULL(ID_FilingStatus, 0) IN (${FilingStatusEnum.Confined}))`;
        filterString +=  " OR ";
        filterString += `(ISNULL(ID_FilingStatus, 0) IN (${FilingStatusEnum.Confined})`;
        filterString +=  " AND ";
        filterString += `ISNULL(BillingInvoice_ID_FilingStatus, 0) NOT IN (0, ${FilingStatusEnum.Cancelled}, ${FilingStatusEnum.FullyPaid}, ${FilingStatusEnum.Done}))`;
        filterString +=  ") ";
      } else {

        if (filterString.length > 0) filterString += " AND ";
        filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
      }
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['Confinement', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Confinement', -1], {});
  }
}
