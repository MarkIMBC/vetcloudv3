import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { PatientConfinementListRoutingModule } from './patient-confinement-list-routing.module';
import { PatientConfinementListComponent } from './patient-confinement-list.component';


@NgModule({
  declarations: [PatientConfinementListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientConfinementListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientConfinementListModule { }
