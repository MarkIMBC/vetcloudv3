import { ItemTypeEnum } from '../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { UserRolesTableComponent } from './user-roles-table/user-roles-table.component';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './user-detail.component.less',
  ],
})
export class UserDetailComponent extends BaseDetailViewComponent {
  @ViewChild('useruserrolestable')
  useruserrolestable: UserRolesTableComponent | undefined;

  ModelName: string = 'User';
  headerTitle: string = 'User';
  displayMember: string = 'Name';

  ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive != true) return;

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Save);
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  useruserrolestable_onModelChanged() {}

  DetailView_onLoad() {
    if (
      this.CurrentObject.User_Roles == null ||
      this.CurrentObject.User_Roles == undefined
    ) {
      this.CurrentObject.User_Roles = [];
    }

    var listViewOptionID_Employee =
      this.ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_Employee != undefined) {
      listViewOptionID_Employee.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tEmployee
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;
    }

    if (this.useruserrolestable) {
      this.useruserrolestable.loadMenuByCurrentObject(this.CurrentObject);
    }
  }
}
