import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PatientReferralDetailRoutingModule } from './patient-referral-detail-routing.module';
import { PatientReferralDetailComponent } from './patient-referral-detail.component';
import { ReferralTypeCheckBoxesComponent } from './referral-type-check-boxes/referral-type-check-boxes.component';


@NgModule({
  declarations: [PatientReferralDetailComponent, ReferralTypeCheckBoxesComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientReferralDetailRoutingModule
  ]
})
export class PatientReferralDetailModule { }

