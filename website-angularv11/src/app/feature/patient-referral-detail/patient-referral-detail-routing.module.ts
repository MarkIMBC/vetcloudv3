import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientReferralDetailComponent } from './patient-referral-detail.component';

const routes: Routes = [{ path: '', component: PatientReferralDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientReferralDetailRoutingModule { }
