import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralDetailComponent } from './patient-referral-detail.component';

describe('PatientReferralDetailComponent', () => {
  let component: PatientReferralDetailComponent;
  let fixture: ComponentFixture<PatientReferralDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
