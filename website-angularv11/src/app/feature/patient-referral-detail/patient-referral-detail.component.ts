import { FilingStatusEnum, FilterCriteriaType, IFormValidation, ItemTypeEnum, ReferralType_DTO } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { ReferralTypeCheckBoxesComponent } from './referral-type-check-boxes/referral-type-check-boxes.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-patient-referral-detail',
  templateUrl: './patient-referral-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-referral-detail.component.less',
  ],
})
export class PatientReferralDetailComponent extends BaseDetailViewComponent {

  public referralTypeList: ReferralType_DTO[] = [];

  _menuItem_Cancel: AdminLTEMenuItem = {
    label: 'Cancel',
    icon: 'fa fa-times',
    class: 'text-danger',
    visible: true,
  };

  _menuItem_UndoCancel: AdminLTEMenuItem = {
    label: 'Undo Cancel',
    icon: 'fa fa-undo',
    class: 'text-warning',
    visible: true,
  };

  _menuItem_Report: AdminLTEMenuItem = {
    label: 'Report',
    icon: 'far fa-file',
    class: 'text-primary',
    visible: true,
    items: [
      {
        label: 'Referral Report',
        name: 'referralreport',
        icon: 'far fa-file',
        class: 'text-primary',
        visible: true,
      },
    ],
  };

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  @ViewChild("referraltypecheckboxes") referraltypecheckboxes: ReferralTypeCheckBoxesComponent | undefined

  ModelName: string = 'PatientReferral';
  headerTitle: string = 'Patient Referral';
  displayMember: string = 'Code';

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._menuItem_Cancel);
    }
    else if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._menuItem_UndoCancel);
    }

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled)
    {
      this.addMenuItem(this._menuItem_Report);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      this.doCancel();
    } else if (e.item.name == 'referralreport') {
      this.customNavigate(['Report'], {
        ReportName: 'ReferralReport',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }

  doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before canceling ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
      'pCancelPatientReferral',
      {
        IDs_PatientReferral: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };
  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  DetailView_onLoad() {

    if (this.CurrentObject.PatientReferral_ReferralType == null || this.CurrentObject.PatientReferral_ReferralType == undefined) {
      this.CurrentObject.PatientReferral_ReferralType = [];
    }

    //this.loadReferralType();
    if (this.referraltypecheckboxes) {
      this.referraltypecheckboxes.loadReferralType(this.CurrentObject.PatientReferral_ReferralType);
    }


    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }

    var listViewOptionAttendingPhysician_ID_Employee = this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vPatientReferral_AttendingVeterinarianDropdown
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  referraltypecheckboxes_onChanged(e: any) {

    this.CurrentObject.PatientReferral_ReferralType = e;
    console.log(e);
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }
}
