import { IControlModelArg, ReferralType_DTO, ReferralTypeCheckBoxObject } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { PatientReferral_ReferralType, ReferralType } from 'src/shared/APP_MODELS';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'referral-type-check-boxes',
  templateUrl: './referral-type-check-boxes.component.html',
  styleUrls: ['./referral-type-check-boxes.component.less']
})
export class ReferralTypeCheckBoxesComponent implements OnInit {
  tempID: number = 0;
  isOtherReferralTypeSelected: boolean = false;

  @Input() MainCurrentObject: any = {};
  @Input() patientReferralReferralTypes: PatientReferral_ReferralType[] = [];
  @Output() onChanged = new EventEmitter<any>();

  public CurrentObject: any = {};
  public selectedReferralTypeIDs: number[] = [];

  public referralTypeList: ReferralType_DTO[] = [];

  constructor(
    protected ds: DataService,
    protected cs: CrypterService
  ) { }

  ngOnInit(): void {
  }

  async loadReferralType(_patientReferralReferralTypes: PatientReferral_ReferralType[]) {
    this.isOtherReferralTypeSelected = false;
    this.patientReferralReferralTypes = _patientReferralReferralTypes;
    this.referralTypeList = [];

    var sql = `
        /*encryptsqlstart*/
        SELECT  *
        FROM dbo.tReferralType
        WHERE
          IsActive = 1
        /*encryptsqlend*/
    `;

    sql = this.cs.encrypt(sql);

    this.referralTypeList = await this.ds.query<ReferralType_DTO>(sql);

    this.referralTypeList.forEach((referralType: ReferralType_DTO) => {

      var name = "IsReferralType_" + referralType.ID + "_Checked";
      this.CurrentObject[name] = false;
      referralType.CheckBoxName = name;
    });

    this.selectedReferralTypeIDs = [];

    if (this.patientReferralReferralTypes == null || this.patientReferralReferralTypes == undefined) {
      this.patientReferralReferralTypes = [];
    }

    this.patientReferralReferralTypes.forEach((patientReferralReferralType: PatientReferral_ReferralType) => {

      if (patientReferralReferralType.ID_ReferralType) {
        var ID_ReferralType = patientReferralReferralType.ID_ReferralType;
        var name = "IsReferralType_" + ID_ReferralType + "_Checked";
        this.CurrentObject[name] = true;
        this.selectedReferralTypeIDs.push(ID_ReferralType);

        if (ID_ReferralType == 6) this.isOtherReferralTypeSelected = true;
      }

    });

  }

  ReferralTypeOthers_onModelChanged(e: IControlModelArg) {
    this.MainCurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.MainCurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }
  }

  checkbox_onModelChanged(referralType: ReferralType, e: IControlModelArg) {

    if (referralType.ID) {
      var index = this.selectedReferralTypeIDs.indexOf(referralType.ID);

      if (index === -1) {
        this.selectedReferralTypeIDs.push(referralType.ID);
        if (referralType.ID == 6) this.isOtherReferralTypeSelected = true;
      } else {
        this.selectedReferralTypeIDs.splice(index, 1);

        if (referralType.ID == 6) this.MainCurrentObject.ReferralTypeOthers = "";
      }

      this.patientReferralReferralTypes = [];

      this.selectedReferralTypeIDs.forEach((id: number) => {

        this.tempID--;

        this.patientReferralReferralTypes.push({
          ID: this.tempID,
          ID_ReferralType: id
        });

      });

    }

    this.onChanged.emit(this.patientReferralReferralTypes);
  }

}
