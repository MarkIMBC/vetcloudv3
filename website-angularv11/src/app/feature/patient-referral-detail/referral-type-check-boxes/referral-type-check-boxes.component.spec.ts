import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralTypeCheckBoxesComponent } from './referral-type-check-boxes.component';

describe('ReferralTypeCheckBoxesComponent', () => {
  let component: ReferralTypeCheckBoxesComponent;
  let fixture: ComponentFixture<ReferralTypeCheckBoxesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferralTypeCheckBoxesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralTypeCheckBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
