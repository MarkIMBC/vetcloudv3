import { Patient_Wellness_DTO, VeterinaryHealthCertificate_DTO } from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';

@Component({
  selector: 'app-veterinary-health-certificate-list',
  templateUrl: './veterinary-health-certificate-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './veterinary-health-certificate-list.component.less'
  ],
})
export class VeterinaryHealthCertificateListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'VeterinaryHealthCertificateListComponent';

  headerTitle: string = 'Veterinary Health Certificate';

  InitCurrentObject: any = {

    Code: '',
    Name_Client: '',
    Name_Patient: '',
    Name_FilingStatus: '',
  }

  dataSource: VeterinaryHealthCertificate_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vVeterinaryHealthCertificate_ListView
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";


    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_Wellness_DTO) {

    this.customNavigate(['VeterinaryHealthCertificate', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['VeterinaryHealthCertificate', -1], {});
  }
}
