import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { VeterinaryHealthCertificateListRoutingModule } from './veterinary-health-certificate-list-routing.module';
import { VeterinaryHealthCertificateListComponent } from './veterinary-health-certificate-list.component';


@NgModule({
  declarations: [VeterinaryHealthCertificateListComponent],
  imports: [
    CommonModule,
    SharedModule,
    VeterinaryHealthCertificateListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class VeterinaryHealthCertificateListModule { }
