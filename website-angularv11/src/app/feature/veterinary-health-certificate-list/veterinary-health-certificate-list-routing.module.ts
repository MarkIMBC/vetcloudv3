import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VeterinaryHealthCertificateListComponent } from './veterinary-health-certificate-list.component';

const routes: Routes = [{ path: '', component: VeterinaryHealthCertificateListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeterinaryHealthCertificateListRoutingModule { }
