import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InactiveSMSSendingDetailComponent } from './inactive-smssending-detail.component';

describe('InactiveSMSSendingDetailComponent', () => {
  let component: InactiveSMSSendingDetailComponent;
  let fixture: ComponentFixture<InactiveSMSSendingDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InactiveSMSSendingDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InactiveSMSSendingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
