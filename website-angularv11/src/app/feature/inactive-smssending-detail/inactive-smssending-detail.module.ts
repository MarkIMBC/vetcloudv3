import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InactiveSMSSendingDetailRoutingModule } from './inactive-smssending-detail-routing.module';
import { InactiveSMSSendingDetailComponent } from './inactive-smssending-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [InactiveSMSSendingDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    InactiveSMSSendingDetailRoutingModule
  ]
})
export class InactiveSMSSendingDetailModule { }
