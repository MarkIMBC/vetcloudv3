import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InactiveSMSSendingDetailComponent } from './inactive-smssending-detail.component';

const routes: Routes = [{ path: '', component: InactiveSMSSendingDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InactiveSMSSendingDetailRoutingModule { }
