import { IFormValidation, ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-inactive-smssending-detail',
  templateUrl: './inactive-smssending-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './inactive-smssending-detail.component.less',
  ],
})
export class InactiveSMSSendingDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'InactiveSMSSending';
  headerTitle: string = 'Inactive SMS Sending';

  displayMember: string = 'Code';

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {
    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (!this.CurrentObject.Date) {
      validations.push({
        message: `Date is required.`,
      });
    }

    if (!this.CurrentObject.Comment) {
      validations.push({
        message: `Comment is required.`,
      });
    }

    return Promise.resolve(validations);
  }
}
