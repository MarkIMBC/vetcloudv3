import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { FilterCriteriaType, IFilterFormValue, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-expenses',
  templateUrl: './report-expenses.component.html',
  styleUrls: ['./report-expenses.component.less']
})
export class ReportExpensesComponent  extends ReportComponent {

  configOptions: any = {
    ReportName: 'EXPENSESREPORT'
  }


  onLoad() {

  }

  protected getFilterValues(filterValues: IFilterFormValue[]): IFilterFormValue[] {

    if (this.CurrentObject['DateStart_BillingInvoice']) {

      var value = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {

        var valueEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        filterValues.push({
          dataField: "Date",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            valueEnd
          ]
        });
      } else {

        filterValues.push({
          dataField: "Date",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            value
          ]
        });
      }
    } else {

      filterValues.push({
        dataField: "Date",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [
          moment().format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD")
        ]
      });
    }

    if (this.CurrentObject['Name']) {

      filterValues.push({
        dataField: "Name",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name']
      });
    }

    if (this.CurrentObject['Name_ExpenseCategory']) {

      filterValues.push({
        dataField: "Name_ExpenseCategory",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_ExpenseCategory']
      });
    }

    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(filterValues, 'dataField', 'Date');
    if (index > -1) {

      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Date From ${moment(dateStart).format("MM/DD/YYYY")} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (caption.length > 0) {

      filterValues.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }

}
