import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportExpensesRoutingModule } from './report-expenses-routing.module';
import { ReportExpensesComponent } from './report-expenses.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportExpensesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportExpensesRoutingModule
  ]
})
export class ReportExpensesModule { }
