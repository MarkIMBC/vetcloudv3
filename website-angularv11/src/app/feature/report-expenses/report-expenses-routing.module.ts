import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportExpensesComponent } from './report-expenses.component';

const routes: Routes = [{ path: '', component: ReportExpensesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportExpensesRoutingModule { }
