import { Component, OnInit, ViewChild } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  UserAuthenticationService,
  TokenSessionFields,
} from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  IFormValidation,
  MergeClient,
  AlignEnum,
} from 'src/shared/APP_HELPER';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'merge-client-dialog',
  templateUrl: './merge-client-dialog.component.html',
  styleUrls: ['./merge-client-dialog.component.less'],
})
export class MergeClientDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();
  SourceClient: any = {};
  CurrentObject: MergeClient = new MergeClient();

  private ID: number = 0;

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
        {
          name: 'Name',
          caption: 'Client',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
      ],
    },
  };

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(_ID?: number): Promise<any> {
    this.CurrentObject = new MergeClient();
    this.CurrentObject.ID_Client_Source = _ID;
    this.CurrentObject.ID_Client_Destination = undefined;

    this.loadSourceClient();

    var modalDialog = this.modalDialog;
    var clientClientListOption = this.ID_Client_LookupboxOption.listviewOption;

    if (clientClientListOption != undefined) {
      clientClientListOption.sql = `/*encryptsqlstart*/
                                      SELECT
                                        ID, Code, Name
                                      FROM vActiveClient
                                      WHERE
                                        ID_Company = ${this.currentUser.ID_Company} AND
                                        ID NOT IN (${_ID})
                                    /*encryptsqlend*/`;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();

        resolve(null);
      }
    });

    return promise;
  }

  private async loadSourceClient() {
    var queryString = this.cs.encrypt(`
    /*encryptsqlstart*/
      SELECT
          ID,
          Name
      FROM vActiveClient
      WHERE
          ID IN (${this.CurrentObject.ID_Client_Source})
    /*encryptsqlend*/
  `);

    var records = await this.ds.query<any>(queryString);
    this.SourceClient = records[0];
  }

  ID_Client_Destination_onModelChanged(e: IControlModelArg) {
    this.CurrentObject.ID_Client_Destination = e.value;
  }

  async btnSubmit_onClick() {
    this.loading = true;

    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject != undefined) {
      if (this.CurrentObject.ID_Client_Destination == null) {
        validationsAppForm.push({ message: 'Destination is required.' });
      }
    }

    validationsAppForm.forEach((validation) => {
      this.toastService.warning(validation.message);
    });

    this.loading = false;

    if (validationsAppForm.length > 0) return;

    this.mergeClient().then(() => {
      if (this.modalDialog != undefined) {
        this.modalDialog.close();
      }
    });
  }

  async mergeClient(): Promise<any> {
    var item = this.CurrentObject;

    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pDoMergeClientRecord',
        {
          From_ID_Client: item.ID_Client_Source,
          To_ID_Client: item.ID_Client_Destination,
          ID_UserSession: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(`Client successfully merge.`);
        res(obj);
      } else {
        this.toastService.danger(obj.message);
        rej(obj);
      }

      this.loading = false;
    });
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }
}
