import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { ClientRoutingModule } from './client-list-routing.module';
import { ClientListComponent } from './client-list.component';
import { DataService } from 'src/app/core/data.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { MergeClientDialogComponent } from './merge-client-dialog/merge-client-dialog.component';



@NgModule({
  declarations: [ClientListComponent, MergeClientDialogComponent],
  imports: [
    CommonModule,
    SharedModule,
    ClientRoutingModule,
    ModalModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ClientModule { }
