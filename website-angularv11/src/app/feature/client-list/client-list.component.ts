import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  Client_DTO,
  PropertyTypeEnum,
  MergeClient,
  AlignEnum,
} from 'src/shared/APP_HELPER';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { MergeClientDialogComponent } from './merge-client-dialog/merge-client-dialog.component';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './client-list.component.less',
  ],
})
export class ClientListComponent extends BaseListViewComponent {
  @ViewChild('mergeclientdialog') mergeclientdialog:
    | MergeClientDialogComponent
    | undefined;

  CustomComponentName: string = 'ClientListComponent';

  headerTitle: string = 'Client';

  InitCurrentObject: any = {
    Code: '',
    Name: '',
    ContactNumbers: '',
    IsDeleted: false,
    LastAttendingPhysician_Name_Employee: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Code,
                    Name,
                    ContactNumbers,
                    DateCreated,
                    LastAttendingPhysician_Name_Employee,
                    TotalRemainingAmount
            FROM dbo.vClient_ListView
            /*encryptsqlend*/
            WHERE
            ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.IsDeleted == null)
      this.CurrentObject.IsDeleted = false;

    this.initializeValue(this.CurrentObject['Code'], PropertyTypeEnum.String);
    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    this.initializeValue(this.CurrentObject['Name'], PropertyTypeEnum.String);
    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    this.initializeValue(
      this.CurrentObject['ContactNumbers'],
      PropertyTypeEnum.String
    );
    if (this.CurrentObject.ContactNumbers.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ContactNumbers LIKE '%${this.CurrentObject.ContactNumbers}%'`;
    }
    if (this.CurrentObject.LastAttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `LastAttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.LastAttendingPhysician_Name_Employee}%'`;
    }
    if (filterString.length > 0) filterString += ' AND ';
    filterString += `IsActive = '${this.CurrentObject.IsDeleted ? '0' : '1'}'`;

    return filterString;
  }

  Row_OnClick(rowData: Client_DTO) {
    this.customNavigate(['Client', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Client', -1], {});
  }

  RowCellAction_CreateAppointment_OnClick(rowData: Client_DTO) {
    this.customNavigate(['PatientAppointment', -1], {
      ID_Client: rowData.ID,
    });
  }

  RowCellAction_CreateBilling_OnClick(rowData: Client_DTO) {
    this.customNavigate(['BillingInvoice', -1], {
      ID_Client: rowData.ID,
    });
  }

  RowCellAction_CreatePet_OnClick(rowData: Client_DTO) {
    this.customNavigate(['Patient', -1], {
      ID_Client: rowData.ID,
    });
  }

  async RowBtnActionMergeClientModal_onClick(rowData: Client_DTO) {
    if (this.mergeclientdialog != undefined) {
      await this.mergeclientdialog.show(rowData.ID);
      this.loadRecords();
    }
  }
}
