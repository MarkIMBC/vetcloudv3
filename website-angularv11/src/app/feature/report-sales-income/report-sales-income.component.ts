import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFilterFormValue,
  ItemTypeEnum,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-sales-income',
  templateUrl: './report-sales-income.component.html',
  styleUrls: ['./report-sales-income.component.less'],
})
export class ReportSalesIncomeComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'SALESINCOMEREPORT',
  };

  CurrentObject: any = {
    AttendingPhysician_ID_Employee: 0,
  };

  ID_ItemType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  onLoad() {
    var listViewOption_ID_ItemType =
      this.ID_ItemType_LookupboxOption.listviewOption;

    if (listViewOption_ID_ItemType != undefined) {
      listViewOption_ID_ItemType.sql = `
      /*encryptsqlstart*/
      SELECT
        ID,
        Name
      FROM tItemType
      WHERE ID IN (
        ${ItemTypeEnum.Inventoriable},
        ${ItemTypeEnum.Service}
      )
    /*encryptsqlend*/`;
    }

    var listViewOptionAttendingPhysician_ID_Employee =
      this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }
  }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['DateStart_BillingInvoice']) {
      var value = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {
        var valueEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        filterValues.push({
          alias: 'schema1',
          dataField: 'Date_BillingInvoice',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [value, valueEnd],
        });

        filterValues.push({
          alias: 'schema2',
          dataField: 'Date_BillingInvoice',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [value, valueEnd],
        });
      } else {
        filterValues.push({
          alias: 'schema1',
          dataField: 'Date_BillingInvoice',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [value, value],
        });

        filterValues.push({
          alias: 'schema2',
          dataField: 'Date_BillingInvoice',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [value, value],
        });
      }
    }

    if (this.CurrentObject['AttendingPhysician_ID_Employee']) {
      filterValues.push({
        alias: 'schema1',
        dataField: 'AttendingPhysician_ID_Employee',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['AttendingPhysician_ID_Employee'],
      });
    }

    if (this.CurrentObject['Name_Client']) {
      filterValues.push({
        alias: 'schema1',
        dataField: 'Name_Client',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Client'],
      });
    }



    if (
      this.CurrentObject['ID_ItemType'] != undefined &&
      this.CurrentObject['ID_ItemType'] != null &&
      this.CurrentObject['ID_ItemType'] != 0
    ) {
      var value = this.CurrentObject['ID_ItemType'];

      filterValues.push({
        alias: 'schema1',
        dataField: 'ID_ItemType',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });

      filterValues.push({
        alias: 'schema2',
        dataField: 'ID_ItemType',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        alias: 'schema1',
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });

      filterValues.push({
        alias: 'schema2',
        dataField: 'ItemString',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }

    /***********Default Values**************/
    filterValues.push({
      alias: 'schema2',
      dataField: 'ID_Company',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    if (!this.CurrentObject['DateEnd_BillingInvoice']) {
      filterValues.push({
        alias: 'schema1',
        dataField: 'Date_BillingInvoice',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
      });

      filterValues.push({
        alias: 'schema2',
        dataField: 'Date_BillingInvoice',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
      });
    }
    /***********************************************/

    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(
      filterValues,
      'dataField',
      'Date_BillingInvoice'
    );
    if (index > -1) {
      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Bill Date From ${moment(dateStart).format(
        'MM/DD/YYYY'
      )} To ${moment(dateEnd).format('MM/DD/YYYY')}<br/>`;
    }

    if (this.CurrentObject['AttendingPhysician_Name_Employee']) {
      if (this.CurrentObject.AttendingPhysician_Name_Employee.length) {
        caption += `Attending Vet: ${this.CurrentObject.AttendingPhysician_Name_Employee}<br>`;
      }
    }

    if (caption.length > 0) {
      filterValues.push({
        dataField: 'Header_CustomCaption',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }
    return filterValues;
  }
}
