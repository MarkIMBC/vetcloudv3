import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportSalesIncomeComponent } from './report-sales-income.component';

const routes: Routes = [{ path: '', component: ReportSalesIncomeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportSalesIncomeRoutingModule { }
