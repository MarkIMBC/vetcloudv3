import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PatientLodgingDetailRoutingModule } from './patient-lodging-detail-routing.module';
import { PatientLodgingDetailComponent } from './patient-lodging-detail.component';


@NgModule({
  declarations: [PatientLodgingDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientLodgingDetailRoutingModule
  ]
})
export class PatientLodgingDetailModule { }

