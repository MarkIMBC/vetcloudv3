import { IFormValidation, ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'app-patient-lodging-detail',
  templateUrl: './patient-lodging-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-lodging-detail.component.less',
  ],
})
export class PatientLodgingDetailComponent extends BaseDetailViewComponent {
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;
  ModelName: string = 'Patient_Lodging';
  headerTitle: string = 'Patient Lodging';
  minCheckOut: string = '9999-12-31T23:59';

  displayMember: string = 'Code';

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.ID > 0) {
      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice',
      });
    }
  }

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'createbillinginvoice') {
      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        ID_Patient_Lodging: this.CurrentObject.ID,
      });
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };
  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  DetailView_onLoad() {
    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    } else if (e.name == 'DateCheckIn') {
      this.minCheckOut;
      this.compute();
    } else if (e.name == 'DateCheckOut') {
      this.compute();
    } else if (e.name == 'RateAmount') {
      this.compute();
    } else if (e.name == 'AdvancedPaymentAmount') {
      this.compute();
    } else if (e.name == 'PaymentAmount') {
      this.compute();
    }
  }

  compute() {
    var now = moment(this.CurrentObject.DateCheckOut); //todays date
    var end = moment(this.CurrentObject.DateCheckIn); // another date
    var duration = moment.duration(now.diff(end));

    var HourCount: number = duration.asHours();
    var RateAmount: number = this.CurrentObject.RateAmount;
    var AdvancedPaymentAmount: number =
      this.CurrentObject.AdvancedPaymentAmount;
    var PaymentAmount: number = this.CurrentObject.PaymentAmount;
    var RemainingAmount: number = this.CurrentObject.RemainingAmount;
    var TotalAmount: number = 0;
    var ChangeAmount: number = 0;

    if (isNaN(HourCount)) HourCount = 0;
    if (isNaN(AdvancedPaymentAmount)) AdvancedPaymentAmount = 0;
    if (isNaN(RemainingAmount)) RemainingAmount = 0;

    HourCount = GeneralfxService.roundOffDecimal(HourCount);
    AdvancedPaymentAmount = GeneralfxService.roundOffDecimal(
      AdvancedPaymentAmount
    );

    TotalAmount = HourCount * RateAmount;
    TotalAmount = GeneralfxService.roundOffDecimal(TotalAmount);

    RemainingAmount = TotalAmount - (AdvancedPaymentAmount + PaymentAmount);
    RemainingAmount = GeneralfxService.roundOffDecimal(RemainingAmount);

    ChangeAmount = PaymentAmount - TotalAmount;
    ChangeAmount = GeneralfxService.roundOffDecimal(ChangeAmount);

    if (RemainingAmount < 0) RemainingAmount = 0;
    if (ChangeAmount < 0) ChangeAmount = 0;

    this.CurrentObject.HourCount = HourCount;
    this.CurrentObject.RateAmount = RateAmount;
    this.CurrentObject.AdvancedPaymentAmount = AdvancedPaymentAmount;
    this.CurrentObject.TotalAmount = TotalAmount;
    this.CurrentObject.PaymentAmount = PaymentAmount;
    this.CurrentObject.RemainingAmount = RemainingAmount;
    this.CurrentObject.ChangeAmount = ChangeAmount;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;
    var ID_Item = this.CurrentObject.ID_Item;
    var RateAmount: number = this.CurrentObject.RateAmount;
    var AdvancedPaymentAmount: number =
      this.CurrentObject.AdvancedPaymentAmount;
    var PaymentAmount: number = this.CurrentObject.PaymentAmount;
    var PayableAmount: number = this.CurrentObject.PayableAmount;

    if (
      this.CurrentObject.ID_Client == undefined ||
      this.CurrentObject.ID_Client == null
    )
      ID_Client = 0;
    if (
      this.CurrentObject.ID_Patient == undefined ||
      this.CurrentObject.ID_Patient == null
    )
      ID_Patient = 0;
    if (
      this.CurrentObject.ID_Item == undefined ||
      this.CurrentObject.ID_Item == null
    )
      ID_Item = 0;

    if (ID_Client == 0) {
      validations.push({
        message: 'Client is required.',
      });
    }

    if (ID_Patient == 0) {
      validations.push({
        message: 'Patient is required.',
      });
    }

    return Promise.resolve(validations);
  }
}
