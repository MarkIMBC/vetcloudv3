import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientLodgingDetailComponent } from './patient-lodging-detail.component';

const routes: Routes = [{ path: '', component: PatientLodgingDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientLodgingDetailRoutingModule { }
