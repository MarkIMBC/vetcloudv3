import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientVaccinationDetailComponent } from './patient-vaccination-detail.component';

const routes: Routes = [{ path: '', component: PatientVaccinationDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientVaccinationDetailRoutingModule { }
