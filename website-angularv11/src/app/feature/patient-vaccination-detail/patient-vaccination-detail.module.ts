
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PatientVaccinationDetailRoutingModule } from './patient-vaccination-detail-routing.module';
import { PatientVaccinationDetailComponent } from './patient-vaccination-detail.component';
import { ScheduleListComponent } from './schedule-list/schedule-list.component';

@NgModule({
  declarations: [PatientVaccinationDetailComponent, ScheduleListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientVaccinationDetailRoutingModule
  ]
})
export class PatientVaccinationDetailModule { }
