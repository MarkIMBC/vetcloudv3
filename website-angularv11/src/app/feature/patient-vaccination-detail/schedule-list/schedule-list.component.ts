import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Patient_Vaccination_Schedule } from 'src/shared/APP_MODELS';

@Component({
  selector: 'schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.less'],
})
export class ScheduleListComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() Schedules: Patient_Vaccination_Schedule[] = [];

  tempID: number = 0;

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Schedule',
      icon: 'fa fa-calendar',
      visible: true,
    },
  ];

  async menuItems_OnClick(arg: any) {
    var _ = this;
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Schedule') {
      var obj: Patient_Vaccination_Schedule = {
        ID: _.tempID,
      };

      _.tempID--;

      obj.ID = _.tempID;

      if (this.Schedules == null) this.Schedules = [];

      this.Schedules.push(obj);
    }
  }

  async BtnDeleteSchedule_OnClick(ScheduleList: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.Schedules,
      'ID',
      ScheduleList.ID + ''
    );

    this.Schedules.splice(index, 1);
  }

  schedule_onModelChanged(ScheduleList: any, e: IControlModelArg) {
    ScheduleList[e.name] = e.value;
  }
}
