import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientVaccinationDetailComponent } from './patient-vaccination-detail.component';

describe('PatientVaccinationDetailComponent', () => {
  let component: PatientVaccinationDetailComponent;
  let fixture: ComponentFixture<PatientVaccinationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientVaccinationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientVaccinationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
