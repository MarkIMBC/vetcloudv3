import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  ConfimentItemServiceModeEnum,
  FilingStatusEnum,
  IControlModelArg,
  ItemTypeEnum,
  Patient_Confinement_ItemsServices_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'confinement-items-services-table',
  templateUrl: './confinement-items-services-table.component.html',
  styleUrls: ['./confinement-items-services-table.component.less'],
})
export class ConfinementItemsServicesTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;
  TotalAmount: number = 0;
  ItemTypeEnum_Inventoriable: ItemTypeEnum = ItemTypeEnum.Inventoriable;
  ItemTypeEnum_Service: ItemTypeEnum = ItemTypeEnum.Service;

  ConfimentItemServiceModeEnum_All: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  ConfimentItemServiceModeEnum_Item: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Item;
  ConfimentItemServiceModeEnum_Service: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Service;
  _itemsServices: Patient_Confinement_ItemsServices_DTO[] = [];

  @Input() set itemsServices(objs: Patient_Confinement_ItemsServices_DTO[]) {
    this.loadMenuItems();
    if (this.ItemServiceMode != ConfimentItemServiceModeEnum.Service)
      this.loadMedicationRouteList();

    if (!objs) return;

    this._itemsServices = objs;

    this.compute();
  }

  get itemsandservices() {
    return this._itemsServices;
  }

  @Input() ItemServiceMode: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  @Output() onModelChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() onMenuItemClick: EventEmitter<any> = new EventEmitter<any>();

  MedicationRouteList: any[] = [];

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuByCurrentObject(CurrentObject: any) {
    var menuItem_AddService = {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_AddItem = {
      label: 'Add Medication',
      icon: 'fas fa-cubes',
      visible: true,
    };
    var menuItem_CreateBillingInvoice = {
      label: 'Create Invoice',
      icon: 'fas fa-file',
      visible: true,
    };
    var menuItem_viewBillingInvoice = {
      label: 'View Invoice',
      icon: 'fas fa-file',
      visible: true,
    };

    this.menuItems = [];

    if (CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      if (
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Item
      ) {
        this.menuItems.push(menuItem_AddItem);
      }

      if (
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Service
      ) {
        this.menuItems.push(menuItem_AddService);
      }
    }

    if (CurrentObject.ID_BillingInvoice) {
      this.menuItems.push(menuItem_viewBillingInvoice);
    } else {
      if (CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged) {
        this.menuItems.push(menuItem_CreateBillingInvoice);
      }
    }
  }

  loadMenuItems() {
    var menuItem_AddService = {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_AddItem = {
      label: 'Add Medication',
      icon: 'fas fa-cubes',
      visible: true,
    };

    this.menuItems = [];

    if (
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Item
    ) {
      this.menuItems.push(menuItem_AddItem);
    }

    if (
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Service
    ) {
      this.menuItems.push(menuItem_AddService);
    }
  }

  compute() {
    var totalAmount: number = 0;

    this.itemsandservices.forEach((itemsandservice) => {
      var Quantity = 0;
      var UnitPrice = 0;

      if (itemsandservice['Quantity']) {
        Quantity = itemsandservice['Quantity'];
      }

      if (itemsandservice['UnitPrice']) {
        UnitPrice = itemsandservice['UnitPrice'];
      }

      if (
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
        (this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Item &&
          itemsandservice.ID_ItemType == this.ItemTypeEnum_Inventoriable) ||
        (this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Service &&
          itemsandservice.ID_ItemType == this.ItemTypeEnum_Service)
      ) {
        var amount = Quantity * UnitPrice;

        itemsandservice.Amount = amount;
        totalAmount += amount;
      }
    });

    this.TotalAmount = totalAmount;
  }

  async loadMedicationRouteList(): Promise<void> {
    this.MedicationRouteList = [];

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tMedicationRoute
    `
    );

    var _MedicationRouteList: any[] = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      _MedicationRouteList.push(obj.Name);
    });

    this.MedicationRouteList = _MedicationRouteList;
  }

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    public toastService: ToastService,
    protected cs: CrypterService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Service') {
      this.doAddService();
    } else if (menuItem.label == 'Add Medication') {
      this.doAddItem();
    }

    this.onMenuItemClick.emit(arg);
  }

  async BtnDeleteSOAPPlan_OnClick(itemsService: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this._itemsServices,
      'ID',
      itemsService.ID + ''
    );

    this._itemsServices.splice(index, 1);

    this.compute();
  }

  async doAddService() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration,
              ID_ItemType
            FROM dbo.vItemService
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
        DateExpiration: record.OtherInfo_DateExpiration,
        ID_ItemType: record.ID_ItemType,
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this._itemsServices == null) this._itemsServices = [];
      this._itemsServices.push(item);
    });
  }

  async doAddItem() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              FormattedCurrentInventoryCount,
              RemainingBeforeExpired,
              ID_ItemType
            FROM dbo.vItemInventoriableForBillingLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Medication',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'RemainingBeforeExpired',
          caption: 'Rem. Before Expire',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.center,
        },
        {
          name: 'FormattedCurrentInventoryCount',
          caption: 'Available Count',
          propertyType: PropertyTypeEnum.Int,
          align: AlignEnum.center,
        },
      ],
    });

    var _ = this;
    _.tempID--;

    console.log(obj);

    obj.rows.forEach((record: any) => {
      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
        ID_ItemType: record.ID_ItemType,
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this._itemsServices == null) this._itemsServices = [];
      this._itemsServices.push(item);
    });
  }

  itemsService_onModelChanged(itemsService: any, e: IControlModelArg) {
    itemsService[e.name] = e.value;

    this.compute();

    this.onModelChanged.emit();
  }
}
