import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdjustCreditDialogComponent } from 'src/app/shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import {
  FilingStatusEnum,
  FilterCriteriaType,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';

@Component({
  selector: 'confinement-client-deposit-list',
  templateUrl: './confinement-client-deposit-list.component.html',
  styleUrls: [
    './confinement-client-deposit-list.component.less',
    './../../../layout/base-list-view/base-list-view.component.less',
  ],
})
export class ConfinementClientDepositListComponent implements OnInit {
  @ViewChild('adjustcreditdialog')
  adjustcreditdialog: AdjustCreditDialogComponent | undefined;

  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any;

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [];

  private ID_Patient_Confinement: number = 0;

  isAllowedToCancel: boolean = false;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private globalfx: GeneralfxService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async initmenubar_OnClick(e: any) {
    if (this.ID_Patient_Confinement > 0) {
      if (e.item.label == 'Deposit Credit') {
        if (this.adjustcreditdialog) {
          await this.adjustcreditdialog.showByPatientConfinement(
            this.ID_Patient_Confinement
          );
          this.load(this.ID_Patient_Confinement);
        }
      }
    }
  }

  async load(ID_Patient_Confinement: number) {
    this.ID_Patient_Confinement = ID_Patient_Confinement;

    var obj = await this.ds.execSP(
      'pGetConfinmentDeposit',
      {
        ID_Patient_Confinement: this.ID_Patient_Confinement,
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.ClientDeposits;

    this.loadMenuItem();
  }

  loadMenuItem() {
    this.menuItems = [];

    var meneItem_DepositCredit = {
      label: 'Deposit Credit',
      icon: 'fa fa-plus',
      class: 'text-default',
      visible: true,
    };

    if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined ||
      (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged &&
        (!this.CurrentObject.BillingInvoice_ID_FilingStatus ||
          this.CurrentObject.BillingInvoice_ID_FilingStatus ==
            FilingStatusEnum.ForBilling))
    ) {
      this.menuItems.push(meneItem_DepositCredit);

      this.isAllowedToCancel = true;
    } else {
      this.isAllowedToCancel = false;
    }
  }

  async cancelClientDeposit(ID_ClientDeposit: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      this.loading = true;
      var obj = await this.ds.execSP(
        'pCancelClientDeposit',
        {
          IDs_ClientDeposit: [ID_ClientDeposit],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        rej(obj);
      }

      this.loading = false;
    });
  }

  onRowClick(rowData: any) {}

  async onRowPrintRecord(rowData: any) {
    GeneralfxService.customNavigate(this.router, this.cs, ['Report'], {
      ReportName: 'ClientDepositReport',
      filterValues: [
        {
          dataField: 'ID',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: rowData.ID,
        },
      ],
    });
  }
  async onRowDeletingRecord(rowData: any) {
    // var result = await this.msgBox.confirm(
    //   `Would you like to remove deposited amount of ${record.DepositAmount}?`,
    //   `Cancel Deposit`,
    //   "Yes",
    //   "No"
    // );

    // if (!result) return;

    var obj = await this.cancelClientDeposit(rowData.ID);

    if (obj.Success) {
      this.toastService.success(`Deposit has been cancelled successfully.`);
    } else {
      this.toastService.danger(obj.message);
    }

    this.load(this.ID_Patient_Confinement);
  }
}
