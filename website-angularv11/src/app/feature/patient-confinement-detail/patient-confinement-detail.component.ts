import { Patient_Confinement_Patient } from './../../../shared/APP_MODELS';
import { ConfinementClientDepositListComponent } from './confinement-client-deposit-list/confinement-client-deposit-list.component';
import {
  BillingInvoice_Detail_DTO,
  ConfimentItemServiceModeEnum,
  FilingStatusEnum,
  IFormValidation,
  ItemTypeEnum,
  Patient_Confinement_ItemsServices_DTO,
} from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ConfinementSOAPListComponent } from './confinement-soap-list/confinement-soap-list.component';
import { ConfinementItemsServicesTableComponent } from './confinement-items-services-table/confinement-items-services-table.component';
import { AdjustCreditDialogComponent } from '../../shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { BillingInvoiceDetailComponent } from '../billing-invoice-detail/billing-invoice-detail.component';
import { Enumerable } from 'linq-typescript';
import { GeneralfxService } from 'src/app/core/generalfx.service';
@Component({
  selector: 'app-patient-confinement-detail',
  templateUrl: './patient-confinement-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-confinement-detail.component.less',
  ],
})
export class PatientConfinementDetailComponent extends BaseDetailViewComponent {
  @ViewChild('confinementclientdepositlist')
  confinementclientdepositlist:
    | ConfinementClientDepositListComponent
    | undefined;

  @ViewChild('confinementsoaplist')
  confinementsoaptable: ConfinementSOAPListComponent | undefined;

  @ViewChild('confinementitemsservicestable')
  confinementitemsservicestable:
    | ConfinementItemsServicesTableComponent
    | undefined;

  @ViewChild('adjustcreditdialog')
  adjustcreditdialog: AdjustCreditDialogComponent | undefined;

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  ConfimentItemServiceModeEnum_All: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  ConfimentItemServiceModeEnum_Item: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Item;
  ConfimentItemServiceModeEnum_Service: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Service;

  ModelName: string = 'Patient_Confinement';
  headerTitle: string = 'Confinement';

  routerFeatureName: string = 'Confinement';
  displayMember: string = 'Code';

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  _menuItem_AdjustCredit: AdminLTEMenuItem = {
    label: 'Adjust Credit',
    icon: 'fa fa-save',
    class: 'text-info',
    visible: true,
  };

  loadMenuItems() {
    var menuItemDischarge = {
      label: 'Discharge',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemRevert = {
      label: 'Revert',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    };

    var menuItem_CreateBillingInvoice = {
      label: 'Create Invoice',
      icon: 'fas fa-file',
      visible: true,
    };

    var menuItem_CreateBillingInvoice = {
      label: 'Create Invoice',
      icon: 'fas fa-file',
      visible: true,
    };

    var menuItem_viewBillingInvoice = {
      label: 'View Invoice',
      icon: 'fas fa-file',
      visible: true,
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
        this.addMenuItem(menuItemDischarge);
        this.addMenuItem(menuItemCancel);
      }

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged) {
        this.addMenuItem(menuItemRevert);
        this.addMenuItem(menuItemCancel);
      }

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
        this.addMenuItem(menuItemRevert);
      }

      if(this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled){
        if (this.CurrentObject.ID_BillingInvoice > 0) {
          this.addMenuItem(menuItem_viewBillingInvoice);
        } else {
          this.addMenuItem(menuItem_CreateBillingInvoice);
        }
      }

    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Discharge') {
      this.doDischarge();
    } else if (e.item.label == 'Revert') {
      this.doRevert();
    }  else if (e.item.label == 'Cancel') {
      this.doCancel();
    } else if (e.item.label == 'Adjust Credit') {
      var ID_Client = this.CurrentObject.ID_Client;
      if (this.adjustcreditdialog) {
        await this.adjustcreditdialog.showByPatientConfinement(
          this.CurrentObject.ID
        );
        this.loadRecord();
      }
    } else if (e.item.label == 'Create Invoice') {
      this.doCreateInvoice();
    } else if (e.item.label == 'View Invoice') {
      this.doViewInvoice();
    }
  }

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client} Info.`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.Code_BillingInvoice) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Code_BillingInvoice}`,
        visible: true,
        name: 'viewbillinginvoice',
      });
    }

  }

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'viewclient') {
      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'viewbillinginvoice') {
      this.customNavigate(
        ['BillingInvoice', this.CurrentObject.ID_BillingInvoice],
        {}
      );
    }
  }

  CurrentObject_onBeforeSaving() {
    var strNames_currentIDPatients = Enumerable.fromSource(
      this.CurrentObject.Patient_Confinement_Patient
    )
      .select((r: any) => r['Name_Patient'])
      .toArray()
      .toString();

    this.CurrentObject.PatientNames = strNames_currentIDPatients;
  }

  async doDischarge() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before discharging ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pDischargePatient_Confinement',
        {
          IDs_Patient_Confinement: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been discharged successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to discharge ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before cancel ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pCancelPatient_Confinement',
        {
          IDs_Patient_Confinement: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been canceled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async doRevert() {
    this.loading = true;

    this.execSP(
        'pRevertToConfined_Patient_Confinement',
        {
          IDs_Patient_Confinement: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been reverted to confined successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to reverted to confined ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async doCreateInvoice() {
    this.customNavigate(['BillingInvoice', -1], {
      ID_Patient_Confinement: this.CurrentObject.ID,
    });
  }

  async doViewInvoice() {
    this.customNavigate(
      ['BillingInvoice', this.CurrentObject.ID_BillingInvoice],
      {}
    );
  }

  DetailView_onLoad() {

    console.log(this.CurrentObject);
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (this.CurrentObject.Patient_Confinement_ItemsServices == null) {
      this.CurrentObject.Patient_Confinement_ItemsServices = [];
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }

    if (this.confinementsoaptable) {
      this.confinementsoaptable.loadMenuByCurrentObject(this.CurrentObject);
      this.confinementsoaptable.load(this.CurrentObject.ID);
    }

    if (this.confinementitemsservicestable) {
      this.confinementitemsservicestable.loadMenuByCurrentObject(
        this.CurrentObject
      );
    }


    if (this.confinementclientdepositlist) {
      this.confinementclientdepositlist.load(this.CurrentObject.ID);
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      this.CurrentObject.Patient_Confinement_Patient = [];
    }
  }

  compute() {
    var totalAmount: number = 0;

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach(
      (obj: Patient_Confinement_ItemsServices_DTO) => {
        var Quantity = obj.Quantity;
        var UnitPrice = obj.UnitPrice;
        var amount = 0;

        if (Quantity == undefined || Quantity == null) Quantity = 0;
        if (UnitPrice == undefined || UnitPrice == null) UnitPrice = 0;

        amount = Quantity * UnitPrice;
        obj.Amount = amount;

        totalAmount += amount;
      }
    );

    this.CurrentObject.SubTotal = totalAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;

    if (
      this.CurrentObject.ID_Client == undefined ||
      this.CurrentObject.ID_Client == null
    )
      ID_Client = 0;

    if (
      this.CurrentObject.Patient_Confinement_Patient == undefined ||
      this.CurrentObject.Patient_Confinement_Patient == null
    )
      this.CurrentObject.Patient_Confinement_Patient = [];

    if (ID_Client == 0) {
      validations.push({
        message: 'Client is required.',
      });
    }

    if (this.CurrentObject.Patient_Confinement_Patient.length == 0) {
      validations.push({
        message: 'Patient is required atleast one.',
      });
    }

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach(
      (itemService: { Quantity: number | null; Name_Item: any }) => {
        if (itemService.Quantity == null || itemService.Quantity == 0) {
          validations.push({
            message: `Quantity is required for ${itemService.Name_Item}.`,
          });
        }
      }
    );

    if (validations.length == 0) {
      var validateObj = await this.execSP(
        'pPatient_Confinement_Validation',
        {
          ID_Patient_Confinement: this.CurrentObject.ID,
          ID_Patient: this.CurrentObject.ID_Patient,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (validateObj.isValid != true) {
        validations.push({ message: validateObj.message });
      }
    }

    return Promise.resolve(validations);
  }

  confinementitemsservicestable_onModelChanged() {
    this.compute();
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = [
      'ID_Client',
      'ID_Patient',
      'ID_SOAPType',
      'ID_Patient_SOAP',
    ];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }

  public CurrentObject_onAfterSaving() {
    //this.SaveBillingInvoiceItems();
  }

  async SaveBillingInvoiceItems() {
    if (
      this.CurrentObject.ID_BillingInvoice == undefined ||
      this.CurrentObject.ID_BillingInvoice == null
    )
      return;

    var biDetailComponent: BillingInvoiceDetailComponent =
      new BillingInvoiceDetailComponent(
        this.router,
        this.route,
        this.elRef,
        this._dataServices,
        this.toastService,
        this.userAuth,
        this.cs,
        this.validationService,
        this.activeRoute,
        this.location
      );
    await biDetailComponent.LoadCurrentObjectRecord(
      this.CurrentObject.ID_BillingInvoice
    );
    if (this.CurrentObject.BillingInvoice_Detail == null)
      this.CurrentObject.BillingInvoice_Detail = [];

    if (biDetailComponent.CurrentObject.ID == -1) return;
    if (biDetailComponent.CurrentObject.ID == null) return;
    if (
      biDetailComponent.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed
    )
      return;

    var objs = Enumerable.fromSource(
      biDetailComponent.CurrentObject.BillingInvoice_Detail
    )
      .where(function (r: any) {
        return r.ID_Patient_Confinement_ItemsServices != null;
      })
      .toArray();

    objs.forEach(function (obj: any) {
      var index = GeneralfxService.findIndexByKeyValue(
        biDetailComponent.CurrentObject.BillingInvoice_Detail,
        'ID',
        obj['ID'] + ''
      );

      biDetailComponent.CurrentObject.BillingInvoice_Detail.splice(index, 1);
    });

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach(
      (itemService: {
        ID: any;
        ID_Item: any;
        Name_Item: any;
        Quantity: any;
        UnitCost: any;
        UnitPrice: any;
        DateExpiration: any;
      }) => {
        var biDetail: any = biDetailComponent.getNewBillingInvoice_Detail();

        biDetail.ID_Patient_Confinement_ItemsServices = itemService.ID;
        biDetail.ID_Item = itemService.ID_Item;
        biDetail.Name_Item = itemService.Name_Item;
        biDetail.Quantity = itemService.Quantity;
        biDetail.UnitCost = itemService.UnitCost;
        biDetail.UnitPrice = itemService.UnitPrice;
        biDetail.DateExpiration = itemService.DateExpiration;

        biDetailComponent.CurrentObject.BillingInvoice_Detail.push(biDetail);
      }
    );

    biDetailComponent.compute();

    this.CurrentObject.TotalAmount_BillingInvoice =
      biDetailComponent.CurrentObject.TotalAmount;
    await biDetailComponent.save();
  }
}
