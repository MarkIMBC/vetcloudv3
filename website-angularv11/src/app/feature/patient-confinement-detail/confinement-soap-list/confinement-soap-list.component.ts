import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'confinement-soap-list',
  templateUrl: './confinement-soap-list.component.html',
  styleUrls: ['./confinement-soap-list.component.less'],
})
export class ConfinementSOAPListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [];

  private ID_Patient_Confinement: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  loadMenuByCurrentObject(CurrentObject: any) {
    this.menuItems = [];

    var meneItem_Create = {
      label: 'Create',
      icon: 'fa fa-plus',
      class: 'text-default',
      visible: true,
    };

    if (
      CurrentObject.ID == 0 ||
      CurrentObject.ID == -1 ||
      CurrentObject.ID == null
    )
      return;

    if (CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      this.menuItems.push(meneItem_Create);
    }
  }

  initmenubar_OnClick(e: any) {
    if (this.ID_Patient_Confinement > 0) {
      if (e.item.label == 'Create') {
        var routeLink = ['Patient_SOAP', -1];
        var configOptions = {
          ID_Patient_Confinement: this.ID_Patient_Confinement,
        };

        GeneralfxService.customNavigate(
          this.router,
          this.cs,
          routeLink,
          configOptions
        );
      }
    }
  }

  load(ID_Patient_Confinement: number) {
    this.dataSource = [];

    this.ID_Patient_Confinement = ID_Patient_Confinement;

    if (this.ID_Patient_Confinement < 0) return;

    var sql = `SELECT
                    ID,
                    Date,
                    Code,
                    Name_SOAPType,
                    History,
                    DiagnosisHTML,
                    Name_FilingStatus
                FROM dbo.vPatient_SOAP
                WHERE
                  ID_Patient_Confinement = ${ID_Patient_Confinement} AND
                  ID_Company = ${this.currentUser.ID_Company} AND
                  ISNULL(ID_Patient_Confinement,  0) > 0
                ORDER BY
                  Date DESC
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;
    });
  }

  onRowClick(rowData: any) {
    var routeLink = ['Patient_SOAP', rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {});
  }
}
