import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import {
  AlignEnum,
  IControlModelArg,
  Patient_Confinement_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'patient-table',
  templateUrl: './patient-table.component.html',
  styleUrls: [
    './../../../layout/base-detail-view/base-detail-view.component.less',
    './patient-table.component.less',
  ],
})
export class PatientTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input()
  CurrentObject: Patient_Confinement_DTO = new Patient_Confinement_DTO();
  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Patient',
      icon: 'fa fa-plus',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuPatient: AdminLTEMenuItem = arg.item;

    if (menuPatient.label == 'Add Patient') {
      this.doAddPatient();
    }
  }

  async BtnDeletePatientDetail_OnClick(detail: any) {
    if (!this.CurrentObject.Patient_Confinement_Patient) return;

    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject.Patient_Confinement_Patient,
      'ID',
      detail.ID + ''
    );

    this.CurrentObject.Patient_Confinement_Patient.splice(index, 1);

    this.onChanged.emit();
  }

  async doAddPatient() {
    if (this.sqllistdialog == undefined) return;

    var strIDs_currentIDPatients = '';
    var IDs_Patient: any[] = [];

    if (
      this.CurrentObject.Patient_Confinement_Patient == undefined ||
      this.CurrentObject.Patient_Confinement_Patient == null
    ) {
      this.CurrentObject.Patient_Confinement_Patient = [];
    }

    this.CurrentObject.Patient_Confinement_Patient.forEach((patient) =>
      IDs_Patient.push(patient.ID_Patient)
    );
    strIDs_currentIDPatients =
      IDs_Patient.length > 0 ? IDs_Patient.join(',') : '0';

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID, Code, Name
            FROM dbo.vPatient
            /*encryptsqlend*/
            WHERE
               ID_Company = ${this.currentUser.ID_Company} AND
               ID_Client = ${this.CurrentObject.ID_Client} AND
               ID NOT IN (${strIDs_currentIDPatients}) AND
               IsActive = 1`,
      columns: [
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name',
          caption: 'Patient',
          align: AlignEnum.left,
          propertyType: PropertyTypeEnum.String,
          format: '1.2',
        },
      ],
    });

    var _ = this;
    if (!this.CurrentObject.Patient_Confinement_Patient) return;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Patient: record.ID,
        Name_Patient: record.Name,
        UnitPrice: record.UnitCost,
      };

      if (this.CurrentObject.Patient_Confinement_Patient == null)
        this.CurrentObject.Patient_Confinement_Patient = [];
      this.CurrentObject.Patient_Confinement_Patient.push(item);
    });

    this.onChanged.emit();
  }

  Patient_onModelChanged(Patient: any, e: IControlModelArg) {
    Patient[e.name] = e.value;

    this.onChanged.emit();
  }

  PatientTable_onChanged(fieldName: string, e: any) {
    this.onChanged.emit();
  }
}
