import { ModalModule } from './../../shared/modal/modal.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleListRoutingModule } from './schedule-list-routing.module';
import { ScheduleListComponent } from './schedule-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppointmentScheduleStatusComponent } from './appointment-schedule-status/appointment-schedule-status.component';
import { UpdateTimeDialogComponent } from './update-time-dialog/update-time-dialog.component';

@NgModule({
  declarations: [ScheduleListComponent, AppointmentScheduleStatusComponent, UpdateTimeDialogComponent],
  imports: [CommonModule, SharedModule, ModalModule, ScheduleListRoutingModule],
})
export class ScheduleListModule {}
