import { UpdateTimeDialogComponent } from './update-time-dialog/update-time-dialog.component';
import { GeneralfxService } from './../../core/generalfx.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  Patient_SOAP_DTO,
} from './../../../shared/APP_HELPER';
import { APP_MODEL, BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import * as moment from 'moment';
import { AppointmentScheduleStatusComponent } from './appointment-schedule-status/appointment-schedule-status.component';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './schedule-list.component.less',
  ],
})
export class ScheduleListComponent extends BaseListViewComponent {
  @ViewChild('appointmentschedulestatus') appointmentschedulestatus:
    | AppointmentScheduleStatusComponent
    | undefined;

  @ViewChild('updatetimedialog') updatetimedialog:
    | UpdateTimeDialogComponent
    | undefined;

  CustomComponentName: string = 'ScheduleListComponent';

  headerTitle: string = 'Schedule';

  WaitingStatus_Cancelled = FilingStatusEnum.Cancelled;
  WaitingStatus_Reschedule = FilingStatusEnum.Reschedule;

  InitCurrentObject: any = {
    Date: moment().format('yyyy-MM-DD'),
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    this.InitCurrentObject.Date = moment().format('yyyy-MM-DD');

    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    this.OrderByString = 'DateStart ASC';

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vAppointmentEvent
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code == null) this.CurrentObject.Code = '';
    if (this.CurrentObject.Name_Client == null)
      this.CurrentObject.Name_Client = '';
    if (this.CurrentObject.Name_Patient == null)
      this.CurrentObject.Name_Patient = '';

    if (this.CurrentObject.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ReferenceCode LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject['DateStart_Date']) {
      var _dateStart = this.CurrentObject['DateStart_Date'];
      var _dateEnd = this.CurrentObject['DateEnd_Date'];

      if (filterString.length > 0) filterString = filterString + ' AND ';

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          'YYYY-MM-DD'
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateStart) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    }

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (!this.CurrentObject['DateStart_Date'] && filterString.length == 0) {
      if (filterString.length > 0) filterString = filterString + ' AND ';

      filterString += `
            CONVERT(DATE, DateStart) BETWEEN
              '${moment(_dateStart).format('YYYY-MM-DD')}' AND
              '${moment(_dateEnd).format('YYYY-MM-DD')}'
          `;
    }

    return filterString;
  }

  Row_OnClick(rowData: any) {
    if (rowData.Oid_Model == APP_MODEL.PATIENT_SOAP.toLowerCase()) {
      this.customNavigate(['Patient_SOAP', rowData.ID_CurrentObject], {});
    } else if (
      rowData.Oid_Model == APP_MODEL.PATIENTAPPOINTMENT.toLowerCase()
    ) {
      this.customNavigate(['PatientAppointment', rowData.ID_CurrentObject], {});
    } else if (rowData.Oid_Model == APP_MODEL.PATIENT_WELLNESS.toLowerCase()) {
      this.customNavigate(['Patient_Wellness', rowData.ID_CurrentObject], {});
    }
  }

  async ColumnTime_OnClick(rowData: any) {
    if (this.updatetimedialog) {
      await this.updatetimedialog.show(rowData);
      this.loadRecords();
    }
  }
  menuItem_New_onClick() {
    this.customNavigate(['PatientAppointment', -1], {});
  }

  async Row_Name_FilingStatus_OnClick(rowData: any) {
    if (this.appointmentschedulestatus) {
      await this.appointmentschedulestatus.show(rowData);
      this.loadRecords();
    }
  }

  Date_onModelChanged(e: IControlModelArg) {
    this.control_onModelChanged(e);

    this.pagingOption.PageNumber = 1;
    this.loadRecords();
  }
}
