import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentScheduleStatusComponent } from './appointment-schedule-status.component';

describe('AppointmentScheduleStatusComponent', () => {
  let component: AppointmentScheduleStatusComponent;
  let fixture: ComponentFixture<AppointmentScheduleStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppointmentScheduleStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentScheduleStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
