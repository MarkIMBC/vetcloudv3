import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  IFormValidation,
  FilingStatusEnum,
  PropertyTypeEnum,
  IControlModelArg,
} from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'appointment-schedule-status',
  templateUrl: './appointment-schedule-status.component.html',
  styleUrls: ['./appointment-schedule-status.component.less'],
})
export class AppointmentScheduleStatusComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;
  private currentUser: TokenSessionFields = new TokenSessionFields();
  public loading: boolean = false;

  Reschedule_FilingStatus: number = FilingStatusEnum.Reschedule;

  PreviousObject: any = {};
  CurrentObject: any = {};

  Appointment_ID_FilingStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `Select
              ID,
              Name
            FROM vAppointment_FilingStatus
    `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
      orderByString: 'Name ASC',
    },
  };

  constructor(
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {}

  async onPositiveButtonClick() {
    var isvalid = await this.validateRecord();
    if (!isvalid) return;

    await this.updateAppointmentStatus();

    if (this.modalDialog) {
      this.modalDialog.close();
    }
  }

  async btnClose_onClick() {
    if (this.modalDialog) {
      this.modalDialog.close();
    }
  }

  async show(obj: any): Promise<any> {
    var modalDialog = this.modalDialog;

    this.CurrentObject = JSON.parse(JSON.stringify(obj));

    if (
      this.CurrentObject.Appointment_ID_FilingStatus ==
      this.Reschedule_FilingStatus
    ) {
      this.CurrentObject.DateReschedule = obj.DateReschedule;
    } else {
      this.CurrentObject.DateReschedule = moment().toDate();
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }
  }

  private async validateRecord() {
    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    var Appointment_ID_FilingStatus =
      this.CurrentObject.Appointment_ID_FilingStatus;
    var Appointment_CancellationRemarks =
      this.CurrentObject.Appointment_CancellationRemarks;

    if (Appointment_ID_FilingStatus == null) Appointment_ID_FilingStatus = 0;
    if (Appointment_CancellationRemarks == null)
      Appointment_CancellationRemarks = '';

    Appointment_CancellationRemarks = Appointment_CancellationRemarks.trim();

    if (Appointment_ID_FilingStatus == 0) {
      validationsAppForm.push({
        message: 'Appointment Status is required.',
      });
    }

    if (
      this.CurrentObject.Appointment_ID_FilingStatus ==
      FilingStatusEnum.Cancelled
    ) {
      if (Appointment_CancellationRemarks.length == 0) {
        validationsAppForm.push({
          message: 'Reason for Cancellation is required.',
        });
      }
    }

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  async updateAppointmentStatus(): Promise<any> {
    if (
      this.CurrentObject.Appointment_ID_FilingStatus !=
      FilingStatusEnum.Cancelled
    ) {
      this.CurrentObject.Appointment_CancellationRemarks = '';
    }

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pDoUpdateAppointmentStatus',
        {
          Oid_Model: this.CurrentObject.Oid_Model,
          Appointment_ID_CurrentObject:
            this.CurrentObject.Appointment_ID_CurrentObject,
          Appointment_ID_FilingStatus:
            this.CurrentObject.Appointment_ID_FilingStatus,
          DateReschedule: this.CurrentObject.DateReschedule,
          ID_UserSession: this.currentUser.ID_UserSession,
          Remarks: this.CurrentObject.Appointment_CancellationRemarks,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(`Status has been change successfully.`);
        res(obj);
      } else {
        this.toastService.danger(`Failed to Change Status`);
        rej(obj);
      }
    });
  }
}
