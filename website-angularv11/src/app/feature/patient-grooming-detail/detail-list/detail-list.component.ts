import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  IControlModelArg,
  Patient_Grooming_Detail_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Patient_Grooming_Detail } from 'src/shared/APP_MODELS';

@Component({
  selector: 'detail-list',
  templateUrl: './detail-list.component.html',
  styleUrls: ['./detail-list.component.less'],
})
export class DetailListComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() Details: Patient_Grooming_Detail_DTO[] = [];

  tempID: number = 0;

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    // {
    //   label: 'Add Medication',
    //   icon: 'fa fa-stethoscope',
    //   visible: true,
    // },
    {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Medication') {
      this.doAddItemInventoriable();
    } else if (menuItem.label == 'Add Service') {
      this.doAddItemService();
    }
  }

  async doAddItemInventoriable() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vActiveItemInventoriable
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Medication',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
      };

      if (this.Details == null) this.Details = [];
      this.Details.push(item);
    });
  }

  async doAddItemService() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vActiveItemService
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Service',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;
    _.tempID--;

    obj.rows.forEach((record: any) => {
      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
      };

      if (this.Details == null) this.Details = [];
      this.Details.push(item);
    });
  }

  async BtnDeleteDetail_OnClick(DetailList: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.Details,
      'ID',
      DetailList.ID + ''
    );

    this.Details.splice(index, 1);
  }

  detail_onModelChanged(DetailList: any, e: IControlModelArg) {
    DetailList[e.name] = e.value;
  }
}
