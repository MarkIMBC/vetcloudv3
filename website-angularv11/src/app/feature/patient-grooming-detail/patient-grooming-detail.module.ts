import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientGroomingDetailRoutingModule } from './patient-grooming-detail-routing.module';
import { PatientGroomingDetailComponent } from './patient-grooming-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DetailListComponent } from './detail-list/detail-list.component';

@NgModule({
  declarations: [PatientGroomingDetailComponent, DetailListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PatientGroomingDetailRoutingModule,
  ],
})
export class PatientGroomingDetailModule {}
