import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientGroomingDetailComponent } from './patient-grooming-detail.component';

const routes: Routes = [{ path: '', component: PatientGroomingDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientGroomingDetailRoutingModule { }
