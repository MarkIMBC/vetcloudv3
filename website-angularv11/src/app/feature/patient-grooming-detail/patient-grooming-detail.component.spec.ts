import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientGroomingDetailComponent } from './patient-grooming-detail.component';

describe('PatientGroomingDetailComponent', () => {
  let component: PatientGroomingDetailComponent;
  let fixture: ComponentFixture<PatientGroomingDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientGroomingDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientGroomingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
