import { BreedSpecie, Patient } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { FilingStatusEnum, Patient_DTO } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { MergePetDialogComponent } from './merge-pet-dialog/merge-pet-dialog.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-list.component.less',
  ],
})
export class PatientListComponent extends BaseListViewComponent {
  @ViewChild('mergepetdialog') mergepetdialog:
    | MergePetDialogComponent
    | undefined;

  CustomComponentName: string = 'PatientListComponent';

  @ViewChild('cellContactNumberTemplate')
  cellContactNumberTemplate!: TemplateRef<any>;

  FILINGSTATUS_PENDING: FilingStatusEnum = FilingStatusEnum.Pending;
  FILINGSTATUS_WAITING: FilingStatusEnum = FilingStatusEnum.Waiting;
  FILINGSTATUS_CANCELLED: FilingStatusEnum = FilingStatusEnum.Cancelled;
  FILINGSTATUS_DONE: FilingStatusEnum = FilingStatusEnum.Done;
  FILINGSTATUS_RESCHEDULE: FilingStatusEnum = FilingStatusEnum.Reschedule;

  headerTitle: string = 'Patient';

  InitCurrentObject: any = {
    Code: '',
    Name_Client: '',
    Name: '',
    Species: '',
    Name_Gender: '',
    IsDeleted: false,
    LastAttendingPhysician_Name_Employee: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vPatient_ListView
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Code == null) this.CurrentObject.Code = '';
    if (this.CurrentObject.Name_Client == null)
      this.CurrentObject.Name_Client = '';
    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    if (this.CurrentObject.Species == null) this.CurrentObject.Species = '';
    if (this.CurrentObject.Microchip == null) this.CurrentObject.Microchip = '';
    if (this.CurrentObject.Name_Gender == null)
      this.CurrentObject.Name_Gender = '';

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.Species.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Species LIKE '%${this.CurrentObject.Species}%'`;
    }

    if (this.CurrentObject.Microchip.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Microchip LIKE '%${this.CurrentObject.Microchip}%'`;
    }

    if (this.CurrentObject.Name_Gender.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Gender LIKE '%${this.CurrentObject.Name_Gender}%'`;
    }

    if (filterString.length > 0) filterString += ' AND ';
    filterString += `IsActive = ${this.CurrentObject.IsDeleted ? '0' : '1'}`;

    if (this.CurrentObject.LastAttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `LastAttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.LastAttendingPhysician_Name_Employee}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_DTO) {
    this.customNavigate(['Patient', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Patient', -1], {});
  }

  async RowBtnActionWaitingStatus_onClick(rowData: Patient_DTO) {
    if (
      rowData.WaitingStatus_ID_FilingStatus == null ||
      rowData.WaitingStatus_ID_FilingStatus == FilingStatusEnum.Cancelled ||
      rowData.WaitingStatus_ID_FilingStatus == FilingStatusEnum.Done ||
      rowData.WaitingStatus_ID_FilingStatus == FilingStatusEnum.Reschedule
    ) {
      await this.doQueue(rowData);
      this.loadRecords();
    } else if (
      rowData.WaitingStatus_ID_FilingStatus == FilingStatusEnum.Waiting
    ) {
      await this.cancelPatientWaiting(rowData);
      this.loadRecords();
    }
  }

  async doQueue(patient: Patient_DTO): Promise<any> {
    this.loading = true;

    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pDoAddPatientToWaitingList',
        {
          IDs_Patient: [patient.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(
          `${patient.Name} has been added to waiting list.`
        );

        this.loading = false;
        res(obj);
      } else {
        this.toastService.danger(obj.message, `Failed to Add ${patient.Name}`);

        this.loading = false;
        rej(obj);
      }
    });
  }

  async cancelPatientWaiting(patient: Patient_DTO): Promise<any> {
    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pUpdateLatestPatientToWaitingListStatus',
        {
          IDs_Patient: [patient.ID],
          WaitingStatus_ID_FilingStatus: FilingStatusEnum.Cancelled,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(
          `${patient.Name} has been cancelled to waiting list.`
        );

        this.loading = false;
        res(obj);
      } else {
        this.toastService.danger(
          obj.message,
          `Failed to cancel ${patient.Name}.`
        );

        this.loading = false;
        rej(obj);
      }
    });
  }

  async RowBtnActionCreateSOAP_onClick(rowData: Patient_DTO) {
    this.customNavigate(['Patient_SOAP', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID,
    });
  }

  async RowBtnActionCreateConfinement_onClick(rowData: Patient_DTO) {
    this.customNavigate(['Confinement', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID,
    });
  }

  async RowBtnActionCreateAppointment_onClick(rowData: Patient_DTO) {
    this.customNavigate(['PatientAppointment', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID,
    });
  }
  async RowBtnActionCreateWellness_onClick(rowData: Patient_DTO) {
    this.customNavigate(['Patient_Wellness', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID,
    });
  }

  async RowBtnActionCreatePetGrooming_onClick(rowData: Patient_DTO) {
    this.customNavigate(['Patient_Grooming', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID,
    });
  }

  async RowBtnActionMergePetModal_onClick(rowData: Patient_DTO) {
    if (this.mergepetdialog != undefined) {
      await this.mergepetdialog.show(rowData.ID);
      this.loadRecords();
    }
  }

  openNewTab(rowData: any) {
    var config: any[] = this.getCustomNavigateCommands(
      ['Patient', rowData.ID],
      {}
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }
}
