import { Component, OnInit, ViewChild } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  UserAuthenticationService,
  TokenSessionFields,
} from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  IFormValidation,
  MergePet,
  AlignEnum,
} from 'src/shared/APP_HELPER';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'merge-pet-dialog',
  templateUrl: './merge-pet-dialog.component.html',
  styleUrls: ['./merge-pet-dialog.component.less'],
})
export class MergePetDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: MergePet = new MergePet();

  SourcePatient: any = {};

  private ID: number = 0;

  ID_Pet_Pet_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
        {
          name: 'Name',
          caption: 'Pet',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
      ],
    },
  };

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(_ID?: number): Promise<any> {
    this.CurrentObject = new MergePet();
    this.CurrentObject.ID_Patient_Source = _ID;
    this.CurrentObject.ID_Patient_Destination = undefined;

    this.loadSourcePatient();

    var modalDialog = this.modalDialog;
    var petPetListOption = this.ID_Pet_Pet_LookupboxOption.listviewOption;

    if (petPetListOption != undefined) {
      petPetListOption.sql = `/*encryptsqlstart*/
                                SELECT
                                  ID, Code, Name_Client + ' - ' + Name Name
                                FROM vActivePatient
                                WHERE
                                  ID_Company = ${this.currentUser.ID_Company} AND
                                  ID NOT IN (${_ID})
                              /*encryptsqlend*/`;
                              }

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();

        resolve(null);
      }
    });

    return promise;
  }

  ID_Patient_Destination_onModelChanged(e: IControlModelArg) {
    this.CurrentObject.ID_Patient_Destination = e.value;
  }

  async btnSubmit_onClick() {
    this.loading = true;

    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject != undefined) {
      if (this.CurrentObject.ID_Patient_Destination == null) {
        validationsAppForm.push({ message: 'Destination is required.' });
      }
    }

    validationsAppForm.forEach((validation) => {
      this.toastService.warning(validation.message);
    });

    this.loading = false;

    if (validationsAppForm.length > 0) return;

    this.mergePet().then(() => {
      if (this.modalDialog != undefined) {
        this.modalDialog.close();
      }
    });
  }

  private async loadSourcePatient() {
    var queryString = this.cs.encrypt(`
    SELECT
        ID,
        Name_Client + ' - ' + Name Name
      FROM vActivePatient
      WHERE
        ID IN (${this.CurrentObject.ID_Patient_Source})
  `);

    var records = await this.ds.query<any>(queryString);
    this.SourcePatient = records[0];
  }

  async mergePet(): Promise<any> {
    var item = this.CurrentObject;

    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pDoMergePatientRecord',
        {
          From_ID_Patient: item.ID_Patient_Source,
          To_ID_Patient: item.ID_Patient_Destination,
          ID_UserSession: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(`Pet successfully merge.`);
        res(obj);
      } else {
        this.toastService.danger(obj.message);
        rej(obj);
      }

      this.loading = false;
    });
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }
}
