import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientListRoutingModule } from './patient-list-routing.module';
import { PatientListComponent } from './patient-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { MergePetDialogComponent } from './merge-pet-dialog/merge-pet-dialog.component';




@NgModule({
  declarations: [PatientListComponent,MergePetDialogComponent ],
  imports: [
    CommonModule,
    SharedModule,
    PatientListRoutingModule,
    ModalModule
  ]
})
export class PatientListModule { }
