import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { PaymentTransactionListRoutingModule } from './payment-transaction-list-routing.module';
import { PaymentTransactionListComponent } from './payment-transaction-list.component';


@NgModule({
  declarations: [PaymentTransactionListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PaymentTransactionListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PaymentTransactionListModule { }
