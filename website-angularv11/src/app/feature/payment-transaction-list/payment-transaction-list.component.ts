import { PaymentTransaction_DTO } from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';

@Component({
  selector: 'app-payment-transaction-list',
  templateUrl: './payment-transaction-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './payment-transaction-list.component.less'
  ],
})
export class PaymentTransactionListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'PaymentTransactionListComponent';

  headerTitle: string = 'Payment Transaction';

  InitCurrentObject: any = {

    Code: '',
    Name_Client: '',
    Name_Patient: '',
    Name_FilingStatus: '',
  }

  dataSource: PaymentTransaction_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM dbo.vPaymentTransaction_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";


    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%'`;
    }

    return filterString;
  }


  dataSource_InitLoad(obj: any) {

    console.log(obj);
  }

  Row_OnClick(rowData: PaymentTransaction_DTO) {

    this.customNavigate(['PaymentTransaction', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['PaymentTransaction', -1], {});
  }
}
