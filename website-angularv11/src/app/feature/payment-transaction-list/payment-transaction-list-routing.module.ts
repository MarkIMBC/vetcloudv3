import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentTransactionListComponent } from './payment-transaction-list.component';

const routes: Routes = [{ path: '', component: PaymentTransactionListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentTransactionListRoutingModule { }
