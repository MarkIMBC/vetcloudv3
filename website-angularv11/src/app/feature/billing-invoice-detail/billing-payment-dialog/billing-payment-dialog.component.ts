import { CrypterService } from 'src/app/core/crypter.service';
import { PaymentTransactionDetailComponent } from './../../payment-transaction-detail/payment-transaction-detail.component';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { UserAuthenticationService } from './../../../core/UserAuthentication.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { DataService } from 'src/app/core/data.service';
import { PaymentMethodEnum } from './../../../../shared/APP_HELPER';
import { APP_MODEL, PaymentMethod } from './../../../../shared/APP_MODELS';
import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { BillingInvoice_DTO, IControlModelArg } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'billing-payment-dialog',
  templateUrl: './billing-payment-dialog.component.html',
  styleUrls: ['./billing-payment-dialog.component.less'],
})
export class BillingPaymentDialogComponent implements OnInit {
  @Input() ID_BillingInvoice: number = 0;

  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;
  @ViewChild('paymentCashTemplate') paymentCashTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentGCashTemplate') paymentGCashTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentDebitCreditTemplate') paymentDebitCreditTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentCheckTemplate') paymentCheckTemplate:
    | TemplateRef<any>
    | undefined;

  paymentTemplate: any = null;
  paymentTranCurrentObject: any = {};
  paymentTranPreviousObject: any = {};
  BankList: any[] = [];

  currentPaymentMethod: PaymentMethodEnum = 0;
  currentUser: TokenSessionFields = new TokenSessionFields();

  loading: boolean = false;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
    await this.loadBankList();
  }

  async loadBankList(): Promise<void> {
    this.BankList = [];

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tBank
    `
    );

    var _BankList: string[] = [];

    var objs = await this.ds.query<any>(sql);

    this.BankList = objs;
  }

  async show(paymentMethod: PaymentMethodEnum): Promise<any> {
    this.currentPaymentMethod = paymentMethod;
    this.setPaymentTemplate(paymentMethod);

    this.getPaymentTransaction();

    var modalDialog = this.modalDialog;
    var modalDialog = this.modalDialog;

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  async show1(paymentMethod: PaymentMethodEnum) {
    this.currentPaymentMethod = paymentMethod;

    if (this.modalDialog == undefined) return;

    this.modalDialog.open();
    this.setPaymentTemplate(paymentMethod);

    await this.getPaymentTransaction();
  }

  private setPaymentTemplate(paymentMethod: PaymentMethodEnum) {
    switch (paymentMethod) {
      case PaymentMethodEnum.Cash:
        this.paymentTemplate = this.paymentCashTemplate;
        break;
      case PaymentMethodEnum.GCash:
        this.paymentTemplate = this.paymentGCashTemplate;
        break;

      case PaymentMethodEnum.DebitCredit:
        this.paymentTemplate = this.paymentDebitCreditTemplate;
        break;

      case PaymentMethodEnum.Check:
        this.paymentTemplate = this.paymentCheckTemplate;
        break;
    }
  }

  private async getPaymentTransaction() {
    this.loading = true;

    var billingInvoice = await this.ds
      .execSP(
        'pGetBillingInvoice',
        {
          ID: this.ID_BillingInvoice,
        },
        { isReturnObject: true }
      )
      .catch(() => {});

    var obj = await this.ds
      .execSP(
        'pGetPaymentTransaction',
        {
          ID: -1,
        },
        { isReturnObject: true }
      )
      .catch(() => {});

    this.paymentTranCurrentObject = JSON.parse(JSON.stringify(obj));
    this.paymentTranPreviousObject = JSON.parse(JSON.stringify(obj));

    this.paymentTranCurrentObject.ID_BillingInvoice = billingInvoice.ID;
    this.paymentTranCurrentObject.ID_Company = billingInvoice.ID_Company;
    this.paymentTranCurrentObject.Code_BillingInvoice = billingInvoice.Code;
    this.paymentTranCurrentObject.ID_PaymentMethod = this.currentPaymentMethod;
    this.paymentTranCurrentObject.RemainingAmount_BillingInvoice =
      billingInvoice.RemainingAmount;

    this.paymentTranCurrentObject.Comment = `This record is auto-generate from ${billingInvoice.Code} Paid feature.`;

    this.compute();

    this.loading = false;
  }

  compute() {
    this.paymentTranCurrentObject.ID_PaymentMethod = this.currentPaymentMethod;

    PaymentTransactionDetailComponent.Compute(this.paymentTranCurrentObject);
  }

  control_onModelChanged(e: IControlModelArg) {
    this.paymentTranCurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.paymentTranCurrentObject[e.displayName] = e.displayValue;
    }

    this.compute();
  }

  async savingPTRecord() {
    return new Promise<any>(async (resolve, reject) => {
      this.loading = true;

      this.ds
        .saveObject(
          APP_MODEL.PAYMENTTRANSACTION,
          this.paymentTranCurrentObject,
          this.paymentTranPreviousObject,
          [],
          this.currentUser
        )
        .then((r) => {
          this.loading = false;

          if (r.key != undefined) {
            var id = (r.key + '').replace("'", '');
            this.paymentTranCurrentObject.ID = parseInt(id);

            resolve(r);
          } else {
            reject('Saving Failed....');

            this.toastService.danger(
              `Unable to pay ${this.paymentTranCurrentObject.Code_BillingInvoice}. Please try again...`
            );
          }
        })
        .catch((reason) => {
          this.loading = false;

          this.toastService.danger(
            `Unable to pay ${this.paymentTranCurrentObject.Code_BillingInvoice}. Please try again...`
          );

          reject('Saving Failed....');
        });
    });
  }

  async approvedPTRecord() {
    return new Promise<any>(async (resolve, reject) => {
      this.loading = true;

      this.ds
        .execSP(
          'pApprovePaymentTransaction',
          {
            IDs_PaymentTransaction: [this.paymentTranCurrentObject.ID],
            ID_UserSession: this.currentUser.ID_UserSession,
          },
          {
            isReturnObject: true,
            isTransaction: true,
          }
        )
        .then((obj) => {

          this.runAfterSaveBillingInvoice();
          this.loading = false;

          resolve(obj);
        })
        .catch((obj) => {
          this.loading = false;

          this.toastService.danger(
            `Unable to pay ${this.paymentTranCurrentObject.Code_BillingInvoice}. Please try again...`
          );

          reject('Saving Failed....');
        });
    });
  }

  async btnPay_onClick() {
    this.compute();

    var obj = await this.savingPTRecord();

    console.log('Saving PT', obj);

    obj = await this.approvedPTRecord();

    console.log('Approved PT', obj);

    this.toastService.success(
      `${this.paymentTranCurrentObject.Code_BillingInvoice} has been successfully paid.`
    );

    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  async runAfterSaveBillingInvoice() {

    this.ds.
    execSP(
      'pReRunBillingInvoiceAfterSavedDummy',
      {
        IDs_BillingInvoice: [this.ID_BillingInvoice],
      },
      {
        isReturnObject: true,
        isTransaction: true,
      } )
      .then(async (obj: any) => {

      })
      .catch(() => {

      });
  }

}
