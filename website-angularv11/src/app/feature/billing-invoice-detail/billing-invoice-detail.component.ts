import { BIPaymentHistoryTableComponent } from './bi-payment-history-table/bi-payment-history-table.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingPaymentDialogComponent } from './billing-payment-dialog/billing-payment-dialog.component';
import {
  ItemTypeEnum,
  FilingStatusEnum,
  PaymentMethodEnum,
  FilterCriteriaType,
  IFormValidation,
  AlignEnum,
} from './../../../shared/APP_HELPER';
import { SQLListDialogComponent } from './../../shared/control/sql-list-dialog/sql-list-dialog.component';
import { CurrentObjectOnValueChangeArg } from './../../layout/base-detail-view/base-detail-view.component';
import { Component, ViewChild } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  PropertyTypeEnum,
  TaxSchemeEnum,
  BillingInvoice_Detail_DTO,
} from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Enumerable } from 'linq-typescript/build/src/Enumerables';
import { BillingInvoiceSMSRemiderListComponent } from './billing-invoice-smsremider-list/billing-invoice-smsremider-list.component';
import { PasswordUserPromptComponent } from 'src/app/shared/password-user-prompt/password-user-prompt.component';

@Component({
  selector: 'app-billing-invoice-detail',
  templateUrl: './billing-invoice-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './billing-invoice-detail.component.less',
  ],
})
export class BillingInvoiceDetailComponent extends BaseDetailViewComponent {
  @ViewChild('bipaymenthistorytable') bipaymenthistorytable:
    | BIPaymentHistoryTableComponent
    | undefined;
  @ViewChild('passworduserprompt') passworduserprompt:
    | PasswordUserPromptComponent
    | undefined;
  @ViewChild('biPaymentDialog') biPaymentDialog:
    | BillingPaymentDialogComponent
    | undefined;
  @ViewChild('billinginvoicesmsremiderlist') billinginvoicesmsremiderlist:
    | BillingInvoiceSMSRemiderListComponent
    | undefined;
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  ModelName: string = 'BillingInvoice';
  headerTitle: string = 'Billing Invoice';
  IsWalkIn: boolean = false;

  configParameterName = [
    'ID_Client',
    'ID_Patient',
    'AttendingPhysician_ID_Employee',
    'ID_Patient_SOAP',
    'ID_Patient_Confinement',
    'ID_Patient_Wellness',
    'IsWalkIn',
    'ID_Patient_Lodging',
    'ID_Patient_Grooming',
  ];

  FILINGSTATUS_FILED: FilingStatusEnum = FilingStatusEnum.Filed;

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client} Info.`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.Name_Patient != null) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Patient} Info.`,
        visible: true,
        name: 'viewpatient',
      });
    }

    if (this.CurrentObject.ID_Patient_Confinement != null) {
      this.rightDropDownItems.push({
        label: `Go Confinement Record`,
        visible: true,
        name: 'viewconfinement',
      });
    }
  }

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'viewclient') {
      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'viewpatient') {
      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {});
    } else if (event.item.name == 'viewconfinement') {
      this.customNavigate(
        ['Confinement', this.CurrentObject.ID_Patient_Confinement],
        {}
      );
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuItemApprove = {
      label: 'Approve',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemSendSMSRemimder = {
      label: 'Sent SMS Reminder',
      name: 'SentSMSReminder',
      icon: 'fa fa-mobile',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    };

    var menuItemPayment: AdminLTEMenuItem = {
      label: 'Payment',
      icon: 'far fa-money-bill-alt',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Cash',
          name: 'Cash',
          icon: 'far fa-money-bill-alt',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'G Cash',
          name: 'GCash',
          icon: 'far fa-money-bill-alt',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Debit / Credit',
          name: 'DebitCredit',
          icon: 'far fa-credit-card',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Check',
          name: 'Check',
          icon: 'fas fa-money-check',
          class: 'text-primary',
          visible: true,
        },
      ],
    };

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Billing Invoice Report',
          name: 'billinginvoicereport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Billing Invoice w/o Disc',
          name: 'billinginvoicewodiscreport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Receipt',
          name: 'receipt',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
      ],
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(menuItemApprove);
        this.addMenuItem(menuItemCancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        if (
          this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending
        ) {
          this.addMenuItem(menuItemCancel);
        }

        if (
          this.CurrentObject.Payment_ID_FilingStatus ==
            FilingStatusEnum.Pending ||
          this.CurrentObject.Payment_ID_FilingStatus ==
            FilingStatusEnum.PartiallyPaid
        ) {
          //this.addMenuItem(menuItemSendSMSRemimder);
          this.addMenuItem(menuItemPayment);
        }
      }

      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Approve') {

      /*Temp lang ito need mafix ung hindi makita ang mga confinement sa biling Invoice Report*/
      if(this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed && this.CurrentObject.ID_Patient_Confinement > 0){
        this.isHideSavingPrompt = true;
        await this._menuItem_Save_onClick();
      }

      if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
        return;

      this.doApprove();
    } else if (e.item.label == 'Cancel') {
      if (this.CurrentObject.ID_FilingStatus == this.FILINGSTATUS_FILED) {
        if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
          return;
      }

      if(this.passworduserprompt){

        await this.passworduserprompt.close();

        await this.passworduserprompt.show(this.model, this.CurrentObject.ID);

        if(this.passworduserprompt.IsValidated){

          this.doCancel();
        }
      }

    } else if (e.item.name == 'Cash') {
      if (this.biPaymentDialog == undefined) return;

      await this.biPaymentDialog.show(PaymentMethodEnum.Cash);
      this.loadRecord();
    } else if (e.item.name == 'GCash') {
      if (this.biPaymentDialog == undefined) return;

      await this.biPaymentDialog.show(PaymentMethodEnum.GCash);
      this.loadRecord();
    } else if (e.item.name == 'DebitCredit') {
      if (this.biPaymentDialog == undefined) return;
      await this.biPaymentDialog.show(PaymentMethodEnum.DebitCredit);
      this.loadRecord();
    } else if (e.item.name == 'Check') {
      if (this.biPaymentDialog == undefined) return;
      await this.biPaymentDialog.show(PaymentMethodEnum.Check);
      this.loadRecord();
    } else if (e.item.name == 'billinginvoicereport') {

      /*Temp lang ito need mafix ung hindi makita ang mga confinement sa biling Invoice Report*/
      if(this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed && this.CurrentObject.ID_Patient_Confinement > 0){
        this.isHideSavingPrompt = true;
        await this._menuItem_Save_onClick();
      }

      if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
        return;

      this.customNavigate(['Report'], {
        ReportName: 'PatientBillingInvoiceReport',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == 'billinginvoicewodiscreport') {

      /*Temp lang ito need mafix ung hindi makita ang mga confinement sa biling Invoice Report*/
      if(this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed && this.CurrentObject.ID_Patient_Confinement > 0){
        this.isHideSavingPrompt = true;
        await this._menuItem_Save_onClick();
      }
      if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
        return;

      this.customNavigate(['Report'], {
        ReportName: 'PatientBillingInvoiceReportNoDiscountPerItem',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == 'receipt') {
      if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
        return;
      this.PrintDialog();
    }
    else if (e.item.name == 'SentSMSReminder') {

      this.doInsertBillingInvoiceSMSPayableRemider();
    }
  }

  async PrintDialog() {
    var obj = await this.execSP(
      'pGetBillingInvoicePrintReceiptLayout',
      {
        ID_BillingInvoice: this.CurrentObject.ID,
      },
      {
        isReturnObject: true,
      }
    );

    var w = 400;
    var h = 800;

    const dualScreenLeft =
      window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    const dualScreenTop =
      window.screenTop !== undefined ? window.screenTop : window.screenY;

    const width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
      ? document.documentElement.clientWidth
      : screen.width;
    const height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
      ? document.documentElement.clientHeight
      : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - w) / 2 / systemZoom + dualScreenLeft;
    const top = (height - h) / 2 / systemZoom + dualScreenTop;

    var mywindow: any = window.open(
      '',
      'PRINT',
      `
      scrollbars=yes,
      width=${w / systemZoom},
      height=${h / systemZoom},
      top=10,
      left=${left}
    `
    );

    mywindow.document.write(`

      <html>
        <head>
          <title>${obj.title}</title>
          <style>${obj.style}</style>
        </head>
        <body>
          ${obj.content}
        </body>
      </html>
    `);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.onload = function () {
      var isMobile =
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        );

      if (obj.isAutoPrint) {
        setTimeout(async () => {
          mywindow.print();
          if (!isMobile) mywindow.close();
        }, obj.millisecondDelay);
      }
    };

    return true;
  }

  async runAfterSaveItem() {

    var IDs_Items: any[] = [];

    this.CurrentObject.BillingInvoice_Detail.forEach((detail: any) =>{

      if(!IDs_Items.includes(detail.ID_Item)){

        IDs_Items.push(detail.ID_Item);
      }
    });

    this.execSP(
      'pReRunItemAfterSavedDummy',
      {
        IDs_Item: IDs_Items,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      } )
      .then(async (obj) => {

      })
      .catch(() => {

      });
  }

  async runAfterSaveBillingInvoice() {

    this.execSP(
      'pReRunBillingInvoiceAfterSavedDummy',
      {
        IDs_BillingInvoice: [this.CurrentObject.ID],
      },
      {
        isReturnObject: true,
        isTransaction: true,
      } )
      .then(async (obj) => {

      })
      .catch(() => {

      });
  }

  async doApprove() {

    this.loading = true;

    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      //this.loading = false;
      //return;
    }

    this.execSP(
      'pApproveBillingInvoice',
      {
        IDs_BillingInvoice: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {

        this.runAfterSaveItem();

        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been approved successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to approve ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before canceling ${this.CurrentObject.Code}.`
      );
      //this.loading = false;
      //return;
    }

    this.loading = true;

    this.execSP(
      'pCancelBillingInvoice',
      {
        IDs_BillingInvoice: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {

        this.runAfterSaveItem();

        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }


  doInsertBillingInvoiceSMSPayableRemider() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before Sending Notification for ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
      'pInsertBillingInvoiceSMSPayableRemider',
      {
        ID_BillingInvoice: this.CurrentObject.ID,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been queue as successfully SMS reminder.`
          );
        } else {
          this.toastService.danger(
            `${obj.message}.`
          );
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to Queue SMS Reminder ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  ID_SOAPType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `SELECT ID, Name FROM tSOAPType WHERE IsActive = 1`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  getNewBillingInvoice_Detail() {
    var item = {
      ID: this.getTempID(),
      Quantity: 0,
      UnitCost: 0,
      UnitPrice: 0,
      DiscountRate: 0,
      DiscountAmount: 0,
    };

    return item;
  }

  compute() {
    var subTotal = 0;
    var initialSubtotalAmount = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;
    var initialConfimentDepositAmount =
      this.CurrentObject.InitialConfinementDepositAmount;
    var confimentDepositAmount = this.CurrentObject.ConfinementDepositAmount;
    var remainingDepositAmount = this.CurrentObject.RemainingDepositAmount;
    var consumedDepositAmount = this.CurrentObject.ConsumedDepositAmount;

    if (
      initialConfimentDepositAmount == null ||
      initialConfimentDepositAmount == undefined
    )
      initialConfimentDepositAmount = 0;
    if (confimentDepositAmount == null || confimentDepositAmount == undefined)
      confimentDepositAmount = 0;
    if (consumedDepositAmount == null || consumedDepositAmount == undefined)
      consumedDepositAmount = 0;
    if (remainingDepositAmount == null || remainingDepositAmount == undefined)
      remainingDepositAmount = 0;
    if (discountRate == null || discountRate == undefined) discountRate = 0;
    if (discountAmount == null || discountAmount == undefined)
      discountAmount = 0;

    discountRate = GeneralfxService.roundOffDecimal(discountRate);
    discountAmount = GeneralfxService.roundOffDecimal(discountAmount);

    this.CurrentObject.BillingInvoice_Detail.forEach((detail: any) => {
      var discountRate = detail.DiscountRate;
      var discountAmount = detail.DiscountAmount;
      var amount = detail.DiscountAmount;

      if (discountRate == null || discountRate == undefined) discountRate = 0;
      if (discountAmount == null || discountAmount == undefined)
        discountAmount = 0;

      if (detail.Quantity == null || detail.Quantity == undefined)
        detail.Quantity = 0;
      if (detail.UnitPrice == null || detail.UnitPrice == undefined)
        detail.UnitPrice = 0;

      amount = detail.Quantity * detail.UnitPrice;

      if (detail.IsComputeDiscountRate) {
        discountAmount = amount * (discountRate / 100);
        discountAmount = GeneralfxService.roundOffDecimal(discountAmount);
      } else {
        discountRate = (discountAmount / amount) * 100;
        discountRate = GeneralfxService.roundOffDecimal(discountRate);
      }

      detail.ID_BillingInvoice = this.CurrentObject.ID;
      detail.DiscountRate = discountRate;
      detail.DiscountAmount = discountAmount;
      detail.Amount = amount - discountAmount;

      subTotal += detail.Amount;
    });

    initialSubtotalAmount = subTotal + 0;
    initialSubtotalAmount = GeneralfxService.roundOffDecimal(
      initialSubtotalAmount
    );

    if (confimentDepositAmount > initialConfimentDepositAmount) {
      confimentDepositAmount = initialConfimentDepositAmount;
    }

    remainingDepositAmount = confimentDepositAmount - subTotal;
    if (remainingDepositAmount < 0) remainingDepositAmount = 0;
    remainingDepositAmount = GeneralfxService.roundOffDecimal(
      remainingDepositAmount
    );
    consumedDepositAmount = confimentDepositAmount - remainingDepositAmount;

    subTotal = subTotal - consumedDepositAmount;
    if (subTotal < 0) subTotal = 0;

    totalAmount = GeneralfxService.roundOffDecimal(subTotal);

    if (this.CurrentObject.IsComputeDiscountRate) {
      discountAmount = totalAmount * (discountRate / 100);
      discountAmount = GeneralfxService.roundOffDecimal(discountAmount);
    } else {
      discountRate = (discountAmount / totalAmount) * 100;
      discountRate = GeneralfxService.roundOffDecimal(discountRate);
    }

    totalAmount = totalAmount - discountAmount;
    totalAmount = GeneralfxService.roundOffDecimal(totalAmount);

    grossAmount = totalAmount;

    vatAmount = (grossAmount / 1.12) * 0.12;
    vatAmount = GeneralfxService.roundOffDecimal(vatAmount);

    if (
      this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
      this.CurrentObject.ID_TaxScheme == 0 ||
      this.CurrentObject.ID_TaxScheme == undefined ||
      this.CurrentObject.ID_TaxScheme == null
    ) {
      vatAmount = 0;
      netAmount = grossAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive) {
      netAmount = grossAmount + vatAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive) {
      netAmount = grossAmount - vatAmount;
    }

    if (discountRate == null || discountRate == undefined) discountRate = 0;
    if (discountAmount == null || discountAmount == undefined)
      discountAmount = 0;

    this.CurrentObject.ConfinementDepositAmount = confimentDepositAmount;
    this.CurrentObject.InitialSubtotalAmount = initialSubtotalAmount;
    this.CurrentObject.ConsumedDepositAmount = consumedDepositAmount;
    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;
    this.CurrentObject.RemainingDepositAmount = remainingDepositAmount;
  }

  protected async setConfinementDepositField(currentObject: any): Promise<any> {
    if (currentObject.BillingInvoice_Patient == null)
      currentObject.BillingInvoice_Patient = [];

    var IDs_Patient = Enumerable.fromSource(
      currentObject.BillingInvoice_Patient
    )
      .select((r: any) => r['ID_Patient'])
      .toArray();

    if (currentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      var obj = await this.execSP(
        'pGetConfinmentDepositByPatients',
        {
          ID_Patient_Confinement: currentObject.ID_Patient_Confinement,
          IDs_Patient: IDs_Patient,
        },
        {
          isReturnObject: true,
        }
      );
      console.log(this.CurrentObject.BillingInvoice_Patient, obj);

      currentObject.InitialConfinementDepositAmount =
        obj.ConfinementDepositAmount;
      currentObject.ConfinementDepositAmount = obj.ConfinementDepositAmount;
    }

    return currentObject;
  }

  async DetailView_onLoad() {


    this.IsWalkIn = this.CurrentObject.IsWalkIn;
    this.headerTitle = `Billing Invoice ${this.IsWalkIn ? '(Walk In)' : ''}`;

    if (this.CurrentObject.BillingInvoice_Detail == null) {
      this.CurrentObject.BillingInvoice_Detail = [];
    }

    if (this.CurrentObject.BillingInvoice_Patient == null) {
      this.CurrentObject.BillingInvoice_Patient = [];
    }

    var listViewOptionAttendingPhysician_ID_Employee =
      this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            IsActive = 1 AND
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    IsActive = 1 AND
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }

    if (this.bipaymenthistorytable != undefined) {
      this.bipaymenthistorytable.load(this.CurrentObject.ID);
    }

    if (this.billinginvoicesmsremiderlist != undefined) {
      this.billinginvoicesmsremiderlist.load(this.CurrentObject.ID);
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      await this.setConfinementDepositField(this.CurrentObject);

      if (
        this.PreviousObject.InitialConfinementDepositAmount !==
          this.CurrentObject.InitialConfinementDepositAmount ||
        this.PreviousObject.ConfinementDepositAmount !==
          this.CurrentObject.ConfinementDepositAmount
      ) {
        this.isDirty = true;
      }

      this.compute();
    }

  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      this.CurrentObject.BillingInvoice_Patient = [];

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  private async IsValidBillingInvoiceConfinementDepositUpdated(): Promise<boolean> {

    this.loading = true;

    return new Promise<boolean>(async (res, rej) => {

      var result: boolean = false;
      var beforInitialConfinementDepositAmount = 0;
      var afterInitialConfinementDepositAmount = 0;

      await this.execSP(
        'pGetBillingInvoice',
        {
          ID: this.CurrentObject.ID,
          ID_Session: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      ).then((obj: any) => {

        beforInitialConfinementDepositAmount =
        obj.InitialConfinementDepositAmount;
        afterInitialConfinementDepositAmount =
          this.CurrentObject.InitialConfinementDepositAmount;

        if (beforInitialConfinementDepositAmount == null)
          beforInitialConfinementDepositAmount = 0;
        if (afterInitialConfinementDepositAmount == null)
          afterInitialConfinementDepositAmount = 0;

        result =
          beforInitialConfinementDepositAmount ==
          afterInitialConfinementDepositAmount;

        if (!result) {
          var msg = `/*Code*/ has an updated confinement deposit from /*BeforeInitialConfinementDeposit*/ to /*AfterInitialConfinementDeposit*/. Please save the BI record first.`;

          msg = msg.replace('/*Code*/', this.CurrentObject.Code);
          msg = msg.replace(
            '/*BeforeInitialConfinementDeposit*/',
            'Php ' +
              beforInitialConfinementDepositAmount
                .toLocaleString('en-US')
                .replace('$', '') +
              ''
          );
          msg = msg.replace(
            '/*AfterInitialConfinementDeposit*/',
            'Php ' +
              afterInitialConfinementDepositAmount
                .toLocaleString('en-US')
                .replace('$', '') +
              ''
          );

          this.toastService.warning(msg);
        }

        res(result);
        this.loading = false;

      })
      .catch((reason) => {

        this.loading = false;
      });

    });
  }

  biDetailQuantity_onModelChange(biDetail: BillingInvoice_Detail_DTO) {
    this.compute();
    this.isDirty = true;
  }

  biDetailUnitPrice_onModelChange(biDetail: BillingInvoice_Detail_DTO) {
    this.compute();
    this.isDirty = true;
  }

  btnDeleteBIDetail_onClick(biDetail: BillingInvoice_Detail_DTO) {
    this.deleteDetail('BillingInvoice_Detail', biDetail);
  }

  biDetailDiscountRate_onModelChange(biDetail: BillingInvoice_Detail_DTO) {
    biDetail.IsComputeDiscountRate = true;
    this.compute();
    this.isDirty = true;
  }

  biDetailDiscountAmount_onModelChange(biDetail: BillingInvoice_Detail_DTO) {
    biDetail.IsComputeDiscountRate = false;
    this.compute();
    this.isDirty = true;
  }

  biDetailMenuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-cart-plus',
      visible: true,
    },
    {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    },
  ];

  async biDetailMenuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {
      this.doAddItem();
    } else if (menuItem.label == 'Add Service') {
      this.doAddService();
    }
  }

  async doAddItem() {
    var obj: any;

    obj = await this.sqllistdialog?.open({
      sql: `/*encryptsqlstart*/
            SELECT
                  ID,
                  Code,
                  Name,
                  UnitPrice,
                  CurrentInventoryCount,
                  UnitCost,
                  OtherInfo_DateExpiration
            FROM dbo.vItemInventoriableForBillingLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.right,
        },
        {
          name: 'CurrentInventoryCount',
          caption: 'Inventory Count',
          propertyType: PropertyTypeEnum.Int,
          format: '1.0',
          align: AlignEnum.right,
        },
      ],
    });

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        DiscountRate: 0,
        DiscountAmount: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
        DateExpiration: record.OtherInfo_DateExpiration,
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.CurrentObject.BillingInvoice_Detail == null)
        this.CurrentObject.BillingInvoice_Detail = [];
      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();

    this.isDirty = true;
  }

  async doAddService() {
    var obj: any;
    obj = await this.sqllistdialog?.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              UnitCost
            FROM dbo.vItemServiceLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.right,
        },
      ],
    });

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.CurrentObject.BillingInvoice_Detail == null)
        this.CurrentObject.BillingInvoice_Detail = [];
      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();

    this.isDirty = true;
  }

  DiscountRate_onModelChange() {
    this.CurrentObject.IsComputeDiscountRate = true;
    this.compute();
    this.isDirty = true;
  }

  DiscountAmount_onModelChange() {
    this.CurrentObject.IsComputeDiscountRate = false;
    this.compute();
    this.isDirty = true;
  }

  bipaymenthistorytable_onCancel() {
    this.loadRecord();
  }
  protected pGetRecordOptions(): any {
    var options: any = {};

    this.configParameterName.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.ID_Client == null) this.CurrentObject.ID_Client = 0;
    if (this.CurrentObject.ID_Patient == null)
      this.CurrentObject.ID_Patient = 0;

    if (this.IsWalkIn !== true) {
      if (this.CurrentObject.ID_Client == 0) {
        validations.push({
          message: `Client is required.`,
        });
      }
    }

    this.CurrentObject.BillingInvoice_Detail.forEach(
      (detail: { Quantity: number; Name_Item: any }) => {
        if (detail.Quantity == 0) {
          validations.push({
            message: `Quantity is required for item ${detail.Name_Item}.`,
          });
        }
      }
    );

    if (this.CurrentObject.BillingInvoice_Detail.length == 0) {
      validations.push({
        message: 'Add Item or service atleast one.',
      });
    }

    if (validations.length == 0) {
      validations = await this.validateBackend();
    }

    return Promise.resolve(validations);
  }

  public async PatientTable_onChanged() {
    await this.getInitCurrentObject(this.CurrentObject);

    this.isDirty = true;
  }

  public async CurrentObject_onBeforeSaving() {
    this.CurrentObject.PatientNames = '';

    this.CurrentObject.BillingInvoice_Patient.forEach(
      (patient: { Name_Patient: string }) => {
        this.CurrentObject.PatientNames += patient.Name_Patient + ', ';
      }
    );

    if (this.CurrentObject.PatientNames.length > 0) {
      this.CurrentObject.PatientNames = this.CurrentObject.PatientNames.slice(
        0,
        -2
      );
    }

    await this.setConfinementDepositField(this.CurrentObject);

    if (
      this.PreviousObject.InitialConfinementDepositAmount !==
        this.CurrentObject.InitialConfinementDepositAmount ||
      this.PreviousObject.ConfinementDepositAmount !==
        this.CurrentObject.ConfinementDepositAmount
    ) {
      this.isDirty = true;
    }

    this.compute();
  }

  async validateBackend(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var validateObj = await this.execSP(
      'pBillingInvoice_Validation',
      {
        ID_BillingInvoice: this.CurrentObject.ID,
        ID_Patient_Confinement: this.CurrentObject.ID_Patient_Confinement,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true) {
      validations.push({ message: validateObj.message });
    }

    return validations;
  }
}
