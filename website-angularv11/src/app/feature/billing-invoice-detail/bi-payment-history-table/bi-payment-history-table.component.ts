import { PaymentTransactionDetailComponent } from './../../payment-transaction-detail/payment-transaction-detail.component';
import { ToastService } from './../../../shared/toast.service';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { UserAuthenticationService } from './../../../core/UserAuthentication.service';
import { DataService } from './../../../core/data.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';
import { CrypterService } from 'src/app/core/crypter.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Router } from '@angular/router';

@Component({
  selector: 'bi-payment-history-table',
  templateUrl: './bi-payment-history-table.component.html',
  styleUrls: ['./bi-payment-history-table.component.less'],
})
export class BIPaymentHistoryTableComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  load(billingInvoiceID: number) {
    this.ID = billingInvoiceID;

    var sql = `SELECT
                    ID,
                    Date,
                    Code,
                    PayableAmount,
                    PaymentAmount,
                    Name_PaymentMethod,
                    RemainingAmount,
                    ChangeAmount,
                    Name_FilingStatus
                FROM dbo.vPaymentTransaction
                WHERE
                  ID_BillingInvoice = ${billingInvoiceID} AND
                  ID_FilingStatus IN (${FilingStatusEnum.Approved})
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;
    });
  }

  btnPrint_onClick(rowData: any) {
    PaymentTransactionDetailComponent.PrintDialog(this.ds, rowData.ID);
  }

  async btnCancel_onClick(ptRecord: any) {
    await this.cancelPTRecord(ptRecord);

    this.load(this.ID);
  }

  async cancelPTRecord(ptRecord: any) {
    this.loading = true;

    return new Promise<any>(async (resolve, reject) => {
      this.ds
        .execSP(
          'pCancelPaymentTransaction',
          {
            IDs_PaymentTransaction: [ptRecord.ID],
            ID_UserSession: this.currentUser.ID_UserSession,
          },
          {
            isReturnObject: true,
            isTransaction: true,
          }
        )
        .then((obj) => {

          this.runAfterSaveBillingInvoice();

          if (obj['Success']) {
            this.toastService.success(
              `${ptRecord.Code} has been successfully cancelled.`
            );
          } else {
            this.toastService.danger(obj.message);
          }

          this.loading = false;
          resolve(obj);

          this.onCancel.emit();
        })
        .catch((obj) => {

          this.runAfterSaveBillingInvoice();

          this.loading = false;

          this.toastService.danger(
            `Unable to cancel ${ptRecord.Code}. Please try again...`
          );

          reject('Saving Failed....');
        });
    });
  }
  row_OnClick(rowData: any) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      ['PaymentTransaction', rowData.ID],
      {}
    );
  }


  async runAfterSaveBillingInvoice() {

    this.ds.execSP(
      'pReRunBillingInvoiceAfterSavedDummy',
      {
        IDs_BillingInvoice: [this.ID],
      },
      {
        isReturnObject: true,
        isTransaction: true,
      } )
      .then(async (obj) => {

      })
      .catch(() => {

      });
  }
}
