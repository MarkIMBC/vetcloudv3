import { FormsModule } from '@angular/forms';
import { ModalModule } from './../../shared/modal/modal.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingInvoiceDetailRoutingModule } from './billing-invoice-detail-routing.module';
import { BillingInvoiceDetailComponent } from './billing-invoice-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BillingPaymentDialogComponent } from './billing-payment-dialog/billing-payment-dialog.component';
import { BIPaymentHistoryTableComponent } from './bi-payment-history-table/bi-payment-history-table.component';
import { PatientTableComponent } from './patient-table/patient-table.component';
import { BillingInvoiceSMSRemiderListComponent } from './billing-invoice-smsremider-list/billing-invoice-smsremider-list.component';

@NgModule({
  declarations: [BillingInvoiceDetailComponent, BillingPaymentDialogComponent, BIPaymentHistoryTableComponent, PatientTableComponent, BillingInvoiceSMSRemiderListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BillingInvoiceDetailRoutingModule
  ]
})
export class BillingInvoiceDetailModule { }
