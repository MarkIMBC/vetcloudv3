import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingInvoiceSMSRemiderListComponent } from './billing-invoice-smsremider-list.component';

describe('BillingInvoiceSMSRemiderListComponent', () => {
  let component: BillingInvoiceSMSRemiderListComponent;
  let fixture: ComponentFixture<BillingInvoiceSMSRemiderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillingInvoiceSMSRemiderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingInvoiceSMSRemiderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
