import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'billing-invoice-smsremider-list',
  templateUrl: './billing-invoice-smsremider-list.component.html',
  styleUrls: ['./billing-invoice-smsremider-list.component.less'],
})
export class BillingInvoiceSMSRemiderListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any = {};
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID_BillingInvoice: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItems() {
    var menuItem_New = {
      label: 'Create',
      icon: 'fa fa-plus',
      visible: true,
    };

    var menuItem_View = {
      label: 'View',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true,
    };

    this.menuItems = [];

    if (this.CurrentObject.ID < 0) return;
    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_New);
    this.menuItems.push(menuItem_Refresh);
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create') {
      this.doCreate();
    } else if (menuItem.label == 'Refresh') {
      this.doRefresh();
    }
  }

  doCreate() {}

  doRefresh() {
    this.load(this.ID_BillingInvoice);
  }

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async load(ID_BillingInvoice: number) {
    this.menuItems = [];

    this.ID_BillingInvoice = ID_BillingInvoice;

    var sql = `SELECT *
               FROM dbo.vBillingInvoice_SMSPayableRemider_Listview
               WHERE
                ID_BillingInvoice = ${this.ID_BillingInvoice}
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;

      this.loadMenuItems();
    });
  }

  onRowClick(rowData: any) {
    if (this.IsDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }
  }
}
