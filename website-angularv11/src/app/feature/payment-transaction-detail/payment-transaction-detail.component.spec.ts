import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentTransactionDetailComponent } from './payment-transaction-detail.component';

describe('PaymentTransactionDetailComponent', () => {
  let component: PaymentTransactionDetailComponent;
  let fixture: ComponentFixture<PaymentTransactionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentTransactionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentTransactionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
