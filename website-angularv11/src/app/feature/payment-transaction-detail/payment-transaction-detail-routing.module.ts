import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentTransactionDetailComponent } from './payment-transaction-detail.component';

const routes: Routes = [{ path: '', component: PaymentTransactionDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentTransactionDetailRoutingModule { }
