import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PaymentTransactionDetailRoutingModule } from './payment-transaction-detail-routing.module';
import { PaymentTransactionDetailComponent } from './payment-transaction-detail.component';

@NgModule({
  declarations: [PaymentTransactionDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PaymentTransactionDetailRoutingModule
  ]
})
export class PaymentTransactionDetailModule { }
