import {
  AlignEnum,
  FilingStatusEnum,
  FilterCriteriaType,
  IFormValidation,
  ItemTypeEnum,
  PaymentMethodEnum,
  TaxSchemeEnum,
} from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { DataService } from 'src/app/core/data.service';

@Component({
  selector: 'app-payment-transaction-detail',
  templateUrl: './payment-transaction-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './payment-transaction-detail.component.less',
  ],
})
export class PaymentTransactionDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'PaymentTransaction';
  headerTitle: string = 'Payment Transaction';

  @ViewChild('paymentCashTemplate') paymentCashTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentGCashTemplate') paymentGCashTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentDebitCreditTemplate') paymentDebitCreditTemplate:
    | TemplateRef<any>
    | undefined;
  @ViewChild('paymentCheckTemplate') paymentCheckTemplate:
    | TemplateRef<any>
    | undefined;

  paymentTemplate: any = null;

  displayMember: string = 'Code';

  ID_PaymentMethod_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `/*encryptsqlstart*/
            SELECT * FROM tPaymentMethod WHERE IsActive = 1
            AND ID IN (${PaymentMethodEnum.Cash}, ${PaymentMethodEnum.GCash}, ${PaymentMethodEnum.Check}, ${PaymentMethodEnum.DebitCredit})
            /*encryptsqlend*/
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Payment Method',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
      ],
    },
  };

  ID_CardType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `/*encryptsqlstart*/
            SELECT * FROM tCardType
            /*encryptsqlend*/
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Type',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
      ],
    },
  };

  ID_BillingInvoice_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: ``,
      columns: [
        {
          name: 'Code',
          caption: 'BI #',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
        {
          name: 'Name_Client',
          caption: 'Client',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Patient',
          caption: 'Patient',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Payment_Name_FilingStatus',
          caption: 'Status',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.center,
        },
      ],
    },
  };

  DetailView_onLoad() {
    if (this.CurrentObject.PaymentTransaction_Detail == null) {
      this.CurrentObject.PaymentTransaction_Detail = [];
    }

    var listViewOptionID_BillingInvoice =
      this.ID_BillingInvoice_LookupboxOption.listviewOption;

    if (listViewOptionID_BillingInvoice != undefined) {
      listViewOptionID_BillingInvoice.sql = `/*encryptsqlstart*/
                                                SELECT  ID,
                                                    Code,
                                                    Name_Patient,
                                                    ISNULL(Name_Client, WalkInCustomerName) Name_Client,
                                                    RemainingAmount,
                                                    Payment_Name_FilingStatus
                                                FROM dbo.vBillingInvoice
                                                WHERE
                                                    Payment_ID_FilingStatus IN (${FilingStatusEnum.Pending}, ${FilingStatusEnum.PartiallyPaid}) AND
                                                    ID_Company = ${this.currentUser.ID_Company} AND
                                                    ID_FilingStatus IN (${FilingStatusEnum.Approved})
                                            /*encryptsqlend*/`;
    }

    this.displayPaymentMethodField(this.CurrentObject.ID_PaymentMethod);
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuItemApprove = {
      label: 'Approve',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    };

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'PT Report',
          name: 'ptreport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Receipt',
          name: 'posreceipt',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        },
      ],
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID < 1) return;

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(menuItemApprove);
        this.addMenuItem(menuItemCancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        this.addMenuItem(menuItemCancel);
      }

      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.label == 'Approve') {
      this.doApprove();
    } else if (e.item.label == 'Cancel') {
      this.doCancel();
    } else if (e.item.name == 'ptreport') {
      this.customNavigate(['Report'], {
        ReportName: 'PaymentTransactionReport',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == 'posreceipt') {
      PaymentTransactionDetailComponent.PrintDialog(
        this._dataServices,
        this.CurrentObject.ID
      );
    }
  }

  async doApprove() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before approving ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pApprovePaymentTransaction',
        {
          IDs_PaymentTransaction: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {

        this.runAfterSaveBillingInvoice();

        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been approved successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }



        this.loading = false;
      })
      .catch(() => {

        this.runAfterSaveBillingInvoice();

        this.toastService.danger(
          `Unable to approve ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before canceling ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
        'pCancelPaymentTransaction',
        {
          IDs_PaymentTransaction: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {

        this.runAfterSaveBillingInvoice();

        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been cancelled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {

        this.runAfterSaveBillingInvoice();

        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_BillingInvoice = this.CurrentObject.ID_BillingInvoice;
    if (
      this.CurrentObject.ID_BillingInvoice == undefined ||
      this.CurrentObject.ID_BillingInvoice == null
    )
      ID_BillingInvoice = 0;

    var ID_PaymentMethod = this.CurrentObject.ID_PaymentMethod;
    if (
      this.CurrentObject.ID_PaymentMethod == undefined ||
      this.CurrentObject.ID_PaymentMethod == null
    )
      ID_PaymentMethod = 0;

    if (ID_BillingInvoice == 0) {
      validations.push({
        message: 'BI # is required.',
      });
    }

    if (ID_PaymentMethod == 0) {
      validations.push({
        message: 'Payment method is required.',
      });
    }

    if (!this.CurrentObject.Date) {
      validations.push({
        message: 'Date is required.',
      });
    }

    return Promise.resolve(validations);
  }

  compute() {
    PaymentTransactionDetailComponent.Compute(this.CurrentObject);
    this.isDirty = true;
  }

  static Compute(paymentTranCurrentObject: any) {
    var netAmount = 0;
    var cashAmount = 0;
    var checkAmount = 0;

    var gCashAmount = 0;
    var cardAmount = 0;

    var payableAmount = 0;
    var paymentAmount = 0;
    var changeAmount = 0;

    PaymentTransactionDetailComponent.ResetPaymentMethodAmount(
      paymentTranCurrentObject
    );

    netAmount = paymentTranCurrentObject.RemainingAmount_BillingInvoice;
    netAmount = GeneralfxService.roundOffDecimal(netAmount);

    cashAmount = paymentTranCurrentObject.CashAmount;
    checkAmount = paymentTranCurrentObject.CheckAmount;
    gCashAmount = paymentTranCurrentObject.GCashAmount;
    cardAmount = paymentTranCurrentObject.CardAmount;

    payableAmount = netAmount;
    paymentAmount = cashAmount + checkAmount + gCashAmount + cardAmount;
    paymentAmount = GeneralfxService.roundOffDecimal(paymentAmount);

    if (payableAmount < paymentAmount) {
      changeAmount = paymentAmount - payableAmount;
    }

    changeAmount = GeneralfxService.roundOffDecimal(changeAmount);

    paymentTranCurrentObject.PayableAmount = payableAmount;
    paymentTranCurrentObject.PaymentAmount = paymentAmount;
    paymentTranCurrentObject.ChangeAmount = changeAmount;
  }

  static ResetPaymentMethodAmount(paymentTranCurrentObject: any) {
    if (paymentTranCurrentObject.ID_PaymentMethod != PaymentMethodEnum.Cash) {
      paymentTranCurrentObject.CashAmount = 0.0;
    }

    if (paymentTranCurrentObject.ID_PaymentMethod != PaymentMethodEnum.Check) {
      paymentTranCurrentObject.CheckNumber = '';
      paymentTranCurrentObject.CheckAmount = 0.0;
    }

    if (paymentTranCurrentObject.ID_PaymentMethod != PaymentMethodEnum.GCash) {
      paymentTranCurrentObject.ReferenceTransactionNumber = '';
      paymentTranCurrentObject.GCashAmount = 0.0;
    }

    if (
      paymentTranCurrentObject.ID_PaymentMethod != PaymentMethodEnum.DebitCredit
    ) {
      paymentTranCurrentObject.ID_CardType = 0;
      paymentTranCurrentObject.CardNumber = '';
      paymentTranCurrentObject.CardNumber = '';
      paymentTranCurrentObject.CardHolderName = '';
      paymentTranCurrentObject.CardAmount = 0.0;
    }
  }

  static async PrintDialog(ds: DataService, ID_PaymentTransaction: number) {
    var obj = await ds.execSP(
      'pGetPaymentTransactionPrintReceiptLayout',
      {
        ID_PaymentTransaction: ID_PaymentTransaction,
      },
      {
        isReturnObject: true,
      }
    );

    var w = 400;
    var h = 800;

    const dualScreenLeft =
      window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    const dualScreenTop =
      window.screenTop !== undefined ? window.screenTop : window.screenY;

    const width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
      ? document.documentElement.clientWidth
      : screen.width;
    const height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
      ? document.documentElement.clientHeight
      : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - w) / 2 / systemZoom + dualScreenLeft;
    const top = (height - h) / 2 / systemZoom + dualScreenTop;

    var mywindow: any = window.open(
      '',
      'PRINT',
      `
      scrollbars=yes,
      width=${w / systemZoom},
      height=${h / systemZoom},
      top=10,
      left=${left}
    `
    );

    mywindow.document.write(`

      <html>
        <head>
          <title>${obj.title}</title>
          <style>${obj.style}</style>
        </head>
        <body>
          ${obj.content}
        </body>
      </html>
    `);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.onload = function () {
      var isMobile =
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        );

      if (obj.isAutoPrint) {
        setTimeout(async () => {
          mywindow.print();
          if (!isMobile) mywindow.close();
        }, obj.millisecondDelay);
      }
    };

    return true;
  }

  PaymentTransactionDetailTable_onChanged(e: any) {
    this.compute();
  }

  public CurrentObject_onBeforeSaving() {
    this.compute();
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_BillingInvoice') {
      if (e.data) {
        var data: any = e.data;
        this.CurrentObject.Name_Client = data['Name_Client'];
        this.CurrentObject.Name_Patient = data['Name_Patient'];
        this.CurrentObject.Code_BillingInvoice = data['Code'];
        this.CurrentObject.RemainingAmount_BillingInvoice =
          data['RemainingAmount'];
        this.CurrentObject.RemainingAmount = data['RemainingAmount'];
      }

      this.CurrentObject.PaymentTransaction_Detail = [];
    } else if (e.name == 'ID_PaymentMethod') {
      this.displayPaymentMethodField(this.CurrentObject.ID_PaymentMethod);
    }

    this.compute();
  }

  displayPaymentMethodField(ID_PaymentMethod: PaymentMethodEnum) {
    if (ID_PaymentMethod == PaymentMethodEnum.Cash) {
      this.paymentTemplate = this.paymentCashTemplate;
    } else if (ID_PaymentMethod == PaymentMethodEnum.DebitCredit) {
      this.paymentTemplate = this.paymentDebitCreditTemplate;
    } else if (ID_PaymentMethod == PaymentMethodEnum.GCash) {
      this.paymentTemplate = this.paymentGCashTemplate;
    } else if (ID_PaymentMethod == PaymentMethodEnum.Check) {
      this.paymentTemplate = this.paymentCheckTemplate;
    } else {
      this.paymentTemplate = null;
    }
  }

  async runAfterSaveBillingInvoice() {

    this.execSP(
      'pReRunBillingInvoiceAfterSavedDummy',
      {
        IDs_BillingInvoice: [this.CurrentObject.ID_BillingInvoice],
      },
      {
        isReturnObject: true,
        isTransaction: true,
      } )
      .then(async (obj: any) => {

      })
      .catch(() => {

      });
  }
}
