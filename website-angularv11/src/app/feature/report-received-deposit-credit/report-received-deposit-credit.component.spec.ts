import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportReceivedDepositCreditComponent } from './report-received-deposit-credit.component';

describe('ReportReceivedDepositCreditComponent', () => {
  let component: ReportReceivedDepositCreditComponent;
  let fixture: ComponentFixture<ReportReceivedDepositCreditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportReceivedDepositCreditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportReceivedDepositCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
