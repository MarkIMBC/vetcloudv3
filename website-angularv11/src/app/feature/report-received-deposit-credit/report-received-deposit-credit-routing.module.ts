import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportReceivedDepositCreditComponent } from './report-received-deposit-credit.component';

const routes: Routes = [{ path: '', component: ReportReceivedDepositCreditComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportReceivedDepositCreditRoutingModule { }
