import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportReceivedDepositCreditRoutingModule } from './report-received-deposit-credit-routing.module';
import { ReportReceivedDepositCreditComponent } from './report-received-deposit-credit.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportReceivedDepositCreditComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportReceivedDepositCreditRoutingModule
  ]
})
export class ReportReceivedDepositCreditModule { }
