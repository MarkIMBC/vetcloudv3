import { Component } from '@angular/core';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-received-deposit-credit',
  templateUrl: './report-received-deposit-credit.component.html',
  styleUrls: ['./report-received-deposit-credit.component.less'],
})
export class ReportReceivedDepositCreditComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'ReceivedDepositCredits',
  };

  onLoad() {}

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    var hasFilter = false;

    if (this.CurrentObject['DateStart']) {
      var _dateStart = this.CurrentObject['DateStart'];

      if (this.CurrentObject['DateEnd']) {
        var _dateEnd = this.CurrentObject['DateEnd'];

        if (_dateStart != null && _dateEnd == null) {
          filterValues.push({
            dataField: 'Date',
            filterCriteriaType: FilterCriteriaType.Like,
            propertyType: PropertyTypeEnum.Date,
            value: [_dateStart, _dateStart],
          });
        } else {
          filterValues.push({
            dataField: 'Date',
            filterCriteriaType: FilterCriteriaType.Like,
            propertyType: PropertyTypeEnum.Date,
            value: [_dateStart, _dateEnd],
          });
        }
      }

      hasFilter = true;
    }

    if (this.CurrentObject['Name_Client']) {
      filterValues.push({
        dataField: 'Name_Client',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Client'],
      });

      hasFilter = true;
    }

    /** Default Filter **/
    if (!hasFilter) {
      if (!this.CurrentObject['DateStart']) {
        filterValues.push({
          dataField: 'Date',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [new Date(), new Date()],
        });
      }
    }

    return filterValues;
  }
}
