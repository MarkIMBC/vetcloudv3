import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  FilingStatusEnum,
  PayablePayment_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';

@Component({
  selector: 'payment-dialog',
  templateUrl: './payment-dialog.component.html',
  styleUrls: ['./payment-dialog.component.less'],
})
export class PaymentDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;
  title: string = '';

  protected _tempID: number = 0;

  Filed_FilingStatus: number = FilingStatusEnum.Filed;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = new PayablePayment_DTO();
  PreviousObject: any = new PayablePayment_DTO();

  private _ID_PayablePayment: number = 0;

  menuItems: AdminLTEMenuItem[] = [];

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {}

  async show(ID_PayablePayment: number, ID_Payable: number): Promise<any> {
    var modalDialog = this.modalDialog;

    this._ID_PayablePayment = ID_PayablePayment;

    await this.loadRecord(ID_PayablePayment, ID_Payable);

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  getTempID(): number {
    this._tempID -= 1;
    return this._tempID;
  }

  async loadRecord(ID_PayablePayment: number, ID_Payable: number) {
    var _ = this;
    this._ID_PayablePayment = ID_PayablePayment;

    var obj = await this.ds.execSP(
      'pGetPayablePayment',
      {
        ID: this._ID_PayablePayment,
        ID_Payable: ID_Payable,
        ID_FilingStatus: FilingStatusEnum.ForApprove,
        ID_Session: this.currentUser.ID_Session,
      },
      {
        isReturnObject: true,
      }
    );

    this.PreviousObject = JSON.parse(JSON.stringify(obj));
    this.CurrentObject = JSON.parse(JSON.stringify(obj));

    if (this.CurrentObject.ID > 0) {
      this.title = '';
    } else {
      this.title = 'Pay ' + this.CurrentObject.Name_Payable_Detail + '';
    }
  }

  private loadMenuItems() {
    this.menuItems = [];

    if (this.CurrentObject.ID > 0) {
      if (
        this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Filed ||
        this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending
      ) {
        this.menuItems.push({
          label: 'Cancel',
          icon: 'fa fa-times',
          class: 'text-danger',
          visible: true,
        });
      }
    }
  }

  isHideSaveButton: boolean = false;

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Filed &&
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Pending
    ) {
      isDisabled = true;
    }

    this.isHideSaveButton = !isDisabled;

    return isDisabled;
  }

  private compute() {
    var totalAmount: number = 0;

    totalAmount = this.CurrentObject.CashAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }

    this.compute();
  }

  async validation(): Promise<boolean> {
    var validations: IFormValidation[] = [];

    var isValid = true;

    if (this.CurrentObject.TotalAmount < 1) {
      validations.push({
        message: `Amount must be valid.`,
      });
    }

    if (this.CurrentObject.RemaningAmount < this.CurrentObject.TotalAmount) {
      validations.push({
        message: `Amount must not more than Remaining Amount.`,
      });
    }

    if (validations.length > 0) {
      this.toastService.warning(validations[0].message);
      isValid = false;
    }

    return isValid;
  }

  public async save() {
    if (this.CurrentObject['ID_Company'] == null && this.CurrentObject.ID < 1) {
      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    this.loading = true;

    var r = await this.ds.saveObject(
      'EFD5E1AC-D635-403D-9FDF-561135C50B49',
      this.CurrentObject,
      this.PreviousObject,
      []
    );
    var id = (r.key + '').replace("'", '');
    var ID_CurrentObject = parseInt(id);
    this._ID_PayablePayment = ID_CurrentObject;

    this.loading = false;
    return true;
  }

  menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      //this.cancel();
    }
  }

  async btnChange_onClick() {
    var isValid = await this.validation();
    if (isValid) {
      var isSaved = await this.save();

      if (isSaved) {
        this.toastService.success('Saving Successfull', 'Payment');
        if (this.modalDialog != undefined) {
          this.modalDialog.close();
        }
      }
    }
  }

  btnClose_onClick() {
    if (this.modalDialog != undefined) {
      this.modalDialog.close();
    }
  }
}
