import { PaymentDialogComponent } from './payment-dialog/payment-dialog.component';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, ViewChild, OnInit } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Payable_DTO, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { PayableDetailDialogComponent } from './payable-detail-dialog/payable-detail-dialog.component';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import * as moment from 'moment';

@Component({
  selector: 'app-payable-list',
  templateUrl: './payable-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './payable-list.component.less',
  ],
})
export class PayableListComponent extends BaseListViewComponent {
  @ViewChild('payabledetaildialog') payabledetaildialog:
    | PayableDetailDialogComponent
    | undefined;
  @ViewChild('paymentdialog') paymentdialog: PaymentDialogComponent | undefined;
  CustomComponentName: string = 'PayableListComponent';

  OrderByString: string = 'Date DESC';

  ID_ExpenseCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM vActiveExpenseCategory',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
      orderByString: 'Name ASC',
    },
  };
  items: AdminLTEMenuItem[] = [
    {
      label: 'Add Expense',
      icon: 'fas fa-file',
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  headerTitle: string = 'Expenses';

  InitCurrentObject: any = {
    Date: '',
    Name_Payable_Detail: '',
    Name_ExpenseCategory: '',
    ID_ExpenseCategory: 0,
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Expense',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vPayable_PayableDetail_Listview
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString} AND
              1 = 1
            /*encryptsqlend*/
          `;

    this.getRecordPaging(sql);
  }

  menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == 'Refresh') {
      this.loadRecords();
    } else if (menuItem.label == 'Add Expense') {
      this.menuItem_New_onClick();
    }
  }

  getFilterString(): string {
    var filterString = '';

    this.initializeValue(
      this.CurrentObject['Name_Payable_Detail'],
      PropertyTypeEnum.String
    );
    this.initializeValue(
      this.CurrentObject['ID_ExpenseCategory'],
      PropertyTypeEnum.Int
    );

    if (this.CurrentObject.Name_Payable_Detail.length > 0) {
      filterString += `Name_Payable_Detail LIKE '%${this.CurrentObject.Name_Payable_Detail}%'`;
    }

    if (this.CurrentObject.ID_ExpenseCategory > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_ExpenseCategory = ${this.CurrentObject.ID_ExpenseCategory}`;
    }

    if (this.CurrentObject['DateStart_Date']) {
      var _dateStart = this.CurrentObject['DateStart_Date'];
      var _dateEnd = this.CurrentObject['DateEnd_Date'];

      if (filterString.length > 0) filterString = filterString + ' AND ';

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          'YYYY-MM-DD'
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    }

    // /** Default Filter **/
    // var hasAssignedFilter = filterString.length > 0;

    // if (!this.CurrentObject['DateStart_Date'] && filterString.length == 0) {
    //   if (filterString.length > 0) filterString = filterString + ' AND ';

    //   filterString += `
    //         CONVERT(DATE, DateStart) BETWEEN
    //           '${moment(_dateStart).format('YYYY-MM-DD')}' AND
    //           '${moment(_dateEnd).format('YYYY-MM-DD')}'
    //       `;
    // }

    return filterString;
  }

  async Row_OnClick(rowData: Payable_DTO) {
    if (rowData.ID) {
      if (this.payabledetaildialog) {
        await this.payabledetaildialog.show(rowData.ID);
        this.loadRecords();
      }
    }
  }

  async menuItem_New_onClick() {
    if (this.payabledetaildialog) {
      await this.payabledetaildialog.show(-1);
      this.loadRecords();
    }
  }
  async BtnPayment_OnClick(rowData: Payable_DTO) {
    if (rowData.ID) {
      if (this.paymentdialog) {
        await this.paymentdialog.show(-1, rowData.ID);
        this.loadRecords();
      }
    }
  }
  RowCellAction_CreateAppointment_OnClick(rowData: Payable_DTO) {
    this.customNavigate(['PatientAppointment', -1], {
      ID_Payable: rowData.ID,
    });
  }

  RowCellAction_CreateBilling_OnClick(rowData: Payable_DTO) {
    this.customNavigate(['BillingInvoice', -1], {
      ID_Payable: rowData.ID,
    });
  }

  RowCellAction_CreatePet_OnClick(rowData: Payable_DTO) {
    this.customNavigate(['Patient', -1], {
      ID_Payable: rowData.ID,
    });
  }

  openNewTab(rowData: any) {
    var config: any[] = this.getCustomNavigateCommands(
      ['Payable', rowData.ID],
      {}
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }
}
