import { PayablePaymentTableComponent } from './payable-payment-table/payable-payment-table.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  FilingStatusEnum,
  Payable_Detail_DTO,
  Payable_DTO,
  PropertyTypeEnum,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { CurrentObjectOnValueChangeArg } from '../../../layout/base-detail-view/base-detail-view.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { Payable_Detail } from 'src/shared/APP_MODELS';

@Component({
  selector: 'payable-detail-dialog',
  templateUrl: './payable-detail-dialog.component.html',
  styleUrls: ['./payable-detail-dialog.component.less'],
})
export class PayableDetailDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;
  @ViewChild('payablepaymenttable') payablepaymenttable:
    | PayablePaymentTableComponent
    | undefined;

  loading: boolean = false;
  title: string = '';
  protected _tempID: number = 0;

  Filed_FilingStatus: number = FilingStatusEnum.Filed;
  Pending_FilingStatus: number = FilingStatusEnum.Pending;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = new Payable_DTO();
  PreviousObject: any = new Payable_DTO();

  selectedPayableDetail: any = new Payable_Detail_DTO();

  private _ID_Payable: number = 0;

  ID_ExpenseCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tExpenseCategory',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
      orderByString: 'Name ASC',
    },
  };

  menuItems: AdminLTEMenuItem[] = [];

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {}

  async show(ID_Payable: number): Promise<any> {
    var modalDialog = this.modalDialog;

    this.currentTabIndex = 0;

    this._ID_Payable = ID_Payable;

    await this.loadRecord(ID_Payable);

    if (this.payablepaymenttable) {
      this.payablepaymenttable.ID_Payable = ID_Payable;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  getTempID(): number {
    this._tempID -= 1;
    return this._tempID;
  }

  async loadRecord(ID_Payable: number) {
    var _ = this;
    this._ID_Payable = ID_Payable;

    var obj = await this.ds.execSP(
      'pGetPayable',
      {
        ID: this._ID_Payable,
        ID_Session: this.currentUser.ID_Session,
      },
      {
        isReturnObject: true,
      }
    );

    this.PreviousObject = JSON.parse(JSON.stringify(obj));
    this.CurrentObject = JSON.parse(JSON.stringify(obj));

    if (!this.CurrentObject.Payable_Detail)
      this.CurrentObject['Payable_Detail'] = [];
    if (this.CurrentObject.Payable_Detail.length == 0) {
      this.CurrentObject.Payable_Detail.push({
        ID: _.getTempID(),
        Name: '',
        ID_ExpenseCategory: null,
        Amount: 0,
      });
    }

    this.selectedPayableDetail = this.CurrentObject.Payable_Detail[0];
    if (this.CurrentObject.ID > 0) {
      if (
        this.CurrentObject.Payment_ID_FilingStatus !== FilingStatusEnum.Filed &&
        this.CurrentObject.Payment_ID_FilingStatus !== FilingStatusEnum.Pending
      ) {
        this.title = `(${this.CurrentObject.Payment_Name_FilingStatus}) Expense`;
      } else {
        this.title = 'Edit Expense';
      }
    } else {
      this.title = 'Add Expense';
    }

    this.formDisabled();

    this.loadMenuItems();
  }

  private loadMenuItems() {
    this.menuItems = [];

    if (this.CurrentObject.ID > 0) {
      if (
        this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Filed ||
        this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending
      ) {
        this.menuItems.push({
          label: 'Cancel',
          icon: 'fa fa-times',
          class: 'text-danger',
          visible: true,
        });
      }
    }
  }

  isHideSaveButton: boolean = false;

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Filed &&
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Pending
    ) {
      isDisabled = true;
    }

    return isDisabled;
  }

  private compute() {
    var totalAmount: number = 0;

    this.CurrentObject.Payable_Detail.forEach((payableDetail: any) => {
      totalAmount += payableDetail.Amount;
    });

    this.CurrentObject.RemaningAmount = totalAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }

    this.compute();
  }

  selectedPayableDetail_onModelChanged(e: IControlModelArg) {
    this.selectedPayableDetail[e.name] = e.value;

    if (e.displayName != undefined) {
      this.selectedPayableDetail[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }

    this.compute();
  }
  async validation(): Promise<boolean> {
    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject.TotalAmount < 1) {
      validationsAppForm.push({
        message: `Amount must be valid.`,
      });
    }

    if (this.CurrentObject.RemaningAmount < this.CurrentObject.TotalAmount) {
      validationsAppForm.push({
        message: `Amount must not more than Remaining Amount.`,
      });
    }

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  public async save() {
    if (this.CurrentObject['ID_Company'] == null && this.CurrentObject.ID < 1) {
      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    this.loading = true;

    var r = await this.ds.saveObject(
      '48FB7081-BDE7-48A9-8E7D-EC060F5436BC',
      this.CurrentObject,
      this.PreviousObject,
      []
    );
    var id = (r.key + '').replace("'", '');
    var ID_CurrentObject = parseInt(id);
    this._ID_Payable = ID_CurrentObject;

    this.loading = false;
    return true;
  }

  menubar_OnClick(e: any) {
    if (e.item.label == 'Cancel') {
      this.cancel();
    }
  }

  async cancel(): Promise<any> {
    this.loading = true;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pCancelPayable',
        {
          IDs_Payable: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.loading = false;
        this.toastService.success(
          `${this.selectedPayableDetail.Name} has been canceled successfully.`,
          `Canceled Payable`
        );

        if (this.modalDialog) {
          this.modalDialog.close();
        }
        res(obj);
      } else {
        this.loading = false;
        this.toastService.danger(obj.message, `Failed to Canceled Payable`);
        rej(obj);
      }
    });
  }
  async btnChange_onClick() {
    var isValid = await this.validation();
    if (isValid) {
      var isSaved = await this.save();

      if (isSaved) {
        this.toastService.success('Saving Successfull', 'Expense');
        if (this.modalDialog != undefined) {
          this.modalDialog.close();
        }
      }
    }
  }

  btnClose_onClick() {
    if (this.modalDialog != undefined) {
      this.modalDialog.close();
    }
  }

  currentTabIndex = 0;
  navlink_onClick(tabIndex: number) {
    this.currentTabIndex = tabIndex;
  }
}
