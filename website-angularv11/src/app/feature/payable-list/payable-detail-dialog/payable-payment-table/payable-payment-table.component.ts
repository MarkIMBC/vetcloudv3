import { Component, Input, OnInit } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'payable-payment-table',
  templateUrl: './payable-payment-table.component.html',
  styleUrls: ['./payable-payment-table.component.less'],
})
export class PayablePaymentTableComponent implements OnInit {
  dataSource: any[] = [];

  private __ID_CurrentObject: number = -1;
  private _ID_Payable: number = -1;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  @Input() set ID_Payable(value: number) {
    this._ID_Payable = value;
    this.load();
  }

  async load() {
    this.dataSource = [];

    if (this._ID_Payable == null || this._ID_Payable == undefined) return;

    var sql = this.cs.encrypt(`
            SELECT  ID,
                    Date,
                    TotalAmount
            FROM dbo.tPayablePayment
            WHERE
              ID_Payable = ${this._ID_Payable} AND
              ID_FilingStatus IN (${FilingStatusEnum.Approved})
          `);

    this.dataSource = await this.ds.query<any>(sql);
  }

  async btnCancelPayment(record: any) {
    await this.doCancel(record.ID);

    this.load();
  }
  async doCancel(ID_PayablePayment: number): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pCancelPayablePayment',
        {
          IDs_PayablePayment: [ID_PayablePayment],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(`Cancel Payment is successfull`, `Pay`);
        res(obj);
      } else {
        this.toastService.danger(`Cancel Payment is failed`, `Pay`);
        rej(obj);
      }
    });
  }
}
