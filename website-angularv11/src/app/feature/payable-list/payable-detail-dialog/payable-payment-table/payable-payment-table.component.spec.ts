import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayablePaymentTableComponent } from './payable-payment-table.component';

describe('PayablePaymentTableComponent', () => {
  let component: PayablePaymentTableComponent;
  let fixture: ComponentFixture<PayablePaymentTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayablePaymentTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayablePaymentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
