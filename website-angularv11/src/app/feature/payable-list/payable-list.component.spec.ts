import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayableListComponent } from './payable-list.component';

describe('PayableListComponent', () => {
  let component: PayableListComponent;
  let fixture: ComponentFixture<PayableListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayableListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayableListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
