import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';

import { PayableListRoutingModule } from './payable-list-routing.module';
import { PayableListComponent } from './payable-list.component';
import { PayableDetailDialogComponent } from './payable-detail-dialog/payable-detail-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { PaymentDialogComponent } from './payment-dialog/payment-dialog.component';
import { PayablePaymentTableComponent } from './payable-detail-dialog/payable-payment-table/payable-payment-table.component';

@NgModule({
  declarations: [PayableListComponent, PayableDetailDialogComponent, PaymentDialogComponent, PayablePaymentTableComponent],
  imports: [
    CommonModule,
    SharedModule,
    ModalModule,
    PayableListRoutingModule,
  ],
  providers: [DataService, CrypterService],
})
export class PayableListModule {}
