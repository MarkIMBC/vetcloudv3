import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PayableListComponent } from './payable-list.component';

const routes: Routes = [{ path: '', component: PayableListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayableListRoutingModule { }
