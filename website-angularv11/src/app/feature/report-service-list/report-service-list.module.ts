import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportServiceListRoutingModule } from './report-service-list-routing.module';
import { ReportServiceListComponent } from './report-service-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportServiceListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportServiceListRoutingModule
  ]
})
export class ReportServiceListModule { }
