import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportServiceListComponent } from './report-service-list.component';

const routes: Routes = [{ path: '', component: ReportServiceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportServiceListRoutingModule { }
