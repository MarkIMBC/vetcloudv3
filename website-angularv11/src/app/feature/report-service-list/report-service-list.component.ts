import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-service-list',
  templateUrl: './report-service-list.component.html',
  styleUrls: ['./report-service-list.component.less'],
})
export class ReportServiceListComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'ServiceListReport',
  };

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        Select
          ID, Name
        FROM tItemCategory
        WHERE ID_ItemType = 1

      `,
      orderByString: 'Name ASC',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  onLoad() {}

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }
    if (this.CurrentObject['ID_ItemCategory']) {
      filterValues.push({
        dataField: 'ID_ItemCategory',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_ItemCategory'],
      });
    }

    return filterValues;
  }
}
