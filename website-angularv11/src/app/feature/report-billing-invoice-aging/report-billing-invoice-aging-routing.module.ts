import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportBillingInvoiceAgingComponent } from './report-billing-invoice-aging.component';

const routes: Routes = [{ path: '', component: ReportBillingInvoiceAgingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportBillingInvoiceAgingRoutingModule { }
