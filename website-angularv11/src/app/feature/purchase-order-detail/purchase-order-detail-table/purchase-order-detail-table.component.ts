import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import {
  AlignEnum,
  IControlModelArg,
  ItemTypeEnum,
  Patient_SOAP_Plan_DTO,
  Patient_SOAP_Prescription_DTO,
  PropertyTypeEnum,
  PurchaseOrder_Detail_DTO,
  PurchaseOrder_DTO,
} from './../../../../shared/APP_HELPER';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'purchase-order-detail-table',
  templateUrl: './purchase-order-detail-table.component.html',
  styleUrls: [
    './../../../layout/base-detail-view/base-detail-view.component.less',
    './purchase-order-detail-table.component.less',
  ],
})
export class PurchaseOrderDetailTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() CurrentObject: PurchaseOrder_DTO = {};
  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-plus',
      visible: true,
    },
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {
      this.doAddItemInventoriable();
    }
  }

  async BtnDeletePurchaseOrderDetail_OnClick(detail: any) {
    if (!this.CurrentObject.PurchaseOrder_Detail) return;

    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject.PurchaseOrder_Detail,
      'ID',
      detail.ID + ''
    );

    this.CurrentObject.PurchaseOrder_Detail.splice(index, 1);

    this.onChanged.emit();
  }

  async doAddItemInventoriable() {
    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Code,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vItemInventoriable
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Code',
          caption: 'Code',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'UnitCost',
          caption: 'Cost',
          align: AlignEnum.right,
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
        },
      ],
    });

    var _ = this;

    if (!this.CurrentObject.PurchaseOrder_Detail) return;

    obj.rows.forEach((record: any) => {
      _.tempID--;

      var item = {
        ID: _.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitPrice: record.UnitCost,
      };

      if (this.CurrentObject.PurchaseOrder_Detail == null)
        this.CurrentObject.PurchaseOrder_Detail = [];
      this.CurrentObject.PurchaseOrder_Detail.push(item);
    });

    this.onChanged.emit();
  }

  PurchaseOrderDetail_onModelChanged(
    PurchaseOrderDetail: any,
    e: IControlModelArg
  ) {
    PurchaseOrderDetail[e.name] = e.value;

    this.onChanged.emit();
  }

  PurchaseOrder_onModelChanged(fieldName: string, e: any) {
    if (fieldName == 'DiscountRate') {
      this.CurrentObject.IsComputeDiscountRate = true;
    } else if (fieldName == 'DiscountAmount') {
      this.CurrentObject.IsComputeDiscountRate = false;
    }

    this.onChanged.emit();
  }
}
