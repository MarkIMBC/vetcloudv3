import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { PurchaseOrderDetailRoutingModule } from './purchase-order-detail-routing.module';
import { PurchaseOrderDetailComponent } from './purchase-order-detail.component';
import { PurchaseOrderDetailTableComponent } from './purchase-order-detail-table/purchase-order-detail-table.component';

@NgModule({
  declarations: [PurchaseOrderDetailComponent, PurchaseOrderDetailTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    PurchaseOrderDetailRoutingModule
  ]
})
export class PurchaseOrderDetailModule { }
