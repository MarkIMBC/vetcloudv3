import { FilingStatusEnum, FilterCriteriaType, IFormValidation, ItemTypeEnum, TaxSchemeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-purchase-order-detail',
  templateUrl: './purchase-order-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './purchase-order-detail.component.less'
  ]
})
export class PurchaseOrderDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'PurchaseOrder'
  headerTitle: string = 'Purchase Order'

  displayMember: string = "Code";

  ID_Supplier_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name
            FROM vSupplier
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1`,
      columns: [
        {
          name: 'Name',
          caption: 'Supplier',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  DetailView_onLoad() {

    if (this.CurrentObject.PurchaseOrder_Detail == null) {

      this.CurrentObject.PurchaseOrder_Detail = [];
    }

    var listViewOptionID_Supplier = this.ID_Supplier_LookupboxOption.listviewOption;

    if (listViewOptionID_Supplier != undefined) {
      listViewOptionID_Supplier.sql = `/*encryptsqlstart*/
                                      SELECT
                                        ID,
                                        Name
                                      FROM vSupplier
                                      WHERE
                                        ID_Company = ${this.currentUser.ID_Company} AND
                                        IsActive = 1
                                     /*encryptsqlend*/`;
    }

  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {

    var menuItemApprove = {
      label: 'Approve',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    }

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    }


    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'PO Report',
          name: 'poreport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        }
      ]
    }

    if (this.CurrentObject.ID > 0) {

      if (this.CurrentObject.ID < 1) return;

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(menuItemApprove);
        this.addMenuItem(menuItemCancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        this.addMenuItem(menuItemCancel);
      }

      this.addMenuItem(menuReport);
    }
  }


  async menubar_OnClick(e: any) {

    if (e.item.label == 'Approve') {

      this.doApprove();
    } else if (e.item.label == 'Cancel') {

      this.doCancel();
    } else if (e.item.name == 'poreport') {

      this.customNavigate(['Report'], {
        ReportName: 'PurchaseOrderReport',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }
  }

  async doApprove() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before approving ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.execSP(
      "pApprovePurchaseOrder",
      {
        IDs_PurchaseOrder: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been approved successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to approve ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  doCancel() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before canceling ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.execSP(
      "pCancelPurchaseOrder",
      {
        IDs_PurchaseOrder: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been cancelled successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to cancel ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }


  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var ID_Supplier = this.CurrentObject.ID_Supplier;

    if (this.CurrentObject.ID_Supplier == undefined || this.CurrentObject.ID_Supplier == null) ID_Supplier = 0;

    if (ID_Supplier == 0) {
      validations.push({
        message: "Supplier is required.",
      });
    }

    if (!this.CurrentObject.Date) {
      validations.push({
        message: "Date is required.",
      });
    }

    if (this.CurrentObject.PurchaseOrder_Detail.length == 0) {

      validations.push({
        message: "Add purchase Item atleast one.",
      });
    }

    return Promise.resolve(validations);
  }

  compute() {
    var subTotal = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;

    discountRate = GeneralfxService.roundOffDecimal(discountRate);
    discountAmount = GeneralfxService.roundOffDecimal(discountAmount);

    this.CurrentObject.PurchaseOrder_Detail.forEach((detail: { Amount: number; Quantity: number; UnitPrice: number; ID_PurchaseOrder: any; }) => {
      detail.Amount = detail.Quantity * detail.UnitPrice;
      detail.ID_PurchaseOrder = this.CurrentObject.ID;

      subTotal += detail.Amount;
    });

    totalAmount = GeneralfxService.roundOffDecimal(subTotal);


    if (this.CurrentObject.IsComputeDiscountRate) {
      discountAmount = totalAmount * (discountRate / 100);
    } else {

      if (discountAmount > 0) {

        discountRate = (discountAmount / totalAmount) * 100;
        discountRate = GeneralfxService.roundOffDecimal(discountRate);
      } else {

        discountRate = 0;
      }
    }

    totalAmount = totalAmount - discountAmount;
    totalAmount = GeneralfxService.roundOffDecimal(totalAmount);

    grossAmount = totalAmount;

    vatAmount = (grossAmount / 1.12) * 0.12;
    vatAmount = GeneralfxService.roundOffDecimal(vatAmount);

    if (
      this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
      this.CurrentObject.ID_TaxScheme == 0
    ) {
      vatAmount = 0;
      netAmount = grossAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive) {
      netAmount = grossAmount + vatAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive) {
      netAmount = grossAmount - vatAmount;
    }

    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;

    this.isDirty = true;
  }

  PurchaseOrderDetailTable_onChanged(e: any){

    this.compute();
  }

  public CurrentObject_onBeforeSaving() {

    this.compute();
  }

}
