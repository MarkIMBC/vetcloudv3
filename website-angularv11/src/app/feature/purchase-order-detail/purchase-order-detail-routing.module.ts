import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderDetailComponent } from './purchase-order-detail.component';

const routes: Routes = [{ path: '', component: PurchaseOrderDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderDetailRoutingModule { }
