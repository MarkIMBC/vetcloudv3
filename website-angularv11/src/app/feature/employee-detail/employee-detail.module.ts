import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeDetailRoutingModule } from './employee-detail-routing.module';
import { EmployeeDetailComponent } from './employee-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChangeUserAccountDialogBoxComponent } from '../../shared/change-user-account-dialog-box/change-user-account-dialog-box.component';
import { ModalModule } from 'src/app/shared/modal/modal.module';

@NgModule({
  declarations: [EmployeeDetailComponent],
  imports: [
    CommonModule,
    ModalModule,
    SharedModule,
    EmployeeDetailRoutingModule,
  ],
})
export class EmployeeDetailModule {}
