import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilingStatusEnum,
  FilterCriteriaType,
  IFilterFormValue,
  ItemTypeEnum,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-item-price-comparison-per-supplier',
  templateUrl: './report-item-price-comparison-per-supplier.component.html',
  styleUrls: ['./report-item-price-comparison-per-supplier.component.less']
})
export class ReportItemPriceComparisonPerSupplierComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'ItemPriceComparisonPerSupplier',
  };

  ID_ItemType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  onLoad() {
    var listViewOption_ID_ItemType =
      this.ID_ItemType_LookupboxOption.listviewOption;

    if (listViewOption_ID_ItemType != undefined) {
      listViewOption_ID_ItemType.sql = `
      /*encryptsqlstart*/
      SELECT
        ID,
        Name
      FROM tItemType
      WHERE ID IN (
        ${ItemTypeEnum.Inventoriable},
        ${ItemTypeEnum.Service}
      )
    /*encryptsqlend*/`;
    }
  }


  protected getDefaultFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    filterValues.push({
      alias: 'schema1',
      dataField: 'ID_Company',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return filterValues;
  }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }
    if (this.CurrentObject['ID_InventoryStatus']) {
      filterValues.push({
        dataField: 'ID_InventoryStatus',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_InventoryStatus'],
      });
    }

    return filterValues;
  }
}
