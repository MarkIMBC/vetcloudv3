import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportItemPriceComparisonPerSupplierComponent } from './report-item-price-comparison-per-supplier.component';

const routes: Routes = [{ path: '', component: ReportItemPriceComparisonPerSupplierComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportItemPriceComparisonPerSupplierRoutingModule { }
