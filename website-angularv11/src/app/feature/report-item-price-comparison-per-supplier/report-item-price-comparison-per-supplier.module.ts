import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportItemPriceComparisonPerSupplierRoutingModule } from './report-item-price-comparison-per-supplier-routing.module';
import { ReportItemPriceComparisonPerSupplierComponent } from './report-item-price-comparison-per-supplier.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportItemPriceComparisonPerSupplierComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportItemPriceComparisonPerSupplierRoutingModule
  ]
})
export class ReportItemPriceComparisonPerSupplierModule { }

