import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportItemPriceComparisonPerSupplierComponent } from './report-item-price-comparison-per-supplier.component';

describe('ReportItemPriceComparisonPerSupplierComponent', () => {
  let component: ReportItemPriceComparisonPerSupplierComponent;
  let fixture: ComponentFixture<ReportItemPriceComparisonPerSupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportItemPriceComparisonPerSupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportItemPriceComparisonPerSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
