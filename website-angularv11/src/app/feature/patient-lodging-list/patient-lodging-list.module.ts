import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';


import { PatientLodgingListRoutingModule } from './patient-lodging-list-routing.module';
import { PatientLodgingListComponent } from './patient-lodging-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [PatientLodgingListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientLodgingListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientLodgingListModule { }
