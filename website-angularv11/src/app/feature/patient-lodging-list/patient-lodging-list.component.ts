
import { Item } from './../../../shared/APP_MODELS';
import { Patient_Lodging_DTO } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-patient-lodging-list',
  templateUrl: './patient-lodging-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-lodging-list.component.less'
  ],
})
export class PatientLodgingListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'PatientLodgingListComponent';

  headerTitle: string = 'Patient Lodging';

  InitCurrentObject: any = {
    Name: '',
    Name_ItemCategory: ''
  }

  dataSource: Patient_Lodging_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'PatientLodging',
      visible: true,
      isActive: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vPatient_Lodging_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['Patient_Lodging', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Patient_Lodging', -1], {});
  }



}
