import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientLodgingListComponent } from './patient-lodging-list.component';

const routes: Routes = [{ path: '', component: PatientLodgingListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientLodgingListRoutingModule { }
