import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportInventorySummaryRoutingModule } from './report-inventory-summary-routing.module';
import { ReportInventorySummaryComponent } from './report-inventory-summary.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportInventorySummaryComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportInventorySummaryRoutingModule
  ]
})
export class ReportInventorySummaryModule { }
