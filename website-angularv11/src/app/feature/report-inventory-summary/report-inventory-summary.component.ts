import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-inventory-summary',
  templateUrl: './report-inventory-summary.component.html',
  styleUrls: ['./report-inventory-summary.component.less'],
})
export class ReportInventorySummaryComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'INVENTORYSUMMARYREPORT',
  };

  ID_InventoryStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        Select
          ID, Name
        FROM tInventoryStatus
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  onLoad() {}

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['Name_Item']) {
      filterValues.push({
        dataField: 'Name_Item',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item'],
      });
    }
    if (this.CurrentObject['ID_InventoryStatus']) {
      filterValues.push({
        dataField: 'ID_InventoryStatus',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_InventoryStatus'],
      });
    }

    return filterValues;
  }
}
