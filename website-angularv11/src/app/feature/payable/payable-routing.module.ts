import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PayableComponent } from './payable.component';

const routes: Routes = [{ path: '', component: PayableComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayableRoutingModule { }
