import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientAppointmentDetailComponent } from './patient-appointment-detail.component';

const routes: Routes = [{ path: '', component: PatientAppointmentDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientAppointmentDetailRoutingModule { }
