import {
  FilingStatusEnum,
  IFormValidation,
  ItemTypeEnum,
} from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import {
  AdminLTEDataLookupboxComponent,
  AdminLTEDataLookupboxOption,
} from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-patient-appointment-detail',
  templateUrl: './patient-appointment-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './patient-appointment-detail.component.less',
  ],
})
export class PatientAppointmentDetailComponent extends BaseDetailViewComponent {
  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox:
    | AdminLTEDataLookupboxComponent
    | undefined;

  ModelName: string = 'PatientAppointment';
  headerTitle: string = 'Patient Appointment';

  routerFeatureName: string = 'PatientAppointment';
  displayMember: string = 'Name_Client';

  ID_SOAPType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `SELECT ID, Name FROM tSOAPType`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption =
    {
      listviewOption: {
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
          {
            name: 'Name_Position',
            caption: 'Position',
            propertyType: PropertyTypeEnum.String,
          },
        ],
      },
    };

  Groomer_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadMenuItems() {
    var menuItemDischarge = {
      label: 'Discharge',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    };

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      name: 'btnCancel',
      class: 'text-danger',
      visible: true,
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(menuItemCancel);
      }
    }
  }

  loadRightDrowDownMenu() {
    this.rightDropDownItems = [];

    if (this.CurrentObject.ID_Client > 0) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Client} info`,
        visible: true,
        name: 'viewclient',
      });
    }

    if (this.CurrentObject.ID_Patient > 0) {
      this.rightDropDownItems.push({
        label: `Go to ${this.CurrentObject.Name_Patient} info`,
        visible: true,
        name: 'viewpatient',
      });
    }

    this.rightDropDownItems.push({
      label: 'Create Medical Record',
      visible: true,
      name: 'createsoap',
    });

    this.rightDropDownItems.push({
      label: 'Create Billing Invoice',
      visible: true,
      name: 'createbillinginvoice',
    });
  }

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {
    if (event.item.name == 'viewclient') {
      this.customNavigate(['Client', this.CurrentObject.ID_Client], {});
    } else if (event.item.name == 'viewpatient') {
      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {});
    } else if (event.item.name == 'createsoap') {
      this.customNavigate(['Patient_SOAP', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        ID_SOAPType: this.CurrentObject.ID_SOAPType,
      });
    } else if (event.item.name == 'createbillinginvoice') {
      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        ID_SOAPType: this.CurrentObject.ID_SOAPType,
      });
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name == 'btnCancel') {
      this.doCancel();
    }
  }

  async doCancel() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before cancel ${this.CurrentObject.Code}.`
      );
      return;
    }

    this.loading = true;

    this.execSP(
      'pCanceledPatientAppointment',
      {
        IDs_PatientAppointment: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    )
      .then(async (obj) => {
        if (obj.Success) {
          await this.loadRecord();
          this.toastService.success(
            `${this.CurrentObject.Code} has been canceled successfully.`
          );
        } else {
          this.toastService.danger(obj.message);
        }

        this.loading = false;
      })
      .catch(() => {
        this.toastService.danger(
          `Unable to cancel ${this.CurrentObject.Code}.`
        );
        this.loading = false;
      });
  }

  DetailView_onLoad() {
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;
    var listViewOptionAttendingPhysician_ID_Employee =
      this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionGroomer_ID_Employee =
      this.Groomer_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.vActiveClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vActivePatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;
    }

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }
    if (listViewOptionGroomer_ID_Employee != undefined) {
      listViewOptionGroomer_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (e.name == 'ID_Client') {
      var listViewOptionID_Patient =
        this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }

    this.CurrentObject.IsSentSMS = false;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.ID_Client == null) this.CurrentObject.ID_Client = 0;
    if (this.CurrentObject.ID_Patient == null)
      this.CurrentObject.ID_Patient = 0;

    if (this.CurrentObject.ID_Client == 0) {
      validations.push({
        message: `Client is required.`,
      });
    }

    if (this.CurrentObject.ID_Patient == 0) {
      validations.push({
        message: `Patient is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Client', 'ID_Patient', 'ID_SOAPType'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
