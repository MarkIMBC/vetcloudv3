import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientAppointmentDetailRoutingModule } from './patient-appointment-detail-routing.module';
import { PatientAppointmentDetailComponent } from './patient-appointment-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [PatientAppointmentDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientAppointmentDetailRoutingModule
  ]
})
export class PatientAppointmentDetailModule { }
