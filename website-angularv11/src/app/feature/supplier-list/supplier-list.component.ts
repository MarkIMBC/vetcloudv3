import { ReceivingReport } from './../../../shared/APP_MODELS';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingInvoice, Supplier } from 'src/shared/APP_MODELS';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './supplier-list.component.less',
  ],
})
export class SupplierListComponent extends BaseListViewComponent {

  CustomComponentName: string = 'SupplierListComponent';

  headerTitle: string = 'Supplier';

  InitCurrentObject: any = {
    Name: '',
    ContactNumber: '',
  };

  dataSource: Supplier[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Supplier',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Name
            FROM dbo.vSupplier
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Supplier) {
    this.customNavigate(['Supplier', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Supplier', -1], {});
  }
}
