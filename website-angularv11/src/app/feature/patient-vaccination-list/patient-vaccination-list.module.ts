import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientVaccinationListRoutingModule } from './patient-vaccination-list-routing.module';
import { PatientVaccinationListComponent } from './patient-vaccination-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [PatientVaccinationListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientVaccinationListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientVaccinationListModule { }
