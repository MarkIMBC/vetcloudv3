import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientVaccinationListComponent } from './patient-vaccination-list.component';

const routes: Routes = [{ path: '', component: PatientVaccinationListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientVaccinationListRoutingModule { }
