import {
  PatientSOAPRecordInfoEnum,
  Patient_SOAP_DTO,
} from './../../../shared/APP_HELPER';
import { BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import * as moment from 'moment';
import { Enumerable } from 'linq-typescript';

@Component({
  selector: 'app-patient-soap-list',
  templateUrl: './patient-soap-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-soap-list.component.less',
  ],
})
export class PatientSOAPListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'PatientSOAPListComponent';

  headerTitle: string = 'Patient Medical Record';

  InitCurrentObject: any = {
    Code: '',
    Name_SOAPType: '',
    Name_Client: '',
    Name_Patient: '',
    Name_FilingStatus: '',
    CaseType: '',
    Comment: '',
    DateStart_Date: '',
    DateEnd_Date: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM dbo.vPatient_SOAP_ListView
          /*encryptsqlend*/
            WHERE
                ID_Company = ${this.currentUser.ID_Company}
                ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  dataSource_InitLoad(obj: any) {
    obj.Records.forEach((record: any) => {
      record['SOAPRecords'] = [];

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.PrimaryComplaintOrHistory,
        label: 'Primary Complaint / History',
        value: record.History,
        hasValue: record.Count_History > 0,
        isLoading: false,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.ClinicalExam,
        label: 'Clinical Exam',
        value: record.ClinicalExamination,
        hasValue: record.Count_ClinicalExamination > 0,
        isLoading: false,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation,
        label: 'Laboratory / Interpretation',
        value: record.Interpretation,
        hasValue: false,
        isLoading: true,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.Plan,
        label: 'Plan',
        value: '',
        hasValue: false,
        isLoading: true,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.Diagnosis,
        label: 'Diagnosis',
        value: record.Diagnosis,
        hasValue: record.Count_Diagnosis > 0,
        isLoading: false,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.Treatment,
        label: 'Treatment',
        value: record.Treatment,
        hasValue: false,
        isLoading: true,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.Prescription,
        label: 'Prescription',
        value: record.Prescription,
        hasValue: false,
        isLoading: true,
      });

      record.SOAPRecords.push({
        id: PatientSOAPRecordInfoEnum.ClientCommunication,
        label: 'Client Communication',
        value: record.ClientCommunication,
        hasValue: record.Count_ClientCommunication > 0,
        isLoading: false,
      });
    });

    this.load_Patient_SOAP_RecordInfo(obj, {
      id: PatientSOAPRecordInfoEnum.Plan,
    });

    this.load_Patient_SOAP_RecordInfo(obj, {
      id: PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation,
    });

    this.load_Patient_SOAP_RecordInfo(obj, {
      id: PatientSOAPRecordInfoEnum.Prescription,
    });

    this.load_Patient_SOAP_RecordInfo(obj, {
      id: PatientSOAPRecordInfoEnum.Treatment,
    });
  }

  async load_Patient_SOAP_RecordInfo(obj: any, options: any) {
    this.loading = true;

    var id = -1;
    var sp = '';

    if (options) {
      if (options['id']) {
        id = options['id'];
      }
    }

    switch (id) {
      case PatientSOAPRecordInfoEnum.Plan:
        sp = 'pGet_Patient_SOAP_RecordInfo_Plan';
        break;

      case PatientSOAPRecordInfoEnum.LaboratoryOrInterpretation:
        sp = 'pGet_Patient_SOAP_RecordInfo_LaboratoryImages';
        break;
      case PatientSOAPRecordInfoEnum.Prescription:
        sp = 'pGet_Patient_SOAP_RecordInfo_Prescription';
        break;
      case PatientSOAPRecordInfoEnum.Treatment:
        sp = 'pGet_Patient_SOAP_RecordInfo_Treatment';
        break;
    }

    var IDs_Patient_SOAP = Enumerable.fromSource(obj.Records)
      .select((r: any) => r['ID'])
      .toArray();

    this.ds
      .execSP(
        sp,
        {
          IDs_Patient_SOAP: IDs_Patient_SOAP,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (result) => {
        this.loading = false;

        if (result.Details == null) result.Details = [];

        obj.Records.forEach((record: any) => {
          record.SOAPRecords.forEach((SOAPRecord: any) => {
            if (SOAPRecord.id == id) {
              result.Details.forEach((detail: any) => {
                if (record.ID == detail.ID_Patient_SOAP) {
                  detail.TagString = detail.TagString.replaceAll(
                    '/*break*/ ',
                    '\n'
                  );

                  if (detail.TagString == null || detail.TagString == undefined)
                    detail.TagString = '';

                  SOAPRecord.value = detail.TagString;
                  SOAPRecord.hasValue = detail.TagString.length;
                }

                SOAPRecord.isLoading = false;
              });
            }
          });
        });
      });
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.AttendingPhysician_Name_Employee == null) {
      this.CurrentObject.AttendingPhysician_Name_Employee = '';
    }

    if (this.CurrentObject.AttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `AttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.AttendingPhysician_Name_Employee}%'`;
    }

    if (this.CurrentObject.Code.length > 0) {
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_SOAPType.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_SOAPType LIKE '%${this.CurrentObject.Name_SOAPType}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.AttendingPhysician_Name_Employee.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `AttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.AttendingPhysician_Name_Employee}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%'`;
    }

    if (this.CurrentObject.CaseType.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `CaseType LIKE '%${this.CurrentObject.CaseType}%'`;
    }

    if (this.CurrentObject.Comment.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Comment LIKE '%${this.CurrentObject.Comment}%'`;
    }

    if (this.CurrentObject['DateStart_Date']) {
      var _dateStart = this.CurrentObject['DateStart_Date'];
      var _dateEnd = this.CurrentObject['DateEnd_Date'];

      if (filterString.length > 0) filterString = filterString + ' AND ';

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          'YYYY-MM-DD'
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    }

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (!this.CurrentObject['DateStart_Date'] && filterString.length == 0) {
      if (filterString.length > 0) filterString = filterString + ' AND ';

      filterString += `
        CONVERT(DATE, Date) BETWEEN
          '${moment(_dateStart).format('YYYY-MM-DD')}' AND
          '${moment(_dateEnd).format('YYYY-MM-DD')}'
      `;
    }

    if (!hasAssignedFilter) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ID_FilingStatus NOT IN (4)`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_SOAP_DTO) {
    this.customNavigate(['Patient_SOAP', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Patient_SOAP', -1], {});
  }
}
