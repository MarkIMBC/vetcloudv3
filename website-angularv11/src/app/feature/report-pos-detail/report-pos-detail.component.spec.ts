import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPOSDetailComponent } from './report-pos-detail.component';

describe('ReportPOSDetailComponent', () => {
  let component: ReportPOSDetailComponent;
  let fixture: ComponentFixture<ReportPOSDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportPOSDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPOSDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
