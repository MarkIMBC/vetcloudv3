import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportPOSDetailComponent } from './report-pos-detail.component';

const routes: Routes = [{ path: '', component: ReportPOSDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportPOSDetailRoutingModule { }
