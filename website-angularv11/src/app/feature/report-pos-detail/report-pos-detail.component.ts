import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilingStatusEnum,
  FilterCriteriaType,
  IFilterFormValue,
  ItemTypeEnum,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-pos-detail',
  templateUrl: './report-pos-detail.component.html',
  styleUrls: ['./report-pos-detail.component.less'],
})
export class ReportPOSDetailComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'POSDetail',
  };

  ID_ItemType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  onLoad() {
    var listViewOption_ID_ItemType =
      this.ID_ItemType_LookupboxOption.listviewOption;

    if (listViewOption_ID_ItemType != undefined) {
      listViewOption_ID_ItemType.sql = `
      /*encryptsqlstart*/
      SELECT
        ID,
        Name
      FROM tItemType
      WHERE ID IN (
        ${ItemTypeEnum.Inventoriable},
        ${ItemTypeEnum.Service}
      )
    /*encryptsqlend*/`;
    }
  }


  protected getDefaultFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    filterValues.push({
      alias: 'schema1',
      dataField: 'ID_Company',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return filterValues;
  }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['DateStart_BillingInvoice']) {
      var value = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {
        var valueEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        filterValues.push({
          alias: 'schema1',
          dataField: 'Date',
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [value, valueEnd],
        });
      } else {
        filterValues.push({
          alias: 'schema1',
          dataField: 'Date',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [value, value],
        });
      }
    } else {
      filterValues.push({
        alias: 'schema1',
        dataField: 'Date',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [moment().format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')],
      });
    }

    if (
      this.CurrentObject['ID_ItemType'] != undefined &&
      this.CurrentObject['ID_ItemType'] != null &&
      this.CurrentObject['ID_ItemType'] != 0
    ) {
      var value = this.CurrentObject['ID_ItemType'];

      filterValues.push({
        dataField: 'item.ID_ItemType_QPARAM',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    if (
      this.CurrentObject['Name_Item'] != undefined &&
      this.CurrentObject['Name_Item'] != null &&
      this.CurrentObject['Name_Item'] != ''
    ) {
      var value = this.CurrentObject['Name_Item'];

      filterValues.push({
        dataField: 'item.Name_QPARAM',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });
    }

    if (
      this.CurrentObject['AttendingPhysician_Name_Employee'] != undefined &&
      this.CurrentObject['AttendingPhysician_Name_Employee'] != null &&
      this.CurrentObject['AttendingPhysician_Name_Employee'] != ''
    ) {
      var value = this.CurrentObject['AttendingPhysician_Name_Employee'];

      filterValues.push({
        dataField: '_employee.Name_QPARAM',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });
    }

    filterValues.push({
      alias: 'schema1',
      dataField: 'ID_FilingStatus',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: FilingStatusEnum.Approved,
    });

    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(
      filterValues,
      'dataField',
      'Date'
    );
    if (index > -1) {
      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Bill Date From ${moment(dateStart).format(
        'MM/DD/YYYY'
      )} To ${moment(dateEnd).format('MM/DD/YYYY')}<br/>`;
    }

    if (caption.length > 0) {
      filterValues.push({
        dataField: 'Header_CustomCaption',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }
}
