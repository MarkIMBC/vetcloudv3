import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportPOSDetailRoutingModule } from './report-pos-detail-routing.module';
import { ReportPOSDetailComponent } from './report-pos-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportPOSDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportPOSDetailRoutingModule
  ]
})
export class ReportPOSDetailModule { }
