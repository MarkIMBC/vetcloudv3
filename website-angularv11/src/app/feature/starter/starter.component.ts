import { TokenSessionFields, UserAuthenticationService } from './../../core/UserAuthentication.service';
import { Component, OnInit } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.less']
})
export class StarterComponent implements OnInit {

  cardBodyHeight: string = '100px'
  isVisible: boolean = false;

  company: any = {
    ImageHeaderLocationFilenamePath: '',
    Name: ''
  };

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();

    this.loadCompany();
  }

  async ngAfterViewInit() {
    setTimeout(() => {

      this.resize();
      this.isVisible = true;
    }, 2000);
  }

  async loadCompany() {

    var queryString = this.cs.encrypt(`
                      SELECT
                        Name,
                        IsShowPaymentWarningLabel,
                        ImageHeaderLocationFilenamePath
                      FROM vCompany
                      WHERE
                        ID IN (${this.currentUser.ID_Company})
    `);

    var records = await this.ds.query<any>(queryString);
    this.company = records[0];
  }

  resize() {

    var height = window.innerHeight;
    var cardBodyHeight = 0;

    cardBodyHeight = height - 100;

    if(cardBodyHeight < 350){

      cardBodyHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px'
  }

}
