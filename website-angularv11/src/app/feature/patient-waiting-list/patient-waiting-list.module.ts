import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientWaitingListRoutingModule } from './patient-waiting-list-routing.module';
import { PatientWaitingListComponent } from './patient-waiting-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PatientWaitingListComponent],
  imports: [
    CommonModule,
    PatientWaitingListRoutingModule,
    SharedModule,
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientWaitingListModule { }

