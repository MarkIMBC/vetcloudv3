import { Patient } from './../../../shared/APP_MODELS';
import { BaseListViewComponent } from './../../layout/base-list-view/base-list-view.component';
import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Client_DTO, PatientWaitingList_DTO } from 'src/shared/APP_HELPER';
import { APP_MODEL } from 'src/shared/APP_MODELS';
import { Enumerable } from 'linq-typescript';
import * as moment from 'moment';

@Component({
  selector: 'app-patient-waiting-list',
  templateUrl: './patient-waiting-list.component.html',
  styleUrls: [
    './../../layout/base-list-view/base-list-view.component.less',
    './patient-waiting-list.component.less',
  ],
})
export class PatientWaitingListComponent extends BaseListViewComponent {
  CustomComponentName: string = 'PatientWaitingListComponent';

  headerTitle: string = 'Waiting List';

  InitCurrentObject: any = {
    Name_Client: '',
    Name_Patient: '',
  };

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  *
            FROM dbo.vPatientWaitingList_ListView_temp
            /*encryptsqlend*/
            WHERE
            ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    await this.getRecordPaging(sql);

    var IDs_Patient = Enumerable.fromSource(this.dataSource)
      .select((r: any) => r['ID_Patient'])
      .toArray();

    await this.ds
      .execSP(
        'pGetPatientWaitingAppoitmentReference',
        {
          IDs_Patient: IDs_Patient,
          DateStart: moment().format('YYYY-MM-DD HH:mm:ss'),
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        console.log(obj);

        this.dataSource.forEach((record: any) => {
          record.References = [];
          record['UniqueIDList'] = '';
          if (record.IsQueued) {
            record.References.push({
              Oid_Model: record.Oid_Model_Reference,
              ID_CurrentObject: record.ID_Reference,
              ReferenceCode: 'Queued',
              Description: '',
            });
          }

          obj.PatientWaitingAppoitmentReference.forEach(
            (appoitmentRef: any) => {
              if (record.ID_Patient == appoitmentRef.ID_Patient) {
                var UniqueIDList = appoitmentRef.UniqueIDList;
                var references = UniqueIDList.split('~');
                var _ReferenceCode = '';

                references.forEach((reference: any) => {
                  var infos = reference.split('|');
                  var referenceObj = {
                    Oid_Model: infos[0],
                    ID_CurrentObject: infos[1],
                    ReferenceCode: infos[4],
                    Description: infos[5],
                  };

                  if (_ReferenceCode != referenceObj.ReferenceCode) {
                    record.References.push(referenceObj);
                    _ReferenceCode = referenceObj.ReferenceCode;
                  }
                });
              }
            }
          );

          if (record.References.length == 0) {
            if (record.IsQueued) {
              record.References.push({
                Oid_Model: record.Oid_Model_Reference,
                ID_CurrentObject: record.ID_Reference,
                ReferenceCode: 'Queued',
                Description: '',
              });
            }
          }
        });

        this.loading = false;
      });
  }

  dataSource_InitLoad(obj: any) {}

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name_Client == null)
      this.CurrentObject.Name_Client = '';
    if (this.CurrentObject.Name_Patient == null)
      this.CurrentObject.Name_Patient = '';

    if (this.CurrentObject.Name_Client.length > 0) {
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    return filterString;
  }

  RowCellClient_OnClick(rowData: PatientWaitingList_DTO) {
    this.customNavigate(['Client', rowData.ID_Client], {});
  }

  RowCellPatient_OnClick(rowData: PatientWaitingList_DTO) {
    this.customNavigate(['Patient', rowData.ID_Patient], {});
  }

  RowCellAction_CreateSOAP_OnClick(rowData: PatientWaitingList_DTO) {
    this.customNavigate(['Patient_SOAP', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID_Patient,
    });
  }

  RowCellAction_CreateWellness_OnClick(rowData: PatientWaitingList_DTO) {
    this.customNavigate(['Patient_Wellness', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID_Patient,
    });
  }

  RowCellAction_CreateConfinement_OnClick(rowData: PatientWaitingList_DTO) {
    this.customNavigate(['Confinement', -1], {
      ID_Client: rowData.ID_Client,
      ID_Patient: rowData.ID_Patient,
    });
  }

  BtnChangeWaitingStatus_onUpdatingPatientWaitingStatus(obj: any) {
    this.loading = obj.loading;
  }

  async BtnChangeWaitingStatus_onUpdatedPatientWaitingStatus(obj: any) {
    if (obj.Success) {
      this.toastService.success(obj.message);
      this.loading = true;
      await this.loadRecords();
      this.loading = false;
    } else {
      this.toastService.danger(obj.message);
    }
  }

  Reference_OnRowClick(reference: any) {
    var config = {};
    var routeLink: any[] = [];

    var Oid_Model = reference.Oid_Model;
    var ID_CurrentObject = reference.ID_CurrentObject;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT:
        routeLink = ['Patient', ID_CurrentObject];
        break;
      case APP_MODEL.PATIENT_SOAP:
        routeLink = ['Patient_SOAP', ID_CurrentObject];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = ['PatientAppointment', ID_CurrentObject];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = ['Patient_Wellness', ID_CurrentObject];
        break;
    }

    this.customNavigate(routeLink, config);
  }
}
