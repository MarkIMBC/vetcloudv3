import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MergePatientDetailRoutingModule } from './merge-patient-detail-routing.module';
import { MergePatientDetailComponent } from './merge-patient-detail.component';

@NgModule({
  declarations: [MergePatientDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    MergePatientDetailRoutingModule
  ]
})
export class MergePatientDetailModule { }
