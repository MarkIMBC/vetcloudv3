import {
  AlignEnum,
  FilingStatusEnum,
  FilterCriteriaType,
  IFormValidation,
  ItemTypeEnum,
  PaymentMethodEnum,
  TaxSchemeEnum,
} from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from './../../layout/base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { DataService } from 'src/app/core/data.service';

@Component({
  selector: 'app-merge-patient-detail',
  templateUrl: './merge-patient-detail.component.html',
  styleUrls: [
    './../../layout/base-detail-view/base-detail-view.component.less',
    './merge-patient-detail.component.less',
  ],
})
export class MergePatientDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'MergePatient';
  headerTitle: string = 'Merge Patient';
  paymentTemplate: any = null;
  displayMember: string = '';

  ID_Client_Pet_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `/*encryptsqlstart*/
              SELECT
               c.Name + ' - ' + p.Name AS Name,
                p.ID AS ID
              FROM tPatient  p
              LEFT JOIN tClient c ON c.ID = p.ID_Client
              WHERE p.isActive = 1
            /*encryptsqlend*/
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Client Pet',
          propertyType: PropertyTypeEnum.String,
          align: AlignEnum.left,
        },
      ],
    },
  };

  DetailView_onLoad() {
    this.loadOptionList();
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);
    this.menuItems.push(this._menuItem_Save);
  }

  loadMenuItems() {

  }

  loadOptionList(){

    var where:string = ` WHERE p.ID_Company = ${this.currentUser.ID_Company} AND p.isActive = 1`;
    var clientPetListOption =
    this.ID_Client_Pet_LookupboxOption.listviewOption;



    if(this.CurrentObject.ID_Patient_From != null){

      where+= ` AND p.ID != ${this.CurrentObject.ID_Patient_From}`;
    }

    if(this.CurrentObject.ID_Patient_To != null){

      where+= ` AND p.ID != ${this.CurrentObject.ID_Patient_To}`;
    }

    if (clientPetListOption != undefined) {
        clientPetListOption.sql = `/*encryptsqlstart*/
                                    SELECT
                                    c.Name + ' - ' + p.Name AS Name,
                                    p.ID AS ID
                                  FROM tPatient  p
                                  LEFT JOIN tClient c ON c.ID = p.ID_Client
                                  ${where}
                                  /*encryptsqlend*/`;
    }

  }



  public CurrentObject_onBeforeSaving() {
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    this.loadOptionList();

    console.log(this.CurrentObject)
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];
    if (this.CurrentObject.ID_Patient_From == null) {
      validations.push({
        message: `Source Pet is required.`,
      });
    }

    if (this.CurrentObject.ID_Patient_To == null) {
      validations.push({
        message: `Destination to is required.`,
      });
    }

    console.log(this.CurrentObject);

    return Promise.resolve(validations);
  }


}
