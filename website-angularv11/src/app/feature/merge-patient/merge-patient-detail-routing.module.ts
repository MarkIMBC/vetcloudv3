import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MergePatientDetailComponent } from './merge-patient-detail.component';

const routes: Routes = [{ path: '', component: MergePatientDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MergePatientDetailRoutingModule { }
