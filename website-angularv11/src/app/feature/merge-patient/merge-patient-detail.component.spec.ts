import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MergePatientDetailComponent } from './merge-patient-detail.component';

describe('MergePatientDetailComponent', () => {
  let component: MergePatientDetailComponent;
  let fixture: ComponentFixture<MergePatientDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MergePatientDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MergePatientDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
