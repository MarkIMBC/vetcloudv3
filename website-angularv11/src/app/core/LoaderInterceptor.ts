import { Router } from '@angular/router';

import { Injectable } from '@angular/core';
import {
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { DataService } from './data.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private loaderService: LoaderService,
    //	private msgBox: MessageBoxService,
    //	private toastMsgError: MessageBoxService,
    private router: Router) { }

  private requests: HttpRequest<any>[] = [];

  timeToShow: any = null;

  isMessageBoxShow: boolean = false;

  isNullOrUndefined<T>(obj: T | null | undefined): obj is null | undefined {
    return typeof obj === "undefined" || obj === null;
  }

  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    if (this.timeToShow != null) {
      setTimeout(() => {
        if (this.requests.length == 0) {
          this.loaderService.isLoading.next(false);
          window.clearTimeout(this.timeToShow);
          this.timeToShow = null;
        }
      }, 300);
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var token = window.localStorage.getItem("STORTKN");
    var _req = req;
    const isApiUrl = req.url.startsWith(DataService.API_URL);
    if (isApiUrl == true && this.isNullOrUndefined(token) != true) {
      _req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
          'Cache-Control': 'no-cache',
          Pragma: 'no-cache'
        }
      });
    }
    this.requests.push(_req);
    if (this.requests.length > 0) {
      if (this.timeToShow == null) {
        this.timeToShow = setTimeout(() => {
          if (this.requests.length > 0) {
            this.loaderService.isLoading.next(true);
          }
        }, 300);
      }
    };

    return Observable.create((observer: { next: (arg0: HttpResponse<any>) => void; error: (arg0: HttpErrorResponse) => void; complete: () => void; }) => {
      const subscription = next.handle(_req)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              this.removeRequest(_req);
              observer.next(event);
            }
          },
          async (err: HttpErrorResponse) => {

            if (req.url.includes("Model/GetModuleRights") == true) {
              if ([HTTPStatusCode.NotFound].includes(err.status) == true) {
                this.router.navigate(["/Main/NoAccess"]);
              }
            } else {
              if ([HTTPStatusCode.NotFound, HTTPStatusCode.Unauthorized].includes(err.status) != true) {
                //console.log(err);
                if (err.error) {

                  // this.toastMsgError.setToastTimeOut(10000);
                  // this.toastMsgError.setShowCloseButton(true);
                  // this.toastMsgError.error('From Vet Cloud Server<br/>Please check your connection or contact our Vet Cloud Support Staff.', err.statusText);

                  console.log(err);
                }
              }
            }
            this.removeRequest(_req);
            observer.error(err);

            if (err.status == HTTPStatusCode.Unauthorized) {
              if (this.isMessageBoxShow !== true && req.url.includes("Login/CheckSession") != true) {
                // this.isMessageBoxShow = true;
                // await this.msgBox.show("Redirect to Login...", "UnAuthorized Access");
                // this.isMessageBoxShow = false;

                console.log(err);

                document.location.href = '/Login';
                this.router.navigate(["Login"]);
              }
            }
          },
          () => {
            this.removeRequest(_req);
            observer.complete();
          });
      // remove request from queue when cancelled
      return () => {
        this.removeRequest(_req);
        subscription.unsubscribe();
      };
    });
  }
}



export const enum HTTPStatusCode {
  Continue = 100,
  SwitchingProtocols = 101,
  Processing = 102,
  EarlyHints = 103,

  /**
   * All `1xx` status codes.
   */
  InformationalResponses = Continue | SwitchingProtocols | Processing | EarlyHints,


  OK = 200,
  Created = 201,
  Accepted = 202,
  NonAuthoritativeInformation = 203,
  NoContent = 204,
  ResetContent = 205,
  PartialContent = 206,
  MultiStatus = 207,
  AlreadyReported = 208,
  IMUsed = 226,

  /**
   * All `2xx` status codes.
   */
  Success = (
    OK | Created | Accepted | NonAuthoritativeInformation | NoContent | ResetContent | PartialContent | MultiStatus |
    AlreadyReported | IMUsed
  ),


  MultipleChoices = 300,
  MovedPermanently = 301,
  Found = 302,
  SeeOther = 303,
  NotModified = 304,
  UseProxy = 305,
  SwitchProxy = 306,
  TemporaryRedirect = 307,
  PermanentRedirect = 308,

  /**
   * All `3xx` status codes.
   */
  Redirection = (
    MultipleChoices | MovedPermanently | Found | SeeOther | NotModified | UseProxy | SwitchProxy | TemporaryRedirect |
    PermanentRedirect
  ),


  BadRequest = 400,
  Unauthorized = 401,
  PaymentRequired = 402,
  Forbidden = 403,
  NotFound = 404,
  MethodNotAllowed = 405,
  NotAcceptable = 406,
  ProxyAuthenticationRequired = 407,
  RequestTimeout = 408,
  Conflict = 409,
  Gone = 410,
  LengthRequired = 411,
  PreconditionFailed = 412,
  PayloadTooLarge = 413,
  URITooLong = 414,
  UnsupportedMediaType = 415,
  RangeNotSatisfiable = 416,
  ExpectationFailed = 417,
  ImATeapot = 418,
  MisdirectedRequest = 421,
  UnprocessableEntity = 422,
  Locked = 423,
  FailedDependency = 424,
  UpgradeRequired = 426,
  PreconditionRequired = 428,
  TooManyRequests = 429,
  RequestHeaderFieldsTooLarge = 431,
  UnavailableForLegalReasons = 451,

  /**
   * All `4xx` error codes.
   */
  ClientErrors = (
    BadRequest | Unauthorized | PaymentRequired | Forbidden | NotFound | MethodNotAllowed | NotAcceptable |
    ProxyAuthenticationRequired | RequestTimeout | Conflict | Gone | LengthRequired | PreconditionFailed |
    PayloadTooLarge | URITooLong | UnsupportedMediaType | RangeNotSatisfiable | ExpectationFailed | ImATeapot |
    MisdirectedRequest | UnprocessableEntity | Locked | FailedDependency | UpgradeRequired | PreconditionRequired |
    TooManyRequests | RequestHeaderFieldsTooLarge | UnavailableForLegalReasons
  ),


  InternalServerError = 500,
  NotImplemented = 501,
  BadGateway = 502,
  ServiceUnavailable = 503,
  GatewayTimeout = 504,
  HTTPVersionNotSupported = 505,
  VariantAlsoNegotiates = 506,
  InsufficientStorage = 507,
  LoopDetected = 508,
  NotExtended = 510,
  NetworkAuthenticationRequired = 511,

  /**
   * All `5xx` error codes.
   */
  ServerErrors = (
    InternalServerError | NotImplemented | BadGateway | ServiceUnavailable | GatewayTimeout | HTTPVersionNotSupported |
    VariantAlsoNegotiates | InsufficientStorage | LoopDetected | NotExtended | NetworkAuthenticationRequired
  )
}


@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  public isLoading = new BehaviorSubject(false);
  constructor() { }
}
