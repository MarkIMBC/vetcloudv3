import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  validateEmail(email : string){

    var emailPattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    return emailPattern.test(email);
  }

  validatePHMobileNumber(contactNumber : string){

    var pattern = /^\d+$/;
    var result = pattern.test(contactNumber);

    return result;
  }
}
