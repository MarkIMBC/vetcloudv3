import { BreadCrumbItem } from '../../shared/content-header/content-header.component';
import { AdminLTEMenuItem } from '../../shared/AdminLTEMenuItem';
import { UserAuthenticationService } from '../../core/UserAuthentication.service';
import { GeneralfxService } from '../../core/generalfx.service';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  IDetailViewAfterSavedArg,
  IFile,
  IFormValidation,
  PagingOption,
} from 'src/shared/APP_HELPER';
import { Model } from 'src/shared/APP_MODELS';
import { ToastService } from 'src/app/shared/toast.service';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import { ValidationService } from 'src/app/core/validation.service';
import { Enumerable } from 'linq-typescript';
import { Location } from '@angular/common';
import { AdminLTEImageBoxComponent } from 'src/app/shared/control/admin-lte-image-box/admin-lte-image-box.component';


@Component({
  selector: 'app-base-detail-view',
  templateUrl: './base-detail-view.component.html',
  styleUrls: ['./base-detail-view.component.less'],
})
export class BaseDetailViewComponent implements OnInit {
  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;

  @ViewChildren(AdminLTEBaseControlComponent)
  adminLTEControls!: QueryList<AdminLTEBaseControlComponent>;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected elRef: ElementRef,
    protected _dataServices: DataService,
    public toastService: ToastService,
    protected userAuth: UserAuthenticationService,
    protected cs: CrypterService,
    protected validationService: ValidationService,
    protected activeRoute: ActivatedRoute,
    protected location: Location
  ) { }

  @Output() onAfterSaved: EventEmitter<IDetailViewAfterSavedArg> =
    new EventEmitter<IDetailViewAfterSavedArg>();
  @Output() onModelChanged: EventEmitter<IControlModelArg> =
    new EventEmitter<IControlModelArg>();

  @ViewChildren(AdminLTEImageBoxComponent) adminLTEImageBoxComponents: QueryList<AdminLTEImageBoxComponent> | undefined;

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  protected _tempID: number = 0;

  ModelName: string = '';
  currentUser: TokenSessionFields = new TokenSessionFields();
  headerTitle: string = '';
  @Input() loading: boolean = true;

  displayMember: string = 'Code';
  routerFeatureName: string = '';

  menuItems: AdminLTEMenuItem[] = [];
  breadCrumbItems: BreadCrumbItem[] = [];
  __ID_CurrentObject: number = 0;
  model: Model = new Model();

  isDirty: boolean = false;

  CurrentObject: any = {};
  PreviousObject: any = {};
  CurrentFiles: IFile[] = [];

  private _subscribeActiveRoute: any;

  isComponentInitialized = false;

  async ngOnInit(): Promise<void> {
    var _ = this;
    this.currentUser = this.userAuth.getDecodedToken();

    this._subscribeActiveRoute = this.activeRoute.params.subscribe(
      async (routeParams) => {
        /*Check and Log out by Server User Session Status*/
        if ((await this.userAuth.CheckAndLogOutByUserSessionStatus()) != true) {
          return;
        }

        this.loadConfigOption();

        await this.loadCurrentModel();

        await this.DetailView_Init();

        this.__ID_CurrentObject = await this._getDefault__ID_CurrentObject();
        await this.loadRecord();
      }
    );
  }

  private async validateCurrentRouteLink(url: string) {
    var weblinkParts = url.split('/');
    var currentRouteLink = '';

    if (weblinkParts.length == 0) return;

    if (weblinkParts[1]) {
      console.log('weblinkParts[1]', weblinkParts[1000]);
    }

    console.log('weblinkParts[1]', weblinkParts[1000]);
    console.log(weblinkParts);

    var params: any = {
      ID_UserSession: this.currentUser.ID_UserSession,
    };
    var _arg: string = '';

    var obj: any = await this._dataServices.post(`Model/ExecuteSP2`, {
      args: _arg,
      isReturnObject: true,
      isTransaction: false,
      spName: this.cs.encrypt('pGetLoggedUserSession'),
    });
  }

  ngOnDestroy(): void {
    this._subscribeActiveRoute.unsubscribe();
  }

  ngAfterViewInit() {
    this.DetailView_AfterInit();
  }

  _menuItem_New: AdminLTEMenuItem = {
    label: 'New',
    icon: 'fa fa-file',
    class: 'text-default',
    visible: true,
  };

  configOptions: any = {};

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params['configOptions'];

    if (configOptionsString == undefined || configOptionsString == null) return;

    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  async _menuItem_New_onClick() {
    this.__ID_CurrentObject = -1;
    await this.loadRecord();

    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Save: AdminLTEMenuItem = {
    label: 'Save',
    icon: 'fa fa-save',
    class: 'text-success',
    visible: true,
  };

  async _menuItem_Save_onClick() {

    this.loading = true;

    try {
      /*Check and Log out by Server User Session Status*/
      if ((await this.userAuth.CheckAndLogOutByUserSessionStatus()) != true) {
        return;
      }

      this.CurrentObject_onBeforeSaving();
      var isvalid = true;

      isvalid = await this.validateRecord();
      if (!isvalid) return;

      this.loading = true;
      if (this.isSaving == true) return;

      await this.doSaving();
    } catch (error) {

    }

    this.loading = false;
    this.isHideSavingPrompt = false;
  }

  async execSP(spName: string, params?: any, options?: any): Promise<any> {
    /*Check and Log out by Server User Session Status*/
    if ((await this.userAuth.CheckAndLogOutByUserSessionStatus()) != true) {
      return;
    }

    return await this._dataServices.execSP(spName, params, options);
  }

  isSaving: boolean = false;
  isHideSavingPrompt: boolean = false;

  async doSaving(): Promise<any> {
    var _ = this;

    if (_.isSaving) return;
    _.isSaving = true;
    _.loading = true;

    return _.save()
      .then(async (r) => {
        //
        await _._CurrentObject_onAfterSaving();

        if (r.key != _.__ID_CurrentObject) {
          _.__ID_CurrentObject = r.key;
          await _.redirectAfterSaved();
          _.loading = false;
          _.isSaving = false;
        } else {
          _.loading = false;
          _.isSaving = false;

          await _.loadRecord();
        }

        if (!_.isHideSavingPrompt) {
          _.toastService.success(
            `${_.CurrentObject[_.displayMember]} has been saved successfully.`
          );
        }

        _.loading = false;
        _.isSaving = false;
      })
      .catch((reason) => {

        if (!_.isHideSavingPrompt) {
          _.toastService.danger(
            `Failed to save ${_.CurrentObject[_.displayMember]}.`
          );
        }
        _.loading = false;
        _.isSaving = false;
      });
  }

  public CurrentObject_onBeforeSaving() { }

  private async _CurrentObject_onAfterSaving() {
    await this.CurrentObject_onAfterSaving();

    this.onAfterSaved.emit({
      CurrentObject: this.CurrentObject,
      Model: this.model,
    });
  }

  public CurrentObject_onAfterSaving() { }

  public async validateRecord() {
    this.loading = true;

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    CustomValidations.forEach((customValidation) => {
      validationsAppForm.push(customValidation);
    });

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    this.loading = false;

    return isValid;
  }

  protected async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Refresh: AdminLTEMenuItem = {
    label: 'Refresh',
    icon: 'fa fa-sync',
    class: 'text-info',
    visible: true,
  };

  _menuItem_Delete: AdminLTEMenuItem = {
    label: 'Delete',
    icon: 'fas fa-trash-alt',
    class: 'text-danger',
    visible: true,
  };

  _menuItem_Back: AdminLTEMenuItem = {
    label: 'Back',
    icon: 'fas fa-arrow-left',
    class: 'text-info',
    visible: true,
  };

  async _menuItem_Refresh_onClick() {
    await this.loadRecord();
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);
    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  addMenuItem(item: AdminLTEMenuItem) {
    this.menuItems.push(item);
  }

  protected async _getDefault__ID_CurrentObject() {
    return parseInt(this.activeRoute.snapshot.params['ID_CurrentObject']);
  }

  async loadCurrentModel() {
    this.loading = true;

    var params: any = {};
    params['name'] = this.ModelName;

    var obj = await this.execSP(
      `pGetModelByName`,
      {
        name: this.ModelName,
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;

    this.loading = false;
  }

  async loadRecord() {
    try {

      if(this.adminLTEImageBoxComponents){

        this.adminLTEImageBoxComponents.forEach((adminLTEImageBoxComponent: AdminLTEImageBoxComponent) => {

          adminLTEImageBoxComponent.removeValue();
        });
      }

      this.loading = true;

      var params: any = this.pGetRecordOptions();

      params['ID'] = this.__ID_CurrentObject;
      params['ID_Session'] = this.currentUser.ID_UserSession;

      var obj = await this.execSP(`pGet${this.model.Name}`, params, {
        isReturnObject: true,
      });

      if (
        obj['ID_Company'] == null ||
        obj['ID_Company'] == undefined ||
        obj['ID_Company'] < 1
      ) {
        obj.ID_Company = this.currentUser.ID_Company;
      }

      if (obj['ID_Company'] != undefined) {
        if (obj.ID_Company != this.currentUser.ID_Company) {
          this.router.navigate(['PageNotFound']);
          return;
        }
      }

      obj = await this.getInitCurrentObject(obj);

      this.PreviousObject = JSON.parse(JSON.stringify(obj));
      this.CurrentObject = JSON.parse(JSON.stringify(obj));

      this.menuItems = [];

      this.loadInitMenuItem();
      this.loadMenuItems();
      this.loadBreadCrumbs();
      this.loadRightDrowDownMenu();

      this.loading = false;
      this.isDirty = false;

      this.DetailView_onLoad();
    } catch (error) {
      //
      console.log(error);
      this.reloadCurrentRecordPage();
    }
  }

  private async reloadCurrentRecordPage() {
    var config: any[] = this.getCustomNavigateCommands(
      [this.model.Name, this.__ID_CurrentObject],
      this.configOptions
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.location.href = location.origin + url;
  }

  protected async getInitCurrentObject(currentObject: any): Promise<any> {
    return currentObject;
  }

  public async LoadCurrentObjectRecord(ID_CurrentObject: number) {
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      window.location.href = './Login';
      return;
    }

    this.__ID_CurrentObject = ID_CurrentObject;

    await this.loadCurrentModel();
    await this.loadRecord();
  }

  protected pGetRecordOptions(): any {
    return {};
  }

  loadMenuItems() { }

  loadBreadCrumbs() { }

  ContentHeader_onBreadCrumbClicked(breadCrumbItem: BreadCrumbItem) { }

  DetailView_Init() { }

  DetailView_AfterInit() { }

  DetailView_onLoad() { }

  initmenubar_OnClick(e: any) {
    if (e.item.label == 'New') {
      this._menuItem_New_onClick();
    } else if (e.item.label == 'Save') {
      this._menuItem_Save_onClick();
    } else if (e.item.label == 'Refresh') {
      this._menuItem_Refresh_onClick();
    } else if (e.item.label == 'Delete') {
      this.doDelete();
    } else if (e.item.label == 'Back') {
      this.location.back();
    }

    this.menubar_OnClick(e);
  }

  menubar_OnClick(e: any) { }

  compute() { }

  deleteDetail(detailName: string, obj: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject[detailName],
      'ID',
      obj.ID + ''
    );

    this.CurrentObject['BillingInvoice_Detail'].splice(index, 1);

    this.compute();

    this.isDirty = true;
  }

  async doDelete() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before delete ${this.CurrentObject[this.displayMember]
        }.`
      );
      return;
    }

    await this.loadRecord();

    this.CurrentObject.IsActive = 0;

    this.loading = true;
    await this.doSaving();
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
      data: [],
    };

    if (e.data != undefined) {
      arg['data'] = e.data;
    }

    this.CurrentObject_onValueChange(arg);

    this.isDirty = true;

    this._control_onModelChanged(e);
  }

  _control_onModelChanged(e: IControlModelArg) {
    this.onModelChanged.emit(e);
  }

  getTempID(): number {
    this._tempID -= 1;
    return this._tempID;
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) { }

  getFiles(): IFile[] {
    var currentFiles: IFile[] = [];

    if (this.adminLTEControls) {
      this.adminLTEControls.forEach((i: any) => {
        if (i['file']) {
          currentFiles.push({
            file: i['file'],
            dataField: i.name,
            isImage: true,
          });
        }
      });
    }

    return currentFiles;
  }

  public async save(): Promise<any> {
    var CurrentObject = this.CurrentObject;
    var PreviousObject = this.PreviousObject;
    var model = this.model;
    var currentUser = this.currentUser;
    var currentFiles: IFile[] = [];

    currentFiles = this.getFiles();

    if (
      CurrentObject['ID_Company'] == null &&
      CurrentObject['ID_Company'] == undefined &&
      CurrentObject.ID < 1
    ) {
      CurrentObject.ID_Company = currentUser.ID_Company;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      this._dataServices
        .saveObject(
          model.Oid,
          CurrentObject,
          PreviousObject,
          currentFiles,
          currentUser
        )
        .then((r) => {
          if (r.key != undefined) {
            resolve(r);
          } else {
            reject('Saving Failed....');
          }
        })
        .catch((reason) => {
          reject(reason);
        });
    });

    return promise;
  }

  rightDropDownItems: AdminLTEMenuItem[] = [];

  loadRightDrowDownMenu() {
  }

  onClickBackButton() {
    this.location.back();
  }

  rightDropDown_onMainButtonClick() { }

  rightDropDown_onMenuItemButtonClick(event: any) { }

  getCustomNavigateCommands(routelink: any, config?: any) {
    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    return routelink;
  }

  customNavigate(routelink: any, config?: any) {
    routelink = this.getCustomNavigateCommands(routelink, config);

    this.router.navigate(routelink);
  }
}

export class CurrentObjectOnValueChangeArg {
  name: string = '';
  value: any;
  data?: any[] = [];
}
