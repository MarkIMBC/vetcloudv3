import { DataService } from './../../core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from './../../core/UserAuthentication.service';
import { SystemService } from './../../core/system.service';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { CrypterService } from 'src/app/core/crypter.service';

@Component({
  selector: 'sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.less'],
})
export class SidemenuComponent implements OnInit {
  MenuItems: AdminLTEMenuItem[] = [];

  company: any = {
    ImageHeader: '',
    ImageLogoThumbNameLocationFilenamePath: '',
    Name: '',
  };

  currentUser: TokenSessionFields = new TokenSessionFields();

  message: string = '';
  subscription: Subscription | undefined;

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    protected cs: CrypterService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      this.MenuItems = [];
      return;
    }

    this.loadCompany();

    this.loadMenuItems();
  }

  async loadCompany() {
    var queryString = this.cs.encrypt(`
                      SELECT
                        Name,
                        IsShowPaymentWarningLabel,
                        ImageLogoThumbNameLocationFilenamePath
                      FROM vCompany
                      WHERE
                        ID IN (${this.currentUser.ID_Company})
    `);

    var records = await this.ds.query<any>(queryString);
    this.company = records[0];
  }

  async loadMenuItems() {
    var menus: any = await this.ds.execSP(
      'pGetUserNavigation',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    );

    if (menus['ParentItem'] == undefined) {
      menus['ParentItem'] = [];
    }

    if (menus['ParentItem'] == null) {
      menus['ParentItem'] = [];
    }

    menus.ParentItem.forEach((menu: any) => {
      var menuItem: any = {
        label: menu.Caption,
        icon: menu.Icon,
        isOpen: false,
        visible: true,
        routerLink: menu.Route,
        items: [],
      };

      if (menu['ChildrenItems'] == undefined) {
        menu['ChildrenItems'] = [];
      }

      if (menu['ChildrenItems'] == null) {
        menu['ChildrenItems'] = [];
      }

      menu.ChildrenItems.forEach((child: any) => {
        if (child.Route) {
          menuItem.items.push({
            label: child.Caption,
            icon: child.Icon,
            isOpen: false,
            routerLink: child.Route,
            visible: true,
          });
        }
      });

      if (menuItem.label == 'Admission') {
        menuItem.items.push({
          label: 'Lodging',
          icon: 'far fa-clock',
          isOpen: false,
          routerLink: 'PatientLodgingList',
          visible: true,
        });
      }

      if (menuItem.routerLink || menuItem.items.length > 0) {
        this.MenuItems.push(menuItem);
      }
    });

    var objReferenceMenuItems = await this.ds.execSP(
      'pGetReferenceLinkMenuItems',
      { ID_UserSerssion: this.currentUser.ID_Session },
      {
        isReturnObject: true,
      }
    );

    if (objReferenceMenuItems) {
      var menuItemsReferenceMenuItems: any = [];

      objReferenceMenuItems.MenuItems.forEach((objReferenceMenuItem: any) => {
        var menuItemsReferenceMenuItem = {
          label: objReferenceMenuItem.Caption,
          routerLink: `./ReferenceLink/${objReferenceMenuItem.Name}`,
          icon: 'far fa-circle',
          isOpen: false,
          visible: true,
        };

        menuItemsReferenceMenuItems.push(menuItemsReferenceMenuItem);
      });

      this.MenuItems.push({
        label: 'Reference',
        icon: 'far fa-circle',
        isOpen: false,
        visible: true,
        items: menuItemsReferenceMenuItems,
      });
    }

    this.setNavigationValidation();
  }
  private setNavigationValidation() {
    var routerLinkStrings = '';

    this.MenuItems.forEach((menuItem: AdminLTEMenuItem) => {
      if (menuItem.routerLink) {
        routerLinkStrings += menuItem.routerLink + '|';
      }

      if (menuItem.items == undefined) menuItem.items = [];

      menuItem.items.forEach((subMenuItem: AdminLTEMenuItem) => {
        if (subMenuItem.routerLink) {
          routerLinkStrings += subMenuItem.routerLink + '|';
        }
      });
    });

    window.localStorage.setItem('routerLinkStrings', routerLinkStrings);
  }

  menu_OnClick() {
    this.renderer.removeClass(this.document.body, 'sidebar-open');
  }

  brand_onClick() {
    this.renderer.removeClass(this.document.body, 'sidebar-open');
  }

  ngOnDestroy() {}
}
