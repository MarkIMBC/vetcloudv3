import { UserAuthenticationService } from './../../core/UserAuthentication.service';
import { IControlModelArg } from './../../../shared/APP_HELPER';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/core/data.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { PagingOption, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-base-list-view',
  templateUrl: './base-list-view.component.html',
})
export class BaseListViewComponent implements OnInit {
  CustomComponentName: string = '';

  protected ds: DataService;

  protected isInitiate: boolean = false;

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected _ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.ds = _ds;

    this.currentUser = this.userAuth.getDecodedToken();

    this.gridHeight = '0px';
  }

  sizeofAllStorage() {
    // provide the size in bytes of the data currently stored
    var size = 0;
    var i = 0;
    for (i = 0; i <= localStorage.length - 1; i++) {
      var key = localStorage.key(i);
      if (key) {
        size += this.lengthInUtf8Bytes(localStorage.getItem(key));
      }
    }
    return size;
  }

  lengthInUtf8Bytes(str: any) {
    // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
    var m = encodeURIComponent(str).match(/%[89ABab]/g);
    return str.length + (m ? m.length : 0);
  }

  showFilter: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();

  dataSource: any[] = [];

  headerTitle: string = '';

  OrderByString: string = '';

  breadCrumbItems: AdminLTEMenuItem[] = [];

  hideFilter: boolean = true;

  cardBodyHeight: string = '400px';
  gridHeight: string = '400px';

  pagingOption: PagingOption = new PagingOption();
  loading: boolean = false;
  InitCurrentObject: any = {};

  CurrentObject: any = {};

  TotalRecordCount: number = 0;
  LoadedRecordCount: number = 0;

  items: AdminLTEMenuItem[] = [
    {
      label: 'New',
      icon: 'fas fa-file',
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  initializeValue(obj: any, type: PropertyTypeEnum) {
    if (type == PropertyTypeEnum.String) {
      if (obj == null) obj = '';
    } else if (
      type == PropertyTypeEnum.Decimal ||
      type == PropertyTypeEnum.Int
    ) {
      if (obj == null) obj = 0;
    }
  }

  resetCurrentObject() {
    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurrentObject));
  }

  getClassName() {
    return this.constructor.name.toString();
  }

  getLastFilterObj() {
    var className = this.CustomComponentName;
    var strObj = window.localStorage.getItem(`${className}_ListViewFilter`);
    var obj: any = {};
    if (strObj) {
      strObj = this.cs.decrypt(strObj);
      obj = JSON.parse(strObj);
    }

    return obj;
  }

  setLastFilterObj() {
    var className = this.CustomComponentName;
    var obj = {
      CurrentObject: this.CurrentObject,
      OrderByString: this.OrderByString,
      pagingOption: this.pagingOption,
    };
    var strObj = JSON.stringify(obj);

    strObj = this.cs.encrypt(strObj);

    return window.localStorage.setItem(`${className}_ListViewFilter`, strObj);
  }

  menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == 'Refresh') {
      this.loadRecords();
    } else if (menuItem.label == 'New') {
      this.menuItem_New_onClick();
    }
  }

  btnfilter_OnClick() {
    this.showFilter = this.showFilter == false;
  }

  menuItem_New_onClick() {}

  findIndexByKeyValue(_array: any, key: any, value: any) {
    for (var i = 0; i < _array.length; i++) {
      if (_array[i][key] == value) {
        return i;
      }
    }

    return -1;
  }

  async ngOnInit(): Promise<void> {
    var _ = this.CurrentObject;

    /*Check and Log out by Server User Session Status*/
    if ((await this.userAuth.CheckAndLogOutByUserSessionStatus()) != true) {
      return;
    }

    window.onresize = () => {
      setTimeout(() => {
        this.resize();
      }, 100);
    };
  }

  resetHistoricalFilter() {
    var localstorageItems: any[] = this.getAllLocalStorageItems();

    localstorageItems.forEach((item: any) => {
      var name = item.name;

      if (
        name.includes('_ListViewFilter') &&
        name != this.CustomComponentName + '_ListViewFilter'
      ) {
        localStorage.removeItem(name);
      }
    });
  }

  getAllLocalStorageItems() {
    var archive = [],
      keys = Object.keys(localStorage),
      i = 0,
      key;

    for (; (key = keys[i]); i++) {
      archive.push({
        name: key,
        value: localStorage.getItem(key),
      });
    }

    return archive;
  }

  onAfterViewInit() {}

  async ngAfterViewInit() {
    var _: any = this;

    this.resetCurrentObject();
    this.resetHistoricalFilter();
    var filterObj = this.getLastFilterObj();

    var keyNameFilterObj: any[] = [
      'CurrentObject',
      //"pagingOption",
      'OrderByString',
    ];
    keyNameFilterObj.forEach((key: string) => {
      if (filterObj[key]) {
        _[key] = filterObj[key];
      }
    });

    this.onAfterViewInit();

    this.loadRecords();

    setTimeout(() => {
      this.resize();

      this.ListView_OnAfterView();
    }, 2000);

    this.isInitiate = true;
  }

  btnHideFilter_OnClick() {
    this.hideFilter = this.hideFilter == false;
  }

  resize() {
    var height = window.innerHeight;

    var cardBodyHeight = 0;
    var gridHeight = 0;

    cardBodyHeight = height - 318;
    gridHeight = height - 275;

    if (cardBodyHeight < 350) {
      this.hideFilter = true;
      cardBodyHeight = 400;
      gridHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
    this.gridHeight = gridHeight.toString() + 'px';

    this.onResize();
  }

  onResize(){}

  isListLoadedSuccess: boolean = true;
  async getRecordPaging(query: string) {
    this.isListLoadedSuccess = true;

    var columnName: string = '';
    this.loading = true;
    this.setLastFilterObj();
    var obj: any;

    try {
      obj = await this.ds.execSP(
        'pGetRecordPaging',
        {
          sql: query,
          PageNumber: this.pagingOption.PageNumber,
          DisplayCount: this.pagingOption.DisplayCount,
          OrderByString:
            columnName.length > 0 ? columnName : this.OrderByString,
        },
        {
          isReturnObject: true,
        }
      );
    } catch (error) {
      this.isListLoadedSuccess = false;
    }

    this.loading = false;

    if (obj == null || obj == true || obj == undefined) {
      obj = {};
      obj.Records = [];
      obj.TotalRecord = 0;
      obj.TotalPageNumber = 0;
    }

    if (obj.Records == null || obj.Records == undefined) obj.Records = [];

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;
    this.dataSource = obj.Records;

    this.dataSource_InitLoad(obj);

    this.TotalRecordCount = obj.TotalRecord;

    this.LoadedRecordCount =
      this.dataSource.length < this.pagingOption.DisplayCount
        ? this.dataSource.length
        : this.pagingOption.DisplayCount;
    this.LoadedRecordCount =
      this.pagingOption.DisplayCount * (this.pagingOption.PageNumber - 1) +
      this.dataSource.length;
  }

  dataSource_InitLoad(obj: any) {}

  loadRecords() {}

  btRefresh_OnClick() {
    this.loadRecords();
  }

  onPageChange(event: any) {
    this.pagingOption.PageNumber = event.page;

    this.loadRecords();
  }

  onBtnClearClick(event: any) {
    this.pagingOption.PageNumber = 1;

    this.resetCurrentObject();

    this.loadRecords();
  }

  onBtnSearchClick(event: any) {
    this.pagingOption.PageNumber = 1;

    this.loadRecords();
  }

  getFilterString(): string {
    var filterString = '';

    return filterString;
  }

  ListView_Onload() {}

  ListView_OnAfterView() {}

  tableHeader_onClick(columnName: string) {
    if (this.OrderByString.length == 0 || this.OrderByString != columnName) {
      this.OrderByString =
        this.OrderByString.search(' DESC') < 0 ? columnName : '';
    } else {
      this.OrderByString = columnName + ' DESC';
    }

    this.loadRecords();
  }

  onColumnHeaderClick(columnName: string) {}

  Row_OnClick(rowData: any) {
    this.router.navigate(['Client', 'Detail']);
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;
  }

  checkbox_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;
    this.loadRecords();
  }

  control_onSelectedItemChanged(e: IControlModelArg) {
    this.pagingOption.PageNumber = 1;

    this.CurrentObject[e.name] = e.value;

    if (e.displayName) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    this.loadRecords();
  }

  control_onEnterKeyUp(e: IControlModelArg) {
    this.pagingOption.PageNumber = 1;

    this.CurrentObject[e.name] = e.value;
    this.loadRecords();
  }

  customNavigate(routelink: any, config?: any) {
    routelink = this.getCustomNavigateCommands(routelink, config);

    this.router.navigate(routelink);
  }

  getCustomNavigateCommands(routelink: any, config?: any) {
    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    return routelink;
  }

  openNewTab(options: any) {

    var routerName = "";
    var ID_CurrentObject = 0;

    if(options){

      if(options["RouterName"]) routerName = options["RouterName"];
      if(options["ID_CurrentObject"]) ID_CurrentObject = options["ID_CurrentObject"];
    }

    var config: any[] = this.getCustomNavigateCommands(
      [routerName, ID_CurrentObject],
      {}
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }
}
