import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './defaultpage/page-not-found/page-not-found.component';
import { ClientPatientRecordsComponent } from './feature/client-patient-records/client-patient-records.component';
import { StarterComponent } from './feature/starter/starter.component';
import { DashboardComponent } from './feature/dashboard/dashboard.component';
import { LoginURLComponent } from './login-url/login-url.component';

const routes: Routes = [

  { path: '', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'Login', component: LoginComponent},

  { path: 'LoginUrl/:text', component: LoginURLComponent},

  { path: 'Starter', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'Client', loadChildren: () => import('./feature/client-list/client-list.module').then(m => m.ClientModule) },

  { path: 'Client/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/client-detail/client-detail.module').then(m => m.ClientDetailModule) },

  { path: 'Patient', loadChildren: () => import('./feature/patient-list/patient-list.module').then(m => m.PatientListModule) },

  { path: 'Patient/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-detail/patient-detail.module').then(m => m.PatientDetailModule) },

  { path: 'ItemService', loadChildren: () => import('./feature/item-service-list/item-service-list.module').then(m => m.ItemServiceListModule) },

  { path: 'ItemService/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/item-service-detail/item-service-detail.module').then(m => m.ItemServiceDetailModule) },

  { path: 'ItemInventoriable', loadChildren: () => import('./feature/item-inventoriable-list/item-inventoriable-list.module').then(m => m.ItemInventoriableListModule) },

  { path: 'ItemInventoriable/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/item-inventoriable-detail/item-inventoriable-detail.module').then(m => m.ItemInventoriableDetailModule) },

  { path: 'SupplierList', loadChildren: () => import('./feature/supplier-list/supplier-list.module').then(m => m.SupplierListModule) },

  { path: 'Patient_SOAP', loadChildren: () => import('./feature/patient-soap-list/patient-soap-list.module').then(m => m.PatientSOAPListModule) },

  { path: 'Patient_SOAP/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-soap-detail/patient-soap-detail.module').then(m => m.PatientSoapDetailModule) },

  { path: 'PatientAppointment/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-appointment-detail/patient-appointment-detail.module').then(m => m.PatientAppointmentDetailModule) },

  { path: 'BillingInvoice', loadChildren: () => import('./feature/billing-invoice-list/billing-invoice-list.module').then(m => m.BillingInvoiceListModule) },

  { path: 'BillingInvoice/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/billing-invoice-detail/billing-invoice-detail.module').then(m => m.BillingInvoiceDetailModule) },

  { path: 'PatientSoapDetail', loadChildren: () => import('./feature/patient-soap-detail/patient-soap-detail.module').then(m => m.PatientSoapDetailModule) },

  { path: 'ClientDetail', loadChildren: () => import('./feature/client-detail/client-detail.module').then(m => m.ClientDetailModule) },

  { path: 'PatientDetail', loadChildren: () => import('./feature/patient-detail/patient-detail.module').then(m => m.PatientDetailModule) },

  { path: 'ItemInventoriableDetail', loadChildren: () => import('./feature/item-inventoriable-detail/item-inventoriable-detail.module').then(m => m.ItemInventoriableDetailModule) },

  { path: 'ItemServiceDetail', loadChildren: () => import('./feature/item-service-detail/item-service-detail.module').then(m => m.ItemServiceDetailModule) },

  { path: 'ScheduleList', loadChildren: () => import('./feature/schedule-list/schedule-list.module').then(m => m.ScheduleListModule) },

  { path: 'Employee/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'Employee', loadChildren: () => import('./feature/employee-list/employee-list.module').then(m => m.EmployeeListModule) },

  { path: 'PageNotFound', component: PageNotFoundComponent },

  { path: 'Report/:configOptions', loadChildren: () => import('./feature/report/report.module').then(m => m.ReportModule) },

  { path: 'PatientAppointmentDetail', loadChildren: () => import('./feature/patient-appointment-detail/patient-appointment-detail.module').then(m => m.PatientAppointmentDetailModule) },

  { path: 'CompanyDetail', loadChildren: () => import('./feature/company-detail/company-detail.module').then(m => m.CompanyDetailModule) },

  { path: 'ReportSalesIncome', loadChildren: () => import('./feature/report-sales-income/report-sales-income.module').then(m => m.ReportSalesIncomeModule) },

  { path: 'ReportBillingInvoicePaidList', loadChildren: () => import('./feature/report-billing-invoice-paid-list/report-billing-invoice-paid-list.module').then(m => m.ReportBillingInvoicePaidListModule) },

  { path: 'ReportBillingInvoiceAging', loadChildren: () => import('./feature/report-billing-invoice-aging/report-billing-invoice-aging.module').then(m => m.ReportBillingInvoiceAgingModule) },

  { path: 'ReportInvoiceItemsAndServices', loadChildren: () => import('./feature/report-pos-summary/report-pos-summary.module').then(m => m.ReportPOSSummaryModule) },

  { path: 'ReportPaymentTransactionDetail', loadChildren: () => import('./feature/report-payment-transaction-detail/report-payment-transaction-detail.module').then(m => m.ReportPaymentTransactionDetailModule) },

  { path: 'ReportInventorySummary', loadChildren: () => import('./feature/report-inventory-summary/report-inventory-summary.module').then(m => m.ReportInventorySummaryModule) },

  { path: 'ReportInventoryDetail', loadChildren: () => import('./feature/report-inventory-detail/report-inventory-detail.module').then(m => m.ReportInventoryDetailModule) },

  { path: 'ReportReceivedDepositCredit', loadChildren: () => import('./feature/report-received-deposit-credit/report-received-deposit-credit.module').then(m => m.ReportReceivedDepositCreditModule) },

  { path: 'EmployeeDetail', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'EmployeeList', loadChildren: () => import('./feature/employee-list/employee-list.module').then(m => m.EmployeeListModule) },

  { path: 'ConfinementList', loadChildren: () => import('./feature/patient-confinement-list/patient-confinement-list.module').then(m => m.PatientConfinementListModule) },

  { path: 'Confinement/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-confinement-detail/patient-confinement-detail.module').then(m => m.PatientConfinementDetailModule) },

  { path: 'PatientWaitingList', loadChildren: () => import('./feature/patient-waiting-list/patient-waiting-list.module').then(m => m.PatientWaitingListModule) },

  { path: 'PatientVaccinationList', loadChildren: () => import('./feature/patient-vaccination-list/patient-vaccination-list.module').then(m => m.PatientVaccinationListModule) },

  { path: 'Patient_Vaccination/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-vaccination-detail/patient-vaccination-detail.module').then(m => m.PatientVaccinationDetailModule) },

  { path: 'PurchaseOrderList', loadChildren: () => import('./feature/purchase-order-list/purchase-order-list.module').then(m => m.PurchaseOrderListModule) },

  { path: 'PurchaseOrderDetail', loadChildren: () => import('./feature/purchase-order-detail/purchase-order-detail.module').then(m => m.PurchaseOrderDetailModule) },

  { path: 'PurchaseOrder/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/purchase-order-detail/purchase-order-detail.module').then(m => m.PurchaseOrderDetailModule) },

  { path: 'ForBillingList', loadChildren: () => import('./feature/for-billing-list/for-billing-list.module').then(m => m.ForBillingListModule) },

  { path: 'ClientPatientRecords', loadChildren: () => import('./feature/client-patient-records/client-patient-records.module').then(m => m.ClientPatientRecordsModule) },

  { path: 'BillingInvoiceWalkInList', loadChildren: () => import('./feature/billing-invoice-walk-in-list/billing-invoice-walk-in-list.module').then(m => m.BillingInvoiceWalkInListModule) },

  { path: 'PatientWellnessList', loadChildren: () => import('./feature/patient-wellness-list/patient-wellness-list.module').then(m => m.PatientWellnessListModule) },

  { path: 'PatientWellnessDetail', loadChildren: () => import('./feature/patient-wellness-detail/patient-wellness-detail.module').then(m => m.PatientWellnessDetailModule) },

  { path: 'Patient_Wellness/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-wellness-detail/patient-wellness-detail.module').then(m => m.PatientWellnessDetailModule) },

  { path: 'PatientLodgingList', loadChildren: () => import('./feature/patient-lodging-list/patient-lodging-list.module').then(m => m.PatientLodgingListModule) },

  { path: 'Patient_Lodging/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-lodging-detail/patient-lodging-detail.module').then(m => m.PatientLodgingDetailModule) },

  { path: 'Dashboard', loadChildren: () => import('./feature/dashboard/dashboard.module').then(m => m.DashboardModule) },

  { path: 'VeterinaryHealthCertificateList', loadChildren: () => import('./feature/veterinary-health-certificate-list/veterinary-health-certificate-list.module').then(m => m.VeterinaryHealthCertificateListModule) },

  { path: 'VeterinaryHealthCertificate/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/veterinary-health-certificate-detail/veterinary-health-certificate-detail.module').then(m => m.VeterinaryHealthCertificateDetailModule) },

  { path: 'ReceivingReportList', loadChildren: () => import('./feature/receiving-report-list/receiving-report-list.module').then(m => m.ReceivingReportListModule) },

  { path: 'ReceivingReport/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/receiving-report-detail/receiving-report-detail.module').then(m => m.ReceivingReportDetailModule) },

  { path: 'Supplier/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/supplier-detail/supplier-detail.module').then(m => m.SupplierDetailModule) },

  { path: 'PaymentTransactionList', loadChildren: () => import('./feature/payment-transaction-list/payment-transaction-list.module').then(m => m.PaymentTransactionListModule) },

  { path: 'PaymentTransaction/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/payment-transaction-detail/payment-transaction-detail.module').then(m => m.PaymentTransactionDetailModule) },

  { path: 'PatientWaitingListCanceledList', loadChildren: () => import('./feature/patient-waiting-list-canceled-list/patient-waiting-list-canceled-list.module').then(m => m.PatientWaitingListCanceledListModule) },

  { path: 'CurrentEmployeeInfo', loadChildren: () => import('./feature/employee-current-detail/employee-current-detail.module').then(m => m.EmployeeCurrentDetailModule) },

  { path: 'SMSList', loadChildren: () => import('./feature/smslist-view/smslist-view.module').then(m => m.SMSListViewModule) },

  { path: 'ReferenceLink/:Code', loadChildren: () => import('./feature/reference-link-view/reference-link-view.module').then(m => m.ReferenceLinkViewModule) },

  { path: 'NoAccess', loadChildren: () => import('./feature/no-access/no-access.module').then(m => m.NoAccessModule) },

  { path: 'PayableList', loadChildren: () => import('./feature/payable-list/payable-list.module').then(m => m.PayableListModule) },

  { path: 'Payable', loadChildren: () => import('./feature/payable/payable.module').then(m => m.PayableModule) },

  //{ path: 'UserList', loadChildren: () => import('./feature/user-list/user-list.module').then(m => m.UserListModule) },

  { path: 'User/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/user-detail/user-detail.module').then(m => m.UserDetailModule) },

  { path: 'PatientGroomingList', loadChildren: () => import('./feature/patient-grooming-list/patient-grooming-list.module').then(m => m.PatientGroomingListModule) },

  { path: 'Patient_Grooming/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-grooming-detail/patient-grooming-detail.module').then(m => m.PatientGroomingDetailModule) },

  { path: 'InactiveSMSSendingList', loadChildren: () => import('./feature/inactive-smssending-list/inactive-smssending-list.module').then(m => m.InactiveSMSSendingListModule) },

  { path: 'InactiveSMSSending/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/inactive-smssending-detail/inactive-smssending-detail.module').then(m => m.InactiveSMSSendingDetailModule) },

  { path: 'InactiveSMSSendingMultipleDetail/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/inactive-smssending-multiple-detail/inactive-smssending-multiple-detail.module').then(m => m.InactiveSMSSendingMultipleDetailModule) },

  { path: 'ReportInvoiceItemsAndServicesDetail', loadChildren: () => import('./feature/report-pos-detail/report-pos-detail.module').then(m => m.ReportPOSDetailModule) },

  { path: 'ReportExpenses', loadChildren: () => import('./feature/report-expenses/report-expenses.module').then(m => m.ReportExpensesModule) },

  { path: 'ScheduleCalendar', loadChildren: () => import('./feature/schedule-calendar/schedule-calendar.module').then(m => m.ScheduleCalendarModule) },

  { path: 'PatientReferralList', loadChildren: () => import('./feature/patient-referral-list/patient-referral-list.module').then(m => m.PatientReferralListModule) },

  { path: 'PatientReferral/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-referral-detail/patient-referral-detail.module').then(m => m.PatientReferralDetailModule) },

  { path: 'ReportItemPriceComparisonPerSupplier', loadChildren: () => import('./feature/report-item-price-comparison-per-supplier/report-item-price-comparison-per-supplier.module').then(m => m.ReportItemPriceComparisonPerSupplierModule) },

  { path: 'ReportServiceList', loadChildren: () => import('./feature/report-service-list/report-service-list.module').then(m => m.ReportServiceListModule) },

  { path: 'ReportMedicalServiceList', loadChildren: () => import('./feature/report-medical-service-list/report-medical-service-list.module').then(m => m.ReportMedicalServiceListModule) },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
