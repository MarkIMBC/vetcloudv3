﻿using JSLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;

namespace TextBlastSMSSender
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic appSettings = getAppSettings();

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd;

            Console.WriteLine("Vet Cloud SMS Sender Service run as of {0}", DateTime.Now.ToString("MM/dd/yyyy ddd hh:mm tt"));

            try
            {

                sendSMS(appSettings);


                dateEnd = DateTime.Now;
                System.TimeSpan diff2 = dateEnd - dateStart;
                Console.WriteLine("Done.. Duration - {0}", diff2.ToString("hh\\:mm\\:ss"));
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            Thread.Sleep(5000);

        }

        public static void sendSMS(dynamic appSettings)
        {
            string connectonString = appSettings.ConnectionString;
            string apiCode = appSettings.ITexMoAPICode;
            string apiPassword = appSettings.ITexMoAPIPassword;

            List<int> soapPlan = new List<int>();

            var dbAccess = new DBCollection(connectonString);

            var DetailView = dbAccess.GetDynamicObject("EXEC dbo.pGetForSendTextBlastMessage", true);

            var records = DetailView.records as List<dynamic>;

            if (records != null)
            {

                foreach (var d in records)
                {
           
                    var isAllowedObj = dbAccess.GetDynamicObject("EXEC dbo.pCheckCompanyTextBlastSMSSending " + d.ID_Company + ", '" + d.DateSending + "', " + d.ID_TextBlast_Client + "", true);

                    var result = "";

                    if (isAllowedObj.IsAllowedToSendSMS)
                    {
                        result = JSLibrary.Common.Utility.sendTextMessage(d.ContactNumber, d.TextBlastMessage, apiCode, apiPassword);
                    }
                    else
                    {
                        result = "-1";
                    }

                    Console.WriteLine(
                        "Status {0} - {1} - {2} - {3} - {4}",
                        result,
                        d.Name_Company, d.Code_TextBlast, d.Name_Client, d.ContactNumber
                    );

                    var _result = new DBCollection(connectonString).GetDynamicObject("EXEC dbo.[pNoteTextBlastClientMessage] @ID_TextBlast_Client = " + d.ID_TextBlast_Client + ", @iTextMo_Status = " + result + "", true);
                }

            }

        }

        public static Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }


        public static dynamic getAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader(@$"{currentDirectory}/appsettings.json");

            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }
    }
}

