﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 FUNCTION [dbo].[fGetSendSoapPlanDateCovered](@MinDate          DATE,
                                                   @MaxDate          DATE,
                                                   @IsSMSSent        BIT = NULL,
                                                   @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETIME,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETIME,
  DateCreated          DATETIME,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX),
  Count                INT)
AS
  BEGIN
      DECLARE @_Date DATE = ''
      DECLARE db_cursorDateCovered CURSOR FOR
        SELECT TOP (DATEDIFF(DAY, @MinDate, @MaxDate) + 1) Date = DATEADD(DAY, ROW_NUMBER()
                                                                                 OVER(
                                                                                   ORDER BY a.object_id) - 1, @MinDate)
        FROM   sys.all_objects a
               CROSS JOIN sys.all_objects b;

      OPEN db_cursorDateCovered

      FETCH NEXT FROM db_cursorDateCovered INTO @_Date

      WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT @table
                   (ID_Company,
                    Name_Company,
                    Name_Client,
                    ContactNumber_Client,
                    DateReturn,
                    Name_Item,
                    Comment,
                    Message,
                    DateSending,
                    DateCreated,
                    ID_Reference,
                    Oid_Model,
                    Code,
                    Count)
            SELECT ID_Company,
                   Name_Company,
                   Name_Client,
                   ContactNumber_Client,
                   DateReturn,
                   Name_Item,
                   Comment,
                   Message,
                   DateSending,
                   DateCreated,
                   ID_Reference,
                   Oid_Model,
                   Code,
                   Count
            FROM   dbo.[fGetSendSoapPlan](@_Date, @IsSMSSent, @IDsCompanyString)

            FETCH NEXT FROM db_cursorDateCovered INTO @_Date
        END

      CLOSE db_cursorDateCovered

      DEALLOCATE db_cursorDateCovered

      RETURN
  END

GO
