﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoice_SMSPayableRemider] AS   
    SELECT   
     H.*  
    FROM vBillingInvoice_SMSPayableRemider H  
    WHERE ISNULL(IsActive, 0) = 1
GO
