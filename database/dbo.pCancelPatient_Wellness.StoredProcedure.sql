﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelPatient_Wellness] (@IDs_Patient_Wellness typIntList READONLY,
                                            @ID_UserSession       INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatient_Wellness
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Wellness bi
                 INNER JOIN @IDs_Patient_Wellness ids
                         ON bi.ID = ids.ID;

          INSERT @IDs_Patient
          SELECT ID_Patient
          FROM   dbo.tPatient_Wellness hed
                 inner join @IDs_Patient_Wellness ids
                         on hed.ID = ids.ID

          exec [pUpdatePatientsLastVisitedDate]
            @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
