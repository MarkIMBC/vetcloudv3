﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveRecordValueTransferLogs] AS   
				SELECT   
				 H.*  
				FROM vRecordValueTransferLogs H  
				WHERE ISNULL(IsActive, 0) = 0
GO
