﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzInventorySummaryReport]
AS
  SELECT item.ID                                                 ID,
         LTRIM(RTRIM(item.Name))                                 Name_Item,
         SUM(IsNULL(hed.Quantity, 0))                            TotalQuantity,
         OtherInfo_DateExpiration                                DateExpired,
         FORMAT(OtherInfo_DateExpiration, 'MM/dd/yyyy')          FormattedDateExpired,
         NULL                                                    BatchNo,
         MAX(CASE
               WHEN hed.Quantity > 0 THEN Date
               ELSE NULL
             END)                                                DateLastIn,
         MAX(CASE
               WHEN hed.Quantity < 0 THEN Date
               ELSE NULL
             END)                                                DateLastOut,
         ISNULL(item.UnitCost, 0)                                UnitCost,
         ISNULL(item.UnitPrice, 0)                               UnitPrice,
         item.ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         CASE
           WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN '('
                                                               + dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired' + ')', 'Expired')
           ELSE ''
         END                                                     RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
         invtStatus.Name                                         Name_InventoryStatus,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         item.ID_InventoryStatus
  FROM   dbo.tItem item
         LEFT JOIN dbo.tInventoryTrail hed
                ON item.ID = hed.ID_Item
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
         LEFT JOIN dbo.tInventoryStatus invtStatus
                ON invtStatus.ID = item.ID_InventoryStatus
  WHERE  ISNULL(item.IsActive, 0) = 1
         and ID_ITEMTYpe = 2
  GROUP  BY item.ID,
            item.Name,
            ISNULL(item.UnitCost, 0),
            ISNULL(item.UnitPrice, 0),
            company.ImageLogoLocationFilenamePath,
            company.Name,
            OtherInfo_DateExpiration,
            company.Address,
            item.ID_Company,
            invtStatus.Name,
            company.ContactNumber,
            company.Email,
            item.ID_InventoryStatus

GO
