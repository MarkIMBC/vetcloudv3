﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardMunicipalitySummary] @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @TotalResidentCount INT = 0
      DECLARE @MonthlyVaccinationCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @TotalResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1

      SELECT @MonthlyVaccinationCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND IsVaccinated = 1
             AND YEAR(DateLastVaccination) = YEAR(GETDATE())
             AND MONTH(DateLastVaccination) = MONTH(GETDATE())

      SELECT @TotalResidentMaleCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND resident.ID_Gender = 1

      SELECT @TotalResidentFemaleCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND resident.ID_Gender = 2

      SELECT '_'

      SELECT ISNULL(@TotalResidentCount, 0)       TotalResidentCount,
             ISNULL(@MonthlyVaccinationCount, 0)  MonthlyVaccinationCount,
             ISNULL(@TotalResidentMaleCount, 0)   TotalResidentMaleCount,
             ISNULL(@TotalResidentFemaleCount, 0) TotalResidentFemaleCount
  END

GO
