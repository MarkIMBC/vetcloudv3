﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCustomDetailViewRoute] AS   
				SELECT   
				 H.*  
				FROM vCustomDetailViewRoute H  
				WHERE ISNULL(IsActive, 0) = 0
GO
