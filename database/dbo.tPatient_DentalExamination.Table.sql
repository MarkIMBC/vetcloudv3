﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_DentalExamination](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_Patient] [int] NULL,
	[ID_Doctor] [int] NULL,
	[Date] [datetime] NULL,
	[GUID] [varchar](300) NULL,
	[ID_Dentition] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_ApprovedBy] [int] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[DateCanceled] [datetime] NULL,
 CONSTRAINT [PK_tPatient_DentalExamination] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_DentalExamination] ON [dbo].[tPatient_DentalExamination]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_DentalExamination] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_DentalExamination]  WITH CHECK ADD  CONSTRAINT [FKtPatient_DentalExamination_ID_Patient] FOREIGN KEY([ID_Patient])
REFERENCES [dbo].[tPatient] ([ID])
GO
ALTER TABLE [dbo].[tPatient_DentalExamination] CHECK CONSTRAINT [FKtPatient_DentalExamination_ID_Patient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_DentalExamination]
		ON [dbo].[tPatient_DentalExamination]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tPatient_DentalExamination] ENABLE TRIGGER [rDateCreated_tPatient_DentalExamination]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_DentalExamination]
		ON [dbo].[tPatient_DentalExamination]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tPatient_DentalExamination] ENABLE TRIGGER [rDateModified_tPatient_DentalExamination]
GO
