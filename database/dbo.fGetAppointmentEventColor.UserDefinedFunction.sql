﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fGetAppointmentEventColor](@Name VARCHAR(MAX))
RETURNs VARCHAR(MAX) AS
BEGIN

	DECLARE @Color VARCHAR(MAX) = '';

	IF(@Name ='Patient_SOAP')
	BEGIN 
		SET @Color = 'rgb(17, 111, 191)'
	END
	ELSE 
		IF(@Name = 'PatientAppointment')
		BEGIN 
			SET @Color = 'rgb(218, 72, 3)'
		END

	return @Color
END

GO
