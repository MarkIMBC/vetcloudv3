﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePayable_Detail] AS   
				SELECT   
				 H.*  
				FROM vPayable_Detail H  
				WHERE IsActive = 1
GO
