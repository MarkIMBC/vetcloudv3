﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddAppointmentToWaitingList](@Oid_Model                    VARCHAR(MAX),
                                        @Appointment_ID_CurrentObject INT,
                                        @ID_Patient                   INT,
                                        @ID_UserSession               INT)
AS
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    DECLARE @DateCreatedMax Date

    SELECT @DateCreatedMax = CONVERT(DATE, MAX(DateCreated))
    FROm   tPatientWaitingList

    if( @DateCreatedMax < CONVERT(DATE, GETDATE()) )
      begin
          Update tDocumentSeries
          set    Counter = 1
          FROM   tDocumentSeries docSeries
                 INNER JOIN _tModel model
                         on docSeries.ID_Model = model.Oid
          where  TableName = 'tPatientWaitingList'
      END

    INSERT INTO [dbo].[tPatientWaitingList]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_Client],
                 [ID_Patient],
                 [WaitingStatus_ID_FilingStatus],
                 Oid_Model_Reference,
                 ID_Reference)
    SELECT H.Name,
           H.IsActive,
           patient.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           patient.ID_Client,
           patient.ID ID_Patient,
           @Waiting_ID_FilingStatus,
           @Oid_Model,
           @Appointment_ID_CurrentObject
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDate() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatient patient
    where  ID = @ID_Patient

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @Waiting_ID_FilingStatus
    FROM   tPatient patient
    where  ID = @ID_Patient

GO
