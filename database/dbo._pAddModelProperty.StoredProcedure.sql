﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[_pAddModelProperty] @TableName    VARCHAR(100),
                                           @PropertyName VARCHAR(100),
                                           @Type         INT = 1
AS
  BEGIN
      IF COL_LENGTH(@TableName, @PropertyName) IS NOT NULL
        BEGIN
            return;
        END

      BEGIN TRANSACTION;

      BEGIN TRY
          SET NOCOUNT ON;

          DECLARE @Message     VARCHAR(MAX),
                  @DataType    VARCHAR(200),
                  @ID_Model    UNIQUEIDENTIFIER = NULL,
                  @InfoSection UNIQUEIDENTIFIER = NULL

          IF NOT EXISTS (SELECT object_id
                         FROM   sys.tables
                         WHERE  name = @TableName)
            BEGIN
                SET @Message = @TableName + ' NOT EXISTS'

                RAISERROR (@Message,16,1 );

                RETURN
            END

          SELECT @ID_Model = Oid
          FROM   dbo.[_tModel]
          WHERE  TableName = @TableName

          IF ( @ID_Model = NULL )
            BEGIN
                SET @Message = @TableName + ' must exits in tModelTable'

                RAISERROR (@Message,16,1 );

                RETURN
            END

          SELECT @DataType = CASE @Type
                               WHEN 1 THEN 'VARCHAR(300)'
                               WHEN 2 THEN 'INT'
                               WHEN 3 THEN 'DECIMAL(18,4)'
                               WHEN 4 THEN 'BIT'
                               WHEN 5 THEN 'DATETIME'
                               WHEN 6 THEN 'DATE'
                               WHEN 7 THEN 'DATETIME'
                               WHEN 8 THEN 'UNIQUEIDENTIFIER'
                             --WHEN 9 THEN '' Doest Not Support List      
                             END

          DECLARE @SQL VARCHAR(300) = 'ALTER TABLE ' + @TableName + ' ADD '
            + @PropertyName + ' ' + @DataType

          EXEC( @SQL )

          DECLARE @Oid_Property UNIQUEIDENTIFIER = NEWID()

          INSERT INTO dbo.[_tModel_Property]
                      (Oid,
                       Name,
                       IsActive,
                       Caption,
                       ID_Model,
                       ID_PropertyType,
                       ID_CreatedBy,
                       ID_LastModifiedBy,
                       DateCreated,
                       DateModified)
          VALUES      ( @Oid_Property,
                        @PropertyName,
                        1,
                        NULL,
                        @ID_Model,
                        @Type,
                        1,
                        NULL,
                        GETDATE(),
                        NULL )

          INSERT INTO dbo.[_tDetailView_Detail]
                      (Oid,
                       Code,
                       Name,
                       IsActive,
                       Comment,
                       ID_DetailView,
                       ID_ModelProperty,
                       ID_CreatedBy,
                       ID_LastModifiedBy,
                       DateCreated,
                       DateModified,
                       ID_Section,
                       SeqNo)
          SELECT NEWID(),
                 NULL,
                 @PropertyName,
                 1,
                 NULL,
                 Oid,
                 @Oid_Property,
                 1,
                 NULL,
                 GETDATE(),
                 NULL,
                 NULL,
                 --                        ( SELECT    TOP 1 Oid      
                 --                          FROM      dbo.[_tDetailView_Detail]      
                 --                          WHERE     Name LIKE '%InfoSection%'      
                 --                                    AND ID_ControlType = 13      
                 --                                    AND ID_DetailView = D.Oid      
                 --                        ), --TAB      
                 -10000
          FROM   dbo.[_tDetailView] D
          WHERE  D.ID_Model = @ID_Model

          INSERT INTO dbo.[_tListView_Detail]
                      (Oid,
                       Code,
                       Name,
                       IsActive,
                       Comment,
                       ID_ListView,
                       ID_ModelProperty,
                       ID_CreatedBy,
                       ID_LastModifiedBy,
                       DateCreated,
                       DateModified,
                       IsVisible,
                       VisibleIndex)
          SELECT NEWID(),
                 NULL,
                 @PropertyName,
                 1,
                 NULL,
                 Oid,
                 @Oid_Property,
                 1,
                 NULL,
                 GETDATE(),
                 NULL,
                 1,
                 1000
          FROM   dbo.[_tListView]
          WHERE  ID_Model = @ID_Model

          PRINT @PropertyName + ' (' + @TableName
                + ') Successfully Added'

          SET NOCOUNT OFF
      END TRY
      BEGIN CATCH
          SELECT ERROR_NUMBER()    AS ErrorNumber,
                 ERROR_SEVERITY()  AS ErrorSeverity,
                 ERROR_STATE()     AS ErrorState,
                 ERROR_PROCEDURE() AS ErrorProcedure,
                 ERROR_LINE()      AS ErrorLine,
                 ERROR_MESSAGE()   AS ErrorMessage,
                 @TableName        TableName,
                 @PropertyName     PropertyName;
          IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
      END CATCH;

      IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
  END

GO
