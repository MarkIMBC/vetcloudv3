﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pUpdateBillingInvoiceSMSReminderRemainingBalance](@IDs_BillingInvoice typINTLIst Readonly)
AS
  BEGIN
      IF(SELECT COUNT(*)
         FROM   @IDs_BillingInvoice) > 0
        BEGIN
            Update tBillingInvoice_SMSPayableRemider
            SET    RemainingAmount_BillingInvoice = bi.RemainingAmount
            FROM   tBillingInvoice_SMSPayableRemider bisms
                   inner join tBillingInvoice bi
                           on bi.ID = bisms.ID_BillingInvoice
                   INNER JOIN @IDs_BillingInvoice ids
                           on bi.ID = ids.ID
            where  ISNULL(bisms.IsSentSMS, 0) = 0
                   AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
                   AND bi.Payment_ID_FilingStatus IN ( 2, 11 )
                   and bi.ID_FilingStatus = 3
        END
      else
        BEGIN
            Update tBillingInvoice_SMSPayableRemider
            SET    RemainingAmount_BillingInvoice = bi.RemainingAmount
            FROM   tBillingInvoice_SMSPayableRemider bisms
                   inner join tBillingInvoice bi
                           on bi.ID = bisms.ID_BillingInvoice
            where  ISNULL(bisms.IsSentSMS, 0) = 0
                   AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
                   AND bi.Payment_ID_FilingStatus IN ( 2, 11 )
                   and bi.ID_FilingStatus = 3
        END
  END

GO
