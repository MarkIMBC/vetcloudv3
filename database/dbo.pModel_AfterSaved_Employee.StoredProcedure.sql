﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_Employee] (@ID_CurrentObject VARCHAR(10),
                                               @IsNew            BIT = 0)
AS
  BEGIN
      Update tUser
      set    Name = _employee.Name
      FROM   tUser _user
             inner join tEmployee _employee
                     on _user.ID_Employee = _employee.ID
      WHERE  _employee.ID = @ID_CurrentObject
  END;

GO
