﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGet_Patient_SOAP_RecordInfo_LaboratoryImages](@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @LaboratoryImageCounter TABLE
        (
           ID_Patient_SOAP INT,
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @LaboratoryImageCounter
             (ID_Patient_SOAP,
              Count)
      SELECT _soapimage.ID_Patient_SOAP,
             COUNT(*) LaboratoryImages
      FROM   vPatient_SOAP_LaboratoryImages _soapimage WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _soapimage.ID_Patient_SOAP = ids.ID
      GROUP  BY _soapimage.ID_Patient_SOAP

      UPDATE @detail
      SET    TagString = CASE
                           WHEN LEN(ISNULL(_soap.Interpretation, '')) > 0 THEN 'Interpretation: '
                                                                               + ISNULL(_soap.Interpretation, '')
                           ELSE ''
                         END
      FROM   @detail _detail
             inner JOIN tPatient_SOAP _soap
                     on _detail.ID_Patient_SOAP = _soap.ID

      UPDATE @detail
      SET    TagString = TagString
                         + CASE
                             WHEN LEN(TagString) > 0 THEN '/*break*/ '
                             ELSE ''
                           END
                         + CASE
                             WHEN LEN(_labImageCounter.Count) > 0 THEN 'Laboratory Count: '
                                                                       + FORMAT(_labImageCounter.Count, '#,#0')
                                                                       + '/*break*/ '
                             ELSE ''
                           END,
             Count = _labImageCounter.Count
      FROM   @detail _detail
             inner JOIN @LaboratoryImageCounter _labImageCounter
                     on _detail.ID_Patient_SOAP = _labImageCounter.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO
