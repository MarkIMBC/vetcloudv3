﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tClient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](2000) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ContactNumber] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[Address] [varchar](8000) NULL,
	[ContactNumber2] [varchar](300) NULL,
	[Old_client_id] [int] NULL,
	[tempID] [varchar](300) NULL,
	[DateLastVisited] [datetime] NULL,
	[CurrentCreditAmount] [decimal](18, 4) NULL,
	[CustomCode] [varchar](300) NULL,
	[ID_User] [int] NULL,
	[ReferenceID] [varchar](300) NULL,
	[ProfileImageFile] [varchar](300) NULL,
	[LastAttendingPhysician_ID_Employee] [int] NULL,
	[TotalRemainingAmount] [decimal](18, 4) NULL,
 CONSTRAINT [PK_tClient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tClient] ADD  CONSTRAINT [DF__tClient__IsActiv__26788A3F]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tClient]  WITH CHECK ADD  CONSTRAINT [FK_tClient_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tClient] CHECK CONSTRAINT [FK_tClient_ID_Company]
GO
ALTER TABLE [dbo].[tClient]  WITH CHECK ADD  CONSTRAINT [FK_tClient_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClient] CHECK CONSTRAINT [FK_tClient_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tClient]  WITH CHECK ADD  CONSTRAINT [FK_tClient_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClient] CHECK CONSTRAINT [FK_tClient_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tClient]
		ON [dbo].[tClient]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClient
			SET    DateCreated = GETDATE()
			FROM   dbo.tClient hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClient] ENABLE TRIGGER [rDateCreated_tClient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tClient]
		ON [dbo].[tClient]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClient
			SET    DateModified = GETDATE()
			FROM   dbo.tClient hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClient] ENABLE TRIGGER [rDateModified_tClient]
GO
