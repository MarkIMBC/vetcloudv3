﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoMergePatientRecord](@From_ID_Patient INT,
                                 @To_ID_Patient   INT,
                                 @ID_UserSession  INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Patient VARCHAR(MAX) = ''
      DECLARE @To_Code_Patient VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Patient

      SELECT @To_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Patient

      --select @To_Code_Patient,
      --       @To_ID_Patient
      BEGIN TRY
          exec pMergePatientRecordByCompanyID
            @ID_Company,
            @From_Code_Patient,
            @To_Code_Patient
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO
