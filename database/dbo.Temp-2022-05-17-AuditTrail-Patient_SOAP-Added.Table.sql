﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp-2022-05-17-AuditTrail-Patient_SOAP-Added](
	[Name_Company] [varchar](200) NULL,
	[Name_Employee] [varchar](300) NULL,
	[ID_Company] [int] NULL,
	[Date] [datetime] NULL,
	[Description] [varchar](300) NULL,
	[Model] [varchar](150) NULL,
	[ID_CurrentObject] [varchar](50) NULL
) ON [PRIMARY]
GO
