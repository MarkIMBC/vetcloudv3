﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pNoteTextBlastClientMessage] (@ID_TextBlast_Client INT,
                                                @iTextMo_Status      INT)
AS
  BEGIN
      /*  
       iTextMo Status  
       
       "1" = Invalid Number.  
       "2" = Number prefix not supported. Please contact us so we can add.  
       "3" = Invalid ApiCode.  
       "4" = Maximum Message per day reached. This will be reset every 12MN.  
       "5" = Maximum allowed characters for message reached.  
       "6" = System OFFLINE.  
       "7" = Expired ApiCode.  
       "8" = iTexMo Error. Please try again later.  
       "9" = Invalid Function Parameters.  
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.  
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.  
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.  
       "13" = Invalid or Not Registered Custom Sender ID.  
       "14" = Invalid preferred server number.  
       "15" = IP Filtering enabled - Invalid IP.  
       "16" = Authentication error. Contact support at support@itexmo.com  
       "17" = Telco Error. Contact Support support@itexmo.com  
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com  
       "19" = Account suspended. Contact Support support@itexmo.com  
       "0" = Success! Message is now on queue and will be sent soon  
      */
      DECLARE @Success BIT = 1;

      IF @iTextMo_Status = 0
        BEGIN
            UPDATE dbo.tTextBlast_Client
            SET    IsSent = 1,
                   DateSent = GETDATE()
            FROM   tTextBlast_Client psp
            WHERE  psp.ID = @ID_TextBlast_Client
        END

      INSERT INTO [dbo].[tTextBlast_Client_SMSStatus]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [iTextMo_Status],
                   [ID_TextBlast_Client])
      VALUES      (NULL,
                   NULL,
                   1,
                   1,
                   NULL,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @iTextMo_Status,
                   @ID_TextBlast_Client)

      SELECT '_'

      SELECT @Success Success;
  END

GO
