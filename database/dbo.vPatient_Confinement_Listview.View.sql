﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Confinement_Listview]
AS
  SELECT ID,
         Name_Client,
         Name_Patient,
         Code,
         Date,
         REPLACE(dbo.fGetAge(h.Date, case
                                       WHEN ISNULL(h.ID_FilingStatus, 0) = 15 then h.DateDischarge
                                       ELSE
                                         case
                                           WHEN ISNULL(h.ID_FilingStatus, 0) = 4 then h.DateCanceled
                                           ELSE GETDATE()
                                         END
                                     END, '1 day'), ' old', '') ConfinementDays,
         DATEDIFF(Day, h.Date, case
                                 WHEN ISNULL(h.ID_FilingStatus, 0) = 15 then h.DateDischarge
                                 ELSE
                                   case
                                     WHEN ISNULL(h.ID_FilingStatus, 0) = 4 then h.DateCanceled
                                     ELSE GETDATE()
                                   END
                               END)                             ConfinementDayCount,
         ID_Company,
         ID_FilingStatus,
         BillingInvoice_ID_FilingStatus,
         DateDischarge,
         Name_FilingStatus,
         BillingInvoice_Name_FilingStatus,
         ID_Client,
         ID_Patient
  FROM   vPatient_Confinement h
  WHERE ISNULL(h.IsActive, 0) = 1

GO
