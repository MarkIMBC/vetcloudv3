﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTextBlast](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[ReccurrenceCount] [int] NULL,
	[ID_TimePeriodType] [int] NULL,
	[Message] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
	[DateApproved] [datetime] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[ID_CompanyTextBlastTemplate] [int] NULL,
 CONSTRAINT [PK_tTextBlast] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tTextBlast] ON [dbo].[tTextBlast]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tTextBlast] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tTextBlast]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast] CHECK CONSTRAINT [FK_tTextBlast_ID_Company]
GO
ALTER TABLE [dbo].[tTextBlast]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast] CHECK CONSTRAINT [FK_tTextBlast_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tTextBlast]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast] CHECK CONSTRAINT [FK_tTextBlast_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tTextBlast]
		ON [dbo].[tTextBlast]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTextBlast
			SET    DateCreated = GETDATE()
			FROM   dbo.tTextBlast hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTextBlast] ENABLE TRIGGER [rDateCreated_tTextBlast]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tTextBlast]
		ON [dbo].[tTextBlast]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTextBlast
			SET    DateModified = GETDATE()
			FROM   dbo.tTextBlast hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTextBlast] ENABLE TRIGGER [rDateModified_tTextBlast]
GO
