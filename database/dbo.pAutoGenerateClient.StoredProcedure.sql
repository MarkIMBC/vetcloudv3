﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create    
 PROC [dbo].[pAutoGenerateClient]  
as  
    DECLARE @IDs_Company TABLE  
      (  
         RowID      INT,  
         ID_Company INT  
      )  
    DECLARE @currentCounter INT = 1  
    DECLARE @maxCounter INT = 1  
  
    INSERT @IDs_Company  
    SELECT ROW_NUMBER()  
             OVER(  
               ORDER BY ID ASC) AS RowID,  
           ID  
    FROM   tCompany  
  
    SELECT @maxCounter = COUNT(*)  
    FROM   @IDs_Company  
  
    WHILE @currentCounter <= @maxCounter  
      BEGIN  
          DECLARE @ID_Company INT = 0  
  
          SELECT @ID_Company = ID_Company  
          FROM   @IDs_Company  
          WHERE  RowID = @currentCounter  
  
          exec pAutoGenerateClientPerCompany  
            @ID_Company  
  
          SET @currentCounter = @currentCounter + 1  
      END  
GO
