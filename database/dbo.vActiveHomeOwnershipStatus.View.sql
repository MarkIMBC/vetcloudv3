﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveHomeOwnershipStatus] AS   
				SELECT   
				 H.*  
				FROM vHomeOwnershipStatus H  
				WHERE IsActive = 1
GO
