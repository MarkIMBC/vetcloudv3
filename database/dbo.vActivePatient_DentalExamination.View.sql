﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_DentalExamination] AS   
				SELECT   
				 H.*  
				FROM vPatient_DentalExamination H  
				WHERE IsActive = 1
GO
