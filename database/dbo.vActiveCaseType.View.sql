﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCaseType] AS   
				SELECT   
				 H.*  
				FROM vCaseType H  
				WHERE IsActive = 1
GO
