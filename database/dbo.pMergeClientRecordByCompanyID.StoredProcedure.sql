﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pMergeClientRecordByCompanyID](@ID_Company              INT,
                                         @Source_Code_Client      VARCHAR(MAX),
                                         @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      --SELECT *
      --FROm   vCompanyActive
      --WHERE  ID = @ID_Company
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------      
      DECLARE @Source1_ID_Client INT
      DECLARE @Destination_ID_Client INT
      DECLARE @Source1_Name_Client VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Client VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Client = ID,
             @Source1_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Source_Code_Client
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Client = ID,
             @Destination_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Destination_Code_Client
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Client - '
                     + ' from ' + @Source1_Name_Client + ' ('
                     + @Source_Code_Client + ') to '
                     + @Destination_Name_Client + ' ('
                     + @Destination_Code_Client + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1'
      --FROm   vClient
      --WHERE  ID = @Source1_ID_Client
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination'
      --FROm   vClient
      --WHERE  ID = @Destination_ID_Client
      --       AND ID_Company = @ID_Company
      exec pMergeClientRecord
        @Source1_ID_Client,
        @Destination_ID_Client,
        @Comment

      Update tClient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Client
             and ID_Company = @ID_Company

      Update tPatient_SOAP
      SET    ID_Client = patient.ID_Client
      FROM   tPatient_SOAP _soap
             inner join vPatient patient
                     on _soap.ID_Patient = patient.ID
      where  _soap.ID_Client <> patient.ID_Client
             AND patient.ID_Client = @Destination_ID_Client
  END

GO
