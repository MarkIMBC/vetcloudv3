﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInsertSystemVersion](@Name VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   dbo.[tSystemVersion]
         WHERE  Name = @Name) = 0
        BEGIN
            INSERT INTO [dbo].[tSystemVersion]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [DateCreated])
            VALUES      ( @Name,
                          1,
                          1,
                          GETDATE())
        END
  END

GO
