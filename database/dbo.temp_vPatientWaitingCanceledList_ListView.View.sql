﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[temp_vPatientWaitingCanceledList_ListView]
AS
  SELECT ROW_NUMBER()
           OVER(
             ORDER BY hed.Name_Client ASC, hed.Name_Patient ASC, hed.DateCreated ASC)          AS ID,
         hed.ID_Company,
        hed.DateCreated            DateStart,
        hed.DateCreated                    DateEnd,
         hed.ID_Client,
         hed.Name_Client,
         hed.ID_Patient,
         hed.Name_Patient,
         hed.WaitingStatus_ID_FilingStatus,
         hed.WaitingStatus_Name_FilingStatus,
         hed.IsQueued
  FROM   vPatientWaitingList hed
         INNER JOIN vPatientWaitingMaxCanceled maxcanceledWaiting
                 ON hed.ID = maxcanceledWaiting.ID_PatientWaitingList
         --LEFT JOIN vAppointmentEvent appointment
         --       on hed.ID_Client = appointment.ID_Client
         --          and hed.ID_Patient = appointment.ID_Patient
         LEFT JOIN tPatient patient
                on patient.ID = hed.ID_Patient
  where  hed.WaitingStatus_ID_FilingStatus = 4
          OR patient.WaitingStatus_ID_FilingStatus = 4
  GROUP  BY 
            hed.IsQueued,
            hed.ID_Company,
            hed.DateCreated,
            hed.DateCreated,
            hed.ID_Client,
            hed.Name_Client,
            hed.ID_Patient,
            hed.Name_Patient,
            hed.WaitingStatus_ID_FilingStatus,
            hed.WaitingStatus_Name_FilingStatus

GO
