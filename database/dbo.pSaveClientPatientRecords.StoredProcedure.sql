﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pSaveClientPatientRecords] (@clientpatients TYPSAVECLIENTPATIENTRECORD READONLY,
                                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_User INT = 0;
      DECLARE @ID_Company INT = 0;
      DECLARE @Oid_Model_Client VARCHAR(MAX) = '';
      DECLARE @Oid_Model_Patient VARCHAR(MAX) = '';

      SELECT @Oid_Model_Client = Oid
      FROM   _tModel
      WHERE  TableName = 'tClient'

      SELECT @Oid_Model_Patient = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient'

      SELECT @ID_User = userSession.ID_User,
             @ID_Company = _user.ID_Company
      FROM   dbo.tUserSession userSession
             INNER JOIN vUser _user
                     ON userSession.ID_User = _user.ID
      WHERE  userSession.ID = @ID_UserSession;

      BEGIN TRY
          DECLARE @ID_Client INT
          DECLARE @Code_Client VARCHAR(MAX) = '';
          DECLARE @Count_Patient INT = 0

          EXEC Psaveclientpatientrecords_validation
            @clientpatients,
            @ID_UserSession

          SET @Code_Client = dbo.Fgeneratedocumentseries(@Oid_Model_Client, @ID_Company, 0, NULL);

          SELECT DISTINCT @Count_Patient = Row_number()
                                             OVER (
                                               ORDER BY Name_Patient)
          FROM   @clientpatients
          GROUP  BY Name_Patient,
                    Species_Patient,
                    ID_Gender_Patient

          INSERT INTO [dbo].[tClient]
                      (Code,
                       [Name],
                       [Email],
                       [ContactNumber],
                       [ContactNumber2],
                       [Address],
                       Comment,
                       [IsActive],
                       [ID_Company],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy])
          SELECT DISTINCT @Code_Client,
                          Name,
                          Email,
                          ContactNumber,
                          ContactNumber2,
                          Address,
                          Comment,
                          1,
                          @ID_Company,
                          Getdate(),
                          Getdate(),
                          @ID_User,
                          @ID_User
          FROM   @clientpatients

          UPDATE dbo.tDocumentSeries
          SET    Counter = Counter + 1
          WHERE  ID_Model = @Oid_Model_Client
                 AND ID_Company = @ID_Company;

          SET @ID_Client = @@IDENTITY

          INSERT INTO [dbo].[tPatient]
                      (Code,
                       [Name],
                       [ID_Client],
                       [Species],
                       [ID_Gender],
                       [ID_Company],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy])
          SELECT DISTINCT dbo.Fgeneratedocumentseries(@Oid_Model_Patient, @ID_Company, Row_number()
                                                                                         OVER (
                                                                                           ORDER BY Name_Patient) - 1, NULL) AS RowNumber,
                          Name_Patient,
                          @ID_Client,
                          Species_Patient,
                          ID_Gender_Patient,
                          @ID_Company,
						  1,
                          Getdate(),
                          Getdate(),
                          @ID_User,
                          @ID_User
          FROM   @clientpatients

          UPDATE dbo.tDocumentSeries
          SET    Counter = Counter + @Count_Patient
          WHERE  ID_Model = @Oid_Model_Patient
                 AND ID_Company = @ID_Company;
      END TRY
      BEGIN CATCH
          SET @Success = 0
          SET @message = Error_message()
      END CATCH

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END 
GO
