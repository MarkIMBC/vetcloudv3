﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetBackUpPath]()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = '';

      SET @result = 'C:\inetpub\wwwroot\VetCloudv3-System\database\'

      return @result
  END

GO
