﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBusinessPartner] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartner H  
				WHERE ISNULL(IsActive, 0) = 0
GO
