﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pDoAddPatientToWaitingList](@IDs_Patient    typIntList READONLY,
                                      @ID_UserSession INT)
AS
    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0;

    BEGIN TRY
        SELECT @ID_User = ID_User
        FROM   dbo.tUserSession
        WHERE  ID = @ID_UserSession;

        exec [pDoAddPatientToWaitingList_validation]
          @IDs_Patient,
          @ID_UserSession

        exec pAddPatientToQueueWaitingList
          @IDs_Patient,
          @ID_UserSession
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO
