﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vSMSList_Patient_Vaccination_Schedule]
AS
  SELECT vacSchedule.ID,
         vacSchedule.ID_Patient_Vaccination,
         vac.Code,
         DateSent,
         vac.Name_Client,
         vac.Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), vac.Name_Patient, ISNULL(vac.Name_Item, ''), ISNULL(vacSchedule.Comment, ''), vacSchedule.Date) Message,
         DATEADD(DAY, -1, vacSchedule.Date)                                                                                                                                                                 DateSending,
         vac.ID_Company,
         ISNULL(vacSchedule.IsSentSMS, 0)                                                                                                                                                                   IsSentSMS,
         model.Oid                                                                                                                                                                                          Oid_Model
  FROM   vPatient_Vaccination vac
         INNER JOIN vPatient_Vaccination_Schedule vacSchedule
                 on vacSchedule.ID_Patient_Vaccination = vac.ID
         LEFT JOIN vPatient_SOAP soap
                on vac.ID_Patient_SOAP = soap.ID
         Left JOIN tClient c
                on c.ID = vac.ID_Client
         LEFT JOIN tCOmpany comp
                on comp.ID = vac.ID_Company,
         _tModel model
  where  model.tableName = 'tPatient_Vaccination_Schedule'
         and vac.ID_FilingStatus NOT IN ( 4 )
         AND vac.ID NOT IN (SELECT DISTINCT ISNULL(ID_Patient_Vaccination, 0)
                            FROM   tPatient_Wellness) 
GO
