﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveGender] AS   
				SELECT   
				 H.*  
				FROM vGender H  
				WHERE ISNULL(IsActive, 0) = 0
GO
