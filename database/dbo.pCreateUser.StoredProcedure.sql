﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCreateUser](@ID_Company  INT,
                       @ID_Employee INT,
                       @Username    varchar(MAX))
as
  BEGIN
      Declare @code Varchar(MAX) = ''

      SET @Username = REPLACE(LOWER(@Username), ' ', '')

      IF(SELECT COUNT(*)
         FROM   vUser
         WHERE  ID_Company = @ID_Company
                AND ID_Employee = @ID_Employee) = 0
        BEGIN
            SELECT @code = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            INSERT dbo.tUser
                   (Code,
                    Name,
                    IsActive,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ID_Employee,
                    Username,
                    ID_UserGroup,
                    Password,
                    IsRequiredPasswordChangedOnLogin,
                    ID_Patient)
            SELECT '',
                   emp.Name,
                   1,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   emp.ID,
                   LOWER(@code) + '-' + LOWER(@Username),
                   1,
                   LEFT(NEWID(), 4),
                   1,
                   NULL
            FROM   dbo.tEmployee emp
            WHERE  emp.ID_Company = @ID_Company
                   AND emp.ID = @ID_Employee
        END
  --DECLARE @ID_User INT  
  --SET @ID_User = @@IDENTITY  
  --SELECT Name_Company,  
  --       Employee,  
  --       Username,  
  --       Password  
  --FROM   vUser  
  --where  ID = @ID_User  
  END

GO
