﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompanyTextBlastTemplate] AS   
				SELECT   
				 H.*  
				FROM vCompanyTextBlastTemplate H  
				WHERE ISNULL(IsActive, 0) = 0
GO
