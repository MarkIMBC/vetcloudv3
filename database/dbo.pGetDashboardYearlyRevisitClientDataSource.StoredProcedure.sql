﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create    
 PROC [dbo].[pGetDashboardYearlyRevisitClientDataSource] (@ID_UserSession INT,
                                                               @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      --SET @DateYear = FORMAT(GETDATE(), 'yyyy')      
      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      Declare @Months Table
        (
           MonthNumber INT,
           MonthName   varchar(20),
           TotalCount  INT
        )

      INSERT INTO @Months
                  (MonthNumber,
                   MonthName,
                   TotalCount)
      VALUES      (1,
                   'January',
                   0),
                  (2,
                   'February',
                   0),
                  (3,
                   'March',
                   0),
                  (4,
                   'April',
                   0),
                  (5,
                   'May',
                   0),
                  (6,
                   'June',
                   0),
                  (7,
                   'July',
                   0),
                  (8,
                   'August',
                   0),
                  (9,
                   'September',
                   0),
                  (10,
                   'October',
                   0),
                  (11,
                   'November',
                   0),
                  (12,
                   'December',
                   0)

      SELECT @DateYear                              DateYear,
             'Monthly Revisit Clients ' + @DateYear Label

      DECLARE @VisitedClientsFromSOAP TABLE
        (
           Year  INT,
           Month INT,
           Count INt
        )

      INSERT @VisitedClientsFromSOAP
      SELECT Year,
             Month,
             COUNT(DISTINCT ID_Client) AS NumberOfRevisitedClients
      FROM   (SELECT soapHed.ID_Client,
                     YEAR(soapHed.Date)  AS Year,
                     MONTH(soapHed.Date) AS Month,
                     COUNT(*)            AS NumberOfVisits
              FROM   tPatient_SOAP soapHed
              WHERE  soapHed.ID_FilingStatus NOT IN ( 4 )
                     AND soapHed.ID_Company = @ID_Company
                     AND YEAR(soapHed.Date) = @DateYear
              GROUP  BY soapHed.ID_Client,
                        YEAR(soapHed.Date),
                        MONTH(soapHed.Date)
              HAVING COUNT(*) > 1) AS RevisitedClientsPerMonth
      GROUP  BY Year,
                Month
      ORDER  BY Year,
                Month

      UPDATE @Months
      SET    TotalCount = tbl.Count
      FROM   @Months mon
             iNNER JOIN @VisitedClientsFromSOAP tbl
                     on mon.MonthNumber = tbl.Month

      SELECT *
      FROM   @Months
  END

GO
