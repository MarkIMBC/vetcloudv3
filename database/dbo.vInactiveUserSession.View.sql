﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUserSession] AS   
				SELECT   
				 H.*  
				FROM vUserSession H  
				WHERE ISNULL(IsActive, 0) = 0
GO
