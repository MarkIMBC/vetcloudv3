﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetRecordPaging] (@sql           VARCHAR(MAX),
                                     @PageNumber    DECIMAL(18, 0),
                                     @DisplayCount  DECIMAL(18, 0),
                                     @OrderByString VARCHAR(MAX) = NULL,
                                     @MinMaxColumn  VARCHAR(MAX) = NULL)
AS
  BEGIN
      DECLARE @RecordCount DECIMAL(18, 0) = 0;
      DECLARE @TotalPageNumber DECIMAL(18, 0) = 0;
      DECLARE @TotalPageNumberTotal TABLE
        (
           Count INT
        );
      DECLARE @sqlCount VARCHAR(8000) = '    
  SELECT COUNT(*)    
  FROM    
  (' + @sql + ') totalRecord    
  ';
      DECLARE @sqlMinMax VARCHAR(8000) = '    
  SELECT Min(' + @MinMAxColumn
        + ') MinValue,  Max(' + @MinMAxColumn + ') MaxValue   
  FROM    
  (' + @sql
        + ') MinMax    
  ';
      DECLARE @sqlResult VARCHAR(8000) = '    
   ' + @sql + '    
   '
        + CASE
            WHEN LEN(ISNULL(@OrderByString, '')) = 0 THEN 'ORDER BY ID DESC'
            ELSE 'ORDER BY ' + @OrderByString
          END
        + '    
   OFFSET '
        + CONVERT(VARCHAR(MAX), @DisplayCount)
        + ' * (' + CONVERT(VARCHAR(MAX), @PageNumber)
        + ' - 1) ROWS    
   FETCH NEXT '
        + CONVERT(VARCHAR(MAX), @DisplayCount) + ' ROWS ONLY;    
  ';

      INSERT @TotalPageNumberTotal
             (Count)
      EXEC (@sqlCount);

      SELECT @RecordCount = Count
      FROM   @TotalPageNumberTotal;

      SET @TotalPageNumber = CEILING(@RecordCount / @DisplayCount);

      SELECT '_',
             '' Records,
             '' MinMax;

      SELECT @RecordCount     TotalRecord,
             @DisplayCount    DisplayCount,
             @PageNumber      PageNumber,
             @TotalPageNumber TotalPageNumber;

      EXEC (@sqlResult);

      if( LEN(ISNULL(@MinMaxColumn, '')) > 0 )
        BEGIN
            EXEC (@sqlMinMax );
        END
      ELSE
        BEGIN
            SELECT NULL MinValue,
                   NULL MaxValue
        END
  END;

GO
