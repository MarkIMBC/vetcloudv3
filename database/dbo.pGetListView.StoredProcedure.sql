﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetListView]
    @Oid UNIQUEIDENTIFIER = NULL ,
    @ID_Session INT = NULL
AS
    BEGIN
        SELECT  '_' AS [_] ,
                '' AS [ListView_Detail]

        DECLARE @ID_User INT ,
            @ID_Warehouse INT
        SELECT  @ID_User = ID_User ,
                @ID_Warehouse = ID_Warehouse
        FROM    tUserSession
        WHERE   ID = @ID_Session

        IF ( @Oid IS NULL )
            BEGIN
                SELECT  H.*
                FROM    ( SELECT    NULL AS [_] ,
                                    NULL AS [Oid] ,
                                    NULL AS [Code] ,
                                    NULL AS [Name] ,
                                    1 AS [IsActive] ,
                                    NULL AS [ID_Model] ,
                                    NULL AS [DataSource] ,
                                    NULL AS [Comment] ,
                                    NULL AS [Caption] ,
                                    NULL AS [ID_CreatedBy] ,
                                    NULL AS [ID_LastModifiedBy] ,
                                    NULL AS [DateCreated] ,
                                    NULL AS [DateModified]
                        ) H
                        LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                        LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
            END
        ELSE
            BEGIN
                SELECT  H.*
                FROM    vListView H
                WHERE   H.Oid = @Oid
            END


        SELECT  *
        FROM    dbo.[_tListView_Detail] D
        WHERE   D.ID_ListView = @Oid
    END
GO
