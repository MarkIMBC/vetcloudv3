﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePayable_Detail] AS   
				SELECT   
				 H.*  
				FROM vPayable_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
