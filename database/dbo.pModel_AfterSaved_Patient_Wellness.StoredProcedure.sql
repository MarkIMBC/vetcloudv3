﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pModel_AfterSaved_Patient_Wellness] (@ID_CurrentObject VARCHAR(10),
                                                       @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Wellness
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Wellness';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Wellness
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Patient_Wellness TYPINTLIST
      DECLARE @IDs_Patient TYPINTLIST

      INSERT @IDs_Patient_Wellness
      VALUES (@ID_CurrentObject)

      INSERT @IDs_Patient
      SELECT ID_Patient
      FROM   dbo.tPatient_Wellness
      WHERE  ID = @ID_CurrentObject;

      exec [pUpdatePatientsLastVisitedDate]
        @IDs_Patient
  END;

GO
