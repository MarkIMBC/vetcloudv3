﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetUserNavigation] (@ID_UserSession INT)
AS
  BEGIN
      Declare @UserModulesNavigation table
        (
           ID_Navigation VARCHAR(MAX)
        )
      Declare @SelectedNavigation table
        (
           tempID      int IDENTITY(1, 1),
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )
      Declare @Navigation table
        (
           tempID      int,
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )
      DECLARE @ID_User INT

      SELECT @ID_User = ID_User
      FROm   tUserSession
      WHERE  ID = @ID_UserSession

      INSERT @UserModulesNavigation
      SELECT ID_Navigation
      FROM   [dbo].fGetUserModules(@ID_User)
      WHERE  ID_Navigation IS NOT NULL

      INSERT @SelectedNavigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route
      FROm   _vNavigation nav
             inner join @UserModulesNavigation userNav
                     on nav.Oid = userNav.ID_Navigation
      WHERE  nav.ID_View IS NULL
              OR nav.IsActive = 1

      INSERT @Navigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route,
              tempID)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route,
                      tempID
      FROm   _vNavigation nav
             INNER JOIN @SelectedNavigation selNav
                     on nav.Oid = selNav.Oid
      WHERE  nav.ID_Parent IS NULL

      INSERT @Navigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route,
              tempID)
      SELECT nav.Oid,
             nav.ID_Parent,
             nav.Name,
             nav.Caption,
             nav.ID_ViewType,
             nav.Icon,
             nav.SeqNo,
             nav.Route,
             MAX(selNav.tempID)
      FROm   _vNavigation nav
             INNER JOIN @SelectedNavigation selNav
                     on nav.Oid = selNav.ID_Parent
      GROUP  BY nav.Oid,
                nav.ID_Parent,
                nav.Name,
                nav.Caption,
                nav.ID_ViewType,
                nav.Icon,
                nav.SeqNo,
                nav.Route

      SELECT ''                         AS _,
             ''                         ParentItem,
             'ParentItem.ID_ParentItem' ChildrenItems

      SELECT 1               ID,
             @ID_UserSession ID_UserSession

      SELECT tempID ID,
             Oid    ID_Parent,
             Name,
             Caption,
             ID_ViewType,
             Icon,
             SeqNo,
             Route
      FROm   @Navigation
      ORder  by SeqNo

      SELECT DISTINCT selNav.tempID                                                 ID,
                      navParent.tempID                                              ID_ParentItem,
                      navParent.Oid                                                 ID_Parent,
                      navParent.Name                                                Name_Parent,
                      selNav.Oid,
                      selNav.Name,
                      ISNULL(selNav.Caption, REPLACE(selNav.Name, '_ListView', '')) Caption,
                      selNav.ID_ViewType,
                      selNav.Icon,
                      navParent.SeqNo,
                      isnull(selNav.SeqNo, 999999999)                               SeqNo,
                      selNav.Route
      FROm   @SelectedNavigation selNav
             INNER JOIN @Navigation navParent
                     on selNav.ID_Parent = navParent.Oid
      ORder  by navParent.SeqNo,
                isnull(selNav.SeqNo, 999999999),
                ISNULL(selNav.Caption, REPLACE(selNav.Name, '_ListView', ''))
  END

GO
