﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[pReRun_AfterSaved_Process_BillingInvoice]
AS
  BEGIN
      DECLARE @IDs_CurrentObject typIntList

      INSERT @IDs_CurrentObject
      SELECT TOP 5 bi.ID
      FROm   tBillingInvoice bi
             INNER JOIN vCompanyActive c
                     on bi.ID_Company = c.ID
                        and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
                        AND CONVERT(Date, bi.DateModified) = CONVERT(Date, GETDATE())
      Order  by bi.DateModified DESC

      exec pReRun_AfterSaved_Process_BillingInvoice_By_IDs
        @IDs_CurrentObject
  END

GO
