﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGetPatient_Grooming] @ID         INT = -1,
                                @ID_Client  INT = NULL,
                                @ID_Patient INT = NULL,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Patient_Grooming_Detail

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   fs.Name      Name_FilingStatus
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '-New-'     AS [Code],
                           GetDate()   AS Date,
                           NULL        AS [Name],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           ID_FilingStatus,
                           @ID_Client  ID_Client,
                           @ID_Patient ID_Patient) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN tClient client
                          ON H.ID_Client = client.ID
                   LEFT JOIN tPatient patient
                          ON H.ID_Patient = patient.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Grooming H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Grooming_Detail
      where  ID_Patient_Grooming = @ID
  END

GO
