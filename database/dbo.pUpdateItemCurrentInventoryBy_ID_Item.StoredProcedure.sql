﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateItemCurrentInventoryBy_ID_Item](@ID_Item int)
AS
  BEGIN
      DECLARE @IDs_Item typIntList

      INSERT @IDs_Item
      VALUES(@ID_Item)

      exec pUpdateItemCurrentInventoryByIDsItems
        @IDs_Item
  END

GO
