﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveRecordValueTransferLogs] AS   
				SELECT   
				 H.*  
				FROM vRecordValueTransferLogs H  
				WHERE IsActive = 1
GO
