﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vReceivingReport]
AS
SELECT H.*,
       UC.Name AS CreatedBy_Name_User,
       UM.Name AS LastModifiedBy_Name_User,
       approvedUser.Name AS ApprovedBy_Name_User,
       cancelUser.Name AS CanceledBy_Name_User,
       company.Name Company,
       fs.Name Name_FilingStatus,
       servingStatus.Name ServingStatus_Name_FilingStatus,
       supplier.Name Name_Supplier,
       poHed.Code Code_PurchaseOrder,
       taxscheme.Name Name_TaxScheme,
       CONVERT(VARCHAR, H.Date, 0) DateString,
       CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,
       CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
       CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
       CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString
FROM dbo.tReceivingReport H
    LEFT JOIN dbo.tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tUser approvedUser
        ON H.ID_ApprovedBy = approvedUser.ID
    LEFT JOIN dbo.tUser cancelUser
        ON H.ID_CanceledBy = cancelUser.ID
    LEFT JOIN dbo.tCompany company
        ON H.ID_Company = company.ID
    LEFT JOIN dbo.tFilingStatus fs
        ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tSupplier supplier
        ON supplier.ID = H.ID_Supplier
    LEFT JOIN dbo.tTaxScheme taxscheme
        ON taxscheme.ID = H.ID_TaxScheme
    LEFT JOIN dbo.tPurchaseOrder poHed
        ON poHed.ID = H.ID_PurchaseOrder
    LEFT JOIN tFilingStatus servingStatus
        ON H.ServingStatus_ID_FilingStatus = servingStatus.ID;

GO
