﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveInventoryTrail_DeletedRecord] AS   
				SELECT   
				 H.*  
				FROM vInventoryTrail_DeletedRecord H  
				WHERE ISNULL(IsActive, 0) = 0
GO
