﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoice_Patient] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoice_Patient H  
				WHERE IsActive = 1
GO
