﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompanyCustomReportObjectValue] AS   
    SELECT   
     H.*  
    FROM vCompanyCustomReportObjectValue H  
    WHERE IsActive = 1
GO
