﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveHomeOwnershipStatus] AS   
				SELECT   
				 H.*  
				FROM vHomeOwnershipStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
