﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetReferenceLinkMenuItem](@Name VARCHAR(MAX))
AS
  BEGIN
      SELECT '_' _,
             ''  MenuItems

      SELECT GETDATE() Date

      SELECT *
      FROM   tReferenceLink
      WHERE  Name = @Name
  END

GO
