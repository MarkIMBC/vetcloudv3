﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientWaitingList_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vPatientWaitingList_SMSStatus H  
    WHERE ISNULL(IsActive, 0) = 1
GO
