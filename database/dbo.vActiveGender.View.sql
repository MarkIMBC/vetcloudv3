﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveGender] AS   
				SELECT   
				 H.*  
				FROM vGender H  
				WHERE IsActive = 1
GO
