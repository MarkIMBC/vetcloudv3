﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[theFurrClientMasterList](
	[CODE] [varchar](500) NULL,
	[CLIENT'S NAME] [varchar](500) NULL,
	[CONTACT #] [varchar](500) NULL,
	[PET NAME] [varchar](500) NULL,
	[ATTENDING VET] [varchar](500) NULL
) ON [PRIMARY]
GO
