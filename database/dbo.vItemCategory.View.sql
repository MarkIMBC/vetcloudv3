﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE VIEW [dbo].[vItemCategory] AS   
    SELECT   
     H.* ,  
     UC.Name AS CreatedBy_Name_User,  
     UM.Name AS LastModifiedBy_Name_User  
    FROM tItemCategory H  
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID   
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID  
GO
