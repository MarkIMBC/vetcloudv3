﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetEmployee] @ID         INT = -1,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Employee INT;
      DECLARE @UserAccount_ID_User INT;
      DECLARE @UserRole_IsAdministrator bit = 0;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserAccount_ID_User = ID
      FROM   tUser
      WHERE  ID_Employee = @ID
             AND IsActive = 1

      SELECT @UserRole_IsAdministrator = ISNULL(IsAdministrator, 0)
      FROM   vUserAdministrator
      where  ID_User = @ID_User

      SELECT @ID_Employee = ID_Employee,
             @ID_Company = emp.ID_Company
      FROM   dbo.tUser _user
             INNER JOIN dbo.tEmployee emp
                     ON emp.ID = _user.ID_Employee
      WHERE  _user.ID = @ID_User;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   (SELECT NULL                 AS [_],
                           -1                   AS [ID],
                           ''                   AS [Code],
                           NULL                 AS [Name],
                           1                    AS [IsActive],
                           NULL                 AS [Comment],
                           NULL                 AS [DateCreated],
                           NULL                 AS [DateModified],
                           NULL                 AS [ID_CreatedBy],
                           NULL                 AS [ID_LastModifiedBy],
                           @ID_Company          [ID_Company],
                           ''                   [Company],
                           ''                   FirstName,
                           ''                   LastName,
                           ''                   MiddleName,
                           @UserAccount_ID_User UserAccount_ID_User,
                           0                    IsSystemUsed) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   @UserAccount_ID_User      UserAccount_ID_User,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   vEmployee H
            WHERE  H.ID = @ID
        END
  END

GO
