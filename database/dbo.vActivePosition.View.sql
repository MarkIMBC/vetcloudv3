﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePosition] AS   
				SELECT   
				 H.*  
				FROM vPosition H  
				WHERE IsActive = 1
GO
