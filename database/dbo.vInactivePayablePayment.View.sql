﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePayablePayment] AS   
				SELECT   
				 H.*  
				FROM vPayablePayment H  
				WHERE ISNULL(IsActive, 0) = 0
GO
