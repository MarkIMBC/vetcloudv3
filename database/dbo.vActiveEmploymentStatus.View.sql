﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEmploymentStatus] AS   
				SELECT   
				 H.*  
				FROM vEmploymentStatus H  
				WHERE IsActive = 1
GO
