﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveModelDefaultValue] AS   
    SELECT   
     H.*  
    FROM vModelDefaultValue H  
    WHERE IsActive = 1
GO
