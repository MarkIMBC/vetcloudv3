﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveItem_UnitPriceLog] AS   
				SELECT   
				 H.*  
				FROM vItem_UnitPriceLog H  
				WHERE IsActive = 1
GO
