﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPatientDentalExamiantionMedHistory]
(@ID_Patient_DentalExamination INT)
AS
BEGIN

    DECLARE @pMedHistory TABLE
    (
        ID_Patient_DentalExamination INT,
        ID_Patient_DentalExamination_MedicalHistory INT,
        ID_MedicalHistoryQuestionnaire INT,
        Comment VARCHAR(MAX),
        Answer VARCHAR(MAX)
    );

    DECLARE @parentQuestion TABLE
    (
        Parent_ID_MedicalHistoryQuestionnaire INT
    );

    INSERT @parentQuestion
    (
        Parent_ID_MedicalHistoryQuestionnaire
    )
    SELECT DISTINCT
           ID_Parent
    FROM dbo.tMedicalHistoryQuestionnaire
    WHERE IsActive = 1
          AND ISNULL(ID_Parent, 0) <> 0;

    INSERT @pMedHistory
    (
        ID_Patient_DentalExamination,
        ID_Patient_DentalExamination_MedicalHistory,
        ID_MedicalHistoryQuestionnaire,
        Comment,
        Answer
    )
    SELECT pMedHistory.ID_Patient_DentalExamination,
           pMedHistory.ID,
           pMedHistory.ID_MedicalHistoryQuestionnaire,
           pMedHistory.Comment,
           pMedHistory.Answer
    FROM dbo.tPatient_DentalExamination_MedicalHistory pMedHistory
    WHERE pMedHistory.ID_Patient_DentalExamination = @ID_Patient_DentalExamination;


    SELECT '_',
           '' Questions;

    SELECT CONVERT(BIT, 0) Success;

    SELECT ISNULL(pMedHistory.ID_Patient_DentalExamination_MedicalHistory, 0) ID,
           @ID_Patient_DentalExamination ID_Patient_DentalExamination,
           question.ID ID_MedicalHistoryQuestionnaire,
		   question.ID_QuestionType,
           question.ID_Parent Parent_ID_MedicalHistoryQuestionnaire,
           ISNULL(parent.Comment, '') Parent_Comment_MedicalHistoryQuestionnaire,
           ISNULL(question.IsParent, '') IsParent,
           question.Comment,
           pMedHistory.Answer
    FROM dbo.tMedicalHistoryQuestionnaire question
        FULL JOIN @pMedHistory pMedHistory
            ON pMedHistory.ID_MedicalHistoryQuestionnaire = question.ID
        LEFT JOIN dbo.tMedicalHistoryQuestionnaire parent
            ON parent.ID = question.ID_Parent
    WHERE ISNULL(question.IsActive, 0) = 1
    ORDER BY question.ID;

END;
GO
