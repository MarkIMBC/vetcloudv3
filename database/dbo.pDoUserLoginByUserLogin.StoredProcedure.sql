﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pDoUserLoginByUserLogin](@ID_User INT)
AS
  BEGIN
      DECLARE @Username VARCHAR(MAX) = ''
      DECLARE @Password VARCHAR(MAX) = ''

      SELECT @Username = Username,
             @Password = Password
      FROM   tUser
      WHERE  ID = @ID_User

      EXEC dbo.pDoUserLogin
        @Username,
        @Password
  END

GO
