﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 VIEW [dbo].[vCompany]
AS
  SELECT H.*,
         UC.Name                                                                AS CreatedBy,
         UM.Name                                                                AS LastModifiedBy,
         dbo.fGetAPILink() + '/api/Image/GetImage/'
         + H.ImageHeaderFilename                                                ImageHeaderLocationFilenamePath,
         dbo.fGetAPILink()
         + '/api/Image/GetThumbnail/'
         + H.ImageHeaderFilename                                                ImageHeaderThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/api/Image/GetImage/'
         + H.ImageLogoFilename                                                  ImageLogoLocationFilenamePath,
         dbo.fGetAPILink()
         + '/api/Image/GetThumbnail/'
         + H.ImageLogoFilename                                                  ImageLogoThumbNameLocationFilenamePath,
         dbo.fGetRegisterClientUrl() + '/' + H.Code + '/'
         + H.ReceptionPortalGuid
         + '/NewClientRegistrationForm'                                         RegisterClientURL,
         lower(REPLACE(REPLACE(REPLACE(H.Name, ' ', ''), '-', ''), '&', 'and')) SubDomainName
  FROM   tCompany H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID;

GO
