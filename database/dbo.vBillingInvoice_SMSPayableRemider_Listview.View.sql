﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vBillingInvoice_SMSPayableRemider_Listview]
AS
  SELECT ID,
         DateSent,
         ID_BillingInvoice
  FROM   vBillingInvoice_SMSPayableRemider
  where  DateSent IS NOT NULL
         and ISNULL(IsSentSMS, 0) = 1

GO
