﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientAppointment_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vPatientAppointment_SMSStatus H  
    WHERE ISNULL(IsActive, 0) = 1
GO
