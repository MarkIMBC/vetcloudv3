﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE       
 PROC [dbo].[pGetDashboardTopBilledClientDataSourceByDateCoverage] (@ID_UserSession INT,
                                                                     @DateStart      DateTime,
                                                                     @DateEnd        DateTime)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT
      DECLARE @Label VARCHAR(MAX) = ''

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @dataSource TABLE
        (
           Name        VARCHAR(MAX),
           TotalAmount DECIMAL(18, 2)
        )

      INSERT @dataSource
      SELECT TOP 10 client.Name,
                    SUM(ISNULL(biHed.TotalAmount, 0)) TotalAmount
      FROM   tBillingInvoice biHed
             INNER JOIN tClient client
                     on biHed.ID_Client = client.ID
      WHERE  CONVERT(Date, biHed.Date) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)
             AND biHed.ID_Company = @ID_Company
             AND biHed.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY client.Name
      ORDER  BY SUM(ISNULL(biHed.TotalAmount, 0)) DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0

            SET @Label = 'No Record'
        END
      ELSE
        BEGIN
            DECLARE @Count INT = 0

            SELECT @Count = COUNT(*)
            FROM   @dataSource

            SET @Label = 'Top ' + FORMAT(@Count, '#,#') + ' ' + 'from '
                         + FORMAT(@DateStart, 'MM/dd/yyyy') + ' to '
                         + FORMAT(@DateEnd, 'MM/dd/yyy')
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT YEAR(@DateStart) DateYear,
             @Label           Label

      SELECT *
      FROM   @dataSource
      order  by TotalAmount DESC
  END

GO
