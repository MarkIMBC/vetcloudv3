﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveInventoryTrail_DeletedRecord] AS   
				SELECT   
				 H.*  
				FROM vInventoryTrail_DeletedRecord H  
				WHERE IsActive = 1
GO
