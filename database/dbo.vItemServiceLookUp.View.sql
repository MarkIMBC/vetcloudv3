﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vItemServiceLookUp]
AS
  SELECT ID,
         Code,
         Name,
         ISNULL(UnitPrice, 0) UnitPrice,
         ISNULL(UnitCost, 0)  UnitCost,
         ID_Company
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 1

GO
