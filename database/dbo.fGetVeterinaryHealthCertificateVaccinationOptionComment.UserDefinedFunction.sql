﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetVeterinaryHealthCertificateVaccinationOptionComment](@VaccinationOptionComment VARCHAR(MAX),
                                                                               @DateVaccinated           DateTime,
                                                                               @Name_Item                VARCHAR(MAX),
                                                                               @LotNumber                VARCHAR(MAX),
                                                                               @CaseType                 VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @DateVacinatedString VARCHAR(MAX) = ''

      IF @DateVaccinated IS NOT NULL
        SET @DateVacinatedString = FORMAT(@DateVaccinated, 'MM/dd/yyyy')

      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Date*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                             + @DateVacinatedString
                                                                                             + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Vaccination*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                                    + IsNULL(@Name_Item, '_______________')
                                                                                                    + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Serial/Lot Number*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                                          + IsNULL(@LotNumber, '_______________')
                                                                                                          + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****CaseType*****/', IsNULL(@CaseType, '&nbsp;&nbsp;&nbsp;rabies&nbsp;&nbsp;&nbsp;'))

      RETURN @VaccinationOptionComment
  END

GO
