﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGetDashboardMonthlyClientsDataSource] (@ID_UserSession INT,
                                                          @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SET @DateYear = FORMAT(GETDATE(), 'yyyy')

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                         DateYear,
             'Monthly New Client ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tClient
      WHERE  IsActive = 1
             AND DateCreated BETWEEN @DateYear + '/01/01' AND @DateYear + '/12/31'
             AND ID_Company = @ID_Company
      GROUP  BY ID_Company
  END

GO
