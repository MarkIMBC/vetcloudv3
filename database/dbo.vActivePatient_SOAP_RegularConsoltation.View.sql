﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_SOAP_RegularConsoltation] AS   
				SELECT   
				 H.*  
				FROM vPatient_SOAP_RegularConsoltation H  
				WHERE IsActive = 1
GO
