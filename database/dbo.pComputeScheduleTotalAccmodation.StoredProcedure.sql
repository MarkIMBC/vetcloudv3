﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pComputeScheduleTotalAccmodation]
(@IDs_Schedule typIntList READONLY)
AS
DECLARE @ID_Schedule INT = 0;
DECLARE @ID_AppointmentSchedule INT = 0;
DECLARE @TotalAccomodation INT = 0;

DECLARE @ScheduleAccomodation TABLE
(
    ID_Schedule INT,
    TotalAccomodation INT
);

INSERT @ScheduleAccomodation
(
    ID_Schedule,
    TotalAccomodation
)
SELECT appSched.ID_Schedule,
       COUNT(*) TotalAccomodation
FROM tAppointmentSchedule appSched
    INNER JOIN @IDs_Schedule ids
        ON ids.ID = appSched.ID_Schedule
GROUP BY ID_Schedule;

UPDATE dbo.tSchedule
SET AccommodateCount = scheAcc.TotalAccomodation
FROM dbo.tSchedule sched
    INNER JOIN @ScheduleAccomodation scheAcc
        ON scheAcc.ID_Schedule = sched.ID;


GO
