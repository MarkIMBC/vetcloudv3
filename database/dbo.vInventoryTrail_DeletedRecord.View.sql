﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIew [dbo].[vInventoryTrail_DeletedRecord]  
as  
  
SELECT   
  hed.*,  
  item.Name Name_Item,  
  status.Name Name_FilingStatus,  
   CONVERT(varchar,hed.Date,9) DateString  
FROM tInventoryTrail_DeletedRecord hed  
LEFT JOIN tItem item   
on item.ID = hed.ID_Item  
LEFT JOIN tFilingStatus status  
on status.ID = hed.ID_FilingStatus  
  
GO
