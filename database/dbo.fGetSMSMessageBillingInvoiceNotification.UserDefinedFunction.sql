﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetSMSMessageBillingInvoiceNotification] (@SMSFormat                    VARCHAR(MAX),
                                                             @Name_Company                 Varchar(MAX),
                                                             @ContactNumber_Company        Varchar(MAX),
                                                             @Code_BillingInvoice          Varchar(MAX),
                                                             @Date_BillingInvoice          DateTime,
                                                             @Client_Company               Varchar(MAX),
                                                             @PayableAmount_BillingInvoice Decimal(18, 2))
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @message VARCHAR(MAX) = ''
        + 'Hello, /*Client*/, This is /*Company*/. '
        + CHAR(13)
        + 'We�d like to remind you that a pending payable for /*BICode*/ amounting Php /*PayableAmount*/ billed from /*BIDate*/. '
        + CHAR(13) + CHAR(13)
        + 'Kindly disregard this message if you already settled your Payment.'
        + CHAR(13) + CHAR(13)
        + 'This is an automated SMS reminder PLEASE DO NOT REPLY or CALL directly to this number.'
        + CHAR(13) + CHAR(13)
        + 'For more information, please contact /*Company*/ /*ContactNumber_Company*/. Thank you.'
      Declare @DateReturnString Varchar(MAX) = FORMAT(@Date_BillingInvoice, 'M/dd/yyyy ddd')
      Declare @PayableAmountString Varchar(MAX) = FORMAT(@PayableAmount_BillingInvoice, '#,#0.00')

      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(ISNULL(@Client_Company, 0))))
      SET @message = REPLACE(@message, '/*Company*/', LTRIM(RTRIM(ISNULL(@Name_Company, 0))))
      SET @message = REPLACE(@message, '/*BICode*/', LTRIM(RTRIM(ISNULL(@Code_BillingInvoice, 0))))
      SET @message = REPLACE(@message, '/*BIDate*/', LTRIM(RTRIM(ISNULL(@DateReturnString, 0))))
      SET @message = REPLACE(@message, '/*PayableAmount*/', LTRIM(RTRIM(ISNULL(@PayableAmountString, 0))))
      SET @message = REPLACE(@message, '/*ContactNumber_Company*/', LTRIM(RTRIM(ISNULL(@ContactNumber_Company, 0))))
      SET @message = REPLACE(@message, '"', '``')

      RETURN @message
  END

GO
