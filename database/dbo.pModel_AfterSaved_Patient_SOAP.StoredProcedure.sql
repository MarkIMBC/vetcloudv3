﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pModel_AfterSaved_Patient_SOAP] (@ID_CurrentObject VARCHAR(10),
                                                   @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @IDs_Patient_SOAP TYPINTLIST

      INSERT @IDs_Patient_SOAP
      VALUES(@ID_CurrentObject)

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_SOAP
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_SOAP';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_SOAP
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      UPDATE dbo.tPatient_SOAP
      SET    RunAfterSavedProcess_ID_FilingStatus = NULL,
             DateModified = GETDATE()
      WHERE  ID = @ID_CurrentObject;

      DECLARE @IDs_Patient TYPINTLIST

      INSERT @IDs_Patient
      SELECT patient.ID
      FROM   tPatient_SOAP soap
             INNER JOIN tPatient patient
                     ON patient.ID = soap.ID_Patient
      WHERE  soap.ID = @ID_CurrentObject
  --EXEC dbo.pUpdatPatientConfinementItemsServicesByPatientSOAP  
  --  @IDs_Patient_SOAP  
  --EXEC dbo.PupDatePatientsLastVisitedDate  
  -- @IDs_Patient  
  -- EXEC _pCancelPatient_Confinement_temp  
  END;

GO
