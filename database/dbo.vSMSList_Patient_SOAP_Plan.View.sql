﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vSMSList_Patient_SOAP_Plan]
AS
  SELECT soapPlan.ID,
         soapPlan.ID_Patient_SOAP,
         soap.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), Name_Patient, ISNULL(soapPlan.Name_Item, ''), ISNULL(soapPlan.Comment, ''), soapPlan.DateReturn) Message,
         DATEADD(DAY, -1, DateReturn)                                                                                                                                                                        DateSending,
         soap.ID_Company,
         ISNULL(soapPlan.IsSentSMS, 0)                                                                                                                                                                       IsSentSMS,
         model.Oid                                                                                                                                                                                           Oid_Model
  FROM   vPatient_SOAP_Plan soapPlan
         INNER JOIN vPatient_SOAP soap
                 on soapPlan.ID_Patient_SOAP = soap.ID
         INNER JOIN tClient c
                 on c.ID = soap.ID_Client
         INNER JOIN tCOmpany comp
                 on comp.ID = soap.ID_Company,
         _tModel model
  where  model.tableName = 'tPatient_SOAP_Plan'
         and soap.ID_FilingStatus NOT IN ( 4 )

GO
