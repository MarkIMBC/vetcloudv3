﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveApproverMatrix_Detail] AS   
				SELECT   
				 H.*  
				FROM vApproverMatrix_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
