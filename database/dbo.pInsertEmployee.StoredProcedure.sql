﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 PROC [dbo].[pInsertEmployee](@LastName_Employee  VARCHAR(MAX),
                           @FirstName_Employee VARCHAR(MAX),
                           @NamePosition       VARCHAR(MAX),
                           @ID_Company         INT)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tEmployee
         WHERE  LastName = @LastName_Employee
                AND FirstName = @FirstName_Employee
                AND ID_Company = @ID_Company) = 0
        BEGIN
            DECLARE @ID_Position INT = 0

            exec pInsertPosition
              @NamePosition,
              1

            SELECT @ID_Position = ID
            FROM   tPosition
            where  name = @NamePosition

            INSERT INTO [dbo].[tEmployee]
                        ([IsActive],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         ID_Company,
                         [DateCreated],
                         [DateModified],
                         [ID_Position],
                         [LastName],
                         [FirstName],
                         [tempID])
            SELECT 1,
                   1,
                   1,
                   @ID_Company,
                   GETDATE(),
                   GETDATE(),
                   @ID_Position,
                   @LastName_Employee,
                   @FirstName_Employee,
                   NEWID()
        END
  END

GO
