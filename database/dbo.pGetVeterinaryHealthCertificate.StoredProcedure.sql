﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROCEDURE [dbo].[pGetVeterinaryHealthCertificate] (@ID         INT = -1,
                                                 @ID_Client  INT = NULL,
                                                 @ID_Patient INT = NULL,
                                                 @ID_Session INT = NULL)
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   patient.Color
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '-- NEW --' AS [Code],
                           NULL        AS [Name],
                           GETDATE()   AS [Date],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           AS ID_FilingStatus,
                           'N/A'       AS [DestinationAddress],
                           @ID_Client  ID_Client,
                           @ID_Patient ID_Patient,
                           1           ID_VaccinationOption) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vVeterinaryHealthCertificate H
            WHERE  H.ID = @ID
        END
  END

GO
