﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Confinement_ItemsServices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient_Confinement] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[Date] [datetime] NULL,
	[DateExpiration] [datetime] NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[Amount] [decimal](18, 4) NULL,
	[ID_Patient_SOAP] [int] NULL,
	[ID_Patient_SOAP_Treatment] [int] NULL,
	[ID_Patient_SOAP_Prescription] [int] NULL,
	[Route] [varchar](300) NULL,
	[ID_Patient] [int] NULL,
 CONSTRAINT [PK_tPatient_Confinement_ItemsServices] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Confinement_ItemsServices] ON [dbo].[tPatient_Confinement_ItemsServices]
(
	[ID_Item] ASC,
	[ID_Patient_Confinement] ASC,
	[ID_Patient_SOAP] ASC,
	[ID_Patient_SOAP_Treatment] ASC,
	[ID_Patient_SOAP_Prescription] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] CHECK CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] CHECK CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] CHECK CONSTRAINT [FK_tPatient_Confinement_ItemsServices_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]  WITH CHECK ADD  CONSTRAINT [FKtPatient_Confinement_ItemsServices_ID_Patient_Confinement] FOREIGN KEY([ID_Patient_Confinement])
REFERENCES [dbo].[tPatient_Confinement] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] CHECK CONSTRAINT [FKtPatient_Confinement_ItemsServices_ID_Patient_Confinement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Confinement_ItemsServices]
		ON [dbo].[tPatient_Confinement_ItemsServices]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Confinement_ItemsServices
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Confinement_ItemsServices hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] ENABLE TRIGGER [rDateCreated_tPatient_Confinement_ItemsServices]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Confinement_ItemsServices]
		ON [dbo].[tPatient_Confinement_ItemsServices]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Confinement_ItemsServices
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Confinement_ItemsServices hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices] ENABLE TRIGGER [rDateModified_tPatient_Confinement_ItemsServices]
GO
