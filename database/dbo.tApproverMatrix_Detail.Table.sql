﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tApproverMatrix_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ApproverMatrix] [int] NULL,
 CONSTRAINT [PK_tApproverMatrix_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] ADD  CONSTRAINT [DF__tApprover__IsAct__351DDF8C]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tApproverMatrix_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] CHECK CONSTRAINT [FK_tApproverMatrix_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tApproverMatrix_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] CHECK CONSTRAINT [FK_tApproverMatrix_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail]  WITH NOCHECK ADD  CONSTRAINT [FKtApproverMatrix_Detail_ID_ApproverMatrix] FOREIGN KEY([ID_ApproverMatrix])
REFERENCES [dbo].[tApproverMatrix] ([ID])
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] CHECK CONSTRAINT [FKtApproverMatrix_Detail_ID_ApproverMatrix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tApproverMatrix_Detail]
		ON [dbo].[tApproverMatrix_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tApproverMatrix_Detail
			SET    DateCreated = GETDATE()
			FROM   dbo.tApproverMatrix_Detail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] ENABLE TRIGGER [rDateCreated_tApproverMatrix_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tApproverMatrix_Detail]
		ON [dbo].[tApproverMatrix_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tApproverMatrix_Detail
			SET    DateModified = GETDATE()
			FROM   dbo.tApproverMatrix_Detail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tApproverMatrix_Detail] ENABLE TRIGGER [rDateModified_tApproverMatrix_Detail]
GO
