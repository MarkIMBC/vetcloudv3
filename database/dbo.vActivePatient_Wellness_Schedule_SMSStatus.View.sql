﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Wellness_Schedule_SMSStatus] AS   
				SELECT   
				 H.*  
				FROM vPatient_Wellness_Schedule_SMSStatus H  
				WHERE IsActive = 1
GO
