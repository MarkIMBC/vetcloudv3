﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient]
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName,
         gender.Name                                          Name_Gender,
         country.Name                                         PhoneCode_Country,
         client.Name                                          Name_Client,
         client.IsActive                                      IsActive_Client,
         dbo.fGetAPILink() + '/Content/Image/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageLocationFile,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageThumbnailLocationFile,
         waitingStatus.Name                                   WaitingStatus_Name_FilingStatus,
         attendingPersonell.Name                              LastAttendingPhysician_Name_Employee
  FROM   tPatient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN dbo.tCountry country
                ON H.ID_Country = country.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tFilingStatus waitingStatus
                ON waitingStatus.ID = H.WaitingStatus_ID_FilingStatus
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO
