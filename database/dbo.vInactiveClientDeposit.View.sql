﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveClientDeposit] AS   
				SELECT   
				 H.*  
				FROM vClientDeposit H  
				WHERE ISNULL(IsActive, 0) = 0
GO
