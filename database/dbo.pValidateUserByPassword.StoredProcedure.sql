﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pValidateUserByPassword](@Oid_Model        VARCHAR(MAX),
                                   @ID_CurrentObject INT,
                                   @Password         Varchar(MAX),
                                   @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @ID_Company INT
      DECLARE @Count INT
      DECLARE @IsValidated BIT = 0
      DECLARE @message VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @ID_Company = _user.ID_Company
          FROM   tUserSession _session
                 INNER JOIN vUser _user
                         on _session.ID_User = _user.ID
          WHERE  _session.ID = @ID_UserSession

          SELECT @Count = Count(*)
          FROM   vUserAdministrator _useradmin
                 inner join vUser _user
                         on _useradmin.ID_User = _user.ID
          WHERE  _user.ID_Company = @ID_Company
                 AND Password = @Password

          IF( @Count > 0 )
            SET @IsValidated = 1;

          IF( @IsValidated = 0 )
            BEGIN;
                THROW 51000, 'Invalid Password', 1;
            END;
      END TRY
      BEGIN CATCH
          SET @IsValidated = 0
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_';

      SELECT @IsValidated Success,
             @message     Message
  END

GO
