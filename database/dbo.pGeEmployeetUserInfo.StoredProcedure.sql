﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGeEmployeetUserInfo](@ID_Employee    INT,
                                @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      INSERT @table
      SELECT c.Name,
             c.Code,
             _user.Username
      FROM   vUser _user
             inner join tCompany c
                     on _user.ID_Company = c.ID
      WHERE  ID_Company = @ID_Company
             AND _user.ID_Employee = @ID_Employee

      SELECT '_'

      SELECT TOP 1 *
      FROm   @table
  END

GO
