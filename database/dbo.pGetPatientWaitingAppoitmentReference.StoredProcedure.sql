﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetPatientWaitingAppoitmentReference](@IDs_Patient typIntList READONLY,
                                                 @DateStart   DATETIme)
as
  BEGIN
      DECLARE @patientAppointment TABLE
        (
           Name_Model    VARCHAR(MAX),
           ReferenceCode VARCHAR(MAX),
           ID_Client     INT,
           ID_Patient    INT,
           Particular    VARCHAR(MAX),
           UniqueID      VARCHAR(MAX),
           Description   VARCHAR(MAX)
        )

      INSERT @patientAppointment
      SELECT DISTINCT _model.Name,
                      ReferenceCode,
                      ID_Client,
                      ID_Patient,
                      Paticular,
                      UniqueID,
                      Description
      FROM   vAppointmentEvent appntEvent
             inner join _tModel _model
                     on appntEvent.Oid_Model = _model.Oid
             INNER JOIN @IDs_Patient idsPatient
                     on idsPatient.ID = appntEvent.ID_Patient
      WHERE  CONVERT(Date, DateStart) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateStart)

      if(SELECT COUNT(*)
         FROm   @patientAppointment) = 0
        BEGIN
            INSERT @patientAppointment
                   (ID_Patient)
            VALUES(0)
        END

      SELECT '_',
             '' PatientWaitingAppoitmentReference

      SELECT GETDATE() DateStart

      select ROW_NUMBER()
               OVER(
                 PARTITION BY ID_Patient
                 ORDER BY ID_Patient ASC)                                           AS ID,
             t.ID_Patient,
             STUFF((SELECT DISTINCT ', ' + ReferenceCode
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS ReferenceCodeList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniqueIDList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniqueIDListTest
      FROM   @patientAppointment t
      GROUP  BY t.ID_Patient
  END

GO
