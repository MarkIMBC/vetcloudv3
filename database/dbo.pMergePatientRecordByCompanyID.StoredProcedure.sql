﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pMergePatientRecordByCompanyID](@ID_Company               INT,
                                          @Source_Code_Patient      VARCHAR(MAX),
                                          @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------    
      DECLARE @Source1_ID_Patient INT
      DECLARE @Destination_ID_Patient INT
      DECLARE @Source1_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Patient = ID,
             @Source1_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Source_Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Patient = ID,
             @Destination_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Destination_Code_Patient
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Patient - '
                     + ' from ' + @Source1_Name_Patient + ' ('
                     + @Source_Code_Patient + ') to '
                     + @Destination_Name_Patient + ' ('
                     + @Destination_Code_Patient + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Source1_ID_Patient
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Destination_ID_Patient
      --       AND ID_Company = @ID_Company
      exec pMergePatientRecord
        @Source1_ID_Patient,
        @Destination_ID_Patient,
        @Comment

      Update tPatient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Patient
             and ID_Company = @ID_Company
  END

GO
