﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUser_Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_User] [int] NULL,
	[ID_UserRole] [int] NULL,
	[SeqNo] [int] NULL,
 CONSTRAINT [PK_tUser_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tUser_Roles] ON [dbo].[tUser_Roles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUser_Roles] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUser_Roles]  WITH CHECK ADD  CONSTRAINT [FKtUser_Roles_ID_User] FOREIGN KEY([ID_User])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUser_Roles] CHECK CONSTRAINT [FKtUser_Roles_ID_User]
GO
ALTER TABLE [dbo].[tUser_Roles]  WITH CHECK ADD  CONSTRAINT [FKtUser_Roles_ID_UserRole] FOREIGN KEY([ID_UserRole])
REFERENCES [dbo].[tUserRole] ([ID])
GO
ALTER TABLE [dbo].[tUser_Roles] CHECK CONSTRAINT [FKtUser_Roles_ID_UserRole]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUser_Roles]
		ON [dbo].[tUser_Roles]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tUser_Roles] ENABLE TRIGGER [rDateCreated_tUser_Roles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUser_Roles]
		ON [dbo].[tUser_Roles]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tUser_Roles] ENABLE TRIGGER [rDateModified_tUser_Roles]
GO
