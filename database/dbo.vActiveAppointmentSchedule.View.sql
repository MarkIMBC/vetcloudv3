﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppointmentSchedule] AS   
				SELECT   
				 H.*  
				FROM vAppointmentSchedule H  
				WHERE IsActive = 1
GO
