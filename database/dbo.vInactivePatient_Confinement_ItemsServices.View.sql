﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Confinement_ItemsServices] AS   
				SELECT   
				 H.*  
				FROM vPatient_Confinement_ItemsServices H  
				WHERE ISNULL(IsActive, 0) = 0
GO
