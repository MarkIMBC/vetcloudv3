﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingType] AS   
    SELECT   
     H.*  
    FROM vBillingType H  
    WHERE IsActive = 1
GO
