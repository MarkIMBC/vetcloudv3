﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_SOAP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Subjective] [varchar](8000) NULL,
	[Objective] [varchar](8000) NULL,
	[Assessment] [varchar](8000) NULL,
	[Prescription] [varchar](8000) NULL,
	[Planning] [varchar](8000) NULL,
	[ID_Patient] [int] NULL,
	[Date] [datetime] NULL,
	[ID_SOAPType] [int] NULL,
	[ID_ApprovedBy] [int] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_FilingStatus] [int] NULL,
	[Old_soap_id] [int] NULL,
	[LabImageFilePath01] [varchar](3000) NULL,
	[LabImageFilePath02] [varchar](3000) NULL,
	[LabImageFilePath03] [varchar](3000) NULL,
	[LabImageFilePath04] [varchar](3000) NULL,
	[LabImageFilePath05] [varchar](3000) NULL,
	[LabImageFilePath06] [varchar](3000) NULL,
	[LabImageFilePath07] [varchar](3000) NULL,
	[LabImageFilePath08] [varchar](3000) NULL,
	[LabImageFilePath09] [varchar](3000) NULL,
	[LabImageFilePath10] [varchar](3000) NULL,
	[LabImageFilePath11] [varchar](3000) NULL,
	[LabImageFilePath12] [varchar](3000) NULL,
	[LabImageFilePath13] [varchar](3000) NULL,
	[LabImageFilePath14] [varchar](3000) NULL,
	[LabImageFilePath15] [varchar](3000) NULL,
	[LabImageRemark01] [varchar](3000) NULL,
	[LabImageRemark02] [varchar](3000) NULL,
	[LabImageRemark03] [varchar](3000) NULL,
	[LabImageRemark04] [varchar](3000) NULL,
	[LabImageRemark05] [varchar](3000) NULL,
	[LabImageRemark06] [varchar](3000) NULL,
	[LabImageRemark07] [varchar](3000) NULL,
	[LabImageRemark08] [varchar](3000) NULL,
	[LabImageRemark09] [varchar](3000) NULL,
	[LabImageRemark10] [varchar](3000) NULL,
	[LabImageRemark11] [varchar](3000) NULL,
	[LabImageRemark12] [varchar](3000) NULL,
	[LabImageRemark13] [varchar](3000) NULL,
	[LabImageRemark14] [varchar](3000) NULL,
	[LabImageRemark15] [varchar](3000) NULL,
	[LabImageRowIndex01] [varchar](300) NULL,
	[LabImageRowIndex02] [varchar](300) NULL,
	[LabImageRowIndex03] [varchar](300) NULL,
	[LabImageRowIndex04] [varchar](300) NULL,
	[LabImageRowIndex05] [varchar](300) NULL,
	[LabImageRowIndex06] [varchar](300) NULL,
	[LabImageRowIndex07] [varchar](300) NULL,
	[LabImageRowIndex08] [varchar](300) NULL,
	[LabImageRowIndex09] [varchar](300) NULL,
	[LabImageRowIndex10] [varchar](300) NULL,
	[LabImageRowIndex11] [varchar](300) NULL,
	[LabImageRowIndex12] [varchar](300) NULL,
	[LabImageRowIndex13] [varchar](300) NULL,
	[LabImageRowIndex14] [varchar](300) NULL,
	[LabImageRowIndex15] [varchar](300) NULL,
	[History] [varchar](8000) NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[Diagnosis] [varchar](3000) NULL,
	[Treatment] [varchar](3000) NULL,
	[ClientCommunication] [varchar](3000) NULL,
	[ClinicalExamination] [varchar](3000) NULL,
	[Interpretation] [varchar](3000) NULL,
	[DateDone] [datetime] NULL,
	[ID_DoneBy] [int] NULL,
	[ID_Patient_Confinement] [int] NULL,
	[CaseType] [varchar](300) NULL,
	[BillingInvoice_ID_FilingStatus] [int] NULL,
	[ID_Client] [int] NULL,
	[_temp] [varchar](300) NULL,
	[_tempID] [varchar](300) NULL,
	[old_history_id] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
	[Weight] [varchar](300) NULL,
	[RunAfterSavedProcess_ID_FilingStatus] [int] NULL,
	[DateRunAfterSavedProcess] [datetime] NULL,
	[IsForBilling] [bit] NULL,
	[GUID] [varchar](300) NULL,
 CONSTRAINT [PK_tPatient_SOAP] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_SOAP] ON [dbo].[tPatient_SOAP]
(
	[Code] ASC,
	[ID_Company] ASC,
	[ID_Patient] ASC,
	[ID_Client] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_SOAP] ADD  CONSTRAINT [DF__tPatient___IsAct__61CE5A37]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_SOAP]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_SOAP_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_SOAP] CHECK CONSTRAINT [FK_tPatient_SOAP_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_SOAP]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_SOAP_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_SOAP] CHECK CONSTRAINT [FK_tPatient_SOAP_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_SOAP]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_SOAP_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_SOAP] CHECK CONSTRAINT [FK_tPatient_SOAP_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_SOAP]
		ON [dbo].[tPatient_SOAP]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_SOAP
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_SOAP hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_SOAP] ENABLE TRIGGER [rDateCreated_tPatient_SOAP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_SOAP]
		ON [dbo].[tPatient_SOAP]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_SOAP
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_SOAP hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_SOAP] ENABLE TRIGGER [rDateModified_tPatient_SOAP]
GO
