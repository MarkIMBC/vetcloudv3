﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveItemType] AS   
				SELECT   
				 H.*  
				FROM vItemType H  
				WHERE IsActive = 1
GO
