﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveIssueTracker] AS   
				SELECT   
				 H.*  
				FROM vIssueTracker H  
				WHERE IsActive = 1
GO
