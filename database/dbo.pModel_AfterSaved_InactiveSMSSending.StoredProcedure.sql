﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pModel_AfterSaved_InactiveSMSSending] (@ID_CurrentObject VARCHAR(10),
                                                         @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @Code_Company VARCHAR(MAX)
            DECLARE @username VARCHAR(MAX);
            DECLARE @password VARCHAR(MAX);

            SELECT @ID_Company = ID_Company
            FROM   dbo.tInactiveSMSSending
            WHERE  ID = @ID_CurrentObject;

            SELECT @Code_Company = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            SELECT @username = Name
            FROM   tInactiveSMSSending
            WHERE  ID = @ID_CurrentObject;

            SET @username = replace(replace(replace(dbo.fGetCleanedString(@username), '.', ''), ',', ''), ' ', '')
            SET @username = LOWER(@username)
            SET @username = @Code_Company + '-' + @username
            SET @password = LEFT(NewId(), 4);

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tInactiveSMSSending';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tInactiveSMSSending
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

GO
