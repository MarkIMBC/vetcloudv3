﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBillingInvoice_ListView]
as
  SELECT ID,
         Date,
         Code,
         ID_SOAPType,
         Name_SOAPType,
         Name_Client,
         Name_Patient,
         PatientNames,
         GrossAmount,
         VatAmount,
         NetAmount,
         SubTotal,
         DiscountAmount,
         TotalAmount,
         RemainingAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         AttendingPhysician_Name_Employee,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Payment_ID_FilingStatus,
         Name_FilingStatus,
         Payment_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         ID_Company,
         Comment,
         OtherReferenceNumber,
         Status,
         CASE
           WHEN isnull(OtherReferenceNumber, '') <> '' then 'Other Ref. #: ' + OtherReferenceNumber
                                                            + CHAR(13)
           ELSE ''
         END
         + CASE
             WHEN isnull(Comment, '') <> '' then 'Comment: ' + CHAR(13) + Comment
             ELSE ''
           END TooltipText
  FROM   dbo.vBillingInvoice with (NOLOCK)
  WHERE  ISNULL(IsWalkIn, 0) = 0

GO
