﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vModelDefaultValue_Listview]
AS
  SELECT _defaultvalue.ID,
         _company.ID        ID_Company,
         _company.Name      Name_Company,
         _model.Oid         Oid_Model,
         _model.Name        Name_Model,
         _model.TableName,
         _defaultvalue.ID_PropertyType,
         _propertytype.Name Name_PropertyType,
         _defaultvalue.PropertyName,
         _defaultvalue.Value,
         _defaultvalue.DateCreated,
         _defaultvalue.DateModified
  FROM   tModelDefaultValue _defaultvalue
         LEFT JOIN tCompany _company
                on _defaultvalue.ID_Company = _company.ID
         LEFT JOIN _tModel _model
                on _defaultvalue.Oid_Model = _model.Oid
         LEFT JOIN _tPropertyType _propertytype
                on _defaultvalue.ID_PropertyType = _propertytype.ID

GO
