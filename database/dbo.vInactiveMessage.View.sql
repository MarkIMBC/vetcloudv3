﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveMessage] AS   
				SELECT   
				 H.*  
				FROM vMessage H  
				WHERE ISNULL(IsActive, 0) = 0
GO
