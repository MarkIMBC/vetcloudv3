﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_Item] (@ID_CurrentObject VARCHAR(10),
                                           @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_ItemType INT;
            DECLARE @Services_ID_ItemType INT = 1
            DECLARE @Inventorialble_ID_ItemType INT = 2

            SELECT @ID_Company = ID_Company,
                   @ID_ItemType = ID_ItemType
            FROM   dbo.tItem
            WHERE  ID = @ID_CurrentObject;

            if( @ID_ItemType = @Inventorialble_ID_ItemType )
              BEGIN
                  SELECT @Oid_Model = m.Oid
                  FROM   dbo._tModel m
                  WHERE  m.TableName = 'tItem';
              END
            ELSE if( @ID_ItemType = @Services_ID_ItemType )
              BEGIN
                  SELECT @Oid_Model = m.Oid
                  FROM   dbo._tModel m
                  WHERE  m.TableName = 'tItemService';
              END

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tItem
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      EXEC PloGitEmuNitPrice
        @ID_CurrentObject

      EXEC PloGitEmuNitCost
        @ID_CurrentObject

      -- pUpdate Item Current Inventory   
      DECLARE @IDs_Item typINTList

      INSERT @IDs_Item
      VALUES(@ID_CurrentObject)

      exec pUpdateItemCurrentInventoryByIDsItems
        @IDs_Item   
  END;

GO
