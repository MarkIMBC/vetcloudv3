﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEmployeeInfo] AS   
				SELECT   
				 H.*  
				FROM vEmployeeInfo H  
				WHERE IsActive = 1
GO
