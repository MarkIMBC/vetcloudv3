﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tListView_Detail](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[Caption] [varchar](200) NULL,
	[ID_ModelProperty] [uniqueidentifier] NULL,
	[ID_ListView] [uniqueidentifier] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DisplayProperty] [varchar](200) NULL,
	[DataSource] [varchar](200) NULL,
	[Format] [varchar](300) NULL,
	[Width] [varchar](300) NULL,
	[Fixed] [bit] NULL,
	[VisibleIndex] [int] NULL,
	[IsAllowEdit] [bit] NULL,
	[ID_ControlType] [int] NULL,
	[ID_ColumnAlignment] [int] NULL,
	[IsVisible] [bit] NULL,
	[FixedPosition] [varchar](300) NULL,
	[IsRequired] [bit] NULL,
	[ID_SummaryType] [int] NULL,
	[Precision] [int] NULL,
	[ID_PropertyType] [int] NULL,
	[IsAddModelClass] [bit] NULL,
	[GroupIndex] [int] NULL,
	[IsFilter] [bit] NULL,
	[ID_FilterControlType] [int] NULL,
 CONSTRAINT [PK__tListView_Detail] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tListView_Detail] ADD  CONSTRAINT [DF__tListView_Detail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tListView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_Detail_ID_ColumnAlignment] FOREIGN KEY([ID_ColumnAlignment])
REFERENCES [dbo].[_tColumnAlignment] ([ID])
GO
ALTER TABLE [dbo].[_tListView_Detail] CHECK CONSTRAINT [FK_tListView_Detail_ID_ColumnAlignment]
GO
ALTER TABLE [dbo].[_tListView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_Detail_ID_ControlType] FOREIGN KEY([ID_ControlType])
REFERENCES [dbo].[_tControlType] ([ID])
GO
ALTER TABLE [dbo].[_tListView_Detail] CHECK CONSTRAINT [FK_tListView_Detail_ID_ControlType]
GO
ALTER TABLE [dbo].[_tListView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tListView_Detail] CHECK CONSTRAINT [FK_tListView_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tListView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_Detail_ID_ListView] FOREIGN KEY([ID_ListView])
REFERENCES [dbo].[_tListView] ([Oid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[_tListView_Detail] CHECK CONSTRAINT [FK_tListView_Detail_ID_ListView]
GO
