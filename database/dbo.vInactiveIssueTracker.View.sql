﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveIssueTracker] AS   
				SELECT   
				 H.*  
				FROM vIssueTracker H  
				WHERE ISNULL(IsActive, 0) = 0
GO
