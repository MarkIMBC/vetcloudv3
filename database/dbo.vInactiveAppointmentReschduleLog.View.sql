﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppointmentReschduleLog] AS   
    SELECT   
     H.*  
    FROM vAppointmentReschduleLog H  
    WHERE ISNULL(IsActive, 0) = 1
GO
