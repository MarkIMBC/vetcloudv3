﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientWaitingListCanceled] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingListCanceled H  
				WHERE IsActive = 1
GO
