﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveToothStatus] AS   
				SELECT   
				 H.*  
				FROM vToothStatus H  
				WHERE IsActive = 1
GO
