﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompany_IsActiveLog] AS   
				SELECT   
				 H.*  
				FROM vCompany_IsActiveLog H  
				WHERE IsActive = 1
GO
