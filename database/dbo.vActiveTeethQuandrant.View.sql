﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTeethQuandrant] AS   
				SELECT   
				 H.*  
				FROM vTeethQuandrant H  
				WHERE IsActive = 1
GO
