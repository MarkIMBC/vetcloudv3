﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vReceivingReport_Listview]
AS
  SELECT ID,
         Date,
         Code,
         GrossAmount,
         VatAmount,
         NetAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Name_FilingStatus,
         ServingStatus_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         Code_PurchaseOrder,
         ID_Company,
         Name_Supplier
  FROM   vReceivingReport

GO
