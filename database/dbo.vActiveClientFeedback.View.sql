﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClientFeedback] AS   
				SELECT   
				 H.*  
				FROM vClientFeedback H  
				WHERE IsActive = 1
GO
