﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveScheduleType] AS   
				SELECT   
				 H.*  
				FROM vScheduleType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
