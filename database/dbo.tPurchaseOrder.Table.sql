﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPurchaseOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Supplier] [int] NULL,
	[TotalQuantity] [decimal](18, 4) NULL,
	[TotalVAT] [decimal](18, 4) NULL,
	[TotalGrossAmount] [decimal](18, 4) NULL,
	[TotalNetAmount] [decimal](18, 4) NULL,
	[TotalVatAmount] [decimal](18, 4) NULL,
	[DocumentDate] [date] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_SubmittedBy] [int] NULL,
	[DateSubmitted] [datetime] NULL,
	[ID_TaxScheme] [int] NULL,
	[DateApproved] [datetime] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateCancelled] [datetime] NULL,
	[ID_CancelledBy] [int] NULL,
	[GrossAmount] [decimal](18, 4) NULL,
	[VatAmount] [decimal](18, 4) NULL,
	[NetAmount] [decimal](18, 4) NULL,
	[Date] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ServingStatus_ID_FilingStatus] [int] NULL,
	[DiscountRate] [decimal](18, 4) NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[IsComputeDiscountRate] [bit] NULL,
	[SubTotal] [decimal](18, 4) NULL,
	[TotalAmount] [decimal](18, 4) NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tPurchaseOrder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPurchaseOrder] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FK_tPurchaseOrder_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FK_tPurchaseOrder_ID_Company]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FK_tPurchaseOrder_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FK_tPurchaseOrder_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FK_tPurchaseOrder_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FK_tPurchaseOrder_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_ID_ApprovedBy] FOREIGN KEY([ID_ApprovedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FKtPurchaseOrder_ID_ApprovedBy]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_ID_FilingStatus] FOREIGN KEY([ID_FilingStatus])
REFERENCES [dbo].[tFilingStatus] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FKtPurchaseOrder_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPurchaseOrder]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_ID_Supplier] FOREIGN KEY([ID_Supplier])
REFERENCES [dbo].[tSupplier] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder] CHECK CONSTRAINT [FKtPurchaseOrder_ID_Supplier]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPurchaseOrder]
		ON [dbo].[tPurchaseOrder]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPurchaseOrder
			SET    DateCreated = GETDATE()
			FROM   dbo.tPurchaseOrder hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPurchaseOrder] ENABLE TRIGGER [rDateCreated_tPurchaseOrder]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPurchaseOrder]
		ON [dbo].[tPurchaseOrder]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPurchaseOrder
			SET    DateModified = GETDATE()
			FROM   dbo.tPurchaseOrder hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPurchaseOrder] ENABLE TRIGGER [rDateModified_tPurchaseOrder]
GO
