﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppointmentRequest] AS   
				SELECT   
				 H.*  
				FROM vAppointmentRequest H  
				WHERE ISNULL(IsActive, 0) = 0
GO
