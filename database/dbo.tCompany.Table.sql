﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Country] [int] NULL,
	[Address] [varchar](300) NULL,
	[ImageLogoFilename] [varchar](300) NULL,
	[ContactNumber] [varchar](300) NULL,
	[ImageHeaderFilename] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[IsShowPOSReceiptLogo] [bit] NULL,
	[SOAPPlanSMSMessage] [varchar](700) NULL,
	[IsRemoveBoldText] [bit] NULL,
	[IsShowHeader] [bit] NULL,
	[POSReceiptFontSize] [varchar](300) NULL,
	[SecurityPIN] [varchar](300) NULL,
	[IsShowPaymentLabel] [bit] NULL,
	[IsShowPaymentWarningLabel] [bit] NULL,
	[ID_PackagePlan] [int] NULL,
	[dbpassword] [varchar](300) NULL,
	[dbusername] [varchar](300) NULL,
	[Guid] [varchar](300) NULL,
	[ReceptionPortalGuid] [varchar](300) NULL,
	[IsShowFooter] [bit] NULL,
 CONSTRAINT [PK_tCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCompany] ADD  CONSTRAINT [DF__tCompany__IsActi__03FB8544]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tCompany] ADD  DEFAULT ((1)) FOR [IsShowPOSReceiptLogo]
GO
ALTER TABLE [dbo].[tCompany] ADD  DEFAULT ((0)) FOR [IsRemoveBoldText]
GO
ALTER TABLE [dbo].[tCompany] ADD  DEFAULT ((1)) FOR [IsShowHeader]
GO
ALTER TABLE [dbo].[tCompany] ADD  CONSTRAINT [DF_Company_IsShowFooter]  DEFAULT ((1)) FOR [IsShowFooter]
GO
ALTER TABLE [dbo].[tCompany]  WITH NOCHECK ADD  CONSTRAINT [FK_tCompany_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tCompany] CHECK CONSTRAINT [FK_tCompany_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tCompany]  WITH NOCHECK ADD  CONSTRAINT [FK_tCompany_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tCompany] CHECK CONSTRAINT [FK_tCompany_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tCompany]
		ON [dbo].[tCompany]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tCompany
			SET    DateCreated = GETDATE()
			FROM   dbo.tCompany hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tCompany] ENABLE TRIGGER [rDateCreated_tCompany]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tCompany]
		ON [dbo].[tCompany]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tCompany
			SET    DateModified = GETDATE()
			FROM   dbo.tCompany hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tCompany] ENABLE TRIGGER [rDateModified_tCompany]
GO
