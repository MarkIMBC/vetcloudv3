﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBillingInvoice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient] [int] NULL,
	[ID_TaxScheme] [int] NULL,
	[Date] [datetime] NULL,
	[BillingAddress] [varchar](3000) NULL,
	[VatAmount] [decimal](18, 4) NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[GrossAmount] [decimal](18, 4) NULL,
	[VatPercentage] [decimal](18, 4) NULL,
	[NetAmount] [decimal](18, 4) NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_ApprovedBy] [int] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[DateCanceled] [datetime] NULL,
	[Payment_ID_FilingStatus] [int] NULL,
	[RemainingAmount] [decimal](18, 4) NULL,
	[ID_Client] [int] NULL,
	[Discount] [decimal](18, 4) NULL,
	[DiscountRate] [decimal](18, 4) NULL,
	[IsComputeDiscountRate] [bit] NULL,
	[SubTotal] [decimal](18, 4) NULL,
	[TotalAmount] [decimal](18, 4) NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[ID_SOAPType] [int] NULL,
	[ID_Patient_SOAP] [int] NULL,
	[ID_Patient_Confinement] [int] NULL,
	[PatientNames] [varchar](300) NULL,
	[ConfinementDepositAmount] [decimal](18, 4) NULL,
	[RemainingDepositAmount] [decimal](18, 4) NULL,
	[ConsumedDepositAmount] [decimal](18, 4) NULL,
	[InitialSubtotalAmount] [decimal](18, 4) NULL,
	[ID_Patient_Vaccination] [int] NULL,
	[IsWalkIn] [bit] NULL,
	[WalkInCustomerName] [varchar](300) NULL,
	[ID_Patient_Wellness] [int] NULL,
	[TotalItemDiscountAmount] [decimal](18, 4) NULL,
	[OtherReferenceNumber] [varchar](300) NULL,
	[InitialConfinementDepositAmount] [decimal](18, 4) NULL,
	[tempID] [varchar](300) NULL,
	[RunAfterSavedProcess_ID_FilingStatus] [int] NULL,
	[DateRunAfterSavedProcess] [datetime] NULL,
	[GrossSubTotalAmount] [decimal](18, 4) NULL,
	[ID_Patient_Lodging] [int] NULL,
	[ID_Patient_Grooming] [int] NULL,
 CONSTRAINT [PK_tBillingInvoice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tBillingInvoice] ON [dbo].[tBillingInvoice]
(
	[Code] ASC,
	[ID_Client] ASC,
	[ID_Patient_Confinement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBillingInvoice] ADD  CONSTRAINT [DF__tBillingI__IsAct__5011CCEA]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBillingInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice] CHECK CONSTRAINT [FK_tBillingInvoice_ID_Company]
GO
ALTER TABLE [dbo].[tBillingInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice] CHECK CONSTRAINT [FK_tBillingInvoice_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice] CHECK CONSTRAINT [FK_tBillingInvoice_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tBillingInvoice]
		ON [dbo].[tBillingInvoice]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice
			SET    DateCreated = GETDATE()
			FROM   dbo.tBillingInvoice hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice] ENABLE TRIGGER [rDateCreated_tBillingInvoice]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tBillingInvoice]
		ON [dbo].[tBillingInvoice]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice
			SET    DateModified = GETDATE()
			FROM   dbo.tBillingInvoice hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice] ENABLE TRIGGER [rDateModified_tBillingInvoice]
GO
