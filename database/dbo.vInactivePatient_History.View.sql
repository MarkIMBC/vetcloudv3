﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_History] AS   
				SELECT   
				 H.*  
				FROM vPatient_History H  
				WHERE ISNULL(IsActive, 0) = 0
GO
