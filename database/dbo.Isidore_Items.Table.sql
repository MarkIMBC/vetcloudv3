﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Isidore_Items](
	[ITEMS] [nvarchar](255) NULL,
	[BUYING PRICE] [nvarchar](255) NULL,
	[SELLING PRICE] [nvarchar](255) NULL,
	[INVENTORY] [nvarchar](255) NULL,
	[EXPIRATION] [datetime] NULL,
	[Category ] [nvarchar](255) NULL
) ON [PRIMARY]
GO
