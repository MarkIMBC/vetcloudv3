﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddReport_Filter] (@ID_Report       VARCHAR(MAX),
                              @Name            VARCHAR(MAX),
                              @Caption         VARCHAR(MAX),
                              @ID_ControlType  INT,
                              @ID_PropertyType INT,
                              @DataSource      VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   _tReport_Filters
         WHERE  ID_Report = @ID_Report
                and Name = @Name) = 0
        BEGIN
            INSERT INTO [dbo].[_tReport_Filters]
                        ([Oid],
                         [Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [ID_Report],
                         [ID_ControlType],
                         [ID_PropertyType],
                         [DataSource],
                         [Caption])
            VALUES      (NEWID(),
                         @Name,
                         1,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @ID_Report,
                         @ID_ControlType,
                         @ID_PropertyType,
                         @DataSource,
                         @Caption)
        END
  END

GO
