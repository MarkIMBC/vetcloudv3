﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveStudent] AS   
				SELECT   
				 H.*  
				FROM vStudent H  
				WHERE IsActive = 1
GO
