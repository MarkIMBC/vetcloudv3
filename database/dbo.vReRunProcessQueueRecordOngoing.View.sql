﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE   view [dbo].[vReRunProcessQueueRecordOngoing]
  as
  WITH CTE        
       as (SELECT DB_NAME()         DatabaseName,        
                  'tBillingInvoice' TableName,        
      c.Name Name_Company,        
                  bi.ID             ID_CurrentObject,        
                  bi.DateModified,        
                  RunAfterSavedProcess_ID_FilingStatus        
           FROm   tBillingInvoice bi        
                  INNER JOIN vCompanyActive c        
                          on bi.ID_Company = c.ID        
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0)  IN( 9 )        
        ANd ID_FilingStatus NOT IN (4)        
                            AND CONVERT(Date, bi.DateModified) BETWEEN DATEADD(DAY, -60,  CONVERT(Date, GETDATE())) AND  CONVERT(Date, GETDATE())         
           UNion ALL        
           SELECT DB_NAME()       DatabaseName,        
                  'tPatient_SOAP' TableName,        
      c.Name Name_Company,        
                  _soap.ID        ID_CurrentObject,        
                  _soap.DateModified,        
                  RunAfterSavedProcess_ID_FilingStatus        
           FROm   tPatient_SOAP _soap        
                  INNER JOIN vCompanyActive c        
                          on _soap.ID_Company = c.ID        
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0)  IN( 9)        
        ANd ID_FilingStatus NOT IN (4)        
                            AND CONVERT(Date, _soap.DateModified)  BETWEEN DATEADD(DAY, -60,  CONVERT(Date, GETDATE())) AND  CONVERT(Date, GETDATE())         
         )        
  SELECT DatabaseName + TableName        
         + CONVERT(varchar(MAX), ID_CurrentObject) Oid,        
         *        
  FROm   CTE 
GO
