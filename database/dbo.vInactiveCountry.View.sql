﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCountry] AS   
				SELECT   
				 H.*  
				FROM vCountry H  
				WHERE ISNULL(IsActive, 0) = 0
GO
