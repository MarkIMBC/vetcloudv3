﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Confinement_ItemsServices]  
AS  
  SELECT H.*,  
         UC.Name                               AS CreatedBy,  
         UM.Name                               AS LastModifiedBy,  
         item.Name                             AS Name_Item,  
         REPLACE(H.Comment, CHAR(13), '<br/>') AS CommentHTML,  
         item.ID_ItemType                      AS ID_ItemType  ,
		 ISNULL(patient.Name, _soap.Name_Patient) Name_Patient
  FROM   tPatient_Confinement_ItemsServices H  
         LEFT JOIN tUser UC  
                ON H.ID_CreatedBy = UC.ID  
         LEFT JOIN tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
         LEFT JOIN tItem item  
                ON H.ID_Item = item.ID     LEFT JOIN tPatient patient
                ON H.ID_Patient= patient.ID   
		LEFT JOIN vPatient_SOAP _soap on H.ID_Patient_SOAP = _soap.ID 


GO
