﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tDetailView_Detail_Link](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_DetailView_Detail] [uniqueidentifier] NOT NULL,
	[ID_DetailView_Detail_Link] [uniqueidentifier] NULL,
	[IsRequired] [bit] NULL,
 CONSTRAINT [PK__tDetailView_Detail_Link] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link] ADD  CONSTRAINT [DF__tDetailView_Detail_Link_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_Link_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link] CHECK CONSTRAINT [FK_tDetailView_Detail_Link_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_Link_ID_DetailView_Detail] FOREIGN KEY([ID_DetailView_Detail])
REFERENCES [dbo].[_tDetailView_Detail] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link] CHECK CONSTRAINT [FK_tDetailView_Detail_Link_ID_DetailView_Detail]
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_Link_ID_DetailView_Detail_Link] FOREIGN KEY([ID_DetailView_Detail_Link])
REFERENCES [dbo].[_tDetailView_Detail] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link] CHECK CONSTRAINT [FK_tDetailView_Detail_Link_ID_DetailView_Detail_Link]
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_Link_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tDetailView_Detail_Link] CHECK CONSTRAINT [FK_tDetailView_Detail_Link_ID_LastModifiedBy]
GO
