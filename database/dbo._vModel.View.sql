﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vModel]
AS
SELECT    H.*,
            U.COLUMN_NAME AS PrimaryKey,
			DV.Name AS DetailView
  FROM      _tModel H
            LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE U ON OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA
                                                              + '.'
                                                              + QUOTENAME(CONSTRAINT_NAME)),
                                                              'IsPrimaryKey') = 1
                                                              AND U.TABLE_NAME = H.TableName
			LEFT JOIN dbo.[_tDetailView] DV ON H.ID_DetailView = DV.Oid
GO
