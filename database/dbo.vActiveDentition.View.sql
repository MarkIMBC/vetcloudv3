﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveDentition] AS   
				SELECT   
				 H.*  
				FROM vDentition H  
				WHERE IsActive = 1
GO
