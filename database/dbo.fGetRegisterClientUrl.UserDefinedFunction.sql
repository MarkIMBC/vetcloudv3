﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fGetRegisterClientUrl]()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = '';

      SET @result = 'https://registerclient1.veterinarycloudsystem.com'

      return @result
  END

GO
