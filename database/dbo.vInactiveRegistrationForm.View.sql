﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveRegistrationForm] AS   
    SELECT   
     H.*  
    FROM vRegistrationForm H  
    WHERE ISNULL(IsActive, 0) = 1
GO
