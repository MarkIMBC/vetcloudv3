﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveRegistrationForm] AS   
    SELECT   
     H.*  
    FROM vRegistrationForm H  
    WHERE IsActive = 1
GO
