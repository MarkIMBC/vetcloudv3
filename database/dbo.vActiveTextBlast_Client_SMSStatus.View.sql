﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTextBlast_Client_SMSStatus] AS   
				SELECT   
				 H.*  
				FROM vTextBlast_Client_SMSStatus H  
				WHERE IsActive = 1
GO
