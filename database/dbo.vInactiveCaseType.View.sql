﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCaseType] AS   
				SELECT   
				 H.*  
				FROM vCaseType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
