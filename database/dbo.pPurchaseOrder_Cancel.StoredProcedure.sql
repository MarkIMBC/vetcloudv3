﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pPurchaseOrder_Cancel] @ID INT
AS
BEGIN
  
  UPDATE tPurchaseOrder SET ID_FilingStatus = 5, DateCancelled = GETDATE(), ID_CancelledBy = 1 WHERE ID = @ID

END

GO
