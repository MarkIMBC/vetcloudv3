﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_SOAP_Plan]
AS
  SELECT H.*,
         ISNULL(item.Name, '')
         + CASE
             WHEN LEN(ISNULL(H.CustomItem, '')) = 0 then ''
             else ' ' + Char(10) + CHAR(13)
           END
         + ISNULL(H.CustomItem, '') Name_Item,
         fs.Name                    Appointment_Name_FilingStatus
  FROM   dbo.tPatient_SOAP_Plan H
         LEFT JOIN dbo.tItem item
                ON item.ID = H.ID_Item
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.Appointment_ID_FilingStatus

GO
