﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_PayablePayment] (@ID_CurrentObject VARCHAR(10),
                                                 @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_FilingStatus INT = 0
      DECLARE @ID_CreatedBy INT = 0
      DECLARE @IDs_PayablePayment typIntList

      INSERT @IDs_PayablePayment
      VALUES (@ID_CurrentObject)

      SELECT @ID_FilingStatus = ID_FilingStatus,
             @ID_CreatedBy = ID_CreatedBy
      FROM   tPayablePayment
      where  iD = @ID_CurrentObject

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT = 0;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPayablePayment
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'PayablePayment';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPayablePayment
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      IF( @ID_FilingStatus = 20 )
        BEGIN
            DECLARE @ID_UserSession INT = 0

            SELECT @ID_UserSession = ID
            FROM   tUserSession
            WHERE  ID_User = @ID_CreatedBy

            exec pApprovePayablePayment
              @IDs_PayablePayment,
              @ID_UserSession
        END
  END;

GO
