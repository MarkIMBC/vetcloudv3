﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUserRole_Detail] AS   
				SELECT   
				 H.*  
				FROM vUserRole_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
