﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vpivotBilledPatientSOAP]
AS
  SELECT *
  FROM   (SELECT ID_Patient_SOAP,
                 BillingInvoice_RowID,
                 Info_BillingInvoice
          FROM   (SELECT bi.ID_Patient_SOAP,
                         ROW_NUMBER()
                           OVER(
                             PARTITION BY bi.ID_Patient_SOAP
                             ORDER BY ID_Patient_SOAP ASC, bi.ID)             BillingInvoice_RowID,
                         format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                  FROM   vBillingINvoice bi
                         inner JOIN tPatient_SOAP soap
                                 ON bi.ID_Patient_SOAP = soap.ID
                  where  ISNULL(bi.ID_Patient_SOAP, 0) <> 0
                         AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
         PIVOT(MAX([Info_BillingInvoice])
              FOR [BillingInvoice_RowID] IN([1],
                                            [2],
                                            [3],
                                            [4],
                                            [5],
                                            [6],
                                            [7],
                                            [8],
                                            [9],
                                            [10] )) AS PivotTable;

GO
