﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pDeleteAppModel] @TableName VARCHAR(100) = NULL, @ID_Model UNIQUEIDENTIFIER = NULL
AS
    BEGIN TRY  
		--DELETE FROM tDocumentSeries WHERE ID_Model = @ID_Model
		--UPDATE tDeliveryReceipt_Detail SET ID_DocModel = NULL WHERE ID_DocModel = @ID_Model
		--UPDATE tReceivingReport_Detail SET ID_DocModel = NULL WHERE ID_DocModel = @ID_Model

        IF @ID_Model IS NULL
        BEGIN
          SELECT @ID_Model = Oid FROM _tModel WHERE UPPER(TableName) = UPPER(@TableName)
          IF ( @ID_Model IS NULL )
              RETURN
        END

        DELETE  FROM dbo.[_tNavigation]
        WHERE   ID_View IN (
                SELECT  Oid
                FROM    dbo.[_tView]
                WHERE   ID_ListView IN ( SELECT Oid
                                         FROM   dbo.[_tListView]
                                         WHERE  ID_Model = @ID_Model ) )

        DELETE  FROM dbo.[_tView]
        WHERE   ID_ListView IN ( SELECT Oid
                                 FROM   dbo.[_tListView]
                                 WHERE  ID_Model = @ID_Model )

        DELETE  FROM dbo.[_tListView_Detail]
        WHERE   ID_ListView IN ( SELECT Oid
                                 FROM   dbo.[_tListView]
                                 WHERE  ID_Model = @ID_Model )


        DELETE  FROM dbo.[_tDetailView_Detail]
        WHERE   ID_ListView IN ( SELECT Oid
                                 FROM   dbo.[_tListView]
                                 WHERE  ID_Model = @ID_Model )

        DELETE  FROM dbo.[_tDetailView_Detail]
        WHERE   ID_LookUp_ListView IN ( SELECT  Oid
                                        FROM    dbo.[_tListView]
                                        WHERE   ID_Model = @ID_Model )

        DELETE  FROM dbo.[_tListView]
        WHERE   ID_Model = @ID_Model

        UPDATE  dbo.[_tModel]
        SET     ID_DetailView = NULL
        WHERE   ID_DetailView IN ( SELECT   Oid
                                   FROM     dbo.[_tDetailView]
                                   WHERE    ID_Model = @ID_Model )

		DELETE FROM _tDetailView_Detail_Link WHERE ID_DetailView_Detail IN (
		SELECT Oid FROM dbo.[_tDetailView_Detail]
        WHERE   ID_DetailView IN ( SELECT   Oid
                                   FROM     dbo.[_tDetailView]
                                   WHERE    ID_Model = @ID_Model )
		)

		DELETE FROM _tDetailView_Detail_Link WHERE ID_DetailView_Detail_Link IN (
		SELECT Oid FROM dbo.[_tDetailView_Detail]
        WHERE   ID_DetailView IN ( SELECT   Oid
                                   FROM     dbo.[_tDetailView]
                                   WHERE    ID_Model = @ID_Model )
		)

        DELETE  FROM dbo.[_tDetailView]
        WHERE   ID_Model = @ID_Model


        DELETE  FROM dbo.[_tDetailView_Detail]
        WHERE   ID_DetailView IN ( SELECT   Oid
                                   FROM     dbo.[_tDetailView]
                                   WHERE    ID_Model = @ID_Model )

        DELETE  FROM dbo.[_tDetailView]
        WHERE   ID_Model = @ID_Model

        UPDATE  dbo.[_tModel_Property]
        SET     ID_PropertyModel = NULL
        WHERE   ID_PropertyModel = @ID_Model

        UPDATE  dbo.[_tModel_Property]
        SET     ID_ModelProperty_Key = NULL
        WHERE   ID_ModelProperty_Key IN ( SELECT    Oid
                                          FROM      dbo.[_tModel_Property]
                                          WHERE     ID_Model = @ID_Model )

        DELETE  FROM dbo.[_tModel_Property]
        WHERE   ID_Model = @ID_Model

        DELETE  FROM dbo.[_tModel]
        WHERE   Oid = @ID_Model

        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;  
    END TRY
    BEGIN CATCH
        SELECT  ERROR_NUMBER() AS ErrorNumber ,
                ERROR_SEVERITY() AS ErrorSeverity ,
                ERROR_STATE() AS ErrorState ,
                ERROR_PROCEDURE() AS ErrorProcedure ,
                ERROR_LINE() AS ErrorLine ,
                ERROR_MESSAGE() AS ErrorMessage;  
    END CATCH

    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
GO
