﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePosition] AS   
				SELECT   
				 H.*  
				FROM vPosition H  
				WHERE ISNULL(IsActive, 0) = 0
GO
