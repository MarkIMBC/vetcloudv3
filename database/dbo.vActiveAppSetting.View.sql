﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppSetting] AS   
				SELECT   
				 H.*  
				FROM vAppSetting H  
				WHERE IsActive = 1
GO
