﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pInsertReferralType](@referralType VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReferralType
         WHERE  Name = @referralType) = 0
        BEGIN
            INSERT INTO [dbo].tReferralType
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@referralType,
                         1,
                         1)
        END
  END

GO
