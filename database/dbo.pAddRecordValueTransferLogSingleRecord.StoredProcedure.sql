﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 PROC [dbo].[pAddRecordValueTransferLogSingleRecord](@From_ID_RecordValue INT,
                                                          @To_ID_RecordValue   INT,
                                                          @PrimaryColumn       VARCHAR(MAX),
                                                          @ColumnName          VARCHAR(MAX),
                                                          @TableName           VARCHAR(MAX),
                                                          @Comment             VARCHAR(MAX))
AS
  BEGIN
      DECLARE @From_IDs_RecordValue typIntList

      INSERT @From_IDs_RecordValue
      VALUES (@From_ID_RecordValue)

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        @PrimaryColumn,
        @ColumnName,
        @TableName,
        @Comment
  END

GO
