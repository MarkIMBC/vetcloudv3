﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CREATE     
 PROC [dbo].[pReRun_AfterSaved_Process_Patient_SOAP_By_ID](@ID_Patient_SOAP INT)  
AS  
  BEGIN  
      DECLARE @IDs_Patient_SOAP typIntList  
  
   INSERT @IDs_Patient_SOAP  
   VALUES (@ID_Patient_SOAP)  
  
      exec pReRun_AfterSaved_Process_Patient_SOAP_By_IDs  
        @IDs_Patient_SOAP  
  END  
GO
