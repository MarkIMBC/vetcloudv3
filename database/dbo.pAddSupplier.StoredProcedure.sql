﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE   
 PROC [dbo].[pAddSupplier](@supplierName VARCHAR(MAX),  
                        @ID_Company   INT)  
as  
  BEGIN  
      IF(SELECT COUNT(*)  
         FROM   tSupplier  
         where  Name = @supplierName  
                AND ID_Company = @ID_Company  
                AND IsActive = 1) = 0  
        BEGIN  
            INSERT INTO [dbo].tSupplier  
                        ([Name],  
                         [IsActive],  
                         [ID_Company],  
                         [DateCreated],  
                         [DateModified],  
                         [ID_CreatedBy],  
                         [ID_LastModifiedBy])  
            VALUES      (@supplierName,  
                         1,  
                         @ID_Company,  
                         GETDATE(),  
                         GETDATE(),  
                         1,  
                         1)  
        END  
  END  
  
GO
