﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGetPaymentTransaction] @ID         INT = -1,
                                      @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail;

      DECLARE @ID_User INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @ID_BillingInvoice INT;

      SELECT @ID_BillingInvoice = ID_BillingInvoice
      FROM   dbo.tPaymentTransaction
      WHERE  ID = @ID;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name Name_FilingStatus
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           GETDATE() [Date],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           1         AS [ID_FilingStatus],
                           NEWID()   GUID) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus;
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPaymentTransaction H
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   dbo.vBillingInvoice_Detail
      WHERE  ID_BillingInvoice = @ID_BillingInvoice;
  END;

GO
