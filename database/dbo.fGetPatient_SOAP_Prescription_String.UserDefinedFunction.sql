﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 FUNCTION [dbo].[fGetPatient_SOAP_Prescription_String] (@ID_Patient_SOAP INT)
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @Rows TABLE
        (
           RowIndex  INT NOT NULL IDENTITY PRIMARY KEY,
           Name_Item VARCHAR(MAX),
           Quantity  INT,
           Comment   VARCHAR(MAX)
        );
      DECLARE @prescription VARCHAR(MAX) = '';
      DECLARE @msg VARCHAR(MAX) = '';

      SELECT @prescription = ISNULL(Prescription, '')
      FROM   tPatient_SOAP
      WHERE  ID = @ID_Patient_SOAP

      INSERT @Rows
             (Name_Item,
              Quantity,
              Comment)
      SELECT Name_Item,
             Quantity,
             Comment
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID_Patient_SOAP

      SELECT @msg = @msg
                    + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                    + FORMAT(RowIndex, '#,#0') + '. '
                    + ISNULL(Name_Item, '')
                    + CASE
                        WHEN ISNULL(Quantity, 0) > 0 THEN ' Qty: '
                                                          + FORMAT(ISNULL(Quantity, 0), '#,#0')
                        ELSE ' '
                      END
                    + '<br/>'
                    + CASE
                        WHEN LEN(ISNULL(Comment, '')) > 0 THEN '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sig. '
                        ELSE ''
                      END
                    + ISNULL(Comment, '') + '<br/><br/>'
      FROM   @Rows;

      SET @msg = @msg + '<br/>'
      SET @msg = @msg + @prescription
      SET @msg = REPLACE(REPLACE(REPLACE(@msg, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>')

      RETURN @msg;
  END;

GO
