﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pUpdateItemCurrentInventory]
AS
  BEGIN
      exec pRemoveMulticateInventoryTrail
        0

      UPDATE dbo.tItem
      SET    CurrentInventoryCount = ISNULL(inventory.Qty, 0),
             ID_InventoryStatus = dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount, 0), ISNULL(MaxInventoryCount, 0))
      FROM   dbo.tItem item
             LEFT JOIN vInventoryTrailTotal inventory
                    ON inventory.ID_Item = item.ID;
  END;

GO
