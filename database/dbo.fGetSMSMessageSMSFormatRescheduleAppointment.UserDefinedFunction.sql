﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 FUNCTION [dbo].[fGetSMSMessageSMSFormatRescheduleAppointment] (@SMSFormat             VARCHAR(MAX),
                                                                 @Name_Company          Varchar(MAX),
                                                                 @ContactNumber_Company Varchar(MAX),
                                                                 @DateReschedule        DateTime,
                                                                 @Client                Varchar(MAX),
                                                                 @Pet                   Varchar(MAX),
                                                                 @ReferenceCode         Varchar(MAX),
                                                                 @Comment               Varchar(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @message VARCHAR(MAX) = ''
        + 'Hi /*Client*/, /*Pet*/ has a reschedule appointment on /*DateReschedule*/ for /*Comment*/. '
        + 'Kindly call /*Company*/ at /*ContactNumber_Company*/ for any question. '
        + 'Thank You'
      Declare @DateString Varchar(MAX) = FORMAT(@DateReschedule, 'M/dd/yyyy ddd')

      if( LEN(ISNULL(@SMSFormat, '')) > 0 )
        BEGIN
            SET @message = @SMSFormat
        END

      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(ISNULL(@Client, ''))))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(ISNULL(@Pet, ''))))
      SET @message = REPLACE(@message, '/*Company*/', LTRIM(RTRIM(ISNULL(@Name_Company, ''))))
      SET @message = REPLACE(@message, '/*DateReschedule*/', LTRIM(RTRIM(ISNULL(@DateString, ''))))
      SET @message = REPLACE(@message, '/*ContactNumber_Company*/', LTRIM(RTRIM(ISNULL(@ContactNumber_Company, ''))))
      SET @message = REPLACE(@message, '/*Comment*/', LTRIM(RTRIM(ISNULL(@Comment, ''))))
      SET @message = REPLACE(@message, '"', '``')

      RETURN @message
  END

GO
