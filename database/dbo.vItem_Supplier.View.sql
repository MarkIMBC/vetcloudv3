﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vItem_Supplier]
as
  SELECT hed.*,
         s.Name Name_Supplier
  FROM   tItem_Supplier hed
         INNER JOIN tSupplier s
                 ON hed.ID_Supplier = s.ID
         INNER JOIN tItem item
                 ON hed.ID_Item = item.ID

GO
