﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE         
 VIEW [dbo].[vPatientWaitingList_ListView_temp_Yesterday]
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         --IsnULL(appointment.UniqueIDList, '') UniqueIDList,            
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.Name_Model_Reference,
         waitingList.ID_Reference,
         Convert(Date, waitingList.DateReschedule)                   DateReschedule
  FROM   vPatientWaitingList waitingList
         LEFT Join tPatient patient
                on waitingList.ID_Patient = patient.ID
  -- OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment            
  WHERE  waitingList.WaitingStatus_ID_FilingStatus IN ( 8, 9 )
          OR ( ISNULL(waitingList.IsQueued, 0) = 1
               and ISNULL(waitingList.WaitingStatus_ID_FilingStatus, 0) NOT IN ( 0, 4, 13, 21 ) )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            --appointment.UniqueIDList,            
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.Name_Model_Reference,
            waitingList.ID_Reference,
            Convert(Date, waitingList.DateReschedule)
  HAVING MAX(CONVERT(Date, waitingList.DateCreated)) = CONVERT(Date, DATEADD(DAY, -1,GETDATE()))

GO
