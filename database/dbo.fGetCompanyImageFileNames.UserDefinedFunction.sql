﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fGetCompanyImageFileNames](@IDsCompanyString varchar(MAX))
RETURNS @ImagePaths TABLE (
  Model            VARCHAR(MAX),
  TableName        VARCHAR(MAX),
  ID_Company       INT,
  Name_Company     VARCHAR(MAX),
  ImageFilePath    VARCHAR(MAX),
  ColumnName       VARCHAR(MAX),
  ID_CurrentObject INT)
AS
  BEGIN
      DECLARE @IDS_Company typIntList
      DECLARE @IsSuccess BIT = 1
      DECLARE @IsFolderized BIT = 1

      if( LEN(@IDsCompanyString) > 0 )
        BEGIN
            INSERT @IDS_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDS_Company
            SELECT ID
            FROM   tCompany
            WHERE  IsActive = 1
        END

      /* Company */
      INSERT @ImagePaths
      SELECT 'Company'             Model,
             'tCompany',
             company.ID,
             company.Name,
             ImageHeaderFilename,
             'ImageHeaderFilename' ColumnName,
             company.ID
      FROm   tCompany company
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageHeaderFilename, '') <> '' )
      UNIOn ALL
      SELECT 'Company'           Model,
             'tCompany',
             company.ID,
             company.Name,
             ImageLogoFilename,
             'ImageLogoFilename' ColumnName,
             company.ID
      FROm   tCompany company
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageLogoFilename, '') <> '' )

      /*Employee*/
      INSERT @ImagePaths
      SELECT 'Employee'  Model,
             'tEmployee',
             ID_Company,
             company.Name,
             ImageFile,
             'ImageFile' ColumnName,
             emp.ID
      FROm   tEmployee emp
             inner join tCompany company
                     on emp.ID_Company = company.ID
             inner join @IDS_Company ids
                     on emp.ID_Company = ids.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )

      /*Patient*/
      INSERT @ImagePaths
      SELECT 'Patient'          Model,
             'tPatient',
             ID_Company,
             company.Name,
             ProfileImageFile,
             'ProfileImageFile' ColumnName,
             patient.ID
      FROm   tPatient patient
             inner join @IDS_Company ids
                     on patient.ID_Company = ids.ID
             inner join tCompany company
                     on patient.ID_Company = company.ID
      WHERE  ( ISNULL(ProfileImageFile, '') <> '' )

      /* SOAP */
      INSERT @ImagePaths
      SELECT 'Patient SOAP' Model,
             'tPatient_SOAP',
             ID_Company,
             company.Name   e,
             FilePath,
             ColumnName,
             ps.ID
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01,
                                 'LabImageFilePath01' ColumnName
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02,
                                 'LabImageFilePath02' ColumnName
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03,
                                 'LabImageFilePath03' ColumnName
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04,
                                 'LabImageFilePath04' ColumnName
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05,
                                 'LabImageFilePath05' ColumnName
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06,
                                 'LabImageFilePath06' ColumnName
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07,
                                 'LabImageFilePath07' ColumnName
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08,
                                 'LabImageFilePath08' ColumnName
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09,
                                 'LabImageFilePath10' ColumnName
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10,
                                 'LabImageFilePath10' ColumnName
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11,
                                 'LabImageFilePath11' ColumnName
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12,
                                 'LabImageFilePath12' ColumnName
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13,
                                 'LabImageFilePath13' ColumnName
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14,
                                 'LabImageFilePath14' ColumnName
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15,
                                 'LabImageFilePath15' ColumnName) c (ImageNo, ImageRowIndex, FilePath, Remark, ColumnName)
             INNER JOIN @IDS_Company idsCompany
                     on idsCompany.ID = ps.ID_Company
             inner join tCompany company
                     on ps.ID_Company = company.ID
      WHERE  ( ISNULL(FilePath, '') <> '' )

      --delete FROm @ImagePaths      where  ImageFilePath NOT IN ( 'Screenshot 2022-04-06 170938_20220406171052.jpg' )  
      RETURN;
  END 
GO
