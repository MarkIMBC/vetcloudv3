﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTeacher] AS   
				SELECT   
				 H.*  
				FROM vTeacher H  
				WHERE ISNULL(IsActive, 0) = 0
GO
