﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vForBilling_ListView_temp]
AS
  SELECT confinement.ID,
         confinement.Code RefNo,
         confinement.ID   ID_CurrentObject,
         m.Oid            Oid_Model,
         m.Name           Name_Model,
         confinement.Date,
         confinement.ID_Company,
         confinement.ID_Client,
         confinement.ID_Patient,
         confinement.BillingInvoice_ID_FilingStatus,
         confinement.Name_Client,
         confinement.Name_Patient,
         confinement.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Confinement confinement,
         _tModel m
  WHERE  m.TableName = 'tPatient_Confinement'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT soap.ID,
         soap.Code RefNo,
         soap.ID   ID_CurrentObject,
         m.Oid     Oid_Model,
         m.Name    Name_Model,
         soap. Date,
         soap.ID_Company,
         soap.ID_Client,
         soap.ID_Patient,
         soap.BillingInvoice_ID_FilingStatus,
         soap.Name_Client,
         soap.Name_Patient,
         soap.BillingInvoice_Name_FilingStatus
  FROM   vPatient_SOAP soap,
         _tModel m
  WHERE  m.TableName = 'tPatient_SOAP'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
         AND Isnull(soap.ID_Patient_Confinement, '') = 0
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Wellness hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Wellness'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Grooming hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Grooming'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 ) 
GO
