﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetSendPatientBirthDate](@BirthDate        VARCHAR(MAX),
                                                @IsSMSSent        Bit = NULL,
                                                @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  ID_Client            INT,
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  ID_Patient           INT,
  Name_Patient         VARCHAR(MAX),
  DateBirth            DateTime,
  Age                  VARCHAR(MAX),
  Message              VARCHAR(MAX))
as
  BEGIN
      DECLARE @IDs_Company typIntList
      DECLARE @IDs_AlreadySentPatient typIntList

      INSERT @IDs_AlreadySentPatient
      SELECT DISTINCT ID_Patient
      FROm   dbo.tPatient_BirthDateSMSGreetingLog _log
             inner join tPatient patient
                     on _log.ID_Patient = patient.ID
      WHERE  FORMAT(patient.DateBirth, 'MM-dd') = @BirthDate
             AND ISNULL(iTextMo_Status, 0) IN ( 0 )

      if( LEN(TRIM(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            Select Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting
            WHERE  ISNULL(IsActive, 0) = 1
        END

      INSERT @table
      select company.ID,
             company.Name,
             client.ID                                                                                                                                                                                                                                                 ID_Client,
             client.Name                                                                                                                                                                                                                                               Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                                                        ContactNumber_Client,
             patient.ID                                                                                                                                                                                                                                                ID_Patient,
             patient.Name                                                                                                                                                                                                                                              Name_Patient,
             DateBirth,
             dbo.fGetAge(DateBirth, GETDATE(), '')                                                                                                                                                                                                                     Age,
             dbo.[fGetPatientBirthDayGreetings](company.Name, 'Good day, Today is /*DateBirth*/. Happy Birthday to your pet /*Pet*/ from /*CompanyName*/.', client.Name, dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2), patient.Name, DateBirth) Message
      From   vPatient patient
             INNER JOIN tClient client
                     on patient.ID_Client = client.ID
             INNER JOIN @IDs_Company idsCompany
                     on patient.ID_Company = idsCompany.ID
             INNER JOIN tCompany company
                     on company.ID = patient.ID_Company
      where  patient.IsActive = 1
             and ISNULL(IsDeceased, 0) = 0
             AND FORMAT(patient.DateBirth, 'MM-dd') = @BirthDate
             AND patient.ID NOT IN (SELECT ID
                                    FROM   @IDs_AlreadySentPatient)

      RETURN
  END

GO
