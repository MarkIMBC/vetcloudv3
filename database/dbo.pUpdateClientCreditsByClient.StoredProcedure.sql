﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateClientCreditsByClient](@IDs_Client typIntList ReadOnly)
AS
  BEGIN
      DECLARE @ClientTotalCreditAmounts TABLE
        (
           ID_Client           INT,
           CurrentCreditAmount DECIMAL(18, 2)
        )

      INSERT @ClientTotalCreditAmounts
      SELECT idsClient.ID,
             ISNULL(SUM(CreditAmount), 0) TotalCreditAmount
      FROm   @IDs_Client idsClient
             LEFT join tClient_CreditLogs _logs
                    on _logs.ID_Client = idsClient.ID
      GROUP  BY idsClient.ID

      UPDATE tClient
      SET    CurrentCreditAmount = ISNULL(tbl.CurrentCreditAmount, 0)
      FROm   tClient client
             Inner JOIN @ClientTotalCreditAmounts tbl
                     ON client.ID = tbl.ID_Client
  END

GO
