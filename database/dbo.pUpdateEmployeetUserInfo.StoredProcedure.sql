﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 PROC [dbo].[pUpdateEmployeetUserInfo](@EmployeeID     INT,
                                    @Username       VARCHAR(MAX),
                                    @Password       VARCHAR(MAX),
                                    @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      Update vUser
      SET    Username = @UserName,
             Password = @Password
      where  ID_Employee = @EmployeeID
             AND ID_Company = @ID_Company

      SELECT '_'

      SELECT @Success Success,
             @message Message
  END

GO
