﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetModelDefaultValueString] (@tableName    VARCHAR(MAX),
                                                @ID_Company   INT,
                                                @PropertyName VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = NULL
      DECLARE @Oid_Model VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      where  TableName = @tableName

      SELECT @result = [Value]
      FROM   tModelDefaultValue
      where  Oid_Model = @Oid_Model
             and ID_Company = @ID_Company
             AND PropertyName = @PropertyName

      RETURN @result
  END

GO
