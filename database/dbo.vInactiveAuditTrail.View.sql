﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAuditTrail] AS   
				SELECT   
				 H.*  
				FROM vAuditTrail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
