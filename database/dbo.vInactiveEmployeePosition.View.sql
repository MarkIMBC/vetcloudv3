﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEmployeePosition] AS   
				SELECT   
				 H.*  
				FROM vEmployeePosition H  
				WHERE ISNULL(IsActive, 0) = 0
GO
