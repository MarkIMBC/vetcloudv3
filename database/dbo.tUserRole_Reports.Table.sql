﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserRole_Reports](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_UserRole] [int] NULL,
	[ID_Report] [varchar](300) NULL,
 CONSTRAINT [PK_tUserRole_Reports] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tUserRole_Reports] ON [dbo].[tUserRole_Reports]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUserRole_Reports] ADD  CONSTRAINT [DF__tUserRole__IsAct__297722B6]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUserRole_Reports]  WITH CHECK ADD  CONSTRAINT [FKtUserRole_Reports_ID_UserRole] FOREIGN KEY([ID_UserRole])
REFERENCES [dbo].[tUserRole] ([ID])
GO
ALTER TABLE [dbo].[tUserRole_Reports] CHECK CONSTRAINT [FKtUserRole_Reports_ID_UserRole]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUserRole_Reports]
		ON [dbo].[tUserRole_Reports]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tUserRole_Reports] ENABLE TRIGGER [rDateCreated_tUserRole_Reports]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUserRole_Reports]
		ON [dbo].[tUserRole_Reports]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tUserRole_Reports] ENABLE TRIGGER [rDateModified_tUserRole_Reports]
GO
