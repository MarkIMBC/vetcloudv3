﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePurchaseOrder_Detail] AS   
				SELECT   
				 H.*  
				FROM vPurchaseOrder_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
