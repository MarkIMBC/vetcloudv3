﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 VIEW [dbo].[vLastestPatientWaitingList]
as
  SELECT waiting.*
  FROM   vPatientWaitingList waiting
         INNER JOIN (SELECT MAX(ID) Max_ID_PatientWaitingList,
                            ID_Patient
                     FROM   vPatientWaitingList
                     WHERE  CONVERT(Date, DateCreated) = CONVERT(Date, GETDATE())
                     GROUP  BY ID_Patient) tbl
                 on tbl.Max_ID_PatientWaitingList = waiting.ID

GO
