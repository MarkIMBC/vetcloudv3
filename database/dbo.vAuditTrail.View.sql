﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vAuditTrail]
AS
SELECT  AD.*,
		ISNULL(M.DisplayName,M.Name) AS Model,
		U.Name AS [User],
		ADT.Name AS AuditType
FROM    dbo.tAuditTrail AD
        LEFT JOIN dbo.[_tModel] M ON AD.ID_Model = M.Oid
		LEFT JOIN dbo.tUser U ON AD.ID_User = U.ID
		LEFT JOIN dbo.tAuditTrailType ADT ON AD.ID_AuditType = ADT.ID
GO
