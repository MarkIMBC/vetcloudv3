﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vCompanyActiveUserAdministrator]
AS
  select _user.*
  FROM   vCompanyActiveUser _user
         inner join vUserAdministrator _useradmin
                 on _user.ID_User = _useradmin.ID_User
  WHERE  IsAdministrator = 1

GO
