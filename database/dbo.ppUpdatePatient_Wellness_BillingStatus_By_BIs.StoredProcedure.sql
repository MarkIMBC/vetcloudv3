﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE   PROC [dbo].[ppUpdatePatient_Wellness_BillingStatus_By_BIs](@IDs_BillingInvoice typIntList READONLY)
  AS
  BEGIN

	DECLARE @IDs_Patient_Wellness typIntList

	INSERT @IDs_Patient_Wellness
	SELECT bi.ID_Patient_Wellness FROM tBillingInvoice bi Inner join @IDs_BillingInvoice ids on bi.ID = ids.ID
	where ISNULL(bi.ID_Patient_Wellness, 0) > 0

	exec pUpdatePatient_Wellness_BillingStatus @IDs_Patient_Wellness
  END

GO
