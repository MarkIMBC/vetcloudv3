﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Vaccination] AS   
				SELECT   
				 H.*  
				FROM vPatient_Vaccination H  
				WHERE ISNULL(IsActive, 0) = 0
GO
