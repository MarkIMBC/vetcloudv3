﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelClientWithdraw] (@IDs_ClientWithdraw typIntList READONLY,
                                          @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelClientWithdraw_validation
            @IDs_ClientWithdraw,
            @ID_UserSession;

          UPDATE dbo.tClientWithdraw
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientWithdraw bi
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON bi.ID = ids.ID;

          -- Subtract Withdraw on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.WithdrawAmount,
                 Code,
                 'Cancel Withdraw'
          FROM   tClientWithdraw cd
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
