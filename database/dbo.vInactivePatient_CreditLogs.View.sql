﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_CreditLogs] AS   
				SELECT   
				 H.*  
				FROM vPatient_CreditLogs H  
				WHERE ISNULL(IsActive, 0) = 0
GO
