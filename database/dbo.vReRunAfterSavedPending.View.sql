﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE   VIEW [dbo].[vReRunAfterSavedPending]  
as  
select 'Patient_SOAP' ModuleName,  
                 DB_NAME()        DatabaseName,  
                 ID               ID_CurrentObject,  
                 DateCreated,  
                 ID_FilingStatus,  
                 DateRunAfterSavedProcess,  
                 RunAfterSavedProcess_ID_FilingStatus  
fRom   tPatient_SOAP WITH (NOLOCK)  
where  DateRunAfterSavedProcess IS NULL  
       AND ID_FilingStatus NOT IN ( 4 )  
       AND ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) = 0  
    AND CONVERT(Date, DateCreated) BETWEEN '2023-05-01' AND CONVERT(Date, GETDATE())  
UNION ALL  
select 'BillingInvoice' ModuleName,  
                 DB_NAME()        DatabaseName,  
                 ID               ID_CurrentObject,  
                 DateCreated,  
                 ID_FilingStatus,  
                 DateRunAfterSavedProcess,  
                 RunAfterSavedProcess_ID_FilingStatus  
fRom   tBillingInvoice WITH (NOLOCK)  
where  DateRunAfterSavedProcess IS NULL  
       AND ID_FilingStatus NOT IN ( 4 )  
       AND ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) = 0  
    AND CONVERT(Date, DateCreated) BETWEEN '2023-05-01' AND CONVERT(Date, GETDATE()) 
GO
