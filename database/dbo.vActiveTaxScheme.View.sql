﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTaxScheme] AS   
				SELECT   
				 H.*  
				FROM vTaxScheme H  
				WHERE IsActive = 1
GO
