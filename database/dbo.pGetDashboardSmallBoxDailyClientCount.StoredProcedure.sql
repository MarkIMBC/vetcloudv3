﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardSmallBoxDailyClientCount] (@ID_UserSession INT,
                                                      @Date           Date)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT
      DECLARE @TotalCount INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession WITH (NOLOCK)
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser WITH (NOLOCK)
      WHERE  ID = @ID_User

      /* @@PaymentDailyAmount */
      SELECT @TotalCount = SUM(IsNull(CASE
                                        WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount
                                        ELSE phed.PaymentAmount
                                      END, 0))
      FROM   tPaymentTransaction phed WITH (NOLOCK)
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) = @Date

      SELECT @TotalCount = COUNT(*)
      FROM   tClient WITH (NOLOCK)
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) = @Date

      SET @TotalCount = ISNULL(@TotalCount, 0)

      DECLARE @subtitle VARCHAR(MAX) = '';

      IF( @Date <> CONVERT(Date, GETDATE()) )
        set @subtitle = ' as of ' + FORMAT(@Date, 'MM/dd/yyyy')

      SELECT '_'

      SELECT @TotalCount                 TotalCount,
             FORMAT(@TotalCount, '#,#0') FormattedTotalCount,
             'Daily Client' + @subtitle  Subtitle
  END

GO
