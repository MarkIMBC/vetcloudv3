﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdateBillingInvoicePayment] (@IDs_BillingInvoice typIntList READONLY)  
AS  
  BEGIN  
      DECLARE @FILINGSTATUS_PENDING INT = 2;  
      DECLARE @FILINGSTATUS_APPROVED INT = 3;  
      DECLARE @FILINGSTATUS_PARTIALLYPAID INT = 11;  
      DECLARE @FILINGSTATUS_FULLYPAID INT = 12;  
  
      exec dbo.pUpdatePaymentTransaction_RemainingAmount_By_BIs  
        @IDs_BillingInvoice  
  
      /*Update SOAP Bill Status*/  
      --declare @IDs_Patient_SOAP typIntList  
  
      --INSERT @IDs_Patient_SOAP  
      --SELECT ID_Patient_SOAP  
      --FROM   tBillingInvoice bi  
      --       INNER JOIN @IDs_BillingInvoice ids  
      --               ON bi.ID = ids.ID  
  
      --exec pUpdatePatient_SOAP_BillingStatus  
      --  @IDs_Patient_SOAP  
  END;   


GO
