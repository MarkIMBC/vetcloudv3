﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VET_CLOUD_SERVICES_SUMMARY](
	[Clinic] [varchar](500) NULL,
	[ID] [varchar](500) NULL,
	[Name] [varchar](500) NULL,
	[Buying Price] [decimal](18, 2) NULL,
	[Selling Price] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
