﻿CREATE TYPE [dbo].[typImportItem] AS TABLE(
	[product_id] [int] NULL,
	[generic_name] [varchar](max) NULL,
	[brand_name] [varchar](max) NULL,
	[packaging_dosage_form] [varchar](max) NULL,
	[category] [varchar](max) NULL,
	[price] [decimal](18, 2) NULL,
	[selling_price] [decimal](18, 2) NULL
)
GO
