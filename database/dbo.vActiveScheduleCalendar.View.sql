﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveScheduleCalendar] AS   
    SELECT   
     H.*  
    FROM vScheduleCalendar H  
    WHERE IsActive = 1
GO
