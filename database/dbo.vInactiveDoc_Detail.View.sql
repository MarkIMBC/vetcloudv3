﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDoc_Detail] AS   
				SELECT   
				 H.*  
				FROM vDoc_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
