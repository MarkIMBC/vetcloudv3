﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetListView]
    @Oid UNIQUEIDENTIFIER
AS
    SELECT  '' AS [_] 

    SELECT  H.Oid ,
            H.Name ,
            H.ID_Model ,
            H.DataSource  AS [$DataSource],
            H.Comment ,
            M.ID_DetailView ,
            ISNULL(H.Caption, ISNULL(M.Caption, M.Name)) AS Caption,
			M.PrimaryKey,
			ISNULL(M.Icon,'fa fa-cube') AS [Icon],
			M.Name AS ModelName,
			M.ControllerPath AS ModelControllerPath,
			H.JsController ,
			ISNULL(M.IsEnableAuditTrail,0) AS IsEnableAuditTrail,
			ISNULL(M.IsEnableComment,0) AS IsEnableComment,
			ISNULL(M.IsReadOnly,0) AS IsReadOnly,
			ISNULL(M.IsEnableFileAttachment,0) AS IsEnableFileAttachment
    FROM    dbo.[_tListView] H
            LEFT JOIN dbo.[_vModel] M ON H.ID_Model = M.Oid
    WHERE   H.Oid = @Oid
GO
