﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveReceivingReport_Detail] AS   
				SELECT   
				 H.*  
				FROM vReceivingReport_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
