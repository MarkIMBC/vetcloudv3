﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Wellness](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[AttendingPhysician] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[ID_Patient_SOAP] [int] NULL,
	[ID_Patient_Vaccination] [int] NULL,
	[old_schedule_id] [int] NULL,
	[old_appointment_id] [int] NULL,
	[old_return_patient_id] [int] NULL,
	[Weight] [varchar](300) NULL,
	[Temperature] [varchar](300) NULL,
	[BillingInvoice_ID_FilingStatus] [int] NULL,
	[IsForBilling] [bit] NULL,
 CONSTRAINT [PK_tPatient_Wellness] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Wellness] ON [dbo].[tPatient_Wellness]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Wellness] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Wellness]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness] CHECK CONSTRAINT [FK_tPatient_Wellness_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Wellness]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness] CHECK CONSTRAINT [FK_tPatient_Wellness_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Wellness]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness] CHECK CONSTRAINT [FK_tPatient_Wellness_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Wellness]
		ON [dbo].[tPatient_Wellness]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Wellness hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness] ENABLE TRIGGER [rDateCreated_tPatient_Wellness]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Wellness]
		ON [dbo].[tPatient_Wellness]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Wellness hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness] ENABLE TRIGGER [rDateModified_tPatient_Wellness]
GO
