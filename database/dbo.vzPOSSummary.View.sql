﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE         
 VIEW [dbo].[vzPOSSummary]
AS
  SELECT biDetail.ID_Item,
         biDetail.Name_Item,
         biDetail.UnitPrice,
         biDetail.Quantity * biDetail.UnitPrice GrossAmount,
         biDetail.DiscountAmount                DiscountAmountDetail,
         biDetail.Amount,
         biDetail.Quantity,
         biDetail.ID_BillingInvoice,
         bi.Date,
         Isnull(bi.DiscountAmount, 0)           DiscountAmount,
         bi.ConfinementDepositAmount,
         bi.Date                                Date1,
         item.ID_ItemType,
         item.Name_ItemType,
         bi.ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                           Name_Company,
         company.Address                        Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                  HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee
  FROM   dbo.vBillingInvoice_Detail biDetail
         LEFT JOIN dbo.vBillingInvoice bi
                ON bi.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vItem item
                ON item.ID = biDetail.ID_Item
         LEFT JOIN dbo.vCompany company
                ON company.ID = bi.ID_Company
  WHERE  bi.Payment_ID_FilingStatus IN ( 2, 11, 12, 13 )
         AND bi.ID_FilingStatus = 3
         AND Name_Item IS NOT NULL;

GO
