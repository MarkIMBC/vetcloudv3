﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoClientLogin](@Username VARCHAR(300),
                          @Password VARCHAR(300))
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0
    DECLARE @ID_UserSession INT = 0

    BEGIN TRY
        EXEC pDoClientLogin_validation
          @Username,
          @Password

        SELECT @ID_User = ID
        FROM   tUser
        WHERE  username = @Username
               AND Password = @Password
               AND IsActive = 1

        INSERT INTO [dbo].[tUserSession]
                    ([Code],
                     [Name],
                     [IsActive],
                     [Comment],
                     [ID_User],
                     [ID_Warehouse],
                     [DateCreated])
        VALUES      (NULL,
                     NULL,
                     1,
                     '',
                     @ID_User,
                     1,
                     GetDate())

        SET @ID_UserSession = @@IDENTITY
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_',
           '' CurrentUser;

    SELECT @Success Success,
           @message message;

    SELECT _user.*,
           client.Name,
           @ID_UserSession ID_UserSession,
           client.ID       ID_User_Reference,
           model.Oid       ID_Model_Reference,
           company.dbusername,
           company.dbpassword,
           company.Code    Code_Company
    FROM   vClientUser _user
           INNER JOIN tClient client
                   ON _user.ID = client.ID_User
           INNER JOIN tCompany company
                   ON client.ID_Company = company.ID
           INNER JOIN _tModel model
                   ON model.Name = 'Client'
    WHERE  _user.ID = @ID_User

GO
