﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE     
 VIEW [dbo].[vBillingInvoicePaymentStatus]
AS
  SELECT ID,
         Name
  FROM   tFilingStatus
  WHERE  ID IN ( 2, 11, 12, 13 ) 

GO
