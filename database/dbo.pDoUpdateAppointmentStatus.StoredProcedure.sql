﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoUpdateAppointmentStatus](@Oid_Model                    VARCHAR(MAX),
                                      @Appointment_ID_CurrentObject INT,
                                      @Appointment_ID_FilingStatus  INT,
                                      @DateReschedule               DateTime,
                                      @ID_UserSession               INT,
                                      @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_User INT = 0;

      BEGIN TRY
          EXEC pUpdateAppointmentStatus
            @Oid_Model,
            @Appointment_ID_CurrentObject,
            @Appointment_ID_FilingStatus,
            @DateReschedule,
            @ID_UserSession,
            @Remarks
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO
