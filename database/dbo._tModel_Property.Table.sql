﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tModel_Property](
	[Oid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Caption] [varchar](200) NULL,
	[ID_Model] [uniqueidentifier] NOT NULL,
	[ID_PropertyType] [int] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_PropertyModel] [uniqueidentifier] NULL,
	[ID_ModelProperty_Key] [uniqueidentifier] NULL,
	[DefaultValue] [varchar](300) NULL,
	[IsAggregated] [bit] NULL,
	[DisplayProperty] [varchar](300) NULL,
 CONSTRAINT [PK__tModel_Property] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tModel_Property]  WITH NOCHECK ADD  CONSTRAINT [FK__tModel_Property__tModel] FOREIGN KEY([ID_Model])
REFERENCES [dbo].[_tModel] ([Oid])
GO
ALTER TABLE [dbo].[_tModel_Property] CHECK CONSTRAINT [FK__tModel_Property__tModel]
GO
ALTER TABLE [dbo].[_tModel_Property]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_Property_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tModel_Property] CHECK CONSTRAINT [FK_tModel_Property_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tModel_Property]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_Property_ID_ModelProperty_Key] FOREIGN KEY([ID_ModelProperty_Key])
REFERENCES [dbo].[_tModel_Property] ([Oid])
GO
ALTER TABLE [dbo].[_tModel_Property] CHECK CONSTRAINT [FK_tModel_Property_ID_ModelProperty_Key]
GO
ALTER TABLE [dbo].[_tModel_Property]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_Property_ID_PropertyModel] FOREIGN KEY([ID_PropertyModel])
REFERENCES [dbo].[_tModel] ([Oid])
GO
ALTER TABLE [dbo].[_tModel_Property] NOCHECK CONSTRAINT [FK_tModel_Property_ID_PropertyModel]
GO
