﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pUpdatePatientGroomingBillStatus](@IDs_Patient_Grooming           TYPINTLIST READONLY,
                                            @BillingInvoice_ID_FilingStatus INT,
                                            @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @isForBilling BIT = 0;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT
    DECLARE @ForBilling_BillingInvoice_ID_FilingStatus INT = 16

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    BEGIN TRY
        IF( @isForBilling = @ForBilling_BillingInvoice_ID_FilingStatus )
          BEGIN
              SET @isForBilling = 1
          END

        UPDATE tPatient_Grooming
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus,
               IsForBilling = @isForBilling
        FROM   tPatient_Grooming _hed
               INNER JOIN @IDs_Patient_Grooming ids
                       ON _hed.ID = ids.ID;
    END TRY
    BEGIN CATCH
        SET @message = Error_message();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO
