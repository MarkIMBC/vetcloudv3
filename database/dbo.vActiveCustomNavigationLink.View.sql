﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCustomNavigationLink] AS   
				SELECT   
				 H.*  
				FROM vCustomNavigationLink H  
				WHERE IsActive = 1
GO
