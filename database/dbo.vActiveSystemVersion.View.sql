﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSystemVersion] AS   
				SELECT   
				 H.*  
				FROM vSystemVersion H  
				WHERE IsActive = 1
GO
