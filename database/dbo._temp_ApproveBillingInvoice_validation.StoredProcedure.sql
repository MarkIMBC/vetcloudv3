﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  
 PROC [dbo].[_temp_ApproveBillingInvoice_validation] (@IDs_BillingInvoice typIntList READONLY,
                                                      @ID_UserSession     INT)
AS
  BEGIN
DECLARE @Filed_ID_FilingStatus INT = 1;
DECLARE @Discharged_ID_FilingStatus INT = 15;
DECLARE @message VARCHAR(MAX) = '';
/*---------------------------------------------------------------------------------------------------------------------*/
DECLARE @ValidateNotFiled TABLE
  (
     Code              VARCHAR(30),
     Name_FilingStatus VARCHAR(30)
  );
DECLARE @Count_ValidateNotFiled INT = 0;
/*---------------------------------------------------------------------------------------------------------------------*/
/* Validate Remaining Inventory */
DECLARE @Inventoriable_ID_ItemType INT = 2;
DECLARE @ValidateItemInventory TABLE
  (
     ID_Item  INT,
     ItemName VARCHAR(MAX),
     BIQty    INT,
     RemQty   INT
  )
Declare @Count_ValidateItemInventory INT = 0

INSERT @ValidateItemInventory
       (ID_Item,
        ItemName,
        BIQty,
        RemQty)
SELECT biDetail.ID_Item,
       item.Name,
       SUM(biDetail.Quantity),
       0
FROM   dbo.tBillingInvoice_Detail biDetail
       INNER JOIN dbo.tBillingInvoice biHed
               ON biDetail.ID_BillingInvoice = biHed.ID
       INNER JOIN dbo.tItem item
               ON item.ID = biDetail.ID_Item
       INNER JOIN @IDs_BillingInvoice ids
               ON ids.ID = biHed.ID
WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType
GROUP  BY biDetail.ID_Item,
          item.Name

SELECT *
FROm   @ValidateItemInventory

UPDATE @ValidateItemInventory
SET    RemQty = ISNULL(tbl.TotalRemQty, 0)
FROM   @ValidateItemInventory invtItem
       INNER JOIN (SELECT ID_Item,
                          SUM(Quantity) TotalRemQty
                   FROM   tInventoryTrail
                   GROUP  BY ID_Item) tbl
               ON invtItem.ID_Item = tbl.ID_Item

Update @ValidateItemInventory
SET    RemQty = 0,
       BIQty = 3

DELETE FROM @ValidateItemInventory
WHERE  ( ISNULL(RemQty, 0) - ISNULL(BIQty, 0) ) >= 0

SELECT @Count_ValidateItemInventory = COUNT(*)
FROM   @ValidateItemInventory

IF ( @Count_ValidateItemInventory > 0 )
  BEGIN
      SET @message = 'The following item'
                     + CASE
                         WHEN ISNULL(@Count_ValidateItemInventory, 0) > 1 THEN 's are '
                         ELSE ' is '
                       END
                     + 'insufficient inventory count:';

      SET @message =  @message + 'asdasda'

	  Update @ValidateItemInventory set ItemName = REPLACE(ItemName, '%', '‰')

      SELECT TOP 1 @message = @message + CHAR(10) + ItemName
                        + ': Rem. Qty - '
                        + CONVERT(varchar(MAX), ISNULL(RemQty, 0))
                        + ' Bi Qty - '
                        + CONVERT(varchar(MAX), ISNULL(BIQty, 0))
      FROM   @ValidateItemInventory


      SELECT  @message;

      THROW 50008, @message, 1;
  END;

/*---------------------------------------------------------------------------------------------------------------------*/
/* Validate Has No TotalAmount */
DECLARE @ValidateHasNoTotalAmount TABLE
  (
     Code VARCHAR(30)
  );
DECLARE @Count_ValidateHasNoTotalAmount INT = 0;

INSERT @ValidateHasNoTotalAmount
       (Code)
SELECT Code
FROM   dbo.vBillingInvoice bi
       INNER JOIN @IDs_BillingInvoice ids
               ON bi.ID = ids.ID
WHERE  bi.TotalAmount IS NULL
        OR bi.NetAmount IS NULL

SELECT @Count_ValidateHasNoTotalAmount = COUNT(*)
FROM   @ValidateHasNoTotalAmount;

IF ( @Count_ValidateHasNoTotalAmount > 0 )
  BEGIN
      SET @message = 'The following record'
                     + CASE
                         WHEN @Count_ValidateHasNoTotalAmount > 1 THEN 's have'
                         ELSE ' has '
                       END
                     + 'incorrect computation. Please check and try again';

      SELECT @message = @message + CHAR(10) + Code
      FROM   @ValidateHasNoTotalAmount;

      THROW 50001, @message, 1;
  END;

/*---------------------------------------------------------------------------------------------------------------------*/
--/* Validate if the confinement deposit amount is OUTDATED*/    
--DECLARE @Count_validateOutDatedConfinemntDepositAmount INT = 0;    
--DECLARE @validateOutDatedConfinemntDepositAmount TABLE    
--  (    
--     Code Varchar(100)    
--  )    
--INSERT @validateOutDatedConfinemntDepositAmount    
--SELECT bi.Code    
--FROM   dbo.vBillingInvoice bi    
--       INNER JOIN @IDs_BillingInvoice ids    
--               ON bi.ID = ids.ID    
--       INNER JOIN tClient client    
--               on bi.ID_Client = client.ID    
--WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0    
--       AND ISNULL(ConfinementDepositAmount, 0) <> ISNULL(client.CurrentCreditAmount, 0)    
--SELECT @Count_validateOutDatedConfinemntDepositAmount = COUNT(*)    
--FROM   @validateOutDatedConfinemntDepositAmount;    
--IF ( @Count_validateOutDatedConfinemntDepositAmount > 0 )    
--  BEGIN    
--      SET @message = 'The following record'    
--                     + CASE    
--                         WHEN @Count_validateOutDatedConfinemntDepositAmount > 1 THEN 's have'    
--                         ELSE ' has '    
--                       END    
--                     + 'updated Deposit Credit. Please click refresh and save and try to click Approve Button.';    
--      SELECT @message = @message + CHAR(10) + Code    
--      FROM   @validateOutDatedConfinemntDepositAmount;    
--      THROW 50001, @message, 1;    
--  END;    
/*---------------------------------------------------------------------------------------------------------------------*/
DECLARE @ValidateNotDischarge TABLE
  (
     Code_Patient_Confinement              VARCHAR(30),
     Name_FilingStatus_Patient_Confinement VARCHAR(30)
  );
DECLARE @Count_ValidateNotDischarge INT = 0;

/* Validate Confinement is not Discharge*/
INSERT @ValidateNotDischarge
       (Code_Patient_Confinement,
        Name_FilingStatus_Patient_Confinement)
SELECT confinement.Code,
       confinement.Name_FilingStatus
FROM   dbo.vBillingInvoice bi
       INNER JOIN @IDs_BillingInvoice ids
               ON bi.ID = ids.ID
       INNER JOIN vPatient_Confinement confinement
               on confinement.ID = bi.ID_Patient_Confinement
WHERE  confinement.ID_FilingStatus NOT IN ( @Discharged_ID_FilingStatus );

SELECT @Count_ValidateNotDischarge = COUNT(*)
FROM   @ValidateNotDischarge;

IF ( @Count_ValidateNotDischarge > 0 )
  BEGIN
      SET @message = 'The following record'
                     + CASE
                         WHEN @Count_ValidateNotDischarge > 1 THEN 's are'
                         ELSE ' is '
                       END
                     + ' required to discharge first:';

      SELECT @message = @message + CHAR(10) + Code_Patient_Confinement
                        + ' - '
                        + Name_FilingStatus_Patient_Confinement
      FROM   @ValidateNotDischarge;

      THROW 50001, @message, 1;
  END;

END;
GO
