﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveToothStatusType] AS   
				SELECT   
				 H.*  
				FROM vToothStatusType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
