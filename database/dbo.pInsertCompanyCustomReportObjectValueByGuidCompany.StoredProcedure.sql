﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertCompanyCustomReportObjectValueByGuidCompany](@GUID_Company VARCHAR(MAX),
                                                              @Name_Report  VARCHAR(MAX),
                                                              @ObjectName   VARCHAR(MAX),
                                                              @PropertyName VARCHAR(MAX),
                                                              @Value        VARCHAR(MAX))
as
  BEGIN
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company
             AND IsActive = 1

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @Name_Report

      EXEC dbo.pInsertCompanyCustomReportObjectValue
        @ID_Company,
        @Oid_Report,
        @ObjectName,
        @PropertyName,
        @Value
  END

GO
