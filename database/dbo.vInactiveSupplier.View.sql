﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSupplier] AS   
				SELECT   
				 H.*  
				FROM vSupplier H  
				WHERE ISNULL(IsActive, 0) = 0
GO
