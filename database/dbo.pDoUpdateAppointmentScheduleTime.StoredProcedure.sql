﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pDoUpdateAppointmentScheduleTime](@Oid_Model                    VARCHAR(MAX),
                                            @Appointment_ID_CurrentObject INT,
                                            @DateTime                     DateTime,
                                            @ID_UserSession               INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @TableName VARCHAR(MAX) = ''
          DECLARE @ID_Company INT = 0
          DECLARE @ID_User INT = 0
          DECLARE @Reschedule_ID_FilingStatus INT = 21
          DECLARE @ID_Patient INT

          SELECT @ID_User = ID_User
          FROM   tUserSession
          WHERE  ID = @ID_UserSession

          SELECT @ID_Company = ID_Company
          FROM   vUser
          WHERE  ID = @ID_User

          SELECT @TableName = TableName
          FROM   _tModel
          WHERE  Oid = @Oid_Model

          IF( @TableName = 'tPatientAppointment' )
            BEGIN
                UPDATE tPatientAppointment
                SET    DateStart = Convert(DateTime, FORMAT(DateStart, 'yyyy-MM-dd') + ' '
                                                     + FORMAT(@DateTime, 'HH:mm')),
                       DateEnd = Convert(DateTime, FORMAT(DateStart, 'yyyy-MM-dd') + ' '
                                                   + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
          ELSE IF( @TableName = 'tPatient_SOAP' )
            BEGIN
                UPDATE tPatient_SOAP_Plan
                SET    DateReturn = Convert(DateTime, FORMAT(DateReturn, 'yyyy-MM-dd') + ' '
                                                      + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
          ELSE IF( @TableName = 'tPatient_Wellness' )
            BEGIN
                UPDATE tPatient_Wellness_Schedule
                SET    Date = Convert(DateTime, FORMAT(Date, 'yyyy-MM-dd') + ' '
                                                + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO
