﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Lodging] AS   
				SELECT   
				 H.*  
				FROM vPatient_Lodging H  
				WHERE ISNULL(IsActive, 0) = 0
GO
