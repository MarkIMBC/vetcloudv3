﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUserRole_Detail] AS   
				SELECT   
				 H.*  
				FROM vUserRole_Detail H  
				WHERE IsActive = 1
GO
