﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveVeterinaryCertificate] AS   
				SELECT   
				 H.*  
				FROM vVeterinaryCertificate H  
				WHERE IsActive = 1
GO
