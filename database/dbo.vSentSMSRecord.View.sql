﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vSentSMSRecord]
AS
  SELECT soap.Code,
         CONVERT(DATE, DateSent)                     DateSent,
         CONVERT(DATE, DateReturn)                   DateSchedule,
         CONVERT(DATE, DateAdd(DAY, -1, DateReturn)) DateSending,
         ID_Company
  FROM   tPatient_SOAP_Plan soapPlan
         INNER JOIN tPatient_SOAP soap
                 ON soapPlan.ID_Patient_SOAP = soap.ID
  WHERE  DateSent IS NOT NULL
  UNION ALL
  SELECT vac.Code,
         CONVERT(DATE, DateSent)                        DateSent,
         vacSched.Date                                  DateSchedule,
         CONVERT(DATE, DateAdd(DAY, -1, vacSched.Date)) DateSending,
         vac.ID_Company
  FROM   tPatient_Vaccination_Schedule vacSched
         INNER JOIN tPatient_Vaccination vac
                 ON vacSched.ID_Patient_Vaccination = vac.ID
  WHERE  DateSent IS NOT NULL
  UNION ALL
  SELECT well.Code,
         CONVERT(DATE, DateSent)                         DateSent,
         wellSched.Date                                  DateSchedule,
         CONVERT(DATE, DateAdd(DAY, -1, wellSched.Date)) DateSending,
         well.ID_Company
  FROM   vPatient_Wellness_Schedule wellSched
         INNER JOIN tPatient_Wellness well
                 ON wellSched.ID_Patient_Wellness = well.ID
  WHERE  DateSent IS NOT NULL

GO
