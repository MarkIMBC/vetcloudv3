﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetPatientReferral] @ID         INT = -1,
                               @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS PatientReferral_ReferralType

      DECLARE @ID_Company   INT,
              @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROm   vUser
      where  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '--New--'   AS [Code],
                           NULL        AS [Name],
                           GETDATE()   AS [Date],
                           1           AS [IsActive],
                           @ID_Company AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           AS ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatientReferral H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatientReferral_ReferralType
      WHERE  ID_PatientReferral = @ID
  END

GO
