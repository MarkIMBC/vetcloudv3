﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pApprove_ClientAppointmentRequest] (@IDs_ClientAppointment typIntList READONLY,
                                                      @ID_UserSession        INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IsActive INT=1;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tClientAppointmentRequest
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 ID_ApprovedBy = @ID_User
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientAppointment)

          INSERT INTO [dbo].[tPatientAppointment]
                      ([Comment],
                       [IsActive],
                       [ID_Company],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [ID_Patient],
                       [DateStart],
                       [DateEnd],
                       [ID_Doctor],
                       [ID_SOAPType],
                       [ID_Client],
                       [ID_FilingStatus])
          SELECT req.Comment,
                 @IsActive,
                 req.ID_Company,
                 GETDATE(),
                 GETDATE(),
                 1,
                 1,
                 pat.ID_Patient,
                 req.DateStart,
                 req.DateEnd,
                 req.AttendingPhysician_ID_Employee,
                 req.ID_SOAPType,
                 req.ID_Client,
                 @Filed_ID_FilingStatus
          FROM   vClientAppointmentRequest req
                 INNER JOIN vClientAppointmentRequest_Patient pat
                         ON req.ID = pat.ID_ClientAppointmentRequest
          WHERE  req.ID IN (SELECT ID
                            FROM   @IDs_ClientAppointment)
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
