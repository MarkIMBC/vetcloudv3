﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoMergeClientRecord](@From_ID_Client INT,
                                @To_ID_Client   INT,
                                @ID_UserSession INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Client VARCHAR(MAX) = ''
      DECLARE @To_Code_Client VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Client

      SELECT @To_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Client

      --select @From_ID_Client,
      --       @From_Code_Client,
      --       @ID_Company
      --select @To_ID_Client,
      --       @To_Code_Client,
      --       @ID_Company
      BEGIN TRY
          exec pMergeClientRecordByCompanyID
            @ID_Company,
            @From_Code_Client,
            @To_Code_Client
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO
