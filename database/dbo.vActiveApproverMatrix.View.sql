﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveApproverMatrix] AS   
				SELECT   
				 H.*  
				FROM vApproverMatrix H  
				WHERE IsActive = 1
GO
