﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tAuditTrail_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[ID_AuditTrail] [int] NULL,
	[ID_CurrentObject] [varchar](50) NULL,
	[OldValue] [text] NULL,
	[NewValue] [text] NULL,
	[ModelProperty] [varchar](300) NULL,
	[ID_AuditTrailType] [int] NULL,
 CONSTRAINT [PK_tAuditTrail_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tAuditTrail_Detail] ADD  CONSTRAINT [DF_tAuditTrail_Detail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tAuditTrail_Detail]  WITH NOCHECK ADD  CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail] FOREIGN KEY([ID_AuditTrail])
REFERENCES [dbo].[tAuditTrail] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tAuditTrail_Detail] CHECK CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tAuditTrail_Detail]
		ON [dbo].[tAuditTrail_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tAuditTrail_Detail] ENABLE TRIGGER [rDateCreated_tAuditTrail_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tAuditTrail_Detail]
		ON [dbo].[tAuditTrail_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tAuditTrail_Detail] ENABLE TRIGGER [rDateModified_tAuditTrail_Detail]
GO
