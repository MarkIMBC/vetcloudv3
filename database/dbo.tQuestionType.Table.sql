﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tQuestionType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[SeqNo] [int] NOT NULL,
 CONSTRAINT [PK_tQuestionType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tQuestionType] ON [dbo].[tQuestionType]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tQuestionType] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tQuestionType]  WITH CHECK ADD  CONSTRAINT [FK_tQuestionType_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tQuestionType] CHECK CONSTRAINT [FK_tQuestionType_ID_Company]
GO
ALTER TABLE [dbo].[tQuestionType]  WITH CHECK ADD  CONSTRAINT [FK_tQuestionType_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tQuestionType] CHECK CONSTRAINT [FK_tQuestionType_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tQuestionType]  WITH CHECK ADD  CONSTRAINT [FK_tQuestionType_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tQuestionType] CHECK CONSTRAINT [FK_tQuestionType_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateCreated_tQuestionType] ON [dbo].[tQuestionType] FOR INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tQuestionType SET DateCreated = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tQuestionType] ENABLE TRIGGER [rDateCreated_tQuestionType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateModified_tQuestionType] ON [dbo].[tQuestionType] FOR UPDATE, INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tQuestionType SET DateModified = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tQuestionType] ENABLE TRIGGER [rDateModified_tQuestionType]
GO
