﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInventory]
AS
WITH CTE
AS (
   SELECT hed.ID_Item,
          item.Name Name_Item,
          SUM(hed.Quantity) Quantity,
          hed.ID_Company,
          dbo.fGetInventoryStatus(SUM(hed.Quantity), item.MinInventoryCount, item.MaxInventoryCount) ID_InventoryStatus
   FROM dbo.tInventoryTrail hed
       LEFT JOIN tItem item
           ON item.ID = hed.ID_Item
   GROUP BY hed.ID_Item,
            item.Name,
            hed.ID_Company,
            item.MinInventoryCount,
            item.MaxInventoryCount)
SELECT hed.*,
       status.Name Name_InventoryStatus,
       CASE
           WHEN hed.ID_InventoryStatus = 1 THEN
               'danger'
           ELSE
               CASE
                   WHEN hed.ID_InventoryStatus = 2 THEN
                       'warning'
                   ELSE
                       'success'
               END
       END colorStatus
FROM CTE hed
    LEFT JOIN dbo.tInventoryStatus status
        ON status.ID = hed.ID_InventoryStatus;
GO
