﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE VIEW [dbo].[vItemInventoriable]  
AS  
  SELECT H.*,  
         UC.Name                                               AS CreatedBy,  
         UM.Name                                               AS LastModifiedBy,  
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,  
         CASE  
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')  
           ELSE ''  
         END                                                   RemainingBeforeExpired,  
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays  
  FROM   dbo.tItem H  
         LEFT JOIN tUser UC  
                ON H.ID_CreatedBy = UC.ID  
         LEFT JOIN tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
  WHERE  H.ID_ItemType = 2 and h.IsActive = 1;  
  
GO
