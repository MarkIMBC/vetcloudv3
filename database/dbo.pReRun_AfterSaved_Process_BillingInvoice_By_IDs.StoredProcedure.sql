﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 proc [dbo].[pReRun_AfterSaved_Process_BillingInvoice_By_IDs](@IDs_CurrentObject TYPINTLIST ReadOnly)
AS
  BEGIN
      DECLARE @success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''

      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                DECLARE @TranName VARCHAR(MAX);
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

                SET @TranName = 'pReRun_AfterSaved_Process_BillingInvoice_By_IDs-'
                                + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID

                IF(SELECT COUNT(*)
                   FROM   @IDs_CurrentObject) > 0
                  BEGIN
                      DECLARE @IDs_BillingINvoice TYPINTLIST
                      DECLARE @IDs_Patient_Confinement TYPINTLIST

                      insert @IDs_BillingINvoice
                      select distinct ID
                      FROM   @IDs_CurrentObject

                      /*Update And remove Duplicate Inventory Trails*/
                      DECLARE @IDs_Item TYPINTLIST

                      INSERT @IDs_Item
                      SELECT DISTINCT biDetail.ID_Item
                      FROM   tBillingInvoice_Detail biDetail WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on biDetail.ID_BillingInvoice = ids.ID

                      exec pUpdateItemCurrentInventoryByIDsItems
                        @IDs_Item

                      /*Update Confinement Bill Status*/
                      INSERT @IDs_Patient_Confinement
                      SELECT DISTINCT ID_Patient_Confinement
                      FROM   tBillingInvoice bi WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      EXEC PupDatePatient_ConfineMen_BillIngStatus
                        @IDs_Patient_Confinement

                      /*Update SOAP Bill Status*/
                      DECLARE @IDs_Patient_SOAP TYPINTLIST

                      INSERT @IDs_Patient_SOAP
                      SELECT DISTINCT ID_Patient_SOAP
                      FROM   tBillingInvoice bi
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      exec dbo.pModel_AfterSaved_BillingInvoice_Computation
                        @IDs_BillingInvoice

                      EXEC pUpdatePatient_SOAP_BillingStatus
                        @IDs_Patient_SOAP

                      exec ppUpdatePatient_Wellness_BillingStatus_By_BIs
                        @IDs_CurrentObject

                      exec ppUpdatePatient_Grooming_BillingStatus_By_BIs
                        @IDs_CurrentObject

                      Update tBillingInvoice
                      SET    DateRunAfterSavedProcess = GETDATE(),
                             RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                      FROm   tBillingInvoice bi
                             INNER JOIN @IDs_CurrentObject ids
                                     ON bi.ID = ids.ID
                  END
            END TRY
            BEGIN CATCH
                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID
                SET @success = 0;
                SET @message = ERROR_MESSAGE();
            END CATCH

            SELECT @success Success,
                   @message Message
        END
  END

GO
