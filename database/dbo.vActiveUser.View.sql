﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUser] AS   
				SELECT   
				 H.*  
				FROM vUser H  
				WHERE IsActive = 1
GO
