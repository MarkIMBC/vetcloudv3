﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE 
 PROC [dbo].[pRemoveMulticateInventoryTrailByIDsItems](@IDs_Item              typIntList READONLY,
                                                    @IsShowQueryTestResult BIT = 0)
as
  BEGIN
      if OBJECT_ID('dbo.tInventoryTrail_DeletedRecord') is null
        BEGIN
            CREATE TABLE [dbo].[tInventoryTrail_DeletedRecord]
              (
                 [ID]                [int],
                 [Code]              [varchar](50) NULL,
                 [Name]              [varchar](200) NULL,
                 [IsActive]          [bit] NULL,
                 [ID_Company]        [int] NULL,
                 [Comment]           [varchar](max) NULL,
                 [DateCreated]       [datetime] NULL,
                 [DateModified]      [datetime] NULL,
                 [ID_CreatedBy]      [int] NULL,
                 [ID_LastModifiedBy] [int] NULL,
                 [ID_Item]           [int] NULL,
                 [Quantity]          [int] NULL,
                 [UnitPrice]         [decimal](18, 4) NULL,
                 [ID_FilingStatus]   [int] NULL,
                 [Date]              [datetime] NULL,
                 [DateExpired]       [datetime] NULL,
                 [BatchNo]           [int] NULL,
                 [tempID]            [varchar](300) NULL,
                 DateDeleted         [datetime] NULL,
                 LastDuplicateCount  INT,
                 Oid_Model_Reference [varchar](200) NULL,
                 ID_Reference        [int] NULL
              )
        END

      DECLARE @record TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           [Count]             [int] NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL
        )
      DECLARE @forDeleterecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           [Count]             [int] NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL
        )
      DECLARE @NotDeletedrecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           [Count]             [int] NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL
        )

      INSERT @record
      select c.ID,
             c.Name,
             MAX(invt.ID) MaxID,
             invt.Code,
             invt.ID_Item,
             invt.Name_Item,
             invt.Quantity,
             invt.UnitPrice,
             invt.Date,
             NULL,
             invt.Name_FilingStatus,
             COUNT(*)     Count,
             Oid_Model_Reference,
             ID_Reference
      FROm   vInventoryTrail invt
             Inner join tItem item
                     on invt.ID_Item = item.ID
             INNER join @IDs_Item ids
                     on item.ID = ids.ID
             inner join vCompanyActive c
                     on item.ID_Company = c.ID
      GROUP  BY c.ID,
                c.Name,
                invt.Code,
                invt.ID_Item,
                invt.Name_Item,
                invt.Quantity,
                invt.UnitPrice,
                invt.Date,
                invt.Name_FilingStatus,
                Oid_Model_Reference,
                ID_Reference
      HAVING COUNT(*) > 1

      --SELECT *  
      --FROM   @record  
      DECLARE @ItemWithBiDetail TABLE
        (
           ID_BIllingInvoice_Detail INT,
           Code_BillingInvoice      VARCHAR(MAX),
           ID_Company               INT,
           ID_Item                  Int,
           Quantity                 Int
        )
      DECLARE @BiWithMultipleItem TABLE
        (
           Code_BillingInvoice VARCHAR(MAX),
           ID_Company          INT,
           ID_Item             INT,
           Quantity            Int
        )

      INSERT @ItemWithBiDetail
      SELECT DISTINCT biDet.ID,
                      bi.Code,
                      bi.ID_Company,
                      biDet.ID_Item,
                      biDet.Quantity
      FROM   tBillingInvoice bi
             inner join tBillingInvoice_Detail biDet
                     on bi.ID = bidet.ID_BillingInvoice
             INNER JOIN @record record
                     on bi.Code = record.Code
                        and bi.ID_Company = record.ID_Company
                        and biDet.ID_Item = record.ID_Item
                        and biDet.Quantity = record.Quantity

      INSERT @BiWithMultipleItem
      SELECT Code_BillingInvoice,
             ID_Company,
             ID_Item,
             Quantity
      FROM   @ItemWithBiDetail
      GROUP  BY Code_BillingInvoice,
                ID_Company,
                ID_Item,
                Quantity
      HAVING COUNT(*) > 1

      INSERT @forDeleterecord
      SELECT *
      FROM   @record
      EXCEPT
      SELECT rec.*
      FROm   @record rec
             inner join @BiWithMultipleItem bi
                     on rec.Code = bi.Code_BillingInvoice
                        and rec.ID_Company = bi.ID_Company
                        and bi.ID_Item = rec.ID_Item
                        and bi.ID_Item = rec.ID_Item

      IF( @IsShowQueryTestResult = 1 )
        BEGIN
            INSERT @NotDeletedrecord
            SELECT *
            FROM   @record
            except
            SELECT rec.*
            FROm   @forDeleterecord rec

            SELECT *
            FROm   tInventoryTrail_DeletedRecord

            SELECT *
            FROm   @record
            Order  by Code

            SELEct 'Not For Deleted',
                   *
            FROm   @NotDeletedrecord

            SELEct 'For Deleted',
                   *
            FROm   @forDeleterecord
        END

      INSERT tInventoryTrail_DeletedRecord
             (ID,
              [Code],
              [Name],
              [IsActive],
              [ID_Company],
              [Comment],
              [DateCreated],
              [DateModified],
              [ID_CreatedBy],
              [ID_LastModifiedBy],
              [ID_Item],
              [Quantity],
              [UnitPrice],
              [ID_FilingStatus],
              [Date],
              [DateExpired],
              [BatchNo],
              [tempID],
              Oid_Model_Reference,
              ID_Reference,
              DateDeleted,
              LastDuplicateCount)
      SELECT invt.ID,
             invt.[Code],
             invt.[Name],
             invt.[IsActive],
             invt.[ID_Company],
             invt.[Comment],
             invt.[DateCreated],
             invt.[DateModified],
             invt.[ID_CreatedBy],
             invt.[ID_LastModifiedBy],
             invt.[ID_Item],
             invt.[Quantity],
             invt.[UnitPrice],
             invt.[ID_FilingStatus],
             invt.[Date],
             invt.[DateExpired],
             invt.[BatchNo],
             invt.[tempID],
             invt.Oid_Model_Reference,
             invt.ID_Reference,
             GETDATE(),
             rec.Count
      FROM   tInventoryTrail invt
             inner join @forDeleterecord rec
                     on invt.ID = rec.MaxID

      DELETE FROM tInventoryTrail
      WHERE  ID IN (SELECT MaxID
                    FROM   @forDeleterecord)
  END

GO
