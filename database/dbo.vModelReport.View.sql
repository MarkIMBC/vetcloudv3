﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[vModelReport] 
as
SELECT modelReport.ID,
       modelReport.ID_Company,
       company.Name     Name_Company,
       Oid_Model,
       model.Name       Name_Model,
       Oid_Report,
       report.Name      Name_Report,
       modelReport.Name Name_ModelReport
FROm   _tModelReport modelReport
       LEFT JOIN _tModel model
              ON modelReport.Oid_Model = model.Oid
       LEFT JOIN _tReport report
              ON modelReport.Oid_Report = report.Oid
       LEFT JOIN tCompany company
              ON modelReport.ID_Company = company.ID 
GO
