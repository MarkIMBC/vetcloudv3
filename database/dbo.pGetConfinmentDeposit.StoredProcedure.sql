﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetConfinmentDeposit](@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @IDs_Patient typIntlist

      exec pGetConfinmentDepositByPatients
        @ID_Patient_Confinement,
        @IDs_Patient
  END

GO
