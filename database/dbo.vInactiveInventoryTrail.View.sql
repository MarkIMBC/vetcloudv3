﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveInventoryTrail] AS   
				SELECT   
				 H.*  
				FROM vInventoryTrail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
