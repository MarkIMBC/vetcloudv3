﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveLocalShippingPermitIssuance_Requirement] AS   
				SELECT   
				 H.*  
				FROM vLocalShippingPermitIssuance_Requirement H  
				WHERE IsActive = 1
GO
