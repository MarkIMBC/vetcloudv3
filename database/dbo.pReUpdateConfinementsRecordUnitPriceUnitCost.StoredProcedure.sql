﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pReUpdateConfinementsRecordUnitPriceUnitCost] (@IDs_Patient_Confinement TYPINTLIST READONLY)
as
    DECLARE @IDs_Patient_Confinement_ItemsServices typIntList
    DECLARE @IDs_Patient_SOAP_Treatment typIntList
    DECLARE @IDs_Patient_SOAP_Prescription typIntList

    INSERT @IDs_Patient_Confinement_ItemsServices
    SELECT confItems.ID
    FROm   vPatient_Confinement_ItemsServices confItems
           inner join tPatient_Confinement conf
                   on conf.ID = confItems.ID_Patient_Confinement
           inner join @IDs_Patient_Confinement idsConf
                   on conf.ID = idsConf.ID
				    
    INSERT @IDs_Patient_SOAP_Treatment
    SELECT ID_Patient_SOAP_Treatment
    FROm   tPatient_Confinement_ItemsServices confItems
           inner join @IDs_Patient_Confinement idsConf
                   on confItems.ID_Patient_Confinement = idsConf.ID
    WHERE  ISNULL(ID_Patient_SOAP_Treatment, 0) > 0
           and ( UnitPrice IS NULL
                  OR UnitCost IS NULL )

    INSERT @IDs_Patient_SOAP_Prescription
    SELECT ID_Patient_SOAP_Prescription
    FROm   tPatient_Confinement_ItemsServices confItems
           inner join @IDs_Patient_Confinement idsConf
                   on confItems.ID_Patient_Confinement = idsConf.ID
    WHERE  ISNULL(ID_Patient_SOAP_Prescription, 0) > 0
           and ( UnitPrice IS NULL
                  OR UnitCost IS NULL )

    Update tPatient_SOAP_Prescription
    SET    UnitPrice = item.UnitPrice
    FROM   tPatient_SOAP_Prescription soapPrescription
           INNER JOIN @IDs_Patient_SOAP_Prescription ids
                   on soapPrescription.ID = ids.ID
           INNER JOIN tItem item
                   on soapPrescription.ID_Item = item.ID

    Update tPatient_SOAP_Prescription
    SET    UnitCost = item.UnitCost
    FROM   tPatient_SOAP_Prescription soapPrescription
           INNER JOIN @IDs_Patient_SOAP_Prescription ids
                   on soapPrescription.ID = ids.ID
           INNER JOIN tItem item
                   on soapPrescription.ID_Item = item.ID

    Update tPatient_SOAP_Treatment
    SET    UnitPrice = item.UnitPrice
    FROM   tPatient_SOAP_Treatment soapTreatment
           INNER JOIN @IDs_Patient_SOAP_Treatment ids
                   on soapTreatment.ID = ids.ID
           INNER JOIN tItem item
                   on soapTreatment.ID_Item = item.ID

    Update tPatient_SOAP_Treatment
    SET    UnitCost = item.UnitCost
    FROM   tPatient_SOAP_Treatment soapTreatment
           INNER JOIN @IDs_Patient_SOAP_Treatment ids
                   on soapTreatment.ID = ids.ID
           INNER JOIN tItem item
                   on soapTreatment.ID_Item = item.ID

    Update tPatient_Confinement_ItemsServices
    SET    UnitPrice = item.UnitPrice
    FROM   tPatient_Confinement_ItemsServices confItem
           INNER JOIN tItem item
                   on confItem.ID_Item = item.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemIDs
                   on confItem.ID = confItemIDs.ID

    Update tPatient_Confinement_ItemsServices
    SET    UnitCost = item.UnitCost
    FROM   tPatient_Confinement_ItemsServices confItem
           INNER JOIN tItem item
                   on confItem.ID_Item = item.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemIDs
                   on confItem.ID = confItemIDs.ID

    Update tBillingInvoice_Detail
    SET    UnitPrice = item.UnitPrice
    FROM   tBillingInvoice_Detail biDetail
           inner join tBillingInvoice billing
                   on biDetail.ID_BillingInvoice = billing.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemID
                   on confItemID.ID = biDetail.ID_Patient_Confinement_ItemsServices
           INNER JOIN tItem item
                   on item.ID = biDetail.ID_Item
                      ANd ID_FilingStatus = 1

    Update tBillingInvoice_Detail
    SET    UnitCost = item.UnitCost
    FROM   tBillingInvoice_Detail biDetail
           inner join tBillingInvoice billing
                   on biDetail.ID_BillingInvoice = billing.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemID
                   on confItemID.ID = biDetail.ID_Patient_Confinement_ItemsServices
           INNER JOIN tItem item
                   on item.ID = biDetail.ID_Item
                      ANd ID_FilingStatus = 1

GO
