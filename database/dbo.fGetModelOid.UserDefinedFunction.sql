﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetModelOid](@tableName VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      where  TableName = @tableName

      RETURN @Oid_Model
  END;

GO
