﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vAppSetting]
AS
    SELECT  H.* ,
            UC.Name AS CreatedBy ,
            UM.Name AS LastModifiedBy ,
            PT.Name AS [Type] ,
            ( CASE PT.ID
                WHEN 1 THEN H.StringValue
                WHEN 2 THEN CAST(H.IntValue AS VARCHAR(20))
                WHEN 3 THEN CAST(H.DecimalValue AS VARCHAR(20)) --Decimal
                WHEN 4 THEN CAST(H.BoolValue AS VARCHAR(10))   --Bit
                WHEN 5
                THEN ( CASE WHEN H.DateTimeValue IS NOT NULL
                            THEN CAST(h.DateTimeValue AS VARCHAR(100))
                            ELSE ''
                       END ) --DateTime
                WHEN 6
                THEN ( CASE WHEN H.DateTimeValue IS NOT NULL
                            THEN CAST(h.DateTimeValue AS VARCHAR(100))
                            ELSE ''
                       END ) --DateTime
                WHEN 11 THEN H.ColorValue
				WHEN 12 THEN H.ImageValue
                ELSE ''
              END ) AS [Value] --Color
    FROM    tAppSetting H
            LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.[_tPropertyType] PT ON H.ID_PropertyType = PT.ID
GO
