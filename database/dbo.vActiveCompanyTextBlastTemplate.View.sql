﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompanyTextBlastTemplate] AS   
				SELECT   
				 H.*  
				FROM vCompanyTextBlastTemplate H  
				WHERE IsActive = 1
GO
