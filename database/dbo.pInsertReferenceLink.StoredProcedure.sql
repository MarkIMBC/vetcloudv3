﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 PROC [dbo].[pInsertReferenceLink](@Name    VARCHAR(MAX),
                                @Caption VARCHAR(MAX),
                                @UrlLink VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReferenceLink
         WHERE  Name = @Name) = 0
        BEGIN
            INSERT INTO [dbo].tReferenceLink
                        ([Name],
                         Caption,
                         UrlLink,
                         [IsActive],
                         ID_Company,
                         DateCreated,
                         DateModified)
            VALUES      (@Name,
                         @Caption,
                         @UrlLink,
                         1,
                         1,
                         GETDATE(),
                         GETDATE())
        END
      ELSE
        BEGIN
            Update tReferenceLink
            SET    Caption = @Caption,
                   UrlLink = @UrlLink
            where  Name = @Name
        END
  END

GO
