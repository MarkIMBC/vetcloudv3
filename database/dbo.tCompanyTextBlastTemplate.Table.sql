﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCompanyTextBlastTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Template] [varchar](300) NULL,
 CONSTRAINT [PK_tCompanyTextBlastTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tCompanyTextBlastTemplate] ON [dbo].[tCompanyTextBlastTemplate]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate]  WITH CHECK ADD  CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] CHECK CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_Company]
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate]  WITH CHECK ADD  CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] CHECK CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate]  WITH CHECK ADD  CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] CHECK CONSTRAINT [FK_tCompanyTextBlastTemplate_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tCompanyTextBlastTemplate]
		ON [dbo].[tCompanyTextBlastTemplate]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tCompanyTextBlastTemplate
			SET    DateCreated = GETDATE()
			FROM   dbo.tCompanyTextBlastTemplate hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] ENABLE TRIGGER [rDateCreated_tCompanyTextBlastTemplate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tCompanyTextBlastTemplate]
		ON [dbo].[tCompanyTextBlastTemplate]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tCompanyTextBlastTemplate
			SET    DateModified = GETDATE()
			FROM   dbo.tCompanyTextBlastTemplate hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tCompanyTextBlastTemplate] ENABLE TRIGGER [rDateModified_tCompanyTextBlastTemplate]
GO
