﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBusinessPartner] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartner H  
				WHERE IsActive = 1
GO
