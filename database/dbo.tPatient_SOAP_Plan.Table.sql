﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_SOAP_Plan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[ID_Patient_SOAP] [int] NULL,
	[DateReturn] [datetime] NULL,
	[ID_Item] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[IsSentSMS] [bit] NULL,
	[CustomItem] [varchar](300) NULL,
	[old_return_patient_id] [int] NULL,
	[Appointment_ID_FilingStatus] [int] NULL,
	[Appointment_CancellationRemarks] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
	[DateRescheduletUpdated] [datetime] NULL,
	[DateReschedule] [datetime] NULL,
 CONSTRAINT [PK_tPatient_SOAP_Plan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_SOAP_Plan] ON [dbo].[tPatient_SOAP_Plan]
(
	[ID_Patient_SOAP] ASC,
	[ID_Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_SOAP_Plan] ADD  DEFAULT ((2)) FOR [Appointment_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPatient_SOAP_Plan]  WITH CHECK ADD  CONSTRAINT [FKtPatient_SOAP_Plan_ID_Patient_SOAP] FOREIGN KEY([ID_Patient_SOAP])
REFERENCES [dbo].[tPatient_SOAP] ([ID])
GO
ALTER TABLE [dbo].[tPatient_SOAP_Plan] CHECK CONSTRAINT [FKtPatient_SOAP_Plan_ID_Patient_SOAP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_SOAP_Plan]
		ON [dbo].[tPatient_SOAP_Plan]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_SOAP_Plan
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_SOAP_Plan hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_SOAP_Plan] ENABLE TRIGGER [rDateCreated_tPatient_SOAP_Plan]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_SOAP_Plan]
		ON [dbo].[tPatient_SOAP_Plan]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_SOAP_Plan
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_SOAP_Plan hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_SOAP_Plan] ENABLE TRIGGER [rDateModified_tPatient_SOAP_Plan]
GO
