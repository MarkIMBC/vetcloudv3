﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vViewType]
AS

SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       viewType.Name Name_ViewType
FROM tViewType H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tViewType viewType
        ON viewType.ID = H.ID;
GO
