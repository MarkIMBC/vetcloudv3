﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCustomDetailViewRoute] AS   
				SELECT   
				 H.*  
				FROM vCustomDetailViewRoute H  
				WHERE IsActive = 1
GO
