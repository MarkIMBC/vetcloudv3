﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vItemService_Listview]
AS
  SELECT item.ID,
         item.Code,
         item.Name,
         item.ID_Company,
         ISNULL(item.UnitCost, 0)           UnitCost,
         ISNULL(item.UnitPrice, 0)          UnitPrice,
         item.IsActive,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '') Name_ItemCategory
  FROM   dbo.vitem item
  WHERE  item.ID_ItemType = 1
         AND ISNULL(item.IsActive, 0) = 1

GO
