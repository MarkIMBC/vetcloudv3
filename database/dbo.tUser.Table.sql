﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Employee] [int] NULL,
	[Username] [varchar](300) NULL,
	[ID_UserGroup] [int] NOT NULL,
	[Password] [varchar](300) NULL,
	[IsRequiredPasswordChangedOnLogin] [bit] NULL,
	[ID_Patient] [int] NULL,
	[OldPassword] [varchar](300) NULL,
 CONSTRAINT [PK_tUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUser] ADD  CONSTRAINT [DF__tUser__IsActive__44FF419A]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUser]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FKtUser_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tUser]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_ID_Employee] FOREIGN KEY([ID_Employee])
REFERENCES [dbo].[tEmployee] ([ID])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FKtUser_ID_Employee]
GO
ALTER TABLE [dbo].[tUser]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FKtUser_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tUser]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_ID_UserGroup] FOREIGN KEY([ID_UserGroup])
REFERENCES [dbo].[tUserGroup] ([ID])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FKtUser_ID_UserGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUser]
		ON [dbo].[tUser]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUser
			SET    DateCreated = GETDATE()
			FROM   dbo.tUser hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUser] ENABLE TRIGGER [rDateCreated_tUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUser]
		ON [dbo].[tUser]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUser
			SET    DateModified = GETDATE()
			FROM   dbo.tUser hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUser] ENABLE TRIGGER [rDateModified_tUser]
GO
