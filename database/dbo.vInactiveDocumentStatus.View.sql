﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDocumentStatus] AS   
				SELECT   
				 H.*  
				FROM vDocumentStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
