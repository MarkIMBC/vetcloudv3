﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientWaitingList] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingList H  
				WHERE IsActive = 1
GO
