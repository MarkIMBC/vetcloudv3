﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveDocumentStatus] AS   
				SELECT   
				 H.*  
				FROM vDocumentStatus H  
				WHERE IsActive = 1
GO
