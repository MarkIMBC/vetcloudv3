﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePaymentMethod] AS   
				SELECT   
				 H.*  
				FROM vPaymentMethod H  
				WHERE ISNULL(IsActive, 0) = 0
GO
