﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSMSPatientSOAP_Company] AS   
				SELECT   
				 H.*  
				FROM vSMSPatientSOAP_Company H  
				WHERE ISNULL(IsActive, 0) = 0
GO
