﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUserRole] AS   
				SELECT   
				 H.*  
				FROM vUserRole H  
				WHERE ISNULL(IsActive, 0) = 0
GO
