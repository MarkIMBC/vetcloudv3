﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Grooming_Detail] AS   
    SELECT   
     H.*  
    FROM vPatient_Grooming_Detail H  
    WHERE IsActive = 1
GO
