﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveFileUpload] AS   
				SELECT   
				 H.*  
				FROM vFileUpload H  
				WHERE IsActive = 1
GO
