﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCheckModuleAccessByRouterLink] (@RouteLink      VARCHAR(MAX),
                                           @ID_UserSession INT)
AS
  BEGIN
      DECLARE @IsValid bit = 1
      DECLARE @Message bit = 1
      DECLARE @Max_Admin_User INT
      Declare @Admin_UserModulesNavigation table
        (
           ID_Navigation VARCHAR(MAX)
        )
      Declare @Admin_SelectedNavigation table
        (
           tempID      int IDENTITY(1, 1),
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )

      select @Max_Admin_User = _userRoles.ID_User
      FROM   tUser_Roles _userRoles
             inner join tUserRole _role
                     on _userRoles.ID_UserRole = _role.ID
      where  _role.Name = 'Administrator'

      INSERT @Admin_UserModulesNavigation
             (ID_Navigation)
      SELECT ID_Navigation
      FROM   [dbo].fGetUserModules(@Max_Admin_User)
      WHERE  ID_Navigation IS NOT NULL

      INSERT @Admin_SelectedNavigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route
      FROm   _vNavigation nav
             inner join @Admin_UserModulesNavigation userNav
                     on nav.Oid = userNav.ID_Navigation
      WHERE  nav.ID_View IS NULL
              OR nav.IsActive = 1

      --------------------------------------------------
      DECLARE @ID_User INT
      Declare @User_UserModulesNavigation table
        (
           ID_Navigation VARCHAR(MAX)
        )
      Declare @User_SelectedNavigation table
        (
           tempID      int IDENTITY(1, 1),
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      where  iD = @ID_UserSession

      INSERT @User_UserModulesNavigation
      SELECT ID_Navigation
      FROM   [dbo].fGetUserModules(@ID_User)
      WHERE  ID_Navigation IS NOT NULL

      INSERT @User_SelectedNavigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route
      FROm   _vNavigation nav
             inner join @User_UserModulesNavigation userNav
                     on nav.Oid = userNav.ID_Navigation
      WHERE  nav.ID_View IS NULL
              OR nav.IsActive = 1

      ---------------------------------------------------
      DECLARE @CountNavigationAdmin INT = 0
      DECLARE @CountNavigationUser INT = 0

      SELECT @CountNavigationAdmin = COUNT(*)
      FROM   @Admin_SelectedNavigation
      WHERE  Route = @RouteLink

      SELECT @CountNavigationUser = COUNT(*)
      FROM   @User_SelectedNavigation
      WHERE  Route = @RouteLink

      if( @CountNavigationAdmin > 0
          AND @CountNavigationUser = 0 )
        SET @IsValid = 0;

      SELECT '' AS _

      SELECT @IsValid IsValid
  END

GO
