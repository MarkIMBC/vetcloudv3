﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveLocalShippingPermitIssuance_Item] AS   
				SELECT   
				 H.*  
				FROM vLocalShippingPermitIssuance_Item H  
				WHERE ISNULL(IsActive, 0) = 0
GO
