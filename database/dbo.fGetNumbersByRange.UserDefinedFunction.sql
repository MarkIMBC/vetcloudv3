﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetNumbersByRange](@from INT,
                                  @to   INT)
RETURNS @CountTable TABLE (
  Number INT)
AS
  BEGIN
      with cte_n
           as (select @from as [CountNumber]
               union all
               select CountNumber + 1
               from   cte_n
               where  CountNumber < @to)
      Insert @CountTable
      select CountNumber
      from   cte_n
      option (maxrecursion 0)

      return
  END

GO
