﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEmployeeStatus] AS   
				SELECT   
				 H.*  
				FROM vEmployeeStatus H  
				WHERE IsActive = 1
GO
