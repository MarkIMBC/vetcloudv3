﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientWaitingListCanceled] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingListCanceled H  
				WHERE ISNULL(IsActive, 0) = 0
GO
