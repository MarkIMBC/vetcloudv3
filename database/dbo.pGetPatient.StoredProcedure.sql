﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetPatient] @ID         INT = -1,  
                               @ID_Client  INT = NULL,  
                               @ID_Session INT = NULL  
AS  
  BEGIN  
      SELECT '_'
             --'' AS Patient_SOAP;  
  
      DECLARE @Approved_ID_FIlingStatus INT = 3;  
      DECLARE @Cancelled_ID_FIlingStatus INT = 4;  
      DECLARE @ID_User      INT,  
              @ID_Employee  INT,  
              @ID_Company   INT,  
              @ID_Warehouse INT;  
  
      SELECT @ID_User = ID_User,  
             @ID_Warehouse = ID_Warehouse  
      FROM   tUserSession  
      WHERE  ID = @ID_Session;  
  
      if( @ID_Client = -1 )  
        SET @ID_Client = NULL  
  
      SELECT @ID_Employee = ID_Employee  
      FROM   dbo.tUser  
      WHERE  ID = @ID_User;  
  
      SELECT @ID_Company = ID_Company  
      FROM   dbo.tEmployee  
      WHERE  ID = @ID_Employee;  
  
      IF ( @ID = -1 )  
        BEGIN  
            SELECT H.*,  
                   client.Name Name_Client  
            FROM   (SELECT NULL       AS [_],  
                           -1         AS [ID],  
                           NULL       AS [Code],  
                           NULL       AS [Name],  
                           1          AS [IsActive],  
                           NULL       AS [Comment],  
                           NULL       AS [DateCreated],  
                           NULL       AS [DateModified],  
                           NULL       AS [ID_CreatedBy],  
                           NULL       AS [ID_LastModifiedBy],  
                           @ID_Client AS ID_Client,  
                           0          IsDeceased) H  
                   LEFT JOIN dbo.tUser UC  
                          ON H.ID_CreatedBy = UC.ID  
                   LEFT JOIN dbo.tUser UM  
                          ON H.ID_LastModifiedBy = UM.ID  
                   LEFT JOIN dbo.tClient client  
                          ON client.ID = H.ID_Client;  
        END;  
      ELSE  
        BEGIN  
            SELECT H.*,  
                   dbo.fGetAge(h.DateBirth, case  
                                              WHEN ISNULL(h.IsDeceased, 0) = 1 then h.DateDeceased  
                                              ELSE NULL  
                                            END, 'N/A') Age  
            FROM   dbo.vPatient H  
            WHERE  H.ID = @ID;  
        END;  
  
  
      --SELECT ID,  
      --       DateString,  
      --       Code,  
      --       Name_SOAPType,  
      --       CASE  
      --         WHEN LEN(History) >= 200 THEN LTRIM(RTRIM(LEFT(History, 200 - 3))) + '...'  
      --         ELSE History  
      --       END History,  
      --       Subjective,  
      --       Name_FilingStatus  
      --FROM   dbo.vPatient_SOAP  
      --WHERE  ID_Patient = @ID  
      --       and ID_Patient > 0  
      --       AND ID_FilingStatus NOT IN ( @Cancelled_ID_FIlingStatus )  
      --ORDER  BY Date DESC;  
  END;   
GO
