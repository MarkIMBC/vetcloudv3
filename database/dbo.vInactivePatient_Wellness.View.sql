﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Wellness] AS   
				SELECT   
				 H.*  
				FROM vPatient_Wellness H  
				WHERE ISNULL(IsActive, 0) = 0
GO
