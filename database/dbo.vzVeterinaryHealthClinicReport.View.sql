﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzVeterinaryHealthClinicReport]
AS
  SELECT vetcert.ID,
         vetcert.Code,
         vetcert.Date,
         vetcert.DestinationAddress,
         vetcert. Color,
         vetcert. Weight,
         vetcert.ID_Item,
         vetcert.Name_Item,
         vetcert.LotNumber,
         patient.ID                                                                                                                                                      ID_Patient,
         patient.NAME                                                                                                                                                    Name_Patient,
         IsNull(Species, '')                                                                                                                                             Name_Species,
         IsNull(Name_Gender, '')                                                                                                                                         Name_Gender,
         patient.Microchip,
         IsNull(FORMAT(patient.DateBirth, 'MM/dd/yyyy'), '')                                                                                                             Birthdate,
         dbo.fGetAge(patient.DateBirth, CASE
                                          WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                                                      Age_Patient,
         IsNull(client.NAME, '')                                                                                                                                         Name_Client,
         IsNull(client.Address, '')                                                                                                                                      Address_Client,
         IsNull(client.ContactNumber, '')                                                                                                                                ContactNumber_Client,
         IsNull(client.ContactNumber2, '')                                                                                                                               ContactNumber2_Client,
         IsNull(client.Email, '')                                                                                                                                        Email_Client,
         company.ID                                                                                                                                                      ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.NAME                                                                                                                                                    Name_Company,
         company.Address                                                                                                                                                 Address_Company,
         company.ContactNumber                                                                                                                                           ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                                                           HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         _AttendingPhysician.TINNumber,
         _AttendingPhysician.PTR,
         _AttendingPhysician.PRCLicenseNumber,
         CASE
           WHEN _AttendingPhysician.DatePRCExpiration IS NOT NULL THEN FORMAT(_AttendingPhysician.DatePRCExpiration, 'MM/dd/yyyy')
           ELSE ''
         END                                                                                                                                                             DatePRCExpiration,
         CASE
           WHEN vetCert.DateVaccination IS NOT NULL THEN FORMAT(vetCert.DateVaccination, 'MM/dd/yyyy')
           ELSE '                '
         END                                                                                                                                                             DateVaccination,
         dbo.fGetVeterinaryHealthCertificateVaccinationOptionComment(vacOption.Comment, vetCert.DateVaccination, vetcert.Name_Item, vetcert.LotNumber, vetCert.CaseType) Comment_VaccinationOption,
         ISNULL(vetCert.ValidityDayCount, 0)                                                                                                                             ValidityDayCount,
         ISNULL(vetCert.CaseType, '')                                                                                                                                    CaseType,
         ID_VaccinationOption
  FROM   vVeterinaryHealthCertificate vetCert
         LEFT JOIN vPatient patient
                ON vetCert.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = vetCert.ID_Company
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = vetCert.AttendingPhysician_ID_Employee
         LEFT JOIN tVaccinationOption vacOption
                on vetCert.ID_VaccinationOption = vacOption.ID

GO
