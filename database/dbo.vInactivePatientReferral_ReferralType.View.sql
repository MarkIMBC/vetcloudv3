﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientReferral_ReferralType] AS   
    SELECT   
     H.*  
    FROM vPatientReferral_ReferralType H  
    WHERE ISNULL(IsActive, 0) = 1
GO
