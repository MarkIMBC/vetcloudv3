﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBillingInvoice_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_BillingInvoice] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[Amount] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[DateExpiration] [datetime] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[IsComputeDiscountRate] [bit] NULL,
	[DiscountRate] [decimal](18, 4) NULL,
	[ID_Patient_Confinement_ItemsServices] [int] NULL,
	[ID_Patient_Vaccination] [int] NULL,
	[ID_Patient_Wellness_Detail] [int] NULL,
	[tempID] [varchar](300) NULL,
	[ID_Patient_Grooming_Detail] [int] NULL,
 CONSTRAINT [PK_tBillingInvoice_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBillingInvoice_Detail] ON [dbo].[tBillingInvoice_Detail]
(
	[ID_BillingInvoice] ASC,
	[ID_Item] ASC,
	[ID_Patient_Confinement_ItemsServices] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Detail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] CHECK CONSTRAINT [FK_tBillingInvoice_Detail_ID_Company]
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] CHECK CONSTRAINT [FK_tBillingInvoice_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] CHECK CONSTRAINT [FK_tBillingInvoice_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail]  WITH CHECK ADD  CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice])
REFERENCES [dbo].[tBillingInvoice] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] CHECK CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tBillingInvoice_Detail]
		ON [dbo].[tBillingInvoice_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice_Detail
			SET    DateCreated = GETDATE()
			FROM   dbo.tBillingInvoice_Detail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] ENABLE TRIGGER [rDateCreated_tBillingInvoice_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tBillingInvoice_Detail]
		ON [dbo].[tBillingInvoice_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice_Detail
			SET    DateModified = GETDATE()
			FROM   dbo.tBillingInvoice_Detail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice_Detail] ENABLE TRIGGER [rDateModified_tBillingInvoice_Detail]
GO
