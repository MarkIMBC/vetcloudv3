﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vBillingInvoiceSMSRemiderForTodayIDs]
as
  SELECT bisms.ID_BillingInvoice
  FROM   tBillingInvoice_SMSPayableRemider bisms
         inner join tBillingInvoice bi
                 on bi.ID = bisms.ID_BillingInvoice
  where  ISNULL(bisms.IsSentSMS, 0) = 0
         AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
         AND bi.Payment_ID_FilingStatus IN ( 2, 11 )

GO
