﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppointment] AS   
				SELECT   
				 H.*  
				FROM vAppointment H  
				WHERE ISNULL(IsActive, 0) = 0
GO
