﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vReRunProcessQueueRecord]
AS
  WITH CTE
       as (SELECT DB_NAME()         DatabaseName,
                  'tBillingInvoice' TableName,
                  c.Name            Name_Company,
                  bi.ID             ID_CurrentObject,
                  bi.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tBillingInvoice bi WITH (NOLOCK)
                  INNER JOIN vCompanyActive c WITH (NOLOCK)
                          on bi.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN( 13 )
                             ANd ID_FilingStatus NOT IN ( 4 )
                             AND CONVERT(Date, bi.DateModified) BETWEEN DATEADD(DAY, -10, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE())
           UNion ALL
           SELECT DB_NAME()       DatabaseName,
                  'tPatient_SOAP' TableName,
                  c.Name          Name_Company,
                  _soap.ID        ID_CurrentObject,
                  _soap.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tPatient_SOAP _soap WITH (NOLOCK)
                  INNER JOIN vCompanyActive c WITH (NOLOCK)
                          on _soap.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN( 13 )
                             --ANd ID_FilingStatus NOT IN (4)                  
                             AND CONVERT(Date, _soap.DateModified) BETWEEN DATEADD(DAY, -10, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE()))
  SELECT DatabaseName + TableName
         + CONVERT(varchar(MAX), ID_CurrentObject) Oid,
         *
  FROm   CTE

GO
