﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vzReceivingReportReport]
AS
SELECT rrHed.ID,
       rrHed.Code,
       rrHed.Code_PurchaseOrder,
       rrHed.Date,
       rrHed.ID_Supplier,
       rrHed.Name_Supplier,
       rrHed.Name_FilingStatus,
       rrHed.CreatedBy_Name_User,
       rrHed.ApprovedBy_Name_User,
       rrHed.Name_TaxScheme,
       rrHed.DateApproved,
       rrHed.GrossAmount,
       rrHed.VatAmount,
       rrHed.NetAmount,
       rrDetail.ID ID_ReceivingReport_Detail,
       rrDetail.Name_Item,
       rrDetail.Quantity,
       rrDetail.UnitPrice,
       rrDetail.Amount,
       rrHed.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.vReceivingReport rrHed
    LEFT JOIN dbo.vReceivingReport_Detail rrDetail
        ON rrHed.ID = rrDetail.ID_ReceivingReport
    LEFT JOIN dbo.vCompany company
        ON company.ID = rrHed.ID_Company;
GO
