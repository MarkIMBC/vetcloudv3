﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveInventoryStatus] AS   
				SELECT   
				 H.*  
				FROM vInventoryStatus H  
				WHERE IsActive = 1
GO
