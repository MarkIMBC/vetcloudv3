﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientGrooming] AS   
				SELECT   
				 H.*  
				FROM vPatientGrooming H  
				WHERE ISNULL(IsActive, 0) = 0
GO
