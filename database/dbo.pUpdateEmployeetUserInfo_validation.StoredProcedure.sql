﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateEmployeetUserInfo_validation](@ID_Employee    INT,
                                               @UserName       VARCHAR(MAX),
                                               @Password       VARCHAR(MAX),
                                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @IsActive_UserSession BIT
      DECLARE @ID_Company INT
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      SELECT '_'

      SELECT *
      FROm   @table
  END

GO
