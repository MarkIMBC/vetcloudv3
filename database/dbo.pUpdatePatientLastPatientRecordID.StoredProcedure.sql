﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdatePatientLastPatientRecordID](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @PatientSOAPMaxDate TABLE
        (
           ID_Patient      INT,
           ID_Patient_SOAP INT,
           Date     DateTime
        )

      INSERT @PatientSOAPMaxDate
             (ID_Patient,
              Date)
      SELECT _hed.ID_Patient,
             MAX(_hed.Date) Date
      FROM   tPatient_SOAP _hed
             INNER JOIN @IDs_Patient idsPatient
                     on idsPatient.ID = _hed.ID_Patient
      WHERE  _hed.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY ID_Patient

      UPDATE @PatientSOAPMaxDate
      SET    ID_Patient_SOAP = _hed.ID
      FROM   @PatientSOAPMaxDate record
             INNER JOIN tPatient_SOAP _hed
                     on record.Date = _hed.Date
                        and record.ID_Patient = _hed.ID_Patient

      Update tPatient
      SET    Last_ID_Patient_SOAP = record.ID_Patient_SOAP
      FROM   tPatient patient
             INNER JOIN @PatientSOAPMaxDate record
                     ON patient.ID = record.ID_Patient
  END

GO
