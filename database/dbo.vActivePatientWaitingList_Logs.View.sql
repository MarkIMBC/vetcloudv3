﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientWaitingList_Logs] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingList_Logs H  
				WHERE IsActive = 1
GO
