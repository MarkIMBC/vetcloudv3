﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pAddUserRoleModel](@Name_UserRole VARCHAR(MAX),
                             @Name_Model    VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Model VARCHAR(MAX) = '';
      DECLARE @ID_UserRole INT = 0;

      IF(SELECT COUNT(*)
         FROm   tUserRole
         WHERE  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess],
                         [IsAdministrator])
            VALUES      (@Name_UserRole,
                         1,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         0,
                         0)
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      SELECT @ID_Model = Oid
      FROM   _tModel
      where  Name = @Name_Model

      if(select COUNT(*)
         FROM   tUserRole_Detail
         WHERE  ID_UserRole = @ID_UserRole
                AND ID_Model = @ID_Model) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Detail]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Model])
            VALUES      (NULL,
                         1,
                         @ID_UserRole,
                         @ID_Model)
        END

      declare @IDs_UserAdministrator typIntlist

      INSERT @IDs_UserAdministrator
      select ID_User
      FROm   vCompanyActiveUserAdministrator WITH (NOLOCK)
      EXCEPT
      SELECT ID_User
      FROM   tUser_Roles WITH (NOLOCK)
      WHERE  ID_UserRole = @ID_UserRole

      INSERT INTO [dbo].[tUser_Roles]
                  ([IsActive],
                   [ID_User],
                   [ID_UserRole])
      SELECT 1,
             ID,
             @ID_UserRole
      FROM   @IDs_UserAdministrator
  END

GO
