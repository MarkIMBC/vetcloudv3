﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_BillingInvoice] (@ID_CurrentObject VARCHAR(10),
                                                     @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @HayopTownVeterinaryClinic_ID_Company INT = 73;
      DECLARE @Code VARCHAR(MAX) = '';
      DECLARE @ID_CreatedBy INT;
      DECLARE @ID_USERSESSION INT;
      DECLARE @ID_FilingStatus INT;
      DECLARE @ID_Company INT;
      DECLARE @IDs_BillingINvoice TYPINTLIST

      INSERT @IDs_BillingINvoice
      VALUES (@ID_CurrentObject)

      SELECT @ID_Company = ID_Company,
             @ID_FilingStatus = ID_FilingStatus,
             @ID_CreatedBy = ID_CreatedBy
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tBillingInvoice
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'BillingInvoice';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tBillingInvoice
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      UPDATE dbo.tBillingInvoice
      SET    RunAfterSavedProcess_ID_FilingStatus = NULL,
             DateModified = GETDATE()
      WHERE  ID = @ID_CurrentObject;

      UPDATE [tBillingInvoice]
      SET    TotalItemDiscountAmount = biDetailDiscount.TotalItemDiscountAmount
      FROM   [tBillingInvoice] biHed
             INNER JOIN (SELECT [ID_BillingInvoice],
                                Sum(IsNull([DiscountAmount], 0)) TotalItemDiscountAmount
                         FROM   [tBillingInvoice_Detail]
                         GROUP  BY [ID_BillingInvoice]) biDetailDiscount
                     ON biHed.[ID] = biDetailDiscount.[ID_BillingInvoice]
      WHERE  biHed.ID = @ID_CurrentObject

      exec pUpdateBillingInvoicePatientName
        @IDs_BillingINvoice
     /*Update Confinement Bill Status*/    
  --   DECLARE @IDs_Patient_Confinement TYPINTLIST    
  --   INSERT @IDs_Patient_Confinement    
  --   SELECT ID_Patient_Confinement    
  --   FROM   tBillingInvoice    
  --   WHERE  ID = @ID_CurrentObject    
  --   EXEC PupDatePatient_ConfineMen_BillIngStatus    
  --     @IDs_Patient_Confinement    
  --   /*Update SOAP Bill Status*/    
  --   DECLARE @IDs_Patient_SOAP TYPINTLIST    
  --   INSERT @IDs_Patient_SOAP    
  --   SELECT ID_Patient_SOAP    
  --   FROM   tBillingInvoice    
  --   WHERE  ID = @ID_CurrentObject    
  --   EXEC pUpdatePatient_SOAP_BillingStatus    
  --     @IDs_Patient_SOAP    
  --exec dbo.pModel_AfterSaved_BillingInvoice_Computation @IDs_BillingInvoice   
  --exec ppUpdatePatient_Wellness_BillingStatus_By_BIs
  --  @IDs_BillingInvoice
  --exec ppUpdatePatient_Grooming_BillingStatus_By_BIs
  --  @IDs_BillingInvoice
  END;

GO
