﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveReferenceLink] AS   
    SELECT   
     H.*  
    FROM vReferenceLink H  
    WHERE ISNULL(IsActive, 0) = 1
GO
