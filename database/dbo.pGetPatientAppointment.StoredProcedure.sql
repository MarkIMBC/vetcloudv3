﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPatientAppointment] @ID         INT = -1,
                                          @ID_Client  INT = -1,
                                          @ID_Patient INT = -1,
                                          @DateStart  DATETIME = NULL,
                                          @DateEnd    DATETIME = NULL,
                                          @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_';

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      if @DateStart IS NULL
        SET @DateStart = GETDATE()

      if @DateEnd IS NULL
        SET @DateEnd = GETDATE()

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        BEGIN
            SET @ID_Patient = NULL
        END
      ELSE
        BEGIN
            SELECT @ID_Client = ID_Client
            FROm   tPatient
            WHERE  ID = @ID_Patient
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           NULL        AS [Code],
                           NULL        AS [Name],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           @ID_Client  AS ID_Client,
                           @ID_Patient AS ID_Patient,
                           @DateStart  DateStart,
                           @DateEnd    DateEnd,
                           1           [ID_FilingStatus]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON H.ID_Client = client.ID
                   LEFT JOIN tPatient patient
                          ON H.ID_Patient = patient.ID
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatientAppointment H
            WHERE  H.ID = @ID;
        END;
  END; 
GO
