﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Grooming] AS   
    SELECT   
     H.*  
    FROM vPatient_Grooming H  
    WHERE IsActive = 1
GO
