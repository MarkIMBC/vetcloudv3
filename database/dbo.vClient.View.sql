﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name                 AS CreatedBy,
         UM.Name                 AS LastModifiedBy,
         H.DateLastVisited,
         H.CurrentCreditAmount,
         acct.Username,
         acct.Password,
         h.ProfileImageFile,
         attendingPersonell.Name LastAttendingPhysician_Name_Employee,
         H.TotalRemainingAmount
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tUser acct
                ON H.ID_User = acct.ID
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO
