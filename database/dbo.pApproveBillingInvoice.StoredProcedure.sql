﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE      
 PROC [dbo].[pApproveBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,  
                                           @ID_UserSession     INT)  
AS  
  BEGIN  
      DECLARE @Inventoriable_ID_ItemType INT = 2;  
      DECLARE @Approved_ID_FilingStatus INT = 3  
      DECLARE @Used_ID_FilingStatus INT = 17  
      DECLARE @Success BIT = 1;  
      DECLARE @message VARCHAR(300) = '';  
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';  
  
      BEGIN TRY  
          SELECT @Oid_Model_BillingInvoice_Detail = Oid  
          FROM   _tModel  
          where  TableName = 'tBillingInvoice_Detail'  
  
          DECLARE @ID_User INT = 0;  
  
          SELECT @ID_User = ID_User  
          FROM   dbo.tUserSession  
          WHERE  ID = @ID_UserSession;  
  
          EXEC dbo.pApproveBillingInvoice_validation  
            @IDs_BillingInvoice,  
            @ID_UserSession  
  
          Update tBillingInvoice_Detail  
          SET    UnitCost = ISNULL(item.UnitCost, 0)  
          FROM   tBillingInvoice_Detail biDetail  
                 INNER JOIn @IDs_BillingInvoice ids  
                         on biDetail.ID_BillingInvoice = ids.iD  
                 INNER JOIN tItem item  
                         on biDetail.ID_Item = item.ID  
  
          UPDATE dbo.tBillingInvoice  
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,  
                 DateApproved = GETDATE(),  
                 ID_ApprovedBy = @ID_User  
          FROM   dbo.tBillingInvoice bi  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON bi.ID = ids.ID;  
  
          /*Inventory Trail */  
          INSERT INTO dbo.tInventoryTrail  
                      (Code,  
                       ID_Company,  
                       DateCreated,  
                       ID_Item,  
                       Quantity,  
                       UnitPrice,  
                       ID_FilingStatus,  
                       Date,  
                       DateExpired,  
                       Oid_Model_Reference,  
                       ID_Reference)  
          SELECT hed.Code,  
                 hed.ID_Company,  
                 hed.DateCreated,  
                 detail.ID_Item,  
                 0 - detail.Quantity,  
                 detail.UnitPrice,  
                 hed.ID_FilingStatus,  
                 hed.Date,  
                 detail.DateExpiration,  
                 @Oid_Model_BillingInvoice_Detail,  
                 detail.ID  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          -- pUpdate Item Current Inventory         
          DECLARE @IDs_Item typINTList  
  
          INSERT @IDs_Item  
          SELECT DISTINCT ID_Item  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  
  
          -------------------------------------------------------        
          ---        
          EXEC dbo.pUpdateBillingInvoicePayment  
            @IDs_BillingInvoice;  
  
          exec pInsertBillingInvoiceSMSPayableRemider  
            @IDs_BillingInvoice,  
            @ID_UserSession  
			
          exec ppUpdatePatient_Wellness_BillingStatus_By_BIs @IDs_BillingInvoice
          exec ppUpdatePatient_Grooming_BillingStatus_By_BIs @IDs_BillingInvoice
  
          -- Used Deposit on Credit Logs            
          Declare @IDs_ClientDeposit typIntList  
          DECLARE @ClientCredits typClientCredit  
  
          INSERT @IDs_ClientDeposit  
          SELECT cd.ID  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
                 inner join tClientDeposit cd  
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement  
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )  
  
          INSERT @ClientCredits  
          SELECT distinct bi.ID_Client,  
                          bi.Date,  
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ) * -1,  
                          bi.Code,  
                          'Use Deposit from '  
                          + FORMAT(ISNULL(bi.ConfinementDepositAmount, 0), '#,#0.00')  
                          + ' to '  
                          + FORMAT(ISNULL( bi.RemainingDepositAmount, 0), '#,#0.00')  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0  
  
          UPDATE tClientDeposit  
          SET    ID_FilingStatus = @Used_ID_FilingStatus  
          WHERE  ID IN (SELECT ID  
                        FROM   @IDs_ClientDeposit)  
  
          IF (SELECT COUNT(*)  
              FROM   @ClientCredits) > 0  
            begin  
                EXEC pAdjustClientCredits_validation  
                  @ClientCredits,  
                  @ID_UserSession  
  
                exec pAdjustClientCredits  
                  @ClientCredits,  
                  @ID_UserSession  
            END  
      END TRY  
      BEGIN CATCH  
          SET @message = ERROR_MESSAGE();  
          SET @Success = 0;  
      END CATCH;  
  
      SELECT '_';  
  
      SELECT @Success Success,  
             @message message;  
  END;  
  
GO
