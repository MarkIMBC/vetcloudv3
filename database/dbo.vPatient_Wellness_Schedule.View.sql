﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Wellness_Schedule]
AS
  SELECT H.*,
         UC.Name               AS CreatedBy,
         UM.Name               AS LastModifiedBy,
         appointmentStaus.Name Appointment_Name_FilingStatus
  FROM   tPatient_Wellness_Schedule H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus appointmentStaus
                ON H.Appointment_ID_FilingStatus = appointmentStaus.ID

GO
