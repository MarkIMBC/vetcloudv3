﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-c0c5b4c1-82f8-4461-b2f4-d901932a8c79] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79') > 0)   DROP SERVICE [SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79]; if (OBJECT_ID('SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-c0c5b4c1-82f8-4461-b2f4-d901932a8c79]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-c0c5b4c1-82f8-4461-b2f4-d901932a8c79]; END COMMIT TRANSACTION; END
GO
