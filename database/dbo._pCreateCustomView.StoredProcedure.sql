﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[_pCreateCustomView]
    @CustomName VARCHAR(300),
    @IsNavigation BIT = 1
AS
DECLARE @Oid_Custom UNIQUEIDENTIFIER = NEWID();

DECLARE @Oid_View UNIQUEIDENTIFIER = NEWID();

INSERT INTO dbo._tView
(
    Oid,
    Code,
    Name,
    IsActive,
    ID_ListView,
    ID_Dashboard,
    ID_ViewType,
    CustomViewPath,
    ControllerPath,
    Comment,
    DateCreated,
    ID_CreatedBy,
    ID_LastModifiedBy,
    DateModified,
    ID_Model,
    DataSource
)
VALUES
(   @Oid_View,                   -- Oid - uniqueidentifier
    NULL,                        -- Code - varchar
    @CustomName + '_CustomView', -- Name - varchar
    1,                           -- IsActive - bit
    NULL,                        -- ID_ListView - uniqueidentifier
    NULL,                        -- ID_Dashboard - uniqueidentifier
    4,                           -- ID_ViewType - int
    NULL,                        -- CustomViewPath - varchar
    NULL,                        -- ControllerPath - varchar
    NULL,                        -- Comment - varchar
    GETDATE(),                   -- DateCreated - datetime
    1,                           -- ID_CreatedBy - int
    NULL,                        -- ID_LastModifiedBy - int
    GETDATE(),                   -- DateModified - datetime
    NULL,                        -- ID_Model - uniqueidentifier
    NULL                         -- DataSource - varchar
    );

IF (@IsNavigation = 1)
BEGIN

    INSERT INTO dbo._tNavigation
    (
        Oid,
        Code,
        Name,
        IsActive,
        Comment,
        ID_CreatedBy,
        ID_LastModifiedBy,
        DateCreated,
        DateModified,
        ID_View,
        Caption,
        Icon,
        SeqNo,
        ID_Parent
    )
    VALUES
    (   NEWID(),                     -- Oid - uniqueidentifier
        NULL,                        -- Code - varchar
        @CustomName + '_Navigation', -- Name - varchar
        1,                           -- IsActive - bit
        NULL,                        -- Comment - varchar
        1,                           -- ID_CreatedBy - int
        NULL,                        -- ID_LastModifiedBy - int
        GETDATE(),                   -- DateCreated - datetime
        GETDATE(),                   -- DateModified - datetime
        @Oid_View,                   -- ID_View - uniqueidentifier
        @CustomName,                 -- Caption - varchar
        NULL,                        -- Icon - varchar
        NULL,                        -- SeqNo - int
        NULL                         -- ID_Parent - uniqueidentifier
        );
END;
GO
