﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 PROC [dbo].[pGetDashboardBarangayRegisteredResidentDataSource] (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT TOP 10 resident.Name_Company,
                    Count(resident.ID_Company) as Count
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  resident.ID_Company = @ID_Company
             AND YEAR(resident.DateCreated) = @DateYear
      GROUP  BY resident.ID_Company,
                resident.Name_Company
      ORDER  BY Count DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO
