﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tInventoryTrail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[ID_FilingStatus] [int] NULL,
	[Date] [datetime] NULL,
	[DateExpired] [datetime] NULL,
	[BatchNo] [int] NULL,
	[tempID] [varchar](300) NULL,
	[Oid_Model_Reference] [varchar](300) NULL,
	[ID_Reference] [int] NULL,
 CONSTRAINT [PK_tInventoryTrail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tInventoryTrail] ON [dbo].[tInventoryTrail]
(
	[ID_Item] ASC,
	[ID_Reference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tInventoryTrail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tInventoryTrail]  WITH CHECK ADD  CONSTRAINT [FK_tInventoryTrail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tInventoryTrail] CHECK CONSTRAINT [FK_tInventoryTrail_ID_Company]
GO
ALTER TABLE [dbo].[tInventoryTrail]  WITH CHECK ADD  CONSTRAINT [FK_tInventoryTrail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tInventoryTrail] CHECK CONSTRAINT [FK_tInventoryTrail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tInventoryTrail]  WITH CHECK ADD  CONSTRAINT [FK_tInventoryTrail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tInventoryTrail] CHECK CONSTRAINT [FK_tInventoryTrail_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tInventoryTrail]
		ON [dbo].[tInventoryTrail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tInventoryTrail
			SET    DateCreated = GETDATE()
			FROM   dbo.tInventoryTrail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tInventoryTrail] ENABLE TRIGGER [rDateCreated_tInventoryTrail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tInventoryTrail]
		ON [dbo].[tInventoryTrail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tInventoryTrail
			SET    DateModified = GETDATE()
			FROM   dbo.tInventoryTrail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tInventoryTrail] ENABLE TRIGGER [rDateModified_tInventoryTrail]
GO
