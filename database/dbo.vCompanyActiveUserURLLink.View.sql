﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE       
 VIEW [dbo].[vCompanyActiveUserURLLink]
as
  SELECT user_.ID_Company,
         user_.Name_Company,
         emp.ID                            ID_Employee,
         emp.Name                          Name_Employee,
         emp.Name_Position,
         user_.ID                          ID_User,
         '/LoginUrl/' + c.Guid + '|'
         + CONVERT(VARCHAR(MAX), user_.ID) UserURLLink
  FROm   vUser user_
         inner join vCompanyActive c
                 on user_.ID_Company = c.ID
         LEFT JOIN vEmployee emp
                on user_.ID_Employee = emp.ID
  WHERE  user_.IsActive = 1
         AND ( user_.Username NOT LIKE '%system'
               AND user_.UserName NOT LIKE '%-sole'
               AND user_.UserName NOT LIKE '%receptionportal'
               AND user_.UserName NOT LIKE '%clientform' )

GO
