﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSalesReturn_ListView]
AS
  SELECT srHed.ID,
         srDetail.ID_SalesReturn,
         srHed.ID_BillingInvoice,
         ID_Item,
         Name_Item,
         Quantity,
         Amount
  FROM   vSalesReturn_Detail srDetail
         INNER JOIN vSalesReturn srHed
                 ON srDetail.ID_SalesReturn = srHed.ID
  WHERE  srHed.ID_FilingStatus IN ( 3 )

GO
