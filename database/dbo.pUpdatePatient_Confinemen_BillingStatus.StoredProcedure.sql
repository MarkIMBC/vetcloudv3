﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 PROC [dbo].[pUpdatePatient_Confinemen_BillingStatus] (@IDs_Patient_Confinement typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @PatientConfinement TABLE
        (
           ID_Patient_Confinement         INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @PatientConfinement
      SELECT ID,
             NULL
      FROM   @IDs_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = tbl.ID_FilingStatus
      FROM   @PatientConfinement pConfine
             INNER JOIN (SELECT itemservices.ID_Patient_Confinement,
                                CASE
                                  WHEN Count(*) > 0 THEN @ForBilling_ID_FilingStatus
                                  ELSE NULL
                                END ID_FilingStatus
                         FROM   tPatient_Confinement_ItemsServices itemservices WITH (NOLOCK)
                                INNER JOIN tPatient_Confinement confineRec WITH (NOLOCK)
                                        ON confineRec.ID = itemservices.ID_Patient_Confinement
                                INNER JOIN @IDs_Patient_Confinement ids
                                        ON ids.ID = itemservices.ID_Patient_Confinement
                         WHERE  confineRec.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )
                         GROUP  BY itemservices.ID_Patient_Confinement) tbl
                     on tbl.ID_Patient_Confinement = pConfine.ID_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = fs.ID
      FROM   @PatientConfinement ids
             INNER JOIN vBillingInvoice biHed WITH (NOLOCK)
                     on ids.ID_Patient_Confinement = biHed.ID_Patient_Confinement
             LEFT JOIN tFilingStatus fs
                    on fs.Name = biHed.Status
      WHERE  biHed.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )

      Update tPatient_Confinement
      SET    BillingInvoice_ID_FilingStatus = ids.BillingInvoice_ID_FilingStatus
      FROM   tPatient_Confinement hed WITH (NOLOCK)
             inner join @PatientConfinement ids
                     ON ids.ID_Patient_Confinement = hed.ID
  END

GO
