﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPayable_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Payable] [int] NULL,
	[ID_ExpenseCategory] [int] NULL,
	[Amount] [decimal](18, 4) NULL,
 CONSTRAINT [PK_tPayable_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPayable_Detail] ON [dbo].[tPayable_Detail]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPayable_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPayable_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPayable_Detail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPayable_Detail] CHECK CONSTRAINT [FK_tPayable_Detail_ID_Company]
GO
ALTER TABLE [dbo].[tPayable_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPayable_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPayable_Detail] CHECK CONSTRAINT [FK_tPayable_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPayable_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPayable_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPayable_Detail] CHECK CONSTRAINT [FK_tPayable_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPayable_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPayable_Detail_ID_Payable] FOREIGN KEY([ID_Payable])
REFERENCES [dbo].[tPayable] ([ID])
GO
ALTER TABLE [dbo].[tPayable_Detail] CHECK CONSTRAINT [FKtPayable_Detail_ID_Payable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPayable_Detail]
		ON [dbo].[tPayable_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPayable_Detail
			SET    DateCreated = GETDATE()
			FROM   dbo.tPayable_Detail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPayable_Detail] ENABLE TRIGGER [rDateCreated_tPayable_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPayable_Detail]
		ON [dbo].[tPayable_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPayable_Detail
			SET    DateModified = GETDATE()
			FROM   dbo.tPayable_Detail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPayable_Detail] ENABLE TRIGGER [rDateModified_tPayable_Detail]
GO
