﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Description] [varchar](300) NULL,
	[IsFullAccess] [bit] NULL,
	[IsAdministrator] [bit] NULL,
 CONSTRAINT [PK_tUserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUserRole] ADD  CONSTRAINT [DF__tUserRole__IsAct__15702A09]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUserRole]  WITH NOCHECK ADD  CONSTRAINT [FK_tUserRole_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUserRole] CHECK CONSTRAINT [FK_tUserRole_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tUserRole]  WITH NOCHECK ADD  CONSTRAINT [FK_tUserRole_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUserRole] CHECK CONSTRAINT [FK_tUserRole_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUserRole]
		ON [dbo].[tUserRole]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUserRole
			SET    DateCreated = GETDATE()
			FROM   dbo.tUserRole hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUserRole] ENABLE TRIGGER [rDateCreated_tUserRole]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUserRole]
		ON [dbo].[tUserRole]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUserRole
			SET    DateModified = GETDATE()
			FROM   dbo.tUserRole hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUserRole] ENABLE TRIGGER [rDateModified_tUserRole]
GO
