﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveFileUpload] AS   
				SELECT   
				 H.*  
				FROM vFileUpload H  
				WHERE ISNULL(IsActive, 0) = 0
GO
