﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddModelDefaultValueString](@tableName       VARCHAR(MAX),
                                       @GUID_Company    VARCHAR(MAX),
                                       @PropertyName    VARCHAR(MAX),
                                       @ID_PropertyType INT,
                                       @Value           VARCHAR(MAX))
as
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      where  TableName = @tableName

      DECLARE @ID_Company INT = 0

      BEGIN TRY
          SELECT @ID_Company = ID
          FROM   vActiveCompany
          where  Guid = @GUID_Company

          if( ISNULL(@ID_Company, 0) ) = 0
            BEGIN;
                THROW 50005, N'Company does not exist.', 1;
            END

          INSERT INTO [dbo].[tModelDefaultValue]
                      ([IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [Oid_Model],
                       [ID_Company],
                       [GUID_Company],
                       [ID_PropertyType],
                       [Value],
                       [PropertyName])
          Select 1,
                 GETDATE(),
                 GETDATE(),
                 1,
                 1,
                 @Oid_Model,
                 @ID_Company,
                 @GUID_Company,
                 @ID_PropertyType,
                 @Value,
                 @PropertyName

          select *
          FROM   vModelDefaultValue_Listview
          where  Oid_Model = @Oid_Model
                 and ID_Company = @ID_Company
                 AND PropertyName = @PropertyName
      END TRY
      BEGIN CATCH
          select ERROR_MESSAGE() Message,
                 @tableName      TableName,
                 @GUID_Company   GUID_Company,
                 @PropertyName   PropertyName,
                 @Value          VALUE;
      END CATCH
  END

GO
