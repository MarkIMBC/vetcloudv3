﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pModel_AfterSaved_Schedule] (@ID_CurrentObject VARCHAR(10),
@IsNew BIT = 0)
AS
BEGIN

	DECLARE @ID_Schedule INT = 0
	DECLARE @TotalAccomodation INT = 0

	SET @ID_Schedule = CONVERT(INT, @ID_CurrentObject)

	SELECT
		@TotalAccomodation = COUNT(*)
	   ,@ID_Schedule = [as].ID_Schedule
	FROM tAppointmentSchedule [as]
	WHERE [as].ID_Schedule = @ID_Schedule
	GROUP BY [as].ID_Schedule
	
	UPDATE tSchedule
	SET AccommodateCount = @TotalAccomodation
	WHERE ID = @ID_Schedule

END

GO
