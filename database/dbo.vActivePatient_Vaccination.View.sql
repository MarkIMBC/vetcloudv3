﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Vaccination] AS   
				SELECT   
				 H.*  
				FROM vPatient_Vaccination H  
				WHERE IsActive = 1
GO
