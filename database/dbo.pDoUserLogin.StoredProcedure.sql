﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pDoUserLogin](@Username VARCHAR(300),
                        @Password VARCHAR(300))
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0
    DECLARE @ID_UserSession INT = 0

    BEGIN TRY
        EXEC pDoUserLogin_validation
          @Username,
          @Password

        SELECT @ID_User = ID
        FROM   tUser
        WHERE  username = @Username
               AND Password = @Password
               AND IsActive = 1

        INSERT INTO [dbo].[tUserSession]
                    ([Code],
                     [Name],
                     [IsActive],
                     [Comment],
                     [ID_User],
                     [ID_Warehouse],
                     [DateCreated])
        VALUES      (NULL,
                     NULL,
                     1,
                     '',
                     @ID_User,
                     1,
                     GetDate())

        SET @ID_UserSession = @@IDENTITY
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_',
           '' CurrentUser;

    SELECT @Success Success,
           @message message;

    SELECT _user.*,
           employee.LastName,
           employee.MiddleName,
           employee.FirstName,
           @ID_UserSession ID_UserSession,
           company.dbusername,
           company.dbpassword,
		   company.Code Code_Company
    FROM   vUser _user
           INNER JOIN temployee employee
                   ON _user.ID_Employee = employee.ID
           INNER JOIN tCompany company
                   ON employee.ID_Company = company.ID
    WHERE  _user.ID = @ID_User

GO
