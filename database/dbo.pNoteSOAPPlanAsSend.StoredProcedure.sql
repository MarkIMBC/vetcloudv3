﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT,
                                        @DateSent       DateTime)
AS
  BEGIN
      /*                    
       iTextMo Status                    
                         
       "1" = Invalid Number.                    
       "2" = Number prefix not supported. Please contact us so we can add.                    
       "3" = Invalid ApiCode.                    
       "4" = Maximum Message per day reached. This will be reset every 12MN.                    
       "5" = Maximum allowed characters for message reached.                    
       "6" = System OFFLINE.                    
       "7" = Expired ApiCode.                    
       "8" = iTexMo Error. Please try again later.                    
       "9" = Invalid Function Parameters.                    
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.                    
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.                    
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.                    
       "13" = Invalid or Not Registered Custom Sender ID.                    
       "14" = Invalid preferred server number.                    
       "15" = IP Filtering enabled - Invalid IP.                    
       "16" = Authentication error. Contact support at support@itexmo.com                    
       "17" = Telco Error. Contact Support support@itexmo.com                    
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com                    
       "19" = Account suspended. Contact Support support@itexmo.com                    
       "0" = Success! Message is now on queue and will be sent soon                   
      "-1" = Reach VetCloud SMS Count Limit                   
      "20" = Manual Sent SMS                
                    
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientAppointment_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1

      IF @DateSent IS NULL
        SET @DateSent = GETDATE()

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      select @PatientWaitingList_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatientWaitingList'

      SELECT @PatientAppointment_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatientAppointment'

      select @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientWaitingList_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientWaitingList
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientWaitingList_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientWaitingList,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @BillingInvoice_SMSPayableRemider_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tBillingInvoice_SMSPayableRemider
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tBillingInvoice_SMSPayableRemider_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_BillingInvoice_SMSPayableRemider,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientAppointment_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientAppointment
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientAppointment_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientAppointment,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO
