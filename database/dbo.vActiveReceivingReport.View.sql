﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveReceivingReport] AS   
				SELECT   
				 H.*  
				FROM vReceivingReport H  
				WHERE IsActive = 1
GO
