﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE    
 VIEW [dbo].[vMulticateInventoryTrail]  
as  
  select MIN(ID)  Min_ID_InventoryTrail,  
         MAX(ID)  Max_ID_InventoryTrail,  
   MIN(Date) Min_Date,  
   MAX(Date) Max_Date,  
         ID_Item,  
         Code,  
         Quantity,  
         Oid_Model_Reference,  
         ID_Reference,  
         ID_Company,  
         Count(*) Count  
  FROM   tInventoryTrail  WITH (NOLOCK)
  WHERE  Oid_Model_Reference IS NOT NULL  
         and ID_Reference IS NOT NULL  
  Group  by ID_Item,  
            Code,  
            Quantity,  
            Oid_Model_Reference,  
            ID_Reference,  
            ID_Company  
  HAVING COUNT(*) > 1  
GO
