﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetView]
    @Oid UNIQUEIDENTIFIER = NULL,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_';

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    IF (@Oid = NULL)
    BEGIN
        SELECT H.*
        FROM
        (
            SELECT NULL AS [_],
                   NULL AS [Oid],
                   NULL AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [ID_ListView],
                   NULL AS [ID_Report],
                   NULL AS [ID_Dashboard],
                   NULL AS [ID_ViewType],
                   NULL AS [CustomViewPath],
                   NULL AS [ControllerPath],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   NULL AS [DateModified],
                   NULL AS [ID_Model]
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM _vView H
        WHERE H.Oid = @Oid;
    END;
END;
GO
