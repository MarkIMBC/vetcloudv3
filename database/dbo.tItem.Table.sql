﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ItemType] [int] NULL,
	[ID_ItemCategory] [int] NULL,
	[MinInventoryCount] [int] NULL,
	[MaxInventoryCount] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[CurrentInventoryCount] [int] NULL,
	[Old_item_id] [int] NULL,
	[Old_procedure_id] [int] NULL,
	[OtherInfo_DateExpiration] [datetime] NULL,
	[ID_InventoryStatus] [int] NULL,
	[BarCode] [varchar](300) NULL,
	[CustomCode] [varchar](300) NULL,
	[_tempSupplier] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
	[SKUCode] [varchar](300) NULL,
	[SKU] [varchar](300) NULL,
	[UnitName] [varchar](300) NULL,
	[StockNumber] [varchar](300) NULL,
	[StockLocation] [varchar](300) NULL,
	[ProjectName] [varchar](300) NULL,
	[PONumber] [varchar](300) NULL,
	[DateDelivered] [datetime] NULL,
 CONSTRAINT [PK_tItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tItem] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tItem]  WITH CHECK ADD  CONSTRAINT [FK_tItem_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tItem] CHECK CONSTRAINT [FK_tItem_ID_Company]
GO
ALTER TABLE [dbo].[tItem]  WITH CHECK ADD  CONSTRAINT [FK_tItem_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tItem] CHECK CONSTRAINT [FK_tItem_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tItem]  WITH CHECK ADD  CONSTRAINT [FK_tItem_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tItem] CHECK CONSTRAINT [FK_tItem_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tItem]  WITH CHECK ADD  CONSTRAINT [FKtItem_ID_ItemCategory] FOREIGN KEY([ID_ItemCategory])
REFERENCES [dbo].[tItemCategory] ([ID])
GO
ALTER TABLE [dbo].[tItem] CHECK CONSTRAINT [FKtItem_ID_ItemCategory]
GO
ALTER TABLE [dbo].[tItem]  WITH CHECK ADD  CONSTRAINT [FKtItem_ID_ItemType] FOREIGN KEY([ID_ItemType])
REFERENCES [dbo].[tItemType] ([ID])
GO
ALTER TABLE [dbo].[tItem] CHECK CONSTRAINT [FKtItem_ID_ItemType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tItem]
		ON [dbo].[tItem]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tItem
			SET    DateCreated = GETDATE()
			FROM   dbo.tItem hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tItem] ENABLE TRIGGER [rDateCreated_tItem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tItem]
		ON [dbo].[tItem]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tItem
			SET    DateModified = GETDATE()
			FROM   dbo.tItem hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tItem] ENABLE TRIGGER [rDateModified_tItem]
GO
