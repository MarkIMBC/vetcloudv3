﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatientWaitingList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient] [int] NULL,
	[WaitingStatus_ID_FilingStatus] [int] NULL,
	[BillingInvoice_ID_FilingStatus] [int] NULL,
	[ID_Client] [int] NULL,
	[Code] [varchar](300) NULL,
	[Oid_Model_Reference] [varchar](300) NULL,
	[IsQueued] [bit] NULL,
	[ID_Reference] [int] NULL,
	[DateSent] [datetime] NULL,
	[IsSentSMS] [bit] NULL,
	[DateReschedule] [datetime] NULL,
 CONSTRAINT [PK_tPatientWaitingList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatientWaitingList] ON [dbo].[tPatientWaitingList]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatientWaitingList] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatientWaitingList]  WITH CHECK ADD  CONSTRAINT [FK_tPatientWaitingList_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatientWaitingList] CHECK CONSTRAINT [FK_tPatientWaitingList_ID_Company]
GO
ALTER TABLE [dbo].[tPatientWaitingList]  WITH CHECK ADD  CONSTRAINT [FK_tPatientWaitingList_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientWaitingList] CHECK CONSTRAINT [FK_tPatientWaitingList_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatientWaitingList]  WITH CHECK ADD  CONSTRAINT [FK_tPatientWaitingList_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientWaitingList] CHECK CONSTRAINT [FK_tPatientWaitingList_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatientWaitingList]
		ON [dbo].[tPatientWaitingList]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatientWaitingList
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatientWaitingList hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatientWaitingList] ENABLE TRIGGER [rDateCreated_tPatientWaitingList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatientWaitingList]
		ON [dbo].[tPatientWaitingList]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatientWaitingList
			SET    DateModified = GETDATE()
			FROM   dbo.tPatientWaitingList hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatientWaitingList] ENABLE TRIGGER [rDateModified_tPatientWaitingList]
GO
