﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pRemoveMulticatePatientConfinementFromSOAP](@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @IDs_Patient_Confinement typIntList
      DECLARE @ConfineItemServices TABLE
        (
           ID_Patient_Confinement               INT,
           ID_Patient_SOAP                      INT,
           ID_Patient_SOAP_Treatment            INT,
           ID_Patient_SOAP_Prescription         INT,
           ID_Patient_Confinement_ItemsServices INT,
           Count                                INT
        )

      INSERT @ConfineItemServices
             (ID_Patient_Confinement,
              ID_Patient_SOAP,
              ID_Patient_SOAP_Treatment,
              ID_Patient_Confinement_ItemsServices,
              Count)
      SELECT confItemServices.ID_Patient_Confinement,
             ID_Patient_SOAP,
             ID_Patient_SOAP_Treatment,
             MIN(confItemServices.ID) ID_Patient_Confinement_ItemsServices,
             COUNT(*)                 Count
      FRom   vPatient_Confinement_ItemsServices confItemServices
             inner join tPatient_SOAP _soap
                     on confItemServices.ID_Patient_SOAP = _soap.ID
             INNER JOIN @IDs_Patient_SOAP soapIDs
                     on soapIDs.ID = _soap.ID
      WHERE  ISNULL(ID_Patient_SOAP_Treatment, 0) > 0
             and _soap.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY confItemServices.ID_Patient_Confinement,
                ID_Patient_SOAP,
                ID_Patient_SOAP_Treatment
      HAVING COUNT(*) > 1

      INSERT @ConfineItemServices
             (ID_Patient_Confinement,
              ID_Patient_SOAP,
              ID_Patient_SOAP_Prescription,
              ID_Patient_Confinement_ItemsServices,
              Count)
      SELECT confItemServices.ID_Patient_Confinement,
             ID_Patient_SOAP,
             ID_Patient_SOAP_Prescription,
             MIN(confItemServices.ID) ID_Patient_Confinement_ItemsServices,
             COUNT(*)                 Count
      FRom   vPatient_Confinement_ItemsServices confItemServices
             inner join tPatient_SOAP _soap
                     on confItemServices.ID_Patient_SOAP = _soap.ID
             INNER JOIN @IDs_Patient_SOAP soapIDs
                     on soapIDs.ID = _soap.ID
      WHERE  ISNULL(ID_Patient_SOAP_Prescription, 0) > 0
             and _soap.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY confItemServices.ID_Patient_Confinement,
                ID_Patient_SOAP,
                ID_Patient_SOAP_Prescription

      DELETE FROM tPatient_Confinement_ItemsServices
      WHERE  ID IN (SELECT ID_Patient_Confinement_ItemsServices
                    FROM   @ConfineItemServices
                    WHERE  Count > 1)

      EXEC dbo.pModel_AfterSaved_Patient_Confinement_Computation
        @IDs_Patient_Confinement

      EXEC dbo.pUpdateBillingInvoiceItemsByPatientConfinement
        @IDs_Patient_Confinement
  END

GO
