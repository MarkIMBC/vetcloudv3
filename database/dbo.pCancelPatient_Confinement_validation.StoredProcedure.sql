﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelPatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @ForBilling_ID_FilingStatus INT = 16;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotDischarged TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30),
		   BillingInvoice_Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotDischarged INT = 0;

      /* Validate Patient_Confinement Status is not Discharged*/
      INSERT @ValidateNotDischarged
             (Code,
              Name_FilingStatus, BillingInvoice_Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus,
			 BillingInvoice_Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus IN ( @Discharged_ID_FilingStatus, @Confined_ID_FilingStatus )
             and ISNULL(BillingInvoice_ID_FilingStatus, 0) NOT IN (0,  @ForBilling_ID_FilingStatus );

      SELECT @Count_ValidateNotDischarged = COUNT(*)
      FROM   @ValidateNotDischarged;

      IF ( @Count_ValidateNotDischarged > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotDischarged > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to Canceled:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus + ' (BI - '+ BillingInvoice_Name_FilingStatus +')'
            FROM   @ValidateNotDischarged;

            THROW 50001, @message, 1;
        END;
  END; 
GO
