﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePayable] AS   
				SELECT   
				 H.*  
				FROM vPayable H  
				WHERE ISNULL(IsActive, 0) = 0
GO
