﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout] (@ID_PaymentTransaction INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Guid_Company VARCHAR(MAX)= ''
      DECLARE @cardinalpetclinic_Guid_Company VARCHAR(MAX)= 'D91D497F-6108-4D50-88A2-BBC73C81A86D'
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @CustomCode_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0
      DECLARE @ID_BillingInvoice INT = 0

      SELECT @ID_Company = ID_Company,
             @ID_BillingInvoice = ID_BillingInvoice,
             @ID_PaymentMode = ID_PaymentMethod,
             @Code_PaymentTranaction = Code,
             @PayableAmount = PayableAmount,
             @RemainingAmount = RemainingAmount,
             @ChangeAmount = ChangeAmount,
             @CashAmount = CashAmount,
             @ReferenceTransactionNumber = ISNULL(ReferenceTransactionNumber, ''),
             @GCashAmount = GCashAmount,
             @Name_CardType = ISNULL(Name_CardType, ''),
             @CardNumber = ISNULL(CardNumber, ''),
             @CardHolderName = ISNULL(CardHolderName, ''),
             @CardAmount = ISNULL(CardAmount, 0),
             @CheckNumber = ISNULL(CheckNumber, 0),
             @CheckAmount = ISNULL(CheckAmount, 0),
             @PaymentAmount = ISNULL(PaymentAmount, 0)
      FROM   vPaymentTransaction
      WHERE  ID = @ID_PaymentTransaction

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px')
      FROM   vCompany
      WHERE  ID = @ID_Company

      SELECT @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = ISNULL(Code, ''),
             @CustomCode_BillingInvoice = ISNULL(CustomCode, ''),
             @TotalItemDiscountAmount_BillingInvoice = ISNULL(TotalItemDiscountAmount, 0),
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      DECLARE @style VARCHAR(MAX)= '                            
  body{                            
   margin: 0px;                            
   padding: 0px;                            
   font-family:  arial, sans-serif;                            
   font-weight: normal;                            
   font-style: normal;                            
   font-size: ' + @POSReceiptFontSize
        + ';                            
  }                            
                            
  .logo{                            
                            
   width: 120px;                            
   margin-bottom: 10px;                            
  }                            
                            
  .receipt-container{                            
     width: 100vw;                        
  position: relative;                        
  left: 50%;                        
  right: 50%;                        
  margin-left: -50vw;                        
  margin-right: -50vw;                        
  }                            
                            
  .company-logo{                            
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                            
   text-align: center;                            
   word-wrap: break-word;                            
   margin-bottom: 12px;                            
  }                            
                            
  .company-name{                            
   display: block;                            
   text-align: center;                            
   word-wrap: break-word;                            
  }                            
                            
  .company-address{                            
   display: block;                            
   text-align: center;                            
   word-wrap: break-word;                            
   font-size: 11px                            
  }                            
       
  .company-contactnum-container{                            
   display: block;                            
   text-align: center;                            
   word-wrap: break-word;                            
   font-size: 11px                            
  }                            
                            
  .company-contactnum{                            
   text-align: center;                            
   word-wrap: break-word;                            
                            
  }                            
                
  .float-left{                            
   float: left;                            
  }                            
                            
  .float-right{                            
   float: right;                            
  }                            
                  
  .hide{           
 display: none;          
  }          
                               
  .clearfix {                            
   overflow: auto;                  
  }                            
                            
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                         
                            
  .display-block{                            
  word-wrap: break-word;                        
 display: block;                            
  }                            
                            
  .title{                            
   text-align: center;                            
   word-wrap: break-word;                            
   display: block;                            
   padding-top: 15px;                            
   padding-bottom: 15px;                            
  }                            
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'

      IF( @IsShowHeader = 1 )
        BEGIN
            /*Billing Invoice Layout*/
            IF ( @IsShowPOSReceiptLogo = 1 )
              BEGIN
                  SET @content = @content
                                 + '<div class="company-logo"><img src="'
                                 + @ImageLogoLocationFilenamePath_Company
                                 + '" class="logo"></div>'
              END

            SET @content = @content + '<center><div class="bold">'
                           + @Name_Company + '</div>'
            SET @content = @content + '<div>' + @Address_Company + '</div>'
            SET @content = @content + '<div>' + @ContactNumber_Company
                           + '</div></center>'
        END

      SET @content = @content
                     + '<div class="title bold">INVOICE</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', ISNULL(FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'), ''));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('BI #:', @CustomCode_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(ISNULL(Quantity, 0), '#,#0'), FORMAT(ISNULL(Amount, 0), '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      Declare @DiscountWord VARCHAR(MAX) = 'Disc.'

      IF @Guid_Company IN ( @cardinalpetclinic_Guid_Company )
        BEGIN
            SET @DiscountWord = 'Discount'
        END

      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Item '+ @DiscountWord +' Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));
      /*PAYMENT*/
      SET @content = @content
                     + '<div class="title bold">PAYMENT</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('PT #', @Code_PaymentTranaction);

      /*----------------PAYMENT METHODS--------------------*/
      IF( @ID_PaymentMode = @Cash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Cash Amount', FORMAT(@CashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @GCash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Ref. No.', @ReferenceTransactionNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('G-Cash Amount', FORMAT(@GCashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Type', @Name_CardType);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card #', @CardNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Holder', @CardHolderName);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Amt.', FORMAT(@CardAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @Check_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check No.', @CheckNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check Amt.', FORMAT(@CheckAmount, '#,#0.00'));
        END

      /*----------------PAYMENT METHODS END----------------*/
      SET @content = @content + '<br/>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Balance', FORMAT(@PayableAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Payment', FORMAT(@PaymentAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining', FORMAT(@RemainingAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Change', FORMAT(@ChangeAmount, '#,#0.00'));

      /*END Main Layout */
      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO
