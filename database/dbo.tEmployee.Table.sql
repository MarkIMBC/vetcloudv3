﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tEmployee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ImageFile] [varchar](300) NULL,
	[ID_Department] [int] NULL,
	[ID_EmployeePosition] [int] NULL,
	[ID_Position] [int] NULL,
	[LastName] [varchar](300) NULL,
	[FirstName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[ID_Gender] [varchar](300) NULL,
	[ID_EmployeeStatus] [varchar](300) NULL,
	[FullAddress] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[ContactNumber] [varchar](300) NULL,
	[Name] [varchar](300) NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](300) NULL,
	[IsSystemUsed] [bit] NULL,
	[PRCLicenseNumber] [varchar](300) NULL,
	[PTR] [varchar](300) NULL,
	[S2] [varchar](300) NULL,
	[TINNumber] [varchar](300) NULL,
	[DatePRCExpiration] [datetime] NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tEmployee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tEmployee]  WITH NOCHECK ADD  CONSTRAINT [FKtEmployee_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tEmployee] CHECK CONSTRAINT [FKtEmployee_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tEmployee]  WITH CHECK ADD  CONSTRAINT [FKtEmployee_ID_Position] FOREIGN KEY([ID_Position])
REFERENCES [dbo].[tPosition] ([ID])
GO
ALTER TABLE [dbo].[tEmployee] CHECK CONSTRAINT [FKtEmployee_ID_Position]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tEmployee]
		ON [dbo].[tEmployee]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tEmployee
			SET    DateCreated = GETDATE()
			FROM   dbo.tEmployee hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tEmployee] ENABLE TRIGGER [rDateCreated_tEmployee]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tEmployee]
		ON [dbo].[tEmployee]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tEmployee
			SET    DateModified = GETDATE()
			FROM   dbo.tEmployee hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tEmployee] ENABLE TRIGGER [rDateModified_tEmployee]
GO
