﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Confinement] AS   
				SELECT   
				 H.*  
				FROM vPatient_Confinement H  
				WHERE IsActive = 1
GO
