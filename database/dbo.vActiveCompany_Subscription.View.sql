﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompany_Subscription] AS   
				SELECT   
				 H.*  
				FROM vCompany_Subscription H  
				WHERE IsActive = 1
GO
