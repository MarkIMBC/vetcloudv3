﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vItemInventoriable_ListvIew]
AS
  SELECT item.ID,
         item.Code,
         item.Name,
         CASE
           WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN
             CASE
               WHEN item.OtherInfo_DateExpiration > GETDATE() THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
               ELSE dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'expired', 'Expired')
             END
           ELSE ''
         END                                                     RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
         item.OtherInfo_DateExpiration                           DateExpired,
         item.ID_Company,
         item.CurrentInventoryCount,
         item.ID_InventoryStatus,
         invtStatus.Name                                         Name_InventoryStatus,
         item.IsActive,
         item.UnitCost,
         Item.UnitPrice,
         item.OtherInfo_DateExpiration,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '')                      Name_ItemCategory
  FROM   dbo.vitem item
         LEFT JOIN tInventoryStatus invtStatus
                on item.ID_InventoryStatus = invtStatus.ID
  WHERE  item.ID_ItemType = 2
         AND ISNULL(item.IsActive, 0) = 1

GO
