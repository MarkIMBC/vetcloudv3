﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUndoCancelPatient_SOAP] (@IDs_Patient_SOAP typIntList READONLY,
                                        @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @IDs_Patient typIntList

          INSERT @IDs_Patient
          SELECT DISTINCT ID_Patient
          FROM   tPatient_SOAP soap
                 INNER JOIN @IDs_Patient_SOAP tbl
                         ON soap.ID = tbl.ID

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pUndoCancelPatient_SOAP_validation
            @IDs_Patient_SOAP,
            @ID_UserSession;

          UPDATE dbo.tPatient_SOAP
          SET    ID_FilingStatus = 1
          FROM   dbo.tPatient_SOAP bi
                 INNER JOIN @IDs_Patient_SOAP ids
                         ON bi.ID = ids.ID;

          exec dbo.pUpdatePatientsLastVisitedDate
            @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
GO
