﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertcustomReportTitleByCompanyGuidReportName](@GUID_Company      VARCHAR(MAX),
                                                           @Name_Report       VARCHAR(MAX),
                                                           @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company
             AND IsActive = 1
		
      EXEC dbo.pInsertcustomReportTitleByReportName
        @ID_Company,
        @Name_Report,
        @customReportTitle
  END

GO
