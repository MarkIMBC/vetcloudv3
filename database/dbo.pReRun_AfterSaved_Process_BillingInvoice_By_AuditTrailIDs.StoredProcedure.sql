﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 proc [dbo].[pReRun_AfterSaved_Process_BillingInvoice_By_AuditTrailIDs](@IDs_AuditTrail typIntList ReadOnly)
AS
  BEGIN
      BEGIN TRY
          DECLARE @TranName VARCHAR(MAX);
          DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
          DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

          SET @TranName = 'pReRun_AfterSaved_Process_BillingInvoice-'
                          + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

          BEGIN TRANSACTION @TranName;

          Update tAuditTrail
          SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
          FROm   tAuditTrail auditTrail
                 inner join _tModel model
                         on model.Oid = auditTrail.ID_Model
                 INNER JOIN @IDs_AuditTrail ids
                         ON auditTrail.ID = ids.ID
          WHERE  TableName = 'tBillingInvoice'

          DECLARE @IDs_CurrentObject typIntList

          INSERT @IDs_CurrentObject
          SELECT DISTINCT ID_CurrentObject
          FROm   tAuditTrail auditTrail
                 inner join @IDs_AuditTrail ids
                         on auditTrail.ID = ids.ID

          IF(SELECT COUNT(*)
             FROM   @IDs_CurrentObject) > 0
            BEGIN
                DECLARE @IDs_BillingINvoice TYPINTLIST
                /*Update Confinement Bill Status*/
                DECLARE @IDs_Patient_Confinement TYPINTLIST

                INSERT @IDs_Patient_Confinement
                SELECT DISTINCT ID_Patient_Confinement
                FROM   tBillingInvoice bi
                       inner join @IDs_CurrentObject ids
                               on bi.ID = ids.ID

                EXEC PupDatePatient_ConfineMen_BillIngStatus
                  @IDs_Patient_Confinement

                /*Update SOAP Bill Status*/
                DECLARE @IDs_Patient_SOAP TYPINTLIST

                INSERT @IDs_Patient_SOAP
                SELECT DISTINCT ID_Patient_SOAP
                FROM   tBillingInvoice bi
                       inner join @IDs_CurrentObject ids
                               on bi.ID = ids.ID

                exec dbo.pModel_AfterSaved_BillingInvoice_Computation
                  @IDs_BillingInvoice

                EXEC pUpdatePatient_SOAP_BillingStatus
                  @IDs_Patient_SOAP

                Update tAuditTrail
                SET    IsRunAfterSavedProcess = 1,
                       DateRunAfterSavedProcess = GETDATE(),
                       RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                FROm   tAuditTrail auditTrail
                       inner join _tModel model
                               on model.Oid = auditTrail.ID_Model
                       INNER JOIN @IDs_AuditTrail ids
                               ON auditTrail.ID = ids.ID
                WHERE  TableName = 'tBillingInvoice'
                       and ISNULL(auditTrail.IsRunAfterSavedProcess, 0) = 0
            END

          COMMIT TRANSACTION @TranName
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION @TranName;
          Update tAuditTrail
          SET    RunAfterSavedProcess_ID_FilingStatus = NULL
          FROm   tAuditTrail auditTrail
                 inner join _tModel model
                         on model.Oid = auditTrail.ID_Model
                 INNER JOIN @IDs_AuditTrail ids
                         ON auditTrail.ID = ids.ID
          WHERE  TableName = 'tBillingInvoice'
      END CATCH
  END

GO
