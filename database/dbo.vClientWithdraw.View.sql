﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vClientWithdraw]
AS
  SELECT H.*,
         confinement.Name_Client,
         patient.Name      Name_Patient,
         UC.Name           AS CreatedBy,
         UM.Name           AS LastModifiedBy,
         approvedUser.Name AS ApprovedBy_Name_User,
         cancelUser.Name   AS CanceledBy_Name_User,
         fs.Name           Name_FilingStatus,
         confinement.Code  Code_Patient_Confinement
  FROM   tClientWithdraw H
         LEFT JOIN vPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.ID_FilingStatus
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN tPatient patient
                ON patient.ID = h.ID_Patient

GO
