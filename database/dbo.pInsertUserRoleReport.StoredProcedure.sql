﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertUserRoleReport] (@Name_UserRole VARCHAR(MAX),
                                  @Name_Report   VARCHAR(MAX))
AS
  BEGIN
      DECLARE @ID_UserRole INT = 0
      DECLARE @Oid_Report VARCHAR(MAX) = ''

      SELECT @Oid_Report = Oid
      FROM   _tReport
      where  Name = @Name_Report

      IF(SELECT COUNT(*)
         FROM   tUserRole
         where  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess])
            SELECT @Name_UserRole,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   0;
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      if(SELECT COUNT(*)
         FROM   tUserRole_Reports
         where  ID_Report = @Oid_Report
                AND ID_UserRole = @ID_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Reports]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Report])
            SELECT @Name_Report,
                   1,
                   @ID_UserRole,
                   @Oid_Report
        END
  END

GO
