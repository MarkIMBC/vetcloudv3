﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveViewType] AS   
				SELECT   
				 H.*  
				FROM vViewType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
