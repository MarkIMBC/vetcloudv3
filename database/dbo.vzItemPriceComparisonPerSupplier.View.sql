﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzItemPriceComparisonPerSupplier]
AS
  select isupplier.ID,
         isupplier.ID_Item,
         item.Name                     Name_Item,
         supplier.Name                 Name_Supplier,
         ISNULL(isupplier.UnitCost, 0) UnitCost_Item_Supplier,
         company.ID                    ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                  Name_Company,
         company.Address               Address_Company,
         company.ContactNumber         ContactNumber_Company,
         CASE
           WHEN LEN(ISNULL(company.Address, '')) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(ISNULL(company.ContactNumber, '')) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(company.Email, '')) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                         HeaderInfo_Company
  FROM   tItem_Supplier isupplier
         INNER JOIN tItem item
                 ON item.ID = isupplier.ID_Item
         INNER JOIN tSupplier supplier
                 ON isupplier.ID_Supplier = supplier.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company

GO
