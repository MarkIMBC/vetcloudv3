﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddUserRoleReport](@Name_UserRole VARCHAR(MAX),
                              @Name_Report   VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Report VARCHAR(MAX) = '';
      DECLARE @ID_UserRole INT = 0;

      IF(SELECT COUNT(*)
         FROm   tUserRole
         WHERE  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess],
                         [IsAdministrator])
            VALUES      (@Name_UserRole,
                         1,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         0,
                         0)

            exec _pCreateReportView
              @Name_Report,
              1
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      SELECT @ID_Report = Oid
      FROM   _tReport
      where  Name = @Name_Report

      if(select COUNT(*)
         FROM   tUserRole_Reports
         WHERE  ID_UserRole = @ID_UserRole
                AND ID_Report = @ID_Report) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Reports]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Report])
            VALUES      (NULL,
                         1,
                         @ID_UserRole,
                         @ID_Report)
        END

      declare @IDs_UserAdministrator typIntlist

      INSERT @IDs_UserAdministrator
      select ID_User
      FROm   vCompanyActiveUserAdministrator WITH (NOLOCK)
      EXCEPT
      SELECT ID_User
      FROM   tUser_Roles WITH (NOLOCK)
      WHERE  ID_UserRole = @ID_UserRole

      INSERT INTO [dbo].[tUser_Roles]
                  ([IsActive],
                   [ID_User],
                   [ID_UserRole])
      SELECT 1,
             ID,
             @ID_UserRole
      FROM   @IDs_UserAdministrator
  END

GO
