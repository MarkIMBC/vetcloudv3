﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[_solo_pBackUpDatabase] @Comment VARCHAR(MAX) = NULL
AS
    DECLARE @name VARCHAR(50) -- database name            
    DECLARE @path VARCHAR(500) -- path for backup files            
    DECLARE @fileName VARCHAR(256) -- filename for backup            
    DECLARE @fileDate VARCHAR(20) -- used for file name          
    DECLARE @databaseName VARCHAR(100) = DB_NAME()

    SET @path = dbo.fGetBackUpPath()

    SELECT @fileDate = CONVERT(VARCHAR(20), GETDATE(), 112)
                       + REPLACE(CONVERT(VARCHAR(8), GETDATE(), 108), ':', '')

    SET @fileName = @path + @name + '_' + @fileDate + '.BAK'

    DECLARE db_cursor CURSOR FOR
      SELECT name
      FROM   master.dbo.sysdatabases
      WHERE  name = DB_NAME()

    OPEN db_cursor

    FETCH NEXT FROM db_cursor INTO @name

    WHILE @@FETCH_STATUS = 0
      BEGIN
          SET @fileName = @path + @name + '_' + @fileDate + '.BAK'

          --DBCC SHRINKDATABASE(@name)          
          BACKUP DATABASE @name TO DISK = @fileName

          FETCH NEXT FROM db_cursor INTO @name
      END

    CLOSE db_cursor

    DEALLOCATE db_cursor

GO
