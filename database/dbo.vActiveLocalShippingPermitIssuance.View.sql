﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveLocalShippingPermitIssuance] AS   
				SELECT   
				 H.*  
				FROM vLocalShippingPermitIssuance H  
				WHERE IsActive = 1
GO
