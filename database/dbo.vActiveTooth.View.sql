﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTooth] AS   
				SELECT   
				 H.*  
				FROM vTooth H  
				WHERE IsActive = 1
GO
