﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSalesReturn] AS   
				SELECT   
				 H.*  
				FROM vSalesReturn H  
				WHERE ISNULL(IsActive, 0) = 0
GO
