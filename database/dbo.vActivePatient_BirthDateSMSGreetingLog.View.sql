﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_BirthDateSMSGreetingLog] AS   
				SELECT   
				 H.*  
				FROM vPatient_BirthDateSMSGreetingLog H  
				WHERE IsActive = 1
GO
