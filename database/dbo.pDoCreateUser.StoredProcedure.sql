﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoCreateUser](@CompanyID   INT,
                         @ID_Employee INT,
                         @Username    varchar(MAX))
as
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''

      BEGIN TRY
          exec pCreateUser
            @CompanyID,
            @ID_Employee,
            @Username
      END TRY
      BEGIN CATCH
          SET @Success = 0
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_'

      SELECT @Success Success,
             @message message
  END

GO
