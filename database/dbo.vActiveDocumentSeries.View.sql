﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveDocumentSeries] AS   
				SELECT   
				 H.*  
				FROM vDocumentSeries H  
				WHERE IsActive = 1
GO
