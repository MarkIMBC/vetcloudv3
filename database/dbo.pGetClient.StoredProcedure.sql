﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGetClient] @ID         INT = -1,
                              @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Client_Patient;

      DECLARE @ID_User INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @UserName VARCHAR(MAX);
      DECLARE @Password VARCHAR(MAX);
      DECLARE @CommentTemplate VARCHAR(MAX)= '' + 'Owner''s birthday: ' + CHAR(9) + CHAR(13)
        + 'Occupation: ' + CHAR(9) + CHAR(13)
        + 'How did you find out about our clinic? : '
        + CHAR(9) + CHAR(13) + 'Reffered by: ' + CHAR(9)
        + CHAR(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserName = _user.Username,
             @Password = _user.Password
      FROM   tClient client
             inner join tUser _user
                     on client.ID_User = _user.ID
      WHERE  client.ID = @ID
             and _user.IsActive = 1

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL             AS [_],
                           -1               AS [ID],
                           NULL             AS [Code],
                           NULL             AS [Name],
                           1                AS [IsActive],
                           ''               [ContactNumber],
                           ''               [ContactNumber2],
                           NULL             AS [ID_Company],
                           NULL             AS [Comment],
                           NULL             AS [DateCreated],
                           NULL             AS [DateModified],
                           NULL             AS [ID_CreatedBy],
                           NULL             AS [ID_LastModifiedBy],
                           @UserName        AS [Username],
                           @Password        AS [Password],
                           @CommentTemplate AS CommentTemplate) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @CommentTemplate CommentTemplate
            FROM   dbo.vClient H
            WHERE  H.ID = @ID
        END;

      SELECT patient.*
      FROM   dbo.vPatient patient
      WHERE  patient.ID_Client = @ID
             AND ISNULL(patient.IsActive, 0) = 1
             AND ID_Company = @ID_Company
  END;

GO
