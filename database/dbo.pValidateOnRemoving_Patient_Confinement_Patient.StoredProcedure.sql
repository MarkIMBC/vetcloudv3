﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pValidateOnRemoving_Patient_Confinement_Patient] (@ID_Patient_Confinement INT,
                                                            @ID_Patient             INT)
AS
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(400) = '';

      BEGIN TRY
          DECLARE @msg VARCHAR(400) = '';
          Declare @hasApprovedClientDeposit TABLE
            (
               Code_ClientDeposit VARCHAR(Max),
               DepositAmount      Decimal(18, 2),
               Name_Patient       VARCHAR(MAX),
               Name_FilingStatus  VARCHAR(MAX)
            )
          Declare @Count_hasApprovedClientDeposit INT

          INSERT @hasApprovedClientDeposit
          SELECT Distinct cDeposit.Code,
                          cDeposit.DepositAmount,
                          patient.Name Name_Patient,
                          fs.Name      Name_FilingStatus
          FROm   tClientDeposit cDeposit
                 left join tpatient patient
                        on cDeposit.ID_Patient = patient.ID
                 LEFT JOIN tFilingStatus fs
                        on fs.ID = cDeposit.ID_FilingStatus
          where  cDeposit.ID_FilingStatus IN ( 3 )
                 and cDeposit.ID_Patient_Confinement = @ID_Patient_Confinement
                 and cDeposit.ID_Patient = @ID_Patient

          SELECT @Count_hasApprovedClientDeposit = count(*)
          FROm   @hasApprovedClientDeposit

          IF @Count_hasApprovedClientDeposit > 0
            BEGIN
                SET @msg = 'The following Patient'
                           + CASE
                               WHEN @Count_hasApprovedClientDeposit > 1 THEN 's are '
                               ELSE ' is '
                             END
                           + 'not allowed to removed:';

                SELECT @msg = @msg + CHAR(10) + Code_ClientDeposit + ' - '
                              + Name_FilingStatus
                FROM   @hasApprovedClientDeposit;

                THROW 50001, @msg, 1;
            END
      ------------------------------------------------------------------------------
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message
  END

GO
