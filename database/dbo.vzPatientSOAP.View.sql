﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzPatientSOAP]
AS
  SELECT patientSOAP.ID,
         patientSOAP.Code,
         patientSOAP.Name_Client,
         patientSOAP.Name_Patient,
         patientSOAP.Date,
         patientSOAP.Comment,
         patientSOAP.Name_CreatedBy,
         patient.Name_Gender,
         patient.Species,
         patientSOAP.Weight,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                   Age_Patient,
         REPLACE(REPLACE(REPLACE(patientSOAP.History, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>')             History,
         REPLACE(REPLACE(REPLACE(patientSOAP.ClinicalExamination, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClinicalExamination,
         dbo.fGet_vzPatientSOAP_LaboratoryRemarks(patientSOAP.ID)
         + CASE
             WHEN LEN(ISNULL(patientSOAP.Interpretation, '')) > 0 THEN CHAR(13) + CHAR(13)
                                                                       + ISNULL(patientSOAP.Interpretation, '')
             ELSE ''
           END                                                                                                                        LaboratoryRemarks,
         dbo.fGetPatient_SOAP_String(patientSOAP.ID)                                                                                  Planning,
         REPLACE(REPLACE(REPLACE(patientSOAP.Diagnosis, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>')           Diagnosis,
         dbo.fGetPatient_SOAP_Treatment_String(patientSOAP.ID)                                                                        Treatment,
         dbo.fGetPatient_SOAP_Prescription_String(patientSOAP.ID)                                                                     Prescription,
         REPLACE(REPLACE(REPLACE(patientSOAP.ClientCommunication, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClientCommunication,
         REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>')          Subjective,
         REPLACE(REPLACE(REPLACE(patientSOAP.Objective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>')           Objective,
         REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>')                                                Assessment,
         patientSOAP.ID_Company,
         patientSOAP.Name_FilingStatus,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                                                                                 Name_Company,
         company.Address                                                                                                              Address_Company,
         company.ContactNumber                                                                                                        ContactNumber_Company,
         CASE
           WHEN LEN(ISNULL(company.Address, '')) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(ISNULL(company.ContactNumber, '')) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(company.Email, '')) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                        HeaderInfo_Company,
         emp.PRCLicenseNumber                                                                                                         AttendingPhysician_PRCLicenseNumber_Employee,
         emp.PTR                                                                                                                      AttendingPhysician_PTR_Employee,
         emp.TINNumber                                                                                                                AttendingPhysician_TINNumber_Employee,
         emp.S2                                                                                                                       AttendingPhysician_S2_Employee,
         patientSOAP.AttendingPhysician_Name_Employee                                                                                 AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_SOAP patientSOAP
         LEFT JOIN dbo.vPatient patient
                ON patientSOAP.ID_Patient = patient.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = patientSOAP.ID_Company
         LEFT JOIN tEmployee emp
                ON patientSOAP.AttendingPhysician_ID_Employee = emp.ID

GO
