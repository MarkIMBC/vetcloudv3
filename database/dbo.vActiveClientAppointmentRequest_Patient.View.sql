﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClientAppointmentRequest_Patient] AS   
				SELECT   
				 H.*  
				FROM vClientAppointmentRequest_Patient H  
				WHERE IsActive = 1
GO
