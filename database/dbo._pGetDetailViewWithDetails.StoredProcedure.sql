﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetDetailViewWithDetails] @Oid UNIQUEIDENTIFIER
AS
	SELECT
		'' AS [_]
	   ,'' AS [Details]

	SELECT
		H.Oid
	   ,H.Code
	   ,H.Name
	   ,H.IsActive
	   ,H.Comment
	   ,
		--H.Caption ,
		H.ID_CreatedBy
	   ,H.ID_LastModifiedBy
	   ,H.DateCreated
	   ,H.DateModified
	   ,H.ID_Model
	   ,M.Name AS ModelName
	   ,ISNULL(H.Caption, ISNULL(M.Caption, M.Name)) AS Caption
	FROM dbo.[_tDetailView] H
	LEFT JOIN dbo.[_tModel] M
		ON H.ID_Model = M.Oid
	WHERE H.Oid = @Oid
	ORDER BY H.DateCreated, H.Oid


	SELECT
		ISNULL(D.SeqNo, 1000) AS SeqNo
	   ,ISNULL(MP.ID_PropertyType, D.ID_PropertyType) AS ID_PropertyType
	   ,ISNULL(D.ValueExpr, 'ID') AS ValueExpr
	   ,ISNULL(D.DisplayExpr, 'Name') AS ValueExpr
	   ,ISNULL(D.GroupIndex, -1) AS GroupIndex
	   ,D.DataSource AS DataSourceKey
	   , --[$DataSourceKey],
		(CASE
			WHEN D.IsLoadData = 1 THEN D.DataSource
			ELSE NULL
		END) AS [DataSource]
	   ,ISNULL(MP.DisplayProperty, D.DisplayProperty) AS DisplayProperty
	   ,D.*
	   ,MP.ID_PropertyModel
	   ,MP.Name AS PropertyName
	   ,M.Name AS PropertyModel
	   ,
		---Start DetailView
		M.ID_DetailView AS ID_PropertyModel_DetailView
	   ,lV_D.Height AS ID_PropertyModel_DetailView_Height
	   ,lV_D.Width AS ID_PropertyModel_DetailView_Width
	   ,
		---End DetailView
		M.PrimaryKey AS PropertyModel_PrimaryKey
	   ,ISNULL(M.Name, M.Caption) AS [PropertyModel_Caption]
	   ,M.ID_DetailView AS [ID_PropertyModel_ID_DetailView]
	   ,D.LookUp_ListView_DataSource AS LookUpDataSource
	   , --[$LookUpDataSource],
		L_LV_M.PrimaryKey AS LookUp_ListView_PK
	   ,ISNULL(L_LV_M.Caption, L_LV_M.Name) AS LookUp_ListView_Model
	   ,MP.ID_ModelProperty_Key
	   ,MPK.Name AS ModelProperty_Key
	   ,L_D.IsAllowAdd
	   ,L_D.Name AS ListViewDetailName
	FROM dbo.[_tDetailView_Detail] D
	LEFT JOIN dbo.[_tModel_Property] MP
		ON D.ID_ModelProperty = MP.Oid
	LEFT JOIN dbo.[_vModel] M
		ON MP.ID_PropertyModel = M.Oid
	LEFT JOIN dbo.[_tListView] L_D
		ON D.ID_ListView = L_D.Oid
	LEFT JOIN dbo.[_tListView] L_LV
		ON L_LV.Oid = D.ID_LookUp_ListView
	LEFT JOIN dbo.[_vModel] L_LV_M
		ON L_LV.ID_Model = L_LV_M.Oid
	LEFT JOIN dbo.[_tModel_Property] MPK
		ON MP.ID_ModelProperty_Key = MPK.Oid
	LEFT JOIN dbo.[_tDetailView] lV_D
		ON M.ID_DetailView = lV_D.Oid
	WHERE D.ID_DetailView = @Oid
	AND D.Name NOT IN ('Oid', 'ID')
	AND D.IsActive = 1
	ORDER BY D.DateCreated, d.Oid

GO
