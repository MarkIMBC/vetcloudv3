﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CREATE    
 PROC [dbo].[pGetForBillingBillingInvoiceRecords_Patient_Wellness](@IDs_Patient_Wellness        typIntList READONLY,  
                                               @IDs_Patient_Confinement typIntList READONLY)  
as  
    DECLARE @Patient_Wellness_ID_Model VARCHAR(MAX) = ''  
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''  
  
    SELECT @Patient_Wellness_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Wellness'  
  
    SELECT @Patient_Confinement_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Confinement'  
  
    DECLARE @record TABLE  
      (  
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,  
         [RefNo]            [varchar](50) NULL,  
         [ID_CurrentObject] [int] NOT NULL,  
         [Oid_Model]        [uniqueidentifier] NOT NULL,  
         [1]                VARCHAR(MAX) NULL,  
         [2]                VARCHAR(MAX) NULL,  
         [3]                VARCHAR(MAX) NULL,  
         [4]                VARCHAR(MAX) NULL,  
         [5]                VARCHAR(MAX) NULL,  
         [6]                VARCHAR(MAX) NULL,  
         [7]                VARCHAR(MAX) NULL,  
         [8]                VARCHAR(MAX) NULL,  
         [9]                VARCHAR(MAX) NULL,  
         [10]               VARCHAR(MAX) NULL  
      )  
  
    /*Patient hed*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_Wellness_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_Wellness,  
                   Code_Patient_Wellness,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_Wellness,  
                           hed.Code                                            Code_Patient_Wellness,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_Wellness  
                               ORDER BY ID_Patient_Wellness ASC, bi.ID)             BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_Wellness hed  
                                   ON bi.ID_Patient_Wellness = hed.ID  
                           INNER JOIN @IDs_Patient_Wellness ids  
                                   on hed.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_Wellness, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    SELECT '_',  
           '' BillingInvoices  
  
    SELECT GETDate() Date  
  
    SELECT *  
    FROM   @record  
  

GO
