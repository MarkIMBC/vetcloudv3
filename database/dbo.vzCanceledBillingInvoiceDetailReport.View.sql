﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 view [dbo].[vzCanceledBillingInvoiceDetailReport]
AS
  select --bi.ID,
	company.Name Name_Company,
         bi.Date,
         bi.Code,
         DateCanceled,
         CanceledBy_Name_User,
         ISNULL(bi.Name_Client, bi.WalkInCustomerName) Name_Client,
         bi.PatientNames,
         biDetail.Name_Item,
         Quantity,
         UnitPrice,
         biDetail.DiscountAmount,
         biDetail.Amount
         --biDetail.ID     ID_BillingInvoice_Detail,
         --company.ImageLogoLocationFilenamePath,
         --company.Name    Name_Company,
         --company.Address Address_Company,
         --CASE
         --  WHEN len(company.Address) > 0 THEN '' + company.Address
         --  ELSE ''
         --END
         --+ CASE
         --    WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
         --    ELSE ''
         --  END
         --+ CASE
         --    WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
         --    ELSE ''
         --  END           HeaderInfo_Company
  FROM   vBillingInvoice bi
         inner join vBillingInvoice_Detail biDetail
                 on bi.ID = biDetail.ID_BillingInvoice
         INNER JOIN vCompanyActive company
                 ON company.ID = bi.ID_Company
  WHERE  bi.ID_FilingStatus = 4
         and company.ID  IN ( 21)
         AND ( CONVERT(DATE, bi.Date) BETWEEN '2023-01-01' AND '2023-03-31' )

GO
