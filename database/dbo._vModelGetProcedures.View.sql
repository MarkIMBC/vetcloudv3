﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vModelGetProcedures]
AS
     SELECT [name]
     FROM sys.procedures
     WHERE [name] LIKE 'pGet%'
           OR [name] LIKE '_pGET%';
GO
