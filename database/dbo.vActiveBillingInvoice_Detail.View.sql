﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoice_Detail] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoice_Detail H  
				WHERE IsActive = 1
GO
