﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetAppointmentSchedule]
    @ID INT = -1,
    @ID_Schedule INT = NULL,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_';

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN

        DECLARE @DateStart DATETIME;
        DECLARE @DateEnd DATETIME;
        DECLARE @ID_ScheduleType INT = 0;
        DECLARE @ScheduleType VARCHAR(MAX) = '';
        DECLARE @ID_ServiceType INT = 0;
        DECLARE @ServiceType VARCHAR(MAX) = '';
        DECLARE @ID_Doctor INT = 0;
        DECLARE @Doctor VARCHAR(MAX) = '';
        DECLARE @Waiting_ID_FilingStatus INT = 8;

        IF (ISNULL(@ID_Schedule, 0) > 0)
        BEGIN

            SELECT @DateStart = s.DateStart,
                   @DateEnd = s.DateEnd,
                   @ID_ScheduleType = s.ID_ScheduleType,
                   @ScheduleType = s.ScheduleType,
                   @ID_Doctor = s.ID_Doctor,
                   @Doctor = s.Doctor,
                   @ID_ServiceType = ID_ServiceType,
                   @ServiceType = Name_ServiceType
            FROM vSchedule s
            WHERE ID = @ID_Schedule;
        END;

        SELECT H.*,
               appointStatus.Name AppointmentStatus_Name_FilingStatus
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   @ID_Schedule ID_Schedule,
                   @DateStart DateStart,
                   @DateEnd DateEnd,
                   @ID_ScheduleType ID_ScheduleType,
                   @ScheduleType ScheduleType,
                   @ID_ServiceType ID_ServiceType,
                   @ServiceType Name_ServiceType,
                   @ID_Doctor ID_Doctor,
                   @Doctor Doctor,
                   @Waiting_ID_FilingStatus AppointmentStatus_ID_FilingStatus
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus appointStatus
                ON appointStatus.ID = H.AppointmentStatus_ID_FilingStatus;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vAppointmentSchedule H
        WHERE H.ID = @ID;
    END;
END;
GO
