﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCivilStatus] AS   
				SELECT   
				 H.*  
				FROM vCivilStatus H  
				WHERE IsActive = 1
GO
