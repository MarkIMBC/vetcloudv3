﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_DentalExamination_MedicalHistory] AS   
				SELECT   
				 H.*  
				FROM vPatient_DentalExamination_MedicalHistory H  
				WHERE ISNULL(IsActive, 0) = 0
GO
