﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pUpdatePatientToWaitingListStatus](@IDs_PatientWaitingList        typIntList READONLY,
                                             @WaitingStatus_ID_FilingStatus INT,
                                             @DateReschedule                DateTime,
                                             @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    exec _pUpdatePatientToWaitingListStatus
      @IDs_PatientWaitingList,
      @WaitingStatus_ID_FilingStatus,
      @DateReschedule,
      @ID_UserSession

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO
