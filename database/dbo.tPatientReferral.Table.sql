﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatientReferral](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[ClientContactNumber] [varchar](300) NULL,
	[Age] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
	[History] [varchar](5000) NULL,
	[Treatment] [varchar](5000) NULL,
	[Concerns] [varchar](5000) NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateUndoCanceled] [datetime] NULL,
	[ID_UndoCanceledBy] [int] NULL,
	[ReferralTypeOthers] [varchar](5000) NULL,
 CONSTRAINT [PK_tPatientReferral] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatientReferral] ON [dbo].[tPatientReferral]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatientReferral] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatientReferral]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral] CHECK CONSTRAINT [FK_tPatientReferral_ID_Company]
GO
ALTER TABLE [dbo].[tPatientReferral]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral] CHECK CONSTRAINT [FK_tPatientReferral_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatientReferral]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral] CHECK CONSTRAINT [FK_tPatientReferral_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateCreated_tPatientReferral] ON [dbo].[tPatientReferral] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatientReferral SET DateCreated = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatientReferral] ENABLE TRIGGER [rDateCreated_tPatientReferral]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateModified_tPatientReferral] ON [dbo].[tPatientReferral] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatientReferral SET DateModified = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatientReferral] ENABLE TRIGGER [rDateModified_tPatientReferral]
GO
