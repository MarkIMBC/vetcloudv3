﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveModelDefaultValue] AS   
    SELECT   
     H.*  
    FROM vModelDefaultValue H  
    WHERE ISNULL(IsActive, 0) = 1
GO
