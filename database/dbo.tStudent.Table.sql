﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tStudent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[LastName] [varchar](300) NULL,
	[FirstName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[PhoneNumber] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
 CONSTRAINT [PK_tStudent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tStudent] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tStudent]  WITH CHECK ADD  CONSTRAINT [FK_tStudent_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tStudent] CHECK CONSTRAINT [FK_tStudent_ID_Company]
GO
ALTER TABLE [dbo].[tStudent]  WITH CHECK ADD  CONSTRAINT [FK_tStudent_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tStudent] CHECK CONSTRAINT [FK_tStudent_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tStudent]  WITH CHECK ADD  CONSTRAINT [FK_tStudent_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tStudent] CHECK CONSTRAINT [FK_tStudent_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tStudent]
		ON [dbo].[tStudent]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tStudent
			SET    DateCreated = GETDATE()
			FROM   dbo.tStudent hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tStudent] ENABLE TRIGGER [rDateCreated_tStudent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tStudent]
		ON [dbo].[tStudent]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tStudent
			SET    DateModified = GETDATE()
			FROM   dbo.tStudent hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tStudent] ENABLE TRIGGER [rDateModified_tStudent]
GO
