﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE       
 PROC [dbo].[pUpdateItemCurrentInventoryByIDsItems](@IDs_Item typIntList READONLY)
AS
  BEGIN
      DECLARE @inventoryTotal TABLE
        (
           ID_Item INT,
           Qty     INT
        )

      INSERT @inventoryTotal
      SELECT ID_Item,
             Qty
      FROM   vInventoryTrailTotal inventTotal
             inner join @IDs_Item ids
                     on inventTotal.ID_Item = ids.iD

      exec pRemoveMulticateInventoryTrailByIDsItems
        @IDs_Item,
        0

      UPDATE dbo.tItem
      SET    CurrentInventoryCount = ISNULL(inventory.Qty, 0),
             ID_InventoryStatus = dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount, 0), ISNULL(MaxInventoryCount, 0))
      FROM   dbo.tItem item
             INNER JOIN @inventoryTotal inventory
                     ON inventory.ID_Item = item.ID;
  END;

GO
