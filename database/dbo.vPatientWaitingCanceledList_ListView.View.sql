﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 VIEW [dbo].[vPatientWaitingCanceledList_ListView]
AS
  SELECT DISTINCT ISNULL(appointment.UniqueID, CONVERT(VARCHAR(MAX), MAX(hed.ID))) UniqueID,
                  hed.ID_Company,
                  ISNULL(appointment.DateStart, hed.DateCreated)                   DateStart,
                  hed.DateCreated                                                  DateCreated,
                  ISNULL(appointment.DateEnd, hed.DateCreated)                     DateEnd,
                  hed.ID_Client,
                  hed.Name_Client,
                  hed.ID_Patient,
                  hed.Name_Patient,
                  ISNULL(appointment.ReferenceCode, CASE
                                                      WHEN hed.IsQueued = 1 THEN 'Queued'
                                                      ELSE ''
                                                    END)                           ReferenceCode,
                  hed.WaitingStatus_ID_FilingStatus,
                  hed.WaitingStatus_Name_FilingStatus,
                  appointment.Description,
                  hed.IsQueued,
                  hed.Oid_Model_Reference,
                  hed.ID_Reference,
                  CONVERT(Date, hed.DateReschedule)                                DateReschedule
  FROM   vPatientWaitingList hed
         LEFT JOIN vAppointmentEvent appointment
                on hed.ID_Client = appointment.ID_Client
                   and hed.ID_Patient = appointment.ID_Patient
                   AND hed.Oid_Model_Reference = appointment.Oid_Model
                   and hed.ID_Reference = appointment.Appointment_ID_CurrentObject
         LEFT JOIN tPatient patient
                on patient.ID = appointment.ID_Patient
  where  hed.WaitingStatus_ID_FilingStatus IN ( 4, 21 )
  GROUP  BY appointment.UniqueID,
            hed.IsQueued,
            hed.ID_Company,
            hed.DateCreated,
            appointment.DateStart,
            appointment.DateEnd,
            hed.ID_Client,
            hed.Name_Client,
            hed.ID_Patient,
            hed.Name_Patient,
            appointment.ReferenceCode,
            hed.WaitingStatus_ID_FilingStatus,
            hed.WaitingStatus_Name_FilingStatus,
            appointment.Description,
            hed.Oid_Model_Reference,
            hed.ID_Reference,
            CONVERT(Date, hed.DateReschedule)

GO
