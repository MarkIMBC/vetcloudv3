﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveOccupationalStatus] AS   
				SELECT   
				 H.*  
				FROM vOccupationalStatus H  
				WHERE IsActive = 1
GO
