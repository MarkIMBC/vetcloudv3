﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSMSList_BillingInvoice_SMSPayableRemider]
AS
  SELECT biSMSReminder.ID,
         biSMSReminder.ID_BillingInvoice,
         bihed.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, company.ContactNumber, biHed.Code, biHed.Date, biHed.Name_Client, ISNULL(biSMSReminder.RemainingAmount_BillingInvoice, 0)) Message,
         DATEADD(DAY, -1, biSMSReminder.DateSchedule)                                                                                                                                                                                          DateSending,
         bihed.ID_Company,
         ISNULL(biSMSReminder.IsSentSMS, 0)                                                                                                                                                                                                    IsSentSMS,
         model.Oid                                                                                                                                                                                                                             Oid_Model
  FROM   vBillingInvoice_SMSPayableRemider biSMSReminder
         INNER JOIN vBillingInvoice biHed
                 on biSMSReminder.ID_BillingInvoice = bihed.ID
         INNER JOIN tCompany_SMSSetting smsSetting
                 on smsSetting.ID_Company = biSMSReminder.ID_Company
         INNER JOIN tClient c
                 on c.ID = bihed.ID_Client
         INNER JOIN tCOmpany company
                 on company.ID = bihed.ID_Company,
         _tModel model
  where  model.tableName = 'tBillingInvoice_SMSPayableRemider'
         AND ISNULL(biSMSReminder.IsActive, 0) = 1
         AND biHed.ID_FilingStatus = 3
         AND Payment_ID_FilingStatus IN ( 2, 11 )
         AND ISNULL(RemainingAmount, 0) > 0

GO
