﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUser] AS   
				SELECT   
				 H.*  
				FROM vUser H  
				WHERE ISNULL(IsActive, 0) = 0
GO
