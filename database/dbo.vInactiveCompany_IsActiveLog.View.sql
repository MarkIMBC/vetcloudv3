﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompany_IsActiveLog] AS   
				SELECT   
				 H.*  
				FROM vCompany_IsActiveLog H  
				WHERE ISNULL(IsActive, 0) = 0
GO
