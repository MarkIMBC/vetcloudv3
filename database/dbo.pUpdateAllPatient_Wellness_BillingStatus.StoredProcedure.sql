﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateAllPatient_Wellness_BillingStatus]
AS
  BEGIN
      DECLARE @IDs_Patient_Wellness typINtList

      INSERT @IDs_Patient_Wellness
      SELECT ID_CurrentObject
      FROM   vForBilling_ListView hed
             inner join _tModel model
                     on model.Oid = hed.Oid_Model
      where  model.TableName = 'tPatient_Wellness'

      exec pUpdatePatient_Wellness_BillingStatus
        @IDs_Patient_Wellness
  END

GO
