﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Wellness_Schedule_SMSStatus] AS   
				SELECT   
				 H.*  
				FROM vPatient_Wellness_Schedule_SMSStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
