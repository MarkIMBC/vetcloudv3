﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveHelpDeskVideoTutorial] AS   
				SELECT   
				 H.*  
				FROM vHelpDeskVideoTutorial H  
				WHERE ISNULL(IsActive, 0) = 0
GO
