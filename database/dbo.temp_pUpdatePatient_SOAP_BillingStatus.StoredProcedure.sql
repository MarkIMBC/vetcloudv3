﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 PROC [dbo].[temp_pUpdatePatient_SOAP_BillingStatus](@IDs_Patient_SOAP typIntList READONLY)
AS
    DECLARE @Cancelled_ID_FilingStatus INT = 4
    DECLARE @ForBilling_ID_FilingStatus INT = 16
    DECLARE @Patient_SOAP TABLE
      (
         ID_Patient_SOAP                INT,
         BillingInvoice_ID_FilingStatus INT
      )
    DECLARE @Patient_SOAP_FromBI TABLE
      (
         ID_Patient_SOAP                INT,
         BillingInvoice_ID_FilingStatus INT
      )

    INSERT @Patient_SOAP
    SELECT _soap.ID,
           CASE
             WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus
             ELSE NULL
           END
    FROM   tPatient_SOAP _soap
           inner join @IDs_Patient_SOAP ids
                   on _soap.ID = ids.ID

    INSERT @Patient_SOAP_FromBI
    SELECT ID_Patient_SOAP,
           dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])
    FROM   (SELECT idsSOAP.ID ID_Patient_SOAP,
                   fs.ID      BillingInvoice_ID_FilingStatus,
                   COUNT(*)   BillingInvoiceCount
            FROM   vBillingInvoice bi
                   inner JOIN @IDs_Patient_SOAP idsSOAP
                           on idsSOAP.ID = bi.ID_Patient_SOAP
                   LEFT JOIN tFilingStatus fs
                          ON fs.Name = bi.Status
            WHERE  ID_FilingStatus NOT IN ( 4 )
            GROUP  BY idsSOAP.ID,
                      fs.ID) AS SourceTable
           PIVOT ( AVG(BillingInvoiceCount)
                 FOR BillingInvoice_ID_FilingStatus IN ([1],
                                                        [3],
                                                        [2],
                                                        [11],
                                                        [12],
                                                        [13]) ) AS PivotTable;

    UPDATE @Patient_SOAP
    SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus
    FROM   @Patient_SOAP soapRec
           INNER JOIN @Patient_SOAP_FromBI soapFromPivot
                   ON soapRec.ID_Patient_SOAP = soapFromPivot.ID_Patient_SOAP
SELECT * FROM @Patient_SOAP
    UPDATE tPatient_SOAP
    SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus
    FROM   tPatient_SOAP soap
           INNER JOIN @Patient_SOAP soapRec
                   ON soap.ID = soapRec.ID_Patient_SOAP

GO
