﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROCEDURE [dbo].[pGetClientAppointmentRequest] @ID         INT = -1,
                                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS ClientAppointmentRequest_Patient

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Client    INT
      DECLARE @FILED_ID_FilingStatus INT = 1;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Client = ID
      FROM   tClient
      WHERE  ID_User = @ID_User

      IF ( @ID = -1 )
        BEGIN
            DECLARE @_dateStart DateTime = Convert(datetime, FORMAT(GETDATE(), 'yyyy-MM-dd HH:00:00.000'))
            DECLARE @_dateEnd DateTime = DATEADD(HOUR, 1, @_dateStart)

            SELECT H.*,
                   client.Name Name_Client,
                   fs.Name     Name_FilingStatus
            FROM   (SELECT NULL                   AS [_],
                           -1                     AS [ID],
                           '- New -'              AS [Code],
                           NULL                   AS [Name],
                           1                      AS [IsActive],
                           NULL                   AS [ID_Company],
                           NULL                   AS [Comment],
                           NULL                   AS [DateCreated],
                           NULL                   AS [DateModified],
                           @_dateStart            AS [DateStart],
                           @_dateEnd              AS [DateEnd],
                           NULL                   AS [ID_CreatedBy],
                           NULL                   AS [ID_LastModifiedBy],
                           @FILED_ID_FilingStatus AS ID_FilingStatus,
                           @ID_Client             AS ID_Client,
                           NULL                   AS ID_SOAPType) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON H.ID_Client = client.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vClientAppointmentRequest H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROm   vClientAppointmentRequest_Patient
      WHERE  ID_ClientAppointmentRequest = @ID
  END

GO
