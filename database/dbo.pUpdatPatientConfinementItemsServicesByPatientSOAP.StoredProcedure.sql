﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdatPatientConfinementItemsServicesByPatientSOAP](@IDs_Patient_SOAP TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      SELECT DISTINCT soap.ID_Patient_Confinement
      FROM   tPatient_SOAP soap
             INNER JOIN @IDs_Patient_SOAP idsSOAP
                     ON idsSOAP.ID = soap.ID

      -- WHERE  ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )
      -- Remove Deleted Treatment Items and Services on Confinement      
      DECLARE @ForRemove_IDs_Patient_SOAP_Treatment TYPINTLIST

      INSERT @ForRemove_IDs_Patient_SOAP_Treatment
      SELECT ID_Patient_SOAP_Treatment
      FROM   (SELECT itemServices.ID_Patient_SOAP_Treatment
              FROM   tPatient_Confinement_ItemsServices itemServices
                     INNER JOIN tPatient_SOAP soap
                             ON soap.ID = itemServices.ID_Patient_SOAP
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  ISNULL(itemServices.ID_Patient_SOAP_Treatment, 0) > 0
              EXCEPT
              SELECT treatment.ID ID_Patient_SOAP_Treatment
              FROM   tPatient_SOAP_Treatment treatment
                     INNER JOIN tPatient_SOAP soap
                             ON treatment.ID_Patient_SOAP = soap.ID
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  soap.ID_FilingStatus NOT IN ( 4 )) tbl

      -- Remove Deleted Prescription Items and Services on Confinement      
      DECLARE @ForRemove_IDs_Patient_SOAP_Prescription TYPINTLIST

      INSERT @ForRemove_IDs_Patient_SOAP_Prescription
      SELECT ID_Patient_SOAP_Prescription
      FROM   (SELECT itemServices.ID_Patient_SOAP_Prescription
              FROM   tPatient_Confinement_ItemsServices itemServices
                     INNER JOIN tPatient_SOAP soap
                             ON soap.ID = itemServices.ID_Patient_SOAP
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  ISNULL(itemServices.ID_Patient_SOAP_Prescription, 0) > 0
              EXCEPT
              SELECT prescription.ID ID_Patient_SOAP_Prescription
              FROM   tPatient_SOAP_Prescription prescription
                     INNER JOIN tPatient_SOAP soap
                             ON prescription.ID_Patient_SOAP = soap.ID
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  IsNull(IsCharged, 0) = 1
                     AND soap.ID_FilingStatus NOT IN ( 4 )) tbl

      DELETE FROM tPatient_Confinement_ItemsServices
      WHERE  ID_Patient_SOAP_Prescription IN (SELECT ID
                                              FROM   @ForRemove_IDs_Patient_SOAP_Prescription)
             AND ID_Patient_Confinement IN (SELECT ID
                                            FROM   @IDs_Patient_Confinement)

      DELETE FROM tPatient_Confinement_ItemsServices
      WHERE  ID_Patient_SOAP_Treatment IN (SELECT ID
                                           FROM   @ForRemove_IDs_Patient_SOAP_Treatment)
             AND ID_Patient_Confinement IN (SELECT ID
                                            FROM   @IDs_Patient_Confinement)

      -- Update Treatment Items and Services on Confinement      
      UPDATE tPatient_Confinement_ItemsServices
      SET    Date = soap.Date,
             ID_Patient_SOAP = soap.ID,
             ID_Patient_SOAP_Treatment = treatment.ID,
             ID_Item = treatment.ID_Item,
             Quantity = treatment.Quantity,
             UnitPrice = treatment.UnitPrice,
             UnitCost = treatment.UnitCost,
             Comment = treatment.Comment,
             DateExpiration = treatment.DateExpiration
      FROM   tPatient_Confinement_ItemsServices itemServices
             INNER JOIN tPatient_SOAP_Treatment treatment
                     ON itemServices.ID_Patient_SOAP_Treatment = treatment.ID
             INNER JOIN tPatient_SOAP soap
                     ON soap.ID = treatment.ID_Patient_SOAP
                        AND soap.ID_Patient_Confinement = itemServices.ID_Patient_Confinement
             INNER JOIN @IDs_Patient_SOAP idsSOAP
                     ON idsSOAP.ID = soap.ID
      WHERE  soap.ID_FilingStatus NOT IN ( 4 )

      -- Update Prescription Items and Services on Confinement      
      UPDATE tPatient_Confinement_ItemsServices
      SET    Date = soap.Date,
             ID_Patient_SOAP = soap.ID,
             ID_Patient_SOAP_Prescription = prescription.ID,
             ID_Item = prescription.ID_Item,
             Quantity = prescription.Quantity,
             UnitPrice = prescription.UnitPrice,
             UnitCost = prescription.UnitCost,
             Comment = prescription.Comment,
             DateExpiration = prescription.DateExpiration
      FROM   tPatient_Confinement_ItemsServices itemServices
             INNER JOIN tPatient_SOAP_Prescription prescription
                     ON itemServices.ID_Patient_SOAP_Prescription = prescription.ID
             INNER JOIN tPatient_SOAP soap
                     ON soap.ID = prescription.ID_Patient_SOAP
                        AND soap.ID_Patient_Confinement = itemServices.ID_Patient_Confinement
             INNER JOIN @IDs_Patient_SOAP idsSOAP
                     ON idsSOAP.ID = soap.ID
      WHERE  IsNull(IsCharged, 0) = 1
             AND soap.ID_FilingStatus NOT IN ( 4 )

      -- Insert Treatment Items and Services on Confinement      
      DECLARE @ForInsert_IDs_Patient_SOAP_Treatment TYPINTLIST

      INSERT @ForInsert_IDs_Patient_SOAP_Treatment
      SELECT ID_Patient_SOAP_Treatment
      FROM   (SELECT treatment.ID ID_Patient_SOAP_Treatment
              FROM   tPatient_SOAP_Treatment treatment
                     INNER JOIN tPatient_SOAP soap
                             ON treatment.ID_Patient_SOAP = soap.ID
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              EXCEPT
              SELECT itemServices.ID_Patient_SOAP_Treatment
              FROM   tPatient_Confinement_ItemsServices itemServices
                     INNER JOIN tPatient_SOAP soap
                             ON soap.ID = itemServices.ID_Patient_SOAP
                                and itemServices.ID_Patient_Confinement = soap.ID_Patient_Confinement
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  ISNULL(itemServices.ID_Patient_SOAP_Treatment, 0) > 0
                     AND soap.ID_FilingStatus NOT IN ( 4 )) tbl

      INSERT INTO [dbo].[tPatient_Confinement_ItemsServices]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Confinement],
                   [Date],
                   [Comment],
                   [ID_Item],
                   [Quantity],
                   [DateExpiration],
                   [UnitPrice],
                   [UnitCost],
                   [Amount],
                   [ID_Patient_SOAP],
                   [ID_Patient_SOAP_Treatment])
      SELECT 1,
             soap.ID_Company,
             GetDate(),
             GetDate(),
             1,
             1,
             soap.ID_Patient_Confinement,
             soap.Date,
             treatment.Comment,
             treatment.ID_Item,
             treatment.Quantity,
             treatment.DateExpiration,
             treatment.UnitPrice,
             treatment.UnitCost,
             0,
             soap.ID,
             treatment.ID
      FROM   tPatient_SOAP_Treatment treatment
             INNER JOIN tPatient_SOAP soap
                     ON treatment.ID_Patient_SOAP = soap.ID
             INNER JOIN @IDs_Patient_SOAP idsSOAP
                     ON idsSOAP.ID = soap.ID
             INNER JOIN @ForInsert_IDs_Patient_SOAP_Treatment idsTreatment
                     ON idsTreatment.ID = treatment.ID
					 where soap.ID_FilingStatus NOT IN ( 4 )

      -- Insert Prescription Items and Services on Confinement      
      DECLARE @ForInsert_IDs_Patient_SOAP_Prescription TYPINTLIST

      INSERT @ForInsert_IDs_Patient_SOAP_Prescription
      SELECT ID_Patient_SOAP_Prescription
      FROM   (SELECT prescription.ID ID_Patient_SOAP_Prescription
              FROM   tPatient_SOAP_Prescription prescription
                     INNER JOIN tPatient_SOAP soap
                             ON prescription.ID_Patient_SOAP = soap.ID
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  IsNull(IsCharged, 0) = 1
              EXCEPT
              SELECT itemServices.ID_Patient_SOAP_Prescription
              FROM   tPatient_Confinement_ItemsServices itemServices
                     INNER JOIN tPatient_SOAP soap
                             ON soap.ID = itemServices.ID_Patient_SOAP
                                and itemServices.ID_Patient_Confinement = soap.ID_Patient_Confinement
                     INNER JOIN @IDs_Patient_SOAP idsSOAP
                             ON idsSOAP.ID = soap.ID
              WHERE  ISNULL(itemServices.ID_Patient_SOAP_Prescription, 0) > 0
                     AND soap.ID_FilingStatus NOT IN ( 4 )) tbl

      INSERT INTO [dbo].[tPatient_Confinement_ItemsServices]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Confinement],
                   [Date],
                   [Comment],
                   [ID_Item],
                   [Quantity],
                   [DateExpiration],
                   [UnitPrice],
                   [UnitCost],
                   [Amount],
                   [ID_Patient_SOAP],
                   [ID_Patient_SOAP_Prescription])
      SELECT 1,
             soap.ID_Company,
             GetDate(),
             GetDate(),
             1,
             1,
             soap.ID_Patient_Confinement,
             soap.Date,
             prescription.Comment,
             prescription.ID_Item,
             prescription.Quantity,
             prescription.DateExpiration,
             prescription.UnitPrice,
             prescription.UnitCost,
             0,
             soap.ID,
             prescription.ID
      FROM   tPatient_SOAP_Prescription prescription
             INNER JOIN tPatient_SOAP soap
                     ON prescription.ID_Patient_SOAP = soap.ID
             INNER JOIN @IDs_Patient_SOAP idsSOAP
                     ON idsSOAP.ID = soap.ID
             INNER JOIN @ForInsert_IDs_Patient_SOAP_Prescription idsPrescription
                     ON idsPrescription.ID = prescription.ID
					 where  soap.ID_FilingStatus NOT IN ( 4 )

      EXEC dbo.pModel_AfterSaved_Patient_Confinement_Computation
        @IDs_Patient_Confinement

      EXEC dbo.pUpdateBillingInvoiceItemsByPatientConfinement
        @IDs_Patient_Confinement
  END

GO
