﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vInventoryTrailTotal]
AS
  SELECT inventory.ID_Item,
         SUM(inventory.Quantity) Qty
  FROM   dbo.tInventoryTrail inventory
         INNER JOIN tItem item
                 on item.ID = inventory.ID_Item
  GROUP  BY inventory.ID_Item,
            item.MinInventoryCount,
            item.MaxInventoryCount

GO
