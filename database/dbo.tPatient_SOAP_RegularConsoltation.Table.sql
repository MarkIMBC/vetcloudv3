﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_SOAP_RegularConsoltation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_Patient] [int] NULL,
 CONSTRAINT [PK_tPatient_SOAP_RegularConsoltation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation]  WITH CHECK ADD  CONSTRAINT [FKtPatient_SOAP_RegularConsoltation_ID_Patient] FOREIGN KEY([ID_Patient])
REFERENCES [dbo].[tPatient] ([ID])
GO
ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation] CHECK CONSTRAINT [FKtPatient_SOAP_RegularConsoltation_ID_Patient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_SOAP_RegularConsoltation]
		ON [dbo].[tPatient_SOAP_RegularConsoltation]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation] ENABLE TRIGGER [rDateCreated_tPatient_SOAP_RegularConsoltation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_SOAP_RegularConsoltation]
		ON [dbo].[tPatient_SOAP_RegularConsoltation]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation] ENABLE TRIGGER [rDateModified_tPatient_SOAP_RegularConsoltation]
GO
