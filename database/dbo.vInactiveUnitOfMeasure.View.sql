﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUnitOfMeasure] AS   
				SELECT   
				 H.*  
				FROM vUnitOfMeasure H  
				WHERE ISNULL(IsActive, 0) = 0
GO
