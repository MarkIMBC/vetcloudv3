﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSalesReturn_Detail] AS   
				SELECT   
				 H.*  
				FROM vSalesReturn_Detail H  
				WHERE IsActive = 1
GO
