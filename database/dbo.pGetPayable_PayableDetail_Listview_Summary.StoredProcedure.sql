﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPayable_PayableDetail_Listview_Summary](@CompanyID    INT,
                                                      @filterString VARCHAR(MAX))
AS
  BEGIN
      Declare @Payable       Decimal(18, 0),
              @TotalRemaning Decimal(18, 0),
              @TotalPaid     Decimal(18, 0)
      DECLARE @Record TABLE
        (
           TotalPayable  Decimal(18, 0),
           TotalRemaning Decimal(18, 0),
           TotalPaid     Decimal(18, 0)
        )

      IF( LEN(RTRIM(LTRIM(ISNULL(@filterString, '')))) > 0 )
        SET @filterString = ' AND ' + @filterString

      INSERT @Record
      EXEC ('SELECT 
					SUM(TotalAmount) TotalPayable,
					SUM(RemaningAmount) TotalRemaning,
					SUM(ISNULL(PaidAmount, 0 )) TotalPaidAmount
             FROM   vPayable_PayableDetail_Listview 
			 WHERE 
				ID_Company = '+ @CompanyID +' 
				'+ @filterString +'
				AND 1 = 1
			 ')

      SELECT '_'

      SELECT *
      FROM   @Record
  END

GO
