﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEducationalLevel] AS   
				SELECT   
				 H.*  
				FROM vEducationalLevel H  
				WHERE IsActive = 1
GO
