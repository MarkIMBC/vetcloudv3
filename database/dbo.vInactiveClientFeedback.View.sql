﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveClientFeedback] AS   
				SELECT   
				 H.*  
				FROM vClientFeedback H  
				WHERE ISNULL(IsActive, 0) = 0
GO
