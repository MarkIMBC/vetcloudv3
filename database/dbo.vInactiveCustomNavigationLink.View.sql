﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCustomNavigationLink] AS   
				SELECT   
				 H.*  
				FROM vCustomNavigationLink H  
				WHERE ISNULL(IsActive, 0) = 0
GO
