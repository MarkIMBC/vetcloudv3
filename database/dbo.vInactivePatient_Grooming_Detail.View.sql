﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Grooming_Detail] AS   
    SELECT   
     H.*  
    FROM vPatient_Grooming_Detail H  
    WHERE ISNULL(IsActive, 0) = 1
GO
