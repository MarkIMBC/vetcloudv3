﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vModelPropertiesCrud]
AS
SELECT  P.Name ,
		P.ID_Model,
        P.ID_PropertyType ,
        M.TableName ,
        P.ID_PropertyModel ,
		PM.Name AS PropertyModel,
        PM.TableName AS PropertyModel_TableName ,
		PM.PrimaryKey AS PropertyModel_PrimaryKey,
		MPK.Name AS PropertyModel_Link,
		MPK.ID_PropertyType AS PropertyModel_Type
FROM    dbo.[_tModel_Property] P
        LEFT JOIN dbo.[_tModel] M ON P.ID_Model = M.Oid
        LEFT JOIN dbo.[_vModel] PM ON P.ID_PropertyModel = PM.Oid
		LEFT JOIN dbo.[_tModel_Property] MPK ON P.ID_ModelProperty_Key = MPK.Oid
WHERE   P.IsActive = 1
        AND P.Name NOT IN ('DateCreated', 'DateModified', 'Oid', 'ID' )
GO
