﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveClientAppointmentRequest_Patient] AS   
				SELECT   
				 H.*  
				FROM vClientAppointmentRequest_Patient H  
				WHERE ISNULL(IsActive, 0) = 0
GO
