﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pApproveReceivingReport] (@IDs_ReceivingReport typIntList READONLY,
                                            @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model_ReceivingReport_Detail VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @Oid_Model_ReceivingReport_Detail = Oid
          FROM   _tModel
          where  TableName = 'tReceivingReport_Detail'

          DECLARE @ID_User INT = 0;
          DECLARE @IDs_PurchaseOrder typIntList;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveReceivingReport_validation
            @IDs_ReceivingReport,
            @ID_UserSession;

          UPDATE dbo.tReceivingReport
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tReceivingReport bi
                 INNER JOIN @IDs_ReceivingReport ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       Oid_Model_Reference,
                       ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 @Oid_Model_ReceivingReport_Detail,
                 detail.ID
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------  
          /* Update PO Serving Status */
          INSERT @IDs_PurchaseOrder
                 (ID)
          SELECT DISTINCT rrHed.ID_PurchaseOrder
          FROM   dbo.tReceivingReport rrHed
                 INNER JOIN @IDs_ReceivingReport ids
                         ON rrHed.ID = ids.ID;

          EXEC dbo.pUpdatePOServingStatus
            @IDs_PurchaseOrder;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
