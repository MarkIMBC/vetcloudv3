﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 FUNCTION [dbo].[fGetClientMobileNumberByCompany](@GUID_Company VARCHAR(MAX))
RETURNS @table TABLE (
  Name_Company          VARCHAR(MAX),
  OriginalContactNumber VARCHAR(MAX),
  ContactNumber         VARCHAR(MAX))
  BEGIN
      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   vActiveCompany
      WHERE  Guid = @GUID_Company

      -----------------------------------------------------------------------  
      DECLARE @Number TABLE
        (
           OriginalContactNumber VARCHAR(MAX),
           Number                VARCHAR(MAX)
        )

      INSERT @Number
      SELECT ContactNumber,
             ContactNumber
      FROM   tClient
      where  ID_Company = @ID_Company
             AND LEN(ContactNumber) > 0
             and IsActive = 1
      UNION ALL
      SELECT ContactNumber2,
             ContactNumber2
      FROM   tClient
      where  ID_Company = @ID_Company
             AND LEN(ContactNumber2) > 0
             and IsActive = 1

      Update @Number
      SET    Number = REPLACE(Number, '-', '')

      Update @Number
      SET    Number = REPLACE(Number, ' ', '')

      Update @Number
      SET    Number = TRIM(Number)

      Update @Number
      SET    Number = REPLACE(Number, '+63', '0')

      DECLARE @_tempNumber TABLE
        (
           OriginalContactNumber VARCHAR(MAX),
           Number                Varchar(MAX)
        )
      DECLARE @_OriginalContactNumber VARCHAR(MAX)
      DECLARE @_Number VARCHAR(MAX)
      DECLARE db_cursor CURSOR FOR
        SELECT OriginalContactNumber,
               Number
        FROM   @Number
        WHERE  ISNUMERIC(Number) = 0
        Order  by Number DESC

      OPEN db_cursor

      FETCH NEXT FROM db_cursor INTO @_OriginalContactNumber, @_Number

      WHILE @@FETCH_STATUS = 0
        BEGIN
            Insert @_tempNumber
            SELECT @_OriginalContactNumber,
                   Part
            FROM   dbo.fGetSplitString(@_Number, '/')

            DELETE @Number
            WHERE  OriginalContactNumber = @_OriginalContactNumber

            FETCH NEXT FROM db_cursor INTO @_OriginalContactNumber, @_Number
        END

      CLOSE db_cursor

      DEALLOCATE db_cursor

      INSERT @table
      SELECT @Name_Company,
             OriginalContactNumber,
             Number
      FROM   (SELECT *
              FROM   @Number
              WHERE  ISNUMERIC(Number) = 1
              UNION ALL
              SELECT *
              FROM   @_tempNumber
              WHERE  ISNUMERIC(Number) = 1) tbl
      WHERE  LEN(Number) = 11
      Order  by LEN(Number)

      ------------------------------------------  
      RETURN
  END

GO
