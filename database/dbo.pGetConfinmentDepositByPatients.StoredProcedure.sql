﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetConfinmentDepositByPatients](@ID_Patient_Confinement int,
                                                   @IDs_Patient            typIntList Readonly)
AS
  BEGIN
      DECLARE @CurrentCreditAmount Decimal(18, 4)= 0
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @RemainingDepositAmount Decimal(18, 4)= 0
      DECLARE @ID_Client INT = 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @MaxDate_ClientDeposit DateTime
      DECLARE @ClientDeposit TABLE
        (
           ID                INT,
           Date              DateTIme,
           Code              VARCHAR(50),
           Name_Patient      VARCHAR(MAX),
           DepositAmount     Decimal(18, 4),
           Name_FilingStatus VARCHAR(MAX)
        )
      DECLARE @________IDs_Patient typIntList

      INSERT @________IDs_Patient
      SELECT DISTINCT ID
      FROM   @IDs_Patient

      SELECT @ID_Client = ID_Client
      FROM   tPatient_Confinement
      where  ID = @ID_Patient_Confinement

      IF(SELECT COUNT(*)
         FROM   @________IDs_Patient) = 0
        BEGIN
            SELECT @CurrentCreditAmount = client.CurrentCreditAmount
            FROM   tclient client
            WHERE  ID = @ID_Client

            SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement, @________IDs_Patient);
            SET @RemainingDepositAmount = ISNULL(@CurrentCreditAmount, 0) - ISNULL(@ConfinementDepositAmount, 0)

            if( @RemainingDepositAmount < 0 )
              SET @RemainingDepositAmount = 0

            SELECT @MaxDate_ClientDeposit = MAX(Date)
            FROm   tClient_CreditLogs
            where  ID_Client = @ID_Client
                   AND Code NOT IN (SELECT Code
                                    FROM   @ClientDeposit)

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount)
            SELECT NULL,
                   @MaxDate_ClientDeposit,
                   'Remaining',
                   '',
                   @RemainingDepositAmount

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount,
                    Name_FilingStatus)
            SELECT ID,
                   Date,
                   Code,
                   Name_Patient,
                   DepositAmount,
                   Name_FilingStatus
            FROM   dbo.vClientDeposit
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END
      else
        begin
            SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement, @________IDs_Patient);
            SET @RemainingDepositAmount = ISNULL(@ConfinementDepositAmount, 0)
            SET @CurrentCreditAmount = ISNULL(@RemainingDepositAmount, 0)

            if( @RemainingDepositAmount < 0 )
              SET @RemainingDepositAmount = 0

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount,
                    Name_FilingStatus)
            SELECT clientDeposit.ID,
                   Date,
                   clientDeposit.Code,
                   patient.Name Name_Patient,
                   DepositAmount,
                   fs.Name
            FROM   dbo.tClientDeposit clientDeposit
                   INNER JOIN tPatient patient
                           on patient.ID = clientDeposit.ID_Patient
                   inner join @________IDs_Patient idsPatient
                           on idsPatient.ID = clientDeposit.ID_Patient
                   INNER JOIN tFilingStatus fs
                           on fs.ID = clientDeposit.ID_FilingStatus
            WHERE  clientDeposit.ID_Patient_Confinement = @ID_Patient_Confinement
                   AND clientDeposit.ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END

      SELECT '_',
             '' AS ClientDeposits;

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             @RemainingDepositAmount              RemainingDepositAmount,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount,
             ISNULL(@CurrentCreditAmount, 0)      TotalDepositAmount;

      SELECT *
      FROM   @ClientDeposit
  END 
GO
