﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pUpdateAppointmentStatus](@Oid_Model                    VARCHAR(MAX),
                                    @Appointment_ID_CurrentObject INT,
                                    @Appointment_ID_FilingStatus  INT,
                                    @DateReschedule               DateTime,
                                    @ID_UserSession               INT,
                                    @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @TableName VARCHAR(MAX) = ''
      DECLARE @ID_Company INT = 0
      DECLARE @ID_User INT = 0
      DECLARE @Reschedule_ID_FilingStatus INT = 21
      DECLARE @ID_Patient INT

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @TableName = TableName
      FROM   _tModel
      WHERE  Oid = @Oid_Model

      IF( @TableName = 'tPatientAppointment' )
        BEGIN
            UPDATE tPatientAppointment
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatientAppointment
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_SOAP' )
        BEGIN
            UPDATE tPatient_SOAP_Plan
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_SOAP_Plan _plan
                   INNER JOIN tPatient_SOAP _soap
                           on _plan.ID_Patient_SOAP = _soap.ID
            WHERE  _plan.ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_Wellness' )
        BEGIN
            UPDATE tPatient_Wellness_Schedule
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_Wellness_Schedule _schedule
                   INNER JOIN tPatient_Wellness _wellness
                           on _schedule.ID_Patient_Wellness = _wellness.ID
            WHERE  _schedule.ID = @Appointment_ID_CurrentObject
        END

      ---------------------------------------------------------------------------------  
      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      SELECT MAX(_waiting.ID)
      FROM   tPatientWaitingList _waiting
      WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
             AND ID_Reference = @Appointment_ID_CurrentObject
             AND ID_Patient = @ID_Patient

      DELETE FROM @IDs_PatientWaitingList
      WHERE  ID IS NULL

      IF(SELECT COUNT(*)
         FROM   @IDs_PatientWaitingList) = 0
        BEGIN
            exec pAddAppointmentToWaitingList
              @Oid_Model,
              @Appointment_ID_CurrentObject,
              @ID_Patient,
              @ID_UserSession

            INSERT @IDs_PatientWaitingList
            SELECT MAX(_waiting.ID)
            FROM   tPatientWaitingList _waiting
            WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
                   AND ID_Reference = @Appointment_ID_CurrentObject
                   AND ID_Patient = @ID_Patient
        END

      exec _pUpdatePatientToWaitingListStatus
        @IDs_PatientWaitingList,
        @Appointment_ID_FilingStatus,
        @DateReschedule,
        @ID_UserSession

      ---------------------------------------------------------------------------------  
      /*Logs*/
      INSERT INTO [dbo].[tAppointmentStatusLog]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Appointment_ID_CurrentObject],
                   [Appointment_ID_FilingStatus],
                   DateReschedule)
      VALUES      ( 1,
                    @ID_Company,
                    @Remarks,
                    GetDate(),
                    GetDate(),
                    @ID_User,
                    @ID_User,
                    @Oid_Model,
                    @Appointment_ID_CurrentObject,
                    @Appointment_ID_FilingStatus,
                    @DateReschedule)
  END

GO
