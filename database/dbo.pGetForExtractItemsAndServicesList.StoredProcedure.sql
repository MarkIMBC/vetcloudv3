﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetForExtractItemsAndServicesList](@GUID VARCHAR(MAX))
AS
  BEGIN
      DEclare @GUID_Company VARCHAR(MAX) = @GUID

      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      SELECT '' [Inventoriable],
             '' [Service]

      SELECT @Name_Company                                                            Name_Company,
             dbo.fGetCleanedStringForExtratedRecord(_item.Code)                       Code,
             dbo.fGetCleanedStringForExtratedRecord(item.CustomCode)                  CustomCode,
             dbo.fGetCleanedStringForExtratedRecord(item.Name)                        Name,
             dbo.fGetCleanedStringForExtratedRecord(item.Name_ItemCategory)           Category,
             ISNULL(item.UnitCost, 0)                                                 BuyingPrice,
             ISNULL(item.UnitPrice, 0)                                                SellingPrice,
             item.CurrentInventoryCount,
             dbo.fGetCleanedStringForExtratedRecord(item.Name_InventoryStatus)        InventoryStatus,
             ISNULL(FORMAT(item.OtherInfo_DateExpiration, 'yyyy-MM-dd HH:mm:ss'), '') DateExpiration
      FROM   vActiveItem item
             INNER JOIN tItem _item
                     on item.ID = _item.ID
      WHERE  item.ID_Company = @ID_Company
             AND item.ID_ItemType = 2

      SELECT @Name_Company                                                  Name_Company,
             dbo.fGetCleanedStringForExtratedRecord(_item.Code)             Code,
             dbo.fGetCleanedStringForExtratedRecord(item.CustomCode)        CustomCode,
             dbo.fGetCleanedStringForExtratedRecord(item.Name)              Name,
             dbo.fGetCleanedStringForExtratedRecord(item.Name_ItemCategory) Category,
             ISNULL(item.UnitCost, 0)                                       BuyingPrice,
             ISNULL(item.UnitPrice, 0)                                      SellingPrice
      FROM   vActiveItem item
             INNER JOIN tItem _item
                     on item.ID = _item.ID
      WHERE  item.ID_Company = @ID_Company
             AND item.ID_ItemType = 1
  END

GO
