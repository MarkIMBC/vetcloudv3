﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tReceivingReport_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ReceivingReport] [int] NULL,
	[ID_PurchaseOrder_Detail] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[Amount] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tReceivingReport_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tReceivingReport_Detail] ON [dbo].[tReceivingReport_Detail]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tReceivingReport_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_Detail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] CHECK CONSTRAINT [FK_tReceivingReport_Detail_ID_Company]
GO
ALTER TABLE [dbo].[tReceivingReport_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] CHECK CONSTRAINT [FK_tReceivingReport_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tReceivingReport_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] CHECK CONSTRAINT [FK_tReceivingReport_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tReceivingReport_Detail]  WITH CHECK ADD  CONSTRAINT [FKtReceivingReport_Detail_ID_ReceivingReport] FOREIGN KEY([ID_ReceivingReport])
REFERENCES [dbo].[tReceivingReport] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] CHECK CONSTRAINT [FKtReceivingReport_Detail_ID_ReceivingReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tReceivingReport_Detail]
		ON [dbo].[tReceivingReport_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tReceivingReport_Detail
			SET    DateCreated = GETDATE()
			FROM   dbo.tReceivingReport_Detail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] ENABLE TRIGGER [rDateCreated_tReceivingReport_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tReceivingReport_Detail]
		ON [dbo].[tReceivingReport_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tReceivingReport_Detail
			SET    DateModified = GETDATE()
			FROM   dbo.tReceivingReport_Detail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tReceivingReport_Detail] ENABLE TRIGGER [rDateModified_tReceivingReport_Detail]
GO
