﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetItem] @ID          INT = -1,
                        @ID_ItemType INT = NULL,
                        @ID_Session  INT = NULL
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT '_',
             '' Item_Supplier;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   itemType.Name Name_ItemType
            FROM   (SELECT -1           AS [ID],
                           NULL         AS [Code],
                           NULL         AS [Name],
                           1            AS [IsActive],
                           NULL         AS [ID_Company],
                           NULL         AS [Comment],
                           NULL         AS [DateCreated],
                           NULL         AS [DateModified],
                           NULL         AS [ID_CreatedBy],
                           NULL         AS [ID_LastModifiedBy],
                           @ID_ItemType ID_ItemType) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tItemType itemType
                          ON itemType.ID = H.ID_ItemType;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   CASE
                     WHEN H.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(H.OtherInfo_DateExpiration, '', 'Expired')
                     ELSE ''
                   END                                                  RemainingBeforeExpired,
                   DATEDIFF(DAY, GETDATE(), H.OtherInfo_DateExpiration) RemainingDays
            FROM   vItem H
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROm   vItem_Supplier
      WHERE  ID_Item = @ID
      ORDER  BY Name_Supplier
  END;

GO
