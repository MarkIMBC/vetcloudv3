﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGenerateListViewQuery]
    @Name VARCHAR(300) ,
    @SQL NVARCHAR(MAX) ,
    @ID_Model UNIQUEIDENTIFIER = NULL
AS
    DECLARE @tmpTable TABLE
        (
          is_hidden BIT ,
          column_ordinal INT ,
          name VARCHAR(300) ,
          is_nullable BIT ,
          system_type_id INT ,
          system_type_name VARCHAR(300) ,
          max_length INT ,
          precision INT ,
          scale INT ,
          collation_name VARCHAR(300) ,
          user_type_id INT ,
          user_type_database VARCHAR(300) ,
          user_type_schema VARCHAR(300) ,
          user_type_name VARCHAR(300) ,
          assembly_qualified_type_name VARCHAR(300) ,
          xml_collection_id INT ,
          xml_collection_database VARCHAR(300) ,
          xml_collection_schema VARCHAR(300) ,
          xml_collection_name VARCHAR(300) ,
          is_xml_document BIT ,
          is_case_sensitive BIT ,
          is_fixed_length_clr_type BIT ,
          source_server VARCHAR(300) ,
          source_database VARCHAR(300) ,
          source_schema VARCHAR(300) ,
          source_table VARCHAR(300) ,
          source_column VARCHAR(300) ,
          is_identity_column BIT ,
          is_part_of_unique_key BIT ,
          is_updateable BIT ,
          is_computed_column BIT ,
          is_sparse_column_set BIT ,
          ordinal_in_order_by_list VARCHAR(300) ,
          order_by_is_descending VARCHAR(300) ,
          order_by_list_length INT ,
          tds_type_id INT ,
          tds_length INT ,
          tds_collation_id INT ,
          tds_collation_sort_id INT
        )

    INSERT  INTO @tmpTable
            EXEC sp_describe_first_result_set @SQL

    IF EXISTS ( SELECT  name
                FROM    @tmpTable )
        BEGIN

            DECLARE @ID_ListView UNIQUEIDENTIFIER = NEWID()

            INSERT  INTO dbo.[_tListView]
                    ( Oid ,
                      Code ,
                      Name ,
                      IsActive ,
                      ID_Model ,
                      DataSource ,
                      Comment ,
                      Caption ,
                      ID_CreatedBy ,
                      ID_LastModifiedBy ,
                      DateCreated ,
                      DateModified ,
                      PageSize ,
                      IsAllowAdd ,
                      IsAllowDelete ,
                      IsAllowEdit
	                )
            VALUES  ( @ID_ListView , -- Oid - uniqueidentifier
                      NULL , -- Code - varchar(50)
                      @Name , -- Name - varchar(200)
                      NULL , -- IsActive - bit
                      @ID_Model , -- ID_Model - uniqueidentifier
                      @SQL , -- DataSource - varchar(1000)
                      NULL , -- Comment - varchar(200)
                      NULL , -- Caption - varchar(200)
                      NULL , -- ID_CreatedBy - int
                      NULL , -- ID_LastModifiedBy - int
                      GETDATE() , -- DateCreated - datetime
                      GETDATE() , -- DateModified - datetime
                      50 , -- PageSize - int
                      0 , -- IsAllowAdd - bit
                      0 , -- IsAllowDelete - bit
                      0  -- IsAllowEdit - bit
	                )

	
            INSERT  INTO dbo.[_tListView_Detail]
                    ( Oid ,
                      Code ,
                      Name ,
                      IsActive ,
                      Comment ,
                      Caption ,
                      ID_ModelProperty ,
                      ID_ListView ,
                      ID_CreatedBy ,
                      ID_LastModifiedBy ,
                      DateCreated ,
                      DateModified ,
                      DisplayProperty ,
                      DataSource ,
                      Format ,
                      Width ,
                      Fixed ,
                      VisibleIndex ,
                      IsAllowEdit ,
                      ID_ControlType ,
                      ID_ColumnAlignment ,
                      IsVisible ,
                      FixedPosition ,
                      IsRequired ,
                      ID_SummaryType ,
                      Precision ,
                      ID_PropertyType
	                )
                    SELECT  NEWID() ,
                            NULL ,
                            t.name ,
                            1 ,
                            NULL ,
                            mp.Caption,
                            MP.Oid ,
                            @ID_ListView ,
                            NULL ,
                            NULL ,
                            GETDATE() ,
                            GETDATE() ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            1 ,
                            NULL ,
                            NULL ,
                            NULL ,
                            NULL ,
                            ISNULL(MP.ID_PropertyType,
                                   ( CASE t.system_type_id
                                       WHEN 35 THEN 1 --STRING
                                       WHEN 36 THEN 1
                                       WHEN 99 THEN 1 --STRING
                                       WHEN 167 THEN 1	--STRING
                                       WHEN 175 THEN 1	--STRING
                                       WHEN 231 THEN 1 --STRING
                                       WHEN 239 THEN 1	--STRING
                                       WHEN 106 THEN 3 -- DECIMAL
                                       WHEN 40 THEN 5 --DATE
                                       WHEN 61 THEN 5 --DATE
                                       WHEN 104 THEN 4 --BOOL
                                       WHEN 56 THEN 2 --INT
                                       ELSE 0
                                     END ))
                    FROM    @tmpTable t
                            LEFT JOIN dbo.[_tModel_Property] MP ON MP.Name = t.name
                                                              AND MP.ID_Model = @ID_Model


	
        END

   

   SELECT @ID_ListView
GO
