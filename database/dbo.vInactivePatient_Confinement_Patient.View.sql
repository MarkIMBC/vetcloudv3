﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Confinement_Patient] AS   
				SELECT   
				 H.*  
				FROM vPatient_Confinement_Patient H  
				WHERE ISNULL(IsActive, 0) = 0
GO
