﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vzServiceListReport]
AS
  SELECT TOP 100 item.ID                   ID,
                 item.ID_ItemCategory,
                 ISNULL(cat.Name, '---')   Name_ItemCategory,
                 LTRIM(RTRIM(item.Name))   Name_Item,
                 ISNULL(item.UnitCost, 0)  UnitCost,
                 ISNULL(item.UnitPrice, 0) UnitPrice,
                 item.ID_Company,
                 company.ImageLogoLocationFilenamePath,
                 company.Name              Name_Company,
                 company.Address           Address_Company,
                 CASE
                   WHEN LEN(company.Address) > 0 THEN '' + company.Address
                   ELSE ''
                 END
                 + CASE
                     WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
                     ELSE ''
                   END
                 + CASE
                     WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
                     ELSE ''
                   END                     HeaderInfo_Company
  FROM   dbo.tItem item
         LEFT JOIN tItemCategory cat
                on item.ID_ItemCategory = cat.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
  WHERE  ISNULL(item.IsActive, 0) = 1
         and item.ID_ITEMTYpe = 1

GO
