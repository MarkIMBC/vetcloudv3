﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tItem_Supplier](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_Item] [int] NULL,
	[ID_Supplier] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
 CONSTRAINT [PK_tItem_Supplier] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tItem_Supplier] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tItem_Supplier]  WITH CHECK ADD  CONSTRAINT [FKtItem_Supplier_ID_Item] FOREIGN KEY([ID_Item])
REFERENCES [dbo].[tItem] ([ID])
GO
ALTER TABLE [dbo].[tItem_Supplier] CHECK CONSTRAINT [FKtItem_Supplier_ID_Item]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tItem_Supplier]
		ON [dbo].[tItem_Supplier]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tItem_Supplier] ENABLE TRIGGER [rDateCreated_tItem_Supplier]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tItem_Supplier]
		ON [dbo].[tItem_Supplier]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tItem_Supplier] ENABLE TRIGGER [rDateModified_tItem_Supplier]
GO
