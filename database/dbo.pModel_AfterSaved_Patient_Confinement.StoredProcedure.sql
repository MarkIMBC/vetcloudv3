﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pModel_AfterSaved_Patient_Confinement] (@ID_CurrentObject VARCHAR(10),
                                                          @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0
      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      VALUES (@ID_CurrentObject)

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_Patient_SOAP INT;

            SELECT @ID_Company = ID_Company,
                   @ID_Patient_SOAP = ID_Patient_SOAP
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;

            UPDATE tPatient_SOAP
            SET    ID_Patient_Confinement = @ID_CurrentObject
            WHERE  ID = @ID_Patient_SOAP
        END;

      --SELECT @ID_Patient = ID_Patient
      --FROM   tPatient_Confinement
      --WHERE  ID = @ID_CurrentObject

      --UPDATE tPatient_SOAP
      --SET    ID_Patient = @ID_Patient
      --WHERE  ID_Patient_Confinement = @ID_CurrentObject 

      /* Auto add Patient to Patient List para sa mga lumang format ng pag add ng patient sa confinement*/
      DECLARE @PatientCount INT = 0

      SELECT @PatientCount = Count(*)
      FROM   [tPatient_Confinement_Patient]
      WHERE  ID_Patient_Confinement = @ID_CurrentObject

      IF ( @PatientCount = 0 )
        BEGIN
            INSERT INTO [dbo].[tPatient_Confinement_Patient]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [ID_Patient_Confinement],
                         [ID_Patient])
            SELECT NULL,
                   NULL,
                   1,
                   ID_Company,
                   '',
                   GetDate(),
                   GetDate(),
                   1,
                   1,
                   ID,
                   ID_Patient
            FROM   tPatient_Confinement
            WHERE  ID = @ID_CurrentObject
                   AND ISNULL(ID_Patient, 0) <> 0
        END

      -----------------------------------------------------------------------------------------------------------
      -----------------------------------------------------------------------------------------------------------
      EXEC pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement

      ------EXEC _pCancelPatient_Confinement_temp
      EXEC dbo.pModel_AfterSaved_Patient_Confinement_Computation
        @IDs_Patient_Confinement

      EXEC dbo.pUpdateBillingInvoiceItemsByPatientConfinement
        @IDs_Patient_Confinement
  END; 
GO
