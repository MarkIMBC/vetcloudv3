﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzPaymentTransactionSummaryReport]
as
  SELECT biHed.ID                                                          ID_BillingInvoice,
         pHed.ID                                                           ID_PaymentTransaction,
         biHed.Code                                                        Code_BillingInvoice,
         biHed.Date                                                        Date_BillingInvoice,
         biHed.TotalAmount                                                 BalanceAmount,
         pHed.Code                                                         Code_PaymentTransaction,
         pHed.Date                                                         Date_PaymentTransaction,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         ISNULL(biHed.PatientNames, patient.Name)                          Name_Patient,
         biHed.ID_Company,
         pHed.ID_PaymentMethod,
         payMethod.Name                                                    Name_PaymentMethod,
         phed.PayableAmount,
         CASE
           WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount
           ELSE phed.PaymentAmount
         END                                                               PaymentAmount,
         pHed.RemainingAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                             HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')                  AttendingPhysician_ID_Employee,
         ISNULL(attendingVeterinarian.Name, '(No Assigned)')               AttendingPhysician_Name_Employee,
         attendingVeterinarian.Name_Position,
         ISNULL(pHed.ReferenceTransactionNumber, '')                       ReferenceTransactionNumber
  FROM   tPaymentTransaction pHed
         INNER JOIN vBillingInvoice biHed
                 on biHed.ID = pHed.ID_BillingInvoice
         LEFT JOIN tClient client
                ON client.ID = biHed.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = biHed.ID_Patient
         LEFT JOIN tPaymentMethod payMethod
                ON payMethod.ID = pHed.ID_PaymentMethod
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
         LEFT JOIN dbo.vEmployee attendingVeterinarian
                ON attendingVeterinarian.ID = biHed.[AttendingPhysician_ID_Employee]
  WHERE  biHed.Payment_ID_FilingStatus IN ( 11, 12 )
         AND pHed.ID_FilingStatus IN ( 3 )
  UNION ALL
  SELECT _deposit.ID                       ID_BillingInvoice,
         NULL                              ID_PaymentTransaction,
         _deposit.Code                     Code_BillingInvoice,
         _deposit.Date                     Date_BillingInvoice,
         0                                 BalanceAmount,
         ''                                Code_PaymentTransaction,
         _deposit.Date                     Date_PaymentTransaction,
         _deposit.Name_Client,
         ISNULL(_deposit.Name_Patient, '') Name_Patient,
         _deposit.ID_Company               ID_Company,
         6                                 ID_PaymentMethod,
         'Deposit'                         Name_PaymentMethod,
         0                                 PayableAmount,
         _deposit.DepositAmount            PaymentAmount,
         0                                 RemainingAmount,
         company. ImageLogoLocationFilenamePath,
         company.Name                      Name_Company,
         company.Address                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                             HeaderInfo_Company,
         NULL                              AttendingPhysician_ID_Employee,
         ''                                AttendingPhysician_Name_Employee,
         ''                                Name_Position,
         ''                                ReferenceTransactionNumber
  FROM   vClientDeposit _deposit
         LEFT JOIN dbo.vCompany company
                ON company.ID = _deposit.ID_Company
  WHERE  ID_FilingStatus IN ( 3, 17 )

GO
