﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSupplier](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Address] [varchar](300) NULL,
	[ContactDetail] [varchar](300) NULL,
	[TINNumber] [varchar](300) NULL,
	[MobileNumber] [varchar](300) NULL,
 CONSTRAINT [PK_tSupplier] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tSupplier] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tSupplier]  WITH CHECK ADD  CONSTRAINT [FK_tSupplier_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tSupplier] CHECK CONSTRAINT [FK_tSupplier_ID_Company]
GO
ALTER TABLE [dbo].[tSupplier]  WITH CHECK ADD  CONSTRAINT [FK_tSupplier_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSupplier] CHECK CONSTRAINT [FK_tSupplier_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tSupplier]  WITH CHECK ADD  CONSTRAINT [FK_tSupplier_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSupplier] CHECK CONSTRAINT [FK_tSupplier_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tSupplier]
		ON [dbo].[tSupplier]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSupplier
			SET    DateCreated = GETDATE()
			FROM   dbo.tSupplier hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSupplier] ENABLE TRIGGER [rDateCreated_tSupplier]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tSupplier]
		ON [dbo].[tSupplier]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSupplier
			SET    DateModified = GETDATE()
			FROM   dbo.tSupplier hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSupplier] ENABLE TRIGGER [rDateModified_tSupplier]
GO
