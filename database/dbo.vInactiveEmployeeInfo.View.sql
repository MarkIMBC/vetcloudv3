﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEmployeeInfo] AS   
				SELECT   
				 H.*  
				FROM vEmployeeInfo H  
				WHERE ISNULL(IsActive, 0) = 0
GO
