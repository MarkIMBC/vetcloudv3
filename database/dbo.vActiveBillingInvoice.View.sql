﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoice] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoice H  
				WHERE IsActive = 1
GO
