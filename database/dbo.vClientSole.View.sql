﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[vClientSole]  
AS  
  SELECT H.ID,
         H.Code,  
         H.Name,  
         H.IsActive,  
         H.ID_Company,  
         H.Comment,  
         H.DateCreated,  
         H.DateModified,  
         H.ID_CreatedBy,  
         H.ID_LastModifiedBy,  
         H.ContactNumber,  
         H.Email,  
         H.Address,  
         H.ContactNumber2,  
         H.Old_client_id,  
         H.tempID,  
         UC.Name AS CreatedBy,  
         UM.Name AS LastModifiedBy,  
         H.DateLastVisited,  
         H.CurrentCreditAmount,
		 p.Name PetName,
		 p.Code PatientCode

	
  FROM   tClient H
		 LEFT JOIN tPatient p
				ON H.ID=p.ID_Client
         LEFT JOIN tUser UC  
                ON H.ID_CreatedBy = UC.ID  
         LEFT JOIN tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
GO
