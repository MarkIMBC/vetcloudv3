﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pChangeModelReport](@ID_Company     INT,
                               @ModelName      Varchar(200),
                               @ReportNameFrom VARCHAR(200),
                               @ReportNameTo   VARCHAR(200), 
							   @Label VARCHAR(200)
							   )
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @From_Oid_Report VARCHAR(MAX)
      DECLARE @To_Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @From_Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportNameFrom

      SELECT @To_Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportNameTo

      Update _tModelReport
      Set    Oid_Report = @To_Oid_Report, Name = @Label
      WHERE  OId_Model = @Oid_Model
             AND Oid_Report = @From_Oid_Report
             AND ID_Company = @ID_Company
  END

GO
