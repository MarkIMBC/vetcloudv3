﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdatePatient_Grooming_BillingStatus](@IDs_Patient_Grooming typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @Patient_Grooming TABLE
        (
           ID_Patient_Grooming            INT,
           BillingInvoice_ID_FilingStatus INT
        )
      DECLARE @Patient_Grooming_FromBI TABLE
        (
           ID_Patient_Grooming            INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @Patient_Grooming
      SELECT _soap.ID,
             CASE
               WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus
               ELSE NULL
             END
      FROM   tPatient_Grooming _soap 
             inner join @IDs_Patient_Grooming ids
                     on _soap.ID = ids.ID

      INSERT @Patient_Grooming_FromBI
      SELECT ID_Patient_Grooming,
             dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])
      FROM   (SELECT idsSOAP.ID                                             ID_Patient_Grooming,
                     ISNULL(bi.Payment_ID_FilingStatus, bi.ID_FilingStatus) BillingInvoice_ID_FilingStatus,
                     COUNT(*)                                               BillingInvoiceCount
              FROM   vBillingInvoice bi 
                     inner JOIN @IDs_Patient_Grooming idsSOAP
                             on idsSOAP.ID = bi.ID_Patient_Grooming
                     INNER JOIN tCompany company
                             on company.ID = bi.ID_Company
              WHERE  ID_FilingStatus NOT IN ( 4 )
                     AND ISNULL(company.IsActive, 0) = 1
              GROUP  BY idsSOAP.ID,
                        ISNULL(bi.Payment_ID_FilingStatus, bi.ID_FilingStatus)) AS SourceTable
             PIVOT ( AVG(BillingInvoiceCount)
                   FOR BillingInvoice_ID_FilingStatus IN ([1],
                                                          [3],
                                                          [2],
                                                          [11],
                                                          [12],
                                                          [13]) ) AS PivotTable;

      UPDATE @Patient_Grooming
      SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus
      FROM   @Patient_Grooming soapRec
             INNER JOIN @Patient_Grooming_FromBI soapFromPivot
                     ON soapRec.ID_Patient_Grooming = soapFromPivot.ID_Patient_Grooming

      UPDATE tPatient_Grooming
      SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus
      FROM   tPatient_Grooming soap 
             INNER JOIN @Patient_Grooming soapRec
                     ON soap.ID = soapRec.ID_Patient_Grooming
  END

GO
