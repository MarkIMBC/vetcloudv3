﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 PROC [dbo].[pGetDashboardYearlySalesDataSource] (@ID_UserSession INT,
                                                   @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      --SET @DateYear = FORMAT(GETDATE(), 'yyyy')  
      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      --Declare a months table (or use a Date Dimension table)        
      Declare @Months Table
        (
           MonthNumber INT,
           MonthName   varchar(20)
        )

      INSERT INTO @Months
                  (MonthNumber,
                   MonthName)
      VALUES      (1,
                   'January'),
                  (2,
                   'February'),
                  (3,
                   'March'),
                  (4,
                   'April'),
                  (5,
                   'May'),
                  (6,
                   'June'),
                  (7,
                   'July'),
                  (8,
                   'August'),
                  (9,
                   'September'),
                  (10,
                   'October'),
                  (11,
                   'November'),
                  (12,
                   'December')

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear              DateYear,
             'Monthly Sales ' + @DateYear
             + ' (Billing Invoice)' Label

      SELECT MonthNumber,
             MonthName,
             SUM(TotalSales) TotalSales
      FROM   (SELECT m.MonthNumber,
                     m.MonthName,
                     ISNULL(clientDeposit.DepositAmount, 0) AS TotalSales
              FROM   @Months m
                     LEFT JOIN tClientDeposit clientDeposit
                            ON DatePart(MONTH, [Date]) = M.MonthNumber
                               AND clientDeposit.ID_Company = @ID_Company
                               AND clientDeposit.ID_FilingStatus IN ( 3, 17 )
                               AND YEAR(clientDeposit.Date) = @DateYear
              UNION ALL
              SELECT m.MonthNumber,
                     m.MonthName,
                     ISNULL(billing.TotalAmount, 0) AS TotalSales
              FROM   @Months m
                     LEFT JOIN tBillingInvoice billing
                            ON DatePart(MONTH, [Date]) = M.MonthNumber
                               AND billing.ID_Company = @ID_Company
                               AND billing.ID_FilingStatus = 3
                               AND YEAR(billing.Date) = @DateYear) tbl
      GROUP  BY MonthNumber,
                MonthName
      ORDER  BY MonthNumber,
                MonthName
  END

GO
