﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create   
 view [dbo].[vAppointment_FilingStatus]
AS
  Select ID,
         Name
  FROM   tFilingStatus
  WHERE  ID IN ( 2, 8, 13, 4, 21 )

GO
