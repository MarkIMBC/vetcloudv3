﻿CREATE TYPE [dbo].[typPatienDentalExamImage] AS TABLE(
	[ID] [int] NOT NULL,
	[Comment] [varchar](max) NULL,
	[ImageValue] [varchar](max) NULL
)
GO
