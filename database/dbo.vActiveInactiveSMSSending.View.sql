﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveInactiveSMSSending] AS   
				SELECT   
				 H.*  
				FROM vInactiveSMSSending H  
				WHERE IsActive = 1
GO
