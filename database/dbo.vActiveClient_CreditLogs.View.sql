﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClient_CreditLogs] AS   
				SELECT   
				 H.*  
				FROM vClient_CreditLogs H  
				WHERE IsActive = 1
GO
