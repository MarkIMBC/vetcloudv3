﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoice_SMSPayableRemider_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vBillingInvoice_SMSPayableRemider_SMSStatus H  
    WHERE ISNULL(IsActive, 0) = 1
GO
