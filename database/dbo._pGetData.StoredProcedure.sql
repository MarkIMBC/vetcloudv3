﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetData]
    @ID_ViewType INT ,
    @Oid UNIQUEIDENTIFIER ,
    @Oid_Control UNIQUEIDENTIFIER = NULL,
	@ID_Tab UNIQUEIDENTIFIER = NULL,
	@ID_Section UNIQUEIDENTIFIER = NULL
AS
    DECLARE @SQL VARCHAR(MAX) , @ID_Model UNIQUEIDENTIFIER = NULL
    SELECT  '' AS [_]
    IF @ID_ViewType = 1
        BEGIN
            SET @SQL = 'SELECT Oid, Name, Caption, DisplayProperty,CAST(IsNull(IsAllowEdit,0) AS BIT) AS IsAllowEdit, CAST(IsNull(IsRequired,0) AS BIT) AS IsRequired,
					 CAST(IsNull(IsActive,1) AS BIT) AS IsActive, VisibleIndex, ID_ControlType, DataSource, CAST(IsNull(Fixed,0) AS BIT) AS Fixed, 
							FixedPosition ,Width, CAST(IsNull(IsVisible,1) AS BIT) AS IsVisible, ID_SummaryType FROM _tListView_Detail WHERE ID_ListView = '''
                + CAST(@Oid AS VARCHAR(100)) + '''' + ISNULL(' AND Oid = ''' + CAST(@Oid_Control AS VARCHAR(100)) + '''', '')
                + ' ORDER BY DateCreated DESC'
            SET @ID_Model = '62A90881-9DC1-4420-AF8A-ABC6A26B1C82' --ListView_Detail
        END
    ELSE
        IF @ID_ViewType = 2
            BEGIN
                SET @SQL = 'SELECT Oid, Name, Caption, CAST(IsNull(IsActive,1) AS BIT) AS IsActive, CAST(IsNull(IsReadOnly,0) AS BIT) AS IsReadOnly, 
				CAST(IsNull(IsRequired,0) AS BIT) AS IsRequired,
				CAST(IsNull(IsDisabled,0) AS BIT) AS IsDisabled,
				ID_LabelLocation,
				GroupIndex,
				ValueExpr,
				 SeqNo, ID_ControlType, ID_PropertyType, DisplayProperty, DataSource, ID_Tab, ID_Section, ColCount, ColSpan, Height, ID_LookUp_ListView, LookUp_ListView_Caption , LookUp_ListView_DataSource
				FROM _tDetailView_Detail WHERE ID_DetailView = '''
                    + CAST(@Oid AS VARCHAR(100)) + ''''
                    + ISNULL(' AND Oid = ''' + CAST(@Oid_Control AS VARCHAR(100)) + '''', '')
					+ ISNULL(' OR ( ID_Tab = ''' + CAST(@ID_Tab AS VARCHAR(100)) + ''')', '')
					+ ISNULL(' OR ( ID_Section = ''' + CAST(@ID_Section AS VARCHAR(100)) + ''')', '')
                    + ' ORDER BY DateCreated DESC'
                SET @ID_Model = '71286C19-EA5D-4CD8-8B83-33670D418C0E'--DetailView_Detail
            END
        ELSE
            IF @ID_ViewType = 3
                BEGIN
                    SET @SQL = 'SELECT Oid, Name, Caption, IsActive, ID_PropertyType, ID_PropertyModel, ID_ModelProperty_Key FROM _tModel_Property WHERE ( 1 = 1 ) '
					+ ISNULL(' AND ID_Model = ''' + CAST(@Oid AS VARCHAR(100)) + '''', '')
					+ ISNULL(' AND Oid = ''' + CAST(@Oid_Control AS VARCHAR(100)) + '''', '')
                    SET @ID_Model = '00F4342C-BB85-4E56-AF82-4D5342627A50'--DetailView_Detail
                END
		ELSE IF @ID_ViewType = 100
			BEGIN
				SET @SQL = 'SELECT Oid, Name, Caption, SeqNo, IsActive, ID_Parent FROM dbo.[_tNavigation] WHERE ID_Parent = ''' + CAST(@Oid AS VARCHAR(100)) + ''' OR Oid  = ''' + CAST(@Oid AS VARCHAR(100))  + ''''
				SET @ID_Model = '3F43B3EF-B221-444B-89F1-214EA25869B0'
			END

    DECLARE @PrimaryKey VARCHAR(100);
    SELECT  @PrimaryKey = PrimaryKey
    FROM    dbo.[_vModel]
    WHERE   Oid = @ID_Model
--SET @SQL = 'SELECT * FROM tBusinessPartner' 
--SET @PrimaryKey = 'ID'
    SELECT  @SQL AS [$DataSource],
            @ID_Model AS ID_Model ,
            @PrimaryKey AS PK
GO
