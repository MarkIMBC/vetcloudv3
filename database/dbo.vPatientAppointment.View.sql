﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vPatientAppointment]
AS
  SELECT H.*,
         CONVERT(VARCHAR, H.DateStart, 0) DateStartString,
         CONVERT(VARCHAR, H.DateEnd, 0)   DateEndString,
         SOAPType.Name                    Name_SOAPType,
         client.Name                      Name_Client,
         patient.Name                     Name_Patient,
         fs.Name                          Name_FilingStatus,
         fsAppointment.Name               Appointment_Name_FilingStatus,
         vet.Name                         AttendingPhysician_Name_Employee,
         groomer.Name                     Groomer_Name_Employee
  FROM   dbo.tPatientAppointment H
         LEFT JOIN dbo.tSOAPType SOAPType
                ON SOAPType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tFilingStatus fsAppointment
                ON fsAppointment.ID = H.Appointment_ID_FilingStatus
         LEFT JOIN dbo.vEmployee vet
                ON vet.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.vEmployee groomer
                ON groomer.ID = H.Groomer_ID_Employee

GO
