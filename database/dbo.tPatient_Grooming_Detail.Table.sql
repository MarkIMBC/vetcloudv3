﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Grooming_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient_Grooming] [int] NULL,
	[ID_Item] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[Quantity] [int] NULL,
	[DateExpiration] [datetime] NULL,
 CONSTRAINT [PK_tPatient_Grooming_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Grooming_Detail] ON [dbo].[tPatient_Grooming_Detail]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_Detail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] CHECK CONSTRAINT [FK_tPatient_Grooming_Detail_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] CHECK CONSTRAINT [FK_tPatient_Grooming_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] CHECK CONSTRAINT [FK_tPatient_Grooming_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPatient_Grooming_Detail_ID_Patient_Grooming] FOREIGN KEY([ID_Patient_Grooming])
REFERENCES [dbo].[tPatient_Grooming] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] CHECK CONSTRAINT [FKtPatient_Grooming_Detail_ID_Patient_Grooming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateCreated_tPatient_Grooming_Detail] ON [dbo].[tPatient_Grooming_Detail] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatient_Grooming_Detail SET DateCreated = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] ENABLE TRIGGER [rDateCreated_tPatient_Grooming_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateModified_tPatient_Grooming_Detail] ON [dbo].[tPatient_Grooming_Detail] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatient_Grooming_Detail SET DateModified = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatient_Grooming_Detail] ENABLE TRIGGER [rDateModified_tPatient_Grooming_Detail]
GO
