﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveToothInfo] AS   
				SELECT   
				 H.*  
				FROM vToothInfo H  
				WHERE IsActive = 1
GO
