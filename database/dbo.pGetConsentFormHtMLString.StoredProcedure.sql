﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetConsentFormHtMLString](@ID_UserSession INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConsentFormHtMLString VARCHAR(MAX) = '';

	  SET @ConsentFormHtMLString = '
	<p>
		I am the legal owner and the person in lawful custody of the animal stated above and warrant that I have full 
		power of disposition and to contract in regards of the said animal.
	</p>
	<p>
		I hereby give  full consent __________________ to perform surgery and/or treatments for my pet. I 
		understand that during the performance of this procedure, unforeseen conditions may be revealed that may 
		necessitate an extension or variance in the planned procedure. If the occurrence of unforeseen condition 
		arises in the course of the operation, I further request and authorize the veterinarian to do whatever he/she 
		deems advisable. While I expect all procedures to be done to the best of the abilities of the professional 
		staff, I realize that no guarantee or warranty can ethically or professionally be made regarding the results or 
		cure. 
	</p>
	<p>
		The nature and purpose of the operation and possibility of complications have been explained to me by the 
		veterinarian. I acknowledge that no guarantee and assurance has been made asto the resultsthat may be 
		obtained. It is understood that whatever the outcome of the procedure, the clinic (CLINIC NAME) and the 
		veterinarian/s and the staff will not be held liable for any legal charges and claims which may be demanded 
		by anyone.
	</p>
	<p>
		I agree to assume financial responsibility for payment of charges incurred during this admission to be paid in 
		full at the time of discharge. Failure to pay upon demand, the clinic will initiate legal actions for collection of 
		the fees. I am executing and signing this consent form to attest to the truth of the foregoing facts for 
		whatever legal purpose it may serve.
	</p>
'
      SELECT '_'

      SELECT @Success               Success,
             @ConsentFormHtMLString ConsentFormHtMLString
  END 
GO
