﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Grooming] AS   
    SELECT   
     H.*  
    FROM vPatient_Grooming H  
    WHERE ISNULL(IsActive, 0) = 1
GO
