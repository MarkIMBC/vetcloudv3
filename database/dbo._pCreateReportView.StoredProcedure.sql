﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pCreateReportView] @ReportName   VARCHAR(300), 
                                           @IsNavigation BIT          = 1
AS
     DECLARE @Oid_Report UNIQUEIDENTIFIER= NEWID();

     INSERT INTO dbo._tReport
     (Oid, 
      Code, 
      Name, 
      IsActive, 
      Comment, 
      DateCreated, 
      DateModified, 
      ID_CreatedBy, 
      ID_LastModifiedBy, 
      ReportPath
     )
     VALUES
     (@Oid_Report, -- Oid - uniqueidentifier
      NULL, -- Code - varchar
      @ReportName, -- Name - varchar
      1, -- IsActive - bit
      NULL, -- Comment - varchar
      GETDATE(), -- DateCreated - datetime
      GETDATE(), -- DateModified - datetime
      1, -- ID_CreatedBy - int
      NULL, -- ID_LastModifiedBy - int
      @ReportName	 -- ReportPath - varchar
     );

     DECLARE @Oid_View UNIQUEIDENTIFIER= NEWID();

     INSERT INTO dbo._tView
     (Oid, 
      Code, 
      Name, 
      IsActive, 
      ID_ListView, 
      ID_Report, 
      ID_Dashboard, 
      ID_ViewType, 
      CustomViewPath, 
      ControllerPath, 
      Comment, 
      DateCreated, 
      ID_CreatedBy, 
      ID_LastModifiedBy, 
      DateModified, 
      ID_Model, 
      DataSource
     )
     VALUES
     (@Oid_View, -- Oid - uniqueidentifier
      NULL, -- Code - varchar
      @ReportName + '_ReportView', -- Name - varchar
      1, -- IsActive - bit
      NULL, -- ID_ListView - uniqueidentifier
      @Oid_Report, -- ID_Report - uniqueidentifier
      NULL, -- ID_Dashboard - uniqueidentifier
      3, -- ID_ViewType - int
      NULL, -- CustomViewPath - varchar
      NULL, -- ControllerPath - varchar
      NULL, -- Comment - varchar
      GETDATE(), -- DateCreated - datetime
      1, -- ID_CreatedBy - int
      NULL, -- ID_LastModifiedBy - int
      GETDATE(), -- DateModified - datetime
      NULL, -- ID_Model - uniqueidentifier
      NULL -- DataSource - varchar
     );

     IF(@IsNavigation = 1)
         BEGIN

             INSERT INTO dbo._tNavigation
             (Oid, 
              Code, 
              Name, 
              IsActive, 
              Comment, 
              ID_CreatedBy, 
              ID_LastModifiedBy, 
              DateCreated, 
              DateModified, 
              ID_View, 
              Caption, 
              Icon, 
              SeqNo, 
              ID_Parent
             )
             VALUES
             (NEWID(), -- Oid - uniqueidentifier
              NULL, -- Code - varchar
              @ReportName + '_Navigation', -- Name - varchar
              1, -- IsActive - bit
              NULL, -- Comment - varchar
              1, -- ID_CreatedBy - int
              NULL, -- ID_LastModifiedBy - int
              GETDATE(), -- DateCreated - datetime
              GETDATE(), -- DateModified - datetime
              @Oid_View, -- ID_View - uniqueidentifier
              @ReportName, -- Caption - varchar
              NULL, -- Icon - varchar
              NULL, -- SeqNo - int
              '4E94C28D-7955-4726-8676-65B06A142876' -- ID_Parent - uniqueidentifier
             );
     END;
GO
