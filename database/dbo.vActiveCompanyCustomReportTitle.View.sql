﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompanyCustomReportTitle] AS   
    SELECT   
     H.*  
    FROM vCompanyCustomReportTitle H  
    WHERE IsActive = 1
GO
