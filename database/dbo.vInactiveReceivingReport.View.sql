﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveReceivingReport] AS   
				SELECT   
				 H.*  
				FROM vReceivingReport H  
				WHERE ISNULL(IsActive, 0) = 0
GO
