﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pCheckForeignKeyObject] @TableName VARCHAR(MAX), @ID_CurrentObject VARCHAR(100) = -1
AS
DECLARE @SQL VARCHAR(MAX) = 'SELECT NULL AS Model, 0 AS Count UNION ALL '
SELECT  @SQL = @SQL + 'SELECT ''' + ISNULL(m.DisplayName, m.Name) + ''' , COUNT(ID)	FROM ' + t.name + ' WHERE ' + c.name + ' = '+ @ID_CurrentObject + ' UNION ALL '
FROM    sys.foreign_key_columns AS fk
        INNER JOIN sys.tables AS t ON fk.parent_object_id = t.object_id
        INNER JOIN sys.columns AS c ON fk.parent_object_id = c.object_id
                                       AND fk.parent_column_id = c.column_id
        INNER JOIN sys.objects AS o ON fk.constraint_object_id = o.object_id
		LEFT JOIN dbo.[_tModel] m ON UPPER(m.TableName) = UPPER(t.name)
WHERE   fk.referenced_object_id = ( SELECT  object_id
                                    FROM    sys.tables
                                    WHERE   name = @TableName
                                  ) 
									AND	m.Oid IS NOT NULL

SET @SQL = @SQL + ' SELECT NULL,0'
--PRINT @SQL
EXEC( 'SELECT * FROM (' + @SQL + ') H WHERE H.Model IS NOT NULL AND Count > 0')
GO
