﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create   
 PROC [dbo].[pAutoGeneratePatientWaitingListByCompanies](@IDs_Company typIntList ReadOnly)
as
    DECLARE @record TABLE
      (
         RowID      INT,
         ID_Company INT
      )
    DECLARE @currentCounter INT = 1
    DECLARE @maxCounter INT = 1

    INSERT @record
    SELECT ROW_NUMBER()
             OVER(
               ORDER BY ID ASC) AS RowID,
           ID
    FROM   @IDs_Company

    SELECT @maxCounter = COUNT(*)
    FROM   @IDs_Company

    WHILE @currentCounter <= @maxCounter
      BEGIN
          DECLARE @ID_Company INT = 0

          SELECT @ID_Company = ID_Company
          FROM   @record
          WHERE  RowID = @currentCounter

          exec pAutoGeneratePatientWaitingListPerCompany
            @ID_Company

          SET @currentCounter = @currentCounter + 1
      END

GO
