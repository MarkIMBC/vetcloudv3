﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClient] AS   
				SELECT   
				 H.*  
				FROM vClient H  
				WHERE IsActive = 1
GO
