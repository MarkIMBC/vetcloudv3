﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  
 PRoc [dbo].[temp_pInsertPatientWellness](@ID_Company                     int,  
                                       @ID_Client                      int,  
                                       @ID_Patient                     INT,  
                                       @AttendingPhysician_ID_Employee INT)  
as  
  begin  
      DECLARE @ID_Patient_Wellness INT = 0  
  
      INSERT INTO [dbo].[tPatient_Wellness]  
                  ([IsActive],  
                   [ID_Company],  
                   [DateCreated],  
                   [DateModified],  
                   [ID_CreatedBy],  
                   [ID_LastModifiedBy],  
                   [Date],  
                   [ID_Client],  
                   [ID_Patient],  
                   [AttendingPhysician_ID_Employee],  
                   ID_FilingStatus)  
      VALUES      (1,  
                   @ID_Company,  
                   GEtdate(),  
                   GETDate(),  
                   1,  
                   1,  
                   GETDATE(),  
                   @ID_Client,  
                   @ID_Patient,  
                   @AttendingPhysician_ID_Employee,  
                   1)  
  
      SET @ID_Patient_Wellness = @@IDENTITY  
  
      INSERT INTO [dbo].[tPatient_Wellness_Schedule]  
                  ([IsActive],  
                   [ID_Company],  
                   [Comment],  
                   [DateCreated],  
                   [DateModified],  
                   [ID_CreatedBy],  
                   [ID_LastModifiedBy],  
                   [ID_Patient_Wellness],  
                   [Date])  
      VALUES      (1,  
                   @ID_Company,  
                   '',  
                   GETDATE(),  
                   GETDATE(),  
                   1,  
                   1,  
                   @ID_Patient_Wellness,  
                   '2023-03-18')  
  END  

GO
