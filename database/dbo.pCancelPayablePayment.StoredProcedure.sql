﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE 
 PROC [dbo].[pCancelPayablePayment](@IDs_PayablePayment typIntList READONLY,
                                   @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT '_';

      Update tPayablePayment
      SET    ID_FIlingStatus = 4
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      DECLARE @IDs_Payable typIntList

      INSERT @IDs_Payable
      SELECT ID_Payable
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      exec dbo.pUpdatePayableStatus
        @IDs_Payable

      SELECT @Success Success,
             @message message;
  END

GO
