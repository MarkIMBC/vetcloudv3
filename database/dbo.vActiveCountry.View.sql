﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCountry] AS   
				SELECT   
				 H.*  
				FROM vCountry H  
				WHERE IsActive = 1
GO
