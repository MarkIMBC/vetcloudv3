﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pReceiveInventory] (@record         typReceiveInventory READONLY,
                                  @ID_UserSession Int)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(MAX) = '';
      DECLARE @ID_User INT = 0;
      DECLARE @count_noitemid INT = 0;

      SELECT @ID_User = ID_User
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      BEGIN TRY
          SELECT @count_noitemid = COUNT(*)
          FROM   @record
          WHERE  ISNULL(ID_Item, 0) = 0;

          IF ( @count_noitemid > 0 )
            BEGIN;
                THROW 50005, N'Reference Item is required.', 1;
            END;

          INSERT INTO dbo.tInventoryTrail
                      (Code,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       DateExpired,
                       BatchNo,
                       ID_FilingStatus,
                       ID_Company,
                       DateCreated,
                       Date,
                       Comment,
                       ID_CreatedBy)
          SELECT CASE
                   WHEN ISNULL(record.Code, '') = '' THEN 'Adjust Inventory'
                   ELSE record.Code
                 END,
                 record.ID_Item,
                 case
                   when record.IsAddInventory = 0 then 0 - Abs(record.Quantity)
                   else Abs(record.Quantity)
                 end,
                 record.UnitPrice,
                 record.DateExpired,
                 record.BatchNo,
                 record.ID_FilingStatus,
                 item.ID_Company,
                 GETDATE(),
                 GETDATE(),
                 record.Comment,
                 @ID_User
          FROM   @record record
                 INNER JOIN dbo.tItem item
                         ON item.ID = record.ID_Item;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   @record

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item
      -------------------------------------------------------  
      END TRY
      BEGIN CATCH
          SET @Success = 0;
          SET @message = ERROR_MESSAGE();
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
