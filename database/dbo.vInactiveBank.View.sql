﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBank] AS   
				SELECT   
				 H.*  
				FROM vBank H  
				WHERE ISNULL(IsActive, 0) = 0
GO
