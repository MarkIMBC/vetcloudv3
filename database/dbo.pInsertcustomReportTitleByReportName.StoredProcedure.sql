﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertcustomReportTitleByReportName](@ID_Company        INT,
                                                @Name_Report       VARCHAR(MAX),
                                                @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @Oid_Report VARCHAR(MAX) = '';

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @Name_Report

      EXEC dbo.pInsertcustomReportTitle
        @ID_Company,
        @Oid_Report,
        @customReportTitle
  END

GO
