﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTeacher] AS   
				SELECT   
				 H.*  
				FROM vTeacher H  
				WHERE IsActive = 1
GO
