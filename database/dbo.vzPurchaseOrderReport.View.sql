﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vzPurchaseOrderReport]
AS
SELECT poHed.ID,
       poHed.Code,
       poHed.Date,
       poHed.ID_Supplier,
       poHed.Name_Supplier,
       poHed.Name_FilingStatus,
       poHed.CreatedBy_Name_User,
       poHed.ApprovedBy_Name_User,
       poHed.Name_TaxScheme,
       poHed.DateApproved,
       poHed.GrossAmount,
       poHed.VatAmount,
       poHed.NetAmount,
       poDetail.ID ID_PurchaseOrder_Detail,
       poDetail.Name_Item,
       poDetail.Quantity,
       poDetail.UnitPrice,
       poDetail.Amount,
       poHed.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.vPurchaseOrder poHed
    LEFT JOIN dbo.vPurchaseOrder_Detail poDetail
        ON poHed.ID = poDetail.ID_PurchaseOrder
    LEFT JOIN dbo.vCompany company
        ON company.ID = poHed.ID_Company;
GO
