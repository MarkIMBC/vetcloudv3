﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInsertDistributionUtilitiesFromTempTable](@importTableName VARCHAR(MAX))
as
  BEGIN
      DECLARE @tempID VARCHAR(MAX) = @importTableName + ' '
        + CONVERT(VARCHAR(MAX), NEWID())
      DECLARE @sqlStatement VARCHAR(MAX) = 'SELECT 
	  [Column 1] ,
	  [Column 2] ,
	  [Column 3] ,
	  [Column 4] ,
	  [Column 5] ,
	  [Column 6] ,
	  [Column 7] ,
	  [Column 8] ,
	  [Column 9] ,
	  [Column 10],
	  [Column 11],
	  [Column 12],
	  [Column 13],
	  [GUID]     ,
	  [RowIndex] 
	  
	  
	  
	  FROM /*tempTableName*/'
      DECLARE @RowIndex int = 0
      DECLARE @Year int = 0
      DECLARE @Month int = 0

      set @sqlStatement = REPLACE(@sqlStatement, '/*tempTableName*/', @importTableName)

      DECLARE @excel TABLE
        (
           [Column 1]  VARCHAR(MAX),
           [Column 2]  VARCHAR(MAX),
           [Column 3]  VARCHAR(MAX),
           [Column 4]  VARCHAR(MAX),
           [Column 5]  VARCHAR(MAX),
           [Column 6]  VARCHAR(MAX),
           [Column 7]  VARCHAR(MAX),
           [Column 8]  VARCHAR(MAX),
           [Column 9]  VARCHAR(MAX),
           [Column 10] VARCHAR(MAX),
           [Column 11] VARCHAR(MAX),
           [Column 12] VARCHAR(MAX),
           [Column 13] VARCHAR(MAX),
           [GUID]      VARCHAR(MAX),
           [RowIndex]  VARCHAR(MAX)
        )

      INSERT @excel
      EXEC (@sqlStatement)

      Update @excel
      SET    GUID = GUID + @importTableName

      INSERT INTO [dbo].[DistributionUtilities]
                  ([ShortName],
                   [BusinessName],
                   [Active],
                   [CreatedAt],
                   [UpdatedAt],
                   [Uid])
      SELECT [Column 1],
             [Column 2],
             1,
             GETDATE(),
             GETDATE(),
             GUID
      FROm   @excel
      where  LEN([Column 1]) > 0
             and RowIndex > 1
             AND Guid NOT IN (SELECT [Uid]
                              FROM   [DistributionUtilities]
                              WHERE  [Uid] IS NOT NULL)
             AND [Column 1] NOT IN (SELECT ShortName
                                    FROM   [DistributionUtilities]
                                    WHERE  [Uid] IS NOT NULL)

      Update [DistributionUtilities]
      set    ShortName = excel.[Column 1],
             BusinessName = excel.[Column 2],
             BusinessAddress = excel.[Column 3],
             Region = excel.[Column 4],
             UtilityType = excel.[Column 5],
             RepLastName = excel.[Column 6],
             RepFirstName = excel.[Column 7],
             RepPosition = excel.[Column 8],
             RepEmail = excel.[Column 9],
             RepOfficeNo = excel.[Column 10]
      FROM   [DistributionUtilities] hed
             inner join @excel excel
                     on hed.ShortName = excel.[Column 1]
  --SELECT * FROM DistributionUtilities hed inner join @excel excel on [Column 1] = ShortName 
  --LEFT JOIN Provinces province on excel.[Column 4] = province.ProvinceDesc
  --select * FROM Provinces
  END

GO
