﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs](@IDs_CurrentObject TYPINTLIST ReadOnly)
as
  BEGIN
      DECLARE @success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @TranName VARCHAR(MAX);
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

      SET @TranName = 'pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs-'
                      + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID

                DECLARE @IDs_Patient_SOAP TYPINTLIST

                INSERT @IDs_Patient_SOAP
                SELECT ID
                FROM   @IDs_CurrentObject

                EXEC dbo.pUpdatPatientConfinementItemsServicesByPatientSOAP
                  @IDs_Patient_SOAP

                --------------------------------------------            
                exec pRemoveMulticatePatientConfinementFromSOAP
                  @IDs_Patient_SOAP

                --------------------------------------------            
                DECLARE @IDs_Patient TYPINTLIST

                INSERT @IDs_Patient
                SELECT patient.ID
                FROM   tPatient_SOAP soap
                       INNER JOIN tPatient patient
                               ON patient.ID = soap.ID_Patient
                       INNER JOIN @IDs_CurrentObject ids
                               ON soap.ID = ids.ID

                EXEC dbo.PupDatePatientsLastVisitedDate
                  @IDs_Patient

                exec pUpdatePatientLastPatientRecordID
                  @IDs_Patient

                -----------------------------------------------        
                exec pCancelMulticatePatient_SOAP_Record
                  @IDs_Patient_SOAP

                exec pUpdatePatient_SOAP_BillingStatus
                  @IDs_Patient_SOAP

                --------------------------------------------            
                EXEC _pCancelPatient_Confinement_temp

                Update tPatient_SOAP
                SET    DateRunAfterSavedProcess = GETDATE(),
                       RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID
            END TRY
            BEGIN CATCH
                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID
                SET @success = 0;
                SET @message = ERROR_MESSAGE();
            END CATCH

            SELECT @success Success,
                   @message Message
        END
  END

GO
