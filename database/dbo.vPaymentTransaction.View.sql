﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPaymentTransaction]
AS
  SELECT H.*,
         CONVERT(VARCHAR(100), H.Date, 101)            DateString,
         UC.Name                                       AS CreatedBy,
         UM.Name                                       AS LastModifiedBy,
         approvedUser.Name                             AS ApprovedBy_Name_User,
         cancelUser.Name                               AS CanceledBy_Name_User,
         fs.Name                                       Name_FilingStatus,
         ISNULL(biHed.Name_Client, WalkInCustomerName) Name_Client,
         biHed.Name_Patient,
         taxScheme.Name                                Name_TaxScheme,
         biHed.Code                                    Code_BillingInvoice,
         biHed.GrossAmount                             GrossAmount_BillingInvoice,
         biHed.VatAmount                               VatAmount_BillingInvoice,
         biHed.NetAmount                               NetAmount_BillingInvoice,
         biHed.RemainingAmount                         RemainingAmount_BillingInvoice,
         pMth.Name                                     Name_PaymentMethod,
         cardType.Name                                 Name_CardType,
         CONVERT(VARCHAR, H.DateCreated, 0)            DateCreatedString,
         CONVERT(VARCHAR, H.DateModified, 0)           DateModifiedString,
         CONVERT(VARCHAR, H.DateApproved, 0)           DateApprovedString,
         CONVERT(VARCHAR, H.DateCanceled, 0)           DateCanceledString,
         biHed.PatientNames,
         bank.Name                                     Name_Bank
  FROM   tPaymentTransaction H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.vBillingInvoice biHed
                ON biHed.ID = H.ID_BillingInvoice
         LEFT JOIN dbo.tPaymentMethod pMth
                ON pMth.ID = H.ID_PaymentMethod
         LEFT JOIN dbo.tTaxScheme taxScheme
                ON taxScheme.ID = biHed.ID_TaxScheme
         LEFT JOIN dbo.tCardType cardType
                ON cardType.ID = H.ID_CardType
         LEFT JOIN dbo.tBank bank
                ON bank.ID = H.ID_Bank

GO
