﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[vzReceiveDepositCredit]
as
Select credLogs.ID,
       CONVERT(DATE, credLogs.DateCreated) Date ,
       credLogs.Code,
       credLogs.Comment,
       credLogs.ID_Client                    ID_Client,
       client.Name                           Name_Client,
       credLogs.CreditAmount                 CreditAmount,
       client.ID_Company                     ID_Company,
       company.Name                          Name_Company,
       company.ImageLogoLocationFilenamePath ImageLogoLocationFilenamePath,
       company.Address                       Address_Company,
       company.ContactNumber                 ContactNumber_Company,
       CASE
         WHEN LEN(company.Address) > 0 THEN '' + company.Address
         ELSE ''
       END
       + CASE
           WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
           ELSE ''
         END
       + CASE
           WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
           ELSE ''
         END                                 HeaderInfo_Company
FRom   tClient_CreditLogs credLogs
       LEFT JOIN tClient client
              on credLogs.ID_Client = client.ID
       LEFT JOIN vCompany company
              on company.ID = client.ID_Company
	--		  WHERE ISNULL(CreditAmount, 0) > 0
GO
