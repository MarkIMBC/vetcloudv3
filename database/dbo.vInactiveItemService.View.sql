﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItemService] AS   
				SELECT   
				 H.*  
				FROM vItemService H  
				WHERE ISNULL(IsActive, 0) = 0
GO
