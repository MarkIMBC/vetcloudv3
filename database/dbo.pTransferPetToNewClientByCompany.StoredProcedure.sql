﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pTransferPetToNewClientByCompany](@GUID_Company            VARCHAR(MAX),
                                            @Code_Patient            VARCHAR(MAX),
                                            @Source_Code_Client      VARCHAR(MAX),
                                            @Destination_Code_Client VARCHAR(MAX))
as
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   vCompanyActive
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      DECLARE @ID_Patient INT = 0
      DECLARE @Source_ID_Client INT = 0
      DECLARE @Destination_ID_Client INT = 0
      DECLARE @Source_Name_Client Varchar(MAX) = ''
      DECLARE @Destination_Name_Client Varchar(MAX) = ''
      DECLARE @Name_Patient Varchar(MAX) = ''

      SELECT @ID_Patient = ID,
             @Name_Patient = Name
      FROm   vPatient
      WHERE  COde = @Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Source_ID_Client = ID,
             @Source_Name_Client = Name
      FROm   vClient
      WHERE  COde = @Source_Code_Client
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Client = ID,
             @Destination_Name_Client = Name
      FROm   vClient
      WHERE  COde = @Destination_Code_Client
             AND ID_Company = @ID_Company

      SELECT Code,
             Name,
             Name_Client
      FROm   vPatient
      where  ID = @ID_Patient
             and ID_Company = @ID_Company

      SELECT ID,
             Code,
             Name,
             'Source 1',
             IsActive
      FROm   vClient
      WHERE  ID = @Source_ID_Client
             AND ID_Company = @ID_Company
      Union ALL
      SELECT ID,
             Code,
             Name,
             'Destination',
             IsActive
      FROm   vClient
      WHERE  ID = @Destination_ID_Client
             AND ID_Company = @ID_Company

      DEclare @Comment VARCHAR(MAX) = 'Transfer Patient /*Name_Patient*/ From /*Source_Name_Client*/ to /*Destination_Name_Client*/.'
      DEclare @_Comment VARCHAR(MAX) = ''

      SET @Comment = REPLACE(@Comment, '/*Name_Patient*/', @Name_Patient)
      SET @Comment = REPLACE(@Comment, '/*Source_Name_Client*/', @Source_Name_Client)
      SET @Comment = REPLACE(@Comment, '/*Destination_Name_Client*/', @Destination_Name_Client)

      exec [pAddRecordValueTransferLogSingleRecord]
        @Source_ID_Client,
        @Destination_ID_Client,
        'ID',
        'ID_Client',
        'tPatient',
        @Comment

      SET @_Comment = @_Comment + '- Medical Record'

      exec [pAddRecordValueTransferLogSingleRecord]
        @Source_ID_Client,
        @Destination_ID_Client,
        'ID',
        'ID_Client',
        'tPatient_SOAP',
        @Comment

      SET @_Comment = @_Comment + '- Patient Wellness'

      exec [pAddRecordValueTransferLogSingleRecord]
        @Source_ID_Client,
        @Destination_ID_Client,
        'ID',
        'ID_Client',
        'tPatient_Wellness',
        @Comment

      SET @_Comment = @_Comment
                      + '- Veterinary Health Certificate'

      exec [pAddRecordValueTransferLogSingleRecord]
        @Source_ID_Client,
        @Destination_ID_Client,
        'ID',
        'ID_Client',
        'tVeterinaryHealthCertificate',
        @Comment

      SELECT Code,
             Name,
             Name_Client
      FROm   vPatient
      where  ID = @ID_Patient
             and ID_Company = @ID_Company
  -----------------------------------------------------------------    
  END

GO
