﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBusinessPartner_Contact] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartner_Contact H  
				WHERE IsActive = 1
GO
