﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 FUNCTION [dbo].[fGetCleanedStringForExtratedRecord](@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @record VARCHAR(MAX) = ''

      SET @record = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@Value, '  ', ' '), CHAR(9), ''), CHAR(10), ''), CHAR(13), '')))
      SET @record = REPLACE(@Value, '"', '``')

      RETURN @record
  END 
GO
