﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tColumnAlignment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK__tColumnAlignment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tColumnAlignment] ADD  CONSTRAINT [DF___tColumnA__IsAct__28B808A7]  DEFAULT ((1)) FOR [IsActive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated__tColumnAlignment]
		ON [dbo].[_tColumnAlignment]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[_tColumnAlignment] ENABLE TRIGGER [rDateCreated__tColumnAlignment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified__tColumnAlignment]
		ON [dbo].[_tColumnAlignment]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[_tColumnAlignment] ENABLE TRIGGER [rDateModified__tColumnAlignment]
GO
