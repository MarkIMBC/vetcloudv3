﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGetForExtractCancelledBillingInvoiceAndPaymentTransaction](@GUID      VARCHAR(MAX),
                                                                      @DateStart DATE,
                                                                      @DateEnd   DATE)
AS
  BEGIN
      DEclare @GUID_Company VARCHAR(MAX) = @GUID

      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT '' [Cancelled Billing Invoice],
             '' [Cancelled Payment Transaction]

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      SELECT company.Name                                                                             Name_Company,
             biHed.Code,
             ISNULL(FORMAT(biHed.Date, 'yyyy-MM-dd HH:mm:ss'), '')                                    Date,
             lTrim(rTrim(ISNULL(biHed.Name_Client + ' - ' + PatientNames, biHed.WalkInCustomerName))) Name_Client,
             bihed.Status,
             ISNULL(DiscountAmount, 0)                                                                DiscountAmount,
             ISNULL(SubTotal, 0)                                                                      SubTotal,
             ISNULL(TotalAmount, 0)                                                                   TotalAmount,
             ISNULL(RemainingAmount, 0)                                                               RemainingAmount,
             ISNULL(FORMAT(DateApproved, 'yyyy-MM-dd HH:mm:ss'), '')                                  DateApproved,
             ISNULL(ApprovedBy_Name_User, '')                                                         ApprovedBy_Name_User,
             ISNULL(FORMAT(DateCanceled, 'yyyy-MM-dd HH:mm:ss'), '')                                  DateCanceled,
             ISNULL(CanceledBy_Name_User, '')                                                         CanceledBy_Name_User
      FROM   vBillingInvoice biHed
             INNER JOIN vCompany company
                     ON company.ID = biHed.ID_Company
      WHERE  biHed.ID_FilingStatus IN ( 4 )
             AND company.ID = @ID_Company
             AND CONVERT(DATE, biHed.Date) BETWEEN @DateStart AND @DateEnd

      SELECT company.Name                                                                                   Name_Company,
             ptHed.Code,
             ISNULL(FORMAT(ptHed.Date, 'yyyy-MM-dd HH:mm:ss'), '')                                          Date,
             biHed.Code                                                                                     Code_BillingInvoice,
             ISNULL(FORMAT(biHed.Date, 'yyyy-MM-dd HH:mm:ss'), '')                                          Date_BillingInvoice,
             lTrim(rTrim(ISNULL(biHed.Name_Client + ' - ' + biHed.PatientNames, biHed.WalkInCustomerName))) Name_Client,
             ptHed.Name_FilingStatus,
             PayableAmount,
             PaymentAmount,
             ChangeAmount,
             ISNULL(FORMAT(ptHed.DateApproved, 'yyyy-MM-dd HH:mm:ss'), '')                                  DateApproved,
             ISNULL(ptHed.ApprovedBy_Name_User, '')                                                         ApprovedBy_Name_User,
             ISNULL(FORMAT(ptHed.DateCanceled, 'yyyy-MM-dd HH:mm:ss'), '')                                  DateCanceled,
             ISNULL(ptHed.CanceledBy_Name_User, '')                                                         CanceledBy_Name_User
      FROM   vPaymentTransaction ptHed
             INNER JOIN vBillingInvoice biHed
                     ON ptHed.ID_BillingInvoice = biHed.ID
             INNER JOIN vCompany company
                     ON company.ID = ptHed.ID_Company
      WHERE  ptHed.ID_FilingStatus IN ( 4 )
             AND company.ID = @ID_Company
             AND CONVERT(DATE, ptHed.Date) BETWEEN @DateStart AND @DateEnd
  END

GO
