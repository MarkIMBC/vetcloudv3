﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientSOAPList] AS   
				SELECT   
				 H.*  
				FROM vPatientSOAPList H  
				WHERE IsActive = 1
GO
