﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vUserDefinedTypes]
AS
    SELECT  TT.name AS [Type] ,
			TT.user_type_id,
            C.name AS [ColumnName] ,
            ST.name AS [DataType] ,
            C.system_type_id AS TypeID ,
            C.column_id AS ColumnId
    FROM    sys.table_types TT
            INNER JOIN sys.columns C ON C.object_id = TT.type_table_object_id
            INNER JOIN sys.systypes AS ST ON ST.xtype = C.system_type_id
--ORDER BY TT.NAME

GO
