﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetForSendSOAPPlan]
AS
  BEGIN
      DECLARE @Date DateTime = GETDATE();

      exec pGetSendSOAPPlan
        @Date,
        0
  END

GO
