﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pAddRecordValueTransferLog](@From_IDs_RecordValue typIntList READONLY,
                                      @To_ID_RecordValue    INT,
                                      @PrimaryColumn        VARCHAR(MAX),
                                      @ColumnName           VARCHAR(MAX),
                                      @TableName            VARCHAR(MAX),
                                      @Comment              VARCHAR(MAX))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX) = ''
      DECLARE @From_ID_RecordValueString VARCHAR(MAX) = ''
      DECLARE @log TABLE
        (
           PrimaryKeyID        INT,
           From_ID_RecordValue INT,
           To_ID_RecordValue   INT
        )

      SELECT @From_ID_RecordValueString = CONVERT(VARCHAR(MAX), ID) + ','
      FROM   @From_IDs_RecordValue

      SET @From_ID_RecordValueString = LEFT(@From_ID_RecordValueString, LEN(@From_ID_RecordValueString) - 1)

      SELECT @Oid_Model = Oid
      FROm   _tModel
      where  TableName = @TableName

      DECLARE @sql VARCHAR(MAX) = ''

      SET @sql = 'SELECT DISTINCT ' + @PrimaryColumn + ', '
                 + @ColumnName + ', '
                 + CONVERT(varchar(MAX), @To_ID_RecordValue)
                 + ' To_ID_RecordValue FROM ' + @TableName
                 + ' WHERE ' + @ColumnName + ' IN ('
                 + @From_ID_RecordValueString + ')'

      INSERT @log
      ExEC (@sql)

      DECLARE @_PrimaryKeyID INT
      DECLARE @_From_ID_RecordValue INT
      DECLARE @_To_ID_RecordValue INT
      DECLARE cursor_pAddRecordValueTransferLog CURSOR FOR
        SELECT PrimaryKeyID,
               From_ID_RecordValue,
               To_ID_RecordValue
        FROM   @log;

      OPEN cursor_pAddRecordValueTransferLog;

      FETCH NEXT FROM cursor_pAddRecordValueTransferLog INTO @_PrimaryKeyID, @_From_ID_RecordValue, @_To_ID_RecordValue

      WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @sql = 'Update /*TableName*/ SET /*ColumnName*/ = /*To_ID_RecordValue*/ WHERE /*PrimaryColumn*/ = /*PrimaryKeyID*/ AND  /*ColumnName*/ = /*From_ID_RecordValue*/'
            SET @sql = REPLACE(@sql, '/*TableName*/', @TableName)
            SET @sql = REPLACE(@sql, '/*ColumnName*/', @ColumnName)
            SET @sql = REPLACE(@sql, '/*To_ID_RecordValue*/', CONVERT(VARCHAR(MAX), @_To_ID_RecordValue))
            SET @sql = REPLACE(@sql, '/*PrimaryColumn*/', @PrimaryColumn)
            SET @sql = REPLACE(@sql, '/*PrimaryKeyID*/', CONVERT(VARCHAR(MAX), @_PrimaryKeyID))
            SET @sql = REPLACE(@sql, '/*From_ID_RecordValue*/', CONVERT(VARCHAR(MAX), @_From_ID_RecordValue))

            ExEC (@sql)

            INSERT INTO [dbo].[tRecordValueTransferLogs]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [Oid_Model],
                         [PrimaryColumn],
                         [PrimaryKeyID],
                         [ColumnName],
                         [From_ID_RecordValue],
                         [To_ID_RecordValue])
            VALUES      ('',
                         1,
                         1,
                         @Comment,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @Oid_Model,
                         @PrimaryColumn,
                         @_PrimaryKeyID,
                         @ColumnName,
                         @_From_ID_RecordValue,
                         @_To_ID_RecordValue)

            FETCH NEXT FROM cursor_pAddRecordValueTransferLog INTO @_PrimaryKeyID, @_From_ID_RecordValue, @_To_ID_RecordValue
        END;

      CLOSE cursor_pAddRecordValueTransferLog;

      DEALLOCATE cursor_pAddRecordValueTransferLog;
  END

GO
