﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientWaitingList_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vPatientWaitingList_SMSStatus H  
    WHERE IsActive = 1
GO
