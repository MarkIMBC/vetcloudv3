﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  CREATE PROC [dbo].[pCancelPaymentTransaction1] (@IDs_PaymentTransaction typIntList READONLY,
                                          @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Credits_ID_PaymentMethod INT = 5;
      DECLARE @IDs_BillingInvoice typIntList;
      DECLARE @ClientCredits typClientCredit
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          INSERT @IDs_BillingInvoice
                 (ID)
          SELECT ptHed.ID_BillingInvoice
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID;

          INSERT @ClientCredits
          SELECT biHed.ID_Client,
                 ptHed.Date,
                 ABS(ptHed.CreditAmount),
                 ptHed.Code,
                 'Re-deposit Credit as payment cancel.'
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID
                 LEFT JOIN tBillingInvoice biHed
                        on ptHed.ID_BillingInvoice = biHed.ID
          WHERE  ptHed.ID_PaymentMethod = @Credits_ID_PaymentMethod

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelPaymentTransaction_validation
            @IDs_PaymentTransaction,
            @ID_UserSession;

          UPDATE dbo.tPaymentTransaction
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPaymentTransaction bi
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON bi.ID = ids.ID;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          IF(SELECT COUNT(*)
             FROM   @ClientCredits) > 0
            BEGIN
                EXEC pAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
GO
