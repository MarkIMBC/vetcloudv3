﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBillingInvoice_SMSPayableRemider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_BillingInvoice] [int] NULL,
	[DateSent] [datetime] NULL,
	[SMSMessage] [varchar](max) NULL,
	[IsSent] [bit] NULL,
	[ContactNumber] [varchar](300) NULL,
	[ID_Client] [int] NULL,
	[TotalAmount_BillingInvoice] [decimal](18, 4) NULL,
	[RemainingAmount_BillingInvoice] [decimal](18, 4) NULL,
	[DateSchedule] [datetime] NULL,
	[IsSentSMS] [bit] NULL,
 CONSTRAINT [PK_tBillingInvoice_SMSPayableRemider] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBillingInvoice_SMSPayableRemider] ON [dbo].[tBillingInvoice_SMSPayableRemider]
(
	[ID_BillingInvoice] ASC,
	[ID_Client] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] CHECK CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_Company]
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] CHECK CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] CHECK CONSTRAINT [FK_tBillingInvoice_SMSPayableRemider_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateCreated_tBillingInvoice_SMSPayableRemider] ON [dbo].[tBillingInvoice_SMSPayableRemider] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tBillingInvoice_SMSPayableRemider SET DateCreated = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] ENABLE TRIGGER [rDateCreated_tBillingInvoice_SMSPayableRemider]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateModified_tBillingInvoice_SMSPayableRemider] ON [dbo].[tBillingInvoice_SMSPayableRemider] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tBillingInvoice_SMSPayableRemider SET DateModified = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tBillingInvoice_SMSPayableRemider] ENABLE TRIGGER [rDateModified_tBillingInvoice_SMSPayableRemider]
GO
