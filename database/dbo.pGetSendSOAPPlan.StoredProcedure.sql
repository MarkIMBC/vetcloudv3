﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetSendSOAPPlan](@Date      DATETIME,
                                    @IsSMSSent BIT = NULL)
AS
  BEGIN
      exec [pGetSendSOAPPlanByDateCovered]
        @Date,
        @Date,
        @IsSMSSent
  END

GO
