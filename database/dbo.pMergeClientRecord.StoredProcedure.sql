﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pMergeClientRecord](@From_ID_RecordValue INT,
                              @To_ID_RecordValue   INT,
                              @Comment             VARCHAR(MAX))
as
  BEGIN
      Declare @From_IDs_RecordValue typIntList
      DECLARE @_Comment VARCHAR(MAX) = ''

      INSERT @From_IDs_RecordValue
      VALUES (@From_ID_RecordValue)

      --    
      --    
      SET @_Comment = @Comment + ' - Medical Record'

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        'ID',
        'ID_Client',
        'tPatient_SOAP',
        @_Comment

      --    
      --    
      SET @_Comment = @Comment + ' - Wellness Record'

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        'ID',
        'ID_Client',
        'tPatient_Wellness',
        @_Comment

      --    
      --    
      SET @_Comment = @Comment
                      + ' - Veterinary Health Certificate'

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        'ID',
        'ID_Client',
        'tVeterinaryHealthCertificate',
        @_Comment

      --    
      --    
      SET @_Comment = @Comment + ' - Billing Invoice'

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        'ID',
        'ID_Client',
        'tBillingInvoice',
        @_Comment

      --    
      --    
      SET @_Comment = @Comment + ' - Patients'

      exec pAddRecordValueTransferLog
        @From_IDs_RecordValue,
        @To_ID_RecordValue,
        'ID',
        'ID_Client',
        'tPatient',
        @_Comment
  END

GO
