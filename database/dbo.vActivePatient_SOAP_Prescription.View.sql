﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_SOAP_Prescription] AS   
				SELECT   
				 H.*  
				FROM vPatient_SOAP_Prescription H  
				WHERE IsActive = 1
GO
