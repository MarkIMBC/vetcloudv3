﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserSession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[ID_User] [int] NULL,
	[ID_Warehouse] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_tUserSession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUserSession] ADD  CONSTRAINT [DF__tUserSess__IsAct__16644E42]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUserSession]  WITH NOCHECK ADD  CONSTRAINT [FKtUserSession_ID_User] FOREIGN KEY([ID_User])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUserSession] CHECK CONSTRAINT [FKtUserSession_ID_User]
GO
ALTER TABLE [dbo].[tUserSession]  WITH NOCHECK ADD  CONSTRAINT [FKtUserSession_ID_Warehouse] FOREIGN KEY([ID_Warehouse])
REFERENCES [dbo].[tWarehouse] ([ID])
GO
ALTER TABLE [dbo].[tUserSession] CHECK CONSTRAINT [FKtUserSession_ID_Warehouse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUserSession]
		ON [dbo].[tUserSession]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUserSession
			SET    DateCreated = GETDATE()
			FROM   dbo.tUserSession hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUserSession] ENABLE TRIGGER [rDateCreated_tUserSession]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUserSession]
		ON [dbo].[tUserSession]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tUserSession] ENABLE TRIGGER [rDateModified_tUserSession]
GO
