﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSMSList_PatientWaitingList_ReSchedule]
AS
  SELECT DISTINCT wellness.ID                                                                                                                                                                                                                              ID,
                  wellness.ID                                                                                                                                                                                                                              ID_PatientWaitingList,
                  wellness.Code,
                  DateSent,
                  wellness.Name_Client,
                  wellness.Name_Patient,
                  c.ContactNumber,
                  dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, comp.Name, comp.ContactNumber, wellness.DateReschedule, c.Name, wellness.Name_Patient, _appointment.ReferenceCode, _appointment.Description) Message,
                  wellness.DateCreated                                                                                                                                                                                                                     DateSending,
                  wellness.ID_Company,
                  IsNull(wellness.IsSentSMS, 0)                                                                                                                                                                                                            IsSentSMS,
                  model.Oid                                                                                                                                                                                                                                Oid_Model
  FROM   vPatientWaitingList wellness
         INNER JOIN tClient c
                 ON c.ID = wellness.ID_Client
         INNER JOIN tCOmpany comp
                 ON comp.ID = wellness.ID_Company
         INNER JOIN tCompany_SMSSetting smsSetting
                 on smsSetting.ID_Company = comp.ID
         LEFT join vAppointmentEvent _appointment
                on wellness.ID_Reference = _appointment.Appointment_ID_CurrentObject
                   and wellness.Oid_Model_Reference = _appointment.Oid_Model,
         _tModel model
  WHERE  model.tableName = 'tPatientWaitingList'
         AND wellness.WaitingStatus_ID_FilingStatus IN ( 21 )

GO
