﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROCEDURE [dbo].[pGetPayablePayment] @ID              INT = -1,
                                           @ID_Payable      INT = -1,
                                           @ID_FilingStatus INT = NULL,
                                           @ID_Session      INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF( @ID_FilingStatus IS NULL )
        SET @ID_FilingStatus = 1

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   payable.RemaningAmount,
                   payableDet.Name Name_Payable_Detail
            FROM   (SELECT NULL             AS [_],
                           -1               AS [ID],
                           NULL             AS [Code],
                           NULL             AS [Name],
                           GetDate()        AS Date,
                           1                AS [IsActive],
                           NULL             AS [ID_Company],
                           NULL             AS [Comment],
                           NULL             AS [DateCreated],
                           NULL             AS [DateModified],
                           NULL             AS [ID_CreatedBy],
                           NULL             AS [ID_LastModifiedBy],
                           @ID_FilingStatus AS ID_FilingStatus,
                           @ID_Payable      ID_Payable) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tPayable payable
                          ON payable.ID = H.ID_Payable
                   LEFT JOIN tPayable_Detail payableDet
                          ON payable.ID = payableDet.ID_Payable
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPayablePayment H
            WHERE  H.ID = @ID
        END
  END

GO
