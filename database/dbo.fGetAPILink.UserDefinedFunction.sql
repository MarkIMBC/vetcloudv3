﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   FUNCTION [dbo].[fGetAPILink]()  
RETURNS VARCHAR(MAX)  
AS  
  BEGIN  
      DECLARE @result VARCHAR(MAX) = '';  
  
      SET @result = 'https://mainapi.veterinarycloudsystem.com'  
  
      return @result  
  END  
  

GO
