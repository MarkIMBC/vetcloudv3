﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Wellness_Schedule] AS   
				SELECT   
				 H.*  
				FROM vPatient_Wellness_Schedule H  
				WHERE IsActive = 1
GO
