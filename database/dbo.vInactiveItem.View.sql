﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItem] AS   
				SELECT   
				 H.*  
				FROM vItem H  
				WHERE ISNULL(IsActive, 0) = 0
GO
