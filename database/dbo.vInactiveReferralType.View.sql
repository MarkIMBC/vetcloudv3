﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveReferralType] AS   
    SELECT   
     H.*  
    FROM vReferralType H  
    WHERE ISNULL(IsActive, 0) = 1
GO
