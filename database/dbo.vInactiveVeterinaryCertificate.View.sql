﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveVeterinaryCertificate] AS   
				SELECT   
				 H.*  
				FROM vVeterinaryCertificate H  
				WHERE ISNULL(IsActive, 0) = 0
GO
