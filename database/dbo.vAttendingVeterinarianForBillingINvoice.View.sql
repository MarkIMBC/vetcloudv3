﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      
 VIEW [dbo].[vAttendingVeterinarianForBillingINvoice]  
AS  
  SELECT hed.ID,  
         hed.Name,  
         hed.ID_Company,  
         hed.Name_Position  
  FROM   vEmployee hed  
  WHERE  hed.ID_Position IN ( 10, 2, 4, 5,  
                              6, 13, 14, 16,  
                              31, 15 )  
         AND ISNULL(hed.IsActive, 0) = 1  
         AND LOWER(hed.Name) NOT LIKE '%system%'  
         AND LOWER(hed.Name) NOT LIKE '%Sole, %'  
GO
