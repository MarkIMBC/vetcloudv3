﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tItemCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ItemType] [int] NULL,
 CONSTRAINT [PK_tItemCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tItemCategory] ON [dbo].[tItemCategory]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tItemCategory] ADD  CONSTRAINT [DF__tItemCate__IsAct__4336F4B9]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_tItemCategory_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tItemCategory] CHECK CONSTRAINT [FK_tItemCategory_ID_Company]
GO
ALTER TABLE [dbo].[tItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_tItemCategory_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tItemCategory] CHECK CONSTRAINT [FK_tItemCategory_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_tItemCategory_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tItemCategory] CHECK CONSTRAINT [FK_tItemCategory_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tItemCategory]
		ON [dbo].[tItemCategory]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tItemCategory
			SET    DateCreated = GETDATE()
			FROM   dbo.tItemCategory hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tItemCategory] ENABLE TRIGGER [rDateCreated_tItemCategory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tItemCategory]
		ON [dbo].[tItemCategory]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tItemCategory
			SET    DateModified = GETDATE()
			FROM   dbo.tItemCategory hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tItemCategory] ENABLE TRIGGER [rDateModified_tItemCategory]
GO
