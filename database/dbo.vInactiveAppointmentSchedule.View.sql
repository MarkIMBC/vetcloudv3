﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppointmentSchedule] AS   
				SELECT   
				 H.*  
				FROM vAppointmentSchedule H  
				WHERE ISNULL(IsActive, 0) = 0
GO
