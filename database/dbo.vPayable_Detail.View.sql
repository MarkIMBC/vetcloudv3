﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPayable_Detail]
AS
  SELECT H.*,
         UC.Name         AS CreatedBy,
         UM.Name         AS LastModifiedBy,
         expenseCat.Name Name_ExpenseCategory
  FROM   tPayable_Detail H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIn tExpenseCategory expenseCat
                on h.ID_ExpenseCategory = expenseCat.ID

GO
