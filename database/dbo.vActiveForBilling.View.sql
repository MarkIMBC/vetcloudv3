﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveForBilling] AS   
				SELECT   
				 H.*  
				FROM vForBilling H  
				WHERE IsActive = 1
GO
