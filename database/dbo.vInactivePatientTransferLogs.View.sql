﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientTransferLogs] AS   
				SELECT   
				 H.*  
				FROM vPatientTransferLogs H  
				WHERE ISNULL(IsActive, 0) = 0
GO
