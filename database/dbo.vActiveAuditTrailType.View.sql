﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAuditTrailType] AS   
				SELECT   
				 H.*  
				FROM vAuditTrailType H  
				WHERE IsActive = 1
GO
