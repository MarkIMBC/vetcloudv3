﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetSendSoapPlan](@Date             DATETIME,
                                        @IsSMSSent        BIT = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETIME,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETIME,
  DateCreated          DATETIME,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX),
  Count                INT)
AS
  BEGIN
      DECLARE @IDs_Company TYPINTLIST
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientAppointment_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_ID_Model VARCHAR(MAX) =''
      DECLARE @_table TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )

      SELECT @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      SELECT @Patient_Vaccination_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      SELECT @Patient_SOAP_Plan_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      SELECT @Patient_Wellness_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      SELECT @PatientAppointment_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatientAppointment'

      IF( len(Trim(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')

            DELETE FROM @IDs_Company
            WHERE  ID IN (SELECT ID_Company
                          FROM   tCompany_SMSSetting
                          WHERE  IsActive = 0)
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting cSMSSetting
                   INNER JOIN tCompany c
                           on cSMSSetting.ID_Company = c.ID
            WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
                   and c.IsActive = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent TABLE
        (
           IsSMSSent BIT
        )

      IF @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = IsNull(@Date, GetDate())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*Patient Appointment*/
      INSERT @_table
      SELECT c.ID                                                                                                                                                                                      ID_Company,
             c.NAME                                                                                                                                                                                    Name_Company,
             client.NAME                                                                                                                                                                               Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                        ContactNumber_Client,
             vac.DateStart                                                                                                                                                                             DateReturn,
             vac.Comment,
             IsNull(vac.Comment, '')                                                                                                                                                                   Comment,
             dbo.fGetSoaplAnMessage(c.NAME, ISNULL(c.SOAPPlanSMSMessage, ''), client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, IsNull(vac.Comment, ''), IsNull(vac.Comment, ''), vac.DateStart) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vac.DateStart))                                                                                                                                            DateSending,
             vac.DateCreated,
             vac.ID                                                                                                                                                                                    ID_Patient_Vaccination_Schedule,
             @PatientAppointment_ID_Model,
             vac.Code
      FROM   dbo.tPatientAppointment vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vac.IsSentSMS, 0) IN (SELECT IsSMSSent
                                              FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vac.DateStart)) = CONVERT(DATE, @Date) ))

      /*SOAP PLAN */
      INSERT @_table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             IsNull(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, soapPlan.Name_Item, IsNull(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DateAdd(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = ISNULL(patientSOAP.ID_Client, patient.ID_Client)
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     ON patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND IsNull(patientSOAP.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Patient Wellness Schedule*/
      INSERT @_table
      SELECT DISTINCT c.ID                                                                                                                                                                                                                        ID_Company,
                      c.NAME                                                                                                                                                                                                                      Name_Company,
                      client.NAME                                                                                                                                                                                                                 Name_Client,
                      dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                          ContactNumber_Client,
                      wellDetSched.Date,
                      IsNull(wellDetSched.Comment, ''),
                      IsNull(wellDetSched.Comment, ''),
                      dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, IsNull(client.NAME, ''), IsNull(c.ContactNumber, ''), IsNull(patient.NAME, ''), IsNull(wellDetSched.Comment, ''), IsNull(wellDetSched.Comment, ''), wellDetSched.Date) Message,
                      CONVERT(DATE, DateAdd(DAY, -1, wellDetSched.Date))                                                                                                                                                                          DateSending,
                      wellHed.DateCreated,
                      wellDetSched.ID                                                                                                                                                                                                             ID_Patient_Wellness_Schedule,
                      @Patient_Wellness_Schedule_ID_Model,
                      wellHed.Code
      FROM   dbo.vPatient_Wellness_Schedule wellDetSched
             inner JOIN vPatient_Wellness wellHed
                     ON wellHed.ID = wellDetSched.ID_Patient_Wellness
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellHed.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellHed.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellHed.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON c.ID = idsCompany.ID
      WHERE  wellHed.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND IsNull(wellHed.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, wellDetSched.Date)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Vaccination*/
      DECLARE @IDs_Vaccination_ExistingPatientWellness TYPINTLIST

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @_table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             IsNull(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Name_Item, IsNull(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN tPatient_Vaccination_Schedule vacSchedule
                     ON vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
             AND vac.ID NOT IN (SELECT ID
                                FROM   @IDs_Vaccination_ExistingPatientWellness)

      /*Reschedule WaitingList*/
      INSERT @_table
             (ID_Company,
              Name_Company,
              Name_Client,
              ContactNumber_Client,
              DateReturn,
              Name_Item,
              Comment,
              Message,
              DateSending,
              DateCreated,
              ID_Reference,
              Oid_Model,
              Code)
      SELECT hed.ID_Company,
             company.Name,
             client.Name,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),
             hed.DateCreated,
             '',
             '',
             dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, company.Name, company.ContactNumber, hed.DateReschedule, client.Name, patient.Name, _appointment.ReferenceCode, _appointment.Description),
             hed.DateCreated,
             hed.DateCreated,
             hed.ID,
             @PatientWaitingList_ID_Model,
             hed.Code
      FROM   tPatientWaitingList hed
             INNER JOIN tCompany_SMSSetting smsSetting
                     on smsSetting.ID_Company = hed.ID_Company
             INNER JOIN tCompany company
                     on company.ID = hed.ID_Company
             inner join tClient client
                     on hed.ID_Client = client.ID
             inner join tPatient patient
                     on hed.ID_Patient = patient.ID
             LEFT join vAppointmentEvent _appointment
                    on hed.ID_Reference = _appointment.Appointment_ID_CurrentObject
                       and hed.Oid_Model_Reference = _appointment.Oid_Model
             INNER JOIN @IDs_Company idsCompany
                     ON company.ID = idsCompany.ID
      WHERE  --CONVERT(Date, DateAdd(Day, smsSetting.DayInterval, hed.DateCreated)) = CONVERT(Date, @Date)            
        (( CONVERT(DATE, hed.DateCreated) = CONVERT(DATE, GETDATE()) ))
        AND hed.WaitingStatus_ID_FilingStatus IN ( 21 )
        AND IsNull(hed.IsSentSMS, 0) IN (SELECT IsSMSSent
                                         FROM   @SMSSent)
        AND IsNull(hed.ID_CLient, 0) > 0
        AND IsNull(patient.IsDeceased, 0) = 0

      --/*BillingInvoice SMSPayableRemider*/        
      --INSERT @_table        
      --       (ID_Company,        
      --        Name_Company,        
      --        Name_Client,        
      --        ContactNumber_Client,        
      --        DateReturn,        
      --        Name_Item,        
      --        Comment,        
      --        Message,        
      --        DateSending,        
      --        DateCreated,        
      --        ID_Reference,        
      --        Oid_Model,        
      --        Code)        
      --SELECT biHed.ID_Company,        
      --       company.Name,        
      --       client.Name,        
      --       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),        
      --       bSMSReminder.DateSchedule,        
      --       '',        
      --       '',        
      --       dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, company.ContactNumber, biHed.Code, biHed.Date, client.Name, ISNULL(bSMSReminder.RemainingAmount_BillingInvoice, 0)),        
      --       biHed.Date,        
      --       bSMSReminder.DateCreated,        
      --       bSMSReminder.ID,        
      --       @BillingInvoice_SMSPayableRemider_ID_Model,        
      --       bihed.Code        
      --FROM   vBillingInvoice biHed        
      --       INNER JOIN tBillingInvoice_SMSPayableRemider bSMSReminder        
      --               on biHed.ID = bSMSReminder.ID_BillingInvoice        
      --       INNER JOIN tCompany_SMSSetting smsSetting        
      --               on smsSetting.ID_Company = bSMSReminder.ID_Company        
      --       INNER JOIN tCompany company        
      --               on company.ID = bSMSReminder.ID_Company        
      --       inner join tClient client        
      --               on bSMSReminder.ID_Client = client.ID        
      --       INNER JOIN @IDs_Company idsCompany        
      --  ON company.ID = idsCompany.ID        
      --WHERE  CONVERT(Date, bSMSReminder.dateSchedule) = CONVERT(Date, GETDATE())        
      --       AND ISNULL(bSMSReminder.IsActive, 0) = 1        
      --       AND biHed.ID_FilingStatus = 3        
      --       AND Payment_ID_FilingStatus IN ( 2, 11 )        
      --       AND ISNULL(RemainingAmount, 0) > 0        
      --       AND ISNULL(smsSetting.IsEnableBillingInvoiceNotificationSending, 0) = 1        
      DELETE @_table
      FROM   @_table forSMSSending
             inner join tInactiveSMSSending inactiveSendingSchedule
                     on forSMSSending.ID_Company = inactiveSendingSchedule.ID_Company
                        and CONVERT(Date, forSMSSending.DateSending) = CONVERT(Date, inactiveSendingSchedule.Date)
      WHERE  inactiveSendingSchedule.IsActive = 1

      INSERT @table
      SELECT ID_Company,
             Name_Company,
             Name_Client,
             ContactNumber_Client,
             DateReturn,
             Name_Item,
             Comment,
             Message,
             MAX(DateSending),
             MAX(DateCreated),
             MAX(ID_Reference),
             Oid_Model,
             MAX(Code),
             Count(*)
      FROM   @_table
      GROUP  BY ID_Company,
                Name_Company,
                Name_Client,
                ContactNumber_Client,
                DateReturn,
                Name_Item,
                Comment,
                Message,
                Oid_Model

      Update @table
      set    Comment = REPLACE(Comment, '"', '``')

      Update @table
      set    Name_Item = REPLACE(Name_Item, '"', '``')

      RETURN
  END 
GO
