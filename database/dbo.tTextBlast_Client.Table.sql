﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTextBlast_Client](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Client] [int] NULL,
	[DateSent] [datetime] NULL,
	[IsSent] [decimal](18, 4) NULL,
	[ID_TextBlast] [int] NULL,
 CONSTRAINT [PK_tTextBlast_Client] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tTextBlast_Client] ON [dbo].[tTextBlast_Client]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tTextBlast_Client] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tTextBlast_Client]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_Client_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast_Client] CHECK CONSTRAINT [FK_tTextBlast_Client_ID_Company]
GO
ALTER TABLE [dbo].[tTextBlast_Client]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_Client_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast_Client] CHECK CONSTRAINT [FK_tTextBlast_Client_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tTextBlast_Client]  WITH CHECK ADD  CONSTRAINT [FK_tTextBlast_Client_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast_Client] CHECK CONSTRAINT [FK_tTextBlast_Client_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tTextBlast_Client]  WITH CHECK ADD  CONSTRAINT [FKtTextBlast_Client_ID_TextBlast] FOREIGN KEY([ID_TextBlast])
REFERENCES [dbo].[tTextBlast] ([ID])
GO
ALTER TABLE [dbo].[tTextBlast_Client] CHECK CONSTRAINT [FKtTextBlast_Client_ID_TextBlast]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tTextBlast_Client]
		ON [dbo].[tTextBlast_Client]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTextBlast_Client
			SET    DateCreated = GETDATE()
			FROM   dbo.tTextBlast_Client hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTextBlast_Client] ENABLE TRIGGER [rDateCreated_tTextBlast_Client]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tTextBlast_Client]
		ON [dbo].[tTextBlast_Client]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTextBlast_Client
			SET    DateModified = GETDATE()
			FROM   dbo.tTextBlast_Client hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTextBlast_Client] ENABLE TRIGGER [rDateModified_tTextBlast_Client]
GO
