﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClientAppointmentRequest] AS   
				SELECT   
				 H.*  
				FROM vClientAppointmentRequest H  
				WHERE IsActive = 1
GO
