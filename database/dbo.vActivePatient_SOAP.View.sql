﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_SOAP] AS   
				SELECT   
				 H.*  
				FROM vPatient_SOAP H  
				WHERE IsActive = 1
GO
