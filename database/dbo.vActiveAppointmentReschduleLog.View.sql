﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppointmentReschduleLog] AS   
    SELECT   
     H.*  
    FROM vAppointmentReschduleLog H  
    WHERE IsActive = 1
GO
