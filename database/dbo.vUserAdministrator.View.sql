﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vUserAdministrator]
as
  select _user.ID,
         _user.ID                                                    ID_User,
         _user.ID_Employee,
         CONVERT(BIT, MAX(CONVERT(INT, _userroles.IsAdministrator))) IsAdministrator
  FROM   tUser _user
         LEFT join tUser_Roles _user_roles
                on _user.ID = _user_roles.ID_User
         LEFT join tUserRole _userroles
                on _user_roles.ID_UserRole = _userroles.ID
  GROUP  BY _user.ID,
            _user.ID_Employee

GO
