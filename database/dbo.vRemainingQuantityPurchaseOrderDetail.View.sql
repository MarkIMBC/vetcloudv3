﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vRemainingQuantityPurchaseOrderDetail]
AS
  SELECT detail.ID,
         detail.ID_Item,
         detail.Name_Item,
         item.Code Code_Item,
         detail.Quantity,
         detail.RemainingQuantity,
         detail.UnitPrice,
         detail.ID_PurchaseOrder,
         hed.ID_FilingStatus,
         hed.Name_FilingStatus,
         hed.ServingStatus_Name_FilingStatus,
         hed.ID_Company,
         detail.UnitCost
  FROM   vPurchaseOrder_Detail detail
         INNER JOIN vPurchaseOrder hed
                 On hed.ID = detail.ID_PurchaseOrder
         LEFT JOIn tItem item
                on item.ID = detail.ID_Item
  WHERE  ISNULL(detail.RemainingQuantity, 0) > 0

GO
