﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetDatesByDateCoverage](@StartDateTime Date,
                                       @EndDateTime   Date)
RETURNS @table TABLE (
  [Date] Date )
AS
  BEGIN
      WITH DateRange(DateData)
           AS (SELECT @StartDateTime as Date
               UNION ALL
               SELECT DATEADD(d, 1, DateData)
               FROM   DateRange
               WHERE  DateData < @EndDateTime)
      INSERT @table
      SELECT DateData Date
      FROM   DateRange
      OPTION (MAXRECURSION 0)

      RETURN;
  END;

GO
