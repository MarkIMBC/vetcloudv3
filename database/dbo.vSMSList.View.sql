﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSMSList]
as
  SELECT ID,
         ID              ID_Reference,
         ID_Patient_SOAP Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_SOAP_Plan
  Union All
  SELECT ID,
         ID                     ID_Reference,
         ID_Patient_Vaccination Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Vaccination_Schedule
  UNION ALL
  SELECT ID,
         ID                  ID_Reference,
         ID_Patient_Wellness Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Wellness_DetailSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientWaitingList Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientWaitingList_ReSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientAppointment Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientAppointment

--UNION ALL    
--SELECT ID,    
--       ID                ID_Reference,    
--       ID_BillingInvoice Parent_ID_Reference,    
--       Code,    
--       DateSent,    
--       Name_Client,    
--       Name_Patient,    
--       ContactNumber,    
--       Message,    
--       DateSending,    
--       ID_Company,    
--       IsSentSMS,    
--       Oid_Model    
--FROM   vSMSList_BillingInvoice_SMSPayableRemider    
GO
