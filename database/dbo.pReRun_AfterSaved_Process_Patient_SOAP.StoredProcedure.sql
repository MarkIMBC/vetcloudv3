﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[pReRun_AfterSaved_Process_Patient_SOAP]
AS
  BEGIN
      DECLARE @IDs_CurrentObject typIntList

      INSERT @IDs_CurrentObject
      SELECT TOP 5 _soap.ID
      FROm   tPatient_SOAP _soap
             INNER JOIN vCompanyActive c
                     on _soap.ID_Company = c.ID
                        and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
                        AND CONVERT(Date, _soap.DateModified) = CONVERT(Date, GETDATE())
      Order  by _soap.DateModified DESC

      exec pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs
        @IDs_CurrentObject
  END

GO
