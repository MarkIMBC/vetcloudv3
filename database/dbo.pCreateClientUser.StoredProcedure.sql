﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCreateClientUser] (@ID_Client INT,
                                      @Username  VARCHAR(100),
                                      @Password  VARCHAR(100))
AS
  BEGIN
      DECLARE @ClientName VARCHAR(100)
      DECLARE @IsActive INT=1;
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @ID_UserGroup INT=9;
      DECLARE @ID_LastModifiedBy INT=1;
      DECLARE @ID_CreatedBy INT=1;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SET @ClientName=(SELECT Name
                       FROM   tClient
                       WHERE  ID = @ID_Client)

      BEGIN TRY
          INSERT INTO [dbo].[tUser]
                      ([Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [Username],
                       [ID_UserGroup],
                       [Password])
          VALUES      (@ClientName,
                       @IsActive,
                       @DateCreated,
                       @DateModified,
                       @ID_CreatedBy,
                       @ID_LastModifiedBy,
                       @Username,
                       @ID_UserGroup,
                       @Password)

          Update tClient
          SET    ID_User = @@IDENTITY
          WHERE  ID = @ID_Client
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;
  END;

GO
