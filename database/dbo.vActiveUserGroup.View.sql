﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUserGroup] AS   
				SELECT   
				 H.*  
				FROM vUserGroup H  
				WHERE IsActive = 1
GO
