﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelMulticatePatient_SOAP_Record](@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @reftable TABLE
        (
           Date         DateTime,
           ID_Company   INT,
           Name_Client  VARCHAR(MAX),
           Name_Patient VARCHAR(MAX),
           GUID         VARCHAR(MAX)
        )
      DECLARE @table TABLE
        (
           Date                DateTime,
           ID_Company          INT,
           Name_Client         VARCHAR(MAX),
           Name_Patient        VARCHAR(MAX),
           GUID                VARCHAR(MAX),
           Count               Int,
           DateModifiedMin     Datetime,
           DateModifiedMax     DateTime,
           IntervalSecond      INt,
           ID_Patient_SOAP_Min INT,
           ID_Patient_SOAP_MAX INT
        )

      INSERT @reftable
      SELECT soap_.Date,
             soap_.ID_Company,
             soap_.Name_Client,
             soap_.Name_Patient,
             soap_.GUID
      FROM   vPatient_SOAP soap_
             INNER JOIN @IDs_Patient_SOAP ids
                     on soap_.ID = ids.ID

      INSERT @table
      SELECT soap_.Date,
             soap_.ID_Company,
             soap_.Name_Client,
             soap_.Name_Patient,
             soap_.GUID,
             COUNT(*),
             MIN(soap_.DateModified) DateModifiedMin,
             MAX(soap_.DateModified) DateModifiedMax,
             DATEDIFF(SECOND, CONVERT(datetime, MIN(soap_.DateModified)), MAX(soap_.DateModified)),
             Min(soap_.ID),
             MAX(soap_.ID)
      FROM   vPatient_SOAP soap_
             INNER JOIN @reftable soap_1
                     ON soap_1.Date = soap_.Date
                        AND ISNULL(soap_1.ID_Company, '') = ISNULL(soap_.ID_Company, '')
                        AND ISNULL(soap_1.Name_Client, '') = ISNULL(soap_.Name_Client, '')
                        AND ISNULL(soap_1.Name_Patient, '') = ISNULL(soap_.Name_Patient, '')
                        AND ISNULL(soap_1.GUID, '') = ISNULL(soap_.GUID, '')
             inner join vCompany c
                     on soap_.ID_Company = c.ID
      WHERE  soap_.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY soap_.Date,
                soap_.ID_Company,
                soap_.Name_Client,
                soap_.Name_Patient,
                soap_.GUID
      HAVING Count(*) > 1
      Order  by soap_.Date DESC,
                DATEDIFF(SECOND, CONVERT(datetime, MIN(soap_.DateModified)), MAX(soap_.DateModified)) DESC

      DEClare @IDs_Patient_SOAP_forCancel typIntlist

      INSERT @IDs_Patient_SOAP_forCancel
      SELECT soap_.ID
      FROM   tPatient_SOAP soap_
             inner join @table tbl
                     on soap_.ID = tbl.ID_Patient_SOAP_Min
      Order  by soap_.Date DESC,
                soap_.DateModified DESC

      Update tPatient_SOAP
      SET    ID_FilingStatus = NULL,
             Comment = CASE
                         WHEn LEN(RTRIM(LTRIM(ISNULL(Comment, '')))) > 0 then CHAR(13) + CHAR(13)
                         ELSE ''
                       END
                       + 'Cancel via system '
                       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt')
      where  ID IN (SELECT ID
                    FROM   @IDs_Patient_SOAP_forCancel)
  END

GO
