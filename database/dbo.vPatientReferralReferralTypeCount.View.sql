﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatientReferralReferralTypeCount]
as
  SELECT ID_PatientReferral,
         ISNULL([1], 0) AS ReferralType_1,
         ISNULL([2], 0) AS ReferralType_2,
         ISNULL([3], 0) AS ReferralType_3,
         ISNULL([4], 0) AS ReferralType_4,
         ISNULL([5], 0) AS ReferralType_5,
         ISNULL([6], 0) AS ReferralType_6
  FROM   (SELECT ID_PatientReferral,
                 ID_ReferralType
          FROM   tPatientReferral_ReferralType) AS SourceTable
         PIVOT ( COUNT(ID_ReferralType)
               FOR ID_ReferralType IN ([1],
                                       [2],
                                       [3],
                                       [4],
                                       [5],
                                       [6]) ) AS PivotTable;

GO
