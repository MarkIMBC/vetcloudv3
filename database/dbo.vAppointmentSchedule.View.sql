﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vAppointmentSchedule]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       st.Name ScheduleType,
       e.Name AS Doctor,
       p.Name Patient,
       sched.Name_ServiceType,
       appointStatus.Name AppointmentStatus_Name_FilingStatus
FROM dbo.tAppointmentSchedule H
    LEFT JOIN dbo.tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tEmployee e
        ON e.ID = H.ID_Doctor
    LEFT JOIN dbo.tPatient p
        ON p.ID = H.ID_Patient
    LEFT JOIN dbo.tScheduleType st
        ON st.ID = H.ID_ScheduleType
    LEFT JOIN dbo.tServiceType serviceType
        ON serviceType.ID = H.ID_ServiceType
    LEFT JOIN dbo.tFilingStatus appointStatus
        ON appointStatus.ID = H.AppointmentStatus_ID_FilingStatus
    LEFT JOIN dbo.vSchedule sched
        ON sched.ID = H.ID_Schedule;
GO
