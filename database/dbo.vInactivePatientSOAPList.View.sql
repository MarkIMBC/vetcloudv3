﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientSOAPList] AS   
				SELECT   
				 H.*  
				FROM vPatientSOAPList H  
				WHERE ISNULL(IsActive, 0) = 0
GO
