﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppointment] AS   
				SELECT   
				 H.*  
				FROM vAppointment H  
				WHERE IsActive = 1
GO
