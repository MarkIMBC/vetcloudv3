﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveInactiveSMSSending] AS   
				SELECT   
				 H.*  
				FROM vInactiveSMSSending H  
				WHERE ISNULL(IsActive, 0) = 0
GO
