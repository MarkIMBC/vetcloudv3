﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelPatientReferral] (@IDs_PatientReferral typIntList READONLY,
                                           @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatientReferral
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatientReferral bi
                 INNER JOIN @IDs_PatientReferral ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
