﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vReferenceLink] AS   
    SELECT   
     H.* ,  
     UC.Name AS CreatedBy,  
     UM.Name AS LastModifiedBy  
    FROM tReferenceLink H  
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID   
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
GO
