﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vSMSList_Patient_Wellness_DetailSchedule]
AS
  SELECT DISTINCT wellSched.ID                                                                                                                                                                                   ID,
                  wellSched.ID_Patient_Wellness,
                  wellness.Code,
                  DateSent,
                  Name_Client,
                  Name_Patient,
                  c.ContactNumber,
                  dbo.fGetSoaplAnMessage(comp.NAME, comp.SOAPPlanSMSMessage, c.NAME, IsNull(comp.ContactNumber, ''), Name_Patient, IsNull(wellSched.Comment, ''), IsNull(wellSched.Comment, ''), wellSched.Date) Message,
                  DateAdd(DAY, -1, wellSched.Date)                                                                                                                                                               DateSending,
                  wellness.ID_Company,
                  IsNull(wellSched.IsSentSMS, 0)                                                                                                                                                                 IsSentSMS,
                  model.Oid                                                                                                                                                                                      Oid_Model
  FROM   vPatient_Wellness_Schedule wellSched
         INNER JOIN vPatient_Wellness wellness
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient c
                 ON c.ID = wellness.ID_Client
         INNER JOIN tCOmpany comp
                 ON comp.ID = wellness.ID_Company,
         _tModel model
  WHERE  model.tableName = 'tPatient_Wellness_Schedule'
         AND wellness.ID_FilingStatus NOT IN ( 4 )

GO
