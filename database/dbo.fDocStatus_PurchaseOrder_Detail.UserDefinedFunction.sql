﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fDocStatus_PurchaseOrder_Detail] ( @ID_PuchaseOrder INT )
RETURNS @tblDetail TABLE
    (
      ID_DocDetail INT ,
      RRQty DECIMAL(18, 2) ,
      Balance DECIMAL(18, 2) ,
      ID_DocStatus INT
    )
AS
    BEGIN
        INSERT  @tblDetail
                ( ID_DocDetail ,
                  RRQty ,
                  Balance ,
                  ID_DocStatus
                )
                SELECT  H.ID_DocDetail ,
                        H.RDQty ,
                        H.PDBalance ,
                        ( CASE WHEN H.POQty = H.PDBalance
                                    AND H.RDQty = 0 THEN 1
                               WHEN H.PDBalance > 0
                                    AND H.RDQty > 0 THEN 2
                               WHEN H.PDBalance = 0 THEN 3
                          END ) AS ID_DocStatus
                FROM    ( SELECT    PD.ID AS ID_DocDetail ,
                                    PD.Quantity AS POQty ,
                                    ISNULL(RD.Quantity, 0.00) AS RDQty ,
                                    PD.Quantity - ISNULL(RD.Quantity, 0.00) AS PDBalance
                          FROM      dbo.tPurchaseOrder_Detail PD
                                    LEFT JOIN dbo.tReceivingReport_Detail RD ON PD.ID_PurchaseOrder = RD.ID_Doc
                                                              AND PD.ID = RD.ID_DocDetail
                                    LEFT JOIN dbo.tPurchaseOrder PO ON PD.ID_PurchaseOrder = PO.ID
                                    LEFT JOIN dbo.tReceivingReport RR ON RD.ID_ReceivingReport = RR.ID
                                                              AND RR.ID_FilingStatus = 2
                          WHERE     PD.ID_PurchaseOrder = @ID_PuchaseOrder
                                    AND PO.ID_FilingStatus = 2
                        ) H
        RETURN
    END
GO
