﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vAuditTrail_Detail]
as
  SELECT _audDet.*,
         _type.Name Name_AuditTrailType
  FROM   tAuditTrail_Detail _audDet
         LEFT JOIN tAuditTrailType _type
                on _audDet.ID_AuditTrailType = _type.ID

GO
