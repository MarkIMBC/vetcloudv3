﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardSmallBoxMonthlySales] (@ID_UserSession INT,
                                                  @Month          INT,
                                                  @Year           INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT
      DECLARE @TotalAmount DECIMAL(18, 2) = 0
      DECLARE @DateStart DATE;
      DECLARE @DateEnd DATE;

      SET @DateStart = DATEFROMPARTS(@Year, @Month, 1);
      SET @DateEnd = EOMONTH(@DateStart);

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession WITH (NOLOCK)
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser WITH (NOLOCK)
      WHERE  ID = @ID_User

      SELECT @TotalAmount = SUM(IsNull(TotalAmount, 0))
      FROM   tBillingInvoice WITH (NOLOCK)
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) BETWEEN @DateStart AND @DateEnd

      SET @TotalAmount = ISNULL(@TotalAmount, 0)

      SELECT @TotalAmount = @TotalAmount + IsNull(DepositAmount, 0)
      FROM   vClientDeposit WITH (NOLOCK)
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus IN ( 3, 17 )
             AND Cast(Date AS DATE) BETWEEN @DateStart AND @DateEnd

      SET @TotalAmount = ISNULL(@TotalAmount, 0)

      DECLARE @subtitle VARCHAR(MAX) = '';

      IF( MONTH(GETDATE()) <> @Month
           OR YEAR(GETDATE()) <> @Year )
        set @subtitle = ' as of ' + FORMAT(@DateStart, 'MMMM yyyy')

      SELECT '_'

      SELECT @TotalAmount                    TotalAmount,
             FORMAT(@TotalAmount, '#,#0.00') FormattedTotalAmount,
             'Monthly Sales' + @subtitle     Subtitle
  END

GO
