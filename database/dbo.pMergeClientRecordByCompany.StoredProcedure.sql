﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pMergeClientRecordByCompany](@GUID_Company            VARCHAR(MAX),
                                       @Source_Code_Client      VARCHAR(MAX),
                                       @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergeClientRecordByCompanyID
        @ID_Company,
        @Source_Code_Client,
        @Destination_Code_Client
  END

GO
