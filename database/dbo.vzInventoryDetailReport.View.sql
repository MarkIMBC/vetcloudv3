﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzInventoryDetailReport]
AS
  SELECT hed.*,
         item.Name                     Name_Item,
         status.Name                   Name_FilingStatus,
         CONVERT(VARCHAR, hed.Date, 9) DateString,
         CONVERT(Date, hed.Date)       DateTrail,
         company.ImageLogoLocationFilenamePath,
         company.Name                  Name_Company,
         company.Address               Address_Company,
         invtStatus.Name               Name_InventoryStatus,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                         HeaderInfo_Company
  FROM   dbo.tInventoryTrail hed
         LEFT JOIN dbo.tItem item
                ON item.ID = hed.ID_Item
         LEFT JOIN dbo.tFilingStatus status
                ON status.ID = hed.ID_FilingStatus
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
         LEFT JOIN dbo.tInventoryStatus invtStatus
                ON invtStatus.ID = item.ID_InventoryStatus
  WHERE  ISNULL(item.IsActive, 0) = 1 
GO
