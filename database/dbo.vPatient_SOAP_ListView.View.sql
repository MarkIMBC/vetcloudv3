﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_SOAP_ListView]
AS
  SELECT H.ID,
         H.ID_Company,
         H.ID_SOAPType,
         H.ID_FilingStatus,
         H.Date,
         H.Code,
         soapType.Name                                      Name_SOAPType,
         attendingPhysicianEmloyee.Name                     AttendingPhysician_Name_Employee,
         H.ID_Client,
         client.Name                                        Name_Client,
         patient.Name                                       Name_Patient,
         fs.Name                                            Name_FilingStatus,
         H.ID_Patient,
         H.ID_Patient_Confinement,
         CaseType,
         IsNull(History, '')                                History,
         IsNull(ClinicalExamination, '')                    ClinicalExamination,
         IsNull(Interpretation, '')                         Interpretation,
         IsNull(Diagnosis, '')                              Diagnosis,
         IsNull(Treatment, '')                              Treatment,
         IsNull(ClientCommunication, '')                    ClientCommunication,
         IsNull(Prescription, '')                           Prescription,
         len(Trim(IsNull(History, '')))                     Count_History,
         len(Trim(IsNull(ClinicalExamination, '')))         Count_ClinicalExamination,
         0 + len(Trim(IsNull(Interpretation, '')))          Count_LaboratoryImages,
         len(Trim(IsNull(Diagnosis, '')))                   Count_Diagnosis,
         len(Trim(IsNull(Treatment, '')))                   Count_Treatment,
         len(Trim(IsNull(ClientCommunication, '')))         Count_ClientCommunication,
         0                                                  Count_Plan,
         0 + len(Trim(IsNull(H.Prescription, '')))          Count_Prescription,
         H.DateCreated,
         H.DateModified,
         'Date Created: '
         + Format(H.DateCreated, 'MMM. dd, yyyy hh:mm tt')
         + Char(13) + 'Date Modified: '
         + Format(H.DateModified, 'MMM. dd, yyyy hh:mm tt') TooltipText,
         H.Comment
  FROM   dbo.tPatient_SOAP H WITH (NOLOCK)
         LEFT JOIN dbo.tPatient patient WITH (NOLOCK)
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client WITH (NOLOCK)
                ON patient.ID_Client = client.ID
         LEFT JOIN dbo.tSOAPType soapType WITH (NOLOCK)
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee WITH (NOLOCK)
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tFilingStatus fs WITH (NOLOCK)
                ON fs.ID = H.ID_FilingStatus
         INNER JOIN dbo.tCompany company WITH (NOLOCK)
                 ON H.ID_Company = company.ID
  WHERE  ISNULL(H.ID_FilingStatus, 0) NOT IN ( 0 )
         and company.IsActive = 1

GO
