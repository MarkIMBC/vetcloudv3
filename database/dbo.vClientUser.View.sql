﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vClientUser]
AS
  SELECT h.*,
         C.Name       AS Client,
         C.ID         AS ID_Client,
         C.Name       AS Name_Client,
         UG.Name      AS UserGroup,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         C.ID_Company,
         company.Name Name_Company,
         company.Code Code_Company,
         company.Guid,
         company.ReceptionPortalGuid
  FROM   dbo.tUser h
         LEFT JOIN dbo.tClient C
                ON h.ID = C.ID_User
         LEFT JOIN dbo.tUserGroup UG
                ON UG.ID = h.ID_UserGroup
         LEFT JOIN tUser UC
                ON h.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON h.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tCompany company
                ON company.ID = C.ID_Company;

GO
