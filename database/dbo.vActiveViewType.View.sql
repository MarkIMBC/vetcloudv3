﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveViewType] AS   
				SELECT   
				 H.*  
				FROM vViewType H  
				WHERE IsActive = 1
GO
