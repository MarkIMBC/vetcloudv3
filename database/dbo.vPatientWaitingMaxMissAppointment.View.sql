﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vPatientWaitingMaxMissAppointment]
AS
  SELECT MAX(ID) ID_PatientWaitingList
  FROm   vPatientWaitingList
  where  WaitingStatus_ID_FilingStatus IN ( 4, 21 )
  GROUP  BY ID_Client,
            Name_Client,
            ID_Patient,
            Name_Patient

GO
