﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vModelViews]
AS
     SELECT sv.[name] AS [Name]
     FROM sys.views sv
          INNER JOIN dbo._tModel tm ON sv.name LIKE 'v' + tm.Name + '%'
                                       OR sv.name LIKE '_v' + tm.Name + '%';
GO
