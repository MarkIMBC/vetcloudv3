﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientAppointment] AS   
				SELECT   
				 H.*  
				FROM vPatientAppointment H  
				WHERE IsActive = 1
GO
