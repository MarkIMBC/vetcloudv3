﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePaymentMethod] AS   
				SELECT   
				 H.*  
				FROM vPaymentMethod H  
				WHERE IsActive = 1
GO
