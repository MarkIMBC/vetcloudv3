﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoClientLogin_validation](@Username VARCHAR(300),
                                             @Password VARCHAR(300))
AS
    DECLARE @message VARCHAR(400) = '';
    /*Validate if Username is not exist.*/
    DECLARE @Count_ValidateUserNameExist INT = 0;
    DECLARE @ID_Company INT

    SELECT @Count_ValidateUserNameExist = COUNT(*)
    FROM   tUser _user
           inner join tClient client
                   on _user.ID = client.ID_User
    Where  _user.username = @Username
           and _user.IsActive = 1

    IF( @Count_ValidateUserNameExist = 0 )
      BEGIN
          SET @message = 'Username is not exist.';

          THROW 50001, @message, 1;
      END

    /*Validate if Password does not match.*/
    DECLARE @Count_ValidateUserPasswordDoesNotMatch INT = 0;

    SELECT @Count_ValidateUserPasswordDoesNotMatch = COUNT(*)
    FROM   tUser _user
           inner join tClient client
                   on _user.ID = client.ID_User
    Where  _user.username = @Username
           and _user.Password = @Password
           and _user.IsActive = 1

    IF( @Count_ValidateUserPasswordDoesNotMatch = 0 )
      BEGIN
          SET @message = 'Username and Password does not match.';

          THROW 50001, @message, 1;
      END

    /*Validate if User is multiple accounts.*/
    DECLARE @Count_ValidateUserMultipleAccounts INT = 0;

    SELECT @Count_ValidateUserMultipleAccounts = COUNT(*)
    FROM   tUser
    Where  username = @Username
           and Password = @Password
           and IsActive = 1

    IF( @Count_ValidateUserMultipleAccounts > 1 )
      BEGIN
          SET @message = 'User cannot identify login account. Please contact Vet Cloud Support.';

          THROW 50001, @message, 1;
      END

    /*Validate if User is Inactive.*/
    DECLARE @Count_ValidateUserInactive INT = 0;

    SELECT @Count_ValidateUserInactive = COUNT(*)
    FROM   tUser
    Where  username = @Username
           and Password = @Password
           and IsActive = 0

    IF( @Count_ValidateUserInactive = 1 )
      BEGIN
          SET @message = 'Login account is inactive.';

          THROW 50001, @message, 1;
      END

    /*Validate if Company is Inactive.*/
    DECLARE @IsActive_Company BIT = 0;

    SELECT @ID_Company = ID_Company
    FROM   vClientUser
    Where  username = @Username
           and Password = @Password

    SELECT @IsActive_Company = ISNULL(IsActive, 0)
    FROM   tCompany
    WHERE  ID = @ID_Company

    IF( @IsActive_Company = 0 )
      BEGIN
          SET @message = 'Company Access is inactive. Please contact Vet Cloud Support.';

          THROW 50001, @message, 1;
      END

/*Validate if User is is not yet subscribe.*/
--   DECLARE @isSubscribed BIT = 0;        
--DECLARE @CurrentDate DateTIME = '2021-08-19 00:00:00:000'      
--SELECT @ID_Company = ID_Company      
--   FROM   vUser        
--   Where  username = @Username        
--          and Password = @Password        
--          and IsActive = 1        
--SELECT @isSubscribed = isSubscribed      
--FROM   dbo.fGetCompany_CurrentSubscription(@ID_Company)       
----   if(@isSubscribed = 0)      
----BEGIN      
----      SET @message = 'Login account is not yet subscribe.';        
----         THROW 50001, @message, 1;        
----END 
GO
