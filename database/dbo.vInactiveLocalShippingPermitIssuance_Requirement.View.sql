﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveLocalShippingPermitIssuance_Requirement] AS   
				SELECT   
				 H.*  
				FROM vLocalShippingPermitIssuance_Requirement H  
				WHERE ISNULL(IsActive, 0) = 0
GO
