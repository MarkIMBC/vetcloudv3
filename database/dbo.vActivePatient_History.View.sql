﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_History] AS   
				SELECT   
				 H.*  
				FROM vPatient_History H  
				WHERE IsActive = 1
GO
