﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_Wellness_Detail_Listview]
AS
  select welldetail.ID,
         wellHed.ID ID_Patient_Wellness,
         welldetail.Name_Item,
         wellHed.Date,
         welldetail.Comment,
         ID_Patient_SOAP,
         wellHed.ID_FilingStatus,
         wellHed.ID_Company,
         wellHed.ID_Client,
         wellHed.ID_Patient
  FROM   vPatient_Wellness wellHed
         LEFT JOIN vPatient_Wellness_Detail welldetail
                on wellHed.ID = welldetail.ID_Patient_Wellness
  WHERE  wellHed.ID_FilingStatus NOT IN ( 4 ) 
GO
