﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tReceivingReport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_PurchaseOrder] [int] NULL,
	[ID_Supplier] [int] NULL,
	[ID_TaxScheme] [int] NULL,
	[GrossAmount] [decimal](18, 4) NULL,
	[VatAmount] [decimal](18, 4) NULL,
	[NetAmount] [decimal](18, 4) NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[Date] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ServingStatus_ID_FilingStatus] [int] NULL,
	[DiscountRate] [decimal](18, 4) NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[IsComputeDiscountRate] [bit] NULL,
	[SubTotal] [decimal](18, 4) NULL,
	[TotalAmount] [decimal](18, 4) NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tReceivingReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tReceivingReport] ON [dbo].[tReceivingReport]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tReceivingReport] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tReceivingReport]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport] CHECK CONSTRAINT [FK_tReceivingReport_ID_Company]
GO
ALTER TABLE [dbo].[tReceivingReport]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport] CHECK CONSTRAINT [FK_tReceivingReport_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tReceivingReport]  WITH CHECK ADD  CONSTRAINT [FK_tReceivingReport_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tReceivingReport] CHECK CONSTRAINT [FK_tReceivingReport_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tReceivingReport]
		ON [dbo].[tReceivingReport]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tReceivingReport
			SET    DateCreated = GETDATE()
			FROM   dbo.tReceivingReport hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tReceivingReport] ENABLE TRIGGER [rDateCreated_tReceivingReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tReceivingReport]
		ON [dbo].[tReceivingReport]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tReceivingReport
			SET    DateModified = GETDATE()
			FROM   dbo.tReceivingReport hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tReceivingReport] ENABLE TRIGGER [rDateModified_tReceivingReport]
GO
