﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientTransferLogs] AS   
				SELECT   
				 H.*  
				FROM vPatientTransferLogs H  
				WHERE IsActive = 1
GO
