﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveToothSurface] AS   
				SELECT   
				 H.*  
				FROM vToothSurface H  
				WHERE IsActive = 1
GO
