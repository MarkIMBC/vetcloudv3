﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdateAllPatient_SOAP_BillingStatus]
AS
  BEGIN
      DECLARE @IDs_Patient_SOAP typINtList

      INSERT @IDs_Patient_SOAP
      SELECT ID_CurrentObject
      FROM   vForBilling_ListView hed
             inner join _tModel model
                     on model.Oid = hed.Oid_Model
      where  model.TableName = 'tPatient_SOAP'

      exec pUpdatePatient_SOAP_BillingStatus
        @IDs_Patient_SOAP
  END

GO
