﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUserRole_Reports] AS   
				SELECT   
				 H.*  
				FROM vUserRole_Reports H  
				WHERE IsActive = 1
GO
