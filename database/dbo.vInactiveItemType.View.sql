﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItemType] AS   
				SELECT   
				 H.*  
				FROM vItemType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
