﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSMSList_PatientAppointment]
AS
  SELECT patientAppnt.ID,
         patientAppnt.ID                                                                                                                                                                                              ID_PatientAppointment,
         patientAppnt.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), Name_Patient, ISNULL(patientAppnt.Comment, ''), ISNULL(patientAppnt.Comment, ''), patientAppnt.DateStart) Message,
         DATEADD(DAY, -1, patientAppnt.DateStart)                                                                                                                                                                     DateSending,
         patientAppnt.ID_Company,
         ISNULL(patientAppnt.IsSentSMS, 0)                                                                                                                                                                            IsSentSMS,
         model.Oid                                                                                                                                                                                                    Oid_Model
  FROM   vPatientAppointment patientAppnt
         INNER JOIN tClient c
                 on c.ID = patientAppnt.ID_Client
         INNER JOIN tCOmpany comp
                 on comp.ID = patientAppnt.ID_Company,
         _tModel model
  where  model.tableName = 'tPatientAppointment'
         and patientAppnt.ID_FilingStatus NOT IN ( 4 )

GO
