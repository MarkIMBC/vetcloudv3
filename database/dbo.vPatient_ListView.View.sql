﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_ListView]
AS
  SELECT H.ID,
         H.ID_Company,
         CASE
           WHEN len(Trim(IsNull(H.CustomCode, ''))) = 0 THEN H.Code
           ELSE H.CustomCode
         END                                                                         Code,
         H.Name,
         H.ID_Client,
         client.Name                                                                 Name_Client,
         H.Email,
         ISNULL(H.Species, '')                                                       Species,
         ISNULL(gender.Name, '')                                                     Name_Gender,
         client.ContactNumber,
         IsDeceased,
         DateDeceased,
         H.DateLastVisited,
         dbo.fGetAPILink() + '/Content/Image/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')                           ProfileImageThumbnailLocationFile,
         IsNull(H.IsActive, 0)                                                       IsActive,
         WaitingStatus_ID_FilingStatus,
         waitingStatus.Name                                                          WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, waitingStatus.Name) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                       Microchip,
         ISNULL(Color, '')                                                           Color,
         dbo.fGetAge(DateBirth, GETDATE(), '')                                       Age,
         Idiosyncrasies,
         attendingPersonell.Name                                                     LastAttendingPhysician_Name_Employee,
         H.Last_ID_Patient_SOAP
  FROM   tPatient H WITH (NOLOCK)
         LEFT JOIN tUser UC WITH (NOLOCK)
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM WITH (NOLOCK)
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tGender gender WITH (NOLOCK)
                ON H.ID_Gender = gender.ID
         LEFT JOIN dbo.tCountry country WITH (NOLOCK)
                ON H.ID_Country = country.ID
         LEFT JOIN dbo.tClient client WITH (NOLOCK)
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tFilingStatus waitingStatus WITH (NOLOCK)
                ON waitingStatus.ID = H.WaitingStatus_ID_FilingStatus
         LEFT JOIN tEmployee attendingPersonell WITH (NOLOCK)
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO
