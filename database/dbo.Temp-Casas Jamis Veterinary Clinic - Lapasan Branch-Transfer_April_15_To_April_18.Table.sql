﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp-Casas Jamis Veterinary Clinic - Lapasan Branch-Transfer_April_15_To_April_18](
	[ID_Patient_SOAP_Plan] [int] NOT NULL,
	[DateReturn] [datetime] NULL,
	[Code] [varchar](50) NULL,
	[Name_Client] [varchar](2000) NULL,
	[Name_Patient] [varchar](200) NULL,
	[Note] [varchar](max) NOT NULL,
	[ContactNumber_Client] [varchar](601) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
