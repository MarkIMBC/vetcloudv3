﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_SOAP_Treatment] AS   
				SELECT   
				 H.*  
				FROM vPatient_SOAP_Treatment H  
				WHERE IsActive = 1
GO
