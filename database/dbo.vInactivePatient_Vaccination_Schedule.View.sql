﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_Vaccination_Schedule] AS   
				SELECT   
				 H.*  
				FROM vPatient_Vaccination_Schedule H  
				WHERE ISNULL(IsActive, 0) = 0
GO
