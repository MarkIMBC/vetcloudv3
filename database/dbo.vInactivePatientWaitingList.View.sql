﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientWaitingList] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingList H  
				WHERE ISNULL(IsActive, 0) = 0
GO
