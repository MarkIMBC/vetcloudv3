﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pAdjustClientCredits](@ClientCredits  typClientCredit READONLY,
                                @ID_UserSession INT)
AS
  BEGIN
      INSERT INTO [dbo].[tClient_CreditLogs]
                  ([ID_Company],
                   ID_Client,
                   [Date],
                   [CreditAmount],
                   [Code],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [IsActive])
      SELECT client.ID_Company,
             clientCredit.ID_Client,
             clientCredit.Date,
             clientCredit.CreditAmount,
             clientCredit.Code,
             clientCredit.Comment,
             GETDATE(),
             GETDATE(),
             1,
             1,
             1
      FROm   @ClientCredits clientCredit
             LEFT JOIN tClient client
                    ON clientCredit.ID_Client = client.ID

      DECLARE @IDs_Client typIntList

      INSERT @IDs_Client
      SELECT DISTINCT ID_Client
      FROM   @ClientCredits

      exec pUpdateClientCreditsByClient
        @IDs_Client
  END

GO
