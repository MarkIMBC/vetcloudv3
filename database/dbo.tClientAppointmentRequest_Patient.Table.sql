﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tClientAppointmentRequest_Patient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ClientAppointmentRequest] [int] NULL,
	[ID_Patient] [int] NULL,
 CONSTRAINT [PK_tClientAppointmentRequest_Patient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tClientAppointmentRequest_Patient] ON [dbo].[tClientAppointmentRequest_Patient]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] CHECK CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_Company]
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] CHECK CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] CHECK CONSTRAINT [FK_tClientAppointmentRequest_Patient_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]  WITH CHECK ADD  CONSTRAINT [FKtClientAppointmentRequest_Patient_ID_ClientAppointmentRequest] FOREIGN KEY([ID_ClientAppointmentRequest])
REFERENCES [dbo].[tClientAppointmentRequest] ([ID])
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] CHECK CONSTRAINT [FKtClientAppointmentRequest_Patient_ID_ClientAppointmentRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tClientAppointmentRequest_Patient]
		ON [dbo].[tClientAppointmentRequest_Patient]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientAppointmentRequest_Patient
			SET    DateCreated = GETDATE()
			FROM   dbo.tClientAppointmentRequest_Patient hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] ENABLE TRIGGER [rDateCreated_tClientAppointmentRequest_Patient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tClientAppointmentRequest_Patient]
		ON [dbo].[tClientAppointmentRequest_Patient]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientAppointmentRequest_Patient
			SET    DateModified = GETDATE()
			FROM   dbo.tClientAppointmentRequest_Patient hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientAppointmentRequest_Patient] ENABLE TRIGGER [rDateModified_tClientAppointmentRequest_Patient]
GO
