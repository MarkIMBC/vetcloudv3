﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDoc] AS   
				SELECT   
				 H.*  
				FROM vDoc H  
				WHERE ISNULL(IsActive, 0) = 0
GO
