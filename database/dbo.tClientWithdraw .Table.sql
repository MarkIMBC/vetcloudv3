﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tClientWithdraw ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Client] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_Patient_Confinement] [int] NULL,
	[Date] [datetime] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[WithdrawAmount] [decimal](18, 4) NULL,
	[ID_Patient] [int] NULL,
 CONSTRAINT [PK_tClientWithdraw ] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tClientWithdraw ] ON [dbo].[tClientWithdraw ]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tClientWithdraw ] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tClientWithdraw ]  WITH CHECK ADD  CONSTRAINT [FK_tClientWithdraw _ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tClientWithdraw ] CHECK CONSTRAINT [FK_tClientWithdraw _ID_Company]
GO
ALTER TABLE [dbo].[tClientWithdraw ]  WITH CHECK ADD  CONSTRAINT [FK_tClientWithdraw _ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientWithdraw ] CHECK CONSTRAINT [FK_tClientWithdraw _ID_CreatedBy]
GO
ALTER TABLE [dbo].[tClientWithdraw ]  WITH CHECK ADD  CONSTRAINT [FK_tClientWithdraw _ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientWithdraw ] CHECK CONSTRAINT [FK_tClientWithdraw _ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tClientWithdraw ]
		ON [dbo].[tClientWithdraw ]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientWithdraw
			SET    DateCreated = GETDATE()
			FROM   dbo.tClientWithdraw hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientWithdraw ] ENABLE TRIGGER [rDateCreated_tClientWithdraw ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tClientWithdraw ]
		ON [dbo].[tClientWithdraw ]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientWithdraw
			SET    DateModified = GETDATE()
			FROM   dbo.tClientWithdraw hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientWithdraw ] ENABLE TRIGGER [rDateModified_tClientWithdraw ]
GO
