﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient_SOAP_RegularConsoltation] AS   
				SELECT   
				 H.*  
				FROM vPatient_SOAP_RegularConsoltation H  
				WHERE ISNULL(IsActive, 0) = 0
GO
