﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPurchaseOrder_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_PurchaseOrder] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[VATAmount] [decimal](18, 4) NULL,
	[GrossAmount] [decimal](18, 4) NULL,
	[NetAmount] [decimal](18, 4) NULL,
	[ID_UOM] [int] NULL,
	[DiscountAmount] [decimal](18, 4) NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[Balance] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[Amount] [decimal](18, 4) NULL,
	[RemainingQuantity] [int] NULL,
	[ID_PurchaseOrder_Detail] [int] NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tPurchaseOrder_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item] FOREIGN KEY([ID_Item])
REFERENCES [dbo].[tItem] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] CHECK CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item]
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_Detail_ID_PurchaseOrder] FOREIGN KEY([ID_PurchaseOrder])
REFERENCES [dbo].[tPurchaseOrder] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] CHECK CONSTRAINT [FKtPurchaseOrder_Detail_ID_PurchaseOrder]
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPurchaseOrder_Detail_ID_UOM] FOREIGN KEY([ID_UOM])
REFERENCES [dbo].[tUnitOfMeasure] ([ID])
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] CHECK CONSTRAINT [FKtPurchaseOrder_Detail_ID_UOM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPurchaseOrder_Detail]
		ON [dbo].[tPurchaseOrder_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] ENABLE TRIGGER [rDateCreated_tPurchaseOrder_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPurchaseOrder_Detail]
		ON [dbo].[tPurchaseOrder_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tPurchaseOrder_Detail] ENABLE TRIGGER [rDateModified_tPurchaseOrder_Detail]
GO
