﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[_pUpdatePatientToWaitingListStatus](@IDs_PatientWaitingList        typIntList READONLY,
                                              @WaitingStatus_ID_FilingStatus INT,
                                              @DateReschedule                DateTime,
                                              @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    /* Update Waiting List Status */
    Update tPatientWaitingList
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus,
           DateSent = NULL,
           IsSentSMS = NULL,
           DateReschedule = @DateReschedule
    FROM   tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus
    FROM   tPatient patient
           inner join tPatientWaitingList waitingList
                   ON patient.ID = waitingList.ID_Patient
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /* Add Logs */
    INSERT INTO [dbo].[tPatientWaitingList_Logs]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_PatientWaitingList],
                 [WaitingStatus_ID_FilingStatus],
                 DateReschedule)
    SELECT H.Name,
           H.IsActive,
           waitingList.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           waitingList.ID,
           @WaitingStatus_ID_FilingStatus,
           @DateReschedule
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDATE() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

GO
