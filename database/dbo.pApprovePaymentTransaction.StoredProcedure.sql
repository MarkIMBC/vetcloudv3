﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pApprovePaymentTransaction] (@IDs_PaymentTransaction typIntList READONLY,
                                                 @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Credits_ID_PaymentMethod INT = 5;
      DECLARE @IDs_BillingInvoice typIntList;
      DECLARE @ClientCredits typClientCredit
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          INSERT @IDs_BillingInvoice
                 (ID)
          SELECT ptHed.ID_BillingInvoice
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID;

          INSERT @ClientCredits
          SELECT biHed.ID_Client,
                 ptHed.Date,
                 0 - ABS(ptHed.CreditAmount),
                 ptHed.Code,
                 'Consume Credit as payment'
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID
                 LEFT JOIN tBillingInvoice biHed
                        on ptHed.ID_BillingInvoice = biHed.ID
          WHERE  ptHed.ID_PaymentMethod = @Credits_ID_PaymentMethod

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApprovePaymentTransaction_validation
            @IDs_PaymentTransaction,
            @ID_UserSession;

          IF(SELECT COUNT(*)
             FROM   @ClientCredits) > 0
            BEGIN
                EXEC pAdjustClientCredits_validation
                  @ClientCredits,
                  @ID_UserSession
            END

          UPDATE dbo.tPaymentTransaction
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tPaymentTransaction bi
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON bi.ID = ids.ID;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          if(SELECT COUNT(*)
             FROM   @ClientCredits) > 0
            BEGIN
                EXEC pAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
