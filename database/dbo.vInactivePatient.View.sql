﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatient] AS   
				SELECT   
				 H.*  
				FROM vPatient H  
				WHERE ISNULL(IsActive, 0) = 0
GO
