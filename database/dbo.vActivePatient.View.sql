﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient] AS     
    SELECT     
     H.*    
    FROM vPatient H    
    WHERE IsActive = 1
GO
