﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItem_UnitCostLog] AS   
				SELECT   
				 H.*  
				FROM vItem_UnitCostLog H  
				WHERE ISNULL(IsActive, 0) = 0
GO
