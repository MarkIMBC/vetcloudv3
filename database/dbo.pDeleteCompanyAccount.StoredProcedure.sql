﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create   Proc [dbo].[pDeleteCompanyAccount](@ID_Company INT)
as
BEGIN

	Declare @IDs_User typIntList 
	Declare @IDs_Employee typIntList 

	INSERT @IDs_User
	SELECT  
		DISTINCT
		ID
	FROM    vUser 
	WHERE ID_Company =  @ID_Company

	INSERT @IDs_Employee
	SELECT  
		DISTINCT
		ID_Employee
	FROM    vUser 
	WHERE ID_Company =  @ID_Company

	Update tUserSession SET ID_User = 1 WHERE ID_User IN (SELECT ID FROM @IDs_User) 
	Update tCompany set	ID_LastModifiedBy = 1, ID_CreatedBy = 1 WHERE ID = @ID_Company
	DELETE FROM tUser_Roles WHERE  ID_User IN (SELECT  ID FROM @IDs_User)
	DELETE FROM tUser WHERE  ID IN (SELECT  ID FROM @IDs_User)
	DELETE FROM tEmployee WHERE  ID IN (SELECT  ID FROM @IDs_Employee)
	Update tCompany SET IsActive = 0, Comment = ISNULL(Comment,'') + 'Deleted User Access - ' + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt') WHERE ID = @ID_Company

END

GO
