﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelPatient_Confinement] (@IDs_Patient_Confinement typIntList READONLY,
                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_ClientDeposit typIntList

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tClientDeposit cd with (NOLOCK)
                 inner join @IDs_Patient_Confinement idsCOnfinement
                         ON cd.ID_Patient_Confinement = idsCOnfinement.ID
                            and ID_FilingStatus = @Approved_ID_FilingStatus

          exec [pCancelPatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          EXEC dbo.pCancelClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Confinement bi with (NOLOCK)
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          Update tPatient_SOAP
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   tPatient_SOAP soap with (NOLOCK)
                 INNER JOIN tPatient_Confinement confinement with (NOLOCK)
                         ON soap.ID_Patient_Confinement = confinement.ID
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON confinement.ID = ids.ID;

          exec [pCancelClientDeposit]
            @IDs_ClientDeposit,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
