﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE PROC [dbo].[pApprovePatient_DentalExamination]
(    
    @IDs_Patient_DentalExamination typIntList READONLY,    
    @ID_UserSession INT    
)    
AS    
BEGIN    

	Declare @ID_FilingStatus_Filed int = 1
	Declare @ID_FilingStatus_Approved int = 3

	Declare @Success bit = 1

	DECLARE @ID_User INT = 0;    
    
	SELECT @ID_User = ID_User    
	FROM dbo.tUserSession    
	WHERE ID = @ID_UserSession;    

	Update tPatient_DentalExamination 
	set ID_FilingStatus = @ID_FilingStatus_Approved,
		ID_ApprovedBy = @ID_User,
		DateApproved = Getdate()
	FROM tPatient_DentalExamination hed
	INNER JOIN @IDs_Patient_DentalExamination ids on hed.ID = ids.ID

	SELECT '_';    
    
    SELECT @Success Success,    
           '' message;  

END
GO
