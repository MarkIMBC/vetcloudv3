﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveDentalExamination] AS   
				SELECT   
				 H.*  
				FROM vDentalExamination H  
				WHERE IsActive = 1
GO
