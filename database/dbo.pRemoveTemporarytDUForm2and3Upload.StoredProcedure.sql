﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pRemoveTemporarytDUForm2and3Upload]
AS
  BEGIN
      DECLARE @tempTable VARCHAR(MAX)= ''

      SELECT @tempTable = @tempTable + name + ','
      FROM   sys.objects
      WHERE  type_desc = 'USER_TABLE'
             and name like '%__temp%'

      if( LEN(@tempTable) > 0 )
        BEGIN
            SET @tempTable = substring(@tempTable, 1, ( len(@tempTable) - 1 ))

            EXEC ( 'DROP TABLE ' + @tempTable)
        END
  END

GO
