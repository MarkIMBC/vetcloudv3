﻿CREATE TYPE [dbo].[typActiveInactiveSupplier] AS TABLE(
	[ID_Supplier] [int] NULL,
	[IsActive] [bit] NULL
)
GO
