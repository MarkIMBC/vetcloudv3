﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create     PROC [dbo].[tmp_pGetForSendSOAPPlan]( @Date DateTime, @IsSMSSent Bit)
AS
BEGIN

  DECLARE @Success BIT = 1;

  SELECT
  
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

SELECT  
	 tbl.Name_Company	
	 ,Count(*) Count
	 ,SUM(CASE WHEN LEN(tbl.Message) <= 160 THEN 1 ELSE 
		CASE WHEN LEN(tbl.Message) <= 306 THEN 2 ELSE 
			CASE WHEN LEN(tbl.Message) <= 459 THEN 3 ELSE 
				4
			END
		END
	END ) ConsumedSMSCredit
FROM    
	(
 	   	  SELECT
				c.Name Name_Company
			   ,client.Name Name_Client
			   ,ISNULL(client.ContactNumber,'0') ContactNumber_Client
			   ,soapPlan.DateReturn
			   ,soapPlan.Name_Item
			   ,ISNULL(patientSOAP.Comment, '') Comment
			   ,dbo.fGetSOAPLANMessage(c.Name, client.Name , ISNULL(c.ContactNumber, ''), patient.Name,  soapPlan.Name_Item , soapPlan.DateReturn) Message
			   ,CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
			   ,soapPlan.ID ID_Patient_SOAP_Plan
		  FROM dbo.tPatient_SOAP patientSOAP
		  LEFT JOIN dbo.tPatient patient
			ON patient.ID = patientSOAP.ID_Patient
		  LEFT JOIN dbo.tClient client
			ON client.ID = patient.ID_Client
		  LEFT JOIN tCompany c
			ON c.iD = patientSOAP.ID_Company
		  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
			ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
	  WHERE patientSOAP.ID_FilingStatus IN (1, 3)
		AND ISNULL(soapPlan.IsSentSMS, 0) = @IsSMSSent
		AND ISNULL(ID_CLient,0) > 0
		AND ( 
				(CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date))-- OR
				--(CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date)) 
			)
		AND patientSOAP.ID_Company IN (
			SELECT  ID_Company
			FROM    tSMSPatientSOAP_Company
	)
	) tbl 
GROUP BY  Name_Company 
END

GO
