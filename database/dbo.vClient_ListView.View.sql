﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClient_ListView]
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '')    ContactNumbers,
         DateCreated,
         DateLastVisited,
         ISNULL(IsActive, 0)             IsActive,
         LastAttendingPhysician_Name_Employee,
         ISNULL(TotalRemainingAmount, 0) TotalRemainingAmount
  FROM   dbo.vClient

GO
