﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tMessageThread](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Recipient_ID_User] [int] NULL,
	[Sender_ID_User] [int] NULL,
	[Message] [varchar](3000) NULL,
	[DateSent] [datetime] NULL,
	[DateRead] [datetime] NULL,
	[IsRead] [bit] NULL,
 CONSTRAINT [PK_tMessageThread] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tr_dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender] ON [dbo].[tMessageThread] 
WITH EXECUTE AS SELF
AFTER insert, update, delete AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @rowsToProcess INT
    DECLARE @currentRow INT
    DECLARE @records XML
    DECLARE @theMessageContainer NVARCHAR(MAX)
    DECLARE @dmlType NVARCHAR(10)
    DECLARE @modifiedRecordsTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @exceptTable TABLE ([RowNumber] INT, [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
	DECLARE @deletedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @insertedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @var1 int
    DECLARE @var2 int
    DECLARE @var3 int
    DECLARE @var4 varchar(3000)
    DECLARE @var5 datetime
    DECLARE @var6 datetime
    DECLARE @var7 bit

    DECLARE @conversationHandlerExists INT
    SELECT @conversationHandlerExists = COUNT(*) FROM sys.conversation_endpoints WHERE conversation_handle = '462803d1-bf00-eb11-885c-b430c8dc2607';
    IF @conversationHandlerExists = 0
    BEGIN
        DECLARE @conversation_handle UNIQUEIDENTIFIER;
        DECLARE @schema_id INT;
        SELECT @schema_id = schema_id FROM sys.schemas WITH (NOLOCK) WHERE name = N'dbo';

        
        IF EXISTS (SELECT * FROM sys.triggers WITH (NOLOCK) WHERE object_id = OBJECT_ID(N'[dbo].[tr_dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender]')) DROP TRIGGER [dbo].[tr_dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender') EXEC (N'ALTER QUEUE [dbo].[dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender] WITH ACTIVATION (STATUS = OFF)');

        
        SELECT conversation_handle INTO #Conversations FROM sys.conversation_endpoints WITH (NOLOCK) WHERE far_service LIKE N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_%' ORDER BY is_initiator ASC;
        DECLARE conversation_cursor CURSOR FAST_FORWARD FOR SELECT conversation_handle FROM #Conversations;
        OPEN conversation_cursor;
        FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        WHILE @@FETCH_STATUS = 0 
        BEGIN
            END CONVERSATION @conversation_handle WITH CLEANUP;
            FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        END
        CLOSE conversation_cursor;
        DEALLOCATE conversation_cursor;
        DROP TABLE #Conversations;

        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Receiver') DROP SERVICE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Receiver];
        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender') DROP SERVICE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Receiver') DROP QUEUE [dbo].[dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Receiver];
        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender') DROP QUEUE [dbo].[dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_contracts WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573') DROP CONTRACT [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573];
        
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Insert') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Insert];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Update') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Update];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Delete') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Delete];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/EndMessage') DROP MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/EndMessage];

        
        IF EXISTS (SELECT * FROM sys.objects WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_QueueActivationSender') DROP PROCEDURE [dbo].[dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_QueueActivationSender];
        RETURN
    END
    
    IF NOT EXISTS(SELECT 1 FROM INSERTED)
    BEGIN
        SET @dmlType = 'Delete'
        INSERT INTO @modifiedRecordsTable SELECT [ID], [Recipient_ID_User], [Sender_ID_User], [Message], [DateSent], [DateRead], [IsRead] FROM DELETED 
    END
    ELSE
    BEGIN
        IF NOT EXISTS(SELECT * FROM DELETED)
        BEGIN
            SET @dmlType = 'Insert'
            INSERT INTO @modifiedRecordsTable SELECT [ID], [Recipient_ID_User], [Sender_ID_User], [Message], [DateSent], [DateRead], [IsRead] FROM INSERTED 
        END
        ELSE
        BEGIN
            SET @dmlType = 'Update';
            INSERT INTO @deletedTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM DELETED
            INSERT INTO @insertedTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM INSERTED
            INSERT INTO @exceptTable SELECT [RowNumber],[ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @insertedTable EXCEPT SELECT [RowNumber],[ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @deletedTable

            INSERT INTO @modifiedRecordsTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @exceptTable e 
        END
    END

    SELECT @rowsToProcess = COUNT(1) FROM @modifiedRecordsTable    

    BEGIN TRY
        WHILE @rowsToProcess > 0
        BEGIN
            SELECT	@var1 = [ID], @var2 = [Recipient_ID_User], @var3 = [Sender_ID_User], @var4 = [Message], @var5 = [DateSent], @var6 = [DateRead], @var7 = [IsRead]
            FROM	@modifiedRecordsTable
            WHERE	[RowNumber] = @rowsToProcess
                
            IF @dmlType = 'Insert' 
            BEGIN
                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Insert] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/EndMessage] (0x)
            END
        
            IF @dmlType = 'Update'
            BEGIN
                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Update] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/EndMessage] (0x)
            END

            IF @dmlType = 'Delete'
            BEGIN
                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/StartMessage/Delete] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '462803d1-bf00-eb11-885c-b430c8dc2607' MESSAGE TYPE [dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573/EndMessage] (0x)
            END

            SET @rowsToProcess = @rowsToProcess - 1
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
    END CATCH
END
GO
ALTER TABLE [dbo].[tMessageThread] ENABLE TRIGGER [tr_dbo_tMessageThread_82104c6d-59cd-46bc-b535-d0c1a4126573_Sender]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender] ON [dbo].[tMessageThread] 
WITH EXECUTE AS SELF
AFTER insert, update, delete AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @rowsToProcess INT
    DECLARE @currentRow INT
    DECLARE @records XML
    DECLARE @theMessageContainer NVARCHAR(MAX)
    DECLARE @dmlType NVARCHAR(10)
    DECLARE @modifiedRecordsTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @exceptTable TABLE ([RowNumber] INT, [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
	DECLARE @deletedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @insertedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Recipient_ID_User] int, [Sender_ID_User] int, [Message] varchar(3000), [DateSent] datetime, [DateRead] datetime, [IsRead] bit)
    DECLARE @var1 int
    DECLARE @var2 int
    DECLARE @var3 int
    DECLARE @var4 varchar(3000)
    DECLARE @var5 datetime
    DECLARE @var6 datetime
    DECLARE @var7 bit

    DECLARE @conversationHandlerExists INT
    SELECT @conversationHandlerExists = COUNT(*) FROM sys.conversation_endpoints WHERE conversation_handle = '1aa7aef5-dafb-ea11-885b-c67558a5f429';
    IF @conversationHandlerExists = 0
    BEGIN
        DECLARE @conversation_handle UNIQUEIDENTIFIER;
        DECLARE @schema_id INT;
        SELECT @schema_id = schema_id FROM sys.schemas WITH (NOLOCK) WHERE name = N'dbo';

        
        IF EXISTS (SELECT * FROM sys.triggers WITH (NOLOCK) WHERE object_id = OBJECT_ID(N'[dbo].[tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender]')) DROP TRIGGER [dbo].[tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') EXEC (N'ALTER QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender] WITH ACTIVATION (STATUS = OFF)');

        
        SELECT conversation_handle INTO #Conversations FROM sys.conversation_endpoints WITH (NOLOCK) WHERE far_service LIKE N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_%' ORDER BY is_initiator ASC;
        DECLARE conversation_cursor CURSOR FAST_FORWARD FOR SELECT conversation_handle FROM #Conversations;
        OPEN conversation_cursor;
        FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        WHILE @@FETCH_STATUS = 0 
        BEGIN
            END CONVERSATION @conversation_handle WITH CLEANUP;
            FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        END
        CLOSE conversation_cursor;
        DEALLOCATE conversation_cursor;
        DROP TABLE #Conversations;

        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver') DROP SERVICE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver];
        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') DROP SERVICE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver') DROP QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver];
        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') DROP QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_contracts WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24') DROP CONTRACT [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24];
        
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Insert') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Insert];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Update') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Update];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Delete') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Delete];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage];

        
        IF EXISTS (SELECT * FROM sys.objects WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_QueueActivationSender') DROP PROCEDURE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_QueueActivationSender];
        RETURN
    END
    
    IF NOT EXISTS(SELECT 1 FROM INSERTED)
    BEGIN
        SET @dmlType = 'Delete'
        INSERT INTO @modifiedRecordsTable SELECT [ID], [Recipient_ID_User], [Sender_ID_User], [Message], [DateSent], [DateRead], [IsRead] FROM DELETED 
    END
    ELSE
    BEGIN
        IF NOT EXISTS(SELECT * FROM DELETED)
        BEGIN
            SET @dmlType = 'Insert'
            INSERT INTO @modifiedRecordsTable SELECT [ID], [Recipient_ID_User], [Sender_ID_User], [Message], [DateSent], [DateRead], [IsRead] FROM INSERTED 
        END
        ELSE
        BEGIN
            SET @dmlType = 'Update';
            INSERT INTO @deletedTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM DELETED
            INSERT INTO @insertedTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM INSERTED
            INSERT INTO @exceptTable SELECT [RowNumber],[ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @insertedTable EXCEPT SELECT [RowNumber],[ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @deletedTable

            INSERT INTO @modifiedRecordsTable SELECT [ID],[Recipient_ID_User],[Sender_ID_User],[Message],[DateSent],[DateRead],[IsRead] FROM @exceptTable e 
        END
    END

    SELECT @rowsToProcess = COUNT(1) FROM @modifiedRecordsTable    

    BEGIN TRY
        WHILE @rowsToProcess > 0
        BEGIN
            SELECT	@var1 = [ID], @var2 = [Recipient_ID_User], @var3 = [Sender_ID_User], @var4 = [Message], @var5 = [DateSent], @var6 = [DateRead], @var7 = [IsRead]
            FROM	@modifiedRecordsTable
            WHERE	[RowNumber] = @rowsToProcess
                
            IF @dmlType = 'Insert' 
            BEGIN
                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Insert] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage] (0x)
            END
        
            IF @dmlType = 'Update'
            BEGIN
                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Update] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage] (0x)
            END

            IF @dmlType = 'Delete'
            BEGIN
                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Delete] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (CONVERT(NVARCHAR(MAX), @var5, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (CONVERT(NVARCHAR(MAX), @var6, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (CONVERT(NVARCHAR(MAX), @var7))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead] (0x)
                END

                ;SEND ON CONVERSATION '1aa7aef5-dafb-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage] (0x)
            END

            SET @rowsToProcess = @rowsToProcess - 1
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
    END CATCH
END
GO
ALTER TABLE [dbo].[tMessageThread] ENABLE TRIGGER [tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender]
GO
