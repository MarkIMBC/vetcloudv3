﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveClientDeposit] AS   
				SELECT   
				 H.*  
				FROM vClientDeposit H  
				WHERE IsActive = 1
GO
