﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fIsModelExistByTableName](@tableName VARCHAR(MAX))
RETURNS BIT
AS
  BEGIN
      DECLARE @IsExist BIT = 0

      IF(SELECT COUNT(*)
         FROM   _tModel
         where  TableName = @tableName) > 0
        BEGIN
            SET @IsExist = 1
        END

      RETURN @IsExist
  END;

GO
