﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 FUNCTION [dbo].[fGetImageFileList](@IDsCompanyString varchar(MAX))
RETURNS @ImagePaths TABLE (
  ID_Company       INT,
  Name_Company     VARCHAR(MAX),
  GUID_Company     VARCHAR(MAX),
  TableName        VARCHAR(MAX),
  Model            VARCHAR(MAX),
  ID_CurrentObject INT,
  ColumnName       VARCHAR(MAX),
  ImageFileName    VARCHAR(MAX))
AS
  BEGIN
      DECLARE @IDS_Company typIntList
      DECLARE @IsSuccess BIT = 1
      DECLARE @IsFolderized BIT = 1

      if( LEN(@IDsCompanyString) > 0 )
        BEGIN
            INSERT @IDS_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDS_Company
            SELECT ID
            FROM   tCompany
            WHERE  IsActive = 1
                   and ID NOT IN ( 1 )
        END

      /* Company */
      INSERT @ImagePaths
      SELECT company.ID,
             company.Name,
             company.Guid,
             'tCompany',
             'Company' Model,
             company.ID,
             'ImageHeaderFilename',
             ImageHeaderFilename
      FROm   tCompany company
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageHeaderFilename, '') <> '' )
      UNIOn ALL
      SELECT company.ID,
             company.Name,
             company.Guid,
             'tCompany',
             'Company' Model,
             company.ID,
             'ImageLogoFilename',
             ImageLogoFilename
      FROm   tCompany company
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageLogoFilename, '') <> '' )

      /*Employee*/
      INSERT @ImagePaths
      SELECT company.ID,
             company.Name,
             company.Guid,
             'tEmployee',
             'Employee' Model,
             emp.ID,
             'ImageFile',
             ImageFile
      FROm   tEmployee emp
             inner join @IDS_Company ids
                     on emp.ID_Company = ids.ID
             inner join tCompany company
                     on emp.ID_Company = company.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )

      /*Patient*/
      INSERT @ImagePaths
      SELECT company.ID,
             company.Name,
             company.Guid,
             'tPatient',
             'Patient' Model,
             patient.ID,
             'ProfileImageFile',
             ProfileImageFile
      FROm   tPatient patient
             inner join @IDS_Company ids
                     on patient.ID_Company = ids.ID
             inner join tCompany company
                     on patient.ID_Company = company.ID
      WHERE  ( ISNULL(ProfileImageFile, '') <> '' )

      /* SOAP */
      INSERT @ImagePaths
      SELECT company.ID,
             company.Name,
             company.Guid,
             'tPatient_SOAP',
             'Patient_SOAP' Model,
             ps.ID,
             'LabImageFilePath' + ImageNo,
             FilePath
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
             INNER JOIN @IDS_Company idsCompany
                     on idsCompany.ID = ps.ID_Company
             inner join tCompany company
                     on ps.ID_Company = company.ID
      WHERE  ( ISNULL(FilePath, '') <> '' )

      RETURN;
  END

GO
