﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pInsertPosition](@Position   VARCHAR(MAX),
                           @ID_Company INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tPosition
         WHERE  Name = @Position) = 0
        BEGIN
            INSERT INTO [dbo].tPosition
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Position,
                         1,
                         @ID_Company)
        END
  END 
GO
