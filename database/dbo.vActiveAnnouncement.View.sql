﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAnnouncement] AS   
				SELECT   
				 H.*  
				FROM vAnnouncement H  
				WHERE IsActive = 1
GO
