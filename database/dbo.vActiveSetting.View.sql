﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSetting] AS   
				SELECT   
				 H.*  
				FROM vSetting H  
				WHERE IsActive = 1
GO
