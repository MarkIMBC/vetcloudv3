﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzPatientReferralForm]
AS
  SELECT hed.ID,
         hed.Code,
         hed.Date,
         hed.History,
         hed.Treatment,
         hed.Concerns,
         client.Name                                   Name_Client,
         client.ContactNumber                          ContactNumber_Client,
         patient.Name                                  Name_Patient,
         patient.Species,
         patient.DateBirth,
         gender.Name                                   Name_Gender,
         dbo.fGetAge(patient.DateBirth, GETDATE(), '') Age_Patient,
         attendingPhysician.Name                       AttendingPhysician_Name_Employee,
         attendingPhysician.PRCLicenseNumber           AttendingPhysician_PRCLicenseNumber_Employee,
         referralTypeCount.ReferralType_1,
         referralTypeCount.ReferralType_2,
         referralTypeCount.ReferralType_3,
         referralTypeCount.ReferralType_4,
         referralTypeCount.ReferralType_5,
         referralTypeCount.ReferralType_6,
         hed.ReferralTypeOthers,
         hed.ID_Company,
         filingStatus.Name                             Name_FilingStatus,
         company.ImageLogoLocationFilenamePath,
         company.Name                                  Name_Company,
         company.Address                               Address_Company,
         company.ContactNumber                         ContactNumber_Company,
         CASE
           WHEN LEN(ISNULL(company.Address, '')) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(ISNULL(company.ContactNumber, '')) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(company.Email, '')) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                         HeaderInfo_Company
  FROM   tPatientReferral hed
         LEFT JOIN tClient client
                on hed.ID_Client = client.ID
         LEFT JOIN tPatient patient
                on hed.ID_Patient = patient.ID
         LEFT JOIN tEmployee attendingPhysician
                on hed.AttendingPhysician_ID_Employee = attendingPhysician.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
         LEFT JOIN dbo.tFilingStatus filingStatus
                ON filingStatus.ID = hed.ID_FilingStatus
         LEFT JOIN dbo.vPatientReferralReferralTypeCount referralTypeCount
                on hed.ID = referralTypeCount.ID_PatientReferral
         LEFT JOIN dbo.tGender gender
                on patient.ID_Gender = gender.ID

GO
