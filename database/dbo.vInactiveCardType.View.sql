﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCardType] AS   
				SELECT   
				 H.*  
				FROM vCardType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
