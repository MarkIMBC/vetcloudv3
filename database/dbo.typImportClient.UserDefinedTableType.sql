﻿CREATE TYPE [dbo].[typImportClient] AS TABLE(
	[Name] [varchar](max) NULL,
	[Old_client_id] [varchar](max) NULL,
	[Old_client_code] [varchar](max) NULL,
	[ContactNumber] [varchar](max) NULL,
	[Email] [varchar](max) NULL,
	[Address] [varchar](max) NULL
)
GO
