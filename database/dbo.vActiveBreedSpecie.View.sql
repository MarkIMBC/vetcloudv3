﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBreedSpecie] AS   
				SELECT   
				 H.*  
				FROM vBreedSpecie H  
				WHERE IsActive = 1
GO
