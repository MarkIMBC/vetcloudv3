﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create     
 proc [dbo].[pGetLoggedUserSession](@ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @IsActive     bit = 0

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @IsActive = IsActive
      FROM   tUserSession WITH (NOLOCK)
      WHERE  ID = @ID_UserSession

      SELECT '_'

      SELECT @ID_UserSession         ID,
             CONVERT(BIT, @IsActive) IsActive,
             CONVERT(BIT, @IsActive) IsUserSessionActive
  END

GO
