﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vActiveItemService]
AS
  SELECT *
  FROM   dbo.vItemService
  Where  IsActive = 1;

GO
