﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePurchaseOrder] AS   
				SELECT   
				 H.*  
				FROM vPurchaseOrder H  
				WHERE IsActive = 1
GO
