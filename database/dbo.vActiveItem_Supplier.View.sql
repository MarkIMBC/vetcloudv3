﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveItem_Supplier] AS   
				SELECT   
				 H.*  
				FROM vItem_Supplier H  
				WHERE IsActive = 1
GO
