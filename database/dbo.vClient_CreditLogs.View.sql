﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vClient_CreditLogs]
AS
  SELECT H.*,
         UC.Name                                                               AS CreatedBy,
         UM.Name                                                               AS LastModifiedBy,
         ISNULL(clientDeposit.Name_Patient, clientWithdraw.Name_Patient)       Name_Patient,
         ISNULL(clientDeposit.ID_FilingStatus, clientWithdraw.ID_FilingStatus) ID_FilingStatus,
         ISNULL(clientDeposit.Name_FilingStatus, clientWithdraw.Name_FilingStatus) Name_FilingStatus
  FROM   tClient_CreditLogs H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN vClientDeposit clientDeposit
                on clientDeposit.ID_Company = h.ID_Company
                   and clientDeposit.ID_Client = h.ID_Client
                   and clientDeposit.Code = h.Code
         LEFT JOIN vClientWithdraw clientWithdraw
                on clientWithdraw.ID_Company = h.ID_Company
                   and clientWithdraw.ID_Client = h.ID_Client
                   and clientWithdraw.Code = h.Code

GO
