﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInsertUserRoleModel] (@Name_UserRole   VARCHAR(MAX),
                                 @TableName_Model VARCHAR(MAX))
AS
  BEGIN
      DECLARE @ID_UserRole INT = 0
      DECLARE @Oid_Model VARCHAR(MAX) = ''
      DECLARE @Name_Model VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid,
             @Name_Model = Name
      FROM   _tModel
      where  TableName = @TableName_Model

      SELECT @Name_Model

      IF(SELECT COUNT(*)
         FROM   tUserRole
         where  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess])
            SELECT @Name_UserRole,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   0;
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      if(SELECT COUNT(*)
         FROM   tUserRole_Detail
         where  ID_Model = @Oid_Model
                AND ID_UserRole = @ID_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Detail]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Model])
            SELECT @Name_Model,
                   1,
                   @ID_UserRole,
                   @Oid_Model
        END
  END

GO
