﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveScheduleType] AS   
				SELECT   
				 H.*  
				FROM vScheduleType H  
				WHERE IsActive = 1
GO
