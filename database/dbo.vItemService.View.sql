﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Change Item Service Datasource  
CREATE VIEW [dbo].[vItemService]
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy
  FROM   dbo.tItem H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
  WHERE  H.ID_ItemType = 1
         AND h.IsActive = 1; 
GO
