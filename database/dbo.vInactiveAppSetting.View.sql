﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppSetting] AS   
				SELECT   
				 H.*  
				FROM vAppSetting H  
				WHERE ISNULL(IsActive, 0) = 0
GO
