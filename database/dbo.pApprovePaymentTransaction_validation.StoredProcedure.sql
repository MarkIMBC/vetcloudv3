﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pApprovePaymentTransaction_validation] (@IDs_PaymentTransaction typIntList READONLY,
                                                          @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateHaZeroPayable INT = 0;
      DECLARE @ValidateHaZeroPayable TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;
      DECLARE @ValidateNotAbleToPay TABLE
        (
           Code                           VARCHAR(30),
           Code_BillingInvoice            VARCHAR(30),
           RemainingAmount_BillingInvoice DECIMAL(18, 4),
           PaymentAmount                  DECIMAL(18, 4)
        );
      DECLARE @Count_ValidateNotAbleToPay INT = 0;

      /* Validate Payment Amount is Zero*/
      INSERT @ValidateHaZeroPayable
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPaymentTransaction bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_PaymentTransaction ids
                     WHERE  ids.ID = bi.ID)
             AND ISNULL(bi.PaymentAmount, 0) = 0

      SELECT @Count_ValidateHaZeroPayable = COUNT(*)
      FROM   @ValidateHaZeroPayable;

      IF ( @Count_ValidateHaZeroPayable > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateHaZeroPayable > 1 THEN 's have'
                               ELSE ' has '
                             END
                           + 'ZERO Payment Amount:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateHaZeroPayable;

            THROW 50001, @message, 1;
        END;

      /* Validate PaymentTransaction Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPaymentTransaction bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_PaymentTransaction ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;

      /* Validate Validate Not Able To Pay*/
      insert INTO @ValidateNotAbleToPay
                  (Code,
                   Code_BillingInvoice,
                   RemainingAmount_BillingInvoice,
                   PaymentAmount)
      SELECT p.Code,
             bi.Code,
             bi.RemainingAmount,
             p.PaymentAmount
      FROM   tPaymentTransaction p
             INNER JOIN @IDs_PaymentTransaction ids
                     ON ids.ID = p.ID
             INNER JOIN tBillingInvoice bi
                     ON p.ID_BillingInvoice = bi.ID
      WHERE  ISNULL(bi.RemainingAmount, 0) <= 0

      SELECT @Count_ValidateNotAbleToPay = COUNT(*)
      FROM   @ValidateNotAbleToPay;

      IF ( @Count_ValidateNotAbleToPay > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code_BillingInvoice
                              + ' Rem. Bal: '
                              + FORMAT(RemainingAmount_BillingInvoice, '#,##0.00')
                              + ' Payment: '
                              + FORMAT(PaymentAmount, '#,##0.00')
            FROM   @ValidateNotAbleToPay;

            THROW 50001, @message, 1;
        END;
  END; 
GO
