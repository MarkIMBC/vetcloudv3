﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientReferral] AS   
    SELECT   
     H.*  
    FROM vPatientReferral H  
    WHERE IsActive = 1
GO
