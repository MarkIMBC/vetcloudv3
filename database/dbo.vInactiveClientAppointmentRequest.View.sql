﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveClientAppointmentRequest] AS   
				SELECT   
				 H.*  
				FROM vClientAppointmentRequest H  
				WHERE ISNULL(IsActive, 0) = 0
GO
