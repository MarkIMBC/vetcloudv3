﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pValidateClient] (@ID_Client      INT,
                                    @Name           VARCHAR(MAX),
                                    @ID_UserSession INT)
AS
  BEGIN
      DECLARE @isValid BIT = 1;
      DECLARE @IsWarning BIT = 0;
      DECLARE @message VARCHAR(MAX) = '';
      DECLARE @msgTHROW VARCHAR(MAX) = '';
      DECLARE @ID_User INT = 0;
      DECLARE @ID_Company INT = 0;

      SELECT @ID_User = userSession.ID_User,
             @ID_Company = _user.ID_Company
      FROM   dbo.tUserSession userSession
             INNER JOIN vUser _user
                     ON userSession.ID_User = _user.ID
      WHERE  userSession.ID = @ID_UserSession;

      SET @Name = LTRIM(RTRIM(ISNULL(@Name, '')))

      if ISNULL(@ID_Company, 0) = 0
        BEGIN
            SELECT @ID_Company = ID_Company
            FROM   tClient
            WHERE  ID = @ID_Client
        END

      BEGIN TRY
          DECLARE @ExistName Varchar(MAX) = ''
          DECLARE @Count INT = 0

          IF LEN(@Name) = 0
            BEGIN
                SET @msgTHROW = 'Client Name is required.';

                THROW 50005, @msgTHROW, 1;
            END

          IF( @ID_Client = -1 )
            BEGIN
                if(SELECT COUNT(*)
                   FROM   tClient
                   WHERE  ID_Company = @ID_Company
                          AND LTRIM(RTRIM(ISNULL(Name, ''))) = @Name) > 0
                  BEGIN
                      SET @IsWarning = 0;
                      SET @msgTHROW = 'Client ''' + @Name + ''' is already exist';

                      THROW 50005, @msgTHROW, 1;
                  END
            END
          ELSE
            BEGIN
                DECLARE @CountClientExist INT = 0

                SELECT @CountClientExist = COUNT(*)
                FROM   tClient
                WHERE  ID_Company = @ID_Company
                       AND ID NOT IN ( @ID_Client )
                       AND LTRIM(RTRIM(ISNULL(Name, ''))) = @Name

                if( @CountClientExist ) > 0
                  BEGIN
                      SET @IsWarning = 1;
                      SET @msgTHROW = 'Client ''' + @Name + ''' is already exist for ' + FORMAT(@CountClientExist, '#,#0') + ' times.';

                      THROW 50005, @msgTHROW, 1;
                  END
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH;

      SELECT '_';

      SELECT @isValid   isValid,
             @IsWarning isWarning,
             @message   message;
  END;

GO
