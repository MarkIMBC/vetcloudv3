﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pDoAddPatientToWaitingList_validation] (@IDs_Patient    typIntList READONLY,
                                                          @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Reschedule_ID_FilingStatus INT = 21;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @PatientAlreadyWaitingList TABLE
        (
           Name_Client                     VARCHAR(30),
           Name_Patient                    VARCHAR(30),
           WaitingStatus_Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_PatientAlreadyWaitingList INT = 0;

      INSERT @PatientAlreadyWaitingList
             (Name_Client,
              Name_Patient,
              WaitingStatus_Name_FilingStatus)
      SELECT Distinct Name_Client,
                      Name_Patient,
                      WaitingStatus_Name_FilingStatus
      FROm   vPatientWaitingList pWait
             INNER JOIN @IDs_Patient ids
                     ON pWait.ID_Patient = ids.ID
      WHERE  WaitingStatus_ID_FilingStatus NOT IN ( @Done_ID_FilingStatus, @Canceled_ID_FilingStatus, @Reschedule_ID_FilingStatus )

      SELECT @Count_PatientAlreadyWaitingList = COUNT(*)
      FROM   @PatientAlreadyWaitingList;

      IF ( @Count_PatientAlreadyWaitingList > 0 )
        BEGIN
            SET @message = 'The following patient'
                           + CASE
                               WHEN @Count_PatientAlreadyWaitingList > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'already on waiting list:';

            SELECT @message = @message + CHAR(10) + Name_Client + ' Pet:'
                              + Name_Patient + ' - '
                              + WaitingStatus_Name_FilingStatus
            FROM   @PatientAlreadyWaitingList;

            THROW 50001, @message, 1;
        END;
  END;

GO
