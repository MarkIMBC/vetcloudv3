﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tClientFeedback](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Client] [int] NULL,
 CONSTRAINT [PK_tClientFeedback] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tClientFeedback] ON [dbo].[tClientFeedback]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tClientFeedback] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tClientFeedback]  WITH CHECK ADD  CONSTRAINT [FK_tClientFeedback_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tClientFeedback] CHECK CONSTRAINT [FK_tClientFeedback_ID_Company]
GO
ALTER TABLE [dbo].[tClientFeedback]  WITH CHECK ADD  CONSTRAINT [FK_tClientFeedback_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientFeedback] CHECK CONSTRAINT [FK_tClientFeedback_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tClientFeedback]  WITH CHECK ADD  CONSTRAINT [FK_tClientFeedback_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tClientFeedback] CHECK CONSTRAINT [FK_tClientFeedback_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tClientFeedback]
		ON [dbo].[tClientFeedback]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientFeedback
			SET    DateCreated = GETDATE()
			FROM   dbo.tClientFeedback hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientFeedback] ENABLE TRIGGER [rDateCreated_tClientFeedback]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tClientFeedback]
		ON [dbo].[tClientFeedback]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tClientFeedback
			SET    DateModified = GETDATE()
			FROM   dbo.tClientFeedback hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tClientFeedback] ENABLE TRIGGER [rDateModified_tClientFeedback]
GO
