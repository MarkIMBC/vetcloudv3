﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBusinessPartner_Contact] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartner_Contact H  
				WHERE ISNULL(IsActive, 0) = 0
GO
