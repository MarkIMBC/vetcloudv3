﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE   PROC [dbo].[ppUpdatePatient_Grooming_BillingStatus_By_BIs](@IDs_BillingInvoice typIntList READONLY)
  AS
  BEGIN

	DECLARE @IDs_Patient_Grooming typIntList

	INSERT @IDs_Patient_Grooming
	SELECT bi.ID_Patient_Grooming FROM tBillingInvoice bi Inner join @IDs_BillingInvoice ids on bi.ID = ids.ID
	where ISNULL(bi.ID_Patient_Grooming, 0) > 0

	exec pUpdatePatient_Grooming_BillingStatus @IDs_Patient_Grooming
  END

GO
