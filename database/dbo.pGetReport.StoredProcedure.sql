﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pGetReport] @Oid uniqueidentifier = NULL,
                            @ID_Session INT = NULL
AS
    BEGIN
        SELECT '_','' AS Report_Filters
        DECLARE @ID_User INT, @ID_Warehouse INT;
        SELECT @ID_User = ID_User, 
               @ID_Warehouse = ID_Warehouse
        FROM tUserSession
        WHERE ID = @ID_Session;
        IF(@Oid  IS NULL)
            BEGIN
                SELECT H.*
                FROM
                (
                    SELECT NULL AS [_], 
                           NULL AS [Oid], 
                           NULL AS [Code], 
                           NULL AS [Name], 
                           1 AS [IsActive], 
                           NULL AS [Comment], 
                           NULL AS [DateCreated], 
                           NULL AS [DateModified], 
                           NULL AS [ID_CreatedBy], 
                           NULL AS [ID_LastModifiedBy], 
                           NULL AS [ReportPath]
                ) H
                LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID;
        END;
            ELSE
            BEGIN
                SELECT H.*
                FROM dbo._tReport H
                WHERE H.Oid = @Oid;
        END;

		SELECT H.*  FROM _tReport_Filters H 
		LEFT JOIN dbo._tPropertyType P ON H.ID_PropertyType = P.ID	
		LEFT JOIN dbo._tControlType C ON H.ID_ControlType	= C.ID
		WHERE ID_Report = @Oid	
    END;

GO
