﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pReUpdateItemCurrentInventory](@ID_Item INT)
AS
  BEGIN
      DECLARE @IDs_Item TYPINTLIST

      INSERT @IDs_Item
      VALUES (@ID_Item)

      exec pUpdateItemCurrentInventoryByIDsItems
        @IDs_Item

      SELECT '_'

      SELECT GETDATE() Date
  END

GO
