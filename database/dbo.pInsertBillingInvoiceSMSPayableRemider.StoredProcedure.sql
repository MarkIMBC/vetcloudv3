﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertBillingInvoiceSMSPayableRemider](@IDs_BillingInvoice typIntList READONLY,
                                                  @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @SendStartDate Date = DateAdd(Day, 1, GETDATE())
      DECLARE @SendEndDate Date = DateAdd(YEAR, 1, GETDATE())
      DECLARE @DateList Table
        (
           DateSchedule DateTime
        )

      INSERT @DateList
      select Date
      FROM   dbo.fGetDatesByDateCoverage(@SendStartDate, @SendEndDate)
      WHERE  CONVERT(Date, Date) = @SendStartDate
              OR DATEPART(day, Date) = DATEPART(day, @SendStartDate)

      BEGIN TRY
          DECLARE @ID_Company INT
          DECLARE @DateSent DateTime
          DECLARE @ContactNumber VARCHAR(MAX)
          DECLARE @SMSMessage VARCHAR(MAX)
          DECLARE @ID_User INT = 0

          SELECT @ID_User = ID_User
          FROm   tUserSession
          where  ID = @ID_UserSession

          SELECT @SMSMessage = SMSFormatBillingInvoiceNotification
          FROM   tCompany_SMSSetting
          where  ID_Company = @ID_Company

          Update tBillingInvoice_SMSPayableRemider
          SET    IsActive = 0
          FROM   tBillingInvoice_SMSPayableRemider biSMSReminder
                 INNER JOIN @IDs_BillingInvoice bi
                         on biSMSReminder.ID_BillingInvoice = bi.ID

          INSERT INTO [dbo].[tBillingInvoice_SMSPayableRemider]
                      ([ID_Company],
                       ID_BillingInvoice,
                       ID_Client,
                       TotalAmount_BillingInvoice,
                       RemainingAmount_BillingInvoice,
                       DateSchedule,
                       [Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [SMSMessage],
                       [IsSentSMS],
                       [ContactNumber])
          SELECT biHed.ID_Company,
                 biHed.ID,
                 bihed.ID_Client,
                 ISNULL(biHed.TotalAmount, 0),
                 ISNULL(biHed.RemainingAmount, 0),
                 _dateSchedule.DateSchedule,
                 '',
                 1,
                 GETDATE(),
                 GETDATE(),
                 @ID_User,
                 @ID_User,
                 @SMSMessage,
                 0,
                 client.ContactNumber
          FROM   vBillingInvoice biHed
                 inner join tClient client
                         on biHed.ID_Client = client.ID
                 INNER JOIN @IDs_BillingInvoice biID
                         on biHed.ID = biID.ID,
                 @DateList _dateSchedule
          where  biHed.Payment_ID_FilingStatus IN ( 2, 11 )
      END TRY
      BEGIN CATCH
          SET @Success = 0;
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_'

      SELECT @Success Success,
             @message message
  END

GO
