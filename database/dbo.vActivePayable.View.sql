﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePayable] AS   
				SELECT   
				 H.*  
				FROM vPayable H  
				WHERE IsActive = 1
GO
