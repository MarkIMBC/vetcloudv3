﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoiceWalkIn] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoiceWalkIn H  
				WHERE ISNULL(IsActive, 0) = 0
GO
