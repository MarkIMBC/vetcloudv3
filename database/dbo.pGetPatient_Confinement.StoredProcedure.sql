﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPatient_Confinement] @ID              INT = -1,
                                           @ID_Client       INT = NULL,
                                           @ID_Patient      INT = NULL,
                                           @ID_Patient_SOAP INT = NULL,
                                           @ID_Session      INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Confinement_ItemsServices,
             '' AS Patient_Confinement_Patient

      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Employee INT
      DECLARE @ID_Company INT
      DECLARE @Confinement_ID_FilingStatus INT = 14

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session
	  
      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        BEGIN
            SET @ID_Patient = NULL
        END
      ELSE
        BEGIN
            SELECT @ID_Client = ID_Client
            FROm   tPatient
            WHERE  ID = @ID_Patient
        END

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF( ISNULL(@ID_Patient_SOAP, 0) <> 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   fs.Name      Name_FilingStatus
            FROM   (SELECT NULL                         AS [_],
                           -1                           AS [ID],
                           GetDate()                    AS [Date],
                           '-- NEW --'                  AS [Code],
                           NULL                         AS [Name],
                           1                            AS [IsActive],
                           @ID_Company                  AS [ID_Company],
                           NULL                         AS [Comment],
                           GetDate()                    AS [DateCreated],
                           GetDate()                    AS [DateModified],
                           NULL                         AS [ID_CreatedBy],
                           NULL                         AS [ID_LastModifiedBy],
                           @Confinement_ID_FilingStatus AS ID_FilingStatus,
                           @ID_Client                   AS ID_Client,
                           @ID_Patient                  AS ID_Patient,
                           @ID_Patient_SOAP             AS ID_Patient_SOAP) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            DECLARE @BillingInvoice TABLE
              (
                 ID_BillingInvoice          INT,
                 ID_Patient_Confinement     INT,
                 Code_BillingInvoice        VARCHAR(MAX),
                 Status_BillingInvoice      VARCHAR(MAX),
                 TotalAmount_BillingInvoice DECIMAL(18, 2)
              )

            INSERT @BillingInvoice
            SELECT TOP 1 biHed.ID          ID_BillingInvoice,
                         biHed.ID_Patient_Confinement,
                         biHed.Code        Code_BillingInvoice,
                         biHed.Status      Status_BillingInvoice,
                         biHed.TotalAmount TotalAmount_BillingInvoice
            FROM   vBillingInvoice biHed
            WHERE  biHed.ID_Patient_Confinement = @ID
                   AND biHed.ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )

            SELECT H.*,
                   biHed.*
            FROM   vPatient_Confinement H
                   LEFT JOIN @BillingInvoice biHed
                          ON H.ID = biHed.ID_Patient_Confinement
            WHERE  H.ID = @ID
        END

      IF ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapTreatment.ID DESC) ) - 799999 ID,
                   soap.Date,
                   soapTreatment.ID                                 ID_Patient_SOAP_Treatment,
                   soapTreatment.ID_Item,
                   soapTreatment.Name_Item,
                   ISNULL(soapTreatment.Quantity, 0)                Quantity,
                   ISNULL(soapTreatment.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(soapTreatment.UnitCost, item.UnitCost)    UnitCost,
                   soapTreatment.DateExpiration                     DateExpiration,
                   item.ID_ItemType
            FROM   dbo.vPatient_SOAP_Treatment soapTreatment
                   INNER JOIN vPatient_SOAP soap
                           ON soap.ID = soapTreatment.ID_Patient_SOAP
                   INNER JOIN tItem item
                           ON item.ID = soapTreatment.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
            UNION ALL
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999 ID,
                   soap.Date,
                   soapPrescription.ID                                 ID_Patient_SOAP_Prescription,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                Quantity,
                   ISNULL(soapPrescription.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(soapPrescription.UnitCost, item.UnitCost)    UnitCost,
                   soapPrescription.DateExpiration                     DateExpiration,
                   item.ID_ItemType
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN vPatient_SOAP soap
                           ON soap.ID = soapPrescription.ID_Patient_SOAP
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
                   AND soapPrescription.IsCharged = 1
        END
      ELSE
        BEGIN
            SELECT *
            FROM   vPatient_Confinement_ItemsServices
            WHERE  ID_Patient_Confinement = @ID
            ORDER  BY Date,
                      Name_Item
        END

      SELECT *
      FROM   vPatient_Confinement_Patient
      WHERE  ID_Patient_Confinement = @ID
  END 
GO
