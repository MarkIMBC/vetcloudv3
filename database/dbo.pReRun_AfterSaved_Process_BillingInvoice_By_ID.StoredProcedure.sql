﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     
 PROC [dbo].[pReRun_AfterSaved_Process_BillingInvoice_By_ID](@ID_BillingInvoice INT)  
AS  
  BEGIN  
      DECLARE @IDs_BillingInvoice typIntList  
  
   INSERT @IDs_BillingInvoice  
   VALUES (@ID_BillingInvoice)  
  
      exec pReRun_AfterSaved_Process_BillingInvoice_By_IDs  
        @IDs_BillingInvoice  
  END  
GO
