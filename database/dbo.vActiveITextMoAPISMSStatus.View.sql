﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveITextMoAPISMSStatus] AS   
				SELECT   
				 H.*  
				FROM vITextMoAPISMSStatus H  
				WHERE IsActive = 1
GO
