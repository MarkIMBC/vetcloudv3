﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSchedule] AS   
				SELECT   
				 H.*  
				FROM vSchedule H  
				WHERE ISNULL(IsActive, 0) = 0
GO
