﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePaymentTransaction] AS   
				SELECT   
				 H.*  
				FROM vPaymentTransaction H  
				WHERE ISNULL(IsActive, 0) = 0
GO
