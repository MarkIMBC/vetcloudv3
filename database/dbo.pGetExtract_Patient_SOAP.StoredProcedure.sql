﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetExtract_Patient_SOAP](@ID_Company INT)
AS
  BEGIN
      DECLARE @IDs_Patient_SOAP TABLE
        (
           ID INT
        )
      DECLARE @LabImage TABLE
        (
           ImageRowIndex   INT,
           RowIndex        INT,
           ImageNo         VARCHAR(MAX),
           FilePath        VARCHAR(MAX),
           Remark          VARCHAR(MAX),
           ID_Patient_SOAP INT
        );

      INSERT @IDs_Patient_SOAP
      SELECT DISTINCT _soap.ID
      FROM   tPatient_SOAP _soap
      WHERE  _soap.ID_Company = @ID_Company
      Order  by ID ASC

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark,
              ID_Patient_SOAP)
      SELECT distinct soapImage.ImageRowIndex,
                      soapImage.RowIndex,
                      soapImage.ImageNo,
                      soapImage.FilePath,
                      soapImage.Remark,
                      _soap.ID ID_Patient_SOAP
      FROM   tPatient_SOAP _soap
             CROSS APPLY dbo.fGetPatient_Soap_LaboratoryImages(_soap.ID) soapImage
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = _soap.ID

      SELECT '_',
             ''                             Patient_SOAP,
             'Patient_SOAP.ID_Patient_SOAP' Patient_SOAP_LaboratoryImage,
             'Patient_SOAP.ID_Patient_SOAP' Patient_SOAP_Plan,
             'Patient_SOAP.ID_Patient_SOAP' Patient_Wellness,
             'Patient_SOAP.ID_Patient_SOAP' Patient_SOAP_Treatment,
             'Patient_SOAP.ID_Patient_SOAP' Patient_SOAP_Prescription;

      SELECT '' MainHeader,
             '' DisplayValueForFilename

      SELECT _soap.*
      FROM   vPatient_SOAP _soap
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = _soap.ID
      Order  by _soap.ID ASC

      SELECT *
      FROM   @LabImage
      ORDER  BY ID_Patient_SOAP,
                ImageRowIndex DESC;

      SELECT soapPlan.*
      FROM   vPatient_SOAP_Plan soapPlan
             INNER JOIN tPatient_SOAP _soap
                     ON soapPlan.ID_Patient_SOAP = _soap.ID
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = _soap.ID

      SELECT wellness.ID,
             wellness.Code,
             wellness.Date,
             wellness.ID_Patient_SOAP,
             wellnessDetail.Name_Item,
             wellnessDetail.Comment
      FROm   vPatient_Wellness wellness
             INNER JOIN vPatient_Wellness_Detail wellnessDetail
                     on wellness.ID = wellnessDetail.ID_Patient_Wellness
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = wellness.ID_Patient_SOAP
      Order  BY wellness.Date

      SELECT soapTreatment.*
      FROM   vPatient_SOAP_Treatment soapTreatment
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = soapTreatment.ID_Patient_SOAP

      SELECT soapPlan.*
      FROM   vPatient_SOAP_Prescription soapPlan
             INNER JOIN @IDs_Patient_SOAP ids
                     ON ids.ID = soapPlan.ID_Patient_SOAP
  END 
GO
