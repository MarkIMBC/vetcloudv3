﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vModelProperyDetail]
AS
SELECT  P.Name ,
        MP.Name AS FieldKey,
        M.TableName,
		M.PrimaryKey,
		P.IsActive,
		p.IsAggregated
FROM    dbo.vModel_Property P
        LEFT JOIN dbo.[_vModel] M ON P.ID_PropertyModel = M.Oid
        LEFT JOIN dbo.[_tModel_Property] MP ON P.ID_ModelProperty_Key = MP.Oid
WHERE   P.ID_PropertyType = 10
GO
