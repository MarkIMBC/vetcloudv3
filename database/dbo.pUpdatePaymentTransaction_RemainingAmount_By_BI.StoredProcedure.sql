﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdatePaymentTransaction_RemainingAmount_By_BI] (@ID_BillingInvoice INT)
AS
  BEGIN
      Declare @Approved_ID_FilingStatus INT = 3
      Declare @PayableAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @PaymentHistory TABLE
        (
           RowID                 int identity(1, 1) primary key,
           ID_PaymentTransaction Int,
           Date                  DateTime,
           Code                  Varchar(20),
           Name_PaymentMethod    Varchar(200),
           PayableAmount         DECIMAL(18, 4),
           PaymentAmount         DECIMAL(18, 4),
           RemainingBalance      DECIMAL(18, 4)
        )
      Declare @IDs_Client typIntList

      SELECT @PayableAmount_BillingInvoice = bi.NetAmount
      FROM   tBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      INSERT @IDs_Client
      SELECT bi.ID_Client
      FROM   tBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      INSERT @PaymentHistory
             (ID_PaymentTransaction,
              Date,
              Code,
              Name_PaymentMethod,
              PaymentAmount,
              RemainingBalance)
      SELECT pt.ID,
             pt.Date,
             pt.Code,
             pt.Name_PaymentMethod,
             pt.PaymentAmount,
             0
      FROM   dbo.vPaymentTransaction pt
      WHERE  ID_BillingInvoice = @ID_BillingInvoice
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus )

      DECLARE @maxID   INT,
              @counter INT

      SET @counter = 1

      SELECT @maxID = COUNT(*)
      FROM   @PaymentHistory

      WHILE ( @counter <= @maxID )
        BEGIN
            Declare @PaymentAmount DECIMAL(18, 4) = 0
            Declare @PayableAmount DECIMAL(18, 4) = 0
            Declare @ChangeAmount DECIMAL(18, 4) = 0

            SELECT @PaymentAmount = PaymentAmount
            FROM   @PaymentHistory
            WHERE  RowID = @counter

            SET @PayableAmount = @PayableAmount_BillingInvoice
            SET @PayableAmount_BillingInvoice = @PayableAmount_BillingInvoice - @PaymentAmount

            IF @PayableAmount < @PaymentAmount
              BEGIN
                  SET @PayableAmount_BillingInvoice = 0
              END

            Update @PaymentHistory
            SET    PayableAmount = @PayableAmount,
                   RemainingBalance = @PayableAmount_BillingInvoice
            WHERE  RowID = @counter

            SET @counter = @counter + 1
        END

      UPDATE tPaymentTransaction
      SET    RemainingAmount = payHistory.RemainingBalance
      FROM   tPaymentTransaction pt
             INNER JOIN @PaymentHistory payHistory
                     on pt.ID = payHistory.ID_PaymentTransaction

      UPDATE dbo.tBillingInvoice
      SET    RemainingAmount = @PayableAmount_BillingInvoice
      WHERE  ID = @ID_BillingInvoice

      UPDATE dbo.tBillingInvoice
      SET    Payment_ID_FilingStatus = dbo.fGetPaymentStatus(NetAmount, RemainingAmount)
      WHERE  ID = @ID_BillingInvoice

      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_BillingInvoice

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement

      exec pUpdateClientTotalRemainingAmount
        @IDs_Client
  END

GO
