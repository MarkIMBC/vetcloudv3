﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetSOAPLANMessage] (@CompanyName        Varchar(MAX),
                                       @SOAPPlanSMSMessage VARCHAR(MAX),
                                       @Client             Varchar(MAX),
                                       @ContactNumber      Varchar(MAX),
                                       @Pet                Varchar(MAX),
                                       @Service            Varchar(MAX),
                                       @Reason             Varchar(MAX),
                                       @DateReturn         DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')
      Declare @TimeString Varchar(MAX) = FORMAT(@DateReturn, 'hh:mm tt')
      Declare @message Varchar(MAX) = @SOAPPlanSMSMessage

      if( @TimeString = '12:00 AM' )
        begin
            SET @TimeString = '09:00 AM*'
        END

      if( LEN(ISNULL(@Reason, '')) > 0 )
        SET @Reason = '- ' + @Reason

      SET @Pet = ISNULL(@Pet, 'your Pet')
      SET @Client = ISNULL(@Client, '')
      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Pet)))
      SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
      SET @message = REPLACE(@message, '/*Reason*/', LTRIM(RTRIM(@Reason)))
      SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))
      SET @message = REPLACE(@message, '/*TimeReturn*/', LTRIM(RTRIM(ISNULL(@TimeString, ''))))
      SET @message = REPLACE(@message, '"', '``')
      SET @message = REPLACE(@message, ' for for ', ' for ')

      RETURN @message
  END

GO
