﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetMessageThread]
(
    @Sender_ID_User INT,
    @Recipient_ID_User INT
)
AS
DECLARE @Succes BIT = 1;

UPDATE dbo.tMessageThread
SET IsRead = 1, DateRead = GETDATE()	
WHERE Sender_ID_User = @Sender_ID_User
      AND Recipient_ID_User = @Recipient_ID_User;

SELECT '',
       '' MessageThreads;

SELECT @Succes Success;

SELECT ID,
       Recipient_ID_User,
       Sender_ID_User,
       Message,
       DateSent,
       DateRead,
       IsRead
FROM tMessageThread
WHERE Sender_ID_User = @Sender_ID_User
      AND Recipient_ID_User = @Recipient_ID_User;
GO
