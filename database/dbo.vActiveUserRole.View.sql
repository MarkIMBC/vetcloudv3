﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUserRole] AS   
				SELECT   
				 H.*  
				FROM vUserRole H  
				WHERE IsActive = 1
GO
