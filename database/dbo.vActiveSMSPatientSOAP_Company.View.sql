﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSMSPatientSOAP_Company] AS   
				SELECT   
				 H.*  
				FROM vSMSPatientSOAP_Company H  
				WHERE IsActive = 1
GO
