﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,
                                    @ID_Client              INT = NULL,
                                    @ID_Patient             INT = NULL,
                                    @ID_SOAPType            INT = NULL,
                                    @ID_Patient_Confinement INT = NULL,
                                    @ID_Session             INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription,
             '' AS Patient_SOAP_Treatment;

      DECLARE @FilingStatus_Confined INT = 14
      DECLARE @FilingStatus_Discharged INT = 15
      DECLARE @ID_User INT;
      DECLARE @ID_Company INT;
      DECLARE @GUID_Company VARCHAR(MAX) = ''
      DECLARE @AttendingPhysician_ID_Employee INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @Consultation_ID_SOAPType INT = 1;
      DECLARE @Confinement_ID_SOAPType INT = 2;
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @PrimaryComplaintTemplate VARCHAR(MAX) = NULL
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = NULL
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = NULL
      DECLARE @AssessmentTemplate VARCHAR(MAX) = NULL
      DECLARE @DiagnosisTemplate VARCHAR(MAX) = NULL
      DECLARE @LaboratoryTemplate VARCHAR(MAX) = NULL

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      SELECT @ID_Company = ID_Company,
             @GUID_Company = c.Guid
      FROM   vUser u
             inner join tCompany c
                     on c.ID = u.ID_Company
      WHERE  u.ID = @ID_User

      SET @AttendingPhysician_ID_Employee = dbo.fGetModelDefaultValueInteger('tPatient_SOAP', @ID_Company, 'AttendingPhysician_ID_Employee')
      SET @PrimaryComplaintTemplate = dbo.fGetModelDefaultValueString('tPatient_SOAP', @ID_Company, 'PrimaryComplaintTemplate')
      SET @DiagnosisTemplate = dbo.fGetModelDefaultValueString('tPatient_SOAP', @ID_Company, 'DiagnosisTemplate')
      SET @LaboratoryTemplate = dbo.fGetModelDefaultValueString('tPatient_SOAP', @ID_Company, 'LaboratoryTemplate')

      /*Default Values if not existing */
      IF( @AttendingPhysician_ID_Employee IS NULL )
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID_Employee
            FROM   vUser _user
                   inner join vAttendingVeterinarian _attending
                           on _user.ID_Employee = _attending.ID
            WHERE  _user.ID = @ID_User
        END

      IF( @PrimaryComplaintTemplate IS NULL )
        BEGIN
            SET @PrimaryComplaintTemplate = ''
                                            + 'Eyes - Normal/Discharge/Infection/Sclelorosis/Inflamed/Eyelid tumor = '
                                            + Char(9) + Char(13)
                                            + 'Ears - Normal/Inflamed/Tumor/Dirty/Painful = '
                                            + Char(9) + Char(13)
                                            + 'Nose - Normal/Discharge = ' + Char(9)
                                            + Char(13)
                                            + 'Mouth,Teeth,Gums - Normal/Tumors/Gingivitis/Periodontitis/Tartar buildup/Loose teeth/Bite over/Under = '
                                            + Char(9) + Char(13)
                                            + 'Coat and skin - Normal/Scaly/Infection/Matted/Pruritus/Hair loss/Mass = '
                                            + Char(9) + Char(13)
                                            + 'Muskuloskeletal - Normal/Joint problems/Lameness/Nail Problems/Ligaments = '
                                            + Char(9) + Char(13)
                                            + 'Lungs - Normal/Breathing Difficulty/Rapid Respiration/ Tracheal Pinch + - / Congestion / Abn Sound = '
                                            + Char(9) + Char(13)
                                            + 'Heart - Normal/Murmur/Arrythmia/Muffled/Fast/Slow = '
                                            + Char(9) + Char(13)
                                            + 'GI System - Normal/Excessive Gas/Parasites/Abn Feces/Anorexia = '
                                            + Char(9) + Char(13)
                                            + 'Abdomen - Normal/Abnormal Mass/Tense/Painful/Bloated/Fluid/Hernia/Enlarged Organ = '
                                            + Char(9) + Char(13)
                                            + 'Urogentital - Normal/Abn Urination/Genital Discharge/Blood Seen/Abn Testicle = '
                                            + Char(9) + Char(13)
                                            + 'Neurological - Normal/Eye Reflex/Pain Reflex = '
                                            + Char(9) + Char(13)
                                            + 'Lymph Nodes - Normal/Submandubular/Prescapular/Axillary/Popiteal/Inguinal = '
                                            + Char(9) + Char(13)
        END

      IF( @ObjectiveTemplate IS NULL )
        BEGIN
            SET @ObjectiveTemplate = 'Heart Rate (bpm): ' + Char(9) + Char(13)
                                     + 'Respiratory Rate (brpm): ' + Char(9)
                                     + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
                                     + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
                                     + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
                                     + 'Lymph Nodes: ' + Char(9) + Char(13)
                                     + 'Palpebral Reflex: ' + Char(9) + Char(13)
                                     + 'Temperature: ' + Char(9) + Char(13)
        END

      IF( @ClinicalExaminationTemplate IS NULL )
        BEGIN
            SET @ClinicalExaminationTemplate = 'Heart Rate (bpm): ' + Char(9) + Char(13)
                                               + 'Respiratory Rate (brpm): ' + Char(9)
                                               + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
                                               + 'Temperature: ' + Char(9) + Char(13)
                                               + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
                                               + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
                                               + 'Lymph Nodes: ' + Char(9) + Char(13)
                                               + 'Palpebral Reflex: ' + Char(9) + Char(13)
        END

      IF( @AssessmentTemplate IS NULL )
        BEGIN
            SET @AssessmentTemplate ='Differential Diagnosis: ' + Char(9)
                                     + Char(13) + 'Notes: ' + Char(9) + Char(13)
                                     + 'Test Results: ' + Char(9) + Char(13)
                                     + 'Final Diagnosis: ' + Char(9) + Char(13)
                                     + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
                                     + Char(9) + Char(13) + 'Procedure Done : '
                                     + Char(9) + Char(13)
        END

      IF( @DiagnosisTemplate IS NULL )
        BEGIN
            SET @DiagnosisTemplate ='Differential Diagnosis: ' + Char(9)
                                    + Char(13) + 'Notes: ' + Char(9) + Char(13)
                                    + 'Test Results: ' + Char(9) + Char(13)
                                    + 'Final Diagnosis: ' + Char(9) + Char(13)
                                    + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
                                    + Char(9) + Char(13) + 'Procedure Done : '
                                    + Char(9) + Char(13)
        END

      IF( @LaboratoryTemplate IS NULL )
        BEGIN
            SET @LaboratoryTemplate = 'CBC: ' + Char(9) + Char(13) + '  Wbc= ' + Char(9)
                                      + Char(13) + '  Lym= ' + Char(9) + Char(13)
                                      + '  Mon= ' + Char(9) + Char(13) + '  Neu= ' + Char(9)
                                      + Char(13) + '  Eos= ' + Char(9) + Char(13)
                                      + '  Bas= ' + Char(9) + Char(13) + '  Rbc= ' + Char(9)
                                      + Char(13) + '  Hgb= ' + Char(9) + Char(13)
                                      + '  Hct= ' + Char(9) + Char(13) + '  Mcv= ' + Char(9)
                                      + Char(13) + '  Mch= ' + Char(9) + Char(13)
                                      + '  Mchc ' + Char(9) + Char(13) + '  Plt= ' + Char(9)
                                      + Char(13) + '  Mpv= ' + Char(9) + Char(13) + Char(9)
                                      + Char(13) + 'Blood Chem: ' + Char(9) + Char(13)
                                      + '  Alt= ' + Char(9) + Char(13) + '  Alp= ' + Char(9)
                                      + Char(13) + '  Alb= ' + Char(9) + Char(13)
                                      + '  Amy= ' + Char(9) + Char(13) + '  Tbil= '
                                      + Char(9) + Char(13) + '  Bun= ' + Char(9) + Char(13)
                                      + '  Crea= ' + Char(9) + Char(13) + '  Ca= ' + Char(9)
                                      + Char(13) + '  Phos= ' + Char(9) + Char(13)
                                      + '  Glu= ' + Char(9) + Char(13) + '  Na= ' + Char(9)
                                      + Char(13) + '  K= ' + Char(9) + Char(13) + '  TP= '
                                      + Char(9) + Char(13) + '  Glob= ' + Char(9) + Char(13)
                                      + Char(9) + Char(13) + 'Microscopic Exam: '
                                      + Char(9) + Char(13)
        END

      /* Patient Confinement */
      IF(SELECT Count(*)
         FROM   tPatient_Confinement
         WHERE  ID_Patient_SOAP = @ID
                AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )) > 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID
            FROM   tPatient_Confinement
            WHERE  ID_Patient_SOAP = @ID
                   AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )
        END

      IF( IsNull(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            DECLARE @confinedPatientCount INT = 0

            SET @ID_SOAPType = @Confinement_ID_SOAPType

            SELECT @ID_Client = hed.ID_Client,
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus
            FROM   tPatient_Confinement hed
            WHERE  hed.ID = @ID_Patient_Confinement

            SELECT @confinedPatientCount = COUNT(*)
            FROM   tPatient_Confinement_Patient confPatient
            WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
            GROUP  BY confPatient.ID_Patient_Confinement

            if( @confinedPatientCount = 1 )
              BEGIN
                  SELECT @ID_Patient = ID_Patient
                  FROM   tPatient_Confinement_Patient confPatient
                  WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
              END
        END

      /* Patient Confinement END*/
      IF IsNull(@ID_Patient, 0) <> 0
        BEGIN
            SELECT @IsDeceased = IsNull(IsDeceased, 0),
                   @ID_Client = ID_Client
            FROM   tPatient
            WHERE  ID = @ID_Patient;
        END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT ImageRowIndex,
             RowIndex,
             ImageNo,
             FilePath,
             Remark
      FROM   dbo.fGetPatient_Soap_LaboratoryImages(@ID)

      DECLARE @IsLastConfinmentSOAP BIT = 0
      DECLARE @maxDate DATETIME

      SELECT @maxDate = MaxDate
      FROM   vPatient_Confinement_MaxSOAP
      WHERE  ID_Patient_SOAP = @ID

      IF( @maxDate IS NOT NULL
          AND @Patient_Confinement_ID_FilingStatus = @FilingStatus_Discharged )
        SET @IsLastConfinmentSOAP = 1
      ELSE
        SET @IsLastConfinmentSOAP = 0

      IF ( @ID = -1 )
        BEGIN
            SET @ID_SOAPType = IsNull(@ID_SOAPType, @Consultation_ID_SOAPType)

            SELECT H.*,
                   patient.Name                               Name_Patient,
                   client.Name                                Name_Client,
                   fs.Name                                    Name_FilingStatus,
                   soapType.Name                              Name_SOAPType,
                   AttendingPhysician.Name                    AttendingPhysician_Name_Employee,
                   confinement.Code                           Code_Patient_Confinement,
                   confinement.Date                           Date_Patient_Confinement,
                   confinement.DateDischarge                  DateDischarge_Patient_Confinement,
                   CASE
                     WHEN @Patient_Confinement_ID_FilingStatus = 0 THEN confinement.ID_FilingStatus
                     ELSE @Patient_Confinement_ID_FilingStatus
                   END,
                   confinement.ID_FilingStatus                ID_FilingStatus_Patient_Confinement,
                   confinement.Name_FilingStatus              Name_FilingStatus_Patient_Confinement,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient,
                   ISNULL(patient.Comment, '')                Comment_Patient,
                   ISNULL(client.Comment, '')                 Comment_Client
            FROM   (SELECT NULL                                  AS [_],
                           CONVERT(VARCHAR(MAX), NEWID()) + '-'
                           + FORMAT(GETDATE(), 'yyyyMMddHHmmss') [GUID],
                           -1                                    AS [ID],
                           '-New-'                               AS [Code],
                           NULL                                  AS [Name],
                           1                                     AS [IsActive],
                           NULL                                  AS [ID_Company],
                           NULL                                  AS [Comment],
                           NULL                                  AS [DateCreated],
                           NULL                                  AS [DateModified],
                           @ID_User                              AS [ID_CreatedBy],
                           NULL                                  AS [ID_LastModifiedBy],
                           @ID_Client                            AS ID_Client,
                           @ID_Patient                           AS ID_Patient,
                           @AttendingPhysician_ID_Employee       AS AttendingPhysician_ID_Employee,
                           @ID_Patient_Confinement               AS ID_Patient_Confinement,
                           GetDate()                             Date,
                           @FILED_ID_FilingStatus                ID_FilingStatus,
                           @IsDeceased                           IsDeceased,
                           @ID_SOAPType                          ID_SOAPType,
                           @Patient_Confinement_ID_FilingStatus  Patient_Confinement_ID_FilingStatus,
                           @PrimaryComplaintTemplate             PrimaryComplaintTemplate,
                           @ObjectiveTemplate                    ObjectiveTemplate,
                           @AssessmentTemplate                   AssessmentTemplate,
                           @LaboratoryTemplate                   LaboratoryTemplate,
                           @ClinicalExaminationTemplate          ClinicalExaminationTemplate,
                           @DiagnosisTemplate                    DiagnosisTemplate,
                           @IsLastConfinmentSOAP                 IsLastConfinmentSOAP) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType
                   LEFT JOIN vPatient_Confinement confinement
                          ON confinement.ID = H.ID_Patient_Confinement
                   LEFT JOIN tEmployee AttendingPhysician
                          ON AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @Patient_Confinement_ID_FilingStatus       Patient_Confinement_ID_FilingStatus,
                   patient.IsDeceased,
                   @PrimaryComplaintTemplate                  PrimaryComplaintTemplate,
                   @ObjectiveTemplate                         ObjectiveTemplate,
                   @AssessmentTemplate                        AssessmentTemplate,
                   @LaboratoryTemplate                        LaboratoryTemplate,
                   @ClinicalExaminationTemplate               ClinicalExaminationTemplate,
                   @DiagnosisTemplate                         DiagnosisTemplate,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient,
                   @IsLastConfinmentSOAP                      IsLastConfinmentSOAP
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          ON h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID

      SELECT *
      FROM   dbo.vPatient_SOAP_Treatment
      WHERE  ID_Patient_SOAP = @ID
  END;

GO
