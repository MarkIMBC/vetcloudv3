﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vSchedule]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       schType.Name ScheduleType,
       e.Name Doctor,
       H.VacantCount - H.AccommodateCount AvailableCount,
       serviceType.Name Name_ServiceType,
       FORMAT(H.DateStart, 'yyyy-MM-dd') FormattedDateStart,
       FORMAT(H.DateEnd, 'yyyy-MM-dd') FormattedDateEnd
FROM tSchedule H
    LEFT JOIN tScheduleType schType
        ON H.ID_ScheduleType = schType.ID
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN tEmployee e
        ON e.ID = H.ID_Doctor
    LEFT JOIN tServiceType serviceType
        ON serviceType.ID = H.ID_ServiceType;

GO
