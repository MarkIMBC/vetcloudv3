﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEmployeePosition] AS   
				SELECT   
				 H.*  
				FROM vEmployeePosition H  
				WHERE IsActive = 1
GO
