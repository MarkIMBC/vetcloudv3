﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vPatient_Grooming]
AS
  SELECT H.*,
         UC.Name            AS CreatedBy,
         UM.Name            AS LastModifiedBy,
         _client.Name       Name_Client,
         _patient.Name      Name_Patient,
         _emp.Name          AttendingPhysician_Name_Employee,
         _filingStatus.Name Name_FilingStatus,
         bifs.Name          BillingInvoice_Name_FilingStatus
  FROM   tPatient_Grooming H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee _emp
                ON H.AttendingPhysician_ID_Employee = _emp.ID
         LEFT JOIN tClient _client
                ON H.ID_Client = _client.ID
         LEFT JOIN tPatient _patient
                ON H.ID_Patient = _patient.ID
         LEFT JOIN tFilingStatus _filingStatus
                ON H.ID_FilingStatus = _filingStatus.ID
         LEFT JOIN tFilingStatus bifs
                ON bifs.ID = H.BillingInvoice_ID_FilingStatus

GO
