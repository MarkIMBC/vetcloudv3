﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetReferenceLinkMenuItems](@ID_UserSession INT)
AS
  BEGIN
      SELECT '_' _,
             ''  MenuItems

      SELECT GETDATE() Date

      SELECT *
      FROM   tReferenceLink
  END

GO
