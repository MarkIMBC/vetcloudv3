﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClientAppointmentRequest_ListView]
AS
  SELECT ID,
         ID_Company,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         DateStart,
         DateEnd,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
		 AttendingPhysician_Name_Employee
  FROM   vClientAppointmentRequest
  WHERE  ID_FilingStatus NOT IN ( 4 )

GO
