﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUserGroup] AS   
				SELECT   
				 H.*  
				FROM vUserGroup H  
				WHERE ISNULL(IsActive, 0) = 0
GO
