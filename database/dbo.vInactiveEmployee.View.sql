﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEmployee] AS   
				SELECT   
				 H.*  
				FROM vEmployee H  
				WHERE ISNULL(IsActive, 0) = 0
GO
