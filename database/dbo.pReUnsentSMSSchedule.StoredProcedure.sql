﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pReUnsentSMSSchedule]
as
  BEGIN
      Update tPatient_Wellness_Schedule
      Set    IsSentSMS = 0
      FROM   tPatient_Wellness_Schedule _sched
             INNER JOIN tPatient_Wellness _h
                     on _h.ID = _sched.ID_Patient_Wellness
             INNER JOIN vCompanyActive c
                     on _h.ID_Company = c.ID
      Where  CONVERT(Date, _sched.Date) > CONVERT(Date, _sched.DateSent)
             AND CONVERT(Date, _sched.Date) >= CONVERT(Date, GETDATE())
             and _sched.IsSentSMS = 1
             AND ISNULL(ID_FilingStatus, 0) NOT IN ( 0, 4 )

      Update tPatient_SOAP_Plan
      Set    IsSentSMS = 0
      FROM   tPatient_SOAP_Plan _sched
             INNER JOIN tPatient_SOAP _h
                     on _h.ID = _sched.ID_Patient_SOAP
             INNER JOIN vCompanyActive c
                     on _h.ID_Company = c.ID
      Where  CONVERT(Date, _sched.DateReturn) > CONVERT(Date, _sched.DateSent)
             AND CONVERT(Date, _sched.DateReturn) >= CONVERT(Date, GETDATE())
             and _sched.IsSentSMS = 1
             AND ISNULL(ID_FilingStatus, 0) NOT IN ( 0, 4 )

      Update tPatientAppointment
      Set    IsSentSMS = 0
      FROM   tPatientAppointment _h
             INNER JOIN vCompanyActive c
                     on _h.ID_Company = c.ID
      Where  CONVERT(Date, _h.DateStart) > CONVERT(Date, _h.DateSent)
             AND CONVERT(Date, _h.DateStart) >= CONVERT(Date, GETDATE())
             and _h.IsSentSMS = 1
             AND ISNULL(ID_FilingStatus, 0) NOT IN ( 0, 4 )
  END

GO
