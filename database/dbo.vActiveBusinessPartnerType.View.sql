﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBusinessPartnerType] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartnerType H  
				WHERE IsActive = 1
GO
