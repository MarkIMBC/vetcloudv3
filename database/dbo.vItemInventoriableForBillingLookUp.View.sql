﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vItemInventoriableForBillingLookUp]
AS
  SELECT ID,
         Code,
         Name,
         ISNULL(UnitPrice, 0)                                  UnitPrice,
         ISNULL(UnitCost, 0)                                   UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company,
         CASE
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
           ELSE ''
         END                                                   RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays,
         OtherInfo_DateExpiration,
         ID_ItemType
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO
