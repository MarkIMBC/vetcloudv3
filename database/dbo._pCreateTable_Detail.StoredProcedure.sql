﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateTable_Detail]
    @TableName VARCHAR(500)
AS
    BEGIN
        EXEC 
        (
        'CREATE TABLE ['+@TableName+'] (
			[ID] [int] IDENTITY (1, 1) NOT NULL ,
			[Code] [varchar] (50),
			[Name] [varchar] (200),
			[IsActive] [bit] NULL DEFAULT (1),
			[Comment] [varchar] (MAX) NULL,
        CONSTRAINT [PK_'+@TableName+'] PRIMARY KEY  CLUSTERED 
        (
        [ID]
        )  ON [PRIMARY] ,
        ) ON [PRIMARY]

        CREATE  INDEX [IX_'+@TableName+'] ON [dbo].['+@TableName+']([Name]) ON [PRIMARY]'
        )

    END
GO
