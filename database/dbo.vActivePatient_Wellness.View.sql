﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Wellness] AS   
				SELECT   
				 H.*  
				FROM vPatient_Wellness H  
				WHERE IsActive = 1
GO
