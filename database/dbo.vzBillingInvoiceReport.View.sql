﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzBillingInvoiceReport]
AS
  SELECT biHed.ID,
         biHed.Code,
         bihed.CustomCode,
         biHed.Date,
         biHed.Name_Patient,
         biHed.BillingAddress,
         biHed.Name_FilingStatus,
         biHed.CreatedBy_Name_User,
         biHed.ApprovedBy_Name_User,
         biHed.Name_TaxScheme,
         biHed.DateApproved,
         biHed.SubTotal,
         ISNULL(biHed.TotalItemDiscountAmount, 0) TotalItemDiscountAmount,
         biHed.DiscountRate,
         biHed.DiscountAmount,
         biHed.TotalAmount,
         biHed.GrossAmount,
         biHed.VatAmount,
         biHed.NetAmount,
         biDetail.ID                              ID_BillingInvoice_Detail,
         biDetail.Name_Item,
         biDetail.Quantity,
         biDetail.UnitPrice,
         biDetail.Amount,
         biHed.ID_Company,
         ISNULL(biHed.Comment, '')                Comment,
         company.ImageLogoLocationFilenamePath,
         company.Name                             Name_Company,
         company.Address                          Address_Company
  FROM   dbo.vBillingInvoice biHed
         LEFT JOIN dbo.vBillingInvoice_Detail biDetail
                ON biHed.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company

GO
