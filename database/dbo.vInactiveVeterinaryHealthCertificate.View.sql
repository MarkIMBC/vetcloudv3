﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveVeterinaryHealthCertificate] AS   
				SELECT   
				 H.*  
				FROM vVeterinaryHealthCertificate H  
				WHERE ISNULL(IsActive, 0) = 0
GO
