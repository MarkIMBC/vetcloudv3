﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetExtract_SendSOAPPlan](@Date             DATETIME)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @record_unsent TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )
      DECLARE @record_sent TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )
      DECLARE @CompanySMSSetting TABLE
        (
           ID_Company        INT,
           MaxSMSCountPerDay INT
        )

      INSERT @CompanySMSSetting
      SELECT ID_Company,
             Max(MaxSMSCountPerDay)
      FROM   tCompany_SMSSetting
      WHERE  IsActive = 1
      GROUP  BY ID_Company

      INSERT @record_unsent
      SELECT *
      FROM   dbo.[fGetSendSoapPlan](@Date, 0, '')

      INSERT @record_sent
      SELECT *
      FROM   dbo.[fGetSendSoapPlan](@Date, 1, '')

      SELECT '_',
             ''             AS Main,
             'Main.ID_Main' AS UnsentSummary,
             'Main.ID_Main' AS UnsentDetailed,
             'Main.ID_Main' AS SentSummary,
             'Main.ID_Main' AS SentDetailed;

      SELECT @Success Success;

      --Main  
      SELECT 1                           ID,
             Format(@Date, 'yyyy-MM-dd') DateSendingString

      --UnsentSummary  
      SELECT 1        ID_Main,
             DateSending,
             tbl.Name_Company,
             Count(*) Count,
             Sum(CASE
                   WHEN len(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN len(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN len(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END) TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record_unsent) tbl
      GROUP  BY DateSending,
                Name_Company
      ORDER  BY DateSending DESC,
                Name_Company

      --UnsentDetaied  
      SELECT 1            ID_Main,
             DateSending,
             rec.*,
             CASE
               WHEN len(Message) <= 160 THEN 1
               ELSE
                 CASE
                   WHEN len(Message) <= 306 THEN 2
                   ELSE
                     CASE
                       WHEN len(Message) <= 459 THEN 3
                       ELSE 4
                     END
                 END
             END          ConsumedSMSCredit,
             len(Message) CharLength
      FROM   @record_unsent rec
             INNER JOIN tCompany com
                     ON com.ID = rec.ID_Company
             INNER JOIN @CompanySMSSetting comSmsSetting
                     ON comSmsSetting.ID_Company = com.ID
      ORDER  BY rec.DateSending DESC,
                Name_Company,
                Name_Client

      --SentSummary  
      SELECT 1        ID_Main,
             DateSending,
             tbl.Name_Company,
             Count(*) Count,
             Sum(CASE
                   WHEN len(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN len(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN len(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END) TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record_sent) tbl
      GROUP  BY DateSending,
                Name_Company
      ORDER  BY DateSending DESC,
                Name_Company

      --SentDetaied  
      SELECT 1            ID_Main,
             DateSending,
             rec.*,
             CASE
               WHEN len(Message) <= 160 THEN 1
               ELSE
                 CASE
                   WHEN len(Message) <= 306 THEN 2
                   ELSE
                     CASE
                       WHEN len(Message) <= 459 THEN 3
                       ELSE 4
                     END
                 END
             END          ConsumedSMSCredit,
             len(Message) CharLength
      FROM   @record_sent rec
             INNER JOIN tCompany com
                     ON com.ID = rec.ID_Company
             INNER JOIN @CompanySMSSetting comSmsSetting
                     ON comSmsSetting.ID_Company = com.ID
      ORDER  BY rec.DateSending DESC,
                Name_Company,
                Name_Client
  END 


GO
