﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CReate 
 PROC [dbo].[_tempGeneratePatient](@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_Patient TABLE
        (
           RowID      INT,
           ID_Patient INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_Patient
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tPatient
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_Patient

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   vCompanyActive
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_Patient INT = 0

            SELECT @ID_Patient = ID_Patient
            FROM   @IDs_Patient
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_Patient]
              @ID_Patient,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO
