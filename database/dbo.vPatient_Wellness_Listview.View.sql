﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Wellness_Listview]
AS
  SELECT wellness.ID,
         wellness.Date,
         wellness.Code,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         ID_Company,
         ID_Patient_SOAP,
         AttendingPhysician_Name_Employee,
         Weight,
         Temperature
  FROM   vPatient_Wellness wellness
  --WHERE  ID_FilingStatus NOT IN ( 4 )

GO
