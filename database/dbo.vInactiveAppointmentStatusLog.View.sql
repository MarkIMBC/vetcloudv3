﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAppointmentStatusLog] AS   
				SELECT   
				 H.*  
				FROM vAppointmentStatusLog H  
				WHERE ISNULL(IsActive, 0) = 0
GO
