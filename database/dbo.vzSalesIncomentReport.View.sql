﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE      
 VIEW [dbo].[vzSalesIncomentReport]  
AS  
  SELECT biHed.ID_Company,  
         company.ImageLogoLocationFilenamePath,  
         company.Name                                                                     Name_Company,  
         company.Address                                                                  Address_Company,  
         CASE  
           WHEN len(company.Address) > 0 THEN '' + company.Address  
           ELSE ''  
         END  
         + CASE  
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber  
             ELSE ''  
           END  
         + CASE  
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email  
             ELSE ''  
           END                                                                            HeaderInfo_Company,  
         ID_Client,  
         lTrim(rTrim(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName)))                Name_Client,  
         ID_Item,  
         lTrim(rTrim(Name_Item))                                                          Name_Item,  
         CONVERT(DATE, biHed.Date)                                                        Date_BillingInvoice,  
         Sum(Quantity)                                                                    TotalQuantity,  
         ISNULL(biDetail.UnitCost, 0)                                                     UnitCost,  
         ISNULL(biDetail.UnitPrice, 0)                                                    UnitPrice,  
         ISNULL(biDetail.UnitPrice, 0) * Sum(Quantity)                                    GrossSales,  
         ( ISNULL(biDetail.UnitPrice, 0) - ISNULL(biDetail.UnitCost, 0) ) * Sum(Quantity) NetCost,  
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')                                 AttendingPhysician_ID_Employee,  
         ISNULL(biHed.Name, '(No Assigned)')                                              AttendingPhysician_Name_Employee,  
         item.ID_ItemType  
  FROM   vBillingInvoice biHed  
         INNER JOIN vCompany company  
                 ON company.ID = biHed.ID_Company  
         INNER JOIN vBillingInvoice_Detail biDetail  
                 ON biHed.ID = biDetail.ID_BillingInvoice  
         INNER JOIN tItem item  
                 on biDetail.ID_Item = item.ID  
  WHERE  biHed.ID_FilingStatus IN ( 3 )  
  GROUP  BY biHed.ID_Company,  
            company.ImageLogoLocationFilenamePath,  
            company.Name,  
            company.Address,  
            company.ContactNumber,  
            company.Address,  
            company.Email,  
            CONVERT(DATE, biHed.Date),  
            ID_Client,  
            biHed.Name_Client,  
            biHed.WalkInCustomerName,  
            ID_Item,  
            Name_Item,  
            biDetail.UnitCost,  
            biDetail.UnitPrice,  
            biHed.AttendingPhysician_ID_Employee,  
            biHed.Name,  
            item.ID_ItemType  
GO
