﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDocumentSeries] AS   
				SELECT   
				 H.*  
				FROM vDocumentSeries H  
				WHERE ISNULL(IsActive, 0) = 0
GO
