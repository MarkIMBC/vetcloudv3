﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertcustomReportTitle](@ID_Company        INT,
                                    @Oid_Report        VARCHAR(MAX),
                                    @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_CompanyCustomReportTitle INT = 0

	  IF @ID_Company IS NULL return;

      IF(SELECT COUNT(*)
         FROM   dbo.tCompanyCustomReportTitle
         WHERE  Oid_Report = @Oid_Report
                AND ID_Company = @ID_Company) = 0
        BEGIN
            INSERT INTO [dbo].[tCompanyCustomReportTitle]
                        ([Name],
                         [IsActive],
                         [Oid_Report],
                         ID_Company)
            VALUES      (@customReportTitle,
                         1,
                         @Oid_Report,
                         @ID_Company)
        END

      SELECT @ID_CompanyCustomReportTitle = ID
      FROM   dbo.tCompanyCustomReportTitle
      WHERE  Oid_Report = @Oid_Report
             AND ID_Company = @ID_Company

      UPDATE dbo.tCompanyCustomReportTitle
      SET    Name = @customReportTitle
      WHERE  ID = @ID_CompanyCustomReportTitle
  END

GO
