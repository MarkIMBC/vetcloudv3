﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompanyInfo] AS   
				SELECT   
				 H.*  
				FROM vCompanyInfo H  
				WHERE ISNULL(IsActive, 0) = 0
GO
