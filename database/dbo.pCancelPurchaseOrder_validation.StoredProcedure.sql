﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

      
CREATE PROC [dbo].[pCancelPurchaseOrder_validation]    
(    
    @IDs_PurchaseOrder typIntList READONLY,    
    @ID_UserSession INT    
)    
AS    
BEGIN    
    
    DECLARE @Filed_ID_FilingStatus INT = 1;    
    DECLARE @Approved_ID_FilingStatus INT = 3;    

	DECLARE @Pending_ID_FilingStatus INT = 2
	DECLARE @PartiallyServed_ID_FilingStatus INT = 5
	DECLARE @FullyServed_ID_FilingStatus INT = 6
	DECLARE @OverServed_ID_FilingStatus INT = 7

    DECLARE @message VARCHAR(400) = '';    
    
    DECLARE @ValidateNotApproved TABLE    
    (    
        Code VARCHAR(30),    
        Name_FilingStatus VARCHAR(30)    
    );    
    DECLARE @Count_ValidateNotApproved INT = 0;  

	DECLARE @ValidateOnServe TABLE    
    (    
        Code VARCHAR(30),    
        Name_FilingStatus VARCHAR(30)    
    );    
    DECLARE @Count_ValidateOnServe INT = 0; 
    
    /* Validate Billing Invoices Status is not Approved*/    
    INSERT @ValidateNotApproved    
    (    
        Code,    
        Name_FilingStatus    
    )    
    SELECT Code,    
           Name_FilingStatus    
    FROM dbo.vPurchaseOrder bi    
    WHERE EXISTS    
    (    
        SELECT ID FROM @IDs_PurchaseOrder ids WHERE ids.ID = bi.ID    
    )    
          AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus );    
    
    SELECT @Count_ValidateNotApproved = COUNT(*)    
    FROM @ValidateNotApproved;    
    
    IF (@Count_ValidateNotApproved > 0)    
    BEGIN    
    
        SET @message = 'The following record' + CASE    
                                                    WHEN @Count_ValidateNotApproved > 1 THEN    
                                                        's are'    
                                                    ELSE    
                                                        ' is '    
                                                END + 'not allowed to cancel:';    
    
        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus    
        FROM @ValidateNotApproved;    
        THROW 50001, @message, 1;    
    END; 

	 /*Validate Purchase Order is on served*/ 
	INSERT @ValidateOnServe    
    (    
        Code,    
        Name_FilingStatus    
    )    
    SELECT Code,    
           ServingStatus_Name_FilingStatus    
    FROM dbo.vPurchaseOrder poHed
	INNER JOIN @IDs_PurchaseOrder ids
		ON ids.ID = poHed.ID
	WHERE
		poHed.ServingStatus_ID_FilingStatus IN ( @PartiallyServed_ID_FilingStatus, 
												 @FullyServed_ID_FilingStatus, 
												 @OverServed_ID_FilingStatus)

	SELECT @Count_ValidateOnServe = COUNT(*)    
    FROM @ValidateOnServe;    

	IF (@Count_ValidateOnServe > 0)    
    BEGIN    
    
        SET @message = 'The following record' + CASE    
                                                    WHEN @Count_ValidateOnServe > 1 THEN    
                                                        's are'    
                                                    ELSE    
                                                        ' is '    
                                                END + 'not allowed to cancel:';    
    
        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus    
        FROM @ValidateOnServe;    
        THROW 50001, @message, 1;    
    END;
END;    
 
GO
