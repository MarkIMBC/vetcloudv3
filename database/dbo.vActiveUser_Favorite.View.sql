﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUser_Favorite] AS   
				SELECT   
				 H.*  
				FROM vUser_Favorite H  
				WHERE IsActive = 1
GO
