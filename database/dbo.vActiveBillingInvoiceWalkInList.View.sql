﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoiceWalkInList] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoiceWalkInList H  
				WHERE IsActive = 1
GO
