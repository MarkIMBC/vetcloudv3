﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveItem_UnitCostLog] AS   
				SELECT   
				 H.*  
				FROM vItem_UnitCostLog H  
				WHERE IsActive = 1
GO
