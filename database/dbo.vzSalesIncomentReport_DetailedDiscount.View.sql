﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE           
 VIEW [dbo].[vzSalesIncomentReport_DetailedDiscount]  
as  
  SELECT biHed.ID             ID_BillingInvoice,  
         biHed.ID_Company     ID_Company,  
         biHed.Date           Date_BillingInvoice,  
         bihed.Code           Code_BillingInvoice,  
         biHed.DiscountAmount DiscountAmount,  
         ''                   ItemString,  
         0                    ID_BillingInvoice_Detail,  
         0                    ID_ItemType  
  FROM   vBillingInvoice biHed  
         INNER JOIN vCompanyActive company  
                 on biHed.ID_Company = company.ID  
  WHERE  biHed.ID_FilingStatus IN ( 3 )  
         AND ISNULL(biHed.DiscountAmount, 0) > 0  
  UNION ALL  
  SELECT biHed.ID                ID_BillingInvoice,  
         biHed.ID_Company,  
         biHed.Date              Date_BillingInvoice,  
         bihed.Code              Code_BillingInvoice,  
         biDetail.DiscountAmount DiscountAmount,  
         biDetail.Name_Item      ItemString,  
         bidetail.ID             ID_BillingInvoice_Detail,  
         item.ID_ItemType  
  FROM   vBillingInvoice biHed  
         INNER JOIN vCompanyActive company  
                 on biHed.ID_Company = company.ID  
         INNER JOIN vBillingInvoice_Detail bidetail  
                 on biHed.ID = biDetail.ID_BillingInvoice  
         INNER JOIN tItem item  
                 on biDetail.ID_Item = item.ID  
  WHERE  biHed.ID_FilingStatus IN ( 3 )  
         AND ISNULL(bidetail.DiscountAmount, 0) > 0  
  
GO
