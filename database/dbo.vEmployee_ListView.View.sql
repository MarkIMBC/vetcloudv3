﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 view [dbo].[vEmployee_ListView]
AS
  SELECT ID,
         Name,
         Name_Position,
		 ID_Company, ContactNumber, 
		 FullAddress
  FROM   vEmployee

GO
