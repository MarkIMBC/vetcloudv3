﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClientAppointmentRequest_Patient]
AS
  SELECT H.*,
         patient.Name Name_Patient,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy
  FROM   tClientAppointmentRequest_Patient H
         INNER JOIN tPatient patient
                 ON H.ID_Patient = patient.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO
