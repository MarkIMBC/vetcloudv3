﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveToothStatus] AS   
				SELECT   
				 H.*  
				FROM vToothStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
