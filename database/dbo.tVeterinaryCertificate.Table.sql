﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tVeterinaryCertificate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[PatientDateBirth] [datetime] NULL,
	[DestinationAddress] [varchar](300) NULL,
	[DateVaccinated] [datetime] NULL,
	[ID_Item] [int] NULL,
	[SerialNumber] [varchar](300) NULL,
	[LotNumber] [varchar](300) NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
 CONSTRAINT [PK_tVeterinaryCertificate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tVeterinaryCertificate] ON [dbo].[tVeterinaryCertificate]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tVeterinaryCertificate]  WITH CHECK ADD  CONSTRAINT [FK_tVeterinaryCertificate_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] CHECK CONSTRAINT [FK_tVeterinaryCertificate_ID_Company]
GO
ALTER TABLE [dbo].[tVeterinaryCertificate]  WITH CHECK ADD  CONSTRAINT [FK_tVeterinaryCertificate_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] CHECK CONSTRAINT [FK_tVeterinaryCertificate_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tVeterinaryCertificate]  WITH CHECK ADD  CONSTRAINT [FK_tVeterinaryCertificate_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] CHECK CONSTRAINT [FK_tVeterinaryCertificate_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tVeterinaryCertificate]
		ON [dbo].[tVeterinaryCertificate]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tVeterinaryCertificate
			SET    DateCreated = GETDATE()
			FROM   dbo.tVeterinaryCertificate hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] ENABLE TRIGGER [rDateCreated_tVeterinaryCertificate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tVeterinaryCertificate]
		ON [dbo].[tVeterinaryCertificate]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tVeterinaryCertificate
			SET    DateModified = GETDATE()
			FROM   dbo.tVeterinaryCertificate hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tVeterinaryCertificate] ENABLE TRIGGER [rDateModified_tVeterinaryCertificate]
GO
