﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 VIEW [dbo].[vpivotBilledPatientConfinement]
AS
  SELECT *
  FROM   (SELECT ID_Patient_Confinement,
                 BillingInvoice_RowID,
                 Info_BillingInvoice
          FROM   (SELECT bi.ID_Patient_Confinement,
                         ROW_NUMBER()
                           OVER(
                             PARTITION BY bi.ID_Patient_Confinement
                             ORDER BY ID_Patient_Confinement ASC, bi.ID)      BillingInvoice_RowID,
                         format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                  FROM   vBillingINvoice bi
                         inner JOIN tPatient_Confinement soap
                                 ON bi.ID_Patient_Confinement = soap.ID
                  where  ISNULL(bi.ID_Patient_Confinement, 0) <> 0
                         AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
         PIVOT(MAX([Info_BillingInvoice])
              FOR [BillingInvoice_RowID] IN([1],
                                            [2],
                                            [3],
                                            [4],
                                            [5],
                                            [6],
                                            [7],
                                            [8],
                                            [9],
                                            [10] )) AS PivotTable;

GO
