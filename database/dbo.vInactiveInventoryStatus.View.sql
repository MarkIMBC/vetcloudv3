﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveInventoryStatus] AS   
				SELECT   
				 H.*  
				FROM vInventoryStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
