﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGenerateDocumentSeries]
(@ID_Model UNIQUEIDENTIFIER)
AS
BEGIN
    DECLARE @ID_DocumentSeries INT = 0;
    DECLARE @Prefix VARCHAR(50);
    DECLARE @IsAppendCurrentDate BIT = 0;
    DECLARE @DigitCount INT = 0;
    DECLARE @Counter INT = 0;
    DECLARE @GenerateDocSeries VARCHAR(20) = '';

    SELECT @ID_DocumentSeries = ID
    FROM dbo.tDocumentSeries ds
    WHERE ds.ID_Model = @ID_Model;

    SELECT @Prefix = Prefix,
           @IsAppendCurrentDate = ISNULL(IsAppendCurrentDate, 0),
           @DigitCount = DigitCount,
           @Counter = Counter
    FROM tDocumentSeries
    WHERE ID = @ID_DocumentSeries;

    SET @GenerateDocSeries = @Prefix + '-' + REPLICATE('0', 5 - LEN(@Counter)) + CONVERT(VARCHAR(50), @Counter);

    SELECT @GenerateDocSeries;
END;
GO
