﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGet_Patient_SOAP_RecordInfo_Plan](@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @___detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @___detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT _plan.ID_Patient_SOAP,
             TagString = STUFF((SELECT '' + 'Date Return: '
                                       + FORMAT(DateReturn, 'MM/dd/yyyy (ddd) ')
                                       + '/*break*/ '
                                       + CASE
                                           WHEN LEN(ISNULL(__plan.Comment, '')) > 0 THEN __plan.Comment
                                           ELSE item.Name
                                         END
                                       + ' ' + '/*break*/ ' + '/*break*/ '
                                FROM   tPatient_SOAP_Plan __plan WITH (NOLOCK)
                                       INNER JOIN tItem item
                                               on __plan.ID_Item = item.ID
                                WHERE  __plan.ID_Patient_SOAP = _plan.ID_Patient_SOAP
                                Order  by DateReturn DESC
                                FOR XML PATH ('')), 1, 0, ''),
             COUNT(*)  Count
      FROM   tPatient_SOAP_Plan _plan WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _plan.ID_Patient_SOAP = ids.ID
      GROUP  BY _plan.ID_Patient_SOAP

      Update @detail
      set    TagString = det2.TagString,
             Count = det2.Count
      FROM   @detail det1
             INNER JOIN @___detail det2
                     on det1.ID_Patient_SOAP = det2.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO
