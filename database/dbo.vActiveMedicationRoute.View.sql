﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveMedicationRoute] AS   
				SELECT   
				 H.*  
				FROM vMedicationRoute H  
				WHERE IsActive = 1
GO
