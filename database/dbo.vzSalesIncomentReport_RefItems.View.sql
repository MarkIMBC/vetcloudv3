﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE    
 VIEW [dbo].[vzSalesIncomentReport_RefItems]  
as  
  SELECT biHed.ID_Company,  
         biHed.Date           Date_BillingInvoice,  
   bihed.Code Code_BillingInvoice,  
         AttendingPhysician_ID_Employee,  
         AttendingPhysician_Name_Employee,  
         ID_Client,  
         ISNULL(Name_Client, WalkInCustomerName) Customer,  
         ID_Item,  
         biDetail. Name_Item,  
         ISNULL(biHed.DiscountAmount, 0) + ISNULL(biHed.TotalItemDiscountAmount, 0) DiscountAmount_BillingInvoice  
  FROM   vBillingInvoice biHed  
         inner join vBillingInvoice_Detail biDetail  
                 on biHed.ID = biDetail.ID_BillingInvoice  
  WHERE  biHed.ID_FilingStatus IN ( 3 )  
  
GO
