﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Wellness_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient_Wellness] [int] NULL,
	[ID_Item] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[DateExpiration] [datetime] NULL,
	[tPatient_Vaccination_Detail] [int] NULL,
	[ID_Patient_Vaccination_Detail] [int] NULL,
	[Quantity] [int] NULL,
	[CustomItem] [varchar](300) NULL,
	[Appointment_ID_FilingStatus] [int] NULL,
	[Appointment_CancellationRemarks] [varchar](300) NULL,
 CONSTRAINT [PK_tPatient_Wellness_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Wellness_Detail] ON [dbo].[tPatient_Wellness_Detail]
(
	[ID_Patient_Wellness] ASC,
	[ID_Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] ADD  DEFAULT ((2)) FOR [Appointment_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Detail_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] CHECK CONSTRAINT [FK_tPatient_Wellness_Detail_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] CHECK CONSTRAINT [FK_tPatient_Wellness_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] CHECK CONSTRAINT [FK_tPatient_Wellness_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail]  WITH CHECK ADD  CONSTRAINT [FKtPatient_Wellness_Detail_ID_Patient_Wellness] FOREIGN KEY([ID_Patient_Wellness])
REFERENCES [dbo].[tPatient_Wellness] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] CHECK CONSTRAINT [FKtPatient_Wellness_Detail_ID_Patient_Wellness]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Wellness_Detail]
		ON [dbo].[tPatient_Wellness_Detail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness_Detail
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Wellness_Detail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] ENABLE TRIGGER [rDateCreated_tPatient_Wellness_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Wellness_Detail]
		ON [dbo].[tPatient_Wellness_Detail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness_Detail
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Wellness_Detail hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness_Detail] ENABLE TRIGGER [rDateModified_tPatient_Wellness_Detail]
GO
