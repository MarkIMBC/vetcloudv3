﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompanyCustomReportTitle] AS   
    SELECT   
     H.*  
    FROM vCompanyCustomReportTitle H  
    WHERE ISNULL(IsActive, 0) = 1
GO
