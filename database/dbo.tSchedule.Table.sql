﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSchedule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ScheduleType] [int] NULL,
	[DateStart] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[AccommodateCount] [int] NULL,
	[Count] [int] NULL,
	[ID_Doctor] [int] NULL,
	[VacantCount] [int] NULL,
	[ID_ServiceType] [int] NULL,
 CONSTRAINT [PK_tSchedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tSchedule] ADD  CONSTRAINT [DF__tSchedule__IsAct__0C90CB45]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tSchedule]  WITH NOCHECK ADD  CONSTRAINT [FK_tSchedule_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tSchedule] CHECK CONSTRAINT [FK_tSchedule_ID_Company]
GO
ALTER TABLE [dbo].[tSchedule]  WITH NOCHECK ADD  CONSTRAINT [FK_tSchedule_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSchedule] CHECK CONSTRAINT [FK_tSchedule_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tSchedule]  WITH NOCHECK ADD  CONSTRAINT [FK_tSchedule_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSchedule] CHECK CONSTRAINT [FK_tSchedule_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tSchedule]
		ON [dbo].[tSchedule]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSchedule
			SET    DateCreated = GETDATE()
			FROM   dbo.tSchedule hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSchedule] ENABLE TRIGGER [rDateCreated_tSchedule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tSchedule]
		ON [dbo].[tSchedule]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSchedule
			SET    DateModified = GETDATE()
			FROM   dbo.tSchedule hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSchedule] ENABLE TRIGGER [rDateModified_tSchedule]
GO
