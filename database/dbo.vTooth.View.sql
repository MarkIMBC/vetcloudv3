﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vTooth]
AS
SELECT t.ID,
       t.Code,
       t.Name,
       t.IsActive,
       t.Comment,
       t.ToothNumber,
       t.Location,
       t.Top_ID_ToothSurface,
       t.Left_ID_ToothSurface,
       t.Middle_ID_ToothSurface,
       t.Bottom_ID_ToothSurface,
       t.Right_ID_ToothSurface,
       tsTop.Name Top_Name_ToothSurface,
       tsLeft.Name Left_Name_ToothSurface,
       tsMiddle.Name Middle_Name_ToothSurface,
       tsBottom.Name Bottom_Name_ToothSurface,
       tsRight.Name Right_Name_ToothSurface,
       t.ID_Dentition,
	   d.Name Name_Dentition,
	   t.ID_TeethQuandrant
FROM tTooth t
    LEFT JOIN tToothSurface tsTop
        ON tsTop.ID = t.Top_ID_ToothSurface
    LEFT JOIN tToothSurface tsLeft
        ON tsLeft.ID = t.Left_ID_ToothSurface
    LEFT JOIN tToothSurface tsMiddle
        ON tsMiddle.ID = t.Middle_ID_ToothSurface
    LEFT JOIN tToothSurface tsBottom
        ON tsBottom.ID = t.Bottom_ID_ToothSurface
    LEFT JOIN tToothSurface tsRight
        ON tsRight.ID = t.Right_ID_ToothSurface
    LEFT JOIN tDentition d
        ON d.ID = t.ID_Dentition;
GO
