﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetPurchaseOrder]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' AS PurchaseOrder_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;

    DECLARE @ID_Company INT;
    DECLARE @ID_Employee INT;

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    SELECT @ID_Employee = ID_Employee,
           @ID_Company = emp.ID_Company
    FROM dbo.tUser _user
        INNER JOIN dbo.tEmployee emp
            ON emp.ID = _user.ID_Employee
    WHERE _user.ID = @ID_User;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               status.Name FilingStatus
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '-NEW-' AS [Code],
                   NULL AS [Name],
                   GETDATE() AS [DocumentDate],
                   GETDATE() AS Date,
                   1 AS [IsActive],
                   @ID_Company AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
				   0 IsComputeDiscountRate
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN tFilingStatus status
                ON status.ID = H.ID_FilingStatus;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM vPurchaseOrder H
        WHERE H.ID = @ID;
    END;


    SELECT *
    FROM vPurchaseOrder_Detail detail
    WHERE detail.ID_PurchaseOrder = @ID;
END;

GO
