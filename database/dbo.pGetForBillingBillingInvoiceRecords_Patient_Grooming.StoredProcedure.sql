﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE    
 PROC [dbo].[pGetForBillingBillingInvoiceRecords_Patient_Grooming](@IDs_Patient_Grooming        typIntList READONLY,  
                                               @IDs_Patient_Confinement typIntList READONLY)  
as  
    DECLARE @Patient_Grooming_ID_Model VARCHAR(MAX) = ''  
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''  
  
    SELECT @Patient_Grooming_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Grooming'  
  
    SELECT @Patient_Confinement_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Confinement'  
  
    DECLARE @record TABLE  
      (  
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,  
         [RefNo]            [varchar](50) NULL,  
         [ID_CurrentObject] [int] NOT NULL,  
         [Oid_Model]        [uniqueidentifier] NOT NULL,  
         [1]                VARCHAR(MAX) NULL,  
         [2]                VARCHAR(MAX) NULL,  
         [3]                VARCHAR(MAX) NULL,  
         [4]                VARCHAR(MAX) NULL,  
         [5]                VARCHAR(MAX) NULL,  
         [6]                VARCHAR(MAX) NULL,  
         [7]                VARCHAR(MAX) NULL,  
         [8]                VARCHAR(MAX) NULL,  
         [9]                VARCHAR(MAX) NULL,  
         [10]               VARCHAR(MAX) NULL  
      )  
  
    /*Patient hed*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_Grooming_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_Grooming,  
                   Code_Patient_Grooming,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_Grooming,  
                           hed.Code                                            Code_Patient_Grooming,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_Grooming  
                               ORDER BY ID_Patient_Grooming ASC, bi.ID)             BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_Grooming hed  
                                   ON bi.ID_Patient_Grooming = hed.ID  
                           INNER JOIN @IDs_Patient_Grooming ids  
                                   on hed.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_Grooming, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    SELECT '_',  
           '' BillingInvoices  
  
    SELECT GETDate() Date  
  
    SELECT *  
    FROM   @record  
  

GO
