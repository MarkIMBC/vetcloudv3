﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInsertSOAPType](@soapType VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tSOAPType
         WHERE  Name = @soapType) = 0
        BEGIN
            INSERT INTO [dbo].tSOAPType
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@soapType,
                         1,
                         1)
        END
  END

GO
