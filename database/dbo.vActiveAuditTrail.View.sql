﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAuditTrail] AS   
				SELECT   
				 H.*  
				FROM vAuditTrail H  
				WHERE IsActive = 1
GO
