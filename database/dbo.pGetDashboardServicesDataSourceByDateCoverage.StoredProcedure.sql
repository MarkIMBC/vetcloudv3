﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardServicesDataSourceByDateCoverage] (@ID_UserSession INT,
                                                              @DateStart      DateTime,
                                                              @DateEnd        DateTime)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT type.Name,
             Count(type.Name) as Count
      FROM   tPatient_SOAP soap
             INNER JOIN tSOAPType type
                     ON type.ID = soap.ID_SOAPType
      WHERE  soap.ID_Company = @ID_Company
             AND ID_FilingStatus NOT IN ( 4 )
             AND CONVERT(Date, soap.Date) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)
      GROUP  BY soap.ID_Company,
                type.Name
      Having COUNT(*) > 0
      ORDER  BY Count DESC

      DELETE FROM @dataSource
      WHERE  NAME NOT IN (SELECT TOP 10 Name
                          FROM   @dataSource)

      INSERT @dataSource
      SELECT 'Wellness',
             Count(*)
      FROm   tPatient_Wellness wellness
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus NOT IN ( 4 )
             AND CONVERT(Date, wellness.Date) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)

      DELETE FROM @dataSource
      WHERE  COUNT = 0

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT YEAR(@DateStart)                         DateYear,
             'From ' + FORMAT(@DateStart, 'MM/dd/yyyy')
             + ' To ' + FORMAT(@DateEnd, 'MM/dd/yyy') Label

      SELECT *
      FROM   @dataSource
      order  by Count DESC
  END

GO
