﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTextBlast_Client] AS   
				SELECT   
				 H.*  
				FROM vTextBlast_Client H  
				WHERE IsActive = 1
GO
