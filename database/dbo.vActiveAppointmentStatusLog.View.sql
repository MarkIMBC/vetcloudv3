﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppointmentStatusLog] AS   
				SELECT   
				 H.*  
				FROM vAppointmentStatusLog H  
				WHERE IsActive = 1
GO
