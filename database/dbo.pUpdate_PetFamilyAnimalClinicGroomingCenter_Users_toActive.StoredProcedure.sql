﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdate_PetFamilyAnimalClinicGroomingCenter_Users_toActive]
as
    DEclare @GUID_Company VARCHAR(MAX) = 'A0226302-B81A-4EC0-8DD6-282D2AE04676'

    IF(SELECT Count(*)
       FROM   tCompany
       WHERE  Guid = @GUID_Company
              AND IsActive = 1) <> 1
      BEGIN ;
          THROW 51000, 'Company does not exist.', 1;
      END

    --SELECT *
    --FROm   tCompany
    --WHERE  GUID = @GUID_Company
    ----------------------------------------------------------------
    DECLARE @ID_Company INT
    DECLARE @IDs_Users typIntList

    SELECT @ID_Company = ID
    FROM   tCompany
    WHERE  Guid = @GUID_Company

    INSERT @IDs_Users
    VALUES (109),
           (110),
           (110),
           (111),
           (112),
           (113),
           (289),
           (290),
           (926),
           (927)

		  --     Update tUserSession
    --SET    IsActive = 1
    --FROM   tUserSession _session
    --       inner join @IDs_Users _user
    --               on _session.ID_User = _user.ID
    --where  IsActive = 0


    Update vUser
    SET    IsActive = 1
    WHERE  ID IN (SELECT *
                  FROM   @IDs_Users)
           AND ID_Company = @ID_Company

    print 'Pet Family Animal Clinic Grooming Center staff users has been actived.'

GO
