﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTextBlast_Client] AS   
				SELECT   
				 H.*  
				FROM vTextBlast_Client H  
				WHERE ISNULL(IsActive, 0) = 0
GO
