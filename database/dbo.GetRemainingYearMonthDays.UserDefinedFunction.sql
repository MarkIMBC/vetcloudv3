﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetRemainingYearMonthDays] (@Date DATETIME, @RemainingCaption VARCHAR(MAX) = NULL, @InvalidRemainingCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = ''
  DECLARE @date1 DATETIME
         ,@date2 DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @Date IS NULL
    RETURN ''

  SET @RemainingCaption = ISNULL(@RemainingCaption, 'remaining')

  SELECT
    @date1 = GETDATE()
   ,@date2 = @Date

  SET @years = DATEDIFF(mm, @date1, @date2) / 12
  SET @months = DATEDIFF(mm, @date1, @date2) % 12
  SET @days = DATEDIFF(dd, DATEADD(mm, DATEDIFF(mm, @date1, @date2), @date1), @date2)

  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years = 0
    AND @months > 0)
    SET @result = CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years <= 0
    AND @months <= 0)
  BEGIN

    IF (ISNULL(@days, 0) > 0)
    BEGIN

      SET @result = CONVERT(VARCHAR(MAX), @days) + ' day' +
      CASE
        WHEN @days > 0 THEN 's'
        ELSE ''
      END + ' ' + @RemainingCaption

    END
    ELSE
    BEGIN
      SET @result = @InvalidRemainingCaption
    END

  END


  RETURN @result
END

GO
