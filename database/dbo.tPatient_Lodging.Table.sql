﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Lodging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[DateStart] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[ID_FilingStatus] [int] NULL,
	[DateCheckIn] [datetime] NULL,
	[DateCheckOut] [datetime] NULL,
	[RateAmount] [decimal](18, 4) NULL,
	[AdvancedPaymentAmount] [decimal](18, 4) NULL,
	[TotalAmount] [decimal](18, 4) NULL,
	[PayableAmount] [decimal](18, 4) NULL,
	[HourCount] [decimal](18, 4) NULL,
	[PaymentAmount] [decimal](18, 4) NULL,
	[ChangeAmount] [decimal](18, 4) NULL,
	[RemainingAmount] [decimal](18, 4) NULL,
 CONSTRAINT [PK_tPatient_Lodging] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Lodging] ON [dbo].[tPatient_Lodging]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Lodging] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Lodging]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Lodging_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Lodging] CHECK CONSTRAINT [FK_tPatient_Lodging_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Lodging]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Lodging_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Lodging] CHECK CONSTRAINT [FK_tPatient_Lodging_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Lodging]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Lodging_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Lodging] CHECK CONSTRAINT [FK_tPatient_Lodging_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Lodging]
		ON [dbo].[tPatient_Lodging]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Lodging
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Lodging hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Lodging] ENABLE TRIGGER [rDateCreated_tPatient_Lodging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Lodging]
		ON [dbo].[tPatient_Lodging]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Lodging
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Lodging hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Lodging] ENABLE TRIGGER [rDateModified_tPatient_Lodging]
GO
