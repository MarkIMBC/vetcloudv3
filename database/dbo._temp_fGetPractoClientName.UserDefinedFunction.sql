﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[_temp_fGetPractoClientName](@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      DECLARE @string VARCHAR(MAX) = ''
      DECLARE @charIndex INT = 0

      SET @string = @Value
      SET @charIndex = LEN(@string)-CHARINDEX('-', @string)
      SET @string = LTRIM(RTRIM(RIGHT(@string, @charIndex)))

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('[', @Value)
            SET @string = LTRIM(RTRIM(RIGHT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('=', @Value)
            SET @string = LTRIM(RTRIM(RIGHT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('(', @Value)
            SET @string = LTRIM(RTRIM(RIGHT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('{', @Value)
            SET @string = LTRIM(RTRIM(RIGHT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX(']', @Value)
            SET @string = LTRIM(RTRIM(RIGHT(@Value, @charIndex)))
        END

      SET @string = REPLACE(@string, '-', '')
      SET @string = REPLACE(@string, '[', '')
      SET @string = REPLACE(@string, '=', '')
      SET @string = REPLACE(@string, '(', '')
      SET @string = REPLACE(@string, '{', '')
      SET @string = REPLACE(@string, ']', '')

      RETURN @string
  END

GO
