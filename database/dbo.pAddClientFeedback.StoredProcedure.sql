﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddClientFeedback] (@Feedback       VARCHAR(600),
                                   @ID_Client      INT,
                                   @ID_UserSession INT)
AS
  BEGIN
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @IsActive INT =1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Success BIT = 1;
      DECLARE @ID_Company INT;
      DECLARE @ID_User INT;

      SET @ID_Company= (SELECT ID_Company
                        FROM   tClient
                        WHERE  ID = @ID_Client)

      SELECT @ID_User = ID_User
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      BEGIN TRY
          INSERT INTO [dbo].[tClientFeedback]
                      ([IsActive],
                       [ID_Company],
                       [Comment],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy])
          VALUES      (@IsActive,
                       @ID_Company,
                       @Feedback,
                       @DateCreated,
                       @DateModified,
                       @ID_User,
                       @ID_User )
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
