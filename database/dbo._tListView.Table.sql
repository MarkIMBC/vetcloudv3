﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tListView](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[DataSource] [varchar](1000) NULL,
	[Comment] [varchar](200) NULL,
	[Caption] [varchar](200) NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[PageSize] [int] NULL,
	[IsAllowAdd] [bit] NULL,
	[IsAllowDelete] [bit] NULL,
	[IsAllowEdit] [bit] NULL,
	[JsController] [varchar](300) NULL,
 CONSTRAINT [PK__tListView] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tListView] ADD  CONSTRAINT [DF__tListView_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tListView]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tListView] CHECK CONSTRAINT [FK_tListView_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tListView]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tListView] CHECK CONSTRAINT [FK_tListView_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[_tListView]  WITH NOCHECK ADD  CONSTRAINT [FK_tListView_ID_Model] FOREIGN KEY([ID_Model])
REFERENCES [dbo].[_tModel] ([Oid])
GO
ALTER TABLE [dbo].[_tListView] CHECK CONSTRAINT [FK_tListView_ID_Model]
GO
