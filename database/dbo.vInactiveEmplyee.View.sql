﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEmplyee] AS   
				SELECT   
				 H.*  
				FROM vEmplyee H  
				WHERE ISNULL(IsActive, 0) = 0
GO
