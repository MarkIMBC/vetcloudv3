﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientReferral] AS   
    SELECT   
     H.*  
    FROM vPatientReferral H  
    WHERE ISNULL(IsActive, 0) = 1
GO
