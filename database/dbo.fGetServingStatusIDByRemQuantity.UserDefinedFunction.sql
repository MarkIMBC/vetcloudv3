﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    CREATE FUNCTION [dbo].[fGetServingStatusIDByRemQuantity](@Quantity INT, @RemainingQuantity INT)  
    RETURNS INT  
    BEGIN  

		DECLARE @Pending_ID_FilingStatus INT = 2
		DECLARE @PartiallyServed_ID_FilingStatus INT = 5
		DECLARE @FullyServed_ID_FilingStatus INT = 6
		DECLARE @OverServed_ID_FilingStatus INT = 7

		DECLARE @ID_FilingStatus INT = 0

		SET @Quantity = ISNULL(@Quantity, 0)
		SET @RemainingQuantity = ISNULL(@RemainingQuantity, 0)

		IF @Quantity = @RemainingQuantity
			set @ID_FilingStatus = @Pending_ID_FilingStatus
		ELSE 
			IF @RemainingQuantity < @Quantity
			BEGIN
				
				IF @RemainingQuantity > 0 
					set @ID_FilingStatus = @PartiallyServed_ID_FilingStatus 
				ELSE 
					IF @RemainingQuantity = 0 
						set @ID_FilingStatus = @FullyServed_ID_FilingStatus 

			END


		RETURN @ID_FilingStatus
    END

GO
