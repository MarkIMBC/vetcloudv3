﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vAttendingVeterinarian]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Company,
         hed.Name_Position
  FROM   vEmployee hed
  WHERE  ( hed.ID_Position IN ( 10, 4, 5, 6,
                                13, 14, 15, 16,
                                3, 19, 20, 31 )
            OR ISNULL(hed.Name_Position, '') LIKE '%Veterinarian%'
            OR ISNULL(hed.Name_Position, '') LIKE '%Groomer%' )
         AND ISNULL(hed.IsActive, 0) = 1
         AND LOWER(hed.Name) NOT LIKE 'system%'
         AND LOWER(hed.Name) NOT LIKE 'sole%'

GO
