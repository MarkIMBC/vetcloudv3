﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveOccupationalStatus] AS   
				SELECT   
				 H.*  
				FROM vOccupationalStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
