﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tInventoryTrail_DeletedRecord](
	[ID] [int] NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[ID_FilingStatus] [int] NULL,
	[Date] [datetime] NULL,
	[DateExpired] [datetime] NULL,
	[BatchNo] [int] NULL,
	[tempID] [varchar](300) NULL,
	[DateDeleted] [datetime] NULL,
	[LastDuplicateCount] [int] NULL,
	[Oid_Model_Reference] [varchar](max) NULL,
	[ID_Reference] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20240322-173652] ON [dbo].[tInventoryTrail_DeletedRecord]
(
	[ID_Item] ASC,
	[ID_Reference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
