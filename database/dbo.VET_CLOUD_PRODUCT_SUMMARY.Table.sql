﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VET_CLOUD_PRODUCT_SUMMARY](
	[Item] [varchar](500) NULL,
	[Buying Price] [decimal](18, 2) NULL,
	[Selling Price] [decimal](18, 2) NULL,
	[Quantity] [decimal](18, 2) NULL,
	[Status] [varchar](500) NULL
) ON [PRIMARY]
GO
