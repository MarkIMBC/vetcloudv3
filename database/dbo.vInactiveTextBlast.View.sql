﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTextBlast] AS   
				SELECT   
				 H.*  
				FROM vTextBlast H  
				WHERE ISNULL(IsActive, 0) = 0
GO
