﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBank] AS   
				SELECT   
				 H.*  
				FROM vBank H  
				WHERE IsActive = 1
GO
