﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePayablePayment] AS   
				SELECT   
				 H.*  
				FROM vPayablePayment H  
				WHERE IsActive = 1
GO
