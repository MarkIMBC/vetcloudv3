﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItemCategory] AS   
				SELECT   
				 H.*  
				FROM vItemCategory H  
				WHERE ISNULL(IsActive, 0) = 0
GO
