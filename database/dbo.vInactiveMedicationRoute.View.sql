﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveMedicationRoute] AS   
				SELECT   
				 H.*  
				FROM vMedicationRoute H  
				WHERE ISNULL(IsActive, 0) = 0
GO
