﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSetting] AS   
				SELECT   
				 H.*  
				FROM vSetting H  
				WHERE ISNULL(IsActive, 0) = 0
GO
