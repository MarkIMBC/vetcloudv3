﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[_fSQLPGetScript]( @TableName VARCHAR(200) )
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @DEFAULT_VALUES VARCHAR(MAX) = 'SELECT ' + CHAR(13) + '	NULL AS [_]' + CHAR(13), 
			@ModelName VARCHAR(500), @PrimaryKey VARCHAR(300)

	SET  @ModelName = ( CASE WHEN SUBSTRING(@TableName, 1, 1) = '_'
                                THEN SUBSTRING(@TableName, 3, LEN(@TableName))
                                ELSE SUBSTRING(@TableName, 2, LEN(@TableName))
                           END )


    SELECT  @PrimaryKey = UPPER(U.COLUMN_NAME)
    FROM    _tModel H
            LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE U ON OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA
                                                    + '.'
                                                    + QUOTENAME(CONSTRAINT_NAME)),
                                                    'IsPrimaryKey') = 1
                                                    AND U.TABLE_NAME = @TableName


	SELECT  --NULL ,
		@DEFAULT_VALUES = COALESCE(@DEFAULT_VALUES + ' , ', '') + 
		( CASE 
			WHEN t.system_type_id = 56 AND c.name = 'ID' THEN '	-1' --2 --INT
			WHEN t.system_type_id = 56 AND c.name <> 'ID' THEN 'NULL' --2 --INT
			WHEN t.system_type_id = 167 THEN 'NULL' --1 --STRING
			WHEN t.system_type_id = 104 AND c.name = 'IsActive' THEN '1' --BOOL
			WHEN t.system_type_id = 104 THEN 'NULL' --BOOL
			WHEN t.system_type_id = 106 THEN 'NULL' -- DECIMAL
			WHEN t.system_type_id = 40 THEN 'NULL' --DATE
			ELSE ' NULL'
		END )  + ' AS [' + C.name + ']' + CHAR(13)
	FROM sys.columns c
	LEFT JOIN sys.types t ON c.system_type_id = t.system_type_id
	WHERE   OBJECT_NAME(object_id) = @TableName

	DECLARE @strReturn VARCHAR(MAX) = ''

	SET @strReturn = 'CREATE PROCEDURE pGet' + @ModelName + ' ' +  ( CASE WHEN @PrimaryKey = '@OID' THEN '@Oid UNIQUEIDENTIFIER' ELSE '@ID INT = -1' END ) + ' , @ID_Session INT = NULL
		AS
		BEGIN
			SELECT ''_''

			DECLARE @ID_User INT, @ID_Warehouse INT
			SELECT @ID_User = ID_User, @ID_Warehouse = ID_Warehouse FROM tUserSession WHERE ID = @ID_Session

			IF (@ID = -1)
				BEGIN
					SELECT 
						H.* 
					FROM ( 
						' + @DEFAULT_VALUES + '
					) H
					LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
					LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
				END
			ELSE
				BEGIN
					SELECT 
						H.* 
					FROM v' + @ModelName   + ' H 
					WHERE H.ID = @ID
				END
		END'

		RETURN @strReturn
END
GO
