﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePaymentTransaction] AS   
				SELECT   
				 H.*  
				FROM vPaymentTransaction H  
				WHERE IsActive = 1
GO
