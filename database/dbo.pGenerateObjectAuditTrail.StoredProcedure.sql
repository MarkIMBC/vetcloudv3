﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGenerateObjectAuditTrail]
    @ID_Model UNIQUEIDENTIFIER ,
    @ID_CurrentObject VARCHAR(100),
	@Description VARCHAR(MAX) = NULL,
	@ID_Session INT = NULL,
	@ID_AuditType INT = 1
AS
    DECLARE @TempTable TABLE
        (
          ID_CurrentObject VARCHAR(100) ,
          ID_Model UNIQUEIDENTIFIER ,
          Name VARCHAR(MAX) ,
          Value VARCHAR(MAX)
        )


    INSERT  @TempTable
            ( ID_CurrentObject ,
              ID_Model ,
              Name ,
              Value
            )
            EXEC dbo.pObjectAuditTrail @Oid_Model = @ID_Model,
                @ID_CurrentObject = @ID_CurrentObject
            
    DECLARE @ID_PropertyModel UNIQUEIDENTIFIER ,
        @PropertyLink VARCHAR(1000)
    DECLARE TEMPC CURSOR
    FOR
        SELECT  P.ID_PropertyModel ,
                PL.Name
        FROM    dbo.[_tModel_Property] P
                LEFT JOIN dbo.[_tModel_Property] PL ON P.ID_ModelProperty_Key = PL.Oid
        WHERE   P.ID_Model = @ID_Model
                AND P.ID_PropertyType = 10
    OPEN TEMPC
    FETCH NEXT FROM TEMPC INTO @ID_PropertyModel, @PropertyLink
    WHILE @@FETCH_STATUS = 0
        BEGIN
			
            INSERT  @TempTable
                    ( ID_CurrentObject ,
                      ID_Model ,
                      Name ,
                      Value
                    )
                    EXEC dbo.pObjectAuditTrail @Oid_Model = @ID_Model,
						 @ID_CurrentObject = @ID_CurrentObject

            FETCH NEXT FROM TEMPC INTO @ID_PropertyModel, @PropertyLink
        END	
    CLOSE TEMPC
    DEALLOCATE TEMPC

    DECLARE @AuditDetails typAuditTrail
    INSERT  @AuditDetails
            ( PropertyName ,
              NewValue ,
              OldValue ,
              ID_Model ,
              ID_AuditTrailType					
            )
            SELECT  Name ,
                    (CASE @ID_AuditType WHEN 3 THEN  NULL ELSE Value END) ,
                    (CASE @ID_AuditType WHEN 3 THEN  Value ELSE NULL END) ,
                    ID_Model ,
                    --ID_CurrentObject ,
                    (CASE @ID_AuditType WHEN 3 THEN  'D' ELSE 'I' END)
            FROM    @TempTable
GO
