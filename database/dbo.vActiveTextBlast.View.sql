﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveTextBlast] AS   
				SELECT   
				 H.*  
				FROM vTextBlast H  
				WHERE IsActive = 1
GO
