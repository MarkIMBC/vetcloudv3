﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzAdmissionReport]
AS
  SELECT patient.ID,
         patient.Name                               Name_Patient,
         ISNULL(Species, '')                        Name_Species,
         ISNULL(Name_Gender, '')                    Name_Gender,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A') Age_Patient,
         ISNULL(client.Name, '')                    Name_Client,
         ISNULL(client.Address, '')                 Address_Client,
         ISNULL(client.ContactNumber, '')           ContactNumber_Client,
         ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
         ISNULL(client.Email, '')                   Email_Client,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company 
GO
