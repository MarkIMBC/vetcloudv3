﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetForSendPatientBirthDate]
AS
  BEGIN
      DECLARE @BirthDateString VARCHAR(MAX) = Format(GetDate(), 'MM-dd')

      EXEC dbo.pGetSendPatientBirthDate
        @BirthDateString,
        0,
        '168, 48, 23, 176, 22, 92, 166, 142, 100, 102'
  END

GO
