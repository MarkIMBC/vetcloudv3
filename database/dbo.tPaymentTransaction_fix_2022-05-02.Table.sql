﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPaymentTransaction_fix_2022-05-02](
	[ID] [int] NOT NULL,
	[Code] [varchar](50) NULL,
	[Comment] [varchar](max) NULL,
	[Date] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [date] NULL,
	[Name_Company] [varchar](200) NULL,
	[Name_FilingStatus] [varchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
