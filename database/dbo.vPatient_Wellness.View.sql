﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vPatient_Wellness]
AS
  SELECT H.*,
         UC.Name                                                CreatedBy,
         UM.Name                                                LastModifiedBy,
         client.Name                                            Name_Client,
         patient.Name                                           Name_Patient,
         fs.Name                                                Name_FilingStatus,
         Isnull(_AttendingPhysician.Name, H.AttendingPhysician) AttendingPhysician_Name_Employee,
         bifs.Name                                              BillingInvoice_Name_FilingStatus
  FROM   tPatient_Wellness H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tFilingStatus bifs
                ON bifs.ID = H.BillingInvoice_ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee

GO
