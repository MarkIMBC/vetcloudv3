﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 VIEW [dbo].[vCompanyInactive]
AS
  SELECT *
  FROm   vCompany
  WHERE  ISNULL(IsActive, 0) = 0

GO
