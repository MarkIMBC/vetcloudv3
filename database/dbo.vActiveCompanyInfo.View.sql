﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompanyInfo] AS   
				SELECT   
				 H.*  
				FROM vCompanyInfo H  
				WHERE IsActive = 1
GO
