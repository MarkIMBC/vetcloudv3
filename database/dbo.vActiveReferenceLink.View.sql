﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveReferenceLink] AS   
    SELECT   
     H.*  
    FROM vReferenceLink H  
    WHERE IsActive = 1
GO
