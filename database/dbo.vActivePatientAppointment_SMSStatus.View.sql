﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientAppointment_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vPatientAppointment_SMSStatus H  
    WHERE IsActive = 1
GO
