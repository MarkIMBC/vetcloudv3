﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    
CREATE      
 VIEW [dbo].[vBillingInvoice]    
AS    
  SELECT H.*,    
         CONVERT(VARCHAR(100), H.Date, 101)  DateString,    
         UC.Name                             AS CreatedBy_Name_User,    
         UM.Name                             AS LastModifiedBy_Name_User,    
         approvedUser.Name                   AS ApprovedBy_Name_User,    
         cancelUser.Name                     AS CanceledBy_Name_User,    
         company.Name                        Company,    
         h.PatientNames                      Name_Patient,    
         taxScheme.Name                      Name_TaxScheme,    
         fs.Name                             Name_FilingStatus,    
         fsPayment.Name                      Payment_Name_FilingStatus,    
         client.Name                         Name_Client,    
         CONVERT(VARCHAR, H.DateCreated, 0)  DateCreatedString,    
         CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,    
         CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,    
         CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString,    
         attendingPhysicianEmloyee.Name      AttendingPhysician_Name_Employee,    
         CASE    
           WHEN H.ID_FilingStatus = 3 THEN fsPayment.Name    
           ELSE fs.Name    
         END                                 Status,    
         soapType.Name                       Name_SOAPType,    
         h.Code    
         + CASE    
             WHEN ISNULL(h.OtherReferenceNumber, '') <> '' THEN ' (' + h.OtherReferenceNumber + ')'    
             ELSE ''    
           END                               CustomCode,    
         confinement.Code                    Code_Patient_Confinement    
  FROM   dbo.tBillingInvoice H    
         LEFT JOIN dbo.tUser UC    
                ON H.ID_CreatedBy = UC.ID    
         LEFT JOIN dbo.tUser UM    
                ON H.ID_LastModifiedBy = UM.ID    
         LEFT JOIN dbo.tUser approvedUser    
                ON H.ID_ApprovedBy = approvedUser.ID    
         LEFT JOIN dbo.tUser cancelUser    
                ON H.ID_CanceledBy = cancelUser.ID    
         LEFT JOIN dbo.tCompany company    
                ON H.ID_Company = company.ID    
         LEFT JOIN dbo.tPatient patient    
                ON patient.ID = H.ID_Patient    
         LEFT JOIN dbo.tTaxScheme taxScheme    
                ON taxScheme.ID = H.ID_TaxScheme    
         LEFT JOIN dbo.tFilingStatus fs    
                ON fs.ID = H.ID_FilingStatus    
         LEFT JOIN dbo.tFilingStatus fsPayment    
                ON fsPayment.ID = H.Payment_ID_FilingStatus    
         LEFT JOIN dbo.tClient client    
                ON client.ID = H.ID_Client    
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee    
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee    
         LEFT JOIN tSOAPType soapType    
                ON H.ID_SOAPType = soapType.ID    
         LEFT JOIN tPatient_Confinement confinement    
                ON H.ID_Patient_Confinement = confinement.ID    
GO
