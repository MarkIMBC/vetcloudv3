﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetLinkCafe_Items](
	[Item] [varchar](500) NULL,
	[Quantity] [varchar](500) NULL
) ON [PRIMARY]
GO
