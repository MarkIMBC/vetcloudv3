﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   
 VIEW [dbo].[vzSalesIncomeReportNewDetail]  
AS  
  select biHed.ID_Company,  
         company.ImageLogoLocationFilenamePath,  
         company.Name    Name_Company,  
         company.Address Address_Company,  
         CASE  
           WHEN len(company.Address) > 0 THEN '' + company.Address  
           ELSE ''  
         END  
         + CASE  
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber  
             ELSE ''  
           END  
         + CASE  
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email  
             ELSE ''  
           END           HeaderInfo_Company,  
         biHed.ID,  
         biHed.Code,  
         biHed.Date,  
         biHed.Name_Client,  
         biDetail.Name_Item,  
         biDetail.Quantity,  
         biDetail.UnitPrice,  
         biDetail.UnitCost,  
         biDetail.DiscountAmount,  
         biTotalDiscBillDate.TotalDiscountAmountPerDate  
  FROm   vBillingInvoice_Detail biDetail  
         inner join vBillingInvoice biHed  
                 on biDetail.ID_BillingInvoice = biHed.ID  
         INNER JOin vzSalesIncomentReport_TotalDiscountPerBillDate biTotalDiscBillDate  
                 on biTotalDiscBillDate.ID_Company1 = biHed.ID_Company  
                    and biTotalDiscBillDate.Date = Convert(Date, biHed.Date)  
         INNER JOIN vCompany company  
                 ON company.ID = biHed.ID_Company  
  WHERE  biHed.ID_FilingStatus = 3  
         and CONVERT(Date, biHed.Date) = '2022-08-25'  
  
GO
