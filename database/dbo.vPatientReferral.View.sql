﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatientReferral]
AS
  SELECT H.*,
         UC.Name           AS CreatedBy,
         UM.Name           AS LastModifiedBy,
         client.Name       Name_Client,
         patient.Name      Name_Patient,
         fs.Name           Name_FilingStatus,
         attendingVet.Name AttendingPhysician_Name_Employee
  FROM   tPatientReferral H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on client.ID = h.ID_Client
         LEFT JOIN tPatient patient
                on H.ID_Patient = patient.ID
         LEFT JOIN tEmployee attendingVet
                on H.AttendingPhysician_ID_Employee = attendingVet.ID
         LEFT JOIN tFilingStatus fs
                on H.ID_FilingStatus = fs.ID

GO
