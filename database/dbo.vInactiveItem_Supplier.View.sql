﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItem_Supplier] AS   
				SELECT   
				 H.*  
				FROM vItem_Supplier H  
				WHERE ISNULL(IsActive, 0) = 0
GO
