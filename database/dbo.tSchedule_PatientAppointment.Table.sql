﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSchedule_PatientAppointment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_Schedule] [int] NULL,
	[ID_ScheduleType] [int] NULL,
	[DateStart] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[ID_Doctor] [int] NULL,
	[ID_Patient] [int] NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_tSchedule_PatientAppointment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tSchedule_PatientAppointment] ON [dbo].[tSchedule_PatientAppointment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tSchedule_PatientAppointment] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tSchedule_PatientAppointment]  WITH CHECK ADD  CONSTRAINT [FKtSchedule_PatientAppointment_ID_Schedule] FOREIGN KEY([ID_Schedule])
REFERENCES [dbo].[tSchedule] ([ID])
GO
ALTER TABLE [dbo].[tSchedule_PatientAppointment] CHECK CONSTRAINT [FKtSchedule_PatientAppointment_ID_Schedule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tSchedule_PatientAppointment]
		ON [dbo].[tSchedule_PatientAppointment]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSchedule_PatientAppointment
			SET    DateCreated = GETDATE()
			FROM   dbo.tSchedule_PatientAppointment hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSchedule_PatientAppointment] ENABLE TRIGGER [rDateCreated_tSchedule_PatientAppointment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tSchedule_PatientAppointment]
		ON [dbo].[tSchedule_PatientAppointment]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tSchedule_PatientAppointment] ENABLE TRIGGER [rDateModified_tSchedule_PatientAppointment]
GO
