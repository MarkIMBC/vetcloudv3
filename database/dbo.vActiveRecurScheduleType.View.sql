﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveRecurScheduleType] AS   
				SELECT   
				 H.*  
				FROM vRecurScheduleType H  
				WHERE IsActive = 1
GO
