﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClientPatient]
AS
  SELECT patient.ID,
         patient.Name Name_Patient,
         client.ID    ID_Client,
         client.Name  Name_Client,
         client.ID_Company,
         _user.ID     ID_User
  FROm   tPatient patient
         INNER JOIN tClient client
                 on patient.ID_Client = client.ID
         Inner JOIN tUser _user
                 on _user.ID = client.ID_User

GO
