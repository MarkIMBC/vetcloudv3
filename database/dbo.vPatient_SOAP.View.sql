﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                                           AS CreatedBy,
         UM.Name                                           AS LastModifiedBy,
         UC.Name                                           AS Name_CreatedBy,
         UM.Name                                           AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101)                DateString,
         soapType.Name                                     Name_SOAPType,
         patient.Name                                      Name_Patient,
         ISNULL(patient.Comment, '')                       Comment_Patient,
         patient.Name_Client                               Name_Client,
         ISNULL(client.Comment, '')                        Comment_Client,
         approvedUser.Name                                 Name_ApprovedBy,
         canceledUser.Name                                 Name_CanceledBy,
         fs.Name                                           Name_FilingStatus,
         LTRIM(RTRIM(ISNULL(client.ContactNumber, '') + ' '
                     + ISNULL(client.ContactNumber2, ''))) ContactNumber_Client,
         attendingPhysicianEmloyee.Name                    AttendingPhysician_Name_Employee,
         confinement.Code                                  Code_Patient_Confinement,
         confinement.Date                                  Date_Patient_Confinement,
         confinement.DateDischarge                         DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus                       ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus                     Name_FilingStatus_Patient_Confinement,
         REPLACE(H.Diagnosis, CHAR(13), '<br/>')           DiagnosisHTML,
         billfs.Name                                       BillingInvoice_Name_FilingStatus
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.vClient client
                ON patient.ID_Client = client.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement
         LEFT JOIN dbo.tFilingStatus billfs
                ON billfs.ID = H.BillingInvoice_ID_FilingStatus

GO
