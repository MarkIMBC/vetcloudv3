﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveFilingStatus] AS   
				SELECT   
				 H.*  
				FROM vFilingStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
