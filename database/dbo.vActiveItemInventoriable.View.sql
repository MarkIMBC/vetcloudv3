﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vActiveItemInventoriable]
AS
  SELECT *
  FROM   dbo.vItemInventoriable
  Where  IsActive = 1;

GO
