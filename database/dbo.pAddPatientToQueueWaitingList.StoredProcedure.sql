﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddPatientToQueueWaitingList](@IDs_Patient    typIntList READONLY,
                                         @ID_UserSession INT)
AS
    DECLARE @Patient_Oid_Model Varchar(MAX) = ''
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @Patient_Oid_Model = Oid
    FROM   _tModel
    WHERE  TableName = 'tPatient'

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    DECLARE @DateCreatedMax Date

    SELECT @DateCreatedMax = CONVERT(DATE, MAX(DateCreated))
    FROm   tPatientWaitingList

    if( @DateCreatedMax < CONVERT(DATE, GETDATE()) )
      begin
          Update tDocumentSeries
          set    Counter = 1
          FROM   tDocumentSeries docSeries
                 INNER JOIN _tModel model
                         on docSeries.ID_Model = model.Oid
          where  TableName = 'tPatientWaitingList'
      END

    INSERT INTO [dbo].[tPatientWaitingList]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_Client],
                 [ID_Patient],
                 [WaitingStatus_ID_FilingStatus],
                 Oid_Model_Reference,
                 ID_Reference,
                 IsQueued)
    SELECT H.Name,
           H.IsActive,
           patient.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           patient.ID_Client,
           patient.ID ID_Patient,
           @Waiting_ID_FilingStatus,
           @Patient_Oid_Model,
           patient.ID,
           1
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDate() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @Waiting_ID_FilingStatus
    FROM   tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    DECLARE @IDs_Company typIntList

    INSERT @IDs_Company
    SELECT Distinct ID_Company
    FROM   tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    exec pAutoGeneratePatientWaitingListByCompanies @IDs_Company

GO
