﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetPatient_SOAP_LaboratoryImages] (@ID_Patient_SOAP INT)
RETURNS TABLE
AS
    RETURN
      SELECT c.ImageRowIndex,
             ROW_NUMBER()
               OVER (
                 ORDER BY ImageNo) AS RowIndex,
             c.ImageNo,
             FilePath,
             c.Remark
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
      WHERE  ps.ID = @ID_Patient_SOAP
             AND ( ISNULL(FilePath, '') <> ''
                    OR LEN(ISNULL(Remark, '')) > 0 )

GO
