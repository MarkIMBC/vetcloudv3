﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CREATE    
 PROC [dbo].[pUpdatePatientSOAPBillStatus](@IDs_Patient_SOAP               TYPINTLIST READONLY,  
                                        @BillingInvoice_ID_FilingStatus INT,  
                                        @ID_UserSession                 INT)  
AS  
    DECLARE @Success BIT = 1;  
    DECLARE @isForBilling BIT = 0;  
    DECLARE @message VARCHAR(300) = '';  
    DECLARE @ID_User INT  
    DECLARE @ID_Warehouse INT  
    DECLARE @ID_Company INT  
    DECLARE @ForBilling_BillingInvoice_ID_FilingStatus INT = 16  
  
    SELECT @ID_User = ID_User,  
           @ID_Warehouse = ID_Warehouse  
    FROM   tUserSession  
    WHERE  ID = @ID_UserSession  
  
    SELECT @ID_Company = ID_Company  
    FROM   vUser  
    WHERE  ID = @ID_User  
  
    BEGIN TRY  
        if( @BillingInvoice_ID_FilingStatus = @ForBilling_BillingInvoice_ID_FilingStatus )  
          BEGIN  
              SET @isForBilling = 1  
          END  
  
        UPDATE tPatient_SOAP  
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus,  
               IsForBilling = @isForBilling  
        FROM   tPatient_SOAP _SOAP  
               INNER JOIN @IDs_Patient_SOAP ids  
                       ON _SOAP.ID = ids.ID;  
    END TRY  
    BEGIN CATCH  
        SET @message = ERROR_MESSAGE();  
        SET @Success = 0;  
    END CATCH;  
  
    SELECT '_';  
  
    SELECT @Success Success,  
           @message message;  
  
GO
