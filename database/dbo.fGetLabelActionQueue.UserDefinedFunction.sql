﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetLabelActionQueue] (@WaitingStatus_ID_FilingStatus   INT,
                                           @WaitingStatus_Name_FilingStatus VARCHAR(MAX))
RETURNS VARCHAR(200)
AS
  BEGIN
      DECLARE @label VARCHAR(200) = ''
      DECLARE @ID_FilingStatus_Pending INT = 4
      DECLARE @ID_FilingStatus_Canceled INT = 4
      DECLARE @ID_FilingStatus_Waiting INT = 8
      DECLARE @ID_FilingStatus_Done INT = 13
      DECLARE @ID_FilingStatus_Rescheduled INT = 21

      SET @WaitingStatus_ID_FilingStatus = ISNULL(@WaitingStatus_ID_FilingStatus, @ID_FilingStatus_Canceled)

      IF( @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Canceled
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Done
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Rescheduled
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Pending )
        BEGIN
            SET @label = 'Add to Queue'
        END
      else IF ( @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Waiting )
        BEGIN
            SET @label = 'Remove From Queue'
        END
      ELSE
        SET @label = @WaitingStatus_Name_FilingStatus

      RETURN @label;
  END;

GO
