﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tRecordValueTransferLogs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Oid_Model] [varchar](300) NULL,
	[PrimaryColumn] [varchar](300) NULL,
	[PrimaryKeyID] [int] NULL,
	[ColumnName] [varchar](300) NULL,
	[From_ID_RecordValue] [int] NULL,
	[To_ID_RecordValue] [int] NULL,
 CONSTRAINT [PK_tRecordValueTransferLogs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tRecordValueTransferLogs] ON [dbo].[tRecordValueTransferLogs]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs]  WITH CHECK ADD  CONSTRAINT [FK_tRecordValueTransferLogs_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] CHECK CONSTRAINT [FK_tRecordValueTransferLogs_ID_Company]
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs]  WITH CHECK ADD  CONSTRAINT [FK_tRecordValueTransferLogs_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] CHECK CONSTRAINT [FK_tRecordValueTransferLogs_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs]  WITH CHECK ADD  CONSTRAINT [FK_tRecordValueTransferLogs_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] CHECK CONSTRAINT [FK_tRecordValueTransferLogs_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tRecordValueTransferLogs]
		ON [dbo].[tRecordValueTransferLogs]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tRecordValueTransferLogs
			SET    DateCreated = GETDATE()
			FROM   dbo.tRecordValueTransferLogs hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] ENABLE TRIGGER [rDateCreated_tRecordValueTransferLogs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tRecordValueTransferLogs]
		ON [dbo].[tRecordValueTransferLogs]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tRecordValueTransferLogs
			SET    DateModified = GETDATE()
			FROM   dbo.tRecordValueTransferLogs hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tRecordValueTransferLogs] ENABLE TRIGGER [rDateModified_tRecordValueTransferLogs]
GO
