﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveInventorySummary] AS   
				SELECT   
				 H.*  
				FROM vInventorySummary H  
				WHERE IsActive = 1
GO
