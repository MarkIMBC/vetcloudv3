﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveExpenseCategory] AS   
				SELECT   
				 H.*  
				FROM vExpenseCategory H  
				WHERE IsActive = 1
GO
