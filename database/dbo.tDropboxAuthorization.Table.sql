﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tDropboxAuthorization](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](3000) NULL,
	[DateCreated] [datetime] NULL
) ON [PRIMARY]
GO
