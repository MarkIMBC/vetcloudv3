﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateAppointmentDateStartFromDateReschedule]
as
  BEGIN
      DECLARE @DateStart DateTime = GETDATE()
      DECLARE @DateEND DateTime = CONVERT(Date, DATEADD(Day, 1, GETDATE()))

      IF DATEDIFF(SECOND, @DateStart, @DateEND) <= 3600
        BEGIN
            DECLARE @Patient_SOAP_Plan_Oid_Model VARCHAR(MAX) = ''
            DECLARE @Patient_Wellness_Schedule_Oid_Model VARCHAR(MAX) = ''
            DECLARE @PatientAppointment_Oid_Model VARCHAR(MAX) = ''

            SET @Patient_SOAP_Plan_Oid_Model = dbo.fGetModelOid('tPatient_SOAP_Plan')
            SET @Patient_Wellness_Schedule_Oid_Model = dbo.fGetModelOid('tPatient_Wellness_Schedule')
            SET @PatientAppointment_Oid_Model = dbo.fGetModelOid('tPatientAppointment')

            INSERT INTO [dbo].[tAppointmentReschduleLog]
                        ([Oid_Model],
                         ID_CurrentObject,
                         [DateSchedule],
                         [DateReschedule],
                         [ID_Company],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy])
            SELECT @Patient_SOAP_Plan_Oid_Model,
                   _plan.ID,
                   _plan.DateReturn,
                   _plan.DateReschedule,
                   _soap.ID_Company,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1
            FROM   tPatient_SOAP_Plan _plan
                   inner join tPatient_SOAP _soap
                           on _plan.ID_Patient_SOAP = _soap.ID
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, DateReturn) = CONVERT(Date, GETDATE())

            INSERT INTO [dbo].[tAppointmentReschduleLog]
                        ([Oid_Model],
                         ID_CurrentObject,
                         [DateSchedule],
                         [DateReschedule],
                         [ID_Company],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy])
            SELECT @Patient_Wellness_Schedule_Oid_Model,
                   _schedule.ID,
                   _schedule.Date,
                   _schedule.DateReschedule,
                   _wellness.ID_Company,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1
            FROM   tPatient_Wellness_Schedule _schedule
                   inner join tPatient_Wellness _wellness
                           on _schedule.ID_Patient_Wellness = _wellness.ID
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, _schedule.Date) = CONVERT(Date, GETDATE())

            INSERT INTO [dbo].[tAppointmentReschduleLog]
                        ([Oid_Model],
                         ID_CurrentObject,
                         [DateSchedule],
                         [DateReschedule],
                         [ID_Company],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy])
            SELECT @PatientAppointment_Oid_Model,
                   _appointment.ID,
                   _appointment.DateStart,
                   _appointment.DateReschedule,
                   _appointment.ID_Company,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1
            FROM   tPatientAppointment _appointment
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, _appointment.DateStart) = CONVERT(Date, GETDATE())

            -----------------------------------------------------------------------------------  
            Update tPatient_SOAP_Plan
            set    DateReturn = Convert(DateTime, FORMAT(DateReschedule, 'yyyy-MM-dd') + ' '
                                                  + FORMAT(DateReturn, 'HH:mm:ss')),
                   DateSent = NULL,
                   IsSentSMS = 0
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, DateReturn) = CONVERT(Date, GETDATE())
                   AND DateReschedule IS NOT NULL

            Update tPatient_Wellness_Schedule
            set    Date = Convert(DateTime, FORMAT(DateReschedule, 'yyyy-MM-dd') + ' '
                                            + FORMAT(Date, 'HH:mm:ss')),
                   DateSent = NULL,
                   IsSentSMS = 0
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, Date) = CONVERT(Date, GETDATE())
                   AND DateReschedule IS NOT NULL

            Update tPatientAppointment
            set    DateStart = Convert(DateTime, FORMAT(DateReschedule, 'yyyy-MM-dd') + ' '
                                                 + FORMAT(DateStart, 'HH:mm:ss')),
                   Appointment_ID_FilingStatus = 2
            where  Appointment_ID_FilingStatus = 21
                   AND CONVERT(Date, DateStart) = CONVERT(Date, GETDATE())
                   AND DateReschedule IS NOT NULL
        END
  END 
GO
