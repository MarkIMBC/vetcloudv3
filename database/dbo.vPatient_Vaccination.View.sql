﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_Vaccination]
AS
  SELECT H.*,
         UC.Name                  CreatedBy,
         UM.Name                  LastModifiedBy,
         item.Name                Name_Item,
         client.Name              Name_Client,
         patient.Name             Name_Patient,
         fs.Name                  Name_FilingStatus,
         ISNULL(_AttendingPhysician.Name, H.AttendingPhysician ) AttendingPhysician_Name_Employee
  FROM   tPatient_Vaccination H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItem item
                on item.ID = H.ID_Item
         LEFT JOIN tClient client
                on client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                on patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                on fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                on _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee

GO
