﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertBreedSpecie](@BreedSpecie VARCHAR(MAX),
                              @ID_Company  INT = 1)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tBreedSpecie
         WHERE  Name = @BreedSpecie) = 0
        BEGIN
            INSERT INTO [dbo].tBreedSpecie
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@BreedSpecie,
                         1,
                         @ID_Company)
        END
  END

GO
