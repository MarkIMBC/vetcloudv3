﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveServiceType] AS   
				SELECT   
				 H.*  
				FROM vServiceType H  
				WHERE IsActive = 1
GO
