﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pCustomAddGroupControl]
    @ID_Control INT,
    @ID_DetailView UNIQUEIDENTIFIER,
    @ID_Parent UNIQUEIDENTIFIER = NULL,
    @label VARCHAR(MAX) = NULL
AS
DECLARE @ID_Parent_ControlType INT,
        @Count INT = 0;


DECLARE @labelName VARCHAR(MAX) = '';

SELECT @Count = COUNT(*)
FROM _tDetailView_Detail
WHERE ID_DetailView = @ID_DetailView
      AND ID_ControlType = @ID_Control;
SET @Count = @Count + 1;
SELECT @ID_Parent_ControlType = ID_ControlType
FROM _tDetailView_Detail
WHERE Oid = @ID_Parent;


IF LEN(ISNULL(@label, 0)) > 0
BEGIN

    SET @labelName = @label;

END;
ELSE
BEGIN

    SET @labelName = (CASE @ID_Control
                          WHEN 12 THEN
                              'Tab'
                          ELSE
                              'Section'
                      END
                     ) + CAST(@Count * CAST(RAND() AS INT) AS VARCHAR(10));

END;


INSERT INTO dbo.[_tDetailView_Detail]
(
    Oid,
    Code,
    Name,
    IsActive,
    SeqNo,
    Comment,
    Caption,
    ID_DetailView,
    ID_ControlType,
    ID_CreatedBy,
    DateCreated,
    ColCount,
    ColSpan,
    ID_Tab,
    ID_Section,
    IsShowLabel
)
VALUES
(   NEWID(), NULL, @labelName, 1, 1000, NULL, (CASE @ID_Control
                                                   WHEN 12 THEN
                                                       'Tab'
                                                   ELSE
                                                       'Section'
                                               END
                                              ) + CAST(@Count AS VARCHAR(10)), @ID_DetailView, @ID_Control, NULL,
    GETDATE(), 1, 3, (CASE
                          WHEN @ID_Parent_ControlType = 12 THEN
                              @ID_Parent
                          ELSE
                              NULL
                      END
                     ), (CASE
                             WHEN @ID_Parent_ControlType = 13 THEN
                                 @ID_Parent
                             ELSE
                                 NULL
                         END
                        ), 1);
GO
