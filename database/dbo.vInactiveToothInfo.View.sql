﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveToothInfo] AS   
				SELECT   
				 H.*  
				FROM vToothInfo H  
				WHERE ISNULL(IsActive, 0) = 0
GO
