﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveFilingStatus] AS   
				SELECT   
				 H.*  
				FROM vFilingStatus H  
				WHERE IsActive = 1
GO
