﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddItemCategoryService](@categoryName VARCHAR(MAX))
as
  BEGIN
      DECLARE @Service_ID_ItemType INT = 1

      IF(SELECT COUNT(*)
         FROM   tItemCategory
         where  Name = @categoryName
                AND ID_ItemType = @Service_ID_ItemType
                AND IsActive = 1) = 0
        BEGIN
            INSERT INTO [dbo].[tItemCategory]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [ID_ItemType])
            VALUES      (@categoryName,
                         1,
                         1,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @Service_ID_ItemType)
        END
  END

GO
