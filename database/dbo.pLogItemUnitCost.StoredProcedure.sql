﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pLogItemUnitCost] (@ID_Item INT)
AS
BEGIN

	DECLARE @ID_Company INT = 0
	DECLARE @Price DECIMAL(18,4) = 0

	SELECT @Price = ISNULL(UnitCost, 0),
			 @ID_Company = ID_Company
	FROM tItem 
	WHERE 
		ID = @ID_Item

	INSERT INTO [dbo].[tItem_UnitCostLog]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,ID_Item 
			   ,Price
			   )
		 VALUES(
			   ''
			   ,''
			   ,1
			   ,@ID_Company 
			   ,''
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,@ID_Item 
			   ,@Price 
			   )
	END
GO
