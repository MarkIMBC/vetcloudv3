﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveReceivingReport_Detail] AS   
				SELECT   
				 H.*  
				FROM vReceivingReport_Detail H  
				WHERE IsActive = 1
GO
