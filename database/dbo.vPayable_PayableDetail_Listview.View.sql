﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPayable_PayableDetail_Listview]
as
  SELECT payable.ID,
         payable.Date,
         payable.ID_Company,
         payableDetail.ID_ExpenseCategory ID_ExpenseCategory,
         expensesCat.Name                 Name_ExpenseCategory,
         payableDetail.Name               Name_Payable_Detail,
         payable.TotalAmount,
         payable.Payment_ID_FilingStatus,
         fs.Name                          Payment_Name_FilingStatus,
         payable.RemaningAmount,
         payable.PaidAmount
  FROM   tPayable payable
         INNER JOIN tPayable_Detail payableDetail
                 ON payableDetail.ID_Payable = payable.ID
         LEFT JOIN tExpenseCategory expensesCat
                ON expensesCat.ID = payableDetail.ID_ExpenseCategory
         LEFT JOIN tFilingStatus fs
                ON fs.ID = payable.Payment_ID_FilingStatus
  WHERE  Payment_ID_FilingStatus NOT IN ( 4 )

GO
