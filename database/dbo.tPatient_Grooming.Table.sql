﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Grooming](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[Weight] [varchar](300) NULL,
	[Temperature] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[BillingInvoice_ID_FilingStatus] [int] NULL,
	[IsForBilling] [bit] NULL,
 CONSTRAINT [PK_tPatient_Grooming] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Grooming] ON [dbo].[tPatient_Grooming]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Grooming] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Grooming]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming] CHECK CONSTRAINT [FK_tPatient_Grooming_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Grooming]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming] CHECK CONSTRAINT [FK_tPatient_Grooming_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Grooming]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Grooming_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Grooming] CHECK CONSTRAINT [FK_tPatient_Grooming_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateCreated_tPatient_Grooming] ON [dbo].[tPatient_Grooming] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatient_Grooming SET DateCreated = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatient_Grooming] ENABLE TRIGGER [rDateCreated_tPatient_Grooming]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateModified_tPatient_Grooming] ON [dbo].[tPatient_Grooming] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatient_Grooming SET DateModified = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatient_Grooming] ENABLE TRIGGER [rDateModified_tPatient_Grooming]
GO
