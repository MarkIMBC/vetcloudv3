﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pReRun_AfterSaved_Process_Item_BY_IDs](@ID_Item INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @IDs_Item TYPINTLIST

      INSERT @IDs_Item
      VALUES(@ID_Item)

      BEGIN TRY
          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item
      END TRY
      BEGIN CATCH
          SET @success = 0;
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT @success Success,
             @message Message
  END

GO
