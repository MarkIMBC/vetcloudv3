﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveLocalShippingPermitIssuance_Item] AS   
				SELECT   
				 H.*  
				FROM vLocalShippingPermitIssuance_Item H  
				WHERE IsActive = 1
GO
