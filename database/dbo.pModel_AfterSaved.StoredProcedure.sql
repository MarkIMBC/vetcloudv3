﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PayablePayment'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_PayablePayment]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Grooming'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Patient_Grooming]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'InactiveSMSSending'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_InactiveSMSSending]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Employee'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Employee
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE
        BEGIN
            DECLARE @StoredProcedureName VARCHAR(MAX) = '';
            DECLARE @sql VARCHAR(MAX) = '';

            SET @StoredProcedureName = 'pModel_AfterSaved_/*ModelName*/'
            SET @StoredProcedureName = REPLACE(@StoredProcedureName, '/*ModelName*/', @ModelName)

            IF EXISTS (SELECT 1
                       FROM   sys.objects
                       WHERE  type = 'P' -- 'P' stands for stored procedure
                              AND name = 'pModel_AfterSaved_')
              BEGIN
                  SET @sql = 'EXEC dbo./*StoredProcedureName*/ ''/*ID_CurrentObject*/'', ''/*IsNew*/''';
                  SET @sql = REPLACE(@sql, '/*StoredProcedureName*/', @StoredProcedureName)
                  SET @sql = REPLACE(@sql, '/*ID_CurrentObject*/', @ID_CurrentObject)
                  SET @sql = REPLACE(@sql, '/*IsNew*/', ISNULL(@IsNew, 'NULL'))

                  EXEC (@sql);
              END

            PRINT 1;
        END

      PRINT 1;
  END; 
GO
