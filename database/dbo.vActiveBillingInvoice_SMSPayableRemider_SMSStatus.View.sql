﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoice_SMSPayableRemider_SMSStatus] AS   
    SELECT   
     H.*  
    FROM vBillingInvoice_SMSPayableRemider_SMSStatus H  
    WHERE IsActive = 1
GO
