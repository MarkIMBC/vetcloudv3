﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tAuditTrail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[ID_User] [int] NULL,
	[ID_CurrentObject] [varchar](50) NULL,
	[ID_AuditType] [int] NULL,
	[ID_DetailView] [int] NULL,
	[ID_Session] [int] NULL,
	[Description] [varchar](300) NULL,
	[DateCreated] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[IsRunAfterSavedProcess] [bit] NULL,
	[DateRunAfterSavedProcess] [datetime] NULL,
	[RunAfterSavedProcess_ID_FilingStatus] [int] NULL,
 CONSTRAINT [PK_tAuditTrail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tAuditTrail] ADD  CONSTRAINT [DF_tAuditTrail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tAuditTrail]
		ON [dbo].[tAuditTrail]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tAuditTrail
			SET    DateCreated = GETDATE()
			FROM   dbo.tAuditTrail hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tAuditTrail] ENABLE TRIGGER [rDateCreated_tAuditTrail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tAuditTrail]
		ON [dbo].[tAuditTrail]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

	
GO
ALTER TABLE [dbo].[tAuditTrail] ENABLE TRIGGER [rDateModified_tAuditTrail]
GO
