﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetReportByName](@ReportName VARCHAR(200),
                            @ID_Session INT)
AS
  BEGIN
      DECLARE @Oid_Report UNIQUEIDENTIFIER

      SELECT '_'

      SELECT TOP 1 Oid
      FROM   _tReport
      WHERE  Name = @ReportName
  END

GO
