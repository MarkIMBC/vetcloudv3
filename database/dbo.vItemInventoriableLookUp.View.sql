﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vItemInventoriableLookUp]
AS
  SELECT ID,
         Code,
         Name,
         FORMAT(ISNULL(UnitPrice, 0), '#,###,##0.00')          UnitPrice,
         FORMAT(ISNULL(UnitCost, 0), '#,###,##0.00')           UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO
