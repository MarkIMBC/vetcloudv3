﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoice_Patient] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoice_Patient H  
				WHERE ISNULL(IsActive, 0) = 0
GO
