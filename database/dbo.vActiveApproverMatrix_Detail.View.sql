﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveApproverMatrix_Detail] AS   
				SELECT   
				 H.*  
				FROM vApproverMatrix_Detail H  
				WHERE IsActive = 1
GO
