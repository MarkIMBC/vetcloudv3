﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSalesReturn_Detail] AS   
				SELECT   
				 H.*  
				FROM vSalesReturn_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
