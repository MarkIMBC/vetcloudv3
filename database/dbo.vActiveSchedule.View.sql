﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSchedule] AS   
				SELECT   
				 H.*  
				FROM vSchedule H  
				WHERE IsActive = 1
GO
