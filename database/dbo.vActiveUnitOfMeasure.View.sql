﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUnitOfMeasure] AS   
				SELECT   
				 H.*  
				FROM vUnitOfMeasure H  
				WHERE IsActive = 1
GO
