﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompany] AS   
				SELECT   
				 H.*  
				FROM vCompany H  
				WHERE ISNULL(IsActive, 0) = 0
GO
