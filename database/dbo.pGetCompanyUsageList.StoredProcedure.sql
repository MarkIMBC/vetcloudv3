﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[pGetCompanyUsageList]
AS
  BEGIN
      Declare @ComapanyUsageList TABLE
        (
           ID_Company       INT,
           Name_Company     VARCHAR(MAX),
           LastDateAccess   DateTime,
           TotalAccessCount INT,
           LastBillDate     DateTime,
           TotalBillCount   INT,
           LastSOAPDate     DateTime,
           TotalSOAPCount   INT
        )
      DECLARE @Date Date = GETDATE();

      INSERT @ComapanyUsageList
      SELECT *
      FROM   dbo.fGetCompanyUsageList()

      SELECT *
      FROM   @ComapanyUsageList
      ORDER  BY Name_Company
  END 

GO
