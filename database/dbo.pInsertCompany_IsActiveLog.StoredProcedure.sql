﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertCompany_IsActiveLog](@ID_Company INT,
                                      @IsActive   bit,
                                      @Date       Datetime)
as
  BEGIN
      INSERT INTO [dbo].tCompany_IsActiveLog
                  ([Name],
                   [IsActive],
                   ID_Company,
                   Date)
      VALUES      ('',
                   @IsActive,
                   @ID_Company,
                   @Date)
  END

GO
