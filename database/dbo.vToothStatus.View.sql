﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vToothStatus]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       statusType.Name ToothStatusType,
       statusType.Name Name_ToothStatusType
FROM tToothStatus H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tToothStatusType statusType
        ON H.ID_ToothStatusType = statusType.ID;
GO
