﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveScheduleCalendar] AS   
    SELECT   
     H.*  
    FROM vScheduleCalendar H  
    WHERE ISNULL(IsActive, 0) = 1
GO
