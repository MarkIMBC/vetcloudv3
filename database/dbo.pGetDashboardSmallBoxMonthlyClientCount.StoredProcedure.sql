﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardSmallBoxMonthlyClientCount] (@ID_UserSession INT,
                                                        @Month          INT,
                                                        @Year           INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT
      DECLARE @TotalCount INT = 0
      DECLARE @DateStart DATE;
      DECLARE @DateEnd DATE;

      SET @DateStart = DATEFROMPARTS(@Year, @Month, 1);
      SET @DateEnd = EOMONTH(@DateStart);

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession WITH (NOLOCK)
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser WITH (NOLOCK)
      WHERE  ID = @ID_User

      SELECT @TotalCount = COUNT(*)
      FROM   tClient WITH (NOLOCK)
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) BETWEEN @DateStart AND @DateEnd

      DECLARE @subtitle VARCHAR(MAX) = '';

      IF( MONTH(GETDATE()) <> @Month
           OR YEAR(GETDATE()) <> @Year )
        set @subtitle = ' as of ' + FORMAT(@DateStart, 'MMMM yyyy')

      SELECT '_'

      SELECT @TotalCount                   TotalCount,
             FORMAT(@TotalCount, '#,#0')   FormattedTotalCount,
             'Monthly Clients' + @subtitle Subtitle
  END

GO
