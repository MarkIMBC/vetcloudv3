﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveDoc] AS   
				SELECT   
				 H.*  
				FROM vDoc H  
				WHERE IsActive = 1
GO
