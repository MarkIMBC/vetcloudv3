﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveAppointmentRequest] AS   
				SELECT   
				 H.*  
				FROM vAppointmentRequest H  
				WHERE IsActive = 1
GO
