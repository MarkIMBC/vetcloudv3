﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientGrooming] AS   
				SELECT   
				 H.*  
				FROM vPatientGrooming H  
				WHERE IsActive = 1
GO
