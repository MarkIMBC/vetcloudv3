﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 FUNCTION [dbo].[fGetPOSReceipt2ColumnsLayout] (@LeftText  VARCHAR(MAX),
                                                   @RightText VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = ''
      Declare @layout VARCHAR(MAX) = '  
  <div class="display-block">  
   <span class="bold">  
    /*LeftText*/  
   </span>  
   /*RightText*/  
     </div>      
  
  '

      SET @layout = REPLACE(@layout, '/*LeftText*/', @LeftText)
      SET @layout = REPLACE(@layout, '/*RightText*/', @RightText)

      RETURN @layout
  END

GO
