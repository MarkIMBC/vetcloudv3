﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPaymentTransaction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_BillingInvoice] [int] NULL,
	[ID_TaxScheme] [int] NULL,
	[GrossAmount] [decimal](18, 4) NULL,
	[VatAmount] [decimal](18, 4) NULL,
	[NetAmount] [decimal](18, 4) NULL,
	[Date] [datetime] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_PaymentMethod] [int] NULL,
	[CashAmount] [decimal](18, 4) NULL,
	[CheckAmount] [decimal](18, 4) NULL,
	[CheckNumber] [varchar](300) NULL,
	[PayableAmount] [decimal](18, 4) NULL,
	[PaymentAmount] [decimal](18, 4) NULL,
	[ChangeAmount] [decimal](18, 4) NULL,
	[DateApproved] [datetime] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[CardAmount] [decimal](18, 4) NULL,
	[GCashAmount] [decimal](18, 4) NULL,
	[ID_CardType] [int] NULL,
	[CardHolderName] [varchar](300) NULL,
	[ReferenceTransactionNumber] [varchar](300) NULL,
	[CardNumber] [varchar](300) NULL,
	[RemainingAmount] [decimal](18, 4) NULL,
	[CreditAmount] [decimal](18, 4) NULL,
	[tempID] [varchar](300) NULL,
	[ID_Bank] [int] NULL,
	[GUID] [varchar](300) NULL,
 CONSTRAINT [PK_tPaymentTransaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPaymentTransaction] ON [dbo].[tPaymentTransaction]
(
	[ID_BillingInvoice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPaymentTransaction] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPaymentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_tPaymentTransaction_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPaymentTransaction] CHECK CONSTRAINT [FK_tPaymentTransaction_ID_Company]
GO
ALTER TABLE [dbo].[tPaymentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_tPaymentTransaction_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPaymentTransaction] CHECK CONSTRAINT [FK_tPaymentTransaction_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPaymentTransaction]  WITH CHECK ADD  CONSTRAINT [FK_tPaymentTransaction_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPaymentTransaction] CHECK CONSTRAINT [FK_tPaymentTransaction_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPaymentTransaction]
		ON [dbo].[tPaymentTransaction]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPaymentTransaction
			SET    DateCreated = GETDATE()
			FROM   dbo.tPaymentTransaction hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPaymentTransaction] ENABLE TRIGGER [rDateCreated_tPaymentTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPaymentTransaction]
		ON [dbo].[tPaymentTransaction]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPaymentTransaction
			SET    DateModified = GETDATE()
			FROM   dbo.tPaymentTransaction hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPaymentTransaction] ENABLE TRIGGER [rDateModified_tPaymentTransaction]
GO
