﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_Grooming_Detail_Listview]
AS
  SELECT det.ID,
         hed.ID_Company,
         det.ID_Patient_Grooming,
         det.ID_Item,
         det.Name_Item,
         ISNULL(det.Comment, '') Comment,
         hed.ID_FilingStatus,
         hed.Date,
         hed.ID_Patient ,
         hed.Code ,
         hed.AttendingPhysician_Name_Employee 
  FROM   vPatient_Grooming_Detail det
         inner join vPatient_Grooming hed
                 on hed.ID = det.ID_Patient_Grooming

GO
