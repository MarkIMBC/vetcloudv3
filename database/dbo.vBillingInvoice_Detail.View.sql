﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBillingInvoice_Detail]
AS
  SELECT detail.*,
         item.Name Name_Item
  FROM   dbo.tBillingInvoice_Detail detail
         LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item;

GO
