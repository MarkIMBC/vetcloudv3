﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveWarehouse] AS   
				SELECT   
				 H.*  
				FROM vWarehouse H  
				WHERE ISNULL(IsActive, 0) = 0
GO
