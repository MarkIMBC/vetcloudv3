﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdateClientCredits]
AS
  BEGIN
      DECLARE @IDs_Client typIntList

      INSERT @IDs_Client
      SELECT ID
      FROM   vActiveClient

      exec pUpdateClientCreditsByClient
        @IDs_Client
  END

GO
