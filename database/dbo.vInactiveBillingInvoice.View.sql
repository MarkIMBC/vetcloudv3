﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoice] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoice H  
				WHERE ISNULL(IsActive, 0) = 0
GO
