﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAuditTrailType] AS   
				SELECT   
				 H.*  
				FROM vAuditTrailType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
