﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vSupplier_Item_List]
as
  SELECT hed.ID,
         item.ID_Company,
         item.Name                  Name_Item,
         item.ID                    ID_Item,
         s.ID                       ID_Supplier,
         s.Name                     Name_Supplier,
         item.CurrentInventoryCount CurrentInventoryCount,
         item.ID_InventoryStatus,
         item.Name_InventoryStatus
  FROM   tItem_Supplier hed
         INNER JOIN tSupplier s
                 ON hed.ID_Supplier = s.ID
         INNER JOIN vActiveItem item
                 ON hed.ID_Item = item.ID

GO
