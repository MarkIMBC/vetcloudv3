﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[pAdd_SMSPatientSOAP_Company](@ID_Company INT)
AS
BEGIN
	
	Declare @Already_Exist_Count INT = 0
	DECLARE @CompanyName VARCHAR(400) = '';
	DECLARE @message VARCHAR(400) = '';


	/* Validate if Company Exis*/  
    SELECT 
		@Already_Exist_Count = COUNT(*)
    FROM dbo.tSMSPatientSOAP_Company
    WHERE 
		ID_Company = @ID_Company;

    IF @Already_Exist_Count > 0
    BEGIN

		SELECT  
			@CompanyName = Name
		FROM    tCompany
		WHERE
			ID = @ID_Company

        SET @message = @CompanyName + ' is already exist.';
        THROW 50001, @message, 1;
    END;

	INSERT INTO [dbo].[tSMSPatientSOAP_Company]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy])
		 VALUES
			   (NULL
			   ,NULL
			   ,1
			   ,@ID_Company
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1)	
END

GO
