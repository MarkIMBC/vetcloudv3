﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveVaccinationOption] AS   
				SELECT   
				 H.*  
				FROM vVaccinationOption H  
				WHERE IsActive = 1
GO
