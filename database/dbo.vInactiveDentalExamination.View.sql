﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDentalExamination] AS   
				SELECT   
				 H.*  
				FROM vDentalExamination H  
				WHERE ISNULL(IsActive, 0) = 0
GO
