﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBusinessPartnerType] AS   
				SELECT   
				 H.*  
				FROM vBusinessPartnerType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
