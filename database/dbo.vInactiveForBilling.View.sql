﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveForBilling] AS   
				SELECT   
				 H.*  
				FROM vForBilling H  
				WHERE ISNULL(IsActive, 0) = 0
GO
