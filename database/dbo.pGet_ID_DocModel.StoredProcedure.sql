﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGet_ID_DocModel] ( @Model AS Varchar(max) )
AS
    SELECT  Oid, Name, TableName
    FROM    dbo.[_tModel]
    WHERE   Name LIKE '%'+@Model+'%'
GO
