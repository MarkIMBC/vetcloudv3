﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUserSession] AS   
				SELECT   
				 H.*  
				FROM vUserSession H  
				WHERE IsActive = 1
GO
