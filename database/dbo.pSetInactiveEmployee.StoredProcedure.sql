﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pSetInactiveEmployee] (@IDs_Employee   TYPINTLIST READONLY,
                                         @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_User INT = 0;
      DECLARE @ID_Company INT = 0;

      SELECT @ID_User = userSession.ID_User,
             @ID_Company = _user.ID_Company
      FROM   dbo.tUserSession userSession
             INNER JOIN vUser _user
                     ON userSession.ID_User = _user.ID
      WHERE  userSession.ID = @ID_UserSession;

      UPDATE tEmployee
      SET    IsActive = 0
      FROM   tEmployee Employee
             INNER JOIN @IDs_Employee record
                     ON Employee.ID = record.ID
      WHERE  Employee.ID_Company = @ID_Company

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END 
GO
