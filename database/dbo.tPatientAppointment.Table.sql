﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatientAppointment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient] [int] NULL,
	[DateStart] [datetime] NULL,
	[DateEnd] [datetime] NULL,
	[ID_Doctor] [int] NULL,
	[ID_ScheduleType] [int] NULL,
	[AppointmentStatus_ID_FilingStatus] [int] NULL,
	[ID_SOAPType] [int] NULL,
	[ID_Client] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[DateDone] [datetime] NULL,
	[ID_DoneBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[Appointment_ID_FilingStatus] [int] NULL,
	[Appointment_CancellationRemarks] [varchar](300) NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[DateRescheduletUpdated] [datetime] NULL,
	[DateReschedule] [datetime] NULL,
	[Groomer_ID_Employee] [int] NULL,
	[DateSent] [datetime] NULL,
	[IsSentSMS] [bit] NULL,
 CONSTRAINT [PK_tPatientAppointment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatientAppointment] ON [dbo].[tPatientAppointment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatientAppointment] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatientAppointment] ADD  DEFAULT ((2)) FOR [Appointment_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPatientAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tPatientAppointment_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatientAppointment] CHECK CONSTRAINT [FK_tPatientAppointment_ID_Company]
GO
ALTER TABLE [dbo].[tPatientAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tPatientAppointment_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientAppointment] CHECK CONSTRAINT [FK_tPatientAppointment_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatientAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tPatientAppointment_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientAppointment] CHECK CONSTRAINT [FK_tPatientAppointment_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatientAppointment]
		ON [dbo].[tPatientAppointment]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatientAppointment
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatientAppointment hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatientAppointment] ENABLE TRIGGER [rDateCreated_tPatientAppointment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatientAppointment]
		ON [dbo].[tPatientAppointment]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatientAppointment
			SET    DateModified = GETDATE()
			FROM   dbo.tPatientAppointment hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatientAppointment] ENABLE TRIGGER [rDateModified_tPatientAppointment]
GO
