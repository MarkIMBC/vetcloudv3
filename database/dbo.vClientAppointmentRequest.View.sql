﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vClientAppointmentRequest]
AS
  SELECT H.*,
         UC.Name                  CreatedBy,
         UM.Name                  LastModifiedBy,
         client.Name              Name_Client,
         patient.Name             Name_Patient,
         fs.Name                  Name_FilingStatus,
         soapType.Name            Name_SOAPType,
         _AttendingPhysician.Name AttendingPhysician_Name_Employee
  FROM   tClientAppointmentRequest H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType

GO
