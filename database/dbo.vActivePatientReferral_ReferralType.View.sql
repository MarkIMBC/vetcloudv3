﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatientReferral_ReferralType] AS   
    SELECT   
     H.*  
    FROM vPatientReferral_ReferralType H  
    WHERE IsActive = 1
GO
