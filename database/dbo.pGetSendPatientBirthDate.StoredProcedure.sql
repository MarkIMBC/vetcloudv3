﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetSendPatientBirthDate](@BirthDate        VARCHAR(MAX),
                                            @IsSMSSent        Bit = NULL,
                                            @IDsCompanyString VARCHAR(MAX))
AS
  BEGIN
      DECLARE @Success BIT = 1;

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           ID_Client            INT,
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           ID_Patient           INT,
           Name_Patient         VARCHAR(MAX),
           DateBirth            DateTime,
           Age                  VARCHAR(MAX),
           Message              VARCHAR(MAX)
        )

      INSERT @record
      SELECT *
      FROM   dbo.[fGetSendPatientBirthDate](@BirthDate, @IsSMSSent, @IDsCompanyString)

      SELECT @Success Success;

      SELECT FORMAT(DateBirth, 'MM-dd')                        DateBirth,
             tbl.Name_Company,
             Count(*)                                          Count,
             SUM(dbo.fGetITextMessageCreditCount(tbl.Message)) TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateBirth, 'MM-dd'),
                Name_Company
      Order  BY DateBirth DESC,
                Name_Company

      SELECT rec.*,
             dbo.fGetITextMessageCreditCount(rec.Message) ConsumedSMSCredit,
             LEN(Message)                                 CharLength
      FROM   @record rec
             inner join tCompany com
                     on com.ID = rec.ID_Company
      Order  BY FORMAT(DateBirth, 'MM-dd') DESC,
                com.ID_PackagePlan DESC,
                Name_Company,
                Name_Client
  END

GO
