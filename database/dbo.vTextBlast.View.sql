﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vTextBlast]
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         fs.Name Name_FilingStatus
  FROM   tTextBlast H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus

GO
