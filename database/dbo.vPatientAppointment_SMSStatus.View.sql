﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatientAppointment_SMSStatus] AS   
    SELECT   
     H.* ,  
     UC.Name AS CreatedBy,  
     UM.Name AS LastModifiedBy  
    FROM tPatientAppointment_SMSStatus H  
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID   
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
GO
