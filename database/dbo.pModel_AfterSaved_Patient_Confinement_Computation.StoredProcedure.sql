﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROC [dbo].[pModel_AfterSaved_Patient_Confinement_Computation](@IDs_Patient_Confinement TYPINTLIST READONLY)  
AS  
  BEGIN  
      DECLARE @ComputeditemsServices TABLE  
        (  
           ID_Patient_Confinement               INT,  
           ID_Patient_Confinement_ItemsServices INT,  
           Quantity                             INT,  
           UnitPrice                            DECIMAL(18, 4),  
           Amount                               DECIMAL(18, 4)  
        )  
      DECLARE @ComputedPatient_Confinement TABLE  
        (  
           ID_Patient_Confinement INT,  
           TotalAmount            DECIMAL(18, 4)  
        )  
  
      /*  
      Todo: Need I update by backend ang unit price and cost   
           but need to fix qng bakit nwawala ang unit cost and unit price   
      sa Confinement from SOAP  
     
   
      */  
         exec pReUpdateConfinementsRecordUnitPriceUnitCost  
        @IDs_Patient_Confinement 
  
      INSERT @ComputeditemsServices  
             (ID_Patient_Confinement,  
              ID_Patient_Confinement_ItemsServices,  
              Quantity,  
              UnitPrice)  
      SELECT itemServices.ID_Patient_Confinement,  
             itemServices.ID,  
             Quantity,  
             UnitPrice  
      FROM   tPatient_Confinement_ItemsServices itemServices  
             INNER JOIN @IDs_Patient_Confinement ids  
                     ON itemServices.ID_Patient_Confinement = ids.ID  
  
      /*Compute Amount on Items Services*/  
      UPDATE @ComputeditemsServices  
      SET    Amount = CONVERT(DECIMAL(18, 4), Quantity) * IsNull(UnitPrice, 0)  
  
      /*Compute Total Amount Patient Computation*/  
      INSERT @ComputedPatient_Confinement  
             (ID_Patient_Confinement,  
              TotalAmount)  
      SELECT ID_Patient_Confinement,  
             Sum(Amount) TotalAmount  
      FROM   @ComputeditemsServices  
      GROUP  BY ID_Patient_Confinement  
  
      /*Update Patient_Confinement_ItemsServices*/  
      UPDATE tPatient_Confinement_ItemsServices  
      SET    Amount = itemServComputed.Amount,  
             UnitPrice = IsNull(itemServices.UnitPrice, 0)  
      FROM   tPatient_Confinement_ItemsServices itemServices  
             INNER JOIN @ComputeditemsServices itemServComputed  
                     ON itemServices.ID = itemServComputed.ID_Patient_Confinement_ItemsServices  
  
      /*Update Patient_Confinement*/  
      UPDATE tPatient_Confinement  
      SET    SubTotal = ComputedConfinement.TotalAmount,  
             TotalAmount = ComputedConfinement.TotalAmount  
      FROM   tPatient_Confinement confinement  
             INNER JOIN @ComputedPatient_Confinement ComputedConfinement  
                     ON confinement.ID = ComputedConfinement.ID_Patient_Confinement  
  END  
  
GO
