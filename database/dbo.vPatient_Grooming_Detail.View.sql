﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_Grooming_Detail]
AS
  SELECT H.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         item.Name Name_Item
  FROM   tPatient_Grooming_Detail H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItem item
                ON H.ID_Item = item.ID

GO
