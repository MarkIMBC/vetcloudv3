﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_vPatient_SOAP_ListView](
	[Name] [varchar](200) NULL,
	[Date] [datetime] NULL,
	[Code] [varchar](50) NULL,
	[Name_SOAPType] [varchar](200) NULL,
	[AttendingPersonel] [varchar](300) NULL,
	[Name_Client] [varchar](2000) NULL,
	[Name_Patient] [varchar](200) NULL,
	[History] [varchar](8000) NOT NULL,
	[ClinicalExamination] [varchar](3000) NOT NULL,
	[Interpretation] [varchar](3000) NOT NULL,
	[Diagnosis] [varchar](3000) NOT NULL,
	[Treatment] [varchar](3000) NOT NULL,
	[ClientCommunication] [varchar](3000) NOT NULL,
	[Prescription] [varchar](8000) NOT NULL,
	[Name_FilingStatus] [varchar](200) NULL
) ON [PRIMARY]
GO
