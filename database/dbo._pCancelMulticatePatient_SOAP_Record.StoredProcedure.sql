﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[_pCancelMulticatePatient_SOAP_Record]
as
  BEGIN
      DECLARE @table TABLE
        (
           Date                DateTime,
           ID_Company          INT,
           Name_Client         VARCHAR(MAX),
           Name_Patient        VARCHAR(MAX),
           History             VARCHAR(MAX),
           ClientCommunication VARCHAR(MAX),
           Interpretation      VARCHAR(MAX),
           DIagnosis           VARCHAR(MAX),
           Prescription        VARCHAR(MAX),
           Count               Int,
           DateModifiedMin     Datetime,
           DateModifiedMax     DateTime,
           IntervalSecond      INt,
           ID_Patient_SOAP_Min INT,
           ID_Patient_SOAP_MAX INT
        )

      INSERT @table
      SELECT soap_.Date,
             soap_.ID_Company,
             soap_.Name_Client,
             soap_.Name_Patient,
             soap_.History,
             soap_.ClientCommunication,
             soap_.Interpretation,
             soap_.Diagnosis,
             soap_.Prescription,
             COUNT(*),
             MIN(soap_.DateModified) DateModifiedMin,
             MAX(soap_.DateModified) DateModifiedMax,
             DATEDIFF(SECOND, CONVERT(datetime, MIN(soap_.DateModified)), MAX(soap_.DateModified)),
             Min(soap_.ID),
             MAX(soap_.ID)
      FROM   vPatient_SOAP soap_
             inner join vCompanyActive c
                     on soap_.ID_Company = c.ID
      WHERE  ISNULL(soap_.ID_FilingStatus, 0) NOT IN ( 4, 0 )
      GROUP  BY soap_.Date,
                soap_.ID_Company,
                soap_.Name_Client,
                soap_.Name_Patient,
                soap_.History,
                soap_.ClientCommunication,
                soap_.Interpretation,
                soap_.Diagnosis,
                soap_.Prescription
      HAVING Count(*) > 1
      Order  by soap_.Date DESC,
                DATEDIFF(SECOND, CONVERT(datetime, MIN(soap_.DateModified)), MAX(soap_.DateModified)) DESC

      DEClare @IDs_Patient_SOAP typIntlist

      INSERT @IDs_Patient_SOAP
      SELECT soap_.ID
      FROM   tPatient_SOAP soap_
             inner join @table tbl
                     on soap_.ID = tbl.ID_Patient_SOAP_Min
      Order  by soap_.Date DESC,
                soap_.DateModified DESC

      Update tPatient_SOAP
      SET    ID_FilingStatus = NULL,
             Comment = CASE
                         WHEn LEN(ISNULL(Comment, '')) > 0 then CHAR(13) + CHAR(13)
                         ELSE ''
                       END
                       + 'Cancel via system '
                       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt')
      where  ID IN (SELECT ID
                    FROM   @IDs_Patient_SOAP)

      SELECT *
      FROM   @table
  END

GO
