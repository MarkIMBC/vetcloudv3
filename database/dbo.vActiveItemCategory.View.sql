﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveItemCategory] AS   
				SELECT   
				 H.*  
				FROM vItemCategory H  
				WHERE IsActive = 1
GO
