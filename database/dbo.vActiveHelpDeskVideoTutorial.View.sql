﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveHelpDeskVideoTutorial] AS   
				SELECT   
				 H.*  
				FROM vHelpDeskVideoTutorial H  
				WHERE IsActive = 1
GO
