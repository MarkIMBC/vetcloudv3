﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vzExpensesReport]
as
  SELECT payable.ID,
         payable.Date,
         FORMAT(payable.Date, 'yyyy-MM-dd') FormattedDate,
         payableDetail.ID_ExpenseCategory,
         category.Name                      Name_ExpenseCategory,
         payableDetail.Name,
         payable.TotalAmount,
         payable.PaidAmount,
         payable.RemaningAmount,
         fs.Name                            Payment_Name_FilingStatus,
         company.ID                         ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                       Name_Company,
         company.Address                    Address_Company,
         company.ContactNumber              ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                              HeaderInfo_Company
  FROM   tPayable payable WITH (NOLOCK)
         INNER JOIN tPayable_Detail payableDetail WITH (NOLOCK)
                 on payable.ID = payableDetail.ID_Payable
         INNER JOIN tFilingStatus fs WITH (NOLOCK)
                 on payable.Payment_ID_FilingStatus = fs.ID
         INNER JOIN tExpenseCategory category WITH (NOLOCK)
                 on payableDetail.ID_ExpenseCategory = category.ID
         INNER JOIN vCompany company WITH (NOLOCK)
                 on payable.ID_Company = company.ID 
GO
