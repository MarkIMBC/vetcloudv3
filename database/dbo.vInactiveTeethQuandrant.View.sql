﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTeethQuandrant] AS   
				SELECT   
				 H.*  
				FROM vTeethQuandrant H  
				WHERE ISNULL(IsActive, 0) = 0
GO
