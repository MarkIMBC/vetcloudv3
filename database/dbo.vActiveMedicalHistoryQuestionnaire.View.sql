﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveMedicalHistoryQuestionnaire] AS   
				SELECT   
				 H.*  
				FROM vMedicalHistoryQuestionnaire H  
				WHERE IsActive = 1
GO
