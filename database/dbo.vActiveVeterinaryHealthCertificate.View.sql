﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveVeterinaryHealthCertificate] AS   
				SELECT   
				 H.*  
				FROM vVeterinaryHealthCertificate H  
				WHERE IsActive = 1
GO
