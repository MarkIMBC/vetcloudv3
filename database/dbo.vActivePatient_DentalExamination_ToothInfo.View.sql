﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_DentalExamination_ToothInfo] AS   
				SELECT   
				 H.*  
				FROM vPatient_DentalExamination_ToothInfo H  
				WHERE IsActive = 1
GO
