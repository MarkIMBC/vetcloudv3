﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pUpdateLatestPatientToWaitingListStatus](@IDs_Patient                   typIntList READONLY,
                                                   @WaitingStatus_ID_FilingStatus INT,
                                                   @ID_UserSession                INT)
AS
  BEGIN
      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      SELECT MAX(patientWaitingList.ID)
      FROM   tPatientWaitingList patientWaitingList
             inner join @IDs_Patient patient
                     on patientWaitingList.ID_Patient = patient.ID
      GROUP  BY patientWaitingList.ID_Patient

      exec pUpdatePatientToWaitingListStatus
        @IDs_PatientWaitingList,
        null,
        @WaitingStatus_ID_FilingStatus,
        @ID_UserSession
  END

GO
