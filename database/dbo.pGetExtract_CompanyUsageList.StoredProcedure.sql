﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetExtract_CompanyUsageList]
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @ID_Main INT = 1
      Declare @ComapanyUsageList TABLE
        (
           ID_Company       INT,
           Name_Company     VARCHAR(MAX),
           LastDateAccess   DateTime,
           TotalAccessCount INT,
           LastBillDate     DateTime,
           TotalBillCount   INT,
           LastSOAPDate     DateTime,
           TotalSOAPCount   INT
        )

      INSERT @ComapanyUsageList
      SELECT *
      FROM   dbo.fGetCompanyUsageList()

      SELECT '_',
             ''             AS Main,
             'Main.ID_Main' AS CompanyUsage

      SELECT @Success Success;

      --Main
      SELECT @ID_Main                    ID,
             Format(GETDATE(), 'yyyy-MM-dd') DateString

      --CompanyUsage
      SELECT *,
             @ID_Main ID_Main
      FROM   @ComapanyUsageList
      ORDER  BY Name_Company
  END

GO
