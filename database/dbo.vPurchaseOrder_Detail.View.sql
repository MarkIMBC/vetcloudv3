﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPurchaseOrder_Detail]
as
  SELECT DET.*,
         I.Name                   AS Item,
         UOM.Name                 AS UOM,
         I.Name                   AS Name_Item,
         UOM.Name                 AS Name_UOM,
         purchaseOrder.ID_Company Main_ID_Company
  FROM   tPurchaseOrder_Detail DET
         LEFT JOIN tItem I
                ON DET.ID_Item = I.ID
         LEFT JOIN tUnitOfMeasure UOM
                ON DET.ID_UOM = UOM.ID
         LEFT JOIN vPurchaseOrder purchaseOrder
                on purchaseOrder.ID = DET.ID_PurchaseOrder

GO
