﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Vaccination_Schedule_SMSStatus] AS   
				SELECT   
				 H.*  
				FROM vPatient_Vaccination_Schedule_SMSStatus H  
				WHERE IsActive = 1
GO
