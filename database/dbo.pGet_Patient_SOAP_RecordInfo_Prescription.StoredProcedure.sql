﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pGet_Patient_SOAP_RecordInfo_Prescription](@IDs_Patient_SOAP TYPINTLIST READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @___detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @___detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT _prescription.ID_Patient_SOAP,
             TagString = STUFF((SELECT ''
                                       + Case
                                           WHEN item.ID_ItemType = 1 THEN 'Service: '
                                           ELSE
                                             Case
                                               WHEN item.ID_ItemType = 2 THEN 'Item: '
                                               ELSE 'Other: '
                                             END
                                         END
                                       + ISNULL(item.Name, '') + '/*break*/ '
                                       + 'Quantity: ' + FORMAT(Quantity, '#,#0')
                                       + '/*break*/ '
                                       + CASE
                                           WHEN LEN(ISNULL(__prescription.Comment, '')) > 0 THEN 'Reason: ' + __prescription.Comment
                                           ELSE ''
                                         END
                                       + ' ' + '/*break*/ '
                                FROM   tPatient_SOAP_Prescription __prescription WITH (NOLOCK)
                                       INNER JOIN tItem item WITH (NOLOCK)
                                               on item.ID = __prescription.ID_Item
                                WHERE  _prescription.ID_Patient_SOAP = __prescription.ID_Patient_SOAP
                                Order  by __prescription.ID ASC
                                FOR XML PATH ('')), 1, 0, ''),
             COUNT(*)  Count
      FROM   tPatient_SOAP_Prescription _prescription WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _prescription.ID_Patient_SOAP = ids.ID
      GROUP  BY _prescription.ID_Patient_SOAP

      Update @detail
      set    TagString = det2.TagString,
             Count = det2.Count
      FROM   @detail det1
             INNER JOIN @___detail det2
                     on det1.ID_Patient_SOAP = det2.ID_Patient_SOAP

      UPdate @detail
      SET    TagString = TagString + '/*break*/ '
      FROM   @detail det
             INNER JOIN tPatient_SOAP _soap
                     on det.ID_Patient_SOAP = _soap.ID
      where  LEN(ISNULL(_soap.Prescription, '')) > 0

      UPdate @detail
      SET    TagString = TagString + 'Others:/*break*/ '
                         + ISNULL(_soap.Prescription, '')
      FROM   @detail det
             INNER JOIN tPatient_SOAP _soap
                     on det.ID_Patient_SOAP = _soap.ID
      where  LEN(ISNULL(_soap.Prescription, '')) > 0

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END 
GO
