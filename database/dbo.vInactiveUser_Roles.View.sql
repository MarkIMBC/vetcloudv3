﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUser_Roles] AS   
				SELECT   
				 H.*  
				FROM vUser_Roles H  
				WHERE ISNULL(IsActive, 0) = 0
GO
