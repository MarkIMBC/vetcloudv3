﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveAuditTrail_Detail] AS   
				SELECT   
				 H.*  
				FROM vAuditTrail_Detail H  
				WHERE ISNULL(IsActive, 0) = 0
GO
