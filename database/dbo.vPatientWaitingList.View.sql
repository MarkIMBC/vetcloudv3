﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatientWaitingList]
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         client.Name  Name_Client,
         patient.Name Name_Patient,
         waitfs.Name  WaitingStatus_Name_FilingStatus,
         billfs.Name  BillingInvoice_Name_FilingStatus,
         _model.Name  Name_Model_Reference
  FROM   tPatientWaitingList H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON H.ID_Client = client.ID
         LEFT JOIN tPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN tFilingStatus waitfs
                ON H.WaitingStatus_ID_FilingStatus = waitfs.ID
         LEFT JOIN tFilingStatus billfs
                ON H.BillingInvoice_ID_FilingStatus = billfs.ID
         LEFT JOIN _tModel _model
                on H.Oid_Model_Reference = _model.Oid

GO
