﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSupplier] AS   
				SELECT   
				 H.*  
				FROM vSupplier H  
				WHERE IsActive = 1
GO
