﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertCompanyCustomReportObjectValue](@ID_Company   INT,
                                                 @Oid_Report   VARCHAR(MAX),
                                                 @ObjectName   VARCHAR(MAX),
                                                 @PropertyName VARCHAR(MAX),
                                                 @Value        VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_CompanyCustomReportObjectValue INT = 0

      IF @ID_Company IS NULL
        return;

      IF(SELECT COUNT(*)
         FROM   dbo.tCompanyCustomReportObjectValue
         WHERE  Oid_Report = @Oid_Report
                AND ID_Company = @ID_Company
                AND ObjectName = @ObjectName
                AND PropertyName = @PropertyName) = 0
        BEGIN
            INSERT INTO [dbo].tCompanyCustomReportObjectValue
                        ([Name],
                         [IsActive],
                         [Oid_Report],
                         ObjectName,
                         PropertyName,
                         ID_Company)
            VALUES      (@ObjectName + '.' + @PropertyName,
                         1,
                         @Oid_Report,
                         @ObjectName,
                         @PropertyName,
                         @ID_Company)
        END

      SELECT @ID_CompanyCustomReportObjectValue = ID
      FROM   dbo.tCompanyCustomReportObjectValue
      WHERE  Oid_Report = @Oid_Report
             AND ID_Company = @ID_Company
             AND ObjectName = @ObjectName
             AND PropertyName = @PropertyName

      UPDATE dbo.tCompanyCustomReportObjectValue
      SET    Value = @Value
      WHERE  ID = @ID_CompanyCustomReportObjectValue
  END

GO
