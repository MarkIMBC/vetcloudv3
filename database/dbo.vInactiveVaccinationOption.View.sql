﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveVaccinationOption] AS   
				SELECT   
				 H.*  
				FROM vVaccinationOption H  
				WHERE ISNULL(IsActive, 0) = 0
GO
