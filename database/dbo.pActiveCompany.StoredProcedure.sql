﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pActiveCompany](@GUID_Company VARCHAR(MAX))
as
  begin
      IF(SELECT Count(*)
         FROM   vCompany
         WHERE  Guid = @GUID_Company) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   tCompany
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------
      if OBJECT_ID('dbo.tCompany_IsActiveLog') is null
        BEGIN
            exec _pCreateAppModuleWithTable
              'tCompany_IsActiveLog',
              1,
              1,
              NULL

            exec _pAddModelProperty
              'tCompany_IsActiveLog',
              'Date',
              5

            exec _pRefreshAllViews
        END

      Update tCompany
      SET    IsActive = 1
      FROM   tCompany company
      WHERE  ID = @ID_Company


      DECLARE @Date Datetime = getDAte()

      exec pInsertCompany_IsActiveLog
        @ID_Company,
        1,
        @Date
  END

GO
