﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vClientCredit_ListView]
AS
  SELECT creditLogs.ID,
         creditLogs.Code,
         creditLogs.ID_Client,
         client.Name Name_Client,
         creditLogs.ID_Patient,
         creditLogs.Name_Patient,
         creditLogs.Date,
         creditLogs.Comment,
         creditLogs.CreditAmount,
         creditLogs.ID_FilingStatus,
         creditLogs.Name_FilingStatus,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) > 0 THEN creditLogs.CreditAmount
           ELSE 0
         END         DepositAmount,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) < 0 THEN abs(creditLogs.CreditAmount)
           ELSE 0
         END         WithdrawAmount,
         deposit.Code_Patient_Confinement,
         deposit.ID_Patient_Confinement
  FROM   vClient_CreditLogs creditLogs
         INNER JOIN tClient client
                 ON client.ID = creditLogs.ID_Client
         LEFT join vClientDeposit deposit
                on creditLogs.Code = deposit.Code
                   and creditLogs.ID_Company = deposit.ID_Company
                   and deposit.ID_Client = client.ID

GO
