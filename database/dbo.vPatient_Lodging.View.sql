﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_Lodging]
AS
  SELECT H.*,
         client.Name  Name_Client,
         patient.Name Name_Patient,
         fs.Name      Name_FilingStatus
  FROm   TPatient_Lodging H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                on patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                on fs.ID = H.ID_FilingStatus

GO
