﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelClientDeposit] (@IDs_ClientDeposit typIntList READONLY,
                                         @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tClientDeposit
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientDeposit bi
                 INNER JOIN @IDs_ClientDeposit ids
                         ON bi.ID = ids.ID;

          -- Subtract Deposit on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.DepositAmount * -1,
                 Code,
                 'Cancel Deposit'
          FROM   tClientDeposit cd
                 INNER JOIN @IDs_ClientDeposit ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
