﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInActiveCompany](@GUID_Company VARCHAR(MAX))
as
  begin
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   tCompany
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------
      if OBJECT_ID('dbo.tCompany_IsActiveLog') is null
        BEGIN
            exec _pCreateAppModuleWithTable
              'tCompany_IsActiveLog',
              1,
              1,
              NULL

            exec _pAddModelProperty
              'tCompany_IsActiveLog',
              'Date',
              5

            exec _pRefreshAllViews
        END

      Update tCompany
      SET    IsActive = 0
      FROM   tCompany company
      WHERE  IsActive = 1
             and ID = @ID_Company

      Update tUserSession
      set    IsActive = 0
      FROm   tUserSession _userSession
             inner join vUser _user
                     on _userSession.ID_User = _user.ID
             INNER Join vCompany company
                     on company.ID = _user.ID_Company
      WHERE  _userSession.IsActive = 1
             and company.ID = @ID_Company

      DECLARE @Date Datetime = getDAte()

      exec pInsertCompany_IsActiveLog
        @ID_Company,
        0,
        @Date
  END

GO
