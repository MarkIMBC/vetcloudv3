﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveApproverMatrix] AS   
				SELECT   
				 H.*  
				FROM vApproverMatrix H  
				WHERE ISNULL(IsActive, 0) = 0
GO
