﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 PROC [dbo].[pCancel_ClientAppointmentRequest] (@IDs_ClientAppointment typIntList READONLY,
                                                     @ID_UserSession        INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @IDs_Patient typIntList

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelPatient_SOAP_validation
            @IDs_ClientAppointment,
            @ID_UserSession;

          UPDATE dbo.tClientAppointmentRequest
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientAppointmentRequest bi
                 INNER JOIN @IDs_ClientAppointment ids
                         ON bi.ID = ids.ID;
      --exec dbo.pUpdatePatientsLastVisitedDate
      --  @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
