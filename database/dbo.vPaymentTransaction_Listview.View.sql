﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPaymentTransaction_Listview]
AS
  SELECT ID,
         Date,
         Code,
         DateString,
         Name_Client,
         Name_Patient,
         Name_PaymentMethod,
         PayableAmount,
         PaymentAmount,
         ChangeAmount,
         Name_FilingStatus,
         ID_Company
  FROM   dbo.vPaymentTransaction

GO
