﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetModelReports](@CompanyID INT,
                                    @Oid_Model VARCHAR(200))
AS
  BEGIN
      DECLARE @Success BIT = 1;

      SELECT '_',
             '' AS Reports

      SELECT @Success Success

      SELECT model.Name       Name_Model,
             modelReport.Oid_Report,
             modelReport.Name Name_ModelReport,
             report.Name      Name_Report
      FROM   _tModelReport modelReport
             INNER JOIN _tMOdel model
                     ON model.Oid = modelReport. Oid_Model
             INNER JOIN _tReport report
                     ON report.Oid = modelReport. Oid_Report
      WHERE  Lower(modelReport.Oid_Model) = Lower(@Oid_Model)
             AND modelReport.ID_Company = @CompanyID
             AND modelReport.IsActive = 1
      ORDER  BY report.Name
  END

GO
