﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingInvoiceWalkInList] AS   
				SELECT   
				 H.*  
				FROM vBillingInvoiceWalkInList H  
				WHERE ISNULL(IsActive, 0) = 0
GO
