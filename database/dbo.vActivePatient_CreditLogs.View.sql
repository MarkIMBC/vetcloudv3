﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_CreditLogs] AS   
				SELECT   
				 H.*  
				FROM vPatient_CreditLogs H  
				WHERE IsActive = 1
GO
