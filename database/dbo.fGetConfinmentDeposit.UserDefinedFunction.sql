﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetConfinmentDeposit] (@ID_Patient_Confinement INT,
                                      @IDs_Patient            typIntList Readonly)
RETURNS DECIMAL(18, 4)
AS
  BEGIN
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3

      IF(SELECT COUNT(*)
         FROM   @IDs_Patient) = 0
        BEGIN
            SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
            FROM   tClientDeposit
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END
      ELSE
        BEGIN
            SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
            FROM   tClientDeposit clientDeposit
                   inner join @IDs_Patient idsPatient
                           on clientDeposit.ID_Patient = idsPatient.ID
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END

      RETURN @ConfinementDepositAmount
  END

GO
