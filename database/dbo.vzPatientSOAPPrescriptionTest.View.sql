﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[vzPatientSOAPPrescriptionTest]  
AS  
  SELECT  *, dbo.fGetPatient_SOAP_Prescription_String(ID) _Prescription FROM vzPatientSOAPPrescription WHERE ID = 1266163
GO
