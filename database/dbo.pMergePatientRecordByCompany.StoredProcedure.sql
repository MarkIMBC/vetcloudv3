﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pMergePatientRecordByCompany](@GUID_Company             VARCHAR(MAX),
                                        @Source_Code_Patient      VARCHAR(MAX),
                                        @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergePatientRecordByCompany
        @ID_Company,
        @Source_Code_Patient,
        @Destination_Code_Patient
  END

GO
