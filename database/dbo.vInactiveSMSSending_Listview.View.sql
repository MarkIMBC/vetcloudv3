﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vInactiveSMSSending_Listview]
AS
  SELECT hed.*
  FROM   vActiveInactiveSMSSending hed

GO
