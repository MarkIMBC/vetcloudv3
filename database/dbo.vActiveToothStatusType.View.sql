﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveToothStatusType] AS   
				SELECT   
				 H.*  
				FROM vToothStatusType H  
				WHERE IsActive = 1
GO
