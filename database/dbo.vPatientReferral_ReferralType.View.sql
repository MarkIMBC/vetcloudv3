﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatientReferral_ReferralType]
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         refType.Name Name_ReferralType
  FROM   tPatientReferral_ReferralType H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tReferralType refType
                ON H.ID_ReferralType = refType.ID

GO
