﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePurchaseOrder_Detail] AS   
				SELECT   
				 H.*  
				FROM vPurchaseOrder_Detail H  
				WHERE IsActive = 1
GO
