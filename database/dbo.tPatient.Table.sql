﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[FirstName] [varchar](300) NULL,
	[LastName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[ID_Gender] [int] NULL,
	[Email] [varchar](300) NULL,
	[DateBirth] [datetime] NULL,
	[FullAddress] [varchar](300) NULL,
	[ID_Country] [int] NULL,
	[ContactNumber] [varchar](300) NULL,
	[ID_Company] [int] NULL,
	[Species] [varchar](300) NULL,
	[ID_Client] [int] NULL,
	[IsNeutered] [bit] NULL,
	[IsDeceased] [bit] NULL,
	[Old_patient_id] [int] NULL,
	[AnimalWellness] [varchar](7000) NULL,
	[DateDeceased] [datetime] NULL,
	[DateLastVisited] [datetime] NULL,
	[CurrentCreditAmount] [decimal](18, 4) NULL,
	[ProfileImageFile] [varchar](300) NULL,
	[CustomCode] [varchar](300) NULL,
	[WaitingStatus_ID_FilingStatus] [int] NULL,
	[Microchip] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
	[Color] [varchar](300) NULL,
	[Idiosyncrasies] [varchar](300) NULL,
	[LastAttendingPhysician_ID_Employee] [int] NULL,
	[Last_ID_Patient_SOAP] [int] NULL,
 CONSTRAINT [PK_tPatient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient] ADD  CONSTRAINT [DF__tPatient__IsActi__7B663F43]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient]  WITH NOCHECK ADD  CONSTRAINT [FK_tPatient_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient] CHECK CONSTRAINT [FK_tPatient_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient]  WITH NOCHECK ADD  CONSTRAINT [FK_tPatient_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient] CHECK CONSTRAINT [FK_tPatient_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient]
		ON [dbo].[tPatient]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient] ENABLE TRIGGER [rDateCreated_tPatient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient]
		ON [dbo].[tPatient]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient] ENABLE TRIGGER [rDateModified_tPatient]
GO
