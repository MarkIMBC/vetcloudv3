﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompany_Subscription] AS   
				SELECT   
				 H.*  
				FROM vCompany_Subscription H  
				WHERE ISNULL(IsActive, 0) = 0
GO
