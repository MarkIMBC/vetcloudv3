﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vNonSystemUseEmployee]  
AS  
  SELECT H.*  
  FROM   vEmployee H  
  WHERE  ISNULL(IsSystemUsed, 0) = 0  
         and h.FirstName NOT IN ( 'Demo Sole', 'Receptionportal', 'ClientForm' )  
         and IsActive = 1   
GO
