﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEmployeeStatus] AS   
				SELECT   
				 H.*  
				FROM vEmployeeStatus H  
				WHERE ISNULL(IsActive, 0) = 0
GO
