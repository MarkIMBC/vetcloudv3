﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @records TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @records2 TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @MaxRecords TABLE
        (
           ID_Patient  INT,
           DateCreated DATETIME
        )

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_SOAP hed WITH (NOLOCK)
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_SOAP'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_Wellness hed WITH (NOLOCK)
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_Wellness'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @MaxRecords
             (ID_Patient,
              DateCreated)
      SELECT ID_Patient,
             MAX(DateCreated)
      FROM   @records
      GROUP  BY ID_Patient

      INSERT @records2
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT record.ID_Patient,
             record.TableName,
             record.DateCreated,
             record.Count
      FROM   @records record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_SOAP hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_SOAP'

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_Wellness hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_Wellness'

      --SELECT *  
      --FROM   @records2 record  
      UPDATE tPatient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tPatient patient WITH (NOLOCK)
             INNER JOIN @records2 record
                     ON record.ID_Patient = patient.ID

      UPDATE tClient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tClient client WITH (NOLOCK)
             INNER JOIN @records2 record
                     ON record.ID_Client = client.ID
  END

GO
