﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelSalesReturn_validation] (@IDs_SalesReturn typIntList READONLY,
                                                  @ID_UserSession  INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @PartiallyServed_ID_FilingStatus INT = 5;
      DECLARE @FullyServed_ID_FilingStatus INT = 6;
      DECLARE @OverServed_ID_FilingStatus INT = 7;
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Text Blast Status is not Approved*/
      INSERT @ValidateNotApproved
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vSalesReturn bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_SalesReturn ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus, @Done_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO
