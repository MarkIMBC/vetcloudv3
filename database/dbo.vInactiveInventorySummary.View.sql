﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveInventorySummary] AS   
				SELECT   
				 H.*  
				FROM vInventorySummary H  
				WHERE ISNULL(IsActive, 0) = 0
GO
