﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CReate     
 PROC [dbo].[pAutoGenerateClientPerCompany](@ID_Company INT)  
as  
  BEGIN  
      DECLARE @IDs_Client TABLE  
        (  
           RowID                 INT,  
           ID_Client INT  
        )  
      DECLARE @currentCounter INT = 1  
      DECLARE @maxCounter INT = 1  
  
      INSERT @IDs_Client  
      SELECT ROW_NUMBER()  
               OVER(  
                 ORDER BY ID ASC) AS RowID,  
             ID  
      FROM   tClient  
      WHERE  ID_Company = @ID_Company  
             AND ISNULL(Code, '') = ''  
  
      SELECT @maxCounter = COUNT(*)  
      FROM   @IDs_Client  
  
      --IF( @maxCounter > 0 )  
      --  SELECT Name,  
      --         @maxCounter  
      --  FROM   tCompany  
      --  WHERE  ID = @ID_Company  
      WHILE @currentCounter <= @maxCounter  
        BEGIN  
            DECLARE @ID_Client INT = 0  
  
            SELECT @ID_Client = ID_Client  
            FROM   @IDs_Client  
            WHERE  RowID = @currentCounter  
  
            exec [pModel_AfterSaved_Client]  
              @ID_Client,  
              1  
  
            SET @currentCounter = @currentCounter + 1  
        END  
  END  
GO
