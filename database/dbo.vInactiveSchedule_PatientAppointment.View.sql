﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveSchedule_PatientAppointment] AS   
				SELECT   
				 H.*  
				FROM vSchedule_PatientAppointment H  
				WHERE ISNULL(IsActive, 0) = 0
GO
