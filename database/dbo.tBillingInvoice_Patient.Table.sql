﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBillingInvoice_Patient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_BillingInvoice] [int] NULL,
	[ID_Patient] [int] NULL,
	[ID_Patient_Confinement_Patient] [int] NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tBillingInvoice_Patient] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBillingInvoice_Patient] ON [dbo].[tBillingInvoice_Patient]
(
	[ID_BillingInvoice] ASC,
	[ID_Patient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Patient_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] CHECK CONSTRAINT [FK_tBillingInvoice_Patient_ID_Company]
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Patient_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] CHECK CONSTRAINT [FK_tBillingInvoice_Patient_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient]  WITH CHECK ADD  CONSTRAINT [FK_tBillingInvoice_Patient_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] CHECK CONSTRAINT [FK_tBillingInvoice_Patient_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient]  WITH CHECK ADD  CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice])
REFERENCES [dbo].[tBillingInvoice] ([ID])
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] CHECK CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tBillingInvoice_Patient]
		ON [dbo].[tBillingInvoice_Patient]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice_Patient
			SET    DateCreated = GETDATE()
			FROM   dbo.tBillingInvoice_Patient hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] ENABLE TRIGGER [rDateCreated_tBillingInvoice_Patient]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tBillingInvoice_Patient]
		ON [dbo].[tBillingInvoice_Patient]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tBillingInvoice_Patient
			SET    DateModified = GETDATE()
			FROM   dbo.tBillingInvoice_Patient hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tBillingInvoice_Patient] ENABLE TRIGGER [rDateModified_tBillingInvoice_Patient]
GO
