﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateBillingInvoicePatientName](@IDs_BillingInvoice typIntList READONLY)
AS
  BEGIN
      DECLARE @table TABLE
        (
           ID_BIllingInvoice INT,
           PatientNames      VARCHAR(MAX)
        )

      Update tBillingInvoice
      SET    PatientNames = STUFF((SELECT ', ' + Name_Patient
                                   FROM   vBillingInvoice_Patient biP2
                                   WHERE  biPatient.ID_BillingInvoice = biP2.ID_BillingInvoice
                                   FOR XML PATH('')), 1, 1, '')
      FROM   tBillingInvoice bi
             inner join vBillingInvoice_Patient biPatient
                     on bi.ID = biPatient.ID_BillingInvoice
             INNER JOIN @IDs_BillingInvoice ids
                     on biPatient.ID_BillingInvoice = ids.ID
  END 
GO
