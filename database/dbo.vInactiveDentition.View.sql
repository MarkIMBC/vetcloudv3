﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDentition] AS   
				SELECT   
				 H.*  
				FROM vDentition H  
				WHERE ISNULL(IsActive, 0) = 0
GO
