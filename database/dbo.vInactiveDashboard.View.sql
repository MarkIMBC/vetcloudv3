﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveDashboard] AS   
				SELECT   
				 H.*  
				FROM vDashboard H  
				WHERE ISNULL(IsActive, 0) = 0
GO
