﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_SOAP_Plan_Count]
as
  select ID_Patient_SOAP,
         Count(*) TotalCount
  FROM   tPatient_SOAP_Plan
  GROUP  BY ID_Patient_SOAP

GO
