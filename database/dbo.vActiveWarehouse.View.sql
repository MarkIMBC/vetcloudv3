﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveWarehouse] AS   
				SELECT   
				 H.*  
				FROM vWarehouse H  
				WHERE IsActive = 1
GO
