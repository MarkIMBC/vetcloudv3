﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompany] AS   
				SELECT   
				 H.*  
				FROM vCompany H  
				WHERE IsActive = 1
GO
