﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGeneratePatientWaitingListFromTodayAppointmentEvent]
AS
    DECLARE @AppointmentIDs_Patient TABLE
      (
         Oid_Model                    VARCHAR(MAX),
         Appointment_ID_CurrentObject INT,
         ID_Patient                   INT
      )
    DECLARE @WaitingList_Patient typIntList
    DECLARE @OudatedWaiting_PatientWaitingList typIntList
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @Cancelled_ID_FilingStatus INT = 4
    DECLARE @ID_UserSession INT

    SELECT @ID_UserSession = MAX(ID)
    FROM   tUserSession
    WHERE  ID_USer = 10

    INSERT @OudatedWaiting_PatientWaitingList
    select DIstinct ID
    FROm   vPatientWaitingList
    where  CONVERT(Date, DateCreated) < CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0
           AND WaitingStatus_ID_FilingStatus IN ( @Waiting_ID_FilingStatus, @Ongoing_ID_FilingStatus )

    INSERT @AppointmentIDs_Patient
    select Oid_Model,
           Appointment_ID_CurrentObject,
           ID_Patient
    FROm   vAppointmentEvent
    where  CONVERT(Date, DateStart) = CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0

    INSERT @WaitingList_Patient
    select DIstinct ID_Patient
    FROm   vPatientWaitingList
    where  CONVERT(Date, DateCreated) = CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0

    Delete FROM @AppointmentIDs_Patient
    WHERE  ID_Patient IN (SELECT ID
                          FROM   @WaitingList_Patient)

    Update tPatient
    set    WaitingStatus_ID_FilingStatus = NULL
    FROM   tPatient patient
           inner join @OudatedWaiting_PatientWaitingList outdate
                   on patient.ID = outdate.ID

    -----------------------------------------------------------------------------  
    DECLARE @Oid_Model VARCHAR(max)
    DECLARE @Appointment_ID_CurrentObject INT
    DECLARE @ID_Patient INT
    DECLARE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent CURSOR FOR
      select Oid_Model,
             Appointment_ID_CurrentObject,
             ID_Patient
      FROm   @AppointmentIDs_Patient

    OPEN db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    FETCH NEXT FROM db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent INTO @Oid_Model, @Appointment_ID_CurrentObject, @ID_Patient

    WHILE @@FETCH_STATUS = 0
      BEGIN
          exec pAddAppointmentToWaitingList
            @Oid_Model,
            @Appointment_ID_CurrentObject,
            @ID_Patient,
            @ID_UserSession

          FETCH NEXT FROM db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent INTO @Oid_Model, @Appointment_ID_CurrentObject, @ID_Patient
      END

    CLOSE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    DEALLOCATE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    exec pAutoGeneratePatientWaitingList

    -----------------------------------------------------------------------------  
    exec pUpdatePatientToWaitingListStatus
      @OudatedWaiting_PatientWaitingList,
      @Cancelled_ID_FilingStatus,
      NULL,
      @ID_UserSession

GO
