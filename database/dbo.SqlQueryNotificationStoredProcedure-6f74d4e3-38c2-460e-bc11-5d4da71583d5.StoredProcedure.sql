﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-6f74d4e3-38c2-460e-bc11-5d4da71583d5] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5') > 0)   DROP SERVICE [SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5]; if (OBJECT_ID('SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-6f74d4e3-38c2-460e-bc11-5d4da71583d5]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-6f74d4e3-38c2-460e-bc11-5d4da71583d5]; END COMMIT TRANSACTION; END
GO
