﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveBillingInvoice_WaterReading] AS   
    SELECT   
     H.*  
    FROM vBillingInvoice_WaterReading H  
    WHERE IsActive = 1
GO
