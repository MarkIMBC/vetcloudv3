﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetPatientAppoinmentEventStuff](@DateStart  DateTime,
                                               @DateEnd    DateTime,
                                               @ID_Patient INT)
RETURNS @PateintAppoinmentEventStuff TABLE (
  ID_Patient        INT,
  ReferenceCodeList VARCHAR(MAX),
  UniqueIDList      VARCHAR(MAX),
  UniqueIDListTest  VARCHAR(MAX))
AS
  BEGIN
      DECLARE @table TABLE
        (
           Name_Model    VARCHAR(MAX),
           ReferenceCode VARCHAR(MAX),
           ID_Client     INT,
           ID_Patient    INT,
           Particular    VARCHAR(MAX),
           UniqueID      VARCHAR(MAX),
           Description   VARCHAR(MAX)
        )

      INSERT @table
      SELECT DISTINCT _model.Name,
                      ReferenceCode,
                      ID_Client,
                      ID_Patient,
                      Paticular,
                      UniqueID,
                      Description
      FROM   vAppointmentEvent appntEvent
             inner join _tModel _model
                     on appntEvent.Oid_Model = _model.Oid
      WHERE  CONVERT(Date, DateStart) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)
             AND ID_Patient IN ( @ID_Patient )

      INSERT @PateintAppoinmentEventStuff
      select t.ID_Patient,
             STUFF((SELECT DISTINCT ', ' + ReferenceCode
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS ReferenceCodeList,
             STUFF((SELECT DISTINCT ', ' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniquiIDList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniquiIDListTest
      FROM   @table t
      GROUP  BY t.ID_Patient

      RETURN;
  END

GO
