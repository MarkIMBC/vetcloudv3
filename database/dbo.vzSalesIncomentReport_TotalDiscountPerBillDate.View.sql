﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 
   
CREATE View [dbo].[vzSalesIncomentReport_TotalDiscountPerBillDate]  
as  
  SELECT biHed.ID_Company               ID_Company1,  
         Convert(Date, Date)            Date,  
         SUM(ISNULL(DiscountAmount, 0)  
             + TotalItemDiscountAmount) TotalDiscountAmountPerDate  
  FROM   tBillingInvoice biHed  
  WHERE  biHed.ID_FilingStatus IN ( 3 )  
  GROUP  BY biHed.ID_Company,  
            Convert(Date, Date)  
  
GO
