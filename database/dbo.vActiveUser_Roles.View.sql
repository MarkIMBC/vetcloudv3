﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveUser_Roles] AS   
				SELECT   
				 H.*  
				FROM vUser_Roles H  
				WHERE IsActive = 1
GO
