﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROCEDURE [dbo].[pSaveDropboxAuthorizationCode] (@Code VARCHAR(MAX) = '')
AS
  BEGIN
      IF OBJECT_ID('tDropboxAuthorization', 'U') IS NULL
        BEGIN;
            CREATE TABLE [dbo].[tDropboxAuthorization]
              (
                 [ID]          [int] IDENTITY(1, 1) NOT NULL,
                 [Code]        [varchar](3000) NULL,
                 [DateCreated] [datetime] NULL
              )
            ON [PRIMARY];
        END;

      SELECT '_'

      SELECT @Code AuthoizationCode

	  if(SELECT COUNT(*) FROM dbo.tDropboxAuthorization WHERE Code = @Code) = 0
	  BEGIN

		  INSERT INTO [dbo].[tDropboxAuthorization]
					  ([Code],
					   [DateCreated])
		  VALUES      (@Code,
					   GETDATE())

	  END
  END

GO
