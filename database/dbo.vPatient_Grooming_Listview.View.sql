﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Grooming_Listview]
AS
  SELECT grooming.ID,
         grooming.Date,
         grooming.Code,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         ID_Company
  FROM   vPatient_Grooming grooming
  --WHERE  ID_FilingStatus NOT IN ( 4 )

GO
