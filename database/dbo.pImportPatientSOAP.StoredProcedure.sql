﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pImportPatientSOAP] (@records        typImportPatientSOAP READONLY,
                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_Company INT = 0;
      DECLARE @ID_User INT = 0;
      DECLARE @Count INT = 0;
      DECLARE @Counter_DocumentNo INT = 0;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model UNIQUEIDENTIFIER;
      DECLARE @Old_soap_ids TABLE
        (
           Old_soap_id INT
        )
      DECLARE @forImport TABLE
        (
           Old_soap_id     INT,
           Old_patient_id  INT,
           Date            DATETIME,
           ID_SOAPType     INT,
           ID_FilingStatus INT,
           Subjective      VARCHAR(MAX),
           Objective       VARCHAR(MAX),
           Assessment      VARCHAR(MAX),
           Prescription    VARCHAR(MAX),
           Planning        VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = u.ID_Company
      FROM   vUser u
      WHERE  u.ID = @ID_User

      INSERT @Old_soap_ids
             (Old_soap_id)
      SELECT CONVERT(INT, soap_id)
      FROM   @records
      EXCEPT
      SELECT ps.Old_soap_id
      FROM   tPatient_SOAP ps
      WHERE  ps.ID_Company = @ID_Company

      SELECT @Oid_Model = m.Oid
      FROM   dbo._tModel m
      WHERE  m.TableName = 'tPatient_SOAP';

      SELECT @Count = COUNT(*)
      FROM   @Old_soap_ids

      INSERT @forImport
             (Old_soap_id,
              Old_patient_id,
              Date,
              ID_SOAPType,
              ID_FilingStatus,
              Subjective,
              Objective,
              Assessment,
              Planning,
              Prescription)
      SELECT record.soap_id,
             record.pet_id,
             record.date,
             CASE
               WHEN LOWER(record.status) = 'confinement' THEN 2
               ELSE 1
             END,
             3,
             record.subj,
             'Heart Rate (bpm): '
             + CASE
                 WHEN LEN(ISNULL(heart_rate, '')) > 0 THEN record.heart_rate
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13)
             + 'Respiratory Rate (brpm): '
             + CASE
                 WHEN LEN(ISNULL(respiratory_rate, '')) > 0 THEN record.respiratory_rate
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Weight: '
             + CASE
                 WHEN LEN(ISNULL(weight, '')) > 0 THEN record.weight + ' ' + record.weight_unit
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Length: '
             + CASE
                 WHEN LEN(ISNULL(Length, '')) > 0 THEN record.Length
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'BCS: '
             + CASE
                 WHEN LEN(ISNULL(bcs, '')) > 0 THEN record.bcs
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Temperature: '
             + CASE
                 WHEN LEN(ISNULL(Temperature, '')) > 0
                      AND Temperature <> 'NULL' THEN record.Temperature + ' '
                                                     + record.temperature_degrees
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             'Differential Diagnosis: : '
             + CASE
                 WHEN LEN(ISNULL(diagnosis, '')) > 0
                      AND diagnosis <> 'NULL' THEN record.diagnosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Notes: '
             + CASE
                 WHEN LEN(ISNULL(notes, '')) > 0
                      AND notes <> 'NULL' THEN record.notes
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Test Results: '
             + CASE
                 WHEN LEN(ISNULL(test_result, '')) > 0
                      AND test_result <> 'NULL' THEN record.test_result
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Final Diagnosis: '
             + CASE
                 WHEN LEN(ISNULL(final_diagnosis, '')) > 0
                      AND final_diagnosis <> 'NULL' THEN record.final_diagnosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Prognosis: '
             + CASE
                 WHEN LEN(ISNULL(prognosis, '')) > 0
                      AND prognosis <> 'NULL' THEN record.prognosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Category: '
             + CASE
                 WHEN LEN(ISNULL(category, '')) > 0
                      AND category <> 'NULL' THEN record.category
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             'Treatment: '
             + CASE
                 WHEN LEN(ISNULL(treatment, '')) > 0 THEN record.treatment
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Remarks: '
             + CASE
                 WHEN LEN(ISNULL(remarks, '')) > 0 THEN record.remarks
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Date Return: '
             + CASE
                 WHEN record.date_return IS NOT NULL THEN FORMAT(record.date_return, 'MMMM dd, yyyy')
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Reason for Follow-up: '
             + CASE
                 WHEN LEN(ISNULL(reason, '')) > 0 THEN record.reason
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             CASE
               WHEN LEN(ISNULL(prescribed, '')) > 0 THEN record.prescribed
               ELSE 'N/A'
             END
      FROM   @records record

      INSERT tPatient_SOAP
             (Old_soap_id,
              Code,
              Date,
              ID_SOAPType,
              ID_FilingStatus,
              ID_Patient,
              History,
              Subjective,
              Objective,
              Assessment,
              Diagnosis,
              Prescription,
              Planning,
              Name,
              IsActive,
              ID_Company,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_ApprovedBy,
              ID_CanceledBy,
              DateApproved,
              DateCanceled)
      SELECT import.Old_soap_id,
             dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, ROW_NUMBER()
                                                                    OVER (
                                                                      ORDER BY Old_soap_id) - 1, NULL) AS Row_Number,
             import.Date,
             import.ID_SOAPType,
             import.ID_FilingStatus,
             ID                                                                                  ID_Patient,
             import.Subjective                                                                   History,
             import.Subjective,
             import.Objective,
             import.Assessment,
             import.Assessment                                                                   Diagnosis,
             import.Prescription,
             '',
             NULL,
             1,
             @ID_Company,
             NULL,
             GETDATE(),
             GETDATE(),
             1,
             1,
             NULL,
             NULL,
             NULL,
             NULL
      FROM   tPatient p
             INNER JOIN @forImport import
                     ON import.Old_patient_id = p.Old_patient_id
      WHERE  p.ID_Company = @ID_Company
             AND import.Old_soap_id IN (SELECT Old_soap_id
                                        FROM   @Old_soap_ids)

      IF @Count > 0
        BEGIN
            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + @Count
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;
        END

      UPDATE tPatient_SOAP
      SET    Subjective = import.Subjective,
             Objective = import.Objective,
             Assessment = import.Assessment,
             Planning = import.Planning,
             Prescription = import.Prescription,
             ID_FilingStatus = import.ID_FilingStatus
      FROM   tPatient_SOAP ps
             INNER JOIN @forImport import
                     ON ps.Old_soap_id = import.Old_soap_id
      WHERE  ps.ID_Company = @ID_Company

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END 
GO
