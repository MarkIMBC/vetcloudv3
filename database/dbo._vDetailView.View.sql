﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vDetailView]
AS
    SELECT  D.*,
			M.Name AS Model
    FROM    dbo.[_tDetailView] D 
	LEFT JOIN dbo.[_tModel] M ON D.ID_Model = M.Oid
GO
