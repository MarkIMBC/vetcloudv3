﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pCancelPaymentTransaction_validation] (@IDs_PaymentTransaction typIntList READONLY,
                                                         @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;

      /* Validate Billing Invoices Status is not Approved*/
      INSERT @ValidateNotApproved
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPaymentTransaction bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_PaymentTransaction ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;

      /*Validate Need to cancel Last Payment Transaction*/
      DECLARE @BillingInvoiceLastPTtrans TABLE
        (
           RowID                        int identity(1, 1) primary key,
           ID_BillingInvoice            INT,
           Code_BillingInvoice          VARCHAR(MAX),
           ID_PaymentTransaction        INT,
           Last_ID_PaymentTransaction   INT,
           Last_Code_PaymentTransaction VARCHAR(MAX),
           ID_FilingStatus              INT
        )

      INSERT @BillingInvoiceLastPTtrans
             (ID_BillingInvoice,
              Code_BillingInvoice,
              ID_PaymentTransaction,
              ID_FilingStatus)
      SELECT DISTINCT p.ID_BillingInvoice,
                      biHed.Code,
                      p.ID,
                      p.ID_FilingStatus
      FROM   tPaymentTransaction p
             INNER JOIN @IDs_PaymentTransaction idspt
                     ON p.ID = idspt.ID
             INNER JOIN tBillingInvoice biHed
                     ON biHed.ID = p.ID_BillingInvoice
      WHERE  p.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

      UPDATE @BillingInvoiceLastPTtrans
      SET    Last_ID_PaymentTransaction = tbl.Last_ID_PaymentTransaction,
             Last_Code_PaymentTransaction = pt.Code
      FROM   (SELECT MAX(p.ID) Last_ID_PaymentTransaction,
                     p.ID_BillingInvoice
              FROM   @BillingInvoiceLastPTtrans biPTTrans
                     INNER JOIN tPaymentTransaction p
                             ON biPTTrans.ID_BillingInvoice = p.ID_BillingInvoice
              WHERE  p.ID_FilingStatus IN ( 3 )
              GROUP  BY p.ID_BillingInvoice) tbl
             INNER JOIN tPaymentTransaction pt
                     ON pt.ID = tbl.Last_ID_PaymentTransaction

      DECLARE @ValidateNotLastPaymentTransaction TABLE
        (
           Code                 VARCHAR(MAX),
           Code_BillinigInvoice VARCHAR(MAX)
        )
      DECLARE @Count_NotLastPaymentTransaction INT = 0

      INSERT @ValidateNotLastPaymentTransaction
             (Code,
              Code_BillinigInvoice)
      SELECT Last_Code_PaymentTransaction,
             Code_BillingInvoice
      FROM   @BillingInvoiceLastPTtrans
      WHERE  Last_ID_PaymentTransaction <> ID_PaymentTransaction

      SELECT @Count_NotLastPaymentTransaction = COUNT(*)
      FROM   @ValidateNotLastPaymentTransaction

      IF ( @Count_NotLastPaymentTransaction > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_NotLastPaymentTransaction > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'need to cancel first:';

            SELECT @message = @message + CHAR(10) + Code + ' of '
                              + Code_BillinigInvoice
            FROM   @ValidateNotLastPaymentTransaction;

            THROW 50001, @message, 1;
        END;
  END;

GO
