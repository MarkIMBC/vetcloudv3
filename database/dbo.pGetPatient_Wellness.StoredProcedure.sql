﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pGetPatient_Wellness] @ID                             INT = -1,
                                     @AttendingPhysician_ID_Employee INT,
                                     @ID_Client                      INT,
                                     @ID_Patient                     INT,
                                     @ID_Item                        INT,
                                     @ID_Patient_SOAP                INT,
                                     @ID_Session                     INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Wellness_Detail,
             '' AS Patient_Wellness_Schedule

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT
      DECLARE @GUID_Company VARCHAR(MAX) = ''
      DECLARE @CardinalPetClinic_GUID_Company VARCHAR(MAX) = 'D91D497F-6108-4D50-88A2-BBC73C81A86D'

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company,
             @GUID_Company = Guid
      FROM   vUser
      WHERE  ID = @ID_User

      if( @GUID_Company = @CardinalPetClinic_GUID_Company )
        AND ISNULL(@AttendingPhysician_ID_Employee, 0) = 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID_Company = @ID_Company
                   AND ID = 50
        END

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        BEGIN
            SET @ID_Patient = NULL
        END
      ELSE
        BEGIN
            SELECT @ID_Client = ID_Client
            FROm   tPatient
            WHERE  ID = @ID_Patient
        END

      if( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_SOAP
            where  ID = @ID_Patient_SOAP
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                  Name_FilingStatus,
                   patient.ID_Client        ID_Client,
                   client.Name              Name_Client,
                   patient.Name             Name_Patient,
                   _AttendingPhysician.Name AttendingPhysician_Name_Employee
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '-- NEW --'                     AS [Code],
                           NULL                            AS [Name],
                           GETDATE()                       as Date,
                           1                               AS [IsActive],
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           NULL                            AS [ID_CreatedBy],
                           NULL                            AS [ID_LastModifiedBy],
                           @AttendingPhysician_ID_Employee AS [AttendingPhysician_ID_Employee],
                           @ID_Client                      AS [ID_Client],
                           @ID_Patient                     AS [ID_Patient],
                           @ID_Patient_SOAP                AS [ID_Patient_SOAP],
                           1                               ID_FilingStatus,
                           ''                              AttendingPhysician) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          on client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          on patient.ID = H.ID_Patient
                   LEFT JOIN tFilingStatus fs
                          on fs.ID = H.ID_FilingStatus
                   LEFT JOIN tEmployee _AttendingPhysician
                          on _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Wellness H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Wellness_Detail
      WHERE  ID_Patient_Wellness = @ID

      SELECT *
      FROM   vPatient_Wellness_Schedule
      WHERE  ID_Patient_Wellness = @ID
      ORder  By Date ASC
  END 
GO
