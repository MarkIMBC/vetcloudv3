﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateClientTotalRemainingAmount](@IDs_Client typIntList ReadOnly)
as
  BEGIN
      DECLARE @clientBI TABLE
        (
           ID_Client            INT,
           TotalRemainingAmount DECIMAL(18, 2)
        )

      INSERT @clientBI
             (ID_Client,
              TotalRemainingAmount)
      SELECT bi.ID_Client,
             SUM(ISNULL(bi.RemainingAmount, 0))
      FROm   tBillingInvoice bi
             inner join tClient client
                     on bi.ID_Client = client.ID
             INNER JOIN @IDs_Client id
                     on id.ID = client.ID
      WHERE  bi.ID_FilingStatus = 3
      GROUP  BY bi.ID_Client

      Update tClient
      SET    TotalRemainingAmount = clientbi.TotalRemainingAmount
      FROM   tClient client
             inner join @clientBI clientbi
                     on client.ID = clientbi.ID_Client
  END

GO
