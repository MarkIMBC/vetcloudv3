﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardBarangaySummary] @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)          ResidentCount,
             ISNULL(@PermanentResidentCount, 0) PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)   MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0) TrancientResidentCount,
             ISNULL(@TrancientResidentCount, 0) HomeOwnershipStatusOwnerCount,
             ISNULL(@TrancientResidentCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@TrancientResidentCount, 0) TotalResidentMaleCount,
             ISNULL(@TrancientResidentCount, 0) TotalResidentFemaleCount
  END

GO
