﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveInventoryTrail] AS   
				SELECT   
				 H.*  
				FROM vInventoryTrail H  
				WHERE IsActive = 1
GO
