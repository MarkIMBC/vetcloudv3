﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveRecurScheduleType] AS   
				SELECT   
				 H.*  
				FROM vRecurScheduleType H  
				WHERE ISNULL(IsActive, 0) = 0
GO
