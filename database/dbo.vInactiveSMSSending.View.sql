﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vInactiveSMSSending]
AS
  SELECT inactiveSMS.*,
         company.Name Name_Company
  FROm   tInactiveSMSSending inactiveSMS
         inner join tCompany company
                 on inactiveSMS.ID_Company = company.ID

GO
