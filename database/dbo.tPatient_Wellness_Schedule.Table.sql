﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Wellness_Schedule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Patient_Wellness] [int] NULL,
	[Date] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[IsSentSMS] [bit] NULL,
	[tPatient_Vaccination_Schedule] [int] NULL,
	[ID_Patient_Vaccination_Schedule] [int] NULL,
	[Appointment_ID_FilingStatus] [int] NULL,
	[Appointment_CancellationRemarks] [varchar](300) NULL,
	[DateRescheduletUpdated] [datetime] NULL,
	[DateReschedule] [datetime] NULL,
 CONSTRAINT [PK_tPatient_Wellness_Schedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Wellness_Schedule] ON [dbo].[tPatient_Wellness_Schedule]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] ADD  DEFAULT ((2)) FOR [Appointment_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] CHECK CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] CHECK CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] CHECK CONSTRAINT [FK_tPatient_Wellness_Schedule_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule]  WITH CHECK ADD  CONSTRAINT [FKtPatient_Wellness_Schedule_ID_Patient_Wellness] FOREIGN KEY([ID_Patient_Wellness])
REFERENCES [dbo].[tPatient_Wellness] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] CHECK CONSTRAINT [FKtPatient_Wellness_Schedule_ID_Patient_Wellness]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Wellness_Schedule]
		ON [dbo].[tPatient_Wellness_Schedule]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness_Schedule
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Wellness_Schedule hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] ENABLE TRIGGER [rDateCreated_tPatient_Wellness_Schedule]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Wellness_Schedule]
		ON [dbo].[tPatient_Wellness_Schedule]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Wellness_Schedule
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Wellness_Schedule hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Wellness_Schedule] ENABLE TRIGGER [rDateModified_tPatient_Wellness_Schedule]
GO
