﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveCompanyCustomReportObjectValue] AS   
    SELECT   
     H.*  
    FROM vCompanyCustomReportObjectValue H  
    WHERE ISNULL(IsActive, 0) = 1
GO
