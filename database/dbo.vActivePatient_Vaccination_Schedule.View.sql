﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Vaccination_Schedule] AS   
				SELECT   
				 H.*  
				FROM vPatient_Vaccination_Schedule H  
				WHERE IsActive = 1
GO
