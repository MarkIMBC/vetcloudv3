﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pApproveClientWithdraw_validation] (@IDs_ClientWithdraw typIntList READONLY,
                                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @ValidateInsufficientNonConfinementCreditAmount TABLE
        (
           Name_Client                VARCHAR(MAX),
           CurrentCreditAmount        DECIMAL(18, 4),
           NonConfinementCreditAmount DECIMAL(18, 4),
           WithdrawAmount             DECIMAL(18, 4)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;
      Declare @IDs_Client typIntList

      INSERT @IDs_Client
      SELECT ID_Client
      FROM   dbo.vClientWithdraw bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientWithdraw ids
                     WHERE  ids.ID = bi.ID)

      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientWithdraw bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientWithdraw ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus )

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;
  ----/***************************************************************/  
  --DECLARE @ClientNonConfinementCreditAmount TABLE
  --  (
  --     ID_Client                  Int,
  --     Name_Client                VARCHAR(MAX),
  --     CurrentCreditAmount        DECIMAL(18, 4),
  --     NonConfinementCreditAmount DECIMAL(18, 4),
  --     WithdrawAmount             DECIMAL(18, 4)
  --  );
  --INSERT @ClientNonConfinementCreditAmount
  --       (ID_Client,
  --        Name_Client,
  --        CurrentCreditAmount,
  --        WithdrawAmount)
  --SELECT ID_Client,
  --       client.Name Name_Client,
  --       client.CurrentCreditAmount,
  --       SUM(WithdrawAmount)
  --FROM   dbo.vClientWithdraw bi
  --       inner join @IDs_ClientWithdraw ids
  --               on bi.ID = ids.ID
  --       INNER JOIN tCLient client
  --               on bi.ID_Client = client.ID
  --GROUP  BY ID_Client,
  --          client.Name,
  --          CurrentCreditAmount
  --UPdate @ClientNonConfinementCreditAmount
  --SET    NonConfinementCreditAmount = tbl.NonConfinementCreditAmount
  --FROM   @ClientNonConfinementCreditAmount hed
  --       INNER JOIN (SELECT client.ID                                                 ID_Client,
  --                          client.CurrentCreditAmount - SUM(creditLogs.CreditAmount) NonConfinementCreditAmount
  --                   FROM   @ClientNonConfinementCreditAmount clientCreditAmount
  --                          inner join tClient client
  --                                  on client.ID = clientCreditAmount.ID_Client
  --                          inner join tClientDeposit deposit
  --                                  on deposit.ID_Client = client.ID
  --                          inner join tClient_CreditLogs creditLogs
  --                                  on creditLogs.Code = deposit.Code
  --                                     and creditLogs.ID_Company = deposit.ID_Company
  --                   WHERE  ISNULL(deposit.ID_Patient_Confinement, 0) > 0
  --                   GROUP  BY client.ID,
  --                             client.CurrentCreditAmount) tbl
  --               on tbl.ID_Client = hed.ID_Client
  --INSERT @ValidateInsufficientNonConfinementCreditAmount
  --       (Name_Client,
  --        CurrentCreditAmount,
  --        NonConfinementCreditAmount,
  --        WithdrawAmount)
  --SELECT Name_Client,
  --       CurrentCreditAmount,
  --       NonConfinementCreditAmount,
  --       WithdrawAmount
  --FROM   @ClientNonConfinementCreditAmount
  --WHERE  NonConfinementCreditAmount < WithdrawAmount
  --IF (SELECT COUNT(*)
  --    FROm   @ValidateInsufficientNonConfinementCreditAmount) > 0
  --  BEGIN
  --      SET @message = 'The following record'
  --                     + CASE
  --                         WHEN (SELECT COUNT(*)
  --                               FROm   @ValidateInsufficientNonConfinementCreditAmount) > 1 THEN 's are'
  --                         ELSE ' is '
  --                       END
  --                     + 'not allowed withdraw';
  --      SELECT @message = @message + CHAR(10) + Name_Client
  --                        + ': Able to withdraw: '
  --                        + FORMAT(NonConfinementCreditAmount, '#,#0.00')
  --      FROM   @ValidateInsufficientNonConfinementCreditAmount;
  --      THROW 50001, @message, 1;
  --  END;
  END; 
GO
