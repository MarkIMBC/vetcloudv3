﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTaxScheme] AS   
				SELECT   
				 H.*  
				FROM vTaxScheme H  
				WHERE ISNULL(IsActive, 0) = 0
GO
