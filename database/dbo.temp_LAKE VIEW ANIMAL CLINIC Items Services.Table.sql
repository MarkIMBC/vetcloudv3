﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_LAKE VIEW ANIMAL CLINIC Items Services](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ItemType] [int] NULL,
	[ID_ItemCategory] [int] NULL,
	[MinInventoryCount] [int] NULL,
	[MaxInventoryCount] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[CurrentInventoryCount] [int] NULL,
	[Old_item_id] [int] NULL,
	[Old_procedure_id] [int] NULL,
	[OtherInfo_DateExpiration] [datetime] NULL,
	[ID_InventoryStatus] [int] NULL,
	[BarCode] [varchar](300) NULL,
	[CustomCode] [varchar](300) NULL,
	[_tempSupplier] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
	[SKU] [varchar](300) NULL,
	[SKUCode] [varchar](300) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
