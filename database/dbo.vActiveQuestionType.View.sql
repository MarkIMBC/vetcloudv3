﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveQuestionType] AS   
				SELECT   
				 H.*  
				FROM vQuestionType H  
				WHERE IsActive = 1
GO
