﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetAppointmentEvents](@CompanyID                        INT,
                                 @DateStart                        DateTime,
                                 @DateEnd                          DateTime,
                                 @ReferenceCode                    VARCHAR(MAX),
                                 @Name_Client                      VARCHAR(MAX),
                                 @Name_Patient                     VARCHAR(MAX),
                                 @AttendingPhysician_Name_Employee VARCHAR(MAX))
AS
  BEGIN
      DECLARE @table TABLE
        (
           [ID]                               [varchar](max) NULL,
           [UniqueID]                         [varchar](max) NULL,
           [Oid_Model]                        [uniqueidentifier] NOT NULL,
           [Name_Model]                       [varchar](200) NULL,
           [ID_Company]                       [int] NULL,
           [ID_Client]                        [int] NULL,
           [ID_CurrentObject]                 [int] NOT NULL,
           [Appointment_ID_CurrentObject]     [int] NOT NULL,
           [DateStart]                        [datetime] NULL,
           [DateEnd]                          [datetime] NULL,
           [FormattedDateStart]               [nvarchar](4000) NULL,
           [FormattedDateEnd]                 [nvarchar](4000) NULL,
           [FormattedDateStartTime]           [nvarchar](4000) NULL,
           [FormattedDateEndTime]             [nvarchar](4000) NULL,
           [ReferenceCode]                    [varchar](50) NULL,
           [Paticular]                        [varchar](2203) NULL,
           [Description]                      [varchar](max) NULL,
           [TimeCoverage]                     [varchar](1000) NULL,
           [ID_FilingStatus]                  [int] NULL,
           [Name_FilingStatus]                [varchar](200) NULL,
           [Name_Client]                      [varchar](2000) NULL,
           [ID_Patient]                       [int] NULL,
           [Name_Patient]                     [varchar](200) NULL,
           [ContactNumber]                    [varchar](max) NULL,
           [Appointment_ID_FilingStatus]      [int] NULL,
           [Appointment_Name_FilingStatus]    [varchar](200) NULL,
           [Appointment_CancellationRemarks]  [varchar](300) NULL,
           [DateReschedule]                   [datetime] NULL,
           [AttendingPhysician_Name_Employee] [varchar](300) NULL
        )

      /* From tPatient_SOAP */
      INSERT @table
      SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
             + CONVERT(VARCHAR(MAX), hed.ID)                                         ID,
             CONVERT(VARCHAR(MAX), _model.Oid) + '|'
             + CONVERT(VARCHAR(MAX), hed.ID) + '|'
             + CONVERT(VARCHAR(MAX), schedule.ID) + '|'
             + CONVERT(VARCHAR(MAX), hed.ID_Client)                                  UniqueID,
             _model.Oid                                                              Oid_Model,
             _model.Name                                                             Name_Model,
             hed.ID_Company,
             hed.ID_Client,
             hed.ID                                                                  ID_CurrentObject,
             schedule.ID                                                             Appointment_ID_CurrentObject,
             schedule.DateReturn                                                     DateStart,
             schedule.DateReturn                                                     DateEnd,
             Format(schedule.DateReturn, 'yyyy-MM-dd')                               FormattedDateStart,
             Format(schedule.DateReturn, 'yyyy-MM-dd ')                              FormattedDateEnd,
             Format(schedule.DateReturn, 'hh:mm tt')                                 FormattedDateStartTime,
             ''                                                                      FormattedDateEndTime,
             hed.Code                                                                ReferenceCode,
             client.Name + ' - ' + patient.Name                                      Paticular,
             IsNull(soapType.Name, '')
             + CASE
                 WHEN LEN(IsNull(item.Name, '')) > 0 THEN ' - ' + IsNull(item.Name, '')
                 ELSE ''
               END
             + CASE
                 WHEN LEN(ISNULL(schedule.Comment, '')) > 0 THEN ' - ' + schedule.Comment
                 ELSE ''
               END                                                                   Description,
             dbo.fGetDateCoverageString(schedule.DateReturn, schedule.DateReturn, 1) TimeCoverage,
             ID_FilingStatus,
             filingStatus.Name,
             client.Name,
             hed.ID_Patient,
             patient.Name,
             dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)    ContactNumber,
             schedule.Appointment_ID_FilingStatus,
             appointmentfilingStatus.Name,
             schedule.Appointment_CancellationRemarks,
             schedule.DateReschedule,
             attendingVet.Name
      FROM   dbo.tPatient_SOAP hed WITH (NOLOCK)
             INNER JOIN dbo.tPatient_SOAP_Plan schedule WITH (NOLOCK)
                     ON schedule.ID_Patient_SOAP = hed.ID
             INNER JOIN tClient client WITH (NOLOCK)
                     ON client.ID = hed.ID_Client
             INNER JOIN tPatient patient WITH (NOLOCK)
                     ON patient.ID = hed.ID_Patient
             INNER JOIN tEmployee attendingVet WITH (NOLOCK)
                     ON attendingVet.ID = hed.AttendingPhysician_ID_Employee
             INNER JOIN tSOAPType soapType WITH (NOLOCK)
                     ON soapType.ID = hed.ID_SOAPType
             INNER JOIN tFilingStatus filingStatus WITH (NOLOCK)
                     ON filingStatus.ID = hed.ID_FilingStatus
             INNER JOIN tFilingStatus appointmentfilingStatus WITH (NOLOCK)
                     ON appointmentfilingStatus.ID = schedule.Appointment_ID_FilingStatus
             INNER JOIN tItem item WITH (NOLOCK)
                     ON item.ID = schedule.ID_Item
             INNER JOIN dbo._tModel _model WITH (NOLOCK)
                     ON _model.TableName = 'tPatient_SOAP'
      WHERE  hed.ID_FilingStatus IN ( 1, 3, 13 )
             AND hed.ID_Company = @CompanyID
             AND CONVERT(DATE, schedule.DateReturn) BETWEEN @DateStart AND @DateEnd
             AND ISNULL(hed.Code, '') LIKE '%' + @ReferenceCode + '%'
             AND ISNULL(client.Name, '') LIKE '%' + @Name_Client + '%'
             AND ISNULL(patient.Name, '') LIKE '%' + @Name_Patient + '%'
             AND ISNULL(attendingVet.Name, '') LIKE '%' + @AttendingPhysician_Name_Employee + '%'

      /* From tPatientAppointment */
      INSERT @table
      SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
             + CONVERT(VARCHAR(MAX), hed.ID)                                      ID,
             CONVERT(VARCHAR(MAX), _model.Oid) + '|'
             + CONVERT(VARCHAR(MAX), hed.ID) + '|' + '0' + '|'
             + CONVERT(VARCHAR(MAX), hed.ID_Client)                               UniqueID,
             _model.Oid                                                           Oid_Model,
             _model.Name                                                          Name_Model,
             hed.ID_Company,
             hed.ID_Client,
             hed.ID                                                               ID_CurrentObject,
             hed.ID                                                               Appointment_ID_CurrentObject,
             hed.DateStart                                                        DateStart,
             hed.DateEnd                                                          DateEnd,
             Format(hed.DateStart, 'yyyy-MM-dd')                                  FormattedDateStart,
             Format(hed.DateEnd, 'yyyy-MM-dd')                                    FormattedDateEnd,
             Format(hed.DateStart, 'hh:mm tt')                                    FormattedDateStartTime,
             Format(hed.DateEnd, 'hh:mm tt')                                      FormattedDateEndTime,
             IsNull(hed.Code, 'Patient Appt.')                                    ReferenceCode,
             client.Name + ' - ' + patient.Name                                   Paticular,
             IsNull(patient.Name, '') + ' - '
             + IsNull(soapType.Name, '') + ' '
             + IsNull(hed.Comment, '' )                                           Description,
             dbo.fGetDateCoverageString(hed.DateStart, hed.DateEnd, 1)            TimeCoverage,
             ID_FilingStatus,
             filingStatus.Name,
             client.Name,
             hed.ID_Patient,
             patient.Name,
             dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
             hed.Appointment_ID_FilingStatus,
             appointmentfilingStatus.Name,
             hed.Appointment_CancellationRemarks,
             hed.DateReschedule,
             ISNULL(groomer.Name, attendingVet.Name)                              AttendingPhysician_Name_Employee
      FROM   dbo.tPatientAppointment hed WITH (NOLOCK)
             LEFT JOIN tClient client WITH (NOLOCK)
                     ON client.ID = hed.ID_Client
             LEFT JOIN tPatient patient WITH (NOLOCK)
                     ON patient.ID = hed.ID_Patient
             LEFT JOIN tFilingStatus filingStatus WITH (NOLOCK)
                     ON filingStatus.ID = hed.ID_FilingStatus
             LEFT JOIN tFilingStatus appointmentfilingStatus WITH (NOLOCK)
                     ON appointmentfilingStatus.ID = hed.Appointment_ID_FilingStatus
             LEFT JOIN tEmployee attendingVet WITH (NOLOCK)
                     ON attendingVet.ID = hed.AttendingPhysician_ID_Employee
             LEFT JOIN tEmployee groomer WITH (NOLOCK)
                     ON groomer.ID = hed.Groomer_ID_Employee
             LEFT JOIN tSOAPType soapType WITH (NOLOCK)
                     ON soapType.ID = hed.ID_SOAPType
             INNER JOIN dbo._tModel _model WITH (NOLOCK)
                     ON _model.TableName = 'tPatientAppointment'
      WHERE  hed.ID_FilingStatus IN ( 1, 13 )
             AND hed.ID_Company = @CompanyID
             AND CONVERT(DATE, hed.DateStart) BETWEEN @DateStart AND @DateEnd
             AND ISNULL(hed.Code, '') LIKE '%' + @ReferenceCode + '%'
             AND ISNULL(client.Name, '') LIKE '%' + @Name_Client + '%'
             AND ISNULL(patient.Name, '') LIKE '%' + @Name_Patient + '%'
             AND ISNULL(attendingVet.Name, '') LIKE '%' + @AttendingPhysician_Name_Employee + '%'

      /* From tPatient_Wellness */
      INSERT @table
      SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                      + CONVERT(VARCHAR(MAX), hed.ID)                                      ID,
                      CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                      + CONVERT(VARCHAR(MAX), hed.ID) + '|'
                      + CONVERT(VARCHAR(MAX), schedule.ID) + '|'
                      + CONVERT(VARCHAR(MAX), hed.ID_Client)                               UniqueID,
                      _model.Oid                                                           Oid_Model,
                      _model.Name                                                          Name_Model,
                      hed.ID_Company,
                      hed.ID_Client,
                      hed.ID                                                               ID_CurrentObject,
                      schedule.ID                                                          Appointment_ID_CurrentObject,
                      schedule.Date                                                        DateStart,
                      schedule.Date                                                        DateEnd,
                      Format(schedule.Date, 'yyyy-MM-dd')                                  FormattedDateStart,
                      Format(schedule.Date, 'yyyy-MM-dd ')                                 FormattedDateEnd,
                      Format(schedule.Date, 'hh:mm tt')                                    FormattedDateStartTime,
                      ''                                                                   FormattedDateEndTime,
                      hed.Code                                                             ReferenceCode,
                      client.Name + ' - ' + hed.Name                           Paticular,
                      IsNull(schedule.Comment, '')                                         Description,
                      dbo.fGetDateCoverageString(schedule.Date, schedule.Date, 1)          TimeCoverage,
                      hed.ID_FilingStatus,
                      filingStatus.Name,
                      client.Name,
                      hed.ID_Patient,
                      patient.Name,
                      dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                      schedule.Appointment_ID_FilingStatus,
                      schedule.Appointment_Name_FilingStatus,
                      schedule.Appointment_CancellationRemarks,
                      schedule.DateReschedule,
                      attendingVet.Name
      FROM   dbo.tPatient_Wellness hed WITH (NOLOCK)
             LEFT JOIN dbo.vPatient_Wellness_Schedule schedule WITH (NOLOCK)
                     ON schedule.ID_Patient_Wellness = hed.ID
             LEFT JOIN tClient client WITH (NOLOCK)
                     ON client.ID = hed.ID_Client
             LEFT JOIN tPatient patient WITH (NOLOCK)
                     ON patient.ID = hed.ID_Patient
             LEFT JOIN tFilingStatus filingStatus WITH (NOLOCK)
                     ON filingStatus.ID = hed.ID_FilingStatus
             LEFT JOIN tEmployee attendingVet WITH (NOLOCK)
                     ON attendingVet.ID = hed.AttendingPhysician_ID_Employee
             INNER JOIN dbo._tModel _model WITH (NOLOCK)
                     ON _model.TableName = 'tPatient_Wellness'
      WHERE  hed.ID_FilingStatus NOT IN ( 4 )
             AND hed.ID_Company = @CompanyID
             AND CONVERT(DATE, schedule.Date) BETWEEN @DateStart AND @DateEnd
             AND ISNULL(hed.Code, '') LIKE '%' + @ReferenceCode + '%'
             AND ISNULL(client.Name, '') LIKE '%' + @Name_Client + '%'
             AND ISNULL(patient.Name, '') LIKE '%' + @Name_Patient + '%'
             AND ISNULL(attendingVet.Name, '') LIKE '%' + @AttendingPhysician_Name_Employee + '%'

      SELECT '_',
             '' Records

      SELECT GETDATE() Date

      SELECT *
      FROm   @table
  END

GO
