﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveTooth] AS   
				SELECT   
				 H.*  
				FROM vTooth H  
				WHERE ISNULL(IsActive, 0) = 0
GO
