﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_ImagePaths_202301291609](
	[ID_Company] [int] NULL,
	[Name_Company] [varchar](max) NULL,
	[Name_Model] [varchar](max) NULL,
	[ImageFilePath] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
