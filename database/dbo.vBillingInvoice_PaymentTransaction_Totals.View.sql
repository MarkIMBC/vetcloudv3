﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vBillingInvoice_PaymentTransaction_Totals]
AS
  SELECT ID_BillingInvoice,
         SUM(ChangeAmount)  TotalChangeAmount,
         SUM(PaymentAmount) TotalPaymentAmount
  FROm   tPaymentTransaction
  WHERE  ID_FilingStatus NOT IN ( 1, 4 )
  GROUP  BY ID_BillingInvoice

GO
