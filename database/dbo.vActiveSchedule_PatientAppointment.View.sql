﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveSchedule_PatientAppointment] AS   
				SELECT   
				 H.*  
				FROM vSchedule_PatientAppointment H  
				WHERE IsActive = 1
GO
