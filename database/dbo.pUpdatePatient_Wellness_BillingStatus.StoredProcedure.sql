﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[pUpdatePatient_Wellness_BillingStatus](@IDs_Patient_Wellness typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @Patient_Wellness TABLE
        (
           ID_Patient_Wellness            INT,
           BillingInvoice_ID_FilingStatus INT
        )
      DECLARE @Patient_Wellness_FromBI TABLE
        (
           ID_Patient_Wellness            INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @Patient_Wellness
      SELECT _soap.ID,
             CASE
               WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus
               ELSE NULL
             END
      FROM   tPatient_Wellness _soap 
             inner join @IDs_Patient_Wellness ids
                     on _soap.ID = ids.ID

      INSERT @Patient_Wellness_FromBI
      SELECT ID_Patient_Wellness,
             dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])
      FROM   (SELECT idsSOAP.ID                                             ID_Patient_Wellness,
                     ISNULL(bi.Payment_ID_FilingStatus, bi.ID_FilingStatus) BillingInvoice_ID_FilingStatus,
                     COUNT(*)                                               BillingInvoiceCount
              FROM   vBillingInvoice bi 
                     inner JOIN @IDs_Patient_Wellness idsSOAP
                             on idsSOAP.ID = bi.ID_Patient_Wellness
                     INNER JOIN tCompany company
                             on company.ID = bi.ID_Company
              WHERE  ID_FilingStatus NOT IN ( 4 )
                     AND ISNULL(company.IsActive, 0) = 1
              GROUP  BY idsSOAP.ID,
                        ISNULL(bi.Payment_ID_FilingStatus, bi.ID_FilingStatus)) AS SourceTable
             PIVOT ( AVG(BillingInvoiceCount)
                   FOR BillingInvoice_ID_FilingStatus IN ([1],
                                                          [3],
                                                          [2],
                                                          [11],
                                                          [12],
                                                          [13]) ) AS PivotTable;

      UPDATE @Patient_Wellness
      SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus
      FROM   @Patient_Wellness soapRec
             INNER JOIN @Patient_Wellness_FromBI soapFromPivot
                     ON soapRec.ID_Patient_Wellness = soapFromPivot.ID_Patient_Wellness

      UPDATE tPatient_Wellness
      SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus
      FROM   tPatient_Wellness soap 
             INNER JOIN @Patient_Wellness soapRec
                     ON soap.ID = soapRec.ID_Patient_Wellness
  END

GO
