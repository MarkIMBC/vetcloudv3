﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveBillingType] AS   
    SELECT   
     H.*  
    FROM vBillingType H  
    WHERE ISNULL(IsActive, 0) = 1
GO
