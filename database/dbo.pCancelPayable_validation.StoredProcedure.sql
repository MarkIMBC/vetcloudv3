﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCancelPayable_validation] (@IDs_Payable    typIntList READONLY,
                                              @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Name_Payable_Detail VARCHAR(30),
           Name_FilingStatus   VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Name_Payable_Detail VARCHAR(30),
           Name_FilingStatus   VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Text Blast Status is not Approved*/
      INSERT @ValidateNotApproved
             (Name_Payable_Detail,
              Name_FilingStatus)
      SELECT paymentDetail.Name,
             Payment_Name_FilingStatus
      FROM   dbo.vPayable bi
             inner join tPayable_Detail paymentDetail
                     on bi.ID = paymentDetail.ID_Payable
             inner join @IDs_Payable ids
                     on ids.ID = bi.ID
      WHERE  bi.Payment_ID_FilingStatus NOT IN ( @Pending_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Name_Payable_Detail + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO
