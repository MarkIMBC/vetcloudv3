﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveItem_UnitPriceLog] AS   
				SELECT   
				 H.*  
				FROM vItem_UnitPriceLog H  
				WHERE ISNULL(IsActive, 0) = 0
GO
