﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 PROC [dbo].[pAddModelReport](@ID_Company INT,
                           @ModelName  Varchar(200),
                           @ReportName VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportName

      /* Check if Model is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tModel
      WHERE  Oid = @Oid_Model;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ModelName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Report is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tReport
      WHERE  Oid = @Oid_Report;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ReportName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Model and Report is already exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   [_tModelReport]
      WHERE  Oid_Report = @Oid_Report
             AND Oid_Model = @Oid_Model
             AND ID_Company = @ID_Company;

      IF( @Count_Validation > 0 )
        BEGIN
            SET @msg = @ModelName + ' ' + @ReportName
                       + ' is already exist.';

            THROW 51000, @msg, 1;
        END

      INSERT INTO [dbo].[_tModelReport]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   DateCreated,
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Oid_Report])
      VALUES      ('',
                   1,
                   @ID_Company,
                   GETDATE(),
                   1,
                   1,
                   @Oid_Model,
                   @Oid_Report)
  END

GO
