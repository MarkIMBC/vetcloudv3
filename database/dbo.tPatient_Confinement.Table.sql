﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Confinement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[DateDischarge] [datetime] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_DischargeBy] [int] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[BillingInvoice_ID_FilingStatus] [int] NULL,
	[SubTotal] [decimal](18, 4) NULL,
	[TotalAmount] [decimal](18, 4) NULL,
	[ID_Patient_SOAP] [int] NULL,
	[PatientNames] [varchar](300) NULL,
	[tempID] [varchar](300) NULL,
 CONSTRAINT [PK_tPatient_Confinement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Confinement] ON [dbo].[tPatient_Confinement]
(
	[Code] ASC,
	[ID_Client] ASC,
	[ID_Patient] ASC,
	[ID_Patient_SOAP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Confinement] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Confinement]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement] CHECK CONSTRAINT [FK_tPatient_Confinement_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Confinement]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement] CHECK CONSTRAINT [FK_tPatient_Confinement_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Confinement]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Confinement_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Confinement] CHECK CONSTRAINT [FK_tPatient_Confinement_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Confinement]
		ON [dbo].[tPatient_Confinement]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Confinement
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Confinement hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Confinement] ENABLE TRIGGER [rDateCreated_tPatient_Confinement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Confinement]
		ON [dbo].[tPatient_Confinement]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Confinement
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Confinement hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Confinement] ENABLE TRIGGER [rDateModified_tPatient_Confinement]
GO
