﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveReferralType] AS   
    SELECT   
     H.*  
    FROM vReferralType H  
    WHERE IsActive = 1
GO
