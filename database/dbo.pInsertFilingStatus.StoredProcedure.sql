﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertFilingStatus](@FilingStatus VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tFilingStatus
         WHERE  Name = @FilingStatus) = 0
        BEGIN
            INSERT INTO [dbo].tFilingStatus
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@FilingStatus,
                         1,
                         1)
        END
  END

GO
