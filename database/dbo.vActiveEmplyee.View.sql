﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveEmplyee] AS   
				SELECT   
				 H.*  
				FROM vEmplyee H  
				WHERE IsActive = 1
GO
