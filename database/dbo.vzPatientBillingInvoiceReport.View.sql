﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzPatientBillingInvoiceReport]
AS
  SELECT biHed.ID,
         biHed.Code,
         bihed.CustomCode,
         biHed.Date,
         ISNULL(biHed.PatientNames, biHed.Name_Patient) Name_Patient,
         biHed.BillingAddress,
         biHed.Name_FilingStatus,
         CASE
           WHEN Lower(biHed.CreatedBy_Name_User) NOT LIKE 'admin%' THEN biHed.CreatedBy_Name_User
           ELSE ''
         END                                            CreatedBy_Name_User,
         CASE
           WHEN Lower(biHed.CreatedBy_Name_User) NOT LIKE 'admin%' THEN biHed.ApprovedBy_Name_User
           ELSE ''
         END                                            ApprovedBy_Name_User,
         biHed.Name_TaxScheme,
         biHed.DateApproved,
         ISNULL(biHed.InitialSubtotalAmount, 0)         InitialSubtotalAmount,
         ISNULL(biHed.ConfinementDepositAmount, 0)      ConfinementDepositAmount,
         ISNULL(biHed.ConsumedDepositAmount, 0)         ConsumedDepositAmount,
         ISNULL(biHed.RemainingDepositAmount, 0)        RemainingDepositAmount,
         biHed.SubTotal,
         ISNULL(biHed.TotalItemDiscountAmount, 0)       TotalItemDiscountAmount,
         ISNULL(biHed.DiscountRate, 0)                  DiscountRate,
         ISNULL(biHed.DiscountAmount, 0)                DiscountAmount,
         biHed.TotalAmount,
         biHed.GrossAmount,
         biHed.VatAmount,
         biHed.NetAmount,
         ISNULL(bihed.RemainingAmount, 0)               RemainingAmount,
         biDetail.ID                                    ID_BillingInvoice_Detail,
         biDetail.Name_Item,
         biDetail.Quantity,
         biDetail.UnitPrice,
         biDetail.DiscountRate                          DiscountRate_BillingInvoice_Detail,
         biDetail.DiscountAmount                        DiscountAmount_BillingInvoice_Detail,
         biDetail.Amount,
         biHed.ID_Company,
         biHed.Name_Client,
         ISNULL(biHed.IsWalkIn, 0)                      IsWalkIn,
         ISNULL(biHed.WalkInCustomerName, '')           WalkInCustomerName,
         ISNULL(biHed.Comment, '')                      Comment,
         company.ImageLogoLocationFilenamePath,
         company.Name                                   Name_Company,
         company.Address                                Address_Company,
         company.ContactNumber                          ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                          HeaderInfo_Company,
         CASE
           WHEN itemservices.Date IS NOT NULL THEN FORMAT(itemservices.Date, 'MM/dd/yyyy')
           ELSE
             CASE
               WHEN ISNULL(biHed.ID_Patient_Confinement, 0) = 0 THEN ''
               ELSE '   '
             END
         END                                            Date_Patient_Confinement_ItemsServices,
         ISNULL(confi.Code, '')                         Code_Patient_Confinement,
         paymentTotals.TotalChangeAmount,
         paymentTotals.TotalPaymentAmount
  FROM   dbo.vBillingInvoice biHed
         LEFT JOIN dbo.vBillingInvoice_Detail biDetail
                ON biHed.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
         LEFT JOIN tPatient_Confinement_ItemsServices itemservices
                ON biDetail.ID_Patient_Confinement_ItemsServices = itemservices.ID
         LEFT JOIN tPatient_Confinement confi
                ON biHed.ID_Patient_Confinement = confi.ID
         LEFT JOIN vBillingInvoice_PaymentTransaction_Totals paymentTotals
                on biHed.ID = paymentTotals.ID_BillingInvoice 
GO
