﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCardType] AS   
				SELECT   
				 H.*  
				FROM vCardType H  
				WHERE IsActive = 1
GO
