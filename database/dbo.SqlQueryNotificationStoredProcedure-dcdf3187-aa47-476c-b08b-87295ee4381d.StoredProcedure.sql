﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-dcdf3187-aa47-476c-b08b-87295ee4381d] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d') > 0)   DROP SERVICE [SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d]; if (OBJECT_ID('SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-dcdf3187-aa47-476c-b08b-87295ee4381d]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-dcdf3187-aa47-476c-b08b-87295ee4381d]; END COMMIT TRANSACTION; END
GO
