﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSMSPatientSOAP_Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_tSMSPatientSOAP_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tSMSPatientSOAP_Company] ON [dbo].[tSMSPatientSOAP_Company]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company]  WITH CHECK ADD  CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] CHECK CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_Company]
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company]  WITH CHECK ADD  CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] CHECK CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company]  WITH CHECK ADD  CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] CHECK CONSTRAINT [FK_tSMSPatientSOAP_Company_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tSMSPatientSOAP_Company]
		ON [dbo].[tSMSPatientSOAP_Company]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSMSPatientSOAP_Company
			SET    DateCreated = GETDATE()
			FROM   dbo.tSMSPatientSOAP_Company hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] ENABLE TRIGGER [rDateCreated_tSMSPatientSOAP_Company]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tSMSPatientSOAP_Company]
		ON [dbo].[tSMSPatientSOAP_Company]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tSMSPatientSOAP_Company
			SET    DateModified = GETDATE()
			FROM   dbo.tSMSPatientSOAP_Company hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tSMSPatientSOAP_Company] ENABLE TRIGGER [rDateModified_tSMSPatientSOAP_Company]
GO
