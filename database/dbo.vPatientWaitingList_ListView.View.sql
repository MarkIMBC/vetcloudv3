﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatientWaitingList_ListView]
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         IsnULL(appointment.UniqueIDList, '') UniqueIDList,
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.ID_Reference
  FROM   vPatientWaitingList waitingList
         OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            appointment.UniqueIDList,
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.ID_Reference 
GO
