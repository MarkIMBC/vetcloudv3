﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActivePatient_Confinement_Patient] AS   
				SELECT   
				 H.*  
				FROM vPatient_Confinement_Patient H  
				WHERE IsActive = 1
GO
