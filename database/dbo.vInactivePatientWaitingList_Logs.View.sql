﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactivePatientWaitingList_Logs] AS   
				SELECT   
				 H.*  
				FROM vPatientWaitingList_Logs H  
				WHERE ISNULL(IsActive, 0) = 0
GO
