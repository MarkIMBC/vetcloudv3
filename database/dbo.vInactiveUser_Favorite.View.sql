﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveUser_Favorite] AS   
				SELECT   
				 H.*  
				FROM vUser_Favorite H  
				WHERE ISNULL(IsActive, 0) = 0
GO
