﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vActiveCompany_SMSSetting] AS   
				SELECT   
				 H.*  
				FROM vCompany_SMSSetting H  
				WHERE IsActive = 1
GO
