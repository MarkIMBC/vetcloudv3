﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vPatientReferral_ListvIew]
AS
  SELECT ID,
         Date,
         Code,
         ID_Client,
         ID_Patient,
         Name_Client,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         ID_Company,
         AttendingPhysician_Name_Employee,
         History,
         Treatment,
         Concerns,
         LEN(ISNULL(History, ''))   Count_History,
         LEN(ISNULL(Treatment, '')) Count_Treatment,
         LEN(ISNULL(Concerns, ''))  Count_Concerns
  FROm   vPatientReferral

GO
