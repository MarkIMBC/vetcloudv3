﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vzSalesIncomentReport_SummarizedDiscount]
as
  SELECT ID_Company,
         Date_BillingInvoice,
         Code_BillingInvoice,
         SUM(DiscountAmount) DiscountAmount_BillingInvoice,
         MAX(ItemString)     ItemString
  FROM   vzSalesIncomentReport_DetailedDiscount
  GROUP  BY ID_Company,
            Date_BillingInvoice,
            Code_BillingInvoice

GO
