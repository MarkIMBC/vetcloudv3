﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatientReferral_ReferralType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_PatientReferral] [int] NULL,
	[ID_ReferralType] [int] NULL,
 CONSTRAINT [PK_tPatientReferral_ReferralType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatientReferral_ReferralType] ON [dbo].[tPatientReferral_ReferralType]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ReferralType_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] CHECK CONSTRAINT [FK_tPatientReferral_ReferralType_ID_Company]
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ReferralType_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] CHECK CONSTRAINT [FK_tPatientReferral_ReferralType_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType]  WITH CHECK ADD  CONSTRAINT [FK_tPatientReferral_ReferralType_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] CHECK CONSTRAINT [FK_tPatientReferral_ReferralType_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType]  WITH CHECK ADD  CONSTRAINT [FKtPatientReferral_ReferralType_ID_PatientReferral] FOREIGN KEY([ID_PatientReferral])
REFERENCES [dbo].[tPatientReferral] ([ID])
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] CHECK CONSTRAINT [FKtPatientReferral_ReferralType_ID_PatientReferral]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateCreated_tPatientReferral_ReferralType] ON [dbo].[tPatientReferral_ReferralType] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatientReferral_ReferralType SET DateCreated = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] ENABLE TRIGGER [rDateCreated_tPatientReferral_ReferralType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE TRIGGER [dbo].[rDateModified_tPatientReferral_ReferralType] ON [dbo].[tPatientReferral_ReferralType] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.tPatientReferral_ReferralType SET DateModified = GETDATE() WHERE ID = @ID  
  
GO
ALTER TABLE [dbo].[tPatientReferral_ReferralType] ENABLE TRIGGER [rDateModified_tPatientReferral_ReferralType]
GO
