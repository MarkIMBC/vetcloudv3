﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vInactiveEducationalLevel] AS   
				SELECT   
				 H.*  
				FROM vEducationalLevel H  
				WHERE ISNULL(IsActive, 0) = 0
GO
