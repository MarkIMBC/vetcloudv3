﻿public enum PropertyTypeEnum 
{
    String = 1,
    Int = 2,
    Decimal = 3,
    Bit = 4,
    DateTime = 5,
    Date = 6,
    Time = 7,
    Uniqueidentifier = 8,
    Object = 9,
    List = 10,
    Color = 11,
    Image = 12
}