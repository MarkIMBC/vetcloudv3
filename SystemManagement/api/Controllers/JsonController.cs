﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ngJSApp.Database;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using ngJSApp.Database.Models;
using TableDependency.SqlClient.Base;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base.EventArgs;
using ngJSApp.HubConfig;
using Microsoft.AspNetCore.SignalR;
using JSLibrary;
using System.Dynamic;
using FastReport.Table;
using FastReport;
using Newtonsoft.Json;
using ngJSApp.Helper;

namespace ngJSApp.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class JsonController : Controller
    {
        private IConfiguration _config;
        AppDbContext ctx = null;
        IAppCrypterService crypter = null;

        private readonly IHubContext<ChatHub> _chathubContext;
        public JsonController(
            IConfiguration config,
            AppDbContext _ctx,
            IHubContext<ChatHub> chathubContext,
            IAppCrypterService _crypter)
        {
            _config = config;
            this.ctx = _ctx;
            this.crypter = _crypter;

            _chathubContext = chathubContext;
        }

        [AllowAnonymous]
        [Route("FromSQLString")]
        [HttpPost]
        public ActionResult FromSQLString(string sql)
        {
            var dbAccess = new DBCollection();
            var result = dbAccess.GetDynamicObject(sql, true);


            dynamic currentObjectValues = new ExpandoObject();
            Guid g = Guid.NewGuid();
            string filename = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + StringHelper.ConvertToValidFileName(sql)  + "-" + g.ToString() + ".json";
            currentObjectValues.Date = DateTime.Now;
            currentObjectValues.PreviousObject = "{}";
            currentObjectValues.CurrentObject = result;

            var fileContent = JsonConvert.SerializeObject(currentObjectValues);

            TextLogHelper.WriteLogFile("D:\\Projects\\AuditTrailCompare\\AuditTrailCompare\\bin\\Debug\\net8.0-windows\\Files\\" + filename, fileContent);

            return Ok(new
            {
                currentObjectValues
            });
        }
    }
}
