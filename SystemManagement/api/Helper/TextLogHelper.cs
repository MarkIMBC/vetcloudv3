﻿using System.IO;
using System.Text;
using System;

namespace ngJSApp.Helper
{
    public class TextLogHelper
    {
        public static void WriteLogFile(string fileName, string value)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            if (!Directory.Exists(fileInfo.Directory.FullName))
            {
                DirectoryInfo di = Directory.CreateDirectory(fileInfo.Directory.FullName);
            }

            if (!System.IO.File.Exists(fileName))
            {
                FileStream fs = System.IO.File.Create(fileName);
                fs.Close();
            }

            if (value.Length == 0) return;

            Byte[] valueByte = new UTF8Encoding(true).GetBytes(value);
            FileStream fsWrite = new FileStream(fileName, FileMode.Append);

            fsWrite.Write(valueByte, 0, valueByte.Length);
            fsWrite.Close();
        }
    }
}
