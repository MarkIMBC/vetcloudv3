﻿using System.IO;
using System.Text.RegularExpressions;

namespace ngJSApp.Helper
{
    public class StringHelper
    {
        public static string ConvertToValidFileName(string input)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex invalidCharsRegex = new Regex($"[{Regex.Escape(regexSearch)}]");

            string validFileName = invalidCharsRegex.Replace(input, "_");

            return validFileName;
        }
    }
}
