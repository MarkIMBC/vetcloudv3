﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloundAdmin.Data
{
    public class ReturnMessage
    {
        public bool IsSuccess = true;
        public string Message { get; set; }
        public static ReturnMessage NewInstance()
        { 
            return new ReturnMessage();
        }
    }
}
