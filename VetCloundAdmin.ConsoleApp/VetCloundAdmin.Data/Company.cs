﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloundAdmin.Data
{
    public class Company
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string IsActive { get; set; }
        public string DateCreated { get; set; }
        public string Address { get; set; }
        public string Guid { get; set; }
        public string DatabaseName { get; set; }
    }
}
