﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetCloundAdmin.ConsoleApp.Library.Helpers;
using VetCloundAdmin.Data;

namespace VetCloundAdmin.ConsoleApp.Library.Services
{
    public static class ServerService
    {
        public static Server[] GetAll()
        {
            Server[] servers = new Server[0];

            DataTable dt = MainSQLQuery.GetDataTable("SELECT * FROM vActiveServer");

            servers = DatatableConverter.ToArray<Server>(dt);

            return servers;
        }
    }
}
