﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VetCloundAdmin.ConsoleApp.Library.Helpers;
using VetCloundAdmin.Data;

namespace VetCloundAdmin.ConsoleApp.Library.Services
{
    public static class SysManService
    {
        public static async Task<Company[]> GetCompaniesByUrl(string url)
        { 
            Company[] companies = DatatableConverter.ToArray<Company>(await GetCompaniesDatatableByURL(url));

            return companies;
        }
        public static async Task<DataTable> GetCompaniesDatatableByURL(string url)
        {
            DataTable dt = new DataTable();

            using (HttpClient client = new HttpClient())
            {
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(new { }), Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync(url, httpContent);
                response.EnsureSuccessStatusCode();

                string responseData = await response.Content.ReadAsStringAsync();
                responseData = responseData.Replace("'", "`");

                dynamic deserializedData = JsonConvert.DeserializeObject(responseData);

                foreach (JObject record in deserializedData.Records)
                {
                    if (dt.Columns.Count == 0)
                    {
                        foreach (var property in record.Properties())
                        {
                            dt.Columns.Add(property.Name);
                        }
                    }

                    DataRow dataRow = dt.NewRow();

                    foreach (var property in record.Properties())
                    {
                        string propertyName = property.Name;

                        dataRow[propertyName] = record[propertyName];
                    }

                    dt.Rows.Add(dataRow);
                }
            }
            return dt;
        }
    }
}
