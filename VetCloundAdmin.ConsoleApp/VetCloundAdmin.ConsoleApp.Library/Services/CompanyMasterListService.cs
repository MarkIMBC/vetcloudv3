﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetCloundAdmin.ConsoleApp.Library.Helpers;
using VetCloundAdmin.Data;

namespace VetCloundAdmin.ConsoleApp.Library.Services
{
    public static class CompanyMasterListService
    {
        public static ReturnMessage Add(Server server, Company company)
        { 
            ReturnMessage returnMessage = ReturnMessage.NewInstance();

            string sql = $"EXEC dbo.pAddCompanyMasterList " +
                $"'{company.Name.Replace("'","`")}', " +
                $"'{company.Guid}', " +
                $"'{company.Address}', " +
                $"'{Convert.ToDateTime(company.DateCreated).ToString("yyyy-MM-dd HH:mm:ss")}', " +
                $"'{company.DatabaseName}', " +
                $"'{server.ID}'" +
                $";";

            MainSQLQuery.ExecuteNonQuery(sql);
            MainSQLQuery.ExecuteNonQuery(sql);

            return returnMessage;
        }
    }
}
