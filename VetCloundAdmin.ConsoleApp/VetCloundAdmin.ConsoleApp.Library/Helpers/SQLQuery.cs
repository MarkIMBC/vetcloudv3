﻿using System.Data;
using Microsoft.Data.SqlClient;

namespace VetCloundAdmin.ConsoleApp.Library.Helpers
{
    public static class SQLQuery
    {
        public static void ExecuteNonQuery(string sql, string ConnectionString)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
        public static DataTable GetDataTable(string sql, string ConnectionString)
        {
            DataTable dataTable = new DataTable();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    SqlDataReader reader = command.ExecuteReader();

                    dataTable.Load(reader);
                }
            }

            return dataTable;
        }
        public static object GetScalarValue(string sql, string ConnectionString)
        {
            DataTable dataTable = new DataTable();
            object result = null;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    result = command.ExecuteScalar();
                }
            }

            return result;
        }
    }
}
