﻿using System.Data;
using Microsoft.Data.SqlClient;

namespace VetCloundAdmin.ConsoleApp.Library.Helpers
{
    public static class MainSQLQuery
    {
        public static void ExecuteNonQuery(string sql)
        {
            SQLQuery.ExecuteNonQuery(sql, AppSettings.ConnectionString);
        }
        public static DataTable GetDataTable(string sql)
        {
            return SQLQuery.GetDataTable(sql, AppSettings.ConnectionString);
        }
        public static object GetScalarValue(string sql)
        {
            return SQLQuery.GetScalarValue(sql, AppSettings.ConnectionString);
        }
    }
}
