﻿using Newtonsoft.Json;
using System.IO;
using Microsoft.Data.SqlClient;


namespace VetCloundAdmin.ConsoleApp.Library.Helpers
{
    public static class AppSettings
    {
        private static dynamic _AppSettings = null;
        private static string _SystemManagementConnectionString = "";
        public static AppSettingDatabase Database = new AppSettingDatabase();
        private static string _ConnectionString = "";
        public static string ConnectionString
        {
            get
            {
                if (_ConnectionString.Length == 0) SetAppSettings();

                return _ConnectionString;
            }
        }

        public static dynamic Config
        {
            get
            {
                if (_AppSettings == null) SetAppSettings();

                return _AppSettings;
            }
        }

        private static void SetAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader(@$"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            _ConnectionString = _tempObject.ConnectionString;

            _AppSettings = _tempObject;

        }
    }

    public class AppSettingDatabase
    {
        public string DataSource { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
    }
}
