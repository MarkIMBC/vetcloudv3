﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloundAdmin.ConsoleApp.Library.Helpers
{
    public static class DatatableConverter
    {
        public static T[] ToArray<T>(DataTable dt) where T : new()
        {
            var list = new System.Collections.Generic.List<T>();

            foreach (DataRow row in dt.Rows)
            {
                T obj = new T();

                foreach (DataColumn column in dt.Columns)
                {
                    var property = typeof(T).GetProperty(column.ColumnName);
                    if (property != null && row[column] != DBNull.Value)
                    {
                        property.SetValue(obj, row[column]);
                    }
                }

                list.Add(obj);
            }

            return list.ToArray();
        }
    }
}
