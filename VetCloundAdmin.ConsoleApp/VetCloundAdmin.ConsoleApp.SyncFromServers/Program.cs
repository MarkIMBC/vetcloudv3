﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VetCloundAdmin.ConsoleApp.Library.Services;
using VetCloundAdmin.Data;

class Program
{
    static async Task Main(string[] args)
    {
        foreach (Server server in ServerService.GetAll())
        {
            try
            {
                string url = server.SysmanAPIAddress + "/api/Company/GetRecords";

                Company[] companies = await SysManService.GetCompaniesByUrl(url);

                foreach (Company company in companies)
                {
                    try
                    {
                        CompanyMasterListService.Add(server, company);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Unable to Add '{company.Name}' - {ex.Message}");
                        Console.WriteLine("");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to Add '{server.Name}' - {ex.Message}");
                Console.WriteLine("");
            }
        }
    }
}
