﻿namespace ngJSApp.Helper
{
    public static class StringHelper
    {
        public static string RemoveFileExtension(string filename)
        {
            int lastDotIndex = filename.LastIndexOf('.');
            if (lastDotIndex == -1) // No extension found
            {
                return filename;
            }
            return filename.Substring(0, lastDotIndex);
        }
    }
}
