﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GlobalFx
{
    public static class ValueConverter
    {
        public static decimal ToDecimal(dynamic value)
        {
            decimal result = 0;

            if (value is DBNull) value = "0";
            if (value is null) value = "0";

            string str = value.ToString();

            if (!str.All(char.IsNumber)) value = "0";

            try
            {
                result = Convert.ToDecimal(str);
            }
            catch (Exception ex)
            {

                result = 0;
            }

            return result;
        }
        public static int ToInt(dynamic value)
        {
            int result = 0;

            if (value is DBNull) value = "0";
            if (value is null) value = "0";

            string str = value.ToString();

            if (!str.All(char.IsNumber)) value = "0";

            try
            {
                result = Convert.ToInt16(str);
            }
            catch (Exception ex)
            {

                result = 0;
            }

            return result;
        }
        public static string[] ToStringArray(dynamic values)
        {
            string[] result = new string[100];

            int i = 0;
            foreach (string value in values)
            {
                result[i] = value;
                i++;
            }

            return result;
        }
        public static string ToJSON(DataTable Dt)
        {
            string[] StrDc = new string[Dt.Columns.Count];

            string HeadStr = string.Empty;
            for (int i = 0; i < Dt.Columns.Count; i++)
            {

                StrDc[i] = Dt.Columns[i].Caption;
                HeadStr += "\"" + StrDc[i] + "\":\"" + StrDc[i] + i.ToString() + "¾" + "\",";

            }

            HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);

            StringBuilder Sb = new StringBuilder();

            Sb.Append("[");

            for (int i = 0; i < Dt.Rows.Count; i++)
            {

                string TempStr = HeadStr;

                for (int j = 0; j < Dt.Columns.Count; j++)
                {

                    TempStr = TempStr.Replace(Dt.Columns[j] + j.ToString() + "¾", Dt.Rows[i][j].ToString().Trim());
                }

                Sb.Append("{" + TempStr + "},");
            }

            Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));

            if (Sb.ToString().Length > 0)
                Sb.Append("]");

            return StripControlChars(Sb.ToString());
        }
  
        private static string StripControlChars(string s)
        {
            return Regex.Replace(s, @"[^\x20-\x7F]", "");
        }
    }
}
