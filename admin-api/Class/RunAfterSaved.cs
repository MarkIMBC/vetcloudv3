﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobalFx;
using Newtonsoft.Json;
using ngJSApp.Class;

namespace SQLLite
{
    public class RunAfterSavedSQLMapperParameter
    {
        public string ModuleName { get; set; }

        public string ObjectName { get; set; }

        public string[] ParameterNames { get; set; }
        public RunAfterSavedSQLMapperParameter()
        {

        }
        public RunAfterSavedSQLMapperParameter(string moduleName, string objectName, string[] parameterNames)
        {
            this.ModuleName = moduleName;
            this.ObjectName = objectName;
            this.ParameterNames = parameterNames;
        }
    }
    public class RunAfterSavedSQLMapperParameterCollection: System.Collections.CollectionBase
    {
        public int Add(RunAfterSavedSQLMapperParameter runAfterSavedSQLMapperParameter) {

            return this.List.Add(runAfterSavedSQLMapperParameter);
        }
    }

    public class RunAfterSaved
    {
        private SQLLiteQuery sqlLiteQuery;
        enum FilingStatusEnum: int
        {
            Filed = 1,
            Pending = 2,
            Approved = 3,
            Cancelled = 4,
            PartiallyServed = 5,
            FullyServed = 6,
            OverServed = 7,
            Waiting = 8,
            Ongoing = 9,
            Payment = 10,
            PartiallyPaid = 11,
            FullyPaid = 12,
            Done = 13,
            Confined = 14,
            Discharged = 15,
            ForBilling = 16,
            Used = 17
        }

        public static void InsertToSQLite(string databaseName, string sql, List<SqlParameter> sqlParameters)
        {
            dynamic appSettings = OtherFx.getAppSettings();
            dynamic runAfterSavedSQLMapperParameters = appSettings["RunAfterSavedSQLMapperParameters"];
            bool isEnableSQLLiteReRunAfterSaved = (bool)appSettings["IsEnableSQLLiteReRunAfterSaved"];

            if (!isEnableSQLLiteReRunAfterSaved) return;

            foreach (dynamic runAfterSavedSQLMapperParameter in runAfterSavedSQLMapperParameters)
            {
                string moduleName = runAfterSavedSQLMapperParameter.ModuleName;
                string objectName = runAfterSavedSQLMapperParameter.ObjectName;
                string[] parameterNames = ValueConverter.ToStringArray(runAfterSavedSQLMapperParameter.ParameterNames);

                List<int> listCurrentObject = new List<int>();

                if (sql.Contains(objectName))
                {
                    foreach (SqlParameter sqlParameter in sqlParameters)
                    {
                        string sqlParameterName = sqlParameter.ParameterName;
                        sqlParameterName = sqlParameterName.Replace("@", "");

                        if (parameterNames.Contains(sqlParameterName))
                        {

                            object sqlParameterValue = sqlParameter.Value;

                            foreach (int ID in GetID_CurrentObject_From_ParameterValue(sqlParameterValue))
                            {

                                listCurrentObject.Add(ID);
                            }
                        }
                    }
                }

                if (listCurrentObject.Count > 0)
                {

                    SQLLite.RunAfterSaved runAfterSaved = new SQLLite.RunAfterSaved(moduleName);

                    foreach (int ID_CurrentObject in listCurrentObject)
                    {
                        runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
                    }
                }
            }
        }

        public static void InsertToSQLite(string databaseName, string currentTableName, int ID_CurrentObject)
        {
            dynamic appSettings = OtherFx.getAppSettings();
            dynamic sqlTablesForRunAfterSaved = appSettings["SQLTablesForRunAfterSaved"];
            dynamic runAfterSavedSQLMapperParameters = appSettings["RunAfterSavedSQLMapperParameters"];
            bool isEnableSQLLiteReRunAfterSaved = (bool)appSettings["IsEnableSQLLiteReRunAfterSaved"];

            string[] sqlTables = ValueConverter.ToStringArray(sqlTablesForRunAfterSaved);

            if (!isEnableSQLLiteReRunAfterSaved) return;

            if (sqlTables.Contains(currentTableName)) {

                string moduleName = "";

                if (currentTableName.Substring(0, 1) == "t") moduleName = currentTableName.Remove(0, 1);

                SQLLite.RunAfterSaved runAfterSaved = new SQLLite.RunAfterSaved(moduleName);
                runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
            }
        }

        public static List<int> GetID_CurrentObject_From_ParameterValue(object parameterValue)
        {
            List<int> list = new List<int>();

            string _type = parameterValue.GetType().ToString();

            if (_type.Contains("DataTable"))
            {
                DataTable dt = (DataTable)parameterValue;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dt.Columns.Contains("ID"))
                    {
                        int ID = (int)dr["ID"];

                        list.Add(ID);
                    }
                }
            }

            return list;
        }
        public static void InsertPending(string moduleName, string databaseName, int ID_CurrentObject)
        {
            SQLLite.RunAfterSaved runAfterSaved = new SQLLite.RunAfterSaved(moduleName);

            runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
        }

        public static void InsertBillingInvoicePending(string databaseName, int ID_CurrentObject)
        {
            SQLLite.RunAfterSaved_BillingInvoice runAfterSaved = new SQLLite.RunAfterSaved_BillingInvoice();

            runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
        }
        public static void InsertPatient_SOAPPending(string databaseName, int ID_CurrentObject)
        {
            SQLLite.RunAfterSaved_Patient_SOAP runAfterSaved = new SQLLite.RunAfterSaved_Patient_SOAP();

            runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
        }
        public static void InsertItemPending(string databaseName, int ID_CurrentObject)
        {
            SQLLite.RunAfterSaved_Item runAfterSaved = new SQLLite.RunAfterSaved_Item();

            runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
        }

        private string tableName = "";
        private string databaseName = "";
        public RunAfterSaved(string Name) {

            this.tableName = $"tList";
            this.databaseName = $"SQLLite_VetCloud_{Name.Replace(" ", "")}";

            this.sqlLiteQuery = new SQLLiteQuery(databaseName);

            if (!this.sqlLiteQuery.IsTableExist(tableName)) {

                this.sqlLiteQuery.ExecuteNonQuery($"CREATE TABLE {this.tableName} ( " +
                    $"Date DateTime, " +
                    $"DatabaseName VARCHAR(500), " +
                    $"ID_CurrentObject INT, " +
                    $"ID_FilingStatus INT, " +
                    $"DateOngoing DateTime, " +
                    $"DateDone DateTime " +
                    $")");
            }
        }
        public DataTable getPending(int topCount = 0)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            string topCountString = "";

            if (topCount > 0) topCountString = $"TOP {topCount}";

            arg = this.sqlLiteQuery.GetDataTable($"" +
                $"SELECT { topCountString } * " +
                $"FROM {tableName} " +
                $"WHERE " +
                $"IFNULL(ID_FilingStatus, 0) IN (0, 2, 9) " +
                $"ORDER BY Date DESC ");

            return arg.datatable;
        }
        public DataTable getAll()
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            arg = this.sqlLiteQuery.GetDataTable($"SELECT * FROM {tableName}");

            return arg.datatable;
        }
        public string getAllJSONString()
        {
            DataTable dt = this.getAll();
            string result = "";

            result = ValueConverter.ToJSON(dt);

            return result;
        }

        public void deleteAllDone()
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Done;
            this.sqlLiteQuery.ExecuteNonQuery($"DELETE FROM {tableName} WHERE ID_FilingStatus IN ({ID_FilingStatus});");
        }
        public void Insert(string DatabaseName, int ID_CurrentObject)
        {
            this.sqlLiteQuery.ExecuteNonQuery($"INSERT INTO {tableName} (DatabaseName, ID_CurrentObject, Date) VALUES('{DatabaseName}', { ID_CurrentObject }, datetime('now', 'localtime'));");
        }

        public void InsertPending(string DatabaseName, int ID_CurrentObject)
        {
            if (this.IsExist(DatabaseName, ID_CurrentObject)) return;

            this.Insert(DatabaseName, ID_CurrentObject);
        }
        public bool IsExist(string DatabaseName, int ID_CurrentObject)
        {
            bool result = false;
            SQLLiteQueryResultArg arg = this.sqlLiteQuery.GetScalarValue($"SELECT COUNT(*) " +
                $"FROM {this.tableName} " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}'  AND " +
                $"IFNULL(ID_FilingStatus, 0) IN (0, 2, 9) NOT IN (0, 2, 9) ");
                
            result =ValueConverter.ToInt(arg.scalarValue) > 0;

            return result;
        }
        public void SetStatusAsOngoing(string DatabaseName, int ID_CurrentObject)
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Ongoing;

            this.sqlLiteQuery.ExecuteNonQuery($"UPDATE {tableName} " +
                $"SET ID_FilingStatus = { ID_FilingStatus }, " +
                $"DateOngoing = datetime('now', 'localtime') " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}' " +
                $"");
        }

        public void SetStatusAsDone(string DatabaseName, int ID_CurrentObject)
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Done;

            this.sqlLiteQuery.ExecuteNonQuery($"UPDATE {tableName} " +
                $"SET ID_FilingStatus = { ID_FilingStatus }, " +
                $"DateDone = datetime('now', 'localtime') " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}' " +
                $"");
        }
    }
}
