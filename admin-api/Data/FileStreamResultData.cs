﻿using System.IO;

namespace ngJSApp.Data
{
    public class FileStreamResultData
    {
        public Stream stream;
        public string mimeType;
    }
}
