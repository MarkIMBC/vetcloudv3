﻿using System;

namespace ngJSApp.Data
{
    public class AuditTrailContent
    {
        public string TableName { get; set; }
        public int ID_CurrentObject { get; set; }
        public dynamic PreviousObject { get; set; }
        public dynamic CurrentObject { get; set; }
    }
}
