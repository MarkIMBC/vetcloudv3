﻿using System;

namespace ngJSApp.Data
{
    public class AuditTrailLogInfo
    {
        private enum AuditTrailEnum
        { 
            Date = 0,
            TableName  = 1,
            ID_CurrentObject = 2,
            Guid = 3,
        }
        public DateTime Date { get; set; }
        public string TableName { get; set; }
        public int ID_CurrentObject { get; set; }
        public string Guid { get; set; }
        public AuditTrailLogInfo(string fileName)
        { 
            string result = Helper.StringHelper.RemoveFileExtension(fileName);
            string[] splittedResults = result.Split('-');

            this.Date = DateTime.Parse(splittedResults[(int)AuditTrailEnum.Date]);
            this.TableName = splittedResults[(int)AuditTrailEnum.TableName];
            this.ID_CurrentObject = int.Parse(splittedResults[(int)AuditTrailEnum.ID_CurrentObject]);
            this.Guid = splittedResults[(int)AuditTrailEnum.Guid];
        }
    }
}
