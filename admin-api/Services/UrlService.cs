﻿using Microsoft.AspNetCore.Http;

namespace ngJSApp.Services
{
    public interface IUrlService
    {
        string GetCurrentUrl();
        string GetBaseUrl();
    }
    public class UrlService : IUrlService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UrlService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public string GetCurrentUrl()
        {
            var request = _httpContextAccessor.HttpContext?.Request;
            if (request == null)
            {
                return null;
            }
            var currentUrl = $"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}";
            return currentUrl;
        }
        public string GetBaseUrl()
        {
            var request = _httpContextAccessor.HttpContext?.Request;
            if (request == null)
            {
                return null;
            }
            var currentUrl = $"{request.Scheme}://{request.Host}";
            return currentUrl;
        }
    }

}
