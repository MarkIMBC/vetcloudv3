﻿using System.IO;
using System.Text;
using System;

namespace ngJSApp.Services
{
    public interface ITextFilerWriterService
    {
        public void Create(string fileName, string content);
    }
    public class TextFilerWriterService : ITextFilerWriterService
    {
        public void Create(string fileName, string content)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            if (!Directory.Exists(fileInfo.Directory.FullName))
            {
                DirectoryInfo di = Directory.CreateDirectory(fileInfo.Directory.FullName);
            }

            if (!System.IO.File.Exists(fileName))
            {
                FileStream fs = System.IO.File.Create(fileName);
                fs.Close();
            }

            if (content.Length == 0) return;

            Byte[] valueByte = new UTF8Encoding(true).GetBytes(content);
            FileStream fsWrite = new FileStream(fileName, FileMode.Append);

            fsWrite.Write(valueByte, 0, valueByte.Length);
        }
    }
}
