﻿using Microsoft.AspNetCore.Mvc;
using Google.Apis.Services;
using System.IO;
using System.Threading.Tasks;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using System.Threading;
using System;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using ngJSApp.Data;
using System.Net.Http;

namespace ngJSApp.Services.MirgrationServices
{
    public class GoogleDriveService: Services.MigrationService
    {
        private readonly DriveService _driveService;
        private IConfiguration _configuration;
        public GoogleDriveService(IConfiguration configuration)
        {
            this._configuration = configuration;

            string credentialsPath = this._configuration.GetValue<string>("GoogleDriveCredentialConfigPath");
            string[] Scopes = { DriveService.Scope.Drive };

            UserCredential credential;
            using (var stream = new FileStream(credentialsPath + @"/google-drive-credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = credentialsPath + @"/token";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            _driveService = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "VetCloud Image Migration Tool App v1",
            });

        }
        public override async Task<Data.FileStreamResultData> GetImageAsync(string fileId)
        {
            Data.FileStreamResultData fileStreamResultData = new Data.FileStreamResultData();

            var request = _driveService.Files.Get(fileId);

            var file = await request.ExecuteAsync();
            string mimeType = file.MimeType;

            var stream = new MemoryStream();
            await request.DownloadAsync(stream);
            stream.Position = 0;

            fileStreamResultData.stream = stream;
            fileStreamResultData.mimeType = mimeType;

            return fileStreamResultData;
        }
        public override async Task<Data.FileStreamResultData> GetThumbnailAsync(string fileId)
        {
            string thumbNailURL = await this.GetThumbnailUrlAsync(fileId);

            return await this.FromImageUrlAsync(thumbNailURL);
        }
        public override async Task<string> GetThumbnailUrlAsync(string fileId)
        {
            var request = _driveService.Files.Get(fileId);
            request.Fields = "thumbnailLink";

            var file = request.Execute();

            return file.ThumbnailLink;
        }
        private async Task<dynamic> GetFileMetadataAsync(string fileId)
        {
            var request = _driveService.Files.Get(fileId);
            return await request.ExecuteAsync();
        }

        private async Task<FileStreamResultData> FromImageUrlAsync(string imageUrl)
        {
            Data.FileStreamResultData fileStreamResultData = new Data.FileStreamResultData();

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(imageUrl);

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed to download image from {imageUrl}. Status code: {response.StatusCode}");
                }

                var stream = await response.Content.ReadAsStreamAsync();
                var mimeType = response.Content.Headers.ContentType?.ToString() ?? "application/octet-stream";

                fileStreamResultData.stream = stream;
                fileStreamResultData.mimeType = mimeType;

                return fileStreamResultData;
            }
        }

    }
}
