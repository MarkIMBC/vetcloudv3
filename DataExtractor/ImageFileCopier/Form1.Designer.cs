﻿namespace ImageFileCopier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFileFound = new System.Windows.Forms.TextBox();
            this.txtFileNotFound = new System.Windows.Forms.TextBox();
            this.txtFileTobeCopy = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(12, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(785, 23);
            this.textBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(799, 87);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFolder.Location = new System.Drawing.Point(12, 87);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(785, 23);
            this.txtOutputFolder.TabIndex = 2;
            this.txtOutputFolder.Text = "Output";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Folder Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Company ID (separator \",\")";
            // 
            // txtFileFound
            // 
            this.txtFileFound.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileFound.Location = new System.Drawing.Point(12, 302);
            this.txtFileFound.Multiline = true;
            this.txtFileFound.Name = "txtFileFound";
            this.txtFileFound.ReadOnly = true;
            this.txtFileFound.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFileFound.Size = new System.Drawing.Size(438, 278);
            this.txtFileFound.TabIndex = 5;
            this.txtFileFound.Text = "Output";
            // 
            // txtFileNotFound
            // 
            this.txtFileNotFound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileNotFound.Location = new System.Drawing.Point(456, 302);
            this.txtFileNotFound.Multiline = true;
            this.txtFileNotFound.Name = "txtFileNotFound";
            this.txtFileNotFound.ReadOnly = true;
            this.txtFileNotFound.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFileNotFound.Size = new System.Drawing.Size(418, 278);
            this.txtFileNotFound.TabIndex = 6;
            this.txtFileNotFound.Text = "Output";
            // 
            // txtFileTobeCopy
            // 
            this.txtFileTobeCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileTobeCopy.Location = new System.Drawing.Point(12, 116);
            this.txtFileTobeCopy.Multiline = true;
            this.txtFileTobeCopy.Name = "txtFileTobeCopy";
            this.txtFileTobeCopy.ReadOnly = true;
            this.txtFileTobeCopy.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFileTobeCopy.Size = new System.Drawing.Size(862, 180);
            this.txtFileTobeCopy.TabIndex = 7;
            this.txtFileTobeCopy.Text = "Output";
            this.txtFileTobeCopy.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 592);
            this.Controls.Add(this.txtFileTobeCopy);
            this.Controls.Add(this.txtFileNotFound);
            this.Controls.Add(this.txtFileFound);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtOutputFolder);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VetCloud Image Copier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFileFound;
        private System.Windows.Forms.TextBox txtFileNotFound;
        private System.Windows.Forms.TextBox txtFileTobeCopy;
    }
}