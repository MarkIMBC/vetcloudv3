﻿using JSLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ImageFileCopier
{
    public partial class Form1 : Form
    {

        dynamic _appSettings;
        public Form1()
        {
            InitializeComponent();

            this._appSettings = this.getAppSettings();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var sql = @$"exec pGetCompanyImageFileNames '{this.textBox1.Text}'";
            var connectionString = this._appSettings.ConnectionString;
            var dbAccess = new DBCollection(connectionString);
            var result = dbAccess.GetDynamicObject(sql, true);
            IDictionary<string, dynamic> dataSources = (IDictionary<string, dynamic>)result;

            this.txtFileTobeCopy.Text = "";
            this.txtFileFound.Text = "";
            this.txtFileNotFound.Text = "";

            this.GenerateImageCopier(dataSources);

            MessageBox.Show("Done");
        }
        dynamic getAppSettings()
        {
            StreamReader r = new StreamReader(@$"appsettings.json");
            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }
        void GenerateImageCopier(IDictionary<string, dynamic> dataSources) {

            foreach (IDictionary<string, dynamic> company in dataSources["Company"])
            {
                foreach (IDictionary<string, dynamic> ImagePath in company["ImagePaths"])
                {
                    this.txtFileTobeCopy.Text += ImagePath["Filename"] + Environment.NewLine;

                    string folder = "";


                    if (dataSources["IsFolderized"]) {

                        folder = this.GetValidFileName((string)company["Name"]);
                    }

                    string sourceImageFilename = @$"../api/wwwroot/Content/Image/{ImagePath["Filename"]}";
                    string destinationImageFilename = @$"{folder}/Content/Image/{ImagePath["Filename"]}";

                    string sourceThumbnailFilename = @$"../api/wwwroot/Content/Thumbnail/{ImagePath["Filename"]}";
                    string destinationThumbnailFilename = @$"{folder}/Content/Thumbnail/{ImagePath["Filename"]}";


                    if (this.copyFile(sourceImageFilename, destinationImageFilename))
                    {

                        this.txtFileFound.Text += sourceImageFilename + Environment.NewLine;
                    }
                    else {

                        this.txtFileNotFound.Text += sourceImageFilename + Environment.NewLine;
                    }

                    if (this.copyFile(sourceThumbnailFilename, destinationThumbnailFilename))
                    {

                        this.txtFileFound.Text += sourceThumbnailFilename + Environment.NewLine;
                    }
                    else
                    {
                        this.txtFileNotFound.Text += sourceThumbnailFilename + Environment.NewLine;
                    }

                }
            }
        }

        bool copyFile(string sourceFilename, string destinationFilename) {

            FileInfo sourceFile = new FileInfo(@$"{sourceFilename}");
            FileInfo destinationFile = new FileInfo(@$"{this.txtOutputFolder.Text}/{destinationFilename}");

            bool isSuccess = true;

            try
            {
                Directory.CreateDirectory(destinationFile.DirectoryName);
                File.Copy(sourceFile.FullName, destinationFile.FullName, true);

            }
            catch (IOException iox)
            {
                this.txtFileNotFound.Text += destinationFile.Name + Environment.NewLine;
                isSuccess = false;
            }

            return isSuccess;
        }

        string GetValidFileName(string text)
        {
            text = text.Replace('\'', '’'); // U+2019 right single quotation mark
            text = text.Replace('"', '”'); // U+201D right double quotation mark
            text = text.Replace('/', '⁄');  // U+2044 fraction slash
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                text = text.Replace(c, '_');
            }
            return text;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
