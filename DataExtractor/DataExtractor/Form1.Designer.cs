﻿namespace DataExtractor
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radiobtn_ExtractInOneSheet = new System.Windows.Forms.RadioButton();
            this.radiobtn_ExtractPerSheet = new System.Windows.Forms.RadioButton();
            this.txtStoredPrecedure = new System.Windows.Forms.TextBox();
            this.txtParameters = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInitialCatalog = new System.Windows.Forms.TextBox();
            this.txtDataSource = new System.Windows.Forms.TextBox();
            this.cboServer = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(12, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(580, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Stored Procedure";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radiobtn_ExtractInOneSheet);
            this.groupBox1.Controls.Add(this.radiobtn_ExtractPerSheet);
            this.groupBox1.Location = new System.Drawing.Point(314, 111);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(277, 76);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Extract Type";
            // 
            // radiobtn_ExtractInOneSheet
            // 
            this.radiobtn_ExtractInOneSheet.AutoSize = true;
            this.radiobtn_ExtractInOneSheet.Location = new System.Drawing.Point(130, 28);
            this.radiobtn_ExtractInOneSheet.Name = "radiobtn_ExtractInOneSheet";
            this.radiobtn_ExtractInOneSheet.Size = new System.Drawing.Size(107, 19);
            this.radiobtn_ExtractInOneSheet.TabIndex = 8;
            this.radiobtn_ExtractInOneSheet.Text = "One Sheet Only";
            this.radiobtn_ExtractInOneSheet.UseVisualStyleBackColor = true;
            this.radiobtn_ExtractInOneSheet.CheckedChanged += new System.EventHandler(this.radiobtn_ExtractInOneSheet_CheckedChanged);
            // 
            // radiobtn_ExtractPerSheet
            // 
            this.radiobtn_ExtractPerSheet.AutoSize = true;
            this.radiobtn_ExtractPerSheet.Checked = true;
            this.radiobtn_ExtractPerSheet.Location = new System.Drawing.Point(9, 28);
            this.radiobtn_ExtractPerSheet.Name = "radiobtn_ExtractPerSheet";
            this.radiobtn_ExtractPerSheet.Size = new System.Drawing.Size(113, 19);
            this.radiobtn_ExtractPerSheet.TabIndex = 7;
            this.radiobtn_ExtractPerSheet.TabStop = true;
            this.radiobtn_ExtractPerSheet.Text = "Extract Per Sheet";
            this.radiobtn_ExtractPerSheet.UseVisualStyleBackColor = true;
            this.radiobtn_ExtractPerSheet.CheckedChanged += new System.EventHandler(this.radiobtn_ExtractPerSheet_CheckedChanged);
            // 
            // txtStoredPrecedure
            // 
            this.txtStoredPrecedure.Location = new System.Drawing.Point(12, 68);
            this.txtStoredPrecedure.Name = "txtStoredPrecedure";
            this.txtStoredPrecedure.Size = new System.Drawing.Size(296, 23);
            this.txtStoredPrecedure.TabIndex = 8;
            this.txtStoredPrecedure.Text = "pGetExtract_SendSOAPPlanPerCompany";
            this.txtStoredPrecedure.TextChanged += new System.EventHandler(this.txtStoredPrecedure_TextChanged);
            // 
            // txtParameters
            // 
            this.txtParameters.Location = new System.Drawing.Point(12, 118);
            this.txtParameters.Multiline = true;
            this.txtParameters.Name = "txtParameters";
            this.txtParameters.Size = new System.Drawing.Size(296, 69);
            this.txtParameters.TabIndex = 9;
            this.txtParameters.Text = "\'2022-01-01\',  \'2021-01-01\',  \'\' ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Parameters";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(314, 68);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(278, 23);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "Output";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "Folder Name";
            // 
            // txtInitialCatalog
            // 
            this.txtInitialCatalog.Location = new System.Drawing.Point(314, 15);
            this.txtInitialCatalog.Name = "txtInitialCatalog";
            this.txtInitialCatalog.ReadOnly = true;
            this.txtInitialCatalog.Size = new System.Drawing.Size(278, 23);
            this.txtInitialCatalog.TabIndex = 12;
            this.txtInitialCatalog.Visible = false;
            // 
            // txtDataSource
            // 
            this.txtDataSource.Location = new System.Drawing.Point(12, 15);
            this.txtDataSource.Name = "txtDataSource";
            this.txtDataSource.ReadOnly = true;
            this.txtDataSource.Size = new System.Drawing.Size(296, 23);
            this.txtDataSource.TabIndex = 10;
            this.txtDataSource.Visible = false;
            this.txtDataSource.TextChanged += new System.EventHandler(this.txtDataSource_TextChanged);
            // 
            // cboServer
            // 
            this.cboServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboServer.FormattingEnabled = true;
            this.cboServer.Location = new System.Drawing.Point(57, 12);
            this.cboServer.Name = "cboServer";
            this.cboServer.Size = new System.Drawing.Size(535, 23);
            this.cboServer.TabIndex = 15;
            this.cboServer.SelectedIndexChanged += new System.EventHandler(this.cboServer_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "Server";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 246);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboServer);
            this.Controls.Add(this.txtInitialCatalog);
            this.Controls.Add(this.txtDataSource);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtParameters);
            this.Controls.Add(this.txtStoredPrecedure);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vet Cloud Data Extractor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radiobtn_ExtractInOneSheet;
        private System.Windows.Forms.RadioButton radiobtn_ExtractPerSheet;
        private System.Windows.Forms.TextBox txtStoredPrecedure;
        private System.Windows.Forms.TextBox txtParameters;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDataSource;
        private System.Windows.Forms.TextBox txtInitialCatalog;
        private System.Windows.Forms.ComboBox cboServer;
        private System.Windows.Forms.Label label7;
    }
}
