﻿using JSLibrary;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataExtractor
{
    public partial class Form1 : Form
    {
        ExcelExtractTypeEnum currenrtExcelExtractType = ExcelExtractTypeEnum.PerSheet;
        dynamic _appSettings;
        SqlConnectionStringBuilder connectionString;

        public Form1()
        {
            InitializeComponent();

            this._appSettings = this.getAppSettings();

            DataTable dt = ToDataTable(_appSettings.ConnectionStringDatasource);
            this.cboServer.DataSource = dt;
            this.cboServer.ValueMember = "ConnectionString";
            this.cboServer.DisplayMember = "Name";
        }
        public DataTable ToDataTable(dynamic items)
        {

            var data = items.ToArray();
            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                dt.Columns.Add(key);
            }
            foreach (var d in data)
            {
                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
            }
            return dt;
        }
        dynamic getAppSettings()
        {
            StreamReader r = new StreamReader(@$"appsettings.json");
            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        void GenerateHeaderRowWorkSheet(ref int startingRowIndex, ExcelWorksheet worksheet, dynamic templateFormat, IDictionary<string, dynamic> record)
        {
            int columnIndex = 1;

            foreach (IDictionary<string, dynamic> format in templateFormat.Columns)
            {
                foreach (string key in record.Keys)
                {
                    if (format["ColumnName"] == key)
                    {
                        string ColumnName = format["ColumnName"];
                        string Caption = format["Caption"];

                        ExcelRangeBase cellLabel = worksheet.Cells[startingRowIndex, columnIndex];

                        if (Caption is null) Caption = "";
                        if (Caption.Length > 0) ColumnName = Caption;


                        cellLabel.Value = ColumnName;
                        cellLabel.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        cellLabel.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        cellLabel.Style.Font.Bold = true;

                        worksheet.Rows[startingRowIndex].CustomHeight = true;
                        worksheet.Rows[startingRowIndex].Height = 35;

                        cellLabel.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        cellLabel.Style.Border.Top.Color.SetColor(Color.Black);

                        cellLabel.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        cellLabel.Style.Border.Left.Color.SetColor(Color.Black);

                        cellLabel.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        cellLabel.Style.Border.Bottom.Color.SetColor(Color.Black);

                        cellLabel.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        cellLabel.Style.Border.Right.Color.SetColor(Color.Black);

                        columnIndex++;
                    }
                }
            }

            startingRowIndex++;
        }

        void GenerateDataRowWorkSheet(ref int startingRowIndex, ExcelWorksheet worksheet, dynamic templateFormat, IDictionary<string, dynamic> record)
        {
            int columnIndex = 1;
            foreach (IDictionary<string, dynamic> format in templateFormat.Columns)
            {
                foreach (string key in record.Keys)
                {
                    if (format["ColumnName"] == key)
                    {
                        string ColumnName = format["ColumnName"];

                        int ID_PropertyType = (int)format["ID_PropertyType"];
                        int ID_TextAlign = (int)format["ID_TextAlign"];
                        string formatString = format["formatString"];
                        dynamic value = record[ColumnName];


                        ExcelRangeBase cellValue = worksheet.Cells[startingRowIndex, columnIndex];

                        if (format.ContainsKey("HyperLinkOptions"))
                        {
                            dynamic HyperLinkOptions = format["HyperLinkOptions"];
                            string UriFileName = HyperLinkOptions.UriFileName;

                            UriFileName = UriFileName.Replace("{" + ColumnName + "}", value);

                            cellValue.Hyperlink = new Uri(UriFileName, UriKind.Relative);
                        }

                        cellValue.Style.Numberformat.Format = "@";
                        cellValue.Style.HorizontalAlignment = (OfficeOpenXml.Style.ExcelHorizontalAlignment) ID_TextAlign;
                        cellValue.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                        if (value != null)
                        {

                            if (formatString is null) formatString = "";
                            value = this.formatCellValue(cellValue, ID_PropertyType, formatString, value);
                            cellValue.Value = value;
                        }

                        columnIndex++;
                    }
                }

            }
            startingRowIndex++;
        }

        dynamic formatCellValue(ExcelRangeBase cellValue, int ID_PropertyType, string formatString, dynamic value)
        {
            dynamic result = value;
            var PropertyType = (PropertyTypeEnum)ID_PropertyType;

            if (value is null) return value;

            if (PropertyType == PropertyTypeEnum.Date || PropertyType == PropertyTypeEnum.DateTime)
            {
                cellValue.Style.Numberformat.Format = formatString;
                DateTime obj = (DateTime)value;

                return GetExcelDecimalValueForDate(obj);
            }
            else if (PropertyType == PropertyTypeEnum.String)
            {
                string valueString = (string)value;
                char tab = '\u0009';

                valueString = valueString.Replace(tab.ToString(), "");
                valueString = valueString.Replace("\r", "\n");

                return valueString;
            }
            else if (PropertyType == PropertyTypeEnum.Int || PropertyType == PropertyTypeEnum.Decimal)
            {
                cellValue.Style.Numberformat.Format = "0";
             
                return value;
            }

            return value;
        }
        private static decimal GetExcelDecimalValueForDate(DateTime date)
        {
            DateTime start = new DateTime(1900, 1, 1);
            TimeSpan diff = date - start;
            return diff.Days + 2;
        }

        void GenerateDetailTableWorkSheet(ref int startingRowIndex, ExcelWorksheet worksheet, dynamic templateFormat, dynamic dataSource)
        {
            bool isGenerateHeader = false;

            foreach (IDictionary<string, dynamic> record in dataSource)
            {
                if (isGenerateHeader == false)
                {
                    this.GenerateHeaderRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, record);
                    isGenerateHeader = true;
                }
                this.GenerateDataRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, record);
            }
        }

        void AutoFillWorkSheetColumns(ExcelWorksheet worksheet, dynamic templateFormat)
        {
            int columnIndex = 1;
            foreach (IDictionary<string, dynamic> format in templateFormat.Columns)
            {

                if (format.ContainsKey("Width"))
                {
                    worksheet.Columns[columnIndex].Width = (double)format["Width"];
                }
                else {

                    worksheet.Columns[columnIndex].AutoFit();
                }

                columnIndex++;
            }
        }

        dynamic getExportExcelFormat(string filename)
        {

            StreamReader r = new StreamReader(@$"ExportTemplate/{filename}.json");
            string jsonString = r.ReadToEnd();
            ExpandoObject _tempObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString);

            r.Close();

            return _tempObject;
        }

        static string GetValidFileName(string text)
        {
            text = text.Replace('\'', '’'); // U+2019 right single quotation mark
            text = text.Replace('"', '”'); // U+201D right double quotation mark
            text = text.Replace('/', '⁄');  // U+2044 fraction slash
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                text = text.Replace(c, '_');
            }
            return text;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string sql = @$"{ this.txtStoredPrecedure.Text } {this.txtParameters.Text}";
         
            var dbAccess = new DBCollection(connectionString.ConnectionString);
            var result = dbAccess.GetDynamicObject(sql, true);
            var resultformatData = this.getExportExcelFormat(@$"{this.txtStoredPrecedure.Text}");

            IDictionary<string, dynamic> dataSources = (IDictionary<string, dynamic>)result;
            dynamic templateFormats = (dynamic)resultformatData.template;
            string MainHeader = resultformatData.MainHeader;

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            if (this.currenrtExcelExtractType == ExcelExtractTypeEnum.OnlyOneSheet)
            {
                this.ExcelExtractType_OnlyOneSheet(dataSources);
            }
            else if (this.currenrtExcelExtractType == ExcelExtractTypeEnum.PerSheet)
            {
                this.ExcelExtractType_PerSheet(dataSources);
            }

            MessageBox.Show("Export Done...");
        }

        private void radiobtn_ExtractInOneSheet_CheckedChanged(object sender, EventArgs e)
        {
            this.currenrtExcelExtractType = ExcelExtractTypeEnum.OnlyOneSheet;
        }
        private void radiobtn_ExtractPerSheet_CheckedChanged(object sender, EventArgs e)
        {
            this.currenrtExcelExtractType = ExcelExtractTypeEnum.PerSheet;
        }
        public void ExcelExtractType_OnlyOneSheet(IDictionary<string, dynamic> dataSources)
        {

            var resultformatData = this.getExportExcelFormat(@$"{this.txtStoredPrecedure.Text}");

            dynamic templateFormats = (dynamic)resultformatData.template;
            string MainHeader = resultformatData.MainHeader;
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            foreach (IDictionary<string, dynamic> obj in dataSources[MainHeader])
            {
                ExcelPackage excelPackage = new ExcelPackage();
                int startingRowIndex = 1;

                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet 1");
                worksheet.View.ShowGridLines = false;

                foreach (dynamic templateFormat in templateFormats)
                {

                    if (MainHeader == templateFormat.Name)
                    {
                        ExcelRangeBase cellLabel = worksheet.Cells[startingRowIndex, 1];
                        cellLabel.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        cellLabel.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        cellLabel.Style.Font.Bold = true;
                        cellLabel.Style.Font.Italic = true;
                        cellLabel.Value = templateFormat.Caption;


                        worksheet.Cells[startingRowIndex, 1, startingRowIndex, 27].Merge = true;
                        startingRowIndex++;

                        this.GenerateHeaderRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, obj);
                        this.GenerateDataRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, obj);
                        this.AutoFillWorkSheetColumns(worksheet, templateFormat);
                        startingRowIndex++;
                        startingRowIndex++;
                    }
                    else
                    {

                        if (obj.ContainsKey(templateFormat.Name))
                        {
                            List<object> dataSource = obj[templateFormat.Name];
                            if (dataSource.Count > 0)
                            {
                                ExcelRangeBase cellLabel = worksheet.Cells[startingRowIndex, 1];
                                cellLabel.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                cellLabel.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                cellLabel.Style.Font.Bold = true;
                                cellLabel.Style.Font.Italic = true;
                                cellLabel.Value = templateFormat.Caption;


                                worksheet.Cells[startingRowIndex, 1, startingRowIndex, 27].Merge = true;
                                startingRowIndex++;

                                this.GenerateDetailTableWorkSheet(ref startingRowIndex, worksheet, templateFormat, dataSource);
                                this.AutoFillWorkSheetColumns(worksheet, templateFormat);
                                startingRowIndex++;
                                startingRowIndex++;
                            }
                        }
                    } 
                }

                this.SaveToExcelFile(excelPackage, resultformatData, obj);
            }
        }

        public void ExcelExtractType_PerSheet(IDictionary<string, dynamic> dataSources)
        {
            var resultformatData = this.getExportExcelFormat(@$"{this.txtStoredPrecedure.Text}");

            dynamic templateFormats = (dynamic)resultformatData.template;
            string MainHeader = resultformatData.MainHeader;

            foreach (IDictionary<string, dynamic> obj in dataSources[MainHeader])
            {
                ExcelPackage excelPackage = new ExcelPackage();
            
                foreach (dynamic templateFormat in templateFormats)
                {
                    int startingRowIndex = 1;

                    if (MainHeader == templateFormat.Name)
                    {
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(templateFormat.Caption);

                        worksheet.Cells[startingRowIndex, 1, startingRowIndex, 4].Merge = true;

                        this.GenerateHeaderRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, obj);
                        this.GenerateDataRowWorkSheet(ref startingRowIndex, worksheet, templateFormat, obj);
                        this.AutoFillWorkSheetColumns(worksheet, templateFormat);
                    }
                    else
                    {

                        if (obj.ContainsKey(templateFormat.Name))
                        {
                            List<object> dataSource = obj[templateFormat.Name];
                            if (dataSource.Count > 0)
                            {
                                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add(templateFormat.Caption);

                                this.GenerateDetailTableWorkSheet(ref startingRowIndex, worksheet, templateFormat, dataSource);
                                this.AutoFillWorkSheetColumns(worksheet, templateFormat);
                            }
                        }
                    }

                }

                this.SaveToExcelFile(excelPackage, resultformatData, obj);
            }
        }
        private void SaveToExcelFile(ExcelPackage excelPackage, dynamic resultformatData, IDictionary<string, dynamic> obj)
        {
            if (excelPackage.Workbook.Worksheets.Count == 0) return;

            string filename = @$"{this.textBox1.Text.TrimStart().TrimEnd()}";
            string directoryPath = @$"ExportFiles/{ filename }";
            string fileName = resultformatData.FilenameFormat;

            foreach (string key in obj.Keys)
            {
                if (fileName.Contains("{" + key + "}"))
                {

                    string valueString = (string)obj[key];

                    if (valueString == null) valueString = "NoData";

                    valueString = valueString.Trim();

                    valueString = GetValidFileName(valueString);

                    fileName = fileName.Replace("{" + key + "}", valueString);
                }
            }

            fileName = fileName.Replace("{CONSTANTDATESTRING_YYYYMMDD_HHMMSS}", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
       
            Directory.CreateDirectory(directoryPath);
            FileInfo fi1 = new FileInfo(@$"{ directoryPath }/{fileName}.xlsx");

            excelPackage.SaveAs(fi1);
            excelPackage.Dispose();

        }

        private void txtStoredPrecedure_TextChanged(object sender, EventArgs e)
        {

        }


        void loadConnectionString() {
            
          
        }

        void SetConnectionString()
        {
      
        }

 
        private void txtDataSource_TextChanged(object sender, EventArgs e)
        {
            this.SetConnectionString();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            string input = "";
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Confirmation Password";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            textBox.PasswordChar = '*';
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            if (inputBox.ShowDialog() == DialogResult.OK)
            {

                this.txtInitialCatalog.ReadOnly = (textBox.Text != "liv12345sqL$ss11");
                this.txtDataSource.ReadOnly = (textBox.Text != "liv12345sqL$ss11");
            }
            else {

                this.txtInitialCatalog.ReadOnly = true;
                this.txtDataSource.ReadOnly = true;
            }
        }

        private void cboServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView drView =(DataRowView) this.cboServer.SelectedItem;
            string conString = (string) drView.Row["ConnectionString"];

            this.connectionString = new SqlConnectionStringBuilder(conString);

            this.txtDataSource.Text = connectionString.DataSource;
            this.txtInitialCatalog.Text = connectionString.InitialCatalog;
        }
    }


    public enum ExcelExtractTypeEnum
    {

        PerSheet,
        OnlyOneSheet
    }
}
