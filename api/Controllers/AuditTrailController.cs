﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ngJSApp.Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ngJSApp.Controllers
{
    [CustomAuthorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AuditTrailController : ControllerBase
    {
        private IAuditTrailLogService _auditTrailLogService;
        public AuditTrailController(IAuditTrailLogService auditTrailLogService)
        {
            this._auditTrailLogService = auditTrailLogService;
        }
    }
}


