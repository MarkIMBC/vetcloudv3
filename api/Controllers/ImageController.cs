﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ngJSApp.Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ngJSApp.Controllers
{
    [CustomAuthorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private IMigrationService _migrationService = null;
        private IImageURLService _imageURLService = null;
        private IImageContentService _imageContentService = null;
        public ImageController(IMigrationService migrationService, IImageContentService imageContentService, IImageURLService imageURLService)
        {
            this._migrationService = migrationService;
            this._imageURLService = imageURLService;
            this._imageContentService = imageContentService;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetImage/{fileId}")]
        public async Task<IActionResult> GetImage(string fileId)
        {
            try
            {
                Data.FileStreamResultData streamData = new Data.FileStreamResultData();

                if (this._imageContentService.IsImageExist(fileId))
                {
                    streamData = this._imageContentService.GetImage(fileId);
                }
                else
                {
                    streamData = await this._migrationService.GetImageAsync(fileId);
                }

                return File(streamData.stream, streamData.mimeType);
            }
            catch (Exception ex)
            {
                return BadRequest("Unable to load image.");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetThumbnail/{fileId}")]
        public async Task<IActionResult> GetThumbnail(string fileId)
        {
            try
            {
                Data.FileStreamResultData streamData = new Data.FileStreamResultData();

                if (this._imageContentService.IsThumbnailExist(fileId))
                {
                    streamData = this._imageContentService.GetThumbnail(fileId);
                }
                else if (this._imageContentService.IsImageExist(fileId))
                {
                    streamData = this._imageContentService.GetImage(fileId);
                }
                else
                {
                    streamData = await this._migrationService.GetThumbnailAsync(fileId);
                }

                return File(streamData.stream, streamData.mimeType);
            }
            catch (Exception ex)
            {
                return BadRequest("Unable to load image.");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetImageURL/{fileId}")]
        public async Task<IActionResult> GetImageURL(string fileId)
        {
            string sharedStringURL = await this._imageURLService.GetImage(fileId);

            return Ok(new
            {
                sharedStringURL
            });
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("GetThumbnailURL/{fileId}")]
        public async Task<IActionResult> GetThumbnailURL(string fileId)
        {
            string sharedStringURL = await this._imageURLService.GetThumbnail(fileId);

            return Ok(new
            {
                sharedStringURL
            });
        }
    }
}
