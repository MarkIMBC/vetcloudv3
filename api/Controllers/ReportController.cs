﻿using System;
using FastReport.Web;
using Microsoft.AspNetCore.Mvc;
using FastReport.Data;
using ngJSApp.Database;
using Microsoft.EntityFrameworkCore;
using FastReport.Utils;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using ngJSApp.Controllers;
using System.Linq;
using FastReport;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using FastReport.Export.PdfSimple;
using System.Threading.Tasks;
using OfficeOpenXml;
using JSLibrary;
using Newtonsoft.Json;

namespace ngJSApp
{

    [AllowAnonymous]
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        protected AppDbContext ctx = null;
        public ReportController(AppDbContext _ctx) {
            this.ctx = _ctx;
        }

        [HttpGet("Get/{Oid_Report}")]
        public IActionResult Get(string Oid_Report) {
            var rpt = this.ctx._tReport.SingleOrDefault(d => d.Oid.ToString() == Oid_Report);
            return Ok(rpt);
        }

        [HttpPost("Viewer/{Oid_Report}")]
        public async Task<IActionResult> Viewer(string Oid_Report)
        {

            var webReport = this.GetWebReport(Oid_Report);
            if (webReport == null) return NotFound("Report'" + Oid_Report + "' not found");

            webReport.ShowPreparedReport = false;
            webReport.ShowToolbar = true;


            ViewBag.ID_WebReport = webReport.ID;
            ViewBag.WebReport = webReport;
            return View();
        }

        [HttpPost("PrintHTML/{Oid_Report}")]
        public async Task<IActionResult> PrintHTML(string Oid_Report)
        {
            var webReport = this.GetWebReport(Oid_Report);
            if (webReport == null) return NotFound("Report'" + Oid_Report + "' not found");

            webReport.ShowPreparedReport = false;
            webReport.Report.Prepare(true);

            return webReport.PrintHtml();
        }

        [HttpPost("Excel/{Oid_Report}")]
        public ActionResult Excel(string Oid_Report)
        {
            ExportExcel exportExcelFormat = this.getExportExcelFormat(Oid_Report);
            DataTable dataSource;
            var dbAccess = new DBCollection();
            var webReport = this.GetInitWebReport(Oid_Report);
            var sql = this.GetWebReportSQLString(webReport);

            string[] groupNames = (from groupName in exportExcelFormat.groups.AsQueryable()
                                   select groupName.name).ToArray();

            string[] columnNames = (from columnName in exportExcelFormat.columns.AsQueryable()
                                    select columnName.expression == null ? columnName.name : columnName.expression + " " + columnName.name).ToArray();

            if (groupNames.Length == 0)
            {
                dataSource = dbAccess.getDataTable(sql);
            }
            else {
                string _sql = "" +
                   "SELECT " + String.Join(", ", columnNames) + " " +
                   "FROM " +
                   "   (" +
                   "   " + sql.ToString() + "" +
                   "   ) tbl " +
                   " ";

                if (groupNames.Length > 0)
                {
                    _sql += "GROUP BY " + String.Join(", ", groupNames) + " ";
                }

                if (exportExcelFormat.sort.Length > 0)
                {
                    _sql += "ORDER BY " + exportExcelFormat.sort + " ";
                }

                dataSource = dbAccess.getDataTable(_sql);
            }

            DataView dView = new DataView(dataSource);

            dView.Sort = exportExcelFormat.sort;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
           
            string filePath = "";
            string fullName = "your file name";
            string fileName = "your file name";

            string GuidString = Guid.NewGuid().ToString();

            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet 1");
                int startingRowIndex = 1;

                this.filleExcelWorksheetHeader(ref startingRowIndex, dView, exportExcelFormat, worksheet);

                startingRowIndex++;
                this.generateExcelWorksheetDetailColumns(ref startingRowIndex, exportExcelFormat, worksheet);

                this.fillExcelWorksheetDetail(ref startingRowIndex, dView, exportExcelFormat, worksheet);
                
                startingRowIndex++;
                this.filleExcelWorksheeFooter(ref startingRowIndex, dView, exportExcelFormat, worksheet);

                //Save your file
                FileInfo fi1 = new FileInfo(@"tempfiles/" + webReport.ReportName + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-"+ GuidString + ".xlsx");
                excelPackage.SaveAs(fi1);

                fileName = fi1.Name;
                fullName = fi1.FullName;
                filePath = fi1.DirectoryName;
            }

            byte[] fileBytes = System.IO.File.ReadAllBytes(fullName);

            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpPost("Pdf/{Oid_Report}")]
        public async Task<IActionResult> Pdf(string Oid_Report)
        {
            dynamic pdfFile ;
            var webReport = this.GetWebReport(Oid_Report);
            if (webReport == null) return NotFound("Report'" + Oid_Report + "' not found");

            webReport.ShowPreparedReport = false;
            webReport.Report.Prepare();

            using (MemoryStream ms = new MemoryStream())
            {
                PDFSimpleExport pdfExport = new PDFSimpleExport();
                pdfExport.Export(webReport.Report, ms);
                ms.Flush();

                pdfFile = File(ms.ToArray(), "application/pdf", Path.GetFileNameWithoutExtension(webReport.ReportName) + ".pdf");
            }

            return pdfFile;
        }

        ExportExcel getExportExcelFormat(string Oid_Report)
        {
            var rpt = this.ctx._tReport.SingleOrDefault(d => d.Oid.ToString() == Oid_Report);

            ExportExcel exportExcelFormat = new ExportExcel();

            StreamReader r = new StreamReader(@$"App_Data/Report/{rpt.ReportPath}.json");
            string jsonString = r.ReadToEnd();
            ExportExcel _tempObject = JsonConvert.DeserializeObject<ExportExcel>(jsonString);
            
            r.Close();

            return _tempObject;
        }
        void filleExcelWorksheetHeader(ref int startingRowIndex, DataView dataView, ExportExcel exportExcelFormat, ExcelWorksheet worksheet)
        {

            DataTable dataSource = dataView.ToTable();

            if (exportExcelFormat.headerData == null) return;
            if (exportExcelFormat.headerData.Count == 0) return;

            for (int headerDataIndex = 0; headerDataIndex < exportExcelFormat.headerData.Count; headerDataIndex++) {

                ExportExcelHeaderData headerData = exportExcelFormat.headerData[headerDataIndex];
                ExcelRangeBase cellLabel = worksheet.Cells[startingRowIndex, 1];
                ExcelRangeBase cellValue = worksheet.Cells[startingRowIndex, 2];

                cellLabel.Value = headerData.caption.Length > 0 ? headerData.caption : headerData.name;
                cellLabel.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                cellLabel.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                cellLabel.Style.Font.Bold = true;

                cellValue.Value = dataSource.Compute(@"MAX("+ headerData.name + ")", "");
                cellValue.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                cellValue.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                startingRowIndex++;
            }
        }
        void filleExcelWorksheeFooter(ref int startingRowIndex, DataView dataView, ExportExcel exportExcelFormat, ExcelWorksheet worksheet)
        {

            DataTable dataSource = dataView.ToTable();

            if (exportExcelFormat.footerData == null) return;
            if (exportExcelFormat.footerData.Count == 0) return;

            for (int footerDataIndex = 0; footerDataIndex < exportExcelFormat.footerData.Count; footerDataIndex++)
            {
                ExportExcelFooterData footerData = exportExcelFormat.footerData[footerDataIndex];
                ExcelRangeBase cellLabel = worksheet.Cells[startingRowIndex, 1];
                ExcelRangeBase cellValue = worksheet.Cells[startingRowIndex, 2];

                cellLabel.Value = footerData.caption.Length > 0 ? footerData.caption : footerData.name;
                cellLabel.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                cellLabel.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                cellLabel.Style.Font.Bold = true;

                cellValue.Value = dataSource.Compute(footerData.expression, "");
                cellValue.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                cellValue.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                startingRowIndex++;
            }
        }

        void generateExcelWorksheetDetailColumns(ref int startingRowIndex, ExportExcel exportExcelFormat, ExcelWorksheet worksheet)
        {
            var columnNames = from columnName in exportExcelFormat.columns.AsEnumerable()
                                                    .Where(fruit => (fruit.visible != false))
                              select columnName;

            int i = 0;
            foreach (ExportExcelColumn col in columnNames) {

                int rowIndex = startingRowIndex;
                int columnIndex = i + 1;
                ExcelRangeBase cell = worksheet.Cells[rowIndex, columnIndex];

                if (col.caption == null) col.caption = "";

                cell.Value = col.caption.Length > 0 ? col.caption : col.name;

                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                cell.Style.Font.Bold = true;
                i++;
            }

            startingRowIndex++;
        }

      
        void fillExcelWorksheetDetail(ref int startingRowIndex, DataView dataView, ExportExcel exportExcelFormat, ExcelWorksheet worksheet)
        {
            DataTable dataSource = dataView.ToTable();

            for (int rowIndex = 0; rowIndex < dataSource.Rows.Count; rowIndex++)
            {
                DataRow row = dataSource.Rows[rowIndex];

                var columnNames = from columnName in exportExcelFormat.columns.AsEnumerable()
                                        .Where(fruit => (fruit.visible != false))
                                  select columnName;
                int columnIndex = 0;
                foreach (ExportExcelColumn col in columnNames)
                {
                    ExcelRangeBase cell = worksheet.Cells[startingRowIndex, columnIndex + 1];

                    cell.Value = row[col.name];
                    cell.Style.Numberformat.Format = col.format;
                    columnIndex++;
                }
                startingRowIndex++;
            }
        }

        private WebReport GetWebReport(string Oid_Report) {

            var webReport = this.GetInitWebReport(Oid_Report);
            var SQL = this.GetWebReportSQLString(webReport);
            var MainTable = webReport.Report.Dictionary.Connections[0].Tables[0];
            webReport.Report.Dictionary.Connections[0].CommandTimeout = 5000;

            MainTable.SelectCommand = SQL;

            FillCompanyCustomReportObjectValues(webReport, Oid_Report);

            return webReport;
        }

        private void FillCompanyCustomReportObjectValues(WebReport webReport, string Oid_Report)
        {
            var filterFormValue = this.Request.Form["filterValue"];

            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                byte[] data = Convert.FromBase64String(filterFormValue);
                filterFormValue = Encoding.UTF8.GetString(data);
            }

            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                var filterFormValues = JArray.Parse(filterFormValue);

                var filters = from f in filterFormValues
                              where !f["dataField"].Value<string>().Contains("_QPARAM") && !f["dataField"].Value<string>().Contains("_CustomCaption") && f["dataField"].Value<string>() == "ID_Company"
                              select f;

                if (filters.Count() > 0)
                {
                    foreach (var f in filters)
                    {
                        var dataField = f["dataField"].Value<string>();
                        var ID_Company = f["value"];

                        var sql = $"SELECT * FROM dbo.tCompanyCustomReportObjectValue WHERE ID_Company = {ID_Company} AND Oid_Report = '{Oid_Report}'";

                        var records = new DBCollection().GetDynamicCollections(sql);

                        if (records != null)
                        {
                            foreach (var record in records)
                            {
                                string objectName = record.ObjectName;
                                string propertyName = record.PropertyName;
                                string value = record.Value;

                                if(propertyName == "Text") this.SetReportTextboxValue(webReport, objectName, value);
                            }
                        }
                    }
                }
            }

        }


        private void FillCustomReportTitle(WebReport webReport, string Oid_Report)
        {
            var filterFormValue = this.Request.Form["filterValue"];

            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                byte[] data = Convert.FromBase64String(filterFormValue);
                filterFormValue = Encoding.UTF8.GetString(data);
            }

            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                var filterFormValues = JArray.Parse(filterFormValue);

                var filters = from f in filterFormValues
                              where !f["dataField"].Value<string>().Contains("_QPARAM") && !f["dataField"].Value<string>().Contains("_CustomCaption") && f["dataField"].Value<string>() == "ID_Company"
                              select f;

                if (filters.Count() > 0)
                {
                    foreach (var f in filters)
                    {
                        var dataField = f["dataField"].Value<string>();
                        var value = f["value"];

                        var sql = $"SELECT * FROM dbo.tCompanyCustomReportTitle WHERE ID_Company = { value } AND Oid_Report = '{ Oid_Report }'";

                        var records = new DBCollection().GetDynamicCollections(sql);

                        if(records != null)
                        {
                            string name = records[0].Name;
                            this.SetReportTextboxValue(webReport, "txtReportTitle", name);
                        }
                    }
                }
            }

        }

        private void SetReportTextboxValue(WebReport webReport, string textboxName, string value)
        {
            var reportObjects = webReport.Report.AllObjects.ToArray();

            foreach (var rptObj in reportObjects)
            {
                if (rptObj.GetType() == typeof(FastReport.TextObject))
                {
                    var txtObj = rptObj as TextObject;

                    if (txtObj.Name == textboxName)
                    {
                        txtObj.Text = value;
                        break;
                    }
                }
            }
        }

        private WebReport GetInitWebReport(string Oid_Report)
        {
            var rpt = this.ctx._tReport.SingleOrDefault(d => d.Oid.ToString() == Oid_Report);

            if (rpt == null) return null;

            var webReport = new WebReport();

            var filterFormValue = this.Request.Form["filterValue"];
            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                byte[] data = Convert.FromBase64String(filterFormValue);
                filterFormValue = Encoding.UTF8.GetString(data);
            }
            webReport.Report.Load(@$"App_Data/Report/{rpt.ReportPath}.frx");
            webReport.Mode = WebReportMode.Preview;
            webReport.DesignScriptCode = false;
            webReport.Report.Dictionary.Connections[0].ConnectionString = this.ctx.Database.GetDbConnection().ConnectionString;

            return webReport;
        }
        private string GetWebReportSQLString(WebReport webReport)
        {
            var filterFormValue = this.Request.Form["filterValue"];
            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                byte[] data = Convert.FromBase64String(filterFormValue);
                filterFormValue = Encoding.UTF8.GetString(data);
            }
            var MainTable = webReport.Report.Dictionary.Connections[0].Tables[0];

            var SQL = MainTable.SelectCommand;
            string where = "";
            var whereClauseList = new List<string>();
            var where2ClauseList = new List<string>();
            var where3ClauseList = new List<string>();
            var where4ClauseList = new List<string>();
            Dictionary<string, string> dictFilter  = new Dictionary<string, string>();
            var qClauseList = new Dictionary<string, string>();

            if (SQL.Length == 0) SQL = $"SELECT * FROM {MainTable.TableName}";

            if (String.IsNullOrEmpty(filterFormValue) != true)
            {
                var filterFormValues = JArray.Parse(filterFormValue);

                var filters = from f in filterFormValues
                              where !f["dataField"].Value<string>().Contains("_QPARAM") && !f["dataField"].Value<string>().Contains("_CustomCaption")
                              select f;

                var qParamfilters = from f in filterFormValues
                                    where f["dataField"].Value<string>().Contains("_QPARAM")
                                    select f;

                var customCaptionfilters = from f in filterFormValues
                                           where f["dataField"].Value<string>().Contains("_CustomCaption")
                                           select f;

                foreach (var customCaptionfilter in customCaptionfilters)
                {
                    var dataField = customCaptionfilter["dataField"].Value<string>();
                    var filterCriteriaType = (FilterCriteriaType)Int32.Parse(customCaptionfilter["filterCriteriaType"].Value<string>());
                    var PropertyType = (PropertyTypeEnum)customCaptionfilter["propertyType"].Value<Int32>();
                    var _value = customCaptionfilter["value"];
                    var Values = new List<string>();

                    if (_value.Type == JTokenType.Array)
                    {
                        Values = _value.ToArray<object>().Select(s => s.ToString()).ToList();
                    }
                    else
                    {
                        Values.Add(_value.Value<object>().ToString());
                    }


                    var rptObjs = webReport.Report.AllObjects.ToArray();
                    foreach (var rptObj in rptObjs)
                    {

                        if (rptObj.GetType() == typeof(FastReport.TextObject))
                        {
                            var txtObj = rptObj as TextObject;
                            if (txtObj.Name == "txt" + dataField)
                            {
                                txtObj.Text = (string)Values[0];
                            }
                        }
                    }

                }

                foreach (var qParam in qParamfilters)
                {
                    var dataField = qParam["dataField"].Value<string>();
                    var filterCriteriaType = (FilterCriteriaType)Int32.Parse(qParam["filterCriteriaType"].Value<string>());
                    var PropertyType = (PropertyTypeEnum)qParam["propertyType"].Value<Int32>();
                    var _value = qParam["value"];
                    var Values = new List<string>();

                    dataField = dataField.Replace("_QPARAM", "");

                    if (_value.Type == JTokenType.Array)
                    {
                        Values = _value.ToArray<object>().Select(s => s.ToString()).ToList();
                    }
                    else
                    {
                        Values.Add(_value.Value<object>().ToString());
                    }

                    if (Values.Count == 0) continue;

                    switch (PropertyType)
                    {
                        case PropertyTypeEnum.Uniqueidentifier:
                        case PropertyTypeEnum.String:
                            break;
                        case PropertyTypeEnum.Date:
                        case PropertyTypeEnum.DateTime:
                            if (Values.Count == 1)
                            {
                                Values[0] = DateTime.Parse(Values[0]).ToString("yyyy-MM-dd");
                            }
                            break;
                    }

                    string whereClause = "";

                    switch (filterCriteriaType)
                    {
                        case FilterCriteriaType.Equal:
                            if (PropertyType == PropertyTypeEnum.String)
                            {
                                whereClause = $"{dataField} = '{Values[0].Replace("'", "''")}'";
                            }
                            else if (PropertyType == PropertyTypeEnum.Date)
                            {

                                whereClause = $"CONVERT(DATE,{dataField}) = '{Values[0].Replace("'", "''")}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} = {Values[0].Replace("'", "''")}";
                            }
                            break;
                        case FilterCriteriaType.NotEqual:
                            if (PropertyType == PropertyTypeEnum.String)
                            {
                                whereClause = $"{dataField} <> '{Values[0].Replace("'", "''")}'";
                            }
                            else if (PropertyType == PropertyTypeEnum.Date)
                            {

                                whereClause = $"CONVERT(DATE,{dataField}) <> '{Values[0].Replace("'", "''")}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} <> {Values[0].Replace("'", "''")}";
                            }
                            break;
                        case FilterCriteriaType.Contains:
                            whereClause = $"{dataField} IN ({string.Join(',', Values)})";
                            break;
                        case FilterCriteriaType.NotContains:
                            whereClause = $"{dataField} NOT IN ({string.Join(',', Values)})";
                            break;
                        case FilterCriteriaType.Like:
                            whereClause = $"{dataField} LIKE '%{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.NotLike:
                            whereClause = $"{dataField} NOT LIKE '%{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.StartWith:
                            whereClause = $"{dataField} NOT LIKE '{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.EndWith:
                            whereClause = $"{dataField} NOT LIKE '%{Values[0].Replace("'", "''")}'";
                            break;
                        case FilterCriteriaType.GreaterThan:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                whereClause = $"CONVERT(DATE,{dataField}) > '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} > {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.LessThan:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                whereClause = $"CONVERT(DATE,{dataField}) < '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} < {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.GreaterThanEqual:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                Values[0] = DateTime.Parse(Values[0]).AddDays(-1).ToString("yyyy-MM-dd");
                                whereClause = $"CONVERT(DATE,{dataField}) > '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} >= {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.LessThanEqual:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                Values[0] = DateTime.Parse(Values[0]).AddDays(1).ToString("yyyy-MM-dd");
                                whereClause = $"CONVERT(DATE,{dataField}) < '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} <= {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.Between:

                            bool isStringDate = false;
                            if (PropertyType == PropertyTypeEnum.String)
                            {

                                if (DateTime.TryParse(Values[0], out DateTime theDate))
                                {
                                    isStringDate = true;
                                }
                            }

                            if (isStringDate == true) PropertyType = PropertyTypeEnum.Date;

                            if (PropertyType == PropertyTypeEnum.Date)
                            {

                                var StartDate = DateTime.Parse(Values[0]).ToString("yyyy-MM-dd");
                                var EndDate = DateTime.Parse(Values[1]).ToString("yyyy-MM-dd");

                                whereClause = $"CONVERT(DATE,{dataField})  BETWEEN '{StartDate}' AND '{EndDate}'";
                            }
                            break;
                    }

                    qClauseList.Add("@" + dataField + "_QPARAM", whereClause);
                }

                foreach (var f in filters)
                {
                    var dataField = f["dataField"].Value<string>();
             
                    var filterCriteriaType = (FilterCriteriaType)Int32.Parse(f["filterCriteriaType"].Value<string>());
                    var PropertyType = (PropertyTypeEnum)f["propertyType"].Value<Int32>();
                    var _value = f["value"];
                    var Values = new List<string>();
                    var alias = "";

                    if (f["alias"] != null) {
                        alias = f["alias"].Value<string>();
                    }
                    if (alias.Length == 0) {
                        alias = "schema1";
                    }

                    if (_value.Type == JTokenType.Array)
                    {
                        Values = _value.ToArray<object>().Select(s => s.ToString()).ToList();
                    }
                    else
                    {
                        Values.Add(_value.Value<object>().ToString());
                    }
                    if (Values.Count == 0) continue;
                    switch (PropertyType)
                    {
                        case PropertyTypeEnum.Uniqueidentifier:
                        case PropertyTypeEnum.String:
                            break;
                        case PropertyTypeEnum.Date:
                        case PropertyTypeEnum.DateTime:
                            if (Values.Count == 1)
                            {
                                Values[0] = DateTime.Parse(Values[0]).ToString("yyyy-MM-dd");
                            }
                            break;
                    }
                    string whereClause = null;

                    switch (filterCriteriaType)
                    {
                        case FilterCriteriaType.Equal:
                            if (PropertyType == PropertyTypeEnum.String)
                            {
                                whereClause = $"{dataField} = '{Values[0].Replace("'", "''")}'";
                            }
                            else if (PropertyType == PropertyTypeEnum.Date)
                            {

                                whereClause = $"CONVERT(DATE,{alias}.{dataField}) = '{Values[0].Replace("'", "''")}'";
                            }
                            else
                            {
                                whereClause = $"{alias}.{dataField} = {Values[0].Replace("'", "''")}";
                            }
                            break;
                        case FilterCriteriaType.NotEqual:
                            if (PropertyType == PropertyTypeEnum.String)
                            {
                                whereClause = $"{alias}.{dataField} <> '{Values[0].Replace("'", "''")}'";
                            }
                            else if (PropertyType == PropertyTypeEnum.Date)
                            {

                                whereClause = $"CONVERT(DATE,{alias}.{dataField}) <> '{Values[0].Replace("'", "''")}'";
                            }
                            else
                            {
                                whereClause = $"{alias}.{dataField} <> {Values[0].Replace("'", "''")}";
                            }
                            break;
                        case FilterCriteriaType.Contains:
                            whereClause = $"{alias}.{dataField} IN ({string.Join(',', Values)})";
                            break;
                        case FilterCriteriaType.NotContains:
                            whereClause = $"{alias}.{dataField} NOT IN ({string.Join(',', Values)})";
                            break;
                        case FilterCriteriaType.Like:
                            whereClause = $"{alias}.{dataField} LIKE '%{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.NotLike:
                            whereClause = $"{alias}.{dataField} NOT LIKE '%{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.StartWith:
                            whereClause = $"{alias}.{dataField} NOT LIKE '{Values[0].Replace("'", "''")}%'";
                            break;
                        case FilterCriteriaType.EndWith:
                            whereClause = $"{alias}.{dataField} NOT LIKE '%{Values[0].Replace("'", "''")}'";
                            break;
                        case FilterCriteriaType.GreaterThan:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                whereClause = $"CONVERT(DATE,{alias}.{dataField}) > '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{alias}.{dataField} > {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.LessThan:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                whereClause = $"CONVERT(DATE,{alias}.{dataField}) < '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{dataField} < {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.GreaterThanEqual:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                Values[0] = DateTime.Parse(Values[0]).AddDays(-1).ToString("yyyy-MM-dd");
                                whereClause = $"CONVERT(DATE,schema1.{dataField}) > '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{alias}.{dataField} >= {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.LessThanEqual:
                            if (PropertyType == PropertyTypeEnum.Date)
                            {
                                Values[0] = DateTime.Parse(Values[0]).AddDays(1).ToString("yyyy-MM-dd");
                                whereClause = $"CONVERT(DATE,{alias}.{dataField}) < '{Values[0]}'";
                            }
                            else
                            {
                                whereClause = $"{alias}.{dataField} <= {Values[0]}";
                            }
                            break;
                        case FilterCriteriaType.Between:

                            bool isStringDate = false;
                            if (PropertyType == PropertyTypeEnum.String)
                            {

                                if (DateTime.TryParse(Values[0], out DateTime theDate))
                                {
                                    isStringDate = true;
                                }
                            }

                            if (isStringDate == true) PropertyType = PropertyTypeEnum.Date;

                            if (PropertyType == PropertyTypeEnum.Date)
                            {

                                var StartDate = DateTime.Parse(Values[0]).ToString("yyyy-MM-dd");
                                var EndDate = DateTime.Parse(Values[1]).ToString("yyyy-MM-dd");

                                whereClause = $"CONVERT(DATE,{alias}.{dataField})  BETWEEN '{StartDate}' AND '{EndDate}'";
                            }
                            break;
                    }

                    string dictFilterKey = "";
                    switch (alias) {

                        case "schema1":

                            dictFilterKey = $"/*where1-{dataField}*/";
                            whereClauseList.Add($"({whereClause})");
                            break;

                        case "schema2":

                            dictFilterKey = $"/*where2-{dataField}*/";
                            where2ClauseList.Add($"({whereClause})");
                            break;

                        case "schema3":

                            dictFilterKey = $"/*where3-{dataField}*/";
                            where3ClauseList.Add($"({whereClause})");
                            break;

                        case "schema4":

                            dictFilterKey = $"/*where4-{dataField}*/";
                            where4ClauseList.Add($"({whereClause})");
                            break;
                    }

                    if (!dictFilter.ContainsKey(dictFilterKey)) {

                        whereClause = whereClause.Replace("schema1.", "where1.");
                        whereClause = whereClause.Replace("schema2.", "where2.");
                        whereClause = whereClause.Replace("schema3.", "where3.");
                        whereClause = whereClause.Replace("schema4.", "where4.");

                        dictFilter.Add(dictFilterKey, whereClause);
                    }
                }

                if (qClauseList.Count > 0)
                {

                    foreach (var qParam in qClauseList)
                    {
                        whereClauseList.Add(qParam.Value);
                    }
                }
            }

            where += String.Join(" AND ", whereClauseList);
            string where2 = String.Join(" AND ", where2ClauseList);
            string where3 = String.Join(" AND ", where3ClauseList);
            string where4 = String.Join(" AND ", where4ClauseList);

            if (SQL.Contains("/*@WHERE*/") || SQL.Contains("/*@WHERE1*/") || SQL.Contains("/*@WHERE2*/") || SQL.Contains("/*@WHERE3*/") || SQL.Contains("/*@WHERE4*/"))
            {
                string where1 = where.Replace("schema1.", "where1.");
                where2 = where2.Replace("schema2.", "where2.");
                where = where.Replace("schema1.", "");

                SQL = SQL.Replace("/*@WHERE*/", " WHERE " + where);
                SQL = SQL.Replace("/*@WHERE1*/", " WHERE " + where1);
                SQL = SQL.Replace("/*@WHERE2*/", " WHERE " + where2);
                SQL = SQL.Replace("/*@WHERE3*/", " WHERE " + where3);
                SQL = SQL.Replace("/*@WHERE4*/", " WHERE " + where4);
            }
            else
            {
                where = where.Replace("schema1.", "");
                SQL += !SQL.Contains("/*@WHERE*/") ? "/*@WHERE*/" : "";
                SQL = SQL.Replace("/*@WHERE*/", " WHERE " + where);
            }

            foreach (KeyValuePair<string, string> record in dictFilter)
            {
                SQL = SQL.Replace(record.Key, record.Value);
            }

            SQL = SQL.Replace("/*AND*/", " AND ");
            SQL = SQL.Replace("/*OR*/", " OR ");

            return SQL;
        }
    }
    public class PayLoad
    {
        public string filterValue { get; set; }
    }

    public class ExportExcel
    {
        public string ReportFileName { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string? sort { get; set; }
        public string? SQLSelectStatement { get; set; }

        public List<ExportExcelHeaderData> headerData = new List<ExportExcelHeaderData>();
        public List<ExportExcelColumn> columns = new List<ExportExcelColumn>();
        public List<ExportExcelFooterData> footerData = new List<ExportExcelFooterData>();
        public List<ExportExcelGroupData> groups = new List<ExportExcelGroupData>();
    }

    public class ExportExcelColumn
    {
        public string name { get; set; }
        public string? caption { get; set; }
        public string? dataType { get; set; }
        public string? expression { get; set; }
        public string? format { get; set; }
        public bool visible { get; set; } = true;
    }

    public class ExportExcelHeaderData
    {
        public string name { get; set; }
        public string? caption { get; set; }
        public string? dataType { get; set; }
        public string? format { get; set; }
    }
    public class ExportExcelFooterData
    {
        public string name { get; set; }
        public string? caption { get; set; }
        public string? expression { get; set; }
        public string? dataType { get; set; }
        public string? format { get; set; }
    }
    public class ExportExcelGroupData
    {
        public string name { get; set; }
    }

    public enum FilterCriteriaType 
    {
        Exclude = 0,
        Equal = 1,
        NotEqual = 2,
        Contains = 3,
        NotContains = 4,
        Like = 5,
        NotLike = 6,
        StartWith = 7,
        EndWith = 8,
        GreaterThan = 9,
        LessThan = 10,
        GreaterThanEqual = 11,
        LessThanEqual = 12,
        Between = 13
    }

   public enum PropertyTypeEnum
    {
        String = 1,
        Int = 2,
        Decimal = 3,
        Bit = 4,
        DateTime = 5,
        Date = 6,
        Time = 7,
        Uniqueidentifier = 8,
        Object = 9,
        List = 10,
        Color = 11,
        Image = 12
    }

}