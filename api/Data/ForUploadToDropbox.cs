﻿using System;

namespace ngJSApp.Data
{
    public class ForUploadToDropbox
    {
        public int ID { get; set; }
        public string DatabaseName { get; set; }
        public int? ID_Company { get; set; }
        public string Name_Company { get; set; }
        public string GUID_Company { get; set; }
        public string TableName { get; set; }
        public string Model { get; set; }
        public int? ID_CurrentObject { get; set; }
        public string ColumnName { get; set; }
        public string OriginalImageFileName { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFilePath { get; set; }
        public bool? IsUpload { get; set; }
        public string FileID { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUploaded { get; set; }
        public string Comment { get; set; }
        public ForUploadToDropbox ShallowCopy()
        {
            return (ForUploadToDropbox)this.MemberwiseClone();
        }
    }
}
