﻿namespace ngJSApp.Data
{
    public class FailedSavingLogInfo
    {
        public string DatabaseName { get; set; }
        public string TableName { get; set; }
        public string ErrorMessage { get; set; }
        public int ID_CurrentObject { get; set; }
        
    }
}
