﻿using System.Data.SQLite;
using System;
using ngJSApp.Class;

namespace GlobalFx
{
    public class SQLLiteQueryResultArg {

        public bool success = true;
        public int rowsAffected = 0;
        public string message = "";
        public System.Data.DataTable datatable;
        public object scalarValue;
    }
    public class SQLLiteQuery
    {
        private string _databaseName = "";
        public SQLLiteQuery(string databaseName)
        {
            this._databaseName = databaseName;
        }

        public SQLiteConnection CreateConnection()
        {
            dynamic appSettings = OtherFx.getAppSettings();
            string sqliteConnectionStringPath = appSettings["SQLiteConnectionStringPath"];

            SQLiteConnection sqlite_conn;

            sqlite_conn = new SQLiteConnection($"Data Source={sqliteConnectionStringPath}\\{this._databaseName}.db;Version=3;New=True;Compress=True;");

            return sqlite_conn;
        }

        public SQLLiteQueryResultArg ExecuteNonQuery(string sql)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            string msg = "";
            using (SQLiteConnection con = this.CreateConnection())
            {
                con.Open();

                try
                {
                    SQLiteCommand sqlite_cmd = con.CreateCommand();

                    sqlite_cmd.CommandText = sql;
                    arg.rowsAffected = sqlite_cmd.ExecuteNonQuery();
                }
                catch (Exception ex) {

                    arg.message = ex.Message;
                    arg.success = false;
                }

                con.Close();
                con.Dispose();
            }

            return arg;
        }

        public SQLLiteQueryResultArg GetDataTable(string sql)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();
            System.Data.DataTable dataTable = new System.Data.DataTable();

            using (SQLiteConnection con = CreateConnection())
            {

                con.Open();

                SQLiteCommand cmd;
                SQLiteDataAdapter da;

                try
                {
                    cmd = con.CreateCommand();
                    cmd.CommandText = sql;
                    da = new SQLiteDataAdapter(cmd);

                    da.Fill(dataTable);
                    da.Dispose();
                }
                catch (Exception ex)
                {

                    arg.message = ex.Message;
                    arg.success = false;
                }

                con.Close();
                con.Dispose();
            }

            arg.datatable = dataTable;


            return arg;
        }

        public SQLLiteQueryResultArg GetScalarValue(string sql)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();
            System.Data.DataTable dataTable = new System.Data.DataTable();

            using (SQLiteConnection con = CreateConnection())
            {

                con.Open();

                SQLiteCommand cmd;
                SQLiteDataAdapter da;

                try
                {
                    SQLiteCommand sqlite_cmd = con.CreateCommand();

                    sqlite_cmd.CommandText = sql;

                    arg.rowsAffected = sqlite_cmd.ExecuteNonQuery();
                    arg.scalarValue = sqlite_cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {

                    arg.message = ex.Message;
                    arg.success = false;
                }

                con.Close();
                con.Dispose();
            }

            arg.datatable = dataTable;


            return arg;
        }

        public bool IsTableExist(string tableName) {

            bool result = false;

            SQLLiteQueryResultArg arg = this.GetScalarValue($"SELECT COUNT(*) FROM sqlite_master WHERE type = 'table' AND name = '{tableName}';");

            result = ValueConverter.ToInt(arg.scalarValue) > 0; 

            return result;
        }
        
      
    }
}
