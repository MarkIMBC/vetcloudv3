﻿using FastReport;
using System;
using System.Data.SqlClient;
using System.Text;

namespace ngJSApp.Utility
{
    public static class SysManDbService
    {
        public static void InsertForUploadImageAndThumbnail(Data.ForUploadToDropbox forUploadToDropbox)
        {
            Data.ForUploadToDropbox forUploadToDropboxImage = forUploadToDropbox.ShallowCopy();
            Data.ForUploadToDropbox forUploadToDropboxThumbnail = forUploadToDropbox.ShallowCopy();

            forUploadToDropboxImage.ImageFilePath = @"Image/" + forUploadToDropboxImage.OriginalImageFileName;
            Insert(forUploadToDropboxImage);

            forUploadToDropboxThumbnail.ImageFilePath = @"Thumbnail/" + forUploadToDropboxThumbnail.OriginalImageFileName;
            Insert(forUploadToDropboxThumbnail);
        }
        public static void Insert(Data.ForUploadToDropbox forUploadToDropbox)
        {
            string connectionString = Startup.SysManDBConnectionString;

            StringBuilder varname12 = new StringBuilder();
            varname12.Append("INSERT INTO [dbo].[tForUploadToDropbox] \n");
            varname12.Append("           ( \n");
            varname12.Append("            [DatabaseName] \n");
            varname12.Append("           ,[ID_Company] \n");
            varname12.Append("           ,[Name_Company] \n");
            varname12.Append("           ,[GUID_Company] \n");
            varname12.Append("           ,[TableName] \n");
            varname12.Append("           ,[Model] \n");
            varname12.Append("           ,[ID_CurrentObject] \n");
            varname12.Append("           ,[ColumnName] \n");
            varname12.Append("           ,[OriginalImageFileName] \n");
            varname12.Append("           ,[ImageFileName] \n");
            varname12.Append("           ,[ImageFilePath] \n");
            varname12.Append("           ,[DateCreated] \n");
            varname12.Append("           ) \n");
            varname12.Append("     VALUES \n");
            varname12.Append($"           ( \n");
            varname12.Append($"            '{forUploadToDropbox.DatabaseName}' \n");
            varname12.Append($"           ,{forUploadToDropbox.ID_Company} \n");
            varname12.Append($"           ,'{forUploadToDropbox.Name_Company}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.GUID_Company}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.TableName}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.Model}' \n");
            varname12.Append($"           ,{forUploadToDropbox.ID_CurrentObject} \n");
            varname12.Append($"           ,'{forUploadToDropbox.ColumnName}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.OriginalImageFileName}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.ImageFileName}' \n");
            varname12.Append($"           ,'{forUploadToDropbox.ImageFilePath}' \n");
            varname12.Append($"           ,GETDATE() \n");
            varname12.Append($"           ) \n");

            var sql = varname12.ToString();

            using (var sqlConn = JSLibrary.JSCommon.GetSQLConnection(connectionString))
            {
                sqlConn.Open();

                SqlCommand sqlCommand = new SqlCommand(sql, sqlConn);

                sqlCommand.ExecuteNonQuery();
            }
        }
    }
}
