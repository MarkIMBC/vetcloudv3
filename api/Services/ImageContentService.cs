﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using ngJSApp.Data;
using System.IO;

namespace ngJSApp.Services
{
    public interface IImageContentService
    {
        Data.FileStreamResultData GetImage(string fileID);
        Data.FileStreamResultData GetThumbnail(string fileID);
        bool IsImageExist(string fileName);
        bool IsThumbnailExist(string fileName);
    }
    public class ImageContentService : IImageContentService
    {
        private readonly IWebHostEnvironment _env;
        public ImageContentService(IWebHostEnvironment env)
        {
            _env = env;
        }
        public bool IsImageExist(string fileName)
        {
            string path = Path.Combine(_env.WebRootPath, "Content/Image", fileName);
            bool imageExists = System.IO.File.Exists(path);

            return imageExists;
        }
        public bool IsThumbnailExist(string fileName)
        {
            string path = Path.Combine(_env.WebRootPath, "Content/Thumbnail", fileName);
            bool imageExists = System.IO.File.Exists(path);

            return imageExists;
        }
        public FileStreamResultData GetImage(string fileName)
        {
            Data.FileStreamResultData fileStreamResultData = new Data.FileStreamResultData();

            string path = Path.Combine(_env.WebRootPath, "Content/Image", fileName);
            var stream = new FileStream(path, System.IO.FileMode.Open, FileAccess.Read);
            var mimeType = GetMimeType(path);

            fileStreamResultData.stream = stream;
            fileStreamResultData.mimeType = mimeType;

            return fileStreamResultData;
        }
        public FileStreamResultData GetThumbnail(string fileName)
        {
            Data.FileStreamResultData fileStreamResultData = new Data.FileStreamResultData();

            string path = Path.Combine(_env.WebRootPath, "Content/Thumbnail", fileName);
            var stream = new FileStream(path, System.IO.FileMode.Open, FileAccess.Read);
            var mimeType = GetMimeType(path);

            fileStreamResultData.stream = stream;
            fileStreamResultData.mimeType = mimeType;

            return fileStreamResultData;
        }
        private static string GetMimeType(string filePath)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(filePath, out string mimeType))
            {
                mimeType = "application/octet-stream"; // Default MIME type if unknown
            }
            return mimeType;
        }
    }
}
