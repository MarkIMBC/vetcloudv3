﻿using ngJSApp.Services.MirgrationServices;
using System.Threading.Tasks;

namespace ngJSApp.Services
{
    public interface IImageURLService
    {
        Task<string> GetImage(string fileID);
        Task<string> GetThumbnail(string fileID);
    }

    public class ImageURLService : IImageURLService
    {
        IMigrationService _googleDriveService;
        IImageContentService _imageContentService;
        IUrlService _urlService;
        public ImageURLService(IMigrationService googleDriveService, IImageContentService imageContentService, IUrlService urlService)
        {
            this._googleDriveService = googleDriveService;
            this._imageContentService = imageContentService;
            this._urlService = urlService;
        }

        async Task<string> IImageURLService.GetImage(string fileID)
        {
            string baseUrl = "";

            bool isLocalFileExist = this._imageContentService.IsThumbnailExist(fileID);

            if (isLocalFileExist)
            {
                baseUrl = this._urlService.GetBaseUrl() + $"/Content/Image/{fileID}";
            }
            else
            {
                baseUrl = this._urlService.GetBaseUrl() + $"/api/Image/GetImage/{fileID}";
            }

            return baseUrl;
        }

        async Task<string> IImageURLService.GetThumbnail(string fileID)
        {
            string baseUrl = "";
            bool isLocalFileExist = this._imageContentService.IsThumbnailExist(fileID);

            if (isLocalFileExist)
            {
                baseUrl = this._urlService.GetBaseUrl() + $"/Content/Thumbnail/{fileID}";
            }
            else
            {
                baseUrl =  await this._googleDriveService.GetThumbnailUrlAsync(fileID);
            }

            return baseUrl;
        }
    }
}
