﻿using Castle.Core.Resource;
using Microsoft.AspNetCore.Mvc;
using ngJSApp.Data;
using System.Threading.Tasks;

namespace ngJSApp.Services
{
    public interface IMigrationService
    {
        Task<Data.FileStreamResultData> GetImageAsync(string fileId);
        Task<Data.FileStreamResultData> GetThumbnailAsync(string fileId);
        Task<string> GetThumbnailUrlAsync(string fileId);
    }
    public class MigrationService : IMigrationService
    {
        public virtual Task<Data.FileStreamResultData> GetImageAsync(string fileId)
        {
            throw new System.NotImplementedException();
        }
        public virtual Task<Data.FileStreamResultData> GetThumbnailAsync(string fileId)
        {
            throw new System.NotImplementedException();
        }
        public virtual Task<string> GetThumbnailUrlAsync(string fileId)
        {
            throw new System.NotImplementedException();
        }
    }
}
