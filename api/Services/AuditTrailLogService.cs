﻿using FastReport.Table;
using FastReport;
using Newtonsoft.Json;
using ngJSApp.Data;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ngJSApp.Class;
using System.Net;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace ngJSApp.Services
{
    public interface IAuditTrailLogService
    {
        List<Data.AuditTrailLogInfo> GetList(string tableName, int ID_CurrentObject);
        void BlockDuplicateRecord(dynamic currentObject, string tableName);
        void WriteLogFile(Data.AuditTrailContent auditTrailContent);
        void WriteFailedSavingLogFile(Data.FailedSavingLogInfo failedSavingLogInfo);
    }
    public class AuditTrailLogService : IAuditTrailLogService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ITextFilerWriterService _textFilerWriterService;
        public AuditTrailLogService(ITextFilerWriterService textFilerWriterService, IHttpContextAccessor httpContextAccessor)
        {
            this._textFilerWriterService = textFilerWriterService;
            this._httpContextAccessor = httpContextAccessor;
        }
        private string GetClientIpAddress()
        {
            var context = _httpContextAccessor.HttpContext;
            string clientIpAddress = context.Request.HttpContext.Connection.RemoteIpAddress?.ToString();
            var forwardedFor = context.Request.Headers["X-Forwarded-For"].ToString();
            if (!string.IsNullOrEmpty(forwardedFor))
            {
                clientIpAddress = forwardedFor.Split(',')[0].Trim();
            }
            return clientIpAddress;
        }       
        private string GetClientUserAgent()
        {
            var userAgent = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString();

            return userAgent;
        }
        public void WriteLogFile(Data.AuditTrailContent auditTrailContent)
        {
            dynamic currentObjectValues = new ExpandoObject();
            Guid g = Guid.NewGuid();
            string filename = DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + auditTrailContent.TableName + "-" + auditTrailContent.ID_CurrentObject.ToString() + "-" + g.ToString() + ".json";

            currentObjectValues.Date = DateTime.Now;
            currentObjectValues.ClientAddress = this.GetClientIpAddress();
            //currentObjectValues.UserAgent = this.GetClientUserAgent();
            currentObjectValues.PreviousObject = auditTrailContent.PreviousObject;
            currentObjectValues.CurrentObject = auditTrailContent.CurrentObject;

            var fileContent = JsonConvert.SerializeObject(currentObjectValues);

            this._textFilerWriterService.Create("./Logs/AuditTrail/" + filename, fileContent);
        }
        public void BlockDuplicateRecord(dynamic currentObject, string tableName)
        {
            dynamic currentObjectValues = new ExpandoObject();
            Guid g = Guid.NewGuid();
            string filename = "BlockedDuplicateRecord-"+ currentObject.GUID + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "-" + tableName + "-" + g.ToString() + ".json";

            currentObjectValues.CurrentGUID = currentObject.GUID;
            currentObjectValues.Date = DateTime.Now;
            currentObjectValues.ClientAddress = this.GetClientIpAddress();
            currentObjectValues.CurrentObject = currentObject;

            var fileContent = JsonConvert.SerializeObject(currentObjectValues);

            this._textFilerWriterService.Create("./Logs/BlockedDuplicateRecord/" + filename, fileContent);
        }
        public void WriteFailedSavingLogFile(Data.FailedSavingLogInfo failedSavingLogInfo)
        {
            dynamic currentObjectValues = new ExpandoObject();
            Guid g = Guid.NewGuid();

            string filename = $"FailedSaving-{DateTime.Now.ToString("yyyyMMdd_HHmmss")}-{failedSavingLogInfo.DatabaseName}-Saving-{failedSavingLogInfo.TableName}.json";
            currentObjectValues.ID_CurrentObject = failedSavingLogInfo.ID_CurrentObject;
            currentObjectValues.Date = DateTime.Now;
            currentObjectValues.ErrorMessage = failedSavingLogInfo.ErrorMessage;

            var fileContent = JsonConvert.SerializeObject(currentObjectValues);
            this._textFilerWriterService.Create("./Logs/ReRunAfterSaved/" + filename, fileContent);
        }
        List<AuditTrailLogInfo> IAuditTrailLogService.GetList(string tableName, int ID_CurrentObject)
        {
            string auditTrailFolder = "./Logs/AuditTrail/";
            string searchPattern = $"_{tableName}-{ID_CurrentObject.ToString()}_";
            List<Data.AuditTrailLogInfo> auditTrailInfoList = new List<Data.AuditTrailLogInfo>();

            foreach (string filePath in Directory.GetFiles("./Logs/AuditTrail/", searchPattern))
            {
                auditTrailInfoList.Add(new Data.AuditTrailLogInfo(filePath));
            }

            return auditTrailInfoList;
        }
    }
}
