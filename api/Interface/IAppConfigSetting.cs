﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.IO;

namespace ngJSApp.Interface
{
    public interface IAppConfigSetting
    {
        public dynamic getAppSettings();
    }
    public class AppConfigSetting : IAppConfigSetting
    {
        public AppConfigSetting()
        {
        
        }
        public dynamic getAppSettings() {

            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader($@"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            return _tempObject;
        }
    }
}
