const sql = require('mssql');
var fs = require('fs');
const prettier = require("prettier");
var colors = require('colors');
require('linqjs');

const config = {
    user: 'sa',
    password: 'sql123$%^',
    server: 'DESKTOP-BQIE424',
    database:'db_vetcloudv3_live_dev',
    options: {

        encrypt: false,
    }
};

sql.on('error', err => {
    console.log('On ERR ->' + err);
})

async function Query(qry) {
    return new Promise((resolve, reject) => {
        sql.connect(config).then(pool => {
            return pool.request().query(qry)
        }).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
}

(async() => {
    var rsModel = await Query("SELECT * FROM _vModel ORDER BY DateCreated");
    var rsDetailView = await Query("SELECT * FROM _vDetailView ORDER BY Model, DateCreated, Oid");
    var rsListView = await Query("SELECT * FROM _vListView WHERE ISNULL(IsActive, 0) = 1 ORDER BY Model, DateCreated, Oid");
    var rsReportView = await Query("SELECT Oid, REPLACE(Name, '-', '') Name FROM dbo._tReport ORDER BY Name ASC");
    var rsModelProperty = await Query(`SELECT Model, Name, ID_PropertyType, PropertyModel FROM _vModel_Property WHERE isActive = 1 ORDER BY Model, DateCreated, Oid`);

    var models = rsModel.recordset;
    var strModels = "";
    models.forEach(el => {
        var ModelName = el.Name.toUpperCase();
        var Oid = el.Oid;
        strModels += `static readonly ${ModelName}:string = '${Oid}';\n`;
    });

    var str = `
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    //Jefrey Sambile :)
    `;
    var properties = rsModelProperty.recordset;
    //console.log(JSON.stringify(properties));
    var gModels = properties.groupBy(function(t) { return t.Model }).select(function(t) {
        //console.log(t)
        return {
            key: t.key,
            properties: t
        }
    });

    gModels.forEach((model) => {
        var propertyStr = '';
        model.properties.forEach(p => {
            var property = (() => {
                switch (p.ID_PropertyType) {
                    case 3: //Decimal
                    case 2: //Int
                        return "number";
                    case 4: //Bit
                        return "boolean";
                    case 5: //DateTime
                    case 6: //Date
                        return "Date";
                    case 7: //Time
                        return "string";
                    case 8: //Uniqueidentifier
                    case 9:
                    case 11:
                    case 12:
                    default:
                        return "string";
                }
            })();
            if (p.ID_PropertyType == 10) {
                if (p.PropertyModel) {
                    property = p.PropertyModel + "[]"
                } else {
                    property = "any[]"
                }
            }
            //console.log( `properties--->${p.Name}?:${property};\n`);
            propertyStr += `${p.Name}?:${property};\n`;
        });


        var classStr = `
            export class ${model.key} {
                ${propertyStr}
            }
        `;
        // var str = prettier.format(classStr, { tabWidth: 5, 
        //     semi: true, parser: "typescript", proseWrap: 'always', bracketSpacing : true, printWidth: 1000 });
        // fs.writeFileSync(`src/app/_Models/${model.key}.model.ts`,str);//
        str += classStr;
    });


    str += `
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_MODEL {

        ${strModels}

    }`;

    var detailViews = rsDetailView.recordset;
    var strDetailView = "";
    detailViews.forEach(el => {
        var ModelName = el.Name.toUpperCase();
        var Oid = el.Oid;
        strDetailView += `static readonly ${ModelName}:string = '${Oid}';\n`;
    });
    str += `
    export class APP_DETAILVIEW {

        ${strDetailView}

    }`;

    var listViews = rsListView.recordset;
    var strListView = "";
    listViews.forEach(el => {
        var ModelName = el.Name.toUpperCase();
        var Oid = el.Oid;
        strListView += `static readonly ${ModelName}:string = '${Oid}';\n`;
    });
    str += `
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_LISTVIEW {

        ${strListView}

    }`;

    var reportView = rsReportView.recordset;
    var strReportView = "";
    reportView.forEach(el => {
        var ReportName = el.Name.toUpperCase();
        var Oid = el.Oid;
        strReportView += `static readonly ${ReportName}:string = '${Oid}';\n`;
    });
    str += `
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_REPORTVIEW {

        ${strReportView}
    }`;

    var str = prettier.format(str, {
        tabWidth: 5,
        semi: true,
        parser: "typescript",
        proseWrap: 'always',
        bracketSpacing: true,
        printWidth: 1000
    });

    fs.writeFileSync('D:/Projects/vetcloudv3_master/website/src/bo/APP_MODELS.ts', str);
    //console.log(colors.green("SUCCESS BUILD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"));
    setTimeout((function() {
        return process.exit(-1);
    }), 1000);
})();