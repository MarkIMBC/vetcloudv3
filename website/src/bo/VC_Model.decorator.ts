export const REGISTRY_VC_MODEL_CONTROLS = new Map<string,Map<string,string>>();
export const REGISTRY_VC_MODEL = new Map<string, Function[]>();

function registerVCModel(cls: Function, model: IVC_Model) {
  let map: Function[];
  var viewIds: string[] = [];
  if (typeof model.viewId == "string") {
    viewIds.push(model.viewId as string);
  } else {
    viewIds = model.viewId as string[];
  }
  viewIds.forEach(id => {
    if (REGISTRY_VC_MODEL.has(id)) {
      map = REGISTRY_VC_MODEL.get(id);
    } else {
      map = [];
      REGISTRY_VC_MODEL.set(id, map);
    };

    var _cls = map.find(d => d.name == cls.name);
    if (_cls) return;
    map.push(cls);
    //
    //
    //
  });
};

export function VC_Model(model: IVC_Model): ClassDecorator {

  return function (cls: Function) {
    //if ( !(cls.prototype instanceof VC_BaseView) ) throw error(`${cls.name} not assignable to class VC_Model.`);
    registerVCModel(cls, model)
  }
}

export class IVC_Model {
  viewId?: string | string[];
  modelName?: string | string[];
}

function registerVCControlProperty(propertyName: string, c: IVC_Control, cls: Function): void {
  if ( REGISTRY_VC_MODEL_CONTROLS.has(cls.name) != true ) {
    REGISTRY_VC_MODEL_CONTROLS.set(cls.name, new Map<string,string>());
  };
  var map = REGISTRY_VC_MODEL_CONTROLS.get(cls.name);

  if ( map.has(c.name) != true ) {
    map.set(c.name, propertyName);
  }
};

//
//
//

export function App_Control(c: IVC_Control) {
  return function (target: Object, key: string | symbol) {
    let val = target[key];
    const getter = () => {
      return val;
    };
    const setter = (next) => {
      // console.log('updating flavor...');
      // console.log(key, "this is the value i set", next);
      val = next;
    };
    registerVCControlProperty(key as string, c, target.constructor);
    Object.defineProperty(target, key, {
      get: getter,
      set: setter,
      enumerable: true,
      configurable: true,
    });
  }
}


export class IVC_Control {
  name : string;
  type?: Function;
}