import { VC_Report } from './VC/Report/VC_Report';
import { VC_Appointment } from './VC/Schedule/VC_Appointment';
import { VC_DocListView } from './VC/Transaction/VC_DocListView';
import { VC_DetailView_Detail_ListView } from './VC/System/VC_DetailView_Detail';
import { DEBUG } from 'src/utils/controls/class_helper';
import { VC_ListView_Detail } from './VC/System/VC_ListView_Detail';
import { VC_Patient_View } from './VC/VC_Patient';
import { VC_User_Role_DetailView } from './VC/UserMaintenance/VC_UserRole';
import { VC_ItemView } from './VC/VC_Item';
import { VC_PurchaseOrder } from './VC/Transaction/VC_PurchaseOrder';
import { VC_Position_View } from './VC/VC_Position';
import { VC_Employee_View } from './VC/VC_Employee';
import { VC_Schedule } from './VC/VC_Schedule';
import { VC_User } from './VC/UserMaintenance/VC_User';
import { VC_ItemMasterFile } from './VC/Report/VC_ItemMasterFile';
import { VC_Navigation } from './VC/VC_Navigation';
import { VC_View } from './VC/VC_View';
import { VC_DocumentSeries } from './VC/VC_DocumentSeries';
import { VC_BillingInvoice } from './VC/VC_BillingInvoice';
import { VC_PatientAppointment } from './VC/VC_PatientAppointment';

export function VC_Helper_Init(): void {
  const VC_Class_Helper = [

    VC_DetailView_Detail_ListView,
    VC_ListView_Detail,
    VC_DocListView,
    VC_Patient_View,
    VC_Employee_View,
    VC_Schedule,
    VC_User_Role_DetailView,
    VC_ItemView,
    VC_PurchaseOrder,
    VC_Position_View,
    VC_Appointment,
    VC_User,
    VC_ItemMasterFile,
    VC_Report,
    VC_Navigation,
    VC_View,
    VC_DocumentSeries,
    VC_BillingInvoice,
    VC_PatientAppointment
  ];

}

//
//
//

