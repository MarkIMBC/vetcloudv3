import { BillingInvoice } from '../APP_MODELS';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn, AppDataTableComponent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { async } from '@angular/core/testing';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { ViewMenuItem } from 'src/app/View';

@VC_Model({
  viewId: [
      "BillingInvoice_DetailView",
      "BillingInvoice_ListView"
  ]
})
export class VC_BillingInvoice extends VC_BaseView {

    onDataTableOption_Initialized(opt:DataTableOption) : void {
        
    }

    onCurrentObject_Load() : Promise<void> {

        return Promise.resolve();
    }

    onDetail_ViewMenus_Initialized(name:string, viewMenuItems: ViewMenuItem[]) : Promise<void> {

        return Promise.resolve();
    };

    onForm_Validations() : Promise<IFormValidation[]> {
        
        var BillingInvoice = this.CurrentObject as BillingInvoice;
        var validations : IFormValidation[] = [];

        return Promise.resolve(validations);
    };

    onFormValue_Changed(evt : IControlValueChanged) : Promise<void> { 
        var BillingInvoice = this.CurrentObject as BillingInvoice;

        return Promise.resolve();
    }

    async ngAfterViewInit(): Promise<void> {

        if(this.ViewType == ViewTypeEnum.DetailView){

        }

        return Promise.resolve();
    };

}
