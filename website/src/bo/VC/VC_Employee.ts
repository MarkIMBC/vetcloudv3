import { Employee } from './../APP_MODELS';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { async } from '@angular/core/testing';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ValidationService } from 'src/utils/service/validation.service';

@VC_Model({
  viewId: [
      "Employee_DetailView",
      "Employee_ListView"
  ]
})
export class VC_Employee_View extends VC_BaseView {

    onForm_Validations() : Promise<IFormValidation[]> {

        var Employee = this.CurrentObject as Employee;
        var validations : IFormValidation[] = [];

        /* Validate Email Address */
        var isValidEmail = this.validationService.validateEmail(Employee.Email);
        if(isValidEmail != true){

            validations.push({
                message: 'Invalid Email Address Format'
            });
        }

        return Promise.resolve(validations)
    };

    onFormValue_Changed(evt : IControlValueChanged) : Promise<void> { 
        var Employee = this.CurrentObject as Employee;
        if ( ["FirstName","MiddleName","LastName"].includes(evt.name) == true ) { 

            Employee.Name = ( Employee.LastName ?? "" ) + ", " + ( Employee.FirstName ?? "" )   + " " + ( Employee.MiddleName ?? "" );
        }
        return Promise.resolve();
    }

    async ngAfterViewInit(): Promise<void> {
        return Promise.resolve();
    };

}
