import { ControlType, PropertyType } from './../../APP_MODELS';
import { IAppSelectBoxOption } from './../../../utils/controls/appSelectBox/appSelectBox.component';
import { VC_Model } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from '../Base/VC_BaseView';
import { BaseAppControlOption } from 'src/utils/controls';
import { DataTableOption, DataTableColumn, IDataRow } from 'src/utils/controls/appDataTable/appDataTable.component';
import { isNullOrUndefined } from 'util';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';



@VC_Model({
    viewId: "Report_DetailView"
})
export class VC_Report extends VC_BaseView {

    ControlTypes : ControlType[] = [];
    PropertyTypes : PropertyType[] = [];

    async onControlOption_Initialized(name?: string, opt?: BaseAppControlOption): Promise<void> {   
        if ( name == "Report_Filters" ) {
            this.ControlTypes = await this.dataService.query<ControlType>(this.encrypt("SELECT ID, Name FROM dbo._tControlType WHERE ID IN (1, 3, 6, 18, 19);"));
            this.PropertyTypes = await this.dataService.query<PropertyType>(this.encrypt("SELECT ID, Name FROM dbo._tPropertyType"));
            var gridOption = opt as DataTableOption;
            gridOption.onColumns_Initialized = (cols: DataTableColumn[]) => {
                var EditableCols: string[] = ["Name", "IsActive" , "ID_ControlType", "ID_PropertyType", "DataSource", "Caption"];   
                cols.forEach(c => {
                    if (EditableCols.includes(c.dataField) == true) { 
                        c.allowEdit = true;

                        if ( c.dataField !== "Name" ) {
                            c.allowEdit = (d: any) => {
                                if ( isNullOrUndefined(d.Name) == true ) {
                                    this.msgBox.warning("Data Field Name is required.","Report");
                                    return false;
                                }

                                if ( c.dataField == "DataSource" ) {
                                    this.dvFormSvc.open({
                                        DataSource: d.DataSource
                                    },`${c.dataField} Datasource`,[
                                        {
                                            Name: "DataSource",
                                            Caption : "Data Source",
                                            ID_ControlType: ControlTypeEnum.TextArea
                                        }
                                    ]).then((res) => { 
                                        if ( res == false ) return false;
                                        d.DataSource = res.DataSource;
                                    });
                                    return false;
                                }

                                return true;
                            }
                            if (c.dataField == "ID_ControlType" || c.dataField == "ID_PropertyType") {
                                c.editorType = ControlTypeEnum.SelectBox;
                                c.displayText = (rowKey: any, val?: number) => { 
                                    var str = val + "";
                                    if (val) {
                                        switch (c.dataField) {
                                            case "ID_ControlType":
                                                str = this.ControlTypes.find(d => d.ID == val).Name;
                                                break;
                                            case "ID_PropertyType":
                                                str = this.PropertyTypes.find(d => d.ID == val).Name;
                                                break;
                                        }
                                    } else {
                                        str = "";
                                    }
                                    return str;
                                };
                                c.onEditorOption_Initialized = (row: IDataRow, opt: BaseAppControlOption) => { 
                                    var items: any[] = [];
                                    switch ( c.dataField ) {
                                        case "ID_ControlType":
                                            items = this.ControlTypes;
                                            break;
                                        case "ID_PropertyType":
                                            items = this.PropertyTypes;
                                            break;
                                    };
                                   (opt as IAppSelectBoxOption).items = items;
                                }
                             }

                        } else {
                            c.required = true;
                        }
                    }
                })
            }
        }
        return Promise.resolve();
    }
    
    
}