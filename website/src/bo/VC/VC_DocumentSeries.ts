import { DocumentSeries } from '../APP_MODELS';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { async } from '@angular/core/testing';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { isNullOrUndefined, isNull } from 'util';

@VC_Model({
  viewId: [
      "DocumentSeries_DetailView",
      "DocumentSeries_ListView"
  ]
})
export class VC_DocumentSeries extends VC_BaseView {

    async onForm_Validations() : Promise<IFormValidation[]> {

        this.currentUser = this.currentUser;
        
        var DocumentSeries = this.CurrentObject as DocumentSeries;
        var validations : IFormValidation[] = [];
        var validationFromDBMsg : string;

        validationFromDBMsg = await this.validateFromDB();

        if(isNull(validationFromDBMsg)) validationFromDBMsg = "";

        if(validationFromDBMsg.length > 0){

            validations.push({
                message: validationFromDBMsg
            });
        }

        return Promise.resolve(validations);
    };

    async validateFromDB() : Promise<string> {

        var DocumentSeries = this.CurrentObject as DocumentSeries;
        var message : string = "";

        var validation : IFormValidation;

        var obj = await this.dataService.execSP("pValidateDocumentSeries", {
            ID_CurrentObject : DocumentSeries.ID,
            ID_Model : DocumentSeries.ID_Model
        },{
            isReturnObject: true
        })

        //console.log("pValidateDocumentSeries", obj);

        if(isNullOrUndefined(obj)) obj = { isValid: false, message: ''}

        if(obj.isValid == false){

            message = obj.message;
        }

        return message;
    }
}
