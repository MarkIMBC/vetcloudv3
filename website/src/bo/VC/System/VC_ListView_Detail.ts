import { APP_LISTVIEW, ListView_Detail, APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_Model } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from '../Base/VC_BaseView';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';


@VC_Model({
    viewId: "ListView_Detail_ListView"
})
export class VC_ListView_Detail extends VC_BaseView {

    onDataTableOption_Initialized(opt: DataTableOption): void { 
        opt.actionRowItems = [];
        var Oid_ListView = this.ViewParams.Oid_ListView;
        var Oid = this.ViewParams.Oid;
        opt.propertyKey = "Oid";
        opt.Oid_Model = APP_MODEL.LISTVIEW_DETAIL;
        opt.remoteEditing = true;
        opt.apiUrl = `Model/ListViewColumns/${Oid_ListView}/${Oid ? Oid : "-"}`;
        opt.allowEditing = true;
        opt.customizeColumns = (cols: DataTableColumn[]) => {
            var strCols = ["Name", "Caption", "IsActive", "VisibleIndex", "DisplayProperty", "ID_PropertyType"];
            var _tempCols: DataTableColumn[] = [];
            cols.forEach(c => {
                if (strCols.includes(c.dataField) == false) return;
                if ( c.dataField == "Name" ){
                    c.fixed = true;
                }
                c.visible = true;
                c.allowEdit = true;
                _tempCols.push(c);
            });
            var seqNo: number = 1;
            strCols.forEach(s => { 
                var col = _tempCols.find(d => d.dataField == s);
                col.seqNo = ( seqNo++ ) * 100;
                seqNo++;
            });
            return _tempCols;
         }
    };

    ngAfterViewInit(): Promise<void> {
        return Promise.resolve();
    };

}