import { AppLookupBoxComponent, IAppLookupBoxOption } from './../../../utils/controls/appLookupBox/appLookupBox.component';
import { IPagingOption } from './../../../utils/service/data.service';
import { AppPickListBoxComponent } from './../../../utils/controls/appPickListBox/appPickListBox.component';
import { User_Roles, User } from './../../APP_MODELS';
import { Guid } from 'guid-typescript';
import { UserRole_Detail, Model, UserRole } from '../../APP_MODELS';
import { AppDataTableComponent, DataTableColumn, IRowValueChange, IDataRow, DataTypeEnum } from '../../../utils/controls/appDataTable/appDataTable.component';
import { ViewMenuItem } from '../../../app/View/index';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from '../Base/VC_BaseView';
import { DataTableOption } from 'src/utils/controls/appDataTable/appDataTable.component';
import { isNullOrUndefined } from 'util';
import { IAppPickListOption } from 'src/utils/controls';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';
import * as TS from "linq-typescript";

@VC_Model({
    viewId:"User_DetailView"
})
export class VC_User extends VC_BaseView {

    @App_Control({ name: "User_Roles" })
    roleDetails: AppDataTableComponent;

    @App_Control({ name : "ID_Employee" })
    employee: AppLookupBoxComponent;

    async ngAfterViewInit(): Promise<void> {    
        this.roleDetails.newRowEditing = false;
        this.roleDetails.option.onColumns_Initialized = (cols: DataTableColumn[]) => {
            var EditableColumns : string[] = ["Comment", "SeqNo"];
            cols.forEach((c) => { 
                if ( EditableColumns.includes(c.dataField) ) {
                    c.allowEdit = true;
                }
            });
            return cols;
        }
        this.menuItems.push({
            label : "Reset Password",
            IsVisible: () => { 
                return this.CurrentObject?.ID > 0
            },
            command: async () => { 
              var res = await this.dvFormSvc.open({},'Reset Password', [
                    {
                        Name : "NewPassword",
                        Caption : "Old Password",
                        IsRequired : true
                    },
                    {
                        Name : "ConfirmPassword",
                        Caption : "Confirm Password",
                        IsRequired : true
                    }
                ]);
            }
        });
        (this.employee.option as IAppLookupBoxOption).onBefore_LoadData.subscribe((op: IPagingOption)=>{
            var session = this.userAuth.getDecodedToken();
            op.SQL = this.encrypt("SELECT * FROM tEmployee WHERE ID = "  + session.ID_Employee );
        });
    }

    onDetail_ViewMenus_Initialized(name:string, detail:ViewMenuItem[] ) : Promise<void> {
        detail.push({
            label: "Add Roles",
            command: async () => { 
                var CurrentUser = this.CurrentObject as User;
                var CurrentRoles = [ -1 ];
                if ( CurrentUser?.User_Roles?.length > 0 ) {
                    CurrentRoles = new TS.List(CurrentUser?.User_Roles).select(s=>s.ID).toArray();
                }
                var UserRoles = await this.lvModal.ListLookUp<UserRole>({
                    sql :  `SELECT * FROM dbo.vUserRole  WHERE IsActive	= 1 AND ID NOT IN (${CurrentRoles.join(",")})`,
                    onGridOption_Initialized : (gridOption:DataTableOption) : void => { 
                       gridOption.columns = [
                           {
                               dataField : "Name",
                               caption: "Role Name"
                           },
                           {
                               dataField : "IsFullAccess",
                               caption : "Full Access",
                               dataType : DataTypeEnum.Boolean
                           }
                       ] as  DataTableColumn[];
                     },
                    width: 700,
                    title : "Select Roles"
                });
                if ( UserRoles.length == 0 ) return;
                if (isNullOrUndefined(CurrentUser.User_Roles) == true ) CurrentUser.User_Roles = [];
                // alert(UserRoles.length);
                UserRoles.forEach(d => { 
                    var det : User_Roles = {
                        ID: -1,
                        Name: d.Name,
                        IsActive: true,
                        ID_UserRole: d.ID
                    }
                    CurrentUser.User_Roles.push(det)
                });
                //alert(CurrentUser.User_Roles.length);
            },
            icon: "blue-text mdi mdi-plus"
        })
        return Promise.resolve();
    }    
}

class UserRole_Detail_DTO extends UserRole_Detail {
    ModelName:string;
}

class UserRole_DTO extends UserRole {

}