import { QueryList } from '@angular/core';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { User, AppointmentSchedule } from '../APP_MODELS';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';
import { ViewMenuItem } from '../../app/VIew/index';
import { CalendarInfo } from './Schedule/VC_Appointment';
import { CalendarOptions, EventApi, DateSelectArg, EventClickArg, EventInput, Calendar, FullCalendarComponent, DatesSetArg } from '@fullcalendar/angular';

@VC_Model({
  viewId: [
      "AppointmentSchedule_DetailView",
      "AppointmentSchedule_ListView"
  ]
})
export class VC_AppointmentSchedule extends VC_BaseView {

  public CurrentAppointmentSchedule: AppointmentSchedule;
    
  onForm_Validations() : Promise<IFormValidation[]> {

    var Sched = this.CurrentObject as AppointmentSchedule;
    var validations : IFormValidation[] = [];

    if ( isNullOrUndefined(Sched.DateStart) != true && isNullOrUndefined(Sched.DateEnd) != true ) {

      var isValidDateRange = this.validationService.validateDateTimeRange(Sched.DateStart, Sched.DateEnd, true);

      if ( isValidDateRange != true ) {
        validations.push({
          message : "Invalid Date AppointmentSchedule"
        })
      }
    };

    return Promise.resolve(validations)
  };

  onCurrentObject_Load() : Promise<void> {

    var AppointmentSchedule = this.CurrentObject as AppointmentSchedule;
    if ( AppointmentSchedule.ID == -1 ) {
       var info = this.ViewParams ; 

        if (info) { 

            AppointmentSchedule.ID_Doctor = info.ID_Doctor;
            AppointmentSchedule.DateStart = info.DateStart;
            AppointmentSchedule.DateEnd = info.DateEnd;
        }
    }
    this.CurrentAppointmentSchedule = AppointmentSchedule;
    return super.onCurrentObject_Load();
}

}
