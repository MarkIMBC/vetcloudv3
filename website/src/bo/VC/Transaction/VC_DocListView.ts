import { APP_LISTVIEW } from 'src/bo/APP_MODELS';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';
import { AppDataTableComponent, DataTableColumn, IRowValueChange, DataTableOption, IDataRow } from '../../../utils/controls/appDataTable/appDataTable.component';
import { IAppPickListOption } from '../../../utils/controls/appPickListBox/appPickListBox.component';
import { VC_BaseView } from '../Base/VC_BaseView';
import { App_Control, VC_Model } from 'src/bo/VC_Model.decorator';


@VC_Model({
  viewId: "*"
})
export class VC_DocListView extends VC_BaseView {


  onDataTableOption_Initialized(opt: DataTableOption): void {

    opt.onRowClass_Initialized = (cls: any, item: IDataRow, rowIndex: number): void => {
      if (item.colGroup && item.groupIndex > 0) {
        if (["ID_FilingStatus"].includes(item.colGroup.dataField)) {
          if (item.groupValue) {
            var value: string = (item.groupValue + "").toString().trim();
            cls["FilingStatus " + value] = true;
          }
        }
      }
    }
  
    opt.onColumns_Initialized = (cols: DataTableColumn[]): void => {

      var ID_FilingStatus = cols.find(d => d.dataField == "ID_FilingStatus");
      var ID_DocStatus = cols.find(d => d.dataField == "ID_DocStatus");
      var AppointmentStatus_ID_FilingStatus = cols.find(d => d.dataField == "AppointmentStatus_ID_FilingStatus");

      if (ID_FilingStatus) {
        ID_FilingStatus.displayText = (rowKey: any, value: string): string => {
          var ID = rowKey.ID_FilingStatus;
          if (ID == undefined) return "";
          if (value == null || value == undefined) return "";
          return `<div class="FilingStatus FS_${value.replace(" ", "")}">
                        <span class="mdi mdi-circle"></span> 
                  ${value}</div>`
        };
      }

      if (ID_DocStatus) {
        ID_DocStatus.displayText = (rowKey: any, value: string): string => {
          var ID = rowKey.ID_DocStatus;
          if (ID == undefined) return "";
          if (value == null || value == undefined) return "";
          return `<div class="DocStatus DS_${value.replace(" ", "")}">
                          <span class="mdi mdi-circle"></span> 
                    ${value}</div>`
        };
      }

      if (AppointmentStatus_ID_FilingStatus) {
        AppointmentStatus_ID_FilingStatus.displayText = (rowKey: any, value: string): string => {
          var ID = rowKey.AppointmentStatus_ID_FilingStatus;
          if (ID == undefined) return "";
          if (value == null || value == undefined) return "";
          return `<div class="AppointmentStatus AS_${value.replace(" ", "")}">
                        <span class="mdi mdi-circle"></span> 
                  ${value}</div>`
        };
      }
    }
  }

}

