import {
  IUserSession,
  TokenSessionFields,
} from "./../AppServices/UserAuthentication.service";
import { TabView } from "primeng/tabview";
import * as TS from "linq-typescript";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { MenuItem } from "primeng/api/menuitem";
import { PanelMenu } from "primeng/panelmenu/panelmenu";
import * as $ from "jquery";
import { ContextMenu } from "primeng/contextmenu/contextmenu";
import { isNullOrUndefined } from "util";
import { APP_DETAILVIEW, APP_MODEL } from "src/bo/APP_MODELS";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { async } from "@angular/core/testing";
import {
  PropertyTypeEnum,
  IMenuItem,
  ViewTypeEnum,
  DEBUG,
  UserGroupEnum,
  PositionEnum,
} from "src/utils/controls/class_helper";
import { TabService } from "../AppServices/tab.service";
import { DataService } from "src/utils/service/data.service";
import { LoaderService } from "../AppServices/LoaderInterceptor";
import { DetailViewDialogService } from "../View/DetailViewDialog/DetailViewDialog.component";
import { FormHelperDialogService } from "../View/FormHelperView/FormHelperView.component";
import { MegaMenuItem } from "primeng/api";
import { UserAuthenticationService } from "../AppServices/UserAuthentication.service";
import { CustomnavigationrouteService } from "src/utils/service/customnavigationroute.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { QRCodeScanRedirectRecordComponent } from "src/utils/qrcode-scan-redirect-record/qrcode-scan-redirect-record.component";

@Component({
  selector: "app-MainPortal",
  templateUrl: "./MainPortal.component.html",
  styleUrls: ["./MainPortal.component.less"],
})
export class MainPortalComponent implements OnInit {
  tabSvc: TabService;
  isLoading = false;

  isShowAdminContextMenu: boolean = false;
  IsShowPaymentWarningLabel: boolean = false;

  constructor(
    private customNavRoute: CustomnavigationrouteService,
    protected router: Router,
    protected globalFx: GlobalfxService,
    private ds: DataService,
    _tabSvc: TabService,
    protected cs: CrypterService,
    private changeDetector: ChangeDetectorRef,
    private dialogSvc: DetailViewDialogService,
    private formDialogSvc: FormHelperDialogService,
    private msgBox: MessageBoxService,
    private userAuthSvc: UserAuthenticationService
  ) {
    this.tabSvc = _tabSvc;
    this.currentUserSession = userAuthSvc.getDecodedToken();

    this.isShowAdminContextMenu =
      this.currentUserSession.ID_UserGroup == UserGroupEnum.System;
  }

  @ViewChild("qrcodescanredirectrecorddialog")
  qrScanRedirectRecordDialog: QRCodeScanRedirectRecordComponent;

  @ViewChild("panelMenu")
  panelMenu: PanelMenu;

  @ViewChild("tabView")
  tabView: TabView;

  activeMenuItems: IMenuItem[] = [];

  menuItems: IMenuItem[] = [];
  menuNavigations: MenuNavigation[] = [];

  contextMenus: MenuItem[] = [];

  @ViewChild("contextMenu")
  contextMenu: ContextMenu;

  accountMenuItems: MenuItem[] = [];

  targetMenu: JQuery<any> = null;

  currentUserSession: TokenSessionFields;

  onRight_Click($event: any): void {
    var targetMenus = new TS.List($event.path)
      .select((d) => $(d))
      .where((d) => isNullOrUndefined(d.attr("class")) !== true)
      .where(
        (d) =>
          $(d).attr("class").includes("ui-panelmenu-header-link") == true ||
          $(d).attr("class").includes("ui-menuitem-link") == true
      )
      .toArray();
    if (targetMenus.length > 0) {
      this.targetMenu = targetMenus[0];
    } else {
      this.targetMenu = null;
    }
  }

  onContextMenu_Show(): void {
    this.contextMenus = [];
    this.contextMenus.push({
      label: "Refresh",
      icon: "mdi mdi-folder-sync",
      command: async () => {
        await this.ngOnInit();
      },
    });
    if (this.targetMenu != null) {
      var Oid = this.targetMenu.attr("id");
      var menuNav: MenuNavigation = null;
      if (isNullOrUndefined(Oid) != true) {
        Oid = Oid.replace("_header", "");
        menuNav = new TS.List(this.menuNavigations)
          .where((d) => d.Oid == Oid)
          .toArray()[0];
        if (menuNav) {
          var menuNavItems: MenuItem[] = [];

          menuNavItems.push({
            label: "Open DetailView",
            icon: "mdi mdi-folder-open",
            command: () => {
              this.dialogSvc.open(
                APP_DETAILVIEW.NAVIGATION_DETAILVIEW,
                menuNav.Oid,
                {
                  title: `Edit ${menuNav.Text} Navigation`,
                }
              );
            },
          });

          menuNavItems.push({
            label: "Hide",
            command: async () => {
              await this.ds.saveObject(APP_MODEL.NAVIGATION, {
                IsActive: false,
                Oid: menuNav.Oid,
              });
              this.ngOnInit();
            },
          });

          if (menuNav.ID_Model) {
            menuNavItems.push({
              label: "Open Model",
              command: () => {
                this.dialogSvc.open(
                  APP_DETAILVIEW.MODEL_DETAILVIEW,
                  menuNav.ID_Model,
                  {
                    title: `${menuNav.Model}`,
                  }
                );
              },
            });
          }

          var folders = new TS.List(this.menuNavigations)
            .where((d) => d.ID_View == null && d.Oid != Oid)
            .toArray();
          if (folders.length > 0) {
            var MoveToFolder: IMenuItem[] = [];
            if (menuNav.ID_Parent != null) {
              MoveToFolder.push({
                label: "Root",
                icon: "mdi mdi-folder",
                command: async () => {
                  await this.ds.saveObject(APP_MODEL.NAVIGATION, {
                    ID_Parent: null,
                    Oid: menuNav.Oid,
                  });
                  this.ngOnInit();
                },
              });
            }
            folders.forEach((d) => {
              MoveToFolder.push({
                label: d.Text,
                icon: "mdi mdi-folder",
                command: async () => {
                  await this.ds.saveObject(APP_MODEL.NAVIGATION, {
                    ID_Parent: d.Oid,
                    Oid: menuNav.Oid,
                  });
                  this.ngOnInit();
                },
              });
            });
            menuNavItems.push({
              label: "Move to folder",
              icon: "mdi mdi-folder-upload",
              items: MoveToFolder,
            });
          }

          if (menuNav.ID_View == null) {
            menuNavItems.push({
              label: "Delete folder",
              icon: "mdi mdi-folder-remove",
              command: async () => {
                if (
                  (await this.msgBox.confirm(
                    "Are you sure you want to delete this folder?",
                    menuNav.Text,
                    "Yes",
                    "No"
                  )) != true
                )
                  return;
                await this.ds.execSP("_pDeleteNavigation", {
                  Oid: menuNav.Oid,
                });
                this.ngOnInit();
              },
            });
          }

          this.contextMenus.push({
            label: menuNav.Text,
            icon: "mdi mdi-folder",
            items: menuNavItems,
          });
        }
      }
    }
    this.contextMenus.push({
      label: "Create New folder",
      icon: "mdi mdi-folder-plus-outline",
      command: async () => {
        var res = await this.formDialogSvc.open({}, "Create New folder", [
          {
            Name: "Name",
            IsRequired: true,
          },
          {
            Name: "Caption",
            IsRequired: true,
          },
        ]);
        if (res == false) return;
        await this.ds.saveObject(APP_MODEL.NAVIGATION, {
          Oid: null,
          Name: res.Name,
          Caption: res.Caption,
          IsActive: true,
          ID_Parent: menuNav != null ? menuNav.Oid : null,
          DateCreated: null,
          ID_CreatedBy: 1,
        });
        this.ngOnInit();
      },
    });
    //
    // Model
    //

    this.contextMenus.push({
      label: "Create New App Module",
      icon: "mdi mdi-hexagon-slice-6",
      command: async () => {
        var res = await this.formDialogSvc.open(
          {
            IsCreateNavigation: true,
          },
          "Create New App Module",
          [
            {
              Name: "TableName",
              Caption: "Table Name",
              IsRequired: true,
            },
            {
              Name: "IsCreateNavigation",
              Caption: "Is Navigation Menu",
              ID_PropertyType: PropertyTypeEnum.Bit,
            },
          ]
        );
        if (res == false) return;
        if (
          (await this.msgBox.confirm(
            "Are you sure you want to create module?",
            res.TableName,
            "Yes",
            "No"
          )) != true
        )
          return;
        var data = {
          TableName: (res.TableName + "").replace(" ", "_"),
          IsCreateNavigation: res.IsCreateNavigation,
          ID_Parent: null,
        };
        if (menuNav != null) {
          if (menuNav.ID_View == null) {
            data.ID_Parent = menuNav.Oid;
          }
        }
        await this.ds.execSP("_pCreateAppModuleWithTable", data);
        this.ngOnInit();
      },
    });

    var OtherMenus: MenuItem[] = [];
    OtherMenus.push({
      label: "Fix Default Properties",
      command: async () => {
        await this.ds.execSP("_pFixDefaultProperties", {});
        this.ngOnInit();
      },
    });
    this.contextMenus.push({
      label: "Others",
      items: OtherMenus,
    });
  }

  CompanyName: string = "";
  Name_Position: string = "";

  async ngOnInit(): Promise<any> {
    var sqlPosition = this.cs.encrypt(`
      SELECT Name_Position
      FROM vEmployee 
      WHERE 
        ID = ${this.currentUserSession.ID_Employee}
    `);

    var positienDS = await this.ds.query<any>(sqlPosition);

    if (positienDS.length > 0) {
      this.Name_Position = positienDS[0]["Name_Position"];
    }

    var menus: MenuNavigation[] = await this.ds.get("Model/GetNavigations");
    await this.customNavRoute.load();

    this.menuNavigations = menus;

    this.initMenus(menus);

    this.accountMenuItems = [
      {
        label: "File",
        items: [
          {
            label: "New",
            icon: "pi pi-fw pi-plus",
            items: [{ label: "Project" }, { label: "Other" }],
          },
          { label: "Open" },
          { label: "Quit" },
        ],
      },
      {
        label: "Edit",
        icon: "pi pi-fw pi-pencil",
        items: [
          { label: "Delete", icon: "pi pi-fw pi-trash" },
          { label: "Refresh", icon: "pi pi-fw pi-refresh" },
        ],
      },
    ];

    var queryString = this.cs.encrypt(`
        SELECT 
          Name, IsShowPaymentWarningLabel
        FROM vCompany
        WHERE 
          ID IN (${this.currentUserSession.ID_Company})
      `);

    var records = await this.ds.query<any>(queryString);

    this.CompanyName = records[0]["Name"];
    this.IsShowPaymentWarningLabel = records[0]["IsShowPaymentWarningLabel"];
  }

  async initMenus(menus: MenuNavigation[], expand?: boolean): Promise<void> {
    var menuItems: IMenuItem[] = [];
    var rootMenus = new TS.List(menus)
      .where((d) => d.ID_Parent == null && d.Name != "Dashboard_ListView")
      .orderBy((d) => d.SeqNo)
      .toArray();
    var initChilds = (menu: MenuItem, nav: MenuNavigation): void => {
      var childs = new TS.List(menus)
        .where((d) => d.ID_Parent == nav.Oid)
        .orderBy((d) => d.SeqNo)
        .toArray();
      var childMenuItems: MenuItem[] = [];

      childs.forEach(async (d) => {
        var item: IMenuItem = {
          label: d.Text,
          routerLink: null,
          id: d.Oid,
          appMenu: d,
        };

        switch (d.ID_ViewType) {
          case ViewTypeEnum.ListView:
            var CustomRouterLink: string = "";
            CustomRouterLink = this.customNavRoute.GetLink(
              d.ID_ListView,
              ViewTypeEnum.ListView
            );

            if (isNullOrUndefined(CustomRouterLink)) CustomRouterLink = "";
            if (CustomRouterLink.length == 0)
              CustomRouterLink = `lv/${d.Model}/${btoa(d.ID_ListView)}`;

            item.routerLink = CustomRouterLink;
            item.icon = "mdi mdi-hexagon-slice-6";
            break;
          case ViewTypeEnum.ReportView:
            var CustomRouterLink: string = "";
            CustomRouterLink = this.customNavRoute.GetLink(
              d.ID_Report,
              ViewTypeEnum.ReportView
            );

            if (isNullOrUndefined(CustomRouterLink)) CustomRouterLink = "";
            if (CustomRouterLink.length == 0)
              CustomRouterLink = `rv/${d.Text}/${btoa(d.ID_Report)}/${btoa(
                "null"
              )}/${btoa("null")}`;

            item.routerLink = CustomRouterLink;
            item.icon = "mdi mdi-note-outline";
            item.items = null;
            break;
          case ViewTypeEnum.Custom:
            item.routerLink = `${d.Route}`;
            item.icon = "mdi mdi-note-outline";
            item.items = null;
            break;
        }

        initChilds(item, d);
        if (
          item.items?.length > 0 ||
          isNullOrUndefined(item.routerLink) !== true
        )
          childMenuItems.push(item);
      });

      if (childMenuItems.length > 0) {
        menu.items = childMenuItems;
        menu.expanded = expand;
      } else {
        menu.command = () => {
          //this.onPanelMenu_Click(menu, nav);
        };
      }
    };

    rootMenus.forEach((r) => {
      //console.log("MenuItem", r);
      var menu: IMenuItem = {
        label: r.Text,
        url: null,
        items: [],
        id: r.Oid,
        appMenu: r,
        expanded: expand,
      };
      //
      //
      switch (r.ID_ViewType) {
        case ViewTypeEnum.ListView:
          var CustomRouterLink: string = "";
          CustomRouterLink = this.customNavRoute.GetLink(
            r.ID_ListView,
            ViewTypeEnum.ListView
          );

          if (isNullOrUndefined(CustomRouterLink)) CustomRouterLink = "";
          if (CustomRouterLink.length == 0)
            CustomRouterLink = `lv/${r.Model}/${btoa(r.ID_ListView)}`;

          menu.routerLink = CustomRouterLink;
          menu.icon = "mdi mdi-hexagon-slice-6";
          menu.items = null;
          break;
        case ViewTypeEnum.ReportView:
          var CustomRouterLink: string = "";
          CustomRouterLink = this.customNavRoute.GetLink(
            r.ID_Report,
            ViewTypeEnum.ReportView
          );

          if (isNullOrUndefined(CustomRouterLink)) CustomRouterLink = "";
          if (CustomRouterLink.length == 0)
            CustomRouterLink = `rv/${r.Text}/${btoa(r.ID_Report)}/${btoa(
              "null"
            )}/${btoa("null")}`;

          menu.routerLink = CustomRouterLink;
          menu.icon = "mdi mdi-note-outline";
          menu.items = null;
          break;
        case ViewTypeEnum.Custom:
          menu.routerLink = `${r.Route}`;
          menu.icon = "mdi mdi-note-outline";
          menu.items = null;
          break;
      }
      //
      //
      initChilds(menu, r);
      if (
        menu.items?.length > 0 ||
        isNullOrUndefined(menu.routerLink) != true
      ) {
        menuItems.push(menu);
      }
    });
    //
    //
    //
    //temporary
    // menuItems.unshift({
    //   label : "Report",
    //   routerLink : "Report",
    //   icon : "mdi mdi-file"
    // });
    // menuItems.unshift({
    //   label: "Message",
    //   routerLink: "Message",
    //   icon: "mdi mdi-calendar",
    // });

    rootMenus.forEach((r) => {
      var menu: IMenuItem = {
        label: r.Text,
        url: null,
        items: [],
        id: r.Oid,
        appMenu: r,
        expanded: expand,
      };
      //
      //

      switch (r.ID_ViewType) {
        case ViewTypeEnum.ListView:
          var CustomRouterLink: string = "";
          CustomRouterLink = this.customNavRoute.GetLink(
            r.ID_ListView,
            ViewTypeEnum.ListView
          );

          if (isNullOrUndefined(CustomRouterLink)) CustomRouterLink = "";
          if (CustomRouterLink.length == 0)
            CustomRouterLink = `lv/${r.Model}/${btoa(r.ID_ListView)}`;

          menu.routerLink = CustomRouterLink;

          menu.icon = "mdi mdi-hexagon-slice-6";
          menu.items = null;
          break;
      }
    });

    menuItems.unshift({
      label: "Home",
      routerLink: "Home",
      icon: "mdi mdi-home",
    });
    this.menuItems = menuItems;

    var objReferenceMenuItems = await this.ds.execSP(
      "pGetReferenceLinkMenuItems",
      { ID_UserSerssion: this.currentUserSession.ID_Session },
      {
        isReturnObject: true,
      }
    );

    if (objReferenceMenuItems) {
      var menuItemsReferenceMenuItems = [];

      objReferenceMenuItems.MenuItems.forEach((objReferenceMenuItem) => {
        var menuItemsReferenceMenuItem = {
          label: objReferenceMenuItem.Caption,
          routerLink: `./ReferenceLink/${objReferenceMenuItem.Name}`,
          icon: "mdi mdi-book",
        };

        menuItemsReferenceMenuItems.push(menuItemsReferenceMenuItem);
      });

      menuItems.push({
        label: "Reference",
        icon: "mdi mdi-books",
        items: menuItemsReferenceMenuItems,
      });
    }
  }

  async LogOutUser(): Promise<void> {
    if (
      (await this.msgBox.confirm(
        "Are you sure you want to Logout?",
        "VeCS - Vet. Cloud System"
      )) !== true
    )
      return;
    await this.userAuthSvc.LogOut();
    window.location.href = "Login";
  }

  async gotoMessage(): Promise<void> {
    var routerLink = [];

    routerLink = [`/Main`, "Message"];

    this.router.navigate(routerLink);
  }

  ngAfterViewInit() {}

  searchValue: string = null;
  onSearchBox_KeyDown(evt: KeyboardEvent): void {
    if (evt.keyCode == 13 || evt.keyCode == 27) {
      if (this.searchValue) {
        if (evt.keyCode == 27) this.searchValue = "";
        if (this.searchValue.trim().length > 0) {
          var list = new TS.List(this.menuNavigations)
            .where((s) =>
              s.Text.toUpperCase().includes(this.searchValue.toUpperCase())
            )
            .toArray();
          var parentOid: string[] = [];
          var findParent = (Oid: string): void => {
            var p = this.menuNavigations.find((d) => d.Oid == Oid);
            if (p) {
              if (!list.find((d) => d.Oid == p.Oid)) {
                parentOid.push(p.Oid);
              }
              findParent(p.ID_Parent);
            }
          };

          list.forEach((p) => {
            if (p.ID_Parent) {
              if (!list.find((d) => d.Oid == p.ID_Parent)) {
                parentOid.push(p.ID_Parent);
              }
              findParent(p.ID_Parent);
            }
          });
          var list = list.concat(
            new TS.List(this.menuNavigations)
              .where((s) => parentOid.includes(s.Oid))
              .toArray()
          );
          this.initMenus(list, true);
        } else {
          this.initMenus(this.menuNavigations);
        }
      } else {
        this.initMenus(this.menuNavigations);
      }
      this.changeDetector.detectChanges();
    }
  }

  onKeyDown(evt: KeyboardEvent): void {}

  btnQRCode_onClick() {
    this.qrScanRedirectRecordDialog.show().then((obj) => {
      if (obj["ID_Model"] == undefined) return;

      this.redirectRecord(obj["ID_Model"], obj["ID_CurrentObject"]);
    });
  }

  redirectRecord(ID_Model: any, ID_CurrentObject: any) {
    var routeLink = [];

    var routerLinkPath = this.getRouterLinkByIDModel(ID_Model);

    if (routerLinkPath.length > 0) {
      var config = {
        // BackRouteLink: [`/Main`, "ClientList"],
        // ID_Client: record.ID
      };

      routeLink = [`/Main`, routerLinkPath, ID_CurrentObject];
      this.globalFx.customNavigate(routeLink, config);
    } else {
      this.globalFx.gotoNoAccessPage();
    }
  }

  getRouterLinkByIDModel(ID_Model: any) {
    var routerLink = "";

    switch (ID_Model) {
      case APP_MODEL.BILLINGINVOICE:
        routerLink = "BillingInvoice";
        break;
      case APP_MODEL.CLIENT:
        routerLink = "Client";
        break;
      case APP_MODEL.EMPLOYEE:
        routerLink = "Employee";
        break;

      case APP_MODEL.ITEM:
        routerLink = "Item";
        break;
      case APP_MODEL.PATIENT:
        routerLink = "Patient";
        break;
      case APP_MODEL.PATIENT_SOAP:
        routerLink = "Patient_SOAP";
        break;
      case APP_MODEL.PATIENT_CONFINEMENT:
        routerLink = "Confinement";
        break;

      case APP_MODEL.PAYMENTTRANSACTION:
        routerLink = "PaymentTransaction";
        break;

      case APP_MODEL.PURCHASEORDER:
        routerLink = "PurchaseOrder";
        break;

      case APP_MODEL.RECEIVINGREPORT:
        routerLink = "ReceivingReport";
        break;

      case APP_MODEL.SUPPLIER:
        routerLink = "Supplier";
        break;
      default:
        routerLink = "";
        break;
    }

    return routerLink;
  }
}

export class MenuNavigation {
  Oid: string;
  Name: string;
  ID_Parent: string;
  ID_View: string;
  ID_Report: string;
  View: string;
  Text: string;
  icon: string;
  IsActive: boolean;
  ID_ViewType: ViewTypeEnum;
  ID_Model: string;
  ID_ListView: string;
  Model: string;
  SeqNo: number;
  Route: string;

  //ChildMenus: MenuNavigation[];
}

export enum UserEnum {
  SystemAdministrator_Joseph = 1,
  Receptionist_Edward = 5,
  Dentist_Violet = 6,
}
