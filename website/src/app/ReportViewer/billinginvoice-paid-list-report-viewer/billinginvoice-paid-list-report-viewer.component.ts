import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { APP_REPORTVIEW, PropertyType } from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-billinginvoice-paid-list-report-viewer",
  templateUrl: "./billinginvoice-paid-list-report-viewer.component.html",
  styleUrls: ["./billinginvoice-paid-list-report-viewer.component.less"],
})
export class BillinginvoicePaidListReportViewerComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.BILLINGINVOICEPAIDLISTREPORT;

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      Select 
            DISTINCT 
            AttendingPhysician_ID_Employee ID, 
            AttendingPhysician_Name_Employee Name,
            Name_Position Position
      FROM [vzPaymentTransactionSummaryReport]
      WHERE 
            ID_Company = ${this.currentUser.ID_Company}
    `),
  };

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;

    this.CurrentObject.AttendingPhysician_Name_Employee = event.Name;
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onRemoveSelectedItem(
    event: any
  ) {
    this.CurrentObject.AttendingPhysician_Name_Employee = null;
  }

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;
    var dateStart;
    var dateEnd;
    if (formFilters.length == 0) {
      index = this.globalFx.findIndexByKeyValue(
        formFilters,
        "dataField",
        "Date_BillingInvoice"
      );
      if (index < 0) {
        index = this.globalFx.findIndexByKeyValue(
          formFilters,
          "dataField",
          "Date_PaymentTransaction"
        );
        if (index < 0) {
          formFilters.push({
            dataField: "Date_BillingInvoice",
            filterCriteriaType: FilterCriteriaType.Between,
            propertyType: PropertyTypeEnum.Date,
            value: [new Date(), new Date()],
          });
        }
      }
    }

    /* Header_CustomCaption */
    var caption = "";
    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = formFilters[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      caption += `Bill Date From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_PaymentTransaction"
    );
    if (index > -1) {
      var formFilter = formFilters[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }
      caption += `Last Paid Date From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (
      this.CurrentObject["AttendingPhysician_Name_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_Name_Employee"] != null
    ) {
      caption += `Attending Vet: ${this.CurrentObject.AttendingPhysician_Name_Employee}<br>`;
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];
    var dateStart;
    var dateEnd;
    var filterFormValues = this.appForm.getFilterFormValue();
    var index = -1;

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = filterFormValues[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date_PaymentTransaction"
    );
    if (index > -1) {
      var formFilter = filterFormValues[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    return validations;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    if (
      this.CurrentObject["AttendingPhysician_ID_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_ID_Employee"] != null
    ) {
      var value = this.CurrentObject["AttendingPhysician_ID_Employee"];

      formFilters.push({
        dataField: "AttendingPhysician_ID_Employee",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    return formFilters;
  }
}
