import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { APP_REPORTVIEW, PropertyType } from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-received-deposit-credit-report-viewer",
  templateUrl: "./received-deposit-credit-report-viewer.component.html",
  styleUrls: ["./received-deposit-credit-report-viewer.component.less"],
})
export class ReceivedDepositCreditReportViewerComponent extends BaseReportComponent {
  ID_Report: string = "DD2E14B6-CF4D-4532-83A5-7F8B65A315AE";

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;
    var dateStart;
    var dateEnd;

    if (formFilters.length == 0) {
      index = this.globalFx.findIndexByKeyValue(
        formFilters,
        "dataField",
        "Date"
      );
      if (index < 0) {
        formFilters.push({
          dataField: "Date",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [new Date(), new Date()],
        });
      }
    }

    /* Header_CustomCaption */
    var caption = "";
    index = this.globalFx.findIndexByKeyValue(formFilters, "dataField", "Date");
    if (index > -1) {
      var formFilter = formFilters[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      caption += `Date From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();
    var index = -1;
    var dateStart;
    var dateEnd;

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date"
    );
    if (index > -1) {

      var formFilter = filterFormValues[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    return validations;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    if (this.CurrentObject["Date"]) {
      var dateStart = this.CurrentObject.Date[0];
      var dateEnd = this.CurrentObject.Date[1];

      formFilters.push({
        dataField: "Date",
        filterCriteriaType: FilterCriteriaType.Between,
        propertyType: PropertyTypeEnum.Date,
        value: [dateStart, dateEnd],
      });
    }

    if (this.CurrentObject["Name_Client"]) {
      formFilters.push({
        dataField: "Name_Client",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject["Name_Client"],
      });
    }

    return formFilters;
  }
}
