import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { APP_REPORTVIEW, PropertyType } from './../../../bo/APP_MODELS';
import { BaseReportComponent } from './../BaseReportViewer';
import { Component, OnInit } from '@angular/core';
import { extend } from 'jquery';

@Component({
  selector: 'item-master-file-report-viewer',
  templateUrl: './item-master-file-report-viewer.component.html',
  styleUrls: ['./item-master-file-report-viewer.component.less']
})
export class ItemMasterFileReportVIewerComponent extends BaseReportComponent {

  ID_Report: string = APP_REPORTVIEW.ITEMMASTERFILE;
}
