import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { APP_REPORTVIEW, PropertyType } from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-sales-income-report-viewer-component",
  templateUrl: "./sales-income-report-viewer-component.component.html",
  styleUrls: ["./sales-income-report-viewer-component.component.less"],
})
export class SalesIncomeReportViewerComponentComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.SALESINCOMEREPORT;

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      Select 
            DISTINCT 
            AttendingPhysician_ID_Employee ID, 
            AttendingPhysician_Name_Employee Name,
            Name_Position Position
      FROM [vzPaymentTransactionSummaryReport]
      WHERE 
            ID_Company = ${this.currentUser.ID_Company}
    `),
  };

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;

    this.CurrentObject.AttendingPhysician_Name_Employee = event.Name;
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onRemoveSelectedItem(
    event: any
  ) {
    this.CurrentObject.AttendingPhysician_Name_Employee = null;
  }

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;
    var dateStart;
    var dateEnd;

    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index < 0) {
      index = this.globalFx.findIndexByKeyValue(
        formFilters,
        "dataField",
        "Date_BillingInvoice"
      );
      if (index < 0) {
        formFilters.push({
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [new Date(), new Date()],
        });
      }
    }

    var caption = "";
    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = formFilters[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }

        caption += `Bill Date From ${moment(dateStart).format(
          "MM/DD/YYYY"
        )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
      }
    }

    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_PaymentTransaction"
    );
    if (index > -1) {
      var formFilter = formFilters[index];
      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }

        caption += `Last Paid Date From ${moment(dateStart).format(
          "MM/DD/YYYY"
        )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
      }
    }

    if (
      this.CurrentObject["AttendingPhysician_Name_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_Name_Employee"] != null
    ) {
      caption += `Attending Vet: ${this.CurrentObject.AttendingPhysician_Name_Employee}<br>`;
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    formFilters.push({
      alias: "schema2",
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    if (
      this.CurrentObject["Date_BillingInvoice"] != undefined &&
      this.CurrentObject["Date_BillingInvoice"] != null
    ) {
      var values = this.CurrentObject["Date_BillingInvoice"];

      if (this.CurrentObject["Date_BillingInvoice"].length == 1) {
        formFilters.push({
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: moment(values[0]).format("YYYY-MM-DD"),
        });
        formFilters.push({
          alias: "schema2",
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: moment(values[0]).format("YYYY-MM-DD"),
        });
      } else if (this.CurrentObject["Date_BillingInvoice"].length == 2) {
        formFilters.push({
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            moment(values[0]).format("YYYY-MM-DD"),
            moment(values[1]).format("YYYY-MM-DD"),
          ],
        });

        formFilters.push({
          alias: "schema2",
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            moment(values[0]).format("YYYY-MM-DD"),
            moment(values[1]).format("YYYY-MM-DD"),
          ],
        });
      }
    }

    if (
      this.CurrentObject["Name_Client"] != undefined &&
      this.CurrentObject["Name_Client"] != null
    ) {
      var value = this.CurrentObject["Name_Client"];

      formFilters.push({
        dataField: "Name_Client",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });
    }

    if (
      this.CurrentObject["AttendingPhysician_ID_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_ID_Employee"] != null
    ) {
      var value = this.CurrentObject["AttendingPhysician_ID_Employee"];

      formFilters.push({
        dataField: "AttendingPhysician_ID_Employee",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    if (
      this.CurrentObject["Name_Item"] != undefined &&
      this.CurrentObject["Name_Item"] != null
    ) {
      var value = this.CurrentObject["Name_Item"];

      formFilters.push({
        dataField: "Name_Item",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });

      formFilters.push({
        alias: "schema2",
        dataField: "ItemString",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject["Name_Item"],
      });
    }

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();
    filterFormValues = this.getFilterValues(filterFormValues);

    var index = -1;

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = filterFormValues[index];
      var dateStart;
      var dateEnd;

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    return validations;
  }

  getFilterString(): string {
    var filterString = "";

    return filterString;
  }
}
