import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import {
  APP_MODEL,
  APP_REPORTVIEW,
  Employee,
  PropertyType,
} from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-paymenttransaction-detail-report-viewer",
  templateUrl: "./paymenttransaction-detail-report-viewer.component.html",
  styleUrls: ["./paymenttransaction-detail-report-viewer.component.less"],
})
export class PaymenttransactionDetailReportViewerComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.PAYMENTTRANSACTIONDETAILREPORT;

  isHideDate_PaymentTransaction: boolean = false;

  onInit(): void {
    this.isHideDate_PaymentTransaction =
      this.currentUser.ID_Company == 44 && this.currentUser.ID_User == 274;
  }

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      Select 
            DISTINCT 
            AttendingPhysician_ID_Employee ID, 
            AttendingPhysician_Name_Employee Name,
            Name_Position Position
      FROM [vzPaymentTransactionSummaryReport]
      WHERE 
            ID_Company = ${this.currentUser.ID_Company}
    `),
  };

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;

    this.CurrentObject.AttendingPhysician_Name_Employee = event.Name;
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onRemoveSelectedItem(
    event: any
  ) {
    this.CurrentObject.AttendingPhysician_Name_Employee = null;
  }
  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;

    if (!isNullOrUndefined(this.CurrentObject["ID_PaymentMethod"])) {
      formFilters.push({
        dataField: "ID_PaymentMethod",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject["ID_PaymentMethod"],
      });
    }

    if (formFilters.length == 0) {
      index = this.globalFx.findIndexByKeyValue(
        formFilters,
        "dataField",
        "Date_PaymentTransaction"
      );
      if (index < 0) {
        formFilters.push({
          dataField: "Date_PaymentTransaction",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD")],
        });
      }
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    /* Header_CustomCaption */
    var captionHeader = "";

    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_PaymentTransaction"
    );
    if (index > -1) {
      var formFilter = formFilters[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      captionHeader = `From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")} `;
    }

    if (
      this.CurrentObject["AttendingPhysician_Name_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_Name_Employee"] != null
    ) {
      captionHeader += `<br>Attending Vet: ${this.CurrentObject.AttendingPhysician_Name_Employee} `;
    }

    if (captionHeader.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: captionHeader,
      });
    }

    return formFilters;
  }

  ID_PaymentMethod_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tPaymentMethod"),
  };

  async getCustomValidation(): Promise<any[]> {
    var validations = [];
    var filterFormValues = this.appForm.getFilterFormValue();
    var index = -1;
    var dateStart;
    var dateEnd;

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date_PaymentTransaction"
    );
    if (index > -1) {
      var formFilter = filterFormValues[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    return validations;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    if (
      this.CurrentObject["AttendingPhysician_ID_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_ID_Employee"] != null
    ) {
      var value = this.CurrentObject["AttendingPhysician_ID_Employee"];

      formFilters.push({
        dataField: "AttendingPhysician_ID_Employee",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    return formFilters;
  }
}
