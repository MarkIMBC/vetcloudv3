import {
  FilingStatusEnum,
  ItemTypeEnum,
} from "./../../../utils/controls/class_helper";
import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import {
  APP_REPORTVIEW,
  PropertyType,
  ItemType,
} from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";

@Component({
  selector: "possummary-report-viewer",
  templateUrl: "./possummary-report-viewer.component.html",
  styleUrls: ["./possummary-report-viewer.component.less"],
})
export class POSSummaryReportViewerComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.POSSUMMARY;

  ID_ItemType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
        SELECT 
          0     ID, 
          'All' Name 
        UNION ALL
        SELECT 
          ID, 
          Name 
        FROM tItemType 
        WHERE ID IN (
          ${ItemTypeEnum.Inventoriable},
          ${ItemTypeEnum.Service}
        )
      /*encryptsqlend*/
    `),
  };

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    formFilters.push({
      alias: "schema1",
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    var index = -1;
    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date"
    );
    if (index < 0) {
      formFilters.push({
        alias: "schema1",
        dataField: "Date",
        filterCriteriaType: FilterCriteriaType.Between,
        propertyType: PropertyTypeEnum.String,
        value: [moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD")],
      });
    }

    /* Header_CustomCaption */
    index = this.globalFx.findIndexByKeyValue(formFilters, "dataField", "Date");
    if (index > -1) {
      var dateStart;
      var dateEnd;
      var caption;

      var formFilter = formFilters[index];
      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
        caption = `From ${moment(dateStart).format("MM/DD/YYYY")} To ${moment(
          dateEnd
        ).format("MM/DD/YYYY")} `;
      }
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();

    filterFormValues.forEach((filterFormValue) => {
      if (filterFormValue.dataField == "Date_QPARAM") {
        if (filterFormValue.filterCriteriaType == FilterCriteriaType.Between) {
          var dateStart;
          var dateEnd;

          var formFilter = filterFormValue;
          if (formFilter.value) {
            if (Array.isArray(formFilter.value)) {
              dateStart = new Date(formFilter.value[0]);
              dateEnd = new Date(formFilter.value[1]);
            } else {
              dateStart = new Date(formFilter.value);
              dateEnd = dateStart;
            }
          }

          if (!moment(dateStart).isValid()) {
            validations.push({
              message: "Invalid Date Start.",
            });
          }

          if (!moment(dateEnd).isValid()) {
            validations.push({
              message: "Invalid Date End.",
            });
          }

          if (validations.length == 0) {
            if (dateStart > dateEnd) {
              validations.push({
                message: "Invalid Date Range.",
              });
            }
          }
        }
      }
    });

    return validations;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    formFilters = [];

    if (this.CurrentObject["Date_QPARAM"]) {
      var dateStart = this.CurrentObject.Date_QPARAM[0];
      var dateEnd = this.CurrentObject.Date_QPARAM[1];

      formFilters.push({
        alias: "schema1",
        dataField: "Date",
        filterCriteriaType: FilterCriteriaType.Between,
        propertyType: PropertyTypeEnum.Date,
        value: [dateStart, dateEnd],
      });
    }

    if (
      this.CurrentObject["ID_ItemType"] != undefined &&
      this.CurrentObject["ID_ItemType"] != null &&
      this.CurrentObject["ID_ItemType"] != 0
    ) {
      var value = this.CurrentObject["ID_ItemType"];

      formFilters.push({
        dataField: "item.ID_ItemType_QPARAM",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: value,
      });
    }

    if (
      this.CurrentObject["Name_Item"] != undefined &&
      this.CurrentObject["Name_Item"] != null &&
      this.CurrentObject["Name_Item"] != ""
    ) {
      var value = this.CurrentObject["Name_Item"];

      formFilters.push({
        dataField: "item.Name_QPARAM",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });
    }

    if (
      this.CurrentObject["AttendingPhysician_Name_Employee"] != undefined &&
      this.CurrentObject["AttendingPhysician_Name_Employee"] != null &&
      this.CurrentObject["AttendingPhysician_Name_Employee"] != ""
    ) {
      var value = this.CurrentObject["AttendingPhysician_Name_Employee"];

      formFilters.push({
        dataField: "_employee.Name_QPARAM",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: value,
      });
    }

    formFilters.push({
      alias: "schema1",
      dataField: "ID_FilingStatus",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: FilingStatusEnum.Approved,
    });

    var index = -1;
    var caption = "";

    index = this.globalFx.findIndexByKeyValue(formFilters, "dataField", "Date");
    if (index > -1) {
      var formFilter = formFilters[index];
       dateStart = new Date(formFilter.value[0]);
       dateEnd = new Date(formFilter.value[1]);

      caption += `Bill Date From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return formFilters;
  }
}
