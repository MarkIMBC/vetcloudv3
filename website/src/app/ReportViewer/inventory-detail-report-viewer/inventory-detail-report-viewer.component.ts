import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { APP_REPORTVIEW, PropertyType } from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import * as moment from "moment";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";

@Component({
  selector: "inventory-detail-report-viewer",
  templateUrl: "./inventory-detail-report-viewer.component.html",
  styleUrls: ["./inventory-detail-report-viewer.component.less"],
})
export class InventoryDetailReportViewerComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.INVENTORYDETAILREPORT;

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;
    var dateStart;
    var dateEnd;

    /* Header_CustomCaption */
    var caption = "";
    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "DateTrail"
    );
    if (index > -1) {
      var formFilter = formFilters[index];

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      caption += `Date Trail From ${moment(dateStart).format(
        "MM/DD/YYYY"
      )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }
}
