import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { APP_REPORTVIEW, PropertyType } from "./../../../bo/APP_MODELS";
import { BaseReportComponent } from "./../BaseReportViewer";
import { Component, OnInit } from "@angular/core";
import { extend } from "jquery";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import * as moment from "moment";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-billing-invoice-aging-report",
  templateUrl: "./billing-invoice-aging-report.component.html",
  styleUrls: ["./billing-invoice-aging-report.component.less"],
})
export class BillingInvoiceAgingReportComponent extends BaseReportComponent {
  ID_Report: string = APP_REPORTVIEW.BILLINGINVOICEAGINGREPORT;

  protected getDefaultFormFilter(formFilters): IFilterFormValue[] {
    var index = -1;

    /* Header_CustomCaption */
    var caption = "";
    index = this.globalFx.findIndexByKeyValue(
      formFilters,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = formFilters[index];

      var dateStart;
      var dateEnd;

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }

        caption += `Bill Date From ${moment(dateStart).format(
          "MM/DD/YYYY"
        )} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
      }
    }

    if (caption.length > 0) {
      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();
    var index = -1;

    index = this.globalFx.findIndexByKeyValue(
      filterFormValues,
      "dataField",
      "Date_BillingInvoice"
    );
    if (index > -1) {
      var formFilter = filterFormValues[index];

      var dateStart;
      var dateEnd;

      if (formFilter.value) {
        if (Array.isArray(formFilter.value)) {
          dateStart = new Date(formFilter.value[0]);
          dateEnd = new Date(formFilter.value[1]);
        } else {
          dateStart = new Date(formFilter.value);
          dateEnd = dateStart;
        }
      }

      var validate = this.validateService.validateDate(dateStart, dateEnd);

      if (validate.isValid != true) {
        validations.push({
          message: validate.message,
        });
      }
    }

    return validations;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    return formFilters;
  }
}
