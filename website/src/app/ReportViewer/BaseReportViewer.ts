import { ValidationService } from "src/utils/service/validation.service";
import {
  APP_MODEL,
  APP_REPORTVIEW,
  DetailView_Detail,
  Report,
} from "src/bo/APP_MODELS";
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  AppFormComponent,
  IFilterFormValue,
} from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IMenuItem, PropertyTypeEnum } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { LoaderService } from "../AppServices/LoaderInterceptor";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "../AppServices/UserAuthentication.service";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import * as moment from "moment";
@Component({
  template: "",
})
export abstract class BaseReportComponent implements OnInit {
  @ViewChild("form")
  form: ElementRef;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  ID_Report: string = "";

  currentUser: TokenSessionFields;

  CurrentReport: Report = null;
  CurrentObject: any = {};

  showFilterBox: boolean = false;

  defaultFormFilterValue: IFilterFormValue[] = [];

  menuItems: IMenuItem[] = [
    {
      icon: "pi pi-fw pi-file-o green-text",
      label: "Refresh",
      command: () => {
        this.refreshView();
      },
    },
    {
      label: "Export",
      icon: "pi pi-fw pi-folder blue-text",
      items: [
        {
          label: "PDF",
          icon: "pi pi-file-pdf",
          command: async () => {
            this.refreshView("Pdf");
          },
        },
        {
          label: "Excel",
          icon: "pi pi-file-excel",
          command: async () => {
            this.refreshView("Excel");
          },
        },
      ],
    },
  ];

  isLoading: boolean = false;

  constructor(
    private el: ElementRef,
    public ds: DataService,
    private changeDetector: ChangeDetectorRef,
    private msgBox: MessageBoxService,
    public globalFx: GlobalfxService,
    public cs: CrypterService,
    public validateService: ValidationService,
    private userAuth: UserAuthenticationService,
    private loaderService: LoaderService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();

    this.loaderService.isLoading.subscribe((r) => {
      this.isLoading = r;
    });
  }

  async ngOnInit(): Promise<void> {
    var rpt = (await this.ds.loadObject(
      APP_MODEL.REPORT,
      this.ID_Report
    )) as Report;

    this.CurrentReport = rpt;

    await this.initFilterFields();

    setTimeout(() => {
      this.refreshView();
    }, 500);

    this.onInit();
  }

  initFilterFields() {
    if (this.CurrentReport.Report_Filters == null)
      this.CurrentReport.Report_Filters = [];

    this.CurrentReport.Report_Filters.forEach((f) => {
      if (f.IsActive == false) return;

      this.appForm.details.push({
        ID_PropertyType: f.ID_PropertyType,
        Name: f.Name,
      });
    });
  }

  onInit(): void {}

  onLoad(event): void {}

  refreshView(method: string = "Viewer"): Promise<void> {
    this.loaderService.isLoading.next(true);
    var form = $(this.form.nativeElement);

    form.attr(
      "action",
      DataService.API_URL + `Report/${method}/${this.ID_Report}`
    );

    var formFilterInput = null;
    var formFilters: IFilterFormValue[] = [];
    var defaultFilterValue: IFilterFormValue[] = [];

    formFilters = this.appForm.getFilterFormValue();
    formFilters = this.getFilterValues(formFilters);
    formFilters = this.getDefaultFormFilter(formFilters);

    formFilters.forEach((formFilters) => {
      try {
        if (
          formFilters.propertyType == PropertyTypeEnum.Date ||
          formFilters.propertyType == PropertyTypeEnum.DateTime
        ) {
          if (Array.isArray(formFilters.value)) {
            if (formFilters.value.length == 1) {
              formFilters.value[0] = moment(formFilters.value[0]).format(
                "YYYY-MM-DD"
              );
            } else {
              formFilters.value[0] = moment(formFilters.value[0]).format(
                "YYYY-MM-DD"
              );
              formFilters.value[1] = moment(formFilters.value[1]).format(
                "YYYY-MM-DD"
              );
            }
          } else {
            var value = formFilters.value;

            formFilters.value = [
              moment(value).format("YYYY-MM-DD"),
              moment(value).format("YYYY-MM-DD"),
            ];
          }
        }
      } catch (error) {
        console.log(formFilters, error);
      }
    });

    if (formFilters.length > 0) {
      formFilterInput = $(
        '<input type="hidden" name="filterValue" value="' +
          btoa(JSON.stringify(formFilters)) +
          '" />'
      );
      form.append(formFilterInput);
    } else {
      formFilterInput = $(
        '<input type="hidden" name="filterValue" value="" />'
      );
      form.append(formFilterInput);
    }

    form.submit();

    if (formFilterInput) formFilterInput.remove();

    setTimeout(() => {
      this.loaderService.isLoading.next(false);
    }, 500);

    return Promise.resolve();
  }

  toggeFilterBox(): void {
    this.showFilterBox = !this.showFilterBox;
  }

  async getCustomValidation(): Promise<any[]> {
    var validations = [];

    return validations;
  }

  async searchFilter(): Promise<void> {
    var validations = [];

    (await this.getCustomValidation()).forEach((validation) => {
      validations.push(validation);
    });

    if (validations.length > 0) {
      this.msgBox.showValidationsBox(validations);
      return;
    }

    this.refreshView();
  }

  async setCustomHeader(): Promise<any[]> {
    var validations = [];

    return validations;
  }

  async clearFilter(): Promise<void> {
    var list = this.appForm.getFilterFormValue().length;

    if (
      (await this.msgBox.confirm(
        "Are you sure you want to clear?",
        this.CurrentReport.Name,
        "Yes",
        "No"
      )) == true
    ) {
      this.CurrentObject = {};

      this.appForm.clearFilter();

      if (list == 0) return;

      setTimeout(() => {
        this.refreshView();
      }, 500);
    }
  }

  protected getDefaultFormFilter(formFilters?): IFilterFormValue[] {
    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  protected getFilterValues(formFilters?): IFilterFormValue[] {
    return formFilters;
  }

  filterOnChange(event) {
    this.searchFilter();
  }

  filterOnKeyUp(event) {
    if (event.keyCode == 13) this.searchFilter();
  }

  appForm_onAfterContentInit(e: any) {}
}
