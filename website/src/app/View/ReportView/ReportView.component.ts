import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { DetailView_Detail, LabelLocaltionEnum, PropertyTypeEnum } from './../../../utils/controls/class_helper';
import { DetailView } from './../../../bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { APP_MODEL, Report } from 'src/bo/APP_MODELS';
import { IMenuItem, ViewTypeEnum } from 'src/utils/controls/class_helper';
import { ElementRef, ViewChild, Injector, NgModuleRef, ChangeDetectorRef, ViewContainerRef } from '@angular/core';
import { Guid } from 'guid-typescript';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/utils/service/data.service';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { REGISTRY_VC_MODEL } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { DynamicTemplateService, DetailViewHolder } from 'src/app/AppServices/dynamic-temaplate.svc';
import { ListViewModalService } from '../ListView/ListView.service';
import { DetailViewDialogService } from '../DetailViewDialog/DetailViewDialog.component';
import { FormHelperDialogService, generateFormHtmlText } from '../FormHelperView/FormHelperView.component';
import { HttpClient } from '@angular/common/http';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { LoaderService } from 'src/app/AppServices/LoaderInterceptor';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { FilterTypeEnum } from 'src/utils/controls/appDataTable/appDataTable.component';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-report-view',
  templateUrl: './ReportView.component.html',
  styleUrls: ['./ReportView.component.less']
})
export class ReportViewComponent implements OnInit {


  constructor(private _injector: Injector, private _m: NgModuleRef<any>, private ds: DataService,
    private route: ActivatedRoute, private tpl: DynamicTemplateService,
    private changeDetector: ChangeDetectorRef, private msgBox: MessageBoxService,
    private el: ElementRef,
    private cs: CrypterService,
    private lvModalSvc: ListViewModalService,
    private dvModalSvc: DetailViewDialogService,
    private dvFormSvc: FormHelperDialogService,
    private userAuth: UserAuthenticationService,
    private loaderService: LoaderService,
    private globalFx: GlobalfxService,
  ) { }

  isExpanded = false;
  state = 'collapsed';

  ID_Report: string = null;

  @ViewChild('form')
  form: ElementRef;
  
  @ViewChild('reportView')
  reportView:ElementRef;

  dvViewHolder: DetailViewHolder = null;

    

  menuItems: IMenuItem[] = [{
    icon: "mdi mdi-refresh green-text",
    label: "Refresh",
    command: () => { 
      this.refreshView();
    }
  }];

  showFilterBox = false;

  title: string = '';

  vcModels: VC_BaseView[] = [];

  defaultFormFilterValue: IFilterFormValue[] = [];

  filterControls: DetailView_Detail[] = [];

  CurrentObject: any = {};

  CurrentReport:Report = null;

  toggeFilterBox(): void {
    this.showFilterBox = !this.showFilterBox;
  }

  searchFilter() : void {
    var validations = this.dvViewHolder.appForm.getValidations();
    if (validations.length > 0) {
      this.msgBox.showValidationsBox(validations);
      return;
    };
    this.refreshView();
  }

  async clearFilter(): Promise<void> {

    var list = this.dvViewHolder.appForm.getFilterFormValue().length;
    if (await this.msgBox.confirm("Are you sure you want to clear?", this.CurrentReport.Name, "Yes", "No") == true) {
      this.CurrentObject = {};
      this.dvViewHolder.CurrentObject = {};
      this.dvViewHolder.appForm.clearFilter();
      if ( list == 0 ) return;
      setTimeout(() => {
        this.refreshView();
      }, 500);
    }
  }

  @ViewChild('vc', { read: ViewContainerRef })
  _container: ViewContainerRef;

  ngOnInit() {
  }

  ngAfterViewInit(): void {

    this.route.paramMap.subscribe(async (p: any) => {

      this.loadOptionsParams(p);

      this.ID_Report = atob(p.params.Oid_Report);

      var rpt = await this.ds.loadObject(APP_MODEL.REPORT, this.ID_Report) as Report;
   
      this.CurrentReport = rpt;
      var vcClasses = REGISTRY_VC_MODEL.get(rpt.Name);
      this.filterControls = [];
      this._container.clear();
      if (vcClasses) {
        vcClasses.forEach(c => {
          var newInstance = Object.create(c.prototype) as VC_BaseView;
          newInstance.ViewType = ViewTypeEnum.DetailView;
          newInstance.msgBox = this.msgBox;
          //console.log(c.name);
          newInstance.ClassName = c.name;
          //newInstance.ViewParams = this.dvParams;
          newInstance.dataService = this.ds;
          newInstance.userAuth = this.userAuth;
          newInstance.dvFormSvc = this.dvFormSvc;
          newInstance.lvModal = this.lvModalSvc;
          newInstance.dvModal = this.dvModalSvc;
          this.vcModels.push(newInstance);
        });
      };

      if (rpt.Report_Filters) { 
        rpt.Report_Filters.forEach(f => { 
          if ( f.IsActive == false ) return;
          var det:DetailView_Detail = {
            Name : f.Name,
            Caption: f.Caption,
            ID_ControlType : f.ID_ControlType,
            ID_PropertyType : f.ID_PropertyType,
          };
          this.filterControls.push(det);
        });
      }

      if (this.vcModels.length > 0) {
        this.vcModels.forEach(async d => {
          await d.onFilterControls_Initialized(this.filterControls);
        })
      };
      this.filterControls.forEach(d => {
        d.ColSpan = 12;
        d.ID_LabelLocation = LabelLocaltionEnum.Top;
      });
      var cmpHolder = this as any;
      cmpHolder.ControlOption = {};
      if (this.filterControls?.length > 0) {
        //this.showFilterBox = true;
        var html = generateFormHtmlText({
          Oid: this.ID_Report,
          Details: this.filterControls,
        }, cmpHolder, true);
        var cmpRef = await this.tpl.createAndCompileComponents(html, this._injector, this._m);//.then(cmpRef => {
        var instance = cmpRef.instance as DetailViewHolder;
        instance.CurrentObject = this.CurrentObject;
        this.dvViewHolder = instance;
        var OptionKeys = Object.keys(cmpHolder.ControlOption);
        OptionKeys.forEach(key => {
          instance[key] = cmpHolder.ControlOption[key];
        });
        this._container.insert(cmpRef.hostView);
        instance.onView_Initialzied = () => {
          
          instance.appForm.details = this.filterControls;
          instance.appForm.controls.forEach(c => {

            c.onEnterKey.subscribe((evt) => {
               this.refreshView();
            });
          });
        }
      }
     $(this.reportView.nativeElement).on('onload',()=>{
     
     });
      setTimeout(() => {
        this.refreshView();
      }, 500);
    })
    
  }

  loadOptionsParams(p: any){

    var options = {};
    var defaultfilterjson = atob(p.params.defaultfilterjson);
    var optionsJSON = atob(p.params.options);

    if(defaultfilterjson != 'null'){

      this.defaultFormFilterValue = JSON.parse(defaultfilterjson);
    }

    if(optionsJSON != 'null'){

      options = JSON.parse(optionsJSON);

      console.log(options);

      if(!isNullOrUndefined(options["backLink"])){

        var link = options["backLink"];

        this.menuItems.unshift({
         
          label: "Back",
          icon: "pi pi-arrow-left",
          command: (event: any) => {

            var routeLink = [];
            var prevConfig;
        
            if (!isNullOrUndefined(options["prevConfig"])) {
              prevConfig = options["prevConfig"];
            }
        
            if (isNullOrUndefined(options["BackRouteLink"])) return;
        
            routeLink = options["BackRouteLink"];
            this.globalFx.customNavigate(routeLink, prevConfig);
          } 
        });
      }
    }
  }

  onLoad(event): void {
  }

  refreshView(): Promise<void> {

    this.loaderService.isLoading.next(true);
    var form = $(this.form.nativeElement);
    form.attr("action", DataService.API_URL + `Report/Viewer/${this.ID_Report}`);

    var formFilterInput = null;
    var formFilters: IFilterFormValue[] = [];

    if(this.defaultFormFilterValue.length != 0){

      formFilters = this.defaultFormFilterValue;
    }else{

      formFilters = this.dvViewHolder.appForm.getFilterFormValue();
    }
    
    if (formFilters.length > 0) {
      formFilterInput = $('<input type="hidden" name="filterValue" value="' + btoa(JSON.stringify(formFilters)) + '" />');
      form.append(formFilterInput);
    } else {
      formFilterInput = $('<input type="hidden" name="filterValue" value="" />');
      form.append(formFilterInput);
    }
    
    form.submit();

    if (formFilterInput) formFilterInput.remove();

    setTimeout(() => {

      this.loaderService.isLoading.next(false);
    }, 500);
    return Promise.resolve();
  }

}



