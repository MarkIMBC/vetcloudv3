import { Position } from '../../bo/APP_MODELS';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'dialog-box',
  templateUrl: './dialog-box.html',
  styleUrls: ['./dialog-box.less']
})
export class DialogBoxComponent implements OnInit {

  @ViewChild('mymodal')
  modalComponent: NgbModal

  @Input()
  childTemplate: TemplateRef<any>;

  title: string = "";

  modal: any;

  size: string = "lg";

  captionPositiveButton: string = "OK";
  captionNegativeButton: string = "Cancel";

  closeOnPositiveButton: boolean = true;
  closeOnNegativeButton: boolean = true;

  IsShowBtnPositive: boolean = true;
  IsShowBtnNegative: boolean = true;

  @Output() onLoad = new EventEmitter();
  @Output() onClosed = new EventEmitter();
  @Output() onPositiveButtonClick = new EventEmitter();
  @Output() onNegativeButtonClick = new EventEmitter();

  constructor(protected modalService: NgbModal) { }

  ngOnInit(): void {

  }

  async open(): Promise<any[]> {

    var promise = new Promise<any[]>(async (resolve, reject) => {

      var cmp = this.modalService.open(this.modalComponent, {
        ariaLabelledBy: 'modal-basic-title',
        backdrop: "static",
        size: this.size,
        centered: true,
      }).result.then((result) => {

        this.onClosed.emit();
        resolve(null);
      }, () => {

        
        this.onClosed.emit();
        reject(null);
      });

      await this.onLoad.emit();
      
      this.formatDialogBox(cmp);
    });

    return promise
  }

  private formatDialogBox(cmp) {

    var parent = $(((cmp as any)._windowCmptRef as ElementRef));

    setTimeout(() => {

      var modalDialog = parent.find('.modal-dialog');
      var dialogBody = $('.modal-body')

      modalDialog.css("width", `100%!important`);

      setTimeout(() => {

        parent.css("cssText", "opacity:1!important");
      });
    });
  }

  btnPositive_OnClick(modal){

    this.onPositiveButtonClick.emit(modal);

    if(this.closeOnPositiveButton == true) modal.close();
  }

  btnNegative_OnClick(modal){

    this.onNegativeButtonClick.emit(modal);
    if(this.closeOnNegativeButton == true) modal.close();
  }

  
  close(){

    this.modalComponent.dismissAll("asdasd");
  }

}
