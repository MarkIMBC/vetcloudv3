import { CrypterService } from 'src/utils/service/Crypter.service';
import { IMenuItem } from './../../utils/controls/class_helper';
import { SafeStyle } from '@angular/platform-browser';
import { APP_DETAILVIEW, Appointment } from './../../bo/APP_MODELS';
import { DetailViewDialogService } from './../View/DetailViewDialog/DetailViewDialog.component';
import { DataService } from './../../utils/service/data.service';
import { CalendarOptions, EventApi, DateSelectArg, EventClickArg, EventInput, Calendar, FullCalendarComponent, DatesSetArg } from '@fullcalendar/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CalendarInfo } from 'src/bo/VC/Schedule/VC_Appointment';
import * as TS from 'linq-typescript';
import * as moment from 'moment';
import { style } from '@angular/animations';

@Component({
  selector: 'app-CalendarView',
  templateUrl: './CalendarView.component.html',
  styleUrls: ['./CalendarView.component.less']
})
export class CalendarViewComponent implements OnInit {

  constructor(private ds:DataService, private dvModalSvc: DetailViewDialogService,private msgBox: MessageBoxService,private encryptSvc:CrypterService) { }

  @ViewChild('fullCalendar')
  fullCalendar: FullCalendarComponent;

  appoinments: Appointment[] = [];
  dateSetArgs: DatesSetArg = null;

  calendarVisible = true;
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,listMonth'
    },
    initialView: 'dayGridMonth',
    initialEvents: [], // alternatively, use the `events` setting to fetch from a feed
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    datesSet: (p:DatesSetArg) => {
      this.dateSetArgs = p;
      this.refreshEvents();
    }
    /* you can update a remote database when these fire:
    eventAdd:
    eventChange:
    eventRemove:
    */
  };
  currentEvents: EventApi[] = [];

  handleCalendarToggle() {
    this.calendarVisible = !this.calendarVisible;
  }

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  menuItems:IMenuItem[] = [
    {
      label: "Refresh",
      icon: "mdi mdi-refresh",
      command: () => { 
        this.refreshEvents();
      }
    }
  ]

  async handleDateSelect(selectInfo: DateSelectArg) {
    if ( await this.msgBox.confirm("Do you to add new appointment?", "Appointment") != true ) return;
    //console.log(selectInfo);
    var cInfo : CalendarInfo = {
      startDate : selectInfo.start,
      endDate: selectInfo.end,
    }
    await this.dvModalSvc.open(APP_DETAILVIEW.APPOINTMENT_DETAILVIEW,"-1", {
       title : "New Appointment", 
       params : cInfo
    });
    this.refreshEvents();
  }

  async handleEventClick(clickInfo: EventClickArg) {
    var id = Number.parseInt(clickInfo.event.id);
    var appointment = this.appoinments.find(d => d.ID == id);
    await this.dvModalSvc.open(APP_DETAILVIEW.APPOINTMENT_DETAILVIEW,appointment.ID + "", {
      title : "New Appointment" ,
      height : 0.5 * window.outerHeight
    });
    this.refreshEvents();
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() : void {
    //this.refreshEvents();
  };

  async refreshEvents() : Promise<void> {
    var dateSetArgs = this.dateSetArgs;
    var appointments = await this.ds.query<Appointment>(this.encryptSvc.encrypt(`
    SELECT 
      *
    FROM tAppointment
    WHERE DateStart >= DATEADD(day, DATEDIFF(day, 0, '${moment(dateSetArgs.start).format("YYYY-MM-DD")}'), 0)
    AND DateEnd < DATEADD(day, 1, DATEDIFF(day, 0, '${moment(dateSetArgs.end).format("YYYY-MM-DD")}'))`));
    const calendarApi = this.fullCalendar.getApi();
    calendarApi.removeAllEvents();
    if ( appointments ) {
      appointments.forEach((a) => {
        calendarApi.addEvent({
          start: a.DateStart,
          end: a.DateEnd,
          id : a.ID + "",
          title: "Sample Title"
        })
      });
    }
    this.appoinments = appointments;
    return Promise.resolve();
  }
  
}



// let eventGuid = 0;
// const TODAY_STR = new Date().toISOString().replace(/T.*$/, ''); // YYYY-MM-DD of today

// export const INITIAL_EVENTS: EventInput[] = [
//   {
//     id: createEventId(),
//     title: 'All-day event',
//     start: TODAY_STR
//   },
//   {
//     id: createEventId(),
//     title: 'Timed event',
//     start: TODAY_STR + 'T12:00:00'
//   }
// ];

// export function createEventId() {
//   return String(eventGuid++);
// }
