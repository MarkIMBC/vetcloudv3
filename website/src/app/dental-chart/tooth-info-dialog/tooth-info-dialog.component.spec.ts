import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothInfoDialogComponent } from './tooth-info-dialog.component';

describe('ToothInfoDialogComponent', () => {
  let component: ToothInfoDialogComponent;
  let fixture: ComponentFixture<ToothInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToothInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
