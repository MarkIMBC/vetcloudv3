import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { APP_REPORTVIEW } from 'src/bo/APP_MODELS';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { Inventory, PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { TokenSessionFields, UserAuthenticationService } from '../AppServices/UserAuthentication.service';
import { ListViewModalService } from '../View/ListView/ListView.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.less']
})
export class InventoryComponent implements OnInit {

  constructor( 
    private ds: DataService, 
    private globalFx: GlobalfxService, 
    private msgBox: MessageBoxService,
    private userAuthSvc: UserAuthenticationService,
    private lvModal: ListViewModalService,
    private route: ActivatedRoute,
    private router: Router,
    private cs: CrypterService,
    private toastr: ToastrService
    ) { } 

  inventoryList: Inventory[] = []
  selInventory: Inventory = null;
  currentUser: TokenSessionFields;

  menuItems: MenuItem[] = [];

  @ViewChild('dt') table: Table;


  private _ID_Report_InventorySummary: number = -1;

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuthSvc.getDecodedToken();

    this.menuItems.push({
      label: 'Refresh',
      icon: 'pi pi-fw pi-refresh green-text',
      command: async () => {

        this.loadInventoryList();
      }
    })

    this.menuItems.push({
      label: 'Report',
      icon: 'pi pi-fw pi-file blue-text',
      items: [
        {
          label: 'Inventory Summ.', 
          icon: 'pi pi-file-o',
          routerLink:`/Main/InventorySummaryReport`
        }
      ]
    });
  }

  async loadInventoryList(){

    var sql = `SELECT * 
               FROM vInventory
               WHERE
                ID_Company = ${this.currentUser.ID_Company} 
               ORDER BY 
                Name_Item`;

    sql = this.cs.encrypt(sql);

    this.inventoryList = await this.ds.query<Inventory>(sql);
  }

  dt_onRowDblClick(inventory: Inventory){

    this.router.navigate([`/Main/InventoryTrail/${inventory.ID_Item}`], {
    });
  }

  ngAfterViewInit() {
  
    this.loadInventoryList();
  }
}
