import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { DataService } from "src/utils/service/data.service";
import { Injectable } from "@angular/core";
import { CrypterService } from "../../utils/service/Crypter.service";
import { isNullOrUndefined } from "util";
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: "root",
})
export class UserAuthenticationService {
  constructor(private ds: DataService, private ck: CrypterService, private globalFx: GlobalfxService) {}

  LogIn(username: string, password: string): Promise<IUserSession> {

    var _ = this;

    if (password) {
      if (password.length > 0) {
        password = this.ck.encrypt(password);
      }
    }

    return new Promise<IUserSession>((res, rej) => {
      this.ds
        .post("Login/Token", {
          Username: username,
          Password: password,
        })
        .then((r: IUserSession) => {
          var str = JSON.stringify(r);
          window.localStorage.setItem("STORTKN", r.Token);
          window.localStorage.setItem("STORDNTL", r.EncryptToken);
          res(r);
        })
        .catch((err) => {
          rej(err);
        });
    });
  }

  async CheckAndLogOutByUserSessionStatus(): Promise<boolean> {

    await this.ds.loadConfig();

    var isActive = true;
    var currentUser: TokenSessionFields = this.getDecodedToken();

    try {
      var obj = await this.ds.execSP(
        "pGetLoggedUserSession",
        {
          ID_UserSession: currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );
  
      if (
        currentUser.ID_User == undefined ||
        currentUser.ID_User == null ||
        obj.IsActive != true
      ) {
        isActive = false;
        await this.LogOut();
      }
  
    } catch (error) {
      isActive = true;
    }
  
    return isActive;
  }
  
  async LogOut(): Promise<IUserSession> {
    
    await this.ds.loadConfig();

    var session = this.getDecodedToken();
    var ID_Session = session.ID_UserSession;

    return new Promise<IUserSession>((res, rej) => {
      this.ds
        .get("Login/LogOut/" + ID_Session)
        .then((r: IUserSession) => {
          window.localStorage.clear();
          window.location.href = './Login';
          res(r);
        })
        .catch((err) => {
          rej(err);
        });
    });
  }

  async CheckSession(): Promise<ICheckSession> {
    var session = this.getDecodedToken();

    var res: ICheckSession = {
      ID: -1,
      ID_Warehouse: -1,
      IsValid: false,
    };
    if (session == null) {
      return Promise.resolve(res);
    }
    //res =  await this.ds.get(`Login/CheckSession`) as ICheckSession;
    return new Promise<ICheckSession>((resolve, reject) => {
      this.ds.get(`Login/CheckSession`).then(
        (d: ICheckSession) => {
          resolve(d);
        },
        (r) => {
          resolve(res);
        }
      );
    });
    //return Promise.resolve(res);
  }

  getCurrentSession(): IUserSession {
    var str = window.localStorage.getItem("STORDNTL");
    if (isNullOrUndefined(str) == true) return null;
    var user = JSON.parse(str) as IUserSession;
    return user;
  }

  getDecodedToken(): TokenSessionFields {
    var str = window.localStorage.getItem("STORDNTL");
    str = this.ck.decrypt(str);

    try {
      var obj: TokenSessionFields = JSON.parse(str);

      obj.ID_UserSession = obj.ID_Session;

      return obj;
    } catch (Error) {
      return null;
    }
  }
}

export class IUserSession {
  ID?: number;
  ID_Employee?: number;
  FirstName?: string;
  LastName?: string;
  ID_UserSession?: number;
  Token?: string;
  EncryptToken?: string;
}

export class TokenSessionFields {
  ID_Company?: number;
  ID_User?: number;
  ID_UserGroup?: number;
  ID_Employee?: number;
  LastName?: string;
  FirstName?: string;
  Name_Employee?: string;
  ID_Position?: number;
  ID_Session?: number;
  ID_UserSession?: number;
  Password?: string;
}

export class ICheckSession {
  ID?: number;
  ID_Warehouse?: number;
  IsValid?: boolean;
}
