import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryTrailComponent } from './inventory-trail.component';

describe('InventoryTrailComponent', () => {
  let component: InventoryTrailComponent;
  let fixture: ComponentFixture<InventoryTrailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryTrailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryTrailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
