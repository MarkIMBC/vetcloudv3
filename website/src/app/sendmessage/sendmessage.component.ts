import { BulkTextMessage } from './../../utils/controls/class_helper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { UserAuthenticationService } from '../AppServices/UserAuthentication.service';
import { FormHelperDialogService } from '../View/FormHelperView/FormHelperView.component';
import { ListViewModalService } from '../View/ListView/ListView.service';
import { BaseCustomDetailView } from '../CustomDetailView/BaseCustomDetailView';
import { MenuItem } from 'primeng/api';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-sendmessage',
  templateUrl: './sendmessage.component.html',
  styleUrls: ['./sendmessage.component.less']
})
export class SendmessageComponent implements OnInit{

  cellphoneNumbers: string;
 
  CurrentObject: BulkTextMessage;
  
  menuItems: MenuItem[] = [];

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService) {

      this.CurrentObject = new BulkTextMessage();

      this.CurrentObject._strCellPhoneNumbers = "";
      this.CurrentObject.Log = "";
      
      var userTOken = this.userAuthSvc.getDecodedToken();
  }

  async ngOnInit(): Promise<void> {

    this.menuItems.push(
    {
      label: 'Send',
      icon: 'pi pi-fw pi-mobile green-text',
      command: async () => {
  
        var isAllSend = true;
        var log = ""

        this.CurrentObject.CellPhoneNumbers = this.CurrentObject._strCellPhoneNumbers.split(",");
        

        if(await this.validateNumbers() != true) return;

        var r = await this.ds.sendtext(this.CurrentObject);

        if(r == null || isNullOrUndefined(r.PhoneNumberStatus)){

          this.toastr.warning(`Error`,"There is an error encounter in sending text message.");
        } else{

          this.CurrentObject.CellPhoneNumbers.forEach(function (number) {

            var status = r.PhoneNumberStatus[number];

            log += `${number.toString()} (${ status ? 'Sent' : 'Sent Failed'})\n`;

            if(status == false) isAllSend = false;
          });
        }

        if(isAllSend){

          this.toastr.success(`Sent Done`,"Message has been sent...");
        }else{

          this.toastr.warning(`Sent Done`,"Not all message has been sent. Please");
        }

        log += `Timestamp: ${(new Date().toLocaleString())}`;

        this.CurrentObject.Log += log + '\n' + '\n';
      }
    });
  }

  private  validateNumbers(): boolean{

    var isValid = true;
    var invalidNumbers = [];
    var message = "";
    var log = "";

    this.CurrentObject.CellPhoneNumbers.forEach(function (number) {

      number.replace(" ", "");

      if(isNaN(Number(number))){

        invalidNumbers.push(number);
      }
    });

    if(this.CurrentObject.CellPhoneNumbers.length == 0 && isValid){

      isValid = false;
      this.toastr.warning(`Validation`, "Cellphone Number is requried.");
    }

    if(invalidNumbers.length > 0 && isValid){

      isValid = false;

      invalidNumbers.forEach(function (number) {

        log += `${number} is not a number. \n`;
      });

      this.toastr.warning(`Validation`, "There are numbers are not valid.");
      log += `Timestamp: ${(new Date().toLocaleString())}`;
      
      this.CurrentObject.Log += log + '\n' + '\n';
    }

    return isValid
  }
}
