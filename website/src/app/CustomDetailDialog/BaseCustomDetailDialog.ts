import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { MenuItem } from 'primeng/api';
import { isNull, isNullOrUndefined } from 'util';
import { Model } from 'src/bo/APP_MODELS';

export class BaseCustomDetailDialog implements OnInit {

  currentModelID: string = "";

  ModelDisplayName: string = '';

  @ViewChild('appForm')
  appForm: AppFormComponent;

  @ViewChild('mymodal')
  modalComponent: NgbModal

  modal: any;

  menuItems: MenuItem[] = [];

  model: Model;
  currentUser: TokenSessionFields;

  CurrentObject: any = {};
  PreviousObject: any = {};

  private _ID_CurrentObject: number

  constructor(protected modalService: NgbModal, protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService) {


    this.msgBox.setToastTimeOut(5000);
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  async ngOnInit() {

    await this.loadCurrentModel();

    this.menuItems.push(this.menuSave);
  }

  protected async _open<T>(): Promise<T[]> {

    var promise = new Promise<T[]>((resolve, reject) => {

      var cmp = this.modalService.open(this.modalComponent, {
        ariaLabelledBy: 'modal-basic-title',
        backdrop: "static",
        size: "lg",
        centered: true,
      }).result.then((result) => {

        resolve(null);
      }, () => {

        reject(null);
      });

      this.formatDialogBox(cmp);
    });

    return promise
  }

  private async loadCurrentModel() {

    var obj = await this.ds.execSP("pGetModel", {
      Oid: this.currentModelID,
    }, {
      isReturnObject: true
    })

    this.model = obj;

    this.ModelDisplayName = isNull(this.model.DisplayName) ? this.model.Name : this.model.DisplayName;
  }

  protected async loadRecord(option) {

    var obj = await this.ds.execSP(`pGet${this.model.Name}`, option, {
      isReturnObject: true
    })

    this.CurrentObject = obj;
    this.PreviousObject = this.globalFx.cloneObject(obj);
  }

  private formatDialogBox(cmp) {

    var parent = $(((cmp as any)._windowCmptRef as ElementRef));

    setTimeout(() => {

      var modalDialog = parent.find('.modal-dialog');
      var dialogBody = $('.modal-body')

      modalDialog.css("width", `100%!important`);

      setTimeout(() => {

        parent.css("cssText", "opacity:1!important");
      });
    });
  }

  protected menuSave: MenuItem = {
    label: 'Save',
    icon: 'pi pi-fw pi-save green-text',
    command: async () => {

      var isvalid = true;

      isvalid = await this.validateRecord();
      if (!isvalid) return;

      var result = await this.msgBox.confirm(
        "Do you want to save changes?",
        'Saving',
        "Yes", "No");

      await this.save();

      this.loadRecord({
        ID: this._ID_CurrentObject
      });
      this.msgBox.success("Saved Successfully.", this.model.Caption);
    }
  }

  public async validateRecord() {

    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    if (!isNullOrUndefined(this.appForm)) {

      validationsAppForm = await this.getFormValidations();
    }

    CustomValidations.forEach((customValidation) => {

      validationsAppForm.push(customValidation);
    });

    if (validationsAppForm.length > 0) {

      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  private async getFormValidations(): Promise<IFormValidation[]> {

    var validations = this.appForm.getValidations();
    return Promise.resolve(validations);
  };

  protected async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  };

  private async save() {

    if ('ID_Company' in this.CurrentObject && this.CurrentObject.ID < 1) {

      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    var r = await this.ds.saveObject(this.model.Oid, this.CurrentObject, this.PreviousObject, []);
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);
    this._ID_CurrentObject = ID_CurrentObject;

    this.onAfterSaved();
  }

  private async _loadCurrentModel() {

    var obj = await this.ds.execSP("pGetModel", {
      Oid: this.currentModelID,
    }, {
      isReturnObject: true
    })

    this.model = obj;

    this.ModelDisplayName = isNull(this.model.DisplayName) ? this.model.Name : this.model.DisplayName;
  }

  protected async btnSave_onChanged(modal) {

    var isvalid = true;

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    var result = await this.msgBox.confirm(
      "Do you want to save changes?",
      'Saving',
      "Yes", "No");

    if(result !== true) return;

    await this.save();

    this.loadRecord({
      ID: this._ID_CurrentObject
    });

    this.msgBox.success("Saved Successfully.", this.model.Caption);

    modal.close();
  }

  protected btnCancel_onChanged(modal) {

    modal.close();

  }
  protected onAfterSaved() {

  }
  
  ngAfterViewInit() {
    

    this.OnLoad();
  }

  OnLoad(){

    
  }

}


/* Template

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { resolve } from 'dns';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'appointment-schedule-dialog',
  templateUrl: './appointment-schedule-dialog.component.html',
  styleUrls: ['./appointment-schedule-dialog.component.less']
})
export class AppointmentScheduleDialogComponent implements OnInit {

  @ViewChild('mymodal')
  modalComponent: NgbModal

  constructor(private modalService: NgbModal, protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService) { }

  async ngOnInit() {

  }

  async open<T>(): Promise<T[]> {

    var promise = new Promise<T[]>((resolve, reject) => {

      var cmp = this.modalService.open(this.modalComponent, {
        ariaLabelledBy: 'modal-basic-title',
        backdrop: "static",
        size: "lg",
        centered: true,
      }).result.then((result) => {



      }, () => {



      });

      this.formatDialogBox(cmp);
    });

    return promise
  }

  private formatDialogBox(cmp) {

    var parent = $(((cmp as any)._windowCmptRef as ElementRef));

    setTimeout(() => {

      var modalDialog = parent.find('.modal-dialog');
      var dialogBody = $('.modal-body')

      modalDialog.css("width", `100%!important`);

      setTimeout(() => {

        parent.css("cssText", "opacity:1!important");
      });
    });
  }
}

*/