import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { UserAuthenticationService } from './../AppServices/UserAuthentication.service';
import { Component, OnInit } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { HttpErrorResponse } from '@angular/common/http';
import { HTTPStatusCode } from '../AppServices/LoaderInterceptor';
import { ActivatedRoute } from '@angular/router';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'app-Login',
  templateUrl: './Login.component.html',
  styleUrls: ['./Login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(
    private userSvc: UserAuthenticationService,
    private globalFx: GlobalfxService,
    private activatedRoute: ActivatedRoute) {
     document.title = "VeCS - Vet. Cloud System - Login";
  }

  Username: string = '';
  Password: string = '';

  ErrorMessage: string = '';

  LogIn(): void {
    this.ErrorMessage = '';
    if (isNullOrUndefined(this.Username) != true) {
      if (this.Username.trim().length == 0) {
        // this.msgBox.warning("Enter your username", "Login");
        this.ErrorMessage = "Enter your username";
        return;
      }
    }
    this.userSvc.LogIn(this.Username, this.Password).then(() => {

      // var params:any = this.globalFx.getParams(window.location.href);

      // if(!isNullOrUndefined(params.url)){

      //   window.location.href = params.url;
      // }else{

      //   window.location.href = "/Main";
      // }

      window.location.href = "/Main";

    }).catch((err: HttpErrorResponse) => {
      if ( err.status == HTTPStatusCode.NotFound) {
        this.ErrorMessage = err.error;
      }
    })
  }

  onKeyDown(e: any): void {
      
    if (e.keyCode == 13) {
      // Enter
      this.LogIn();
    }
  }

  ngOnInit() {
  }

}
