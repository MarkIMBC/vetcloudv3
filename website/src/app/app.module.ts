import "@angular/compiler";
import { ReportViewComponent } from "./View/ReportView/ReportView.component";
import { TabService, NgInitDirective } from "./AppServices/tab.service";
import {
  DynamicTemplateService,
  RUNTIME_COMPILER_PROVIDERS,
} from "./AppServices/dynamic-temaplate.svc";
import { SharedModule } from "./../utils/shared.module";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { TableModule } from "primeng/table";
import { StepsModule } from "primeng/steps";
import { InputSwitchModule } from "primeng/inputswitch";
import { NgModule, APP_INITIALIZER, NO_ERRORS_SCHEMA } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { Routes, RouterModule } from "@angular/router";
import { ZXingScannerModule } from "@zxing/ngx-scanner";
import { HomeComponent } from "./Home/Home.component";
import { DataService } from "src/utils/service/data.service";
import { ValidationService } from "src/utils/service/validation.service";
import { TreeviewModule } from "ngx-treeview";
import { TreeModule } from "primeng/tree";
import { MenuModule } from "primeng/menu";
import { VC_Helper_Init } from "src/bo";
import { DetailViewComponent } from "./View/DetailView/DetailView.component";
import { ListViewComponent } from "./View/ListView/ListView.component";
import {
  LoaderService,
  LoaderInterceptor,
} from "./AppServices/LoaderInterceptor";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import {
  ListViewComponentDialog,
  ListViewModalService,
} from "./View/ListView/ListView.service";
import {
  DetailViewDialogComponent,
  DetailViewDialogService,
} from "./View/DetailViewDialog/DetailViewDialog.component";
import {
  FormHelperViewComponent,
  FormHelperDialogService,
} from "./View/FormHelperView/FormHelperView.component";
import { CalendarViewComponent } from "./CalendarView/CalendarView.component";
import { MainPortalComponent } from "./MainPortal/MainPortal.component";
import { FullCalendarModule } from "@fullcalendar/angular";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin
import interactionPlugin from "@fullcalendar/interaction"; // a plugin
import timeGridWeek from "@fullcalendar/timegrid";
import timeGridDay from "@fullcalendar/timegrid";
import timeGrid from "@fullcalendar/timegrid";
import list from "@fullcalendar/list";
import { LoginComponent } from "./Login/Login.component";
import { UserAuthenticationService } from "./AppServices/UserAuthentication.service";
import { CrypterService } from "src/utils/service/Crypter.service";
import { AppointmentScheduleComponent } from "./appointment-schedule/appointment-schedule.component";
import { MessageComponent } from "./message/message.component";
import { ChartsModule } from "ng2-charts";
import { RecipientComponent } from "./message/recipient/recipient.component";
import { AuthGuard, ModelDirtyGuardService } from "./auth.guard";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { ChipsModule } from "primeng/chips";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { ModuleNoAccessComponent } from "./ModuleNoAccess/ModuleNoAccess.component";
import { ToothComponent } from "./dental-chart/tooth/tooth.component";
import { ToothInfoComponent } from "./dental-chart/tooth-info/tooth-info.component";
import { ToothSmallComponent } from "./dental-chart/tooth-small/tooth-small.component";
import { DentalChartComponent } from "./dental-chart/dental-chart.component";
import { DialogModule } from "primeng/dialog";
import { ToothInfoDialogComponent } from "./dental-chart/tooth-info-dialog/tooth-info-dialog.component";
import { ToothSurfaceComponent } from "./dental-chart/tooth-surface/tooth-surface.component";
import { PatientInfoPanelComponent } from "./Shared/patient-info-panel/patient-info-panel.component";
import { SidenavModule } from "angular-ng-sidenav";
import { DentalExaminationComponent } from "./dental-examination/dental-examination.component";
import { MessageFormComponent } from "./message/message-form/message-form.component";
import { PopupUploadExcelComponent } from "./Shared/popup-upload-excel/popup-upload-excel.component";
import { ReceivingReportComponent } from "./CustomDetailView/receiving-report/receiving-report.component";
import { PurchaseOrderComponent } from "./CustomDetailView/purchase-order/purchase-order.component";
import { InventoryComponent } from "./inventory/inventory.component";
import { InventoryTrailComponent } from "./inventory-trail/inventory-trail.component";
import { SidemenuComponent } from "./sidemenu/sidemenu.component";
import { BookComponent } from "./book/book.component";
import { AppointmentscheduleStepStatusComponent } from "./Shared/appointmentschedule-step-status/appointmentschedule-step-status.component";
import { environment } from "../environments/environment";
import { SendmessageComponent } from "./sendmessage/sendmessage.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserRoleComponent } from "./CustomDetailView/user-role/user-role.component";
import { PaymentTransactionComponent } from "./CustomDetailView/payment-transaction/payment-transaction.component";
import { PatientAppointmentComponent } from "./CustomDetailView/patient-appointment/patient-appointment.component";
import { ItemservicesComponent } from "./CustomListView/itemservices/itemservices.component";
import { BillingInvoiceListComponent } from "./CustomListView/billing-invoice-list/billing-invoice-list.component";
import { AppointmentScheduleDialogComponent } from "./appointment-schedule/appointment-schedule-dialog/appointment-schedule-dialog.component";
import { ScheduleDialogComponent } from "./Schedule/schedule-dialog/schedule-dialog.component";
import { InventoryDetailReportViewerComponent } from "./ReportViewer/inventory-detail-report-viewer/inventory-detail-report-viewer.component";
import { POSSummaryReportViewerComponent } from "./ReportViewer/possummary-report-viewer/possummary-report-viewer.component";
import { InventorySummaryReportViewerComponent } from "./ReportViewer/inventory-summary-report-viewer/inventory-summary-report-viewer.component";
import { ItemMasterFileReportVIewerComponent } from "./ReportViewer/item-master-file-report-viewer/item-master-file-report-viewer.component";
import { PaymentTransactionReportViewerComponent } from "./ReportViewer/payment-transaction-report-viewer/payment-transaction-report-viewer.component";
import { DialogBoxComponent } from "./dialog-box/dialog-box";
import { CompanyComponent } from "./CustomDetailView/company/company.component";
import { UserComponent } from "./CustomDetailView/user/user.component";
import { UserGroupComponent } from "./CustomDetailView/user-group/user-group.component";
import { UserRolesListComponent } from "./CustomListView/user-roles-list/user-roles-list.component";
import { UserListComponent } from "./CustomListView/user-list/user-list.component";
import { SupplierComponent } from "./CustomDetailView/supplier/supplier.component";
import { SupplierListComponent } from "./CustomListView/supplier-list/supplier-list.component";
import { DocumentSeriesComponent } from "./CustomDetailView/document-series/document-series.component";
import { DocumentSeriesListComponent } from "./CustomListView/document-series-list/document-series-list.component";
import { SplitButtonModule } from "primeng/splitbutton";
import { ImportRecordComponent } from "./import-record/import-record.component";
import { PaymenttransactionDetailReportViewerComponent } from "./ReportViewer/paymenttransaction-detail-report-viewer/paymenttransaction-detail-report-viewer.component";
import { ReportComponentComponent } from "./ReportViewer/report-component/report-component.component";
import { BillinginvoicePaidListReportViewerComponent } from "./ReportViewer/billinginvoice-paid-list-report-viewer/billinginvoice-paid-list-report-viewer.component";
import { IssuetrackerListComponent } from "./CustomListView/issuetracker-list/issuetracker-list.component";
import { IssuetrackerComponent } from "./CustomDetailView/issuetracker/issuetracker.component";
import { SalesIncomeReportViewerComponentComponent } from "./ReportViewer/sales-income-report-viewer-component/sales-income-report-viewer-component.component";
import { ScheduleListComponent } from "./CustomListView/schedule-list/schedule-list.component";
import { SMSListComponent } from "./Shared/sms-list/sms-list.component";
import { BillingInvoiceAgingReportComponent } from "./ReportViewer/billing-invoice-aging-report/billing-invoice-aging-report.component";
import { QRCodeScanRedirectRecordComponent } from "../utils/qrcode-scan-redirect-record/qrcode-scan-redirect-record.component";
import { QRCodeModule } from "angularx-qrcode";
import { BaseListViewComponent } from "./base-list-view/base-list-view.component";
import { AppointmentScheduleStatusComponent } from "./CustomListView/schedule-list/appointment-schedule-status/appointment-schedule-status.component";
import { ClientFeedbackComponent } from "./CustomDetailView/client-feedback/client-feedback.component";
import { AnnouncementComponent } from "./CustomDetailView/announcement/announcement.component";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const routes: Routes = [
  {
    path: "Main",
    component: MainPortalComponent,
    children: [
      {
        path: "",
        component: HomeComponent,
      },
      // {
      //   path: "dv/:ModelName/:Oid_DetailView/:ID_CurrentObject",
      //   component: DetailViewComponent,
      //   canDeactivate: [ModelDirtyGuardService],
      // },
      // {
      //   path: "lv/:ModelName/:Oid_ListView",
      //   component: ListViewComponent,
      //   children: [],
      // },
      {
        path: "rv/:ReportName/:Oid_Report/:defaultfilterjson/:options",
        component: ReportViewComponent,
      },
      {
        path: "Home",
        component: HomeComponent,
      },
      {
        path: "Calendar",
        component: CalendarViewComponent,
      },
      {
        path: "Schedule",
        component: ScheduleListComponent,
      },
      {
        path: "ScheduleCalendar",
        loadChildren: () =>
          import("./Schedule/Schedule.module").then((m) => m.ScheduleModule),
      },
      {
        path: "AppointmentSchedule",
        component: AppointmentScheduleComponent,
      },
      {
        path: "AnnouncementList",
        loadChildren: () =>
          import(
            "./CustomListView/announcement-list/announcement-list.module"
          ).then((m) => m.AnnouncementListModule),
        canActivate: [AuthGuard],
      },
      {
        path: "Announcement/:ID_CurrentObject/:configOptions",
        component: AnnouncementComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "Message",
        component: MessageComponent,
      },
      {
        path: "NoAccess",
        component: ModuleNoAccessComponent,
      },
      {
        path: "DentalChart/:patientid",
        component: DentalChartComponent,
      },
      {
        path: "DentalExamination/:patientid/:id/:configOptions",
        component: DentalExaminationComponent,
      },
      {
        path: "MessageForm",
        component: MessageFormComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "Inventory",
        component: InventoryComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "InventoryTrail/:ID_Item",
        component: InventoryTrailComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "BillingInvoice/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/billing-invoice/billing-invoice.module"
          ).then((m) => m.BillingInvoiceModule),
      },
      {
        path: "DocumentSeries/:ID_CurrentObject/:configOptions",
        component: DocumentSeriesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "Client/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/client/client.module").then(
            (m) => m.ClientModule
          ),
      },
      {
        path: "CompanyInfo",
        component: CompanyComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "ConfinementList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-confinement-list/patient-confinement-list.module"
          ).then((m) => m.PatientConfinementListModule),
      },
      {
        path: "Confinement/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/patient-confinement/patient-confinement.module"
          ).then((m) => m.PatientConfinementModule),
      },
      {
        path: "PatientWaitingList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-waiting-list/patient-waiting-list.module"
          ).then((m) => m.PatientWaitingListModule),
      },
      {
        path: "ForBillingList",
        loadChildren: () =>
          import(
            "./CustomListView/for-billing-list/for-billing-list.module"
          ).then((m) => m.ForBillingListModule),
      },
      {
        path: "ImportRecord",
        component: ImportRecordComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "IssueTracker/:ID_CurrentObject/:configOptions",
        component: IssuetrackerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "PurchaseOrder/:ID_CurrentObject/:configOptions",
        component: PurchaseOrderComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "ReceivingReport/:ID_CurrentObject/:configOptions",
        component: ReceivingReportComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "PaymentTransaction/:ID_CurrentObject/:configOptions",
        component: PaymentTransactionComponent,
      },
      {
        path: "Patient/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/patient/patient.module").then(
            (m) => m.PatientModule
          ),
      },
      {
        path: "Patient_SOAP/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/patient-soap/patient-soap.module").then(
            (m) => m.PatientSOAPModule
          ),
      },
      {
        path: "PatientAppointment/:ID_CurrentObject/:configOptions",
        component: PatientAppointmentComponent,
      },
      {
        path: "Item/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/item/item.module").then(
            (m) => m.ItemModule
          ),
      },
      {
        path: "Employee/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/employee/employee.module").then(
            (m) => m.EmployeeModule
          ),
      },
      {
        path: "EmployeeInfo",
        loadChildren: () =>
          import("./CustomDetailView/employee/employee.module").then(
            (m) => m.EmployeeModule
          ),
      },
      {
        path: "Supplier/:ID_CurrentObject/:configOptions",
        component: SupplierComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "User/:ID_CurrentObject/:configOptions",
        component: UserComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "UserRole/:ID_CurrentObject/:configOptions",
        component: UserRoleComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "SendMessage",
        component: SendmessageComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "BillingInvoiceAgingReport",
        component: BillingInvoiceAgingReportComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "BillingInvoicePaidListReport",
        component: BillinginvoicePaidListReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "ItemMasterFileReport",
        component: ItemMasterFileReportVIewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "InventoryDetailReport",
        component: InventoryDetailReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "InventorySummaryReport",
        component: InventorySummaryReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "PaymentTransactionReport",
        component: PaymentTransactionReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "POSSummaryReport",
        component: POSSummaryReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "PaymentTransactionDetailReport",
        component: PaymenttransactionDetailReportViewerComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "ReceivedDepositCreditReportViewer",
        loadChildren: () =>
          import(
            "./ReportViewer/received-deposit-credit-report-viewer/received-deposit-credit-report-viewer.module"
          ).then((m) => m.ReceivedDepositCreditReportViewerModule),
      },
      {
        path: "SalesIncomeReport",
        component: SalesIncomeReportViewerComponentComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "BillingInvoices",
        component: BillingInvoiceListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "ClientList",
        loadChildren: () =>
          import("./CustomListView/client-list/client-list.module").then(
            (m) => m.ClientListModule
          ),
        canActivate: [AuthGuard],
      },

      {
        path: "ClientAppointmentRequestList",
        loadChildren: () =>
          import(
            "./CustomListView/client-appointment-request-list/client-appointment-request-list.module"
          ).then((m) => m.ClientAppointmentRequestListModule),
        canActivate: [AuthGuard],
      },
      {
        path: "ClientAppointmentRequest/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/client-appointment-request/client-appointment-request.module"
          ).then((m) => m.ClientAppointmentRequestModule),
      },
      {
        path: "ClientFeedbackList",
        loadChildren: () =>
          import(
            "./CustomListView/client-feedback-list/client-feedback-list.module"
          ).then((m) => m.ClientFeedbackListModule),
        canActivate: [AuthGuard],
      },

      {
        path: "ClientFeedback/:ID_CurrentObject/:configOptions",
        component: ClientFeedbackComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "DocumentSeriesList",
        component: DocumentSeriesListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "EmployeeList",
        loadChildren: () =>
          import("./CustomListView/employee-list/employee-list.module").then(
            (m) => m.EmployeeListModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "IssueTrackerList",
        component: IssuetrackerListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "HelpDeskVideoTutorialList",
        loadChildren: () =>
          import(
            "./CustomListView/help-desk-video-tutorial-list/help-desk-video-tutorial-list.module"
          ).then((m) => m.HelpDeskVideoTutorialListModule),
      },
      {
        path: "HelpDeskVideoTutorial",
        loadChildren: () =>
          import(
            "./CustomDetailView/help-desk-video-tutorial/help-desk-video-tutorial.module"
          ).then((m) => m.HelpDeskVideoTutorialModule),
      },
      {
        path: "Items",
        loadChildren: () =>
          import(
            "./CustomListView/iteminventoriables/iteminventoriables.module"
          ).then((m) => m.IteminventoriablesModule),
      },
      {
        path: "Patients",
        loadChildren: () =>
          import("./CustomListView/patient-list/patient-list.module").then(
            (m) => m.PatientListModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: "PayableList",
        loadChildren: () =>
          import("./CustomListView/payable-list/payable-list.module").then(
            (m) => m.PayableListModule
          ),
      },
      {
        path: "PaymentTransactions",
        loadChildren: () =>
          import(
            "./CustomListView/paymenttransactionlist/paymenttransactionlist.module"
          ).then((m) => m.PaymenttransactionlistModule),
        canActivate: [AuthGuard],
      },
      {
        path: "PatientSOAPList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-soaplist/patient-soaplist.module"
          ).then((m) => m.PatientSOAPListModule),
        canActivate: [AuthGuard],
      },
      {
        path: "PurchaseOrders",
        loadChildren: () =>
          import(
            "./CustomListView/purchaseorderlist/purchaseorderlist.module"
          ).then((m) => m.PurchaseorderlistModule),
        canActivate: [AuthGuard],
      },
      {
        path: "ReceivingReports",
        loadChildren: () =>
          import(
            "./CustomListView/receivingreportlist/receivingreportlist.module"
          ).then((m) => m.ReceivingreportlistModule),
        canActivate: [AuthGuard],
      },
      {
        path: "ScheduleList",
        component: ScheduleListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "Services",
        component: ItemservicesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "SupplierList",
        component: SupplierListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "TextBlast/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import("./CustomDetailView/text-blast/text-blast.module").then(
            (m) => m.TextBlastModule
          ),
      },
      {
        path: "TextBlastList",
        loadChildren: () =>
          import(
            "./CustomListView/text-blast-list/text-blast-list.module"
          ).then((m) => m.TextBlastListModule),
      },
      {
        path: "UserList",
        component: UserListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "UserRoleList",
        component: UserRolesListComponent,
        canActivate: [AuthGuard],
      },

      {
        path: "Patient_Vaccination/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/patient-vaccination/patient-vaccination.module"
          ).then((m) => m.PatientVaccinationModule),
      },
      {
        path: "PatientVaccinationList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-vaccination-list/patient-vaccination-list.module"
          ).then((m) => m.PatientVaccinationListModule),
      },
      {
        path: "VeterinaryHealthCertificate/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/veterinary-health-certificate/veterinary-health-certificate.module"
          ).then((m) => m.VeterinaryHealthCertificateModule),
      },
      {
        path: "VeterinaryHealthCertificateList",
        loadChildren: () =>
          import(
            "./CustomListView/veterinary-health-certificate-list/veterinary-health-certificate-list.module"
          ).then((m) => m.VeterinaryHealthCertificateListModule),
      },
      {
        path: "Patient_Wellness/:ID_CurrentObject/:configOptions",
        loadChildren: () =>
          import(
            "./CustomDetailView/patient-wellness/patient-wellness.module"
          ).then((m) => m.PatientWellnessModule),
      },
      {
        path: "PatientWellnessList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-wellness-list/patient-wellness-list.module"
          ).then((m) => m.PatientWellnessListModule),
      },
      {
        path: "BillingInvoiceWalkInList",
        loadChildren: () =>
          import(
            "./CustomListView/billing-invoice-walk-in-list/billing-invoice-walk-in-list.module"
          ).then((m) => m.BillingInvoiceWalkInListModule),
      },

      {
        path: "TermsAndConditions",
        loadChildren: () =>
          import(
            "./CustomDetailView/terms-and-conditions/terms-and-conditions.module"
          ).then((m) => m.TermsAndConditionsModule),
      },
      {
        path: "PatientWaitingListCanceledList",
        loadChildren: () =>
          import(
            "./CustomListView/patient-waiting-list-canceled-list/patient-waiting-list-canceled-list.module"
          ).then((m) => m.PatientWaitingListCanceledListModule),
      },
      {
        path: "ReferenceLink/:Code",
        loadChildren: () =>
          import(
            "./CustomDetailView/reference-link-view/reference-link-view.module"
          ).then((m) => m.ReferenceLinkViewModule),
      },
      { path: "**", component: ModuleNoAccessComponent },
    ],
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    canDeactivate: [AuthGuard],
  },
  {
    path: "Login",
    component: LoginComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "PatientVaccination",
    loadChildren: () =>
      import(
        "./CustomDetailView/patient-vaccination/patient-vaccination.module"
      ).then((m) => m.PatientVaccinationModule),
  },
  {
    path: "PatientVaccinationList",
    loadChildren: () =>
      import(
        "./CustomListView/patient-vaccination-list/patient-vaccination-list.module"
      ).then((m) => m.PatientVaccinationListModule),
  },
  { path: 'Report-POS-Detail', loadChildren: () => import('./feature/report-pos-detail/report-pos-detail.module').then(m => m.ReportPOSDetailModule) },
];

export function appInit(dataService: DataService) {
  return () => dataService.loadConfig();
}

FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin,
  timeGridWeek,
  timeGridDay,
  timeGrid,
  list,
  interactionPlugin,
]);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListViewComponent,
    DetailViewComponent,
    ReportViewComponent,
    ListViewComponentDialog,
    DetailViewDialogComponent,
    FormHelperViewComponent,
    NgInitDirective,
    CalendarViewComponent,
    MainPortalComponent,
    DentalChartComponent,
    LoginComponent,
    AppointmentScheduleComponent,
    MessageComponent,
    RecipientComponent,
    ModuleNoAccessComponent,
    ToothComponent,
    ToothInfoComponent,
    ToothSmallComponent,
    ToothInfoDialogComponent,
    ToothSurfaceComponent,
    PatientInfoPanelComponent,
    DentalExaminationComponent,
    MessageFormComponent,
    PopupUploadExcelComponent,
    ReceivingReportComponent,
    PurchaseOrderComponent,
    InventoryComponent,
    InventoryTrailComponent,
    SidemenuComponent,
    BookComponent,
    AppointmentscheduleStepStatusComponent,
    SendmessageComponent,
    UserRoleComponent,
    PaymentTransactionComponent,
    PatientAppointmentComponent,
    ItemservicesComponent,
    BillingInvoiceListComponent,
    AppointmentScheduleDialogComponent,
    ScheduleDialogComponent,
    InventoryDetailReportViewerComponent,
    POSSummaryReportViewerComponent,
    InventorySummaryReportViewerComponent,
    ItemMasterFileReportVIewerComponent,
    PaymentTransactionReportViewerComponent,
    DialogBoxComponent,
    CompanyComponent,
    UserComponent,
    UserGroupComponent,
    UserRolesListComponent,
    UserListComponent,
    SupplierComponent,
    SupplierListComponent,
    DocumentSeriesComponent,
    DocumentSeriesListComponent,
    ImportRecordComponent,
    PaymenttransactionDetailReportViewerComponent,
    ReportComponentComponent,
    BillinginvoicePaidListReportViewerComponent,
    IssuetrackerListComponent,
    IssuetrackerComponent,
    SalesIncomeReportViewerComponentComponent,
    ScheduleListComponent,
    SMSListComponent,
    BillingInvoiceAgingReportComponent,
    QRCodeScanRedirectRecordComponent,
    BaseListViewComponent,
    AppointmentScheduleStatusComponent,
    ClientFeedbackComponent,
    AnnouncementComponent,
  ],
  imports: [
    SplitButtonModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    SharedModule,
    TreeviewModule.forRoot(),
    TreeModule,
    MenuModule,
    FullCalendarModule,
    ChartsModule,
    PerfectScrollbarModule,
    TableModule,
    DialogModule,
    SidenavModule,
    ChipsModule,
    StepsModule,
    InputSwitchModule,
    ZXingScannerModule,
    QRCodeModule,
  ],
  providers: [
    ModelDirtyGuardService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    ...RUNTIME_COMPILER_PROVIDERS,
    DataService,
    ValidationService,
    DynamicTemplateService,
    TabService,
    ListViewModalService,
    DetailViewDialogService,
    FormHelperDialogService,
    UserAuthenticationService,
    CrypterService,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {
  constructor() {
    VC_Helper_Init();
  }
}
