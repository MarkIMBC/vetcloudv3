import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DentalExaminationComponent } from './dental-examination.component';

describe('DentalExaminationComponent', () => {
  let component: DentalExaminationComponent;
  let fixture: ComponentFixture<DentalExaminationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DentalExaminationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DentalExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
