import { AppImageBoxComponent } from './../../utils/controls/appImageBox/appImageBox.component';
import { Component, OnInit, NgModule, ViewChild, Input } from "@angular/core";
import { ToothComponent } from "./../dental-chart/tooth/tooth.component";
import { ToothInfoDialogComponent } from "./../dental-chart/tooth-info-dialog/tooth-info-dialog.component";
import { DentalChartComponent } from "./../dental-chart/dental-chart.component";
import { DialogModule } from "primeng/dialog";
import { MenubarModule } from "primeng/menubar";
import { MenuItem } from "primeng/api";
import {
  ToothInfo_DTO,
  Patient_DentalExamination_DTO,
  Doctor,
  Patient_DentalExamination_ToothInfo_DTO,
  ToothSurfaceEnum,
  ToothInfoEventArg,
  Tooth_DTO,
  DentitionEnum,
  SavingPatientDentalExamToothInfo,
  ITooth_ToothStatusArg,
  Patient_DTO,
  FilingStatusEnum,
  PropertyTypeEnum,
  ControlTypeEnum,
  IFile,
  PaymentMethodEnum,
} from "src/utils/controls/class_helper";
import {
  APP_MODEL,
  Patient,
  ToothStatus,
  ToothSurface,
  ToothInfo,
  APP_DETAILVIEW,
  APP_REPORTVIEW,
  Patient_DentalExamination_Image,
} from "src/bo/APP_MODELS";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { isNullOrUndefined, isNumber } from "util";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { DataTableOption } from "src/utils/controls/appDataTable/appDataTable.component";
import {
  UserAuthenticationService,
  TokenSessionFields,
} from "../AppServices/UserAuthentication.service";
import { ToastrService } from "ngx-toastr";
import { ActivatedRoute, Router } from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { Enumerable } from "linq-typescript";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { FormHelperDialogService } from "../View/FormHelperView/FormHelperView.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RadioButtonModule } from "primeng/radiobutton";
import { DialogBoxComponent } from "../dialog-box/dialog-box";

@Component({
  selector: "app-dental-examination",
  templateUrl: "./dental-examination.component.html",
  styleUrls: ["./dental-examination.component.less"],
})
export class DentalExaminationComponent {

  @ViewChild("PatientImageDialogBox")
  PatientImageDialogBoxCmp: DialogBoxComponent;

  @ViewChild("PatientImageBox")
  PatientImageBox: AppImageBoxComponent;

  _IsSaving: boolean = false;

  menuItems: MenuItem[] = [];

  setToSavingMode() {
    this._IsSaving = true;

    this.menuItems = [];

    if (this.selPatientDentalExam.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.menuItems.push(this._menuItemsRecordSaving_btnSave);

      if (this.selPatientDentalExam.ID > 0) {
        this.menuItems.push(this._menuItemsRecordSaving_btnApprove);
      }
    }

    if (this.selPatientDentalExam.ID > 0) {
      if (
        this.selPatientDentalExam.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        this.menuItems.push(this._menuItemsRecordSaving_btnCancel);
      }
    }

    if (this.selPatientDentalExam.ID > 0) {
      this.menuItems.push(this._menuItemsRecordSaving_btnReport);
    }

    this.isAllowClick = true;
  }

  setToUnSavingMode() {
    this._IsSaving = false;
    this.menuItems = Object.assign([], this._menuItemsDefault);
    this.isAllowClick = false;
  }

  chartToothInfos: ToothInfo_DTO[] = [];

  isAllowClick: boolean = false;

  @Input()
  patient: Patient = new Patient();
  currentDoctor: Doctor = new Doctor();

  Filed_ID_FilingStatus: number = FilingStatusEnum.Filed;

  patientDentalExams: Patient_DentalExamination_DTO[] = [];
  selPatientDentalExam: Patient_DentalExamination_DTO = new Patient_DentalExamination_DTO();

  configOptions: any = {};

  selectedToothInfo: Patient_DentalExamination_ToothInfo_DTO;

  @ViewChild("toothInfoDialog")
  toothInfoDialogComponent: ToothInfoDialogComponent;

  @ViewChild("dentalChart")
  dentalChartComponent: DentalChartComponent;

  currentUser: TokenSessionFields;

  constructor(
    private ds: DataService,
    private globalFx: GlobalfxService,
    private msgBox: MessageBoxService,
    private userAuthSvc: UserAuthenticationService,
    private lvModal: ListViewModalService,
    private route: ActivatedRoute,
    private router: Router,
    private cs: CrypterService,
    private toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();

    this._imageURLPath = DataService.API_URL;
    this._imageURLPath = this._imageURLPath.substring(
      0,
      this._imageURLPath.length - 4
    );
  }

  _imageURLPath: string = "";

  _menuItemsDefault: MenuItem[] = [
    {
      label: "Create",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.selPatientDentalExam = new Patient_DentalExamination_DTO();

        this.selPatientDentalExam = (await this.ds.loadObject(
          APP_MODEL.PATIENT_DENTALEXAMINATION,
          -1 + ""
        )) as Patient_DentalExamination_DTO;

        if (isNullOrUndefined(this.selPatientDentalExam.ToothInfos))
          this.selPatientDentalExam.ToothInfos = [];

        this.dentalChartComponent.loadToothInfosByID(DentitionEnum.Mixed);

        this.setToSavingMode();
      },
    },
    {
      label: "Browse",
      icon: "pi pi-fw pi-search",
      command: async () => {
        this.browseDentalExamRecord().then(
          async (record: Patient_DentalExamination_DTO) => {
            this.selPatientDentalExam = record;
            await this.loadCurrentDentalExamToothInfos();
            await this.loadPatientDentalExamiantionMedHistory();
            this.setToSavingMode();
          }
        );
      },
    },
  ];

  imageMenu: MenuItem[] = [
    {
      label: "Add Image",
      icon: "pi pi-fw pi-plus",
      command: async () => {

        this.openPatientDentalImageDialog();
      },
    },
  ];

  async btnImagesCloseButton_onClick(image: Patient_DentalExamination_Image) {
    if (
      (await this.msgBox.confirm(
        `Do you want to delete this image?`,
        "Deleting Image",
        "Yes",
        "No"
      )) != true
    )
      return;

    var obj = await this.ds.execSP(
      "pDelete_Patient_DentalExamination_Image",
      {
        ID_Patient_DentalExamination_Image: image.ID,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success == true) {
      this.loadPatientDentalExamImages();
      this.toastr.success(`Image has been deleted.`, "Delete Image...");
    } else {
      this.toastr.warning(`Unable to delete image.`, "Delete Failed");
    }
  }

  _menuItemsRecordSaving_btnReport: MenuItem = {
    label: "Report",
    icon: "pi pi-fw pi-file",
    items: [
      {
        label: "Dental Exam",
        icon: "pi pi-fw pi-file",
        command: async () => {
          if (isNullOrUndefined(this.selPatientDentalExam)) return;

          var filterFormValue: IFilterFormValue[] = [];
          var options = {
            backLink: this.router.url,
          };

          filterFormValue.push({
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.selPatientDentalExam.ID,
          });

          this.globalFx.navigateReport(
            "DentalExamination",
            APP_REPORTVIEW.DENTALEXAMINATION,
            filterFormValue,
            options
          );
        },
      },
      {
        label: "Prescription",
        icon: "pi pi-fw pi-file",
        command: async () => {
          if (isNullOrUndefined(this.selPatientDentalExam)) return;

          var filterFormValue: IFilterFormValue[] = [];
          var options = {
            backLink: this.router.url,
          };

          filterFormValue.push({
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.selPatientDentalExam.ID,
          });

          this.globalFx.navigateReport(
            "DentalExamination",
            APP_REPORTVIEW.DENTALEXAMINATIONPRESCRIPTION,
            filterFormValue,
            options
          );
        },
      },
    ],
  };

  _menuItemsRecordSaving_btnClose: MenuItem = {
    label: "Close",
    icon: "pi pi-fw pi-times",
    command: async () => {
      this.selPatientDentalExam = new Patient_DentalExamination_DTO();
      this.dentalChartComponent.clearToothStatuses();
      this.setToUnSavingMode();
    },
  };

  btnCloseButton_onClick() {
    this.selPatientDentalExam = new Patient_DentalExamination_DTO();
    this.dentalChartComponent.clearToothStatuses();
    this.setToUnSavingMode();
  }

  _menuItemsRecordSaving_btnSave: MenuItem = {
    label: "Save",
    icon: "pi pi-fw pi-save",
    command: async () => {
      if (this.selPatientDentalExam.ToothInfos.length < 1) {
        this.toastr.warning(
          `Tooth Status is required atleast one.`,
          "Unable to Save record..."
        );
        return;
      }

      if (isNullOrUndefined(this.selPatientDentalExam)) return;
      if (
        (await this.msgBox.confirm(
          `Do you want to save ${this.selPatientDentalExam.Code}?`,
          "Dental Examination",
          "Yes",
          "No"
        )) != true
      )
        return;

      var records = await this.getsavingPatientDentalExamToothInfo();
      var images = await this.getsavingPatientDentalExamImage();
      var medicalhistories = await this.getsavingPatientDentalExamMedicalHistory();

      console.log(medicalhistories);

      var obj = await this.ds.execSP(
        "pSaveDentalExamination",
        {
          data: records,
          images: images,
          medicalhistories: medicalhistories,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      this.selPatientDentalExam = obj.records[0];

      this.selPatientDentalExam = (await this.ds.loadObject(
        APP_MODEL.PATIENT_DENTALEXAMINATION,
        this.selPatientDentalExam.ID + ""
      )) as Patient_DentalExamination_DTO;

      if (isNullOrUndefined(this.selPatientDentalExam.ToothInfos))
        this.selPatientDentalExam.ToothInfos = [];

      await this.loadCurrentDentalExamToothInfos();
      await this.loadPatientDentalExamiantionMedHistory();

      var routeLink = [];
      var configJSON = JSON.stringify(this.configOptions);

      configJSON = this.cs.encrypt(configJSON);
      routeLink = [
        `/Main`,
        "DentalExamination",
        this.patient.ID,
        this.selPatientDentalExam.ID,
        configJSON,
      ];
      this.router.navigate(routeLink);

      this.toastr.success(
        `${this.selPatientDentalExam.Code} has been save successfully.`,
        ""
      );

      this.setToSavingMode();
    },
  };

  _menuItemsRecordSaving_btnApprove: MenuItem = {
    label: "Approve",
    icon: "pi pi-fw pi-save",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to approve ${this.selPatientDentalExam.Code}?`,
        "Dental Exam",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.approve();

      if (obj.Success) {
        var patientDentalExam = (await this.ds.loadObject(
          APP_MODEL.PATIENT_DENTALEXAMINATION,
          this.selPatientDentalExam.ID + ""
        )) as Patient_DentalExamination_DTO;
        this.selPatientDentalExam = patientDentalExam;
        await this.loadCurrentDentalExamToothInfos();
        if (isNullOrUndefined(this.selPatientDentalExam.ToothInfos))
          this.selPatientDentalExam.ToothInfos = [];

        this.setToSavingMode();
      }
    },
  };

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params["configOptions"];
    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  async approve(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApprovePatient_DentalExamination",
        {
          IDs_Patient_DentalExamination: [this.selPatientDentalExam.ID],
          ID_UserSession: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selPatientDentalExam.Code} has been approved successfully.`,
          `Approved Dental Exam`
        );
        res(obj);
      } else {
        this.msgBox.error(obj.message, `Failed to Approve Dental Exam`);
        rej(obj);
      }
    });
  }

  _menuItemsRecordSaving_btnCancel: MenuItem = {
    label: "Cancel",
    icon: "pi pi-fw pi-save",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to cancel ${this.selPatientDentalExam.Code}?`,
        "Dental Exam",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.cancel();

      if (obj.Success) {
        var patientDentalExam = (await this.ds.loadObject(
          APP_MODEL.PATIENT_DENTALEXAMINATION,
          this.selPatientDentalExam.ID + ""
        )) as Patient_DentalExamination_DTO;
        this.selPatientDentalExam = patientDentalExam;

        await this.loadCurrentDentalExamToothInfos();
        if (isNullOrUndefined(this.selPatientDentalExam.ToothInfos))
          this.selPatientDentalExam.ToothInfos = [];

        this.setToSavingMode();
      }
    },
  };

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPatient_DentalExamination",
        {
          IDs_Patient_DentalExamination: [this.selPatientDentalExam.ID],
          ID_UserSession: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selPatientDentalExam.Code} has been canceled successfully.`,
          `Canceled Dental Exam`
        );
        res(obj);
      } else {
        this.msgBox.error(obj.message, `Failed to Cancel Dental Exam`);
        rej(obj);
      }
    });
  }

  menuDentistExamDetail: MenuItem[] = [
    {
      label: "Add",
      icon: "pi pi-fw pi-plus",
      command: async () => {},
    },
  ];

  async ngOnInit() {
    this.loadConfigOption();

    this.setToUnSavingMode();
    await this.loadPatientInfo();

    await this.loadCurrentDentalExamination();

    await this.loadDoctorInfo();
  }

  gotoPatientDetailView() {
    var routeLink = [];
    var prevConfig = this.configOptions["prevConfig"];

    routeLink = [`/Main`, "Patient", this.patient.ID];

    this.globalFx.customNavigate(routeLink, prevConfig);
  }

  browseDentalExamRecord() {
    var promise = new Promise(async (resolve, reject) => {
      var sql = `SELECT ID, 
                        Code, 
                        DateString,
                        Doctor,
                        Patient,
                        ID_Doctor, 
                        ID_Patient, 
                        ID_Dentition,
                        Date
                FROM vPatient_DentalExamination 
                WHERE  
                  ID_Patient = ${this.patient.ID} AND  
                  ID_Doctor = ${this.currentDoctor.ID}  
            `;

      var selectedRecords = await this.lvModal.ListLookUp<Patient_DentalExamination_DTO>(
        {
          sql: sql,
          onGridOption_Initialized: (gridOption: DataTableOption) => void {},
          title: "Select Items...",
        }
      );

      if (selectedRecords.length == 0) {
        reject();
      } else {
        var patientDentalExam = (await this.ds.loadObject(
          APP_MODEL.PATIENT_DENTALEXAMINATION,
          selectedRecords[0].ID + ""
        )) as Patient_DentalExamination_DTO;

        if (isNullOrUndefined(patientDentalExam.ToothInfos))
          patientDentalExam.ToothInfos = [];

        resolve(patientDentalExam);
      }
    });

    return promise;
  }

  private _ID_Patient_DentalExamination: number = 0;

  async loadCurrentDentalExamination() {
    var ID_Patient_DentalExamination = parseInt(
      this.route.snapshot.params["id"]
    );

    if (isNullOrUndefined(ID_Patient_DentalExamination))
      ID_Patient_DentalExamination = -1;

    var patientDentalExam = (await this.ds.loadObject(
      APP_MODEL.PATIENT_DENTALEXAMINATION,
      ID_Patient_DentalExamination + ""
    )) as Patient_DentalExamination_DTO;

    this.selPatientDentalExam = patientDentalExam;

    this.dentalChartComponent.loadToothInfosByID(DentitionEnum.Mixed);

    this.toothInfoDialogComponent.IsHideButton =
      this.selPatientDentalExam.ID_FilingStatus != FilingStatusEnum.Filed;

    await this.loadCurrentDentalExamToothInfos();
    await this.loadPatientDentalExamiantionMedHistory();

    this.setToSavingMode();
  }

  async loadDoctorInfo() {
    this.currentDoctor = (await this.ds.loadObject(
      APP_MODEL.EMPLOYEE,
      this.currentUser.ID_Employee + ""
    )) as Doctor;
    return Promise.resolve();
  }

  async loadPatientInfo() {
    var patientID = parseInt(this.route.snapshot.params["patientid"]);

    this.patient = (await this.ds.loadObject(
      APP_MODEL.PATIENT,
      patientID + ""
    )) as Patient;
    return Promise.resolve();
  }

  DivisionQuestionaires: any = [];

  async loadPatientDentalExamiantionMedHistory() {
    var obj = await this.ds.execSP(
      "pGetPatientDentalExamiantionMedHistory",
      {
        ID_Patient_DentalExamination: this.selPatientDentalExam.ID,
      },
      {
        isReturnObject: true,
      }
    );

    this.selPatientDentalExam.MedicalHistories = obj.Questions;

    var parents = Enumerable.fromSource(
      this.selPatientDentalExam.MedicalHistories
    )
      .distinct(function (x) {
        return x.Parent_ID_MedicalHistoryQuestionnaire;
      })
      .select(function (x) {
        return {
          ID: x.Parent_ID_MedicalHistoryQuestionnaire,
          Comment: x.Parent_Comment_MedicalHistoryQuestionnaire,
          Questionaires: [],
        };
      })
      .orderBy(function (x) {
        return x.ID;
      })
      .toArray();

    var divisionQuestionaires = [[]];
    var index = 0;
    var counter = 0;
    var maxItemCount = 3;
    var parentContainer = [];

    parents.forEach((parent) => {
      parent.Questionaires = Enumerable.fromSource(
        this.selPatientDentalExam.MedicalHistories
      )
        .where(
          (r) =>
            r.Parent_ID_MedicalHistoryQuestionnaire == parent.ID &&
            r.IsParent == false
        )
        .toArray();

      if (divisionQuestionaires[index].length == maxItemCount) {
        index++;
        divisionQuestionaires.push([]);
      }

      if (divisionQuestionaires[index].length < maxItemCount) {
        divisionQuestionaires[index].push(parent);
      }
    });

    this.DivisionQuestionaires = divisionQuestionaires;

    return Promise.resolve();
  }

  async loadCurrentDentalExamToothInfos() {
    var IDs_Tooth: number[] = [];

    //Load ToothStatus
    var sql = `SELECT ID,
                  ID_Tooth,
                  IDs_ToothSurface,
                  ID_ToothStatus,
                  ToothNumber_Tooth,
                  Location_Tooth,
                  Name_ToothStatus,
                  Code_ToothStatus,
                  Comment
              FROM dbo.vPatient_DentalExamination_ToothInfo
              WHERE ID_Patient_DentalExamination = ${this.selPatientDentalExam.ID}
            `;
    sql = this.cs.encrypt(sql);
    this.selPatientDentalExam.ToothInfos = await this.ds.query<Patient_DentalExamination_ToothInfo_DTO>(
      sql
    );

    this.loadPatientDentalExamImages();

    this.dentalChartComponent.loadToothInfosByID(
      this.selPatientDentalExam.ID_Dentition
    );
    this.toothInfoDialogComponent.IsHideButton =
      this.selPatientDentalExam.ID_FilingStatus != FilingStatusEnum.Filed;

    // Set Tooth Surfaces
    this.selPatientDentalExam.ToothInfos.forEach((toothInfo) => {
      this.generateToothSurfacesNames(toothInfo);
      IDs_Tooth.push(toothInfo.ID_Tooth);
    });

    // Set Tooth Status
    var uniqueIDs_Tooth = this.globalFx.getDistinctValues(IDs_Tooth);

    uniqueIDs_Tooth.forEach((ID_Tooth) => {
      this.setToothStatuses(ID_Tooth);
    });

    return Promise.resolve();
  }

  private async loadPatientDentalExamImages() {
    //Load Images
    var sql = `SELECT ID,
                Name,
                ImageValue,
                Comment
              FROM dbo.tPatient_DentalExamination_Image
              WHERE ID_Patient_DentalExamination = ${this.selPatientDentalExam.ID}
              `;

    sql = this.cs.encrypt(sql);

    this.selPatientDentalExam.Images = await this.ds.query<Patient_DentalExamination_Image>(
      sql
    );
  }

  DentalExams_Dropdown_onChange = async (event) => {
    await this.loadCurrentDentalExamToothInfos();
  };

  getsavingPatientDentalExamToothInfo(): SavingPatientDentalExamToothInfo[] {
    var records: SavingPatientDentalExamToothInfo[] = [];

    if (isNullOrUndefined(this.selPatientDentalExam._deletedItems))
      this.selPatientDentalExam._deletedItems = {};

    if (
      isNullOrUndefined(
        this.selPatientDentalExam._deletedItems[
          "Patient_DentalExamination_ToothInfo"
        ]
      )
    ) {
      this.selPatientDentalExam._deletedItems[
        "Patient_DentalExamination_ToothInfo"
      ] = [];
    }

    this.selPatientDentalExam.ToothInfos.forEach((ToothInfo) => {
      records.push({
        ID_Patient_DentalExamination: this.selPatientDentalExam.ID,
        Date: this.selPatientDentalExam.Date,
        ID_Patient: this.patient.ID,
        ID_Doctor: this.currentDoctor.ID,
        ID_Patient_DentalExamination_ToothInfo: ToothInfo.ID,
        ID_Tooth: ToothInfo.ID_Tooth,
        IDs_ToothSurface: ToothInfo.IDs_ToothSurface,
        ID_ToothStatus: ToothInfo.ID_ToothStatus,
        Comment: ToothInfo.Comment,
        ID_Dentition: this.selPatientDentalExam.ID_Dentition,
        Prescription: this.selPatientDentalExam.Comment,
      });
    });

    this.selPatientDentalExam._deletedItems[
      "Patient_DentalExamination_ToothInfo"
    ].forEach((ToothInfo) => {
      var toothInfo = ToothInfo as Patient_DentalExamination_ToothInfo_DTO;

      records.push({
        ID_Patient_DentalExamination: this.selPatientDentalExam.ID,
        Date: this.selPatientDentalExam.Date,
        ID_Patient: this.patient.ID,
        ID_Doctor: this.currentDoctor.ID,
        ID_Patient_DentalExamination_ToothInfo: toothInfo.ID,
        ID_Tooth: toothInfo.ID_Tooth,
        IDs_ToothSurface: toothInfo.IDs_ToothSurface,
        ID_ToothStatus: toothInfo.ID_ToothStatus,
        Comment: toothInfo.Comment,
        ID_Dentition: this.selPatientDentalExam.ID_Dentition,
        IsDelete: true,
      });
    });

    return records;
  }

  getsavingPatientDentalExamImage(): any[] {
    var records: any[] = [];

    this.selPatientDentalExam.Images.forEach((image) => {
      records.push({
        ID: image.ID,
        Comment: image.Comment,
      });
    });

    return records;
  }

  getsavingPatientDentalExamMedicalHistory(): any[] {
    var records: any[] = [];

    this.selPatientDentalExam.MedicalHistories.forEach((medHistory) => {
      records.push({
        ID: medHistory.ID,
        ID_Patient_DentalExamination: this.selPatientDentalExam.ID,
        ID_MedicalHistoryQuestionnaire:
          medHistory.ID_MedicalHistoryQuestionnaire,
        Comment: medHistory.Comment,
        Answer: medHistory.Answer,
      });
    });

    return records;
  }

  generateToothSurfacesNames(
    ToothInfo: Patient_DentalExamination_ToothInfo_DTO
  ): void {
    var IDs_ToothSurface = ToothInfo.IDs_ToothSurface.split(",");

    ToothInfo.ToothSurfaces = [];

    IDs_ToothSurface.forEach((idString) => {
      var id = parseInt(idString);
      var name = this.getToothSurfaceName(id);

      ToothInfo.ToothSurfaces.push({
        ID: id,
        Name: name,
      });
    });
  }

  getToothSurfaceName(ID_ToothSurface: number): string {
    var toothSurfaceName = "";

    switch (ID_ToothSurface) {
      case ToothSurfaceEnum.Buccal:
        toothSurfaceName = "Buccal";
        break;
      case ToothSurfaceEnum.Distal:
        toothSurfaceName = "Distal";
        break;
      case ToothSurfaceEnum.Labial:
        toothSurfaceName = "Labial";
        break;
      case ToothSurfaceEnum.Incisal:
        toothSurfaceName = "Incisal";
        break;
      case ToothSurfaceEnum.Lingual:
        toothSurfaceName = "Lingual";
        break;
      case ToothSurfaceEnum.Misial:
        toothSurfaceName = "Misial";
        break;
      case ToothSurfaceEnum.Occlusal:
        toothSurfaceName = "Occlusal";
        break;
    }

    return toothSurfaceName;
  }

  DentalChart_onToothClick(tooth: Tooth_DTO): void {
    var toothInfo: ToothInfo_DTO = new ToothInfo_DTO();

    toothInfo.ID_Tooth = tooth.ID;
    toothInfo.ToothNumber_Tooth = tooth.ToothNumber;
    toothInfo.Location_Tooth = tooth.Location;
    toothInfo.IDs_ToothSurface = "";

    toothInfo.ID_ToothStatus = 0;
    toothInfo.Name_ToothStatus = "";
    toothInfo.Comment = "";

    this.toothInfoDialogComponent
      .open(toothInfo)
      .then((result: ToothInfoEventArg) => {
        var patientDenExamToothInfo: Patient_DentalExamination_ToothInfo_DTO = new Patient_DentalExamination_ToothInfo_DTO();

        patientDenExamToothInfo.ID = this.globalFx.getTempID();
        (patientDenExamToothInfo.ID_Tooth = toothInfo.ID_Tooth),
          (patientDenExamToothInfo.IDs_ToothSurface =
            result.toothInfo.IDs_ToothSurface);
        patientDenExamToothInfo.ID_ToothStatus =
          result.toothInfo.ID_ToothStatus;
        patientDenExamToothInfo.Code_ToothStatus =
          result.toothInfo.Code_ToothStatus;
        patientDenExamToothInfo.Name_ToothStatus =
          result.toothInfo.Name_ToothStatus;
        patientDenExamToothInfo.Comment = result.toothInfo.Comment;
        patientDenExamToothInfo.ToothNumber_Tooth =
          result.toothInfo.ToothNumber_Tooth;
        patientDenExamToothInfo.Location_Tooth =
          result.toothInfo.Location_Tooth;

        this.generateToothSurfacesNames(patientDenExamToothInfo);

        this.selPatientDentalExam.ToothInfos.push(patientDenExamToothInfo);

        this.setToothStatuses(tooth.ID);
      });
  }

  dataGridToothInfo_BtnEdit_onClick(
    denExamToothInfo: Patient_DentalExamination_ToothInfo_DTO
  ): void {
    var toothInfo: ToothInfo_DTO = new ToothInfo_DTO();

    toothInfo.ID_Tooth = denExamToothInfo.ID_Tooth;
    toothInfo.ToothNumber_Tooth = denExamToothInfo.ToothNumber_Tooth;
    toothInfo.Location_Tooth = denExamToothInfo.Location_Tooth;
    toothInfo.IDs_ToothSurface = denExamToothInfo.IDs_ToothSurface;
    toothInfo.ID_ToothStatus = denExamToothInfo.ID_ToothStatus;
    toothInfo.Code_ToothStatus = denExamToothInfo.Code_ToothStatus;
    toothInfo.Name_ToothStatus = denExamToothInfo.Name_ToothStatus;
    toothInfo.Comment = denExamToothInfo.Comment;

    this.toothInfoDialogComponent
      .open(toothInfo)
      .then((result: ToothInfoEventArg) => {
        denExamToothInfo.IDs_ToothSurface = result.toothInfo.IDs_ToothSurface;
        denExamToothInfo.ID_ToothStatus = result.toothInfo.ID_ToothStatus;
        denExamToothInfo.Name_ToothStatus = result.toothInfo.Name_ToothStatus;
        denExamToothInfo.Code_ToothStatus = result.toothInfo.Code_ToothStatus;
        denExamToothInfo.Comment = result.toothInfo.Comment;

        this.generateToothSurfacesNames(denExamToothInfo);
        this.setToothStatuses(denExamToothInfo.ID_Tooth);
      });
  }

  dataGridToothInfo_BtnView_onClick(
    denExamToothInfo: Patient_DentalExamination_ToothInfo_DTO
  ): void {
    var toothInfo: ToothInfo_DTO = new ToothInfo_DTO();

    toothInfo.ID_Tooth = denExamToothInfo.ID_Tooth;
    toothInfo.ToothNumber_Tooth = denExamToothInfo.ToothNumber_Tooth;
    toothInfo.Location_Tooth = denExamToothInfo.Location_Tooth;
    toothInfo.IDs_ToothSurface = denExamToothInfo.IDs_ToothSurface;
    toothInfo.ID_ToothStatus = denExamToothInfo.ID_ToothStatus;
    toothInfo.Code_ToothStatus = denExamToothInfo.Code_ToothStatus;
    toothInfo.Name_ToothStatus = denExamToothInfo.Name_ToothStatus;

    toothInfo.Comment = denExamToothInfo.Comment;

    this.toothInfoDialogComponent
      .open(toothInfo)
      .then((result: ToothInfoEventArg) => {});
  }

  dataGridToothInfo_BtnDelete_onClick(
    denExamToothInfo: Patient_DentalExamination_ToothInfo_DTO
  ): void {
    var index = this.globalFx.findIndexByKeyValue(
      this.selPatientDentalExam.ToothInfos,
      "ID",
      denExamToothInfo.ID + ""
    );

    if (isNullOrUndefined(this.selPatientDentalExam._deletedItems))
      this.selPatientDentalExam._deletedItems = {};
    if (
      isNullOrUndefined(
        this.selPatientDentalExam._deletedItems[
          "Patient_DentalExamination_ToothInfo"
        ]
      )
    ) {
      this.selPatientDentalExam._deletedItems[
        "Patient_DentalExamination_ToothInfo"
      ] = [];
    }

    this.selPatientDentalExam._deletedItems[
      "Patient_DentalExamination_ToothInfo"
    ].push(denExamToothInfo);
    this.selPatientDentalExam.ToothInfos.splice(index, 1);

    this.setToothStatuses(denExamToothInfo.ID_Tooth);
  }

  DentalChart_onToothStatusClick(eventArg: ITooth_ToothStatusArg): void {
    var denExamToothInfos = Enumerable.fromSource(
      this.selPatientDentalExam.ToothInfos
    )
      .where((r) => r.ID_Tooth == eventArg.tooth.ID)
      .where((r) => r.ID_ToothStatus == eventArg.toothStatus.ID)
      .toArray();

    if (denExamToothInfos.length == 0) return;

    var denExamToothInfo = denExamToothInfos[0];
    var toothInfo: ToothInfo_DTO = new ToothInfo_DTO();

    (toothInfo.ID_Tooth = denExamToothInfo.ID_Tooth),
      (toothInfo.IDs_ToothSurface = denExamToothInfo.IDs_ToothSurface);
    toothInfo.ID_ToothStatus = denExamToothInfo.ID_ToothStatus;
    toothInfo.Code_ToothStatus = denExamToothInfo.Code_ToothStatus;
    (toothInfo.Name_ToothStatus = denExamToothInfo.Name_ToothStatus),
      (toothInfo.ToothNumber_Tooth = eventArg.tooth.ToothNumber),
      (toothInfo.Location_Tooth = eventArg.tooth.Location),
      (toothInfo.Comment = denExamToothInfo.Comment);

    this.toothInfoDialogComponent
      .open(toothInfo)
      .then((result: ToothInfoEventArg) => {
        this.toothInfoDialogComponent.IsHideButton =
          this.selPatientDentalExam.ID_FilingStatus != FilingStatusEnum.Filed;

        denExamToothInfo.IDs_ToothSurface = result.toothInfo.IDs_ToothSurface;
        denExamToothInfo.ID_ToothStatus = result.toothInfo.ID_ToothStatus;
        denExamToothInfo.Name_ToothStatus = result.toothInfo.Name_ToothStatus;
        denExamToothInfo.Code_ToothStatus = result.toothInfo.Code_ToothStatus;
        denExamToothInfo.Comment = result.toothInfo.Comment;

        this.generateToothSurfacesNames(denExamToothInfo);
        this.setToothStatuses(denExamToothInfo.ID_Tooth);
      });
  }

  async selectboxDentition_onChange() {
    var IDs_Tooth = [];

    await this.dentalChartComponent.loadToothInfosByID(
      this.selPatientDentalExam.ID_Dentition
    );

    if (isNullOrUndefined(this.selPatientDentalExam._deletedItems))
      this.selPatientDentalExam._deletedItems = {};
    if (
      isNullOrUndefined(
        this.selPatientDentalExam._deletedItems[
          "Patient_DentalExamination_ToothInfo"
        ]
      )
    ) {
      this.selPatientDentalExam._deletedItems[
        "Patient_DentalExamination_ToothInfo"
      ] = [];
    }

    this.selPatientDentalExam.ToothInfos.forEach((denExamToothInfo) => {
      this.selPatientDentalExam._deletedItems[
        "Patient_DentalExamination_ToothInfo"
      ].push(denExamToothInfo);
    });

    this.selPatientDentalExam.ToothInfos = [];
  }

  async DentalChart_onToothStatusBtnCloseClick(
    eventArg: ITooth_ToothStatusArg
  ): Promise<void> {
    if (this.selPatientDentalExam.ID_FilingStatus != FilingStatusEnum.Filed)
      return;

    if (
      (await this.msgBox.confirm(
        `Would you like to delete ${eventArg.toothStatus.Name}?`,
        "Delete Tooth Status",
        "Yes",
        "No"
      )) == false
    )
      return;

    var denExamToothInfos = Enumerable.fromSource(
      this.selPatientDentalExam.ToothInfos
    )
      .where((r) => r.ID_Tooth == eventArg.tooth.ID)
      .where((r) => r.ID_ToothStatus == eventArg.toothStatus.ID)
      .toArray();

    if (denExamToothInfos.length == 0) return;

    var denExamToothInfo = denExamToothInfos[0];
    var index = this.globalFx.findIndexByKeyValue(
      this.selPatientDentalExam.ToothInfos,
      "ID",
      denExamToothInfo.ID + ""
    );

    if (isNullOrUndefined(this.selPatientDentalExam._deletedItems))
      this.selPatientDentalExam._deletedItems = {};
    if (
      isNullOrUndefined(
        this.selPatientDentalExam._deletedItems[
          "Patient_DentalExamination_ToothInfo"
        ]
      )
    ) {
      this.selPatientDentalExam._deletedItems[
        "Patient_DentalExamination_ToothInfo"
      ] = [];
    }

    this.selPatientDentalExam._deletedItems[
      "Patient_DentalExamination_ToothInfo"
    ].push(denExamToothInfo);

    this.selPatientDentalExam.ToothInfos.splice(index, 1);

    await this.setToothStatuses(denExamToothInfo.ID_Tooth);
  }

  async setToothStatuses(ID_Tooth: number) {
    var toothStatuses: ToothStatus[] = [];
    var toothInfos = Enumerable.fromSource(this.selPatientDentalExam.ToothInfos)
      .where((r) => r.ID_Tooth == ID_Tooth)
      .toArray();

    var IDs_ToothSurface = "";

    toothInfos.forEach((toothInfo) => {
      IDs_ToothSurface = IDs_ToothSurface + toothInfo.IDs_ToothSurface + ",";

      toothStatuses.push({
        ID: toothInfo.ID_ToothStatus,
        Code: toothInfo.Code_ToothStatus,
        Name: toothInfo.Name_ToothStatus,
      });
    });

    var IDs = IDs_ToothSurface.split(",");

    var id_s = Enumerable.fromSource(IDs)
      .where((r) => r != "")
      .distinct()
      .toArray();

    IDs_ToothSurface = id_s.toString();

    this.dentalChartComponent.setToothStatuses(ID_Tooth, toothStatuses);
    this.dentalChartComponent.setToothSurfaces(ID_Tooth, IDs_ToothSurface);
  }

  medicalHistoryQuestion_YesNoValue(event) {
    console.log(this.selPatientDentalExam.MedicalHistories);
  }

  /* Add Image */
  CurrentPatientImageFile: ImageInfo;

  async openPatientDentalImageDialog() {

    this.CurrentPatientImageFile = new ImageInfo();

    this.PatientImageDialogBoxCmp.closeOnPositiveButton = false;
    this.PatientImageDialogBoxCmp.title = "Add Image";
    this.PatientImageDialogBoxCmp.size = "md";
    this.PatientImageDialogBoxCmp.captionPositiveButton = "Save";
    await this.PatientImageDialogBoxCmp.open();
  }

  private async fAddPatientDentalExaminationImage(): Promise<any> {

    var Comment = this.CurrentPatientImageFile.Comment;
    var filename = "";
    var files: IFile[] = [];

    files.push({
      file: this.PatientImageBox.imageFile,
      dataField: 'ImageValue',
      isImage: true,
    });

    var r = await this.ds.UploadFiles(
      APP_MODEL.PATIENT_DENTALEXAMINATION_IMAGE,
      files
    );

    filename = (r.filename + "").replace("'", "");
    return new Promise<any>((resolve, reject) => {
      resolve({
        filename: filename,
        Comment: Comment,
      });
    });
  }

  patientImageValidation(): boolean{

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if(this.CurrentPatientImageFile.ImageFile == null) validationsAppForm.push({ message:"Image is required." })

    if (validationsAppForm.length > 0) {

      this.msgBox.showValidationsBox(validationsAppForm);
      return false;
    }

    return true;
  }

  async PatientImageDialogBox_onPositiveButtonClick(modal){

    var isValid = this.patientImageValidation();
    if(!isValid) return;

    var uploadResult = await this.fAddPatientDentalExaminationImage();

    var obj = await this.ds.execSP(
      "pAdd_Patient_DentalExamination_Image",
      {
        ID_Patient_DentalExamination: this.selPatientDentalExam.ID,
        ImageValue: uploadResult["filename"],
        Comment: uploadResult["Comment"],
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success == true) {
      
      modal.close();

      this.loadPatientDentalExamImages();
      this.toastr.success(`Image has been save.`, "Save Image...");

    } else {
      this.toastr.warning(`Unable to save image.`, "Saving Failed");
    }
  }

  PatientImageDialogBox_onClosed(){

    
  }
}

class ImageInfo{

  ImageFile:File;
  Comment: string;
}
