import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/utils/service/data.service';
import { AppPopupComponent } from 'src/utils/controls/appPopup/appPopup.component';
import { TokenSessionFields, UserAuthenticationService } from '../AppServices/UserAuthentication.service';
import { CrypterService } from 'src/utils/service/Crypter.service';
@Component({
  selector: 'app-Home',
  templateUrl: './Home.component.html',
  styleUrls: ['./Home.component.less']
})
export class HomeComponent implements OnInit {

  constructor(    
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    private ds: DataService) { 
      
    document.title = "VeCS - Vet. Cloud System";
  }

  currentUser: TokenSessionFields;

  imageUrl: string = '';

  async ngOnInit() {

    this.currentUser = this.userAuthSvc.getDecodedToken();

    var queryString = this.cs.encrypt(`
        SELECT 
          ImageHeaderLocationFilenamePath
        FROM vCompany
        WHERE 
          ID IN (${this.currentUser.ID_Company})
      `);

    var records = await this.ds.query<any>(queryString);

    this.imageUrl = records[0]['ImageHeaderLocationFilenamePath'];
  }
}
