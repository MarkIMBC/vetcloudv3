import { Component, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';
import { PagingOption } from '../../../utils/look-up-dialog-box/look-up-dialog-box.component';

@Component({
  selector: 'sms-list',
  templateUrl: './sms-list.component.html',
  styleUrls: [
    './../../CustomListView/BaseCustomListView.less',
    './sms-list.component.less'
  ]
})
export class SMSListComponent implements OnInit {

  showFilterBox: boolean = false;

  dataSource: any[] = [];
  selectedRecord: any = {}

  IsSentSMS_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  t.ID, 
              t.Name
      FROM    ( 
        SELECT -1 ID, 'All' Name UNION 
        SELECT 1 ID, 'Sent' Name UNION 
        SELECT 0 ID, 'Unsent' Name  
      ) t 
      /*encryptsqlend*/
    `),
  };


  menuItems: MenuItem[] = [
    {
      label: "Refresh",
      icon: "pi pi-fw pi-refresh green-text",
      command: async () => {

        await this.loadRecords();
      },
    }
  ];

  pagingOption: PagingOption = new PagingOption();
  LoadedRecordCount: number = 0;
  TotalRecordCount: number = 0;
  loading: boolean = false;
  OrderByString: string = '';

  cardBodyHeight: string = '400px';
  gridHeight: string = '400px';

  currentUser: TokenSessionFields = new TokenSessionFields();

  InitCurrentObject: any = {
    DateSending: new Date(),
    IsSentSMS: -1,
  }

  CurrentObject: any = {}

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected globalFx: GlobalfxService,
    protected elRef: ElementRef,
    protected ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
    this.resetFilter();
    this.loadRecords();
  }

  async ngAfterViewInit() {

    setTimeout(() => {

      this.resize();
    }, 2000);
  }

  resetFilter() {

    this.CurrentObject = this.globalFx.cloneObject(this.InitCurrentObject);
  }

  resize() {

    var height = window.innerHeight;

    var cardBodyHeight = 0;
    var gridHeight = 0;

    cardBodyHeight = height - 320;
    gridHeight = height - 275;

    // if(cardBodyHeight < 350){

    //   cardBodyHeight = 400;
    //   gridHeight = 400;
    // }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
    this.gridHeight = gridHeight.toString() + 'px';
  }

  toggeFilterBox() {

    this.showFilterBox = (this.showFilterBox == false);
  }

  filterOnKeyUp(event) {

    if (event.keyCode == 13) this.loadRecords();
  }

  filterOnChange(event: any) {

    this.loadRecords();
  }

  clearFilter() {

    this.resetFilter();
    this.loadRecords();
  }

  searchFilter() {

    this.loadRecords();
  }

  paginate(event: any) {

    this.pagingOption.PageNumber = event.page + 1;
    this.loadRecords();
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  *
            FROM dbo.vSMSList
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (!isNullOrUndefined(this.CurrentObject["DateSending"])) {

      var _dateStart = this.CurrentObject["DateSending"];

      filterString += `CONVERT(DATE, DateSending) = '${moment(_dateStart).format('YYYY-MM-DD')}'`;
    } else {

      filterString += `CONVERT(DATE, DateSending) = '${moment().format('YYYY-MM-DD')}'`;
    }

    if (!isNullOrUndefined(this.CurrentObject["IsSentSMS"])) {

      if (this.CurrentObject["IsSentSMS"] != '-1') {

        if (filterString.length > 0) filterString += ' AND ';
        filterString += `IsSentSMS = ${this.CurrentObject["IsSentSMS"]}`;
      }
    }

    if (!isNullOrUndefined(this.CurrentObject["Name_Client"])) {

      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Client LIKE '%${this.CurrentObject["Name_Client"]}%'`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Name_Patient"])) {

      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name_Patient LIKE '%${this.CurrentObject["Name_Patient"]}%'`;
    }

    return filterString;
  }

  btnNewTab_OnClick(record: any) {

    var routeLink = [];
    var subRoute = ''

    if(record.Oid_Model == APP_MODEL.PATIENT_SOAP_PLAN.toLowerCase()){

      subRoute = 'Patient_SOAP';
    }else if(record.Oid_Model == APP_MODEL.PATIENT_VACCINATION_SCHEDULE.toLowerCase()){

      subRoute = 'Patient_Vaccination';
    }
    else if(record.Oid_Model == APP_MODEL.PATIENT_WELLNESS_SCHEDULE.toLowerCase()){

      subRoute = 'Patient_Wellness';
    }

    routeLink = [`/Main`, subRoute, record.Parent_ID_Reference]


    var url = this.globalFx.customserializeUrl(routeLink, {});
    window.open(url, '_blank');
  }

  async getRecordPaging(query: string) {
    var columnName: string = '';


    this.loading = true;

    var obj = await this.ds.execSP(
      'pGetRecordPaging',
      {
        sql: query,
        PageNumber: this.pagingOption.PageNumber,
        DisplayCount: this.pagingOption.DisplayCount,
        OrderByString: columnName.length > 0 ? columnName : this.OrderByString,
      },
      {
        isReturnObject: true,
      }
    );

    this.loading = false;

    if (obj == null || obj == true || obj == undefined) {
      obj = {};
      obj.Records = [];
      obj.TotalRecord = 0;
      obj.TotalPageNumber = 0;
    }

    if (obj.Records == null || obj.Records == undefined) obj.Records = [];

    console.log(obj);

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;
    this.dataSource = obj.Records;

    this.TotalRecordCount = obj.TotalRecord;

    this.LoadedRecordCount =
      this.dataSource.length < this.pagingOption.DisplayCount
        ? this.dataSource.length
        : this.pagingOption.DisplayCount;
    this.LoadedRecordCount =
      this.pagingOption.DisplayCount * (this.pagingOption.PageNumber - 1) +
      this.dataSource.length;
  }

}
