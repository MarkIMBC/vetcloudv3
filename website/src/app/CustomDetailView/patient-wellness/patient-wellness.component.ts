import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_DTO,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Client, Employee, Item } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  IAppLookupBoxOption,
  AppLookupBoxComponent,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";

@Component({
  selector: "app-patient-wellness",
  templateUrl: "./patient-wellness.component.html",
  styleUrls: ["./patient-wellness.component.less"],
})
export class PatientWellnessComponent
  extends BaseCustomDetailView
  implements OnInit
{
  @ViewChild("AttendingPhysician_ID_Employee__LookUpBox")
  AttendingPhysician_ID_Employee__LookUpBox: AppLookupBoxComponent;

  attendingPersonelLenCount: number = 0;

  @ViewChild("ID_Client_LookUpBox")
  ID_Client_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  currentModelID: string = APP_MODEL.PATIENT_WELLNESS;

  CurrentObjectDetailNames: string[] = [
    Patient_WellnessDetailNames.Patient_Wellness_Schedule,
    Patient_WellnessDetailNames.Patient_Wellness_Detail,
  ];

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tPatient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];

    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;
    var obj = await this.ds.loadObject<Client>(APP_MODEL.CLIENT, ID_Client, {});

    this.CurrentObject.BillingAddress = obj.Address;
    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);
    }
  }

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name,
               Name_Position Position
        FROM vAttendingVeterinarian
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }

  ID_Item_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
                                  SELECT 
                                      ID, 
                                      Name, 
                                      UnitPrice, 
                                      FormattedCurrentInventoryCount CurrentInventoryCount,
                                      RemainingBeforeExpired
                                  FROM dbo.vActiveItem 
                                  WHERE 
                                  ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  async ID_Client_LookUpBox_onRemoveSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];
  }

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {
    var ID_Patient = event.ID;
    var obj = await this.ds.loadObject<Patient_DTO>(
      APP_MODEL.PATIENT,
      ID_Patient,
      {}
    );
  }

  async ID_Item_LookUpBox_onSelectedItem(event: any) {
    var ID_Item = event.ID;
    var obj = await this.ds.loadObject<Item_DTO>(APP_MODEL.ITEM, ID_Item, {});

    this.CurrentObject.DateExpiration = obj.OtherInfo_DateExpiration;
    this.CurrentObject.UnitCost = obj.UnitCost;
    this.CurrentObject.UnitPrice = obj.UnitPrice;
  }

  loadMenuItem() {
    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        // var result = await this.msgBox.confirm(
        //   `Would you like to cancel ${this.CurrentObject.Code}?`,
        //   "Purchase Order",
        //   "Yes",
        //   "No"
        // );
        // if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.menuItems.push(menuItemCancel);
    }
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPatient_Wellness",
        {
          IDs_Patient_Wellness: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Name_Patient} Wellness has been cancel.`,
          `Wellness`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.CurrentObject.Name_Patient} Wellness`
        );
        rej(obj);
      }
    });
  }

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var menuItem_CreateBilling: MenuItem = {
      label: "Create Billing",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          ID_Client: this.CurrentObject.ID_Client,
          ID_Patient: this.CurrentObject.ID_Patient,
          AttendingPhysician_ID_Employee:
            this.CurrentObject.AttendingPhysician_ID_Employee,
          ID_Patient_Wellness: this.CurrentObject.ID,
          BackRouteLink: [`/Main`, "Patient_Wellness", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "BillingInvoice", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_ClientInfo: MenuItem = {
      label: "Client's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_Wellness", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_PatientInfo: MenuItem = {
      label: "Patient's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_Wellness", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_PatientMedicalRecordInfo: MenuItem = {
      label: "Med. Record Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_Wellness", this.CurrentObject.ID],
        };

        routeLink = [
          `/Main`,
          "Patient_SOAP",
          this.CurrentObject.ID_Patient_SOAP,
        ];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {
      this.gotoMenuItems.push(menuItem_ClientInfo);

      this.gotoMenuItems.push(menuItem_PatientInfo);

      if (this.CurrentObject.ID_Patient_SOAP)
        this.gotoMenuItems.push(menuItem_PatientMedicalRecordInfo);

      this.gotoMenuItems.push(menuItem_CreateBilling);
    }
  }

  async CurrentObject_onLoad() {

    this.attendingPersonelLenCount = 0
    if(this.CurrentObject.AttendingPhysician == null) this.CurrentObject.AttendingPhysician = '';
    if (this.CurrentObject.AttendingPhysician) {
      this.attendingPersonelLenCount =
        this.CurrentObject.AttendingPhysician.length;
    }

    this.loadMenuItem();
    this.loadGotoMenu();

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);

    this.ID_Patient_LookUpBox.initOption();
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.AttendingPhysician_ID_Employee__LookUpBox != null && this.AttendingPhysician_ID_Employee__LookUpBox != null) {
      if (
        isNullOrUndefined(this.AttendingPhysician_ID_Employee__LookUpBox.value)
      ) {
        this.AttendingPhysician_ID_Employee__LookUpBox.value = "";
      }

      if (
        this.CurrentObject.AttendingPhysician_ID_Employee == null &&
        this.AttendingPhysician_ID_Employee__LookUpBox.value != ""
      ) {
        validations.push({
          message: "Please check if the Attending Personnel is registered.",
        });
      }

      if (
        isNullOrUndefined(this.CurrentObject.AttendingPhysician_ID_Employee)
      ) {
        validations.push({
          message: "Attending Personnel is required.",
        });
      }
    }

    this.CurrentObject.Patient_Wellness_Schedule.forEach((detail) => {
      if (detail.Date == null) {
        validations.push({
          message: `Date is required in Schedule List.`,
        });
      }
    });

    // if (this.CurrentObject.Patient_Wellness_Detail.length == 0) {
    //   validations.push({
    //     message: `Medication / Service is required.`,
    //   });
    // }

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = [
      "ID_Client",
      "ID_Patient",
      "ID_Item",
      "AttendingPhysician_ID_Employee",
      "ID_Patient_SOAP",
    ];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}

export class Patient_WellnessDetailNames {
  static readonly Patient_Wellness_Schedule: string =
    "Patient_Wellness_Schedule";
  static readonly Patient_Wellness_Detail: string = "Patient_Wellness_Detail";
}
