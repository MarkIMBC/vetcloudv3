import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  SimpleChanges,
  ViewChild,
  EventEmitter,
  Output,
} from "@angular/core";
import { Enumerable } from "linq-typescript";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { Item, Patient_Wellness_Detail } from "src/bo/APP_MODELS";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "detail-list",
  templateUrl: "./detail-list.component.html",
  styleUrls: ["./detail-list.component.less"],
})
export class DetailListComponent implements OnInit {
  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  _wellnesss: Patient_Wellness_Detail[] = [];

  @Input() IsDisabled: boolean = false;

  @Input() set Wellnesss(objs: Patient_Wellness_Detail[]) {
    this.loadMenuItems();

    if (!objs) return;

    this._wellnesss = objs;
  }

  get Wellnesss() {
    return this._wellnesss;
  }

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    this.loadMenuItems();
  }

  loadMenuItems() {
    var menuItems_AddMedications: MenuItem = {
      label: "Add Medication",
      icon: "pi pi-fw pi-plus",
      command: () => {
        this.doAddMedication();
      },
    };

    var menuItems_AddServices: MenuItem = {
      label: "Add Service",
      icon: "pi pi-fw pi-plus",
      command: () => {
        this.doAddService();
      },
    };

    this.menuItems = [];
    this.menuItems.push(menuItems_AddMedications);
    this.menuItems.push(menuItems_AddServices);
  }

  onChange() {
    this.onChanged.emit({
      data: this._wellnesss,
    });
  }

  onRowDeleting(treatment: Patient_Wellness_Detail) {
    if (this.IsDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this._wellnesss,
      "ID",
      treatment.ID + ""
    );

    this._wellnesss.splice(index, 1);

    this.onChanged.emit({
      data: this._wellnesss,
    });
  }

  browseItemInventoriable(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID,
                      Code, 
                      Name, 
                      UnitPrice, 
                      FormattedCurrentInventoryCount,
                      RemainingBeforeExpired
              FROM dbo.vItemInventoriableForBillingLookUp 
              WHERE 
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },

          {
            name: "RemainingBeforeExpired",
            caption: "Rem. Before Expire",
            width: "100px",
            align: "center",
          },
          {
            name: "UnitPrice",
            caption: "Selling Price",
            width: "150px",
            align: "right",
          },
          {
            name: "FormattedCurrentInventoryCount",
            caption: "Available Count",
            width: "150px",
            align: "center",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  browseItemService(): Promise<Item_DTO[]> {
    return new Promise<Item_DTO[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `
                SELECT ID, 
                       Code,
                       Name, 
                       UnitPrice 
                FROM dbo.vItemServiceLookUp
                WHERE
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
          {
            name: "UnitPrice",
            caption: "Price",
            width: "150px",
            align: "right",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  async doAddMedication() {
    var selectedItems = await this.browseItemInventoriable();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
      };

      this._wellnesss.push(item);
    });
  }

  async doAddService() {
    var selectedItems = await this.browseItemService();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
      };

      this._wellnesss.push(item);
    });
  }
}
