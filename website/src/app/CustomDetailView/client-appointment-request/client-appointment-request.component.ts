import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_Confinement_ItemsServices_DTO,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Client, Item, Employee } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  AppLookupBoxComponent,
  IAppLookupBoxOption,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { Enumerable } from "linq-typescript";
import { ClientCreditAdjustmentDialogBoxComponent } from "../../../utils/client-credit-adjustment/client-credit-adjustment.component";
import { ClientAppointmentTableComponent } from "./client-appointment-table/client-appointment-table.component";
import { Console } from "console";

@Component({
  selector: "client-appointment-request",
  templateUrl: "./client-appointment-request.component.html",
  styleUrls: ["./client-appointment-request.component.less"],
})
export class ClientAppointmentRequestComponent
  extends BaseCustomDetailView
  implements OnInit
{
  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  @ViewChild("clientappointmenttable")
  clientappointmenttable: ClientAppointmentTableComponent;

  ItemsServicesmenuItems: MenuItem[] = [];

  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  protected async loadCurrentModel() {
    var obj = await this.ds.execSP(
      "pGetModelByName",
      {
        name: "ClientAppointmentRequest",
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;
    this.currentModelID = this.model.Oid;

    this._ModelDisplayName = isNullOrUndefined(this.model.DisplayName)
      ? this.model.Name
      : this.model.DisplayName;

    this.ModelDisplayName = this._ModelDisplayName;
  }

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `);

    this.CurrentObject.Patient_Confinement_Patient = [];
  }

  async ID_Client_LookUpBox_onRemoveSelectedItem(event: any) {
    this.CurrentObject.Patient_Confinement_Patient = [];
  }

  protected loadInitMenu() {
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  loadMenuItem() {}

  protected formDisabled(): boolean {
    var isDisabled: boolean = true;

    return isDisabled;
  }

  async CurrentObject_onLoad() {
    this.loadMenuItem();
    this.loadGotoMenu();

    if (this.clientappointmenttable) {
      await this.clientappointmenttable.LoadByAppointment(
        this.CurrentObject.ID
      );
    }

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
          Select ID, 
                Name 
          FROM tPatient
          WHERE 
            ID_Client = ${this.CurrentObject.ID_Client} AND 
            ID_Company = ${this.currentUser.ID_Company}
      `);
    }
  }

  loadGotoMenu() {
    this.gotoMenuItems = [];
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._MenuItem_Approve);
    }
    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._MenuItem_Cancel);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var DateStart = this.CurrentObject.DateStart;
    var DateEnd = this.CurrentObject.DateEnd;
    var Title = this.CurrentObject.Name;
    var Comment = this.CurrentObject.Comment;

    if (isNullOrUndefined(this.CurrentObject.DateStart)) DateStart = 0;
    if (isNullOrUndefined(this.CurrentObject.DateEnd)) DateEnd = 0;

    if (isNullOrUndefined(this.CurrentObject.Title)) Title = 0;
    if (isNullOrUndefined(this.CurrentObject.Comment)) Comment = 0;

    if (DateStart == 0) {
      validations.push({
        message: "Date Start Time is required.",
      });
    }

    if (DateEnd == 0) {
      validations.push({
        message: "Date End  Time is required.",
      });
    }
    // if (ID_Patient == 0) {
    //   validations.push({
    //     message: "Patient is required.",
    //   });
    // }

    if (Date.parse(DateEnd) < Date.parse(DateStart)) {
      validations.push({
        message: `Start Date Time should be earlier than Start Date End.`,
      });
    }

    return Promise.resolve(validations);
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name,
               Name_Position Position
        FROM vAttendingVeterinarian
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_SOAP"])) {
      options["ID_Patient_SOAP"] = this.configOptions["ID_Patient_SOAP"];
    }

    return options;
  }

  protected _MenuItem_Approve: MenuItem = {
    label: "Approve",
    icon: "pi pi-fw pi-save blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to approve ${this.CurrentObject.Code}?`,
        "Client Appointment Request",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.approve();

      if (obj.Success) {
        await this.loadCurrentRecord();
      }
    },
  };

  /* Cancel Client Appointment Request*/

  protected _MenuItem_Cancel: MenuItem = {
    label: "Cancel",
    icon: "pi pi-fw pi-times red-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to cancel ${this.CurrentObject.Code}?`,
        "Client Appointment Request",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.cancel();

      if (obj.Success) {
        await this.loadCurrentRecord();
      }
    },
  };

  async approve(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApprove_ClientAppointmentRequest",
        {
          IDs_ClientAppointment: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been approved successfully.`,
          `Approved Appointment Request`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Approve Appointment Request`);
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancel_ClientAppointmentRequest",
        {
          IDs_ClientAppointment: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Cancelled Appointment Request`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel Client Appointment Request`
        );
        rej(obj);
      }
    });
  }

  PatientTable_onChange(e: any) {
    this.CurrentObject.Patient_Confinement_Patient = e.data;
    this.appForm.setDirty(true);
  }

  CustomDetailView_onBeforeSaving() {
    var strNames_currentIDPatients = Enumerable.fromSource(
      this.CurrentObject.Patient_Confinement_Patient
    )
      .select((r) => r["Name_Patient"])
      .toArray()
      .toString();

    this.CurrentObject.PatientNames = strNames_currentIDPatients;
  }

  /*******************************************************************/
  async CustomDetailView_onAfterSaved() {
    this.msgBox.success("Saved Successfully.", this.model.Caption);

    if (this.CurrentObject.ID_BillingInvoice) {
      //this.SaveBillingInvoiceItems();
    }
  }
}
