

import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { ClientAppointmentRequest_Patient_DTO, FilingStatusEnum, ClientAppointmentRequest_DTO } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'client-appointment-table',
  templateUrl: './client-appointment-table.component.html',
  styleUrls: ['./client-appointment-table.component.less']
})
export class ClientAppointmentTableComponent implements OnInit {

  _ID_Client_Appt: number = 0;
  Records: ClientAppointmentRequest_DTO[] = []
  SelectedRecord: ClientAppointmentRequest_Patient_DTO

  @Input() CurrentObject: any = {};
  @Input() DetailViewConfigOptions: any = {};
  @Input() IsDirty:boolean = false;
  @Input() IsDisabled:boolean = false;

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {

    this.loadMenuItems();
  }

  async LoadByAppointment(ID_ClientAppointmentRequest: number) {

    this._ID_Client_Appt = ID_ClientAppointmentRequest;

    this.loadMenuItems();

    var sql = `SELECT 
              Name_Patient,
              Comment
               FROM dbo.vClientAppointmentRequest_Patient
               WHERE 
                ID_ClientAppointmentRequest = ${this._ID_Client_Appt} 
      `;

    sql = this.cs.encrypt(sql);
    this.Records = await this.ds.query<any>(sql);

    this.loadMenuItems();

  }

  loadMenuItems() {

    var menuItems_View: MenuItem = {
      label: 'View',
      icon: 'pi pi-fw pi-search',
      command: () => {

        if (this.IsDirty) {
          this.msgBox.warning(
            `Patient Medical Record changes is not yet saved.`
          );
          return;
        }

        if (!this.SelectedRecord) return;

        var routeLink = [];
        var config = {

          prevConfig: this.DetailViewConfigOptions,
          ID_Patient_SOAP: this._ID_Client_Appt,
          BackRouteLink: [`/Main`, 'Patient', this.SelectedRecord.ID_Patient]
        };
        console.log
        routeLink = [`/Main`, 'Patient', this.SelectedRecord.ID_Patient];
        this.globalFx.customNavigate(routeLink, config);
      }
    }

    var menuItems_Refresh: MenuItem = {
      label: 'Refresh',
      icon: 'pi pi-fw pi-refresh',
      command: () => {

        this.LoadByAppointment(this._ID_Client_Appt);
      }
    }

    this.menuItems = [];
    
    if(this.CurrentObject.ID < 1) return;


    this.menuItems.push(menuItems_Refresh);
  }

  recordGrid_onDblClick() {

    if (!this.SelectedRecord) return;
    var routeLink = [];
    var config = {

      prevConfig: this.DetailViewConfigOptions,
      ID_Patient_SOAP: this._ID_Client_Appt,
      BackRouteLink: [`/Main`, 'Patient', this._ID_Client_Appt]
    };

    routeLink = [`/Main`, 'Patient', this.SelectedRecord.ID_Patient];

    this.globalFx.customNavigate(routeLink, config);
  }

  recordGrid_onRowSelect() {

    this.loadMenuItems();
  }

  recordGrid_onRowUnselect() {

    this.loadMenuItems();
  }
}