import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { appInit } from 'src/app/app.module';
import { RUNTIME_COMPILER_PROVIDERS, DynamicTemplateService } from 'src/app/AppServices/dynamic-temaplate.svc';
import { LoaderService, LoaderInterceptor } from 'src/app/AppServices/LoaderInterceptor';
import { TabService } from 'src/app/AppServices/tab.service';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ModelDirtyGuardService } from 'src/app/auth.guard';
import { DetailViewDialogService } from 'src/app/View/DetailViewDialog/DetailViewDialog.component';
import { FormHelperDialogService } from 'src/app/View/FormHelperView/FormHelperView.component';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { ValidationService } from 'src/utils/service/validation.service';
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { StepsModule } from 'primeng/steps';
import { SharedModule } from 'src/utils/shared.module';
import { TabViewModule } from 'primeng/tabview';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PatientSOAPComponent } from './patient-soap.component';
import { GenerateSoapPlanRecurringScheduleComponent } from './generate-soap-plan-recurring-schedule/generate-soap-plan-recurring-schedule.component';
import { TreatmentTableComponent } from './treatment-table/treatment-table.component';
import { PatientVaccinationTableComponent } from './patient-vaccination-table/patient-vaccination-table.component';
import { PatientVaccinationListComponent } from './patient-vaccination-list/patient-vaccination-list.component';
import { PatientWellnessListComponent } from './patient-wellness-list/patient-wellness-list.component';

const routes: Routes = [
  { path: '', component: PatientSOAPComponent }
];

@NgModule({
  declarations: [
    PatientSOAPComponent, 
    GenerateSoapPlanRecurringScheduleComponent, 
    TreatmentTableComponent, 
    PatientVaccinationTableComponent, 
    PatientVaccinationListComponent, 
    PatientWellnessListComponent
  ],
  imports: [
    CommonModule,
    MenubarModule,
    TabViewModule,
    TableModule,
    StepsModule,
    SharedModule,
    PerfectScrollbarModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ModelDirtyGuardService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    ...RUNTIME_COMPILER_PROVIDERS,
    DataService,
    ValidationService,
    DynamicTemplateService,
    TabService,
    ListViewModalService,
    DetailViewDialogService,
    FormHelperDialogService,
    UserAuthenticationService,
    CrypterService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class PatientSOAPModule { }
