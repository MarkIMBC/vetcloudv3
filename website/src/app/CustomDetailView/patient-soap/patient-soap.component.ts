import {
  APP_REPORTVIEW,
  Client,
  Employee,
  Item,
  Patient,
  Patient_SOAP,
  Patient_SOAP_Plan,
} from "./../../../bo/APP_MODELS";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import {
  AppLookupBoxComponent,
  IAppLookupBoxOption,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { MenuItem } from "primeng/api";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import {
  FilingStatusEnum,
  ItemTypeEnum,
  Item_DTO,
  Patient_SOAP_Plan_DTO,
  PropertyTypeEnum,
  LabImage,
  PositionEnum,
  Patient_SOAP_Prescription_DTO,
  Patient_SOAP_Treatment_DTO,
  Patient_Confinement_ItemsServices_DTO,
} from "src/utils/controls/class_helper";
import { isNullOrUndefined } from "util";
import { Enumerable } from "linq-typescript/build/src/Enumerables";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import * as moment from "moment";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { GenerateSoapPlanRecurringScheduleComponent } from "./generate-soap-plan-recurring-schedule/generate-soap-plan-recurring-schedule.component";
import { PatientConfinementComponent } from "../patient-confinement/patient-confinement.component";
import { PatientVaccinationTableComponent } from "./patient-vaccination-table/patient-vaccination-table.component";
import { PatientWellnessListComponent } from "./patient-wellness-list/patient-wellness-list.component";

@Component({
  selector: "app-patient-soap",
  templateUrl: "./patient-soap.component.html",
  styleUrls: ["./patient-soap.component.less"],
})
export class PatientSOAPComponent
  extends BaseCustomDetailView
  implements OnInit
{
  currentModelID: string = APP_MODEL.PATIENT_SOAP;
  planMenuItems: MenuItem[] = [];
  prescriptionMenuItems: MenuItem[] = [];
  labMenuItems: MenuItem[] = [];
  lab: MenuItem[] = [];

  @ViewChild("patientwellnesslist")
  patientwellnesslist: PatientWellnessListComponent;

  @ViewChild("generatesoapplanrecurringschedule")
  generatesoapplanrecurringschedule: GenerateSoapPlanRecurringScheduleComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;
  FILINGSTATUS_FORBILLING: number = FilingStatusEnum.ForBilling;
  FILINGSTATUS_PENDING: number = FilingStatusEnum.Pending;
  FILINGSTATUS_PARTIALLYPAID: number = FilingStatusEnum.PartiallyPaid;
  FILINGSTATUS_FULLYPAID: number = FilingStatusEnum.FullyPaid;

  imagePath: string =
    DataService.API_URL.replace("/api/", "/") + `Content/Thumbnail/`;
  imageOrgPath: string =
    DataService.API_URL.replace("/api/", "/") + `Content/Image/`;

  _treatments: Patient_SOAP_Treatment_DTO[] = [];

  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  @ViewChild("AttendingPhysician_ID_Employee__LookUpBox")
  AttendingPhysician_ID_Employee__LookUpBox: AppLookupBoxComponent;

  selPatient_SOAP_Plan: Patient_SOAP_Plan_DTO;
  selPatient_SOAP_Prescription: Patient_SOAP_Prescription_DTO;

  CaseTypeList: any[] = [];

  maxDate: Date = new Date();

  IsPatientConfinement: boolean = false;

  CurrentObjectDetailNames: string[] = [
    Patient_SOAPDetailNames.Patient_SOAP_Plan,
    Patient_SOAPDetailNames.Patient_SOAP_Treatment,
    Patient_SOAPDetailNames.Patient_SOAP_Prescription,
    Patient_SOAPDetailNames.LabImages,
  ];
  FilingStatusEnum: any;

  CustomDetailView_onInit() {
    this.formatCurrentObjectDetails();
    this.loadCaseTypeList();
  }

  async loadCaseTypeList(): Promise<void> {
    this.CaseTypeList = [];

    var sql = this.cs.encrypt(
      `SELECT 
              ID, 
              Name
        FROM dbo.tCaseType
    `
    );

    var _CaseTypeList = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      _CaseTypeList.push(obj.Name);
    });

    this.CaseTypeList = _CaseTypeList;
  }

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_SOAPType"])) {
      options["ID_SOAPType"] = this.configOptions["ID_SOAPType"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_Confinement"])) {
      options["ID_Patient_Confinement"] =
        this.configOptions["ID_Patient_Confinement"];
    }

    return options;
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name,
               Name_Position Position
        FROM vAttendingVeterinarian
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;
    var obj = await this.ds.loadObject<Client>(APP_MODEL.CLIENT, ID_Client, {});

    this.configOptions["ID_Client"] = this.CurrentObject.ID_Client;

    if (obj.Comment == null) obj.Comment = "";

    this.CurrentObject.BillingAddress = obj.Address;
    this.CurrentObject.Comment_Client = obj.Comment;
    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;
    this.CurrentObject.Comment_Patient = null;

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);
  }

  ID_Client_LookUpBox_onRemoveSelectedItem() {
    this.configOptions["ID_Client"] = this.CurrentObject.ID_Client;

    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;
    this.CurrentObject.Comment_Patient = null;
  }

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {
    var ID_Patient = event.ID;

    var obj = await this.ds.loadObject<Patient>(
      APP_MODEL.PATIENT,
      ID_Patient,
      {}
    );

    this.configOptions["ID_Patient"] = this.CurrentObject.ID_Patient;

    if (obj.Comment == null) obj.Comment = "";

    this.CurrentObject.Comment_Patient = obj.Comment;
  }

  ID_Patient_LookUpBox_onRemoveSelectedItem() {
    this.configOptions["ID_Patient"] = this.CurrentObject.ID_Patient;

    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;
    this.CurrentObject.Comment_Patient = null;
  }

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name_Client,
               Name 
        FROM vPatient 
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1 AND
          IsActive_Client = 1
    `),
  };

  ID_SOAPType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tSOAPType"),
  };

  async ID_SOAPType_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_SOAPType = event.Name;
  }

  protected loadInitMenu() {
    if (this.CurrentObject.ID_Patient_Confinement) {
      this.loadInitMenuByPatientConfinement();
      return;
    }

    if (this.CurrentObject.IsDeceased != true) {
      this.addMenuItem(this._InitMenuItem_New);
    }

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  loadInitMenuByPatientConfinement() {
    if (
      this.CurrentObject.Patient_Confinement_ID_FilingStatus ==
      FilingStatusEnum.Confined
    ) {
      this.addMenuItem(this._InitMenuItem_New);
    }

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  detailMenuPrimaryComplaint: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.History =
          this.CurrentObject.PrimaryComplaintTemplate;
      },
    },
  ];

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var menuItem_CreateBilling: MenuItem = {
      label: "Create Billing",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          ID_Client: this.CurrentObject.ID_Client,
          ID_Patient: this.CurrentObject.ID_Patient,
          AttendingPhysician_ID_Employee:
            this.CurrentObject.AttendingPhysician_ID_Employee,
          ID_Patient_SOAP: this.CurrentObject.ID,
          BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "BillingInvoice", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_ClientInfo: MenuItem = {
      label: "Client's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_ConfinementInfo: MenuItem = {
      label: "View's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
        };

        routeLink = [
          `/Main`,
          "Confinement",
          this.CurrentObject.ID_Patient_Confinement,
        ];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {
      this.gotoMenuItems.push(menuItem_ClientInfo);

      if (this.CurrentObject.ID_Patient_Confinement) {
        menuItem_ConfinementInfo.label = `Confine Record`;
        this.gotoMenuItems.push(menuItem_ConfinementInfo);
      } else {
        this.gotoMenuItems.push(menuItem_CreateBilling);
      }
    }
  }

  ObjectiveMenuItems: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.Objective = this.CurrentObject.ObjectiveTemplate;
      },
    },
  ];

  ClinicalExaminationMenuItems: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.ClinicalExamination =
          this.CurrentObject.ClinicalExaminationTemplate;
      },
    },
  ];

  DiagnosisMenuItems: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.Diagnosis = this.CurrentObject.DiagnosisTemplate;
      },
    },
  ];

  AssessmentMenuItems: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.Assessment = this.CurrentObject.AssessmentTemplate;
      },
    },
  ];

  protected _MenuItem_Approve: MenuItem = {
    label: "Approve",
    icon: "pi pi-fw pi-save blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to approve ${this.CurrentObject.Code}?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.approve();

      if (obj.Success) {
        await this.loadCurrentRecord();
      }
    },
  };

  protected _MenuItem_GenerateConfinement: MenuItem = {
    label: "Generate Confinement",
    icon: "pi fas fa-bed blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }
    },
  };

  protected _MenuItem_Cancel: MenuItem = {
    label: "Cancel",
    icon: "pi pi-fw pi-times red-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to cancel ${this.CurrentObject.Code}?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.cancel();

      if (obj.Success) {
        await this.loadCurrentRecord();
      }
    },
  };

  protected _MenuItem_Done: MenuItem = {
    label: "Done",
    icon: "pi pi-fw pi-save blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to done ${this.CurrentObject.Code}?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.done();
      //await this.SavePatient_Confinement();

      if (obj.Success) {
        await this.loadCurrentRecord();
      }
    },
  };

  protected _MenuItem_CreateConfinement: MenuItem = {
    label: "Create Confinement",
    icon: "fas fa-procedures blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var routeLink = [];
      var config = {
        prevConfig: this.configOptions,
        ID_Patient_SOAP: this.CurrentObject.ID,
        BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
      };

      routeLink = [`/Main`, "Confinement", -1];
      this.globalFx.customNavigate(routeLink, config);
    },
  };

  protected _MenuItem_SetAsForBilling: MenuItem = {
    label: "For Billing",
    icon: "fas fa-file blue-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to assign ${this.CurrentObject.Code} for billing?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      this.updateBillStatus(FilingStatusEnum.ForBilling);
    },
  };

  protected _MenuItem_UndoAsForBilling: MenuItem = {
    label: "Undo For Billing",
    icon: "fas fa-file red-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to undo for billing of ${this.CurrentObject.Code}?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      this.updateBillStatus(null);
    },
  };

  protected _MenuItem_UndoCancel: MenuItem = {
    label: "Undo Cancel",
    icon: "fas fa-file red-text",
    command: async () => {
      if (this.appForm.isDirty) {
        this.msgBox.warning(
          `${this.CurrentObject.Code} changes is not yet saved.`
        );
        return;
      }

      var result = await this.msgBox.confirm(
        `Would you like to undo cancel of ${this.CurrentObject.Code}?`,
        "SOAP",
        "Yes",
        "No"
      );
      if (!result) return;

      await this.doUndoCancel();
      this.loadCurrentRecord();
    },
  };

  protected _MenuItem_Report: MenuItem = {
    label: "Report",
    icon: "pi pi-fw pi-folder blue-text",
    items: [
      {
        label: "Patient Medical Form",
        icon: "pi pi-file-o",
        command: async () => {
          var filterFormValue: IFilterFormValue[] = [];
          var options = {
            prevConfig: this.configOptions,
            BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
            backLink: this.router.url,
          };

          filterFormValue.push({
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          });

          this.globalFx.navigateReport(
            "Patient_SOAP",
            APP_REPORTVIEW.PATIENTSOAP,
            filterFormValue,
            options
          );
        },
      },
      {
        label: "Prescription",
        icon: "pi pi-file-o",
        command: async () => {
          var filterFormValue: IFilterFormValue[] = [];
          var options = {
            prevConfig: this.configOptions,
            BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
            backLink: this.router.url,
          };

          filterFormValue.push({
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          });

          this.globalFx.navigateReport(
            "Prescription",
            APP_REPORTVIEW.PATIENTSOAPPRESCRIPTION,
            filterFormValue,
            options
          );
        },
      },
    ],
  };

  loadMenuItem() {
    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_Patient_Confinement) {
      this.loadMenuItemPatientConfinement();
      return;
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      //this.addMenuItem(this._MenuItem_Approve);
      this.addMenuItem(this._MenuItem_Done);
      this.addMenuItem(this._MenuItem_CreateConfinement);
      this.addMenuItem(this._MenuItem_Cancel);
    } else if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
    ) {
      this.addMenuItem(this._MenuItem_Done);
      this.addMenuItem(this._MenuItem_CreateConfinement);
    }

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {
      if (this.CurrentObject.BillingInvoice_ID_FilingStatus == null) {
        if (
          this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Done &&
          !this.CurrentObject.ID_Patient_Confinement
        ) {
          this.addMenuItem(this._MenuItem_Cancel);
        }

        this.addMenuItem(this._MenuItem_SetAsForBilling);
      } else if (
        this.CurrentObject.BillingInvoice_ID_FilingStatus ==
        FilingStatusEnum.ForBilling
      ) {
        this.addMenuItem(this._MenuItem_UndoAsForBilling);
      }
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._MenuItem_UndoCancel);
    }

    this.addMenuItem(this._MenuItem_Report);
  }

  loadMenuItemPatientConfinement() {
    if (this.CurrentObject.ID < 1) return;

    if (
      this.CurrentObject.ID_FilingStatus_Patient_Confinement ==
        FilingStatusEnum.Confined ||
      this.CurrentObject.ID_FilingStatus_Patient_Confinement ==
        FilingStatusEnum.Discharged
    ) {
      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(this._MenuItem_Done);
        this.addMenuItem(this._MenuItem_Cancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {
        this.addMenuItem(this._MenuItem_Done);
      }
    }
    
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
      this.addMenuItem(this._MenuItem_UndoCancel);
    }

    this.addMenuItem(this._MenuItem_Report);
  }

  async approve(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApprovePatient_SOAP",
        {
          IDs_Patient_SOAP: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been approved successfully.`,
          `Approved SOAP`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Approve SOAP`);
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPatient_SOAP",
        {
          IDs_Patient_SOAP: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Canceled SOAP`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Canceled SOAP`);
        rej(obj);
      }
    });
  }

  async done(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDonePatient_SOAP",
        {
          IDs_Patient_SOAP: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been done successfully.`,
          `Done SOAP`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Done SOAP`);
        rej(obj);
      }
    });
  }

  async doUndoCancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pUndoCancelPatient_SOAP",
        {
          IDs_Patient_SOAP: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been undo canceled successfully.`,
          `Undo Canceled SOAP`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Undo Canceled SOAP`);
        rej(obj);
      }
    });
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_Patient_Confinement) {
      return this.formDisabledPatientConfinement();
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
      isDisabled = true;
    }

    return isDisabled;
  }

  protected formDisabledPatientConfinement(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled) {
      isDisabled = true;
    }

    return isDisabled;
  }

  browseItemService(): Promise<Item_DTO[]> {
    return new Promise<Item_DTO[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `
                SELECT ID, 
                       Code,
                       Name, 
                       UnitCost,
                       UnitPrice 
                FROM dbo.vItemServiceLookUp
                WHERE
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
          {
            name: "UnitPrice",
            caption: "Price",
            width: "150px",
            align: "right",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  browseItemInventoriable(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Code,
                      Name, 
                      UnitCost,
                      UnitPrice, 
                      FormattedCurrentInventoryCount,
                      RemainingBeforeExpired
              FROM dbo.vItemInventoriableForBillingLookUp 
              WHERE 
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },

          {
            name: "RemainingBeforeExpired",
            caption: "Rem. Before Expire",
            width: "100px",
            align: "center",
          },
          {
            name: "UnitPrice",
            caption: "Selling Price",
            width: "150px",
            align: "right",
          },
          {
            name: "FormattedCurrentInventoryCount",
            caption: "Available Count",
            width: "150px",
            align: "center",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  DateReturn_SOAP_Plan_ValueChanged(soapPlan) {}

  async doAddService() {
    var selectedItems = await this.browseItemService();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
      };

      this.CurrentObject.Patient_SOAP_Plan.push(item);
    });
  }

  async doAddPrescriptionItems() {
    var selectedItems = await this.browseItemInventoriable();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
        IsCharged: true,
        Quantity: 1,
      };

      this.CurrentObject.Patient_SOAP_Prescription.push(item);
    });
  }

  Patient_SOAP_Plan_onRowDeleting(patient_SOAP_Plan: Patient_SOAP_Plan_DTO) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.Patient_SOAP_Plan,
      "ID",
      patient_SOAP_Plan.ID + ""
    );

    this.CurrentObject.Patient_SOAP_Plan.splice(index, 1);

    this.appForm.setDirty(true);
  }

  loadPlanMenuItems() {
    this.planMenuItems = [];
    if (this.appForm.isDisabled) return;

    // this.planMenuItems.push({
    //   label: "Recur Schedule",
    //   icon: "pi fa fa-calendar",
    //   command: async () => {

    //     if(this.generatesoapplanrecurringschedule){

    //       this.generatesoapplanrecurringschedule.Load(this.selPatient_SOAP_Plan)
    //     }
    //   },
    // });

    this.planMenuItems.push({
      label: "Add Services",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.doAddService();
      },
    });
  }

  Patient_SOAP_Prescription_onRowDeleting(
    patient_SOAP_Prescription: Patient_SOAP_Prescription_DTO
  ) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.Patient_SOAP_Prescription,
      "ID",
      patient_SOAP_Prescription.ID + ""
    );

    this.CurrentObject.Patient_SOAP_Prescription.splice(index, 1);

    this.appForm.setDirty(true);
  }

  loadPrescriptionMenuItems() {
    this.prescriptionMenuItems = [];
    if (this.appForm.isDisabled) return;

    this.prescriptionMenuItems.push({
      label: "Add Item",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.doAddPrescriptionItems();
      },
    });
  }

  loadLabMenuItems() {
    this.labMenuItems = [];
    if (this.appForm.isDisabled) return;

    this.labMenuItems.push({
      label: "Add",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.doAddLaboratoryImage();
      },
    });
  }

  _LabImageRowIndex: number = 0;

  doAddLaboratoryImage() {
    var i = 1;
    var isNotFind = false;
    var imgNo = "";

    this._LabImageRowIndex++;

    if (this.CurrentObject.LabImages.length == 0) {
      this.CurrentObject.LabImages.unshift({
        RowIndex: this._LabImageRowIndex,
        ImageNo: "01",
      });

      this.CurrentObject["LabImageRowIndex01"] = this._LabImageRowIndex;
    } else {
      for (i = 1; i <= 15; i++) {
        if (isNotFind == false) {
          var _imgNo = i.toString();
          if (_imgNo.length == 1) _imgNo = "0" + _imgNo;

          var index = this.globalFx.findIndexByKeyValue(
            this.CurrentObject.LabImages,
            "ImageNo",
            _imgNo
          );

          if (index < 0) {
            var obj = {
              RowIndex: this._LabImageRowIndex,
              ImageNo: _imgNo,
            };

            this.CurrentObject["LabImageRowIndex" + _imgNo] = obj.RowIndex;
            this.CurrentObject.LabImages.unshift(obj);

            isNotFind = true;
          }
        }
      }
    }
  }

  btnNewTab_OnClick() {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
    };

    routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];

    var url = this.globalFx.customserializeUrl(routeLink, config);

    window.open(url, "_blank");
  }

  btnfillTemplate_OnClick(labImage: LabImage) {
    this.CurrentObject["LabImageRemark" + labImage.ImageNo] =
      this.CurrentObject.LaboratoryTemplate;
  }

  btnLabImageRemove(labImage: LabImage) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.LabImages,
      "ImageNo",
      labImage.ImageNo
    );
    this.CurrentObject.LabImages.splice(index, 1);

    this.CurrentObject["LabImageFilePath" + labImage.ImageNo] = "";
    this.CurrentObject["LabImageRemark" + labImage.ImageNo] = "";
    this.CurrentObject["LabImageRowIndex" + labImage.ImageNo] = null;

    this.appForm.setDirty(true);
  }

  async btnLabImageSendEmail(labImage: LabImage) {
    var result = await this.msgBox.confirm(
      `Would you like to send selected laboratory result to ${this.CurrentObject.Name_Client}?`,
      `Sending Laboratory Result thru Email`,
      "Yes",
      "No"
    );

    if (!result) return;
  }

  isFiled: boolean = false;

  async CurrentObject_onLoad() {
    this.loadGotoMenu();
    this.loadMenuItem();
    this.loadPlanMenuItems();
    this.loadPrescriptionMenuItems();
    this.loadLabMenuItems();

    if (this.patientwellnesslist) {
      await this.patientwellnesslist.LoadBySOAP(this.CurrentObject.ID);
    }

    this.IsPatientConfinement =
      this.CurrentObject.ID_Patient_Confinement != null &&
      this.CurrentObject.ID_Patient_Confinement != 0;

    if (this.CurrentObject.LabImages.length > 0) {
      this._LabImageRowIndex = this.CurrentObject.LabImages[0].ImageRowIndex;
    }

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);

    this.ID_Patient_LookUpBox.initOption();

    this.isFiled = this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed;

    this.configOptions["ID_Client"] = this.CurrentObject.ID_Client;
  }

  CustomDetailView_onBeforeSaving() {
    if (isNullOrUndefined(this.CurrentObject.Planning))
      this.CurrentObject.Planning = "";

    if (this.CurrentObject.Planning.length > 0) {
      this.CurrentObject.Patient_SOAP_Plan = [];
    }

    this.CurrentObject.Patient_SOAP_Plan.forEach((soapPlan) => {
      this.PreviousObject.Patient_SOAP_Plan.forEach((prevSOAPPlan) => {
        if (soapPlan.ID == prevSOAPPlan.ID) {
          var date1 = moment(soapPlan.DateReturn).format(
            "MMMM Do YYYY, h:mm a"
          );
          var date2 = moment(prevSOAPPlan.DateReturn).format(
            "MMMM Do YYYY, h:mm a"
          );

          if (date1 !== date2) soapPlan.IsSentSMS = false;
        }
      });
    });
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (isNullOrUndefined(this.AttendingPhysician_ID_Employee__LookUpBox.value))
      this.AttendingPhysician_ID_Employee__LookUpBox.value = "";

    if (
      this.CurrentObject.AttendingPhysician_ID_Employee == null &&
      this.AttendingPhysician_ID_Employee__LookUpBox.value != ""
    ) {
      validations.push({
        message: "Please check if the Attending Personnel is registered.",
      });
    }

    if (isNullOrUndefined(this.CurrentObject.AttendingPhysician_ID_Employee)) {
      validations.push({
        message: "Attending Personnel is required.",
      });
    }

    this.CurrentObject.Patient_SOAP_Plan.forEach((detail) => {
      if (detail.DateReturn == null) {
        validations.push({
          message: `Date Return is required in ${detail.Name_Item}.`,
        });
      }
    });

    this.CurrentObject.Patient_SOAP_Treatment.forEach((detail) => {
      if (detail.Quantity == null || detail.Quantity == 0) {
        validations.push({
          message: `Quantity is required ${detail.Name_Item} (Treatment Tab).`,
        });
      }
    });

    this.CurrentObject.Patient_SOAP_Prescription.forEach((detail) => {
      if (detail.Quantity == null || detail.Quantity == 0) {
        validations.push({
          message: `Quantity is required ${detail.Name_Item} (Prescription Tab).`,
        });
      }
    });

    return Promise.resolve(validations);
  }

  Code_Patient_Confinement_Textbox_onCLick() {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
    };

    routeLink = [
      `/Main`,
      "Confinement",
      this.CurrentObject.ID_Patient_Confinement,
    ];

    var url = this.globalFx.customNavigate(routeLink, config);
  }

  async updateBillStatus(filingStatus: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pUpdatePatientSOAPBillStatus",
        {
          IDs_Patient_SOAP: [this.CurrentObject.ID],
          BillingInvoice_ID_FilingStatus: filingStatus,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `Bill Status has been change successfully.`,
          `Bill Status`
        );

        await this.loadCurrentRecord();

        res(obj);
      } else {
        this.msgBox.error(obj.message, `Failed to Change Bill Status`);
        rej(obj);
      }
    });
  }

  TreatmentTable_onChanged(e: any) {
    this.CurrentObject.Patient_SOAP_Treatment = e.data;
    this.appForm.setDirty(true);
  }

  /*******************************************************************/
  async CustomDetailView_onAfterSaved() {
    this.msgBox.success("Save successfully", this.model.Caption);

    if (this.CurrentObject.ID_Patient_Confinement) {
      //this.SavePatient_Confinement();
    }
  }

  async SavePatient_Confinement() {
    if (this.CurrentObject.ID_Patient_Confinement == null) return;
    if (this.CurrentObject.ID_Patient_Confinement < 1) return;

    await this.loadCurrentRecord();

    var confinementComponent: PatientConfinementComponent =
      new PatientConfinementComponent(
        this.ds,
        this.globalFx,
        this.msgBox,
        this.userAuthSvc,
        this.lvModal,
        this.route,
        this.router,
        this.cs,
        this.toastr,
        this.loaderService,
        this.validationService,
        this.dvFormSvc,
        this.activeRoute
      );

    /*Saving Treatment and Prescription*/
    await confinementComponent.LoadCurrentObjectRecord(
      this.CurrentObject.ID_Patient_Confinement
    );
    if (this.CurrentObject.Patient_Confinement_ItemsServices == null)
      this.CurrentObject.Patient_Confinement_ItemsServices = [];

    if (confinementComponent.CurrentObject.ID == -1) return;
    if (confinementComponent.CurrentObject.ID == null) return;
    if (
      confinementComponent.CurrentObject.ID_FilingStatus ==
      FilingStatusEnum.Discharged
    )
      return;

    /*Delete All Items and Services From SOAP*/
    var objs = Enumerable.fromSource(
      confinementComponent.CurrentObject.Patient_Confinement_ItemsServices
    )
      .where((r) => r["ID_Patient_SOAP"] == this.CurrentObject.ID)
      .toArray();

    objs.forEach((obj) => {
      var index = this.globalFx.findIndexByKeyValue(
        confinementComponent.CurrentObject.Patient_Confinement_ItemsServices,
        "ID",
        obj["ID"] + ""
      );
      confinementComponent.CurrentObject.Patient_Confinement_ItemsServices.splice(
        index,
        1
      );
    });

    /*Save Prescription Items*/
    this.CurrentObject.Patient_SOAP_Prescription.forEach((prescription) => {
      if (prescription.IsCharged) {
        var itemService: Patient_Confinement_ItemsServices_DTO =
          new Patient_Confinement_ItemsServices_DTO();

        itemService.Date = this.CurrentObject.Date;
        itemService.ID_Patient_SOAP = this.CurrentObject.ID;
        itemService.ID_Patient_SOAP_Prescription = prescription.ID;
        itemService.ID_Item = prescription.ID_Item;
        itemService.Quantity = prescription.Quantity;
        itemService.UnitPrice = prescription.UnitPrice;
        itemService.UnitCost = prescription.UnitCost;
        itemService.Comment = prescription.Comment;
        itemService.DateExpiration = prescription.DateExpiration;

        confinementComponent.CurrentObject.Patient_Confinement_ItemsServices.push(
          itemService
        );
      }
    });

    /*Save Treatment Items and Services*/
    this.CurrentObject.Patient_SOAP_Treatment.forEach((treatment) => {
      var itemService: Patient_Confinement_ItemsServices_DTO =
        new Patient_Confinement_ItemsServices_DTO();

      itemService.Date = this.CurrentObject.Date;
      itemService.ID_Patient_SOAP = this.CurrentObject.ID;
      itemService.ID_Patient_SOAP_Treatment = treatment.ID;
      itemService.ID_Item = treatment.ID_Item;
      itemService.Quantity = treatment.Quantity;
      itemService.UnitPrice = treatment.UnitPrice;
      itemService.UnitCost = treatment.UnitCost;
      itemService.Comment = treatment.Comment;
      itemService.DateExpiration = treatment.DateExpiration;

      confinementComponent.CurrentObject.Patient_Confinement_ItemsServices.push(
        itemService
      );
    });

    /*Reorder By Date Inclined*/
    confinementComponent.CurrentObject.Patient_Confinement_ItemsServices =
      Enumerable.fromSource(
        confinementComponent.CurrentObject.Patient_Confinement_ItemsServices
      )
        .orderBy(function (x) {
          return x["Date"];
        })
        .toArray();

    confinementComponent.compute();
    await confinementComponent.save();
    await confinementComponent.CustomDetailView_onAfterSaved();
  }

  Prescription_onControlValueChanged() {
    this.appForm.setDirty(true);
  }

  Plan_onControlValueChanged() {
    this.appForm.setDirty(true);
  }

  clientName_OnClick() {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
    };

    routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
    this.globalFx.customNavigate(routeLink, config);
  }

  patientName_OnClick() {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
    };

    routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];
    this.globalFx.customNavigate(routeLink, config);
  }
}
export class Patient_SOAPDetailNames {
  static readonly Patient_SOAP_Plan?: string = "Patient_SOAP_Plan";
  static readonly Patient_SOAP_Treatment?: string = "Patient_SOAP_Treatment";
  static readonly Patient_SOAP_Prescription?: string =
    "Patient_SOAP_Prescription";
  static readonly LabImages?: string = "LabImages";
}
