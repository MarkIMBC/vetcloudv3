import { KeyCode } from "@ng-select/ng-select/lib/ng-select.types";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import {
  UserAuthenticationService,
  TokenSessionFields,
} from "../AppServices/UserAuthentication.service";
import { ListViewModalService } from "../View/ListView/ListView.service";
import {
  ActivatedRoute,
  ActivationEnd,
  NavigationEnd,
  NavigationStart,
  Router,
} from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { ToastrService } from "ngx-toastr";
import {
  BillingInvoice_DTO,
  FilingStatusEnum,
  IFile,
} from "src/utils/controls/class_helper";
import { Model } from "src/bo/APP_MODELS";
import { Component, OnInit, ViewChild } from "@angular/core";
import { AppFormComponent } from "src/utils/controls/appForm/appForm.component";
import { MenuItem } from "primeng/api";
import { isNull, isNullOrUndefined } from "util";
import * as moment from "moment";
import { BaseAppControl, BaseAppControlOption } from "src/utils/controls";
import { Breadcrumb } from "primeng/breadcrumb";
import { IControlValueChanged } from "src/utils/controls/_base/baseAppControl";
import { LoaderService } from "../AppServices/LoaderInterceptor";
import { ValidationService } from "src/utils/service/validation.service";
import { FormHelperDialogService } from "../View/FormHelperView/FormHelperView.component";
import { DetailViewFooterComponent } from "src/utils/detail-view-footer/detail-view-footer.component";

@Component({
  template: "",
})
export abstract class BaseCustomDetailView implements OnInit {
  currentModelID: string = "";

  public __ID_CurrentObject: number = 0;

  qrCodeValue: string = "";
  currentRouterLink: string = "";

  isLoading: boolean = false;

  breadCrumbs: MenuItem[] = [];
  homeBreadCrumb: MenuItem = { icon: "pi pi-home", routerLink: "/Main" };
  gotoMenuItems: MenuItem[] = [];

  hasBackLinkConfigOption: boolean = true;

  CurrentObjectFiles: IFile[] = [];

  ModelDisplayName: string = "";

  footerLeftMessageHtml: String = "";
  footerRightMessageHtml: String = "";

  CurrentObject: any = {};
  PreviousObject: any = {};

  configOptions: any = {};

  CurrentObjectDetailNames: string[] = [];

  model: Model;
  currentUser: TokenSessionFields;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  @ViewChild("detailviewfooter")
  detailviewfooter: DetailViewFooterComponent;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    protected loaderService: LoaderService,
    protected validationService: ValidationService,
    protected dvFormSvc: FormHelperDialogService,
    protected activeRoute: ActivatedRoute
  ) {
    this.msgBox.setToastTimeOut(5000);
    this.currentUser = this.userAuthSvc.getDecodedToken();
    this._formatCurrentObjectDetails(this.PreviousObject, this.CurrentObject);

    this.gotoMenuItems = [];
  }

  private _subscribeActiveRoute: any;

  async ngOnInit(): Promise<void> {
    this._subscribeActiveRoute = this.activeRoute.params.subscribe(
      async (routeParams) => {
        /*Check and Log out by Server User Session Status*/
        if (
          (await this.userAuthSvc.CheckAndLogOutByUserSessionStatus()) != true
        ) {
          return;
        }

        this.loaderService.isLoading.subscribe((r) => {
          this.isLoading = r;
        });

        this.loadConfigOption();

        this.__ID_CurrentObject = await this._getDefault__ID_CurrentObject();

        try {
          await this.loadCurrentModel();
          await this.CustomDetailView_onInit();

          await this.loadCurrentRecord();
        } catch (error) {
          this.msgBox.warning(
            "Unable to load record. Page is now refreshing.",
            this.model.Caption
          );

          this.reloadCurrentRecordPage();
        }

        if (this.CurrentObject["ID_Company"] != undefined) {
          if (this.CurrentObject.ID_Company != this.currentUser.ID_Company) {
            this.router.navigate(["/Main", "NoAccess"]);
            return;
          }
        }
      }
    );
  }
  ngOnDestroy(): void {
    this._subscribeActiveRoute.unsubscribe();
  }

  setqrCodeValue() {
    var value = `{ 	"ID_CurrentObject": "${this.CurrentObject.ID}", 	"ID_Model": "${this.currentModelID}" }`;

    this.qrCodeValue = value;
  }

  protected async _getDefault__ID_CurrentObject() {
    return parseInt(this.activeRoute.snapshot.params["ID_CurrentObject"]);
  }

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params["configOptions"];

    if (isNullOrUndefined(configOptionsString)) return;

    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);

    console.log(this.configOptions);

    this.hasBackLinkConfigOption = !isNullOrUndefined(
      this.configOptions["BackRouteLink"]
    );
  }

  public async LoadCurrentObjectRecord(ID_CurrentObject: number) {
    this.__ID_CurrentObject = ID_CurrentObject;
    await this.loadCurrentModel();
    await this.loadRecord(ID_CurrentObject);
  }

  isLoadCurrrentObject: boolean = true;

  async loadRecord(ID_CurrentObject: number) {
    try {
      this.isLoadCurrrentObject = true;

      var recordOptions = await this.pGetRecordOptions();

      recordOptions["ID"] = ID_CurrentObject;
      recordOptions["ID_Session"] = this.currentUser.ID_UserSession;

      var obj = await this.ds.execSP(`pGet${this.model.Name}`, recordOptions, {
        isReturnObject: true,
      });

      this.PreviousObject = this.globalFx.cloneObject(obj);

      obj = await this.getInitCurrentObject(obj);
      this.CurrentObject = this.globalFx.cloneObject(obj);

      this._formatCurrentObjectDetails(this.PreviousObject, this.CurrentObject);

      if (!isNullOrUndefined(this.appForm)) this.appForm.setDirty(false);

      this.menuItems = [];
      this.loadInitMenu();

      if (!isNullOrUndefined(this.detailviewfooter)) {
        if (this.detailviewfooter) {
          this.detailviewfooter.reload(this.CurrentObject);
        }
      }

      var isformDisabled = this.formDisabled();
      if (!isNullOrUndefined(this.appForm))
        this.appForm.setFormDisabled(isformDisabled);

      await this.CurrentObject_onLoad();

      if (ID_CurrentObject > 0) this.setqrCodeValue();

      this.isLoadCurrrentObject = false;
    } catch (error) {
      console.log(error);
      this.reloadCurrentRecordPage();
    }
  }

  protected getInitCurrentObject(obj): any {
    return obj;
  }

  protected pGetRecordOptions(): any {
    return {};
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    return isDisabled;
  }

  protected _InitMenuItem_New: MenuItem = {
    label: "New",
    icon: "pi pi-fw pi-plus",
    command: async () => {
      var result = await this.msgBox.confirm(
        "Would you like to create new record?",
        `Create ${
          isNull(this.model.DisplayName)
            ? this.model.Name
            : this.model.DisplayName
        }`,
        "Yes",
        "No"
      );

      if (!result) return;

      this.gotoSelfNavigate(-1);

      this.loadNewRecord();
    },
  };

  protected gotoSelfNavigate(ID_CurrentObject) {
    if (isNullOrUndefined(this.route.snapshot.params["ID_CurrentObject"]))
      return;

    var routeLink = [];
    routeLink = [`/Main`, this.model.Name, ID_CurrentObject];

    this.globalFx.customNavigate(routeLink, this.configOptions);
  }

  protected _InitMenuItem_Refresh: MenuItem = {
    label: "Refresh",
    icon: "pi pi-fw pi-refresh green-text",
    command: async () => {
      if (this.CurrentObject.ID < 0) return;

      var result = await this.msgBox.confirm(
        "Would you reload current record?",
        `Refresh ${
          isNull(this.model.DisplayName)
            ? this.model.Name
            : this.model.DisplayName
        } Record`,
        "Yes",
        "No"
      );

      if (!result) return;

      this.loadCurrentRecord();
    },
  };

  public _IsSaving: boolean = false;

  protected _InitMenuItem_Save: MenuItem = {
    label: "Save",
    icon: "pi pi-fw pi-save green-text",
    command: async () => {
      var _ = this;
      if (_._IsSaving == true) {
        return;
      }

      _.CustomDetailView_onBeforeSaving();

      _._IsSaving = true;

      var isvalid = await _.validateRecord();
      if (!isvalid) {
        _._IsSaving = false;
        return;
      }

      var isSaving = await _.confirmSaving();
      if (!isSaving) {
        _._IsSaving = false;
        return;
      }

      await _.doSaving();
      _._IsSaving = false;
    },
  };

  async doSaving(): Promise<any> {
    var _ = this;

    if (_._IsSaving != true) {
      return;
    }

    return await this.save()
      .then(async (r) => {
        try {
          _._IsSaving = false;

          await _.CustomDetailView_onAfterSaved();

          console.log("Save Result", r.message);

          if (r.key != _.__ID_CurrentObject) {
            console.log("redirect route");
            _.gotoSelfNavigate(r.key);
          } else {
            console.log("load current record");
            _.loadRecord(r.key);
          }
        } catch (error) {
          _._IsSaving = false;
          _.reloadCurrentRecordPage();
        }
      })
      .catch((reason) => {
        console.log(reason);
        _.msgBox.error(
          "Unable to save record. Please try again.",
          this.model.Caption
        );

        _._IsSaving = false;
      });
  }

  private async reloadCurrentRecordPage() {
    var config: any[] = this.globalFx.getCustomNavigateCommands(
      ["Main", this.model.Name, this.__ID_CurrentObject],
      this.configOptions
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.location.href = location.origin + url;
  }

  public async confirmSaving(): Promise<boolean> {
    var options = {
      message: "Do you want to save changes?",
      title: this.ModelDisplayName,
      positiveButtonText: "Yes",
      negativeButtonText: "No",
      backdrop: false,
    };

    var result = await this.msgBox.showConfirmBox(options);

    return result;
  }

  public async validateRecord() {
    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    if (!isNullOrUndefined(this.appForm)) {
      validationsAppForm = await this.getFormValidations();
    }

    CustomValidations.forEach((customValidation) => {
      validationsAppForm.push(customValidation);
    });

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  public async save(): Promise<any> {
    var CurrentObject = this.CurrentObject;
    var PreviousObject = this.PreviousObject;
    var model = this.model;
    var currentUser = this.currentUser;
    var currentFiles: IFile[] = [];

    this.CurrentObjectFiles = [];

    if (this.appForm != undefined) {
      this.appForm.controls.forEach((i) => {
        if (i["imageFile"]) {
          this.CurrentObjectFiles.push({
            file: i["imageFile"],
            dataField: i.name,
            isImage: true,
          });
        }
      });
    }
    currentFiles = this.CurrentObjectFiles;

    if (
      CurrentObject["ID_Company"] == null &&
      CurrentObject["ID_Company"] == undefined &&
      CurrentObject.ID < 1
    ) {
      CurrentObject.ID_Company = currentUser.ID_Company;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      this.ds
        .saveObject(
          model.Oid,
          CurrentObject,
          PreviousObject,
          currentFiles,
          currentUser
        )
        .then(async (r) => {
          if (r.key != undefined) {
            var id = (r.key + "").replace("'", "");
            var ID_CurrentObject = parseInt(id);

            await this.CustomDetailView_onAfterSaving();

            resolve(r);
          } else {
            reject("Saving Failed....");
          }
        })
        .catch((reason) => {
          reject(reason);
        });
    });

    return promise;
  }

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);
    this.addMenuItem(this._InitMenuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  protected addMenuItem(item: MenuItem) {
    this.menuItems.push(item);
  }

  private async loadNewRecord() {
    this.__ID_CurrentObject = -1;

    await this.loadRecord(this.__ID_CurrentObject);
  }

  async loadCurrentRecord() {
    await this.loadRecord(this.__ID_CurrentObject);
  }

  protected formatCurrentObjectDetails() {
    this._formatCurrentObjectDetails(this.PreviousObject, this.CurrentObject);
  }

  protected _formatCurrentObjectDetails(
    previousbject: any,
    currentObject: any
  ) {
    this.CurrentObjectDetailNames.forEach((name) => {
      if (isNullOrUndefined(currentObject[name])) currentObject[name] = [];

      if (isNullOrUndefined(previousbject[name])) previousbject[name] = [];

      if (currentObject[name] == undefined) currentObject[name] = [];

      if (previousbject[name] == undefined) previousbject[name] = [];
    });
  }

  protected _ModelDisplayName: string = "";

  protected async loadCurrentModel() {
    var obj = await this.ds.execSP(
      "pGetModel",
      {
        Oid: this.currentModelID,
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;

    this._ModelDisplayName = isNull(this.model.DisplayName)
      ? this.model.Name
      : this.model.DisplayName;

    this.ModelDisplayName = this._ModelDisplayName;
  }

  menuItems: MenuItem[] = [];

  private initMenus: MenuItem[] = [];

  private async getFormValidations(): Promise<IFormValidation[]> {
    var validations = this.appForm.getValidations();
    return Promise.resolve(validations);
  }

  protected async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  private initControls: any[] = [];

  ngAfterViewInit() {
    this.appForm.controls.forEach((c) => {
      this.initControls.push({
        name: c.name,
        readOnly: c.readOnly,
      });
    });

    this.appForm.initControls = this.initControls;
    //console.log('ngAfterViewInit', this.initControls);

    this.CustomDetailView_onLoad();
  }

  btnBack_onClick() {
    var routeLink = [];
    var prevConfig;

    if (!isNullOrUndefined(this.configOptions["prevConfig"])) {
      prevConfig = this.configOptions["prevConfig"];
    }

    if (isNullOrUndefined(this.configOptions["BackRouteLink"])) return;

    routeLink = this.configOptions["BackRouteLink"];
    this.globalFx.customNavigate(routeLink, prevConfig);
  }

  CustomDetailView_onInit() {}

  CustomDetailView_onLoad() {}

  CurrentObject_onLoad() {}

  CustomDetailView_onBeforeSaving() {}

  CustomDetailView_onAfterSaving() {}

  async CustomDetailView_onAfterSaved() {
    this.msgBox.success("Saved Successfully.", this.model.Caption);
  }
}
