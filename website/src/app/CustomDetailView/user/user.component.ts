import { MenuItem } from "primeng/api";
import {
  UserRole_DTO,
  User_DTO,
  User_Role_DTO,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { APP_MODEL } from "src/bo/APP_MODELS";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";
import { Enumerable } from "linq-typescript";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.less"],
})
export class UserComponent extends BaseCustomDetailView implements OnInit {
  @ViewChild("lookUpDialogBox")
  lookUpDialogBoxCmp: LookUpDialogBoxComponent;

  CurrentObject: User_DTO;

  currentModelID: string = APP_MODEL.USER;

  selUser_Role: User_Role_DTO;

  CurrentObjectDetailNames: string[] = [UserNames.User_Roles];

  browseUserRoles(): Promise<UserRole_DTO[]> {
    var idsString = Enumerable.fromSource(this.CurrentObject.User_Roles)
      .select((r) => r.ID_UserRole)
      .distinct()
      .toArray()
      .toString();

    return new Promise<UserRole_DTO[]>(async (res, rej) => {
      var selectedRecords = await this.lookUpDialogBoxCmp.open<UserRole_DTO>({
        sql: `SELECT 
                    ID,
                    Name
              FROM dbo.tUserRole 
              ${
                idsString.length == 0
                  ? ""
                  : "WHERE ID NOT IN (" + idsString + ")"
              }
        `,
        height: "300px",
        filterColumns: [{ name: "Name", caption: "Name" }],
        columns: [
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => "'" + r.ID + "'")
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  
                *
          FROM tUserRole
          WHERE 
                ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<UserRole_DTO>(queryString);

        res(items);
      }
    });
  }

  user_RoleMenu: MenuItem[] = [
    {
      label: "Add Role",
      icon: "fa fa-plus",
      command: async () => {
        var records = await this.browseUserRoles();

        records.forEach((record) => {
          var obj: User_Role_DTO = {
            ID: this.globalFx.getTempID(),
            ID_UserRole: record.ID,
            Name_UserRole: record.Name,
            IsActive: true,
          };

          this.CurrentObject.User_Roles.push(obj);
        });
      },
    },
  ];

  ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      SELECT ID,
             Name
      FROM tEmployee
      WHERE 
        ID_Company = ${this.currentUser.ID_Company}
    `),
  };

  btnDeleteUser_Role_onClick(user_Role: User_Role_DTO) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.User_Roles,
      "ID",
      user_Role.ID + ""
    );

    this.CurrentObject.User_Roles.splice(index, 1);
  }
}

export class UserNames {
  static readonly User_Roles?: string = "User_Roles";
}
