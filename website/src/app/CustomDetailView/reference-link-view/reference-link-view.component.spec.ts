import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceLinkViewComponent } from './reference-link-view.component';

describe('ReferenceLinkViewComponent', () => {
  let component: ReferenceLinkViewComponent;
  let fixture: ComponentFixture<ReferenceLinkViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceLinkViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceLinkViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
