import { DataService } from "./../../../utils/service/data.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { CrypterService } from "src/utils/service/Crypter.service";

@Component({
  selector: "app-reference-link-view",
  templateUrl: "./reference-link-view.component.html",
  styleUrls: ["./reference-link-view.component.less"],
})
export class ReferenceLinkViewComponent implements OnInit {
  currentUser: TokenSessionFields;
  constructor(
    protected ds: DataService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuthSvc.getDecodedToken();

    var Code = this.route.snapshot.params["Code"];

    var objReferenceMenuItems = await this.ds.execSP(
      "pGetReferenceLinkMenuItem",
      { Name: Code },
      {
        isReturnObject: true,
      }
    );

    setTimeout(function () {
      if (objReferenceMenuItems) {
        var obj = objReferenceMenuItems.MenuItems[0];

        document.location.href = obj.URLlink;
      }
    }, 3000);
  }
}
