import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReferenceLinkViewComponent } from './reference-link-view.component';


const routes: Routes = [
  { path: '', component: ReferenceLinkViewComponent }
];

@NgModule({
  declarations: [ReferenceLinkViewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ReferenceLinkViewModule { }
