import { Company } from './../../../bo/APP_MODELS';
import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { isNullOrUndefined, isNumber } from "util";
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.less']
})
export class CompanyComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.COMPANY;
  
  @ViewChild('appForm')
  appForm: AppFormComponent;

  _IsCompanyInfo: boolean = false;

  protected async _getDefault__ID_CurrentObject(){

    if(!isNullOrUndefined(this.route.snapshot.params["ID_CurrentObject"])){

      return parseInt(
        this.route.snapshot.params["ID_CurrentObject"]
      );
    }else{

      this._IsCompanyInfo = true;
      this.hasBackLinkConfigOption = false;

      return this.currentUser.ID_Company;
    }
  }

  protected loadInitMenu() {

    if(!this._IsCompanyInfo){

      this.addMenuItem(this._InitMenuItem_New);
      this.addMenuItem(this._InitMenuItem_Save);
      this.addMenuItem(this._InitMenuItem_Refresh);
    }else{

      this.addMenuItem(this._InitMenuItem_Save);
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  private isCompanyNameChanged: boolean = false

  CustomDetailView_onBeforeSaving() {

    if(this.CurrentObject.Name != this.PreviousObject.Name) this.isCompanyNameChanged = true
  }

  CurrentObject_onLoad() {

  }

  async CustomDetailView_onAfterSaved() {

    if(this._IsCompanyInfo && this.isCompanyNameChanged){

      this.msgBox.success("Updating Company Information.<br/>Now in Page Refreshing...", this.model.Caption);
    
      setTimeout(function(){ 

        window.location.href = window.location.href;
      }, 3000);

    }else{

      this.msgBox.success("Save successfully", this.model.Caption);
    }
  }
}
