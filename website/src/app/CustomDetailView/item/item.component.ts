import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import { DialogBoxComponent } from "./dialog-box/dialog-box.component";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.less"],
})
export class ItemComponent extends BaseCustomDetailView implements OnInit {
  currentModelID: string = APP_MODEL.ITEM;

  CurrentObjectDetailNames: string[] = [ItemDetailNames.Item_Supplier];

  @ViewChild("dialogbox")
  addItemInventoryDialogBoxCmp: DialogBoxComponent;

  ItemInventory: ReceiveInventory;

  ID_ItemType_Service: number = ItemTypeEnum.Service;
  ID_ItemType_Inventoriable: number = ItemTypeEnum.Inventoriable;

  Current_ID_ItemType: number;

  UnitPriceCaption: string = "";

  ID_ItemType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tItemType Order By Name ASC"),
  };

  async ID_ItemType_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_ItemType = event.Name;
  }

  ID_ItemCategory_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(
      `Select ID, Name FROM tItemCategory WHERE ID_ItemType = ${this.CurrentObject.ID_ItemType} Order By Name ASC`
    ),
  };

  async ID_ItemCategory_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_ItemCategory = event.Name;
  }

  ItemList: any[] = [];

  async loadExistingItem(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT DISTINCT
              LTRIM(RTRIM(Name)) Name
        FROM dbo.tItem
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND
          ID_ItemType = ${this.CurrentObject.ID_ItemType} AND
          IsActive = 1
    `
    );

    var _ItemList = [];

    var objs = await this.ds.query<any>(sql);

    this.ItemList = [];
    objs.forEach(function (obj) {
      _ItemList.push(obj.Name);
    });

    this.ItemList = _ItemList;
  }

  loadMenuItem() {
    var menuItemAddInventory: MenuItem = {
      label: "Adjust Inventory",
      icon: "pi pi-fw pi-table blue-text",
      command: async () => {
        this.openAddInventory();
      },
    };

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_ItemType == ItemTypeEnum.Inventoriable) {
      this.addMenuItem(menuItemAddInventory);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var validateObj = await this.ds.execSP(
      "pValidateItem",
      {
        ID_Item: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_ItemType: this.CurrentObject.ID_ItemType,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true && validateObj.isWarning == false) {
      validations.push({ message: validateObj.message });
    }

    return Promise.resolve(validations);
  }

  async confirmSaving(): Promise<boolean> {
    var title = this.ModelDisplayName;
    var message = "Do you want to save changes?";

    var validateObj = await this.ds.execSP(
      "pValidateItem",
      {
        ID_Item: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_ItemType: this.CurrentObject.ID_ItemType,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true && validateObj.isWarning == true) {
      message = validateObj.message + " Would like to continue saving?";
    }
    
    var options = {
      message: message,
      title: title,
      positiveButtonText: "Yes",
      negativeButtonText: "No",
      backdrop: false,
    };

    var result = await this.msgBox.showConfirmBox(options);

    return result;
  }

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_ItemType"])) {
      options["ID_ItemType"] = this.configOptions["ID_ItemType"];
      this.Current_ID_ItemType = options["ID_ItemType"];
    }

    return options;
  }

  CurrentObject_onLoad() {
    this.loadMenuItem();
    this.loadExistingItem();

    this.UnitPriceCaption =
      this.CurrentObject.ID_ItemType == ItemTypeEnum.Service
        ? "Service Fee"
        : "Selling Price";

    this.ID_ItemCategory_SelectBoxOption = {
      sourceKey: this.cs.encrypt(
        `Select ID, Name FROM tItemCategory WHERE ID_ItemType = ${this.CurrentObject.ID_ItemType} Order By Name ASC`
      ),
    };
  }

  /*  Add Inventory */
  openAddInventory() {
    this.ItemInventory = new ReceiveInventory();
    this.ItemInventory.ID_Item = this.CurrentObject.ID;
    this.ItemInventory.ID_FilingStatus = FilingStatusEnum.Approved;
    this.ItemInventory.IsAddInventory = true;
    this.ItemInventory.Comment = 'N/A';

    if (this.addItemInventoryDialogBoxCmp) {
      this.addItemInventoryDialogBoxCmp.closeOnPositiveButton = false;
      this.addItemInventoryDialogBoxCmp.title = "Adjust Inventory";
      this.addItemInventoryDialogBoxCmp.size = "md";
      this.addItemInventoryDialogBoxCmp.captionPositiveButton = "Apply";

      this.addItemInventoryDialogBoxCmp.open();
    }
  }

  btnTickAddInventory() {
    this.ItemInventory.IsAddInventory = false;
  }

  btnTickDeductInventory() {
    this.ItemInventory.IsAddInventory = true;
  }

  async addItemInventoryDialogBox_onPositiveButtonClick(modal) {
    var validationsAppForm: IFormValidation[] = [];

    if (isNullOrUndefined(this.ItemInventory.Quantity))
      this.ItemInventory.Quantity = 0;

    if (!isNumber(this.ItemInventory.Quantity)) this.ItemInventory.Quantity = 0;

    if (isNullOrUndefined(this.ItemInventory.Comment))
      this.ItemInventory.Comment = "";

    if (this.ItemInventory.Quantity == 0)
      validationsAppForm.push({ message: "Quantity is required." });

    if (this.ItemInventory.Comment.replace(/\s/g, "").length == 0)
      validationsAppForm.push({ message: "Note is required." });

    if (
      validationsAppForm.length == 0 &&
      this.ItemInventory.IsAddInventory != true
    ) {
      var currentInventoryCount = 0;
      var itemInventories = [];
      var sql = `SELECT CurrentInventoryCount
          FROM dbo.tItem
          WHERE ID = ${this.CurrentObject.ID}
        `;

      sql = this.cs.encrypt(sql);
      itemInventories = await this.ds.query<any>(sql);

      if (isNullOrUndefined(itemInventories)) itemInventories = [];

      if (itemInventories.length == 1)
        currentInventoryCount = itemInventories[0].CurrentInventoryCount;

      if (this.ItemInventory.Quantity > currentInventoryCount)
        validationsAppForm.push({
          message: `Insufficient Inventory to deduct for the Current Inventory: ${currentInventoryCount}`,
        });
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      return;
    }

    var result = await this.msgBox.confirm(
      `Would you like to adjust item inventory?`,
      `Confirm ${
        this.ItemInventory.IsAddInventory ? "Add" : "Deduct"
      } Inventory`,
      `${this.ItemInventory.IsAddInventory ? "Add" : "Deduct"} Now`,
      "No"
    );
    if (!result) return;

    var obj = await this.receiveInventory();
    if (obj.Success) {
      await this.loadCurrentRecord();
      modal.close();
    }
  }

  async receiveInventory(): Promise<any> {
    var item = this.CurrentObject;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pReceiveInventory",
        {
          record: [this.ItemInventory],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${item.Name} has been ${
            this.ItemInventory.IsAddInventory ? "Add" : "Deduct"
          } successfully.`,
          `${this.ItemInventory.IsAddInventory ? "Add" : "Deduct"} Done`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to ${
            this.ItemInventory.IsAddInventory ? "Add" : "Deduct"
          } Inventory`
        );
        rej(obj);
      }
    });
  }
}
export class ItemDetailNames {
  static readonly Item_Supplier?: string = "Item_Supplier";
}
