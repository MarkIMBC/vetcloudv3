import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  SimpleChanges,
  ViewChild,
  EventEmitter,
  Output,
} from "@angular/core";
import { Enumerable } from "linq-typescript";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { Item, Patient_SOAP_Treatment } from "src/bo/APP_MODELS";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  Item_DTO,
  Patient_SOAP_DTO,
  Patient_SOAP_Plan_DTO,
  Patient_SOAP_Prescription_DTO,
  Item_Supplier_DTO,
} from "src/utils/controls/class_helper";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "supplier-list",
  templateUrl: "./supplier-list.component.html",
  styleUrls: ["./supplier-list.component.less"],
})
export class SupplierListComponent implements OnInit {
  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  _item_Suppliers: Item_Supplier_DTO[] = [];

  @Input() IsDisabled: boolean = false;

  @Input() set Item_Suppliers(objs: Item_Supplier_DTO[]) {
    this.loadMenuItems();

    if (!objs) return;

    this._item_Suppliers = objs;
  }

  get Item_Suppliers() {
    return this._item_Suppliers;
  }

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    this.loadMenuItems();
  }

  loadMenuItems() {
    var menuItems_AddServices: MenuItem = {
      label: "Add Supplier",
      icon: "pi pi-fw pi-plus",
      command: () => {
        this.doSupplier();
      },
    };

    this.menuItems = [];
    this.menuItems.push(menuItems_AddServices);
  }

  browseSupplier(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var strIDs_currentIDItems = Enumerable.fromSource(this.Item_Suppliers)
        .select((r) => r["ID_Supplier"])
        .toArray()
        .toString();

      if (!strIDs_currentIDItems) strIDs_currentIDItems = "0";

      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Name
              FROM dbo.tSupplier 
              WHERE 
                  ID_Company = ${this.currentUser.ID_Company} AND 
                  ID NOT IN (${strIDs_currentIDItems}) 
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tSupplier
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  async doSupplier() {
    var selectedItems = await this.browseSupplier();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Supplier: record.ID,
        Name_Supplier: record.Name,
      };

      this._item_Suppliers.push(item);
    });
  }
  onChange() {
    this.onChanged.emit({
      data: this._item_Suppliers,
    });
  }

  onRowDeleting(treatment: Item_Supplier_DTO) {
    if (this.IsDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this._item_Suppliers,
      "ID",
      treatment.ID + ""
    );

    this._item_Suppliers.splice(index, 1);

    this.onChanged.emit({
      data: this._item_Suppliers,
    });
  }
}
