import { MenuItem } from 'primeng/api';
import { UserRole_DTO, UserRole_Report_DTO } from './../../../utils/controls/class_helper';
import { Model, UserRole, UserRole_Reports, UserRole_Detail, Report } from './../../../bo/APP_MODELS';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { FormHelperDialogService } from 'src/app/View/FormHelperView/FormHelperView.component';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { ItemTypeEnum, Item_DTO, UserRole_Detail_DTO } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomDetailView } from '../BaseCustomDetailView';
import { Enumerable } from 'linq-typescript';
import { LookUpDialogBoxComponent } from 'src/utils/look-up-dialog-box/look-up-dialog-box.component';

@Component({
  selector: 'app-user-role',
  templateUrl: './user-role.component.html',
  styleUrls: ['./user-role.component.less']
})
export class UserRoleComponent extends BaseCustomDetailView implements OnInit {

  
  @ViewChild("lookUpDialogBox")
  lookUpDialogBoxCmp: LookUpDialogBoxComponent;
  
  CurrentObject: UserRole_DTO;

  currentModelID: string = APP_MODEL.USERROLE;

  selUserRoleDetail: UserRole_Detail_DTO;
  selUserRoleReport: UserRole_Report_DTO;

  CurrentObjectDetailNames: string[] = [
    UserRoleNames.UserRole_Detail,
    UserRoleNames.UserRole_Reports,
  ];

  browseModel(): Promise<Model[]> {

    var ID_ModelString = Enumerable.fromSource(this.CurrentObject.UserRole_Detail)
                  .select(r => "'" + r.ID_Model + "'" )
                  .distinct()
                  .toArray()
                  .toString();

    return new Promise<Model[]>(async (res, rej) => {

      var selectedRecords = await this.lookUpDialogBoxCmp.open<Model>({
        sql: `SELECT 
                    ROW_NUMBER() OVER(ORDER BY Name ASC) ID, 
                    Name, 
                    DisplayName,
                    TableName,
                    Oid
              FROM dbo._tModel 
              ${ID_ModelString.length == 0 ? "" : "WHERE Oid NOT IN (" + ID_ModelString + ")" }
        `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
          {
            name: "DisplayName",
            caption: "Display Name",
            width: "150px",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => "'" + r.Oid + "'" )
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  
                *
          FROM _tModel
          WHERE 
                Oid IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Model>(queryString);

        res(items);
      }
    });
  }

  browseReport(): Promise<Report[]> {

    var ID_ReportString = Enumerable.fromSource(this.CurrentObject.UserRole_Reports)
                  .select(r => "'" + r.ID_Report + "'" )
                  .distinct()
                  .toArray()
                  .toString();

    return new Promise<Report[]>(async (res, rej) => {

      var selectedRecords = await this.lookUpDialogBoxCmp.open<Report>({
        sql: `SELECT 
                    ROW_NUMBER() OVER (ORDER BY Name ASC) ID,
                    Name,
                    Oid
              FROM dbo._tReport
              ${ID_ReportString.length == 0 ? "" : "WHERE Oid NOT IN (" + ID_ReportString + ")" }
        `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => "'" + r.Oid + "'" )
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  
                *
          FROM _tReport
          WHERE 
                Oid IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Report>(queryString);

        res(items);
      }
    });
  }

  userRoleDetailMenu: MenuItem[] = [
    {
      label: 'Add Module',
      icon: 'fa fa-plus',
      command: async () => {

        var records = await this.browseModel();

        records.forEach(record => {
          
          var obj: UserRole_Detail_DTO = {
            ID: this.globalFx.getTempID(),
            ID_Model: record.Oid,
            Name_Model: record.Name,
          }

          this.CurrentObject.UserRole_Detail.push(obj);
        });
      }
    }
  ];

  userRoleReportMenu: MenuItem[] = [
    {
      label: 'Add Report',
      icon: 'fa fa-plus',
      command: async () => {

        var records = await this.browseReport();

        records.forEach(record => {
          
          var obj: UserRole_Report_DTO = {
            ID: this.globalFx.getTempID(),
            ID_Report: record.Oid,
            Name_Report: record.Name,
          }

          this.CurrentObject.UserRole_Reports.push(obj);
        });
      }
    }
  ];

  btnDeleteUserRoleDetail_onClick(userRoleDetail: UserRole_Detail_DTO){

    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.UserRole_Detail,
      "ID",
      userRoleDetail.ID + ""
    );

    this.CurrentObject.UserRole_Detail.splice(index, 1);
  }

  btnDeleteUserRoleReport_onClick(userRoleReport: UserRole_Report_DTO){

    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.UserRole_Reports,
      "ID",
      userRoleReport.ID + ""
    );

    this.CurrentObject.UserRole_Reports.splice(index, 1);
  }
}

export class UserRoleNames {
  static readonly UserRole_Detail?: string = "UserRole_Detail";
  static readonly UserRole_Reports?: string = "UserRole_Reports";
}