import { KeyCode } from "@ng-select/ng-select/lib/ng-select.types";
import { debug } from "console";
import { DialogBoxComponent } from "./../../dialog-box/dialog-box";
import {
  BillingInvoice_Detail,
  Client,
  Employee,
} from "./../../../bo/APP_MODELS";
import { LookUpDialogBoxComponent } from "../../../utils/look-up-dialog-box/look-up-dialog-box.component";
import { PaymentTransactionComponent } from "./../payment-transaction/payment-transaction.component";
import {
  BillingInvoice_Patient_DTO,
  ControlTypeEnum,
  PaymentMethodEnum,
  PaymentTransaction_DTO,
  PositionEnum,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
  AppFormComponent,
  IFilterFormValue,
} from "src/utils/controls/appForm/appForm.component";
import { MenuItem } from "primeng/api/menuitem";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { DataService, IPagingOption } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import {
  MessageBoxService,
  IFormValidation,
} from "src/utils/controls/appModal/appModal.component";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { ToastrService } from "ngx-toastr";
import {
  Model,
  APP_MODEL,
  BillingInvoice,
  Item,
  FilingStatus,
  APP_REPORTVIEW,
} from "src/bo/APP_MODELS";
import { SelectItem } from "primeng/api";
import {
  BillingInvoice_DTO,
  BillingInvoice_Detail_DTO,
  Patient_DTO,
  TaxSchemeEnum,
  FilingStatusEnum,
  Item_DTO,
  ItemTypeEnum,
  PropertyTypeEnum,
} from "src/utils/controls/class_helper";
import {
  IAppLookupBoxOption,
  AppLookupBoxComponent,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { currencyFormatter } from "currencyformatter.js";
import { DataTableOption } from "src/utils/controls/appDataTable/appDataTable.component";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import * as moment from "moment";
import {
  AppSelectBoxComponent,
  IAppSelectBoxOption,
} from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNull, isNullOrUndefined, isNumber } from "util";
import { Enumerable } from "linq-typescript";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { CustomDialogBoxComponent } from "src/utils/custom-dialog-box/custom-dialog-box";

@Component({
  selector: "app-billing-invoice",
  templateUrl: "./billing-invoice.component.html",
  styleUrls: ["./billing-invoice.component.less"],
})
export class BillingInvoiceComponent extends BaseCustomDetailView {
  currentModelID: string = APP_MODEL.BILLINGINVOICE;

  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  @ViewChild("paymentDialogBox")
  paymentDialogBoxCmp: CustomDialogBoxComponent;

  CurrentObjectDetailNames: string[] = [
    BillingInvoiceDetailNames.BillingInvoice_Detail,
    BillingInvoiceDetailNames.BillingInvoice_Patient,
    BillingInvoiceDetailNames.PaymentHistory,
  ];

  PreviousObject: BillingInvoice_DTO = new BillingInvoice_DTO();
  CurrentObject: BillingInvoice_DTO = new BillingInvoice_DTO();

  IsShowFilingStatusField: boolean = true;
  IsShowFilingPaymentStatusField: boolean = true;
  IsShowTaxScheme: boolean = false;

  FILINGSTATUS_APPROVED: FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: FilingStatusEnum.Cancelled;

  PAYMENTMETHOD_CASH: PaymentMethodEnum = PaymentMethodEnum.Cash;
  PAYMENTMETHOD_CHECK: PaymentMethodEnum = PaymentMethodEnum.Check;
  PAYMENTMETHOD_GCASH: PaymentMethodEnum = PaymentMethodEnum.GCash;
  PAYMENTMETHOD_DebitCredit: PaymentMethodEnum = PaymentMethodEnum.DebitCredit;
  PAYMENTMETHOD_CREDITS: PaymentMethodEnum = PaymentMethodEnum.Credits;

  selPaymentTransaction: PaymentTransaction_DTO;
  IsWalkIn: boolean = false;

  patientMenuItems: MenuItem[] = [];
  sellPatient: Patient_DTO;

  @ViewChild("AttendingPhysician_ID_Employee__LookUpBox")
  AttendingPhysician_ID_Employee__LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Client_LookUpBox")
  ID_Client_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Item_LookUpBox")
  ID_Item_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_TaxScheme_SelectBox")
  ID_TaxScheme_SelectBox: AppSelectBoxComponent;

  detailMenu: MenuItem[] = [];

  paymentTransMenuItems: MenuItem[] = [
    {
      label: "Print Receipt",
      icon: "fa fa-print",
      command: async () => {
        var ID_PaymentTransaction = this.selPaymentTransaction.ID;

        PaymentTransactionComponent.PrintDialog(this.ds, ID_PaymentTransaction);
      },
    },
  ];

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      Select ID, 
            Name,
            Name_Position Position
      FROM vAttendingVeterinarianForBillingInvoice
      WHERE 
            ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tPatient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_TaxScheme_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tTaxScheme"),
  };

  ID_SOAPType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tSOAPType"),
  };

  async ID_SOAPType_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_SOAPType = event.Name;
  }

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var menuItem_PetInfo: MenuItem = {
      label: "Pet Info Record",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        var routeLink = [];
        var config = {};

        config = {
          BackRouteLink: [`/Main`, "BillingInvoice", this.CurrentObject.ID],
          prevConfig: this.configOptions,
        };

        routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_ClientInfo: MenuItem = {
      label: "Client's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient_SOAP", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID_Client > 0)
        this.gotoMenuItems.push(menuItem_ClientInfo);
      if (this.CurrentObject.ID_Patient > 0)
        this.gotoMenuItems.push(menuItem_PetInfo);
    }
  }

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  compute() {
    var subTotal = 0;
    var initialSubtotalAmount = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;
    var initialConfimentDepositAmount =
      this.CurrentObject.InitialConfinementDepositAmount;
    var confimentDepositAmount = this.CurrentObject.ConfinementDepositAmount;
    var remainingDepositAmount = this.CurrentObject.RemainingDepositAmount;
    var consumedDepositAmount = this.CurrentObject.ConsumedDepositAmount;

    if (isNullOrUndefined(initialConfimentDepositAmount))
      initialConfimentDepositAmount = 0;
    if (isNullOrUndefined(confimentDepositAmount)) confimentDepositAmount = 0;
    if (isNullOrUndefined(consumedDepositAmount)) consumedDepositAmount = 0;
    if (isNullOrUndefined(remainingDepositAmount)) remainingDepositAmount = 0;
    if (isNullOrUndefined(discountRate)) discountRate = 0;
    if (isNullOrUndefined(discountAmount)) discountAmount = 0;

    discountRate = this.globalFx.roundOffDecimal(discountRate);
    discountAmount = this.globalFx.roundOffDecimal(discountAmount);

    this.CurrentObject.BillingInvoice_Detail.forEach((detail) => {
      if (isNullOrUndefined(detail.Quantity)) detail.Quantity = 0;
      if (isNullOrUndefined(detail.UnitPrice)) detail.UnitPrice = 0;
      if (isNullOrUndefined(detail.DiscountRate)) detail.DiscountRate = 0;
      if (isNullOrUndefined(detail.DiscountAmount)) detail.DiscountAmount = 0;

      var amount = detail.Quantity * detail.UnitPrice;
      var discountRate = detail.DiscountRate;
      var discountAmount = detail.DiscountAmount;

      if (detail.IsComputeDiscountRate) {
        discountAmount = amount * (discountRate / 100);
      } else {
        discountRate = (discountAmount / amount) * 100;
        discountRate = this.globalFx.roundOffDecimal(discountRate);
      }

      amount = amount - discountAmount;

      detail.DiscountRate = discountRate;
      detail.DiscountAmount = discountAmount;
      detail.Amount = amount;

      detail.ID_BillingInvoice = this.CurrentObject.ID;

      subTotal += detail.Amount;
    });

    initialSubtotalAmount = subTotal + 0;
    initialSubtotalAmount = this.globalFx.roundOffDecimal(
      initialSubtotalAmount
    );

    if (confimentDepositAmount > initialConfimentDepositAmount) {
      confimentDepositAmount = initialConfimentDepositAmount;
    }

    remainingDepositAmount = confimentDepositAmount - subTotal;
    if (remainingDepositAmount < 0) remainingDepositAmount = 0;
    remainingDepositAmount = this.globalFx.roundOffDecimal(
      remainingDepositAmount
    );
    consumedDepositAmount = confimentDepositAmount - remainingDepositAmount;

    subTotal = subTotal - consumedDepositAmount;
    if (subTotal < 0) subTotal = 0;

    totalAmount = this.globalFx.roundOffDecimal(subTotal);

    if (subTotal > 0) {
      if (this.CurrentObject.IsComputeDiscountRate) {
        discountAmount = totalAmount * (discountRate / 100);
      } else {
        discountRate = (discountAmount / totalAmount) * 100;
        discountRate = this.globalFx.roundOffDecimal(discountRate);
      }

      totalAmount = totalAmount - discountAmount;
      totalAmount = this.globalFx.roundOffDecimal(totalAmount);

      grossAmount = totalAmount;

      vatAmount = (grossAmount / 1.12) * 0.12;
      vatAmount = this.globalFx.roundOffDecimal(vatAmount);

      if (
        this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
        this.CurrentObject.ID_TaxScheme == 0 ||
        this.CurrentObject.ID_TaxScheme == null
      ) {
        vatAmount = 0;
        netAmount = grossAmount;
      } else if (
        this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive
      ) {
        netAmount = grossAmount + vatAmount;
      } else if (
        this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive
      ) {
        netAmount = grossAmount - vatAmount;
      }

      if (isNullOrUndefined(discountRate)) discountRate = 0;
      if (isNullOrUndefined(discountAmount)) discountAmount = 0;
    } else {
      discountRate = 0;
      discountAmount = 0;
    }

    this.CurrentObject.ConfinementDepositAmount = confimentDepositAmount;
    this.CurrentObject.InitialSubtotalAmount = initialSubtotalAmount;
    this.CurrentObject.ConsumedDepositAmount = consumedDepositAmount;
    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;
    this.CurrentObject.RemainingDepositAmount = remainingDepositAmount;

    if (!this.isLoadCurrrentObject) {
      if (this.appForm) this.appForm.setDirty(true);
    }
  }

  browseItemInventoriable(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Code,
                      Name, 
                      UnitPrice, 
                      FormattedCurrentInventoryCount,
                      RemainingBeforeExpired
              FROM dbo.vItemInventoriableForBillingLookUp 
              WHERE 
                  CurrentInventoryCount > 0 AND
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [
          { name: "Name", caption: "Name" },
          { name: "Code", caption: "Code" },
        ],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },

          {
            name: "RemainingBeforeExpired",
            caption: "Rem. Before Expire",
            width: "100px",
            align: "center",
          },
          {
            name: "UnitPrice",
            caption: "Selling Price",
            width: "150px",
            align: "right",
          },
          {
            name: "FormattedCurrentInventoryCount",
            caption: "Available Count",
            width: "150px",
            align: "center",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  browseItemService(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `
                SELECT ID, 
                       Code,
                       Name, 
                       UnitPrice 
                FROM dbo.vItemServiceLookUp
                WHERE
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [
          { name: "Name", caption: "Name" },
          { name: "Code", caption: "Code" },
        ],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
          {
            name: "UnitPrice",
            caption: "Price",
            width: "150px",
            align: "right",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];

    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;
    var obj = await this.ds.loadObject<Client>(APP_MODEL.CLIENT, ID_Client, {});

    this.CurrentObject.BillingAddress = obj.Address;
    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);
    }
  }

  async ID_Client_LookUpBox_onRemoveSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];
  }

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {
    var ID_Patient = event.ID;
    var obj = await this.ds.loadObject<Patient_DTO>(
      APP_MODEL.PATIENT,
      ID_Patient,
      {}
    );
  }

  async ID_TaxScheme_SelectBox_ValueChanged(event: any) {
    this.appForm.setDirty(true);

    if (isNullOrUndefined(event)) {
      this.CurrentObject.ID_TaxScheme = 0;
      this.CurrentObject.Name_TaxScheme = null;
      this.CurrentObject.VatAmount = 0;
    } else {
      this.CurrentObject.Name_TaxScheme = event.Name;
    }

    this.compute();
  }

  async DiscountRate_onControlValue_Changed(event: any) {
    this.CurrentObject.IsComputeDiscountRate = true;
  }

  async DiscountAmount_onControlValue_Changed(event: any) {
    this.CurrentObject.IsComputeDiscountRate = false;
  }

  async DiscountRate_onChange(event: any) {
    this.compute();
  }

  async DiscountAmount_onChange(event: any) {
    this.compute();
  }

  async loadPaymentHistory(): Promise<void> {
    var sql = `SELECT 
                    ID,
                    Date,
                    Code,
                    PayableAmount,
                    PaymentAmount,
                    Name_PaymentMethod,
                    RemainingAmount,
                    Name_FilingStatus
                FROM dbo.vPaymentTransaction
                WHERE 
                  ID_BillingInvoice = ${this.CurrentObject.ID} 
                    AND
                  ID_FilingStatus IN (${FilingStatusEnum.Approved})
    `;

    sql = this.cs.encrypt(sql);
    this.CurrentObject.PaymentHistory = await this.ds.query<any>(sql);
  }

  async selPaymentTransaction_onDblClick(pt: PaymentTransaction_DTO) {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      BackRouteLink: [`/Main`, "BillingInvoice", this.CurrentObject.ID],
    };

    routeLink = [`/Main`, "PaymentTransaction", pt.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  async CellItemIcon_onClick(detail: BillingInvoice_Detail_DTO) {
    if (this.appForm.isDisabled) return;

    // var selectedItems = await this.browseItem();

    // selectedItems.forEach((record) => {

    //   detail.ID_Item = record.ID;
    //   detail.Name_Item = record.Name;

    //   detail.UnitPrice = record.UnitCost;
    // });
  }

  CellQuantity_onChanged(detail: BillingInvoice_Detail_DTO) {
    this.appForm.setDirty(true);

    if (!isNumber(detail.Quantity)) detail.Quantity = 0;
    if (detail.Quantity < 0) detail.Quantity = 0;

    this.compute();
  }

  CellUnitPrice_onChanged(detail: BillingInvoice_Detail_DTO) {
    this.appForm.setDirty(true);

    if (!isNumber(detail.UnitPrice)) detail.UnitPrice = 0;
    if (detail.UnitPrice < 0) detail.UnitPrice = 0;

    this.compute();
  }

  CellDiscountRate_onChanged(detail: BillingInvoice_Detail_DTO) {
    this.appForm.setDirty(true);

    if (!isNumber(detail.DiscountRate)) detail.DiscountRate = 0;
    if (detail.DiscountRate < 0) detail.DiscountRate = 0;

    detail.IsComputeDiscountRate = true;

    this.compute();
  }

  CellDiscountAmount_onChanged(detail: BillingInvoice_Detail_DTO) {
    this.appForm.setDirty(true);

    if (!isNumber(detail.DiscountAmount)) detail.DiscountAmount = 0;
    if (detail.DiscountAmount < 0) detail.DiscountAmount = 0;

    detail.IsComputeDiscountRate = false;

    this.compute();
  }

  async CellDiscountRate_onControlValue_Changed(
    detail: BillingInvoice_Detail_DTO
  ) {
    detail.IsComputeDiscountRate = true;
  }

  async CellDiscountAmount_onControlValue_Changed(
    detail: BillingInvoice_Detail_DTO
  ) {
    detail.IsComputeDiscountRate = false;
  }

  onRowDeleting(detail: BillingInvoice_Detail_DTO) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.BillingInvoice_Detail,
      "ID",
      detail.ID + ""
    );

    this.CurrentObject.BillingInvoice_Detail.splice(index, 1);

    this.compute();
  }

  onRowDeletingBIPatient(detail: BillingInvoice_Patient_DTO) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.BillingInvoice_Patient,
      "ID",
      detail.ID + ""
    );

    this.CurrentObject.BillingInvoice_Patient.splice(index, 1);

    this.compute();
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.IsWalkIn != true) {
      if (isNullOrUndefined(this.CurrentObject.ID_TaxScheme))
        this.CurrentObject.ID_TaxScheme = 0;

      if (
        isNullOrUndefined(this.AttendingPhysician_ID_Employee__LookUpBox.value)
      )
        this.AttendingPhysician_ID_Employee__LookUpBox.value = "";

      if (this.CurrentObject.AttendingPhysician_ID_Employee == 0)
        this.CurrentObject.AttendingPhysician_ID_Employee = null;

      if (this.CurrentObject.ID_Client == 0)
        this.CurrentObject.ID_Client = null;

      if (this.CurrentObject.ID_Patient == 0)
        this.CurrentObject.ID_Patient = null;

      if (this.ID_Client_LookUpBox.value == null)
        this.ID_Client_LookUpBox.value = "";

      if (this.ID_Patient_LookUpBox.value == null)
        this.ID_Patient_LookUpBox.value = "";

      if (typeof this.ID_Client_LookUpBox.value == "string") {
        this.ID_Client_LookUpBox.value = this.globalFx.lrtrim(
          this.ID_Client_LookUpBox.value
        );
      }

      if (typeof this.ID_Patient_LookUpBox.value == "string") {
        this.ID_Patient_LookUpBox.value = this.globalFx.lrtrim(
          this.ID_Patient_LookUpBox.value
        );
      }

      if (
        this.CurrentObject.ID_Client == null &&
        this.ID_Client_LookUpBox.value != ""
      ) {
        validations.push({
          message: "Please check if the Client is registered.",
        });
      }

      if (this.CurrentObject.ID_Client == null) {
        validations.push({
          message: "Client field is required.",
        });

        if (
          this.CurrentObject.AttendingPhysician_ID_Employee == null &&
          this.AttendingPhysician_ID_Employee__LookUpBox.value != ""
        ) {
          validations.push({
            message: "Please check if the Attending Personnel is registered.",
          });
        }
      }

      if (
        this.CurrentObject.ID_Patient == null &&
        this.ID_Patient_LookUpBox.value != ""
      ) {
        validations.push({
          message: "Please check if the Patient is registered.",
        });
      }
    }

    if (this.CurrentObject.BillingInvoice_Detail.length == 0) {
      validations.push({
        message: "Add Detail atleast one.",
      });
    }

    this.CurrentObject.BillingInvoice_Detail.forEach((detail) => {
      if (detail.Quantity == 0) {
        validations.push({
          message: `Quantity is required for item ${detail.Name_Item}.`,
        });
      }
    });

    if (validations.length == 0) {
      validations = await this.validateBackend();
    }

    return Promise.resolve(validations);
  }

  async validateBackend(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var validateObj = await this.ds.execSP(
      "pBillingInvoice_Validation",
      {
        ID_BillingInvoice: this.CurrentObject.ID,
        ID_Patient_Confinement: this.CurrentObject.ID_Patient_Confinement,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true) {
      validations.push({ message: validateObj.message });
    }

    return validations;
  }

  async CustomDetailView_onBeforeSaving() {
    if (isNullOrUndefined(this.CurrentObject.DiscountRate))
      this.CurrentObject.DiscountRate = 0;

    if (isNullOrUndefined(this.CurrentObject.DiscountAmount))
      this.CurrentObject.DiscountAmount = 0;

    var strNames_currentIDPatients = Enumerable.fromSource(
      this.CurrentObject.BillingInvoice_Patient
    )
      .select((r) => r["Name_Patient"])
      .toArray()
      .toString();

    this.CurrentObject.PatientNames = strNames_currentIDPatients;

    await this.getInitCurrentObject(this.CurrentObject);

    this.compute();
  }

  CustomDetailView_onLoad() {
    this.breadCrumbs.push({
      label: "List",
    });

    this.ID_Client_LookUpBox.initOption();

    if (!isNullOrUndefined(this.ID_TaxScheme_SelectBox))
      this.ID_TaxScheme_SelectBox.initOption();
  }

  protected async getInitCurrentObject(currentObject): Promise<any> {
    if (currentObject.BillingInvoice_Patient == null)
      currentObject.BillingInvoice_Patient = [];

    var IDs_Patient = Enumerable.fromSource(
      currentObject.BillingInvoice_Patient
    )
      .select((r) => r["ID_Patient"])
      .toArray();

    if (currentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      var obj = await this.ds.execSP(
        "pGetConfinmentDepositByPatients",
        {
          ID_Patient_Confinement: currentObject.ID_Patient_Confinement,
          IDs_Patient: IDs_Patient,
        },
        {
          isReturnObject: true,
        }
      );
      currentObject.InitialConfinementDepositAmount =
        obj.ConfinementDepositAmount;
      currentObject.ConfinementDepositAmount = obj.ConfinementDepositAmount;
    }

    this._formatCurrentObjectDetails(this.PreviousObject, currentObject);

    currentObject.BillingInvoice_Detail.forEach((biDetail: any) => {
      biDetail.Note = "";

      if (biDetail["Code_Patient_Confinement"]) {
        biDetail.Note += biDetail["Code_Patient_Confinement"] + "\n";
      }

      if (biDetail["Code_Patient_SOAP"]) {
        biDetail.Note += biDetail["Code_Patient_SOAP"];
      }

      if (biDetail["ID_Patient_SOAP_Prescription"]) {
        biDetail.Note += " (Prescription)";
      }

      if (biDetail["ID_Patient_SOAP_Treatment"]) {
        biDetail.Note += " (Treatment)";
      }
    });

    return currentObject;
  }

  async CurrentObject_onLoad() {
    this.IsWalkIn = this.CurrentObject.IsWalkIn;

    this.loadGotoMenu();
    this.loadMenuItem();
    this.loadMenuItemPatient();

    this.loadDetailMenuItem();
    this.loadPaymentHistory();

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
          Select ID, 
                Name 
          FROM tPatient
          WHERE 
            ID_Client = ${this.CurrentObject.ID_Client} AND 
            IsActive = 1
      `);

      this.ID_Patient_LookUpBox.initOption();
    }

    this.IsShowFilingStatusField =
      this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Approved;
    this.IsShowFilingPaymentStatusField =
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved;

    if (this.PreviousObject.InitialConfinementDepositAmount == null)
      this.PreviousObject.InitialConfinementDepositAmount = 0;
    if (this.CurrentObject.InitialConfinementDepositAmount == null)
      this.CurrentObject.InitialConfinementDepositAmount = 0;
    if (this.PreviousObject.ConfinementDepositAmount == null)
      this.PreviousObject.ConfinementDepositAmount = 0;
    if (this.CurrentObject.ConfinementDepositAmount == null)
      this.CurrentObject.ConfinementDepositAmount = 0;

    if (
      this.PreviousObject.InitialConfinementDepositAmount !=
        this.CurrentObject.InitialConfinementDepositAmount ||
      this.PreviousObject.ConfinementDepositAmount !=
        this.CurrentObject.ConfinementDepositAmount
    ) {
      this.appForm.isDirty = true;
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.compute();
    }
  }

  getNewBillingInvoice_Detail() {
    var item = {
      ID: this.globalFx.getTempID(),
      Quantity: 0,
      UnitCost: 0,
      UnitPrice: 0,
      DiscountRate: 0,
      DiscountAmount: 0,
    };

    return item;
  }

  async doAddItem() {
    var selectedItems = await this.browseItemInventoriable();

    selectedItems.forEach((record) => {
      var item: any = this.getNewBillingInvoice_Detail();

      item.ID_Item = record.ID;
      item.Name_Item = record.Name;
      item.UnitCost = record.UnitCost;
      item.UnitPrice = record.UnitPrice;
      item.DateExpiration = record.OtherInfo_DateExpiration;
      item.Quantity = 1;

      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();
  }

  async doAddService() {
    var selectedItems = await this.browseItemService();

    selectedItems.forEach((record) => {
      var item: any = this.getNewBillingInvoice_Detail();

      item.ID_Item = record.ID;
      item.Name_Item = record.Name;
      item.UnitCost = record.UnitCost;
      item.UnitPrice = record.UnitPrice;

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();
  }

  loadDetailMenuItem() {
    this.detailMenu = [];

    var menuItem_AddItem = {
      label: "Add Item",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        await this.doAddItem();
      },
    };

    var menuItem_AddService = {
      label: "Add Service",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        await this.doAddService();
      },
    };

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.detailMenu.push(menuItem_AddItem);
      this.detailMenu.push(menuItem_AddService);
    }
  }

  private async IsValidBillingInvoiceConfinementDepositUpdated(): Promise<boolean> {
    return new Promise<boolean>(async (res, rej) => {
      var result: boolean = false;
      var beforInitialConfinementDepositAmount = 0;
      var afterInitialConfinementDepositAmount = 0;

      var obj = await this.ds.execSP(
        "pGetBillingInvoice",
        {
          ID: this.CurrentObject.ID,
          ID_Session: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      beforInitialConfinementDepositAmount =
        obj.InitialConfinementDepositAmount;
      afterInitialConfinementDepositAmount =
        this.CurrentObject.InitialConfinementDepositAmount;

      if (beforInitialConfinementDepositAmount == null)
        beforInitialConfinementDepositAmount = 0;
      if (afterInitialConfinementDepositAmount == null)
        afterInitialConfinementDepositAmount = 0;

      result =
        beforInitialConfinementDepositAmount ==
        afterInitialConfinementDepositAmount;

      if (!result) {
        var msg = `/*Code*/ has an updated confinement deposit from /*BeforeInitialConfinementDeposit*/ to /*AfterInitialConfinementDeposit*/. Please save the BI record first.`;

        msg = msg.replace("/*Code*/", this.CurrentObject.Code);
        msg = msg.replace(
          "/*BeforeInitialConfinementDeposit*/",
          "Php " +
            this.globalFx.formatMoney(beforInitialConfinementDepositAmount)
        );
        msg = msg.replace(
          "/*AfterInitialConfinementDeposit*/",
          "Php " +
            this.globalFx.formatMoney(afterInitialConfinementDepositAmount)
        );

        this.msgBox.warning(msg);
      }

      res(result);
    });
  }

  loadMenuItem() {
    var menuItemApprove: MenuItem = {
      label: "Approve",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {
        if (!this.IsValidBillingInvoiceConfinementDepositUpdated()) {
          this.appForm.isDirty = true;
        }

        var validations: IFormValidation[] = [];
        if (validations.length == 0) {
          validations = await this.validateBackend();
        }

        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        if (validations.length > 0) {
          var validation = validations[0];

          this.msgBox.warning(
            `${validation.message}`,
            `Validate ${this.model.Caption}`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to approve ${this.CurrentObject.Code}?`,
          "Billing Invoice",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.approve();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel ${this.CurrentObject.Code}?`,
          "Billing Invoice",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemPaid = this.getMenuItemPayment();

    if (this.CurrentObject.ID < 1) return;
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(menuItemApprove);
      this.addMenuItem(menuItemCancel);
    } else if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
    ) {
      if (
        this.CurrentObject.Payment_ID_FilingStatus ==
          FilingStatusEnum.Pending ||
        this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Done
      )
        this.addMenuItem(menuItemCancel);

      if (
        this.CurrentObject.Payment_ID_FilingStatus ==
          FilingStatusEnum.Pending ||
        this.CurrentObject.Payment_ID_FilingStatus ==
          FilingStatusEnum.PartiallyPaid
      )
        this.addMenuItem(menuItemPaid);
    }

    this.addMenuItem({
      label: "Report",
      icon: "pi pi-fw pi-folder blue-text",
      items: [
        {
          label: "BI Report",
          icon: "pi pi-file-o",
          command: async () => {
            if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
              return;

            if (this.appForm.isDirty) {
              this.msgBox.warning(
                `${this.CurrentObject.Code} changes is not yet saved.`
              );
              return;
            }

            var filterFormValue: IFilterFormValue[] = [];
            var options = {
              prevConfig: this.configOptions,
              BackRouteLink: [`/Main`, "BillingInvoice", this.CurrentObject.ID],
              backLink: this.router.url,
            };

            filterFormValue.push({
              dataField: "ID",
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID,
            });

            this.globalFx.navigateReport(
              "PaitentBillingInvoice",
              APP_REPORTVIEW.PATIENTBILLINGINVOICEREPORT,
              filterFormValue,
              options
            );
          },
        },
        {
          label: "BI Report w/o Discout",
          icon: "pi pi-file-o",
          command: async () => {
            if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
              return;

            if (this.appForm.isDirty) {
              this.msgBox.warning(
                `${this.CurrentObject.Code} changes is not yet saved.`
              );
              return;
            }

            var filterFormValue: IFilterFormValue[] = [];
            var options = {
              prevConfig: this.configOptions,
              BackRouteLink: [`/Main`, "BillingInvoice", this.CurrentObject.ID],
              backLink: this.router.url,
            };

            filterFormValue.push({
              dataField: "ID",
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID,
            });

            this.globalFx.navigateReport(
              "PaitentBillingInvoice",
              "31149445-5F30-419E-9BF2-6580D1CF85F6",
              filterFormValue,
              options
            );
          },
        },
        {
          label: "Receipt",
          icon: "pi pi-file-o",
          command: async () => {
            if (!(await this.IsValidBillingInvoiceConfinementDepositUpdated()))
              return;

            if (this.appForm.isDirty) {
              this.msgBox.warning(
                `${this.CurrentObject.Code} changes is not yet saved.`
              );
              return;
            }
            var ID_BillingInvoice = this.CurrentObject.ID;

            BillingInvoiceComponent.PrintDialog(this.ds, ID_BillingInvoice);
          },
        },
      ],
    });
  }

  static async PrintDialog(dataService, ID_BillingInvoice) {
    var obj = await dataService.execSP(
      "pGetBillingInvoicePrintReceiptLayout",
      {
        ID_BillingInvoice: ID_BillingInvoice,
      },
      {
        isReturnObject: true,
      }
    );

    var mywindow = window.open("", "PRINT", "height=400,width=600");

    mywindow.document.write(`
    
      <html>
        <head>
          <title>${obj.title}</title>
          <style>${obj.style}</style>
        </head>
        <body>
          ${obj.content}
        </body>
      <body>
    `);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    var isMobile =
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      );
    if (isMobile) obj.isAutoPrint = false;

    if (obj.isAutoPrint) {
      setTimeout(() => {
        mywindow.print();
        mywindow.close();
      }, obj.millisecondDelay);
    }

    return true;
  }

  loadMenuItemPatient() {
    this.patientMenuItems = [];

    var menuItemAddPatient: MenuItem = {
      label: "Add Pet",
      icon: "pi pi-fw pi-plus blue-text",
      command: async () => {
        this.doAddPatient();
      },
    };

    if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed &&
      this.CurrentObject.ID_Patient_Confinement == null
    ) {
      this.patientMenuItems.push(menuItemAddPatient);
    }
  }

  browsePatient(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var strIDs_currentIDPatients = Enumerable.fromSource(
        this.CurrentObject.BillingInvoice_Patient
      )
        .select((r) => r["ID_Patient"])
        .toArray()
        .toString();

      if (!strIDs_currentIDPatients) strIDs_currentIDPatients = "0";

      console.log("strIDs_currentIDPatients", strIDs_currentIDPatients);

      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Name
              FROM dbo.vPatient 
              WHERE 
                  IsActive = 1 AND
                  ID NOT IN (${strIDs_currentIDPatients}) AND
                  ID_Client = ${this.CurrentObject.ID_Client}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tPatient
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Patient_DTO>(queryString);

        res(items);
      }
    });
  }

  async doAddPatient() {
    var selectedItems = await this.browsePatient();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Patient: record.ID,
        Name_Patient: record.Name,
      };

      this.CurrentObject.BillingInvoice_Patient.push(item);
    });

    await this.getInitCurrentObject(this.CurrentObject);

    this.compute();
  }

  private getMenuItemPayment(): MenuItem {
    var menuItem = {
      label: "Payment",
      icon: "pi pi-fw pi-money-bill green-text",
      items: [],
    };

    var menuItemCash: MenuItem = {
      label: "Cash",
      icon: "pi pi-fw pi-money-bill green-text",
      command: async () => {
        await this.openPaymentDialogBox(PaymentMethodEnum.Cash);
      },
    };

    var menuItemGCash: MenuItem = {
      label: "G Cash",
      icon: "pi pi-fw pi-money-bill green-text",
      command: async () => {
        await this.openPaymentDialogBox(PaymentMethodEnum.GCash);
      },
    };

    var menuItemDebitCredit: MenuItem = {
      label: "Debit / Credit",
      icon: "pi pi-fw pi-money-bill green-text",
      command: async () => {
        await this.openPaymentDialogBox(PaymentMethodEnum.DebitCredit);
      },
    };

    var menuItemCheckNumber: MenuItem = {
      label: "Check",
      icon: "pi pi-fw pi-money-bill green-text",
      command: async () => {
        await this.openPaymentDialogBox(PaymentMethodEnum.Check);
      },
    };

    var menuItemCredits: MenuItem = {
      label: "Credits",
      icon: "pi pi-fw pi-money-bill green-text",
      command: async () => {
        await this.openPaymentDialogBox(PaymentMethodEnum.Credits);
      },
    };

    menuItem.items.push(menuItemCash);
    menuItem.items.push(menuItemGCash);
    menuItem.items.push(menuItemDebitCredit);
    menuItem.items.push(menuItemCheckNumber);
    menuItem.items.push(menuItemCredits);

    return menuItem;
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  async approve(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveBillingInvoice",
        {
          IDs_BillingInvoice: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been approved successfully.`,
          `Approved ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Approve ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelBillingInvoice",
        {
          IDs_BillingInvoice: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Cancel ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  /* Payment */
  CurrentPaymentInfo: PaymentInfo;

  paymentCmp = new PaymentTransactionComponent(
    this.ds,
    this.globalFx,
    this.msgBox,
    this.userAuthSvc,
    this.lvModal,
    this.route,
    this.router,
    this.cs,
    this.toastr,
    this.loaderService,
    this.validationService,
    this.dvFormSvc,
    this.activeRoute
  );

  async openPaymentDialogBox(ID_PaymentMethod: PaymentMethodEnum) {
    var title = "";
    var currentObject = this.paymentCmp.CurrentObject;

    switch (ID_PaymentMethod) {
      case PaymentMethodEnum.Cash: {
        title = "Payment By Cash";
        break;
      }
      case PaymentMethodEnum.Check: {
        title = "Payment By Check";
        break;
      }
      case PaymentMethodEnum.GCash: {
        title = "Payment By GCash";
        break;
      }
      case PaymentMethodEnum.DebitCredit: {
        title = "Payment By Debit / Credit";
        break;
      }
      case PaymentMethodEnum.Credits: {
        title = "Payment By Credits";
        break;
      }
    }

    await this.paymentCmp.LoadCurrentObjectRecord(-1);

    this.paymentCmp.CurrentObject.ID_BillingInvoice = this.CurrentObject.ID;
    this.paymentCmp.CurrentObject.Code_BillingInvoice = this.CurrentObject.Code;
    this.paymentCmp.CurrentObject.ID_PaymentMethod = ID_PaymentMethod;
    this.paymentCmp.CurrentObject.RemainingAmount_BillingInvoice =
      this.CurrentObject.RemainingAmount;

    this.paymentCmp.CurrentObject.Comment = `This record is auto-generate from ${this.CurrentObject.Code} Paid feature.`;

    this.paymentDialogBoxCmp.closeOnPositiveButton = false;
    this.paymentDialogBoxCmp.title = title;
    this.paymentDialogBoxCmp.size = "md";
    this.paymentDialogBoxCmp.captionPositiveButton = "Pay";
    this.paymentDialogBoxCmp.open();
  }

  PaymentInfoCashAmount_onChanged() {
    this.paymentCmp.compute();
  }

  PaymentInfoCheckAmount_onChanged() {
    this.paymentCmp.compute();
  }

  PaymentInfoGCashAmount_onChanged() {
    this.paymentCmp.compute();
  }

  PaymentInfoCardAmount_onChanged() {
    this.paymentCmp.compute();
  }

  CreditAmount_NumberBox_ValueChanged() {
    this.paymentCmp.compute();
  }

  private _isPaymentTransactionPaying: boolean = false;

  async paymentDialogBox_onPositiveButtonClick(modal) {
    this.paymentDialogBoxCmp.IsShowBtnPositive = false;
    this.paymentDialogBoxCmp.IsShowBtnNegative = false;

    var isvalid = await this.paymentValidation();
    if (!isvalid) return;

    isvalid = await this.paymentCmp.validateRecord();

    this.paymentDialogBoxCmp.IsShowBtnPositive = true;
    this.paymentDialogBoxCmp.IsShowBtnNegative = true;

    if (!isvalid) return;
    if (this._isPaymentTransactionPaying == true) return false;

    try {
      this._isPaymentTransactionPaying = true;

      await this.loadCurrentRecord();
      await this.doPaid();

      this._isPaymentTransactionPaying = false;
    } catch (error) {
      this._isPaymentTransactionPaying = false;
      console.log(error);
    }

    this.paymentDialogBoxCmp.IsShowBtnPositive = true;
    this.paymentDialogBoxCmp.IsShowBtnNegative = true;
    modal.close();

    await this.loadCurrentRecord();
  }

  ID_Bank_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tBank"),
  };

  ID_CardType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tCardType"),
  };

  async ID_CardType_SelectBox_ValueChanged(event: any) {
    this.paymentCmp.CurrentObject.Name_CardType = event.Name;
  }

  paymentValidation(): boolean {
    var currentObject = this.paymentCmp.CurrentObject;
    var validationsAppForm: IFormValidation[] = [];

    if (isNullOrUndefined(currentObject.CashAmount))
      currentObject.CashAmount = 0;

    if (!isNumber(currentObject.CashAmount)) currentObject.CashAmount = 0;
    if (!isNumber(currentObject.CreditAmount)) currentObject.CreditAmount = 0;

    if (isNullOrUndefined(currentObject.CheckNumber))
      currentObject.CheckNumber = "";

    if (isNullOrUndefined(currentObject.CheckAmount))
      currentObject.CheckAmount = 0;

    if (isNullOrUndefined(currentObject.ReferenceTransactionNumber))
      currentObject.ReferenceTransactionNumber = "";

    if (!isNumber(currentObject.GCashAmount)) currentObject.GCashAmount = 0;

    if (isNullOrUndefined(currentObject.ID_CardType))
      currentObject.ID_CardType = 0;

    if (isNullOrUndefined(currentObject.CardNumber))
      currentObject.CardNumber = "";

    if (isNullOrUndefined(currentObject.CardHolderName))
      currentObject.CardHolderName = "";

    if (!isNumber(currentObject.CardAmount)) currentObject.CardAmount = 0;

    switch (currentObject.ID_PaymentMethod) {
      case PaymentMethodEnum.Cash: {
        if (currentObject.CashAmount == 0)
          validationsAppForm.push({ message: "Cash Amount is required." });

        break;
      }
      case PaymentMethodEnum.Check: {
        if (currentObject.CheckNumber == "")
          validationsAppForm.push({ message: "Check Number is required." });
        if (currentObject.CheckAmount == 0)
          validationsAppForm.push({ message: "Check Amount is required." });

        break;
      }
      case PaymentMethodEnum.GCash: {
        if (currentObject.ReferenceTransactionNumber == "")
          validationsAppForm.push({ message: "Ref No. is required." });
        if (currentObject.GCashAmount == 0)
          validationsAppForm.push({ message: "G-Cash Amount is required." });

        break;
      }

      case PaymentMethodEnum.DebitCredit: {
        // if (currentObject.ID_CardType == 0)
        //   validationsAppForm.push({ message: "Card type is required." });

        // if (currentObject.CardNumber == "")
        //   validationsAppForm.push({ message: "Card # is required." });

        // if (currentObject.CardHolderName == "")
        //   validationsAppForm.push({
        //     message: "Card Holder Name is required.",
        //   });

        if (currentObject.CardAmount == 0)
          validationsAppForm.push({ message: "Amount is required." });

        break;
      }
      case PaymentMethodEnum.Credits: {
        if (currentObject.CreditAmount == 0)
          validationsAppForm.push({ message: "Amount is required." });

        if (
          this.CurrentObject.CreditAmount > this.CurrentObject.PayableAmount
        ) {
          validationsAppForm.push({
            message: `Credit Amount must be equal or less than from Payable Amount`,
          });
        }

        break;
      }
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      return false;
    }

    return true;
  }
  async doPaid(): Promise<boolean> {
    var IsDoPaid = true;

    try {
      this.paymentCmp.compute();

      await this.paymentCmp
        .save()
        .then(async (r) => {
          await this.paymentCmp.loadRecord(r.key);
          this.paymentCmp.__ID_CurrentObject = r.key;

          var obj = await this.pApprovePaymentTransaction(this.paymentCmp);

          IsDoPaid = obj.Success;
        })
        .catch((reason) => {
          throw reason;
        });
    } catch (error) {
      IsDoPaid = false;
    }

    return IsDoPaid;
  }

  async pApprovePaymentTransaction(
    pTransComp: PaymentTransactionComponent
  ): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      await pTransComp.loadCurrentRecord();

      var obj = await this.ds.execSP(
        "pApprovePaymentTransaction",
        {
          IDs_PaymentTransaction: [pTransComp.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${pTransComp.CurrentObject.Code} has been approved successfully.`,
          `Approved ${pTransComp.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Approve ${pTransComp.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_SOAP"])) {
      options["ID_Patient_SOAP"] = this.configOptions["ID_Patient_SOAP"];
    }

    if (
      !isNullOrUndefined(this.configOptions["AttendingPhysician_ID_Employee"])
    ) {
      options["AttendingPhysician_ID_Employee"] =
        this.configOptions["AttendingPhysician_ID_Employee"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_Confinement"])) {
      options["ID_Patient_Confinement"] =
        this.configOptions["ID_Patient_Confinement"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_Vaccination"])) {
      options["ID_Patient_Vaccination"] =
        this.configOptions["ID_Patient_Vaccination"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_Wellness"])) {
      options["ID_Patient_Wellness"] =
        this.configOptions["ID_Patient_Wellness"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_Lodging"])) {
      options["ID_Patient_Lodging"] = this.configOptions["ID_Patient_Lodging"];
    }

    if (!isNullOrUndefined(this.configOptions["IsWalkIn"])) {
      options["IsWalkIn"] = this.configOptions["IsWalkIn"];
    }

    return options;
  }
}

export class BillingInvoiceDetailNames {
  static readonly BillingInvoice_Detail?: string = "BillingInvoice_Detail";
  static readonly BillingInvoice_Patient?: string = "BillingInvoice_Patient";
  static readonly PaymentHistory?: string = "PaymentHistory";
}
class PaymentInfo {
  Code: string;
  CashAmount: number;
  CheckNumber: string;
  CheckAmount: number;
  RemainingAmount: number;
  ID_PaymentMethod: number;
  ChangeAmount: number;
  ReferenceTransactionNumber;
  GCashAmount: number;
  ID_CardType: number;
  CardNumber: string;
  CardHolderName: string;
  CardAmount: number;
  CreditAmount: number;
}
