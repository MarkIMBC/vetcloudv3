import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, ViewChild, EventEmitter, Output } from '@angular/core';
import { Enumerable } from 'linq-typescript';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { Item, Patient, Patient_Confinement_Patient } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { Patient_Confinement_ItemsServices_DTO, Patient_Confinement_Patient_DTO } from 'src/utils/controls/class_helper';
import { LookUpDialogBoxComponent } from 'src/utils/look-up-dialog-box/look-up-dialog-box.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'patient-table',
  templateUrl: './patient-table.component.html',
  styleUrls: ['./patient-table.component.less']
})
export class PatientTableComponent implements OnInit {
  
  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  _patients: Patient_Confinement_Patient_DTO[] = [];
  selectedRecord: Patient_Confinement_Patient_DTO

  @Input() IsDisabled: boolean = false;
  @Input() CurrentObject: any = null;
  @Input() ItemsServices: Patient_Confinement_ItemsServices_DTO[] = []
  @Input() set Patients(objs: Patient_Confinement_Patient_DTO[]) {

    this.loadMenuItems();

    if(!objs) return;

    this._patients = objs;
  }

  @Input() set ID_Client(value: number) {

    this.loadMenuItems();
  }

  get Patients() {

    return this._patients;
  }

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    
    this.loadMenuItems();
  }

  loadMenuItems(){

    var menuItems_AddMedications: MenuItem = {
      label: 'Add Patient',
      icon: "pi pi-fw pi-plus",
      command: () =>{

        this.doAddPatient();
      }
    }

    this.menuItems = [];
    this.menuItems.push(menuItems_AddMedications);
  }
  
  browsePatient(): Promise<Patient_Confinement_Patient[]> {
    return new Promise<Item[]>(async (res, rej) => {

      var strIDs_currentIDPatients = Enumerable.fromSource(this._patients)
        .select((r) => r['ID_Patient'])
        .toArray()
        .toString();

      if (!strIDs_currentIDPatients) strIDs_currentIDPatients = "0"

      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Name
              FROM dbo.vPatient 
              WHERE 
                  IsActive = 1 AND
                  ID NOT IN (${strIDs_currentIDPatients}) AND
                  ID_Client = ${this.CurrentObject.ID_Client}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  ID, Name
          FROM vPatient
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Patient>(queryString);

        res(items);
      }
    });
  }

  async doAddPatient() {
    var selectedItems = await this.browsePatient();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Patient: record.ID,
        Name_Patient: record.Name
      };

      this._patients.push(item);
    });

    this.onChanged.emit({
      
      data: this._patients
    });
  }

  onChange(){

    this.onChanged.emit({
      
      data: this._patients
    });
  }

  async onRowDeleting(confinePatient: Patient_Confinement_Patient_DTO) {

    if (this.IsDisabled) return;

    var obj = await this.validateBackEnd_OnRemoving_Patient_Confinement_Patient(confinePatient);
    if(obj.Success != true) return;

    var index = this.globalFx.findIndexByKeyValue(
      this._patients,
      "ID",
      confinePatient.ID + ""
    );

    this._patients.splice(index, 1);

    this.onChanged.emit({
      
      data: this._patients
    });

    var itemsServices = Enumerable.fromSource(this.ItemsServices)
    .where((r) => r['ID_Patient'] == confinePatient.ID_Patient)
    .toArray()

    itemsServices.forEach(itemServices => {

      itemServices.ID_Patient = null;
      itemServices.Name_Patient = null;
    });

  }

  async validateBackEnd_OnRemoving_Patient_Confinement_Patient(confinePatient: Patient_Confinement_Patient_DTO){
   
      var obj = await this.ds.execSP(
        "pValidateOnRemoving_Patient_Confinement_Patient",
        {
          ID_Patient_Confinement: this.CurrentObject.ID,
          ID_Patient: confinePatient.ID_Patient,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success != true) {
     
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Remove Patient`
        );
      }

      return obj;
  }

}

