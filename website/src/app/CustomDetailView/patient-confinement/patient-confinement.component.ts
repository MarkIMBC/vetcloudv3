import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_Confinement_ItemsServices_DTO,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Client, Item } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  AppLookupBoxComponent,
  IAppLookupBoxOption,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { ConfinementSOAPTableComponent } from "./confinement-soap-table/confinement-soap-table.component";
import { ConfinementItemsServicesDialogComponent } from "./confinement-items-services-dialog/confinement-items-services-dialog.component";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { Enumerable } from "linq-typescript";
import { ClientCreditAdjustmentDialogBoxComponent } from "../../../utils/client-credit-adjustment/client-credit-adjustment.component";
import { ConfinementClientDepositListComponent } from "./confinement-client-deposit-list/confinement-client-deposit-list.component";
import { BillingInvoiceComponent } from "../billing-invoice/billing-invoice.component";
import { ConfimentItemServiceModeEnum } from "./confinement-items-services/confinement-items-services.component";

@Component({
  selector: "app-patient-confinement",
  templateUrl: "./patient-confinement.component.html",
  styleUrls: ["./patient-confinement.component.less"],
})
export class PatientConfinementComponent
  extends BaseCustomDetailView
  implements OnInit
{
  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  @ViewChild("confinementclientdepositlist")
  confinementclientdepositlist: ConfinementClientDepositListComponent;

  @ViewChild("confinementsoaptable")
  confinementsoaptable: ConfinementSOAPTableComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  @ViewChild("clientcreditadjustmentdialogbox")
  clientcreditadjustmentdialogbox: ClientCreditAdjustmentDialogBoxComponent;

  currentModelID: string = "CA0490FB-C86A-4892-A99D-482CA8454D2A";

  CurrentObjectDetailNames: string[] = [
    Patient_ConfinementDetailNames.Patient_Confinement_ItemsServices,
    Patient_ConfinementDetailNames.Patient_Confinement_Patient,
  ];

  ItemsServicesmenuItems: MenuItem[] = [];

  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;

  ConfimentItemServiceModeEnum_Item: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Item;
  ConfimentItemServiceModeEnum_Service: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Service;

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `),
  };

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `);

    this.CurrentObject.Patient_Confinement_Patient = [];
  }

  async ID_Client_LookUpBox_onRemoveSelectedItem(event: any) {
    this.CurrentObject.Patient_Confinement_Patient = [];
  }

  protected gotoSelfNavigate(ID_CurrentObject) {
    if (isNullOrUndefined(this.route.snapshot.params["ID_CurrentObject"]))
      return;

    var routeLink = [];
    routeLink = [`/Main`, "Confinement", ID_CurrentObject];

    this.globalFx.customNavigate(routeLink, this.configOptions);
  }

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  loadMenuItem() {
    var menuItemDischarge: MenuItem = {
      label: "Discharge",
      icon: "fas fa-sign-out-alt blue-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to discharge ${this.CurrentObject.Name_Patient}?`,
          "Confinement",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.dischargePatient();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel confinement for ${this.CurrentObject.Name_Patient}?`,
          "SOAP",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItem_CreateBillingInvoice = {
      label: "Create Invoice",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          ID_Patient_Confinement: this.CurrentObject.ID,
          BackRouteLink: [`/Main`, "Confinement", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "BillingInvoice", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_viewBillingInvoice = {
      label: "View Invoice",
      icon: "pi pi-fw pi-file",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Confinement", this.CurrentObject.ID],
        };

        routeLink = [
          `/Main`,
          "BillingInvoice",
          this.CurrentObject.ID_BillingInvoice,
        ];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID < 1) return;

    if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged &&
      (!this.CurrentObject.BillingInvoice_ID_FilingStatus ||
        this.CurrentObject.BillingInvoice_ID_FilingStatus ==
          FilingStatusEnum.ForBilling)
    ) {
      this.addMenuItem(menuItemCancel);
    }

    if (this.CurrentObject.ID_BillingInvoice) {
      this.addMenuItem(menuItem_viewBillingInvoice);
    } else {
      this.addMenuItem(menuItem_CreateBillingInvoice);
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {
      this.addMenuItem(menuItemDischarge);
      this.addMenuItem(menuItemCancel);
    }
  }

  async dischargePatient(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDischargePatient_Confinement",
        {
          IDs_Patient_Confinement: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Name_Patient} has been discharged.`,
          `Discharged Done`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Discharge ${this.CurrentObject.Name_Patient}`
        );
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPatient_Confinement",
        {
          IDs_Patient_Confinement: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Name_Patient} confinement has been cancel.`,
          `Confinement`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.CurrentObject.Name_Patient} Confinement`
        );
        rej(obj);
      }
    });
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Confined) {
      isDisabled = true;
    }

    return isDisabled;
  }

  compute() {
    var totalAmount: number = 0;

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach((obj) => {
      var itemService: Patient_Confinement_ItemsServices_DTO = obj;

      var amount = itemService.Quantity * itemService.UnitPrice;

      itemService.Amount = amount;

      totalAmount += amount;
    });

    this.CurrentObject.SubTotal = totalAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

  ConfinementItem_onChanged() {
    this.compute();
  }

  ConfinementService_onChanged() {
    this.compute();
  }

  CurrentObject_onLoad() {
    this.loadMenuItem();
    this.loadGotoMenu();

    if (this.confinementsoaptable)
      this.confinementsoaptable.loadByCurrentObject(this.CurrentObject);
    if (this.confinementclientdepositlist)
      this.confinementclientdepositlist.loadByCurrentObject(this.CurrentObject);

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
          Select ID, 
                Name 
          FROM tPatient
          WHERE 
            ID_Client = ${this.CurrentObject.ID_Client} AND 
            ID_Company = ${this.currentUser.ID_Company}
      `);
    }
  }

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var menuItem_PetInfo: MenuItem = {
      label: "Pet Info Record",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        var routeLink = [];
        var config = {};

        config = {
          BackRouteLink: [`/Main`, "Confinement", this.CurrentObject.ID],
          prevConfig: this.configOptions,
        };

        routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_ClientInfo: MenuItem = {
      label: "Client's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Confinement", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {
      if (this.CurrentObject.ID_Client > 0)
        this.gotoMenuItems.push(menuItem_ClientInfo);
      if (this.CurrentObject.ID_Patient > 0)
        this.gotoMenuItems.push(menuItem_PetInfo);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;

    if (isNullOrUndefined(this.CurrentObject.ID_Client)) ID_Client = 0;
    if (isNullOrUndefined(this.CurrentObject.ID_Patient)) ID_Patient = 0;

    if (ID_Client == 0) {
      validations.push({
        message: "Client is required.",
      });
    }

    // if (ID_Patient == 0) {
    //   validations.push({
    //     message: "Patient is required.",
    //   });
    // }

    if (this.CurrentObject.Patient_Confinement_Patient.length == 0) {
      validations.push({
        message: "Add Patient atleast one.",
      });
    }

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach(
      (itemService) => {
        if (itemService.Quantity == null || itemService.Quantity == 0) {
          validations.push({
            message: `Quantity is required for ${itemService.Name_Item}.`,
          });
        }
      }
    );

    if (validations.length == 0) {
      var ID_Patients = Enumerable.fromSource(
        this.CurrentObject.Patient_Confinement_Patient
      )
        .select((r) => r["ID_Patient"])
        .toArray();

      var validateObj = await this.ds.execSP(
        "pPatient_Confinement_Validation",
        {
          ID_Patient_Confinement: this.CurrentObject.ID,
          ID_Patients: ID_Patients,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (validateObj.isValid != true) {
        validations.push({ message: validateObj.message });
      }
    }

    return Promise.resolve(validations);
  }

  async ColQuantity_onChange(event: any) {
    this.compute();
  }

  async ColUnitPrice_onChange(event: any) {
    this.compute();
  }

  ItemsServices_onRowDeleting(
    itemsServices: Patient_Confinement_ItemsServices_DTO
  ) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.Patient_Confinement_ItemsServices,
      "ID",
      itemsServices.ID + ""
    );

    this.CurrentObject.Patient_Confinement_ItemsServices.splice(index, 1);
    this.compute();
  }

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient_SOAP"])) {
      options["ID_Patient_SOAP"] = this.configOptions["ID_Patient_SOAP"];
    }

    return options;
  }

  PatientTable_onChange(e: any) {
    this.CurrentObject.Patient_Confinement_Patient = e.data;
    this.appForm.setDirty(true);
  }

  CustomDetailView_onBeforeSaving() {
    var strNames_currentIDPatients = Enumerable.fromSource(
      this.CurrentObject.Patient_Confinement_Patient
    )
      .select((r) => r["Name_Patient"])
      .toArray()
      .toString();

    this.CurrentObject.PatientNames = strNames_currentIDPatients;
  }

  /*******************************************************************/
  async CustomDetailView_onAfterSaved() {
    this.msgBox.success("Saved Successfully.", this.model.Caption);

    if (this.CurrentObject.ID_BillingInvoice) {
      //this.SaveBillingInvoiceItems();
    }
  }

  async SaveBillingInvoiceItems() {
    await this.loadCurrentRecord();

    var biDetailComponent: BillingInvoiceComponent =
      new BillingInvoiceComponent(
        this.ds,
        this.globalFx,
        this.msgBox,
        this.userAuthSvc,
        this.lvModal,
        this.route,
        this.router,
        this.cs,
        this.toastr,
        this.loaderService,
        this.validationService,
        this.dvFormSvc,
        this.activeRoute
      );

    await biDetailComponent.LoadCurrentObjectRecord(
      this.CurrentObject.ID_BillingInvoice
    );
    if (this.CurrentObject.BillingInvoice_Detail == null)
      this.CurrentObject.BillingInvoice_Detail = [];

    if (biDetailComponent.CurrentObject.ID == -1) return;
    if (biDetailComponent.CurrentObject.ID == null) return;
    if (
      biDetailComponent.CurrentObject.ID_FilingStatus ==
      FilingStatusEnum.Approved
    )
      return;

    var objs = Enumerable.fromSource(
      biDetailComponent.CurrentObject.BillingInvoice_Detail
    )
      .where((r) => r["ID_Patient_Confinement_ItemsServices"] != null)
      .toArray();

    objs.forEach((obj) => {
      var index = this.globalFx.findIndexByKeyValue(
        biDetailComponent.CurrentObject.BillingInvoice_Detail,
        "ID",
        obj["ID"] + ""
      );

      biDetailComponent.CurrentObject.BillingInvoice_Detail.splice(index, 1);
    });

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach(
      (itemService) => {
        var biDetail: any = biDetailComponent.getNewBillingInvoice_Detail();

        biDetail.ID_Patient_Confinement_ItemsServices = itemService.ID;
        biDetail.ID_Item = itemService.ID_Item;
        biDetail.Name_Item = itemService.Name_Item;
        biDetail.Quantity = itemService.Quantity;
        biDetail.UnitCost = itemService.UnitCost;
        biDetail.UnitPrice = itemService.UnitPrice;
        biDetail.DateExpiration = itemService.DateExpiration;

        biDetailComponent.CurrentObject.BillingInvoice_Detail.push(biDetail);
      }
    );

    biDetailComponent.compute();

    this.CurrentObject.TotalAmount_BillingInvoice =
      biDetailComponent.CurrentObject.TotalAmount;
    await biDetailComponent.save();
  }
}

export class Patient_ConfinementDetailNames {
  static readonly Patient_Confinement_ItemsServices?: string =
    "Patient_Confinement_ItemsServices";
  static readonly Patient_Confinement_Patient?: string =
    "Patient_Confinement_Patient";
}
