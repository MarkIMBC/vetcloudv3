import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Enumerable } from "linq-typescript";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_REPORTVIEW, Patient_Confinement } from "src/bo/APP_MODELS";
import { ClientCreditAdjustmentDialogBoxComponent } from "src/utils/client-credit-adjustment/client-credit-adjustment.component";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  FilingStatusEnum,
  ClientDeposit_DTO,
  PropertyTypeEnum,
  CreditModeEnum,
} from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "confinement-client-deposit-list",
  templateUrl: "./confinement-client-deposit-list.component.html",
  styleUrls: ["./confinement-client-deposit-list.component.less"],
})
export class ConfinementClientDepositListComponent implements OnInit {
  @Input() configOptions: any = {};

  @ViewChild("clientcreditadjustmentdialogbox")
  clientcreditadjustmentdialogbox: ClientCreditAdjustmentDialogBoxComponent;

  _ID_Patient_Confinement: number = 0;
  isMenuShow: boolean = true;

  _CurrentObject: Patient_Confinement = {};

  private currentUser: TokenSessionFields = new TokenSessionFields();

  FORBILLING_FILINGSTATUS: FilingStatusEnum = FilingStatusEnum.ForBilling;

  dataSource: ClientDeposit_DTO[] = [];
  selectedRecord: ClientDeposit_DTO = null;

  menuItems: MenuItem[] = [];

  isAllowedToCancel: boolean = false;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {}

  async loadByCurrentObject(CurrentObject: Patient_Confinement) {
    this._CurrentObject = CurrentObject;

    this._ID_Patient_Confinement = this._CurrentObject.ID;
    this.loadConfinementClientDeposits();
  }

  async load(ID_Patient_Confinement: number) {
    this._ID_Patient_Confinement = ID_Patient_Confinement;
    this.loadConfinementClientDeposits();
  }

  async loadConfinementClientDeposits(): Promise<void> {
    var obj = await this.ds.execSP(
      "pGetConfinmentDeposit",
      {
        ID_Patient_Confinement: this._ID_Patient_Confinement,
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.ClientDeposits;

    this.loadConfinementClientDepositMenuItems();
  }

  private loadConfinementClientDepositMenuItems() {
    this.menuItems = [];

    var menuItemDepositCredit: MenuItem = {
      label: "Deposit Credit",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {
        var IDs_Patient = Enumerable.fromSource(
          this._CurrentObject.Patient_Confinement_Patient
        )
          .select((r) => r["ID_Patient"])
          .toArray();

        this.clientcreditadjustmentdialogbox.isEnableToChangeMode = false;
        this.clientcreditadjustmentdialogbox.setCreditMode(
          CreditModeEnum.Deposit
        );
        this.clientcreditadjustmentdialogbox
          .LoadByPatientConfinement(this._ID_Patient_Confinement, IDs_Patient)
          .then(() => {
            this.loadConfinementClientDeposits();
          });
      },
    };

    var menuItemPrintReceipt: MenuItem = {
      label: "Print Receipt",
      icon: "pi pi-file-o",
      command: async () => {
        if (!this.selectedRecord.ID) return;

        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Confinement", this._CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.selectedRecord.ID,
        });

        this.globalFx.navigateReport(
          "ClientDepositReport",
          APP_REPORTVIEW.CLIENTDEPOSITREPORT,
          filterFormValue,
          options
        );
      },
    };

    if (
      this._CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined ||
      (this._CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged &&
        (!this._CurrentObject.BillingInvoice_ID_FilingStatus ||
          this._CurrentObject.BillingInvoice_ID_FilingStatus ==
            FilingStatusEnum.ForBilling))
    ) {
      this.menuItems.push(menuItemDepositCredit);

      this.isAllowedToCancel = true;
    } else {
      this.isAllowedToCancel = false;
    }

    if (this.selectedRecord) {
      if (this.selectedRecord.ID) this.menuItems.push(menuItemPrintReceipt);
    }
  }

  private gotoClientDepositRoute(ID_PatienClientDeposit) {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      ID_Patient_Confinement: this._ID_Patient_Confinement,
      BackRouteLink: [`/Main`, "Confinement", this._ID_Patient_Confinement],
    };

    routeLink = [`/Main`, "ClientDeposit", ID_PatienClientDeposit];
    this.globalFx.customNavigate(routeLink, config);
  }

  ClientDepositGrid_onDblClick() {
    if (isNullOrUndefined(this.selectedRecord)) return;

    this.gotoClientDepositRoute(this.selectedRecord.ID);
  }

  async onRowDeletingRecord(record: any) {
    var result = await this.msgBox.confirm(
      `Would you like to remove deposited amount of ${record.DepositAmount}?`,
      `Cancel Deposit`,
      "Yes",
      "No"
    );

    if (!result) return;

    var obj = await this.cancelClientDeposit(record.ID);

    if (obj.Success) {
      this.msgBox.success(`Deposit has been cancelled successfully.`, `Credit`);
    } else {
      this.msgBox.error(obj.message, `Failed to cancelled Deposit`);
    }

    this.loadConfinementClientDeposits();
  }

  async cancelClientDeposit(ID_ClientDeposit: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelClientDeposit",
        {
          IDs_ClientDeposit: [ID_ClientDeposit],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        rej(obj);
      }
    });
  }

  List_onRowSelect(e: any) {
    this.loadConfinementClientDepositMenuItems();
  }

  List_onRowUnselect(e: any) {
    this.loadConfinementClientDepositMenuItems();
  }
}
