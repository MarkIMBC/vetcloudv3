import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { Enumerable } from "linq-typescript";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { Item } from "src/bo/APP_MODELS";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  Patient_Confinement_ItemsServices_DTO,
  Item_DTO,
  ItemTypeEnum,
  Patient_Confinement_Patient_DTO,
} from "src/utils/controls/class_helper";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "confinement-items-services",
  templateUrl: "./confinement-items-services.component.html",
  styleUrls: ["./confinement-items-services.component.less"],
})
export class ConfinementItemsServicesComponent implements OnInit {
  ItemTypeEnum_Inventoriable: ItemTypeEnum = ItemTypeEnum.Inventoriable;
  ItemTypeEnum_Service: ItemTypeEnum = ItemTypeEnum.Service;

  ConfimentItemServiceModeEnum_All: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  ConfimentItemServiceModeEnum_Item: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Item;
  ConfimentItemServiceModeEnum_Service: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.Service;

  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  _itemsandservices: Patient_Confinement_ItemsServices_DTO[] = [];
  TotalAmount: number = 0;

  MedicationRouteList: any[] = [];

  @Input() patients: Patient_Confinement_Patient_DTO[] = [];

  @Input() ItemServiceMode: ConfimentItemServiceModeEnum =
    ConfimentItemServiceModeEnum.All;
  @Input() IsDisabled: boolean = false;
  @Input() set itemsandservices(objs: Patient_Confinement_ItemsServices_DTO[]) {
    this.loadMenuItems();

    if (!objs) return;

    this._itemsandservices = objs;

    this.compute();
  }

  get itemsandservices() {
    return this._itemsandservices;
  }

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    this.compute();

    this.loadMenuItems();
    this.loadMedicationRouteList();
  }

  async loadMedicationRouteList(): Promise<void> {
    this.MedicationRouteList = [];

    var sql = this.cs.encrypt(
      `SELECT 
              ID, 
              Name
        FROM dbo.tMedicationRoute
    `
    );

    var _MedicationRouteList = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      _MedicationRouteList.push(obj.Name);
    });

    this.MedicationRouteList = _MedicationRouteList;
  }

  loadMenuItems() {
    var menuItems_AddMedications: MenuItem = {
      label: "Add Medication",
      icon: "pi pi-fw pi-plus",
      command: () => {
        this.doAddMedication();
      },
    };

    var menuItems_AddServices: MenuItem = {
      label: "Add Service",
      icon: "pi pi-fw pi-plus",
      command: () => {
        this.doAddService();
      },
    };

    this.menuItems = [];

    if (
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Item
    ) {
      this.menuItems.push(menuItems_AddMedications);
    }

    if (
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
      this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Service
    ) {
      this.menuItems.push(menuItems_AddServices);
    }
  }

  browseItemInventoriable(): Promise<Item[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `SELECT 
                      ID, 
                      Code,
                      Name, 
                      UnitPrice, 
                      FormattedCurrentInventoryCount,
                      RemainingBeforeExpired
              FROM dbo.vItemInventoriableForBillingLookUp 
              WHERE 
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },

          {
            name: "RemainingBeforeExpired",
            caption: "Rem. Before Expire",
            width: "100px",
            align: "center",
          },
          {
            name: "UnitPrice",
            caption: "Selling Price",
            width: "150px",
            align: "right",
          },
          {
            name: "FormattedCurrentInventoryCount",
            caption: "Available Count",
            width: "150px",
            align: "center",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  browseItemService(): Promise<Item_DTO[]> {
    return new Promise<Item_DTO[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        sql: `
                SELECT ID,  
                       Code,
                       Name, 
                       UnitPrice 
                FROM dbo.vItemServiceLookUp
                WHERE
                  ID_Company = ${this.currentUser.ID_Company}    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }, { name: "Code" }],
        columns: [
          {
            name: "Code",
            width: "80px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
          {
            name: "UnitPrice",
            caption: "Price",
            width: "150px",
            align: "right",
          },
        ],
      });

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(items);
      }
    });
  }

  browsePatient(): Promise<Item[]> {
    var strIDsPatients = Enumerable.fromSource(this.patients)
      .select((r) => r["ID_Patient"])
      .toArray()
      .toString();

    if (strIDsPatients.length == 0) strIDsPatients = "0";

    return new Promise<Item[]>(async (res, rej) => {
      var selectedRecords = await this.itemServiceDialogCmp.open<Item>({
        IsMultiple: false,
        sql: `SELECT 
                      ID, 
                      Name
              FROM dbo.tPatient 
              WHERE 
                  ID_Company = ${this.currentUser.ID_Company} AND
                  ID IN (${strIDsPatients})   
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT ID, Name
          FROM tItem
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Item_DTO>(queryString);

        res(selectedRecords);
      }
    });
  }

  async doAddMedication() {
    var selectedItems = await this.browseItemInventoriable();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
        ID_ItemType: record.ID_ItemType,
      };

      this._itemsandservices.push(item);
    });

    this.compute();
  }

  async doAddService() {
    var selectedItems = await this.browseItemService();

    selectedItems.forEach((record) => {
      var item = {
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        UnitPrice: record.UnitPrice,
        UnitCost: record.UnitCost,
        DateExpiration: record.OtherInfo_DateExpiration,
        ID_ItemType: record.ID_ItemType,
      };

      this._itemsandservices.push(item);
    });

    this.compute();

    this.onChange();
  }

  onChange() {
    this.compute();

    this.onChanged.emit({
      data: this._itemsandservices,
    });
  }

  async ItemsAndServicePatientColumn_onClick(
    itemsandservice: Patient_Confinement_ItemsServices_DTO
  ) {
    if (this.IsDisabled) return;
    if (itemsandservice.ID_Patient_SOAP) return;

    var selectedItems = await this.browsePatient();
    if (selectedItems.length == 0) return;

    itemsandservice.ID_Patient = selectedItems[0].ID;
    itemsandservice.Name_Patient = selectedItems[0].Name;
  }

  async ItemsAndServicePatientColumnBtnRemove_onClick(
    itemsandservice: Patient_Confinement_ItemsServices_DTO
  ) {
    if (this.IsDisabled) return;
    itemsandservice.ID_Patient = null;
    itemsandservice.Name_Patient = null;
  }

  onRowDeleting(itemsandservice: Patient_Confinement_ItemsServices_DTO) {
    if (this.IsDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this._itemsandservices,
      "ID",
      itemsandservice.ID + ""
    );

    this._itemsandservices.splice(index, 1);

    this.onChanged.emit({
      data: this._itemsandservices,
    });
  }

  compute() {
    var totalAmount: number = 0;

    this._itemsandservices.forEach((itemsandservice) => {
      if (
        this.ItemServiceMode == this.ConfimentItemServiceModeEnum_All ||
        (this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Item &&
          itemsandservice.ID_ItemType == this.ItemTypeEnum_Inventoriable) ||
        (this.ItemServiceMode == this.ConfimentItemServiceModeEnum_Service &&
          itemsandservice.ID_ItemType == this.ItemTypeEnum_Service)
      ) {
        var amount = itemsandservice.Quantity * itemsandservice.UnitPrice;

        itemsandservice.Amount = amount;
        totalAmount += amount;
      }
    });

    this.TotalAmount = totalAmount;
  }
}

export enum ConfimentItemServiceModeEnum {
  All = 1,
  Item = 2,
  Service = 3,
}
