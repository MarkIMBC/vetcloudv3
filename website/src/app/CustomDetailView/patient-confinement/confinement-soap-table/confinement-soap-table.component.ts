import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { FormHelperDialogService } from 'src/app/View/FormHelperView/FormHelperView.component';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { Patient_Confinement } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { FilingStatusEnum, Patient_SOAP_DTO } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'confinement-soap-table',
  templateUrl: './confinement-soap-table.component.html',
  styleUrls: ['./confinement-soap-table.component.less']
})
export class ConfinementSOAPTableComponent implements OnInit {

  @Input() configOptions: any = {};

  _ID_Patient_Confinement: number = 0;
  isMenuShow: boolean = true;

  _CurrentObject: Patient_Confinement = {};

  dataSource: any[] = [];
  selectedRecord: any = null;

  menuItems: MenuItem[] = [];

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService
  ) {

  }

  ngOnInit(): void {
  }

  async loadByCurrentObject(CurrentObject: Patient_Confinement){

    this._CurrentObject = CurrentObject;

    this._ID_Patient_Confinement = this._CurrentObject.ID;
    this.loadPatientSOAPs();
  }

  async load(ID_Patient_Confinement: number){

    this._ID_Patient_Confinement = ID_Patient_Confinement;
    this.loadPatientSOAPs();
  }

  async loadPatientSOAPs(): Promise<void> {

    this.dataSource = [];

    if(this._ID_Patient_Confinement < 0) return;
    
    var sql = `SELECT *
               FROM dbo.vPatient_SOAP_ListView
               WHERE 
                 ID_Patient_Confinement = ${this._ID_Patient_Confinement} 
    `;

    sql = this.cs.encrypt(sql);
    this.dataSource = await this.ds.query<any>(sql);

    this.dataSource.forEach(record => {

      record['SOAPRecords'] = [];

      record.SOAPRecords.push({
        label: 'Primary Complaint / History',
        value: record.History,
        hasValue: (record.Count_History > 0)
      });

      record.SOAPRecords.push({
        label: 'Clinical Exam',
        value: record.ClinicalExamination,
        hasValue: (record.Count_ClinicalExamination > 0)
      });

      record.SOAPRecords.push({
        label: 'Laboratory / Interpretation',
        value: record.Interpretation,
        hasValue: (record.Count_LaboratoryImages > 0)
      });

      record.SOAPRecords.push({
        label: 'Plan',
        value: '',
        hasValue: (record.Count_Plan > 0)
      });

      record.SOAPRecords.push({
        label: 'Diagnosis',
        value: record.Diagnosis,
        hasValue: (record.Count_Diagnosis > 0)
      });

      record.SOAPRecords.push({
        label: 'Treatment',
        value: record.Treatment,
        hasValue: (record.Count_Treatment > 0)
      });

      record.SOAPRecords.push({
        label: 'Prescription',
        value: record.Prescription,
        hasValue: (record.Count_Prescription > 0)
      });

      record.SOAPRecords.push({
        label: 'Client Communication',
        value: record.ClientCommunication,
        hasValue: (record.Count_ClientCommunication > 0)
      });

    });



    this.loadpatientSOAPMenuItems();    
  }

  private loadpatientSOAPMenuItems() {

    this.menuItems = [];

    if(this._CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined){

      this.menuItems.push(
        {
          label: 'New',
          icon: 'pi pi-fw pi-plus',
          command: async () => {
  
            this.gotoPatient_SOAPRoute(-1);
          }
        }
      );
    }

    this.menuItems.push(
      {
        label: 'View',
        icon: 'pi pi-fw pi-search',
        command: async () => {

          if (isNullOrUndefined(this.selectedRecord)) return;

          this.gotoPatient_SOAPRoute(this.selectedRecord.ID);
        }
      }
    );
  }

  private gotoPatient_SOAPRoute(ID_PatienPatient_SOAP) {

    var routeLink = [];
    var config = {

      prevConfig: this.configOptions,
      ID_Patient_Confinement: this._ID_Patient_Confinement,
      BackRouteLink: [`/Main`, 'Confinement', this._ID_Patient_Confinement]
    };

    routeLink = [`/Main`, 'Patient_SOAP', ID_PatienPatient_SOAP];
    this.globalFx.customNavigate(routeLink, config);
  }

  Patient_SOAPGrid_onDblClick(){

    if (isNullOrUndefined(this.selectedRecord)) return;

    this.gotoPatient_SOAPRoute(this.selectedRecord.ID);
  }
}
