import { Company, Employee } from './../../../bo/APP_MODELS';
import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  FilingStatusEnum,
  PositionEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { MenuItem } from "primeng/api";
import { isNullOrUndefined, isNumber } from "util";
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { IAppLookupBoxOption } from 'src/utils/controls/appLookupBox/appLookupBox.component';

@Component({
  selector: 'app-document-series',
  templateUrl: './document-series.component.html',
  styleUrls: ['./document-series.component.less']
})
export class DocumentSeriesComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.DOCUMENTSERIES;
  
  @ViewChild('appForm')
  appForm: AppFormComponent;

  ID_Model_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    propertyKey: 'Oid',
    sourceKey: this.cs.encrypt(`
            SELECT Oid,
                   Name
            FROM dbo._tModel
            WHERE IsActive = 1
    `),
  };

  ID_Model_LookUpBox_onSelectedItem($event){

    this.CurrentObject.ID_Model = $event.Oid;
  }

}
