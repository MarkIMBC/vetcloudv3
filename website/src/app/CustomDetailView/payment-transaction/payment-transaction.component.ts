import { APP_REPORTVIEW, Item, PaymentMethod } from './../../../bo/APP_MODELS';
import { BillingInvoice_Detail_DTO, FilingStatusEnum, PaymentTransaction_DTO, PropertyTypeEnum, TaxSchemeEnum, PaymentMethodEnum } from './../../../utils/controls/class_helper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { FormHelperDialogService } from 'src/app/View/FormHelperView/FormHelperView.component';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_MODEL, BillingInvoice_Detail } from 'src/bo/APP_MODELS';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { BillingInvoice_DTO } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomDetailView } from '../BaseCustomDetailView';
import { IAppLookupBoxOption } from 'src/utils/controls/appLookupBox/appLookupBox.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { isNullOrUndefined, isNumber } from 'util';
import { MenuItem } from 'primeng/api';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';

@Component({
  selector: 'app-payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.less']
})
export class PaymentTransactionComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.PAYMENTTRANSACTION;

  CurrentObjectDetailNames: string[] = [PaymentTransactionDetailNames.BillingInvoice_Detail]
  homeBreadCrumb: MenuItem = { icon: 'pi pi-home', routerLink: '/PaymentTransactions' };;
  PAYMENTMETHOD_CASH = 1;
  PAYMENTMETHOD_CHECK = 2;
  PAYMENTMETHOD_GCASH = 3;
  PAYMENTMETHOD_DebitCredit = 4;
  PAYMENTMETHOD_CREDITS = 5;

  ID_BillingInvoice_LookupBoxOption: IAppLookupBoxOption = {

    apiUrl: 'Model/GetList/null',
    sourceKey: this.cs.encrypt(`
          SELECT  ID,
                  Code Name,
                  Name_Patient [Patient],
                  RemainingAmount [Rem. Amount] ,
                  Payment_Name_FilingStatus [Payment Status]
              FROM dbo.vBillingInvoice
              WHERE 
                  Payment_ID_FilingStatus IN (${FilingStatusEnum.Pending}, ${FilingStatusEnum.PartiallyPaid}) AND
                  ID_Company = ${this.currentUser.ID_Company} AND
                  ID_FilingStatus IN (${FilingStatusEnum.Approved})
             
    `)
  };

  protected loadInitMenu() {

    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {

      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  compute() {

    var netAmount = 0;
    var cashAmount = 0
    var checkAmount = 0
    var gCashAmount = 0
    var cardAmount = 0
    var creditAmount = 0

    var payableAmount = 0
    var paymentAmount = 0
    var changeAmount = 0

    this.resetPaymentMethodAmount();

    netAmount = this.CurrentObject.RemainingAmount_BillingInvoice;
    netAmount = this.globalFx.roundOffDecimal(netAmount);

    cashAmount = this.CurrentObject.CashAmount;
    checkAmount = this.CurrentObject.CheckAmount;
    gCashAmount = this.CurrentObject.GCashAmount;
    cardAmount = this.CurrentObject.CardAmount;
    creditAmount = this.CurrentObject.CreditAmount;

    payableAmount = netAmount;
    paymentAmount = cashAmount + checkAmount + gCashAmount + cardAmount + creditAmount;
    paymentAmount = this.globalFx.roundOffDecimal(paymentAmount);

    if(this.CurrentObject.ID_PaymentMethod != PaymentMethodEnum.Credits)
    if (payableAmount < paymentAmount) {

      changeAmount = paymentAmount - payableAmount;
    }

    changeAmount = this.globalFx.roundOffDecimal(changeAmount);

    this.CurrentObject.PayableAmount = payableAmount;
    this.CurrentObject.PaymentAmount = paymentAmount;
    this.CurrentObject.ChangeAmount = changeAmount;
  }

  async loadBillingInvoiceDetail(): Promise<void> {

    var sql = `SELECT 
                      ID,
                      Name_Item,
                      Quantity,
                      UnitPrice,
                      Amount
                FROM dbo.vBillingInvoice_Detail
                WHERE 
                      ID_BillingInvoice = ${this.CurrentObject.ID_BillingInvoice}
    `;

    sql = this.cs.encrypt(sql);
    this.CurrentObject.BillingInvoice_Detail = await this.ds.query<BillingInvoice_Detail_DTO>(sql);

  }

  resetPaymentMethodAmount() {

    if (this.CurrentObject.ID_PaymentMethod != this.PAYMENTMETHOD_CASH) {

      this.CurrentObject.CashAmount = 0.00;
    }

    if (this.CurrentObject.ID_PaymentMethod != this.PAYMENTMETHOD_CHECK) {

      this.CurrentObject.CheckNumber = "";
      this.CurrentObject.CheckAmount = 0.00;
    }

    if (this.CurrentObject.ID_PaymentMethod != this.PAYMENTMETHOD_GCASH) {

      this.CurrentObject.ReferenceTransactionNumber = "";
      this.CurrentObject.GCashAmount = 0.00;
    }

    if (this.CurrentObject.ID_PaymentMethod != this.PAYMENTMETHOD_DebitCredit) {

      this.CurrentObject.ID_CardType = 0;
      this.CurrentObject.CardNumber = "";
      this.CurrentObject.CardHolderName = "";
      this.CurrentObject.CardAmount = 0.00;
    }
    if (this.CurrentObject.ID_PaymentMethod != this.PAYMENTMETHOD_CREDITS) {

      this.CurrentObject.CreditAmount = 0.00;
    }
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if (Number.isNaN(this.CurrentObject.PaymentAmount)) {

      validations.push({
        message: 'Payment Amount is required.',
      })
    }

    if (isNullOrUndefined(this.CurrentObject.PaymentAmount)) {

      validations.push({
        message: 'Payment Amount is required.',
      })
    }

    if (this.CurrentObject.PaymentAmount < 1) {

      validations.push({
        message: 'Payment Amount is required.',
      })
    }

    if (this.CurrentObject.ChangeAmount < 0) {

      validations.push({
        message: 'Invalid Payment Amount.',
      })
    }

    if(this.CurrentObject.ID_PaymentMethod == PaymentMethodEnum.Credits){

      if (this.CurrentObject.CreditAmount > this.CurrentObject.PayableAmount) {

        validations.push({
          message: `Credit Amount must be equal or less than from Payable Amount`,
        })
      }
    }

    return Promise.resolve(validations);
  };

  async ID_BillingInvoice_LookUpBox_onSelectedItem(event: any) {

    var ID_BillingInvoice = event.ID;
    var obj = await this.ds.loadObject<BillingInvoice_DTO>(APP_MODEL.BILLINGINVOICE, ID_BillingInvoice, {});

    this.CurrentObject.ID_BillingInvoice = obj.ID;
    this.CurrentObject.BillingInvoice = obj.Code;
    this.CurrentObject.ID_TaxScheme = obj.ID_TaxScheme;
    this.CurrentObject.Name_TaxScheme = obj.Name_TaxScheme;


    this.CurrentObject.RemainingAmount_BillingInvoice = obj.RemainingAmount;


    this.CurrentObject.Name_Patient = obj.Name_Patient;

    this.loadBillingInvoiceDetail();
    this.compute();
  }

  async ID_PaymentMethod_SelectBox_ValueChanged(event: any) {

    this.CurrentObject.Name_PaymentMethod = event.Name;
  }

  async ID_CardType_SelectBox_ValueChanged(event: any) {

    this.CurrentObject.Name_CardType = event.Name;
  }

  async CashAmount_NumberBox_ValueChanged(event: any) {

    this.compute();
  }

  async CheckAmount_NumberBox_ValueChanged(event: any) {

    this.compute();
  }

  async GCashAmount_NumberBox_ValueChanged(event: any) {

    this.compute();
  }

  async CardAmount_NumberBox_ValueChanged(event: any) {

    this.compute();
  }

  async CreditAmount_NumberBox_ValueChanged(event: any) {

    this.compute();
  }

  ID_PaymentMethod_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tPaymentMethod")
  };

  ID_CardType_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tCardType")
  };

  loadMenuItem() {

    var menuItemApprove: MenuItem = {

      label: 'Approve',
      icon: 'pi pi-fw pi-save blue-text',
      command: async () => {

        if (this.appForm.isDirty) {

          this.msgBox.warning(`${this.CurrentObject.Code} changes is not yet saved.`);
          return;
        }

        var result = await this.msgBox.confirm(`Would you like to approve ${this.CurrentObject.Code}?`, "Payment Transaction", "Yes", "No");
        if (!result) return;

        var obj = await this.approve();

        if (obj.Success) {

          await this.loadCurrentRecord();
        }
      }
    };

    var menuItemCancel: MenuItem = {

      label: 'Cancel',
      icon: 'pi pi-fw pi-times red-text',
      command: async () => {

        if (this.appForm.isDirty) {

          this.msgBox.warning(`${this.CurrentObject.Code} changes is not yet saved.`);
          return;
        }

        var result = await this.msgBox.confirm(`Would you like to cancel ${this.CurrentObject.Code}?`, "Payment Transaction", "Yes", "No");
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {

          await this.loadCurrentRecord();
        }
      }
    }

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.addMenuItem(menuItemApprove);
      this.addMenuItem(menuItemCancel);
    } else if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved) {

      this.addMenuItem(menuItemCancel);
    }

    this.addMenuItem({
      label: 'Report',
      icon: 'pi pi-fw pi-folder blue-text',
      items: [
        {
          label: 'PT Report',
          icon: 'pi pi-file-o',
          command: async () => {

            var filterFormValue: IFilterFormValue[] = [];
            var options = {

              prevConfig: this.configOptions,
              BackRouteLink: [`/Main`, "PaymentTransaction", this.CurrentObject.ID],
              backLink: this.router.url
            }

            filterFormValue.push({
              dataField: "ID",
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID
            });

            this.globalFx.navigateReport(
              'PaymentTransactionReport',
              APP_REPORTVIEW.PAYMENTTRANSACTIONREPORT,
              filterFormValue,
              options
            );
          }
        }
      ]
    });

  }

  protected formDisabled(): boolean {

    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {

      isDisabled = true;
    }

    return isDisabled;
  }

  async approve(): Promise<any> {

    return new Promise<Item[]>(async (res, rej) => {

      var obj = await this.ds.execSP("pApprovePaymentTransaction", {
        IDs_PaymentTransaction: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession
      }, {
        isReturnObject: true,
        isTransaction: true
      })

      if (obj.Success) {

        this.msgBox.success(`${this.CurrentObject.Code} has been approved successfully.`, `Approved ${this.model.Caption}`);
        res(obj);
      }
      else {

        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Approve ${this.model.Caption}`);
        rej(obj);
      }
    });

  }

  async cancel(): Promise<any> {

    return new Promise<Item[]>(async (res, rej) => {

      var obj = await this.ds.execSP("pCancelPaymentTransaction", {
        IDs_PaymentTransaction: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession
      }, {
        isReturnObject: true,
        isTransaction: true
      })

      if (obj.Success) {

        this.msgBox.success(`${this.CurrentObject.Code} has been canceled successfully.`, `Cancel ${this.model.Caption}`);
        res(obj);
      }
      else {

        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Cancel ${this.model.Caption}`);
        rej(obj);
      }
    });

  }

  CurrentObject_onLoad() {

    this.loadMenuItem();
  }

  static async PrintDialog(dataService, ID_PaymentTransaction) {

    var obj = await dataService.execSP(
      "pGetPaymentTransactionPrintReceiptLayout",
      {
        ID_PaymentTransaction: ID_PaymentTransaction
      },
      {
        isReturnObject: true,
      }
    );

    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write(`
    
      <html>
        <head>
          <title>${obj.title}</title>
          <style>${obj.style}</style>
        </head>
        <body>
          ${obj.content}
        </body>
      <body>
    `);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    if (isMobile) obj.isAutoPrint = false;

    if (obj.isAutoPrint) {

      setTimeout(() => {

        mywindow.print();
        mywindow.close();
      }, obj.millisecondDelay);
    }

    return true;
  }

}

export class PaymentTransactionDetailNames {

  static readonly BillingInvoice_Detail?: string = 'BillingInvoice_Detail'
}
