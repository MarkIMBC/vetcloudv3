import { AppRadioButtonListDataSourceItem } from "./../../../utils/controls/appRadioButtonList/appRadioButtonList.component";
import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_DTO,
  PropertyTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import {
  APP_MODEL,
  APP_REPORTVIEW,
  Client,
  Employee,
  Item,
} from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  IAppLookupBoxOption,
  AppLookupBoxComponent,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";

@Component({
  selector: "app-veterinary-health-certificate",
  templateUrl: "./veterinary-health-certificate.component.html",
  styleUrls: ["./veterinary-health-certificate.component.less"],
})
export class VeterinaryHealthCertificateComponent
  extends BaseCustomDetailView
  implements OnInit
{
  @ViewChild("ID_Client_LookUpBox")
  ID_Client_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  currentModelID: string = APP_MODEL.VETERINARYHEALTHCERTIFICATE;

  CurrentObjectDetailNames: string[] = [];
  VaccinationOption_ListDataSource: AppRadioButtonListDataSourceItem[] = [];
  CaseTypeList: any[] = [];

  async loadCaseTypeList(): Promise<void> {
    this.CaseTypeList = [];

    var sql = this.cs.encrypt(
      `SELECT 
              ID, 
              Name
        FROM dbo.tCaseType
    `
    );

    var _CaseTypeList = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      _CaseTypeList.push(obj.Name);
    });

    this.CaseTypeList = _CaseTypeList;
  }

  async loadVaccinationOptionListDataSource(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT ID, Comment FROM dbo.tVaccinationOption
      `
    );
    debugger;

    var _VaccinationOption_ListDataSource: AppRadioButtonListDataSourceItem[] =
      [];
    var objs = await this.ds.query<any>(sql);

    this.VaccinationOption_ListDataSource = [];

    objs.forEach(function (obj) {
      var item: AppRadioButtonListDataSourceItem =
        new AppRadioButtonListDataSourceItem();

      item.value = obj.ID;
      item.label = obj.Comment;

      _VaccinationOption_ListDataSource.push(item);
    });

    this.VaccinationOption_ListDataSource = _VaccinationOption_ListDataSource;
  }

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_Item_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Code, 
               Name
        FROM vActiveItem
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
    columns: [
      {
        dataField: "Code",
        caption: "Code",
        width: 80,
      },
      {
        dataField: "Name",
        caption: "Name",
      },
    ],
  };

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tPatient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name,
               Name_Position Position
        FROM vAttendingVeterinarian
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} 
    `),
  };

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];

    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;
    var obj = await this.ds.loadObject<Client>(APP_MODEL.CLIENT, ID_Client, {});

    this.CurrentObject.BillingAddress = obj.Address;
    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = null;

    if (this.ID_Patient_LookUpBox) {
      this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);
    }
  }

  ID_Item_LookUpBox_onSelectedItem(event: any) {}

  async ID_Client_LookUpBox_onRemoveSelectedItem(event: any) {
    this.CurrentObject.BillingInvoice_Patient = [];
  }

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {
    var ID_Patient = event.ID;
    var obj = await this.ds.loadObject<Patient_DTO>(
      APP_MODEL.PATIENT,
      ID_Patient,
      {}
    );
  }

  loadMenuItem() {
    var reportMenuItem = {
      label: "Report",
      icon: "pi pi-fw pi-folder blue-text",
      items: [],
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var vetCertificateReportMenu = {
      label: "Vet Certificate",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [
            `/Main`,
            "VeterinaryHealthCertificate",
            this.CurrentObject.ID,
          ],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "VeterinaryHealthCertificate",
          APP_REPORTVIEW.VETERINARYHEALTHCLINIC,
          filterFormValue,
          options
        );
      },
    };

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      reportMenuItem.items.push(vetCertificateReportMenu);

      this.addMenuItem(menuItemCancel);
      this.addMenuItem(reportMenuItem);
    }
  }

  async CustomDetailView_onInit() {
    await this.loadVaccinationOptionListDataSource();
  }

  async CurrentObject_onLoad() {
    this.loadCaseTypeList();
    this.loadMenuItem();

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);

    this.ID_Patient_LookUpBox.initOption();

    console.log(this.CurrentObject);
  }

  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelVeterinaryHealthCertificate",
        {
          IDs_VeterinaryHealthCertificate: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Name_Patient} Veterinary Health Certificate has been cancel.`,
          `Veterinary Health Certificate`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.CurrentObject.Name_Patient} Veterinary Health Certificate`
        );
        rej(obj);
      }
    });
  }

  protected pGetRecordOptions(): any {
    var options = {};

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }
    return options;
  }
}
