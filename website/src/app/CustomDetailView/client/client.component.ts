import {
  BillingInvoice_DTO,
  FilingStatusEnum,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, APP_REPORTVIEW, Client } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { MenuItem } from "primeng/api";
import { isNullOrUndefined, isNumber } from "util";
import {
  AppFormComponent,
  IFilterFormValue,
} from "src/utils/controls/appForm/appForm.component";
import { ValidationService } from "src/utils/service/validation.service";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";
import { Patient_DTO, PropertyTypeEnum } from "src/utils/controls/class_helper";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { InputSwitchModule } from "primeng/inputswitch";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { ClientCreditAdjustmentDialogBoxComponent } from "../../../utils/client-credit-adjustment/client-credit-adjustment.component";
import { CreditTrailListComponent } from "./credit-trail-list/credit-trail-list.component";

@Component({
  selector: "app-client",
  templateUrl: "./client.component.html",
  styleUrls: ["./client.component.less"],
})
export class ClientComponent extends BaseCustomDetailView implements OnInit {
  currentModelID: string = APP_MODEL.CLIENT;

  sellPatient: Patient_DTO;
  selpBilling: BillingInvoice_DTO;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  @ViewChild("clientcreditadjustmentdialogbox")
  clientcreditadjustmentdialogbox: ClientCreditAdjustmentDialogBoxComponent;

  @ViewChild("credittraillist")
  credittraillist: CreditTrailListComponent;

  CurrentObjectDetailNames: string[] = [
    ClientNames.Client_Patient,
    ClientNames.Client_BillingInvoice,
  ];

  detailMenuRemarks: MenuItem[] = [
    {
      label: "Fill Template",
      icon: "",
      command: async () => {
        this.CurrentObject.Comment = this.CurrentObject.CommentTemplate;
      },
    },
  ];

  patientMenuItems: MenuItem[] = [];
  billingInvoiceMenuItems: MenuItem[] = [];

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var menuItem_CreatePet: MenuItem = {
      label: "New Pet Record",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.gotoPatientRoute(-1);
      },
    };

    var menuItem_CreateBilling: MenuItem = {
      label: "Create Billing",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.gotoBillingInvoiceRoute(-1);
      },
    };

    if (this.CurrentObject.ID > 0) {
      this.gotoMenuItems.push(menuItem_CreatePet);
      this.gotoMenuItems.push(menuItem_CreateBilling);
    }
  }

  loadpatientMenuItems() {
    this.patientMenuItems = [];
    if (this.CurrentObject.ID < 1) return;

    this.patientMenuItems.push({
      label: "New",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.gotoPatientRoute(-1);
      },
    });

    this.patientMenuItems.push({
      label: "View",
      icon: "pi pi-fw pi-search",
      command: async () => {
        if (isNullOrUndefined(this.sellPatient)) return;

        this.gotoPatientRoute(this.sellPatient.ID);
      },
    });
  }

  loadbillingInvoiceMenuItems() {
    this.billingInvoiceMenuItems = [];
    if (this.CurrentObject.ID < 1) return;

    this.billingInvoiceMenuItems.push({
      label: "New",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        this.gotoBillingInvoiceRoute(-1);
      },
    });

    this.billingInvoiceMenuItems.push({
      label: "View",
      icon: "pi pi-fw pi-search",
      command: async () => {
        if (isNullOrUndefined(this.selpBilling)) return;

        this.gotoBillingInvoiceRoute(this.selpBilling.ID);
      },
    });
  }

  billingInvoices: BillingInvoice_DTO[];

  async loadBillingInvoice() {
    var sql = this.cs.encrypt(`
            SELECT  ID,
                    DateString,
                    Code,
                    NetAmount,
                    RemainingAmount,
                    Name_FilingStatus,
                    Payment_Name_FilingStatus
            FROM dbo.vBillingInvoice
            WHERE 
              ID_Client = ${this.CurrentObject.ID} AND
              ID_FilingStatus IN (${FilingStatusEnum.Filed}, ${FilingStatusEnum.Approved})
          `);

    this.billingInvoices = await this.ds.query<BillingInvoice_DTO>(sql);
  }

  private gotoPatientRoute(ID_Patient) {
    var routeLink = [];
    var config = {};

    config = {
      ID_Client: this.CurrentObject.ID,
      BackRouteLink: [`/Main`, "Client", this.CurrentObject.ID],
      prevConfig: this.configOptions,
    };

    routeLink = [`/Main`, "Patient", ID_Patient];
    this.globalFx.customNavigate(routeLink, config);
  }

  private gotoBillingInvoiceRoute(ID_BillingInvoice) {
    var routeLink = [];
    var config = {};

    config = {
      ID_Client: this.CurrentObject.ID,
      BackRouteLink: [`/Main`, "Client", this.CurrentObject.ID],
      prevConfig: this.configOptions,
    };

    routeLink = [`/Main`, "BillingInvoice", ID_BillingInvoice];
    this.globalFx.customNavigate(routeLink, config);
  }

  loadMenuItems() {
    // var menuItemAdjustCredit: MenuItem = {
    //   label: "Adjust Credit",
    //   icon: "pi pi-fw pi-save blue-text",
    //   command: async () => {
    //     this.clientcreditadjustmentdialogbox.Load(this.CurrentObject.ID).then(() => {
    //       this.loadCurrentRecord();
    //     });
    //   },
    // };
    // if(this.CurrentObject.ID > 0){
    //   this.addMenuItem(menuItemAdjustCredit);
    // }
  }

  CurrentObject_onLoad() {
    this.loadGotoMenu();
    this.loadMenuItems();

    this.loadpatientMenuItems();
    this.loadbillingInvoiceMenuItems();

    this.loadBillingInvoice();

    if (this.credittraillist) {
      this.credittraillist.loadByID(this.CurrentObject.ID);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];
    var ContactNumber = this.CurrentObject.ContactNumber;

    if (ContactNumber == null || ContactNumber == undefined) ContactNumber = "";

    var validateObj = await this.ds.execSP(
      "pValidateClient",
      {
        ID_Client: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true && validateObj.isWarning == false) {
      validations.push({ message: validateObj.message });
    }

    if (ContactNumber.length > 0) {
      if (!this.validationService.validatePHMobileNumber(ContactNumber)) {
        validations.push({ message: "Invalid Mobile Number" });
      }
    }

    return Promise.resolve(validations);
  }

  async confirmSaving(): Promise<boolean> {
    var title = this.ModelDisplayName;
    var message = "Do you want to save changes?";

    var validateObj = await this.ds.execSP(
      "pValidateClient",
      {
        ID_Client: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true && validateObj.isWarning == true) {
      message = validateObj.message + " Would like to continue saving?";
    }

    var options = {
      message: message,
      title: title,
      positiveButtonText: "Yes",
      negativeButtonText: "No",
      backdrop: false,
    };

    var result = await this.msgBox.showConfirmBox(options);

    return result;
  }

  Patient_Grid_onDblClick(Patient: Patient_DTO) {
    this.gotoPatientRoute(Patient.ID);
  }

  pBilling_onDblClick(pBilling: BillingInvoice_DTO) {
    var routeLink = [];
    var config = {
      ID_Client: this.CurrentObject.ID,
      BackRouteLink: [`/Main`, "Client", this.CurrentObject.ID],
      prevConfig: this.configOptions,
    };

    routeLink = [`/Main`, "BillingInvoice", pBilling.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  credittraillist_onSavedCreditAmount() {
    this.loadCurrentRecord();
  }

  credittraillist_onRowClick(e: any) {
    var record = e.record;

    if (record["ID_Patient_Confinement"]) {
      var routeLink = [];
      var config = {
        ID_Client: this.CurrentObject.ID,
        BackRouteLink: [`/Main`, "Confinement", this.CurrentObject.ID],
        prevConfig: this.configOptions,
      };
      routeLink = [`/Main`, "Confinement", record.ID_Patient_Confinement];
      this.globalFx.customNavigate(routeLink, config);
    }
  }
}

class ClientNames {
  static readonly Client_Patient?: string = "Client_Patient";
  static readonly Client_BillingInvoice?: string = "Client_BillingInvoice";
}
