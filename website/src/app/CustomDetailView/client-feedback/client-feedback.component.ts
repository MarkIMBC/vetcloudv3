import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";

@Component({
  selector: "app-client-feedback",
  templateUrl: "./client-feedback.component.html",
  styleUrls: ["./client-feedback.component.less"],
})
export class ClientFeedbackComponent
  extends BaseCustomDetailView
  implements OnInit
{

  CurrentObjectDetailNames: string[] = [];

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  protected async loadCurrentModel() {
    var obj = await this.ds.execSP(
      "pGetModelByName",
      {
        name: "ClientFeedback",
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;
    this.currentModelID = this.model.Oid;

    this._ModelDisplayName = isNullOrUndefined(this.model.DisplayName)
      ? this.model.Name
      : this.model.DisplayName;

    this.ModelDisplayName = this._ModelDisplayName;
  }
}
