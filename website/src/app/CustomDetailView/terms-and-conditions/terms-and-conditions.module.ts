import {
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { TermsAndConditionsComponent } from "./terms-and-conditions.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { QRCodeModule } from "angularx-qrcode";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { SharedModule } from "primeng/api";
import { MenubarModule } from "primeng/menubar";
import { StepsModule } from "primeng/steps";
import { TableModule } from "primeng/table";
import { TabViewModule } from "primeng/tabview";
import { appInit } from "src/app/app.module";
import {
  RUNTIME_COMPILER_PROVIDERS,
  DynamicTemplateService,
} from "src/app/AppServices/dynamic-temaplate.svc";
import {
  LoaderService,
  LoaderInterceptor,
} from "src/app/AppServices/LoaderInterceptor";
import { TabService } from "src/app/AppServices/tab.service";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ModelDirtyGuardService } from "src/app/auth.guard";
import { DetailViewDialogService } from "src/app/View/DetailViewDialog/DetailViewDialog.component";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { ValidationService } from "src/utils/service/validation.service";

const routes: Routes = [{ path: "", component: TermsAndConditionsComponent }];

@NgModule({
  declarations: [TermsAndConditionsComponent],
  imports: [
    CommonModule,
    MenubarModule,
    TabViewModule,
    TableModule,
    StepsModule,
    SharedModule,
    PerfectScrollbarModule,
    QRCodeModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    ModelDirtyGuardService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    ...RUNTIME_COMPILER_PROVIDERS,
    DataService,
    ValidationService,
    DynamicTemplateService,
    TabService,
    ListViewModalService,
    DetailViewDialogService,
    FormHelperDialogService,
    UserAuthenticationService,
    CrypterService,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class TermsAndConditionsModule {}
