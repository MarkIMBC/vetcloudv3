import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { Patient_Vaccination_DTO, FilingStatusEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'vaccination-list',
  templateUrl: './vaccination-list.component.html',
  styleUrls: ['./vaccination-list.component.less'],
})
export class VaccinationListComponent implements OnInit {

  _ID_Pattent: number = 0;
  Records: Patient_Vaccination_DTO[] = []
  SelectedRecord: Patient_Vaccination_DTO

  @Input() DetailViewConfigOptions: any = {};

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {

    this.loadMenuItems();
  }

  async LoadByPatientID(ID_Patient: number) {

    this._ID_Pattent = ID_Patient;

    this.loadMenuItems();

    var sql = `SELECT *
               FROM dbo.vPatient_Vaccination_Listview
               WHERE 
                ID_Patient = ${this._ID_Pattent} AND
                ID_FilingStatus IN (${FilingStatusEnum.Filed})
      `;

    sql = this.cs.encrypt(sql);
    this.Records = await this.ds.query<any>(sql);

    this.loadMenuItems();

  }

  loadMenuItems() {

    var menuItems_New: MenuItem = {
      label: 'Create',
      icon: "pi pi-fw pi-plus",
      command: () => {

        var routeLink = [];
        var config = {

          prevConfig: this.DetailViewConfigOptions,
          ID_Patient: this._ID_Pattent,
          BackRouteLink: [`/Main`, 'Patient', this._ID_Pattent]
        };

        routeLink = [`/Main`, 'Patient_Vaccination', -1];
        this.globalFx.customNavigate(routeLink, config);
      }
    }

    var menuItems_View: MenuItem = {
      label: 'View',
      icon: 'pi pi-fw pi-search',
      command: () => {

        if (!this.SelectedRecord) return;

        var routeLink = [];
        var config = {

          prevConfig: this.DetailViewConfigOptions,
          ID_Patient: this._ID_Pattent,
          BackRouteLink: [`/Main`, 'Patient', this._ID_Pattent]
        };

        routeLink = [`/Main`, 'Patient_Vaccination', this.SelectedRecord.ID];
        this.globalFx.customNavigate(routeLink, config);
      }
    }

    var menuItems_Refresh: MenuItem = {
      label: 'Refresh',
      icon: 'pi pi-fw pi-refresh',
      command: () => {

        this.LoadByPatientID(this._ID_Pattent);
      }
    }

    this.menuItems = [];

    this.menuItems.push(menuItems_New);

    this.menuItems.push(menuItems_View);

    this.menuItems.push(menuItems_Refresh);
  }

  recordGrid_onDblClick() {

    if (!this.SelectedRecord) return;

    var routeLink = [];
    var config = {

      prevConfig: this.DetailViewConfigOptions,
      ID_Patient: this._ID_Pattent,
      BackRouteLink: [`/Main`, 'Patient', this._ID_Pattent]
    };

    routeLink = [`/Main`, 'Patient_Vaccination', this.SelectedRecord.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  recordGrid_onRowSelect() {

    this.loadMenuItems();
  }

  recordGrid_onRowUnselect() {

    this.loadMenuItems();
  }
}
