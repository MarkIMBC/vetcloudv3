import { BillingInvoice } from "./../../../bo/APP_MODELS";
import { Component, OnInit, ViewChild } from "@angular/core";
import { APP_MODEL, APP_REPORTVIEW, Patient_History } from "src/bo/APP_MODELS";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  Patient_SOAP_DTO,
  Patient_DTO,
  Patient_History_DTO,
  PropertyTypeEnum,
  UserGroupEnum,
} from "src/utils/controls/class_helper";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { isNull, isNullOrUndefined, isNumber } from "util";
import {
  AppSelectBoxComponent,
  IAppSelectBoxOption,
} from "src/utils/controls/appSelectBox/appSelectBox.component";
import { AppointmentscheduleStepStatusComponent } from "src/app/Shared/appointmentschedule-step-status/appointmentschedule-step-status.component";
import { AppLookupBoxComponent } from "src/utils/controls";
import { MenuItem } from "primeng/api";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { DataService } from "src/utils/service/data.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import {
  FilterCriteriaType,
  LabelPositionEnum,
} from "src/utils/controls/appControlContainer/appControlContainer.component";
import * as moment from "moment";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { IAppLookupBoxOption } from "src/utils/controls/appLookupBox/appLookupBox.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import {
  NgxQrcodeElementTypes,
  NgxQrcodeErrorCorrectionLevels,
} from "@techiediaries/ngx-qrcode";
import { VaccinationListComponent } from "./vaccination-list/vaccination-list.component";
import { WellnessDetailListComponent } from "./wellness-detail-list/wellness-detail-list.component";
import { VeterinaryHealthCertificateListComponent } from "./veterinary-health-certificate-list/veterinary-health-certificate-list.component";
import { PatientSOAPListComponent } from "./patient-soaplist/patient-soaplist.component";

@Component({
  selector: "app-patient",
  templateUrl: "./patient.component.html",
  styleUrls: ["./patient.component.less"],
})
export class PatientComponent extends BaseCustomDetailView {
  @ViewChild("vaccinationlist")
  vaccinationlist: VaccinationListComponent;

  @ViewChild("patientsoaplist")
  patientsoaplist: PatientSOAPListComponent;

  @ViewChild("wellnessdetaillist")
  wellnessdetaillist: WellnessDetailListComponent;

  @ViewChild("veterinaryhealthcertificatelist")
  veterinaryhealthcertificatelist: VeterinaryHealthCertificateListComponent;

  @ViewChild("ID_Item_LookUpBox")
  ID_Gender_SelectBox: AppSelectBoxComponent;

  @ViewChild("ID_Country_SelectBox")
  ID_Country_SelectBox: AppSelectBoxComponent;

  @ViewChild("appointSchedStepStatus")
  appointSchedStepStatus: AppointmentscheduleStepStatusComponent;

  @ViewChild("ID_Client_LookUpBox")
  ID_Client_LookUpBox_LookUpBox: AppLookupBoxComponent;

  selPatient_SOAP: Patient_SOAP_DTO = null;
  selHistory: Patient_History_DTO = null;
  selpBilling: any = null;

  currentModelID: string = APP_MODEL.PATIENT;

  isShowDentistTab: boolean = true;

  async CustomDetailView_onInit() {
    var userTOken = this.userAuthSvc.getDecodedToken();
    this.isShowDentistTab =
      userTOken.ID_UserGroup != UserGroupEnum.Receptionist;

    this.loadBreedSpecieList();
  }

  patientSOAPMenuItems: MenuItem[] = [];
  historyExamMenuItems: MenuItem[] = [];

  protected _ID_AppointmentSchedule: number;

  CurrentObjectDetailNames: string[] = [
    PatientDetailNames.Patient_Patient_SOAP,
    PatientDetailNames.Patient_History,
    PatientDetailNames.Patient_History_DTO,
    PatientDetailNames.Patient_SOAP_RegularConsoltation,
    PatientDetailNames.Patient_Billing,
    PatientDetailNames.Patient_SOAP,
  ];

  loadGotoMenu() {
    this.gotoMenuItems = [];

    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      ID_Patient: this.CurrentObject.ID,
      BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
    };

    var menuItem_CreateSOAP: MenuItem = {
      label: "Create Medical Record",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        routeLink = [`/Main`, "Patient_SOAP", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_CreateCertificate: MenuItem = {
      label: "Create Vet Certificate",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        routeLink = [`/Main`, "VeterinaryHealthCertificate", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_CreateAppointment: MenuItem = {
      label: "Create Appointment",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        routeLink = [`/Main`, "PatientAppointment", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {
      this.gotoMenuItems.push(menuItem_CreateSOAP);
      this.gotoMenuItems.push(menuItem_CreateCertificate);
      this.gotoMenuItems.push(menuItem_CreateAppointment);
    }
  }

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND
          IsActive = 1
    `),
  };

  ID_Gender_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tGender"),
  };

  ID_Country_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(
      "Select ID, Name + ' (+' + ISNULL(PhoneCode,'') + ') ' Name FROM dbo.tCountry"
    ),
  };

  async ID_Gender_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_Gender = event.Name;
  }

  async ID_Country_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.PhoneCode_Country = event.Name;
  }

  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;
  }

  async loadPatientBilling(): Promise<void> {
    var sql = `SELECT 
                      ID,
                      DateString,
                      Code,
                      NetAmount,
                      RemainingAmount,
                      Payment_Name_FilingStatus
                FROM dbo.vBillingInvoice
                WHERE 
                  ID_Patient = ${this.CurrentObject.ID} 
                    AND
                  ID_FilingStatus IN (${FilingStatusEnum.Filed}, ${FilingStatusEnum.Approved})
    `;

    sql = this.cs.encrypt(sql);
    this.CurrentObject.Patient_Billing = await this.ds.query<any>(sql);
  }

  BreedSpecieList: string[] = [];

  async loadBreedSpecieList(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT 
              ID, 
              Name
        FROM dbo.tBreedSpecie
    `
    );

    var _BreedSpecieList = [];

    var objs = await this.ds.query<any>(sql);

    this.BreedSpecieList = [];
    objs.forEach(function (obj) {
      _BreedSpecieList.push(obj.Name);
    });

    this.BreedSpecieList = _BreedSpecieList;
  }

  CustomDetailView_onLoad() {
    if (!isNullOrUndefined(this.ID_Country_SelectBox))
      this.ID_Country_SelectBox.initOption();
  }

  protected pGetRecordOptions(): any {
    var options = {};

    options["ID_Client"] = this.configOptions["ID_Client"];

    return options;
  }

  loadMenuItem() {
    this.loadModelReports();
  }

  async loadModelReports() {
    var reportMenuItem = {
      label: "Report",
      icon: "pi pi-fw pi-folder blue-text",
      items: [],
    };

    this.ds
      .execSP(
        "pGetModelReports",
        {
          CompanyID: this.currentUser.ID_Company,
          Oid_Model: this.model.Oid,
        },
        {
          isReturnObject: true,
        }
      )
      .then((obj) => {
        if (!obj.Reports) obj.Reports = [];

        if (obj.Reports.length > 0) {
          obj.Reports.forEach((modelReport) => {
            var menu: MenuItem = {
              label: modelReport.Name_ModelReport,
              command: () => {
                var filterFormValue: IFilterFormValue[] = [];
                var options = {
                  prevConfig: this.configOptions,
                  BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
                  backLink: this.router.url,
                };

                filterFormValue.push({
                  dataField: "ID",
                  filterCriteriaType: FilterCriteriaType.Equal,
                  propertyType: PropertyTypeEnum.Int,
                  value: this.CurrentObject.ID,
                });

                this.globalFx.navigateReport(
                  modelReport.Name_ModelReport,
                  modelReport.Oid_Report,
                  filterFormValue,
                  options
                );
              },
            };

            reportMenuItem.items.push(menu);
          });

          this.addMenuItem(reportMenuItem);
        } else {
          this.loadDefaultReports();
        }
      });
  }

  loadDefaultReports() {
    var reportMenuItem = {
      label: "Report",
      icon: "pi pi-fw pi-folder blue-text",
      items: [],
    };

    var acknowledgeReportMenu = {
      label: "Acknowledgement",
      command: async () => {
        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "ACKNOWLEDGEMENTREPORT",
          APP_REPORTVIEW.ACKNOWLEDGEMENTREPORT,
          filterFormValue,
          options
        );
      },
    };

    var admissionFormReportMenu = {
      label: "Admission Form",
      command: async () => {
        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "ADMISSIONREPORT",
          APP_REPORTVIEW.ADMISSIONREPORT,
          filterFormValue,
          options
        );
      },
    };

    var agreementForConfinementReportMenu = {
      label: "Agreement For Confinement",
      command: async () => {
        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "AgreementForConfinement",
          APP_REPORTVIEW.AGREEMENTFORCONFINEMENT,
          filterFormValue,
          options
        );
      },
    };

    var concentToOperationReportMenu = {
      label: "Consent To Operation",
      command: async () => {
        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "ConcentToOperation",
          APP_REPORTVIEW.CONCENTTOOPERATION,
          filterFormValue,
          options
        );
      },
    };

    var euthanasiaAuthorizationReportMenu = {
      label: "Euthanasia Authorization",
      command: async () => {
        var filterFormValue: IFilterFormValue[] = [];
        var options = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
          backLink: this.router.url,
        };

        filterFormValue.push({
          dataField: "ID",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: this.CurrentObject.ID,
        });

        this.globalFx.navigateReport(
          "EuthanasiaAuthorization",
          APP_REPORTVIEW.EUTHANASIAAUTHORIZATION,
          filterFormValue,
          options
        );
      },
    };

    reportMenuItem.items.push(acknowledgeReportMenu);
    reportMenuItem.items.push(admissionFormReportMenu);
    reportMenuItem.items.push(agreementForConfinementReportMenu);
    reportMenuItem.items.push(concentToOperationReportMenu);
    reportMenuItem.items.push(euthanasiaAuthorizationReportMenu);

    this.addMenuItem(reportMenuItem);
  }

  CurrentObject_onLoad() {
    this.loadMenuItem();
    this.loadGotoMenu();

    this.loadHistoryMenuItems();
    this.loadPatientBilling();

    if (this.patientsoaplist) {
      this.patientsoaplist.LoadByPatientID(this.CurrentObject.ID);
    }

    if (this.vaccinationlist) {
      this.vaccinationlist.LoadByPatientID(this.CurrentObject.ID);
    }

    if (this.wellnessdetaillist) {
      this.wellnessdetaillist.LoadByPatientID(this.CurrentObject.ID);
    }

    if (this.veterinaryhealthcertificatelist) {
      this.veterinaryhealthcertificatelist.LoadByPatientID(
        this.CurrentObject.ID
      );
    }

    this.ModelDisplayName =
      this.CurrentObject.ID > 0 ? this.CurrentObject.Name : "New Patient";
  }

  clientName_OnClick() {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
      ID_Patient: this.CurrentObject.ID,
      BackRouteLink: [`/Main`, "Patient", this.CurrentObject.ID],
    };

    routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
    this.globalFx.customNavigate(routeLink, config);
  }

  loadHistoryMenuItems() {
    this.historyExamMenuItems = [];
    if (this.CurrentObject.ID < 1) return;

    this.historyExamMenuItems.push({
      label: "Add",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        var currentObject = {
          DataSource: "",
          Date: null,
        };

        var title = `Add History`;

        var details = [
          {
            Name: "Date",
            Caption: "Date",
            ID_ControlType: ControlTypeEnum.DatePicker,
            ID_LabelLocation: 2,
            IsRequired: true,
          },
          {
            Name: "Comment",
            Caption: "Comment",
            ID_ControlType: ControlTypeEnum.TextArea,
            IsRequired: true,
          },
        ];

        var options = {
          PositiveButton_Label: "OK",
        };

        this.dvFormSvc
          .open(currentObject, title, details, options)
          .then((res) => {
            if (res == false) return false;

            var history = new Patient_History_DTO();

            history.ID = this.globalFx.getTempID();
            history.Date = res.Date;
            history.DateString = moment(res.Date).format("MM/DD/YYYY");
            history.Comment = res.Comment;
            (history.ID_Doctor = this.currentUser.ID_Employee),
              (history.Name_Doctor =
                this.currentUser.LastName + ", " + this.currentUser.FirstName),
              (history.ID_FilingStatus = FilingStatusEnum.Filed),
              (history.Name_FilingStatus = "Filed"),
              this.CurrentObject.Patient_History.push(history);

            this.appForm.setDirty(true);
          });
      },
    });
  }

  pBilling_onDblClick(bi: BillingInvoice) {
    var routeLink = [];
    var config = {
      prevConfig: this.configOptions,
    };

    routeLink = [`/Main`, "BillingInvoice", bi.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  historyGrid_onDblClick(history: Patient_History_DTO) {
    var currentObject = {
      Comment: history.Comment,
      Date: history.Date,
      Doctor: history.Name_Doctor,
    };

    var title = `Edit History`;

    var details = [
      {
        Name: "Date",
        Caption: "Date",
        ID_ControlType: ControlTypeEnum.DatePicker,
        ID_LabelLocation: 2,
        IsRequired: true,
      },
      {
        Name: "Doctor",
        Caption: "Doctor",
        ID_ControlType: ControlTypeEnum.TextBox,
        ID_LabelLocation: 2,
        IsReadOnly: true,
      },
      {
        Name: "Comment",
        Caption: "Comment",
        ID_ControlType: ControlTypeEnum.TextArea,
        IsRequired: true,
      },
    ];

    var options = {
      PositiveButton_Label: "OK",
    };

    this.dvFormSvc.open(currentObject, title, details, options).then((res) => {
      if (res == false) return false;

      history.Date = res.Date;
      history.DateString = moment(res.Date).format("MM/DD/YYYY");
      history.Comment = res.Comment;

      this.appForm.setDirty(true);
    });
  }

  historyGridBtnDelete_onClick(history: Patient_History_DTO) {
    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.Patient_History,
      "ID",
      history.ID + ""
    );

    this.CurrentObject.Patient_History.splice(index, 1);
    this.appForm.setDirty(true);
  }

  CustomDetailView_onBeforeSaving() {
    if (this.CurrentObject.IsDeceased != true)
      this.CurrentObject.DateDeceased = null;
  }

  generateFullName() {
    var patient = this.CurrentObject;

    patient.Name =
      (patient.LastName ?? "") +
      ", " +
      (patient.FirstName ?? "") +
      " " +
      (patient.MiddleName ?? "");
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var dateBirthDate = new Date(this.CurrentObject.DateBirth);
    var dateDeceased = new Date(this.CurrentObject.DateDeceased);
    var dateCurrent = new Date();

    if (!isNullOrUndefined(this.ID_Client_LookUpBox_LookUpBox)) {
      if (isNullOrUndefined(this.ID_Client_LookUpBox_LookUpBox.value))
        this.ID_Client_LookUpBox_LookUpBox.value = "";

      if (
        this.CurrentObject.ID_Client == null &&
        this.ID_Client_LookUpBox_LookUpBox.value != ""
      ) {
        validations.push({
          message: "Please check if the Client is registered.",
        });
      } else if (
        this.CurrentObject.ID_Client == 0 ||
        this.CurrentObject.ID_Client == null
      ) {
        validations.push({
          message: "Client is required.",
        });
      }
    }

    if (dateBirthDate > dateCurrent && this.CurrentObject.DateBirth != null) {
      validations.push({
        message: "Date Birth must not greater than current date.",
      });
    }

    // if (dateDeceased > dateCurrent && this.CurrentObject.IsDeceased == true && this.CurrentObject.DateDeceased != null) {

    //   validations.push({
    //     message: "Date Deceased must not greater than current date.",
    //   });
    // }

    // if (this.CurrentObject.DateBirth != null && this.CurrentObject.IsDeceased == true && this.CurrentObject.DateDeceased != null) {

    //   if (dateDeceased < dateBirthDate) {

    //     validations.push({
    //       message: "Date Deceased must not greater than Date Birth.",
    //     });
    //   }
    // }

    return Promise.resolve(validations);
  }
}

export class PatientDetailNames {
  static readonly Patient_Patient_SOAP?: string = "Patient_Patient_SOAP";
  static readonly Patient_History?: string = "Patient_History";
  static readonly Patient_History_DTO?: string = "Patient_History_DTO";
  static readonly Patient_SOAP_RegularConsoltation?: string =
    "Patient_SOAP_RegularConsoltation_DTO";
  static readonly Patient_Billing?: string = "Patient_Billing";
  static readonly Patient_SOAP?: string = "Patient_SOAP";
}
