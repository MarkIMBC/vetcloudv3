import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  Input,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  Patient_Wellness_DTO,
  FilingStatusEnum,
  Patient_Wellness_Detail_DTO,
  Patient_DTO,
  Patient_SOAP_DTO,
} from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";

@Component({
  selector: "patient-soaplist",
  templateUrl: "./patient-soaplist.component.html",
  styleUrls: ["./patient-soaplist.component.less"],
})
export class PatientSOAPListComponent implements OnInit {
  _ID_Pattent: number = 0;
  Records: Patient_SOAP_DTO[] = [];
  SelectedRecord: Patient_SOAP_DTO;

  @Input() DetailViewConfigOptions: any = {};
  @Input() CurrentObject: Patient_DTO = {};

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    this.loadMenuItems();
  }

  async LoadByPatientID(ID_Patient: number) {
    this._ID_Pattent = ID_Patient;

    this.loadMenuItems();

    var sql = `SELECT *
               FROM dbo.vPatient_SOAP_ListView
               WHERE 
                ID_Patient = ${this._ID_Pattent} AND
                ID_Company = ${this.currentUser.ID_Company}
               ORDER BY Date DESC
      `;

    sql = this.cs.encrypt(sql);
    this.Records = await this.ds.query<any>(sql);

    this.Records.forEach((record) => {

      record['HistoryHTML'] = record.History.replace(/(?:\r\n|\r|\n)/g, '<br>');
    });

    this.loadMenuItems();
  }

  loadMenuItems() {
    this.menuItems = [];

    if (this.CurrentObject.ID > 0 != true) return;

    var menuItems_New: MenuItem = {
      label: "Create",
      icon: "pi pi-fw pi-plus",
      command: () => {
        var routeLink = [];
        var config = {
          prevConfig: this.DetailViewConfigOptions,
          ID_Client: this.CurrentObject.ID_Client,
          ID_Patient: this.CurrentObject.ID,
          BackRouteLink: [`/Main`, "Patient", this._ID_Pattent],
        };

        routeLink = [`/Main`, "Patient_SOAP", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItems_View: MenuItem = {
      label: "View",
      icon: "pi pi-fw pi-search",
      command: () => {
        if (!this.SelectedRecord) return;

        var routeLink = [];
        var config = {
          prevConfig: this.DetailViewConfigOptions,
          ID_Client: this.CurrentObject.ID_Client,
          ID_Patient: this.CurrentObject.ID,
          BackRouteLink: [`/Main`, "Patient", this._ID_Pattent],
        };

        routeLink = [`/Main`, "Patient_SOAP", this.SelectedRecord.ID];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItems_Refresh: MenuItem = {
      label: "Refresh",
      icon: "pi pi-fw pi-refresh",
      command: () => {
        this.LoadByPatientID(this._ID_Pattent);
      },
    };

    this.menuItems.push(menuItems_New);
    this.menuItems.push(menuItems_View);
    this.menuItems.push(menuItems_Refresh);
  }

  touchtime = 0;

  _recordGrid_onDblClick() {
    if (this.touchtime === 0) {
      this.touchtime = new Date().getTime();
    } else {
      if (new Date().getTime() - this.touchtime < 400) {
        this.recordGrid_onDblClick();
        this.touchtime = 0;
      } else {
        this.touchtime = new Date().getTime();
      }
    }
  }

  recordGrid_onDblClick() {
    if (!this.SelectedRecord) return;

    var routeLink = [];
    var config = {
      prevConfig: this.DetailViewConfigOptions,
      ID_Patient: this._ID_Pattent,
      BackRouteLink: [`/Main`, "Patient", this._ID_Pattent],
    };

    routeLink = [`/Main`, "Patient_SOAP", this.SelectedRecord.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  recordGrid_onRowSelect() {
    this.loadMenuItems();
  }

  recordGrid_onRowUnselect() {
    this.loadMenuItems();
  }
}
