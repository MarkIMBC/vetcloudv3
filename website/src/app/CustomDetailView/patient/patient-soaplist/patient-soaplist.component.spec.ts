import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientSOAPListComponent } from './patient-soaplist.component';

describe('PatientSOAPListComponent', () => {
  let component: PatientSOAPListComponent;
  let fixture: ComponentFixture<PatientSOAPListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSOAPListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientSOAPListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
