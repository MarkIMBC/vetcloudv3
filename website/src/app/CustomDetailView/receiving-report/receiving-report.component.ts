import { Component, OnInit, ViewChild } from "@angular/core";
import {
  AppFormComponent,
  IFilterFormValue,
} from "src/utils/controls/appForm/appForm.component";
import { MenuItem } from "primeng/api/menuitem";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { DataService, IPagingOption } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import {
  MessageBoxService,
  IFormValidation,
} from "src/utils/controls/appModal/appModal.component";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { ToastrService } from "ngx-toastr";
import {
  Model,
  APP_MODEL,
  ReceivingReport,
  Item,
  FilingStatus,
  PurchaseOrder,
  APP_REPORTVIEW,
} from "src/bo/APP_MODELS";
import { SelectItem } from "primeng/api";
import {
  ReceivingReport_DTO,
  ReceivingReport_Detail_DTO,
  Patient_DTO,
  TaxSchemeEnum,
  FilingStatusEnum,
  PurchaseOrder_DTO,
  PurchaseOrder_Detail_DTO,
  PropertyTypeEnum,
} from "src/utils/controls/class_helper";
import {
  IAppLookupBoxOption,
  AppLookupBoxComponent,
} from "src/utils/controls/appLookupBox/appLookupBox.component";
import { currencyFormatter } from "currencyformatter.js";
import { DataTableOption } from "src/utils/controls/appDataTable/appDataTable.component";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import * as moment from "moment";
import {
  AppSelectBoxComponent,
  IAppSelectBoxOption,
} from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNull, isNullOrUndefined, isNumber } from "util";
import { Enumerable } from "linq-typescript";
import { FilterCriteriaType } from "src/utils/controls/appControlContainer/appControlContainer.component";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";

@Component({
  selector: "app-receiving-report",
  templateUrl: "./receiving-report.component.html",
  styleUrls: ["./receiving-report.component.less"],
})
export class ReceivingReportComponent extends BaseCustomDetailView {
  @ViewChild("purchaseOrderLookUpDialog")
  purchaseOrderLookUpDialogCmp: LookUpDialogBoxComponent;

  currentModelID: string = APP_MODEL.RECEIVINGREPORT;

  CurrentObject: ReceivingReport_DTO;
  PreviousObject: ReceivingReport_DTO;

  CurrentObjectDetailNames: string[] = [
    ReceivingReportDetailNames.ReceivingReport_Detail,
  ];

  @ViewChild("ID_Item_LookUpBox")
  ID_Item_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_TaxScheme_SelectBox")
  ID_TaxScheme_SelectBox: AppSelectBoxComponent;

  detailMenu: MenuItem[] = [];

  ID_PurchaseOrder_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
      Select ID, 
             Code Name, 
             Name_Supplier Supplier,
             Name_FilingStatus [Filing Status],
             ServingStatus_Name_FilingStatus [Serving Status]
      FROM vPurchaseOrder
      WHERE 
        ID_FilingStatus = ${FilingStatusEnum.Approved} AND 
        ID_Company = ${this.currentUser.ID_Company} AND
        ServingStatus_ID_FilingStatus IN (
                                            ${FilingStatusEnum.Pending},
                                            ${FilingStatusEnum.PartiallyServed}
                                         )
    `),
  };

  ID_TaxScheme_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tTaxScheme"),
  };

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  compute() {
    var subTotal = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;

    discountRate = this.globalFx.roundOffDecimal(discountRate);
    discountAmount = this.globalFx.roundOffDecimal(discountAmount);

    this.CurrentObject.ReceivingReport_Detail.forEach((detail) => {
      detail.Amount = detail.Quantity * detail.UnitPrice;
      detail.ID_ReceivingReport = this.CurrentObject.ID;

      subTotal += detail.Amount;
    });

    totalAmount = this.globalFx.roundOffDecimal(subTotal);

    if (this.CurrentObject.IsComputeDiscountRate) {
      discountAmount = totalAmount * (discountRate / 100);
    } else {
      if (discountAmount > 0) {
        discountRate = (discountAmount / totalAmount) * 100;
        discountRate = this.globalFx.roundOffDecimal(discountRate);
      } else {
        discountRate = 0;
      }
    }

    totalAmount = totalAmount - discountAmount;
    totalAmount = this.globalFx.roundOffDecimal(totalAmount);

    grossAmount = totalAmount;

    vatAmount = (grossAmount / 1.12) * 0.12;
    vatAmount = this.globalFx.roundOffDecimal(vatAmount);

    if (
      this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
      this.CurrentObject.ID_TaxScheme == 0
    ) {
      vatAmount = 0;
      netAmount = grossAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive) {
      netAmount = grossAmount + vatAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive) {
      netAmount = grossAmount - vatAmount;
    }

    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;

    this.appForm.setDirty(true);
  }

  browsePurchaseOrderItems(): Promise<PurchaseOrder_Detail_DTO[]> {
    return new Promise<Item[]>(async (res, rej) => {
      var strQuery_IDs_PurchaseOrder_Detail = Enumerable.fromSource(
        this.CurrentObject.ReceivingReport_Detail
      )
        .select((r) => r.ID_PurchaseOrder_Detail)
        .distinct()
        .toArray()
        .toString();

      if (strQuery_IDs_PurchaseOrder_Detail.length > 0)
        strQuery_IDs_PurchaseOrder_Detail = `AND ID NOT IN (${strQuery_IDs_PurchaseOrder_Detail})`;

      var selectedRecords =
        await this.purchaseOrderLookUpDialogCmp.open<PurchaseOrder_Detail_DTO>({
          sql: `
                    SELECT ID,
                           Code_Item,
                           Name_Item, 
                           Quantity,
                           RemainingQuantity,
                           ServingStatus_Name_FilingStatus
                    FROM vRemainingQuantityPurchaseOrderDetail
                    WHERE 
                      ID_PurchaseOrder = ${this.CurrentObject.ID_PurchaseOrder} AND
                      ID_FilingStatus = ${FilingStatusEnum.Approved} AND 
                      ISNULL(RemainingQuantity, 0) > 0
                      ${strQuery_IDs_PurchaseOrder_Detail}
                  `,
          height: "400px",
          filterColumns: [
            { name: "Name_Item", caption: "Item Name" },
            { name: "Code_Item", caption: 'Code' },
          ],
          columns: [
            {
              name: "Code_Item",
              width: "80px",
              align: "center",
            },
            {
              name: "Name_Item",
              caption: "Item Name",
              width: "100%",
            },
            {
              name: "Quantity",
              width: "100px",
              align: "center",
            },
            {
              name: "RemainingQuantity",
              caption: "Rem. Quantity",
              width: "100px",
              align: "center",
            },
            {
              name: "ServingStatus_Name_FilingStatus",
              caption: "Serve Status",
              width: "100px",
            },
          ],
        });
      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_PurchaseOrder_Detail = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
            SELECT  ID, 
                    ID_Item, 
                    Name_Item, 
                    RemainingQuantity,
                    UnitPrice
            FROM vPurchaseOrder_Detail
            WHERE 
              ID IN (${strIDs_PurchaseOrder_Detail})
        `);

        var items = await this.ds.query<PurchaseOrder_Detail_DTO>(queryString);

        res(items);
      }
    });
  }

  async ID_PurchaseOrder_LookUpBox_onSelectedItem(event: any) {
    var ID_PurchaseOrder = event.ID;
    var obj = await this.ds.loadObject<PurchaseOrder_DTO>(
      APP_MODEL.PURCHASEORDER,
      ID_PurchaseOrder,
      {}
    );

    this.CurrentObject.ID_Supplier = obj.ID_Supplier;
    this.CurrentObject.Name_Supplier = obj.Name_Supplier;
    this.CurrentObject.ID_TaxScheme = obj.ID_TaxScheme;
    this.CurrentObject.Name_TaxScheme = obj.Name_TaxScheme;

    this.CurrentObject.ReceivingReport_Detail = [];
  }

  async ID_TaxScheme_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_TaxScheme = event.Name;

    this.compute();
  }

  async CellItemIcon_onClick(detail: ReceivingReport_Detail_DTO) {
    if (this.appForm.isDisabled) return;

    if (isNullOrUndefined(this.CurrentObject.ID_PurchaseOrder)) {
      this.msgBox.warning(
        `Please select a Purchase Order.`,
        `Purchase Order is required`
      );
      return false;
    }

    var selectedItems = await this.browsePurchaseOrderItems();

    selectedItems.forEach((record) => {
      detail.ID_Item = record.ID_Item;
      detail.Name_Item = record.Name_Item;
    });
  }

  CellQuantity_onChanged(detail: ReceivingReport_Detail_DTO) {
    if (!isNumber(detail.Quantity)) detail.Quantity = 0;
    if (detail.Quantity < 0) detail.Quantity = 0;

    this.compute();
  }

  CellUnitPrice_onChanged(detail: ReceivingReport_Detail_DTO) {
    if (!isNumber(detail.UnitPrice)) detail.UnitPrice = 0;
    if (detail.UnitPrice < 0) detail.UnitPrice = 0;

    this.compute();
  }

  onRowDeleting(detail: ReceivingReport_Detail_DTO) {
    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.ReceivingReport_Detail,
      "ID",
      detail.ID + ""
    );

    this.CurrentObject.ReceivingReport_Detail.splice(index, 1);

    this.compute();
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (isNullOrUndefined(this.CurrentObject.ID_TaxScheme))
      this.CurrentObject.ID_TaxScheme = 0;

    if (this.CurrentObject.ReceivingReport_Detail.length == 0) {
      validations.push({
        message: "Add Detail atleast one.",
      });
    }

    this.CurrentObject.ReceivingReport_Detail.forEach((detail) => {
      if (detail.Quantity == 0) {
        validations.push({
          message: `Quantity is required for item ${detail.Name_Item}.`,
        });
      }
    });

    return Promise.resolve(validations);
  }

  CustomDetailView_onBeforeSaving() {
    this.compute();
  }

  CustomDetailView_onLoad() {
    this.breadCrumbs.push({
      label: "List",
    });

    if (!isNullOrUndefined(this.ID_TaxScheme_SelectBox))
      this.ID_TaxScheme_SelectBox.initOption();
  }

  CurrentObject_onLoad() {
    this.loadMenuItem();
    this.loadDetailMenuItem();
  }

  async doAddItem() {
    if (isNullOrUndefined(this.CurrentObject.ID_PurchaseOrder)) {
      this.msgBox.warning(
        `Please select a Purchase Order.`,
        `Purchase Order is required`
      );
      return false;
    }

    var selectedItems = await this.browsePurchaseOrderItems();

    selectedItems.forEach((record) => {
      this.CurrentObject.ReceivingReport_Detail.push({
        ID: this.globalFx.getTempID(),
        ID_Item: record.ID_Item,
        Name_Item: record.Name_Item,
        ID_PurchaseOrder_Detail: record.ID,
        Quantity: record.RemainingQuantity,
        UnitPrice: record.UnitPrice,
      });
    });

    this.compute();
  }

  async DiscountRate_onControlValue_Changed(event: any) {
    this.CurrentObject.IsComputeDiscountRate = true;
    this.compute();
  }

  async DiscountAmount_onControlValue_Changed(event: any) {
    this.CurrentObject.IsComputeDiscountRate = false;
    this.compute();
  }

  loadDetailMenuItem() {
    this.detailMenu = [];

    var menuItem_AddItem = {
      label: "Add Item",
      icon: "pi pi-fw pi-plus",
      command: async () => {
        await this.doAddItem();
      },
    };

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.detailMenu.push(menuItem_AddItem);
    }
  }

  loadMenuItem() {
    var menuItemApprove: MenuItem = {
      label: "Approve",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to approve ${this.CurrentObject.Code}?`,
          "Receiving Report",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.approve();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel ${this.CurrentObject.Code}?`,
          "Receiving Report",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(menuItemApprove);
      this.addMenuItem(menuItemCancel);
    } else if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
    ) {
      this.addMenuItem(menuItemCancel);
    }

    this.addMenuItem({
      label: "Report",
      icon: "pi pi-fw pi-folder blue-text",
      items: [
        {
          label: "RR Report",
          icon: "pi pi-file-o",
          command: async () => {
            var filterFormValue: IFilterFormValue[] = [];
            var options = {
              prevConfig: this.configOptions,
              backLink: this.router.url,
            };

            filterFormValue.push({
              dataField: "ID",
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID,
            });

            this.globalFx.navigateReport(
              "ReceivingReport",
              APP_REPORTVIEW.RECEIVINGREPORT,
              filterFormValue,
              options
            );
          },
        },
      ],
    });
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  async approve(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveReceivingReport",
        {
          IDs_ReceivingReport: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been approved successfully.`,
          `Approved ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Approve ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      //console.log(this.currentUser);

      var obj = await this.ds.execSP(
        "pCancelReceivingReport",
        {
          IDs_ReceivingReport: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Cancel ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }
}

export class ReceivingReportDetailNames {
  static readonly ReceivingReport_Detail?: string = "ReceivingReport_Detail";
}
