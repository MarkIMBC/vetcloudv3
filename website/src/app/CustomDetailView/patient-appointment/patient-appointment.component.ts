import { Employee, PatientAppointment } from "./../../../bo/APP_MODELS";
import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  AppLookupBoxComponent,
  IAppLookupBoxOption,
} from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "app-patient-appointment",
  templateUrl: "./patient-appointment.component.html",
  styleUrls: ["./patient-appointment.component.less"],
})
export class PatientAppointmentComponent
  extends BaseCustomDetailView
  implements OnInit {
  currentModelID: string = APP_MODEL.PATIENTAPPOINTMENT;

  @ViewChild("ID_Client_LookUpBox")
  ID_Client_LookUpBox: AppLookupBoxComponent;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  @ViewChild("AttendingPhysician_ID_Employee__LookUpBox")
  AttendingPhysician_ID_Employee__LookUpBox: AppLookupBoxComponent;



  CurrentObjectDetailNames: string[] = [];

  protected pGetRecordOptions(): any {
    var options = {};

    options["DateStart"] = this.configOptions["DateStart"];
    options["DateEnd"] = this.configOptions["DateEnd"];

    if (!isNullOrUndefined(this.configOptions["ID_Client"])) {
      options["ID_Client"] = this.configOptions["ID_Client"];
    }

    if (!isNullOrUndefined(this.configOptions["ID_Patient"])) {
      options["ID_Patient"] = this.configOptions["ID_Patient"];
    }

    return options;
  }

  ID_Client_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tClient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name 
        FROM tPatient
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND 
          IsActive = 1
    `),
  };

  ID_SOAPType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tSOAPType"),
  };

  AttendingPhysician_ID_Employee_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
    sourceKey: this.cs.encrypt(`
        Select ID, 
               Name,
               Name_Position Position
        FROM vAttendingVeterinarian
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} 
    `),
  };



  async AttendingPhysician_ID_Employee_LookUpBox_onSelectedItem(event: any) {
    var AttendingPhysician_ID = event.ID;
    var obj = await this.ds.loadObject<Employee>(
      APP_MODEL.EMPLOYEE,
      AttendingPhysician_ID,
      {}
    );
  }


  async ID_Client_LookUpBox_onSelectedItem(event: any) {
    var ID_Client = event.ID;

    this.CurrentObject.ID_Client = ID_Client;

    this.CurrentObject.ID_Patient = null;
    this.CurrentObject.Name_Patient = "";

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client} AND 
          IsActive = 1
    `);
  }

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {}

  async ID_SOAPType_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_SOAPType = event.Name;
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  protected loadInitMenu() {

    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }


  loadGotoMenu() {
    
    this.gotoMenuItems = [];

    var menuItem_ClientInfo: MenuItem = {
      label: "Client's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "PatientAppointment", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Client", this.CurrentObject.ID_Client];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_PatientInfo: MenuItem = {
      label: "Patient's Info",
      icon: "pi pi-fw pi-user-edit",
      command: async () => {
        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          BackRouteLink: [`/Main`, "PatientAppointment", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Patient", this.CurrentObject.ID_Patient];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_CreateSOAP: MenuItem = {
      label: "Create Medical Record",
      icon: "pi pi-fw pi-plus",
      command: async () => {

        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `Record changes is not yet saved.`
          );
          return;
        }

        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          ID_Patient: this.CurrentObject.ID_Patient,
          ID_Client: this.CurrentObject.ID_Client,
          ID_SOAPType: this.CurrentObject.ID_SOAPType,
          BackRouteLink: [`/Main`, "PatientAppointment", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Patient_SOAP", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    var menuItem_CreatePatientWellness: MenuItem = {
      label: "Create P. Wellness",
      icon: "pi pi-fw pi-plus",
      command: async () => {

        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `Record changes is not yet saved.`
          );
          return;
        }

        var routeLink = [];
        var config = {
          prevConfig: this.configOptions,
          ID_Patient: this.CurrentObject.ID_Patient,
          ID_Client: this.CurrentObject.ID_Client,
          ID_SOAPType: this.CurrentObject.ID_SOAPType,
          BackRouteLink: [`/Main`, "PatientAppointment", this.CurrentObject.ID],
        };

        routeLink = [`/Main`, "Patient_Wellness", -1];
        this.globalFx.customNavigate(routeLink, config);
      },
    };

    if (this.CurrentObject.ID > 0) {

      this.gotoMenuItems.push(menuItem_ClientInfo);
      this.gotoMenuItems.push(menuItem_PatientInfo);
      this.gotoMenuItems.push(menuItem_CreateSOAP);
      this.gotoMenuItems.push(menuItem_CreatePatientWellness);
    }
  }

  loadMenuItem() {

    if (this.CurrentObject.ID < 1) return;

    var menuItemDone: MenuItem = {
      label: "Done",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to done appointment?`,
          "Patient Appointment",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.doDone();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel appointment?`,
          "Patient Appointment",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.addMenuItem(menuItemDone);
      this.addMenuItem(menuItemCancel);
    }
  }

  async doDone(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDonePatientAppointment",
        {
          IDs_PatientAppointment: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been done successfully.`,
          `Done ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Done ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCanceledPatientAppointment",
        {
          IDs_PatientAppointment: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Canceled ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Canceled ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  CurrentObject_onLoad() {
    this.loadGotoMenu();

    this.loadMenuItem();

    this.ID_Patient_LookUpBox.option["sourceKey"] = this.cs.encrypt(`
        Select ID, 
              Name 
        FROM tPatient
        WHERE 
          ID_Client = ${this.CurrentObject.ID_Client}
    `);

    this.ID_Patient_LookUpBox.initOption();
  }
}
