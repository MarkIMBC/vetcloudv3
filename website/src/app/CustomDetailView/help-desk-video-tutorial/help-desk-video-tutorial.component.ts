import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_DTO,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Client, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import {
  IAppLookupBoxOption,
  AppLookupBoxComponent,
} from "src/utils/controls/appLookupBox/appLookupBox.component";

@Component({
  selector: 'app-help-desk-video-tutorial',
  templateUrl: './help-desk-video-tutorial.component.html',
  styleUrls: ['./help-desk-video-tutorial.component.less']
})
export class HelpDeskVideoTutorialComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.VETERINARYHEALTHCERTIFICATE;

  async CurrentObject_onLoad() {

  }

}
