import { GlobalfxService } from "src/utils/service/globalfx.service";
import { APP_MODEL } from "src/bo/APP_MODELS";
import {
  IMenuItem,
  PositionEnum,
  ScheduleColorEnum,
  UserGroupEnum,
  Dentist_DTO,
  Schedule_DTO,
  AppointmentEvent,
} from "../../utils/controls/class_helper";
import { SafeStyle } from "@angular/platform-browser";
import {
  APP_DETAILVIEW,
  Appointment,
  Employee,
  Schedule,
} from "../../bo/APP_MODELS";
import { DetailViewDialogService } from "../View/DetailViewDialog/DetailViewDialog.component";
import { DataService } from "../../utils/service/data.service";
import {
  CalendarOptions,
  EventApi,
  DateSelectArg,
  EventClickArg,
  EventInput,
  Calendar,
  FullCalendarComponent,
  DatesSetArg,
  EventChangeArg,
} from "@fullcalendar/angular";
import { Dropdown } from "primeng/dropdown";
import { ContextMenuModule } from "primeng/contextmenu";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CalendarInfo } from "src/bo/VC/Schedule/VC_Appointment";
import * as TS from "linq-typescript";
import * as moment from "moment";
import { MenuItem, SelectItem } from "primeng/api";
import { ButtonModule } from "primeng/button";
import {
  UserAuthenticationService,
  TokenSessionFields,
} from "../AppServices/UserAuthentication.service";
import { CrypterService } from "src/utils/service/Crypter.service";
import { LoaderService } from "../AppServices/LoaderInterceptor";
@Component({
  selector: "app-schedule",
  templateUrl: "./schedule.component.html",
  styleUrls: ["./schedule.component.less"],
})
export class ScheduleComponent implements OnInit {
  currentModelID: string = APP_MODEL.SCHEDULE;

  @ViewChild("fullCalendar")
  fullCalendar: FullCalendarComponent;

  @ViewChild("dentistListDropdown")
  dropdown: Dropdown;

  @ViewChild("btnRefresh")
  btnRefresh: ButtonModule;

  isShowLookup: boolean = true;

  dentistList: Dentist_DTO[];
  selectedDentist: Dentist_DTO = new Dentist_DTO();
  appointmentEvents: AppointmentEvent[];

  items: MenuItem[] = [
    {
      label: "File",
      items: [
        {
          label: "New",
          icon: "pi pi-fw pi-plus",
          items: [{ label: "Project" }, { label: "Other" }],
        },
        { label: "Open" },
        { label: "Quit" },
      ],
    },
    {
      label: "Edit",
      icon: "pi pi-fw pi-pencil",
      items: [
        { label: "Delete", icon: "pi pi-fw pi-trash" },
        { label: "Refresh", icon: "pi pi-fw pi-refresh" },
      ],
    },
  ];

  currentUser: TokenSessionFields;

  isLoading: boolean = false;

  constructor(
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
    private dvModalSvc: DetailViewDialogService,
    private msgBox: MessageBoxService,
    protected globalFx: GlobalfxService,
    private cs: CrypterService,
    protected loaderService: LoaderService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();

    this.loaderService.isLoading.subscribe((r) => {
      this.isLoading = r;
    })
  }

  dateSetArgs: DatesSetArg = null;
  currentEvents: EventApi[] = [];

  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: "prev,next today",
      center: "title",
      right: "dayGridMonth",
    },
    initialView: "dayGridMonth",
    initialEvents: [],
    weekends: true,
    editable: false,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    datesSet: (p: DatesSetArg) => {
      this.dateSetArgs = p;
      this.refreshEvents();
    },
  };

  async handleDateSelect(selectInfo: DateSelectArg) {
    var dateStart = moment(selectInfo.start).format("YYYY-MM-DD");
    var dateEnd = moment(selectInfo.end).format("YYYY-MM-DD");

    var dateStartNew = dateStart + " 00:00:00";
    var dateEndNew = dateStart + " 23:59:59";

    var routeLink = [];
    var config = {
      BackRouteLink: [`/Main`, "Schedule"],
      DateStart: dateStartNew,
      DateEnd: dateEndNew,
    };

    routeLink = [`/Main`, "PatientAppointment", -1];
    this.globalFx.customNavigate(routeLink, config);

    this.refreshEvents();
  }

  async handleEventClick(clickInfo: EventClickArg) {
    var eventID = clickInfo.event.id;

    this.goto(eventID);
  }

  goto(eventID) {
    var routeLink = [];
    var config = {};
    var Oid_Model = "";
    var ID_CurrentObject = 0;
    var ID_Client = 0;
    var splitted = eventID.split("|");

    Oid_Model = splitted[0];
    ID_CurrentObject = splitted[1];
    ID_Client = splitted[2];

    config = {
      BackRouteLink: [`/Main`, "Schedule"],
      ID_Client: ID_Client,
    };

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        routeLink = [`/Main`, "Patient_SOAP", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = [`/Main`, "PatientAppointment", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = [`/Main`, "Patient_Wellness", ID_CurrentObject];
        break;
    }

    this.globalFx.customNavigate(routeLink, config);
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  dentistListDropdown_onChange(event: EventChangeArg) {
    this.refreshEvents();
  }

  btnRefresh_onClick(event: EventClickArg) {
    this.refreshEvents();
  }

  async refreshEvents(): Promise<void> {
    const calendarApi = this.fullCalendar.getApi();

    calendarApi.removeAllEvents();

    var appointmentEvents = await this.getAppointmentEvent();

    if (appointmentEvents) {
      appointmentEvents.forEach((a) => {

        var title = ""

        //if (a.FormattedDateStartTime.length > 0) title += a.FormattedDateStartTime + ' - ' + a.FormattedDateEndTime + ' '
        title += a.Paticular + " - " + a.Description;

        var event: EventInput = {
          editable: false,
          start: a.DateStart,
          end: a.DateEnd,
          id: a.UniqueID + "",
          title: title,
        };

        event.className = this.getClassByModel(a.Oid_Model);

        calendarApi.addEvent(event);
      });
    }

    return Promise.resolve();
  }

  getColorByModel(Oid_Model) {
    var color;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        color = ScheduleColorEnum.PatientSOAP;
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        color = ScheduleColorEnum.PatientAppointment;
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        color = ScheduleColorEnum.PatientWellness;
        break;

      default:
        color = ScheduleColorEnum.PatientSOAP;
        break;
    }
    return color;
  }

  getClassByModel(Oid_Model) {
    var _class;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        _class = "fc-daygrid-event-patientsoap";
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        _class = "fc-daygrid-event-patientappointment";
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        _class = "fc-daygrid-event-patientwellness";
        break;

      default:
        _class = "fc-daygrid-event-patientsoap";
        break;
    }
    return _class;
  }

  async getAppointmentEvent(): Promise<AppointmentEvent[]> {
    const calendarApi = this.fullCalendar.getApi();

    var currentDate = calendarApi.getDate();
    const startOfMonth = moment(currentDate)
      .clone()
      .startOf("month")
      .format("YYYY-MM-DD 00:00:00");
    const endOfMonth = moment(currentDate)
      .clone()
      .endOf("month")
      .format("YYYY-MM-DD 23:59:59");

    var sql = this.cs.encrypt(`
      SELECT * 
      FROM vAppointmentEvent 
      WHERE 
        ID_Company = ${this.currentUser.ID_Company} AND
        DateStart BETWEEN '${startOfMonth}' AND '${endOfMonth}'
    `);

    var appointmentEvents = await this.ds.query<AppointmentEvent>(sql);

    this.appointmentEvents = appointmentEvents;

    return Promise.resolve(appointmentEvents);
  }

  ngOnInit(): void { }
}
