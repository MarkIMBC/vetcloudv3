import { APP_MODEL } from 'src/bo/APP_MODELS';
import { PositionEnum, AppointmentSchedule_DTO, FilingStatusEnum } from 'src/utils/controls/class_helper';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { resolve } from 'dns';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { AppSelectBoxComponent, IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { MenuItem } from 'primeng/api';
import { isNullOrUndefined } from 'util';
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { BaseCustomDetailDialog } from 'src/app/CustomDetailDialog/BaseCustomDetailDialog';
import { IAppLookupBoxOption } from 'src/utils/controls/appLookupBox/appLookupBox.component';

@Component({
  selector: 'schedule-dialog',
  templateUrl: './schedule-dialog.component.html',
  styleUrls: ['./schedule-dialog.component.less']
})
export class ScheduleDialogComponent extends BaseCustomDetailDialog {

  @ViewChild('ID_ScheduleType_SelectBox')
  ID_ScheduleType_SelectBox: AppSelectBoxComponent;

  @ViewChild('ID_ServiceType_SelectBox')
  ID_ServiceType_SelectBox: AppSelectBoxComponent;
  
  currentModelID: string = APP_MODEL.SCHEDULE;
  CurrentObject: any = {};
  PreviousObject: any = {};

  constructor(protected modalService: NgbModal, protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService) {

    super(modalService, ds,  globalFx, msgBox, userAuthSvc, cs);
  }
  

  ID_Doctor_LookupBoxOption: IAppLookupBoxOption = {

    apiUrl: 'Model/GetList/null',
    sourceKey: this.cs.encrypt(`
      SELECT ID, Name FROM vEmployee WHERE ID_Position = ${PositionEnum.Dentist}
    `)
  };

  async ID_Doctor_LookUpBox_onSelectedItem(event: any) {

    this.CurrentObject.ID_Doctor = event.ID;
  }

  ID_ScheduleType_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tScheduleType")
  };

  ID_ServiceType_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tServiceType")
  };

  protected async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if(isNullOrUndefined(this.CurrentObject.ID_Doctor)) this.CurrentObject.ID_Doctor = null;
    if(isNullOrUndefined(this.CurrentObject.ID_ScheduleType)) this.CurrentObject.ID_ScheduleType = 0;
    if(isNullOrUndefined(this.CurrentObject.ID_ServiceType)) this.CurrentObject.ID_ServiceType = 0;
    if(isNullOrUndefined(this.CurrentObject.VacantCount)) this.CurrentObject.VacantCount = 0;

    if(this.CurrentObject.ID_Doctor == null) validations.push({
      message: 'Doctor is required.'
    })

    if(this.CurrentObject.ID_ScheduleType == 0) validations.push({
      message: 'Schedule Typpe is required.'
    })
    
    if(this.CurrentObject.ID_ServiceType == 0) validations.push({
      message: 'Kind of Service is required.'
    })

    if(this.CurrentObject.VacantCount == 0) validations.push({
      message: 'No. of Accomodation is required.'
    })

    return Promise.resolve(validations);
  };


  async open<T>(option): Promise<T[]> {

    var _option = {

      ID: option.ID,
      ID_Doctor: option.ID_Doctor,
      DateStart: option.DateStart,
      DateEnd: option.DateEnd,
    }

    await this.loadRecord(_option);

    return await this._open()
  }

  OnLoad(){

    if (!isNullOrUndefined(this.ID_ScheduleType_SelectBox)) this.ID_ScheduleType_SelectBox.initOption();
    if (!isNullOrUndefined(this.ID_ServiceType_SelectBox)) this.ID_ServiceType_SelectBox.initOption();
  }

}