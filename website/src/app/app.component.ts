import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { UserAuthenticationService } from './AppServices/UserAuthentication.service';
import { Component, OnInit } from "@angular/core";
import { Router, NavigationStart } from '@angular/router';
import { LoaderService } from './AppServices/LoaderInterceptor';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.less"],
})
export class AppComponent implements OnInit {

  
  isLoading: boolean = false;
  isInitialized: boolean = false;
  
  constructor(
      private router: Router, 
      loaderService: LoaderService, 
      private userAuthSvc: UserAuthenticationService, 
      private msgBox: MessageBoxService
    ) {
    loaderService.isLoading.subscribe((r) => {
      this.isLoading = r;
    })
  }

  ngOnInit(): void {


    var currentHref = window.location.href;
    currentHref = currentHref.slice(0, -1);

    if(currentHref == window.location.origin){

      document.location.href = '/Main';
      return;
    }
    
    this.router.events.subscribe(async (event: any) => {
     
      if ( event instanceof NavigationStart ) {

        var evt:NavigationStart = event;

        if ( evt.url.includes("Login") !== true ) {

          if (this.isInitialized != true) {

            var IUserSession = await this.userAuthSvc.CheckSession();

            if (IUserSession.IsValid == false) {
          
              //await alert("Session Expired. Please Login..")
              document.location.href = '/Login?url=' + window.location.href ;
            } else {
              
              this.isInitialized = true;
            }
          }

        } else {

          var IUserSession = await this.userAuthSvc.CheckSession();
          if ( IUserSession.IsValid == true ) {

            document.location.href = '/Main';
          } 
        }
      }
    });
  }
}