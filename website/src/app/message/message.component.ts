import { Component, OnInit } from '@angular/core';
import { SignalRService } from 'src/utils/service/signal-r.service';
import { HttpClient } from '@angular/common/http';
import { Message, APP_MODEL, APP_DETAILVIEW, User } from 'src/bo/APP_MODELS';
import { IMenuItem,MsgThread, Message_DTO, MessagRecipient, MessageThread } from 'src/utils/controls/class_helper';
import {FormsModule} from '@angular/forms'
import { DatePipe } from '@angular/common';
import { PanelMenuModule } from 'primeng/panelmenu';
import { MenuItem } from 'primeng/api';
import { UserAuthenticationService, TokenSessionFields } from '../AppServices/UserAuthentication.service';
import { DataService } from 'src/utils/service/data.service';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { isNullOrUndefined } from 'util';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.less'],
  providers: [DatePipe]
})

export class MessageComponent implements OnInit {

  items: MenuItem[];

  sender: string;

  recipients: MessagRecipient[];
  selectedRecipient: MessagRecipient = new MessagRecipient();

  msgThreads: MessageThread[] = [];

  _message: string = "";

  currentUser: TokenSessionFields;

  constructor(
    public signalRService: SignalRService, 
    private userAuthSvc: UserAuthenticationService,
    private ds: DataService, 
    private cs: CrypterService,
    public datePipe: DatePipe,
    private globalFx: GlobalfxService, 
    private http: HttpClient) { 

      this.currentUser = this.userAuthSvc.getDecodedToken();
    }

  menuToolItems: IMenuItem[] = [
    {
      label: "Create Message",
      icon: "mdi mdi-plus",
      command: () => {

      }
    }
  ]

  playAudio(){
    // let audio = new Audio();
    // audio.src = "../../../assets/rin3gtone.mp3";
    // audio.load();
    // audio.play();
  };

  async ngOnInit() {

    await this.signalRService.startConnection(this.currentUser.ID_User + "");
    await this.loadRecipient();

    this.signalRService.hubConnection.on('messageReceived', async (sender, msgThread: MessageThread) => {
        
      
        var Recipient_ID_User = msgThread.Recipient_ID_User;
        var recipientUser =  await this.ds.loadObject(APP_MODEL.USER, Recipient_ID_User + "") as User;

        this.modifyRecipient(
          msgThread.Recipient_ID_User, 
          {
            Sender_ID_User:  msgThread.Sender_ID_User,
            Recipient_ID_User : msgThread.Recipient_ID_User,
            Recipient_Name_User: recipientUser.Name,
            IsRead: msgThread.IsRead,
            LastDateSent: msgThread.DateSent,
          }
        );

        if(isNullOrUndefined(this.selectedRecipient.Recipient_ID_User)) return;
      
        if(Recipient_ID_User == this.selectedRecipient.Recipient_ID_User){

          await this.readMessageThread({

            Recipient_ID_User: Recipient_ID_User,
            IsAllowSetMsgThread: false
          });
          this.selectedRecipient.MessageThreads.push(msgThread);
        }

        this.playAudio();
    });

    this.signalRService.hubConnection.onclose(() => { 

      setTimeout(async function(){

        //console.log("reconnecting signal r hub.....");
        await this.signalRService.startConnection(this.currentUser.ID_User + "");
      },3000); 
    }); 

    this.signalRService.hubConnection.on('OnDisconnectedAsync', async (sender, msgThread) => {

      //console.log("reconnect");
      await this.signalRService.startConnection(this.currentUser.ID_User + "");
    });
  }

  loadRecipient = async () => {

    var sql = ` SELECT DISTINCT
                    Sender_ID_User,
                    Recipient_ID_User,
                    Recipient_Name_User,
                    IsRead,
                    LastDateSent
                FROM _vLastestMessageRecipient
                WHERE Sender_ID_User = ${this.currentUser.ID_User}
            `;

    sql = this.cs.encrypt(sql);         

    this.recipients = await this.ds.query<MessagRecipient>(sql);
  }

  modifyRecipient = async (Recipient_ID_User: number, newRecipient: MessagRecipient) => {

    var sql = ` SELECT DISTINCT
                Sender_ID_User,
                Recipient_ID_User,
                Recipient_Name_User,
                IsRead,
                LastDateSent
            FROM _vLastestMessageRecipient
            WHERE Sender_ID_User = ${this.currentUser.ID_User}
            `;

    sql = this.cs.encrypt(sql);         

    var result = await this.ds.query<MessagRecipient>(sql);

    if(result.length == 0) return;

    var record = result[0];

    var recipient = this.getRecipient(record.Recipient_ID_User);

    if(isNullOrUndefined(recipient)){

      this.recipients.push(newRecipient);
    }else{

      var index = this.globalFx.findIndexByKeyValue(this.recipients, 'Recipient_ID_User', Recipient_ID_User);
      this.recipients[index] = recipient;
    }
  }

  getRecipient = (Recipient_ID_User) => {

    var index = this.globalFx.findIndexByKeyValue(this.recipients, 'Recipient_ID_User', Recipient_ID_User);
    
    return this.recipients[index];
  }

  readMessageThread = async (options) => {

    var obj = await this.ds.execSP("pReadMessageThread", {
      Sender_ID_User : this.currentUser.ID_User,
      Recipient_ID_User : options.Recipient_ID_User
    },{
      isReturnObject: true
    })

    if(obj.Success != true) return;
    if(isNullOrUndefined(obj.MessageThreads)) obj.MessageThreads = [];

    var index = this.globalFx.findIndexByKeyValue(this.recipients,"Recipient_ID_User", options.Recipient_ID_User)

    if(index < 0) return;

    this.selectedRecipient = this.recipients[index];

    if(isNullOrUndefined(options.IsAllowSetMsgThread)) options.IsAllowSetMsgThread = false;
    if(options.IsAllowSetMsgThread != true) return;

    this.selectedRecipient.IsRead = true;
    this.selectedRecipient.MessageThreads = obj.MessageThreads;
  }

  loadMessageThread = async () => {

    var sql = `SELECT ID,
                      Recipient_ID_User,
                      Sender_ID_User,
                      Message,
                      DateSent,
                      DateRead,
                      IsRead 
              FROM tMessageThread
              WHERE 
                  Sender_ID_User = ${this.currentUser.ID_User} AND
                  Recipient_ID_User = ${this.selectedRecipient.Recipient_ID_User}
            `;

    sql = this.cs.encrypt(sql);         

    this.selectedRecipient.MessageThreads = await this.ds.query<MessageThread>(sql);
  }

  Recipient_onClick = async (recipient: MessagRecipient) => {

    var Recipient_ID_User = recipient.Recipient_ID_User;
    var options = {

      Recipient_ID_User: Recipient_ID_User,
      IsAllowSetMsgThread: true
    };

    await this.readMessageThread(options);
  }

  btnSend_onClick = async () => {

    await this.sendMessage();
  }

  btnSend_onKeyDown = async (event:any) => {

    if(event.key != "Enter") return;

    await this.sendMessage();
  }

  async sendMessage(){

    if(isNullOrUndefined(this.selectedRecipient.Recipient_ID_User)) return;

    var msg = this._message;

    // Save to Message Thread
    var obj = await this.ds.execSP("pSentMessageThread", {
      Sender_ID_User : this.currentUser.ID_User,
      Recipient_ID_User : this.selectedRecipient.Recipient_ID_User,
      Message: msg,
    },{
      isReturnObject: true,
      isTransaction: true,
    })

    if(isNullOrUndefined(obj.success)) return;
    if(obj.succes != true) return;

    this.selectedRecipient.MessageThreads.push(obj); // Push on chat box

    await this.signalRService.sentMessage( this.currentUser.ID_User + "",  this.selectedRecipient.Recipient_ID_User + "", msg); // Sent message thru signal r

    this._message = "";
  }
}
