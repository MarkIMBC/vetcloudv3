import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import { AppointmentSchedule, APP_DETAILVIEW, DetailView_Detail } from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { isNullOrUndefined } from "util";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import * as moment from "moment";

@Component({
  selector: 'app-patient-vaccination-list',
  templateUrl: './patient-vaccination-list.component.html',
  styleUrls: ['./patient-vaccination-list.component.less']
})
export class PatientVaccinationListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PatientVaccinationListComponent";

  ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  ID,
              Name
      FROM tFilingStatus
      WHERE ID IN (
        ${FilingStatusEnum.Filed},
        ${FilingStatusEnum.Approved},
        ${FilingStatusEnum.Cancelled}
      )
      /*encryptsqlend*/
    `),
  };

  initFilterFields() {
    var details: DetailView_Detail[] = [
      {
        ID_PropertyType: PropertyTypeEnum.Date,
        ID_ControlType: ControlTypeEnum.DatePicker,
        Name: "Date",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {

    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vPatient_Vaccination_ListView
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, null, record);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?, record?: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientVaccinationList"],
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Patient_Vaccination", id];

    if (isNewTab) {

      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, '_blank');
    } else {

      this.globalFx.customNavigate(routeLink, config);
    }
  }

  initializeMenuItems() {

    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    if (!isNullOrUndefined(this.CurrentObject["ID_FilingStatus"])) {

      if (filterString.length > 0) filterString = filterString + " AND ";
      filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]}`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Date"])) {

      var _dateStart = this.CurrentObject["Date"][0];
      var _dateEnd = this.CurrentObject["Date"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {

        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format('YYYY-MM-DD')}'`;
      } else {

        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    } 

    return filterString;
  }

}
