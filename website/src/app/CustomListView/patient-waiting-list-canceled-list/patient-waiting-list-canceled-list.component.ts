import {
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import {
  AppointmentSchedule,
  APP_DETAILVIEW,
  APP_MODEL,
} from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { ChangeWaitingStatusPatientWaitingListDialogComponent } from "src/app/CustomListView/patient-waiting-list/change-waiting-status-patient-waiting-list-dialog/change-waiting-status-patient-waiting-list-dialog.component";
import * as moment from "moment";

@Component({
  selector: "app-patient-waiting-list-canceled-list",
  templateUrl: "./patient-waiting-list-canceled-list.component.html",
  styleUrls: ["./patient-waiting-list-canceled-list.component.less"],
})
export class PatientWaitingListCanceledListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PatientWaitingListCanceledListComponent";

  @ViewChild("changewaitingstatuspatientwaitinglistdialog")
  changewaitingstatuspatientwaitinglistdialog: ChangeWaitingStatusPatientWaitingListDialogComponent;

  OrderByString: string =
    "DateStart DESC, ReferenceCode ASC, Name_Client ASC, Name_Patient ASC";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vPatientWaitingCanceledList_ListView
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  dataSource_InitLoad(obj: any) {
    obj.Records.forEach((record: any) => {});
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    if (!isNullOrUndefined(this.CurrentObject["DateStart"])) {
      var _dateStart = this.CurrentObject["DateStart"][0];
      var _dateEnd = this.CurrentObject["DateStart"][1];

      if (filterString.length > 0) filterString += " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, DateStart) = '${moment(
          _dateStart
        ).format("YYYY-MM-DD")}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateStart) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    } else {
      if (filterString.length > 0) filterString += " AND ";

      filterString += `
          CONVERT(DATE, DateStart) BETWEEN
            '${moment(_dateStart).format("YYYY-MM-DD")}' AND
            '${moment(_dateEnd).format("YYYY-MM-DD")}'
      `;
    }

    return filterString;
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  colWaitingStatus_onClick(record: any) {
    this.changewaitingstatuspatientwaitinglistdialog
      .open(record.ID)
      .then(() => {
        this.loadRecords();
      });
  }

  colAction_CreateSOAP_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
      ID_Patient: record.ID_Patient,
    };

    routeLink = [`/Main`, "Patient_SOAP", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  colAction_CreateConfinement_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
      ID_Client: record.ID_Client,
      ID_Patient: record.ID_Patient,
    };

    routeLink = [`/Main`, "Confinement", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  Reference_OnRowClick(reference: any) {
    var routeLink = [];
    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
    };

    var Oid_Model = reference.Oid_Model;
    var ID_CurrentObject = reference.ID_CurrentObject;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        routeLink = [`/Main`, "Patient_SOAP", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = [`/Main`, "PatientAppointment", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = [`/Main`, "Patient_Wellness", ID_CurrentObject];
        break;
    }

    this.globalFx.customNavigate(routeLink, config);
  }
}
