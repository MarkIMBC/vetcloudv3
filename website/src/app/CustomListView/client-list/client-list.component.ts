import { PropertyTypeEnum } from "./../../../utils/controls/class_helper";
import { Component, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { isNullOrUndefined } from "util";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { MenuItem } from "primeng/api";

@Component({
  selector: "app-client-list",
  templateUrl: "./client-list.component.html",
  styleUrls: ["./client-list.component.less"],
})

export class ClientListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "ClientListComponent";

  AccordionFieldTabs: any = {

    IsActive: true,
    Name: true
  }

  async loadRecords() {

    this.initFilterFields();

    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Code,
                    Name,
                    ContactNumbers,
                    DateLastVisited 
            FROM dbo.vClient_ListView
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);

  }

  IsActive_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  t.ID, 
              t.Name
      FROM    ( 
        SELECT 0 ID, 'Yes' Name UNION 
        SELECT 1 ID, 'No' Name  
      ) t 
      /*encryptsqlend*/
    `),
  };

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "ContactNumbers",
      },
    ];

    this.appForm.details = details;
  }

  getIsIncludeFilterList(fieldName): boolean {

    return this.appForm.IncludeFilterList.includes(fieldName);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    if(this.CurrentObject["IsActive"] != undefined && this.CurrentObject["IsActive"] != null){

      if (filterString.length > 0) filterString = filterString + " AND ";

      filterString += `ISNULL(IsActive, 0) = ${this.CurrentObject["IsActive"]} `;
    }else{

      if (filterString.length > 0) filterString = filterString + " AND ";

      filterString += `ISNULL(IsActive, 0) = 1 `;
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ClientList"],
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Client", id];

    if (isNewTab) {

      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, '_blank');
    } else {

      this.globalFx.customNavigate(routeLink, config);
    }
  }

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {

      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Inactive Client",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetActiveInactiveClient",
        {
          record: [{
            ID_Client: this.selectedRecord.ID,
            IsActive: false
          }],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedRecord.Name} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }
  
  colAction_CreateAppointment_onClick(record: any) {

    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ClientList"],
      ID_Client: record.ID
    };

    routeLink = [`/Main`, "PatientAppointment", -1];
    this.globalFx.customNavigate(routeLink, config);
  }
  
  colAction_CreateBillingInvoice_onClick(record: any) {

    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ClientList"],
      ID_Client: record.ID
    };

    routeLink = [`/Main`, "BillingInvoice", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  colAction_AddPet_onClick(record: any) {

    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ClientList"],
      ID_Client: record.ID
    };

    routeLink = [`/Main`, "Patient", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SetAsInactive);
  }

}
