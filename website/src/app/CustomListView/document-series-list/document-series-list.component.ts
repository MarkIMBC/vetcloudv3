import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_DETAILVIEW, Employee } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';

@Component({  
  selector: 'app-document-series-list',
  templateUrl: './document-series-list.component.html',
  styleUrls: ['./document-series-list.component.less']
})
export class DocumentSeriesListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "DocumentSeriesListComponent";

  async loadRecords() {

    var queryString = this.cs.encrypt(`
      SELECT  ID,
              Name,
              Prefix,
              Counter,
              IsAppendCurrentDate,
              DigitCount
      FROM dbo.vDocumentSeries
      WHERE
          ID_Company = ${this.currentUser.ID_Company} AND
          1 = 1
    `);

    //TODO: Remove all Order By sa lahat ng query


    this.dataSource = await this.ds.query<any>(queryString);
  }
  
  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "DocumentSeriesList"],
    };
    
    routeLink = [`/Main`, 'DocumentSeries', id];
    this.globalFx.customNavigate(routeLink, config);
  }

}
