import { AppointmentSchedule, APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { Patient_DTO, PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';

@Component({
  selector: 'app-paymenttransactionlist',
  templateUrl: './paymenttransactionlist.component.html',
  styleUrls: ['./paymenttransactionlist.component.less']
})
export class PaymenttransactionlistComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PaymenttransactionlistComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.DateTime,
        Name: "Date",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_FilingStatus",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Date,
                    Code,
                    DateString,
                    Name_Client,
                    Name_Patient,
                    Name_PaymentMethod,
                    PayableAmount,
                    PaymentAmount,
                    ChangeAmount,
                    Name_FilingStatus
            FROM dbo.vPaymentTransaction
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  
  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    console.log(this.CurrentObject);
    
    if (!isNullOrUndefined(this.CurrentObject["Date"])) {
      var _dateStart = this.CurrentObject["Date"][0];
      var _dateEnd = this.CurrentObject["Date"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          "YYYY-MM-DD"
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    }

    return filterString;
  }
  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "PaymentTransactions"],
    };
    
    routeLink = [`/Main`, 'PaymentTransaction', id];
    this.globalFx.customNavigate(routeLink, config);
  }

}
