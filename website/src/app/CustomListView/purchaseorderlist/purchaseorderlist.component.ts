import { ElementRef } from '@angular/core';
import { AppointmentSchedule, APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { Patient_DTO, PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-purchaseorderlist',
  templateUrl: './purchaseorderlist.component.html',
  styleUrls: ['./purchaseorderlist.component.less']
})
export class PurchaseorderlistComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PurchaseorderlistComponent";

  initFilterFields() {
    var details = [

      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
    ];

    if (!isNullOrUndefined(this.appForm)) this.appForm.details = details;
  }

  async loadRecords() {

    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT  ID,
                Date,
                Code,
                GrossAmount,
                VatAmount,
                NetAmount,
                DateCreated,
                CreatedBy_Name_User,
                DateModified,
                LastModifiedBy_Name_User,
                DateApproved,
                ApprovedBy_Name_User,
                CanceledBy_Name_User,
                DateCanceled,
                ID_FilingStatus,
                Name_FilingStatus,
                ServingStatus_Name_FilingStatus,
                DateString,
                DateCreatedString,
                DateModifiedString,
                DateApprovedString,
                DateCanceledString
          FROM vPurchaseOrder
          WHERE
            ID_Company = ${this.currentUser.ID_Company}
            ${filterString} AND
            1 = 1
      `;

    await this.getRecordPaging(sql);
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id) {

    var routeLink = [];
    var config = {

      BackRouteLink: [`/Main`, "PurchaseOrders"],
    };

    routeLink = [`/Main`, 'PurchaseOrder', id];
    this.globalFx.customNavigate(routeLink, config);
  }

}
