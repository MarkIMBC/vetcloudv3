import { Component, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";

@Component({
  selector: "app-supplier-list",
  templateUrl: "./supplier-list.component.html",
  styleUrls: ["./supplier-list.component.less"],
})
export class SupplierListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "SupplierListComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Delete Item",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetActiveInactiveSupplier",
        {
          record: [
            {
              ID_Supplier: this.selectedRecord.ID,
              IsActive: false,
            },
          ],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedRecord.Name} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT ID,
                   Name
            FROM dbo.vSupplier
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} AND 
              IsActive = 1
              ${filterString} 
              AND 1 = 1
          `;

    await this.getRecordPaging(sql);
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id) {
    var routeLink = [];
    var config = {
      BackRouteLink: [`/Main`, "SupplierList"],
    };

    routeLink = [`/Main`, "Supplier", id];
    this.globalFx.customNavigate(routeLink, config);
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SetAsInactive);
  }
}
