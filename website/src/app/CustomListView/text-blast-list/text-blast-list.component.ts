import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import {
  ItemTypeEnum,
  Patient_DTO,
  PropertyTypeEnum,
} from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";

@Component({
  selector: 'app-text-blast-list',
  templateUrl: './text-blast-list.component.html',
  styleUrls: ['./text-blast-list.component.less']
})
export class TextBlastListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "TextBlastListComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }
 
  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
          SELECT ID
                ,Code
                ,Date
                ,Message
                ,Name_FilingStatus
          FROM vTextBlast
          WHERE
            ID_Company = ${this.currentUser.ID_Company}
            ${filterString} AND
            1 = 1
       `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }
 
  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "TextBlastList"],
      ID_ItemType: ItemTypeEnum.Service
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "TextBlast", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {

      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Delete Item",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetActiveInactiveItem",
        {
          record: [{
            ID_Item: this.selectedRecord.ID,
            IsActive: false
          }],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }
  
  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }
}
