import {
  FilingStatusEnum,
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import {
  AppointmentSchedule,
  APP_DETAILVIEW,
  APP_MODEL,
} from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { ChangeWaitingStatusPatientWaitingListDialogComponent } from "src/app/CustomListView/patient-waiting-list/change-waiting-status-patient-waiting-list-dialog/change-waiting-status-patient-waiting-list-dialog.component";
import { Enumerable } from "linq-typescript";

@Component({
  selector: "app-for-billing-list",
  templateUrl: "./for-billing-list.component.html",
  styleUrls: ["./for-billing-list.component.less"],
})
export class ForBillingListComponent extends BaseCustomListView {
  CustomComponentName: string = "ForBillingListComponent";

  @ViewChild("changewaitingstatuspatientwaitinglistdialog")
  changewaitingstatuspatientwaitinglistdialog: ChangeWaitingStatusPatientWaitingListDialogComponent;

  FILINGSTATUS_FORBILLING: number = FilingStatusEnum.ForBilling;
  FILINGSTATUS_PENDING: number = FilingStatusEnum.Pending;
  FILINGSTATUS_PARTIALLYPAID: number = FilingStatusEnum.PartiallyPaid;
  FILINGSTATUS_FULLYPAID: number = FilingStatusEnum.FullyPaid;

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vForBilling_ListView_temp
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    return filterString;
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  tableRowDetail_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ForBillingList"],
    };

    if (
      record.Oid_Model.toLowerCase() ==
      APP_MODEL.PATIENT_CONFINEMENT.toLowerCase()
    ) {
      routeLink = [`/Main`, "Confinement", record.ID_CurrentObject];
      this.globalFx.customNavigate(routeLink, config);
    } else if (
      record.Oid_Model.toLowerCase() == APP_MODEL.PATIENT_SOAP.toLowerCase()
    ) {
      routeLink = [`/Main`, "Patient_SOAP", record.ID_CurrentObject];
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  colAction_CreateBillingInvoice_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ForBillingList"],
    };

    if (
      record.Oid_Model.toLowerCase() ==
      APP_MODEL.PATIENT_CONFINEMENT.toLowerCase()
    ) {
      config["ID_Patient_Confinement"] = record.ID_CurrentObject;

      routeLink = [`/Main`, "BillingInvoice", -1];
      this.globalFx.customNavigate(routeLink, config);
    } else if (
      record.Oid_Model.toLowerCase() == APP_MODEL.PATIENT_SOAP.toLowerCase()
    ) {
      config["ID_Patient_SOAP"] = record.ID_CurrentObject;
      routeLink = [`/Main`, "BillingInvoice", -1];
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  async dataSource_InitLoad(obj: any) {
    var IDs_Patient_SOAP = Enumerable.fromSource(obj.Records)
      .where((r: any) => {
        return r["Name_Model"] == "Patient_SOAP";
      })
      .select((r: any) => r["ID_CurrentObject"])
      .toArray();

    var IDs_Patient_Confinement = Enumerable.fromSource(obj.Records)
      .where((r: any) => {
        return r["Name_Model"] == "Patient_Confinement";
      })
      .select((r: any) => r["ID_CurrentObject"])
      .toArray();

    await this.ds
      .execSP(
        "pGetForBillingBillingInvoiceRecords",
        {
          IDs_Patient_SOAP: IDs_Patient_SOAP,
          IDs_Patient_Confinement: IDs_Patient_Confinement,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (result) => {
        obj.Records.forEach((record: any) => {
          record.BillingInvoices = [];

          result.BillingInvoices.forEach((refbi: any) => {
            if (
              record.Oid_Model == refbi.Oid_Model &&
              record.ID_CurrentObject == refbi.ID_CurrentObject
            ) {
              for (let i = 1; i <= 10; i++) {
                var infoBilling = refbi[i + ""];

                if (infoBilling) {
                  var infos = infoBilling.split("|");

                  record.BillingInvoices.push({
                    ID: infos[0],
                    Code: infos[1],
                    Status: infos[2],
                  });
                }
              }
            }
          });
        });
      });
  }

  colAction_ViewBillingInvoice_onClick(bi: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ForBillingList"],
    };

    routeLink = [`/Main`, "BillingInvoice", bi.ID];
    this.globalFx.customNavigate(routeLink, config);
  }
}
