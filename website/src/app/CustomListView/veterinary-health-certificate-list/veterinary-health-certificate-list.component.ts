import {
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";

@Component({
  selector: 'app-veterinary-health-certificate-list',
  templateUrl: './veterinary-health-certificate-list.component.html',
  styleUrls: ['./veterinary-health-certificate-list.component.less']
})
export class VeterinaryHealthCertificateListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "VeterinaryHealthCertificateListComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Species",
      },

    ];

    this.appForm.details = details;
  }

  async loadRecords() {
 
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vVeterinaryHealthCertificate_ListView
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "VeterinaryHealthCertificateList"],
      ID_ItemType: ItemTypeEnum.Inventoriable
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "VeterinaryHealthCertificate", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }
}
