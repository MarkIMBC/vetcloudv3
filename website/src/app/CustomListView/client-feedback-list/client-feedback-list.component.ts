


import { Component, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';

@Component({
  selector: 'client-feedback-list',
  templateUrl: './client-feedback-list.component.html',
  styleUrls: ['./client-feedback-list.component.less']
})
export class ClientFeedbackListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "ClientFeedbackListComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *

            FROM dbo.vClientFeedback
            /*encryptsqlend*/
            WHERE 
          
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString} 
              AND 1 = 1
          `;

    await this.getRecordPaging(sql);
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main/ClientFeedbackList`, ""],
    };
    
    routeLink = [`/Main`, 'ClientFeedback', id];
    this.globalFx.customNavigate(routeLink, config);
  }


  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }
}

