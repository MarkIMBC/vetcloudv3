

import { Component, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';

@Component({
  selector: 'app-client-appointment-request-list',
  templateUrl: './client-appointment-request-list.component.html',
  styleUrls: ['./client-appointment-request-list.component.less']
})

export class ClientAppointmentRequestListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "ClientAppointmentRequestListComponent";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    ID_Patient,
                    Name_Patient,
                    Name_Client,
                    DateStart,
                    Comment,
                    DateEnd,
                    Name_FilingStatus,
                    AttendingPhysician_Name_Employee
            FROM dbo.vClientAppointmentRequest_ListView
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }
  
  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "ClientAppointmentRequestList"],
    };
    
    routeLink = [`/Main`, 'ClientAppointmentRequest', id];
    this.globalFx.customNavigate(routeLink, config);
  }
  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

}

