import {
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { HelpDeskVideoDialogComponent } from "./help-desk-video-dialog/help-desk-video-dialog.component";

@Component({
  selector: 'app-help-desk-video-tutorial-list',
  templateUrl: './help-desk-video-tutorial-list.component.html',
  styleUrls: ['./help-desk-video-tutorial-list.component.less']
})
export class HelpDeskVideoTutorialListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "HelpDeskVideoTutorialListComponent";

  @ViewChild("helpdeskvideodialog")
  helpdeskvideodialog: HelpDeskVideoDialogComponent;

  initFilterFields() {
    var details = [
    
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "SearchTag",
      },

    ];

    this.appForm.details = details;
  }

  async loadRecords() {
 
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vHelpDeskVideoTutorial
        WHERE
          1 = 1  
          ${filterString} 
     `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = '';

    if(this.CurrentObject['SearchTag']){

      filterString += `Name LIKE '%${this.CurrentObject.SearchTag}%'`
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "VeterinaryHealthCertificateList"],
      ID_ItemType: ItemTypeEnum.Inventoriable
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "VeterinaryHealthCertificate", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  btnWatch_onClick(record){

    this.helpdeskvideodialog.Open(record.ID);
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }
}
