import {
  InventoryStatusEnum,
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import * as moment from "moment";
import { debug } from "console";

@Component({
  selector: "app-iteminventoriables",
  templateUrl: "./iteminventoriables.component.html",
  styleUrls: ["./iteminventoriables.component.less"],
})
export class IteminventoriablesComponent extends BaseCustomListView {
  
  CustomComponentName: string = "IteminventoriablesComponent";

  ItemList: any[] = [];

  async loadExistingItem(): Promise<void> {
    var sql = this.cs.encrypt(
      `SELECT DISTINCT
              LTRIM(RTRIM(Name)) Name
        FROM dbo.tItem
        WHERE 
          ID_Company = ${this.currentUser.ID_Company} AND
          ID_ItemType = 2 AND
          IsActive = 1
    `
    );

    var _ItemList = [];

    var objs = await this.ds.query<any>(sql);

    this.ItemList = [];
    objs.forEach(function (obj) {
      _ItemList.push(obj.Name);
    });

    this.ItemList = _ItemList;
  }

  ID_InventoryStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      SELECT '-1' ID, 'Available Stock' Name
      UNION ALL
      Select 
        ID, Name 
      FROM tInventoryStatus 
      WHERE 
        ID IN (1, 2)
    `),
  };

  ID_ItemCategory_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      Select 
        ID, Name 
      FROM tItemCategory 
      WHERE 
        ID_itemType IN (2)
    `),
  };

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.loadExistingItem();
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vItemInventoriable_ListvIew
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    if (this.CurrentObject["ID_InventoryStatus"]) {
      if (this.CurrentObject["ID_InventoryStatus"] == -1) {
        if (filterString.length > 0) filterString += " AND ";
        filterString += `ISNULL(ID_InventoryStatus, 0) IN (
          ${InventoryStatusEnum.High},
          ${InventoryStatusEnum.Medium}
        )`;
      } else {
        if (filterString.length > 0) filterString += " AND ";
        filterString += `ISNULL(ID_InventoryStatus, 0) = ${this.CurrentObject["ID_InventoryStatus"]} `;
      }
    }

    if (this.CurrentObject["ID_ItemCategory"]) {
      if (filterString.length > 0) filterString += " AND ";
      filterString += `ISNULL(ID_ItemCategory, 0) = ${this.CurrentObject["ID_ItemCategory"]} `;
    }

    if (!isNullOrUndefined(this.CurrentObject["OtherInfo_DateExpiration"])) {
      var _dateStart = this.CurrentObject["OtherInfo_DateExpiration"][0];
      var _dateEnd = this.CurrentObject["OtherInfo_DateExpiration"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, OtherInfo_DateExpiration) = '${moment(
          _dateStart
        ).format("YYYY-MM-DD")}'`;
      } else {
        filterString += `
            CONVERT(DATE, OtherInfo_DateExpiration) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "Items"],
      ID_ItemType: ItemTypeEnum.Inventoriable,
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Item", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Delete Item",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetActiveInactiveItem",
        {
          record: [
            {
              ID_Item: this.selectedRecord.ID,
              IsActive: false,
            },
          ],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SetAsInactive);
  }
}
