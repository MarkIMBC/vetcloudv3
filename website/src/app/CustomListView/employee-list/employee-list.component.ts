import { Component, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api/public_api';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { APP_DETAILVIEW, Employee } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';
import { BaseCustomListView } from '../BaseCustomListView';

@Component({  
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.less']
})
export class EmployeeListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "EmployeeListComponent";

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {

      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Inactive Employee",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetInactiveEmployee",
        {
          IDs_Employee: [this.selectedRecord.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedRecord.Name} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
      {
        ID_PropertyType: PropertyTypeEnum.Int,
        Name: "ID_Position",
      },
    ];

    this.appForm.details = details;
  }
  

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT ID,
                Name,
                Name_Position,
                DateCreated,
                DateModified,
                CONVERT(VARCHAR, DateCreated, 0) DateCreatedString,
                CONVERT(VARCHAR, DateModified, 0) DateModifiedString
            FROM dbo.vNonSystemUseEmployee
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  ID_Position_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tPosition"),
  };

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    this.appForm.IncludeFilterList.forEach((filterName) => {
      if (filterName == "ID_Position") {
        if (filterString.length > 0) filterString = filterString + " AND ";

        if (isNullOrUndefined(this.CurrentObject["ID_Position"]))
          this.CurrentObject["ID_Position"] = 0;

        filterString += `ISNULL(ID_Position, 0) = ${this.CurrentObject["ID_Position"]} `;
      }
    });

    return filterString;
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "EmployeeList"],
    };
    
    routeLink = [`/Main`, 'Employee', id];
    this.globalFx.customNavigate(routeLink, config);
  }
  
  initializeMenuItems() {

    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SetAsInactive);
  }

}
