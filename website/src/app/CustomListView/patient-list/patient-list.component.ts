import { APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { Patient_DTO, PropertyTypeEnum } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { MenuItem } from "primeng/api";
import * as moment from "moment";

@Component({
  selector: "app-patient-list",
  templateUrl: "./patient-list.component.html",
  styleUrls: ["./patient-list.component.less"],
})
export class PatientListComponent extends BaseCustomListView {
  CustomComponentName: string = "PatientListComponent";

  mainboxHeight: string = "300px";

  dataSource: Patient_DTO[] = [];

  IsActive_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  t.ID, 
              t.Name
      FROM    ( 
        SELECT 0 ID, 'Yes' Name UNION 
        SELECT 1 ID, 'No' Name  
      ) t 
      /*encryptsqlend*/
    `),
  };

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT  *
        FROM dbo.vPatient_ListView
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Species",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Microchip",
      },
      {
        ID_PropertyType: PropertyTypeEnum.Int,
        Name: "ID_Gender",
      },
    ];

    this.appForm.details = details;
  }

  ID_Gender_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tGender"),
  };

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    this.appForm.IncludeFilterList.forEach((filterName) => {
      if (filterName == "ID_Gender") {
        if (filterString.length > 0) filterString = filterString + " AND ";

        if (isNullOrUndefined(this.CurrentObject["ID_Gender"]))
          this.CurrentObject["ID_Gender"] = 0;

        filterString += `ISNULL(ID_Gender, 0) = ${this.CurrentObject["ID_Gender"]} `;
      }
    });

    if (
      this.CurrentObject["IsActive"] != undefined &&
      this.CurrentObject["IsActive"] != null
    ) {
      if (filterString.length > 0) filterString = filterString + " AND ";

      filterString += `ISNULL(IsActive, 0) = ${this.CurrentObject["IsActive"]} `;
    }

    if (!isNullOrUndefined(this.CurrentObject["DateBirth"])) {
      var _dateStart = this.CurrentObject["DateBirth"][0];
      var _dateEnd = this.CurrentObject["DateBirth"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, DateBirth) = '${moment(
          _dateStart
        ).format("YYYY-MM-DD")}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateBirth) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    }

    if (
      this.CurrentObject["IsActive"] == undefined ||
      this.CurrentObject["IsActive"] == null
    ) {
      if (filterString.length > 0) filterString = filterString + " AND ";

      filterString += `ISNULL(IsActive, 0) = 1 `;
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "Patients"],
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Patient", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  protected _InitMenuItem_SetAsInactive: MenuItem = {
    label: "Delete",
    icon: "pi pi-fw pi-ban red-text",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to delete ${this.selectedRecord.Name}?`,
        "Delete Patient",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doInactive();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doInactive(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pSetActiveInactivePatient",
        {
          record: [
            {
              ID_Patient: this.selectedRecord.ID,
              IsActive: false,
            },
          ],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedRecord.Name} has been inactived successfully.`,
          `${this.selectedRecord.Name} Inactive`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Inactive ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }

  protected _InitMenuItem_Queue: MenuItem = {
    label: "Queue",
    icon: "pi pi-fw pi-ban blue-text",
    command: async () => {
      var result = await this.msgBox.confirm(
        `Would you like to add ${this.selectedRecord.Name} to waiting list?`,
        "Queue Patient",
        "Yes",
        "No"
      );
      if (!result) return;

      var obj = await this.doQueue();

      if (obj.Success) {
        await this.loadRecords();
      }
    },
  };

  async doQueue(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDoAddPatientToWaitingList",
        {
          IDs_Patient: [this.selectedRecord.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedRecord.Name} has been added to waiting list.`,
          `${this.selectedRecord.Name} added`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Add ${this.selectedRecord.Name}`
        );
        rej(obj);
      }
    });
  }

  colAction_CreateAppointment_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "Patients"],
      ID_Client: record.ID_Client,
      ID_Patient: record.ID,
    };

    routeLink = [`/Main`, "PatientAppointment", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  colAction_CreateSOAP_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "Patients"],
      ID_Patient: record.ID,
    };

    routeLink = [`/Main`, "Patient_SOAP", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SetAsInactive);
    this.menuItems.push(this._InitMenuItem_Queue);
  }
}
