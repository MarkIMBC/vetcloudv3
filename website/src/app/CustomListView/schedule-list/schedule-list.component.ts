import { PropertyTypeEnum } from "./../../../utils/controls/class_helper";
import { Component, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { isNullOrUndefined } from "util";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { MenuItem } from "primeng/api";
import * as moment from "moment";
import { AppointmentScheduleStatusComponent } from "./appointment-schedule-status/appointment-schedule-status.component";
import { Enumerable } from "linq-typescript";

@Component({
  selector: "app-schedule-list",
  templateUrl: "./schedule-list.component.html",
  styleUrls: ["./../BaseCustomListView.less", "./schedule-list.component.less"],
})
export class ScheduleListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "ScheduleListComponent";

  @ViewChild("appointmentschedulestatus")
  appointmentschedulestatus: AppointmentScheduleStatusComponent;

  InitCurrentObject: any = {
    ReferenceCode: "",
    Paticular: "",
    Description: "",
    DateStart: [
      new Date(),
      new Date(),
    ],
  };

  OrderByString: string = `
  
    CASE WHEN CONVERT(Date, DateStart) =  CONVERT(Date, GETDATE()) 
    THEN  1
    ELSE  0
    END DESC,  
    DateStart ASC
  `;

  Appointment_ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      Select 
        ID, Name 
      FROM vAppointment_FilingStatus 
    `),
  };

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT * 
            FROM vAppointmentEvent 
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "ReferenceCode",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
    ];

    this.appForm.details = details;
  }

  getIsIncludeFilterList(fieldName): boolean {
    return this.appForm.IncludeFilterList.includes(fieldName);
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    if (!isNullOrUndefined(this.CurrentObject["DateStart"])) {
      var _dateStart = this.CurrentObject["DateStart"][0];
      var _dateEnd = this.CurrentObject["DateStart"][1];

      if (filterString.length > 0) filterString += " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, DateStart) = '${moment(
          _dateStart
        ).format("YYYY-MM-DD")}'`;
      } else {
        filterString += `
            CONVERT(DATE, DateStart) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    } else {
      if (filterString.length > 0) filterString += " AND ";

      filterString += `
          CONVERT(DATE, DateStart) BETWEEN
            '${moment(_dateStart).format("YYYY-MM-DD")}' AND
            '${moment(_dateEnd).format("YYYY-MM-DD")}'
      `;
    }

    if (!isNullOrUndefined(this.CurrentObject["ReferenceCode"])) {
      if (filterString.length > 0) filterString += " AND ";
      filterString += `ReferenceCode LIKE '%${this.CurrentObject["ReferenceCode"]}%'`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Paticular"])) {
      if (filterString.length > 0) filterString += " AND ";
      filterString += `Paticular LIKE '%${this.CurrentObject["Paticular"]}%'`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Appointment_ID_FilingStatus"])) {
      if (filterString.length > 0) filterString += " AND ";
      filterString += `Appointment_ID_FilingStatus = ${this.CurrentObject["Appointment_ID_FilingStatus"]}`;
    }

    return filterString;
  }

  clearFilter() {
    this.CurrentObject = {
      ReferenceCode: "",
      Paticular: "",
      Description: "",
      DateStart: [new Date(), new Date()],
    };

    this.appForm.clearFilter();
    this.loadRecords();
  }

  dataSource_InitLoad(obj: any) {
    var currentDate = moment().format("YYYY-MM-DD");
    var records = obj.Records;
    var rowindex = 1;
    var __tempDateStart = "";

    records.forEach((record: any, index: number) => {
      var dateStart = moment(record.DateStart).format("YYYY-MM-DD");

      record.IsLastRecord = false;

      if (dateStart == currentDate) {
        record.IsCurrentDay = true;
      } else {
        record.IsCurrentDay = false;
      }

      if (__tempDateStart !== dateStart) {
        rowindex = 1;

        if (index - 1 >= 0) records[index - 1].IsLastRecord = true;
        __tempDateStart = dateStart;
      } else {
        rowindex++;
      }

      record.RowIndex = rowindex;
    });

    console.log(obj);

    obj.Records = Enumerable.fromSource(records)
      .orderByDescending(function (x) {
        return x["IsCurrentDay"];
      })
      .toArray();
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1, "PatientAppointment");
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(
      record.ID_CurrentObject,
      record.Name_Model,
      false,
      record
    );
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(
      record.ID_CurrentObject,
      record.Name_Model,
      true
    );
  }

  Col_Appointment_Name_FilingStatus_onClick(record: any) {
    if (this.appointmentschedulestatus) {
      this.appointmentschedulestatus.open(record).then(() => {
        this.loadRecords();
      });
    }
  }

  gotoCustomDetailViewRecord(id, Name_Model, isNewTab?, record?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "ScheduleList"],
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    if (record) config["ID_Client"] = record.ID_Client;

    if (Name_Model == "PatientAppointment") {
      routeLink = [`/Main`, "PatientAppointment", id];
    } else if (Name_Model == "Patient_SOAP") {
      routeLink = [`/Main`, "Patient_SOAP", id];
    } else if (Name_Model == "Patient_Wellness") {
      routeLink = [`/Main`, "Patient_Wellness", id];
    }

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  protected _InitMenuItem_SwitchToCalendar: MenuItem = {
    label: "Switch to Calendar",
    icon: "pi pi-fw pi-calendar-plus blue-text",
    command: async () => {
      this.router.navigate([`/Main`, "ScheduleCalendar"]);
    },
  };

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
    this.menuItems.push(this._InitMenuItem_SwitchToCalendar);
  }
}
