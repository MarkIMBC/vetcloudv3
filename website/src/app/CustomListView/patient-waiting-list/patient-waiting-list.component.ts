import {
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import {
  AppointmentSchedule,
  APP_DETAILVIEW,
  APP_MODEL,
} from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { ChangeWaitingStatusPatientWaitingListDialogComponent } from "src/app/CustomListView/patient-waiting-list/change-waiting-status-patient-waiting-list-dialog/change-waiting-status-patient-waiting-list-dialog.component";
import * as moment from "moment";
import { Enumerable } from "linq-typescript";

@Component({
  selector: "app-patient-waiting-list",
  templateUrl: "./patient-waiting-list.component.html",
  styleUrls: ["./patient-waiting-list.component.less"],
})
export class PatientWaitingListComponent extends BaseCustomListView {
  CustomComponentName: string = "PatientWaitingListComponent";

  @ViewChild("changewaitingstatuspatientwaitinglistdialog")
  changewaitingstatuspatientwaitinglistdialog: ChangeWaitingStatusPatientWaitingListDialogComponent;

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vPatientWaitingList_ListView_temp
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);

    var IDs_Patient = Enumerable.fromSource(this.dataSource)
      .select((r: any) => r["ID_Patient"])
      .toArray();

    await this.ds
      .execSP(
        "pGetPatientWaitingAppoitmentReference",
        {
          IDs_Patient: IDs_Patient,
          DateStart: moment().format("YYYY-MM-DD HH:mm:ss"),
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      )
      .then(async (obj) => {
        console.log(obj);

        this.dataSource.forEach((record: any) => {
          record.References = [];
          record["UniqueIDList"] = "";


          obj.PatientWaitingAppoitmentReference.forEach(
            (appoitmentRef: any) => {
              if (record.ID_Patient == appoitmentRef.ID_Patient) {
                var UniqueIDList = appoitmentRef.UniqueIDList;
                var references = UniqueIDList.split("~");
                var _ReferenceCode = "";

                references.forEach((reference: any) => {
                  var infos = reference.split("|");
                  var referenceObj = {
                    Oid_Model: infos[0],
                    ID_CurrentObject: infos[1],
                    ReferenceCode: infos[4],
                    Description: infos[5],
                  };

                  if (_ReferenceCode != referenceObj.ReferenceCode) {
                    record.References.push(referenceObj);
                    _ReferenceCode = referenceObj.ReferenceCode;
                  }
                });
              }
            }
          );

          if (record.References.length == 0) {
            if (record.IsQueued) {
              record.References.push({
                Oid_Model: record.Oid_Model_Reference,
                ID_CurrentObject: record.ID_Reference,
                ReferenceCode: "Queued",
                Description: "",
              });
            }
          }
        });
      });
  }

  dataSource_InitLoad(obj: any) {}

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    return filterString;
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  colWaitingStatus_onClick(record: any) {
    this.changewaitingstatuspatientwaitinglistdialog
      .open(record.ID)
      .then(() => {
        this.loadRecords();
      });
  }

  colAction_CreateSOAP_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
      ID_Patient: record.ID_Patient,
    };

    routeLink = [`/Main`, "Patient_SOAP", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  colAction_CreateConfinement_onClick(record: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
      ID_Client: record.ID_Client,
      ID_Patient: record.ID_Patient,
    };

    routeLink = [`/Main`, "Confinement", -1];
    this.globalFx.customNavigate(routeLink, config);
  }

  Reference_OnRowClick(reference: any) {
    var routeLink = [];
    var config = {
      BackRouteLink: [`/Main`, "PatientWaitingList"],
    };

    var Oid_Model = reference.Oid_Model;
    var ID_CurrentObject = reference.ID_CurrentObject;

    switch (Oid_Model.toUpperCase()) {
      case APP_MODEL.PATIENT_SOAP:
        routeLink = [`/Main`, "Patient_SOAP", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENTAPPOINTMENT:
        routeLink = [`/Main`, "PatientAppointment", ID_CurrentObject];
        break;
      case APP_MODEL.PATIENT_WELLNESS:
        routeLink = [`/Main`, "Patient_Wellness", ID_CurrentObject];
        break;
    }

    this.globalFx.customNavigate(routeLink, config);
  }
}
