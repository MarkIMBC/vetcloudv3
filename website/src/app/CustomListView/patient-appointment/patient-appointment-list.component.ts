import { AppointmentSchedule, APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { Patient_DTO } from 'src/utils/controls/class_helper';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-patient-appointment-list',
  templateUrl: './patient-appointment-list.component.html',
  styleUrls: ['./patient-appointment-list.component.less']
})
export class PatientAppointmentListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PatientAppointmentListComponent";

  initializeMenuItems(){

    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  async loadRecords() {

    var queryString = this.cs.encrypt(`
        SELECT ID,
              DateStartString,
              DateEndString,
              DateStart,
              DateEnd,
              Patient,
              Doctor,
              AppointmentStatus_Name_FilingStatus
          FROM vPatientAppointment
          ORDER BY ID DESC;
    `);

    this.dataSource = await this.ds.query<any>(queryString);
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  async tableRowDetail_onClick(record: any){

    var sql = this.cs.encrypt(`SELECT ID, ID_Patient FROM vAppointmentSchedule WHERE ID = ${record.ID}`);   
    var records = await this.ds.query<AppointmentSchedule>(sql);

    if(records.length > 0){

      var routerLink = [];
      var config = {

        ID_AppointmentSchedule: records[0].ID,
      };

      this.gotoCustomDetailViewRecord(records[0].ID_Patient);
    }
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "TodaysAppointment"],
    };
    
    routeLink = [`/Main`, 'Patient', id];
    this.globalFx.customNavigate(routeLink, config);
  }
}
