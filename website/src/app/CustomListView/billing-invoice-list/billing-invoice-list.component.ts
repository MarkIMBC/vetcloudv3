import { AppRouteService } from "./../../AppServices/route.service";
import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import {
  FilingStatusEnum,
  Patient_DTO,
  PropertyTypeEnum,
} from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";
import * as moment from "moment";

@Component({
  templateUrl: "./billing-invoice-list.component.html",
  styleUrls: ["./billing-invoice-list.component.less"],
})
export class BillingInvoiceListComponent extends BaseCustomListView {
  CustomComponentName: string = "BillingInvoiceListComponent";

  OrderByString: string = "Date DESC";

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "OtherReferenceNumber",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "AttendingPhysician_Name_Employee",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "PatientNames",
      },
      {
        ID_PropertyType: PropertyTypeEnum.Int,
        Name: "ID_FilingStatus",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Comment",
      },
    ];

    this.appForm.details = details;
  }

  ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(
      "Select ID, Name FROM tFilingStatus WHERE ID IN(1, 4, 2, 11, 12, 13)"
    ),
  };

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM dbo.vBillingInvoice_ListView
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    if (isNullOrUndefined(this.CurrentObject["ID_FilingStatus"]))
      this.CurrentObject["ID_FilingStatus"] = 0;

    this.appForm.IncludeFilterList.forEach((filterName) => {
      if (filterName == "ID_FilingStatus") {
        if (filterString.length > 0) filterString = filterString + " AND ";

        if (
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Filed ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Cancelled
        ) {
          filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
        } else if (
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Pending ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Done ||
          this.CurrentObject["ID_FilingStatus"] ==
            FilingStatusEnum.PartiallyPaid ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.FullyPaid
        ) {
          filterString += `ISNULL(ID_FilingStatus, 0) = 3 AND 
                             ISNULL(Payment_ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
        }
      }
    });

    /** Default Filter **/
    var hasAssignedFilter = filterString.length > 0;

    if (!hasAssignedFilter) {
      filterString += `ISNULL(ID_FilingStatus, 0) NOT IN (${FilingStatusEnum.Cancelled})`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Date"])) {
      var _dateStart = this.CurrentObject["Date"][0];
      var _dateEnd = this.CurrentObject["Date"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {
        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format(
          "YYYY-MM-DD"
        )}'`;
      } else {
        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format("YYYY-MM-DD")}' AND
               '${moment(_dateEnd).format("YYYY-MM-DD")}'
        `;
      }
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "BillingInvoices"],
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "BillingInvoice", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }
}
