import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { DialogBoxComponent } from "src/app/dialog-box/dialog-box";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { Payable, Payable_Detail } from "src/bo/APP_MODELS";
import { AppFormComponent } from "src/utils/controls/appForm/appForm.component";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { FilingStatusEnum, Payable_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "payable-dialog-box",
  templateUrl: "./payable-dialog-box.component.html",
  styleUrls: ["./payable-dialog-box.component.less"],
})
export class PayableDialogBoxComponent implements OnInit {
  menuItems: MenuItem[] = [];

  CurrentObject: Payable_DTO = new Payable_DTO();
  PreviousObject: Payable_DTO = new Payable_DTO();

  title: string = "";

  selectedPayableDetail: Payable_Detail = new Payable_Detail();

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  private __ID_CurrentObject: number = -1;

  ID_ExpenseCategory_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tExpenseCategory"),
  };

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {}

  async getRecord() {
    var obj = await this.ds.execSP(
      "pGetPayable",
      {
        ID: this.__ID_CurrentObject,
        ID_Session: this.currentUser.ID_Session,
      },
      {
        isReturnObject: true,
      }
    );

    this.CurrentObject = obj;

    if (!this.CurrentObject.Payable_Detail)
      this.CurrentObject["Payable_Detail"] = [];
    if (this.CurrentObject.Payable_Detail.length == 0) {
      this.CurrentObject.Payable_Detail.push({
        ID: this.globalFx.getTempID(),
        Name: "",
        ID_ExpenseCategory: null,
        Amount: 0,
      });
    }

    this.selectedPayableDetail = this.CurrentObject.Payable_Detail[0];
    if (this.CurrentObject.ID > 0) {
      if (
        this.CurrentObject.Payment_ID_FilingStatus !== FilingStatusEnum.Filed &&
        this.CurrentObject.Payment_ID_FilingStatus !== FilingStatusEnum.Pending
      ) {
        this.title = `(${this.CurrentObject.Payment_Name_FilingStatus}) Payable`;
      } else {
        this.title = "Edit Payable";
      }
    } else {
      this.title = "Add Payable";
    }

    this.PreviousObject = this.globalFx.cloneObject(obj);

    var isformDisabled = this.formDisabled();
    if (!isNullOrUndefined(this.appForm))
      this.appForm.setFormDisabled(isformDisabled);
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Filed &&
      this.CurrentObject.Payment_ID_FilingStatus != FilingStatusEnum.Pending
    ) {
      isDisabled = true;
    }

    this.CustomDialogBox.IsShowBtnPositive = !isDisabled;

    return isDisabled;
  }

  public async save() {
    if (
      isNullOrUndefined(this.CurrentObject["ID_Company"]) &&
      this.CurrentObject.ID < 1
    ) {
      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    var r = await this.ds.saveObject(
      "48FB7081-BDE7-48A9-8E7D-EC060F5436BC",
      this.CurrentObject,
      this.PreviousObject,
      [],
      this.currentUser
    );
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);
    this.__ID_CurrentObject = ID_CurrentObject;

    return true;
  }

  async load<T>(id: number): Promise<T[]> {
    this.__ID_CurrentObject = id;

    await this.getRecord();

    this.CustomDialogBox.captionNegativeButton = "Close";
    this.loadMenuItems();

    return this.CustomDialogBox.open();
  }

  async PayablePaymentTable_onCancelled() {
    await this.getRecord();

    this.CustomDialogBox.captionNegativeButton = "Close";
    this.loadMenuItems();
  }

  private loadMenuItems() {
    var menuCancelled: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.selectedPayableDetail.Name} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel ${this.selectedPayableDetail.Name}?`,
          "Payable",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          this.CustomDialogBox.close();
        }
      },
    };

    this.menuItems = [];

    if (
      this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending &&
      this.CurrentObject.ID > 0
    ) {
      this.menuItems.push(menuCancelled);
    }
  }

  async cancel(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPayable",
        {
          IDs_Payable: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.selectedPayableDetail.Name} has been canceled successfully.`,
          `Canceled Payable`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to Canceled Payable`);
        rej(obj);
      }
    });
  }

  async onPositiveButtonClick(modal: any) {
    var isvalid;

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    var result = await this.msgBox.confirm(
      "Would you like to save payable?",
      `Saving Payable`,
      "Yes",
      "No"
    );

    if (!result) return;

    this.compute();
    var isSave = await this.save();

    if (isSave) {
      this.toastr.success("Saving Successfull", "Payable");
      modal.close();
    }
  }

  private async validateRecord() {
    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];

    if (!isNullOrUndefined(this.appForm)) {
      validationsAppForm = await this.appForm.getValidations();
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  private compute() {
    var totalAmount: number = 0;

    this.CurrentObject.Payable_Detail.forEach((payableDetail) => {
      totalAmount += payableDetail.Amount;
    });

    this.CurrentObject.RemaningAmount = totalAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }
}
