import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  FilingStatusEnum,
  BillingInvoice_DTO,
} from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";

@Component({
  selector: "payable-payment-table",
  templateUrl: "./payable-payment-table.component.html",
  styleUrls: ["./payable-payment-table.component.less"],
})
export class PayablePaymentTableComponent implements OnInit {
  dataSource: any[] = [];

  private __ID_CurrentObject: number = -1;
  private _ID_Payable: number = -1;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected ds: DataService,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  @Input() set ID_Payable(value: number) {
    this._ID_Payable = value;
    this.loadBillingInvoice();
  }

  async loadBillingInvoice() {
    this.dataSource = [];

    if (this._ID_Payable == null || this._ID_Payable == undefined) return;

    var sql = this.cs.encrypt(`
            SELECT  ID,
                    Date,
                    TotalAmount
            FROM dbo.tPayablePayment
            WHERE 
              ID_Payable = ${this._ID_Payable} AND
              ID_FilingStatus IN (${FilingStatusEnum.Approved})
          `);

    this.dataSource = await this.ds.query<any>(sql);
  }

  async btnCancelPayment(record) {
    var result = await this.msgBox.confirm(
      "Would you like to Cancel Payment?",
      `Payment`,
      "Yes",
      "No"
    );

    if (!result) return;
    await this.doCancel(record.ID);

    this.loadBillingInvoice();
  }
  async doCancel(ID_PayablePayment: number): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelPayablePayment",
        {
          IDs_PayablePayment: [ID_PayablePayment],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(`Cancel Payment is successfull`, `Pay`);
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(`Cancel Payment is failed`, `Pay`);
        rej(obj);
      }
    });
  }
}
