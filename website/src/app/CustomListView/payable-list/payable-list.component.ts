import {
  ItemTypeEnum,
  PropertyTypeEnum,
} from "./../../../utils/controls/class_helper";
import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { Patient_DTO } from "src/utils/controls/class_helper";
import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { PayableDialogBoxComponent } from "./payable-dialog-box/payable-dialog-box.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";
import { PayablePaymentDialogBoxComponent } from "./payable-payment-dialog-box/payable-payment-dialog-box.component";
import * as moment from "moment";

@Component({
  selector: 'app-payable-list',
  templateUrl: './payable-list.component.html',
  styleUrls: ['./payable-list.component.less']
})
export class PayableListComponent extends BaseCustomListView {
  
  CustomComponentName: string = "PayableListComponent";

  @ViewChild('payabledialogbox')
  payabledialogbox: PayableDialogBoxComponent;

  @ViewChild('payablepaymentdialogbox')
  payablepaymentdialogbox: PayablePaymentDialogBoxComponent;

  @Output() onloadRecord: EventEmitter<any> = new EventEmitter();

  SummaryObject: any = {


  }

  OrderByString: string = "Date DESC, Name_ExpenseCategory ASC, Name_Payable_Detail ASC"
  isLoading: boolean = false;
  showFilterBox: boolean = true;

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.Date,
        Name: "Date",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_ExpenseCategory",
      }, {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Payable_Detail",
      },
    ];

    this.appForm.details = details;
  }

  protected resize() {

    var height =
      parseInt(this.elRef.nativeElement.parentNode.offsetHeight) -  280;
    this.mainboxHeight = height.toString() + "px";
  }

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `
        SELECT 
            *
        FROM vPayable_PayableDetail_Listview
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
          ${filterString} AND
          1 = 1
     `;

    await this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    if (!isNullOrUndefined(this.CurrentObject["Date"])) {

      var _dateStart = this.CurrentObject["Date"][0];
      var _dateEnd = this.CurrentObject["Date"][1];

      if (_dateStart != null && _dateEnd == null) {

        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format('YYYY-MM-DD')}'`;
      } else {

        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    }

    if (!isNullOrUndefined(this.CurrentObject["Name_ExpenseCategory"])) {

      if (filterString.length > 0) filterString = filterString + " AND ";

      filterString += `
          Name_ExpenseCategory LIKE '%${this.CurrentObject["Name_ExpenseCategory"]}%'
      `;
    }

    if (!isNullOrUndefined(this.CurrentObject["Name_Payable_Detail"])) {

      if (filterString.length > 0) filterString = filterString + " AND ";
      filterString += `
      Name_Payable_Detail LIKE '%${this.CurrentObject["Name_Payable_Detail"]}%'
      `;
    }

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {

    this.payabledialogbox.load(record.ID).then(() => {

      this.loadRecords();
    });
  }

  dataSource_InitLoad(){


    this.loadSummary()

  }

  btnPayment(record: any) {

    this.payablepaymentdialogbox.CreateByPayable(record.ID).then(() => {

      this.loadRecords();
    });
  }
  

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "Items"],
      ID_ItemType: ItemTypeEnum.Inventoriable
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Item", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  async loadSummary(): Promise<any> {

    var ID_Company = this.currentUser.ID_Company;
    var filterString = this.getFilterString()

    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pGetPayable_PayableDetail_Listview_Summary",
        {
          CompanyID: this.currentUser.ID_Company,
          filterString: filterString,
        },
        {
          isReturnObject: true,
        }
      );

      this.SummaryObject = obj
    });
  }

  initializeMenuItems() {

    var menuItem_Add: MenuItem = {
      label: "Add Payable",
      icon: "pi pi-fw pi-plus",
      command: () => {

        this.payabledialogbox.load(-1).then(() => {

          this.loadRecords();
        });
      }
    };

    this.menuItems.push(menuItem_Add);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }
}

