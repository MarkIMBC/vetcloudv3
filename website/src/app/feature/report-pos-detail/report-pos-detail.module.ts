import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReportPOSDetailComponent } from './report-pos-detail.component';


const routes: Routes = [
  { path: '', component: ReportPOSDetailComponent }
];

@NgModule({
  declarations: [ReportPOSDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ReportPOSDetailModule { }
