import { APP_LISTVIEW } from '../../bo/APP_MODELS';
import { DataService } from 'src/utils/service/data.service';
import { ListViewComponent } from '../../app/View/ListView/ListView.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { InitTestBedModule } from '..';

var ListViewToTest: any[] = [
  {
    Name : "Item",
    Oid_ListView : APP_LISTVIEW.ITEM_LISTVIEW
  },
  // {
  //   Name : "Purchase Order",
  //   Oid_ListView : APP_LISTVIEW.ITEM_LISTVIEW
  // },
  // {
  //   Name : "Opportunity",
  //   Oid_ListView : APP_LISTVIEW.OPPORTUNITY_LISTVIEW
  // }
];

ListViewToTest.forEach(lv => {
  
  describe(`${lv.Name} ListView Test`, () => {

    let component: ListViewComponent;
    let fixture: ComponentFixture<ListViewComponent>;

    beforeEach(async(() => {
      InitTestBedModule()
    }));

    beforeEach(function () {
      DataService.API_URL = "http://localhost:5000/api/";
      jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
    });

    afterEach(() => {
      spyOn(component, 'ngOnDestroy').and.callFake(() => { });
      fixture.destroy();
    });

    beforeEach(async () => {
      fixture = TestBed.createComponent(ListViewComponent);
      component = fixture.componentInstance;
      spyOn(component, 'ngAfterViewInit').and.callFake(() => { });
      fixture.detectChanges();
      await component.initView(lv.Oid_ListView);
      fixture.detectChanges();
    });

    it('should have columns to load', async () => {
      await component.appDataTable.getListViewFormat();
      fixture.detectChanges();
      expect(component.appDataTable.columns.length).toBeGreaterThan(0);
    });

    it('should have reload data properly', async () => {
      await component.appDataTable.reloadData(true);
      fixture.detectChanges();
      expect(component.appDataTable.gridItems.length).toBeGreaterThanOrEqual(0);
    });

  });

})
