import { KeyCode } from "@ng-select/ng-select/lib/ng-select.types";
import { PropertyTypeEnum } from "../controls/class_helper";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";

@Component({
  selector: "look-up-dialog-box",
  templateUrl: "./look-up-dialog-box.component.html",
  styleUrls: ["./look-up-dialog-box.component.less"],
})
export class LookUpDialogBoxComponent implements OnInit {
  ngOnInit(): void {}

  selectedfilterColumn: any;

  filterValue: any;

  records: any[] = [];
  filterColumns: any[] = [];

  selectedRecords: any[];
  @ViewChild("mymodal")
  modalComponent: NgbModal;

  _options: lookUpDialogOption = new lookUpDialogOption();

  pagingOption: PagingOption = new PagingOption();

  title = "appBootstrap";

  closeResult: string;

  constructor(
    private modalService: NgbModal,
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected elRef: ElementRef
  ) {}

  async open<T>(optionn: lookUpDialogOption): Promise<T[]> {
    if (optionn.IsMultiple == null) optionn.IsMultiple = true;

    this._options = optionn;

    this.filterColumns = optionn.filterColumns;
    this.selectedfilterColumn = this.filterColumns[0];

    this.filterValue = "";

    this.pagingOption.PageNumber = 1;

    if (isNullOrUndefined(optionn.title)) optionn.title = "Select...";

    await this.loadRecord();

    var promise = new Promise<T[]>((resolve, reject) => {
      var cmp = this.modalService
        .open(this.modalComponent, {
          ariaLabelledBy: "modal-basic-title",
          backdrop: "static",
          size: "lg",
          centered: true,
        })
        .result.then(
          (result) => {
            resolve(result);
            this.selectedRecords = [];
          },
          () => {
            reject([]);
            this.selectedRecords = [];
          }
        );

      this.formatDialogBox(cmp);
    });

    this.resize();

    return promise;
  }

  private async loadRecord() {
    var sql = this._options.sql;

    console.log("selectedfilterColumn", this.selectedfilterColumn);

    sql = sql.replace(/\n/g, " ");

    if (isNullOrUndefined(this.filterColumns)) this.filterColumns = [];
    this._options.filterColumns.forEach(function (filterColumn) {
      if (isNullOrUndefined(filterColumn.caption))
        filterColumn.caption = filterColumn.name;
    });
    if (this.selectedfilterColumn == null)
      this.selectedfilterColumn = this._options.filterColumns[0];

    if (isNullOrUndefined(this.filterValue)) this.filterValue = "";
    if (this.filterValue.length > 0) {
      sql += sql.includes(" WHERE ") ? " AND " : ` WHERE `;
      sql += ` ${this.selectedfilterColumn.name} LIKE '%${this.filterValue}%' `;
    }

    var obj = await this.ds.execSP(
      "pGetRecordPaging",
      {
        sql: sql,
        PageNumber: this.pagingOption.PageNumber,
        DisplayCount: this.pagingOption.DisplayCount,
      },
      {
        isReturnObject: true,
      }
    );

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;
    this.records = obj.Records;
  }

  private formatDialogBox(cmp) {
    var parent = $((cmp as any)._windowCmptRef as ElementRef);

    setTimeout(() => {
      var modalDialog = parent.find(".modal-dialog");
      var dialogBody = $(".modal-body");

      modalDialog.css("width", `${this._options.width}!important`);
      modalDialog.css("max-width", `${this._options.width}!important`);

      setTimeout(() => {
        parent.css("cssText", "opacity:1!important");
      });
    });
  }

  selectboxFilterColumn_OnChanged(e): void {
    console.log(e);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  container_resize(event) {
    console.log(event);
  }

  mainboxHeight: string = "200px";

  protected resize() {
    var height = this._options.height;
    height = height.replace("px", "");

    height = (parseFloat(height) + 100).toString() + "px";

    this.mainboxHeight = height;
  }

  paginate(event) {
    //event.first = Index of the first record
    //event.rows = Number of rows to display in new page
    //event.page = Index of the new page
    //event.pageCount = Total number of pages

    this.pagingOption.PageNumber = event.page + 1;

    this.loadRecord();
  }

  Row_onClick(modal, record) {
    if (this._options.IsMultiple == true) return;
    this.selectedRecords = [record];

    modal.close(this.selectedRecords);
  }

  btnPositive_OnClick(modal) {
    modal.close(this.selectedRecords);
  }

  txtSearch_onChanged() {
    this.loadRecord();
  }

  dropDownfilterColumns_onKeyUp(e: any) {
    this.loadRecord();
  }

  txtSearch_onKeyUp(e: any) {
    if (e.keyCode == "13") {
      this.loadRecord();
    }
  }
}

export class PagingOption {
  PageNumber: number = 1;
  DisplayCount: number = 10;
  TotalRecord?: number;
  TotalPageNumber?: number;
}

class lookUpDialogOption {
  IsMultiple?: boolean = true;
  title?: string;
  sql: string = "";
  width?: string = "750px";
  height?: string = "200px";
  filterColumns?: filterColumn[] = [];
  columns: lookUpColumnOption[] = [];
}

class lookUpColumnOption {
  name: string;
  caption?: string;
  propertyType?: PropertyTypeEnum = PropertyTypeEnum.String;
  width?: string = "100%";
  align?: string = "left";
}

class filterColumn {
  name: string = "";
  caption?: string = "";
  propertyType?: PropertyTypeEnum = PropertyTypeEnum.String;
}
