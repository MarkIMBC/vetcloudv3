import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { Enumerable } from "linq-typescript";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { DialogBoxComponent } from "src/app/dialog-box/dialog-box";
import { APP_MODEL } from "src/bo/APP_MODELS";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import { ClientCredit, CreditModeEnum } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";
import {
  AppLookupBoxComponent,
  IAppLookupBoxOption,
} from "../controls/appLookupBox/appLookupBox.component";

@Component({
  selector: "client-credit-adjustment-dialogbox",
  templateUrl: "./client-credit-adjustment.component.html",
  styleUrls: ["./client-credit-adjustment.component.less"],
})
export class ClientCreditAdjustmentDialogBoxComponent implements OnInit {
  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;
  private _creditMode: CreditModeEnum = CreditModeEnum.None;

  @Output() onSavedCreditAmount = new EventEmitter<any>();

  isEnableToChangeMode: boolean = true;

  adjustCreditOption: string = "Deposit";
  IsDeposit: boolean = true;

  isAllowedWithdraw: boolean = true;
  isAllowedDeposit: boolean = true;

  @ViewChild("ID_Patient_LookUpBox")
  ID_Patient_LookUpBox: AppLookupBoxComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  private _ID_Client: number = 0;
  private _IDs_Patient: number[] = [];
  private _ID_Patient_Confinement: number = 0;

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {
    apiUrl: "Model/GetList/null",
  };

  CurrentObject: any = {};

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {
    this.load_ID_Patient_LookupBoxOption_DataSource();
  }

  private load_ID_Patient_LookupBoxOption_DataSource() {
    var strIDs_currentIDPatients = Enumerable.fromSource(this._IDs_Patient)
      .toArray()
      .toString();

    if (!strIDs_currentIDPatients) strIDs_currentIDPatients = "0";

    this.ID_Patient_LookupBoxOption.sourceKey = this.cs.encrypt(`
    Select ID, 
          Name_Client OwnerName,
          Name
    FROM vPatient 
    WHERE 
      ID_Company = ${this.currentUser.ID_Company} AND 
      ID_Client IN (${this._ID_Client}) AND 
      ${
        strIDs_currentIDPatients !== "0"
          ? "ID IN (" + strIDs_currentIDPatients + ") AND"
          : ""
      }
      IsActive = 1 AND
      IsActive_Client = 1
  `);
  }

  async Load<T>(ID_Client: number): Promise<T[]> {
    this._ID_Client = ID_Client;
    this._IDs_Patient = [];
    this.load_ID_Patient_LookupBoxOption_DataSource();

    this.loadRecord();
    return this.CustomDialogBox.open();
  }

  async LoadByPatientConfinement<T>(
    ID_Patient_Confinement: number,
    IDs_Patient: number[]
  ): Promise<T[]> {
    var obj = await this.ds.loadObject<any>(
      APP_MODEL.PATIENT_CONFINEMENT,
      ID_Patient_Confinement + "",
      {}
    );

    this._ID_Client = obj.ID_Client;
    this._IDs_Patient = IDs_Patient;
    this._ID_Patient_Confinement = ID_Patient_Confinement;

    this.load_ID_Patient_LookupBoxOption_DataSource();
    this.loadRecord();

    return this.CustomDialogBox.open();
  }

  btn_onClick() {
    if (!this.isEnableToChangeMode) return;

    var IsDeposit = this.IsDeposit != true;
    var creditMode = IsDeposit
      ? CreditModeEnum.Deposit
      : CreditModeEnum.Withdraw;

    this.setCreditMode(creditMode);
  }

  setCreditMode(creditMode: CreditModeEnum) {
    this._creditMode = creditMode;

    if (creditMode == CreditModeEnum.Deposit) {
      this.IsDeposit = true;
    } else if (creditMode == CreditModeEnum.Withdraw) {
      this.IsDeposit = false;
    }

    this.adjustCreditOption = this.IsDeposit ? "Deposit" : "Withdraw";
  }

  async loadRecord(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pGetClient",
        {
          ID: this._ID_Client,
          ID_Session: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
        }
      );

      this.CurrentObject = obj;
      this.CurrentObject.CreditAmount = 0;
      this.CurrentObject.Comment = '';
    });
  }

  async loadPatientConfinentRecord(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pGetClient",
        {
          ID: this._ID_Client,
          ID_Session: this.currentUser.ID_Session,
        },
        {
          isReturnObject: true,
        }
      );

      this.CurrentObject = obj;
      this.CurrentObject.CreditAmount = 0;
    });
  }

  async onPositiveButtonClick(modal: any) {
    var isvalid;

    if (!this.CurrentObject["CreditAmount"])
      this.CurrentObject.CreditAmount = 0;
    if (!this.CurrentObject["Comment"]) this.CurrentObject.Comment = "";

    this.CurrentObject.CreditAmount = Math.abs(this.CurrentObject.CreditAmount);

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    var result = await this.msgBox.confirm(
      `Would you like ${this.adjustCreditOption} Credit?`,
      `Credit`,
      "Yes",
      "No"
    );

    if (!result) return;

    if (this.IsDeposit) {
      await this.depositClientCredit();
    } else {
      await this.withdrawClientCredit();
    }

    this.onSavedCreditAmount.emit();

    modal.close();
  }

  private async validateRecord() {
    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if (typeof this.ID_Patient_LookUpBox.value == "string") {
      this.ID_Patient_LookUpBox.value = this.globalFx.lrtrim(
        this.ID_Patient_LookUpBox.value
      );
    }

    if (this.IsDeposit) {
      if (this.CurrentObject.CreditAmount == 0) {
        validationsAppForm.push({
          message: "Credit Amount is required.",
        });
      }
    }

    if (this.CurrentObject.ID_Patient == null) {
      validationsAppForm.push({
        message: "Pet  is required.",
      });
    }

    if (this.IsDeposit != true) {
      if (this.CurrentObject.CreditAmount < 1) {
        validationsAppForm.push({
          message: "Credit Amount is must be greater than zero.",
        });
      }
    }

    if (this.CurrentObject.Comment.length == 0) {
      validationsAppForm.push({
        message: "Note is required.",
      });
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  async depositClientCredit(): Promise<any> {
    var clientCredits: ClientCredit[] = [];
    var creditAmount: number = this.CurrentObject.CreditAmount;

    creditAmount = this.IsDeposit
      ? this.CurrentObject.CreditAmount
      : 0 - this.CurrentObject.CreditAmount;

    clientCredits.push({
      ID_Client: this._ID_Client,
      Date: moment().format("MM-DD-YYYY HH:mm:ss"),
      CreditAmount: creditAmount,
      Code: `${this.adjustCreditOption}  Credit`,
      Comment: this.CurrentObject.Comment,
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj;
      var ID_ClientDeposit;

      ID_ClientDeposit = await this.saveClientDeposit();
      obj = await this.approveClientDeposit(ID_ClientDeposit);

      if (obj.Success) {
        this.msgBox.success(
          `${this.adjustCreditOption}  successfully.`,
          `Credit`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to ${this.adjustCreditOption}  Credit`
        );
        rej(obj);
      }
    });
  }

  async saveClientDeposit(): Promise<number> {
    var currentObject = {};
    var previousObject = {};

    currentObject = await this.ds.execSP(
      `pGetClientDeposit`,
      {},
      {
        isReturnObject: true,
      }
    );

    previousObject = this.globalFx.cloneObject(currentObject);
    currentObject["ID_Company"] = this.currentUser.ID_Company;
    currentObject["ID_Client"] = this.CurrentObject.ID;
    currentObject["Date"] = new Date();
    currentObject["DepositAmount"] = this.CurrentObject.CreditAmount;
    currentObject["Comment"] = this.CurrentObject.Comment;
    currentObject["ID_Patient"] = this.CurrentObject.ID_Patient;
    currentObject["ID_Patient_Confinement"] = this._ID_Patient_Confinement;

    var r = await this.ds.saveObject(
      APP_MODEL.CLIENTDEPOSIT,
      currentObject,
      previousObject,
      [],
      this.currentUser
    );
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);

    return ID_CurrentObject;
  }

  async approveClientDeposit(ID_ClientDeposit: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveClientDeposit",
        {
          IDs_ClientDeposit: [ID_ClientDeposit],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        res(obj);
      }
    });
  }

  async withdrawClientCredit(): Promise<any> {
    var clientCredits: ClientCredit[] = [];
    var creditAmount: number = this.CurrentObject.CreditAmount;

    creditAmount = this.IsDeposit
      ? this.CurrentObject.CreditAmount
      : 0 - this.CurrentObject.CreditAmount;

    clientCredits.push({
      ID_Client: this._ID_Client,
      Date: moment().format("MM-DD-YYYY HH:mm:ss"),
      CreditAmount: creditAmount,
      Code: `${this.adjustCreditOption}  Credit`,
      Comment: this.CurrentObject.Comment,
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj;
      var ID_ClientWithdraw;

      ID_ClientWithdraw = await this.saveClientWithdraw();
      obj = await this.approveClientWithdraw(ID_ClientWithdraw);

      if (obj.Success) {
        this.msgBox.success(
          `${this.adjustCreditOption}  successfully.`,
          `Credit`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to ${this.adjustCreditOption}  Credit`
        );
        res(obj);
      }
    });
  }

  async saveClientWithdraw(): Promise<number> {
    var currentObject = {};
    var previousObject = {};

    currentObject = await this.ds.execSP(
      `pGetClientWithdraw`,
      {},
      {
        isReturnObject: true,
      }
    );

    previousObject = this.globalFx.cloneObject(currentObject);
    currentObject["ID_Company"] = this.currentUser.ID_Company;
    currentObject["ID_Client"] = this.CurrentObject.ID;
    currentObject["Date"] = new Date();
    currentObject["WithdrawAmount"] = this.CurrentObject.CreditAmount;
    currentObject["Comment"] = this.CurrentObject.Comment;
    currentObject["ID_Patient"] = this.CurrentObject.ID_Patient;
    currentObject["ID_Patient_Confinement"] = this._ID_Patient_Confinement;

    var r = await this.ds.saveObject(
      APP_MODEL.CLIENTWITHDRAW,
      currentObject,
      previousObject,
      [],
      this.currentUser
    );
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);

    return ID_CurrentObject;
  }

  async approveClientWithdraw(ID_ClientWithdraw: number): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveClientWithdraw",
        {
          IDs_ClientWithdraw: [ID_ClientWithdraw],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        res(obj);
      }
    });
  }
}
