import { Input } from '@angular/core';
import { Component,OnInit } from '@angular/core';
import * as moment from 'moment';
import { Model } from 'src/bo/APP_MODELS';
import { FilingStatusEnum } from 'src/utils/controls/class_helper';
import { isNull, isNullOrUndefined } from 'util';

@Component({
  selector: 'app-detail-view-footer',
  templateUrl: './detail-view-footer.component.html',
  styleUrls: ['./detail-view-footer.component.less']
})
export class DetailViewFooterComponent implements OnInit {
      
  footerLeftMessageHtml:String = '';
  footerRightMessageHtml:String = '';

  colorText: string = '';

  constructor() { }

  ngOnInit(): void {

    this.reload();
  }

  @Input() CurrentObject: any = {};
  @Input() model: Model = {};
  @Input() isShowTermsAndCondition: boolean = false;

  reload(currentObject: any = null){

    if(!isNullOrUndefined(currentObject)){

      this.CurrentObject = currentObject;
    }

    this.colorText = this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Cancelled ? 'red' : 'blue';

    if (this.CurrentObject.ID < 1) {
      this.footerLeftMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> New ${isNull(this.model.DisplayName) ? this.model.Name : this.model.DisplayName}</b>`;
      this.footerRightMessageHtml = '';
    } else {
      if (isNullOrUndefined(this.CurrentObject.DateCreated) != true) { 
        this.footerLeftMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> 
            Created on ${moment(this.CurrentObject.DateCreated).format('LLLL')}</b>`;
      }
      if (isNullOrUndefined(this.CurrentObject.DateModified) != true) { 
        this.footerRightMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> 
        Last modified on ${moment(this.CurrentObject.DateModified).format('LLLL')}</b>`;
      } else {
        this.footerRightMessageHtml = '';
      }
    }
  }

  ngAfterViewInit() {
  }

}
