import { AppMultipleSelectBoxComponent } from './controls/appMultipleSelectBox/appMultipleSelectBox.component';
import { AppMultipleListBoxComponent } from './controls/appMultipleListBox/appMultipleListBox.component';
import { AppFormComponent } from './controls/appForm/appForm.component';
import { AppImageBoxComponent } from './controls/appImageBox/appImageBox.component';
import { AppLookupModalComponent, AppLookupModalService } from './controls/appLookupModal/appLookup.component';
import { MessageBoxService, MessageBoxComponent, FormValidationBoxComponent } from './controls/appModal/appModal.component';
import { AppDateBoxComponent } from './controls/appDateBox/appDateBox.component';
import { AppDataTableComponent, cellDisplayPipe } from './controls/appDataTable/appDataTable.component';
import { PanelMenuModule } from 'primeng/panelmenu';
import { OverlayPanelModule } from "primeng/overlaypanel";
import { MenubarModule } from 'primeng/menubar';
import { InputTextModule } from 'primeng/inputtext';
import { DeferModule } from "primeng/defer";
import { CheckboxModule } from "primeng/checkbox";
import { PanelModule } from 'primeng/panel';
import { ListboxModule } from 'primeng/listbox';
import { CalendarModule } from "primeng/calendar";
import { TieredMenuModule } from 'primeng/tieredmenu';
import { MultiSelectModule } from "primeng/multiselect";
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ConfirmationService } from 'primeng/api';
import { FieldsetModule } from 'primeng/fieldset'
import { ToolbarModule } from 'primeng/toolbar';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { BlockUIModule } from 'primeng/blockui';
import { MegaMenuModule } from 'primeng/megamenu';
import { TabViewModule } from "primeng/tabview";
import { TooltipModule } from "primeng/tooltip";
import { ButtonModule } from "primeng/button";
import { PaginatorModule } from "primeng/paginator";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SplitButtonModule } from "primeng/splitbutton";
import { ContextMenuModule } from "primeng/contextmenu";
import { ProgressBarModule } from "primeng/progressbar";
import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Injectable, NgModuleRef, Component } from "@angular/core";
import { CommonModule, CurrencyPipe } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CardModule } from 'primeng/card';
import { HttpClientModule } from "@angular/common/http";
import { NgbModule, NgbDropdownModule, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { FileUploadModule } from 'primeng/fileupload';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import {
  AppTextBoxComponent,
  AppNumberBoxComponent,
  AppSelectBoxComponent,
  AppControlContainerComponent,
  AppPopupComponent,
  AppCheckBoxComponent,
  AppComboBoxComponent,
  AppListBoxComponent,
  AppLookupBoxComponent,
} from "./controls";
import { DataService } from "./service/data.service";
import { PopupService } from './service/popup.service';
import { AppTextAreaComponent } from './controls/appTextArea/appTextArea.component';
import { AppDataGridComponent } from './controls/appDataGrid/appDataGrid.component';
import { AngularResizedEventModule } from 'angular-resize-event';
import { AppPickListBoxComponent } from './controls/appPickListBox/appPickListBox.component';

import { FullCalendarModule } from 'primeng/fullcalendar';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ToastrModule } from 'ngx-toastr';
import { CrypterService } from './service/Crypter.service';
import { AccordionModule } from 'primeng/accordion';
import { AppAutoCompleteBoxComponent } from './controls/appAutoCompleteBox/appAutoCompleteBox.component';
import { CustomDialogBoxComponent } from './custom-dialog-box/custom-dialog-box';
import { LookUpDialogBoxComponent } from './look-up-dialog-box/look-up-dialog-box.component';
import { TableModule } from 'primeng/table';
import { CSSDropdownComponent } from './controls/cssdropdown/cssdropdown.component';
import { ClientCreditAdjustmentDialogBoxComponent } from './client-credit-adjustment/client-credit-adjustment.component';
import { AppDataLookUpBoxComponent } from './controls/appDataLookUpBox/appDataLookUpBox.component';
import { AppQRCodeImageComponent } from './controls/appQRCodeImage/appQRCodeImage.component';
import { LoadingScreenComponentComponent } from './loading-screen-component/loading-screen-component.component';
import { SmallLoadingScreenComponent } from './small-loading-screen/small-loading-screen.component';
import { DetailViewFooterComponent } from './detail-view-footer/detail-view-footer.component';
import { AppRadioButtonListComponent } from 'src/utils/controls/appRadioButtonList/appRadioButtonList.component';

const CONTROLS: any = [
  AppTextBoxComponent,
  AppDataGridComponent,
  AppNumberBoxComponent,
  AppSelectBoxComponent,
  AppControlContainerComponent,
  AppPopupComponent,
  AppTextAreaComponent,
  AppCheckBoxComponent,
  AppComboBoxComponent,
  AppListBoxComponent,
  AppDataTableComponent,
  AppPickListBoxComponent,
  AppLookupBoxComponent,
  AppDateBoxComponent,
  MessageBoxComponent,
  FormValidationBoxComponent,
  AppLookupModalComponent,
  AppImageBoxComponent,
  AppFormComponent,
  AppMultipleListBoxComponent,
  AppMultipleSelectBoxComponent,
  AppAutoCompleteBoxComponent,
  CustomDialogBoxComponent,
  LookUpDialogBoxComponent,
  CSSDropdownComponent,
  ClientCreditAdjustmentDialogBoxComponent,
  AppDataLookUpBoxComponent,
  AppQRCodeImageComponent,
  LoadingScreenComponentComponent,
  SmallLoadingScreenComponent,
  DetailViewFooterComponent,
  AppRadioButtonListComponent,
];

const PrimeNGModules = [
  MenubarModule,
  PanelMenuModule,
  InputTextModule, ButtonModule, TabViewModule, ToolbarModule, TooltipModule,
  SplitButtonModule, ScrollPanelModule, PaginatorModule, ContextMenuModule, ProgressBarModule, CheckboxModule, MultiSelectModule,
  OverlayPanelModule, FieldsetModule, AutoCompleteModule, TieredMenuModule, ListboxModule, FileUploadModule,
  CalendarModule, DeferModule, ConfirmDialogModule, BlockUIModule, TabViewModule, PanelModule, MegaMenuModule, ProgressSpinnerModule,
  FullCalendarModule, BreadcrumbModule, CardModule, AccordionModule, TableModule
]

export let InjectorInstance: Injector;

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgbDropdownModule,
    NgSelectModule,
    NgxMaskModule.forRoot({}),
    DragAndDropModule,
    PrimeNGModules,
    AngularResizedEventModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    LazyLoadImageModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  declarations: [cellDisplayPipe].concat(CONTROLS),
  exports: [].concat(CONTROLS).concat(PrimeNGModules),
  entryComponents: [].concat(CONTROLS),
  providers: [DataService, PopupService, ConfirmationService, MessageBoxService, AppLookupModalService, CrypterService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SharedModule {
  constructor(injector: Injector) {
    InjectorInstance = injector;
  }
}
