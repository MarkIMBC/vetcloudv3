import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { isNullOrUndefined } from "util";
import { IFilterFormValue } from "../controls/appForm/appForm.component";
import { CrypterService } from "src/utils/service/Crypter.service";

@Injectable({
  providedIn: "root",
})
export class GlobalfxService {
  constructor(protected router: Router, protected cs: CrypterService) {}

  _tempID: number = 0;

  findIndexByKeyValue(_array, key, value) {
    for (var i = 0; i < _array.length; i++) {
      if (_array[i][key] == value) {
        return i;
      }
    }

    return -1;
  }

  getDistinctValues(array: any[]): any[] {
    var unique = array.filter((v, i, a) => a.indexOf(v) === i);

    return unique;
  }

  getTempID(): number {
    this._tempID--;
    return this._tempID;
  }

  formatMoney(value: number): string {
    return String(value).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, "$1,");
  }

  roundOffDecimal(value: number, decimalPlaces?: number): number {
    if (isNullOrUndefined(decimalPlaces)) decimalPlaces = 2;
    if (isNullOrUndefined(value)) value = 0;

    var strresult = value.toFixed(decimalPlaces);

    return parseFloat(strresult);
  }

  cloneObject = (inObject) => {
    let outObject, value, key;

    if (typeof inObject !== "object" || inObject === null) {
      return inObject; // Return the value if inObject is not an object
    }

    // Create an array or object to hold the values
    outObject = Array.isArray(inObject) ? [] : {};

    for (key in inObject) {
      value = inObject[key];

      // Recursively (deep) copy for nested objects, including arrays
      outObject[key] = this.cloneObject(value);
    }

    return outObject;
  };

  getParams = function (url) {
    var params = {};
    var parser = document.createElement("a");
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
  };

  convertStringToHTML(text): string {
    var message = "";

    message = text.replace(/\n/g, "<br />");

    return message;
  }

  navigateReport(
    reportName,
    Oid_Report,
    filterFormValue: IFilterFormValue[],
    options?: any
  ) {
    var filterJSON = btoa(JSON.stringify(filterFormValue));
    var optionsJSON = "null";

    if (!isNullOrUndefined(options)) {
      optionsJSON = btoa(JSON.stringify(options));
    }

    this.router.navigate([
      `/Main/rv/${reportName}/${btoa(Oid_Report)}/${filterJSON}/${optionsJSON}`,
    ]);
  }

  getCustomNavigateCommands(routelink: any, config?: any) {
    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    return routelink;
  }

  customNavigate(routelink: any, config?: any) {
    routelink = this.getCustomNavigateCommands(routelink, config);

    this.router.navigate(routelink);
  }

  customserializeUrl(routelink: any, config?: any) {
    routelink = this.getCustomNavigateCommands(routelink, config);

    var url = this.router.serializeUrl(this.router.createUrlTree(routelink));

    return url;
  }

  gotoNoAccessPage() {
    this.router.navigate(["/Main/NoAccess"]);
  }

  generateRamdomCharacters(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
  }
  ltrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/, "");
  }

  rtrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/, "");
  }

  lrtrim(stringToTrim) {
    var value = stringToTrim;

    value = this.ltrim(value);
    value = this.rtrim(value);

    return value;
  }

  removeHistoricalFilter() {
    var localstorageItems: any[] = this.getAllLocalStorageItems();

    localstorageItems.forEach((item: any) => {
      var name = item.name;

      if (name.includes("_ListViewFilter")) {
        localStorage.removeItem(name);
      }
    });
  }

  getAllLocalStorageItems() {
    var archive = [],
      keys = Object.keys(localStorage),
      i = 0,
      key;

    for (; (key = keys[i]); i++) {
      archive.push({
        name: key,
        value: localStorage.getItem(key),
      });
    }

    return archive;
  }
}
