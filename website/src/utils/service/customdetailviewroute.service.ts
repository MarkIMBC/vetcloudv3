import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { isNullOrUndefined } from 'util';
import { CustomDetailView,  } from '../controls/class_helper';
import { CrypterService } from './Crypter.service';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class CustomDetailViewRouteService  implements OnInit  {

  currentUser: TokenSessionFields;

  constructor(    
    private router: Router,
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
    private cs: CrypterService,
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }
  async ngOnInit(): Promise<void> {

    this.load();
  }

  async load(){

    var obj = await this.ds.execSP("pGetAllCustomDetailViewRoute", {
      ID_UserSession: this.currentUser.ID_UserSession
    },
    {
      isReturnObject: true
    });

    this.customDetailViews = obj.CustomDetailViewRoute;

  }

  private customDetailViews: CustomDetailView[] = [];

  private GetCustomDetailView(Oid_DetailView: string): CustomDetailView{

    var result: CustomDetailView;

    result = {

      Oid_DetailView: '',
      RouterLink: '',
    };

    this.customDetailViews.forEach((customDetailView) => {

      if(customDetailView.Oid_DetailView.toLowerCase() == Oid_DetailView.toLowerCase()) result = customDetailView;
    });

    return result;
  }

  IsExist(Oid_DetailView: string): boolean{

    var IsExist = false;

    this.customDetailViews.forEach((customDetailView) => {

      if(customDetailView.Oid_DetailView.toLowerCase() == Oid_DetailView.toLowerCase()){


        //console.log(customDetailView.Oid_DetailView.toLowerCase() ,Oid_DetailView.toLowerCase());
        IsExist = true;
      } 
    });

    return IsExist;
  }

  navigate(Oid_DetailView: string, ID_CurrentObject: string, configOptions?: any){

    var obj: CustomDetailView = new CustomDetailView();
    var routeLink = [];
    var configJSON = '';

    obj = this.GetCustomDetailView(Oid_DetailView);

    if(isNullOrUndefined(configOptions)){

      configOptions = {};
    }
    configOptions['Oid_DetailView'] = Oid_DetailView;
    configJSON = JSON.stringify(configOptions);
    configJSON = this.cs.encrypt(configJSON);

    routeLink = [`/Main`, obj.RouterLink, ID_CurrentObject, configJSON];

    this.router.navigate(routeLink);
  }
}
