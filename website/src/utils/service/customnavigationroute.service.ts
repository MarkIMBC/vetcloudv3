import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { APP_LISTVIEW } from 'src/bo/APP_MODELS';
import { isNullOrUndefined } from 'util';
import { CustomDetailView, CustomNavigationLink,  } from '../controls/class_helper';
import { DataService } from './data.service';


@Injectable({
  providedIn: 'root'
})
export class CustomnavigationrouteService {

  currentUser: TokenSessionFields;
  
  constructor(    
    private router: Router,
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  private customNavigationLinks: CustomNavigationLink[] = [];

  async load(){

    var obj = await this.ds.execSP("pGetAllCustomNavigationLink", {
      ID_UserSession: this.currentUser.ID_UserSession
    },
    {
      isReturnObject: true
    });

    this.customNavigationLinks = obj.CustomNavigationLink;
  }

   GetLink(Oid: string, viewType: ViewTypeEnum): string{

    var result: CustomNavigationLink;
    var link: string;

    result = this.Get(Oid, viewType);

    if(!isNullOrUndefined(result)) link = result.RouterLink;

    return link;
  }

  Get(Oid: string, viewType: ViewTypeEnum): CustomNavigationLink{

    var result: CustomNavigationLink;

    this.customNavigationLinks.forEach((customNavigationLink) => {

      if(isNullOrUndefined(customNavigationLink.Oid_ListView)) customNavigationLink.Oid_ListView = "";
      if(isNullOrUndefined(customNavigationLink.Oid_Report)) customNavigationLink.Oid_Report = "";

      if(customNavigationLink.Oid_ListView.toLowerCase() == Oid.toLowerCase() && customNavigationLink.ID_ViewType == viewType ) {

        result = customNavigationLink;
      } else if(customNavigationLink.Oid_Report.toLowerCase() == Oid.toLowerCase() && customNavigationLink.ID_ViewType == viewType ){
        
        result = customNavigationLink;
      }
    });

    return result;
  }
}