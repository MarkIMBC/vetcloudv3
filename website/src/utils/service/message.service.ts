import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { isNullOrUndefined } from 'util';
import { MessageThread } from '../controls/class_helper';
import { CrypterService } from './Crypter.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private cs: CrypterService,
    private ds: DataService) { }

  async sentMessage(Sender_ID_User, Recipient_ID_User, Message) {

    var promise = new Promise(async (resolve, reject) => {

      var obj = await this.ds.execSP("pSentMessageThread", {
        Sender_ID_User: Sender_ID_User,
        Recipient_ID_User: Recipient_ID_User,
        Message: Message,
      }, {
        isReturnObject: true
      })

      if (isNullOrUndefined(obj.success)){

        reject();
      }else{

        resolve(obj);
      }
    });

    return promise;
  }

  async readMessage(Sender_ID_User, Recipient_ID_User) {

    var promise = new Promise(async (resolve, reject) => {

      var isSuccess = true;

      var obj = await this.ds.execSP("pReadMessageThread", {
        Sender_ID_User : Sender_ID_User,
        Recipient_ID_User : Recipient_ID_User
      },{
        isReturnObject: true,
        isTransaction: true,
      })
  
      if(obj.Success != true) isSuccess = false;
      if(isSuccess){

        if(isNullOrUndefined(obj.MessageThreads)) obj.MessageThreads = [];
      }

      if(isSuccess){

        resolve(obj);
      }else{

        reject();
      }
    });

    return promise;
  }

  async loadMessageThread(Sender_ID_User, Recipient_ID_User): Promise<MessageThread[]> {

    var sql = `SELECT ID,
                      Recipient_ID_User,
                      Sender_ID_User,
                      Message,
                      DateSent,
                      DateRead,
                      IsRead 
              FROM tMessageThread
              WHERE 
                  Sender_ID_User = ${Sender_ID_User} AND
                  Recipient_ID_User = ${Recipient_ID_User}
            `;

    sql = this.cs.encrypt(sql);         

    return await this.ds.query<MessageThread>(sql);
  }
}
