import { Guid } from 'guid-typescript';
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as $ from "jquery";
import { IFile } from '../controls/class_helper';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';
import { CrypterService } from './Crypter.service';
import { IUserSession } from 'src/app/AppServices/UserAuthentication.service';

@Injectable({
  providedIn: "root"
})
export class DataService {

  public appVersion: string;
  public static API_URL: string = 'http://localhost:5000/API/';

  constructor(private http: HttpClient, public crypter: CrypterService) { }

  loadObject<T>(Oid_Model: string, ID_CurrentObject: string, params?: any): Promise<T> {

    if (ID_CurrentObject == undefined) ID_CurrentObject = "-1";
    if (params == undefined || params == null) params = {};
    params.ID_CurrentObject = ID_CurrentObject;
    return this.post<T>(`Model/LoadObject/${ID_CurrentObject}`, {
      Oid_Model: Oid_Model,
      Params: params ? JSON.stringify(params) : null
    });
  };

  query<T>(sql: string): Promise<T[]> {
    return this.GetPagingList<T[]>('Model/Query', {
      SQL: sql
    });
  };

  execSP(spName: string, params?: any, options?: any): Promise<any> {

    var isReturnObject: boolean = false;

    if (isNullOrUndefined(options) != true) {

      if (isNullOrUndefined(options) == true) {

        isReturnObject = false;
      } else {

        isReturnObject = options.isReturnObject;
      }
    }

    var isTransaction: boolean = false;

    if (isNullOrUndefined(options) != true) {

      if (isNullOrUndefined(options) == true) {

        isTransaction = false;
      } else {

        isTransaction = options.isTransaction;
      }
    }

    var args = isNullOrUndefined(params) != true ? JSON.stringify(params) : null;
    args = this.crypter.encrypt(args);

    return this.post(`Model/ExecuteSP2`, {
      args: args,
      isReturnObject: isReturnObject,
      isTransaction: isTransaction,
      spName: this.crypter.encrypt(spName)
    });
  }

  sendtext(CurrentObject: any): Promise<any> {

    var form = new FormData();

    var strdata = JSON.stringify({

      CellPhoneNumbers: CurrentObject.CellPhoneNumbers,
      Message: CurrentObject.Message
    });

    form.append("data", JSON.stringify(strdata));

    return this.post(`Model/SendText`, form);
  }

  private async getModelDateColumns(Oid_Model): Promise<any> {

    var obj = await this.execSP('pGetModelDateColumns', {
      'Oid_Model': Oid_Model
    }, {
      isReturnObject: true
    })

    return obj;
  }

  private async formatObjectDateValue(Oid_Model, CurrentObject, TempObject) {

    var obj = await this.getModelDateColumns(Oid_Model);

    var propertyList = [];
    var dateColumns = [];

    obj.PropertyList.forEach((Obj) => {

      propertyList.push(Obj.ParentName);
    });


    obj.DateColumns.forEach((Obj) => {

      dateColumns.push(Obj.DateColumnName);
    });

    var convertDateToString = (Obj) => {

      for (const property in Obj) {

        var hasOnPRopertyList = propertyList.includes(property);

        if (hasOnPRopertyList == true) {

          if (Obj[property] == null) Obj[property] = [];

          Obj[property].forEach((detail) => {

            convertDateToString(detail);
          })

        } else {

          if (dateColumns.includes(property)) {

            if(Obj[property] != null){

              var _val = moment(Obj[property]).format("YYYY-MM-DD HH:mm:ss");
              Obj[property] = _val;
            }
          }
        }

      }
    }

    convertDateToString(CurrentObject);
    convertDateToString(TempObject);

    //console.log(obj);
  }

  async saveObject(Oid_Model, CurrentObject: any, TempObject?: any, files?: IFile[], currentUser?: IUserSession): Promise<any> {

    var form = new FormData();

    var _CurrentObject = Object.assign({}, CurrentObject);
    var _TempObject = Object.assign({}, TempObject);
    var FileDetails: any[] = [];

    await this.formatObjectDateValue(Oid_Model, _CurrentObject, _TempObject);

    if (files) {
      files.forEach((f, i: number) => {
        var fileId = Guid.create().toString();
        var fileName = f.file.name;
        form.append("files", f.file, `${fileId}`);
        FileDetails.push({
          FileName: fileName,
          DataField: f.dataField,
          FileId: fileId,
          IsImage: f.isImage
        });
      });
    };

    var obj = {

      CurrentObject: JSON.stringify(_CurrentObject),
      PreviousObject: _TempObject ? JSON.stringify(_TempObject) : JSON.stringify({}),
      UploadFileDetails: JSON.stringify(FileDetails),
      CurrentUser: JSON.stringify(currentUser)
    }

    form.append("param", this.crypter.encrypt(JSON.stringify(obj)));

    return this.post(`Model/Save2/${Oid_Model}`, form);
  }

  async UploadFiles(Oid_Model, files?: IFile[]): Promise<any> {

    var form = new FormData();
    var FileDetails: any[] = [];

    var _CurrentObject = Object.assign({}, { ID: 1 });
    var _TempObject = Object.assign({}, { ID: 1 });

    await this.formatObjectDateValue(Oid_Model, _CurrentObject, _TempObject);

    form.append("CurrentObject", JSON.stringify(_CurrentObject));
    form.append("PreviousObject", _TempObject ? JSON.stringify(_TempObject) : undefined);

    if (files) {
      files.forEach((f, i: number) => {
        var fileId = Guid.create().toString();
        var fileName = f.file.name;
        form.append("files", f.file, `${fileId}`);
        FileDetails.push({
          FileName: fileName,
          DataField: f.dataField,
          FileId: fileId,
          IsImage: f.isImage
        });
      });
    };
    form.append("UploadFileDetails", JSON.stringify(FileDetails));
    return this.post(`Model/UploadFiles/${Oid_Model}`, form);
  }

  GetPagingList<T>(url: string, opt: IPagingOption): Promise<any> {
    return this.post<T>(url, opt);
  }

  loadConfig(): Promise<any> {
    const promise = this.http
      .get("/assets/app.config.json?q=" + new Date().getTime())
      .toPromise()
      .then((data: any) => {
        DataService.API_URL = data.apiUrl;
        return data;
      });
    return promise;
  }

  async post<T>(url, opt): Promise<T> {

    var _ = this;
    var result;

    var _url = DataService.API_URL + url;
    result = await this.http.post<T>(_url, opt).toPromise<T>();

    return result;
  }

  async get<T>(url): Promise<T> {

    const headers = new HttpHeaders()
      .set('Cache-Control', 'no-cache')
      .set('Pragma', 'no-cache');

    var _ = this;
    var result;
    try {

      var _url = DataService.API_URL + url;
      result = await this.http.get<T>(_url, { headers }).toPromise<T>();

    } catch (error) {

      var fxTimeOut = await setTimeout(async () => {

        console.log('reloading configration');

        clearTimeout(fxTimeOut);
        result = await _.get(url);
      }, 3000)

    }

    return result;

  }

}

export class IPagingOption {
  Skip?: number = 0;
  Take?: number = 20;//by default
  PrimaryKey?: string = "ID";
  SQL?: string = "";
  Value?: string = null;
  FilterOption?: string = null;
  SummaryOption?: string = null;
  SortingOption?: string = null;
  GroupOptions?: string = null;
  SearchValue?: string = null;
  ActiveColumns?: string[] = null;
  filterFormValue?: string = null;
}

export class ISaveObjectParam {
  CurrentObject: any;
  PreviousObject: any;
}