import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { ClientCredit } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'qrcode-scan-redirect-record-dialog',
  templateUrl: './qrcode-scan-redirect-record.component.html',
  styleUrls: ['./qrcode-scan-redirect-record.component.less']
})
export class QRCodeScanRedirectRecordComponent implements OnInit {

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  adjustCreditOption: string = 'Deposit';
  IsDeposit: boolean = true;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  private _ID_Client: number = 0;
  private _ID_Patient_Confinement: number = 0;

  CurrentObject: any = {}

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {

  }

  async show<T>(): Promise<T[]> {

    return new Promise<any[]>(async (res, rej) => {

      await this.CustomDialogBox.open();

      res(this.CurrentObject);
    });
  }

  async onPositiveButtonClick(modal: any) {

    modal.close()
  }

  async onNegativeButtonClick(modal: any) {

    modal.close()
  }

  public scanSuccessHandler($event: any) {

    var value = JSON.parse($event);

    this.CurrentObject = value;

    this.CustomDialogBox.close();
  }

  public enableScanner() {

  }

}

