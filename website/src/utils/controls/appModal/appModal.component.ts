import { Component, OnInit, Injectable } from "@angular/core";
import {
  NgbActiveModal,
  NgbModal,
  NgbModalOptions,
} from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-modal",
  template: `
    <div class="modal-header">
      <h4 class="modal-title">{{ title }}</h4>
      <button
        type="button"
        class="close"
        aria-label="Close"
        *ngIf="showPositiveButton == false"
        (click)="onNegativeButton_Click()"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{ message }}</p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        [style.min-width.px]="80"
        class="btn btn-primary btn-xs"
        *ngIf="showPositiveButton"
        (click)="OnPositiveButton_Click()"
      >
        {{ postiveTextButton }}
      </button>
      <button
        type="button"
        [style.min-width.px]="80"
        ngbAutofocus
        class="btn btn-secondary btn-xs"
        (click)="onNegativeButton_Click()"
      >
        {{ negativeTextButton }}
      </button>
    </div>
  `,
  styleUrls: ["./appModal.component.less"],
})
export class MessageBoxComponent implements OnInit {
  title: string = "Application";
  postiveTextButton: string = "Ok";
  negativeTextButton: string = "Cancel";
  message: string = null;
  showPositiveButton: boolean = true;

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {}

  OnPositiveButton_Click(): void {
    this.activeModal.close(true);
  }

  onNegativeButton_Click(): void {
    setTimeout(() => {
      this.activeModal.close(false);
    }, 500);
  }
}

@Component({
  selector: "validation-modal",
  template: `
    <div class="modal-header materialize-red darken-4">
      <h4 class="modal-title" style="color:#fff">
        <span class="mdi mdi-alert"></span> {{ title }}
      </h4>
      <button
        type="button"
        class="close"
        aria-label="Close"
        *ngIf="showPositiveButton == false"
        (click)="onNegativeButton_Click()"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div>
        <table>
          <tbody>
            <tr>
              <td></td>
              <td>
                <div>
                  <ul
                    class="validation-list"
                    *ngIf="formValidations.length > 1"
                  >
                    <li *ngFor="let item of formValidations">
                      {{ item.message }}
                    </li>
                  </ul>
                  <div *ngIf="formValidations.length == 1">
                    {{ formValidations[0].message }}
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        [style.min-width.px]="80"
        class="btn btn-secondary btn-xs"
        *ngIf="showPositiveButton"
        (click)="OnPositiveButton_Click()"
      >
        {{ postiveTextButton }}
      </button>
    </div>
  `,
  styleUrls: ["./appModal.component.less"],
})
export class FormValidationBoxComponent implements OnInit {
  title: string = "Validation";
  postiveTextButton: string = "Close";
  negativeTextButton: string = "Cancel";
  showPositiveButton: boolean = true;

  formValidations: IFormValidation[] = [];

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {}

  OnPositiveButton_Click(): void {
    this.activeModal.close(true);
  }

  onNegativeButton_Click(): void {
    this.activeModal.close(false);
  }
}

@Injectable({
  providedIn: "root",
})
export class MessageBoxService {
  constructor(private modalService: NgbModal, private toastr: ToastrService) {
    this.toastr.toastrConfig.enableHtml = true;
  }

  private option: NgbModalOptions = {
    centered: true,
    backdrop: true,
  };

  setToastTimeOut(timeoutNum: number) {
    this.toastr.toastrConfig.timeOut = timeoutNum;
  }
  setShowCloseButton(isShow: boolean) {
    this.toastr.toastrConfig.closeButton = isShow;
  }

  async show(message: string, title?: string): Promise<any> {
    this.option.windowClass = "msg-box";
    this.option.keyboard = true;
    var modal = this.modalService.open(MessageBoxComponent, this.option);
    var cmp = modal.componentInstance as MessageBoxComponent;
    cmp.message = message;
    cmp.negativeTextButton = "Close";
    cmp.showPositiveButton = false;
    if (title) cmp.title = title;
    return await modal.result;
  }

  async confirm(
    message: string,
    title?: string,
    positiveButtonText?: string,
    negativeButtonText?: string
  ): Promise<boolean> {

    this.option.windowClass = "msg-box";
    this.option.keyboard = true;
    this.option.backdrop = true;
    var modal = this.modalService.open(MessageBoxComponent, this.option);
    var cmp = modal.componentInstance as MessageBoxComponent;

    cmp.message = message;
    cmp.postiveTextButton = "Yes";
    cmp.negativeTextButton = "No";
    if (positiveButtonText) cmp.postiveTextButton = positiveButtonText;
    if (negativeButtonText) cmp.negativeTextButton = negativeButtonText;
    if (title) cmp.title = title;
    return await modal.result;
  }

  async showConfirmBox(options: any): Promise<boolean> {
    var backdrop = true;

    if (options.backdrop != undefined) {
      backdrop = options["backdrop"];
    }

    this.option.keyboard = false;
    this.option.backdrop = backdrop;
    this.option.windowClass = "msg-box";

    var modal = this.modalService.open(MessageBoxComponent, this.option);
    var cmp = modal.componentInstance as MessageBoxComponent;

    cmp.message = options.message;
    cmp.postiveTextButton = "Yes";
    cmp.negativeTextButton = "No";
    if (options.positiveButtonText)
      cmp.postiveTextButton = options.positiveButtonText;
    if (options.negativeButtonText)
      cmp.negativeTextButton = options.negativeButtonText;
    if (options.title) cmp.title = options.title;
    return await modal.result;
  }

  success(message: string, title?: string): void {
    this.toastr.success(message, title);
  }

  warning(message: string, title?: string): void {
    this.toastr.warning(message, title);
  }

  error(message: string, title?: string): void {
    this.toastr.error(message, title);
  }

  showValidationsBox(formValidations: IFormValidation[]): Promise<boolean> {
    this.option.windowClass = "form-validation-box";
    this.option.size = "lg";
    var modal = this.modalService.open(FormValidationBoxComponent, this.option);
    var cmp = modal.componentInstance as FormValidationBoxComponent;
    cmp.formValidations = formValidations;
    cmp.title =
      formValidations.length > 1
        ? `Validations (${formValidations.length})`
        : "Validation";
    return modal.result;
  }

  showValidationBox(message: string): Promise<boolean> {
    return this.showValidationsBox([
      {
        message: message,
      },
    ]);
  }
}

export interface IFormValidation {
  message: string;
}
