import { IMenuItem } from "./../class_helper";
import { FilterCriteriaType } from "./../appControlContainer/appControlContainer.component";
import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  ChangeDetectorRef,
  EventEmitter,
  Output,
} from "@angular/core";
import { BaseAppControl, BaseAppControlOption } from "../_base/baseAppControl";
import { AppControlContainerComponent } from "..";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import * as $ from "jquery";
import { debug } from "console";

@Component({
  selector: "app-radio-button-list",
  templateUrl: "./appRadioButtonList.component.html",
  styleUrls: ["./appRadioButtonList.component.less"],
  providers: [
    { provide: BaseAppControl, useExisting: AppRadioButtonListComponent },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppRadioButtonListComponent,
      multi: true,
    },
  ],
})
export class AppRadioButtonListComponent extends BaseAppControl<string> {
  @Input()
  option: BaseAppControlOption = null;

  @Input()
  titleTooltip: string = "";

  @Input()
  listDataSource: AppRadioButtonListDataSourceItem[] = [];

  @ViewChild("appContainer", { static: false })
  _appContainer: AppControlContainerComponent;

  @Output() onValueChanged = new EventEmitter<any>();

  constructor(el: ElementRef, ch: ChangeDetectorRef) {
    super(el, ch);
  }

  focus(): void {
    $(this.input.nativeElement).focus();
  }

  onTextBox_ValueChange(_value): void {
    this.emitValueChange(_value);
    this.onControl_ValueChanged();

    this.onValueChanged.emit(_value);
  }

  isInvalid(): boolean {
    var required = this.isRequired();
    return (
      this.isDirty == true && required == true && this.isEmptyString() == true
    );
  }
}

export class AppRadioButtonListDataSourceItem {
  value: any = 0;
  label: string = "";
}
