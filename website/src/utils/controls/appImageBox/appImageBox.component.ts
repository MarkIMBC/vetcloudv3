import { DataService } from 'src/utils/service/data.service';
import { ViewChild, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseAppControl, BaseAppControlOption } from '..';
import { FileUpload } from 'primeng/fileupload/fileupload';

@Component({
  selector: 'app-image-box',
  templateUrl: './appImageBox.component.html',
  styleUrls: ['./appImageBox.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppImageBoxComponent,
      multi: true
    },
    {
      provide: BaseAppControl,
      useExisting: AppImageBoxComponent,
    }

  ]
})
export class AppImageBoxComponent extends BaseAppControl<string> {

  constructor(el: ElementRef, ch :ChangeDetectorRef) {
    super(el, ch);
  }

  imgUrl:string = null;

  @ViewChild('fileUpload')
  fileUpload: FileUpload

  @Output() onImage_Click = new EventEmitter<any>();

  imageFile: File = null;

  writeValue(value: string): void {
    if ( value ) {
      this.imgUrl = DataService.API_URL.replace("/api/","/") + `Content/Thumbnail/${value}`;
    };
    if ( this.fileUpload ) {
      this.fileUpload.clear();
    }
    //http://localhost:5000/Content/Thumbnail/81c3dc5c41b9e0bde2089d3a1cd2ab77_20200501001424.jpg
    this.value = value;
  }

  onFileUploader_Select(event: any): void { 
    //console.log("FileUpload Select", event);
    this.imageFile = event.currentFiles[0];
    this.emitValueChange(this.imageFile.name);
    this.onControl_ValueChanged();
  }


  onFileUploader_Remove(event: any): void {
    //console.log("FileUpload Remove", event);
    this.imageFile = null;
    this.emitValueChange(null);
    this.onControl_ValueChanged();
  }

  _onImageClick(event: any){

    var url = DataService.API_URL.replace("/api/","/") + `Content/Image/${this.value}`;
    var win = window.open(url, '_blank');
    win.focus();
    
    this.onImage_Click.emit(event);
  }
}


export class IAppImageBoxOption extends BaseAppControlOption {

}