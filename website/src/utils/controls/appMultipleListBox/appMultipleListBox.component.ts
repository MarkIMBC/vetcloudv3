import { Component, OnInit, ElementRef, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { DataService } from 'src/utils/service/data.service';
import * as TS from 'linq-typescript';
import { IMenuItem } from '../class_helper';
import { FilterCriteriaType } from '../appControlContainer/appControlContainer.component';

@Component({
  selector: 'app-multiplelist-box',
  templateUrl: './appMultipleListBox.component.html',
  styleUrls: ['./appMultipleListBox.component.less'],
  providers: [
    {
      provide: BaseAppControl,
      useExisting: AppMultipleListBoxComponent,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppMultipleListBoxComponent,
      multi: true
    }
  ]
})
export class AppMultipleListBoxComponent extends BaseAppControl<number> {

  constructor(el: ElementRef, private ds: DataService, private changeDetector: ChangeDetectorRef) {
    super(el, changeDetector);
  }

  filterCriteriaItems: IMenuItem[] = [
    // {
    //   id: FilterCriteriaType.Equal,
    //   label: 'Equals'
    // },
    // {
    //   id: FilterCriteriaType.NotEqual,
    //   label: 'Not Equals'
    // },
    {
      id: FilterCriteriaType.Contains,
      label: 'Contains'
    },
    {
      id: FilterCriteriaType.NotContains,
      label: 'Not Contains'
    },
    // {
    //   id: FilterCriteriaType.StartWith,
    //   label: 'Start With'
    // },
    // {
    //   id: FilterCriteriaType.EndWith,
    //   label: 'End With'
    // }
  ];

  selecteds: any[] = [];
  displayKey?: string = "Name";
  propertyKey?: string = "ID";
  //sourceKey?:string = null;
  items: any[] = [];
  showToggleAll?: boolean = true;
  multiple?: boolean = true;
  checkbox?: boolean = true;
  sourceKey?: string = null;

  ngOnInit() {
  }

  async writeValue(val: any[]) {
    if (val) {
      if ( this.items.length == 0 ) {
        this.items = await this.getListSource();
      }
      val = new TS.List(this.items).where(d => val.includes(d => d[this.propertyKey] == true)).toArray();
    } else {
      val = [];
    }
    this.selecteds = val;
  }

  async ngOnChanges(ch: SimpleChanges): Promise<void> {
    super.ngOnChanges(ch);
    if (ch.option) {
      var option = ch.option.currentValue as IAppMultipleListBoxOption;
      if (option == undefined) return;
      var items = option.items;
      //console.log(this.name, items);
      this.propertyKey = option.propertyKey ? this.propertyKey : "ID";
      this.displayKey = option.displayKey ? this.displayKey : "Name";
      if (items) {
        //console.log(items);
        setTimeout(() => {
        this.items = items;
        }, 500);
        return;
      }
      if (this.items.length == 0) {
        if (option.sourceKey) {
          this.sourceKey = option.sourceKey;
          var list = await this.ds.query(option.sourceKey);
          this.items = list;
        };
      }
    }
  }

  isInvalid(): boolean { 
    var required = this.isRequired();
    return this.isDirty == true && required == true && !(this.selecteds?.length > 0);
  }

  onNgModelValue_Changed(evt:any[]): void {
    if ( evt?.length > 0 ) {
      var values = new TS.List(evt).select(d=>d[this.propertyKey]).toArray();
      this.emitValueChange(values);
    } else {
      this.emitValueChange(evt);
    }
    this.onControl_ValueChanged();
  }

  private async getListSource(): Promise<any[]> { 
    var list = await  this.ds.query(this.sourceKey); 
    return list;
  }

}



export class IAppMultipleListBoxOption extends BaseAppControlOption {
  items?: any[] = [];
  propertyKey?: string = "ID";
  displayKey?: string = "Name";
  sourceKey?: string = null;
}
