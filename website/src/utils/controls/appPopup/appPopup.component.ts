import {
  Component,
  OnInit,
  ElementRef,
  Injectable,
  Input,
  ViewChild
} from "@angular/core";
import { Guid } from "guid-typescript";
import * as $ from "jquery";
import { DataService } from "src/utils/service/data.service";
import { PopupService } from "src/utils/service/popup.service";

@Component({
  selector: "app-popup",
  templateUrl: "./appPopup.component.html",
  styleUrls: ["./appPopup.component.less"]
})
export class AppPopupComponent implements OnInit {
  id: string;
  private element: any;
  _option: IPopupOption = null;

  @ViewChild('content')
  content: ElementRef;

  constructor(
    private PopupService: PopupService,
    private dataService: DataService,
    private el: ElementRef
  ) {
    var _ = this;
    this.element = el.nativeElement;
    $(this.element).addClass("popup");
    $(this.element).click(($evt) => {
      if ($evt.target.id === _.id) {
        _.close();
      }
    });
  }

  @Input('anchor')
  anchor: ElementRef = null;

  @Input()
  set option(option: IPopupOption) {
    this._option = option;
  }

  get option() {
    return this._option;
  }

  ngOnInit(): void {
    let Popup = this;
    this.id = Guid.create().toString();
    this.element.addEventListener("click", function (e: any) {
      if (e.target.className === "popup") {
        Popup.close();
      }
    });
    $(this.element).attr("id", this.id);
    this.PopupService.add(this);
    this.PopupService.popupContainer.append(this.element);
  }

  ngOnDestroy(): void {
    this.PopupService.remove(this.id);
    this.element.remove();
  }



  open(): void {
    let topOffSet: number = 20;
    var _css: any = {};
    if (this.anchor) {
      var pos = this.getRectPosition();
      _css.top = pos.top + pos.elemHeight + topOffSet;
      _css.left = pos.left;
    }

    $(this.PopupService.popupContainer).css({
      "display": 'block'
    });
    $(this.content.nativeElement).css(_css);
    $(this.element).toggleClass('open');
  }

  close(): void {
    $(this.element).removeClass('open');
    var popups = $(this.PopupService.popupContainer).find('.popup.open');
    //console.log(popups);
    if (popups.length == 0) {
      $(this.PopupService.popupContainer).css({
        "display": 'none'
      });
    }
  }

  ngAfterViewInit(): void {
    this._initOption();
  }

  _initOption(): void {
    if (this.option == null) this.option = new IPopupOption();

  }

  getRectPosition(): IRectPosition {
    // var leftPos  = $(this.anchor)[0].getBoundingClientRect().left   + $(window)['scrollLeft']();
    // var rightPos = $(this.anchor)[0].getBoundingClientRect().right  + $(window)['scrollLeft']();
    // var topPos   = $(this.anchor)[0].getBoundingClientRect().top    + $(window)['scrollTop']();
    // var bottomPos = $(this.anchor)[0].getBoundingClientRect().bottom + $(window)['scrollTop']();
    // return {
    //   left: leftPos,
    //   right: rightPos,
    //   top: topPos,
    //   bottom: bottomPos,
    //   elemHeight : $(this.anchor).height(),
    //   elemWidth  : $(this.anchor).width(),
    // }
    return null;
  }

}

export class IPopupOption {
  width?: number = null;
  height?: number = null;
}

export interface IRectPosition {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
  elemWidth?: number;
  elemHeight?: number;
}
