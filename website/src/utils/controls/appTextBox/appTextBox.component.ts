import { IMenuItem } from './../class_helper';
import { FilterCriteriaType } from './../appControlContainer/appControlContainer.component';
import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { AppControlContainerComponent } from '..';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as $ from 'jquery';

@Component({
  selector: "app-text-box",
  templateUrl: "./appTextBox.component.html",
  styleUrls: ["./appTextBox.component.less"],
  providers: [
    { provide: BaseAppControl, useExisting: AppTextBoxComponent },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppTextBoxComponent,
      multi: true
    }
  ]
})
export class AppTextBoxComponent extends BaseAppControl<string> {

  @Input()
  option: BaseAppControlOption = null;

  @Input()
  titleTooltip: string = '';

  @ViewChild("appContainer", { static: false })
  _appContainer: AppControlContainerComponent;

  @Output() onValueChanged = new EventEmitter<any>();

  filterCriteriaItems: IMenuItem[] = [
    {
      id: FilterCriteriaType.Like,
      label: 'Like'
    },
    {
      id: FilterCriteriaType.NotLike,
      label: 'Not Like'
    },
    {
      id: FilterCriteriaType.StartWith,
      label: 'Start With'
    },
    {
      id: FilterCriteriaType.EndWith,
      label: 'End With'
    },
    {
      id: FilterCriteriaType.Equal,
      label: 'Equals'
    },
    {
      id: FilterCriteriaType.NotEqual,
      label: 'Not Equals'
    },
  ];

  constructor(el: ElementRef, ch: ChangeDetectorRef) {
    super(el, ch);
  }

  focus() : void {
      $(this.input.nativeElement).focus();
  }

  onTextBox_ValueChange(_value): void {

    this.emitValueChange(_value);
    this.onControl_ValueChanged();

    this.onValueChanged.emit(_value);
  }

  isInvalid() : boolean {
    var required = this.isRequired();
    return this.isDirty == true && required == true && this.isEmptyString() == true;
  }

}

export class IAppTextBoxOption extends BaseAppControlOption {
  autoCompleteApiUri?: string;
}

