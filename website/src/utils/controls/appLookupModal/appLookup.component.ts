import { MenuItem } from "primeng/api/menuitem";
import {
  AppDataTableComponent,
  DataTableColumn,
  DataTableOption,
} from "./../appDataTable/appDataTable.component";

import {
  Component,
  OnInit,
  Injectable,
  ViewChild,
  EventEmitter,
} from "@angular/core";
import {
  NgbActiveModal,
  NgbModal,
  NgbModalOptions,
} from "@ng-bootstrap/ng-bootstrap";
import { ICellEvent } from "../class_helper";

@Component({
  selector: "app-modal",
  template: `
    <div class="modal-header">
      <div style="width:100%">
        <p-toolbar>
          <div class="ui-toolbar-group-left">
            <div class="title">
              {{ title }}
            </div>
          </div>
          <div class="ui-toolbar-group-right">
          </div>
        </p-toolbar>
      </div>
    </div>
    <div class="modal-body" [style.height.px]="500">
      <div class="d-flex flex-column expand">
        <p-menubar [model]="menuItems" [hidden]="!(menuItems.length > 0)">
        </p-menubar>
        <div class="d-flex flex-fill">
          <div class="expand">
            <app-data-table #grid [option]="gridOption"></app-data-table>
          </div>
        </div>
      </div>
    </div>
    <div
      class="modal-footer"
      *ngIf="postiveTextButton !== null || negativeTextButton !== null"
    >
      <button
        type="button"
        [style.min-width.px]="80"
        class="btn btn-primary btn-xs"
        *ngIf="postiveTextButton !== null"
        (click)="OnPositiveButton_Click()"
      >
        {{ postiveTextButton }}
      </button>
      <button
        type="button"
        [style.min-width.px]="80"
        ngbAutofocus
        class="btn btn-light btn-xs"
        *ngIf="negativeTextButton !== null"
        (click)="onNegativeButton_Click()"
      >
        {{ negativeTextButton }}
      </button>
    </div>
  `,
  styleUrls: ["./appLookup.component.less"],
})
export class AppLookupModalComponent implements OnInit {
  title: string = "Select...";
  postiveTextButton: string = null;
  negativeTextButton: string = null;
  message: string = null;

  menuItems: MenuItem[] = [
    {
      label: "Refresh",
      icon: "green-text pi pi-fw pi-refresh",
    },
  ];

  gridOption: DataTableOption;

  @ViewChild("grid")
  dataTable: AppDataTableComponent;

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {}

  OnPositiveButton_Click(): void {
    this.activeModal.close(true);
  }

  onNegativeButton_Click(): void {
    this.activeModal.close(false);
  }

  ngAfterViewInit(): void {
    //this.dataTable.focus();
    var opt = this.gridOption;
    //console.log(opt);
    opt.onCell_DoubleClick.subscribe((evt: ICellEvent) => {
      this.activeModal.close(evt);
    });
    opt.onCell_EnterKey.subscribe((evt: ICellEvent) => {
      this.activeModal.close(evt);
    });
  }
}

@Injectable({
  providedIn: "root",
})
export class AppLookupModalService {
  constructor(private modalService: NgbModal) {}

  private option: NgbModalOptions = {
    centered: true,
    backdrop: true,
  };

  show(opt: IAppLookUpOption): Promise<ICellEvent> {
    return new Promise<any>((res, rej) => {
      var ngbOpt: NgbModalOptions = {
        centered: true,
        windowClass: "app-lookup-modal",
      };
      var refMOdal = this.modalService.open(AppLookupModalComponent, ngbOpt);
      var cmp = refMOdal.componentInstance as AppLookupModalComponent;
      opt.showFilter = true;
      opt.onCell_DoubleClick = new EventEmitter<ICellEvent>(true);
      opt.onCell_EnterKey = new EventEmitter<ICellEvent>(true);
      if (opt.title) cmp.title = opt.title;
      cmp.gridOption = opt;
      refMOdal.result.then(
        (key: ICellEvent) => {
          res(key);
        },
        () => {
          res(null);
        }
      );
    });
  }
}

export class IAppLookUpOption extends DataTableOption {
  title?: string;
}
