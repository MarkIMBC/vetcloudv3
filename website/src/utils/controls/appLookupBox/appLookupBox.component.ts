import { DataService } from 'src/utils/service/data.service';
import { AppLookupModalService, IAppLookUpOption } from './../appLookupModal/appLookup.component';
import { ViewChild, ChangeDetectorRef, EventEmitter, Output, Input } from '@angular/core';
import { BaseAppControl } from '../_base/baseAppControl';
import { ElementRef } from '@angular/core';
import { Component } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as $ from 'jquery';
import { ICellEvent } from '../class_helper';
import { isNull } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-lookup-box',
  templateUrl: './appLookupBox.component.html',
  styleUrls: ['./appLookupBox.component.less'],
  providers: [
    AppLookupModalService,
    {
      provide: BaseAppControl,
      useExisting: AppLookupBoxComponent
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppLookupBoxComponent,
      multi: true
    }
  ]
})
export class AppLookupBoxComponent extends BaseAppControl<any> {

  constructor(el: ElementRef, private appLookup: AppLookupModalService, private ds: DataService, ch :ChangeDetectorRef) {
    super(el, ch);
    $(el.nativeElement).keydown(($event) => {
      var keyCode = $event.keyCode;
      if (keyCode == 13) {
        if (this.itemSelected !== null) return;
        this.show();
      }
    })
  }
  itemSelected: any = null;
  propertyKey?: string = "ID";
  displayKey?: string = "Name";

  @ViewChild('displayInput')
  displayInput: ElementRef;

  @Output() onSelectedItem = new EventEmitter<any>();
  @Output() onRemoveSelectedItem = new EventEmitter<any>();
  @Input() readOnly: boolean = false;
  @Input() showClearButton: boolean = false;
  @Input() titleTooltip: string = "";


  ngOnInit() { }

  async show(): Promise<void> {

    if(this.readOnly) return;
    
    var opt = this.option as IAppLookupBoxOption;
    if  ( this.caption ) {
      opt.title = "Select " + this.caption;
    }

    var evt: ICellEvent = await this.appLookup.show(opt);
    this.onItem_Selected(evt)
  }

  getDisplayText(): string {
    if (this.itemSelected == null) return "";
    return this.itemSelected[this.displayKey];
  }

  ngAfterViewInit(): void {

  };

  onItem_Selected(cell: ICellEvent): void {

    setTimeout(() => {
      $(this.displayInput.nativeElement).focus();
    }, 100);

    if(cell == null) return;

    this.itemSelected = cell.key;
    this.value = this.itemSelected[this.propertyKey];
    this.emitValueChange(this.value);
    this.onControl_ValueChanged();
    this.isDirty = true;

    this.onSelectedItem.emit(this.itemSelected);
  };

  onKeyDown(evt: KeyboardEvent): void {
    super.onKeyDown(evt);
    if (evt.keyCode == 8) { //Backspace
      this.clearItemSelected();
      return;
    }
  }

  clearItemSelected(): void {
    this.itemSelected = null;
    this.emitValueChange(null);
    this.onControl_ValueChanged();
    this.value = null;
    this.isDirty = true;

    this.onRemoveSelectedItem.emit();
    setTimeout(() => {
      this.focus();
    }, 100);
  }

  async writeValue(val: any) {
    var itemSelected = null;
    if (val) {
      itemSelected = await this.loadObjectValue(val);
    }
    //console.log(itemSelected);
    this.itemSelected = itemSelected;
    this.value = val;
  }

  async loadObjectValue(val: string): Promise<any> {
    var opt = this.option as IAppLookupBoxOption;
    // if (opt.Oid_ListView == null || opt.Oid_ListView == undefined) opt.Oid_ListView = "xxxxxxx"
    var oidListView = opt.Oid_ListView ? opt.Oid_ListView : "-";
    var apiUrl = `Model/GetList/${oidListView}`;
    var result = await this.ds.GetPagingList(apiUrl, {
      SQL: opt.sourceKey,
      Value: val + "",
    });
    return (result.Data as any[])[0];
  }

}

export class IAppLookupBoxOption extends IAppLookUpOption { }
