import { KeyCode } from '@ng-select/ng-select/lib/ng-select.types';
import { DataGridColumn } from './../appDataGrid/appDataGrid.component';
import { OverlayPanel } from 'primeng/overlaypanel';
import { ListView, APP_MODEL, UserGroup } from 'src/bo/APP_MODELS';
import { IAppPickListOption } from './../appPickListBox/appPickListBox.component';
import { AppCheckBoxComponent } from './../appCheckBox/appCheckBox.component';
import { ICellEvent, ControlTypeEnum, IMenuItem, DetailView_Detail, UserGroupEnum } from './../class_helper';
import { PropertyTypeEnum, ListViewModel, isFloat } from 'src/utils/controls/class_helper';
import { AppSelectBoxComponent, IAppSelectBoxOption } from './../appSelectBox/appSelectBox.component';
import { AppNumberBoxComponent } from './../appNumberBox/appNumberBox.component';
import { DragEndEvent } from 'angular-draggable-droppable';
import { DragMoveEvent } from 'angular-draggable-droppable';
import { IPagingOption } from './../../service/data.service';
import { DataService } from 'src/utils/service/data.service';
import { MenuItem } from 'primeng/api/menuitem';
import {
  SimpleChanges, ElementRef,
  ViewChild,
  ChangeDetectorRef, Pipe,
  PipeTransform,
  ViewContainerRef,
  ComponentFactoryResolver,
  EventEmitter,
  QueryList,
  ViewChildren,
  IterableDiffers,
  IterableDiffer,
  isDevMode,
  ChangeDetectionStrategy
} from "@angular/core";
import {
  Component,
  OnInit,
  Input
} from "@angular/core";
import * as TS from "linq-typescript";
import * as $ from "jquery";
import * as moment from 'moment';
import { Paginator } from 'primeng/paginator';
import { BaseAppControlOption, BaseAppControl } from '../_base/baseAppControl';
import { ContextMenu } from 'primeng/contextmenu';
import { GhostElementCreatedEvent } from 'angular-draggable-droppable';
import { Enumerable } from 'linq-typescript';

import { CurrencyPipe } from '@angular/common';
import { isNullOrUndefined } from 'util';
import { Calendar } from 'primeng/calendar';
import { AppTextBoxComponent } from '../appTextBox/appTextBox.component';
import { Checkbox } from 'primeng/checkbox';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AppPickListBoxComponent, } from '../appPickListBox/appPickListBox.component';
import { MessageBoxService } from '../appModal/appModal.component';
import { HttpClient } from '@angular/common/http';
import { VirtualTimeScheduler } from 'rxjs';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';

@Component({
  selector: "app-data-table",
  templateUrl: "./appDataTable.component.html",
  styleUrls: ["./appDataTable.component.less"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: BaseAppControl, useExisting: AppDataTableComponent
  },
  {
    provide: NG_VALUE_ACCESSOR,
    useExisting: AppDataTableComponent,
    multi: true
  }]
})
export class AppDataTableComponent extends BaseAppControl<any> implements OnInit {

  getOption(): BaseAppControlOption {
    return null;
  }

  columns: DataTableColumn[] = [];
  hiddenColumns: DataTableColumn[] = [];
  deletedItems: IDataRow[] = [];

  fixedLeftColumns: DataTableColumn[] = [];
  fixedRightColumns: DataTableColumn[] = [];
  baseColumns: DataTableColumn[] = [];
  groupColumns: DataTableColumn[] = [];
  actionRowItems?: IActionRowItem[] = [];
  actionRowItemColWidth?: number = 60;

  tableWidth: number = 0;
  tabIndex: number = 0;
  documentBody = document.body;
  isAddRowItem : boolean = false;

  @ViewChild("holder")
  holder: ElementRef;

  @ViewChild("container")
  container: ElementRef;

  @ViewChild("gridHeader")
  gridHeader: ElementRef;

  @ViewChild("gridBody")
  gridBody: ElementRef;

  @ViewChild("gridFooter")
  gridFooter: ElementRef;

  gridItems: IDataRow[] = [];

  @ViewChild("verticalScroll")
  verticalScroll: ElementRef;

  @ViewChild("horizontalScroll")
  horizontalScroll: ElementRef;

  @ViewChild("baseBodyTable")
  baseBodyTable: ElementRef;

  @ViewChild("leftBodyTable")
  leftBodyTable: ElementRef;

  @ViewChild("rightBodyTable")
  rightBodyTable: ElementRef;

  @ViewChild("baseHeaderTable")
  baseHeaderTable: ElementRef;

  @ViewChild("baseFooterTable")
  baseFooterTable: ElementRef;

  @ViewChild("paginator")
  paginator: Paginator;

  @ViewChild("contextMenu")
  contextMenu: ContextMenu;

  @Input()
  option: DataTableOption = null;

  @ViewChild('columnChooserOp')
  columnChooserOp: OverlayPanel;

  @ViewChild('columnChooser')
  columnChooser: ElementRef;

  sortingColumns: SortColumn[] = [];
  showFilterRow: boolean = true;
  summaryObject: any = {};
  contextMenus: MenuItem[] = [];
  filterObject: any = {};
  isEditing: boolean = false;
  isLoading: boolean = false;
  isColumnResizing: boolean = false;
  // itemSelectedIndicator: any[] = [];
  containerPadding: number = 20;
  lastRowHeight: number = 100;
  listItems: any[] = [];
  booleanFilterItems: any[] = [
    {
      label: "True",
      value: true
    },
    {
      label: "False",
      value: false
    }
  ];
  SQL: string = null;
  TotalNumberRecordsPerPage: number = 10;
  TotalRecordCount: number = 0;
  showFooter: boolean = false;
  multiSelect: boolean = false;
  allowEditing: boolean = false;
  newRowEditing: boolean = false;
  rowsPerPageOptions: number[] = [10, 20, 30, 50];
  //
  //
  //

  @ViewChildren(Checkbox)
  Checkboxes: QueryList<Checkbox>

  private iterDiffers: IterableDiffer<any>;

  groupHeaderTdWidth: number = 20;

  userTOken: any = {};

  constructor(
    private changeDetector: ChangeDetectorRef,
    public element: ElementRef,
    public dataService: DataService,
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    iterableDiffers: IterableDiffers,
    _httpClient: HttpClient,
    private msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    ) {
    super(element,changeDetector);


    this.userTOken = this.userAuthSvc.getDecodedToken();
    
    this.iterDiffers = iterableDiffers.find([]).create(null);
    this.httpClient = _httpClient;
  }

  isFirstChange: boolean = false;
  ngDoCheck(): void {
    let changes = this.iterDiffers.diff(this.listItems);
    if (changes) {
      this.reloadData();
      if ( this.isRemoteData !== true && this.isFirstChange !== false ) {
        this.onControl_ValueChanged();
      }
      this.isFirstChange = true;
      if (this.isAddRowItem == true ){
        setTimeout(() => {
          $(this.verticalScroll.nativeElement).scrollTop(100000);
          this.isAddRowItem = false;
        }, 100);
      }
      
    }
  }

  ngOnChanges(ch: SimpleChanges): void {
    if (this.option) {
      this.showFilterRow = this.option.showFilter;
      this.allowEditing = this.option.allowEditing;
      this.multiSelect = this.option.multiSelect == true;
      this.actionRowItems = this.option.actionRowItems ? this.option.actionRowItems : [];
      if (this.allowEditing == true) {
        this.newRowEditing = this.option.newRowEditing;
      }
      setTimeout(() => {
        if (this.option.Oid_ListView) {
          this.getListViewFormat().then(() => {
            this.reloadData();
          });
        } else {
          if (this.option.columns) {
            this.initColumns(this.option.columns);
            this.reloadData();
          } else {
            if (this.option.apiUrl) {
              this.changeDetector.detectChanges();
              this.reloadData();
            }
          }
        }
      }, 100);
    }
  }

  async refreshColumns() {
    if (this.option.Oid_ListView) {
      await this.getListViewFormat();
      this.reloadData();
    }
  }

  async getListViewFormat(): Promise<void> {
    var listViews = await this.httpClient.get("assets/listview/_Index.json?q=" + new Date().getTime()).toPromise<any>();
    var listView: ListViewModel = null;
    if ( isNullOrUndefined(this.option.Oid_ListView) == true ) return Promise.resolve();
    if ( Object.keys(listViews).includes(this.option.Oid_ListView.toUpperCase()) && isDevMode() != true) {
        listView = await this.httpClient.get(`assets/listview/${listViews[this.option.Oid_ListView.toUpperCase()]}.json?q=${new Date().getTime()}`).toPromise<any>();//;.then((listView: ListViewModel) => {
    } else {
        listView = await this.dataService.get(`Model/ListView/${this.option.Oid_ListView}`);//;.then((listView: ListViewModel) => {
    }
    var cols: DataTableColumn[] = [];
    //this.showFilterRow = false;
    this.option.caption = listView.Caption;
    this.option.Oid_DetailView = listView.ID_DetailView;
    var details = new TS.List(listView.Details).orderBy(d => d.VisibleIndex).toArray();
    details.forEach((d) => {
      if (d.IsActive !== true) return;
      //if ( d.VisibleIndex == null ) return;
      var c: DataTableColumn = {
        oid: d.Oid,
        dataField: d.Name,
        caption: d.Caption ? d.Caption : d.Name,
        seqNo: d.VisibleIndex,
        visible : d.IsVisible == true,
        displayDataField: d.DisplayProperty,
        width: parseInt((d.Width ? d.Width : 150) + ""),
        filterType: FilterTypeEnum.TextBox,
        fixed: d.Fixed,
        isFilter: d.IsFilter,
        id_propertyType : d.ID_PropertyType,
        filterEditorType: d.ID_FilterControlType,
        fixedRight: d.FixedPosition == 'right',
        groupIndex : d.GroupIndex
      };
      switch (d.ID_PropertyType) {
        case PropertyTypeEnum.String:
        case PropertyTypeEnum.Uniqueidentifier:
        case PropertyTypeEnum.Color:
          c.dataType = DataTypeEnum.String;
          break;
        case PropertyTypeEnum.Int:
          c.dataType = DataTypeEnum.Integer;
          break;
        case PropertyTypeEnum.Decimal:
          c.dataType = DataTypeEnum.Decimal;
          break;
        case PropertyTypeEnum.Bit:
          c.dataType = DataTypeEnum.Boolean;
          if ( c.textAlignment == null ) c.textAlignment = TextAlignmentEnum.Center;
          break;
        case PropertyTypeEnum.DateTime:
          c.dataType = DataTypeEnum.DateTime;
          break;
        case PropertyTypeEnum.Date:
          c.dataType = DataTypeEnum.Date;
          break;
        case PropertyTypeEnum.Time:
          c.dataType = DataTypeEnum.Time;
          break;
        case PropertyTypeEnum.Image:
          c.dataType = DataTypeEnum.Image;
          break;
      }
      cols.push(c);
    });
    this.initColumns(cols);
    return Promise.resolve();
  };

  private isFirstInitColumns: boolean = false;
  initColumns(cols: DataTableColumn[]): void {
    var elContainer = $(this.container.nativeElement);
    var padding = this.containerPadding;
    var width = elContainer.outerWidth() - padding;
    if (this.isFirstInitColumns != true) {
      if (this.option.customizeColumns) {
        cols = this.option.customizeColumns(cols);
        if (cols.length > 0) cols = new TS.List(cols).orderBy(d => d.seqNo).toArray();
      }
    };
    this.columns = cols;
    if (this.isFirstInitColumns !== true) {
      this.isFirstInitColumns = true;
      if (this.option.onColumns_Initialized) {
        this.option.onColumns_Initialized(cols);
      }
    };
    this.hiddenColumns = new TS.List(cols).where(d => d.visible === false).toArray();
    cols = new TS.List(cols).where(d => d.visible !== false).toArray();
    this.totalColumnCount = cols.length;
    if ( this.multiSelect == true ) {
      var fixedLeftCols = new TS.List(cols).where(d => d.fixed == true && d.fixedRight != true).toArray();
      if ( fixedLeftCols.length == 0 ) {
        cols[0].fixed = true;
        cols[0].fixedRight = false;
      }
    }
    this.groupColumns = new TS.List(cols).where(d => d.groupIndex > 0).orderBy(d => d.groupIndex).toArray();
    var fixedLeftCols = new TS.List(cols).where(d => d.fixed == true && d.fixedRight != true).toArray();
    var fixedRightCols = new TS.List(cols).where(d => d.fixed == true && d.fixedRight == true).toArray();
    var colsWithoutWidth = new TS.List(cols).where(d => !d.width).toArray();
    var totalDefinedWidth = new TS.List(cols).sum(d =>
      !d.width ? 0 : (d.width as number)
    );
    var defaultWidth = 150;
    if (cols) {
      if (width > totalDefinedWidth) {
        defaultWidth = parseInt(width / cols.length + "") + 10;
      }
    };
    colsWithoutWidth.forEach(d => {
      d.width = defaultWidth; //temp
    });

    var nonFixedColumns = new TS.List(cols).where(d=>d.fixed != true).toArray()
    var _tempWidth = new TS.List(nonFixedColumns).sum(d => d.width as number);
    var _parentWidth = $(this.holder.nativeElement).width();
    // alert(_tempWidth);
    // alert(_parentWidth);
    if (this.isRemoteData == true  ) {
      if ( ( _tempWidth ) < _parentWidth) {
        var width = _parentWidth - _tempWidth;
        nonFixedColumns[nonFixedColumns.length -1].width += width;
      }
    }

    this.fixedRightColumns = fixedRightCols;
    this.fixedLeftColumns = fixedLeftCols;
    var baseColumns = new TS.List(cols).where(d => d.fixed !== true).toArray();
    var seqNo = 0;
    this.fixedLeftColumns.forEach(el => {
      el.colNo = seqNo++
      el.seqNo = el.colNo * 100;
      el.uid = Math.floor(Math.random() * 9999) + 1000;
    });
    baseColumns.forEach(el => {
      el.colNo = seqNo++
      el.seqNo = el.colNo * 100;
      el.uid = Math.floor(Math.random() * 9999) + 1000;
    });
    this.fixedRightColumns.forEach(el => {
      el.colNo = seqNo++
      el.seqNo = el.colNo * 100;
      el.uid = Math.floor(Math.random() * 9999) + 1000;
    });
    //this.columns = cols;
    //console.log(this.name, this.baseColumns);
    this.baseColumns = baseColumns;

    var totalTableWidth = new TS.List(cols).sum(d => d.width as number);
    //console.log( new TS.List(cols).select(d=>d.width).toArray() );
    this.tableWidth = totalTableWidth;
    if (this.listItems.length > 0) {
      setTimeout(() => {
        this.computeTotals();
      }, 500);
    }
    this.changeDetector.detectChanges();
  }

  totalColumnCount: number = 0;
  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }

  groupTotals: any[] = [];


  reloadData(IsReset?: boolean, searchValue?:string ): Promise<void> {
	if ( this.option.autoLoad == false ) return Promise.resolve();
    return new Promise<void>(async (res, rej) => {
      this.isEditing = false;
      this.isLoading = true;
      var apiUrl = this.option.apiUrl;
      var sourceKey = this.option.sourceKey;
      var itemSelectedIndicator: any = [];
      // if (this.allowEditing !== true) {
      if (this.isRemoteData == true) {
        if (IsReset == true) {
          this.changeDetector.detectChanges();
        };
        var skip = this.paginator.getPage() * this.TotalNumberRecordsPerPage;
        if (skip == 0) {
          this.groupTotals = [];
        };
        var opt: IPagingOption = {
          Skip: skip,
          Take: this.TotalNumberRecordsPerPage,
          SQL: sourceKey,
          PrimaryKey : this.option.propertyKey,
          FilterOption: JSON.stringify(this.filterObject),
          SortingOption: this.sortingColumns.length > 0 ? JSON.stringify(this.sortingColumns) : null,
          SummaryOption: JSON.stringify(this.summaryObject),
          GroupOptions: this.groupColumns.length > 0 ?
            new TS.List(this.groupColumns).orderBy(d => d.seqNo).select(d => d.displayDataField ?
              d.displayDataField : d.dataField).toArray().join(",") : null,
        };
        if ( searchValue?.length > 0 ) {
          opt.SearchValue = searchValue;
          opt.ActiveColumns = new TS.List(this.baseColumns).select(s => s.dataField).toArray();
        }
        if (sourceKey) apiUrl = `Model/GetList/${this.option.Oid_ListView}`;
        if (apiUrl == undefined) {
          rej();
          return;
        };
        if ( isNullOrUndefined( this.option.onBefore_LoadData ) == true ) this.option.onBefore_LoadData = new EventEmitter<IPagingOption>();
        // if (this.option.onBefore_LoadData) {
        //   await this.option.onBefore_LoadData(opt);
        // }
        this.option.onBefore_LoadData.emit(opt);
        console.log("OPT--->", JSON.stringify(opt));
        this.dataService.GetPagingList(apiUrl, opt).then((d: any) => {
          var list: any[] = d.Data;
          //console.log(list);
          var summaries: any[] = d.Summaries;
          if (this.groupTotals.length == 0) {
            this.groupTotals = d.GroupTotals ? d.GroupTotals : [];
          };
          if (this.columns.length == 0 && list.length > 0) {
            var columns: DataTableColumn[] = [];
            var data: any = list[0];
            var keys = Object.keys(data);
            keys.forEach((key: string) => {
              var col: DataTableColumn = {
                dataField: key,
                caption: key,
                fixed: false,
                width: 200
              };
              columns.push(col);
            });
            this.initColumns(columns);
          }
          this.initColumnDefaultValues(list);
          if (summaries) {
            this.TotalRecordCount = summaries[0].RecordCount;
          }
          list.forEach((d: any, index: number) => {
            d.$uid = "uid" + index;
            itemSelectedIndicator.push({
              $uid: d.$uid,
              value: false
            });
          });
          //this.itemSelectedIndicator = itemSelectedIndicator;
          //
          //
          //
          var rows: IDataRow[] = [];
          if (list) {
            if (this.groupTotals.length > 0 && this.groupColumns.length > 0) {
              //console.log("GROUP TOTLAS", this.groupTotals);

              this.groupTotals.forEach((g, i) => {
                var qry = Enumerable.fromSource(list);
                this.groupColumns.forEach(gc => {
                  var dataField = gc.displayDataField ? gc.displayDataField : gc.dataField;
                  var value = g[dataField];
                  qry = qry.where(d => d[dataField] == value);
                });

                var gcList = qry.toArray();

                if (gcList.length > 0) {

                  var lastGroupValue = null;
                  this.groupColumns.forEach((gc, index) => {
                    var dataField = gc.displayDataField ? gc.displayDataField : gc.dataField;
                    var caption = gc.caption;
                    var _value = (g[dataField] ? g[dataField] : "-")
                    var value = _value + (lastGroupValue ? lastGroupValue : "");
                    lastGroupValue = _value;
                    var totalCount = g[dataField + "_Count"];
                    var isAdd: boolean = new TS.List(rows).where(r => r.groupValue == value && r.groupField == dataField).count() == 0;
                    if (isAdd) {
                      rows.push(
                        {
                          value: {
                            caption: caption,
                            value: lastGroupValue,
                            totalCount: totalCount
                          },
                          rowKey: -1,
                          rowIndex: -1,
                          groupIndex: index + 1,
                          groupField: dataField,
                          groupValue: _value,
                          colGroup: gc,
                          height: 50
                        }
                      )
                    }

                  });
                  //
                  //
                  //
                  gcList.forEach(data => {
                    rows.push({
                      value: data,
                      rowKey: data[this.option.propertyKey],
                      height: 50,
                      rowIndex: -1
                    })
                  });
                }
              });
            } else {
              list.forEach((d,index) => {
                rows.push({
                  value: d,
                  rowKey: d[this.option.propertyKey],
                  rowIndex : index,
                  height: 50
                })
              });
            }
          };
          this.gridItems = rows;
          this.isLoading = false;
          this.changeDetector.detectChanges();
          res();
        });
      } else {
        var list = this.listItems;
        if (list.length > 0) {
          list.forEach((d: any, index: number) => {
            d.$uid = "uid" + index;
          });
        };
        var qry = Enumerable.fromSource(list);
        this.columns.forEach((d: DataTableColumn) => {
          var value = this.filterObject[d.dataField];
          if (value) {
            if (value.toString().trim().length > 0) {
              qry = qry.where((data: any) => {
                if (data[d.dataField]) {
                  if (d.dataType == DataTypeEnum.Boolean) {
                  } else {
                    return data[d.dataField].toString().includes(value);
                  }
                }
                return false;
              });
            }
          }
        });
        list = qry.toArray();
        if (this.groupColumns.length > 0) {
          new TS.List(this.groupColumns).orderByDescending(s => s.groupIndex).toArray().forEach(g => {
            var currentSort = this.sortingColumns.find(d => d.dataField == g.dataField);
            var isDesc: boolean = false;
            if (currentSort) {
              isDesc = currentSort.isDesc;
            }
            this.sortingColumns.unshift({
              dataField: g.dataField,
              isDesc: isDesc
            });
          });
        };
        if (this.sortingColumns.length > 0) {
          var OrderQry = new TS.List(list).orderBy(() => 1);
          this.sortingColumns.forEach((d: SortColumn) => {
            if (this.groupColumns.length > 0) {
              if (this.groupColumns.find(g => g.dataField == d.dataField)) return;
            }
            if (d.isDesc) {
              OrderQry = OrderQry.thenByDescending((x: any) => x[d.dataField]);
            } else {
              OrderQry = OrderQry.thenBy((x: any) => x[d.dataField]);
            }
          });
          list = OrderQry.toArray();
        };
        var GroupTotals: any[] = [];
        var GroupIndicator: any[] = [];
        if (this.groupColumns.length > 0) {
          list.forEach(d => {
            var groupKeys: string[] = [];
            this.groupColumns.forEach(g => {
              var dataField = g.displayDataField ? g.displayDataField : g.dataField;
              var value = d[dataField];
              GroupIndicator.push({
                dataField: dataField,
                colGroup: g
              })
              groupKeys.push(`{"${dataField}":"${value}"}`);
            });
            d.$groupKey = groupKeys.join(",");
          });
          GroupTotals = Enumerable.fromSource(list).groupBy(d => d.$groupKey).toArray();
        };
        this.computeTotals();
        this.initColumnDefaultValues(list);
        var _list = this.paginateList(list);
        var rows: IDataRow[] = [];

        if (GroupTotals.length > 0) {
          console.log(GroupTotals);
          GroupTotals.forEach(d => {
            var groupKey = JSON.parse(d.key);
            Object.keys(groupKey).forEach((p, index: number) => {
              var value = groupKey[p];
              if (new TS.List(rows).where(r => r.groupValue == value && r.groupField == p).count() == 0) {
                var colGroup = GroupIndicator.find(d => d.dataField == p).colGroup;
                rows.push(
                  {
                    value: {
                      caption: p,
                      totalCount: 0
                    },
                    rowIndex : -1,
                    rowKey: -1,
                    groupIndex: index + 1,
                    groupField: p,
                    groupValue: value,
                    colGroup: colGroup,
                    height: 50 // wait
                  }
                )
              };

              var list: any[] = d.value;
              list.forEach((data,index) => {
                rows.push({
                  value: data,
                  rowIndex : index,
                  rowKey: data[this.option.propertyKey],
                  height: 50
                })
              });

            });
          });
        } else {
          _list.forEach((d: any, index: number) => {
            rows.push(
              {
                value: d,
                rowKey: d[this.option.propertyKey],
                id: index + "",
                height: 50,
                rowIndex : index
              }
            )
            itemSelectedIndicator.push({
              $uid: d.$uid,
              value: false
            });
          });
        };
        var parentHeight = ( rows.length * 50 ) + this.tableHeaderHeight + 170 + ( this.showFooter == true ? 60 : 0 );
        $(this.el.nativeElement).parent().css('height', parentHeight + 'px');
        this.gridItems = rows;
        //this.itemSelectedIndicator = itemSelectedIndicator;
        this.TotalRecordCount = list.length;
        this.changeDetector.detectChanges();
        this.isLoading = false;
        res();
      }
    });
  }

  computeTotals(): void {
    var list = this.listItems;
    var summaryCols = new TS.List(this.columns).where(s => s.summaryType != null).toArray();
    summaryCols.forEach(col => {
      if (list.length > 0) {
        switch (col.summaryType) {
          case SummaryTypeEnum.Sum:
            this.summaryObject[col.dataField] = new TS.List(list).sum(
              (d: any) => {
                var val = parseFloat(d[col.dataField] + "");
                return val;
              }
            );
            break;
          case SummaryTypeEnum.Average:
            this.summaryObject[col.dataField] = new TS.List(list).average(
              (d: any) => {
                var val = parseFloat(d[col.dataField] + "");
                return val;
              }
            );
            break;
        }
      } else {
        this.summaryObject[col.dataField] = 0;
      }
    });
  }

  private initColumnDefaultValues(list: any[]) {
    this.columns.forEach((d: DataTableColumn) => {
      if (isNullOrUndefined(d.dataType) == true) {
        if (list.length == 0) return;
        var obj = list[0];
        var t = typeof obj[d.dataField];
        var val = obj[d.dataField];

        if (
          moment(val, "MM/DD/YYYY", true).isValid() &&
          t == "string" &&
          val.toString().includes("T")
        ) {
          d.dataType = DataTypeEnum.DateTime;
        } else if (isFloat(val) == true) {
          d.dataType = DataTypeEnum.Decimal;
        } else {
          switch (t) {
            case "number":
              d.dataType = DataTypeEnum.Integer;
              break;
            case "boolean":
              d.dataType = DataTypeEnum.Boolean;
              if (isNullOrUndefined(d.textAlignment)) {
                d.textAlignment = TextAlignmentEnum.Center;
              }
              break;
            default:
              d.dataType = DataTypeEnum.String;
              break;
          }
        }
      }

      switch (d.dataType) {
        case DataTypeEnum.String:
          if (isNullOrUndefined(d.filterType)) {
            d.filterType = FilterTypeEnum.TextBox;
          }

          if (isNullOrUndefined(d.editorType)) {
            d.editorType = ControlTypeEnum.TextBox;
          }

          break;
        case DataTypeEnum.Decimal:
        case DataTypeEnum.Integer:
          if (isNullOrUndefined(d.filterType)) {
            d.filterType = FilterTypeEnum.NumberBox;
          }
          if (isNullOrUndefined(d.editorType)) {
            d.editorType = ControlTypeEnum.NumberBox;
          }
          if (d.dataType == DataTypeEnum.Decimal) {
            if (isNullOrUndefined(d.textAlignment)) {
              d.textAlignment = TextAlignmentEnum.Right;
            }
            if (d.summaryType == undefined) {
              d.summaryType = SummaryTypeEnum.Sum;
            }
          }
          break;
        case DataTypeEnum.DateTime:
        case DataTypeEnum.Date:
          if (isNullOrUndefined(d.filterType)) {
            d.filterType = FilterTypeEnum.DateRangeBox;
          }

          if (isNullOrUndefined(d.filterType)) {
            d.editorType = ControlTypeEnum.DatePicker;
          }
          break;
        case DataTypeEnum.Boolean:
          if (isNullOrUndefined(d.filterType)) {
            d.filterType = FilterTypeEnum.BooleanSelectBox;
          }

          if (isNullOrUndefined(d.filterType)) {
            d.editorType = ControlTypeEnum.CheckBox;
          }

          break;
      }
    });
    var summaries = new TS.List(this.columns).where(d => d.summaryType != null).toArray();
    if (summaries.length > 0) this.showFooter = true;
    this.changeDetector.detectChanges();
  }

  reload(isReset?: boolean): void { 
    this.paginator.changePage(0);
  }

  paginateList(array: any[]): any[] {
    var page_size: number = this.paginator.rows;
    var page_number: number = this.paginator.getPage() + 1;
    return array.slice((page_number - 1) * page_size, page_number * page_size);
  }

  paginate($event): void {
    var page = this.paginator.getPage();
    this.TotalNumberRecordsPerPage = this.paginator.rows;
    this.reloadData(page == 0);
  }

  IsSelectBoxVisible(): boolean {
    var isVisible = true;
    return isVisible;
  }

  onScroll($event: Event, tag: number): void {
    var isVertical: boolean = tag == 0;
    var bodyTable = $(this.baseBodyTable.nativeElement);
    var e = $event as any;
    if (isVertical) {
      var leftBodyTable = $(this.leftBodyTable.nativeElement);
      var rightBodyTable = $(this.rightBodyTable.nativeElement);
      var scrollTop = $(e.target).scrollTop();
      bodyTable.scrollTop(scrollTop);
      leftBodyTable.scrollTop(scrollTop);
      rightBodyTable.scrollTop(scrollTop);
    } else {
      var headerBase = $(this.baseHeaderTable.nativeElement);

      var scrollLeft = $(e.target).scrollLeft();
      headerBase.scrollLeft(scrollLeft);
      bodyTable.scrollLeft(scrollLeft);

      if (this.baseFooterTable) {
        var baseFooter = $(this.baseFooterTable.nativeElement);
        baseFooter.scrollLeft(scrollLeft);
      }
    }
  }

  ngAfterViewInit(): void {
    $(this.holder.nativeElement).bind('focus', function(e){
      e.preventDefault();
    });
    var _scrollView = $(this.verticalScroll.nativeElement);
    $(this.element.nativeElement).bind("mousewheel", function (e: any) {
      e.preventDefault();
      e.stopPropagation();
      if (e.originalEvent.wheelDelta / 120 > 0) {
        var scrollTop = _scrollView.scrollTop();
        _scrollView.scrollTop(scrollTop - 50);
      } else {
        var scrollTop = _scrollView.scrollTop();
        _scrollView.scrollTop(scrollTop + 50);
      }
    });
    //
    //
    //
    if (this.option) {
      if (this.option.onView_Initialized) this.option.onView_Initialized(this);
    }
  }

  onKeyDown(evt: KeyboardEvent): void {
    super.onKeyDown(evt);
    if (this.isEditing !== true) {
      ;
      if (evt.keyCode === 27) {
        //Escape
        this.clearSelection();
      };

      if ((evt.keyCode >= 37 && evt.keyCode <= 40) || evt.keyCode === 13) {
        evt.stopPropagation();
        evt.preventDefault();
        if ((evt.keyCode >= 37 && evt.keyCode <= 40)) {
          if (this.currentCell == null) {
            var target = $(this.element.nativeElement).find('.rc-0-0');
            this.onCell_SingleClick({ target: target } as any);
            return;
          }
        }
        if (this.currentCell != null) {
          if (this.currentCell.attr("class") == undefined) return;
          var classes = this.currentCell.attr("class").split(" ");
          var cellClass = new TS.List(classes)
            .where((d: string) => d.includes("rc-"))
            .toArray();
          if (cellClass.length > 0) {
            var rc = cellClass[0].replace(/rc-/g, "").split("-");
            var row = parseInt(rc[0]);
            var col = parseInt(rc[1]);
            switch (evt.keyCode) {
              case 32:
                //this.itemSelectedIndicator[row].value = !this.itemSelectedIndicator[row].value;
                return;
              case 37: //left
                if (col == 0) return;
                col--;
                break;
              case 38: //up
                if (row != 0) {
                  $(this.element.nativeElement)
                    .find("tr.jf-focused")
                    .removeClass("jf-focused");
                  row--;
                  var rows = $(this.element.nativeElement).find(".r-" + row);
                  rows.addClass("jf-focused");
                } else {
                  $(this.verticalScroll.nativeElement).scrollTop(0);
                }
                break;
              case 39: //right
                // alert(this.baseColumns.length);
                if (col == this.totalColumnCount - 1) return;
                col++;
                break;
              case 40: //down
                if (row !== this.gridItems.length - 1 + (this.newRowEditing == true ? 1 : 0)) {
                  $(this.element.nativeElement)
                    .find("tr.jf-focused")
                    .removeClass("jf-focused");
                  row++;
                  var rows = $(this.element.nativeElement).find(".r-" + row);
                  rows.addClass("jf-focused");
                } else {
                  return;
                }
                break;
            };

            // if  ( this.gridItems[row].groupIndex > 0 ) {
            //   // alert(row);
            //   return;
            // };

            this.currentCell.removeClass("jf-focused");
            var container = $(this.element.nativeElement);
            var nextCell = container.find(`.rc-${row}-${col}`);
            this.currentCell = $(nextCell[0]);
            this.currentCell.addClass("jf-focused");
            var bodyTable = $(this.baseBodyTable.nativeElement).find("table");
            var verticalScroll = $(this.verticalScroll.nativeElement);
            var horizontalScroll = $(this.horizontalScroll.nativeElement);
            let scrollValueTop = 300;
            switch (evt.keyCode) {
              case 38:
              case 40:
                var top = verticalScroll.offset().top;
                var bottom =
                  verticalScroll.offset().top + verticalScroll.outerHeight();
                var cellTop = this.currentCell.offset().top;
                if (evt.keyCode == 38) {
                  if (!(cellTop >= top && cellTop <= bottom)) {
                    var scrollTop = verticalScroll.scrollTop() - scrollValueTop;
                    verticalScroll.scrollTop(scrollTop);
                  }
                } else if (evt.keyCode == 40) {
                  if (row == (this.gridItems.length - 1) + (this.newRowEditing == true ? 1 : 0)) {
                    verticalScroll.scrollTop(bodyTable.outerHeight());
                    return;
                  }
                  var cellBottom = cellTop + this.currentCell.outerHeight() + 50;
                  if (!(cellBottom >= top && cellBottom <= bottom)) {
                    var scrollTop = verticalScroll.scrollTop() + scrollValueTop;
                    verticalScroll.scrollTop(scrollTop);
                  }
                }
                break;
              case 37:
              case 39:
                var left = horizontalScroll.offset().left;
                var right =
                  horizontalScroll.offset().left + horizontalScroll.outerWidth();
                var tableFixedRightWidth =
                  $(this.rightBodyTable.nativeElement).width() + 17;
                var tableFixedLeftWidth = $(
                  this.rightBodyTable.nativeElement
                ).width();
                var boundaryLeft = left + tableFixedLeftWidth;
                var boundaryRight = right - tableFixedRightWidth;
                var column = this.baseColumns[col];
                //alert(col);
                if (column == undefined) return;
                if (evt.keyCode == 39) {
                  if (column.fixed != true) {
                    var cellRight =
                      this.currentCell.offset().left +
                      this.currentCell.outerWidth();
                    if (cellRight > boundaryRight) {
                      horizontalScroll.scrollLeft(
                        horizontalScroll.scrollLeft() +
                        this.currentCell.outerWidth() +
                        150
                      );
                    }
                  }
                } else if (evt.keyCode == 37) {
                  if (column.fixed != true) {
                    var cellLeft = this.currentCell.offset().left;
                    if (cellLeft < boundaryLeft) {
                      horizontalScroll.scrollLeft(
                        horizontalScroll.scrollLeft() -
                        this.currentCell.outerWidth() -
                        150
                      );
                    }
                  }
                }
                break;
              //
              //
              //
              case 13: //EnterKey
                if (this.currentCell != null) {
                  var rowItem: IDataRow = this.gridItems[row];
                  if (rowItem) {
                    if (rowItem.groupIndex > 0) return;
                  }
                  this.initCellEditing({
                    cellIndex: col,
                    rowIndex: row,
                    key: rowItem ? rowItem.value : null,
                    value: null,
                    isNewRowEdit: row == this.gridItems.length
                  });
                  //
                  //
                  //
                  // TODO
                  if (this.option.allowEditing === true) return;
                  if (this.option.onCell_EnterKey != null) {
                    this.option.onCell_EnterKey.emit({
                      cellIndex: col,
                      rowIndex: row,
                      key: rowItem ? rowItem.value : null,
                      value: null,
                    });
                  }
                }
                break;
            }
          }
        }
      }
    }
  }

  clearSelection(): void {
    if (this.currentCell) this.currentCell.removeClass("jf-focused");
    $(this.element.nativeElement).find("tr.jf-focused").removeClass("jf-focused");
    this.currentCell = null;
    // this.itemSelectedIndicator.forEach((f)=>{
    //   f.value = false;
    // });
    this.changeDetector.detectChanges();
  }

  getHeaderClass(col: DataTableColumn): any {
    var cls: any = {};
    cls[`uid-${col.uid}`] = true;
    return cls;
  }

  getCellClass(col: DataTableColumn, rowIndex: number, isFixed: boolean): any {
    var _class = {};
    _class[`rc-${rowIndex}-${col.colNo}`] = true;
    //  isFixed == false ? col.fixed != true : col.fixed == true;
    _class[`df_${col.dataField}`] = true;
    _class["is-can-edit"] = col.allowEdit == true;
    return _class;
  }

  getRowClass(item: IDataRow, rowIndex: number): any {
    var _class = {};
    _class["r-" + rowIndex] = true;
    //var uid = this.itemSelectedIndicator.find(d => d.$uid == item.id );
    if ( this.isRemoteData == true ) {
      _class["item-selected"] = item.isSelected == true;
    }
    //
    if ( item.groupIndex > 0 ) {
      _class["group-row"] = true;
    }
    if (this.option.onRowClass_Initialized ) { 
      this.option.onRowClass_Initialized(_class, item, rowIndex);
    }
    return _class;
  }

  preventSingleClick: boolean = false;
  timer: any;
  currentCell: JQuery<EventTarget>;
  onSingleClick(evt: MouseEvent, item: ICellEvent): void {
    //console.log(this.Checkboxes);
    if (this.isEditing == true) return;
    this.preventSingleClick = false;
    const delay = 100;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) {
        this.onCell_SingleClick(evt);
      }
    }, delay);
  }

  onDoubleClick(event: any, item: ICellEvent): void {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.initCellEditing(item);
    //
    //
    //
    if (this.option == undefined) return;
    if ( this.option.allowEditing === true ) return;
    if (this.option.onCell_DoubleClick) {
      this.option.onCell_DoubleClick.emit(item);
    }
  }

  onCell_SingleClick(evt: MouseEvent): void {
    if (this.isEditing == true) return;
    if (this.currentCell) this.currentCell.removeClass("jf-focused");
    this.currentCell = $(evt.target);
    if ( this.currentCell.attr('class').split(' ').includes('cn') ) {
      this.currentCell = this.currentCell.parent();
    }
    if (isNullOrUndefined(this.currentCell) == true) return;
    if (isNullOrUndefined(this.currentCell.attr("class")) == true) return;
    if (this.currentCell.attr("class").split(" ").includes("cell-value") == true) {
      this.currentCell = $(this.currentCell).parent();
      if ( this.currentCell.attr('class').split(' ').includes('cn') ) {
        this.currentCell = this.currentCell.parent();
      }
    };
    this.currentCell.addClass("jf-focused");
    var classes = this.currentCell.attr("class").split(" ");
    var cellClass = new TS.List(classes).where((d: string) => d.includes("rc-")).toArray();
    if (cellClass.length > 0) {
      var rc = cellClass[0].replace(/rc-/g, "").split("-");
      var row = parseInt(rc[0]);
      $(this.element.nativeElement).find("tr.jf-focused").removeClass("jf-focused");
      var rows = $(this.element.nativeElement).find(".r-" + row);
      rows.addClass("jf-focused");
    }
  }

  //
  // Editing
  //
  async initCellEditing(cell: ICellEvent): Promise<void> {
    if (this.allowEditing !== true) return;
    if (this.currentCell == null) return;
    if (this.isEditing == true) return;
    var classes = this.currentCell.attr("class").split(" ");
    var cellClass = new TS.List(classes)
      .where((d: string) => d.includes("rc-"))
      .toArray();
    var dfClass = new TS.List(classes)
      .where((d: string) => d.includes("df_"))
      .toArray();
    if (dfClass.length == 0) return;
    if (cellClass.length == 0) return;
    var rc = cellClass[0].replace(/rc-/g, "").split("-");
    var dataField = dfClass[0].replace(/df_/g, "");
    var row = parseInt(rc[0]);
    //
    //
    //
    var DataTableColumn = this.columns.find(d => d.dataField == dataField);
    if ( DataTableColumn.dataType == DataTypeEnum.Boolean ) return;
    //
    //

    var rowItem = this.gridItems[row];
    if ( rowItem ) {
      if (rowItem.groupIndex > 0 ) return;
    }
    var data = cell.isNewRowEdit !== true ? this.gridItems[row].value : {};
    // TODO
    //
    var allowEdit = DataTableColumn.allowEdit instanceof Function ? DataTableColumn.allowEdit(data) : DataTableColumn.allowEdit;
    if (isNullOrUndefined(allowEdit) == true) allowEdit = false;
    if (allowEdit === false) return;

    if (this.option.allowCell_Edit) {
      if (await this.option.allowCell_Edit(data, DataTableColumn, cell.isNewRowEdit) != true) return;
    }

    var type: ControlTypeEnum = ControlTypeEnum.TextBox;
    if (!DataTableColumn.editorType) {
      switch (DataTableColumn.dataType) {
        case DataTypeEnum.String:
          type = ControlTypeEnum.TextBox;
          break;
        case DataTypeEnum.Integer:
          type = ControlTypeEnum.NumberBox;
          break;
        case DataTypeEnum.Decimal:
          type = ControlTypeEnum.NumberBox;
          break;
        case DataTypeEnum.Date:
          type = ControlTypeEnum.DatePicker;
          break;
        case DataTypeEnum.DateTime:
          type = ControlTypeEnum.DatePicker;
          break;
        // case DataTypeEnum.Boolean:
        //   type = ControlTypeEnum.CheckBox;
        //   break;
      }
    } else {
      type = DataTableColumn.editorType;
    }
    //
    //
    //
    var controlType: any = null;
    switch (type) {
      case ControlTypeEnum.TextBox:
        controlType = AppTextBoxComponent;
        break;
      case ControlTypeEnum.NumberBox:
        controlType = AppNumberBoxComponent;
        break;
      case ControlTypeEnum.SelectBox:
        controlType = AppSelectBoxComponent;
        break;
      case ControlTypeEnum.CheckBox:
        //controlType = AppCheckBoxComponent;
        break;
      case ControlTypeEnum.ListBox:
        controlType = AppPickListBoxComponent;
        break;
    };

    if (!controlType) return;
    if (cell.isNewRowEdit == true) {
      $(this.verticalScroll.nativeElement).scrollTop(100000);
    }
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(controlType);
    var componentInstance = this.viewContainerRef.createComponent(
      componentFactory
    );
    var currentValue = data[DataTableColumn.dataField];
    var baseAppControl = componentInstance.instance as BaseAppControl<any>;
    baseAppControl.name = DataTableColumn.dataField;
    baseAppControl.appContainer.clearButton = false;
    baseAppControl.required = DataTableColumn.required == true;
    //alert(DataTableColumn.required);
    baseAppControl.showLabel = false;
    baseAppControl.onBlur = () => {
      setTimeout(() => {
        baseAppControl.focus();
      }, 300);
    };
    var opt: BaseAppControlOption = null;
    if (type == ControlTypeEnum.ListBox) {
      var listOption = new IAppPickListOption();
      opt = listOption;
    } else if (  type == ControlTypeEnum.SelectBox ) {
      var selectBoxOption = new IAppSelectBoxOption();
      opt = selectBoxOption;
    };
    if (opt) {
      if (DataTableColumn.onEditorOption_Initialized) {
        DataTableColumn.onEditorOption_Initialized(rowItem, opt);
      };
      baseAppControl.option = opt;
      if ( controlType == AppSelectBoxComponent) {
        if ( ( opt as IAppSelectBoxOption ).items ) {
          (baseAppControl as AppSelectBoxComponent).items = (opt as IAppSelectBoxOption).items;
        }
      }
    }
    if (controlType == AppPickListBoxComponent) { 
      (baseAppControl as AppPickListBoxComponent).handleEnteryKey = true;
    }
    if (currentValue == undefined) currentValue = null;
    if (currentValue) {
      if (controlType == AppPickListBoxComponent) {
        var ctrl = (baseAppControl as AppPickListBoxComponent);
        var itemSelected: any = {};
        ctrl.showClearButton = false;
        itemSelected[ctrl.propertyKey] = data[DataTableColumn.dataField];
        var displayValue: string = "";
        if (DataTableColumn.displayDataField) {
          displayValue = data[DataTableColumn.displayDataField];
        }
        if (DataTableColumn.displayText) {
          displayValue = DataTableColumn.displayText(data);
        }
        itemSelected[ctrl.displayKey] = displayValue;
        ctrl.itemSelected = itemSelected;
      }
    }
    if (currentValue) baseAppControl.writeValue(currentValue);
    //setTimeout(() => {
    $(componentInstance.location.nativeElement).keydown(async (evt: any) => {
      evt.stopPropagation();
      //evt.preventDefault();
      var keyCode: number = evt.keyCode;
      if (keyCode == 13) {
        evt.preventDefault();
        evt.stopPropagation();
      }

      if (keyCode == 27) {
        if (controlType == AppPickListBoxComponent) {
          if ((baseAppControl as AppPickListBoxComponent).op.overlayVisible == true) {
            (baseAppControl as AppPickListBoxComponent).op.hide();
            return;
          }
        };
      }

      switch (keyCode) {
        case 13: //Enter
        case 27: // Escape
          if (keyCode == 13) {
            var emitValue = true;
            var itemSelected = null;
            var displayProperty = "Name";
            if (isNullOrUndefined(DataTableColumn.required) !== true) {
              if (typeof DataTableColumn.required == "boolean") {
                if ((DataTableColumn.required as boolean) == true) {
                  emitValue = isNullOrUndefined(baseAppControl.value) !== true;
                }
              } else {
                emitValue = await (DataTableColumn.required as Promise<boolean>);
              }
            };
            //alert('enter');
            if (emitValue !== true) {
              return;
            };
            if (controlType == AppPickListBoxComponent) {
              itemSelected = (baseAppControl as AppPickListBoxComponent).itemSelected;
              displayProperty = (baseAppControl as AppPickListBoxComponent).displayKey;
            } else if (controlType == AppSelectBoxComponent) {
              itemSelected = (baseAppControl as AppSelectBoxComponent).itemSelected;
              displayProperty = (baseAppControl as AppSelectBoxComponent).displayKey;
            };
            if ( isNullOrUndefined(displayProperty) == true) displayProperty = "Name";

            var _value = baseAppControl.value;
            if (currentValue !== _value) {
              var commitEditValue = true;
              if (cell.isNewRowEdit != true) {
                if (this.option.remoteEditing == true && this.option.Oid_Model) {
                  commitEditValue = await this.commitCellValue(DataTableColumn, data, _value);
                }
              };
              if (commitEditValue == true) {

                if (DataTableColumn.displayDataField) {
                  if (baseAppControl.value) {
                    if (itemSelected) {
                      data[DataTableColumn.displayDataField] = itemSelected[displayProperty];
                    }
                  } else {
                    data[DataTableColumn.displayDataField] = null;
                  }
                }
                data[DataTableColumn.dataField] = baseAppControl.value;
              }
            }
            if (cell.isNewRowEdit == true) {
              data.ID = -1;
              this.listItems.push(data);
            }

            if (DataTableColumn.onColumn_ValueChange) {
              DataTableColumn.onColumn_ValueChange({
                control: baseAppControl,
                col: DataTableColumn,
                value: data[DataTableColumn.dataField],
                itemKey: data,
                itemSelected: itemSelected,
                isNewRow: cell.isNewRowEdit == true
              });
            }
            if (this.option.onRow_ValueChange) {
              this.option.onRow_ValueChange({
                control: baseAppControl,
                col: DataTableColumn,
                value: data[DataTableColumn.dataField],
                itemKey: data,
                itemSelected: itemSelected,
                isNewRow: cell.isNewRowEdit == true
              });
            }
            //
            //
            //
            if (cell.isNewRowEdit !== true) {
              this.computeTotals();
            } else {
              this.isAddRowItem = true;
            }
            this.onControl_ValueChanged();
            this.emitValueChange(this.listItems);
          };
          
          
          
          this.viewContainerRef.clear();
          this.changeDetector.detectChanges();
          this.focus();
          this.currentCell.toggleClass("edit-cell");
          this.isEditing = false;
          break;
        case 9: //Tab
          break;

      }
    });
    this.currentCell.toggleClass("edit-cell");
    this.currentCell.append(componentInstance.location.nativeElement);
    this.isEditing = true;
    setTimeout(() => {
      baseAppControl.focus();
    });

    //});
  }

  scrollToBottom() : void {
    $(this.verticalScroll.nativeElement).scrollTop(10000000);
  }

  commitCellValue(dataColumn: DataTableColumn, rowKey: any, value: any): Promise<boolean> {
    return new Promise<boolean>((res) => {
      var saveValue: any = {};
      saveValue[dataColumn.dataField] = value;
      saveValue[this.option.propertyKey] = rowKey[this.option.propertyKey];
      this.dataService.saveObject(this.option.Oid_Model, saveValue).then(() => {
        this.msgBox.success("Saved Successfully.", "Application");
        res(true);
      }, (r) => {
        console.log(r);
        alert('error');
      });
    });
  }

  //
  //
  //
  saveListViewFormat(): void {
    if (this.option.Oid_ListView) {
      var lv: ListView = {
        Oid: this.option.Oid_ListView,
        ListView_Detail: [],
      };
      this.columns.forEach(d => { 
        if ( d.oid ) {
          lv.ListView_Detail.push({
            Oid : d.oid,
            Width : d.width,
            Fixed : d.fixed,
            FixedPosition : d.fixedRight == true ? 'right' : null,
            VisibleIndex : d.seqNo,
            IsVisible : d.visible,
            GroupIndex : d.groupIndex > 0 ? d.groupIndex : 0
          });
        };
      });
      console.log({ lv });
      this.dataService.saveObject(APP_MODEL.LISTVIEW, lv).then(() => { 
        this.msgBox.success("Saved Successfully.", "Listview format");
      });
    }
  }

  parentSrollView: ElementRef;

  focus(): void {
    // $(this.holder.nativeElement).focus((e) => {
    //   e.preventDefault();
    //   e.target.focus({ preventScroll: true });
    // });
    var x = 0, y = 0;
    if ( isNullOrUndefined(this.parentSrollView) != true ) {
      y = $(this.parentSrollView.nativeElement).scrollTop();
      x = $(this.parentSrollView.nativeElement).scrollLeft();
    }
    $(this.holder.nativeElement).focus();
    if ( isNullOrUndefined(this.parentSrollView) != true ) {
      this.parentSrollView.nativeElement.scrollTo(x, y);
    }
  }

  contextMenu_X: number = 0;
  contextMenu_Y: number = 0;
  contextMenu_Target: any = null;
  onRight_Click($event: any): void {
    this.contextMenu_X = $event.clientX;
    this.contextMenu_Y = $event.clientY;
    this.contextMenu_Target = event.target;
  }

  onContextMenu_Show(): void {
    //console.log("Context Menu-->",event, cMenu);
    var top = this.contextMenu_Y; //elemMenu.offsetTop;
    var left = this.contextMenu_X; //elemMenu.offsetLeft;
    var baseTableThs = $(this.gridHeader.nativeElement).find(".base-table th");
    var leftTableThs = $(this.gridHeader.nativeElement).find(
      ".fixed-column-left th"
    );
    var RightTableThs = $(this.gridHeader.nativeElement).find(
      ".fixed-column-right th"
    );
    var currentColumn: any = null;
    var leftThs = new TS.List(this.getHeadersPos(leftTableThs))
      .where(d => left >= d.left && left <= d.right)
      .toArray();
    if (leftThs.length > 0) currentColumn = leftThs[0];
    if (currentColumn == null) {
      var rightThs = new TS.List(this.getHeadersPos(RightTableThs))
        .where(d => {
          return left >= d.left && left <= d.right;
        })
        .toArray();
      if (rightThs.length > 0) currentColumn = rightThs[0];
    }
    if (currentColumn == null) {
      var baseThs = new TS.List(this.getHeadersPos(baseTableThs))
        .where(d => left >= d.left && left <= d.right)
        .toArray();
      if (baseThs.length > 0) currentColumn = baseThs[0];
    }
    if (currentColumn.el.attr("id") == undefined) {
      return;
    };
    
    var id = currentColumn.el.attr("id").replace("col-", "");
    var gridColumn = this.columns.find(d => d.uid == id);
    var dataRow: IDataRow = null;
    // var tableRow: IDataRow = null;
    //
    //
    //
    var gridHeader = $(this.gridHeader.nativeElement);
    var isHeader = top >= gridHeader.offset().top &&
      top <= gridHeader.offset().top + gridHeader.outerHeight();
    $(this.element.nativeElement).find("tr.jf-focused").removeClass("jf-focused");
    if (isHeader == false) {
      var cell = $(this.contextMenu_Target);
      if (isNullOrUndefined(cell) == true) return;
      if (isNullOrUndefined(cell.attr("class")) == true) return;
      var classes = cell.attr("class").split(" ");
      var cellClass = new TS.List(classes).where((d: string) => d.includes("rc-")).toArray();
      if (cellClass.length > 0) {
        var rc = cellClass[0].replace(/rc-/g, "").split("-");
        var row = parseInt(rc[0]);
        dataRow = this.gridItems[row];
        var rows = $(this.element.nativeElement).find(".r-" + row);
        rows.addClass("jf-focused");
      }
    }
    //
    //  TODO
    //
    this.contextMenus = [];

    this.contextMenus.push({
      label: "Refresh",
      icon: "pi pi-fw pi-refresh",
      command: () => {
        this.reload(true);
      }
    });

    if(this.userTOken["ID_UserGroup"] != UserGroupEnum.System) return; 

    if (isHeader == true) {
      this.contextMenus.push({
        label: "Freeze",
        icon: "mdi mdi-table-cog",
        items: [
          {
            label: "Left",
            icon: "mdi mdi-table-column-plus-after",
            command: () => {
              gridColumn.fixed = true;
              gridColumn.fixedRight = false;
              this.initColumns(this.columns);
            }
          },
          {
            label: "Right",
            icon: "mdi mdi-table-column-plus-before",
            command: () => {
              gridColumn.fixed = true;
              gridColumn.fixedRight = true;
              this.initColumns(this.columns);
            }
          },
          {
            label: "Unfreeze",
            icon: "mdi mdi-table-column-remove",
            command: () => {
              gridColumn.fixed = false;
              gridColumn.fixedRight = false;
              this.initColumns(this.columns);
            }
          }
        ]
      });
    };

    var groupMenus: IMenuItem[] = [];

    if ( isHeader == true ) {
      groupMenus.push({
        label: "Add Group " + gridColumn.caption + "",
        // icon: "pi pi-fw pi-refresh",
        command: () => {
          var maxGroupIndex = this.groupColumns.length > 0 ? new TS.List(this.groupColumns).max(d=>d.groupIndex ? d.groupIndex : 0) : 1;
          gridColumn.groupIndex = maxGroupIndex * 10;
          this.initColumns(this.columns);
          this.reload(true);
        }
      });
    }
    
    if ( dataRow != null ) {
      if ( dataRow.colGroup ) {
        groupMenus.push({
          label: "Remove " + dataRow.value.caption + "",
          // icon: "pi pi-fw pi-refresh",
          command: () => {
            dataRow.colGroup.groupIndex = null;
            this.initColumns(this.columns);
            this.reload(true);
          }
        });
      }
    }

    if ( this.groupColumns.length > 0 ) {
      groupMenus.push({
        label: "Remove All",
        // icon: "pi pi-fw pi-refresh",
        command: () => {
          this.groupColumns.forEach(g => { 
            g.groupIndex = 0;
          });
          this.initColumns(this.columns);
          this.reload(true);
        }
      });
    }

    if ( groupMenus.length > 0 ) {
      this.contextMenus.push({
        label: "Grouping",
        icon: "pi pi-fw pi-th-large",
        items : groupMenus
      })
    }

    // var itemSelected = new TS.List(this.itemSelectedIndicator).where((d: any) => d.value == true).toArray();
    // if (itemSelected.length > 0) {
    //   this.contextMenus.push({
    //     label: "Clear Selection",
    //     icon: "pi pi-fw pi-bars",
    //     command: () => {
    //       itemSelected.forEach((d: any) => {
    //         d.value = false;
    //       });
    //     }
    //   });
    // }

    if (this.sortingColumns.length > 0) {
      this.contextMenus.push({
        label: "Clear Sorting",
        icon: "pi pi-fw pi-times",
        command: () => {
          this.sortingColumns = [];
          this.reload(true);
        }
      });
    }

    if (Object.keys(this.filterObject).length > 0) {
      this.contextMenus.push({
        label: "Clear filter",
        icon: "pi pi-fw pi-times",
        command: () => {
          this.filterObject = {};
          this.reload(true);
        }
      });
    }


    //var otherMenus: IMenuItem[] = [];

    if ( isHeader == true ) {
      if ( this.option.Oid_ListView ) {
        this.contextMenus.push({
          label: "Save format",
          icon: "pi pi-fw pi-save",
          command: () => {
            this.saveListViewFormat();
          }
        });
      }
    }

    this.contextMenus.push({
      label: "Show Column Chooser",
      icon: "pi pi-fw pi-table",
      command: (event:any) => {
        console.log(event);
        this.columnChooserOp.toggle(event.originalEvent, this.horizontalScroll.nativeElement);
      }
    });

    // if ( otherMenus.length > 0 ) {
    //   this.contextMenus.push({
    //     label: "Others",
    //     icon: "mdi mdi-cog",
    //     items : otherMenus
    //   });
    // }
    var itemsSelecteds = this.getItemsSelected();
    if (itemsSelecteds.length > 0) {
      this.contextMenus.unshift({
        label: 'Clear Selection',
        command: () => {
          this.clearSelection();
        }
      });

      this.contextMenus.unshift({
        label: "Remove Item" + (itemsSelecteds.length > 1 ? "s" : ""),
        command:async () => {
          this.deletedItems = itemsSelecteds;
          this.deletedItems.forEach(d=>{
            this.listItems[d.rowIndex] = null;
          });
          this.listItems = new TS.List(this.listItems).where(s=>s != null).toArray();
          this.emitValueChange(this.listItems);
          await this.reloadData(true);
        }
      });
    }



    if (this.option.onContextMenu_Show) {
      this.option.onContextMenu_Show.emit({
        menus: this.contextMenus,
        contextMenu: this.contextMenu,
        column: gridColumn,
        data: dataRow ? dataRow.value : null,
        isHeader: isHeader,
        grid : this
      })
    }

  }

  private getHeadersPos(ths): any[] {
    var d: any[] = [];
    $.each(ths, (i, div) => {
      var jDiv = $(div);
      var offset = jDiv.offset();
      var _top = Math.round(offset.top);
      var _left = Math.round(offset.left);
      var _bottom = Math.round(_top + jDiv.outerHeight());
      var _right = Math.round(_left + jDiv.outerWidth());
      d.push({
        el: jDiv,
        top: _top,
        left: _left,
        bottom: _bottom,
        right: _right
      });
    });
    //console.log(d);
    return d;
  }

  //
  //
  //
  @ViewChild("colResizer")
  colResizer: ElementRef;

  onColumn_ResizeStart(): void {
    this.isColumnResizing = true;
    $(this.colResizer.nativeElement).css({
      display: "block"
    });
  }

  onColumn_ResizeDragging($event: DragMoveEvent): void {
    var x = $event.x;
    if (x) {
      $(this.colResizer.nativeElement).css({
        left: this.DragPositionX + x
      });
    }
    //console.log($event);
  }

  onColumn_ResizeEnd($event: DragEndEvent, col: DataTableColumn): void {
    this.DragPositionX = null;
    $(this.colResizer.nativeElement).css({
      display: "none",
      left: 0
    });

    var x = $event.x;
    if (col.fixedRight == true) {
      x = x * -1;
    }

    (col.width as number) += x;
    this.tableWidth += x;
    this.changeDetector.detectChanges();
    setTimeout(() => {
      this.isColumnResizing = false;
    }, 500);
  }

  onColumnResizer_Click($event: any): void {
    $event.preventDefault();
    $event.stopPropagation();
  }

  private DragPositionX: number = null;
  onGhost_ElementCreated($event: GhostElementCreatedEvent): void {
    var leftPositionParent = $(this.element.nativeElement).offset().left;
    this.DragPositionX = $event.clientX - leftPositionParent;
  }

  onDrop(): void { }

  getSortingClass(col: DataTableColumn): any {
    var cls = {};
    var dataField = col.displayDataField ? col.displayDataField : col.dataField;
    var sortCol = this.sortingColumns.find(d => d.dataField == dataField);
    var isNumeric = [DataTypeEnum.Integer, DataTypeEnum.Decimal].includes(
      col.dataType
    );
    if (sortCol) {
      if (sortCol.isDesc == true) {
        cls[
          "sorting fas fa-sort-" + (isNumeric ? "numeric" : "alpha") + "-up-alt"
        ] = true;
      } else {
        cls[
          "sorting fas fa-sort-" + (isNumeric ? "numeric" : "alpha") + "-down"
        ] = true;
      }
    }
    return cls;
  }

  onHeaderClick(col: DataTableColumn): void {
    if (this.isEditing == true) return;
    if (this.isColumnResizing == true) return;
    //if (col.groupIndex > 0 ) return;
    var dataField = col.displayDataField ? col.displayDataField : col.dataField;
    var sortCol = this.sortingColumns.find(d => d.dataField == dataField);
    if (sortCol) {
      if (sortCol.isDesc == false) {
        sortCol.isDesc = true;
      } else if (sortCol.isDesc == true) {
        this.sortingColumns = new TS.List(this.sortingColumns)
          .where(d => d.dataField != dataField)
          .toArray();
      }
    } else {
      this.sortingColumns.push({
        dataField: dataField,
        isDesc: false
      });
    }
    this.paginator.changePage(0);
    //this.reload(true);
    this.changeDetector.detectChanges();
  }

  async onCheckBoxReadOnly_Click($event: any, item:any, col:DataTableColumn): Promise<void> {

    if ( item == undefined ) return Promise.resolve();
    if ( item.key == undefined || item.key == null ) return Promise.resolve();
    var allowEdit = col.allowEdit instanceof Function ? col.allowEdit(item.key) : col.allowEdit;
    //alert(allowEdit);
    if (isNullOrUndefined(allowEdit) == true) allowEdit = false;
    var val = !(item.key[col.dataField] == true ? true : false);
    if ( allowEdit == true ) {
      // var itemSelected = item.Key;
      var commitEditValue = true;
      if (this.option.remoteEditing == true && this.option.Oid_Model) {
        commitEditValue = await this.commitCellValue(col, item.key, val);
      }
      if (commitEditValue) {
        item.key[col.dataField] = val;
      }
    }
    $event.stopPropagation();
    $event.preventDefault();
  }

  onFilterMultiSelect_Change(): void {
    this.reload(false);
  }

  getItemsSelected(): IDataRow[] {
    //var Uids = new TS.List(this.itemSelectedIndicator).where(s => s.value == true).select(s=>s.$uid).toArray();
    var list = new TS.List(this.gridItems).where(g => g.isSelected == true).toArray();
    return list;
  }

  onDateRangeFilter_Show(event: any, date: Calendar, col: DataTableColumn): void {
    $(event.element).attr("tabindex", 0);
    $(event.element).focus();
    $(event.element).keydown(($evt: any) => {
      if ($evt.keyCode == 13) {
        date.toggle();
        if (isNullOrUndefined(this.filterObject[col.dataField]) != true) {
          this.reload();
        }
      } else if ($evt.keyCode == 27) {
        date.toggle();
      }
    });
  }

  onDateRangeFilter_Closed(): void { }

  onDateRangeFilter_Clear(): void {
    this.reload();
  }

  onFilterRow_EnterKey(): void {
    this.reload(true);
  }

  //
  // Arrange Columns
  //

  onColumnHeader_DragStart(): void {
    //$(th).toggleClass("dragging");
  }

  onColumnHeader_DragEnd(evt: DragEndEvent, col: DataTableColumn, th: any, isColumnChooser?:boolean): void {
    var el = $(th);
    //el.toggleClass("dragging");
    // var x =  el.offset().left + evt.x;//+ el.outerWidth() / 2;
    var y = Math.floor(el.offset().top + evt.y);// + el.outerHeight() / 2;
    var x = Math.floor(el.offset().left + evt.x);
    //var y = evt.y;

    if ( this.columnChooserOp.overlayVisible == true && isColumnChooser !== true) {
      var columnChooserEl = $(this.columnChooser.nativeElement);
      var clOffset = columnChooserEl.offset();
      var top = clOffset.top;
      var bottom = top + columnChooserEl.outerHeight();
      var left = clOffset.left;
      var right = left + columnChooserEl.outerWidth();
    
      //alert(x);  
      if ((x >= left && x <= right) && (y >= top && y <= bottom)) {
        col.visible = false;
        this.initColumns(this.columns);
        return;
      }
    }

    var gridHeaderTop = Math.floor($(this.gridHeader.nativeElement).offset().top);
    var bottom = Math.floor(gridHeaderTop + $(this.gridHeader.nativeElement).outerHeight());
    var isHeader = (y >= gridHeaderTop) && y <= bottom;
    if (isHeader !== true) return;
    var baseThs: any = null;
    if (col.fixed == false || col.fixed == undefined || col.fixed == null) {
      baseThs = $(this.gridHeader.nativeElement).find(".base-table th.header-column.data-field").not('.dragging');
    } else if (col.fixed == true && col.fixedRight !== true) {
      baseThs = $(this.gridHeader.nativeElement).find(
        ".fixed-column-left th.header-column"
      );
    } else if (col.fixedRight == true) {
      baseThs = $(this.gridHeader.nativeElement).find(
        ".fixed-column-right th.header-column"
      );
    };
    var thsPos = this.getHeadersPos(baseThs);
    //console.log({ x: x, thsPos: new TS.List(thsPos).select((d: any) => $(d.el).text() + " left: " + d.left + ", right: " + d.right).toArray() });
    x += 20;
    var res = new TS.List(thsPos).where(d => x >= (d.left) && x <= (d.right)).toArray();
    if (res.length == 0) return;
    if (!res[0].el.attr("id")) return;
    var id = res[0].el.attr("id").replace("col-", "");
    var gridColumn = this.columns.find(d => d.uid == id);
    if (gridColumn) {
      if (gridColumn.dataField == col.dataField) return;
      col.visible = true;
      if (gridColumn.seqNo > col.seqNo) {
        col.seqNo = gridColumn.seqNo + 10;
      } else {
        col.seqNo = gridColumn.seqNo - 10;
      };
      this.columns = new TS.List(this.columns).orderBy(d => d.seqNo).toArray();
      this.initColumns(this.columns);
    }
    //
    //
    //
  }

  onColumnHeader_Dragging(): void { }

  onColumnHeader_onGhostElementCreated($event: GhostElementCreatedEvent, isColumnChooser?: boolean): void {
    $($event.element).toggleClass('ghost-column-header');
    console.log($($event.element).attr('class'));
  }

  horizontalScrollWidth: number = 0;
  verticalScrollHeight: number = 0;
  onScroll_Resized(event, tag): void {
    if (tag == 'horizontal') {
      var width = event.newWidth;
      var parentWidth = $(this.horizontalScroll.nativeElement).outerWidth();
      if (width > parentWidth) {
        this.verticalScrollHeight = 18;
      } else {
        this.verticalScrollHeight = 0;
      }
    } else {
      var height = event.newHeight;
      var parengHeight = $(this.verticalScroll.nativeElement).outerHeight();
      if (height > parengHeight) {
        this.horizontalScrollWidth = 17;
      } else {
        this.horizontalScrollWidth = 0;
      }
    }
    this.changeDetector.detectChanges();
  }

  //
  // NGMODEL
  //
  isRemoteData: boolean = true;
  writeValue(val: any[]): void {
    var items: any[] = [];
    this.isRemoteData = false;
    if (val) {
      items = val;
    };
    this.lastRowHeight = 0;
    this.listItems = items;
    this.rowsPerPageOptions = [100, 150, 200];
    this.TotalNumberRecordsPerPage = 100;
    if (this.columns.length > 0) {
      this.reload();
    }
  }

  readonly defaultImageSrc: string = "assets/default-img.png"
  getImageSrcValue(value: string, col: DataTableColumn): string {
    if (value) {
      return DataService.API_URL.replace("/api","") + `Content/Thumbnail/${value}`;
     }
    return null;
  }

  tableHeaderHeight: number = 0;
  onTableHeader_Resized(height: number): void {
    this.tableHeaderHeight = height;
    this.changeDetector.detectChanges();
  }

  LeftFixedHeaderWidth : number = 0;
  onLeftFixedHeader_Resized(width:number):void {
    this.LeftFixedHeaderWidth = width;
    this.changeDetector.detectChanges();
  }

  // onRow_Resized(row: IDataRow, rowIndex: number, table: any, rowHeight: any): void { 
  //   if (row.groupIndex > 0) return;
  //   if (rowHeight > row.height) { 
  //     var trs = $(this.gridBody.nativeElement).find('tr.r-' + rowIndex + ' td'); //.css({ 'height': rowHeight });
  //     //trs.css({ 'height': rowHeight });
  //     row.height = rowHeight;
  //   }
  // }

  ngOnInit() { }

}

interface SortColumn {
  dataField: string;
  isDesc: boolean;
}

export enum DataTypeEnum {
  String = "string",
  Integer = "integer",
  Decimal = "decimal",
  Date = "date",
  DateTime = "dateTime",
  Boolean = "boolean",
  Time = "time",
  Custom = "custom",
  Image = "image"
}

// export enum EditorTypeEnum {
//   TextBox = 1,
//   NumberBox = 2,
//   CheckBox = 3,
//   DateBox = 4,
//   SelectBox = 5,
//   ComboBox = 6,
//   ListBox =
// }

export enum FilterTypeEnum {
  TextBox = 'TextBox',
  AutoComplete = 'AutoComplete',
  SelectBox = 'SelectBox',
  NumberBox = 'NumberBox',
  MultiSelectBox = 'MultiSelectBox',
  DateBox = 'DateBox',
  DateRangeBox = 'DateRangeBox',
  BooleanSelectBox = "BooleanSelectBox"
}

export enum SummaryTypeEnum {
  Sum = 1,
  Average = 2
}

export enum TextAlignmentEnum {
  Left = 1, Right = 2, Center = 3
}

export class DataTableColumn {
  oid?: string;
  dataField: string;
  caption?: string;
  dataType?: DataTypeEnum = DataTypeEnum.String;
  fixed?: boolean = false;
  uid?: number;
  fixedRight?: boolean = false;
  width?: number = null;
  visible?: boolean = true;
  groupIndex?: number = null;
  //editable?: boolean = false;
  seqNo?: number;
  colNo?: number;
  isFilter?: boolean;
  filterEditorType?: ControlTypeEnum;
  dataSource?:string;
  id_propertyType?:number;
  //
  //
  //
  allowEdit?: boolean | Function = null;
  filterType?: FilterTypeEnum;
  editorType?: ControlTypeEnum;
  summaryText?: (col: DataTableColumn) => string;
  summaryType?: SummaryTypeEnum = null;
  textAlignment?: TextAlignmentEnum;
  onEditorOption_Initialized?: (row: IDataRow ,opt: BaseAppControlOption) => void = null;
  //
  //
  //
  onColumn_ValueChange?: (det: IRowValueChange) => void = null;
  displayDataField?: string = null;
  displayText?: (key, val?: any) => string = null;
  required?: boolean | Promise<boolean> = null;
}

export class DataTableOption extends BaseAppControlOption {
  columns?: DataTableColumn[] | any[];
  apiUrl?: string;
  editApiUrl?: string;
  caption?: string;
  Oid_Model?: string;
  propertyKey?: string = "ID";
  sourceKey?: string = null;
  Oid_ListView?: string = null;
  Oid_DetailView?: string = null;
  autoLoad?: boolean = true;
  showFilter?: boolean = true;
  allowEditing?: boolean = false;
  newRowEditing?: boolean = false;
  multiSelect?: boolean = false;
  actionRowItems?: IActionRowItem[] = [];
  //
  //
  //
  remoteEditing?: boolean = false;
  //
  //   EVENTS
  //
  onView_Initialized?: (dt: AppDataTableComponent) => void = null;
  customizeColumns?: (cols: DataTableColumn[]) => DataTableColumn[] = null;
  onColumns_Initialized?: (cols: DataTableColumn[]) => void = null;
  onRow_ValueChange?: (det: IRowValueChange) => void = null;
  onCell_DoubleClick?: EventEmitter<ICellEvent> = null;
  onCell_EnterKey?: EventEmitter<ICellEvent> = null;
  allowCell_Edit?: (key: any, cols: DataTableColumn, isNewCellEdit?: boolean) => Promise<boolean> = null;
  onBefore_LoadData?: EventEmitter<IPagingOption> = null;//new EventEmitter<IPagingOption>();
  onRowClass_Initialized?: (cls: any, item: IDataRow, rowIndex: number) => void = null;
  onContextMenu_Show?: EventEmitter<IContextMenuEvent> = null;

  constructor() {
    super();
  }

}

export interface IRowValueChange {
  control: BaseAppControl<any>;
  col: DataTableColumn;
  value: any;
  itemKey: any;
  itemSelected?: any;
  isNewRow: boolean;
}


@Pipe({
  name: 'cellDisplay'
})
export class cellDisplayPipe implements PipeTransform {

  cp: CurrencyPipe;

  constructor() {
    this.cp = new CurrencyPipe('EN', '')
  }

  transform(val: any, col: DataTableColumn, row: any, isfooterCell: boolean): string {
    var type = col.dataType;
    var _value = val;
    if (type && val) {
      switch (type) {
        case DataTypeEnum.String:
          break;
        case DataTypeEnum.Integer:
          break;
        case DataTypeEnum.Decimal:
          _value = this.cp.transform(val, '', 'symbol', '1.2-2');
          break;
        case DataTypeEnum.DateTime:
          _value = moment(val).format("MM/DD/YYYY hh:mm A");
          break;
        case DataTypeEnum.Date:
          _value = moment(val).format("MM/DD/YYYY");
          break;
        case DataTypeEnum.Boolean:
          break;
      }
    } else {
      if (val) {
        var _type = typeof val;
        //console.log(col.dataField, val, _type == "string");
        if ((new Date(val).toString()) != "Invalid Date" && _type == "string" && val.toString().includes("T")) {
          _value = moment(val).format("MM/DD/YYYY hh:mm A");
        } else {
        }
      }
    }
    if ( row ) {
        if (isfooterCell !== true) {
          if (col.displayDataField) {
            _value = row[col.displayDataField];
          }

          if (col.displayText) {
            _value = col.displayText(row, _value);
          }
        }
    }
    return _value;
  }
}

//
//
//

export interface IContextMenuEvent {
  menus: MenuItem[];
  contextMenu: ContextMenu;
  column: DataTableColumn;
  data: any;
  isHeader: boolean;
  grid: AppDataTableComponent;
}

export interface IDataRow {
  value: any;
  rowKey: any;
  groupIndex?: number;
  rowIndex: number;
  groupField?: string;
  groupValue?: any;
  id?: string;
  colGroup?: DataTableColumn;
  height: number;
  isSelected?: boolean;
}

export interface IActionRowItem {
  class?: string;
  icon?: string;
  command?: (evt: any, rowKey: any) => void;
  visible?: boolean;
  url?: (rowKey: any) => string | string[];
}