import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ElementRef, Input, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { LabelPositionEnum } from '../appControlContainer/appControlContainer.component';

@Component({
  selector: 'app-list-box',
  templateUrl: './appListBox.component.html',
  styleUrls: ['./appListBox.component.less'],
  providers:[
    {
      provide: BaseAppControl,
      useExisting: AppListBoxComponent
    }
  ]
})
export class AppListBoxComponent extends BaseAppControl<number> {

  private preventSingleClick = false;
  private timer: any;

  @ViewChild('container')
  listContainer: ElementRef;

  itemSelected: any[] = [];

  currentItemIndex: number = null;

  constructor(private http: HttpClient, el: ElementRef, ch :ChangeDetectorRef) {
    super(el, ch);
  }

  items: any[] = [];

  @Input()
  height: string | number;

  valueKey: string = "id";

  displayKey: string = "first_name";

  @Input()
  showCheckBox?: boolean = false;

  ngOnInit(): void {
    super.ngOnInit();
    this.labelPosition = LabelPositionEnum.top;
    this.reload();
    this.update();
  }

  reload(): void {
    this.items = [];
    this.http
      .get("assets/mock.json")
      .toPromise()
      .then((d: any[]) => {
        this.items = d;
      });
  }

  update(): void {
    var _height: number = 0;
    switch (typeof this.height) {
      case "string":
        $(this.listContainer.nativeElement).css({ 'height': this.height + "" });
        break;
      case "number":
        $(this.listContainer.nativeElement).height(this.height);
        break;
    }
  }

  onItemClick(event: MouseEvent, item: any, index: number): void {
    if (this.showCheckBox === true) return;
    this.currentItemIndex = index;
    this.preventSingleClick = false;
    const delay = 200;
    this.timer = setTimeout(() => {
      if (!this.preventSingleClick) { }
    }, delay);
  }


  doubleClick(evt:Event): void {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.onItemSelected();
  }

  onItemSelected(): void {

  }

  onKeyDown(evt: KeyboardEvent): void {
    switch (evt.keyCode) {
      case 13:
        this.onItemSelected();
        break;
      case 40:
      case 38:
        evt.stopPropagation();
        evt.preventDefault();
        if (this.currentItemIndex == null) {
          this.currentItemIndex = 0;
        } else {
          if (evt.keyCode == 38 && this.currentItemIndex != 0) {
            this.currentItemIndex--;
          }

          if (evt.keyCode == 40 && this.currentItemIndex != (this.items.length - 1)) {
            this.currentItemIndex++;
          }
        }
        let listContainer = $(this.listContainer.nativeElement);
        let elItem = $(listContainer.find('.jf-item')[this.currentItemIndex]);
        let offset = listContainer.offset();

        let parentTop = offset.top;
        let parentBottom = parentTop + listContainer.outerHeight();

        let top = elItem.offset().top;
        var height = elItem.outerHeight();
        let bottom = top + height;

        let scrollTop = listContainer.scrollTop();

        if (evt.keyCode == 40) {
          //down
          if (bottom > parentBottom) {
            listContainer.scrollTop(scrollTop + height);
          }
        } else if (evt.keyCode == 38) {
          //up
          if (top < parentTop) {
            listContainer.scrollTop(scrollTop - height);
          }
        }
        break;
    }
    super.onKeyDown(evt);
  }

}


export class IAppListBoxOption extends BaseAppControlOption { }
