import { FilterCriteriaType } from "./../appControlContainer/appControlContainer.component";
import {
  IMenuItem,
  DetailView_Detail,
  PropertyTypeEnum,
} from "./../class_helper";
import { AppDateBoxComponent } from "./../appDateBox/appDateBox.component";
import { BaseAppControl } from "src/utils/controls";
import {
  Component,
  OnInit,
  ContentChildren,
  QueryList,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  ContentChild,
} from "@angular/core";
import { IFormValidation } from "../appModal/appModal.component";
import { IControlValueChanged } from "../_base/baseAppControl";
import { isNullOrUndefined } from "util";
import * as moment from "moment";
import { Accordion } from "primeng/accordion";
import * as TS from "linq-typescript";

@Component({
  selector: "app-form",
  templateUrl: "./appForm.component.html",
  styleUrls: ["./appForm.component.less"],
})
export class AppFormComponent implements OnInit {
  constructor(private changeDetector: ChangeDetectorRef) {}

  @Input()
  CurrentObject: any = null;

  @ContentChildren(BaseAppControl, { descendants: true })
  controls: QueryList<BaseAppControl<any>>;

  @ContentChild("filterAccordioView")
  filterAccordion: Accordion;

  @Output()
  onFormValueChanged?: EventEmitter<IControlValueChanged> = new EventEmitter();

  isDirty: boolean = false;
  isDisabled: boolean = false;

  filterValue?: any = {};

  initControls: any[] = [];

  IncludeFilterList: string[] = [];
  details: DetailView_Detail[] = [];

  setFormDisabled(disabled: boolean = true): void {
    if (this.initControls.length > 0) {
      this.controls.forEach((c) => {
        this.initControls.forEach((initControl) => {
          if (c.name == initControl.name) {
            if (initControl.readOnly != true) {
              c.readOnly = disabled;
              c.ch.detectChanges();
            }
          }
        });
      });
    } else {
      this.controls.forEach((c) => {
        if (c.readOnly != true) {
          c.readOnly = disabled;
          c.ch.detectChanges();
        }
      });
    }

    this.isDisabled = disabled;
    this.changeDetector.detectChanges();
  }

  setDirty(isDirty?: boolean): void {
    this.controls.forEach((c) => {
      c.isDirty = isDirty;
    });
    this.isDirty = isDirty;
  }

  ngOnInit(): void {}

  isValid(): boolean {
    return this.getValidations().length == 0;
  }

  getValidations(): IFormValidation[] {
    var validations: IFormValidation[] = [];
    this.controls.forEach((c) => {
      c.isDirty = true;
      if (c.isInvalid() == true) {
        validations.push({
          message: `${c.caption} field is required.`,
        });
      }
    });
    return validations;
  }

  getFilterFormValue(): IFilterFormValue[] {
    var filterForm: IFilterFormValue[] = [];

    this.controls.forEach((c) => {
      if (c.appContainer.filterCriteria) {
        var f = c.name;
        var det = this.details.find((d) => d.Name == f);
        var value = this.CurrentObject[f];

        if (det != undefined) {
          if (det.ID_PropertyType == PropertyTypeEnum.Date) {
            if (value) {
              if (Array.isArray(value) == true) {
                var _values = [];

                (value as any[]).forEach((d) => {
                  _values.push(moment(d).format("YYYY-MM-DDTHH:mm:ss"));
                });

                value = _values;
              } else {
                value = moment(value).format("YYYY-MM-DDTHH:mm:ss");
              }
            }
          }

          if (value) {
            filterForm.push({
              dataField: f,
              filterCriteriaType: this.filterValue[f],
              value: value,
              propertyType: det.ID_PropertyType,
            });
          }
        }
      }
    });

    return filterForm;
  }

  clearFilter(): void {
    this.CurrentObject = {};
    this.IncludeFilterList = [];
    this.filterAccordion.tabs.forEach((t) => {
      t.selected = false;
      //t.toggle(false);
    });
    this.controls.forEach((c) => {
      c.isDirty = false;
      c.required = false;
    });
  }

  ngAfterContentInit(): void {
    if (this.filterAccordion) {
      this.filterAccordion.onOpen.subscribe((e) => {
        var targetMenus;
        var dataField;

        if (!isNullOrUndefined(e.originalEvent["path"])) {
          targetMenus = new TS.List(e.originalEvent.path)
            .where((d: any) => d.nodeName == "P-ACCORDIONTAB")
            .toArray();
          dataField = (targetMenus[0] as any).id;
        } else {
          dataField = e.originalEvent.originalTarget.parentNode.parentNode.id;
          if (isNullOrUndefined(dataField)) dataField = "";

          if (dataField.length == 0) {
            dataField =
              e.originalEvent.originalTarget.parentNode.parentNode.parentNode
                .id;
          }
        }

        if (isNullOrUndefined(dataField)) dataField = "";

        if (dataField.length > 0) {
          this.IncludeFilterList.push(dataField);
          var ctrl = this.controls.find((d) => d.name == dataField);
          ctrl.required = true;
          ctrl.isDirty = true;
        }
      });

      this.filterAccordion.onClose.subscribe((e) => {
        var targetMenus;
        var dataField;

        if (!isNullOrUndefined(e.originalEvent["path"])) {
          targetMenus = new TS.List(e.originalEvent.path)
            .where((d: any) => d.nodeName == "P-ACCORDIONTAB")
            .toArray();
          dataField = (targetMenus[0] as any).id;
        } else {
          dataField = e.originalEvent.originalTarget.parentNode.parentNode.id;
          if (isNullOrUndefined(dataField)) dataField = "";

          if (dataField.length == 0) {
            dataField =
              e.originalEvent.originalTarget.parentNode.parentNode.parentNode
                .id;
          }
        }

        if (dataField.length > 0) {
          this.IncludeFilterList = new TS.List(this.IncludeFilterList)
            .where((s) => s != dataField)
            .toArray();
          var ctrl = this.controls.find((d) => d.name == dataField);
          ctrl.required = false;
          ctrl.isDirty = false;
        }
      });
    }

    this.controls.forEach((c) => {
      if (c.filterMode == true) {
        c.appContainer.onFilterCriteria_Changed.subscribe((e: IMenuItem) => {
          if (e.id == FilterCriteriaType.Exclude) {
            this.filterValue[c.name] = undefined;
            c.readOnly = true;
          } else {
            this.filterValue[c.name] = e.id;
            c.readOnly = false;
          }
        });
      }
      c.onControlValue_Changed.subscribe((evt: IControlValueChanged) => {
        this.onFormValueChanged.emit(evt);
        this.isDirty = true;
      });
    });
  }
}

export interface IFilterFormValue {
  alias?: "",
  dataField?: string;
  filterCriteriaType?: FilterCriteriaType;
  value?: any;
  propertyType?: number;
}
