import { DetailView_Detail } from './../class_helper';
import { Component, OnInit, Input, EventEmitter, Output, Injector, SimpleChanges, ViewChild, ElementRef, Injectable, Directive, ChangeDetectorRef } from "@angular/core";
import { ControlValueAccessor } from "@angular/forms";
import { HttpClient, HttpHandler, HttpXhrBackend } from '@angular/common/http';
import { AppControlContainerComponent, LabelPositionEnum } from '../appControlContainer/appControlContainer.component';
import { isFunction, isBoolean, isNullOrUndefined } from 'util';
import * as $ from 'jquery';


export abstract class BaseAppControl<T> implements OnInit, ControlValueAccessor {

  @ViewChild('input')
  input: ElementRef;
  
  @Input() titleTooltip: string = '';
  @Input() tooltipDisabled: boolean = true;

  constructor(public el: ElementRef, public ch: ChangeDetectorRef) {
    var _ = this;
    $(el.nativeElement).keydown(($event) => {
      if ($event.keyCode == 13) {
        _.onEnterKey.emit(
          {
            name: this.name,
            component: this
          }
        );
      }
    });
  }

  @Input()
  oid_Control?: string = null;

  @Input()
  max?: number = null;

  tabIndex: number = 1;

  httpClient: HttpClient;

  value?: T = null;

  @Input()
  name?: string = null;

  @Input()
  caption?: string = null;

  @Input()
  required?: any = false;

  @Input("placeholder")
  placeholder: string = "";

  @Output()
  onModelValue_Changed?: EventEmitter<IValueChanged> = new EventEmitter();

  @Output()
  onControlValue_Changed?: EventEmitter<IControlValueChanged> = new EventEmitter();

  @Output()
  onEnterKey?: EventEmitter<IEnterKey> = new EventEmitter();

  @Input()
  showLabel?: boolean = true;

  @Input()
  readOnly?: boolean = false;

  @Input()
  disabled?: boolean = false;

  @Input()
  filterMode?:boolean = false;

  @Input()
  labelPosition?: LabelPositionEnum = LabelPositionEnum.left;

  @Input()
  option?: BaseAppControlOption;

  @ViewChild('appContainer', { static: true })
  appContainer: AppControlContainerComponent;

  isValidated: boolean = false;
  isFocused: boolean = false;

  protected emitValueChange: any = (val) => { };

  @Input()
  validateOnSubmit: boolean = true;

  isDirty: boolean = false;

  ngOnInit() {
    const injector = Injector.create({
      providers: [
        { provide: HttpClient, deps: [HttpHandler] },
        { provide: HttpHandler, useValue: new HttpXhrBackend({ build: () => new XMLHttpRequest }) },
      ],
    });
    this.httpClient = injector.get(HttpClient);
  }

  initOption(): void { }

  ngOnChanges(ch: SimpleChanges): void {
    if (this.caption == null) {
      //if ( this.isEmptyString(this.name) ) return;
      if (this.name) {
        var result = this.name.replace(/([A-Z])/g, " $1");
        this.caption = result.charAt(0).toUpperCase() + result.slice(1);
      }
    }

    if(this.appContainer){

      this.appContainer.label = this.caption;
      this.appContainer.labelPosition = this.labelPosition;
      this.appContainer.filterMode = this.filterMode;
      this.appContainer.showLabel = this.showLabel;
    }
  }


  getCssClass(): any {
    return {
      'jf-required': this.isRequired(),
      'jf-validated': this.isValid(),
      'jf-invalid': this.isInvalid(),
      'jf-focused': this.isFocused,
      'jf-no-label': this.showLabel == false
    }
  }

  isRequired(): boolean {
    var value: boolean = false;
    if (isFunction(this.required) == true) {
      value = this.required();
    } else {
      value = this.required;
    }
    if (isBoolean(value) == false) throw new Error(`${this.name} Control value of '${value}' is not assignable to Boolean`)
    return value;
  }

  isInvalid(): boolean {
    var required = this.isRequired();
    return this.isDirty == true && required == true && isNullOrUndefined(this.value) == true;
  }

  isValid(): boolean {
    if (this.validateOnSubmit == false) return true;
    if (this.isValidated == false) return false;
    return true;
  }

  onFocus($event: Event) {
    this.isFocused = true;
  }

  onBlur($event: Event) {
    this.isFocused = false;
    this.appContainer.disabledTooltip = true;
  }


  KeyDown: EventEmitter<KeyboardEvent> = new EventEmitter();

  onKeyDown($event: KeyboardEvent) {
    this.KeyDown.emit($event);
  }

  focus(): void {
    if (!this.input) return;
    $(this.input.nativeElement).focus();
    //alert('focus');
  }

  isEmptyString(): boolean {
    if (this.value == undefined || this.value == null) return true;
    return !this.value || !/[^\s]+/.test(this.value + "");
  }

  clearValue(): void {
    this.value = null;
    $(this.input.nativeElement).focus();
  }

  onControl_ValueChanged(): void {
    this.isDirty = true;
    this.onControlValue_Changed.emit({
      name: this.name,
      newValue: this.value,
      control: this
    });
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.emitValueChange = fn;
  }

  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

  afterLoad(): void { }

  ngAfterViewInit(): void {
    var _ = this;
    //console.log(this.appContainer);
    //if (this.name == null) throw new Error("Control name must be defined");
    this.appContainer.onClearButtonClick = () => {
      _.clearValue();
    };

    this.afterLoad();
  }
}

export abstract class BaseAppControlOption {
  name?: string;
  required?: boolean;
}

export interface IValueChanged {
  name?: string;
  oldValue?: any;
  newValue?: any;
  control?: BaseAppControl<any>
}

export interface IEnterKey {
  component?: BaseAppControl<any>;
  name?: string;
}

export interface IControlValueChanged {
  name?: string;
  oldValue?: any;
  newValue?: any;
  control?: BaseAppControl<any>
}
