import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallLoadingScreenComponent } from './small-loading-screen.component';

describe('SmallLoadingScreenComponent', () => {
  let component: SmallLoadingScreenComponent;
  let fixture: ComponentFixture<SmallLoadingScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallLoadingScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallLoadingScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
