import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "small-loading-screen",
  templateUrl: "./small-loading-screen.component.html",
  styleUrls: ["./small-loading-screen.component.less"],
})
export class SmallLoadingScreenComponent implements OnInit {
  @Input() isLoading: boolean = false;
  @Input() target: any;

  constructor() {}

  ngOnInit(): void {}
}
