﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VetCloudImageMigrationTool.DataAccess.FileUpload;
using VetCloudImageMigrationTool.Tools;
using VetCloudImageMigrationTool.Tools.Migration;
using static Dropbox.Api.TeamLog.TimeUnit;

namespace VetCloudImageMigrationTool.Business
{
    public class Migrate
    {
        private int counter = 0;
        private int limiter = 5;
        private IMigrationService migrationService = new GoogleDriveService();
        List<Data.ForUploadToDropbox> forUploadToDropboxWithErrorList = new List<Data.ForUploadToDropbox>();


        DataAccess.SystemManagement.ForUploadToDropbox sysManageDA = new DataAccess.SystemManagement.ForUploadToDropbox();
        public async Task RunAsync()
        {
            var forUploadToDropboxList = new List<Data.ForUploadToDropbox>();

            forUploadToDropboxList.AddRange(sysManageDA.GetRecordsPaging());

            migrationService.InitializeRepositoryFolder(forUploadToDropboxList);

            int totalProcessCount = 2;
            int currentProcessCount = 0;

            List<Task> tastList = new List<Task>();

            foreach (var forUploadToDropbox in forUploadToDropboxList)
            {
                RunUploadAsync(forUploadToDropbox);
            }

            if (this.forUploadToDropboxWithErrorList.Count > 0)
            {
                Console.WriteLine("");
                Console.WriteLine("Reprocess For Upload with Error List");

                foreach (var forUploadToDropbox in this.forUploadToDropboxWithErrorList)
                {
                    RunUploadAsync(forUploadToDropbox);
                }
            }
        }

        public async void RunUploadAsync(Data.ForUploadToDropbox forUploadToDropbox)
        {
            try
            {
                await DoUploadAsync(forUploadToDropbox);
            }
            catch (Exception ex)
            {
                this.forUploadToDropboxWithErrorList.Add(forUploadToDropbox);

                this.sysManageDA.SaveErrorUpload(forUploadToDropbox, ex.Message);
            }
        }

        private async Task DoUploadAsync(Data.ForUploadToDropbox forUploadToDropbox)
        {
            var errorCount = 0;

            string gDriveFolderRepositoryFolder = AppSettings.Instance.MigrationAccountSettings.GoogleDrive.RepositoryFolder;
            string folderString = forUploadToDropbox.ImageFilePath.StartsWith("Image/", StringComparison.OrdinalIgnoreCase) ? "Image" : "Thumbnail";
            string wwwContentFoler = VetCloudDatabaseDirectoryService.GetAPIContentImageFolderByDBName(forUploadToDropbox.DatabaseName);
            string localFilePath = wwwContentFoler + @"/" + forUploadToDropbox.ImageFilePath;
            string dropboxFolderPath = $"{gDriveFolderRepositoryFolder}/{GlobalFx.ConvertToValidFileName(forUploadToDropbox.GUID_Company)}/{folderString}";

        start:
            try
            {
                string message = "";
                var info = await migrationService.UploadFileAsync(localFilePath, dropboxFolderPath);

                forUploadToDropbox.FileID = info.FileID;
                forUploadToDropbox.ImageUrl = info.GeneratedUrl;

                sysManageDA.SaveUpload(forUploadToDropbox);

                message = $"{localFilePath} - {dropboxFolderPath} - Assigned URL: {info.GeneratedUrl}";

                if (!sysManageDA.IsMultipleOriginalImageFileName(forUploadToDropbox))
                {
                    FileInfo deletedFile = new FileInfo(localFilePath);
                    deletedFile.Delete();
                    message += $" - Deleted in local";
                }

                Console.WriteLine(message);

                counter--;
            }
            catch (Exception ex)
            {
                errorCount++;
                string errorMessage = $"{localFilePath} - {dropboxFolderPath} - Error Count: {errorCount} - {ex.Message}";

                Console.WriteLine(errorMessage);

                if (errorCount < 2)
                {
                    goto start;
                }
                else {

                    this.sysManageDA.SaveErrorUpload(forUploadToDropbox, errorMessage);
                }
                counter--;
            }
        }
    }
}
