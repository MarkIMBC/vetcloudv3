﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;
using VetCloudImageMigrationTool.Tools;
using VetCloudImageMigrationTool.Tools.Migration;

namespace VetCloudImageMigrationTool
{
    internal class Program
    {
        const int SW_MINIMIZE = 6;

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static async Task Main(string[] args)
        {
            bool isMinimizeWindow = AppSettings.Instance.IsMinimizeWindow;

            if (isMinimizeWindow)
            {
                IntPtr hWnd = GetConsoleWindow();
                ShowWindow(hWnd, SW_MINIMIZE);
            }

            Console.WriteLine($"Async Run");
            var dateStart = DateTime.Now;
            Console.WriteLine($"Date Start: { dateStart.ToString("HH:mm:ss ffff")}");

            var mirgationProcess = new Business.Migrate();
            await mirgationProcess.RunAsync();

            var dateEnd = DateTime.Now;

            Console.WriteLine($"Date End: {dateEnd.ToString("HH:mm:ss ffff")}");

            var timeElapsed = dateEnd - dateStart;

            Console.WriteLine($"Time Elapsed: {timeElapsed.ToString(@"dd\.hh\:mm\:ss")}");

            Console.WriteLine($"Program will exit in 10 seconds.");
            await Task.Delay(200000);
        }
    }
}
