﻿using System;
using System.Data;
using System.Reflection;

namespace VetCloudImageMigrationTool.Tools
{
    public class PropertyValueSetterHelper
    {
        public static PropertyInfo[] GetProperties(object obj)
        {
            Type t = obj.GetType();
            PropertyInfo[] props = t.GetProperties();

            return props;
        }

        public static void setProperties(object obj, DataRowView dr)
        {
            PropertyInfo[] propertyInfos = GetProperties(obj);

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (dr.DataView.Table.Columns.Contains(propertyInfo.Name))
                {

                    if (propertyInfo != null)
                    {
                        dynamic value = dr[propertyInfo.Name];
                        propertyInfo.SetValue(obj, value, null);
                    }
                }
            }
        }
        public static void setProperties(object obj, DataRow dr)
        {
            PropertyInfo[] propertyInfos = GetProperties(obj);

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (dr.Table.Columns.Contains(propertyInfo.Name))
                {

                    if (propertyInfo != null)
                    {
                        dynamic? value = dr[propertyInfo.Name];
                        if (value is System.DBNull) value = null;

                        propertyInfo.SetValue(obj, value, null);
                    }
                }
            }

        }

    }
}
