﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VetCloudImageMigrationTool.Data;

namespace VetCloudImageMigrationTool.Tools.Migration
{
    public class GoogleDriveService : IMigrationService
    {
        private readonly DriveService _driveService;

        public GoogleDriveService()
        {
            string exePath = AppDomain.CurrentDomain.BaseDirectory;
            string credentialsPath = exePath + @"/google-drive-credentials.json";
            string[] Scopes = { DriveService.Scope.Drive };

            UserCredential credential;
            using (var stream = new FileStream(credentialsPath, FileMode.Open, FileAccess.Read))
            {
                string credPath = exePath + @"/token";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            _driveService = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "VetCloud Image Migration Tool App v1",
            });
        }

        public void InitializeRepositoryFolder(List<Data.ForUploadToDropbox> forUploadToDropboxList)
        { 
            List<string> pathList = new List<string>();

            Console.WriteLine("Initiating Repository Folder");

            foreach (Data.ForUploadToDropbox forUploadToDropbox in forUploadToDropboxList.OrderBy(u => u.Name_Company).ToList())
            { 
                string path = GoogleDriveService.GetGoogleDriveFolder(forUploadToDropbox);

                if (!pathList.Contains(path))
                {
                    this.GetOrCreateFoldersAsync(path);

                    Console.WriteLine(forUploadToDropbox.Name_Company + " - " + path);

                    pathList.Add(path);
                }
            }

            Console.WriteLine("");
        }

        public async Task<FileUploadInfo> UploadFileAsync(string localFilePath, string googleDriveFolderPath)
        {
            var folderId = GetOrCreateFoldersAsync(googleDriveFolderPath);

            var fileMetadata = new Google.Apis.Drive.v3.Data.File
            {
                Name = Path.GetFileName(localFilePath),
                Parents = new List<string> { folderId }
            };

            using (var stream = new FileStream(localFilePath, FileMode.Open))
            {
                var request = _driveService.Files.Create(fileMetadata, stream, GetMimeType(localFilePath));
                request.Fields = "id, name, size, parents, webViewLink, thumbnailLink";
                var uploadProgress = await request.UploadAsync();

                if (uploadProgress.Status == UploadStatus.Completed)
                {
                    var uploadedFile = request.ResponseBody;
                    var url = $"https://drive.google.com/uc?export=download&id={uploadedFile.Id}";

                    return new FileUploadInfo
                    {
                        FileID = uploadedFile.Id,
                        SourceFilePath = localFilePath,
                        DestinationFileFolder = googleDriveFolderPath,
                        GeneratedUrl = url,
                        GeneratedThumbnailUrl = uploadedFile.ThumbnailLink
                    };
                }
                else
                {
                    throw new Exception($"File upload failed: {uploadProgress.Exception}.");
                }
            }
         
        }
        public async Task CreateFolder(string folderPath)
        {
            
        }
        public async Task<bool> FolderExists(string folderPath)
        {
            var request = _driveService.Files.List();
            request.Q = $"mimeType='application/vnd.google-apps.folder' and name='{folderPath}' and trashed=false";
            request.Fields = "files(id, name)";
            var result = await request.ExecuteAsync();

            return result.Files.Count > 0;
        }
        public string GetOrCreateFoldersAsync(string googleDriveFolderPath)
        {
            var folders = ParseFolderPath(googleDriveFolderPath);
            string rootFolderName = "root";
            List<string> nestedFolders = new List<string>();

            if (folders != null)
            {
                rootFolderName = folders[0];
                nestedFolders = folders.GetRange(1, folders.Count - 1);
            }

            string parentId = CreateOrGetFolderId("root", rootFolderName);

            foreach (var folderName in nestedFolders)
            {
                parentId =  CreateOrGetFolderId(parentId, folderName);
            }

            return parentId;
        }
        static List<string> ParseFolderPath(string path)
        {
            string[] parts = path.Split('/');

            if (parts.Length >= 2)
            {
                List<string> folders = new List<string>(parts);
                return folders;
            }
            else
            {
                return null;
            }
        }
        private string CreateOrGetFolderId(string parentId, string folderName)
        {
            string query = $"mimeType='application/vnd.google-apps.folder' and name='{folderName}' and '{parentId}' in parents and trashed=false";

            var folder = new Google.Apis.Drive.v3.Data.File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder",
                Parents = new List<string> { parentId }
            };

            var request = _driveService.Files.List();
            request.Q = query;
            var result = request.Execute();

            if (result.Files.Count > 0)
            {
                return result.Files[0].Id;
            }
            else
            {
                var requestCreate = _driveService.Files.Create(folder);
                requestCreate.Fields = "id";
                var folderResult = requestCreate.Execute();

                Console.WriteLine("Created");

                return folderResult.Id;
            }
        }
        private string GetMimeType(string filePath)
        {
            var mimeType = "application/octet-stream";
            var extension = Path.GetExtension(filePath).ToLowerInvariant();
            RegistryKey key = Registry.ClassesRoot.OpenSubKey(extension);

            if (key != null && key.GetValue("Content Type") != null)
                mimeType = key.GetValue("Content Type").ToString();

            return mimeType;
        }
        public static string GetGoogleDriveFolder(Data.ForUploadToDropbox forUploadToDropbox)
        {
            string gDriveFolderRepositoryFolder = AppSettings.Instance.MigrationAccountSettings.GoogleDrive.RepositoryFolder;
            string folderString = forUploadToDropbox.ImageFilePath.StartsWith("Image/", StringComparison.OrdinalIgnoreCase) ? "Image" : "Thumbnail";
            string dropboxFolderPath = $"{gDriveFolderRepositoryFolder}/{GlobalFx.ConvertToValidFileName(forUploadToDropbox.GUID_Company)}/{folderString}";

            return dropboxFolderPath;
        }
    }
}
