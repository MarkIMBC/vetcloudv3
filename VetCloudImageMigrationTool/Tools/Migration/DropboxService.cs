﻿using Dropbox.Api.Files;
using Dropbox.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VetCloudImageMigrationTool.Data;
using Dropbox.Api.Sharing;
using System.Net.Http;
using static System.Formats.Asn1.AsnWriter;

namespace VetCloudImageMigrationTool.Tools.Migration
{
    public class DropboxService : IMigrationService
    {
        private readonly DropboxClient _dbx;
        private string accessToken;
        public DropboxService()
        {

            string accessToken = AppSettings.Instance.MigrationAccountSettings.Dropbox.AccessToken;

            _dbx = new DropboxClient(accessToken);
        }
        public void InitializeRepositoryFolder(List<Data.ForUploadToDropbox> forUploadToDropboxList)
        {
 
        }
        private async Task RenewAccessTokenAsync()
        {

        
        }
        public async Task<FileUploadInfo> UploadFileAsync(string localFilePath, string dropboxFolderPath)
        {

            Data.FileUploadInfo result = null;

            using (var fileStream = File.OpenRead(localFilePath))
            {
                if (!await FolderExists(dropboxFolderPath))
                {
                    await CreateFolder(dropboxFolderPath);
                }

                result = new Data.FileUploadInfo();

                string path = dropboxFolderPath + "/" + Path.GetFileName(localFilePath);

                var response = await _dbx.Files.UploadAsync(
                    path,
                    WriteMode.Overwrite.Instance,
                    body: fileStream);

                var sharedLink = await _dbx.Sharing.CreateSharedLinkAsync(path);

                result.SourceFilePath = localFilePath;
                result.DestinationFileFolder = dropboxFolderPath;
                result.GeneratedUrl = sharedLink.Url;
            }

            return result;
        }
        public async Task<PathLinkMetadata> GetDropBoxSharedLinkAsync(string dropBoxPath)
        {
            await RenewAccessTokenAsync();



            return await _dbx.Sharing.CreateSharedLinkAsync(dropBoxPath);
        }

        public async Task CreateFolder(string folderPath)
        {
            try
            {
                await _dbx.Files.CreateFolderV2Async(folderPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public async Task<bool> FolderExists(string folderPath)
        {
            try
            {
                await _dbx.Files.GetMetadataAsync(folderPath);
                return true;
            }
            catch (ApiException<GetMetadataError> e)
            {
                if (e.ErrorResponse.IsPath && e.ErrorResponse.AsPath.Value.IsNotFound)
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
