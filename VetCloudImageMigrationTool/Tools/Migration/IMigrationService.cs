﻿using Dropbox.Api.Files;
using Dropbox.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetCloudImageMigrationTool.Data;

namespace VetCloudImageMigrationTool.Tools.Migration
{
    public interface IMigrationService
    {
        public void InitializeRepositoryFolder(List<Data.ForUploadToDropbox> forUploadToDropboxList);
        public Task<FileUploadInfo> UploadFileAsync(string localFilePath, string dropboxFolderPath);
        public Task CreateFolder(string folderPath);
        public Task<bool> FolderExists(string folderPath);
    }
}
