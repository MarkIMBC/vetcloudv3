﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool
{
    public static class AppSettings
    {
        private static dynamic _instance;
        public static dynamic Instance 
        {
            get
            {
                if (_instance == null) _instance = getAppSettings();

                return _instance;
            }
        }
        private static dynamic getAppSettings()
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader($@"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            return _tempObject;
        }
    }
}
