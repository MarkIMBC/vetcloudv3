﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Tools
{
    public static class GlobalFx
    {
        public static string ConvertToValidFileName(string input)
        {
            string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            string replaceString = input;

            foreach (char c in invalidChars)
            {
                replaceString = replaceString.Replace(c.ToString(), "");
            }

            return replaceString;
        }
    }
}
