﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Tools.Database
{
    public interface IDatabase
    {
        public void ExecuteNonQuery(string sql);
        public DataTable GetDataTable(string sql);
        public object ExecuteScalar(string sql);
    }
}
