﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Tools.Database
{
    public class SQLLiteFx: IDatabase
    {
        private string ConnectionString = $"";
        public SQLLiteFx()
        {
            ConnectionString = AppSettings.Instance.DatabaseConnectionString.SQLServer;
        }
        public void ExecuteNonQuery(string sql)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                using (SQLiteCommand createTableCommand = new SQLiteCommand(sql, connection))
                {
                    createTableCommand.ExecuteNonQuery();
                }

                connection.Close();
            }
        }
        public object ExecuteScalar(string sql)
        {
            object result = null;
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                using (SQLiteCommand createTableCommand = new SQLiteCommand(sql, connection))
                {
                    result = createTableCommand.ExecuteScalar();
                }

                connection.Close();
            }

            return result;
        }

        public DataTable GetDataTable(string sql)
        {
            DataTable result = new DataTable();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                using (SQLiteCommand createTableCommand = new SQLiteCommand(sql, connection))
                {
                    using (SQLiteDataReader reader = createTableCommand.ExecuteReader())
                    {
                        result.Load(reader);
                    }
                }

                connection.Close();
            }

            return result;
        }
    }
}
