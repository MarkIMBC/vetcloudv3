﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Tools.Database
{
    public class SQLServer: IDatabase
    {
        private string ConnectionString = $"";
        public SQLServer(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public void ExecuteNonQuery(string sql)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand createTableCommand = new SqlCommand(sql, connection))
                {
                    createTableCommand.ExecuteNonQuery();
                }

                connection.Close();
            }
        }
        public object ExecuteScalar(string sql)
        {
            object result = null;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand createTableCommand = new SqlCommand(sql, connection))
                {
                    result = createTableCommand.ExecuteScalar();
                }

                connection.Close();
            }

            return result;
        }

        public DataTable GetDataTable(string sql)
        {
            DataTable result = new DataTable();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                using (SqlCommand createTableCommand = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = createTableCommand.ExecuteReader())
                    {
                        result.Load(reader);
                    }
                }

                connection.Close();
            }

            return result;
        }
    }
}
