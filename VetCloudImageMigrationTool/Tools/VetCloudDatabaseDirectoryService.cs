﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;


namespace VetCloudImageMigrationTool.Tools
{
    public static class VetCloudDatabaseDirectoryService
    {
        private static Dictionary<string, string> _List;
        public static Dictionary<string, string> List
        {
            get
            {
                if (_List == null) _List = GetDatabaseListFromDirectories();

                return _List;
            }
        }

        public static string GetAPIContentImageFolderByDBName(string dbName)
        {
            return List[dbName] + @"/api/wwwroot/Content";
        }
        private static Dictionary<string, string> GetDatabaseListFromDirectories()
        { 
            var list = new Dictionary<string, string>();
            string rootDirectory = AppSettings.Instance.MigrationAccountSettings.VetCloud.RootFolder;
            string childrenFolderNameStartWith = AppSettings.Instance.MigrationAccountSettings.VetCloud.ChildrenFolderNameStartWith;

            string[] allDirectories = Directory.GetDirectories(rootDirectory);

            var filteredDirectories = allDirectories.Where(dir => Path.GetFileName(dir).StartsWith(childrenFolderNameStartWith));

            foreach (var directory in filteredDirectories)
            {
                string apiFolder = directory + @"/api";

                if (Directory.Exists(apiFolder))
                {
                    dynamic appSettingData = getAppSettings(apiFolder);

                    string connectionString = appSettingData.ConnectionString;
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

                    string databaseName = builder.InitialCatalog;

                    if (!list.ContainsKey(databaseName))
                    {
                        list.Add(databaseName, directory);
                    }
                }
            }

            return list;
        }
        private static dynamic getAppSettings(string directory)
        {
            StreamReader r = new StreamReader($@"{directory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            return _tempObject;
        }
    }
}
