﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Data
{
    public class ReturnMessage
    {
        public bool Success { get; set; } = true;
        public string Message { get; set; } = "";

        public static ReturnMessage CreateInstance()
        {
            return new ReturnMessage();
        }
    }
}
