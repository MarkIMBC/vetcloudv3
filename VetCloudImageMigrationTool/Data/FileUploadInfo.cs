﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Data
{
    public class FileUploadInfo
    {
        public string SourceFilePath;
        public string DestinationFileFolder;
        public string FileID;
        public string GeneratedUrl;
        public string GeneratedThumbnailUrl;
        public FileUploadInfo()
        {

        }
        public FileUploadInfo(string sourceFilePath, string destinationFileFolder)
        {
            this.SourceFilePath = sourceFilePath;
            this.DestinationFileFolder = destinationFileFolder;
        }
    }
}