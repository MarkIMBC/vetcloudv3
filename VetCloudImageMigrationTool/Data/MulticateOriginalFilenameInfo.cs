﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.Data
{
    public class MulticateOriginalFilenameInfo
    {
        public string DatabaseName { get; set; } = "";
        public string OriginalImageFilename { get; set; } = "";
        public int Count { get; set; } = 0;
    }
}
