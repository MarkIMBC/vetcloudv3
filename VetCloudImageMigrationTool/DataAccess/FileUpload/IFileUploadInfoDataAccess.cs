﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.DataAccess.FileUpload
{
    public interface IFileUploadInfoDataAccess
    {
        public bool IsMultipleOriginalImageFileName(Data.ForUploadToDropbox forUploadToDropbox);
        public List<Data.FileUploadInfo> GetRecords();
        public List<Data.ForUploadToDropbox> GetRecordsPaging();
        public List<Data.ForUploadToDropbox> GetWithErrorRecordsPaging();
    }
}
