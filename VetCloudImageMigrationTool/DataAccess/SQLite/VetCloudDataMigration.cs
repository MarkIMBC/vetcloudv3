﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.DataAccess.SQLite
{
    public class VetCloudDataMigration : ISQLite
    {
        private Tools.Database.SQLLiteFx sqlLiteContext = new Tools.Database.SQLLiteFx();
        public VetCloudDataMigration()
        {
            this.CreateTable();
        }
  
        public void CreateTable()
        {
            sqlLiteContext.ExecuteNonQuery(@$"CREATE TABLE IF NOT EXISTS { this.GetType().Name } (
                    Id INTEGER PRIMARY KEY,
                    DatabaseName TEXT,
                    TableName TEXT,
                    ColumnName TEXT,
                    FileName TEXT,
                    AssignedURL TEXT)"
            );
        }
    }
}
