﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetCloudImageMigrationTool.Data;
using VetCloudImageMigrationTool.DataAccess.FileUpload;
using VetCloudImageMigrationTool.DataAccess.SQLServer;

namespace VetCloudImageMigrationTool.DataAccess.SystemManagement
{
    public class ForUploadToDropbox : IFileUploadInfoDataAccess
    {
        private Tools.Database.SQLServer sysManageDb;

        public ForUploadToDropbox()
        {
            string connectionString = AppSettings.Instance.DatabaseConnectionString.SQLServerSystemManagement;
            this.sysManageDb = new Tools.Database.SQLServer(connectionString);
        }
        public List<Data.ForUploadToDropbox> GetRecordsPaging()
        {
            var list = new List<Data.ForUploadToDropbox>();

            var dt = this.sysManageDb.GetDataTable("EXEC dbo.pGetPendingForUploadToDropbox");

            foreach (DataRow dr in dt.Rows)
            {
                var record = new Data.ForUploadToDropbox();

                Tools.PropertyValueSetterHelper.setProperties(record, dr);

                list.Add(record);
            }

            return list;
        }
        public List<Data.ForUploadToDropbox> GetWithErrorRecordsPaging()
        {
            var list = new List<Data.ForUploadToDropbox>();

            var dt = this.sysManageDb.GetDataTable("EXEC dbo.pGetPendingWithErrorForUploadToDropbox");

            foreach (DataRow dr in dt.Rows)
            {
                var record = new Data.ForUploadToDropbox();

                Tools.PropertyValueSetterHelper.setProperties(record, dr);

                list.Add(record);
            }

            return list;
        }
        public void SaveUpload(Data.ForUploadToDropbox forUploadToDropbox)
        {
            string sql = $"" +
                $"UPDATE tForUploadToDropbox SET " +
                $"IsUpload = 1, " +
                $"ImageFileName = '{forUploadToDropbox.FileID}', " +
                $"FileID = '{forUploadToDropbox.FileID}', " +
                $"ImageUrl = '{forUploadToDropbox.ImageUrl}', " +
                $"DateUploaded = GETDATE() " +
                $"" +
                $"WHERE " +
                $"ID = {forUploadToDropbox.ID}";

            this.sysManageDb.ExecuteNonQuery(sql);


            if (forUploadToDropbox.ImageFilePath.StartsWith("Image/"))
            {
                MigrateImageFilePath migrateImageFilePath = new MigrateImageFilePath(forUploadToDropbox);
                migrateImageFilePath.ChangeImageFileNameToFileID();
                migrateImageFilePath.Dispose();
                migrateImageFilePath = null;
            }
        }
        public bool SaveErrorUpload(Data.ForUploadToDropbox forUploadToDropbox, string message)
        {
            var isSaved = true;

            try
            {
                string sql = @$"UPDATE tForUploadToDropbox SET IsError = 1, DateError = GETDATE(), ErrorMessage = ISNULL(ErrorMessage, '') + CHAR(13) + CHAR(10) + '{message.Replace("'", "`")}' + ' - "+ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + $"' WHERE ID = {forUploadToDropbox.ID}";

                this.sysManageDb.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                isSaved = false;
            }

            return isSaved;
        }
        public bool IsMultipleOriginalImageFileName(Data.ForUploadToDropbox forUploadToDropbox)
        {
            string sql = "EXEC dbo.pGetTotalOriginalImageFilenameMulticateCount '/*DatabaseName*/', '/*OriginalImageFileName*/'";

            sql = sql.Replace("/*DatabaseName*/", forUploadToDropbox.DatabaseName);
            sql = sql.Replace("/*OriginalImageFileName*/", forUploadToDropbox.OriginalImageFileName);

            var dt = this.sysManageDb.GetDataTable(sql);

            var record = new Data.MulticateOriginalFilenameInfo();

            foreach (DataRow dr in dt.Rows)
            {
                Tools.PropertyValueSetterHelper.setProperties(record, dr);
            }

            dt.Dispose();
            dt = null;

            return record.Count > 1;
        }
        List<FileUploadInfo> IFileUploadInfoDataAccess.GetRecords()
        {
            throw new NotImplementedException();
        }
    }
}
