﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetCloudImageMigrationTool.DataAccess.SQLServer
{
    public class MigrateImageFilePath
    {
        private string _connectionString = "";
        private Data.ForUploadToDropbox _forUploadToDropbox;
        private Tools.Database.SQLServer _sysManageDb;
        public MigrateImageFilePath(Data.ForUploadToDropbox forUploadToDropbox)
        {
            string instanceName = AppSettings.Instance.DatabaseConnectionString.SQLServerVetCloud.Instance;

            string databaseName = forUploadToDropbox.DatabaseName;
            string userId = "sa";
            string password = "sql123$%^";

            _connectionString = $"Data Source={instanceName};Initial Catalog={databaseName};User ID={userId};Password={password};";
            _forUploadToDropbox = forUploadToDropbox;
            _sysManageDb = new Tools.Database.SQLServer(_connectionString);
        }

        public void ChangeImageFileNameToFileID()
        {
            string sql = "UPDATE /*TableName*/ SET /*Column*/ = '/*VALUE*/' WHERE ID = /*ID_CurrentObject*/";

            sql = sql.Replace("/*TableName*/", this._forUploadToDropbox.TableName);
            sql = sql.Replace("/*Column*/", this._forUploadToDropbox.ColumnName);
            sql = sql.Replace("/*VALUE*/", this._forUploadToDropbox.FileID);
            sql = sql.Replace("/*ID_CurrentObject*/", this._forUploadToDropbox.ID_CurrentObject.ToString());

            _sysManageDb.ExecuteNonQuery(sql);
        }

        public void Dispose()
        { 
            this._sysManageDb = null;
        }
    }

}
