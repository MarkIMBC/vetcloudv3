﻿namespace VetcloudInventoryUpdater
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.bgWorkerExportAndRestSQLLiteDB = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Col_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_ProcessName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_DatabaseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.bgWorkerUploadExcel = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip_Label = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAndRemoveDoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadQueuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromExcelFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fromVetCloudDBsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bgWorkerAddQueueVecloudDbs = new System.ComponentModel.BackgroundWorker();
            this.toolStrip_LabelAddQueueVetCloud = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(3, 27);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.Size = new System.Drawing.Size(1010, 366);
            this.dataGridView4.TabIndex = 8;
            // 
            // bgWorkerExportAndRestSQLLiteDB
            // 
            this.bgWorkerExportAndRestSQLLiteDB.WorkerReportsProgress = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1024, 422);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.menuStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1016, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Process";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_Name,
            this.Col_ProcessName,
            this.Col_DatabaseName,
            this.Col_Message});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1010, 366);
            this.dataGridView1.TabIndex = 2;
            // 
            // Col_Name
            // 
            this.Col_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Col_Name.FillWeight = 200F;
            this.Col_Name.HeaderText = "Name";
            this.Col_Name.Name = "Col_Name";
            this.Col_Name.ReadOnly = true;
            this.Col_Name.Visible = false;
            // 
            // Col_ProcessName
            // 
            this.Col_ProcessName.FillWeight = 180F;
            this.Col_ProcessName.HeaderText = "Process";
            this.Col_ProcessName.Name = "Col_ProcessName";
            this.Col_ProcessName.ReadOnly = true;
            this.Col_ProcessName.Width = 180;
            // 
            // Col_DatabaseName
            // 
            this.Col_DatabaseName.FillWeight = 180F;
            this.Col_DatabaseName.HeaderText = "Database";
            this.Col_DatabaseName.Name = "Col_DatabaseName";
            this.Col_DatabaseName.ReadOnly = true;
            this.Col_DatabaseName.Width = 180;
            // 
            // Col_Message
            // 
            this.Col_Message.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col_Message.HeaderText = "Message";
            this.Col_Message.Name = "Col_Message";
            this.Col_Message.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView4);
            this.tabPage2.Controls.Add(this.menuStrip2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1016, 396);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Details";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.doneToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(3, 3);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1010, 24);
            this.menuStrip2.TabIndex = 9;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(33, 20);
            this.allToolStripMenuItem.Text = "All";
            this.allToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.refreshToolStripMenuItem.Text = "Pending";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // doneToolStripMenuItem
            // 
            this.doneToolStripMenuItem.Name = "doneToolStripMenuItem";
            this.doneToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.doneToolStripMenuItem.Text = "Done";
            this.doneToolStripMenuItem.Click += new System.EventHandler(this.doneToolStripMenuItem_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // bgWorkerUploadExcel
            // 
            this.bgWorkerUploadExcel.WorkerReportsProgress = true;
            this.bgWorkerUploadExcel.WorkerSupportsCancellation = true;
            this.bgWorkerUploadExcel.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerUploadExcel_DoWork);
            this.bgWorkerUploadExcel.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerUploadExcel_ProgressChanged);
            this.bgWorkerUploadExcel.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerUploadExcel_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrip_Label,
            this.toolStrip_LabelAddQueueVetCloud});
            this.statusStrip1.Location = new System.Drawing.Point(0, 437);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1048, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip_Label
            // 
            this.toolStrip_Label.Name = "toolStrip_Label";
            this.toolStrip_Label.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportAndRemoveDoneToolStripMenuItem,
            this.loadQueuesToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItem1.Text = "Option";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // exportAndRemoveDoneToolStripMenuItem
            // 
            this.exportAndRemoveDoneToolStripMenuItem.Name = "exportAndRemoveDoneToolStripMenuItem";
            this.exportAndRemoveDoneToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.exportAndRemoveDoneToolStripMenuItem.Text = "Export and Remove Done";
            this.exportAndRemoveDoneToolStripMenuItem.Click += new System.EventHandler(this.exportAndRemoveDoneToolStripMenuItem_Click);
            // 
            // loadQueuesToolStripMenuItem
            // 
            this.loadQueuesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fromExcelFileToolStripMenuItem,
            this.fromVetCloudDBsToolStripMenuItem});
            this.loadQueuesToolStripMenuItem.Name = "loadQueuesToolStripMenuItem";
            this.loadQueuesToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.loadQueuesToolStripMenuItem.Text = "Load Queues";
            // 
            // fromExcelFileToolStripMenuItem
            // 
            this.fromExcelFileToolStripMenuItem.Name = "fromExcelFileToolStripMenuItem";
            this.fromExcelFileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fromExcelFileToolStripMenuItem.Text = "From Excel File";
            this.fromExcelFileToolStripMenuItem.Click += new System.EventHandler(this.fromExcelFileToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1010, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fromVetCloudDBsToolStripMenuItem
            // 
            this.fromVetCloudDBsToolStripMenuItem.Name = "fromVetCloudDBsToolStripMenuItem";
            this.fromVetCloudDBsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fromVetCloudDBsToolStripMenuItem.Text = "From VetCloud DBs";
            this.fromVetCloudDBsToolStripMenuItem.Click += new System.EventHandler(this.fromVetCloudDBsToolStripMenuItem_Click);
            // 
            // bgWorkerAddQueueVecloudDbs
            // 
            this.bgWorkerAddQueueVecloudDbs.WorkerReportsProgress = true;
            this.bgWorkerAddQueueVecloudDbs.WorkerSupportsCancellation = true;
            this.bgWorkerAddQueueVecloudDbs.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerAddQueueVecloudDbs_DoWork);
            this.bgWorkerAddQueueVecloudDbs.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerAddQueueVecloudDbs_ProgressChanged);
            this.bgWorkerAddQueueVecloudDbs.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerAddQueueVecloudDbs_RunWorkerCompleted);
            // 
            // toolStrip_LabelAddQueueVetCloud
            // 
            this.toolStrip_LabelAddQueueVetCloud.Name = "toolStrip_LabelAddQueueVetCloud";
            this.toolStrip_LabelAddQueueVetCloud.Size = new System.Drawing.Size(0, 17);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 459);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Vetcloud Process Updater";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.ComponentModel.BackgroundWorker bgWorkerExportAndRestSQLLiteDB;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_ProcessName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_DatabaseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Message;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doneToolStripMenuItem;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.ComponentModel.BackgroundWorker bgWorkerUploadExcel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStrip_Label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exportAndRemoveDoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadQueuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromExcelFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromVetCloudDBsToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bgWorkerAddQueueVecloudDbs;
        private System.Windows.Forms.ToolStripStatusLabel toolStrip_LabelAddQueueVetCloud;
    }
}

