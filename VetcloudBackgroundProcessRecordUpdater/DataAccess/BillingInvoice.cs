﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetcloudInventoryUpdater.DataAccess
{
    public class BillingInvoice
    {
        public SqlConnection _connection;

        public BillingInvoice(SqlConnection connection)
        {
            this._connection = connection;
        }
        public BillingInvoice(string connectionString)
        {
            this._connection = new SqlConnection(connectionString); ;
        }

        public DataTable getQueuedRerunProcessRecord(GlobalFx.SQLSelectParamenter param = null) {

            if (param is null) param = new GlobalFx.SQLSelectParamenter();

            string top = "";

            if (param.TopCount > 0) {

                top = $"TOP {param.TopCount.ToString()}";
            }

            string sqlString = $"SELECT " +
                $"{top} * FROM " +
                $"{this._connection.Database}.dbo.vReRunProcessQueueRecord " +
                "WHERE TableName = 'tBillingInvoice' " +
                $"ORDER BY DateModified ASC";
            DataTable dt = GlobalFx.SQL.getDatatable(sqlString, this._connection);

            return dt;
        }
        public GlobalFx.ReturnArg ReRunAfterSaved(int ID_BillingInvoice)
        {
            bool isSuccess = false;
            GlobalFx.ReturnArg arg = new GlobalFx.ReturnArg();

            string sqlString = $"exec {this._connection.Database}.dbo.pReRun_AfterSaved_Process_BillingInvoice_By_ID {ID_BillingInvoice.ToString()};";
            DataTable dt = GlobalFx.SQL.getDatatable(sqlString, this._connection);

            if (dt.Rows.Count > 0) {

                DataRow dr = dt.Rows[0];

                arg.setProperties(dr);
            }

            dt.Dispose();
            dt = null;

            return arg;
        }
    }
}
