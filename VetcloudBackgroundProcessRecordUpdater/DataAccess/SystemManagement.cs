﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetcloudInventoryUpdater.DataAccess
{
    public class SystemManagement
    {
        public static DataTable GetQueuedRerunProcessRecord(GlobalFx.SQLSelectParamenter param = null)
        {
            dynamic appSettings = GlobalFx.SQL.getAppSettings();
            string DataSource = appSettings.DataSource;
            string initialCatalog = appSettings.SystemManagementDatabaseName;
            string userID = appSettings.UserID;
            string password = appSettings.Password;
            string connectionString = "";

            SqlConnectionStringBuilder connectionStringBuider = new SqlConnectionStringBuilder();

            connectionStringBuider.InitialCatalog = initialCatalog;
            connectionStringBuider.DataSource = DataSource;
            connectionStringBuider.UserID = userID;
            connectionStringBuider.Password = password;

            connectionString = connectionStringBuider.ToString();

            if (param is null) param = new GlobalFx.SQLSelectParamenter();

            string top = "";

            if (param.TopCount > 0)
            {
                top = $"TOP {param.TopCount.ToString()}";
            }

            SqlConnection con = new SqlConnection(connectionString); ;

            string sqlString = $"EXEC pGetReRunAfterSavedPending_VetCloudv3";
            DataTable dt = GlobalFx.SQL.getDatatable(sqlString, con);

            return dt;
        }
    }
}
