﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VetcloudInventoryUpdater.DataAccess
{
    public class InventoryTrail
    {
        public SqlConnection _connection;

        public InventoryTrail(SqlConnection connection)
        {
            this._connection = connection;
        }
        public InventoryTrail(string connectionString)
        {
            this._connection = new SqlConnection(connectionString); ;
        }

        public DataTable getMulticateTrail(GlobalFx.SQLSelectParamenter param = null) {

            if (param is null) param = new GlobalFx.SQLSelectParamenter();

            string top = "";

            if (param.TopCount > 0) {

                top = $"TOP {param.TopCount.ToString()}";
            }

            string sqlString = $"SELECT {top} * FROM {this._connection.Database}.dbo.vMulticateInventoryTrail ORDER BY Max_ID_InventoryTrail DESC";
            DataTable dt = GlobalFx.SQL.getDatatable(sqlString, this._connection);

            return dt;
        }
        public int ReUpdateItemInventory(int ID_Item)
        {
            string sqlString = $"exec {this._connection.Database}.dbo.pUpdateItemCurrentInventoryBy_ID_Item {ID_Item.ToString()};";
            return GlobalFx.SQL.ExecuteNonQuery(sqlString, this._connection);
        }
        public GlobalFx.ReturnArg ReRunAfterSaved(int ID_Item)
        {
            bool isSuccess = false;
            GlobalFx.ReturnArg arg = new GlobalFx.ReturnArg();

            string sqlString = $"exec {this._connection.Database}.dbo.pReRun_AfterSaved_Process_Item_BY_IDs {ID_Item.ToString()};";
            DataTable dt = GlobalFx.SQL.getDatatable(sqlString, this._connection);

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                arg.setProperties(dr);
            }

            dt.Dispose();
            dt = null;

            return arg;
        }
    }
}
