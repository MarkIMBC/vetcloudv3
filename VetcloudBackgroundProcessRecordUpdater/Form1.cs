﻿using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VetcloudInventoryUpdater.GlobalFx;
using VetcloudInventoryUpdater.SQLLite;

namespace VetcloudInventoryUpdater
{
    public partial class Form1 : Form
    {
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public Form1()
        {
            InitializeComponent();
        }

        GlobalFx.ExportExcel getExportExcelFormat()
        {
          
            ExportExcel exportExcelFormat = new ExportExcel();

            StreamReader r = new StreamReader(@"sample.json");
            string jsonString = r.ReadToEnd();
            ExportExcel _tempObject = JsonConvert.DeserializeObject<ExportExcel>(jsonString);

            r.Close();

            return _tempObject;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dynamic appSettings = SQL.getAppSettings();
            int exportAndResetTimerTimeOut = appSettings["ExportAndResetTimerTimeOut"];

            this.timer1.Interval = exportAndResetTimerTimeOut;

            List<GlobalFx.DatabaseSetting> dbSettinglist = GlobalFx.SQL.getDatabaseDatabaseSettings();

            foreach (GlobalFx.DatabaseSetting dbSetting in dbSettinglist)
            {
                string connectionString = dbSetting.ConenctionString;

                if (dbSetting.EnablePatientSOAPAfterSaved) this.addPatient_SOAPProcess(connectionString);
                if (dbSetting.EnableBillingInvoiceAfterSaved) this.addBillingInvoiceProcess(connectionString);
                if (dbSetting.EnableReupdateInventoryTrail) this.addInventoryTrailProcess(connectionString);
            }

            loadDgvQueued(ReRunAfterSavedFilingStatusEnum.Pending);
        }
        private void addPatient_SOAPProcess(string connectionString)
        {

            int rowIndex = this.dataGridView1.Rows.Add();
            DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
            DataGridViewCellCollection cell = row.Cells;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

            Business.Patient_SOAP soap = new Business.Patient_SOAP(new SqlConnection(connectionString));
            soap.OnProgressed += this.inventoryTrail_OnProgressed;

            cell["Col_Name"].Value = soap.Name;
            cell["Col_ProcessName"].Value = "Patient SOAP AfterSaved";
            cell["Col_DatabaseName"].Value = builder.InitialCatalog;

            soap.Run();
        }
        private void addBillingInvoiceProcess(string connectionString)
        {

            int rowIndex = this.dataGridView1.Rows.Add();
            DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
            DataGridViewCellCollection cell = row.Cells;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

            Business.BillingInvoice bi = new Business.BillingInvoice(new SqlConnection(connectionString));
            bi.OnProgressed += this.inventoryTrail_OnProgressed;

            cell["Col_Name"].Value = bi.Name;
            cell["Col_ProcessName"].Value = "Billing Invoice AfterSaved";
            cell["Col_DatabaseName"].Value = builder.InitialCatalog;

            bi.Run();
        }
        private void addInventoryTrailProcess(string connectionString)
        {
            int rowIndex = this.dataGridView1.Rows.Add();
            DataGridViewRow row = this.dataGridView1.Rows[rowIndex];
            DataGridViewCellCollection cell = row.Cells;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

            Business.InventoryTrail inventoryTrail = new Business.InventoryTrail(new SqlConnection(connectionString));
            inventoryTrail.OnProgressed += this.inventoryTrail_OnProgressed;

            cell["Col_Name"].Value = inventoryTrail.Name;
            cell["Col_ProcessName"].Value = "Reupdate Inventory Trail";
            cell["Col_DatabaseName"].Value = builder.InitialCatalog;

            inventoryTrail.Run();
        }
        public void inventoryTrail_OnProgressed(object sender, Business.BusinessEventArgs arg)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                DataGridViewCellCollection cell = row.Cells;

                string name = (string)cell["Col_Name"].Value;

                if (name == arg.Name)
                {
                    cell["Col_Message"].Value = arg.message;
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string[] Modules = { "BillingInvoice", "Item", "Patient_SOAP" };
            DataSet ds = new DataSet();
            int index = 0;

            foreach (string module in Modules)
            {
                index++;
                
                RunAfterSaved runAfterSaved = new RunAfterSaved(module);

                DataTable dt = runAfterSaved.getPending();

                runAfterSaved.exportAllJSONString();
                runAfterSaved.deleteAllDone();

                dt.TableName = module;
                ds.Tables.Add(dt);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            dynamic appSettings = SQL.getAppSettings();
            int exportAndResetTimerTimeOut = appSettings["ExportAndResetTimerTimeOut"];

            this.timer1.Interval = exportAndResetTimerTimeOut;

            string[] Modules = { "BillingInvoice", "Item", "Patient_SOAP" };
            DataSet ds = new DataSet();
            int index = 0;

            foreach (string module in Modules)
            {
                index++;

                RunAfterSaved runAfterSaved = new RunAfterSaved(module);
                DataTable dt = runAfterSaved.getPending();

                runAfterSaved.exportAllJSONString();
                runAfterSaved.deleteAllDone();

                dt.TableName = module;
                ds.Tables.Add(dt);
            }
        }

     
        enum ReRunAfterSavedFilingStatusEnum { 
            All= 0,
            Pending = 1,
            Done = 2
        }

        private void loadDgvQueued(ReRunAfterSavedFilingStatusEnum status) {

            string[] Modules = { "BillingInvoice", "Item", "Patient_SOAP" };
            DataSet ds = new DataSet();
            int index = 0;

            DataTable dtQueue = new DataTable();
            dtQueue.Columns.Add("Date", typeof(DateTime));
            dtQueue.Columns.Add("ModuleName", typeof(String));
            dtQueue.Columns.Add("DatabaseName", typeof(String));
            dtQueue.Columns.Add("ID_CurrentObject", typeof(int));
            dtQueue.Columns.Add("DateOngoing", typeof(DateTime));
            dtQueue.Columns.Add("DateDone", typeof(DateTime));
            dtQueue.Columns.Add("ID_FilingStatus", typeof(int));

            foreach (string module in Modules)
            {
                index++;
                RunAfterSaved runAfterSaved = new RunAfterSaved(module);

                DataTable dt = new DataTable();
                switch (status) {

                    case ReRunAfterSavedFilingStatusEnum.Pending:

                        dt = runAfterSaved.getPending();
                        break;

                    case ReRunAfterSavedFilingStatusEnum.Done:

                        dt = runAfterSaved.getAlLDone();
                        break;

                    case ReRunAfterSavedFilingStatusEnum.All:

                        dt = runAfterSaved.getAll();
                        break;
                }

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow drQueue = dtQueue.NewRow();

                        drQueue["ModuleName"] = module;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            drQueue[dc.ColumnName] = dr[dc.ColumnName];
                        }

                        dtQueue.Rows.Add(drQueue);
                    }
                }

                dt.TableName = module;
                ds.Tables.Add(dt);
            }

            dtQueue.DefaultView.Sort = "Date DESC";

            this.dataGridView4.DataSource = dtQueue.DefaultView;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
        private void exportAndRemoveDoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dynamic appSettings = SQL.getAppSettings();
            int exportAndResetTimerTimeOut = appSettings["ExportAndResetTimerTimeOut"];

            this.timer1.Interval = exportAndResetTimerTimeOut;

            string[] Modules = { "BillingInvoice", "Item", "Patient_SOAP" };
            DataSet ds = new DataSet();
            int index = 0;

            foreach (string module in Modules)
            {
                index++;

                RunAfterSaved runAfterSaved = new RunAfterSaved(module);
                DataTable dt = runAfterSaved.getPending();

                runAfterSaved.exportAllJSONString();
                runAfterSaved.deleteAllDone();

                dt.TableName = module;
                ds.Tables.Add(dt);
            }
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadDgvQueued(ReRunAfterSavedFilingStatusEnum.All);
        }
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadDgvQueued(ReRunAfterSavedFilingStatusEnum.Pending);
        }

        private void doneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadDgvQueued(ReRunAfterSavedFilingStatusEnum.Done);
        }

        private bool IsfromExcelFileToolStripMenuItemLoading = false;
        private void fromExcelFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.Filter = "Excel Files|*.xlsx;";

            if (this.openFileDialog1.ShowDialog() == DialogResult.OK) {

                string fileName = this.openFileDialog1.FileName;
            
                this.bgWorkerUploadExcel.RunWorkerAsync(fileName);
            }
        }

        private void bgWorkerUploadExcel_DoWork(object sender, DoWorkEventArgs e)
        {
            this.IsfromExcelFileToolStripMenuItemLoading = true;
            this.bgWorkerUploadExcel.ReportProgress(0, "Loading Data from Excel....");

            string fileName = (string)e.Argument; 

            DataTable dt = ValueConverter.FromExcelToDataTable(fileName);

            if (!dt.Columns.Contains("ModuleName"))
            {
                MessageBox.Show("Column 'ModuleName' is not exist", "Excel File Validation");
                this.IsfromExcelFileToolStripMenuItemLoading = false;
                return;
            }
            if (!dt.Columns.Contains("DatabaseName"))
            {
                MessageBox.Show("Column 'DatabaseName' is not exist", "Excel File Validation");
                this.IsfromExcelFileToolStripMenuItemLoading = false;
                return;
            }
            if (!dt.Columns.Contains("ID_CurrentObject"))
            {
                MessageBox.Show("Column 'ID_CurrentObject' is not exist", "Excel File Validation");
                this.IsfromExcelFileToolStripMenuItemLoading = false;
                return;
            }

            int counter = 0;
            int totaRowCounter = dt.Rows.Count;

            foreach (DataRow dr in dt.Rows)
            {
                counter++;

                if (!(dr["ModuleName"] is System.DBNull))
                {
                    string ModuleName = (string)dr["ModuleName"];
                    string DatabaseName = (string)dr["DatabaseName"];
                    int ID_CurrentObject = Convert.ToInt32(dr["ID_CurrentObject"]);

                    RunAfterSaved.InsertPending(ModuleName, DatabaseName, ID_CurrentObject);
                }

                string labelString = $"Loading Queues {counter}/{totaRowCounter}";

                this.bgWorkerUploadExcel.ReportProgress(counter, labelString);
            }


            this.IsfromExcelFileToolStripMenuItemLoading = false;
        }

        private void bgWorkerUploadExcel_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.toolStrip_Label.Text = "";
            this.fromExcelFileToolStripMenuItem.Visible = true;
        }

        private void bgWorkerUploadExcel_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.toolStrip_Label.Text =(string) e.UserState;
            this.fromExcelFileToolStripMenuItem.Visible = false;
        }

        private void fromVetCloudDBsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bgWorkerAddQueueVecloudDbs.RunWorkerAsync();
        }

        private void bgWorkerAddQueueVecloudDbs_DoWork(object sender, DoWorkEventArgs e)
        {
            this.IsfromExcelFileToolStripMenuItemLoading = true;

            try{

                this.bgWorkerAddQueueVecloudDbs.ReportProgress(0, "Loading Data from VetCloud DBs....");

                DataTable dt = DataAccess.SystemManagement.GetQueuedRerunProcessRecord();

                if (!dt.Columns.Contains("ModuleName"))
                {
                    MessageBox.Show("Column 'ModuleName' is not exist", "Excel File Validation");
                    this.IsfromExcelFileToolStripMenuItemLoading = false;
                    return;
                }
                if (!dt.Columns.Contains("DatabaseName"))
                {
                    MessageBox.Show("Column 'DatabaseName' is not exist", "Excel File Validation");
                    this.IsfromExcelFileToolStripMenuItemLoading = false;
                    return;
                }
                if (!dt.Columns.Contains("ID_CurrentObject"))
                {
                    MessageBox.Show("Column 'ID_CurrentObject' is not exist", "Excel File Validation");
                    this.IsfromExcelFileToolStripMenuItemLoading = false;
                    return;
                }

                int counter = 0;
                int totaRowCounter = dt.Rows.Count;

                foreach (DataRow dr in dt.Rows)
                {
                    counter++;

                    if (!(dr["ModuleName"] is System.DBNull))
                    {
                        string ModuleName = (string)dr["ModuleName"];
                        string DatabaseName = (string)dr["DatabaseName"];
                        int ID_CurrentObject = Convert.ToInt32(dr["ID_CurrentObject"]);

                        RunAfterSaved.InsertPending(ModuleName, DatabaseName, ID_CurrentObject);
                    }

                    string labelString = $"Loading Queues {counter}/{totaRowCounter}";

                    this.bgWorkerAddQueueVecloudDbs.ReportProgress(counter, labelString);
                }

                this.bgWorkerAddQueueVecloudDbs.ReportProgress(0, "");
            }
            catch (Exception ex) {

                this.bgWorkerAddQueueVecloudDbs.ReportProgress(0, ex.Message);
            }

            this.IsfromExcelFileToolStripMenuItemLoading = false;
        }

        private void bgWorkerAddQueueVecloudDbs_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.toolStrip_LabelAddQueueVetCloud.Text = (string)e.UserState;
            this.fromVetCloudDBsToolStripMenuItem.Visible = false;
        }

        private void bgWorkerAddQueueVecloudDbs_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.fromVetCloudDBsToolStripMenuItem.Visible = true;
        }
    }
}
