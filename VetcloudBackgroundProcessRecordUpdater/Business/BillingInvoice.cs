﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VetcloudInventoryUpdater.SQLLite;

namespace VetcloudInventoryUpdater.Business
{

    public delegate void BillingInvoiceOnProgressed(object sender, BusinessEventArgs e);
    public class BillingInvoice
    {
        public event BillingInvoiceOnProgressed OnProgressed;
        private SqlConnection _connection;
        private string _connectionstring;


        public string Name = "";
        public bool IsCancel = false;
        public BillingInvoice()
        {
            Guid guid = Guid.NewGuid();

            this.Name = guid.ToString();
        }
        public BillingInvoice(SqlConnection connection)
        {
            this._connection = connection;
            this._connectionstring = connection.ConnectionString;

            Guid guid = Guid.NewGuid();

            this.Name = guid.ToString();
        }
        public async void Run()
        {
            ThreadStart childref = new ThreadStart(() => _run());
            Thread childThread = new Thread(childref);
            childThread.IsBackground = true;
            childThread.Start();
        }

        private int totalRows = 0;
        private int counter = 0;
        private void _run()
        {
            BusinessEventArgs arg;
            while (!this.IsCancel)
            {
                string message = "";
                try
                {
                    if ((this.counter == 0))
                    {
                        DataAccess.BillingInvoice biDA = new DataAccess.BillingInvoice(this._connectionstring);
                        RunAfterSaved_BillingInvoice runAfterSaved = new RunAfterSaved_BillingInvoice();
                      
                        string databaseName = biDA._connection.Database;

                        //runAfterSaved.exportAllDoneJSONString();
                        //runAfterSaved.deleteAllDone();
                        DataTable dtItem = runAfterSaved.getPendingPerDataBase(databaseName);

                        this.counter = 0;
                        this.totalRows = 0;
                        if (dtItem.Rows.Count == 0)
                        {

                            arg = new BusinessEventArgs();
                            arg.message = "";
                            arg.Name = this.Name;
                            this.OnProgressed.Invoke(this, arg);
                        }

                        this.totalRows = dtItem.Rows.Count;

                        foreach (DataRow dr in dtItem.Rows)
                        {
                            int ID_CurrentObject = (int)dr["ID_CurrentObject"];
                            DateTime DateModified = (DateTime)dr["Date"];

                            try
                            {
                                this.counter++;
                                message = $"" +
                                   $"Processing {counter.ToString()} of {dtItem.Rows.Count.ToString()} - " +
                                   $"Reference ID: {ID_CurrentObject.ToString()} " +
                                   $"DateModified: {DateModified.ToString("yyyy-MM-dd hh:mm:ss tt")}";

                                arg = new BusinessEventArgs();
                                arg.message = message;
                                arg.Name = this.Name;
                                this.OnProgressed.Invoke(this, arg);

                                DataAccess.BillingInvoice _biDA = new DataAccess.BillingInvoice(this._connectionstring);
                                runAfterSaved.SetStatusAsOngoing(databaseName, ID_CurrentObject);
                               
                                GlobalFx.ReturnArg resultArg = _biDA.ReRunAfterSaved(ID_CurrentObject);
                        
                                if (!resultArg.Success)
                                {
                                    throw new Exception(resultArg.Message);
                                }
                                else {

                                    runAfterSaved.SetStatusAsDone(databaseName, ID_CurrentObject);
                                }

                                if (this.counter >= this.totalRows)
                                {
                                    this.counter = 0;

                                    System.Threading.Thread.Sleep(5000);
                                    arg = new BusinessEventArgs();
                                    arg.message = "Refreshing Queued Record";
                                    arg.Name = this.Name;
                                    this.OnProgressed.Invoke(this, arg);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Threading.Thread.Sleep(5000);
                                message = $"" +
                                   $"Processed {this.counter.ToString()} of {dtItem.Rows.Count.ToString()} - " +
                                   $"Reference ID: {ID_CurrentObject.ToString()} " +
                                   $"Message: {ex.Message}";
                                this.counter = 0;

                                arg = new BusinessEventArgs();
                                arg.message = "Refreshing Queued Record";
                                arg.Name = this.Name;
                                this.OnProgressed.Invoke(this, arg);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;

                    arg = new BusinessEventArgs();
                    arg.message = message;
                    arg.Name = this.Name;
                    this.OnProgressed.Invoke(this, arg);

                    this.totalRows = 0;
                    this.totalRows = 0;
                }

                GC.Collect();
            }
        }

    }
}
