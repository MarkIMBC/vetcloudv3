﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetcloudInventoryUpdater.Class;

namespace VetcloudInventoryUpdater.GlobalFx
{
    public class SQL
    {
        public static DataTable getDatatable(string sqlString, SqlConnection _con)
        {
            DataTable dataTable = new DataTable();

            if (_con.State != ConnectionState.Open)
            {
                _con.Open();
            }

            var cmd = new SqlCommand(sqlString, _con);
            var da = new SqlDataAdapter(cmd);
            
            da.SelectCommand.CommandTimeout = 0;
            da.Fill(dataTable);

            if (_con.State != ConnectionState.Closed)
            {
                _con.Close();
            }

            return dataTable;
        }

        public static int ExecuteNonQuery(string sqlString, SqlConnection _con)
        {
            int affetedRows = 0;

            if (_con.State != ConnectionState.Open)
            {
                _con.Open();
            }

            var cmd = new SqlCommand(sqlString, _con);
            affetedRows = cmd.ExecuteNonQuery();

            if (_con.State != ConnectionState.Closed)
            {
                _con.Close();
            }

            return affetedRows;
        }
        public static List<DatabaseSetting> getDatabaseDatabaseSettings()
        {
            List<DatabaseSetting> listConnectionString = new List<DatabaseSetting>();

            dynamic appSettings = getAppSettings();

            foreach (dynamic InitialCatalog in appSettings.InitialCatalogs)
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = appSettings.DataSource;
                builder.UserID = appSettings.UserID;
                builder.Password = appSettings.Password;
                builder.InitialCatalog = InitialCatalog.DatabaseName;
                builder.ConnectTimeout = 15;

                DatabaseSetting dbSetting = new DatabaseSetting();
                dbSetting.ConenctionString = builder.ConnectionString;
                dbSetting.EnablePatientSOAPAfterSaved = InitialCatalog.EnablePatientSOAPAfterSaved;
                dbSetting.EnableBillingInvoiceAfterSaved = InitialCatalog.EnableBillingInvoiceAfterSaved;
                dbSetting.EnableReupdateInventoryTrail = InitialCatalog.EnableReupdateInventoryTrail;

                listConnectionString.Add(dbSetting);
            }

            return listConnectionString;
        }
        public static dynamic getAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader($@"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            return _tempObject;
        }
    }
    public class SQLSelectParamenter
    {
        public SQLSelectParamenter()
        {


        }
        public SQLSelectParamenter(int topCount)
        {

            this.TopCount = topCount;
        }

        public int TopCount = 0;
    }
    public class DatabaseSetting
    {
        public string ConenctionString = "";
        public bool EnablePatientSOAPAfterSaved = false;
        public bool EnableBillingInvoiceAfterSaved = false;
        public bool EnableReupdateInventoryTrail = false;
    }
    public class ExportExcel
    {
        public string ReportFileName { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string sort { get; set; }
        public string SQLSelectStatement { get; set; }

        public List<ExportExcelHeaderData> headerData = new List<ExportExcelHeaderData>();
        public List<ExportExcelColumn> columns = new List<ExportExcelColumn>();
        public List<ExportExcelFooterData> footerData = new List<ExportExcelFooterData>();
        public List<ExportExcelGroupData> groups = new List<ExportExcelGroupData>();
    }
    public class ExportExcelColumn
    {
        public string name { get; set; }
        public string caption { get; set; }
        public string dataType { get; set; }
        public string expression { get; set; }
        public string format { get; set; }
    }
    public class ExportExcelHeaderData
    {
        public string name { get; set; }
        public string caption { get; set; }
        public string dataType { get; set; }
        public string format { get; set; }
    }
    public class ExportExcelFooterData
    {
        public string name { get; set; }
        public string caption { get; set; }
        public string expression { get; set; }
        public string dataType { get; set; }
        public string format { get; set; }
    }
    public class ExportExcelGroupData
    {
        public string name { get; set; }
    }
    public class ReturnArg: BaseProperty {

        public bool Success { get; set; } = true;
        public string Message { get; set; } = "";
    }
}
