﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VetcloudInventoryUpdater.GlobalFx
{
    public static class ValueConverter
    {
        public static decimal ToDecimal(dynamic value)
        {
            decimal result = 0;

            if (value is DBNull) value = "0";
            if (value is null) value = "0";

            string str = value.ToString();

            if (!str.All(char.IsNumber)) value = "0";

            try
            {
                result = Convert.ToDecimal(str);
            }
            catch (Exception ex)
            {

                result = 0;
            }

            return result;
        }

        public static int ToInt(dynamic value)
        {
            int result = 0;

            if (value is DBNull) value = "0";
            if (value is null) value = "0";

            string str = value.ToString();

            if (!str.All(char.IsNumber)) value = "0";

            try
            {
                result = Convert.ToInt16(str);
            }
            catch (Exception ex)
            {

                result = 0;
            }

            return result;
        }
        public static string ToJSON(DataTable Dt)
        {
            string[] StrDc = new string[Dt.Columns.Count];

            string HeadStr = string.Empty;
            for (int i = 0; i < Dt.Columns.Count; i++)
            {

                StrDc[i] = Dt.Columns[i].Caption;
                HeadStr += "\"" + StrDc[i] + "\":\"" + StrDc[i] + i.ToString() + "¾" + "\",";

            }

            HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);

            StringBuilder Sb = new StringBuilder();

            Sb.Append("[");

            for (int i = 0; i < Dt.Rows.Count; i++)
            {

                string TempStr = HeadStr;

                for (int j = 0; j < Dt.Columns.Count; j++)
                {

                    TempStr = TempStr.Replace(Dt.Columns[j] + j.ToString() + "¾", Dt.Rows[i][j].ToString().Trim());
                }

                Sb.Append("{" + TempStr + "},");
            }

            Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));

            if (Sb.ToString().Length > 0)
                Sb.Append("]");

            return StripControlChars(Sb.ToString());
        }
        private static string StripControlChars(string s)
        {
            return Regex.Replace(s, @"[^\x20-\x7F]", "");
        }
        public static System.Data.DataTable FromExcelToDataTable(string fileLocation, string sheetName = "")
        {
            System.Data.DataTable tbl = new System.Data.DataTable();
            bool hasHeader = true;
            FileInfo file = new FileInfo(fileLocation);

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            using (var pck = new ExcelPackage())
            {
                using (var stream = File.OpenRead(fileLocation))
                {
                    pck.Load(stream);
                }
                dynamic ws;

                if (sheetName.Length == 0)
                {
                    ws = pck.Workbook.Worksheets.First();
                }
                else
                {
                    ws = pck.Workbook.Worksheets[sheetName];
                }

                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? Regex.Replace(firstRowCell.Text, @"\r\n?|\n", "") : string.Format("Column {0}", firstRowCell.Start.Column));
                }

                tbl.Columns.Add("GUID", typeof(string));
                tbl.Columns.Add("RowIndex", typeof(string));
                tbl.Columns.Add("FileName", typeof(string));

                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();

                    foreach (var cell in wsRow)
                    {
                        try
                        {
                            row[cell.Start.Column - 1] = cell.Text;
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    Guid obj = Guid.NewGuid();

                    row["GUID"] = DateTime.Now.ToString("yyyyMMddHHmmss") + '-' + file.Name + "-" + rowNum.ToString("0000000000") + "-" + obj.ToString();
                    row["RowIndex"] = rowNum.ToString();
                    row["FileName"] = file.Name;
                }
            }

            return tbl;
        }
    }
}
