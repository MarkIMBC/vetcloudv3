﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace Class
{
    public class OtherFx
    {
        public static dynamic getAppSettings()
        {
            var currentDirectory = System.IO.Directory.GetCurrentDirectory();

            StreamReader r = new StreamReader($@"{currentDirectory}/appsettings.json");
            string jsonString = r.ReadToEnd();
            dynamic _tempObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            r.Close();

            return _tempObject;
        }
        public static void WriteLogFile(string fileName, string value)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            if (!Directory.Exists(fileInfo.Directory.FullName))
            {
                DirectoryInfo di = Directory.CreateDirectory(fileInfo.Directory.FullName);
            }

            if (!System.IO.File.Exists(fileName))
            {
                FileStream fs = System.IO.File.Create(fileName);
                fs.Close();
            }

            if (value.Length == 0) return;

            Byte[] valueByte = new UTF8Encoding(true).GetBytes(value);
            FileStream fsWrite = new FileStream(fileName, FileMode.Append);

            fsWrite.Write(valueByte, 0, valueByte.Length);
        }
    }
}
