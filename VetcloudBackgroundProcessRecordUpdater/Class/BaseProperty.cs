﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VetcloudInventoryUpdater.Class
{
    public class BaseProperty
    {
        public BaseProperty()
        {

        }
        public BaseProperty(DataRowView dr)
        {
            this.setProperties(dr);
        }
        public PropertyInfo[] GetProperties()
        {

            object a = this;
            Type t = this.GetType();
            PropertyInfo[] props = t.GetProperties();

            return props;
        }

        public void setProperties(DataRowView dr)
        {

            PropertyInfo[] propertyInfos = this.GetProperties();

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (dr.DataView.Table.Columns.Contains(propertyInfo.Name))
                {

                    if (propertyInfo != null)
                    {
                        dynamic value = dr[propertyInfo.Name];
                        propertyInfo.SetValue(this, value, null);
                    }
                }
            }

        }
        public void setProperties(DataRow dr)
        {

            PropertyInfo[] propertyInfos = this.GetProperties();

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (dr.Table.Columns.Contains(propertyInfo.Name))
                {

                    if (propertyInfo != null)
                    {
                        dynamic value = dr[propertyInfo.Name];
                        if (value is System.DBNull) value = null;

                        propertyInfo.SetValue(this, value, null);
                    }
                }
            }

        }
    }   
}
