﻿using Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VetcloudInventoryUpdater.GlobalFx;

namespace VetcloudInventoryUpdater.SQLLite
{
    public class RunAfterSaved
    {
        private SQLLiteQuery sqlLiteQuery;
        enum FilingStatusEnum: int
        {
            Filed = 1,
            Pending = 2,
            Approved = 3,
            Cancelled = 4,
            PartiallyServed = 5,
            FullyServed = 6,
            OverServed = 7,
            Waiting = 8,
            Ongoing = 9,
            Payment = 10,
            PartiallyPaid = 11,
            FullyPaid = 12,
            Done = 13,
            Confined = 14,
            Discharged = 15,
            ForBilling = 16,
            Used = 17
        }

        private string tableName = "";
        private string databaseName = "";
        public static void InsertPending(string moduleName, string databaseName, int ID_CurrentObject)
        {
            SQLLite.RunAfterSaved runAfterSaved = new SQLLite.RunAfterSaved(moduleName);

            runAfterSaved.InsertPending(databaseName, ID_CurrentObject);
        }
        public RunAfterSaved(string Name) {

            this.tableName = $"tList";
            this.databaseName = $"SQLLite_VetCloud_{Name.Replace(" ", "")}";

            this.sqlLiteQuery = new SQLLiteQuery($"SQLLite_VetCloud_{Name.Replace(" ","")}");

            if (!this.sqlLiteQuery.IsTableExist(tableName)) {

                this.sqlLiteQuery.ExecuteNonQuery($"CREATE TABLE {this.tableName} ( " +
                    $"Date DateTime, " +
                    $"DatabaseName VARCHAR(500), " +
                    $"ID_CurrentObject INT, " +
                    $"ID_FilingStatus INT, " +
                    $"DateOngoing DateTime, " +
                    $"DateDone DateTime " +
                    $")");
            }
        }

        public DataTable getPending(int topCount = 0)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            string topCountString = "";

            if (topCount > 0) topCountString = $"TOP {topCount}";

            arg = this.sqlLiteQuery.GetDataTable($"" +
                $"SELECT { topCountString } * " +
                $"FROM {tableName} " +
                $"WHERE " +
                $"IFNULL(ID_FilingStatus, 0) IN (0, 2, 9) " +
                $"ORDER BY Date DESC ");

            return arg.datatable;
        }
        public DataTable getAlLDone(int topCount = 0)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            string topCountString = "";

            if (topCount > 0) topCountString = $"TOP {topCount}";

            arg = this.sqlLiteQuery.GetDataTable($"" +
                $"SELECT { topCountString } * " +
                $"FROM {tableName} " +
                $"WHERE " +
                $"IFNULL(ID_FilingStatus, 0) IN (13) " +
                $"ORDER BY Date DESC ");

            return arg.datatable;
        }
        public DataTable getPendingPerDataBase(string databaseName, int topCount = 0)
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            string topCountString = "";

            if (topCount > 0) topCountString = $"TOP {topCount}";

            arg = this.sqlLiteQuery.GetDataTable($"" +
                $"SELECT { topCountString } * " +
                $"FROM {tableName} " +
                $"WHERE " +
                $"IFNULL(ID_FilingStatus, 0) IN (0, 2, 9) AND " +
                $"DatabaseName = '{ databaseName }'" +
                $"ORDER BY Date DESC ");

            return arg.datatable;
        }
        public DataTable getAll()
        {
            SQLLiteQueryResultArg arg = new SQLLiteQueryResultArg();

            arg = this.sqlLiteQuery.GetDataTable($"SELECT * FROM {tableName}");

            return arg.datatable;
        }
        public string getAllJSONString()
        {
            DataTable dt = this.getAll();
            string result = "";

            result = ValueConverter.ToJSON(dt);

            return result;
        }
        public void exportAllJSONString()
        {
            string databaseName = this.databaseName;

            DataTable dt = this.getAll();

            if (dt.Rows.Count > 0) {

                dynamic currentObjectValues = new ExpandoObject();
                Guid g = Guid.NewGuid();
                string filename = $"{DateTime.Now.ToString("yyyyMMdd_HHmmss")}-{databaseName}-{ g.ToString() }.json";
                currentObjectValues.Date = DateTime.Now;
                currentObjectValues.DatabaseName = databaseName;
                currentObjectValues.Data = dt;

                var fileContent = JsonConvert.SerializeObject(currentObjectValues);
                OtherFx.WriteLogFile("./Logs/ReRunAfterSaved/" + filename, fileContent);
            }
        }

        public void deleteAllDone()
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Done;
            this.sqlLiteQuery.ExecuteNonQuery($"DELETE FROM {tableName} WHERE ID_FilingStatus IN ({ID_FilingStatus});");
        }

        public void Insert(string DatabaseName, int ID_CurrentObject)
        {
            this.sqlLiteQuery.ExecuteNonQuery($"INSERT INTO {tableName} (DatabaseName, ID_CurrentObject, Date) VALUES('{DatabaseName}', { ID_CurrentObject }, datetime('now', 'localtime'));");
        }

        public void InsertPending(string DatabaseName, int ID_CurrentObject)
        {
            if (this.IsExist(DatabaseName, ID_CurrentObject)) return;

            this.Insert(DatabaseName, ID_CurrentObject);
        }
        public bool IsExist(string DatabaseName, int ID_CurrentObject)
        {
            bool result = false;
            SQLLiteQueryResultArg arg = this.sqlLiteQuery.GetScalarValue($"SELECT COUNT(*) " +
                $"FROM {this.tableName} " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}'  AND " +
                $"IFNULL(ID_FilingStatus, 0) IN (0, 2, 9) NOT IN (0, 2, 9) ");
                
            result =ValueConverter.ToInt(arg.scalarValue) > 0;

            return result;
        }
        public void SetStatusAsOngoing(string DatabaseName, int ID_CurrentObject)
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Ongoing;

            this.sqlLiteQuery.ExecuteNonQuery($"UPDATE {tableName} " +
                $"SET ID_FilingStatus = { ID_FilingStatus }, " +
                $"DateOngoing = datetime('now', 'localtime') " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}' " +
                $"");
        }

        public void SetStatusAsDone(string DatabaseName, int ID_CurrentObject)
        {
            int ID_FilingStatus = (int)FilingStatusEnum.Done;

            this.sqlLiteQuery.ExecuteNonQuery($"UPDATE {tableName} " +
                $"SET ID_FilingStatus = { ID_FilingStatus }, " +
                $"DateDone = datetime('now', 'localtime') " +
                $"WHERE " +
                $"ID_CurrentObject = {ID_CurrentObject} AND " +
                $"DatabaseName = '{DatabaseName}' " +
                $"");
        }
    }
}
