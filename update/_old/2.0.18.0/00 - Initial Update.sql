GO

exec _paddModelProperty
  'tBillingInvoice',
  'ConfinementDepositAmount',
  3

exec _paddModelProperty
  'tBillingInvoice',
  'RemainingDepositAmount',
  3

exec _prefreshAllviews

GO

CREATE OR
ALTER FUNCTION fGetConfinmentDeposit (@ID_Patient_Confinement INT)
RETURNS DECIMAL(18, 4)
AS
  BEGIN
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3

      SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
      FROM   tClientDeposit
      WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )

      RETURN @ConfinementDepositAmount
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetConfinmentDeposit](@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @CurrentCreditAmount Decimal(18, 4)= 0
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @RemainingDepositAmount Decimal(18, 4)= 0
      DECLARE @ID_Client INT = 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @MaxDate_ClientDeposit DateTime
      DECLARE @ClientDeposit TABLE
        (
           ID            INT,
           Date          DateTIme,
           Code          VARCHAR(50),
           DepositAmount Decimal(18, 4)
        )

      SELECT @ID_Client = ID_Client
      FROM   tPatient_Confinement
      where  ID = @ID_Patient_Confinement

      SELECT @CurrentCreditAmount = client.CurrentCreditAmount
      FROM   tclient client
      WHERE  ID = @ID_Client

      SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement);
      SET @RemainingDepositAmount = ISNULL(@CurrentCreditAmount, 0) - ISNULL(@ConfinementDepositAmount, 0)

      if( @RemainingDepositAmount < 0 )
        SET @RemainingDepositAmount = 0

      SELECT @MaxDate_ClientDeposit = MAX(Date)
      FROm   tClient_CreditLogs
      where  ID_Client = @ID_Client
             AND Code NOT IN (SELECT Code
                              FROM   @ClientDeposit)

      INSERT @ClientDeposit
      SELECT NULL,
             @MaxDate_ClientDeposit,
             'Remaining',
             @RemainingDepositAmount

      INSERT @ClientDeposit
      SELECT ID,
             Date,
             Code,
             DepositAmount
      FROM   dbo.vClientDeposit
      WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )

      SELECT '_',
             '' AS ClientDeposits;

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             @RemainingDepositAmount              RemainingDepositAmount,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount,
             ISNULL(@CurrentCreditAmount, 0)      TotalDepositAmount;

      SELECT *
      FROM   @ClientDeposit
  END

GO

GO

CREATE OR
ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount Decimal(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      if ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT COUNT(*)
         FROM   vUSER
         where  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = ISNULL(CurrentCreditAmount, 0)
            FROM   tPatient_Confinement confinement
                   inner join tClient client
                           on confinement.ID_Client = client.ID
            where  confinement.ID = @ID_Patient_Confinement
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                        Name_FilingStatus,
                   client.Name                    Name_Client,
                   patient.Name                   Name_Patient,
                   client.Address                 BillingAddress,
                   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee,
                   @ConfinementDepositAmount      ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GETDATE()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      if ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999 ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                           UnitPrice,
                   item.OtherInfo_DateExpiration                       DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP;
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                        UnitPrice,
                   ISNULL(item.UnitCost, 0)                         UnitCost,
                   item.OtherInfo_DateExpiration                    DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement;
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Detail
            WHERE  ID_BillingInvoice = @ID;
        END

      if ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID;
        END
  END;

GO

ALTER PROC [dbo].[pApproveBillingInvoice_validation] (@IDs_BillingInvoice typIntList READONLY,
                                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      /*---------------------------------------------------------------------------------------------------------------------*/
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      /* Validate Billing Invoices Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vBillingInvoice bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_BillingInvoice ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      /* Validate Remaining Inventory */
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @ValidateItemInventory TABLE
        (
           ID_Item  INT,
           ItemName VARCHAR(MAX),
           BIQty    INT,
           RemQty   INT
        )
      Declare @Count_ValidateItemInventory INT = 0

      INSERT @ValidateItemInventory
             (ID_Item,
              ItemName,
              BIQty,
              RemQty)
      SELECT biDetail.ID_Item,
             item.Name,
             SUM(biDetail.Quantity),
             0
      FROM   dbo.tBillingInvoice_Detail biDetail
             INNER JOIN dbo.tBillingInvoice biHed
                     ON biDetail.ID_BillingInvoice = biHed.ID
             INNER JOIN dbo.tItem item
                     ON item.ID = biDetail.ID_Item
             INNER JOIN @IDs_BillingInvoice ids
                     ON ids.ID = biHed.ID
      WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType
      GROUP  BY biDetail.ID_Item,
                item.Name

      UPDATE @ValidateItemInventory
      SET    RemQty = tbl.TotalRemQty
      FROM   @ValidateItemInventory invtItem
             INNER JOIN (SELECT ID_Item,
                                SUM(Quantity) TotalRemQty
                         FROM   tInventoryTrail
                         GROUP  BY ID_Item) tbl
                     ON invtItem.ID_Item = tbl.ID_Item

      DELETE FROM @ValidateItemInventory
      WHERE  ( ISNULL(RemQty, 0) - ISNULL(BIQty, 0) ) >= 0

      SELECT @Count_ValidateItemInventory = COUNT(*)
      FROM   @ValidateItemInventory

      IF ( @Count_ValidateItemInventory > 0 )
        BEGIN
            SET @message = 'The following item'
                           + CASE
                               WHEN @Count_ValidateItemInventory > 1 THEN 's are '
                               ELSE ' is '
                             END
                           + 'insufficient inventory count:';

            SELECT @message = @message + CHAR(10) + ItemName + ': Rem. Qty - '
                              + CONVERT(varchar(MAX), RemQty) + ' Bi Qty - '
                              + CONVERT(varchar(MAX), BIQty)
            FROM   @ValidateItemInventory;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      /* Validate Has No TotalAmount */
      DECLARE @ValidateHasNoTotalAmount TABLE
        (
           Code VARCHAR(30)
        );
      DECLARE @Count_ValidateHasNoTotalAmount INT = 0;

      INSERT @ValidateHasNoTotalAmount
             (Code)
      SELECT Code
      FROM   dbo.vBillingInvoice bi
             INNER JOIN @IDs_BillingInvoice ids
                     ON bi.ID = ids.ID
      WHERE  bi.TotalAmount IS NULL
              OR bi.NetAmount IS NULL

      SELECT @Count_ValidateHasNoTotalAmount = COUNT(*)
      FROM   @ValidateHasNoTotalAmount;

      IF ( @Count_ValidateHasNoTotalAmount > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateHasNoTotalAmount > 1 THEN 's have'
                               ELSE ' has '
                             END
                           + 'incorrect computation. Please check and try again';

            SELECT @message = @message + CHAR(10) + Code
            FROM   @ValidateHasNoTotalAmount;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      /* Validate if the confinement deposit amount is OUTDATED*/
      DECLARE @Count_validateOutDatedConfinemntDepositAmount INT = 0;
      DECLARE @validateOutDatedConfinemntDepositAmount TABLE
        (
           Code Varchar(100)
        )

      INSERT @validateOutDatedConfinemntDepositAmount
      SELECT bi.Code
      FROM   dbo.vBillingInvoice bi
             INNER JOIN @IDs_BillingInvoice ids
                     ON bi.ID = ids.ID
             INNER JOIN tClient client
                     on bi.ID_Client = client.ID
      WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0
             AND ISNULL(ConfinementDepositAmount, 0) <> ISNULL(client.CurrentCreditAmount, 0)

      SELECT @Count_validateOutDatedConfinemntDepositAmount = COUNT(*)
      FROM   @validateOutDatedConfinemntDepositAmount;

      IF ( @Count_validateOutDatedConfinemntDepositAmount > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_validateOutDatedConfinemntDepositAmount > 1 THEN 's have'
                               ELSE ' has '
                             END
                           + 'updated Deposit Credit. Please click refresh and save and try to click Approve Button.';

            SELECT @message = @message + CHAR(10) + Code
            FROM   @validateOutDatedConfinemntDepositAmount;

            THROW 50001, @message, 1;
        END;
  END;

GO

Alter PROC dbo.pApproveBillingInvoice (@IDs_BillingInvoice typIntList READONLY,
                                       @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          Update tBillingInvoice_Detail
          SET    UnitCost = ISNULL(item.UnitCost, 0)
          FROM   tBillingInvoice_Detail biDetail
                 INNER JOIn @IDs_BillingInvoice ids
                         on biDetail.ID_BillingInvoice = ids.iD
                 INNER JOIN tItem item
                         on biDetail.ID_Item = item.ID

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO dbo.tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 0 - detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          EXEC pUpdateItemCurrentInventory;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Used Deposit on Credit Logs  
          Declare @IDs_ClientDeposit typIntList
          DECLARE @ClientCredits typClientCredit

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ) * -1,
                          bi.Code,
                          'Use Deposit from '
                          + FORMAT(ISNULL(bi.ConfinementDepositAmount, 0), '#,#0.00')
                          + ' to '
                          + FORMAT(ISNULL( bi.RemainingDepositAmount, 0), '#,#0.00')
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Used_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          IF (SELECT COUNT(*)
              FROM   @ClientCredits) > 0
            begin
                exec pDoAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

GO

ALTER PROC dbo.pCancelBillingInvoice (@IDs_BillingInvoice typIntList READONLY,
                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ClientCredits typClientCredit

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ),
                          bi.Code,
                          'Rollback Deposit '
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0
                 AND ID_FilingStatus = @Approved_ID_FilingStatus

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0
                 AND item.ID_ItemType = @Inventoriable_ID_ItemType;

          EXEC dbo.pUpdateItemCurrentInventory;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Rollback Deposit on Credit Logs  
          Declare @IDs_ClientDeposit typIntList

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 

ALTER PROC [dbo].[pCancelPatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @ForBilling_ID_FilingStatus INT = 16;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotDischarged TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30),
		   BillingInvoice_Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotDischarged INT = 0;

      /* Validate Patient_Confinement Status is not Discharged*/
      INSERT @ValidateNotDischarged
             (Code,
              Name_FilingStatus, BillingInvoice_Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus,
			 BillingInvoice_Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus IN ( @Discharged_ID_FilingStatus, @Confined_ID_FilingStatus )
             and ISNULL(BillingInvoice_ID_FilingStatus, 0) NOT IN (0,  @ForBilling_ID_FilingStatus );

      SELECT @Count_ValidateNotDischarged = COUNT(*)
      FROM   @ValidateNotDischarged;

      IF ( @Count_ValidateNotDischarged > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotDischarged > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to Canceled:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus + ' (BI - '+ BillingInvoice_Name_FilingStatus +')'
            FROM   @ValidateNotDischarged;

            THROW 50001, @message, 1;
        END;
  END; 
GO