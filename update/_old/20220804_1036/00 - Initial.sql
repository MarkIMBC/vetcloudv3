GO
GO

----------------------------------------------------VIEWS --------------------------------------------------------------  
CREATE OR
ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         CASE
           WHEN LEN(ISNULL(patientSOAPPlan.Comment, '')) > 0 THEN patientSOAPPlan.Comment
           ELSE IsNull(patientSOAP.Name_SOAPType, '')
                + ' - '
                + IsNull(patientSOAPPlan.Name_Item, '')
         END                                                                                   Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks,
         patientSOAP.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + '0' + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks,
         patientAppnt.AttendingPhysician_Name_Employee
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellSched.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull(wellSched.Comment, '')                                        Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks,
                  wellness.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO 

ALTER PROC dbo.pGetDashboardDataSource (@ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      /* @@ClientDailyCount */
      SELECT @ClientDailyCount = COUNT(*)
      FROM   tClient
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) = Cast(GetDate() AS DATE)

      /* @@ClientMonthlyCount */
      SELECT @ClientMonthlyCount = COUNT(*)
      FROM   tClient
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) BETWEEN Cast(DateAdd(month, DatedIff(month, 0, GetDate()), 0) AS DATE) AND Cast(eoMonth(GetDate()) AS DATE)

      /* @@SalesDailyAmount */
      SELECT @SalesDailyAmount = SUM(IsNull(TotalAmount, 0))
      FROM   tBillingInvoice
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) = Cast(GetDate() AS DATE)

      SET @SalesDailyAmount = ISNULL(@SalesDailyAmount, 0)

      SELECT @SalesDailyAmount = @SalesDailyAmount + IsNull(DepositAmount, 0)
      FROM   vClientDeposit
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus IN ( 3, 17 )
             AND Cast(Date AS DATE) = Cast(GetDate() AS DATE)

      SET @SalesDailyAmount = ISNULL(@SalesDailyAmount, 0)

      ------------------------------------------
      ------------------------------------------
      /* @@SalesMonthlyAmount */
      SELECT @SalesMonthlyAmount = SUM(IsNull(TotalAmount, 0))
      FROM   tBillingInvoice
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) BETWEEN Cast(DateAdd(month, DatedIff(month, 0, GetDate()), 0) AS DATE) AND Cast(eoMonth(GetDate()) AS DATE)

      SET @SalesMonthlyAmount = ISNULL(@SalesMonthlyAmount, 0)

      SELECT @SalesMonthlyAmount = @SalesMonthlyAmount
                                   + IsNull(DepositAmount, 0)
      FROM   vClientDeposit
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus IN ( 3, 17 )
             AND Cast(Date AS DATE) BETWEEN Cast(DateAdd(month, DatedIff(month, 0, GetDate()), 0) AS DATE) AND Cast(eoMonth(GetDate()) AS DATE)

      ------------------------------------------
      ------------------------------------------
      SET @ClientDailyCount = ISNULL(@ClientDailyCount, 0)
      SET @ClientMonthlyCount = ISNULL(@ClientMonthlyCount, 0)
      SET @SalesDailyAmount = ISNULL(@SalesDailyAmount, 0)
      SET @SalesMonthlyAmount = ISNULL(@SalesMonthlyAmount, 0)

      SELECT '_'

      SELECT @ClientDailyCount                      ClientDailyCount,
             @ClientMonthlyCount                    ClientMonthlyCount,
             @SalesDailyAmount                      SalesDailyAmount,
             @SalesMonthlyAmount                    SalesMonthlyAmount,
             FORMAT(@ClientDailyCount, '#,#0')      FormattedClientDailyCount,
             FORMAT(@ClientMonthlyCount, '#,#0')    FormattedClientMonthlyCount,
             FORMAT(@SalesDailyAmount, '#,#0.00')   FormattedSalesDailyAmount,
             FORMAT(@SalesMonthlyAmount, '#,#0.00') FormattedSalesMonthlyAmount
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardSalesDataSource (@ID_UserSession INT,
                                             @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      --Declare a months table (or use a Date Dimension table)    
      Declare @Months Table
        (
           MonthNumber INT,
           MonthName   varchar(20)
        )

      INSERT INTO @Months
                  (MonthNumber,
                   MonthName)
      VALUES      (1,
                   'January'),
                  (2,
                   'February'),
                  (3,
                   'March'),
                  (4,
                   'April'),
                  (5,
                   'May'),
                  (6,
                   'June'),
                  (7,
                   'July'),
                  (8,
                   'August'),
                  (9,
                   'September'),
                  (10,
                   'October'),
                  (11,
                   'November'),
                  (12,
                   'December')

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear              DateYear,
             'Monthly Sales ' + @DateYear
             + ' (Billing Invoice)' Label

      SELECT MonthNumber,
             MonthName,
             SUM(TotalSales) TotalSales
      FROM   (SELECT m.MonthNumber,
                     m.MonthName,
                     ISNULL(clientDeposit.DepositAmount, 0) AS TotalSales
              FROM   @Months m
                     LEFT JOIN tClientDeposit clientDeposit
                            ON DatePart(MONTH, [Date]) = M.MonthNumber
                               AND clientDeposit.ID_Company = @ID_Company
                               AND clientDeposit.ID_FilingStatus IN ( 3, 17 )
                               AND YEAR(clientDeposit.Date) = @DateYear
              UNION ALL
              SELECT m.MonthNumber,
                     m.MonthName,
                     ISNULL(billing.TotalAmount, 0) AS TotalSales
              FROM   @Months m
                     LEFT JOIN tBillingInvoice billing
                            ON DatePart(MONTH, [Date]) = M.MonthNumber
                               AND billing.ID_Company = @ID_Company
                               AND billing.ID_FilingStatus = 3
                               AND YEAR(billing.Date) = @DateYear) tbl
      GROUP  BY MonthNumber,
                MonthName
      ORDER  BY MonthNumber,
                MonthName
  END

GO

GO

Create OR
ALTER PROC [dbo].[pCanceledPatientAppointment] (@IDs_PatientAppointment typIntList READONLY,
                                                @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCanceledPatientAppointment_validation
            @IDs_PatientAppointment,
            @ID_UserSession;

          UPDATE dbo.tPatientAppointment
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatientAppointment bi
                 INNER JOIN @IDs_PatientAppointment ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 

GO

CREATE OR
ALTER PROCEDURE pGetVeterinaryHealthCertificate (@ID         INT = -1,
                                                 @ID_Client  INT = NULL,
                                                 @ID_Patient INT = NULL,
                                                 @ID_Session INT = NULL)
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   patient.Color
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '-- NEW --' AS [Code],
                           NULL        AS [Name],
                           GETDATE()   AS [Date],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           AS ID_FilingStatus,
                           'N/A'       AS [DestinationAddress],
                           @ID_Client  ID_Client,
                           @ID_Patient ID_Patient,
                           1           ID_VaccinationOption) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vVeterinaryHealthCertificate H
            WHERE  H.ID = @ID
        END
  END

GO 

exec _pRefreshAllViews