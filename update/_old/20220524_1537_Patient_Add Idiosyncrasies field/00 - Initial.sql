IF COL_LENGTH('dbo.tPatient', 'Idiosyncrasies') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatient',
        'Idiosyncrasies',
        1
  END

GO

exec _pRefreshAllViews

GO

ALTER VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         CASE
           WHEN len(Trim(IsNull(CustomCode, ''))) = 0 THEN Code
           ELSE CustomCode
         END                                                                                      Code,
         Name,
         ID_Client,
         Name_Client,
         Email,
         ISNULL(Species, '')                                                                      Species,
         ISNULL(Name_Gender, '')                                                                  Name_Gender,
         ContactNumber,
         IsDeceased,
         DateDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile,
         IsNull(IsActive, 0)                                                                      IsActive,
         WaitingStatus_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                                    Microchip,
         ISNULL(Color, '')                                                                        Color,
         dbo.fGetAge(DateBirth, GETDATE(), '')                                                    Age,
         Idiosyncrasies
  FROM   dbo.vPatient

GO 
