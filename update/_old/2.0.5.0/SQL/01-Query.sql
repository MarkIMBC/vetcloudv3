ALTER VIEW [dbo].[vPaymentTransaction]
AS
SELECT H.*,
       CONVERT(VARCHAR(100), H.Date, 101) DateString,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       approvedUser.Name AS ApprovedBy_Name_User,
       cancelUser.Name AS CanceledBy_Name_User,
       fs.Name Name_FilingStatus,
	   biHed.Name_Client,
       biHed.Name_Patient,
       taxScheme.Name Name_TaxScheme,
       biHed.GrossAmount GrossAmount_BillingInvoice,
       biHed.VatAmount VatAmount_BillingInvoice,
       biHed.NetAmount NetAmount_BillingInvoice,
       biHed.RemainingAmount RemainingAmount_BillingInvoice,
       pMth.Name Name_PaymentMethod,
	   cardType.Name Name_CardType,
       CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,
       CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
       CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
       CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString
FROM tPaymentTransaction H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tUser approvedUser
        ON H.ID_ApprovedBy = approvedUser.ID
    LEFT JOIN dbo.tUser cancelUser
        ON H.ID_CanceledBy = cancelUser.ID
    LEFT JOIN dbo.tFilingStatus fs
        ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.vBillingInvoice biHed
        ON biHed.ID = H.ID_BillingInvoice
    LEFT JOIN dbo.tPaymentMethod pMth
        ON pMth.ID = H.ID_PaymentMethod
    LEFT JOIN dbo.tTaxScheme taxScheme
        ON taxScheme.ID = biHed.ID_TaxScheme
	LEFT JOIN dbo.tCardType cardType
        ON cardType.ID = H.ID_CardType

GO


CREATE OR ALTER  VIEW vzPaymentTransactionSummaryReport
as

	SELECT	biHed.ID ID_BillingInvoice,
			pHed.ID ID_PaymentTransaction,
			biHed.Code Code_BillingInvoice,
			biHed.Date Date_BillingInvoice,
			biHed.TotalAmount BalanceAmount ,
			pHed.Code Code_PaymentTransaction,
			pHed.Date Date_PaymentTransaction,
			client.Name Name_Client,
			patient.Name Name_Patient,
			biHed.ID_Company,
			pHed.ID_PaymentMethod,
			payMethod.Name Name_PaymentMethod,
			phed.PayableAmount,
			CASE WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount ELSE phed.PaymentAmount END PaymentAmount,
			pHed.RemainingAmount,
			company.ImageLogoLocationFilenamePath,
			company.Name Name_Company,
			company.Address Address_Company
	FROM    tPaymentTransaction pHed 
	INNER JOIN tBillingInvoice biHed 
		on biHed.ID = pHed.ID_BillingInvoice
	LEFT JOIN tClient client 
		ON client.ID = biHed.ID_Client
	LEFT JOIN tPatient patient 
		ON patient.ID = biHed.ID_Patient
	LEFT JOIN tPaymentMethod payMethod
		ON payMethod.ID = pHed.ID_PaymentMethod
	LEFT JOIN dbo.vCompany company
		ON company.ID = biHed.ID_Company
	WHERE biHed.Payment_ID_FilingStatus IN (11, 12)
GO

ALTER VIEW [dbo].[vzPatientSOAP]
AS
SELECT
  patientSOAP.ID
 ,patientSOAP.Code
 ,patientSOAP.Name_Client
 ,patientSOAP.Name_Patient
 ,patientSOAP.Date
 ,patientSOAP.Comment
 ,patientSOAP.Name_CreatedBy
 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Subjective
 ,REPLACE(REPLACE(REPLACE(patientSOAP.Objective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
 ,REPLACE(REPLACE(REPLACE(patientSOAP.Prescription, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Prescription
 ,patientSOAP.ID_Company
 ,patientSOAP.Name_FilingStatus
 ,company.ImageLogoLocationFilenamePath
 ,company.Name Name_Company
 ,company.Address Address_Company
FROM dbo.vPatient_SOAP patientSOAP
LEFT JOIN dbo.vCompany company
  ON company.ID = patientSOAP.ID_Company;

GO

ALTER PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

  DECLARE @Success BIT = 1;
  Declare @MDIgnacioAnimalClinic_ID_Company INT = 8
  Declare @AnimaCareVeterinaryClinic_ID_Company INT = 33
  Declare @AssumptaDogAndCatClinic_ID_Company INT = 38
  Declare @DVMAnimalClinic_ID_Company INT = 24
  Declare @BalagtasAnimalClinicandGroomingCenter_ID_Company INT = 35
  Declare @RDTVeterinaryClinic_ID_Company INT = 41
  Declare @PetFamilyAnimalClinicandGroomingCenter_ID_Company INT = 7
  Declare @CardinalPetClinic_ID_Company INT = 14
  Declare @FursLifeVeterinarClinic_ID_Company INT = 16

  SELECT
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

  SELECT
	c.Name Name_Company
   ,client.Name Name_Client
   ,client.ContactNumber ContactNumber_Client
   ,soapPlan.ID ID_Patient_SOAP_Plan
   ,soapPlan.DateReturn
   ,soapPlan.Name_Item
   ,ISNULL(patientSOAP.Comment, '') Comment
   ,'Good day, ' + client.Name + CHAR(9) + CHAR(13) + ' You have an upcoming appointment for your pet ''' +
    patient.Name  + '''' + ' at ' +  c.Name   + ' at ' +
    FORMAT(soapPlan.DateReturn, 'MM/dd/yyyy dddd') + '.' + CHAR(9) + CHAR(13) +
    'Plan: ' + +CHAR(9) + CHAR(13) +
    ' ' + soapPlan.Name_Item +
    CASE
      WHEN LEN(ISNULL(soapPlan.Comment, '')) > 0 THEN ' - '
      ELSE ''
    END + ISNULL(soapPlan.Comment, '')   + CHAR(9) + CHAR(13) + 
	Case WHEN LEN(ISNULL(c.ContactNumber, '')) > 0 THEN 'If you have questions, kindly message ' + c.ContactNumber + '.' ELSE '' END   + ' ' +
	Case WHEN LEN(ISNULL(c.Address, '')) > 0 THEN c.Address ELSE '' END   + ''
	Message,
    CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
  FROM dbo.tPatient_SOAP patientSOAP
  LEFT JOIN dbo.tPatient patient
    ON patient.ID = patientSOAP.ID_Patient
  LEFT JOIN dbo.tClient client
    ON client.ID = patient.ID_Client
  LEFT JOIN tCompany c
    ON c.iD = patientSOAP.ID_Company
  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
    ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
  WHERE patientSOAP.ID_FilingStatus IN (3)
	AND ISNULL(soapPlan.IsSentSMS, 0) = 0
	AND ( (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE()))
	OR (CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE())) )
	AND patientSOAP.ID_Company IN (
		1, 
		@MDIgnacioAnimalClinic_ID_Company, 
		@AnimaCareVeterinaryClinic_ID_Company, 
		@DVMAnimalClinic_ID_Company,
		@BalagtasAnimalClinicandGroomingCenter_ID_Company,
		@PetFamilyAnimalClinicandGroomingCenter_ID_Company,
		@AssumptaDogAndCatClinic_ID_Company,
		@RDTVeterinaryClinic_ID_Company,
		@CardinalPetClinic_ID_Company,
		@FursLifeVeterinarClinic_ID_Company
	)

	ORDEr BY c.Name
END

GO

DECLARE @ID_REPORT_PaymentTransactionDetailReport UNIQUEIDENTIFIER;

SELECT @ID_REPORT_PaymentTransactionDetailReport = Oid
FROM dbo._tReport
WHERE Name = 'PaymentTransactionDetailReport';


UPDATE _tNavigation SET Caption = 'Payment Trans. Detail'
WHERE  Name = 'PaymentTransactionDetailReport_Navigation'

IF (NOT EXISTS
(
    SELECT *
    FROM dbo.tCustomNavigationLink
    WHERE [Oid_Report] = @ID_REPORT_PaymentTransactionDetailReport
))
BEGIN

	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   , 'PAYMENTTRANSACTIONDETAILREPORT'
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,NULL
			   ,'PaymentTransactionDetailReport'
			   ,3 
			   ,@ID_REPORT_PaymentTransactionDetailReport)

END


IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_PaymentTransactionDetailReport
          AND Name = 'Date_PaymentTransaction'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Date_PaymentTransaction',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_PaymentTransactionDetailReport, -- ID_Report - uniqueidentifier
        6,                     -- ID_ControlType - int
        6,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Date_PaymentTransaction'                 -- Caption - varchar(300)
        );
END;



IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_PaymentTransactionDetailReport
          AND Name = 'ID_PaymentMethod'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'ID_PaymentMethod',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_PaymentTransactionDetailReport, -- ID_Report - uniqueidentifier
        8,                     -- ID_ControlType - int
        2,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'ID_PaymentMethod'                 -- Caption - varchar(300)
        );
END;


IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_PaymentTransactionDetailReport
          AND Name = 'Name_Client'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Name_Client',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_PaymentTransactionDetailReport, -- ID_Report - uniqueidentifier
        1,                     -- ID_ControlType - int
        1,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Name_Client'                 -- Caption - varchar(300)
        );
END;


IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_PaymentTransactionDetailReport
          AND Name = 'Name_Patient'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Name_Patient',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_PaymentTransactionDetailReport, -- ID_Report - uniqueidentifier
        1,                     -- ID_ControlType - int
        1,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Name_Patient'                 -- Caption - varchar(300)
        );
END;


IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_PaymentTransactionDetailReport
          AND Name = 'Code_PaymentTransaction'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Code_PaymentTransaction',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_PaymentTransactionDetailReport, -- ID_Report - uniqueidentifier
        1,                     -- ID_ControlType - int
        1,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Code_PaymentTransaction'  -- Caption - varchar(300)
        );
END;

GO