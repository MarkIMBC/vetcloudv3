exec _pAddModelProperty
  'tUserRole',
  'IsAdministrator',
  4

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vUserAdministrator
as
  select _user.ID,
         _user.ID                                                    ID_User,
         _user.ID_Employee,
         CONVERT(BIT, MAX(CONVERT(INT, _userroles.IsAdministrator))) IsAdministrator
  FROM   tUser _user
         LEFT join tUser_Roles _user_roles
                on _user.ID = _user_roles.ID_User
         LEFT join tUserRole _userroles
                on _user_roles.ID_UserRole = _userroles.ID
  GROUP  BY _user.ID,
            _user.ID_Employee

GO

ALTER PROC [dbo].[pGetEmployee] @ID         INT = -1,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Employee INT;
      DECLARE @UserAccount_ID_User INT;
      DECLARE @UserRole_IsAdministrator bit = 0;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserAccount_ID_User = ID
      FROM   tUser
      WHERE  ID_Employee = @ID
             AND IsActive = 1

      SELECT @UserRole_IsAdministrator = ISNULL(IsAdministrator, 0)
      FROM   vUserAdministrator
      where  ID_User = @ID_User

      SELECT @ID_Employee = ID_Employee,
             @ID_Company = emp.ID_Company
      FROM   dbo.tUser _user
             INNER JOIN dbo.tEmployee emp
                     ON emp.ID = _user.ID_Employee
      WHERE  _user.ID = @ID_User;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   (SELECT NULL                 AS [_],
                           -1                   AS [ID],
                           ''                   AS [Code],
                           NULL                 AS [Name],
                           1                    AS [IsActive],
                           NULL                 AS [Comment],
                           NULL                 AS [DateCreated],
                           NULL                 AS [DateModified],
                           NULL                 AS [ID_CreatedBy],
                           NULL                 AS [ID_LastModifiedBy],
                           @ID_Company          [ID_Company],
                           ''                   [Company],
                           ''                   FirstName,
                           ''                   LastName,
                           ''                   MiddleName,
                           @UserAccount_ID_User UserAccount_ID_User,
                           0                    IsSystemUsed) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   @UserAccount_ID_User      UserAccount_ID_User,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   vEmployee H
            WHERE  H.ID = @ID
        END
  END

GO

ALTER PROC pCreateUser(@ID_Company  INT,
                       @ID_Employee INT,
                       @Username    varchar(MAX))
as
  BEGIN
      Declare @code Varchar(MAX) = ''

      SET @Username = REPLACE(LOWER(@Username), ' ', '')

      IF(SELECT COUNT(*)
         FROM   vUser
         WHERE  ID_Company = @ID_Company
                AND ID_Employee = @ID_Employee) = 0
        BEGIN
            SELECT @code = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            INSERT dbo.tUser
                   (Code,
                    Name,
                    IsActive,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ID_Employee,
                    Username,
                    ID_UserGroup,
                    Password,
                    IsRequiredPasswordChangedOnLogin,
                    ID_Patient)
            SELECT '',
                   emp.Name,
                   1,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   emp.ID,
                   LOWER(@code) + '-' + LOWER(@Username),
                   1,
                   LEFT(NEWID(), 4),
                   1,
                   NULL
            FROM   dbo.tEmployee emp
            WHERE  emp.ID_Company = @ID_Company
                   AND emp.ID = @ID_Employee
        END
  --DECLARE @ID_User INT  
  --SET @ID_User = @@IDENTITY  
  --SELECT Name_Company,  
  --       Employee,  
  --       Username,  
  --       Password  
  --FROM   vUser  
  --where  ID = @ID_User  
  END

GO

CREATE OR
ALTER PROC pDoCreateUser(@CompanyID   INT,
                         @ID_Employee INT,
                         @Username    varchar(MAX))
as
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''

      BEGIN TRY
          exec pCreateUser
            @CompanyID,
            @ID_Employee,
            @Username
      END TRY
      BEGIN CATCH
          SET @Success = 0
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_'

      SELECT @Success Success,
             @message message
  END

GO

CREATE  OR
ALTER PROC pUpdateEmployeetUserInfo(@EmployeeID     INT,
                                    @Username       VARCHAR(MAX),
                                    @Password       VARCHAR(MAX),
                                    @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      Update vUser
      SET    Username = @UserName,
             Password = @Password
      where  ID_Employee = @EmployeeID
             AND ID_Company = @ID_Company

      SELECT '_'

      SELECT @Success Success,
             @message Message
  END

GO

Update tUserRole
set    IsAdministrator = 1
where  Name = 'Administrator'

UPDATE tUser
SET    IsActive = 0
WHERE  ID IN (SELECT MIN(ID)
              FROM   VUser
              where  ID_Employee IS NOT NULL
              GROUP  BY ID_Company,
                        ID_Employee
              HAVING COUNT(*) > 1)

Update vActiveUserRole
set    IsActive = 0
where  ID IN ( 13, 5, 6, 7,
               8, 9, 11, 10,
               18, 17, 19, 20,
               21, 22, 47, 69 ) 
