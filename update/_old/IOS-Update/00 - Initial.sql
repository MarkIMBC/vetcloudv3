GO

CREATE OR
ALTER VIEW vReceivingReport_Listview
AS
  SELECT ID,
         Date,
         Code,
         GrossAmount,
         VatAmount,
         NetAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Name_FilingStatus,
         ServingStatus_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         Code_PurchaseOrder,
         ID_Company,
         Name_Supplier
  FROM   vReceivingReport

GO

CREATE OR
ALTER VIEW [dbo].[vPaymentTransaction]
AS
  SELECT H.*,
         CONVERT(VARCHAR(100), H.Date, 101)            DateString,
         UC.Name                                       AS CreatedBy,
         UM.Name                                       AS LastModifiedBy,
         approvedUser.Name                             AS ApprovedBy_Name_User,
         cancelUser.Name                               AS CanceledBy_Name_User,
         fs.Name                                       Name_FilingStatus,
         ISNULL(biHed.Name_Client, WalkInCustomerName) Name_Client,
         biHed.Name_Patient,
         taxScheme.Name                                Name_TaxScheme,
         biHed.Code                                    Code_BillingInvoice,
         biHed.GrossAmount                             GrossAmount_BillingInvoice,
         biHed.VatAmount                               VatAmount_BillingInvoice,
         biHed.NetAmount                               NetAmount_BillingInvoice,
         biHed.RemainingAmount                         RemainingAmount_BillingInvoice,
         pMth.Name                                     Name_PaymentMethod,
         cardType.Name                                 Name_CardType,
         CONVERT(VARCHAR, H.DateCreated, 0)            DateCreatedString,
         CONVERT(VARCHAR, H.DateModified, 0)           DateModifiedString,
         CONVERT(VARCHAR, H.DateApproved, 0)           DateApprovedString,
         CONVERT(VARCHAR, H.DateCanceled, 0)           DateCanceledString,
         biHed.PatientNames
  FROM   tPaymentTransaction H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.vBillingInvoice biHed
                ON biHed.ID = H.ID_BillingInvoice
         LEFT JOIN dbo.tPaymentMethod pMth
                ON pMth.ID = H.ID_PaymentMethod
         LEFT JOIN dbo.tTaxScheme taxScheme
                ON taxScheme.ID = biHed.ID_TaxScheme
         LEFT JOIN dbo.tCardType cardType
                ON cardType.ID = H.ID_CardType

GO

CREATE OR
ALTER VIEW vPaymentTransaction_Listview
AS
  SELECT ID,
         Date,
         Code,
         DateString,
         Name_Client,
         Name_Patient,
         Name_PaymentMethod,
         PayableAmount,
         PaymentAmount,
         ChangeAmount,
         Name_FilingStatus,
         ID_Company
  FROM   dbo.vPaymentTransaction

GO

ALTER VIEW vItemInventoriableForBillingLookUp
AS
  SELECT ID,
         Name,
         ISNULL(UnitPrice, 0)                                  UnitPrice,
         ISNULL(UnitCost, 0)                                   UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company,
         CASE
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
           ELSE ''
         END                                                   RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays,
         OtherInfo_DateExpiration,
         ID_ItemType
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO

ALTER VIEW vRemainingQuantityPurchaseOrderDetail
AS
  SELECT detail.ID,
         detail.ID_Item,
         detail.Name_Item,
         detail.Quantity,
         detail.RemainingQuantity,
         detail.UnitPrice,
         detail.ID_PurchaseOrder,
         hed.ID_FilingStatus,
         hed.Name_FilingStatus,
         hed.ServingStatus_Name_FilingStatus,
         hed.ID_Company,
         UnitCost
  FROM   vPurchaseOrder_Detail detail
         INNER JOIN vPurchaseOrder hed
                 On hed.ID = detail.ID_PurchaseOrder
  WHERE  ISNULL(detail.RemainingQuantity, 0) > 0

GO

UPDATE _tNavigation
SET    SeqNo = 10,
       Route = 'Client'
WHERE  Name = 'Client_ListView'

UPDATE _tNavigation
SET    SeqNo = 200,
       Route = 'Patient'
WHERE  Name = 'Patient_ListView'

UPDATE _tNavigation
SET    SeqNo = 300,
       Route = 'ConfinementList'
WHERE  Name = 'Patient_Confinement_ListView'

UPDATE _tNavigation
SET    SeqNo = 400,
       Route = 'Patient_SOAP'
WHERE  Name = 'PatientSOAPList_ListView'

UPDATE _tNavigation
SET    SeqNo = 500,
       Route = 'PatientWellnessList'
WHERE  Name = 'Patient_Wellness_ListView'

UPDATE _tNavigation
SET    SeqNo = 600,
       Route = 'VeterinaryHealthCertificateList'
WHERE  Name = 'VeterinaryHealthCertificate_ListView'

UPDATE _tNavigation
SET    SeqNo = 2000,
       Route = 'PatientWaitingList'
WHERE  Name = 'PatientWaitingList_ListView'

UPDATE _tNavigation
SET    SeqNo = 50,
       Route = 'ItemInventoriable'
WHERE  Name = 'ItemInventoriable_ListView'

UPDATE _tNavigation
SET    SeqNo = 52,
       Route = 'ItemService'
WHERE  Name = 'ItemService_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = 'PurchaseOrderList',
       Caption = 'Purchase Order'
WHERE  Name = 'PurchaseOrder_ListView'

UPDATE _tNavigation
SET    SeqNo = 200,
       Route = 'ReceivingReportList'
WHERE  Name = 'ReceivingReport_ListView'

UPDATE _tNavigation
SET    SeqNo = 999999999,
       Route = 'SupplierList'
WHERE  Name = 'Supplier_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = 'ForBillingList'
WHERE  Name = 'ForBilling_ListView'

UPDATE _tNavigation
SET    SeqNo = 500,
       Route = 'BillingInvoice'
WHERE  Name = 'BillingInvoice_ListView'

UPDATE _tNavigation
SET    SeqNo = 800,
       Route = 'BillingInvoiceWalkInList'
WHERE  Name = 'BillingInvoiceWalkInList_ListView'

UPDATE _tNavigation
SET    SeqNo = 1200,
       Route = 'PaymentTransactionList'
WHERE  Name = 'PaymentTransaction_ListView'

UPDATE _tNavigation
SET    SeqNo = 1500,
       Route = ''
WHERE  Name = 'Payable_ListView'

UPDATE _tNavigation
SET    SeqNo = 50,
       Caption = 'Employee',
       Route = 'Employee'
WHERE  Name = 'Emplyee_ListView'

UPDATE _tNavigation
SET    SeqNo = 10,
       Route = 'ReportInvoiceItemsAndServices'
WHERE  Name = 'POSSummary_Navigation'

UPDATE _tNavigation
SET    SeqNo = 20,
       Route = 'ReportBillingInvoicePaidList'
WHERE  Name = 'BillingInvoicePaidListReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 30,
       Route = 'ReportPaymentTransactionDetail'
WHERE  Name = 'PaymentTransactionDetailReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 40,
       Route = 'ReportInventorySummary'
WHERE  Name = 'InventorySummaryReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 50,
       Route = 'ReportInventoryDetail'
WHERE  Name = 'InventoryDetailReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 60,
       Route = 'ReportSalesIncome'
WHERE  Name = 'SalesIncomeReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 70,
       Route = ''
WHERE  Name = 'ReceivedDepositCredits_Navigation'

UPDATE _tNavigation
SET    SeqNo = 80,
       Route = 'ReportBillingInvoiceAging'
WHERE  Name = 'BillingInvoiceAgingReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 999999999,
       Route = ''
WHERE  Name = 'TextBlast_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = ''
WHERE  Name = 'EmployeeInfo_ListView'

UPDATE _tNavigation
SET    SeqNo = 2000,
       Route = ''
WHERE  Name = 'CompanyInfo_ListView'

UPDATE _tNavigation
SET    SeqNo = 90,
       Route = 'ScheduleList'
WHERE  Name = 'Schedule_ListView'

Update _tNavigation
SET    Route = null
where  Route = ''

GO

exec pGetUserNavigation
  60792 
