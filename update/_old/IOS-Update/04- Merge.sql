GO

CREATE OR
ALTER PROC pMergePatientRecordByCompanyID(@ID_Company               INT,
                                          @Source_Code_Patient      VARCHAR(MAX),
                                          @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------    
      DECLARE @Source1_ID_Patient INT
      DECLARE @Destination_ID_Patient INT
      DECLARE @Source1_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Patient = ID,
             @Source1_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Source_Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Patient = ID,
             @Destination_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Destination_Code_Patient
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Patient - '
                     + ' from ' + @Source1_Name_Patient + ' ('
                     + @Source_Code_Patient + ') to '
                     + @Destination_Name_Patient + ' ('
                     + @Destination_Code_Patient + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Source1_ID_Patient
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Destination_ID_Patient
      --       AND ID_Company = @ID_Company
      exec pMergePatientRecord
        @Source1_ID_Patient,
        @Destination_ID_Patient,
        @Comment

      Update tPatient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Patient
             and ID_Company = @ID_Company
  END

GO

CREATE OR
ALTER PROC pMergePatientRecordByCompany(@GUID_Company             VARCHAR(MAX),
                                        @Source_Code_Patient      VARCHAR(MAX),
                                        @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergePatientRecordByCompany
        @ID_Company,
        @Source_Code_Patient,
        @Destination_Code_Patient
  END

GO

GO

CREATE OR
ALTER PROC pDoMergePatientRecord(@From_ID_Patient INT,
                                 @To_ID_Patient   INT,
                                 @ID_UserSession  INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Patient VARCHAR(MAX) = ''
      DECLARE @To_Code_Patient VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Patient

      SELECT @To_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Patient

      --select @To_Code_Patient,
      --       @To_ID_Patient
      BEGIN TRY
          exec pMergePatientRecordByCompanyID
            @ID_Company,
            @From_Code_Patient,
            @To_Code_Patient
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO

GO

CREATE   OR
ALTER PROC pMergeClientRecordByCompanyID(@ID_Company              INT,
                                         @Source_Code_Client      VARCHAR(MAX),
                                         @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      --SELECT *
      --FROm   vCompanyActive
      --WHERE  ID = @ID_Company
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------      
      DECLARE @Source1_ID_Client INT
      DECLARE @Destination_ID_Client INT
      DECLARE @Source1_Name_Client VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Client VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Client = ID,
             @Source1_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Source_Code_Client
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Client = ID,
             @Destination_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Destination_Code_Client
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Client - '
                     + ' from ' + @Source1_Name_Client + ' ('
                     + @Source_Code_Client + ') to '
                     + @Destination_Name_Client + ' ('
                     + @Destination_Code_Client + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1'
      --FROm   vClient
      --WHERE  ID = @Source1_ID_Client
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination'
      --FROm   vClient
      --WHERE  ID = @Destination_ID_Client
      --       AND ID_Company = @ID_Company
      exec pMergeClientRecord
        @Source1_ID_Client,
        @Destination_ID_Client,
        @Comment

      Update tClient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Client
             and ID_Company = @ID_Company

      Update tPatient_SOAP
      SET    ID_Client = patient.ID_Client
      FROM   tPatient_SOAP _soap
             inner join vPatient patient
                     on _soap.ID_Patient = patient.ID
      where  _soap.ID_Client <> patient.ID_Client
             AND patient.ID_Client = @Destination_ID_Client
  END

GO

GO

CREATE   OR
ALTER PROC pMergeClientRecordByCompany(@GUID_Company            VARCHAR(MAX),
                                       @Source_Code_Client      VARCHAR(MAX),
                                       @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergeClientRecordByCompanyID
        @ID_Company,
        @Source_Code_Client,
        @Destination_Code_Client
  END

GO

CREATE OR
ALTER PROC pDoMergeClientRecord(@From_ID_Client INT,
                                @To_ID_Client   INT,
                                @ID_UserSession INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Client VARCHAR(MAX) = ''
      DECLARE @To_Code_Client VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Client

      SELECT @To_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Client

      --select @From_ID_Client,
      --       @From_Code_Client,
      --       @ID_Company
      --select @To_ID_Client,
      --       @To_Code_Client,
      --       @ID_Company
      BEGIN TRY
          exec pMergeClientRecordByCompanyID
            @ID_Company,
            @From_Code_Client,
            @To_Code_Client
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO 
