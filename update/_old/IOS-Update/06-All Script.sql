GO

exec _pAddModelProperty
  'tBillingInvoice',
  'ID_Patient_Lodging',
  2

GO

exec _pAddModelProperty
  'tPatientWaitingList',
  'DateSent',
  5

GO

exec _pAddModelProperty
  'tPatientWaitingList',
  'IsSentSMS',
  4

GO

IF COL_LENGTH('dbo.tCompany_SMSSetting', 'SMSFormatRescheduleAppointment') IS NULL
  BEGIN
      ALTER TABLE dbo.tCompany_SMSSetting
        ADD SMSFormatRescheduleAppointment VARCHAR (MAX) NULL;
  END

GO

if OBJECT_ID('dbo.tSMSList') is null
  begin
      exec _pCreateAppModuleWithTable
        'tSMSList',
        1,
        NULL,
        NULL
  END

GO

if OBJECT_ID('dbo.tPatientWaitingList_SMSStatus') is null
  begin
      exec _pCreateAppModuleWithTable
        'tPatientWaitingList_SMSStatus',
        1,
        1,
        NULL
  END

GO

exec _pAddModelProperty
  'tPatientWaitingList_SMSStatus',
  'iTextMo_Status',
  2

GO

exec _pAddModelProperty
  'tPatientWaitingList_SMSStatus',
  'ID_Patient_Wellness_Schedule',
  2

GO

exec _pAddModelProperty
  'tPatientWaitingList_SMSStatus',
  'DateSent',
  5

GO

exec _pAddModelProperty
  'tPatientWaitingList_SMSStatus',
  'ID_PatientWaitingList',
  2

GO

exec _pRefreshAllViews

GO

CREATE   OR
ALTER FUNCTION dbo.fGetSMSMessageSMSFormatRescheduleAppointment (@SMSFormat             VARCHAR(MAX),
                                                                 @Name_Company          Varchar(MAX),
                                                                 @ContactNumber_Company Varchar(MAX),
                                                                 @Date                  DateTime,
                                                                 @Client                Varchar(MAX),
                                                                 @Pet                   Varchar(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @message VARCHAR(MAX) = ''
        + 'Hi /*Client*/, /*Pet*/ missed his appointment today, /*Date*/. '
        + 'Kindly call /*Company*/ at /*ContactNumber_Company*/ to reschedule this appointment. '
        + 'Thank You'
      Declare @DateString Varchar(MAX) = FORMAT(@Date, 'M/dd/yyyy ddd')

      if( LEN(ISNULL(@SMSFormat, '')) > 0 )
        BEGIN
            SET @message = @SMSFormat
        END

      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(ISNULL(@Client, ''))))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(ISNULL(@Pet, ''))))
      SET @message = REPLACE(@message, '/*Company*/', LTRIM(RTRIM(ISNULL(@Name_Company, ''))))
      SET @message = REPLACE(@message, '/*Date*/', LTRIM(RTRIM(ISNULL(@DateString, ''))))
      SET @message = REPLACE(@message, '/*ContactNumber_Company*/', LTRIM(RTRIM(ISNULL(@ContactNumber_Company, ''))))
      SET @message = REPLACE(@message, '"', '``')

      RETURN @message
  END

GO

go

CREATE OR
ALTER FUNCTION [dbo].[fGetSendSoapPlan](@Date             DATETIME,
                                        @IsSMSSent        BIT = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETIME,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETIME,
  DateCreated          DATETIME,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX),
  Count                INT)
AS
  BEGIN
      DECLARE @IDs_Company TYPINTLIST
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_ID_Model VARCHAR(MAX) =''
      DECLARE @_table TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )

      SELECT @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      SELECT @PatientWaitingList_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatientWaitingList'

      SELECT @Patient_Vaccination_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      SELECT @Patient_SOAP_Plan_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      SELECT @Patient_Wellness_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      IF( len(Trim(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')

            DELETE FROM @IDs_Company
            WHERE  ID IN (SELECT ID_Company
                          FROM   tCompany_SMSSetting
                          WHERE  IsActive = 0)
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting cSMSSetting
                   INNER JOIN tCompany c
                           on cSMSSetting.ID_Company = c.ID
            WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
                   and c.IsActive = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent TABLE
        (
           IsSMSSent BIT
        )

      IF @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = IsNull(@Date, GetDate())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*SOAP PLAN */
      INSERT @_table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             IsNull(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, soapPlan.Name_Item, IsNull(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DateAdd(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = ISNULL(patientSOAP.ID_Client, patient.ID_Client)
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     ON patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND IsNull(patientSOAP.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Patient Wellness Schedule*/
      INSERT @_table
      SELECT DISTINCT c.ID                                                                                                                                                                                                                        ID_Company,
                      c.NAME                                                                                                                                                                                                                      Name_Company,
                      client.NAME                                                                                                                                                                                                                 Name_Client,
                      dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                          ContactNumber_Client,
                      wellDetSched.Date,
                      IsNull(wellDetSched.Comment, ''),
                      IsNull(wellDetSched.Comment, ''),
                      dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, IsNull(client.NAME, ''), IsNull(c.ContactNumber, ''), IsNull(patient.NAME, ''), IsNull(wellDetSched.Comment, ''), IsNull(wellDetSched.Comment, ''), wellDetSched.Date) Message,
                      CONVERT(DATE, DateAdd(DAY, -1, wellDetSched.Date))                                                                                                                                                                          DateSending,
                      wellHed.DateCreated,
                      wellDetSched.ID                                                                                                                                                                                                             ID_Patient_Wellness_Schedule,
                      @Patient_Wellness_Schedule_ID_Model,
                      wellHed.Code
      FROM   dbo.vPatient_Wellness_Schedule wellDetSched
             inner JOIN vPatient_Wellness wellHed
                     ON wellHed.ID = wellDetSched.ID_Patient_Wellness
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellHed.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellHed.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellHed.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON c.ID = idsCompany.ID
      WHERE  wellHed.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND IsNull(wellHed.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, wellDetSched.Date)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Vaccination*/
      DECLARE @IDs_Vaccination_ExistingPatientWellness TYPINTLIST

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @_table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             IsNull(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Name_Item, IsNull(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN tPatient_Vaccination_Schedule vacSchedule
                     ON vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
             AND vac.ID NOT IN (SELECT ID
                                FROM   @IDs_Vaccination_ExistingPatientWellness)

      /*Reschedule WaitingList*/
      INSERT @_table
             (ID_Company,
              Name_Company,
              Name_Client,
              ContactNumber_Client,
              DateReturn,
              Name_Item,
              Comment,
              Message,
              DateSending,
              DateCreated,
              ID_Reference,
              Oid_Model,
              Code)
      SELECT hed.ID_Company,
             company.Name,
             client.Name,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),
             hed.DateCreated,
             '',
             '',
             dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, company.Name, company.ContactNumber, hed.DateCreated, client.Name, patient.Name),
             hed.DateCreated,
             hed.DateCreated,
             hed.ID,
             @PatientWaitingList_ID_Model,
             hed.Code
      FROM   tPatientWaitingList hed
             INNER JOIN tCompany_SMSSetting smsSetting
                     on smsSetting.ID_Company = hed.ID_Company
             INNER JOIN tCompany company
                     on company.ID = hed.ID_Company
             inner join tClient client
                     on hed.ID_Client = client.ID
             inner join tPatient patient
                     on hed.ID_Patient = patient.ID
             INNER JOIN @IDs_Company idsCompany
                     ON company.ID = idsCompany.ID
      WHERE  --CONVERT(Date, DateAdd(Day, smsSetting.DayInterval, hed.DateCreated)) = CONVERT(Date, @Date)
        (( CONVERT(DATE, hed.DateCreated) = CONVERT(DATE, @Date) ))
        AND hed.WaitingStatus_ID_FilingStatus IN ( 21 )
        AND IsNull(hed.IsSentSMS, 0) IN (SELECT IsSMSSent
                                         FROM   @SMSSent)
        AND IsNull(hed.ID_CLient, 0) > 0
        AND IsNull(patient.IsDeceased, 0) = 0

      --/*BillingInvoice SMSPayableRemider*/  
      --INSERT @_table  
      --       (ID_Company,  
      --        Name_Company,  
      --        Name_Client,  
      --        ContactNumber_Client,  
      --        DateReturn,  
      --        Name_Item,  
      --        Comment,  
      --        Message,  
      --        DateSending,  
      --        DateCreated,  
      --        ID_Reference,  
      --        Oid_Model,  
      --        Code)  
      --SELECT biHed.ID_Company,  
      --       company.Name,  
      --       client.Name,  
      --       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),  
      --       bSMSReminder.DateSchedule,  
      --       '',  
      --       '',  
      --       dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, biHed.Code, biHed.Date, client.Name, ISNULL(bSMSReminder.TotalAmount_BillingInvoice, 0) - ISNULL(bSMSReminder.RemainingAmount_BillingInvoice, 0)),  
      --       biHed.Date,  
      --       bSMSReminder.DateCreated,  
      --       bSMSReminder.ID,  
      --       @BillingInvoice_SMSPayableRemider_ID_Model,  
      --       bihed.Code  
      --FROM   vBillingInvoice biHed  
      --       INNER JOIN tBillingInvoice_SMSPayableRemider bSMSReminder  
      --               on biHed.ID = bSMSReminder.ID_BillingInvoice  
      --       INNER JOIN tCompany_SMSSetting smsSetting  
      --               on smsSetting.ID_Company = bSMSReminder.ID_Company  
      --       INNER JOIN tCompany company  
      --               on company.ID = bSMSReminder.ID_Company  
      --       inner join tClient client  
      --               on bSMSReminder.ID_Client = client.ID  
      --       INNER JOIN @IDs_Company idsCompany  
      --               ON company.ID = idsCompany.ID  
      --WHERE  CONVERT(Date, DateAdd(Day, smsSetting.DayInterval, bSMSReminder.dateSchedule)) = CONVERT(Date, @Date)  
      DELETE @_table
      FROM   @_table forSMSSending
             inner join tInactiveSMSSending inactiveSendingSchedule
                     on forSMSSending.ID_Company = inactiveSendingSchedule.ID_Company
                        and CONVERT(Date, forSMSSending.DateSending) = CONVERT(Date, inactiveSendingSchedule.Date)
      WHERE  inactiveSendingSchedule.IsActive = 1

      INSERT @table
      SELECT ID_Company,
             Name_Company,
             Name_Client,
             ContactNumber_Client,
             DateReturn,
             Name_Item,
             Comment,
             Message,
             MAX(DateSending),
             MAX(DateCreated),
             MAX(ID_Reference),
             Oid_Model,
             MAX(Code),
             Count(*)
      FROM   @_table
      GROUP  BY ID_Company,
                Name_Company,
                Name_Client,
                ContactNumber_Client,
                DateReturn,
                Name_Item,
                Comment,
                Message,
                Oid_Model

      Update @table
      set    Comment = REPLACE(Comment, '"', '``')

      Update @table
      set    Name_Item = REPLACE(Name_Item, '"', '``')

      RETURN
  END

GO

GO

CREATE OR
ALTER VIEW vSMSList_PatientWaitingList_ReSchedule
AS
  SELECT DISTINCT wellness.ID                                                                                                                              ID,
                  wellness.ID                                                                                                                              ID_PatientWaitingList,
                  wellness.Code,
                  DateSent,
                  Name_Client,
                  Name_Patient,
                  c.ContactNumber,
                  dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, comp.Name, comp.ContactNumber, wellness.DateCreated, c.Name, wellness.Name_Patient) Message,
                  wellness.DateCreated                                                                                                                     DateSending,
                  wellness.ID_Company,
                  IsNull(wellness.IsSentSMS, 0)                                                                                                            IsSentSMS,
                  model.Oid                                                                                                                                Oid_Model
  FROM   vPatientWaitingList wellness
         INNER JOIN tClient c
                 ON c.ID = wellness.ID_Client
         INNER JOIN tCOmpany comp
                 ON comp.ID = wellness.ID_Company
         INNER JOIN tCompany_SMSSetting smsSetting
                 on smsSetting.ID_Company = comp.ID,
         _tModel model
  WHERE  model.tableName = 'tPatientWaitingList'
         AND wellness.WaitingStatus_ID_FilingStatus IN ( 21 )

GO

CREATE   OR
ALTER VIEW [dbo].[vSMSList]
as
  SELECT ID,
         ID              ID_Reference,
         ID_Patient_SOAP Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_SOAP_Plan
  Union All
  SELECT ID,
         ID                     ID_Reference,
         ID_Patient_Vaccination Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Vaccination_Schedule
  UNION ALL
  SELECT ID,
         ID                  ID_Reference,
         ID_Patient_Wellness Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Wellness_DetailSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientWaitingList Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientWaitingList_ReSchedule

GO

CREATE   OR
ALTER VIEW vPatientWaitingMaxMissAppointment
AS
  SELECT MAX(ID) ID_PatientWaitingList
  FROm   vPatientWaitingList
  where  WaitingStatus_ID_FilingStatus IN ( 4, 21 )
  GROUP  BY ID_Client,
            Name_Client,
            ID_Patient,
            Name_Patient

GO

CREATE      OR
ALTER VIEW [dbo].[vPatientWaitingCanceledList_ListView]
AS
  SELECT DISTINCT ISNULL(appointment.UniqueID, CONVERT(VARCHAR(MAX), MAX(hed.ID))) UniqueID,
                  hed.ID_Company,
                  ISNULL(appointment.DateStart, hed.DateCreated)                   DateStart,
                  ISNULL(appointment.DateEnd, hed.DateCreated)                     DateEnd,
                  hed.ID_Client,
                  hed.Name_Client,
                  hed.ID_Patient,
                  hed.Name_Patient,
                  ISNULL(appointment.ReferenceCode, CASE
                                                      WHEN hed.IsQueued = 1 THEN 'Queued'
                                                      ELSE ''
                                                    END)                           ReferenceCode,
                  hed.WaitingStatus_ID_FilingStatus,
                  hed.WaitingStatus_Name_FilingStatus,
                  appointment.Description,
                  hed.IsQueued,
                  hed.Oid_Model_Reference,
                  hed.ID_Reference
  FROM   vPatientWaitingList hed
         INNER JOIN vPatientWaitingMaxMissAppointment maxcanceledWaiting
                 ON hed.ID = maxcanceledWaiting.ID_PatientWaitingList
         LEFT JOIN vAppointmentEvent appointment
                on hed.ID_Client = appointment.ID_Client
                   and hed.ID_Patient = appointment.ID_Patient
                   AND hed.Oid_Model_Reference = appointment.Oid_Model
                   and hed.ID_Reference = appointment.Appointment_ID_CurrentObject
         LEFT JOIN tPatient patient
                on patient.ID = appointment.ID_Patient
  where  hed.WaitingStatus_ID_FilingStatus IN ( 4, 21 )
  GROUP  BY appointment.UniqueID,
            hed.IsQueued,
            hed.ID_Company,
            hed.DateCreated,
            appointment.DateStart,
            appointment.DateEnd,
            hed.ID_Client,
            hed.Name_Client,
            hed.ID_Patient,
            hed.Name_Patient,
            appointment.ReferenceCode,
            hed.WaitingStatus_ID_FilingStatus,
            hed.WaitingStatus_Name_FilingStatus,
            appointment.Description,
            hed.Oid_Model_Reference,
            hed.ID_Reference

GO

create   OR
alter view vAppointment_FilingStatus
AS
  Select ID,
         Name
  FROM   tFilingStatus
  WHERE  ID IN ( 2, 8, 13, 4, 21 )

GO

ALTER VIEW vPayable_Detail
AS
  SELECT H.*,
         UC.Name         AS CreatedBy,
         UM.Name         AS LastModifiedBy,
         expenseCat.Name Name_ExpenseCategory
  FROM   tPayable_Detail H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIn tExpenseCategory expenseCat
                on h.ID_ExpenseCategory = expenseCat.ID

GO

CREATE OR
ALTER VIEW vPatientWaitingList
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         client.Name  Name_Client,
         patient.Name Name_Patient,
         waitfs.Name  WaitingStatus_Name_FilingStatus,
         billfs.Name  BillingInvoice_Name_FilingStatus,
         _model.Name  Name_Model_Reference
  FROM   tPatientWaitingList H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON H.ID_Client = client.ID
         LEFT JOIN tPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN tFilingStatus waitfs
                ON H.WaitingStatus_ID_FilingStatus = waitfs.ID
         LEFT JOIN tFilingStatus billfs
                ON H.BillingInvoice_ID_FilingStatus = billfs.ID
         LEFT JOIN _tModel _model
                on H.Oid_Model_Reference = _model.Oid

GO

CREATE   OR
ALTER VIEW vPatientWaitingList_ListView_temp
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         --IsnULL(appointment.UniqueIDList, '') UniqueIDList,    
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.Name_Model_Reference,
         waitingList.ID_Reference
  FROM   vPatientWaitingList waitingList
  -- OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment    
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4, 21 )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            --appointment.UniqueIDList,    
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.Name_Model_Reference,
            waitingList.ID_Reference

GO

CREATE        OR
ALTER View vForBilling_ListView_temp
as
  Select confinement.ID,
         confinement.Code RefNo,
         confinement.ID   ID_CurrentObject,
         m.Oid            Oid_Model,
         m.Name           Name_Model,
         confinement.Date,
         confinement.ID_Company,
         confinement.ID_Client,
         confinement.ID_Patient,
         confinement.BillingInvoice_ID_FilingStatus,
         confinement.Name_Client,
         confinement.Name_Patient,
         confinement.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Confinement confinement,
         _tModel m
  where  m.TableName = 'tPatient_Confinement'
         AND ID_FilingStatus NOT IN ( 4 )
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  Select soap.ID,
         soap.Code RefNo,
         soap.ID   ID_CurrentObject,
         m.Oid     Oid_Model,
         m.Name    Name_Model,
         soap. Date,
         soap.ID_Company,
         soap.ID_Client,
         soap.ID_Patient,
         soap.BillingInvoice_ID_FilingStatus,
         soap.Name_Client,
         soap.Name_Patient,
         soap.BillingInvoice_Name_FilingStatus
  FROM   vPatient_SOAP soap,
         _tModel m
  where  m.TableName = 'tPatient_SOAP'
         AND ID_FilingStatus NOT IN ( 4 )
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
         and ISNULL(soap.ID_Patient_Confinement, '') = 0

GO

ALTER PROCEDURE [dbo].[pGetPayable] @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' as Payable_Detail

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           GETDATE() AS Date,
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           2         AS Payment_ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPayable H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPayable_Detail
      WHERE  ID_Payable = @ID
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @ID_Patient_Vaccination         INT = NULL,
                                      @ID_Patient_Wellness            INT = NULL,
                                      @ID_Patient_Lodging             INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL,
                                      @IsWalkIn                       BIT = 0
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @InitialConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        SET @ID_Patient = NULL

      IF ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Wellness, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_Wellness
            WHERE  ID = @ID_Patient_Wellness
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = ISNULL(CurrentCreditAmount, 0)
            FROM   tPatient_Confinement confinement
                   INNER JOIN tClient client
                           ON confinement.ID_Client = client.ID
            WHERE  confinement.ID = @ID_Patient_Confinement

            SET @InitialConfinementDepositAmount = @ConfinementDepositAmount
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                          Name_FilingStatus,
                   client.Name                      Name_Client,
                   patient.Name                     Name_Patient,
                   client.Address                   BillingAddress,
                   attendingPhysicianEmloyee.Name   AttendingPhysician_Name_Employee,
                   @InitialConfinementDepositAmount InitialConfinementDepositAmount,
                   @ConfinementDepositAmount        ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GetDate()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement,
                           @ID_Patient_Vaccination         ID_Patient_Vaccination,
                           @ID_Patient_Wellness            ID_Patient_Wellness,
                           @ID_Patient_Lodging             ID_Patient_Lodging,
                           CASE
                             WHEN @IsWalkIn = 1 THEN 'Walk In Customer'
                             ELSE ''
                           END                             WalkInCustomerName,
                           @IsWalkIn                       IsWalkIn) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      IF ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapTreatment.ID DESC) ) - 799999                    ID,
                   soapTreatment.ID_Item,
                   soapTreatment.Name_Item,
                   ISNULL(soapTreatment.Quantity, 0)                                   Quantity,
                   ISNULL(soapTreatment.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapTreatment.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapTreatment.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Treatment soapTreatment
                   INNER JOIN tItem item
                           ON item.ID = soapTreatment.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
            UNION ALL
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999                    ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                                   Quantity,
                   ISNULL(soapPrescription.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapPrescription.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapPrescription.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
                   AND soapPrescription.IsActive = 1
                   AND soapPrescription.IsCharged = 1
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(itemsservices.UnitPrice, 0)               UnitPrice,
                   ISNULL(itemsservices.UnitCost, 0)                UnitCost,
                   itemsservices.DateExpiration                     DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
        END
      ELSE IF ISNULL(@ID_Patient_Wellness, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY wellnessdetail.ID DESC) ) - 999999 ID,
                   wellnessdetail.ID_Item,
                   wellnessdetail.Name_Item,
                   ISNULL(wellnessdetail.Quantity, 1)                Quantity,
                   ISNULL(wellnessdetail.UnitPrice, 0)               UnitPrice,
                   ISNULL(wellnessdetail.UnitCost, 0)                UnitCost,
                   wellnessdetail.DateExpiration                     DateExpiration,
                   wellnessdetail.ID                                 ID_Patient_Wellness_Detail
            FROM   dbo.vPatient_Wellness_Detail wellnessdetail
                   INNER JOIN tItem item
                           ON item.ID = wellnessdetail.ID_Item
            WHERE  ID_Patient_Wellness = @ID_Patient_Wellness
        END
      ELSE IF ISNULL(@ID_Patient_Vaccination, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY vaccination.ID DESC) ) - 999999 ID,
                   vaccination.ID_Item,
                   vaccination.Name_Item,
                   1                                              Quantity,
                   ISNULL(vaccination.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(vaccination.UnitCost, item.UnitCost)    UnitCost,
                   vaccination.DateExpiration                     DateExpiration,
                   vaccination.ID                                 ID_Patient_Vaccination
            FROM   dbo.vPatient_Vaccination vaccination
                   INNER JOIN tItem item
                           ON item.ID = vaccination.ID_Item
            WHERE  vaccination.ID = @ID_Patient_Vaccination
        END
      ELSE
        BEGIN
            SELECT *,
                   conf.Code               Code_Patient_Confinement,
                   conf.Date               Date_Patient_Confinement,
                   soap_.Code              Code_Patient_SOAP,
                   soap_.Name_FilingStatus Name_FilingStatus_Patient_SOAP,
                   confItemServices.ID_Patient_SOAP_Prescription,
                   confItemServices.ID_Patient_SOAP_Treatment,
                   ''                      Note
            FROM   dbo.vBillingInvoice_Detail biDetail
                   INNER join tBillingInvoice bi
                           on biDetail.ID_BillingInvoice = bi.ID
                   LEFT join vPatient_Confinement_ItemsServices confItemServices
                          on biDetail.ID_Patient_Confinement_ItemsServices = confItemServices.ID
                   LEFT JOIN vPatient_SOAP soap_
                          on soap_.ID = confItemServices.ID_Patient_SOAP
                   LEFT join vPatient_Confinement conf
                          on bi.ID_Patient_Confinement = conf.ID
            WHERE  ID_BillingInvoice = @ID
            Order  by ISNULL(ID_Patient_Confinement_ItemsServices, 100000000) ASC,
                      biDetail.ID ASC
        END

      IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID_Patient,
                   Name_Patient,
                   ID                                 ID_Patient_Confinement_Patient
            FROM   vPatient_Confinement_Patient
            WHERE  ID_Patient_Confinement IN ( @ID_Patient_Confinement )
            ORDER  BY ID DESC
        END
      ELSE IF ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID
        END
  END;

GO

CREATE OR
ALTER PROC dbo.pModel_AfterSaved_PayablePayment (@ID_CurrentObject VARCHAR(10),
                                                 @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_FilingStatus INT = 0
      DECLARE @ID_CreatedBy INT = 0
      DECLARE @IDs_PayablePayment typIntList

      INSERT @IDs_PayablePayment
      VALUES (@ID_CurrentObject)

      SELECT @ID_FilingStatus = ID_FilingStatus,
             @ID_CreatedBy = ID_CreatedBy
      FROM   tPayablePayment
      where  iD = @ID_CurrentObject

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT = 0;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPayablePayment
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'PayablePayment';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPayablePayment
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      IF( @ID_FilingStatus = 20 )
        BEGIN
            DECLARE @ID_UserSession INT = 0

            SELECT @ID_UserSession = ID
            FROM   tUserSession
            WHERE  ID_User = @ID_CreatedBy

            exec pApprovePayablePayment
              @IDs_PayablePayment,
              @ID_UserSession
        END
  END;

GO

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PayablePayment'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_PayablePayment]
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROCEDURE [dbo].[pGetPayablePayment] @ID              INT = -1,
                                           @ID_Payable      INT = -1,
                                           @ID_FilingStatus INT = NULL,
                                           @ID_Session      INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF( @ID_FilingStatus IS NULL )
        SET @ID_FilingStatus = 1

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   payable.RemaningAmount,
                   payableDet.Name Name_Payable_Detail
            FROM   (SELECT NULL             AS [_],
                           -1               AS [ID],
                           NULL             AS [Code],
                           NULL             AS [Name],
                           GetDate()        AS Date,
                           1                AS [IsActive],
                           NULL             AS [ID_Company],
                           NULL             AS [Comment],
                           NULL             AS [DateCreated],
                           NULL             AS [DateModified],
                           NULL             AS [ID_CreatedBy],
                           NULL             AS [ID_LastModifiedBy],
                           @ID_FilingStatus AS ID_FilingStatus,
                           @ID_Payable      ID_Payable) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tPayable payable
                          ON payable.ID = H.ID_Payable
                   LEFT JOIN tPayable_Detail payableDet
                          ON payable.ID = payableDet.ID_Payable
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPayablePayment H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE   OR
ALTER PROC pInsertFilingStatus(@FilingStatus VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tFilingStatus
         WHERE  Name = @FilingStatus) = 0
        BEGIN
            INSERT INTO [dbo].tFilingStatus
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@FilingStatus,
                         1,
                         1)
        END
  END

GO

CREATE OR
ALTER PROC pGetForBillingBillingInvoiceRecords(@IDs_Patient_SOAP        typIntList READONLY,
                                               @IDs_Patient_Confinement typIntList READONLY)
as
    DECLARE @Patient_SOAP_ID_Model VARCHAR(MAX) = ''
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''

    SELECT @Patient_SOAP_ID_Model = Oid
    FROm   _tModel
    where  TableName = 'tPatient_SOAP'

    SELECT @Patient_Confinement_ID_Model = Oid
    FROm   _tModel
    where  TableName = 'tPatient_Confinement'

    DECLARE @record TABLE
      (
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,
         [RefNo]            [varchar](50) NULL,
         [ID_CurrentObject] [int] NOT NULL,
         [Oid_Model]        [uniqueidentifier] NOT NULL,
         [1]                VARCHAR(MAX) NULL,
         [2]                VARCHAR(MAX) NULL,
         [3]                VARCHAR(MAX) NULL,
         [4]                VARCHAR(MAX) NULL,
         [5]                VARCHAR(MAX) NULL,
         [6]                VARCHAR(MAX) NULL,
         [7]                VARCHAR(MAX) NULL,
         [8]                VARCHAR(MAX) NULL,
         [9]                VARCHAR(MAX) NULL,
         [10]               VARCHAR(MAX) NULL
      )

    /*Patient SOAP*/
    INSERT @record
           ([Oid_Model],
            [ID_CurrentObject],
            [RefNo],
            [1],
            [2],
            [3],
            [4],
            [5],
            [6],
            [7],
            [8],
            [9],
            [10])
    SELECT @Patient_SOAP_ID_Model,
           *
    FROM   (SELECT ID_Patient_SOAP,
                   Code_Patient_SOAP,
                   BillingInvoice_RowID,
                   Info_BillingInvoice
            FROM   (SELECT bi.ID_Patient_SOAP,
                           soap.Code                                            Code_Patient_SOAP,
                           ROW_NUMBER()
                             OVER(
                               PARTITION BY bi.ID_Patient_SOAP
                               ORDER BY ID_Patient_SOAP ASC, bi.ID)             BillingInvoice_RowID,
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                    FROM   vBillingINvoice bi
                           inner JOIN tPatient_SOAP soap
                                   ON bi.ID_Patient_SOAP = soap.ID
                           INNER JOIN @IDs_Patient_SOAP ids
                                   on soap.ID = ids.ID
                    where  ISNULL(bi.ID_Patient_SOAP, 0) <> 0
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
           PIVOT(MAX([Info_BillingInvoice])
                FOR [BillingInvoice_RowID] IN([1],
                                              [2],
                                              [3],
                                              [4],
                                              [5],
                                              [6],
                                              [7],
                                              [8],
                                              [9],
                                              [10] )) AS PivotTable;

    /*Patient Confinement*/
    INSERT @record
           ([Oid_Model],
            [ID_CurrentObject],
            [RefNo],
            [1],
            [2],
            [3],
            [4],
            [5],
            [6],
            [7],
            [8],
            [9],
            [10])
    SELECT @Patient_Confinement_ID_Model,
           *
    FROM   (SELECT ID_Patient_Confinement,
                   Code_Patient_Confinement,
                   BillingInvoice_RowID,
                   Info_BillingInvoice
            FROM   (SELECT bi.ID_Patient_Confinement,
                           soap.Code                                            Code_Patient_Confinement,
                           ROW_NUMBER()
                             OVER(
                               PARTITION BY bi.ID_Patient_Confinement
                               ORDER BY ID_Patient_Confinement ASC, bi.ID)      BillingInvoice_RowID,
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                    FROM   vBillingINvoice bi
                           inner JOIN tPatient_Confinement soap
                                   ON bi.ID_Patient_Confinement = soap.ID
                           INNER JOIN @IDs_Patient_Confinement ids
                                   on soap.ID = ids.ID
                    where  ISNULL(bi.ID_Patient_Confinement, 0) <> 0
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
           PIVOT(MAX([Info_BillingInvoice])
                FOR [BillingInvoice_RowID] IN([1],
                                              [2],
                                              [3],
                                              [4],
                                              [5],
                                              [6],
                                              [7],
                                              [8],
                                              [9],
                                              [10] )) AS PivotTable;

    SELECT '_',
           '' BillingInvoices

    SELECT GETDate() Date

    SELECT *
    FROM   @record

GO

CREATE OR
ALTER PROC pAddAppointmentToWaitingList(@Oid_Model                    VARCHAR(MAX),
                                        @Appointment_ID_CurrentObject INT,
                                        @ID_Patient                   INT,
                                        @ID_UserSession               INT)
AS
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    DECLARE @DateCreatedMax Date

    SELECT @DateCreatedMax = CONVERT(DATE, MAX(DateCreated))
    FROm   tPatientWaitingList

    if( @DateCreatedMax < CONVERT(DATE, GETDATE()) )
      begin
          Update tDocumentSeries
          set    Counter = 1
          FROM   tDocumentSeries docSeries
                 INNER JOIN _tModel model
                         on docSeries.ID_Model = model.Oid
          where  TableName = 'tPatientWaitingList'
      END

    INSERT INTO [dbo].[tPatientWaitingList]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_Client],
                 [ID_Patient],
                 [WaitingStatus_ID_FilingStatus],
                 Oid_Model_Reference,
                 ID_Reference)
    SELECT H.Name,
           H.IsActive,
           patient.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           patient.ID_Client,
           patient.ID ID_Patient,
           @Waiting_ID_FilingStatus,
           @Oid_Model,
           @Appointment_ID_CurrentObject
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDate() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatient patient
    where  ID = @ID_Patient

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @Waiting_ID_FilingStatus
    FROM   tPatient patient
    where  ID = @ID_Patient

GO

CREATE OR
ALTER PROC _pUpdatePatientToWaitingListStatus(@IDs_PatientWaitingList        typIntList READONLY,
                                              @WaitingStatus_ID_FilingStatus INT,
                                              @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    /* Update Waiting List Status */
    Update tPatientWaitingList
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus,
           DateSent = NULL,
           IsSentSMS = NULL
    FROM   tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus
    FROM   tPatient patient
           inner join tPatientWaitingList waitingList
                   ON patient.ID = waitingList.ID_Patient
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /* Add Logs */
    INSERT INTO [dbo].[tPatientWaitingList_Logs]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_PatientWaitingList],
                 [WaitingStatus_ID_FilingStatus])
    SELECT H.Name,
           H.IsActive,
           waitingList.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           waitingList.ID,
           @WaitingStatus_ID_FilingStatus
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDATE() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

GO

CREATE OR
ALTER PROC pUpdatePatientToWaitingListStatus(@IDs_PatientWaitingList        typIntList READONLY,
                                             @WaitingStatus_ID_FilingStatus INT,
                                             @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    exec _pUpdatePatientToWaitingListStatus
      @IDs_PatientWaitingList,
      @WaitingStatus_ID_FilingStatus,
      @ID_UserSession

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

CREATE OR
ALTER PROC pUpdateAppointmentStatus(@Oid_Model                    VARCHAR(MAX),
                                    @Appointment_ID_CurrentObject INT,
                                    @Appointment_ID_FilingStatus  INT,
                                    @ID_UserSession               INT,
                                    @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @TableName VARCHAR(MAX) = ''
      DECLARE @ID_Company INT = 0
      DECLARE @ID_User INT = 0
      DECLARE @Reschedule_ID_FilingStatus INT = 21
      DECLARE @ID_Patient INT

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @TableName = TableName
      FROM   _tModel
      WHERE  Oid = @Oid_Model

      IF( @TableName = 'tPatientAppointment' )
        BEGIN
            UPDATE tPatientAppointment
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatientAppointment
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_SOAP' )
        BEGIN
            UPDATE tPatient_SOAP_Plan
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_SOAP_Plan _plan
                   INNER JOIN tPatient_SOAP _soap
                           on _plan.ID_Patient_SOAP = _soap.ID
            WHERE  _plan.ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_Wellness' )
        BEGIN
            UPDATE tPatient_Wellness_Schedule
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_Wellness_Schedule _schedule
                   INNER JOIN tPatient_Wellness _wellness
                           on _schedule.ID_Patient_Wellness = _wellness.ID
            WHERE  _schedule.ID = @Appointment_ID_CurrentObject
        END

      ---------------------------------------------------------------------------------
      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      SELECT MAX(_waiting.ID)
      FROM   tPatientWaitingList _waiting
      WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
             AND ID_Reference = @Appointment_ID_CurrentObject
             AND ID_Patient = @ID_Patient

      IF(SELECT COUNT(*)
         FROM   @IDs_PatientWaitingList) = 0
        BEGIN
            DELETE FROM @IDs_PatientWaitingList

            exec pAddAppointmentToWaitingList
              @Oid_Model,
              @Appointment_ID_CurrentObject,
              @ID_Patient,
              @ID_UserSession

            INSERT @IDs_PatientWaitingList
            SELECT MAX(_waiting.ID)
            FROM   tPatientWaitingList _waiting
            WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
                   AND ID_Reference = @Appointment_ID_CurrentObject
                   AND ID_Patient = @ID_Patient
        END

      exec _pUpdatePatientToWaitingListStatus
        @IDs_PatientWaitingList,
        @Appointment_ID_FilingStatus,
        @ID_UserSession

      ---------------------------------------------------------------------------------
      /*Logs*/
      INSERT INTO [dbo].[tAppointmentStatusLog]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Appointment_ID_CurrentObject],
                   [Appointment_ID_FilingStatus])
      VALUES      ( 1,
                    @ID_Company,
                    @Remarks,
                    GetDate(),
                    GetDate(),
                    @ID_User,
                    @ID_User,
                    @Oid_Model,
                    @Appointment_ID_CurrentObject,
                    @Appointment_ID_FilingStatus)
  END

GO

ALTER PROC pGeneratePatientWaitingListFromTodayAppointmentEvent
AS
    DECLARE @AppointmentIDs_Patient TABLE
      (
         Oid_Model                    VARCHAR(MAX),
         Appointment_ID_CurrentObject INT,
         ID_Patient                   INT
      )
    DECLARE @WaitingList_Patient typIntList
    DECLARE @OudatedWaiting_PatientWaitingList typIntList
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @Cancelled_ID_FilingStatus INT = 4
    DECLARE @ID_UserSession INT

    SELECT @ID_UserSession = MAX(ID)
    FROM   tUserSession
    WHERE  ID_USer = 10

    INSERT @OudatedWaiting_PatientWaitingList
    select DIstinct ID
    FROm   vPatientWaitingList
    where  CONVERT(Date, DateCreated) < CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0
           AND WaitingStatus_ID_FilingStatus IN ( @Waiting_ID_FilingStatus, @Ongoing_ID_FilingStatus )

    INSERT @AppointmentIDs_Patient
    select Oid_Model,
           Appointment_ID_CurrentObject,
           ID_Patient
    FROm   vAppointmentEvent
    where  CONVERT(Date, DateStart) = CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0

    INSERT @WaitingList_Patient
    select DIstinct ID_Patient
    FROm   vPatientWaitingList
    where  CONVERT(Date, DateCreated) = CONVERT(Date, GETDATE())
           and ISNULL(ID_Patient, 0) <> 0

    Delete FROM @AppointmentIDs_Patient
    WHERE  ID_Patient IN (SELECT ID
                          FROM   @WaitingList_Patient)

    Update tPatient
    set    WaitingStatus_ID_FilingStatus = NULL
    FROM   tPatient patient
           inner join @OudatedWaiting_PatientWaitingList outdate
                   on patient.ID = outdate.ID

    -----------------------------------------------------------------------------
    DECLARE @Oid_Model VARCHAR(max)
    DECLARE @Appointment_ID_CurrentObject INT
    DECLARE @ID_Patient INT
    DECLARE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent CURSOR FOR
      select Oid_Model,
             Appointment_ID_CurrentObject,
             ID_Patient
      FROm   @AppointmentIDs_Patient

    OPEN db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    FETCH NEXT FROM db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent INTO @Oid_Model, @Appointment_ID_CurrentObject, @ID_Patient

    WHILE @@FETCH_STATUS = 0
      BEGIN
          exec pAddAppointmentToWaitingList
            @Oid_Model,
            @Appointment_ID_CurrentObject,
            @ID_Patient,
            @ID_UserSession

          FETCH NEXT FROM db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent INTO @Oid_Model, @Appointment_ID_CurrentObject, @ID_Patient
      END

    CLOSE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    DEALLOCATE db_cursorpGeneratePatientWaitingListFromTodayAppointmentEvent

    exec pAutoGeneratePatientWaitingList

    -----------------------------------------------------------------------------
    exec pUpdatePatientToWaitingListStatus
      @OudatedWaiting_PatientWaitingList,
      @Cancelled_ID_FilingStatus,
      @ID_UserSession

GO

GO

CREATE   OR
ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT,
                                        @DateSent       DateTime)
AS
  BEGIN
      /*                
       iTextMo Status                
                     
       "1" = Invalid Number.                
       "2" = Number prefix not supported. Please contact us so we can add.                
       "3" = Invalid ApiCode.                
       "4" = Maximum Message per day reached. This will be reset every 12MN.                
       "5" = Maximum allowed characters for message reached.                
       "6" = System OFFLINE.                
       "7" = Expired ApiCode.                
       "8" = iTexMo Error. Please try again later.                
       "9" = Invalid Function Parameters.                
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.                
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.                
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.                
       "13" = Invalid or Not Registered Custom Sender ID.                
       "14" = Invalid preferred server number.                
       "15" = IP Filtering enabled - Invalid IP.                
       "16" = Authentication error. Contact support at support@itexmo.com                
       "17" = Telco Error. Contact Support support@itexmo.com                
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com                
       "19" = Account suspended. Contact Support support@itexmo.com                
       "0" = Success! Message is now on queue and will be sent soon               
      "-1" = Reach VetCloud SMS Count Limit               
      "20" = Manual Sent SMS            
                
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1

      IF @DateSent IS NULL
        SET @DateSent = GETDATE()

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      select @PatientWaitingList_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatientWaitingList'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientWaitingList_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientWaitingList
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientWaitingList_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientWaitingList,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO

exec pInsertFilingStatus
  'For Approve'

exec pInsertFilingStatus
  'Reschedule'

Update _tNavigation
set    Route = 'PayableList'
where  Name = 'Payable_ListView'

Update _tNavigation
set    Route = 'ReportReceivedDepositCredit'
where  Name = 'ReceivedDepositCredits_Navigation'

Update _tNavigation
set    Caption = 'SMS',
       Route = 'SMSList'
where  Name = 'SMSList_ListView'
--DELETE FROM tPatientWaitingList WHERE isnull(IsQueued, 0) = 0
--EXEC pGeneratePatientWaitingListFromTodayAppointmentEvent
GO

exec _pAddModelProperty
  'tPatient',
  'LastAttendingPhysician_ID_Employee',
  2

exec _pAddModelProperty
  'tClient',
  'LastAttendingPhysician_ID_Employee',
  2

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER FUNCTION fGetNumbersByRange(@from INT,
                                  @to   INT)
RETURNS @CountTable TABLE (
  Number INT)
AS
  BEGIN
      with cte_n
           as (select @from as [CountNumber]
               union all
               select CountNumber + 1
               from   cte_n
               where  CountNumber < @to)
      Insert @CountTable
      select CountNumber
      from   cte_n
      option (maxrecursion 0)

      return
  END

GO

ALTER VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name                 AS CreatedBy,
         UM.Name                 AS LastModifiedBy,
         H.DateLastVisited,
         H.CurrentCreditAmount,
         acct.Username,
         acct.Password,
         h.ProfileImageFile,
         attendingPersonell.Name LastAttendingPhysician_Name_Employee
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tUser acct
                ON H.ID_User = acct.ID
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO

ALTER VIEW vClient_ListView
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '') ContactNumbers,
         DateCreated,
         DateLastVisited,
         ISNULL(IsActive, 0)          IsActive,
         LastAttendingPhysician_Name_Employee
  FROM   dbo.vClient

GO

ALTER VIEW [dbo].[vPatient]
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName,
         gender.Name                                          Name_Gender,
         country.Name                                         PhoneCode_Country,
         client.Name                                          Name_Client,
         client.IsActive                                      IsActive_Client,
         dbo.fGetAPILink() + '/Content/Image/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageLocationFile,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageThumbnailLocationFile,
         waitingStatus.Name                                   WaitingStatus_Name_FilingStatus,
         attendingPersonell.Name                              LastAttendingPhysician_Name_Employee
  FROM   tPatient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN dbo.tCountry country
                ON H.ID_Country = country.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tFilingStatus waitingStatus
                ON waitingStatus.ID = H.WaitingStatus_ID_FilingStatus
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO

ALTER VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         CASE
           WHEN len(Trim(IsNull(CustomCode, ''))) = 0 THEN Code
           ELSE CustomCode
         END                                                                                      Code,
         Name,
         ID_Client,
         Name_Client,
         Email,
         ISNULL(Species, '')                                                                      Species,
         ISNULL(Name_Gender, '')                                                                  Name_Gender,
         ContactNumber,
         IsDeceased,
         DateDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile,
         IsNull(IsActive, 0)                                                                      IsActive,
         WaitingStatus_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                                    Microchip,
         ISNULL(Color, '')                                                                        Color,
         dbo.fGetAge(DateBirth, GETDATE(), '')                                                    Age,
         Idiosyncrasies,
         LastAttendingPhysician_Name_Employee
  FROM   dbo.vPatient

GO

CREATE OR
ALTER PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @records TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @records2 TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @MaxRecords TABLE
        (
           ID_Patient  INT,
           DateCreated DATETIME
        )

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_SOAP hed
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_SOAP'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_Wellness hed
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_Wellness'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @MaxRecords
             (ID_Patient,
              DateCreated)
      SELECT ID_Patient,
             MAX(DateCreated)
      FROM   @records
      GROUP  BY ID_Patient

      INSERT @records2
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT record.ID_Patient,
             record.TableName,
             record.DateCreated,
             record.Count
      FROM   @records record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_SOAP hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_SOAP'

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_Wellness hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_Wellness'

      --SELECT *
      --FROM   @records2 record

      UPDATE tPatient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tPatient patient
             INNER JOIN @records2 record
                     ON record.ID_Patient = patient.ID

      UPDATE tClient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tClient client
             INNER JOIN @records2 record
                     ON record.ID_Client = client.ID
  END

GO

--DECLARE @CountRecord INT = 0
--DECLARE @row INT = 1000
--DECLARE @Page INT = 0

--SELECT @CountRecord = COUNT(*)
--FROM   vActivePatient
--WHERE  LastAttendingPhysician_ID_Employee IS NULL

--SET @Page = @CountRecord / @row

--DECLARE @Number INT
--DECLARE db_cursor CURSOR FOR
--  select Number
--  FROM   dbo.fGetNumbersByRange(0, @Page)

--OPEN db_cursor

--FETCH NEXT FROM db_cursor INTO @Number

--WHILE @@FETCH_STATUS = 0
--  BEGIN
--      DECLARE @OffSet INT = @Number * @row
--      DECLARE @LIMIT INT = @row
--      DECLARE @sql VARCHAR(MAX) = '
--	DECLARE @IDs_Patient typIntList
	
--	INSERT @IDs_Patient
--	SELECT patient.ID
--	FROM   vActivePatient patient
--		   inner join vActiveCompany company
--				   on patient.ID_Company = company.ID
--	where  LastAttendingPhysician_ID_Employee IS NULL
--	ORDER BY ID
--	OFFSET /*OFFSET*/ ROWS
--	FETCH NEXT /*LIMIT*/ ROWS ONLY;

--	exec [pUpdatePatientsLastVisitedDate] @IDs_Patient
--  '

--      SET @sql = REPLACE(@sql, '/*OFFSET*/', CONVERT(VARCHAR(MAX), @OffSet))
--      SET @sql = REPLACE(@sql, '/*LIMIT*/', CONVERT(VARCHAR(MAX), @LIMIT))

--      PRINT 'Number'

--      PRINT @Number

--      PRINT ''

--      PRINT 'OffSet'

--      PRINT @OffSet

--      PRINT ''

--      PRINT 'LIMIT'

--      PRINT @LIMIT

--      PRINT ''

--      print @sql

--      EXEC (@sql)
--      FETCH NEXT FROM db_cursor INTO @Number
--  END

--CLOSE db_cursor

--DEALLOCATE db_cursor 
--GO

GO

exec _pAddModelProperty
  'tUserRole',
  'IsAdministrator',
  4

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vUserAdministrator
as
  select _user.ID,
         _user.ID                                                    ID_User,
         _user.ID_Employee,
         CONVERT(BIT, MAX(CONVERT(INT, _userroles.IsAdministrator))) IsAdministrator
  FROM   tUser _user
         LEFT join tUser_Roles _user_roles
                on _user.ID = _user_roles.ID_User
         LEFT join tUserRole _userroles
                on _user_roles.ID_UserRole = _userroles.ID
  GROUP  BY _user.ID,
            _user.ID_Employee

GO

ALTER PROC [dbo].[pGetEmployee] @ID         INT = -1,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Employee INT;
      DECLARE @UserAccount_ID_User INT;
      DECLARE @UserRole_IsAdministrator bit = 0;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserAccount_ID_User = ID
      FROM   tUser
      WHERE  ID_Employee = @ID
             AND IsActive = 1

      SELECT @UserRole_IsAdministrator = ISNULL(IsAdministrator, 0)
      FROM   vUserAdministrator
      where  ID_User = @ID_User

      SELECT @ID_Employee = ID_Employee,
             @ID_Company = emp.ID_Company
      FROM   dbo.tUser _user
             INNER JOIN dbo.tEmployee emp
                     ON emp.ID = _user.ID_Employee
      WHERE  _user.ID = @ID_User;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   (SELECT NULL                 AS [_],
                           -1                   AS [ID],
                           ''                   AS [Code],
                           NULL                 AS [Name],
                           1                    AS [IsActive],
                           NULL                 AS [Comment],
                           NULL                 AS [DateCreated],
                           NULL                 AS [DateModified],
                           NULL                 AS [ID_CreatedBy],
                           NULL                 AS [ID_LastModifiedBy],
                           @ID_Company          [ID_Company],
                           ''                   [Company],
                           ''                   FirstName,
                           ''                   LastName,
                           ''                   MiddleName,
                           @UserAccount_ID_User UserAccount_ID_User,
                           0                    IsSystemUsed) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   @UserAccount_ID_User      UserAccount_ID_User,
                   @UserRole_IsAdministrator IsAdministrator
            FROM   vEmployee H
            WHERE  H.ID = @ID
        END
  END

GO

ALTER PROC pCreateUser(@ID_Company  INT,
                       @ID_Employee INT,
                       @Username    varchar(MAX))
as
  BEGIN
      Declare @code Varchar(MAX) = ''

      SET @Username = REPLACE(LOWER(@Username), ' ', '')

      IF(SELECT COUNT(*)
         FROM   vUser
         WHERE  ID_Company = @ID_Company
                AND ID_Employee = @ID_Employee) = 0
        BEGIN
            SELECT @code = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            INSERT dbo.tUser
                   (Code,
                    Name,
                    IsActive,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ID_Employee,
                    Username,
                    ID_UserGroup,
                    Password,
                    IsRequiredPasswordChangedOnLogin,
                    ID_Patient)
            SELECT '',
                   emp.Name,
                   1,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   emp.ID,
                   LOWER(@code) + '-' + LOWER(@Username),
                   1,
                   LEFT(NEWID(), 4),
                   1,
                   NULL
            FROM   dbo.tEmployee emp
            WHERE  emp.ID_Company = @ID_Company
                   AND emp.ID = @ID_Employee
        END
  --DECLARE @ID_User INT  
  --SET @ID_User = @@IDENTITY  
  --SELECT Name_Company,  
  --       Employee,  
  --       Username,  
  --       Password  
  --FROM   vUser  
  --where  ID = @ID_User  
  END

GO

CREATE OR
ALTER PROC pDoCreateUser(@CompanyID   INT,
                         @ID_Employee INT,
                         @Username    varchar(MAX))
as
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''

      BEGIN TRY
          exec pCreateUser
            @CompanyID,
            @ID_Employee,
            @Username
      END TRY
      BEGIN CATCH
          SET @Success = 0
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_'

      SELECT @Success Success,
             @message message
  END

GO

CREATE  OR
ALTER PROC pUpdateEmployeetUserInfo(@EmployeeID     INT,
                                    @Username       VARCHAR(MAX),
                                    @Password       VARCHAR(MAX),
                                    @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      Update vUser
      SET    Username = @UserName,
             Password = @Password
      where  ID_Employee = @EmployeeID
             AND ID_Company = @ID_Company

      SELECT '_'

      SELECT @Success Success,
             @message Message
  END

GO

Update tUserRole
set    IsAdministrator = 1
where  Name = 'Administrator'

UPDATE tUser
SET    IsActive = 0
WHERE  ID IN (SELECT MIN(ID)
              FROM   VUser
              where  ID_Employee IS NOT NULL
              GROUP  BY ID_Company,
                        ID_Employee
              HAVING COUNT(*) > 1)

Update vActiveUserRole
set    IsActive = 0
where  ID IN ( 13, 5, 6, 7,
               8, 9, 11, 10,
               18, 17, 19, 20,
               21, 22, 47, 69 ) 
			   GO

CREATE OR
ALTER PROC pMergePatientRecordByCompanyID(@ID_Company               INT,
                                          @Source_Code_Patient      VARCHAR(MAX),
                                          @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------    
      DECLARE @Source1_ID_Patient INT
      DECLARE @Destination_ID_Patient INT
      DECLARE @Source1_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Patient = ID,
             @Source1_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Source_Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Patient = ID,
             @Destination_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Destination_Code_Patient
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Patient - '
                     + ' from ' + @Source1_Name_Patient + ' ('
                     + @Source_Code_Patient + ') to '
                     + @Destination_Name_Patient + ' ('
                     + @Destination_Code_Patient + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Source1_ID_Patient
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination',
      --       IsActive
      --FROm   vPatient
      --WHERE  ID = @Destination_ID_Patient
      --       AND ID_Company = @ID_Company
      exec pMergePatientRecord
        @Source1_ID_Patient,
        @Destination_ID_Patient,
        @Comment

      Update tPatient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Patient
             and ID_Company = @ID_Company
  END

GO

CREATE OR
ALTER PROC pMergePatientRecordByCompany(@GUID_Company             VARCHAR(MAX),
                                        @Source_Code_Patient      VARCHAR(MAX),
                                        @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergePatientRecordByCompany
        @ID_Company,
        @Source_Code_Patient,
        @Destination_Code_Patient
  END

GO

GO

CREATE OR
ALTER PROC pDoMergePatientRecord(@From_ID_Patient INT,
                                 @To_ID_Patient   INT,
                                 @ID_UserSession  INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Patient VARCHAR(MAX) = ''
      DECLARE @To_Code_Patient VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Patient

      SELECT @To_Code_Patient = Code
      FROM   tPatient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Patient

      --select @To_Code_Patient,
      --       @To_ID_Patient
      BEGIN TRY
          exec pMergePatientRecordByCompanyID
            @ID_Company,
            @From_Code_Patient,
            @To_Code_Patient
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO

GO

CREATE   OR
ALTER PROC pMergeClientRecordByCompanyID(@ID_Company              INT,
                                         @Source_Code_Client      VARCHAR(MAX),
                                         @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  ID = @ID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      --SELECT *
      --FROm   vCompanyActive
      --WHERE  ID = @ID_Company
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @ID_Company

      ----------------------------------------------------------------------------------------      
      DECLARE @Source1_ID_Client INT
      DECLARE @Destination_ID_Client INT
      DECLARE @Source1_Name_Client VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Client VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Client = ID,
             @Source1_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Source_Code_Client
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Client = ID,
             @Destination_Name_Client = Name
      FROm   vClient
      WHERE  Code = @Destination_Code_Client
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Client - '
                     + ' from ' + @Source1_Name_Client + ' ('
                     + @Source_Code_Client + ') to '
                     + @Destination_Name_Client + ' ('
                     + @Destination_Code_Client + ')'

      --SELECT ID,
      --       Code,
      --       Name,
      --       'Source 1'
      --FROm   vClient
      --WHERE  ID = @Source1_ID_Client
      --       AND ID_Company = @ID_Company
      --Union ALL
      --SELECT ID,
      --       Code,
      --       Name,
      --       'Destination'
      --FROm   vClient
      --WHERE  ID = @Destination_ID_Client
      --       AND ID_Company = @ID_Company
      exec pMergeClientRecord
        @Source1_ID_Client,
        @Destination_ID_Client,
        @Comment

      Update tClient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Client
             and ID_Company = @ID_Company

      Update tPatient_SOAP
      SET    ID_Client = patient.ID_Client
      FROM   tPatient_SOAP _soap
             inner join vPatient patient
                     on _soap.ID_Patient = patient.ID
      where  _soap.ID_Client <> patient.ID_Client
             AND patient.ID_Client = @Destination_ID_Client
  END

GO

GO

CREATE   OR
ALTER PROC pMergeClientRecordByCompany(@GUID_Company            VARCHAR(MAX),
                                       @Source_Code_Client      VARCHAR(MAX),
                                       @Destination_Code_Client VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------    
      exec pMergeClientRecordByCompanyID
        @ID_Company,
        @Source_Code_Client,
        @Destination_Code_Client
  END

GO

CREATE OR
ALTER PROC pDoMergeClientRecord(@From_ID_Client INT,
                                @To_ID_Client   INT,
                                @ID_UserSession INT)
AS
  BEGIN
      DECLARE @success BIT = 1
      DEclare @message VARCHAR(MAX) = ''
      DECLARE @ID_Company INT
      DECLARE @From_Code_Client VARCHAR(MAX) = ''
      DECLARE @To_Code_Client VARCHAR(MAX) = ''

      SELECT @ID_Company = _user.ID_Company
      FROM   tUserSession _session
             inner join vUser _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_UserSession

      SELECT @From_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @From_ID_Client

      SELECT @To_Code_Client = Code
      FROM   tClient
      where  ID_Company = @ID_Company
             AND ID = @To_ID_Client

      --select @From_ID_Client,
      --       @From_Code_Client,
      --       @ID_Company
      --select @To_ID_Client,
      --       @To_Code_Client,
      --       @ID_Company
      BEGIN TRY
          exec pMergeClientRecordByCompanyID
            @ID_Company,
            @From_Code_Client,
            @To_Code_Client
      END TRY
      BEGIN CATCH
          set @success = 0
          SET @message = ERROR_MESSAGE()
      END CATCH

      SELECT '_'

      SELECT @success Success,
             @message message
  END

GO 
GO

CREATE    or
ALTER VIEW [dbo].[vPatient_Wellness_Listview]
AS
  SELECT wellness.ID,
         wellness.Date,
         wellness.Code,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         ID_Company,
         ID_Patient_SOAP,
         AttendingPhysician_Name_Employee,
         Weight,
         Temperature
  FROM   vPatient_Wellness wellness
  WHERE  ID_FilingStatus NOT IN ( 4 )

GO 

CREATE OR
ALTER VIEW [dbo].[vItem]
AS
  SELECT H.[ID],
         ISNULL(h.CustomCode, h.Code) Code,
         h.Code                       _Code,
         H.[Name],
         H.[IsActive],
         H.[ID_Company],
         H.[Comment],
         H.[DateCreated],
         H.[DateModified],
         H.[ID_CreatedBy],
         H.[ID_LastModifiedBy],
         H.[ID_ItemType],
         H.[ID_ItemCategory],
         H.[MinInventoryCount],
         H.[MaxInventoryCount],
         H.[UnitCost],
         H.[UnitPrice],
         H.[CurrentInventoryCount],
         H.[Old_item_id],
         H.[Old_procedure_id],
         H.[OtherInfo_DateExpiration],
         H.[ID_InventoryStatus],
         H.[BarCode],
         H.[CustomCode],
         H.[_tempSupplier],
         H.[tempID],
         H.[SKUCode],
         H.[SKU],
         H.[UnitName],
         H.[StockNumber],
         H.[StockLocation],
         H.[ProjectName],
         H.[PONumber],
         H.[DateDelivered],
         UC.Name                      AS CreatedBy_Name_User,
         UM.Name                      AS LastModifiedBy_Name_User,
         itemType.Name                Name_ItemType,
         itemCategory.Name            Name_ItemCategory,
         inventStatus.Name            Name_InventoryStatus,
         H.CurrentInventoryCount      _CurrentInventoryCount
  FROM   tItem H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItemType itemType
                ON itemType.ID = H.ID_ItemType
         LEFT JOIN tItemCategory itemCategory
                ON itemCategory.ID = H.ID_ItemCategory
         LEFT JOIN tInventoryStatus inventStatus
                ON inventStatus.ID = H.ID_InventoryStatus

GO


exec _prefreshAllViews
