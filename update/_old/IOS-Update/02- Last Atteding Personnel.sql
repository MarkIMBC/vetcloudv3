exec _pAddModelProperty
  'tPatient',
  'LastAttendingPhysician_ID_Employee',
  2

exec _pAddModelProperty
  'tClient',
  'LastAttendingPhysician_ID_Employee',
  2

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER FUNCTION fGetNumbersByRange(@from INT,
                                  @to   INT)
RETURNS @CountTable TABLE (
  Number INT)
AS
  BEGIN
      with cte_n
           as (select @from as [CountNumber]
               union all
               select CountNumber + 1
               from   cte_n
               where  CountNumber < @to)
      Insert @CountTable
      select CountNumber
      from   cte_n
      option (maxrecursion 0)

      return
  END

GO

ALTER VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name                 AS CreatedBy,
         UM.Name                 AS LastModifiedBy,
         H.DateLastVisited,
         H.CurrentCreditAmount,
         acct.Username,
         acct.Password,
         h.ProfileImageFile,
         attendingPersonell.Name LastAttendingPhysician_Name_Employee
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tUser acct
                ON H.ID_User = acct.ID
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO

ALTER VIEW vClient_ListView
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '') ContactNumbers,
         DateCreated,
         DateLastVisited,
         ISNULL(IsActive, 0)          IsActive,
         LastAttendingPhysician_Name_Employee
  FROM   dbo.vClient

GO

ALTER VIEW [dbo].[vPatient]
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName,
         gender.Name                                          Name_Gender,
         country.Name                                         PhoneCode_Country,
         client.Name                                          Name_Client,
         client.IsActive                                      IsActive_Client,
         dbo.fGetAPILink() + '/Content/Image/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageLocationFile,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageThumbnailLocationFile,
         waitingStatus.Name                                   WaitingStatus_Name_FilingStatus,
         attendingPersonell.Name                              LastAttendingPhysician_Name_Employee
  FROM   tPatient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN dbo.tCountry country
                ON H.ID_Country = country.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tFilingStatus waitingStatus
                ON waitingStatus.ID = H.WaitingStatus_ID_FilingStatus
         LEFT JOIN tEmployee attendingPersonell
                ON H.LastAttendingPhysician_ID_Employee = attendingPersonell.ID

GO

ALTER VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         CASE
           WHEN len(Trim(IsNull(CustomCode, ''))) = 0 THEN Code
           ELSE CustomCode
         END                                                                                      Code,
         Name,
         ID_Client,
         Name_Client,
         Email,
         ISNULL(Species, '')                                                                      Species,
         ISNULL(Name_Gender, '')                                                                  Name_Gender,
         ContactNumber,
         IsDeceased,
         DateDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile,
         IsNull(IsActive, 0)                                                                      IsActive,
         WaitingStatus_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                                    Microchip,
         ISNULL(Color, '')                                                                        Color,
         dbo.fGetAge(DateBirth, GETDATE(), '')                                                    Age,
         Idiosyncrasies,
         LastAttendingPhysician_Name_Employee
  FROM   dbo.vPatient

GO

CREATE OR
ALTER PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @records TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @records2 TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @MaxRecords TABLE
        (
           ID_Patient  INT,
           DateCreated DATETIME
        )

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_SOAP hed
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_SOAP'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_Wellness hed
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_Wellness'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @MaxRecords
             (ID_Patient,
              DateCreated)
      SELECT ID_Patient,
             MAX(DateCreated)
      FROM   @records
      GROUP  BY ID_Patient

      INSERT @records2
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT record.ID_Patient,
             record.TableName,
             record.DateCreated,
             record.Count
      FROM   @records record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_SOAP hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_SOAP'

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_Wellness hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_Wellness'

      --SELECT *
      --FROM   @records2 record

      UPDATE tPatient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tPatient patient
             INNER JOIN @records2 record
                     ON record.ID_Patient = patient.ID

      UPDATE tClient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tClient client
             INNER JOIN @records2 record
                     ON record.ID_Client = client.ID
  END

GO

--DECLARE @CountRecord INT = 0
--DECLARE @row INT = 1000
--DECLARE @Page INT = 0

--SELECT @CountRecord = COUNT(*)
--FROM   vActivePatient
--WHERE  LastAttendingPhysician_ID_Employee IS NULL

--SET @Page = @CountRecord / @row

--DECLARE @Number INT
--DECLARE db_cursor CURSOR FOR
--  select Number
--  FROM   dbo.fGetNumbersByRange(0, @Page)

--OPEN db_cursor

--FETCH NEXT FROM db_cursor INTO @Number

--WHILE @@FETCH_STATUS = 0
--  BEGIN
--      DECLARE @OffSet INT = @Number * @row
--      DECLARE @LIMIT INT = @row
--      DECLARE @sql VARCHAR(MAX) = '
--	DECLARE @IDs_Patient typIntList
	
--	INSERT @IDs_Patient
--	SELECT patient.ID
--	FROM   vActivePatient patient
--		   inner join vActiveCompany company
--				   on patient.ID_Company = company.ID
--	where  LastAttendingPhysician_ID_Employee IS NULL
--	ORDER BY ID
--	OFFSET /*OFFSET*/ ROWS
--	FETCH NEXT /*LIMIT*/ ROWS ONLY;

--	exec [pUpdatePatientsLastVisitedDate] @IDs_Patient
--  '

--      SET @sql = REPLACE(@sql, '/*OFFSET*/', CONVERT(VARCHAR(MAX), @OffSet))
--      SET @sql = REPLACE(@sql, '/*LIMIT*/', CONVERT(VARCHAR(MAX), @LIMIT))

--      PRINT 'Number'

--      PRINT @Number

--      PRINT ''

--      PRINT 'OffSet'

--      PRINT @OffSet

--      PRINT ''

--      PRINT 'LIMIT'

--      PRINT @LIMIT

--      PRINT ''

--      print @sql

--      EXEC (@sql)
--      FETCH NEXT FROM db_cursor INTO @Number
--  END

--CLOSE db_cursor

--DEALLOCATE db_cursor 
--GO