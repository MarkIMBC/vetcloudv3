GO

CREATE OR
ALTER VIEW vPatient_Wellness_DetailSchedule
as
  SELECT hed.ID                 ID_Patient_Wellness,
         hed.Code               Code,
         wellSched.ID           ID_Patient_Wellness_Schedule,
         hed.ID_FilingStatus,
         hed.ID_Client,
         hed.ID_Patient,
         hed.ID_Company,
         wellDetail.Name_Item   Name_Item_Patient_Wellness_Detail,
         wellDetail.DateCreated DateCreated_Patient_Wellness_Detail,
         wellDetail.Comment     Comment_Patient_Wellness_Detail,
         wellSched.Date         Date_Patient_Wellness_Schedule,
         wellSched.IsSentSMS
  FROm   tPatient_Wellness hed
         INNER JOIN vPatient_Wellness_Detail wellDetail
                 on hed.ID = wellDetail.ID_Patient_Wellness
         INNER JOIN tPatient_Wellness_Schedule wellSched
                 on hed.ID = wellSched.ID_Patient_Wellness
  where  wellDetail.Name_Item IS NOT NULL

GO

ALTER FUNCTION dbo.fGetSOAPLANMessage (@CompanyName        Varchar(MAX),
                                       @SOAPPlanSMSMessage VARCHAR(MAX),
                                       @Client             Varchar(MAX),
                                       @ContactNumber      Varchar(MAX),
                                       @Pet                Varchar(MAX),
                                       @Service            Varchar(MAX),
                                       @Reason             Varchar(MAX),
                                       @DateReturn         DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')
      Declare @message Varchar(MAX) = @SOAPPlanSMSMessage

      if( LEN(ISNULL(@Reason, '')) > 0 )
        SET @Reason = '- ' + @Reason

      SET @Pet = ISNULL(@Pet, 'your Pet')
      SET @Client = ISNULL(@Client, '')
      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Pet)))
      SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
      SET @message = REPLACE(@message, '/*Reason*/', LTRIM(RTRIM(@Reason)))
      SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))

      RETURN @message
  END

GO

ALTER FUNCTION [dbo].[fGetSendSOAPPlan](@Date             DateTime,
                                        @IsSMSSent        Bit = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETime,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETime,
  DateCreated          DATETime,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX))
as
  BEGIN
      DECLARE @IDs_Company typIntList
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      if( LEN(TRIM(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            Select Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting
            WHERE  ISNULL(IsActive, 0) = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*SOAP PLAN */
      INSERT @table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, ISNULL(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     on patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.Name

      /*Patient Wellness Schedule*/
      INSERT @table
      SELECT c.ID                                                                                                                                                                                                                                                                ID_Company,
             c.Name                                                                                                                                                                                                                                                              Name_Company,
             client.Name                                                                                                                                                                                                                                                         Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                                                                  ContactNumber_Client,
             wellDetSched.Date_Patient_Wellness_Schedule,
             wellDetSched.Name_Item_Patient_Wellness_Detail,
             ISNULL(wellDetSched.Comment_Patient_Wellness_Detail, '')                                                                                                                                                                                                            Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, wellDetSched.Name_Item_Patient_Wellness_Detail, ISNULL(wellDetSched.Comment_Patient_Wellness_Detail, ''), wellDetSched.Date_Patient_Wellness_Schedule) Message,
             CONVERT(DATE, DATEADD(DAY, -1, wellDetSched.Date_Patient_Wellness_Schedule))                                                                                                                                                                                        DateSending,
             wellDetSched.DateCreated_Patient_Wellness_Detail,
             wellDetSched.ID_Patient_Wellness_Schedule                                                                                                                                                                                                                           ID_Patient_Wellness_Schedule,
             @Patient_Wellness_Schedule_ID_Model,
             wellDetSched.Code
      FROM   dbo.vPatient_Wellness_DetailSchedule wellDetSched
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellDetSched.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellDetSched.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellDetSched.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     on wellDetSched.ID_Company = idsCompany.ID
      WHERE  wellDetSched.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND ISNULL(wellDetSched.ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, wellDetSched.Date_Patient_Wellness_Schedule)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.Name

      /*Vaccination*/
      Declare @IDs_Vaccination_ExistingPatientWellness typIntList

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             ISNULL(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, vac.Name_Item, ISNULL(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DATEADD(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             inner join tPatient_Vaccination_Schedule vacSchedule
                     on vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     on vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND ISNULL(vac.ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
			 AND vac.ID NOT IN ( SELECT ID FROM @IDs_Vaccination_ExistingPatientWellness)

      RETURN
  end

GO

ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT)
AS
  BEGIN
      /*        
       iTextMo Status        
             
       "1" = Invalid Number.        
       "2" = Number prefix not supported. Please contact us so we can add.        
       "3" = Invalid ApiCode.        
       "4" = Maximum Message per day reached. This will be reset every 12MN.        
       "5" = Maximum allowed characters for message reached.        
       "6" = System OFFLINE.        
       "7" = Expired ApiCode.        
       "8" = iTexMo Error. Please try again later.        
       "9" = Invalid Function Parameters.        
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.        
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.        
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.        
       "13" = Invalid or Not Registered Custom Sender ID.        
       "14" = Invalid preferred server number.        
       "15" = IP Filtering enabled - Invalid IP.        
       "16" = Authentication error. Contact support at support@itexmo.com        
       "17" = Telco Error. Contact Support support@itexmo.com        
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com        
       "19" = Account suspended. Contact Support support@itexmo.com        
       "0" = Success! Message is now on queue and will be sent soon       
      "-1" = Reach VetCloud SMS Count Limit       
      "20" = Manual Sent SMS    
        
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1;

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO

Update tPatient_Wellness_Schedule
SET    IsSentSMS = vacSched.IsSentSMS,
       DateSent = vacSched.DateSent
FROM   tPatient_Wellness_Schedule wellSched
       INNER JOIN tPatient_Vaccination_Schedule vacSched
               on wellSched.ID_Patient_Vaccination_Schedule = vacSched.ID 
 