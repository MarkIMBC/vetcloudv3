IF NOT EXISTS (SELECT 1
               FROM   INFORMATION_SCHEMA.TABLES
               WHERE  TABLE_TYPE = 'BASE TABLE'
                      AND TABLE_NAME = 'tPatient_Wellness')
  BEGIN
      exec _pCreateAppModuleWithTable
        'tPatient_Wellness',
        1,
        NULL,
        NULL

      exec _pCreateAppModuleWithTable
        'tPatient_Wellness_Detail',
        1,
        NULL,
        NULL

      exec _pCreateAppModuleWithTable
        'tPatient_Wellness_Schedule',
        1,
        NULL,
        NULL

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'ID_Patient_Wellness',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'ID_Patient_Wellness',
        2

      exec _pAddModelDetail
        'tPatient_Wellness_Detail',
        'ID_Patient_Wellness',
        'tPatient_Wellness',
        NULL

      exec _pAddModelDetail
        'tPatient_Wellness_Schedule',
        'ID_Patient_Wellness',
        'tPatient_Wellness',
        NULL

      exec _pRefreshAllViews

      exec _pDeleteModelProperty
        NULL,
        'tPatient_Wellness',
        'AttendingPhysician_ID_Employee',
        1

      /*Additional*/
      exec _pAddModelProperty
        'tPatient_Wellness',
        'Date',
        5

      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_Client',
        2

      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_Patient',
        2

      exec _pAddModelProperty
        'tPatient_Wellness',
        'AttendingPhysician_ID_Employee',
        2

      exec _pAddModelProperty
        'tPatient_Wellness',
        'AttendingPhysician',
        1

      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_FilingStatus',
        2

		
      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_Patient_Vaccination',
        2


      exec _pAddModelProperty
        'tPatient_Wellness',
        'DateCanceled',
        5

      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_CanceledBy',
        2

      exec _pAddModelProperty
        'tPatient_Wellness',
        'ID_Patient_SOAP',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'Date',
        5

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'DateSent',
        5

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'IsSentSMS',
        4

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'ID_Item',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'UnitCost',
        3

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'UnitPrice',
        3

						
      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'ID_Patient_Vaccination_Detail',
        2

		  exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'ID_Patient_Vaccination_Schedule',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'DateExpiration',
        5

      exec _pRefreshAllViews
  END

GO

CREATE OR
ALTER VIEW vPatient_Wellness
AS
  SELECT H.*,
         UC.Name                                                CreatedBy,
         UM.Name                                                LastModifiedBy,
         client.Name                                            Name_Client,
         patient.Name                                           Name_Patient,
         fs.Name                                                Name_FilingStatus,
         ISNULL(_AttendingPhysician.Name, H.AttendingPhysician) AttendingPhysician_Name_Employee
  FROM   tPatient_Wellness H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                on patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                on fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                on _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee

GO

CREATE OR
ALTER VIEW vPatient_Wellness_Listview
AS
  SELECT wellness.ID,
         wellness.Date,
         wellness.Code,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         ID_Company,
         ID_Patient_SOAP,
         AttendingPhysician_Name_Employee
  FROM   vPatient_Wellness wellness

GO

CREATE OR
ALTER VIEW vPatient_Wellness_Detail
AS
  SELECT H.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         item.Name Name_Item
  FROM   tPatient_Wellness_Detail H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItem item
                ON item.ID = H.ID_Item

GO

Create OR
ALTER VIEW vPatient_Wellness_Detail_Listview
AS
  select welldetail.ID,
         welldetail.ID_Patient_Wellness,
         welldetail.Name_Item,
         wellHed.Date,
         welldetail.Comment,
         ID_Patient_SOAP,
         wellHed.ID_FilingStatus,
         wellHed.ID_Company
  FROM   vPatient_Wellness wellHed
         INNER JOIN vPatient_Wellness_Detail welldetail
                 on wellHed.ID = welldetail.ID_Patient_Wellness
				 WHERE wellHed.ID_FilingStatus NOT IN (4)

Go 

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_Patient_Wellness] (@ID_CurrentObject VARCHAR(10),
                                                       @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Wellness
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Wellness';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Wellness
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Patient_Wellness typIntList

      INSERT @IDs_Patient_Wellness
      VALUES (@ID_CurrentObject)
  END;

GO

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROCEDURE pGetPatient_Wellness @ID                             INT = -1,
                                     @AttendingPhysician_ID_Employee INT,
                                     @ID_Client                      INT,
                                     @ID_Patient                     INT,
                                     @ID_Item                        INT,
                                     @ID_Patient_SOAP                INT,
                                     @ID_Session                     INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Wellness_Detail,
             '' AS Patient_Wellness_Schedule

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      if( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_SOAP
            where  ID = @ID_Patient_SOAP
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                  Name_FilingStatus,
                   patient.ID_Client        ID_Client,
                   client.Name              Name_Client,
                   patient.Name             Name_Patient,
                   _AttendingPhysician.Name AttendingPhysician_Name_Employee
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '-- NEW --'                     AS [Code],
                           NULL                            AS [Name],
                           GETDATE()                       as Date,
                           1                               AS [IsActive],
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           NULL                            AS [ID_CreatedBy],
                           NULL                            AS [ID_LastModifiedBy],
                           @AttendingPhysician_ID_Employee AS [AttendingPhysician_ID_Employee],
                           @ID_Client                      AS [ID_Client],
                           @ID_Patient                     AS [ID_Patient],
                           @ID_Patient_SOAP                AS [ID_Patient_SOAP],
                           1                               ID_FilingStatus,
                           ''                              AttendingPhysician) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          on client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          on patient.ID = H.ID_Patient
                   LEFT JOIN tFilingStatus fs
                          on fs.ID = H.ID_FilingStatus
                   LEFT JOIN tEmployee _AttendingPhysician
                          on _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Wellness H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Wellness_Detail
      WHERE  ID_Patient_Wellness = @ID

      SELECT *
      FROM   vPatient_Wellness_Schedule
      WHERE  ID_Patient_Wellness = @ID
      ORder  By Date ASC
  END

GO 
