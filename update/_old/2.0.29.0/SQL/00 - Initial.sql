IF COL_LENGTH('tPatient_Wellness', 'Weight') IS NULL
  BEGIN
      EXEc _pAddModelProperty
        'tPatient_Wellness',
        'Weight',
        1

      exec _pRefreshAllViews
  END

GO

ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         IsNull(patientSOAP.Name_SOAPType, '')
         + ' - '
         + IsNull(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)
                  + '|' + CONVERT(VARCHAR(MAX), wellSched.ID)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull('', '') + ' - '
                  + IsNull(wellSched.Comment, '')                                      Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

ALTER VIEW [dbo].[vBillingInvoice]
AS
  SELECT H.*,
         CONVERT(VARCHAR(100), H.Date, 101)  DateString,
         UC.Name                             AS CreatedBy_Name_User,
         UM.Name                             AS LastModifiedBy_Name_User,
         approvedUser.Name                   AS ApprovedBy_Name_User,
         cancelUser.Name                     AS CanceledBy_Name_User,
         company.Name                        Company,
         h.PatientNames                      Name_Patient,
         taxScheme.Name                      Name_TaxScheme,
         fs.Name                             Name_FilingStatus,
         fsPayment.Name                      Payment_Name_FilingStatus,
         client.Name                         Name_Client,
         CONVERT(VARCHAR, H.DateCreated, 0)  DateCreatedString,
         CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
         CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
         CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString,
         attendingPhysicianEmloyee.Name      AttendingPhysician_Name_Employee,
         CASE
           WHEN H.ID_FilingStatus = 3 THEN fsPayment.Name
           ELSE fs.Name
         END                                 Status,
         soapType.Name                       Name_SOAPType,
         h.Code
         + CASE
             WHEN ISNULL(h.OtherReferenceNumber, '') <> '' THEN ' (' + h.OtherReferenceNumber + ')'
             ELSE ''
           END                               CustomCode
  FROM   dbo.tBillingInvoice H
         LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tCompany company
                ON H.ID_Company = company.ID
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN dbo.tTaxScheme taxScheme
                ON taxScheme.ID = H.ID_TaxScheme
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tFilingStatus fsPayment
                ON fsPayment.ID = H.Payment_ID_FilingStatus
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN tSOAPType soapType
                ON H.ID_SOAPType = soapType.ID

GO

ALTER VIEW vClientDeposit
AS
  SELECT H.*,
         ISNULL(confinement.Name_Client, client.Name) Name_Client,
         confinement.ID_Patient,
         confinement.Name_Patient,
         UC.Name                                      AS CreatedBy,
         UM.Name                                      AS LastModifiedBy,
         approvedUser.Name                            AS ApprovedBy_Name_User,
         cancelUser.Name                              AS CanceledBy_Name_User,
         fs.Name                                      Name_FilingStatus,
         confinement.Code                             Code_Patient_Confinement
  FROM   tClientDeposit H
         LEFT JOIN vPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.ID_FilingStatus
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN tClient client
                ON client.ID = h.ID_Client

GO

ALTER VIEW [dbo].[vItemInventoriable_ListvIew]
AS
  SELECT item.ID,
         item.Name,
         CASE
           WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
           ELSE ''
         END                                                     RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
         item.OtherInfo_DateExpiration                           DateExpired,
         item.ID_Company,
         item.CurrentInventoryCount,
         item.ID_InventoryStatus,
         invtStatus.Name                                         Name_InventoryStatus,
         item.IsActive,
         item.UnitCost,
         Item.UnitPrice,
         item.OtherInfo_DateExpiration,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '')                      Name_ItemCategory
  FROM   dbo.vitem item
         LEFT JOIN tInventoryStatus invtStatus
                on item.ID_InventoryStatus = invtStatus.ID
  WHERE  item.ID_ItemType = 2
         AND ISNULL(item.IsActive, 0) = 1

GO

ALTER VIEW dbo.vItemService_Listview
AS
  SELECT item.ID,
         item.Name,
         item.ID_Company,
         ISNULL(item.UnitCost, 0)           UnitCost,
         ISNULL(item.UnitPrice, 0)          UnitPrice,
         item.IsActive,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '') Name_ItemCategory
  FROM   dbo.vitem item
  WHERE  item.ID_ItemType = 1
         AND ISNULL(item.IsActive, 0) = 1

GO

ALTER VIEW dbo.vzBillingInvoiceReport
AS
  SELECT biHed.ID,
         biHed.Code,
         bihed.CustomCode,
         biHed.Date,
         biHed.Name_Patient,
         biHed.BillingAddress,
         biHed.Name_FilingStatus,
         biHed.CreatedBy_Name_User,
         biHed.ApprovedBy_Name_User,
         biHed.Name_TaxScheme,
         biHed.DateApproved,
         biHed.SubTotal,
         ISNULL(biHed.TotalItemDiscountAmount, 0) TotalItemDiscountAmount,
         biHed.DiscountRate,
         biHed.DiscountAmount,
         biHed.TotalAmount,
         biHed.GrossAmount,
         biHed.VatAmount,
         biHed.NetAmount,
         biDetail.ID                              ID_BillingInvoice_Detail,
         biDetail.Name_Item,
         biDetail.Quantity,
         biDetail.UnitPrice,
         biDetail.Amount,
         biHed.ID_Company,
         ISNULL(biHed.Comment, '')                Comment,
         company.ImageLogoLocationFilenamePath,
         company.Name                             Name_Company,
         company.Address                          Address_Company
  FROM   dbo.vBillingInvoice biHed
         LEFT JOIN dbo.vBillingInvoice_Detail biDetail
                ON biHed.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company

GO

ALTER VIEW [dbo].[vzPatientBillingInvoiceReport]
AS
  SELECT biHed.ID,
         biHed.Code,
         bihed.CustomCode,
         biHed.Date,
         ISNULL(biHed.PatientNames, biHed.Name_Patient) Name_Patient,
         biHed.BillingAddress,
         biHed.Name_FilingStatus,
         CASE
           WHEN Lower(biHed.CreatedBy_Name_User) NOT LIKE 'admin%' THEN biHed.CreatedBy_Name_User
           ELSE ''
         END                                            CreatedBy_Name_User,
         CASE
           WHEN Lower(biHed.CreatedBy_Name_User) NOT LIKE 'admin%' THEN biHed.ApprovedBy_Name_User
           ELSE ''
         END                                            ApprovedBy_Name_User,
         biHed.Name_TaxScheme,
         biHed.DateApproved,
         ISNULL(biHed.InitialSubtotalAmount, 0)         InitialSubtotalAmount,
         ISNULL(biHed.ConfinementDepositAmount, 0)      ConfinementDepositAmount,
         ISNULL(biHed.ConsumedDepositAmount, 0)         ConsumedDepositAmount,
         ISNULL(biHed.RemainingDepositAmount, 0)        RemainingDepositAmount,
         biHed.SubTotal,
         ISNULL(biHed.TotalItemDiscountAmount, 0)       TotalItemDiscountAmount,
         ISNULL(biHed.DiscountRate, 0)                  DiscountRate,
         ISNULL(biHed.DiscountAmount, 0)                DiscountAmount,
         biHed.TotalAmount,
         biHed.GrossAmount,
         biHed.VatAmount,
         biHed.NetAmount,
         biDetail.ID                                    ID_BillingInvoice_Detail,
         biDetail.Name_Item,
         biDetail.Quantity,
         biDetail.UnitPrice,
         biDetail.DiscountRate                          DiscountRate_BillingInvoice_Detail,
         biDetail.DiscountAmount                        DiscountAmount_BillingInvoice_Detail,
         biDetail.Amount,
         biHed.ID_Company,
         biHed.Name_Client,
         ISNULL(biHed.IsWalkIn, 0)                      IsWalkIn,
         ISNULL(biHed.WalkInCustomerName, '')           WalkInCustomerName,
         ISNULL(biHed.Comment, '')                      Comment,
         company.ImageLogoLocationFilenamePath,
         company.Name                                   Name_Company,
         company.Address                                Address_Company,
         company.ContactNumber                          ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                          HeaderInfo_Company,
         CASE
           WHEN itemservices.Date IS NOT NULL THEN FORMAT(itemservices.Date, 'MM/dd/yyyy')
           ELSE
             CASE
               WHEN ISNULL(biHed.ID_Patient_Confinement, 0) = 0 THEN ''
               ELSE '   '
             END
         END                                            Date_Patient_Confinement_ItemsServices,
         ISNULL(confi.Code, '')                         Code_Patient_Confinement
  FROM   dbo.vBillingInvoice biHed
         LEFT JOIN dbo.vBillingInvoice_Detail biDetail
                ON biHed.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
         LEFT JOIN tPatient_Confinement_ItemsServices itemservices
                ON biDetail.ID_Patient_Confinement_ItemsServices = itemservices.ID
         LEFT JOIN tPatient_Confinement confi
                ON biHed.ID_Patient_Confinement = confi.ID

GO

ALTER VIEW [dbo].[vzPaymentTransactionSummaryReport]
as
  SELECT biHed.ID                                                          ID_BillingInvoice,
         pHed.ID                                                           ID_PaymentTransaction,
         biHed.Code                                                        Code_BillingInvoice,
         biHed.Date                                                        Date_BillingInvoice,
         biHed.TotalAmount                                                 BalanceAmount,
         pHed.Code                                                         Code_PaymentTransaction,
         pHed.Date                                                         Date_PaymentTransaction,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         ISNULL(biHed.PatientNames, patient.Name)                          Name_Patient,
         biHed.ID_Company,
         pHed.ID_PaymentMethod,
         payMethod.Name                                                    Name_PaymentMethod,
         phed.PayableAmount,
         CASE
           WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount
           ELSE phed.PaymentAmount
         END                                                               PaymentAmount,
         pHed.RemainingAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                             HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')                  AttendingPhysician_ID_Employee,
         ISNULL(attendingVeterinarian.Name, '(No Assigned)')               AttendingPhysician_Name_Employee,
         attendingVeterinarian.Name_Position
  FROM   tPaymentTransaction pHed
         INNER JOIN vBillingInvoice biHed
                 on biHed.ID = pHed.ID_BillingInvoice
         LEFT JOIN tClient client
                ON client.ID = biHed.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = biHed.ID_Patient
         LEFT JOIN tPaymentMethod payMethod
                ON payMethod.ID = pHed.ID_PaymentMethod
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
         LEFT JOIN dbo.vEmployee attendingVeterinarian
                ON attendingVeterinarian.ID = biHed.[AttendingPhysician_ID_Employee]
  WHERE  biHed.Payment_ID_FilingStatus IN ( 11, 12 )
         AND pHed.ID_FilingStatus IN ( 3 )
  UNION ALL
  SELECT _deposit.ID                       ID_BillingInvoice,
         NULL                              ID_PaymentTransaction,
         _deposit.Code                     Code_BillingInvoice,
         _deposit.Date                     Date_BillingInvoice,
         0                                 BalanceAmount,
         ''                                Code_PaymentTransaction,
         _deposit.Date                     Date_PaymentTransaction,
         _deposit.Name_Client,
         ISNULL(_deposit.Name_Patient, '') Name_Patient,
         _deposit.ID_Company               ID_Company,
         6                                 ID_PaymentMethod,
         'Deposit'                         Name_PaymentMethod,
         0                                 PayableAmount,
         _deposit.DepositAmount            PaymentAmount,
         0                                 RemainingAmount,
         company. ImageLogoLocationFilenamePath,
         company.Name                      Name_Company,
         company.Address                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                             HeaderInfo_Company,
         NULL                              AttendingPhysician_ID_Employee,
         ''                                AttendingPhysician_Name_Employee,
         ''                                Name_Position
  FROM   vClientDeposit _deposit
         LEFT JOIN dbo.vCompany company
                ON company.ID = _deposit.ID_Company
  WHERE  ID_FilingStatus IN ( 3 )

GO

ALTER VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         CASE
           WHEN len(Trim(IsNull(CustomCode, ''))) = 0 THEN Code
           ELSE CustomCode
         END                                                                                      Code,
         Name,
         ID_Client,
         Name_Client,
         Email,
         ISNULL(Species, '')                                                                      Species,
         ISNULL(Name_Gender, '')                                                                  Name_Gender,
         ContactNumber,
         IsDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile,
         IsNull(IsActive, 0)                                                                      IsActive,
         WaitingStatus_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                                    Microchip
  FROM   dbo.vPatient

GO

ALTER VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                                           AS CreatedBy,
         UM.Name                                           AS LastModifiedBy,
         UC.Name                                           AS Name_CreatedBy,
         UM.Name                                           AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101)                DateString,
         soapType.Name                                     Name_SOAPType,
         patient.Name                                      Name_Patient,
         ISNULL(patient.Comment, '')                       Comment_Patient,
         patient.Name_Client                               Name_Client,
         ISNULL(client.Comment, '')                        Comment_Client,
         approvedUser.Name                                 Name_ApprovedBy,
         canceledUser.Name                                 Name_CanceledBy,
         fs.Name                                           Name_FilingStatus,
         LTRIM(RTRIM(ISNULL(client.ContactNumber, '') + ' '
                     + ISNULL(client.ContactNumber2, ''))) ContactNumber_Client,
         attendingPhysicianEmloyee.Name                    AttendingPhysician_Name_Employee,
         confinement.Code                                  Code_Patient_Confinement,
         confinement.Date                                  Date_Patient_Confinement,
         confinement.DateDischarge                         DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus                       ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus                     Name_FilingStatus_Patient_Confinement,
         REPLACE(H.Diagnosis, CHAR(13), '<br/>')           DiagnosisHTML,
         billfs.Name                                       BillingInvoice_Name_FilingStatus
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.vClient client
                ON patient.ID_Client = client.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement
         LEFT JOIN dbo.tFilingStatus billfs
                ON billfs.ID = H.BillingInvoice_ID_FilingStatus

GO

ALTER PROC [dbo].[pCancelPatient_Wellness] (@IDs_Patient_Wellness typIntList READONLY,
                                            @ID_UserSession       INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatient_Wellness
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Wellness bi
                 INNER JOIN @IDs_Patient_Wellness ids
                         ON bi.ID = ids.ID;

          INSERT @IDs_Patient
          SELECT ID_Patient
          FROM   dbo.tPatient_Wellness hed
                 inner join @IDs_Patient_Wellness ids
                         on hed.ID = ids.ID

          exec [pUpdatePatientsLastVisitedDate]
            @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pGetBillingInvoicePrintReceiptLayout] (@ID_BillingInvoice INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @CustomCode_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0

      SELECT @ID_Company = ID_Company,
             @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @CustomCode_BillingInvoice = CustomCode,
             @TotalItemDiscountAmount_BillingInvoice = TotalItemDiscountAmount,
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px')
      FROM   vCompany
      WHERE  ID = @ID_Company

      DECLARE @style VARCHAR(MAX)= '                    
  body{                    
   margin: 0px;                    
   padding: 0px;                    
   font-family:  arial, sans-serif;                    
   font-weight: normal;                    
   font-style: normal;                    
   font-size: ' + @POSReceiptFontSize
        + ';                    
  }                    
                    
  .logo{                    
                    
   width: 120px;                    
   margin-bottom: 10px;                    
  }                    
                    
  .receipt-container{                    
     width: 100vw;                
  position: relative;                
  left: 50%;                
  right: 50%;                
  margin-left: -50vw;                
  margin-right: -50vw;                
  }                    
                    
  .company-logo{                    
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                    
   text-align: center;                    
   word-wrap: break-word;                    
   margin-bottom: 12px;                    
  }                    
                    
  .company-name{                    
   display: block;                    
   text-align: center;                    
   word-wrap: break-word;                    
  }                    
                    
  .company-address{                    
   display: block;                    
   text-align: center;                    
   word-wrap: break-word;                    
   font-size: 11px                    
  }                    
                    
  .company-contactnum-container{                    
   display: block;                    
   text-align: center;                    
   word-wrap: break-word;                    
   font-size: 11px                    
  }                    
                    
  .company-contactnum{                    
   text-align: center;                    
   word-wrap: break-word;                    
                    
  }                    
                    
  .float-left{                    
   float: left;                    
  }                    
                    
  .float-right{                    
   float: right;                    
  }                    
          
  .hide{   
 display: none;  
  }  
                       
  .clearfix {                    
   overflow: auto;                    
  }                    
                    
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                 
                    
  .display-block{                    
  word-wrap: break-word;                
 display: block;                    
  }                    
                    
  .title{                    
   text-align: center;                    
   word-wrap: break-word;                    
   display: block;                    
   padding-top: 15px;                    
   padding-bottom: 15px;                    
  }                    
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'
      /*Billing Invoice Layout*/
      SET @content = @content
                     + '<div class="company-logo"><img src="'
                     + @ImageLogoLocationFilenamePath_Company
                     + '" class="logo"></div>'
      SET @content = @content + '<center><div class="">'
                     + @Name_Company + '</div>'
      SET @content = @content + '<div>' + @Address_Company + '</div>'
      SET @content = @content + '<div>' + @ContactNumber_Company
                     + '</div></center>'
      SET @content = @content
                     + '<div class="title bold">INVOICE</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('BI #:', @CustomCode_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(Quantity, '#,#0'), FORMAT(Amount, '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Itm Disc Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));

      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      /*END Main Layout */
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO

ALTER PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout] (@ID_PaymentTransaction INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @CustomCode_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0
      DECLARE @ID_BillingInvoice INT = 0

      SELECT @ID_Company = ID_Company,
             @ID_BillingInvoice = ID_BillingInvoice,
             @ID_PaymentMode = ID_PaymentMethod,
             @Code_PaymentTranaction = Code,
             @PayableAmount = PayableAmount,
             @RemainingAmount = RemainingAmount,
             @ChangeAmount = ChangeAmount,
             @CashAmount = CashAmount,
             @ReferenceTransactionNumber = ISNULL(ReferenceTransactionNumber, ''),
             @GCashAmount = GCashAmount,
             @Name_CardType = ISNULL(Name_CardType, ''),
             @CardNumber = ISNULL(CardNumber, ''),
             @CardHolderName = ISNULL(CardHolderName, ''),
             @CardAmount = CardAmount,
             @CheckNumber = CheckNumber,
             @CheckAmount = CheckAmount,
             @PaymentAmount = PaymentAmount
      FROM   vPaymentTransaction
      WHERE  ID = @ID_PaymentTransaction

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px')
      FROM   vCompany
      WHERE  ID = @ID_Company

      SELECT @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @CustomCode_BillingInvoice = CustomCode,
             @TotalItemDiscountAmount_BillingInvoice = TotalItemDiscountAmount,
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      DECLARE @style VARCHAR(MAX)= '                      
  body{                      
   margin: 0px;                      
   padding: 0px;                      
   font-family:  arial, sans-serif;                      
   font-weight: normal;                      
   font-style: normal;                      
   font-size: ' + @POSReceiptFontSize
        + ';                      
  }                      
                      
  .logo{                      
                      
   width: 120px;                      
   margin-bottom: 10px;                      
  }                      
                      
  .receipt-container{                      
     width: 100vw;                  
  position: relative;                  
  left: 50%;                  
  right: 50%;                  
  margin-left: -50vw;                  
  margin-right: -50vw;                  
  }                      
                      
  .company-logo{                      
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                      
   text-align: center;                      
   word-wrap: break-word;                      
   margin-bottom: 12px;                      
  }                      
                      
  .company-name{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
  }                      
                      
  .company-address{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
   font-size: 11px                      
  }                      
                      
  .company-contactnum-container{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
   font-size: 11px                      
  }                      
                      
  .company-contactnum{                      
   text-align: center;                      
   word-wrap: break-word;                      
                      
  }                      
          
  .float-left{                      
   float: left;                      
  }                      
                      
  .float-right{                      
   float: right;                      
  }                      
            
  .hide{     
 display: none;    
  }    
                         
  .clearfix {                      
   overflow: auto;            
  }                      
                      
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                   
                      
  .display-block{                      
  word-wrap: break-word;                  
 display: block;                      
  }                      
                      
  .title{                      
   text-align: center;                      
   word-wrap: break-word;                      
   display: block;                      
   padding-top: 15px;                      
   padding-bottom: 15px;                      
  }                      
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'

      IF( @IsShowHeader = 1 )
        BEGIN
            /*Billing Invoice Layout*/
            IF ( @IsShowPOSReceiptLogo = 1 )
              BEGIN
                  SET @content = @content
                                 + '<div class="company-logo"><img src="'
                                 + @ImageLogoLocationFilenamePath_Company
                                 + '" class="logo"></div>'
              END

            SET @content = @content + '<center><div class="bold">'
                           + @Name_Company + '</div>'
            SET @content = @content + '<div>' + @Address_Company + '</div>'
            SET @content = @content + '<div>' + @ContactNumber_Company
                           + '</div></center>'
        END

      SET @content = @content
                     + '<div class="title bold">INVOICE</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('BI #:', @CustomCode_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(Quantity, '#,#0'), FORMAT(Amount, '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Itm Disc Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));
      /*PAYMENT*/
      SET @content = @content
                     + '<div class="title bold">PAYMENT</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('PT #', @Code_PaymentTranaction);

      /*----------------PAYMENT METHODS--------------------*/
      IF( @ID_PaymentMode = @Cash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Cash Amount', FORMAT(@CashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @GCash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Ref. No.', @ReferenceTransactionNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('G-Cash Amount', FORMAT(@GCashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Type', @Name_CardType);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card #', @CardNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Holder', @CardHolderName);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Amt.', FORMAT(@CardAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @Check_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check No.', @CheckNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check Amt.', FORMAT(@CheckAmount, '#,#0.00'));
        END

      /*----------------PAYMENT METHODS END----------------*/
      SET @content = @content + '<br/>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Balance', FORMAT(@PayableAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Payment', FORMAT(@PaymentAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining', FORMAT(@RemainingAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Change', FORMAT(@ChangeAmount, '#,#0.00'));

      /*END Main Layout */
      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_Wellness] (@ID_CurrentObject VARCHAR(10),
                                                       @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Wellness
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Wellness';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Wellness
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Patient_Wellness TYPINTLIST
      DECLARE @IDs_Patient TYPINTLIST

      INSERT @IDs_Patient_Wellness
      VALUES (@ID_CurrentObject)

      INSERT @IDs_Patient
      SELECT ID_Patient
      FROM   dbo.tPatient_Wellness
      WHERE  ID = @ID_CurrentObject;

      exec [pUpdatePatientsLastVisitedDate]
        @IDs_Patient
  END;

GO

ALTER PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,
                                    @ID_Client              INT = NULL,
                                    @ID_Patient             INT = NULL,
                                    @ID_SOAPType            INT = NULL,
                                    @ID_Patient_Confinement INT = NULL,
                                    @ID_Session             INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription,
             '' AS Patient_SOAP_Treatment;

      DECLARE @FilingStatus_Confined INT = 14
      DECLARE @FilingStatus_Discharged INT = 15
      DECLARE @ID_User INT;
      DECLARE @ID_Company INT;
      DECLARE @GUID_Company VARCHAR(MAX) = ''
      DECLARE @AttendingPhysician_ID_Employee INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @Consultation_ID_SOAPType INT = 1;
      DECLARE @Confinement_ID_SOAPType INT = 2;
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @PrimaryComplaintTemplate VARCHAR(MAX) = ''
        + 'Eyes - Normal/Discharge/Infection/Sclelorosis/Inflamed/Eyelid tumor = '
        + Char(9) + Char(13)
        + 'Ears - Normal/Inflamed/Tumor/Dirty/Painful = '
        + Char(9) + Char(13)
        + 'Nose - Normal/Discharge = ' + Char(9)
        + Char(13)
        + 'Mouth,Teeth,Gums - Normal/Tumors/Gingivitis/Periodontitis/Tartar buildup/Loose teeth/Bite over/Under = '
        + Char(9) + Char(13)
        + 'Coat and skin - Normal/Scaly/Infection/Matted/Pruritus/Hair loss/Mass = '
        + Char(9) + Char(13)
        + 'Muskuloskeletal - Normal/Joint problems/Lameness/Nail Problems/Ligaments = '
        + Char(9) + Char(13)
        + 'Lungs - Normal/Breathing Difficulty/Rapid Respiration/ Tracheal Pinch + - / Congestion / Abn Sound = '
        + Char(9) + Char(13)
        + 'Heart - Normal/Murmur/Arrythmia/Muffled/Fast/Slow = '
        + Char(9) + Char(13)
        + 'GI System - Normal/Excessive Gas/Parasites/Abn Feces/Anorexia = '
        + Char(9) + Char(13)
        + 'Abdomen - Normal/Abnormal Mass/Tense/Painful/Bloated/Fluid/Hernia/Enlarged Organ = '
        + Char(9) + Char(13)
        + 'Urogentital - Normal/Abn Urination/Genital Discharge/Blood Seen/Abn Testicle = '
        + Char(9) + Char(13)
        + 'Neurological - Normal/Eye Reflex/Pain Reflex = '
        + Char(9) + Char(13)
        + 'Lymph Nodes - Normal/Submandubular/Prescapular/Axillary/Popiteal/Inguinal = '
        + Char(9) + Char(13)
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + Char(9) + Char(13)
        + 'Respiratory Rate (brpm): ' + Char(9)
        + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
        + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
        + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
        + 'Lymph Nodes: ' + Char(9) + Char(13)
        + 'Palpebral Reflex: ' + Char(9) + Char(13)
        + 'Temperature: ' + Char(9) + Char(13)
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + Char(9) + Char(13)
        + 'Respiratory Rate (brpm): ' + Char(9)
        + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
        + 'Temperature: ' + Char(9) + Char(13)
        + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
        + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
        + 'Lymph Nodes: ' + Char(9) + Char(13)
        + 'Palpebral Reflex: ' + Char(9) + Char(13)
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + Char(9)
       + Char(13) + 'Notes: ' + Char(9) + Char(13)
       + 'Test Results: ' + Char(9) + Char(13)
       + 'Final Diagnosis: ' + Char(9) + Char(13)
       + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
       + Char(9) + Char(13)
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + Char(9)
       + Char(13) + 'Notes: ' + Char(9) + Char(13)
       + 'Test Results: ' + Char(9) + Char(13)
       + 'Final Diagnosis: ' + Char(9) + Char(13)
       + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
       + Char(9) + Char(13) + 'Category: ' + Char(9)
       + Char(13)
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + Char(9) + Char(13) + '  Wbc= ' + Char(9)
        + Char(13) + '  Lym= ' + Char(9) + Char(13)
        + '  Mon= ' + Char(9) + Char(13) + '  Neu= ' + Char(9)
        + Char(13) + '  Eos= ' + Char(9) + Char(13)
        + '  Bas= ' + Char(9) + Char(13) + '  Rbc= ' + Char(9)
        + Char(13) + '  Hgb= ' + Char(9) + Char(13)
        + '  Hct= ' + Char(9) + Char(13) + '  Mcv= ' + Char(9)
        + Char(13) + '  Mch= ' + Char(9) + Char(13)
        + '  Mchc ' + Char(9) + Char(13) + '  Plt= ' + Char(9)
        + Char(13) + '  Mpv= ' + Char(9) + Char(13) + Char(9)
        + Char(13) + 'Blood Chem: ' + Char(9) + Char(13)
        + '  Alt= ' + Char(9) + Char(13) + '  Alp= ' + Char(9)
        + Char(13) + '  Alb= ' + Char(9) + Char(13)
        + '  Amy= ' + Char(9) + Char(13) + '  Tbil= '
        + Char(9) + Char(13) + '  Bun= ' + Char(9) + Char(13)
        + '  Crea= ' + Char(9) + Char(13) + '  Ca= ' + Char(9)
        + Char(13) + '  Phos= ' + Char(9) + Char(13)
        + '  Glu= ' + Char(9) + Char(13) + '  Na= ' + Char(9)
        + Char(13) + '  K= ' + Char(9) + Char(13) + '  TP= '
        + Char(9) + Char(13) + '  Glob= ' + Char(9) + Char(13)
        + Char(9) + Char(13) + 'Microscopic Exam: '
        + Char(9) + Char(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      SELECT @ID_Company = ID_Company,
             @GUID_Company = c.Guid
      FROM   vUser u
             inner join tCompany c
                     on c.ID = u.ID_Company
      WHERE  u.ID = @ID_User

      DECLARE @PetAdventure_Company_GUID VARCHAR(MAX) = '8F73F77E-E09F-49F9-BE73-DA923CA448F0'
      DECLARE @Compostela_ID_Company INT = 65
      DECLARE @DandCVeterinaryClinic_ID_Company INT = 63
      DECLARE @GarciaVeterinaryClinicVeterinaryClinic_ID_Company INT = 75
      DECLARE @PetlinkGuiguintoID_Company INT = 29
      DECLARE @VenVet_ID_Company INT = 12
      DECLARE @FDMD_ID_Company INT = 70

      IF( @GUID_Company = @PetAdventure_Company_GUID )
        BEGIN
            SET @PrimaryComplaintTemplate = '' + 'Appetite - ' + Char(9) + Char(13)
                                            + 'Vomit/F - ' + Char(9) + Char(13) + 'Vomit/C - '
                                            + Char(9) + Char(13) + 'Diarrhea/F - ' + Char(9)
                                            + Char(13) + 'Diarrhea/C - ' + Char(9) + Char(13)
                                            + 'Defacation - ' + Char(9) + Char(13)
                                            + 'Urination - ' + Char(9) + Char(13) + 'Diet - '
                                            + Char(9) + Char(13) + 'Last Vacc - ' + Char(9)
                                            + Char(13) + 'Last Deworm - ' + Char(9) + Char(13)
                                            + 'Environment - ' + Char(9) + Char(13)
                                            + 'Meds given prior - ' + Char(9) + Char(13)
        END

      IF( @ID_Company = @FDMD_ID_Company )
        BEGIN
            SET @LaboratoryTemplate = 'PID=  ' + Char(9) + Char(13) + 'SID=  ' + Char(9)
                                      + Char(13) + 'Needle=  ' + Char(9) + Char(13)
                                      + 'Type=  ' + Char(9) + Char(13) + 'WBC=  ' + Char(9)
                                      + Char(13) + 'LYM=  ' + Char(9) + Char(13) + 'MON=  '
                                      + Char(9) + Char(13) + 'NEU=  ' + Char(9) + Char(13)
                                      + 'EOS=  ' + Char(9) + Char(13) + 'BAS=  ' + Char(9)
                                      + Char(13) + 'LYM%=  ' + Char(9) + Char(13)
                                      + 'MON%=  ' + Char(9) + Char(13) + 'NEU%=  ' + Char(9)
                                      + Char(13) + 'EOS%=  ' + Char(9) + Char(13)
                                      + 'BAS%=  ' + Char(9) + Char(13) + 'RBC=  ' + Char(9)
                                      + Char(13) + 'HGB=  ' + Char(9) + Char(13) + 'HCT=  '
                                      + Char(9) + Char(13) + 'MCV=  ' + Char(9) + Char(13)
                                      + 'MCH=  ' + Char(9) + Char(13) + 'MCHC=  ' + Char(9)
                                      + Char(13) + 'RDWC=  ' + Char(9) + Char(13)
                                      + 'RDWS=  ' + Char(9) + Char(13) + 'PLT=  ' + Char(9)
                                      + Char(13) + 'MPV PCT=  ' + Char(9) + Char(13)
                                      + 'PDWc PDWS=  ' + Char(9) + Char(13)
            SET @DiagnosisTemplate ='Differential Diagnosis: ' + Char(9)
                                    + Char(13) + 'Notes: ' + Char(9) + Char(13)
                                    + 'Test Results: ' + Char(9) + Char(13)
                                    + 'Final Diagnosis: ' + Char(9) + Char(13)
                                    + 'Prognosis: ' + Char(9) + Char(13)
                                    + 'Procedure done: ' + Char(9) + Char(13)
        END

      IF( @ID_Company = @VenVet_ID_Company )
        BEGIN
            SET @PrimaryComplaintTemplate= '' + 'Eyes = ' + Char(9) + Char(13) + 'Ears = '
                                           + Char(9) + Char(13) + 'Nose = ' + Char(9) + Char(13)
                                           + 'Mouth,Teeth,Gums = ' + Char(9) + Char(13)
                                           + 'Coat and skin = ' + Char(9) + Char(13)
                                           + 'Muskuloskeletal = ' + Char(9) + Char(13)
                                           + 'Lungs = ' + Char(9) + Char(13) + 'Heart = '
                                           + Char(9) + Char(13) + 'GI System = ' + Char(9)
                                           + Char(13) + 'Abdomen = ' + Char(9) + Char(13)
                                           + 'Urogentital = ' + Char(9) + Char(13)
                                           + 'Neurological = ' + Char(9) + Char(13)
                                           + 'Lymph Nodes = ' + Char(9) + Char(13)
        END

      SELECT @AttendingPhysician_ID_Employee = ID_Employee
      FROM   tUser _user
             inner join vAttendingVeterinarian _attending
                     on _user.ID_Employee = _attending.ID
      WHERE  _user.ID = @ID_User

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @DandCVeterinaryClinic_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 345
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @GarciaVeterinaryClinicVeterinaryClinic_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 426
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @PetlinkGuiguintoID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 216
        END

      /* Patient Confinement */
      IF(SELECT Count(*)
         FROM   tPatient_Confinement
         WHERE  ID_Patient_SOAP = @ID
                AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )) > 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID
            FROM   tPatient_Confinement
            WHERE  ID_Patient_SOAP = @ID
                   AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )
        END

      IF( IsNull(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            DECLARE @confinedPatientCount INT = 0

            SET @ID_SOAPType = @Confinement_ID_SOAPType

            SELECT @ID_Client = hed.ID_Client,
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus
            FROM   tPatient_Confinement hed
            WHERE  hed.ID = @ID_Patient_Confinement

            SELECT @confinedPatientCount = COUNT(*)
            FROM   tPatient_Confinement_Patient confPatient
            WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
            GROUP  BY confPatient.ID_Patient_Confinement

            if( @confinedPatientCount = 1 )
              BEGIN
                  SELECT @ID_Patient = ID_Patient
                  FROM   tPatient_Confinement_Patient confPatient
                  WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
              END
        END

      /* Patient Confinement END*/
      IF IsNull(@ID_Patient, 0) <> 0
        BEGIN
            SELECT @IsDeceased = IsNull(IsDeceased, 0),
                   @ID_Client = ID_Client
            FROM   tPatient
            WHERE  ID = @ID_Patient;
        END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT ImageRowIndex,
             RowIndex,
             ImageNo,
             FilePath,
             Remark
      FROM   dbo.fGetPatient_Soap_LaboratoryImages(@ID)

      DECLARE @IsLastConfinmentSOAP BIT = 0
      DECLARE @maxDate DATETIME

      SELECT @maxDate = MaxDate
      FROM   vPatient_Confinement_MaxSOAP
      WHERE  ID_Patient_SOAP = @ID

      IF( @maxDate IS NOT NULL
          AND @Patient_Confinement_ID_FilingStatus = @FilingStatus_Discharged )
        SET @IsLastConfinmentSOAP = 1
      ELSE
        SET @IsLastConfinmentSOAP = 0

      IF ( @ID = -1 )
        BEGIN
            SET @ID_SOAPType = IsNull(@ID_SOAPType, @Consultation_ID_SOAPType)

            SELECT H.*,
                   patient.Name                               Name_Patient,
                   client.Name                                Name_Client,
                   fs.Name                                    Name_FilingStatus,
                   soapType.Name                              Name_SOAPType,
                   AttendingPhysician.Name                    AttendingPhysician_Name_Employee,
                   confinement.Code                           Code_Patient_Confinement,
                   confinement.Date                           Date_Patient_Confinement,
                   confinement.DateDischarge                  DateDischarge_Patient_Confinement,
                   CASE
                     WHEN @Patient_Confinement_ID_FilingStatus = 0 THEN confinement.ID_FilingStatus
                     ELSE @Patient_Confinement_ID_FilingStatus
                   END,
                   confinement.ID_FilingStatus                ID_FilingStatus_Patient_Confinement,
                   confinement.Name_FilingStatus              Name_FilingStatus_Patient_Confinement,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient,
                   ISNULL(patient.Comment, '')                Comment_Patient,
                   ISNULL(client.Comment, '')                 Comment_Client
            FROM   (SELECT NULL                                 AS [_],
                           -1                                   AS [ID],
                           '-New-'                              AS [Code],
                           NULL                                 AS [Name],
                           1                                    AS [IsActive],
                           NULL                                 AS [ID_Company],
                           NULL                                 AS [Comment],
                           NULL                                 AS [DateCreated],
                           NULL                                 AS [DateModified],
                           @ID_User                             AS [ID_CreatedBy],
                           NULL                                 AS [ID_LastModifiedBy],
                           @ID_Client                           AS ID_Client,
                           @ID_Patient                          AS ID_Patient,
                           @AttendingPhysician_ID_Employee      AS AttendingPhysician_ID_Employee,
                           @ID_Patient_Confinement              AS ID_Patient_Confinement,
                           GetDate()                            Date,
                           @FILED_ID_FilingStatus               ID_FilingStatus,
                           @IsDeceased                          IsDeceased,
                           @ID_SOAPType                         ID_SOAPType,
                           @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,
                           @PrimaryComplaintTemplate            PrimaryComplaintTemplate,
                           @ObjectiveTemplate                   ObjectiveTemplate,
                           @AssessmentTemplate                  AssessmentTemplate,
                           @LaboratoryTemplate                  LaboratoryTemplate,
                           @ClinicalExaminationTemplate         ClinicalExaminationTemplate,
                           @DiagnosisTemplate                   DiagnosisTemplate,
                           @IsLastConfinmentSOAP                IsLastConfinmentSOAP) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType
                   LEFT JOIN vPatient_Confinement confinement
                          ON confinement.ID = H.ID_Patient_Confinement
                   LEFT JOIN tEmployee AttendingPhysician
                          ON AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @Patient_Confinement_ID_FilingStatus       Patient_Confinement_ID_FilingStatus,
                   patient.IsDeceased,
                   @PrimaryComplaintTemplate                  PrimaryComplaintTemplate,
                   @ObjectiveTemplate                         ObjectiveTemplate,
                   @AssessmentTemplate                        AssessmentTemplate,
                   @LaboratoryTemplate                        LaboratoryTemplate,
                   @ClinicalExaminationTemplate               ClinicalExaminationTemplate,
                   @DiagnosisTemplate                         DiagnosisTemplate,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient,
                   @IsLastConfinmentSOAP                      IsLastConfinmentSOAP
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          ON h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID

      SELECT *
      FROM   dbo.vPatient_SOAP_Treatment
      WHERE  ID_Patient_SOAP = @ID
  END;

GO

ALTER PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @tbl TABLE
        (
           ID_Patient      INT,
           DateLastVisited DATETIME
        )
      DECLARE @tblLastVisited TABLE
        (
           ID_Patient      INT,
           DateLastVisited DATETIME
        )

      INSERT @tbl
             (ID_Patient)
      SELECT ID
      FROM   @IDs_Patient

      INSERT @tblLastVisited
             (ID_Patient,
              DateLastVisited)
      SELECT soap.ID_Patient,
             MAX(Date) DateLastVisited
      FROM   tPatient_SOAP soap
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = soap.ID_Patient
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
      GROUP  BY soap.ID_Patient

      INSERT @tblLastVisited
             (ID_Patient,
              DateLastVisited)
      SELECT soap.ID_Patient,
             MAX(Date) DateLastVisited
      FROM   tPatient_Wellness soap
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = soap.ID_Patient
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
      GROUP  BY soap.ID_Patient

      Update @tbl
      SET    DateLastVisited = tblLastVisited.DateLastVisited
      FROM   @tbl tbl
             INNER JOIN (SELECT ID_Patient,
                                MAX(DateLastVisited) DateLastVisited
                         FROM   @tblLastVisited
                         GROUP  BY ID_Patient) tblLastVisited
                     on tbl.ID_Patient = tblLastVisited.ID_Patient

      UPDATE tPatient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tPatient patient
             INNER JOIN @tbl tbl
                     ON tbl.ID_Patient = patient.ID

      Update tClient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tClient client
             INNER JOIN (SELECT ID_Client,
                                MAX(patient.DateLastVisited) DateLastVisited
                         FROM   tPatient patient
                                INNER JOIN @tbl tbl
                                        ON tbl.ID_Patient = patient.ID
                         GROUP  BY ID_Client) tbl
                     ON client.ID = tbl.ID_Client
  END

GO 
