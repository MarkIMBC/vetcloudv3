IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tCompanyInfo'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tCompanyInfo', 1, 0, NULL
END

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tCompany'
    AND COLUMN_NAME = 'Email'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tCompany'
                         ,'Email'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'ClinicalExamination'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'ClinicalExamination'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'Diagnosis'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'Diagnosis'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'Interpretation'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'Interpretation'
                         , 1
END;

GO


IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'Treatment'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'Treatment'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'ClientCommunication'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'ClientCommunication'
                         , 1
END;

GO

ALTER TABLE tPatient_SOAP
ALTER COLUMN ClinicalExamination VARCHAR(3000);

ALTER TABLE tPatient_SOAP
ALTER COLUMN Diagnosis VARCHAR(3000);

ALTER TABLE tPatient_SOAP
ALTER COLUMN Treatment VARCHAR(3000);

ALTER TABLE tPatient_SOAP
ALTER COLUMN ClientCommunication VARCHAR(3000);

GO

exec _pRefreshAllViews

GO

CREATE OR ALTER VIEW dbo.vAttendingVeterinarian
AS

	SELECT hed.ID
		  ,hed.Name
		  ,hed.ID_Company
		  ,hed.Name_Position
	FROM vEmployee hed
	WHERE
		hed.ID_Position IN (10, 4, 5) AND
		ISNULL(hed.IsActive, 0) = 1
GO

CREATE OR ALTER VIEW [dbo].[vzInventoryDetailReport]
AS
SELECT hed.*,
       item.Name Name_Item,
       status.Name Name_FilingStatus,
       CONVERT(VARCHAR, hed.Date, 9) DateString,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   invtStatus.Name Name_InventoryStatus,
	   CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	   CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	   CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
FROM dbo.tInventoryTrail hed
    LEFT JOIN dbo.tItem item
        ON item.ID = hed.ID_Item
    LEFT JOIN dbo.tFilingStatus status
        ON status.ID = hed.ID_FilingStatus
    LEFT JOIN dbo.vCompany company
        ON company.ID = item.ID_Company
	LEFT JOIN dbo.tInventoryStatus invtStatus
		ON invtStatus.ID = item.ID_InventoryStatus
GO

CREATE OR ALTER VIEW [dbo].[vzInventorySummaryReport]
AS
SELECT hed.ID_Item ID,
       LTRIM(RTRIM(hed.Name_Item)) Name_Item,
       SUM(hed.Quantity) TotalQuantity,
       NULL DateExpired,
       NULL BatchNo,
       MAX(   CASE
                  WHEN hed.Quantity > 0 THEN
                      Date
                  ELSE
                      NULL
              END
          ) DateLastIn,
       MAX(   CASE
                  WHEN hed.Quantity < 0 THEN
                      Date
                  ELSE
                      NULL
              END
          ) DateLastOut,
       item.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   CASE
			WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN '(' + dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired' + ')', 'Expired')
		ELSE ''
			END RemainingBeforeExpired,
		DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
		invtStatus.Name Name_InventoryStatus,
		CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
		CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
		CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
FROM dbo.vInventoryTrail hed
LEFT JOIN dbo.tItem item 
	ON item.ID = hed.ID_Item
LEFT JOIN dbo.vCompany company
    ON company.ID = item.ID_Company
LEFT JOIN dbo.tInventoryStatus invtStatus
    ON invtStatus.ID = item.ID_InventoryStatus
GROUP BY ID_Item,
         Name_Item,
         company.ImageLogoLocationFilenamePath,
         company.Name,
		 OtherInfo_DateExpiration,
         company.Address,
         item.ID_Company,
		 invtStatus.Name,
		 company.ContactNumber,
		 company.Email
GO

CREATE OR ALTER    VIEW [dbo].[vzPaymentTransactionSummaryReport]
as

	SELECT	biHed.ID ID_BillingInvoice,
			pHed.ID ID_PaymentTransaction,
			biHed.Code Code_BillingInvoice,
			biHed.Date Date_BillingInvoice,
			biHed.TotalAmount BalanceAmount ,
			pHed.Code Code_PaymentTransaction,
			pHed.Date Date_PaymentTransaction,
			client.Name Name_Client,
			patient.Name Name_Patient,
			biHed.ID_Company,
			pHed.ID_PaymentMethod,
			payMethod.Name Name_PaymentMethod,
			phed.PayableAmount,
			CASE WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount ELSE phed.PaymentAmount END PaymentAmount,
			pHed.RemainingAmount,
			company.ImageLogoLocationFilenamePath,
			company.Name Name_Company,
			company.Address Address_Company,
			CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM    tPaymentTransaction pHed 
	INNER JOIN tBillingInvoice biHed 
		on biHed.ID = pHed.ID_BillingInvoice
	LEFT JOIN tClient client 
		ON client.ID = biHed.ID_Client
	LEFT JOIN tPatient patient 
		ON patient.ID = biHed.ID_Patient
	LEFT JOIN tPaymentMethod payMethod
		ON payMethod.ID = pHed.ID_PaymentMethod
	LEFT JOIN dbo.vCompany company
		ON company.ID = biHed.ID_Company
	WHERE biHed.Payment_ID_FilingStatus IN (11, 12)
GO

CREATE OR ALTER   VIEW [dbo].[vzBillingInvoicePaidListReport]
as
	SELECT biHed.ID ID_BillingInvoice
			,biHed.Code Code_BillingInvoice
			,biHed.Date Date_BillingInvoice
			,piHed.Date Date_PaymentTransaction
			,biHed.Name_Client
			,bihed.Name_Patient
			,biHed.Payment_ID_FilingStatus
			,biHed.Payment_Name_FilingStatus
			,biHed.ID_Company
			,ISNULL(biHed.TotalAmount,0) TotalAmount
			,CASE WHEN ISNULL(piHed.ChangeAmount, 0) > 0 THEN 
					piHed.PayableAmount 
				ELSE 
					piHed.PaymentAmount 
				END PaymentAmount
			,company.ImageLogoLocationFilenamePath
			,company.Name Name_Company
			,company.Address Address_Company,
			CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM  vBillingInvoice biHed 
	LEFT JOIN tPaymentTransaction piHed
		on piHed.ID_BillingInvoice = biHed.ID
	LEFT JOIN dbo.vCompany company 
	 ON company.ID = biHed.ID_Company
	WHERE
		biHed.Payment_ID_FilingStatus IN (11, 12) AND 
		biHed.ID_FilingStatus IN (3) AND 
		piHed.ID_FilingStatus IN (3)
	GROUP BY
		 biHed.ID
		,biHed.Code
		,biHed.Date
		,piHed.Date
		,piHed.ID_FilingStatus
		,biHed.Name_Client
		,bihed.Name_Patient
		,biHed.Payment_ID_FilingStatus
		,biHed.Payment_Name_FilingStatus
		,biHed.ID_Company
		,biHed.TotalAmount
		,piHed.ChangeAmount
		,piHed.PayableAmount
		,piHed.PaymentAmount
		,company.ImageLogoLocationFilenamePath
		,company.Name 
		,company.Address
		,company.ContactNumber
		,company.Email
UNION ALL
	SELECT   biHed.ID ID_BillingInvoice
			,biHed.Code Code_BillingInvoice
			,biHed.Date Date_BillingInvoice
			,NULL Date_PaymentTransaction
			,biHed.Name_Client
			,bihed.Name_Patient
			,biHed.Payment_ID_FilingStatus
			,biHed.Payment_Name_FilingStatus
			,biHed.ID_Company
			,ISNULL(biHed.TotalAmount,0) TotalAmount
			,0.00 PaymentAmount
			,company.ImageLogoLocationFilenamePath
			,company.Name Name_Company
			,company.Address Address_Company,
			CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM  vBillingInvoice biHed 
	LEFT JOIN dbo.vCompany company 
	 ON company.ID = biHed.ID_Company
	WHERE
		biHed.Payment_ID_FilingStatus IN (2) AND 
		biHed.ID_FilingStatus IN (3) 
	GROUP BY
		 biHed.ID
		,biHed.Code
		,biHed.Date
		,biHed.Name_Client
		,bihed.Name_Patient
		,biHed.Payment_ID_FilingStatus
		,biHed.Payment_Name_FilingStatus
		,biHed.ID_Company
		,biHed.TotalAmount
		,company.ImageLogoLocationFilenamePath
		,company.Name 
		,company.Address
		,company.ContactNumber
		,company.Email
GO

CREATE OR ALTER VIEW [dbo].[vzPOSSummary]
AS
SELECT biDetail.ID_Item,
       biDetail.Name_Item,
       biDetail.UnitPrice,
       biDetail.Amount,
	   biDetail.Quantity,
	   biDetail.ID_BillingInvoice,
       bi.Date,
	   bi.DiscountAmount,
       bi.Date Date1,
       item.ID_ItemType,
       item.Name_ItemType,
       bi.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	   CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	   CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
FROM dbo.vBillingInvoice_Detail biDetail
    LEFT JOIN dbo.vBillingInvoice bi
        ON bi.ID = biDetail.ID_BillingInvoice
    LEFT JOIN dbo.vItem item
        ON item.ID = biDetail.ID_Item
    LEFT JOIN dbo.vCompany company
        ON company.ID = bi.ID_Company
WHERE bi.Payment_ID_FilingStatus IN ( 2, 11, 12 )
      AND bi.ID_FilingStatus = 3
      AND Name_Item IS NOT NULL;
GO

CREATE OR ALTER VIEW [dbo].[vzPatientSOAP]
AS
	SELECT
	  patientSOAP.ID
	 ,patientSOAP.Code
	 ,patientSOAP.Name_Client
	 ,patientSOAP.Name_Patient
	 ,patientSOAP.Date
	 ,patientSOAP.Comment
	 ,patientSOAP.Name_CreatedBy
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.History, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') History
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClinicalExamination, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClinicalExamination
	 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Diagnosis, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Diagnosis
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Treatment, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Treatment
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Prescription, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Prescription
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClientCommunication, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClientCommunication
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Subjective
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Objective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
	 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
	 ,patientSOAP.ID_Company
	 ,patientSOAP.Name_FilingStatus
	 ,company.ImageLogoLocationFilenamePath
	 ,company.Name Name_Company
	 ,company.Address Address_Company
	 ,company.ContactNumber ContactNumber_Company
	 ,CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	  CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	  CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM dbo.vPatient_SOAP patientSOAP
	LEFT JOIN dbo.vCompany company
	  ON company.ID = patientSOAP.ID_Company;

GO

CREATE OR ALTER FUNCTION [dbo].[fGetAge] (@dateBirth DATETIME, @dateDateStart DateTime = NULL, @InvalidDateCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = ''

  DECLARE @date DATETIME
         ,@tmpdate DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @dateBirth IS NULL
    RETURN ''

  IF @dateDateStart IS NULL
	SET @dateDateStart = getdate()	

  SELECT
    @date = @dateBirth

  SELECT
    @tmpdate = @date

  SELECT
    @years = DATEDIFF(yy, @tmpdate, @dateDateStart) -
    CASE
      WHEN (MONTH(@date) > MONTH(@dateDateStart)) OR
        (MONTH(@date) = MONTH(@dateDateStart) AND
        DAY(@date) > DAY(@dateDateStart)) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(yy, @years, @tmpdate)
  SELECT
    @months = DATEDIFF(m, @tmpdate, @dateDateStart) -
    CASE
      WHEN DAY(@date) > DAY(@dateDateStart) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(m, @months, @tmpdate)
  SELECT
    @days = DATEDIFF(d, @tmpdate, @dateDateStart)
	 
  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 1 THEN 's'
      ELSE ''
    END + ' '
  IF (@years >= 0 AND @months > 0)
    SET @result = @result +  CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 1 THEN 's'
      ELSE ''
    END + ' '
  IF (@years >= 0 AND @months >= 0 AND @days > 0)
    SET @result = @result + CONVERT(VARCHAR(MAX), @days) + ' day' +
    CASE
      WHEN @days > 1 THEN 's'
      ELSE ''
    END 

	if(LEN(@result) = 0)
		set @result = @InvalidDateCaption
	ELSE
		set @result = @result + ' old'

   RETURN @result
END

GO

CREATE OR ALTER PROC [dbo].[pGetPatient]    
    @ID INT = -1,    
    @ID_Client INT = NULL,    
    @ID_Session INT = NULL    
AS    
BEGIN    
    SELECT '_',    
           '' AS Patient_DentalExamination,    
           '' AS Patient_History,    
           '' AS Patient_SOAP_RegularConsoltation,    
           '' AS Patient_SOAP;    
    
    DECLARE @Approved_ID_FIlingStatus INT = 3;    
    DECLARE @Cancelled_ID_FIlingStatus INT = 4;    
    
    DECLARE @ID_User INT,    
            @ID_Employee INT,    
            @ID_Company INT,    
            @ID_Warehouse INT;    
    
    SELECT @ID_User = ID_User,    
           @ID_Warehouse = ID_Warehouse    
    FROM tUserSession    
    WHERE ID = @ID_Session;    
    
    SELECT @ID_Employee = ID_Employee    
    FROM dbo.tUser    
    WHERE ID = @ID_User;    
    
    SELECT @ID_Company = ID_Company    
    FROM dbo.tEmployee    
    WHERE ID = @ID_Employee;    
    
    IF (@ID = -1)    
    BEGIN    
        SELECT H.*,    
               client.Name Name_Client    
        FROM    
        (    
            SELECT NULL AS [_],    
                   -1 AS [ID],    
                   NULL AS [Code],    
                   NULL AS [Name],    
                   1 AS [IsActive],    
                   NULL AS [Comment],    
                   NULL AS [DateCreated],    
                   NULL AS [DateModified],    
                   NULL AS [ID_CreatedBy],    
                   NULL AS [ID_LastModifiedBy],    
                   @ID_Client AS ID_Client,
				   0 IsDeceased
        ) H    
            LEFT JOIN dbo.tUser UC    
                ON H.ID_CreatedBy = UC.ID    
            LEFT JOIN dbo.tUser UM    
                ON H.ID_LastModifiedBy = UM.ID    
            LEFT JOIN dbo.tClient client    
                ON client.ID = H.ID_Client;    
    END;    
    ELSE    
    BEGIN    
        SELECT H.*, dbo.fGetAge(h.DateBirth, case WHEN ISNULL(h.IsDeceased,0) = 1 then  h.DateDeceased ELSE NULL END ,'N/A') Age    
        FROM dbo.vPatient H    
        WHERE H.ID = @ID;    
    END;    
    
    SELECT *    
    FROM dbo.vPatient_DentalExamination    
    WHERE ID_Patient = @ID;    
    
    SELECT *    
    FROM dbo.vPatient_History    
    WHERE ID_Patient = @ID    
    ORDER BY Date DESC;    
    
    SELECT *    
    FROM dbo.tPatient_SOAP_RegularConsoltation    
    WHERE ID_Patient = @ID;    
    
    SELECT 
		ID, 
		DateString, 
		Code, 
		Name_SOAPType,  
		CASE WHEN LEN(History) >= 200 THEN LTRIM(RTRIM(LEFT(History, 200 - 3))) + '...' ELSE History END History, 
		Subjective,
		Name_FilingStatus   
    FROM dbo.vPatient_SOAP    
    WHERE ID_Patient = @ID    
      AND ID_FilingStatus NOT IN (@Cancelled_ID_FIlingStatus)    
    ORDER BY Date DESC;    
    
END;   

GO

ALTER PROCEDURE [dbo].[pGetPatient_SOAP] @ID INT = -1,
	@ID_Patient INT = NULL,
	@ID_SOAPType INT = NULL,
	@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'
   ,'' AS LabImages
   ,'' AS Patient_SOAP_Plan;

  DECLARE @ID_User INT;
  DECLARE @ID_Warehouse INT;
  DECLARE @FILED_ID_FilingStatus INT = 1;
  DECLARE @IsDeceased BIT = 1;

  DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): '+ CHAR(9) + CHAR(13) + 
											'Respiratory Rate (brpm): '+ CHAR(9) + CHAR(13) + 	
											'Weight (kg): '+ CHAR(9) + CHAR(13) + 	
											'Length (cm): '+ CHAR(9) + CHAR(13) + 
											'CRT: '+ CHAR(9) + CHAR(13) + 
											'BCS: '+ CHAR(9) + CHAR(13) + 	
											'Lymph Nodes: '+ CHAR(9) + CHAR(13) + 
											'Palpebral Reflex: '+ CHAR(9) + CHAR(13) + 
											'Temperature: '+ CHAR(9) + CHAR(13)

  DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): '+ CHAR(9) + CHAR(13) + 
											'Respiratory Rate (brpm): '+ CHAR(9) + CHAR(13) + 	
											'Weight (kg): '+ CHAR(9) + CHAR(13) + 	
											'Length (cm): '+ CHAR(9) + CHAR(13) + 
											'CRT: '+ CHAR(9) + CHAR(13) + 
											'BCS: '+ CHAR(9) + CHAR(13) + 	
											'Lymph Nodes: '+ CHAR(9) + CHAR(13) + 
											'Palpebral Reflex: '+ CHAR(9) + CHAR(13) + 
											'Temperature: '+ CHAR(9) + CHAR(13)

  DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Notes: '+ CHAR(9) + CHAR(13) + 		
											'Test Results: '+ CHAR(9) + CHAR(13) + 		
											'Final Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Prognosis: '+ CHAR(9) + CHAR(13) + 	
											'Category: '+ CHAR(9) + CHAR(13) 

  DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Notes: '+ CHAR(9) + CHAR(13) + 		
											'Test Results: '+ CHAR(9) + CHAR(13) + 		
											'Final Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Prognosis: '+ CHAR(9) + CHAR(13) + 	
											'Category: '+ CHAR(9) + CHAR(13) 

  DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: '+ CHAR(9) + CHAR(13) + 	
											'  Wbc= '+ CHAR(9) + CHAR(13) + 	
											'  Lym= '+ CHAR(9) + CHAR(13) + 	
											'  Mon= '+ CHAR(9) + CHAR(13) + 	
											'  Neu= '+ CHAR(9) + CHAR(13) + 	
											'  Eos= '+ CHAR(9) + CHAR(13) + 	
											'  Bas= '+ CHAR(9) + CHAR(13) + 	
											'  Rbc= '+ CHAR(9) + CHAR(13) + 	
											'  Hgb= '+ CHAR(9) + CHAR(13) + 	
											'  Hct= '+ CHAR(9) + CHAR(13) + 	
											'  Mcv= '+ CHAR(9) + CHAR(13) + 	
											'  Mch= '+ CHAR(9) + CHAR(13) + 	
											'  Mchc '+ CHAR(9) + CHAR(13) + 
											'  Plt= '+ CHAR(9) + CHAR(13) + 	
											'  Mpv= '+ CHAR(9) + CHAR(13) + CHAR(9) + CHAR(13) +	
											'Blood Chem: '+ CHAR(9) + CHAR(13) + 	
											'  Alt= '+ CHAR(9) + CHAR(13) + 	
											'  Alp= '+ CHAR(9) + CHAR(13) + 	
											'  Alb= '+ CHAR(9) + CHAR(13) + 	
											'  Amy= '+ CHAR(9) + CHAR(13) + 	
											'  Tbil= '+ CHAR(9) + CHAR(13) + 	
											'  Bun= '+ CHAR(9) + CHAR(13) + 	
											'  Crea= '+ CHAR(9) + CHAR(13) + 	
											'  Ca= '+ CHAR(9) + CHAR(13) + 	
											'  Phos= '+ CHAR(9) + CHAR(13) + 	
											'  Glu= '+ CHAR(9) + CHAR(13) + 	
											'  Na= '+ CHAR(9) + CHAR(13) + 	
											'  K= '+ CHAR(9) + CHAR(13) + 	
											'  TP= '+ CHAR(9) + CHAR(13) + 	
											'  Glob= '+ CHAR(9) + CHAR(13) +  CHAR(9) + CHAR(13) + 	 	
											'Microscopic Exam: '+ CHAR(9) + CHAR(13) 

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM dbo.tUserSession
  WHERE ID = @ID_Session;

  SELECT  @IsDeceased = Isnull(IsDeceased,0)
  FROM tPatient WHERE ID = @ID_Patient;
  
  DECLARE @LabImage TABLE (
    ImageRowIndex INT
   ,RowIndex INT
   ,ImageNo VARCHAR(MAX)
   ,FilePath VARCHAR(MAX)
   ,Remark VARCHAR(MAX)
  );

  INSERT @LabImage (ImageRowIndex, RowIndex, ImageNo, FilePath, Remark)
  SELECT
    c.ImageRowIndex
   ,ROW_NUMBER() OVER (ORDER BY ImageNo) AS RowIndex
   ,c.ImageNo
   ,FilePath
   ,c.Remark
  FROM tPatient_SOAP ps
  CROSS APPLY (SELECT
      '01'
     ,LabImageRowIndex01
     ,LabImageFilePath01
     ,LabImageRemark01
    UNION ALL
    SELECT
      '02'
     ,LabImageRowIndex02
     ,LabImageFilePath02
     ,LabImageRemark02
    UNION ALL
    SELECT
      '03'
     ,LabImageRowIndex03
     ,LabImageFilePath03
     ,LabImageRemark03
    UNION ALL
    SELECT
      '04'
     ,LabImageRowIndex04
     ,LabImageFilePath04
     ,LabImageRemark04
    UNION ALL
    SELECT
      '05'
     ,LabImageRowIndex05
     ,LabImageFilePath05
     ,LabImageRemark05
    UNION ALL
    SELECT
      '06'
     ,LabImageRowIndex06
     ,LabImageFilePath06
     ,LabImageRemark06
    UNION ALL
    SELECT
      '07'
     ,LabImageRowIndex07
     ,LabImageFilePath07
     ,LabImageRemark07
    UNION ALL
    SELECT
      '08'
     ,LabImageRowIndex08
     ,LabImageFilePath08
     ,LabImageRemark08
    UNION ALL
    SELECT
      '09'
     ,LabImageRowIndex09
     ,LabImageFilePath09
     ,LabImageRemark09
    UNION ALL
    SELECT
      '10'
     ,LabImageRowIndex10
     ,LabImageFilePath10
     ,LabImageRemark10
    UNION ALL
    SELECT
      '11'
     ,LabImageRowIndex11
     ,LabImageFilePath11
     ,LabImageRemark11
    UNION ALL
    SELECT
      '12'
     ,LabImageRowIndex12
     ,LabImageFilePath12
     ,LabImageRemark12
    UNION ALL
    SELECT
      '13'
     ,LabImageRowIndex13
     ,LabImageFilePath13
     ,LabImageRemark13
    UNION ALL
    SELECT
      '14'
     ,LabImageRowIndex14
     ,LabImageFilePath14
     ,LabImageRemark14
    UNION ALL
    SELECT
      '15'
     ,LabImageRowIndex15
     ,LabImageFilePath15
     ,LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
  WHERE ps.ID = @ID
        AND ISNULL(FilePath, '') <> ''
  ORDER BY c.ImageRowIndex ASC;


  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,patient.Name Name_Patient
     ,patient.Name_Client
     ,fs.Name Name_FilingStatus
     ,soapType.Name Name_SOAPType
    FROM (
		SELECT
			NULL AS [_]
		   ,-1 AS [ID]
		   ,'-New-' AS [Code]
		   ,NULL AS [Name]
		   ,1 AS [IsActive]
		   ,NULL AS [ID_Company]
		   ,NULL AS [Comment]
		   ,NULL AS [DateCreated]
		   ,NULL AS [DateModified]
		   ,@ID_User AS [ID_CreatedBy]
		   ,NULL AS [ID_LastModifiedBy]
		   ,@ID_Patient AS ID_Patient
		   ,GETDATE() Date
		   ,@FILED_ID_FilingStatus ID_FilingStatus
		   ,@IsDeceased IsDeceased
		   ,@ID_SOAPType ID_SOAPType
		   ,@ObjectiveTemplate ObjectiveTemplate 
		   ,@AssessmentTemplate AssessmentTemplate
		   ,@LaboratoryTemplate LaboratoryTemplate
		   ,@ClinicalExaminationTemplate ClinicalExaminationTemplate
		   ,@DiagnosisTemplate DiagnosisTemplate
	   
	   ) H
    LEFT JOIN dbo.tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vPatient patient
      ON patient.ID = H.ID_Patient
    LEFT JOIN dbo.tFilingStatus fs
      ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tSOAPType soapType
      ON soapType.ID = H.ID_SOAPType;
  END;
  ELSE
  BEGIN
    SELECT
		H.*
		,patient.IsDeceased
		,@ObjectiveTemplate ObjectiveTemplate 
		,@AssessmentTemplate AssessmentTemplate
		,@LaboratoryTemplate LaboratoryTemplate
		,@ClinicalExaminationTemplate ClinicalExaminationTemplate
		,@DiagnosisTemplate DiagnosisTemplate
    FROM dbo.vPatient_SOAP H
	LEFT JOIN tPatient patient on h.ID_Patient = patient.ID
    WHERE H.ID = @ID;
  END;


  SELECT * FROM @LabImage ORDER BY ImageRowIndex DESC;

  SELECT
    *
  FROM dbo.vPatient_SOAP_Plan
  WHERE ID_Patient_SOAP = @ID
  ORDER BY DateReturn ASC;
END;

GO

CREATE OR ALTER PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

  DECLARE @Success BIT = 1;
  Declare @MDIgnacioAnimalClinic_ID_Company INT = 8
  Declare @AnimaCareVeterinaryClinic_ID_Company INT = 33
  Declare @AssumptaDogAndCatClinic_ID_Company INT = 38
  Declare @DVMAnimalClinic_ID_Company INT = 24
  Declare @BalagtasAnimalClinicandGroomingCenter_ID_Company INT = 35
  Declare @RDTVeterinaryClinic_ID_Company INT = 41
  Declare @PetFamilyAnimalClinicandGroomingCenter_ID_Company INT = 7
  Declare @CardinalPetClinic_ID_Company INT = 14
  Declare @FursLifeVeterinarClinic_ID_Company INT = 16
  Declare @ELKINGCHAYCLINIC_ID_Company INT = 36
  Declare @ClinicaVeterinaria_ID_Company INT = 44


  SELECT
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

  SELECT
	c.Name Name_Company
   ,client.Name Name_Client
   ,client.ContactNumber ContactNumber_Client
   ,soapPlan.ID ID_Patient_SOAP_Plan
   ,soapPlan.DateReturn
   ,soapPlan.Name_Item
   ,ISNULL(patientSOAP.Comment, '') Comment
   ,'Good day, ' + client.Name + CHAR(9) + CHAR(13) + ' You have an upcoming appointment for your pet ''' +
    patient.Name  + '''' + ' at ' +  c.Name   + ' at ' +
    FORMAT(soapPlan.DateReturn, 'MM/dd/yyyy dddd') + '.' + CHAR(9) + CHAR(13) +
    'Plan: ' + +CHAR(9) + CHAR(13) +
    ' ' + soapPlan.Name_Item +
    CASE
      WHEN LEN(ISNULL(soapPlan.Comment, '')) > 0 THEN ' - '
      ELSE ''
    END + ISNULL(soapPlan.Comment, '')   + CHAR(9) + CHAR(13) + 
	Case WHEN LEN(ISNULL(c.ContactNumber, '')) > 0 THEN 'If you have questions, kindly message ' + c.ContactNumber + '.' ELSE '' END   + ' ' +
	Case WHEN LEN(ISNULL(c.Address, '')) > 0 THEN c.Address ELSE '' END   + ''
	Message,
    CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
  FROM dbo.tPatient_SOAP patientSOAP
  LEFT JOIN dbo.tPatient patient
    ON patient.ID = patientSOAP.ID_Patient
  LEFT JOIN dbo.tClient client
    ON client.ID = patient.ID_Client
  LEFT JOIN tCompany c
    ON c.iD = patientSOAP.ID_Company
  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
    ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
  WHERE patientSOAP.ID_FilingStatus IN (1, 3)
	AND ISNULL(soapPlan.IsSentSMS, 0) = 0
	AND ( (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE()))
	OR (CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE())) )
	AND patientSOAP.ID_Company IN (
		1, 
		@MDIgnacioAnimalClinic_ID_Company, 
		@AnimaCareVeterinaryClinic_ID_Company, 
		@DVMAnimalClinic_ID_Company,
		@BalagtasAnimalClinicandGroomingCenter_ID_Company,
		@PetFamilyAnimalClinicandGroomingCenter_ID_Company,
		@AssumptaDogAndCatClinic_ID_Company,
		@RDTVeterinaryClinic_ID_Company,
		@CardinalPetClinic_ID_Company,
		@FursLifeVeterinarClinic_ID_Company,
		@ELKINGCHAYCLINIC_ID_Company,
		@ClinicaVeterinaria_ID_Company
	)
	ORDEr BY c.Name
END

GO

CREATE OR ALTER PROC pInsertInitialRecord_ItemServices(@ID_Company INT) AS
BEGIN

	Declare @ItemServices Table (ServiceName Varchar(MAX), ID_ItemCategory INT)

	INSERT @ItemServices 
	VALUES
	('3in1- RCP', 4)
	,('4in1- RCCP', 4)
	,('5in1- DHPPiL', 4)
	,('6in1- DHPPiL CV', 4)
	,('8in1- DHPPi L4', 4)
	,('Anal Gland Express D', 3)
	,('Anal Gland Express E', 1)
	,('Antirabies Vaccine', 4)
	,('Blood Test- Send out- CBC', 16)
	,('Blood Test- Send out- CBC, Basic Biochem', 16)
	,('Blood Test- Send out- CBC, Comprehensive Biochem', 16)
	,('Blood Test- Send out- CBC, Comprehensive Biochem, Na, K, Amy, Lip', 16)
	,('Blood Test- Send out- Liver Screen', 16)
	,('Blood Test- Send out- Pancreatic Profile', 16)
	,('Blood Test- Send out- Renal Profile', 16)
	,('Blood Test- Send out- Single Parameter', 16)
	,('Blood Test- Send out- Thyroid Screen', 16)
	,('Consult Fee', 13)
	,('Consult Fee (Discounted)', 13)
	,('Consult Fee- Follow up', 13)
	,('Deworming 1-10kg', 14)
	,('Deworming 10.1-20kg', 14)
	,('Deworming 20.1-30kg', 14)
	,('Deworming 30.1-40kg', 4)
	,('Deworming 40.1-50kg', 14)
	,('Ear Clean I', 1)
	,('Ear Clean N', 1)
	,('Euthanasia', 3)
	,('Kennel Cough', 4)
	,('Leptospirosis Vaccine', 4)
	,('Microscopy- Cytology', 17)
	,('Microscopy- Direct Microscopy', 17)
	,('Microscopy- Ear Swab', 17)
	,('Microscopy- Fecalysis', 17)
	,('Microscopy- Skin Scraping', 17)
	,('Microscopy- Sticky Tape Prep', 17)
	,('Microscopy- Vaginal Smear', 17)
	,('Nail Trim D', 1)
	,('Nail Trim E', 1)
	,('Proheart 1-5kg', 4)
	,('Proheart 10.1-15kg', 4)
	,('Proheart 15.1-20kg', 4)
	,('Proheart 5.1-10kg', 4)
	,('Puppy/Kitten Parasite Check (fecalysis and ear swab)', 3)
	,('Rapid Test- Canine Blood Para 3way', 18)
	,('Rapid Test- Canine Blood Para 3way+ CHW', 18)
	,('Rapid Test- Canine Distemper', 18)
	,('Rapid Test- Feline FIV-FeLV', 18)
	,('Rapid Test- Heartworm', 18)
	,('Rapid Test- Parvo', 18)
	,('Rapid Test- Parvo-Corona', 18)
	,('Sedation', 5)
	,('Subcut Fluids', 3)
	,('Urinalysis (Strip, Cytology and Refractometer)', 17)
	,('Wood''s Lamp', 17)
	,('Wound Cleaning and Bandaging', 3)
	,('Wound Cleaning D', 3)
	,('Wound Cleaning E', 3)

	INSERT INTO [dbo].[tItem]
	(
		[ID_Company]
		,[Code]
		,[Name]
		,[ID_ItemType]
		,[ID_ItemCategory]
		,[MinInventoryCount]
		,[MaxInventoryCount]
		,[UnitCost]
		,[UnitPrice]
		,[CurrentInventoryCount]
		,[ID_InventoryStatus]
		,[IsActive]
		,[Comment]
		,[DateCreated]
		,[DateModified]
		,[ID_CreatedBy]
		,[ID_LastModifiedBy]
		,[Old_item_id]
		,[Old_procedure_id]
		,[OtherInfo_DateExpiration]
	)
	SELECT  @ID_Company
			,NULL
			,ServiceName
			,1
			,ID_ItemCategory
			,0
			,0
			,0
			,0
			,0
			,1
			,1
			,NULL
			,GETDATE()
			,GETDATE()
			,1
			,1
			,NULL
			,NULL
			,NULL
	FROM    @ItemServices


END
GO

ALTER PROC [dbo].[pCreateNewCompanyAccess]
(
    @CompanyName VARCHAR(MAX),
    @code VARCHAR(MAX)
)
AS
BEGIN

    DECLARE @ID_Company INT = 0;
    DECLARE @Admin_ID_UserRole INT = 5;
    DECLARE @Sole_ID_UserRole INT = 11;
    DECLARE @Standard_ID_UserRole INT = 12;

    DECLARE @Default_IDs_Employee typIntList;

    EXEC dbo.pCreateNewCompanyAccess_validation @CompanyName, @code;

    INSERT @Default_IDs_Employee
    (
        ID
    )
    VALUES
    (1  ),
    (13),
    (14);

    INSERT dbo.tCompany
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Country,
        Address
    )
    VALUES
    (   @code,        -- Code - varchar(50)
        @CompanyName, -- Name - varchar(200)
        1,            -- IsActive - bit
        '',           -- Comment - varchar(max)
        GETDATE(),    -- DateCreated - datetime
        GETDATE(),    -- DateModified - datetime
        1,            -- ID_CreatedBy - int
        1,            -- ID_LastModifiedBy - int
        1,            -- ID_Country - int
        ''            -- Address - varchar(300)
        );

    SET @ID_Company = @@IDENTITY;

	--exec dbo.pInsertInitialRecord_ItemServices @ID_Company

    INSERT dbo.tEmployee
    (
        Code,
        IsActive,
        ID_CreatedBy,
        ID_LastModifiedBy,
        DateCreated,
        DateModified,
        ImageFile,
        ID_Department,
        ID_EmployeePosition,
        ID_Position,
        LastName,
        FirstName,
        MiddleName,
        ID_Gender,
        ID_EmployeeStatus,
        FullAddress,
        Email,
        ContactNumber,
        Name,
        ID_Company,
		IsSystemUsed
    )
    SELECT Code,
           IsActive,
           ID_CreatedBy,
           ID_LastModifiedBy,
           DateCreated,
           DateModified,
           ImageFile,
           ID_Department,
           ID_EmployeePosition,
           ID_Position,
           LastName,
           FirstName,
           MiddleName,
           ID_Gender,
           ID_EmployeeStatus,
           FullAddress,
           Email,
           ContactNumber,
           Name,
           @ID_Company,
		   CASE WHEN ids.ID = 1  THEN 1 ELSE 0 END  
    FROM dbo.tEmployee
        INNER JOIN @Default_IDs_Employee ids
            ON ids.ID = tEmployee.ID;

    INSERT dbo.tUser
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Employee,
        Username,
        ID_UserGroup,
        Password,
        IsRequiredPasswordChangedOnLogin,
        ID_Patient
    )
    SELECT '',
           emp.Name,
           1,
           '',
           GETDATE(),
           GETDATE(),
           1,
           1,
           emp.ID,
           LOWER(@code) + '-' + LOWER(emp.LastName),
           1,
           LEFT( NEWID(),4),
           1,
           NULL
    FROM dbo.tEmployee emp
    WHERE emp.ID_Company = @ID_Company;

    INSERT dbo.tUser_Roles
    (
        Code,
        Name,
        IsActive,
        Comment,
        ID_User,
        ID_UserRole,
        SeqNo
    )
    SELECT '',
           userRole.Name,
           1,
           '',
           _user.ID,
           userRole.ID,
           1
    FROM dbo.tUser _user
        INNER JOIN dbo.tEmployee emp
            ON emp.ID = _user.ID_Employee
        INNER JOIN dbo.tUserRole userRole
            ON LOWER(userRole.Name) = LOWER(emp.LastName)
    WHERE emp.ID_Company = @ID_Company;

    INSERT dbo.tDocumentSeries                                                                                                    
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Model,
        Counter,
        Prefix,
        IsAppendCurrentDate,
        DigitCount,
        ID_Company
    )
    SELECT Code,
           Name,
           IsActive,
           Comment,
           DateCreated,
           DateModified,
           ID_CreatedBy,
           ID_LastModifiedBy,
           ID_Model,
           1,
           Prefix,
           IsAppendCurrentDate,
           5,
           @ID_Company
    FROM dbo.tDocumentSeries
    WHERE ID_Company = 1;
END;

GO

--Update tPatient_SOAP SET Diagnosis = Objective, ClinicalExamination = Assessment

Declare @CompanyInfo_ID_ListView UNIQUEIDENTIFIER 

SELECT  @CompanyInfo_ID_ListView = Oid
FROM _tListView 
WHERE 
	Name = 'CompanyInfo_ListView'

IF(SELECT  COUNT(*)
	FROM tCustomNavigationLink 
	WHERE Name = 'COMPANY_INFO_LISTVIEW'
) = 0
BEGIN
	
	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   ,'COMPANY_INFO_LISTVIEW'
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,@CompanyInfo_ID_ListView
			   ,'CompanyInfo'
			   ,1
			   ,NULL)
END

GO


update _tNavigation set SeqNo = 2000, Caption = 'Company Info.', ID_Parent = '6D6F6247-DE8B-44CC-94E4-10C16EF776F2' WHERE Name = 'CompanyInfo_ListView'

update _tNavigation set SeqNo = 600 WHERE Name = 'HumanResourceFolder'

update _tNavigation set SeqNo = 1000 WHERE Name = 'Reports'
update _tNavigation set SeqNo = 10 WHERE Name = 'POSSummary_Navigation'
update _tNavigation set SeqNo = 20 WHERE Name = 'BillingInvoicePaidListReport_Navigation'
update _tNavigation set SeqNo = 30 WHERE Name = 'PaymentTransactionDetailReport_Navigation'
update _tNavigation set SeqNo = 40 WHERE Name = 'InventorySummaryReport_Navigation'
update _tNavigation set SeqNo = 50 WHERE Name = 'InventoryDetailReport_Navigation'