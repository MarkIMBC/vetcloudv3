IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPaymentTransaction'
    AND COLUMN_NAME = 'ID_FilingStatus'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPaymentTransaction'
                         ,'RemainingAmount'
                         , 3
END;

GO

ALTER VIEW [dbo].[vPaymentTransaction]
AS
SELECT H.*,
       CONVERT(VARCHAR(100), H.Date, 101) DateString,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       approvedUser.Name AS ApprovedBy_Name_User,
       cancelUser.Name AS CanceledBy_Name_User,
       fs.Name Name_FilingStatus,
       biHed.Name_Patient,
       taxScheme.Name Name_TaxScheme,
       biHed.GrossAmount GrossAmount_BillingInvoice,
       biHed.VatAmount VatAmount_BillingInvoice,
       biHed.NetAmount NetAmount_BillingInvoice,
       biHed.RemainingAmount RemainingAmount_BillingInvoice,
       pMth.Name Name_PaymentMethod,
	   cardType.Name Name_CardType,
       CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,
       CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
       CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
       CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString
FROM tPaymentTransaction H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tUser approvedUser
        ON H.ID_ApprovedBy = approvedUser.ID
    LEFT JOIN dbo.tUser cancelUser
        ON H.ID_CanceledBy = cancelUser.ID
    LEFT JOIN dbo.tFilingStatus fs
        ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.vBillingInvoice biHed
        ON biHed.ID = H.ID_BillingInvoice
    LEFT JOIN dbo.tPaymentMethod pMth
        ON pMth.ID = H.ID_PaymentMethod
    LEFT JOIN dbo.tTaxScheme taxScheme
        ON taxScheme.ID = biHed.ID_TaxScheme
	LEFT JOIN dbo.tCardType cardType
        ON cardType.ID = H.ID_CardType

GO


ALTER PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

  DECLARE @Success BIT = 1;
  Declare @MDIgnacioAnimalClinic_ID_Company INT = 8
  Declare @AnimaCareVeterinaryClinic_ID_Company INT = 33
  Declare @DVMAnimalClinic_ID_Company INT = 24

  SELECT
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

  SELECT
	c.Name Name_Company
   ,client.Name Name_Client
   ,client.ContactNumber ContactNumber_Client
   ,soapPlan.ID ID_Patient_SOAP_Plan
   ,soapPlan.DateReturn
   ,soapPlan.Name_Item
   ,ISNULL(patientSOAP.Comment, '') Comment
   ,'Good day, ' + client.Name + CHAR(9) + CHAR(13) + ' You have an upcoming appointment for your pet ''' +
    patient.Name  + '''' + ' at ' +  c.Name   + ' at ' +
    FORMAT(soapPlan.DateReturn, 'MM/dd/yyyy dddd') + '.' + CHAR(9) + CHAR(13) +
    'Plan: ' + +CHAR(9) + CHAR(13) +
    ' ' + soapPlan.Name_Item +
    CASE
      WHEN LEN(ISNULL(soapPlan.Comment, '')) > 0 THEN ' - '
      ELSE ''
    END + ISNULL(soapPlan.Comment, '')   + CHAR(9) + CHAR(13) + Case WHEN LEN(ISNULL(c.ContactNumber, '')) > 0 THEN 'If you have questions, kindly message ' + c.ContactNumber + '.' ELSE '' END  Message,
    CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
  FROM dbo.tPatient_SOAP patientSOAP
  LEFT JOIN dbo.tPatient patient
    ON patient.ID = patientSOAP.ID_Patient
  LEFT JOIN dbo.tClient client
    ON client.ID = patient.ID_Client
  LEFT JOIN tCompany c
    ON c.iD = patientSOAP.ID_Company
  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
    ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
  WHERE patientSOAP.ID_FilingStatus IN (3)
	AND ISNULL(soapPlan.IsSentSMS, 0) = 0
	AND (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE()))
	AND patientSOAP.ID_Company IN (@MDIgnacioAnimalClinic_ID_Company, @AnimaCareVeterinaryClinic_ID_Company, 1, @DVMAnimalClinic_ID_Company)
END

GO

ALTER PROC [dbo].[pGetBillingInvoice]
    @ID INT = -1,
    @ID_Client INT = NULL,
    @ID_Patient INT = NULL,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' BillingInvoice_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               fs.Name Name_FilingStatus,
               client.Name Name_Client,
			   patient.Name Name_Patient,
			   client.Address BillingAddress
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- NEW -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   GETDATE() AS Date,
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   0 AS [ID_CreatedBy],
                   0 AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
                   0 AS [ID_Taxscheme],
                   @ID_Client ID_Client,
                   0 IsComputeDiscountRate,
                   @ID_Patient ID_Patient
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
            LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
            LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vBillingInvoice H
        WHERE H.ID = @ID;
    END;

    SELECT *
    FROM dbo.vBillingInvoice_Detail
    WHERE ID_BillingInvoice = @ID;
END;
GO


CREATE OR ALTER PROC [dbo].pGetPaymentTransactionPrintReceiptLayout
(
    @ID_PaymentTransaction INT
)
AS
BEGIN

	DECLARE @isAutoPrint BIT = 1

	DECLARE @Cash_ID_PaymentMethod INT = 1
	DECLARE @Check_ID_PaymentMethod INT = 2
	DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
	DECLARE @GCash_ID_PaymentMethod INT = 3


	DECLARE @billingItemsLayout VARCHAR(MAX)= ''
	DECLARE @paymentLayout VARCHAR(MAX)= ''


	Declare @ID_Company int	= 0
	Declare @ID_BillingInvoice int	= 0
	Declare @Name_Company VARCHAR(MAX)= ''
	Declare @Address_Company VARCHAR(MAX)= ''
	Declare @ContactNumber_Company VARCHAR(MAX)= ''
	Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
	Declare @ID_PaymentMode int	= 0

	Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
	Declare @CheckNumber VARCHAR(MAX)= ''
	Declare @CardNumber VARCHAR(MAX)= ''
	Declare @Name_CardType VARCHAR(MAX)= ''
	Declare @CardHolderName VARCHAR(MAX)= ''
	
	Declare @Name_PaymentStatus VARCHAR(MAX)= ''

	Declare @CashAmount DECIMAL(18, 4) = 0.00
	Declare @GCashAmount DECIMAL(18, 4) =  0.00
	Declare @CardAmount DECIMAL(18, 4) =  0.00
	Declare @CheckAmount DECIMAL(18, 4) =  0.00
	
	Declare @PayableAmount DECIMAL(18, 4) =  0.00
	Declare @PaymentAmount DECIMAL(18, 4) =  0.00
	Declare @ChangeAmount DECIMAL(18, 4) =  0.00
	Declare @RemainingAmount DECIMAL(18, 4) =  0.00

	Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
	Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
	
	Declare @Name_Client VARCHAR(MAX)= ''
	Declare @Name_Patient VARCHAR(MAX)= ''
	Declare @Date_BillingInvoice datetime
	Declare @Code_BillingInvoice VARCHAR(MAX)= ''

	SELECT  @ID_Company = ID_Company,
			@ID_BillingInvoice = ID_BillingInvoice,
			@ID_PaymentMode = ID_PaymentMethod,
			@Code_PaymentTranaction = Code,
			@PayableAmount = PayableAmount,
			@RemainingAmount = RemainingAmount,
			@ChangeAmount = ChangeAmount,
			@CashAmount = CashAmount,
			@ReferenceTransactionNumber = ReferenceTransactionNumber,
			@GCashAmount = GCashAmount,
			@Name_CardType = Name_CardType,
			@CardNumber = CardNumber,
			@CardHolderName = CardHolderName,
			@CardAmount = CardAmount,
			@CheckNumber = CheckNumber,
			@CheckAmount = CheckAmount,
			@PaymentAmount = PaymentAmount
	FROM    vPaymentTransaction 
	WHERE 
		ID = @ID_PaymentTransaction

	SELECT  @Name_Company = ISNULL(Name,'N/A'),
			@Address_Company = ISNULL(Address,''),
			@ContactNumber_Company = ISNULL(ContactNumber,'')
	FROM    tCompany 
	WHERE ID = @ID_Company

	SELECT  @Name_Client = ISNULL(bi.Name_Client,''),
			@Name_Patient = ISNULL(bi.Name_Patient,'N/A'),
			@Date_BillingInvoice = Date,
			@Code_BillingInvoice = Code,
			@SubTotal_BillingInvoice = SubTotal,
			@DiscountRate_BillingInvoice = DiscountRate,
			@DiscountAmount_BillingInvoice = DiscountAmount,
			@TotalAmount_BillingInvoice = TotalAmount, 
			@Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus,'')
	FROM    vBillingInvoice bi
	WHERE
		bi.ID = @ID_BillingInvoice

	SELECT  @billingItemsLayout = @billingItemsLayout + '
			<div class="display-block clearfix">
				<span class="bold">'+  biDetail.Name_Item +'</span><br/>
				<span class="float-left">
					&nbsp;&nbsp;&nbsp;'+  FORMAT(biDetail.Quantity,'#,#0.##') +'
				</span>
				<span class="float-right">'
					+  FORMAT(biDetail.Amount,'#,#0.00') +
				'</span>
			</div>'
	FROM    vBillingInvoice_Detail biDetail
	WHERE 
		biDetail.ID_BillingInvoice = @ID_BillingInvoice

	if @ID_PaymentMode = @Cash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Cash Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@CashAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	else if @ID_PaymentMode = @GCash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Ref. No.
				</span>
				<span class="float-right">'
					+ @ReferenceTransactionNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					G-Cash
				</span>
				<span class="float-right">'
					+ FORMAT(@GCashAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Type
				</span>
				<span class="float-right">'
					+ @Name_CardType +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card #
				</span>
				<span class="float-right">'
					+ @CardNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Holder
				</span>
				<span class="float-right">'
					+ @CardHolderName +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CardAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check No.
				</span>
				<span class="float-right">'
					+ @CheckNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CheckAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	DECLARE @title VARCHAR(MAX)= ''
	DECLARE @style VARCHAR(MAX)= '
		body{
			margin: 0px;
			padding: 0px;
			font-family:  arial, sans-serif;
			font-weight: normal;
			font-style: normal;
			font-size: 13px;
		}

		.receipt-container{
			width: 160px;
		}

		.company-name{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
		}

		.company-address{
			display: block;
			text-align: center;
			word-wrap: break-word;
		}

		.company-contactnum-container{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-weight: bold;
		}

		.company-contactnum{
			font-weight: 200;
			text-align: center;
			word-wrap: break-word;
		}

		.float-left{
			float: left;
		}

		.float-right{
			float: right;
		}

		.clearfix {
			overflow: auto;
		}

		.bold{
			font-weight: bold;
		}

		.display-block{

			display: block;
		}

		.title{
			text-align: center;
			word-wrap: break-word;
			display: block;
			padding-top: 15px;
			padding-bottom: 15px;
		}
	'

	DECLARE @content VARCHAR(MAX)= '
		<div class="receipt-container">
			<div class="company-name">'+ @Name_Company+ '' +'</div>
			<div class="company-address">'+ @Address_Company+ '' +'</div>
			<div class="company-contactnum-container" style="'+ CASE WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none' ELSE '' END +'">
				Contact No: <span class="company-contactnum">'+ @ContactNumber_Company +'</span>
			</div>
			<div class="title bold">INVOICE</div>
			<div class="display-block"><span class="bold">Bill To: </span>'+ @Name_Client +'</div>
			<div class="display-block"><span class="bold">Pet Name: </span>'+ @Name_Patient +'</div>
			<div class="display-block">
				<span class="bold">Invoice Date: </span>'+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy') +'
			</div> 
			<div class="display-block"><span class="bold">BI #: </span>'+ @Code_BillingInvoice +'</div> 
			<br/>
			'+ @billingItemsLayout +'

			<div class="title bold">TOTALS</div>
			
			<div class="display-block clearfix">
				<span class="float-left bold">
					Subtotal
				</span>
				<span class="float-right">'
					+  FORMAT(@SubTotal_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Rate
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountRate_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Total Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@TotalAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>

			<div class="title bold">PAYMENT</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					PT #
				</span>
				<span class="float-right">'
					+  @Code_PaymentTranaction +
				'</span>
			</div>
			'+ @paymentLayout +'
			<br/>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Balance
				</span>
				<span class="float-right">'
					+ FORMAT(@PayableAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Payment
				</span>
				<span class="float-right">'
					+ FORMAT(@PaymentAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Remaining
				</span>
				<span class="float-right">'
					+ FORMAT(@RemainingAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Change
				</span>
				<span class="float-right">'
					+ FORMAT(@ChangeAmount,'#,#0.00') +
				'</span>
			</div>

			<div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>

			<div class="title"> Print Date '+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy hh:mm:ss tt') +'</div>
		</div>
	'

    SELECT '_'

	SELECT @title title, @style style, @content content, @isAutoPrint isAutoPrint, @ID_PaymentTransaction, @ID_BillingInvoice

END
GO

CREATE OR ALTER PROC [dbo].[pUpdatePaymentTransaction_RemainingAmount_By_BIs]
(
    @IDs_BillingInvoice typIntList READONLY
)
AS
BEGIN

	DECLARE @maxID INT, @counter INT

	DECLARE @BillingInvoice TABLE(
		RowID int identity(1,1) primary key,
		ID_BillingInvoice Int
	)

	INSERT @BillingInvoice
		(ID_BillingInvoice)
	SELECT  ID
	FROM   @IDs_BillingInvoice 
	
	SET @counter = 1
	SELECT @maxID = COUNT(*) FROM @BillingInvoice

	WHILE (@counter <= @maxID)
	BEGIN

		DECLARE @ID_BillingInvoice INT = 0

		SELECT  
			@ID_BillingInvoice = ID_BillingInvoice
		FROM    @BillingInvoice 
		WHERE
			RowID = @counter

		exec dbo.[pUpdatePaymentTransaction_RemainingAmount_By_BI] @ID_BillingInvoice

		SET @counter = @counter + 1
	END
END

GO

CREATE OR ALTER PROC [dbo].[pUpdatePaymentTransaction_RemainingAmount_By_BI]
(
    @ID_BillingInvoice INT
)
AS
BEGIN

	Declare @Approved_ID_FilingStatus INT = 3
	Declare @PayableAmount_BillingInvoice DECIMAL(18, 4) = 0

	Declare @PaymentHistory TABLE(
		RowID int identity(1,1) primary key,
		ID_PaymentTransaction Int, 
		Date DateTime, 
		Code Varchar(20), 
		Name_PaymentMethod Varchar(200),
		PayableAmount DECIMAL(18, 4),
		PaymentAmount DECIMAL(18, 4),
		RemainingBalance DECIMAL(18, 4)
	)

	SELECT  @PayableAmount_BillingInvoice = bi.NetAmount
	FROM    tBillingInvoice bi WHERE bi.ID = @ID_BillingInvoice

	INSERT @PaymentHistory(
		 ID_PaymentTransaction
		,Date
		,Code
		,Name_PaymentMethod
		,PaymentAmount
		,RemainingBalance
	)
	SELECT 
		pt.ID,
		pt.Date,
		pt.Code,
		pt.Name_PaymentMethod,
		pt.PaymentAmount,
		0
	FROM dbo.vPaymentTransaction pt
	WHERE 
		ID_BillingInvoice = @ID_BillingInvoice
		AND
		ID_FilingStatus IN (@Approved_ID_FilingStatus)

	DECLARE @maxID INT, @counter INT

	SET @counter = 1
	SELECT @maxID = COUNT(*) FROM @PaymentHistory

	WHILE (@counter <= @maxID)
	BEGIN
		Declare @PaymentAmount DECIMAL(18, 4) = 0
		Declare @PayableAmount DECIMAL(18, 4) = 0
		Declare @ChangeAmount DECIMAL(18, 4) = 0


		SELECT  
			@PaymentAmount = PaymentAmount
		FROM    @PaymentHistory
		WHERE
			RowID =  @counter
	
		SET @PayableAmount = @PayableAmount_BillingInvoice
		SET @PayableAmount_BillingInvoice = @PayableAmount_BillingInvoice - @PaymentAmount

		IF @PayableAmount < @PaymentAmount
		BEGIN
			
			SET @PayableAmount_BillingInvoice = 0
		END
		
		Update @PaymentHistory 
		SET 
			PayableAmount = @PayableAmount,
			RemainingBalance = @PayableAmount_BillingInvoice
		WHERE RowID =  @counter

		SET @counter = @counter + 1
	END

	UPDATE tPaymentTransaction 
	SET
		RemainingAmount = payHistory.RemainingBalance
	FROM tPaymentTransaction pt
	INNER JOIN @PaymentHistory payHistory 
		on pt.ID = payHistory.ID_PaymentTransaction

	UPDATE dbo.tBillingInvoice
    SET RemainingAmount = @PayableAmount_BillingInvoice
    WHERE ID = @ID_BillingInvoice
	
	UPDATE dbo.tBillingInvoice
    SET Payment_ID_FilingStatus = dbo.fGetPaymentStatus(NetAmount, RemainingAmount)
    WHERE ID = @ID_BillingInvoice

END

GO

ALTER PROC [dbo].[pUpdateBillingInvoicePayment]
(@IDs_BillingInvoice typIntList READONLY)
AS
BEGIN

    DECLARE @FILINGSTATUS_PENDING INT = 2;
    DECLARE @FILINGSTATUS_APPROVED INT = 3;
    DECLARE @FILINGSTATUS_PARTIALLYPAID INT = 11;
    DECLARE @FILINGSTATUS_FULLYPAID INT = 12;

	exec dbo.pUpdatePaymentTransaction_RemainingAmount_By_BIs @IDs_BillingInvoice
	 
END;

GO

CREATE OR ALTER PROC [dbo].[pApprovePaymentTransaction_validation]
(
    @IDs_PaymentTransaction typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Filed_ID_FilingStatus INT = 1;
    DECLARE @message VARCHAR(400) = '';

    DECLARE @ValidateNotFiled TABLE
    (
        Code VARCHAR(30),
        Name_FilingStatus VARCHAR(30)
    );
    DECLARE @Count_ValidateNotFiled INT = 0;

	DECLARE @ValidateNotAbleToPay TABLE
    (
        Code VARCHAR(30),
		Code_BillingInvoice VARCHAR(30),
		RemainingAmount_BillingInvoice DECIMAL(18, 4)  ,
        PaymentAmount  DECIMAL(18, 4)
    );
	DECLARE @Count_ValidateNotAbleToPay INT = 0;


    /* Validate PaymentTransaction Status is not Filed*/
    INSERT @ValidateNotFiled
    (
        Code,
        Name_FilingStatus
    )
    SELECT Code,
           Name_FilingStatus
    FROM dbo.vPaymentTransaction bi
    WHERE EXISTS
    (
        SELECT ID FROM @IDs_PaymentTransaction ids WHERE ids.ID = bi.ID
    )
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

    SELECT @Count_ValidateNotFiled = COUNT(*)
    FROM @ValidateNotFiled;

    IF (@Count_ValidateNotFiled > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotFiled > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to approved:';

        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus
        FROM @ValidateNotFiled;
        THROW 50001, @message, 1;

    END;

	/* Validate Validate Not Able To Pay*/
	insert INTO @ValidateNotAbleToPay
	(
        Code,
		Code_BillingInvoice,
		RemainingAmount_BillingInvoice,
        PaymentAmount
	)
	SELECT p.Code
		  ,bi.Code
		  ,bi.RemainingAmount
		  ,p.PaymentAmount
	FROM    tPaymentTransaction p 
	INNER JOIN @IDs_PaymentTransaction ids
		ON ids.ID = p.ID
	INNER JOIN tBillingInvoice bi 
		ON p.ID_BillingInvoice = bi.ID
	WHERE ISNULL(bi.RemainingAmount, 0) <= 0

    SELECT @Count_ValidateNotAbleToPay = COUNT(*)
	FROM @ValidateNotAbleToPay;

	IF (@Count_ValidateNotAbleToPay > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotFiled > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to approved:';

        SELECT @message = @message + CHAR(10) + Code_BillingInvoice  + ' Rem. Bal: ' + FORMAT(RemainingAmount_BillingInvoice,'#,##0.00')  + ' Payment: ' + FORMAT(PaymentAmount,'#,##0.00')
        FROM @ValidateNotAbleToPay;
        THROW 50001, @message, 1;

    END;
	
END;

GO

ALTER PROC [dbo].[pModel_AfterSaved]  
    @ID_Model UNIQUEIDENTIFIER,  
    @ID_CurrentObject VARCHAR(10),  
    @IsNew BIT = 0  
AS  
BEGIN  
  
    DECLARE @ModelName VARCHAR(MAX) = '';  
  
    SELECT @ModelName = Name  
    FROM dbo._tModel  
    WHERE Oid = @ID_Model;  
  
    IF @ModelName = 'AppointmentSchedule'  
    BEGIN  
  
        EXEC dbo.pModel_AfterSaved_AppointmentSchedule @ID_CurrentObject, @IsNew;  
    END;  
  
    ELSE IF @ModelName = 'Schedule'  
    BEGIN  
  
        EXEC dbo.pModel_AfterSaved_Schedule @ID_CurrentObject, @IsNew;  
    END;  
  
    ELSE IF @ModelName = 'BillingInvoice'  
    BEGIN  
  
        EXEC dbo.pModel_AfterSaved_BillingInvoice @ID_CurrentObject, @IsNew;  
    END;  
  
    
    ELSE IF @ModelName = 'PurchaseOrder'  
    BEGIN  
  
        EXEC dbo.pModel_AfterSaved_PurchaseOrder @ID_CurrentObject, @IsNew;  
    END;  


    ELSE IF @ModelName = 'ReceivingReport'  
    BEGIN  
  
        EXEC dbo.pModel_AfterSaved_ReceivingReport @ID_CurrentObject, @IsNew;  
    END;  

    ELSE IF @ModelName = 'Patient'  
    BEGIN  
        EXEC dbo.pModel_AfterSaved_Patient @ID_CurrentObject, @IsNew;  
    END;  
  
  
    ELSE IF @ModelName = 'PaymentTransaction'  
    BEGIN  
        EXEC dbo.pModel_AfterSaved_PaymentTransaction @ID_CurrentObject, @IsNew;  
    END;  
  
    ELSE IF @ModelName = 'Patient_SOAP'  
    BEGIN  
        EXEC dbo.pModel_AfterSaved_Patient_SOAP @ID_CurrentObject, @IsNew;  
    END;  
  
    ELSE IF @ModelName = 'Item'  
    BEGIN  
		EXEC dbo.pUpdateItemCurrentInventory
    END;  
  
    PRINT 1;  
END;  
GO

