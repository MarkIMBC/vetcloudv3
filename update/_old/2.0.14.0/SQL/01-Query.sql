ALTER TABLE tPatient_SOAP
  ALTER COLUMN History VARCHAR(8000)

GO

IF ( NOT EXISTS (SELECT *
                 FROM   INFORMATION_SCHEMA.TABLES
                 WHERE  TABLE_SCHEMA = 'dbo'
                        AND TABLE_NAME = '_tModelReport') )
  BEGIN
      exec _pCreateTable
        '_tModelReport'

      exec _pCreateAppModule
        '_tModelReport',
        1,
        0

      exec _pAddModelProperty
        '_tModelReport',
        'Oid_Model',
        1

      exec _pAddModelProperty
        '_tModelReport',
        'Oid_Report',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'Code',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'Name',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'IsActive',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'Comment',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'DateCreated',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'DateModified',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'ID_CreatedBy',
        1

      exec _pDeleteModelProperty
        NULL,
        '_tModelReport',
        'ID_LastModifiedBy',
        1
  END

EXEC _pRefreshAllViews

GO

ALTER FUNCTION dbo.fGetSOAPLANMessage (@CompanyName        Varchar(MAX),
                                       @SOAPPlanSMSMessage VARCHAR(MAX),
                                       @Client             Varchar(MAX),
                                       @ContactNumber      Varchar(MAX),
                                       @Pet                Varchar(MAX),
                                       @Service            Varchar(MAX),
                                       @Reason             Varchar(MAX),
                                       @DateReturn         DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')
      Declare @message Varchar(MAX) = @SOAPPlanSMSMessage

      if( LEN(ISNULL(@Reason, '')) > 0 )
        SET @Reason = '- ' + @Reason

      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Pet)))
      SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
      SET @message = REPLACE(@message, '/*Reason*/', LTRIM(RTRIM(@Reason)))
      SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))

      RETURN @message
  END

GO

Alter View vModelReport
as
  SELECT modelReport.ID,
         modelReport.ID_Company,
         company.Name     Name_Company,
         Oid_Model,
         model.Name       Name_Model,
         Oid_Report,
         report.Name      Name_Report,
         modelReport.Name Name_ModelReport
  FROm   _tModelReport modelReport
         LEFT JOIN _tModel model
                ON modelReport.Oid_Model = model.Oid
         LEFT JOIN _tReport report
                ON modelReport.Oid_Report = report.Oid
         LEFT JOIN tCompany company
                ON modelReport.ID_Company = company.ID

GO

CREATE OR
ALTER VIEW [dbo].vzClinicaVeterinariaAgainstMedicalAdvice
AS
  SELECT Top 10 patient.ID,
                patient.Name                               Name_Patient,
                ISNULL(Species, '')                        Name_Species,
                ISNULL(Name_Gender, '')                    Name_Gender,
                dbo.fGetAge(patient.DateBirth, case
                                                 WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                                 ELSE NULL
                                               END, 'N/A') Age_Patient,
                ISNULL(client.Name, '')                    Name_Client,
                ISNULL(client.Address, '')                 Address_Client,
                ISNULL(client.ContactNumber, '')           ContactNumber_Client,
                ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
                ISNULL(client.Email, '')                   Email_Client,
                company.ID                                 ID_Company,
                company.ImageLogoLocationFilenamePath,
                company.Name                               Name_Company,
                company.Address                            Address_Company,
                company.ContactNumber                      ContactNumber_Company,
                CASE
                  WHEN LEN(company.Address) > 0 THEN '' + company.Address
                  ELSE ''
                END
                + CASE
                    WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
                    ELSE ''
                  END
                + CASE
                    WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
                    ELSE ''
                  END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company
  WHERE  client.ID_Company = 44

GO

ALTER VIEW vClient_ListView
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '') ContactNumbers,
         DateCreated,
         ISNULL(IsActive, 0)          IsActive
  FROM   dbo.vClient
  WHERE  ISNULL(IsActive, 0) = 1

GO

ALTER VIEW vSMSList_Patient_SOAP_Plan
AS
  SELECT soapPlan.ID,
         soapPlan.ID_Patient_SOAP,
         soap.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), Name_Patient, ISNULL(soapPlan.Name_Item, ''), ISNULL(soapPlan.Comment, ''), soapPlan.DateReturn) Message,
         DATEADD(DAY, -1, DateReturn)                                                                                                                                                                        DateSending,
         soap.ID_Company,
         ISNULL(soapPlan.IsSentSMS, 0)                                                                                                                                                                       IsSentSMS
  FROM   vPatient_SOAP_Plan soapPlan
         INNER JOIN vPatient_SOAP soap
                 on soapPlan.ID_Patient_SOAP = soap.ID
         INNER JOIN tClient c
                 on c.ID = soap.ID_Client
         INNER JOIN tCOmpany comp
                 on comp.ID = soap.ID_Company

GO

ALTER PROC [dbo].[pGetSendSOAPPlan](@Date      DateTime,
                                    @IsSMSSent Bit = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETime,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETime,
           ID_Patient_SOAP_Plan INT
        )

      INSERT @record
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                                            Name_Client,
             ISNULL(client.ContactNumber, '0')                                                                                                                                                      ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, ISNULL(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND ( ( CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date) )
                    OR ( CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ) )
             AND patientSOAP.ID_Company IN (SELECT ID_Company
                                            FROM   tCompany_SMSSetting
                                            WHERE  ISNULL(IsActive, 0) = 1)
      ORDER  BY c.Name

      SELECT @Success Success;

      SELECT FORMAT(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             SUM(CASE
                   WHEN LEN(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN LEN(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN LEN(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          ConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateSending, 'yyyy-MM-dd'),
                Name_Company
      Order  BY DateSending DESC,
                Name_Company
  END

SELECT *
FROM   @record
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,
          Name_Company,
          Name_Client

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC pLogItemUnitPrice
              @ID_CurrentObject

            EXEC pLogItemUnitCost
              @ID_CurrentObject

            EXEC dbo.pUpdateItemCurrentInventory
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_Client] (@ID_CurrentObject VARCHAR(10),
                                             @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tClient
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tClient';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tClient
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Client typIntList

      INSERT @IDs_Client
      VALUES (@ID_CurrentObject)
  END;

GO

CREATE OR
ALTER PROC pAddModelReport(@ID_Company INT,
                           @ModelName  Varchar(200),
                           @ReportName VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportName

      /* Check if Model is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tModel
      WHERE  Oid = @Oid_Model;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ModelName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Report is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tReport
      WHERE  Oid = @Oid_Report;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ReportName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Model and Report is already exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   [_tModelReport]
      WHERE  Oid_Report = @Oid_Report
             AND Oid_Model = @Oid_Model
             AND ID_Company = @ID_Company;

      IF( @Count_Validation > 0 )
        BEGIN
            SET @msg = @ModelName + ' ' + @ReportName
                       + ' is already exist.';

            THROW 51000, @msg, 1;
        END

      INSERT INTO [dbo].[_tModelReport]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   DateCreated,
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Oid_Report])
      VALUES      ('',
                   1,
                   @ID_Company,
                   GETDATE(),
                   1,
                   1,
                   @Oid_Model,
                   @Oid_Report)
  END

GO

CREATE OR
ALTER PROC pGetModelReports(@CompanyID INT,
                            @Oid_Model VARCHAR(200))
AS
  BEGIN
      SELECT '_',
             '' as Reports

      SELECT 1 Success

      SELECT model.Name       Name_Model,
             modelReport.Oid_Report,
             modelReport.Name Name_ModelReport
      FROm   _tModelReport modelReport
             INNER JOIN _tMOdel model
                     on model.Oid = modelReport. Oid_Model
             INNER JOIN _tReport report
                     on report.Oid = modelReport. Oid_Report
      WHERE  LOWER(modelReport.Oid_Model) = LOWER(@Oid_Model)
             AND modelReport.ID_Company = @CompanyID
             AND modelReport.IsActive = 1
      ORDER  BY report.Name
  END

GO

CREATE OR
ALTER PROC pChangeModelReport(@ID_Company     INT,
                              @ModelName      Varchar(200),
                              @ReportNameFrom VARCHAR(200),
                              @ReportNameTo   VARCHAR(200),
                              @Label          VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @From_Oid_Report VARCHAR(MAX)
      DECLARE @To_Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @From_Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportNameFrom

      SELECT @To_Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportNameTo

      Update _tModelReport
      Set    Oid_Report = @To_Oid_Report,
             Name = @Label
      WHERE  OId_Model = @Oid_Model
             AND Oid_Report = @From_Oid_Report
             AND ID_Company = @ID_Company
  END

GO

CREATE OR
ALTER PROC pDeleteModelReport(@ID_Company INT,
                              @ModelName  Varchar(200),
                              @ReportName VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @Oid_Report = Oid
      FROM   _tModel
      WHERE  Name = @ReportName

      DELETE FROM [_tModelReport]
      WHERE  ISNULL(OId_Model, '') = ISNULL(@Oid_Model, '')
             AND ISNULL(Oid_Report, '') = ISNULL(@Oid_Report, '')
             AND ID_Company = @ID_Company;
  END

GO

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*//*Reason*/ on /*DateReturn*/.    
Pls. contact /*CompanyName*/ /*ContactNumber*/'
WHERE  ID IN ( 22, 23 ) 
