VetCloud Ver 2 System update 2.0.14.0 updated at 06:30 am

System 
	- Database-driven Reports per Model

Client
	- Generate Client Code on Client Records for all Clients.
	- Listview - fix filter Code

Patient 
	- Detailview - remove Subjective Column on SOAP List 
	- Listview - Rename Patient List column Owner's Name, Pet's Name, and Last Date Visited
	- Add Patient reports for Clinica Figura
		- Against Medical Advice
		- Discharge Instruction
		- Consent For Surgery
		- Confinement Surgery Agreement
		- Euthanasia Authorization

Item
	- Auto complete box on Name field.

Service
	- Auto complete box on Name field.

Patient SOAP 
	- Change Character Limit up to History

NOTE:
	if these updates are not yet take effect or display, 
	please force reload the Vet Cloud System for the browsers:

	Google Chrome
		On Windows and Linux, use one of the following:

			Hold the Ctrl key and press the F5 key.
			Hold the ⇧ Shift key and press the F5 key.
			Hold the Ctrl key and press the R key.
			Hold the Ctrl key and the ⇧ Shift key, then press the R key.
			Hold the Ctrl key and click the Reload button on the navigation toolbar.
			Hold the ⇧ Shift key and click the Reload button on the navigation toolbar.
		On macOS:

			Hold both the ⌘ Cmd and ⇧ Shift keys and press the R key.
			Hold the ⇧ Shift key and click the Reload button on the navigation toolbar.
			Hold the Ctrl key and click the Reload button on the navigation toolbar.

	Microsoft Edge
			On Windows:

				Hold the Ctrl key and press the F5 key.[1]

	Internet Explorer
		Use one of the following:

		Hold the Ctrl key and press the F5 key.
		Hold the Ctrl key and click the "Refresh" button on the toolbar.
	
	Firefox and other related browsers
		These instructions work for Firefox, SeaMonkey, and other related browsers.

		On Windows and Linux, use one of the following:

			Hold both the Ctrl and ⇧ Shift keys and then press R.
			Hold the ⇧ Shift key and click the Reload button on the navigation toolbar.
			Hold the Ctrl key and press the F5 key.

		On macOS, use one of the following:

			Hold both the ⌘ Cmd and ⇧ Shift keys and then press R.
			Hold the ⇧ Shift key and click the Reload button on the navigation toolbar.

	Safari
		For version 4 and newer:

		Hold down the ⇧ Shift key and click the Reload toolbar button
		or

		Use keyboard shortcut ⌥ Opt+⌘ Cmd+R to clear cache.
		For version 3 and older:

		Hold down the ⌘ Cmd key and press R. This kind of "regular" reload will usually bypass the cache.
	
	Opera
		Use one of the following:

		Hold the Ctrl key and press the F5 key.
		Hold the ⇧ Shift key and click the Reload button on the navigation toolbar.
		Hold the Ctrl key and click the Reload button on the navigation toolbar.
		Hold the ⇧ Shift key and press the F5 key.

	Konqueror
		Press F5 or click the "Reload" button on the toolbar.