if OBJECT_ID('dbo.tPatientWaitingListCanceled') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tPatientWaitingListCanceled',
        1,
        1,
        NULL
  END

GO

EXEC _pRefreshAllViews

GO

Declare @Oid_PatientWaitingListCanceled_ListView UNIQUEIDENTIFIER

SELECT @Oid_PatientWaitingListCanceled_ListView = Oid
FROM   _tListView
WHERE  Name = 'PatientWaitingListCanceled_ListView'

IF(SELECT COUNT(*)
   FROM   tCustomNavigationLink
   WHERE  Name = 'PatientWaitingListCanceled_ListView') = 0
  BEGIN
      INSERT INTO [dbo].[tCustomNavigationLink]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_ListView],
                   [RouterLink],
                   [ID_ViewType],
                   [Oid_Report])
      VALUES      (NULL,
                   'PatientWaitingListCanceled_ListView',
                   1,
                   1,
                   NULL,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @Oid_PatientWaitingListCanceled_ListView,
                   'PatientWaitingListCanceledList',
                   1,
                   NULL)
  END

GO

Update _tNavigation
SET    Caption = 'Missed Appointment',
       Route = 'PatientWaitingListCanceledList',
       ID_Parent = '24C522DA-EA51-439F-BE1B-F7D096BEEF51',
       SeqNo = 3000
where  Name = 'PatientWaitingListCanceled_ListView'

GO

CREATE OR
ALTER view vPatientWaitingCanceledList_ListView
AS
  SELECT DISTINCT ISNULL(appointment.UniqueID, CONVERT(VARCHAR(MAX), MAX(hed.ID))) UniqueID,
                  hed.ID_Company,
                  ISNULL(appointment.DateStart, hed.DateCreated)                   DateStart,
                  ISNULL(appointment.DateEnd, hed.DateCreated)                     DateEnd,
                  hed.ID_Client,
                  hed.Name_Client,
                  hed.ID_Patient,
                  hed.Name_Patient,
                  ISNULL(appointment.ReferenceCode, CASE
                                                      WHEN hed.IsQueued = 1 THEN 'Queued'
                                                      ELSE ''
                                                    END)                           ReferenceCode,
                  hed.WaitingStatus_ID_FilingStatus,
                  hed.WaitingStatus_Name_FilingStatus,
                  appointment.Description,
                  hed.IsQueued
  FROM   vPatientWaitingList hed
         INNER JOIN vPatientWaitingMaxCanceled maxcanceledWaiting
                 ON hed.ID = maxcanceledWaiting.ID_PatientWaitingList
         LEFT JOIN vAppointmentEvent appointment
                on hed.ID_Client = appointment.ID_Client
                   and hed.ID_Patient = appointment.ID_Patient
         LEFT JOIN tPatient patient
                on patient.ID = appointment.ID_Patient
  where  hed.WaitingStatus_ID_FilingStatus = 4
         AND hed.WaitingStatus_ID_FilingStatus = 4
  GROUP  BY appointment.UniqueID,
            hed.IsQueued,
            hed.ID_Company,
            hed.DateCreated,
            appointment.DateStart,
            appointment.DateEnd,
            hed.ID_Client,
            hed.Name_Client,
            hed.ID_Patient,
            hed.Name_Patient,
            appointment.ReferenceCode,
            hed.WaitingStatus_ID_FilingStatus,
            hed.WaitingStatus_Name_FilingStatus,
            appointment.Description

GO

CREATE OR
ALTER VIEW vPatientWaitingMaxCanceled
AS
  SELECT MAX(ID) ID_PatientWaitingList
  FROm   vPatientWaitingList
  where  WaitingStatus_ID_FilingStatus = 4
  GROUP  BY ID_Client,
            Name_Client,
            ID_Patient,
            Name_Patient

GO