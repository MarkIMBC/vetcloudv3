/****** Object:  Table [dbo].[PetDynasty_ItemImport_20210403]    Script Date: 4/3/2021 1:09:34 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PetDynasty_ItemImport_20210403]') AND type in (N'U'))
DROP TABLE [dbo].[PetDynasty_ItemImport_20210403]
GO
/****** Object:  Table [dbo].[PetDynasty_ItemImport_20210403]    Script Date: 4/3/2021 1:09:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetDynasty_ItemImport_20210403](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [nvarchar](255) NULL,
	[Selling Price] [float] NULL,
	[Current Inventory Count] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mr.Giggles shampoo', N'Detergents', NULL, 500, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mr.Giggles dry shampoo (peach) ', N'Detergents', NULL, 200, 38)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mr. Giggels dry shampoo ( cucumber melon)', N'Detergents', NULL, 200, 2)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mr.Giggels dry shampoo (Fuji apple)', N'Detergents', NULL, 200, 42)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N' Papi Groom and bloom shampoo (green)', N'Detergents', NULL, 250, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Groom and bloom shampoo (red)', N'Detergents', NULL, 250, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amitraz shampoo', N'Detergents', NULL, 300, 30)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mycocide shampoo', N'Detergents', NULL, 400, 20)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amitraz soap', N'Detergents', NULL, 130, 45)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coat maintenance soap', N'Detergents', NULL, 130, 46)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Madre de cacao soap ', N'Detergents', NULL, 130, 40)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Biosulfur + VCO soap', N'Detergents', NULL, 130, 24)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hair grower soap', N'Detergents', NULL, 130, 4)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Jerky (Lamb)', N'Pet Food', NULL, 150, 6)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Jerky (Beef)', N'Pet Food', NULL, 150, 11)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Jerky (Chicken)', N'Pet Food', NULL, 150, 11)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Yaho dog chew (Chicken)', N'Pet Food', NULL, 150, 11)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Yufeng biscuit', N'Pet Food', NULL, 200, 4)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Derby 1kg', N'Pet Food', NULL, 90, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pyramid hill (Beef)', N'Pet Food', NULL, 120, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pyramid hill (lamb)', N'Pet Food', NULL, 120, 43)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Daily delight', N'Pet Food', NULL, 120, 19)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sack Topbreed Adult', N'Pet Food', NULL, 1700, 2)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sack Topbreed Puppy', N'Pet Food', NULL, 1800, 3)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Topbreed adult per kilo', N'Pet Food', NULL, 85, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Topbreed puppy per kilo', N'Pet Food', NULL, 90, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sack Maxime Adult', N'Pet Food', NULL, 2200, 3)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sack Maxime Puppy', N'Pet Food', NULL, 2200, 2)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Maxime adult kilo', N'Pet Food', NULL, 220, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Maxime puppy kilo', N'Pet Food', NULL, 220, 11)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Broncure', N'Drug/Medicine', NULL, 300, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Iron syrup 120ml', N'Drug/Medicine', NULL, 300, 22)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ener- G 60ml', N'Drug/Medicine', NULL, 200, 27)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Ob syrup 120ml', N'Drug/Medicine', NULL, 250, 36)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole 15mg/ml (Flavet)', N'Drug/Medicine', NULL, 150, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi MVP 120ml syrup', N'Drug/Medicine', NULL, 250, 17)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coatshine syrup 120ml', N'Drug/Medicine', NULL, 350, 20)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrical syrup 120ml', N'Drug/Medicine', NULL, 300, 29)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Emerplex syrup', N'Drug/Medicine', NULL, 250, 14)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prefolic cee', N'Drug/Medicine', NULL, 400, 18)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Emervit syrup', N'Drug/Medicine', NULL, 250, 76)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline (Lymedox) 60ml', N'Drug/Medicine', NULL, 250, 28)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Livwell', N'Drug/Medicine', NULL, 300, 39)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enrofloxacin (Emerflox)  60ml ', N'Drug/Medicine', NULL, 250, 23)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tolfenamic acid (Tolfenol) 20mg/ml  30ml', N'Drug/Medicine', NULL, 260, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enmalac syrup 120ml ', N'Drug/Medicine', NULL, 300, 34)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC- Doxycycline syrup 120ml', N'Drug/Medicine', NULL, 250, 25)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nacalvit - C syrup 120ml', N'Drug/Medicine', NULL, 300, 24)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Meloxicam 1.5mg/ml (Meloxicam) syrup', N'Drug/Medicine', NULL, 500, 6)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Puppy Nutrigel PASTE', N'Drug/Medicine', NULL, 550, 4)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immune Health ', N'Drug/Medicine', NULL, 10, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy Liver ', N'Drug/Medicine', NULL, 20, 10)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy heart', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy coat', N'Drug/Medicine', NULL, 20, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Allergy- ease tab', N'Drug/Medicine', NULL, 20, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aspirin', N'Drug/Medicine', NULL, 20, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bladder Control', N'Drug/Medicine', NULL, 20, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv.52 tab', N'Drug/Medicine', NULL, 15, 22)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nefrotec tab', N'Drug/Medicine', NULL, 15, 28)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC- Doxycycline tab', N'Drug/Medicine', NULL, 15, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Multivitamins tabs', N'Drug/Medicine', NULL, 10, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Amoxicillin ', N'Drug/Medicine', NULL, 20, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Calcium tabs', N'Drug/Medicine', NULL, 10, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Brewer''s Yeast tab', N'Drug/Medicine', NULL, 5, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Deltacal tab', N'Drug/Medicine', NULL, 15, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Refamol cap', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy heart', N'Drug/Medicine', NULL, 20, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetpramide (metoclopramide) tab', N'Drug/Medicine', NULL, 20, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lactulose syrup 60ml', N'Drug/Medicine', NULL, 250, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin syrup 250mg/5ml (Moxylor) 60ml', N'Drug/Medicine', NULL, 150, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycyline 50mg/ml 60ml syrup', N'Drug/Medicine', NULL, 250, 5)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole 125mg/5ml 60ml syrup', N'Drug/Medicine', NULL, 100, 32)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefalexin 250mg/5ml 60ml syrup', N'Drug/Medicine', NULL, 100, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Co-amoxiclav 250mg/62.5mg/5ml syrup', N'Drug/Medicine', NULL, 250, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone 10mg/5ml (Cort) 60ml syrup', N'Drug/Medicine', NULL, 150, 5)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole 200mg/40mg/5ml syrup (Katherx)', N'Drug/Medicine', NULL, 100, 21)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole 400mg/80mg/5ml syrup (Katherx)', N'Drug/Medicine', NULL, 100, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hemione Iron syrup', N'Drug/Medicine', NULL, 450, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole 500mg/tab', N'Drug/Medicine', NULL, 15, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole 960mg/tab', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metoclopramide ', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone 10mg/tab', N'Drug/Medicine', NULL, 10, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Meloxicam 15mg/tab', N'Drug/Medicine', NULL, 10, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vitamin K 10mg/tab ', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline 100mg/cap', N'Drug/Medicine', NULL, 25, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tobramycin 3mg/mL (0.3 % w/v) - Eye Drops ', N'Drug/Medicine', NULL, 250, 5)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hypomellose 3mg/mL (0.3%)  - Eye Drops ', N'Drug/Medicine', NULL, 300, 3)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tobramycin Dexamethasone (Ramtrex) 3mg/1ml  - Eye Drops ', N'Drug/Medicine', NULL, 250, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Remend  - Eye Drops ', N'Drug/Medicine', NULL, 900, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bioline tearstain  - Eye Drops ', N'Drug/Medicine', NULL, 300, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 eye drops Animal Science  - Eye Drops ', N'Drug/Medicine', NULL, 650, 9)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Eye cleanser Nutri vet  - Eye Drops ', N'Drug/Medicine', NULL, 800, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Otiderm - Ear drops', N'Drug/Medicine', NULL, 400, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 ear drops  - Ear drops', N'Drug/Medicine', NULL, 650, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ear cleanser Nutrivet  - Ear drops', N'Drug/Medicine', NULL, 800, 6)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N' Polymyxin B sulfate, Neomycin, Dexamethasone (Rapidax)  - Ear drops', N'Drug/Medicine', NULL, 250, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 styptic powder', N'Drug/Medicine', NULL, 250, 12)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Hydrocortisone spray', N'Drug/Medicine', NULL, 1200, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furbaby milk', N'Drug/Medicine', NULL, 600, 10)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Topi-derm', N'Drug/Medicine', NULL, 260, 41)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'furfect cream', N'Drug/Medicine', NULL, 220, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Toothpaste beef flavour', N'Drug/Medicine', NULL, 190, 6)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Toothpaste orange flavour', N'Drug/Medicine', NULL, 190, 4)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bioline puppy training', N'Drug/Medicine', NULL, 150, 9)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lavender fragrance perfume', N'Drug/Medicine', NULL, 250, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet dental hygiene kit', N'Drug/Medicine', NULL, 750, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hi-lite', N'Drug/Medicine', NULL, 150, 49)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mondex detrose powder', N'Drug/Medicine', NULL, 65, 20)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline up to 10kg - ANTI-TICK AND FLEAS', N'Drug/Medicine', NULL, 450, 13)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline up to 10-20kg  - ANTI-TICK AND FLEAS', N'Drug/Medicine', NULL, 500, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline up to 20-40kg - ANTI-TICK AND FLEAS', N'Drug/Medicine', NULL, 550, 5)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra >2-3.5kg', N'Drug/Medicine', NULL, 700, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra >3.5-7.5kg', N'Drug/Medicine', NULL, 750, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra > 7.5- 15kg', N'Drug/Medicine', NULL, 800, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra >15-30kg', N'Drug/Medicine', NULL, 850, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra >30-60kg', N'Drug/Medicine', NULL, 900, 8)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate - Cat', N'Drug/Medicine', NULL, NULL, 18)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate - Dog 4kg or less', N'Drug/Medicine', NULL, 450, 16)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate - 4kg to 10kg', N'Drug/Medicine', NULL, 500, 6)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate - 10kg to 25kg', N'Drug/Medicine', NULL, 550, 15)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate - 25kg to 40kg', N'Drug/Medicine', NULL, 600, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto - <4kg ', N'Drug/Medicine', NULL, 1400, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto - 4kg to 10kg', N'Drug/Medicine', NULL, 1400, 2)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto - 10kg-20kg', N'Drug/Medicine', NULL, 1400, 7)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto - 20kg-40kg', N'Drug/Medicine', NULL, 1400, 3)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Defense - 5-10lbs', N'Drug/Medicine', NULL, 500, 1)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Defense - 11-32lbs', N'Drug/Medicine', NULL, 550, 3)
GO
INSERT [dbo].[PetDynasty_ItemImport_20210403] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Defense - Simparica 22.1-44lbs', N'Drug/Medicine', NULL, 600, 2)
GO

DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @ID_Company_PetDynasty INT = 52
DECLARE @InitInventoryImport TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitCost DECIMAL(18, 4) , UnitPrice DECIMAL(18, 4), CurrentInventoryCount int)
DECLARE @InventoryImport TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitCost DECIMAL(18, 4) , UnitPrice DECIMAL(18, 4), CurrentInventoryCount int, ID_Item INT)

INSERT @InitInventoryImport
SELECT  import.Name,
		cat.ID,
		ISNULL(import.[Buying Price],0),
		ISNULL(import.[Selling Price],0),
		ISNULL(import.[Current Inventory Count],0)
FROM    [PetDynasty_ItemImport_20210403] import 
LEFT JOIN tItemCategory cat 
	ON UPPER(import.Category) = UPPER(cat.Name) AND cat.ID_ItemType = 2

INSERT @InventoryImport
SELECT  ItemName, MIN(ID_ItemCategory), UnitCost, UnitPrice, CurrentInventoryCount, 0
FROM    @InitInventoryImport
GROUP BY ItemName, UnitCost, UnitPrice, CurrentInventoryCount


/******************************************************************************************************************************/
 
INSERT tItem (
  ID_Company,
  Name,
  ID_ItemType,
  UnitPrice,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  Comment)
SELECT 
   @ID_Company_PetDynasty
  ,ItemName
  ,@ID_ItemType_Inventoriable
  ,UnitPrice
  , UnitCost
  , ID_ItemCategory
  , NULL
  , 1
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , 'Imported on 2021-04-03 1pm'
 FROM   @InventoryImport

UPDATE @InventoryImport
SET
	ID_Item = item.ID
FROM @InventoryImport import 
	INNER JOIN tItem item on item.Name = import.ItemName
WHERE ID_Company = @ID_Company_PetDynasty AND Comment = 'Imported on 2021-04-03 1pm'


Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory (
	[Code]
	,[ID_Item]
	,[Quantity] 
	,[UnitPrice]
	,[DateExpired]
	,[BatchNo]
	,[ID_FilingStatus]
	,[ID_Company]
	,[Comment]
	,[IsAddInventory]
)
SELECT  
	'Imported on 2021-04-03 1pm'
   ,ID_Item
   ,CurrentInventoryCount
   ,0.00
   ,NULL
   ,NULL
   ,3
   ,@ID_Company_PetDynasty
   ,'Imported on 2021-04-03 1pm'
   ,1
FROM    @InventoryImport 
WHERE
	CurrentInventoryCount > 0
ORDER BY 
	ID_Item

exec pReceiveInventory @adjustInventory, 1




GO





/****** Object:  Table [dbo].[PetDynasty_ItemImport_20210403]    Script Date: 4/3/2021 1:09:34 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PetDynasty_ItemImport_20210403]') AND type in (N'U'))
DROP TABLE [dbo].[PetDynasty_ItemImport_20210403]
GO