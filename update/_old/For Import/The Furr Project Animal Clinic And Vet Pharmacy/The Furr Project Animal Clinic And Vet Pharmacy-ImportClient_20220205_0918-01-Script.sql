DEclare @GUID_Company VARCHAR(MAX) = '33D367F4-AE77-42E6-B253-6D48A0BF8D0B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client             INT,
     CustomCode            Varchar(MAX),
     Name_Client           Varchar(MAX),
     ContactNumber         Varchar(MAX),
     Name_Patient          Varchar(MAX),
     AttendingVeterinarian Varchar(MAX)
  )

INSERT @forImport
SELECT NULL,
       dbo.fGetCleanedString([CODE]),
       dbo.fGetCleanedString([CLIENT'S NAME]),
       dbo.fGetCleanedString([CONTACT #]),
       dbo.fGetCleanedString([PET NAME]),
       dbo.fGetCleanedString([ATTENDING VET])
FROm   [theFurrClientMasterList]

Update @forImport
SET    ContactNumber = '0' + ContactNumber
WHERE  LEN(ContactNumber) = 10

SELECT *
FROm   @forImport

SELECT CustomCode,
       Count(*)
FROm   @forImport
GROUP  BY CustomCode
HAVING COUNT(*) > 1

SELECT Name_Client,
       Count(*)
FROm   @forImport
GROUP  BY Name_Client
HAVING COUNT(*) > 1

SELECT CustomCode
FROm   @forImport
where  CHARINDEX(',', Name_Patient) > 0
        OR CHARINDEX('&', Name_Patient) > 0
        OR CHARINDEX(' AND ', Name_Patient) > 0



SELECT *
FROm   @forImport
WHERE LEN(AttendingVeterinarian) > 0

--INSERT dbo.tClient
--       (ID_Company,
--		CustomCode,
--        Name,
--        ContactNumber,
--        Comment,
--        IsActive,
--        DateCreated,
--        DateModified,
--        ID_CreatedBy,
--        ID_LastModifiedBy)
SELECT @ID_Company,
       CustomCode,
       Name_Client,
       ContactNumber,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   @forImport

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.CustomCode = client.CustomCode
WHERE  ID_Company = @ID_Company 


--INSERT tPatient
--       (ID_Company,
--        ID_Client,
--        Name,
--        Comment,
--        IsActive,
--        DateCreated,
--        DateModified,
--        ID_CreatedBy,
--        ID_LastModifiedBy)
SELECT @ID_Company,
       ID_Client,
       Name_Patient,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   @forImport WHERE LEN (Name_Patient) > 0 

SELECT * FROm tClient where ID_Company = @ID_Company