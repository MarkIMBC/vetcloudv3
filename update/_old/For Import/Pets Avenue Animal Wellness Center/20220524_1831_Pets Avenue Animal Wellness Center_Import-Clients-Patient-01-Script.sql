exec _pBackUpDatabase

GO
DECLARE @GUID_Company VARCHAR(MAX) = '8ADEA02A-F300-4A5D-BDFD-650422F465D0'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client            INT,
     Name_Client          Varchar(MAX),
     CustomCode_Client    Varchar(MAX),
     ContactNumber_Client Varchar(MAX),
     Address_Client       Varchar(MAX),
     ID_Patient           INT,
     Name_Patient         Varchar(MAX),
     CustomCode_Patient   Varchar(MAX),
     Species_Patient      Varchar(MAX),
     BirthDate_Patient    DateTime,
     Color_Patient        Varchar(MAX),
     Gender_Patient       Varchar(MAX),
     Comment_Patient      Varchar(MAX)
  )

INSERT @forImport
       (Name_Client,
        ContactNumber_Client,
        Address_Client,
        Name_Patient,
        CustomCode_Patient,
        Species_Patient,
        BirthDate_Patient,
        Color_Patient,
        Gender_Patient,
        Comment_Patient)
SELECT dbo.fGetCleanedString([`]),
       dbo.fGetCleanedString([Contact No]),
       dbo.fGetCleanedString([ADDRESS]),
       dbo.fGetCleanedString([Pet Name]),
       dbo.fGetCleanedString([Code]),
       dbo.fGetCleanedString([Species ID])
       + CASE
           WHEN LEN(dbo.fGetCleanedString([Species ID])) > 0
                AND LEN(dbo.fGetCleanedString([Breed])) > 0 THEN ' - '
           ELSE ''
         END
       + dbo.fGetCleanedString([Breed]),
       TRY_CONVERT(DateTime, dbo.fGetCleanedString([Pet Date Birth])),
       dbo.fGetCleanedString([Color]),
       dbo.fGetCleanedString([Gender]),
       CASE
         WHEN LEN(dbo.fGetCleanedString([Pet Date Birth])) > 0 THEN 'DateBirth: '
                                                                    + dbo.fGetCleanedString([Pet Date Birth])
                                                                    + CHAR(13) + CHAR(13)
         ELSE ''
       END
       + 'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   [Pets_Avenue_Patient_Record]

Update @forImport
set    BirthDate_Patient = null
WHERE  BirthDate_Patient = '1900-01-01 00:00:00.000'

Update @forImport
SET    ContactNumber_Client = '0' + ContactNumber_Client
WHERE  ContactNumber_Client LIKE '9%'

DELETE FROM @forImport
WHERE  LEN(Name_Patient) = 0
       AND LEN(Name_Client) <= 1

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company
       AND IsActive = 1

Update @forImport
SET    ID_Patient = patient.ID
FROM   @forImport import
       INNER JOIN tPatient patient
               on patient.ID_Client = import.ID_Client
WHERE  patient.ID_Company = @ID_Company
       AND IsActive = 1

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        Address,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber_Client,
                Address_Client,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
where  ID_Client IS NULL
       and LEN(Name_Client) > 0

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company
       AND IsActive = 1

INSERT INTO [dbo].[tPatient]
            ([ID_Company],
             [ID_Client],
             [Name],
             [CustomCode],
             [DateBirth],
             [Comment],
             [ID_Gender],
             [Species],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                CustomCode_Patient,
                BirthDate_Patient,
                Comment_Patient,
                CASE
                  WHEN Gender_Patient LIKE 'M%' THEN 1
                  ELSE
                    CASE
                      WHEN Gender_Patient LIKE 'F%' THEN 2
                      ELSE NULL
                    END
                END,
                Species_Patient,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
WHERE  ID_Client IS NOT NULL
       AND ID_Patient IS NULL

SELECT *
FROM   @forImport
WHERE  ID_Client IS NULL
        OR ID_Patient IS NULL
Order  by Name_Client 
