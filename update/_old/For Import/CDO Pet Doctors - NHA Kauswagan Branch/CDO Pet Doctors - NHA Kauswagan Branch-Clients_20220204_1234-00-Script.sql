DEclare @GUID_Company VARCHAR(MAX) = '0E366C14-B7CE-482A-BD8B-12BFA4BE321B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client     INT,
     Name_Client   Varchar(MAX),
     ID_Patient    Varchar(MAX),
     Name_Patient  Varchar(MAX),
     ContactNumber Varchar(MAX)
  )
DECLARE @forImportClient TABLE
  (
     Name_Client   Varchar(MAX),
     ContactNumber Varchar(MAX)
  )

INSERT @forImport
SELECT NULL,
       dbo.fGetCleanedString([Client Name]),
       NULL,
       dbo.fGetCleanedString([Pet Name]),
       dbo.fGetCleanedString([Mobile Phone])
FROm   [CDO Pet Doctors - NHA Kauswagan Branch Client Patient]

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

Update @forImport
SET    ID_Patient = patient.ID
FROm   @forImport import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company AND patient.ID_Company= @ID_Company 

SELECT DISTINCT client.ID,
                client.Name,
                patient.ID,
                patient.Name
FROm   @forImport import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
WHERE  ID_Client IS NULL

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
WHERE  LEN (Name_Patient) > 0
       and ID_Patient IS NULL

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

SELECT Name_Client,
       COUNT(*) Count
FROm   @forImportClient
GROUP  BY Name_Client
HAVING COUNT(*) > 1

SELECT *
FROM   tClient client
WHERE  ID_Company = @ID_Company

SELECT *
FROM   tPatient patient
WHERE  ID_Company = @ID_Company

SELECT Name_Client,
       Name
FROM   vPatient
WHERE  Comment LIKE 'Imported 02/04/2022%'
Order  by Name_Client,
          Name
/*
DELETE FROM tClient WHERE Comment LIKE 'Imported 02/04/2022%'
DELETE FROM tPatient WHERE Comment LIKE 'Imported 02/04/2022%'
*/
