/****** Object:  Table [dbo].[Pawsome_ImportInventory_20210315]    Script Date: 3/15/2021 11:12:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pawsome_ImportInventory_20210315]') AND type in (N'U'))
DROP TABLE [dbo].[Pawsome_ImportInventory_20210315]
GO

CREATE TABLE [dbo].[Pawsome_ImportInventory_20210315](
	[Ref NO] [float] NULL,
	[Item] [nvarchar](255) NULL,
	[CurrentInventoryCount] [float] NULL,
	[EXPIRY] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13993, N'Accelac Lactulose 3.35 mg syrup', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13982, N'ADVOCATE FOR CATS (PER PIECE) IMADACLOPRID/MOXIDECTIN PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13914, N'ADVOCATE IMIDACLOPRID/MOXIDECTIN PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14142, N'Advocate Spot on Solution per box', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14141, N'Advocate spot-on per piece Spot on Solution .04ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14243, N'Ambidol Tramadol HCl Ampule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14167, N'Anigen CHW Ag 2.0 Heartworm test Test Kit', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14239, N'Anigen CPV/CCV/Giardia CPV test kit test kit', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14240, N'Anigen FIV Ab/FeLV Ag FIV/FeLV test Test kit', 6, N'08/13/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14203, N'Anti-Rabies Defensor Anti-Rabies VIAL', 18, N'07/20/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14203, N'Anti-Rabies Defensor Anti-Rabies VIAL', 50, N'10/05/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14328, N'Apoquel 5.4mg Oclatinib tablet', 64, N'07/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13997, N'Axmel Amoxicillin 250 mgPowder for Suspension', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14000, N'B-Lact Lactulose syrup', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13916, N'BAYTRIL 50MG ENROFLOXACIN 50MG TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14269, N'Baytril Enrofloxacin 50mg', 163, N'01/01/25')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14207, N'Bravecto  4.5 to 10kg Fluralaner 250mg', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13968, N'BRAVECTO 10-20KG FLURALANER TABLET', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14301, N'Bravecto 20-40kg Fluralaner 1000mg', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13969, N'BRAVECTO 20-40KG FLURALANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13967, N'BRAVECTO 4.5-10KG FLURALANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13915, N'BROADLINE 2.5-7.5KG FIPRONIL/EPRINOMECTIN/PRAZI PIPETTE', 24, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14148, N'Broadline Box Cat Spot On Solution 74.7 mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14272, N'Broadline per box Spot ON Solution for Cats 3x0.9ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14149, N'Broadline Per Piece FIPRONIL/PRAZIVET 74.7 mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14271, N'Broadline per piece Spot ON Solution for Cats .9ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13983, N'BROADLINE SPOT-ON (PER PIECE) FIPRONIL/PRAZIVET PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13922, N'BRONCHICINE KENNEL COUGH 1ML VIAL', 5, N'04/26/23')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14221, N'Bronchicine Kennel Cough VIAL', 50, N'07/26/23')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13944, N'BRONCURE LAGUNDI BOTTLE', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14198, N'Broncure Respiratory Strength 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14095, N'BRUCELLA TEST KIT TEST KIT', 14, N'05/29/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14206, N'Caniderm Ketoconazole Antifungal Shampoo 20mg', 25, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14283, N'Caniderm Ketoconazole Shampoo 100ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13965, N'CANIDERM SHAMPOO TUBE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14253, N'Canipril 5mg Benazepril Hydrochloride 5mg', 434, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14337, N'Canipril 5mg Benazepril Hydrochloride 5mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14217, N'Canipril 5mg Benazepril Hydrochloride Tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14048, N'CANIPRIL 5MG BENAZEPRIL HYDROCHLORIDE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14135, N'CANIPRIL 5MG BENAZEPRIL HYDROCHLORIDE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14160, N'Cardiac  Dog Can 410 g Dog Food Can 410 g', 32, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14036, N'CARDIAC BAG DOG FOOD BAG', 8, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14029, N'CARDIAC CAN DOG FOOD CAN', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14248, N'Cardiac Can Dog Food Can 410g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14344, N'Cardiac Can Dog Food Can 410g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14094, N'Cardiac Dog Can Dog Food 410g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14088, N'CERENIA <0.5ML MAROPITANT SOLUTION', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14175, N'Cerenia <0.5ml Maropitant Solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14174, N'Cerenia 0.6-1ml Maropitant Solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14089, N'CERENIA 0.6ml-1ml MAROPITANT SOLUTION', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14340, N'Ceticit Cetirizine 10mg Cetirizine 10mg 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14128, N'Cetrigna Cetirizine Hydrochloride 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13989, N'Cetzy 10 Cetirizine 10MG Tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14050, N'CHLORASONE CHLORAMPENICOL  HYDROCORTISONE OINTMENT', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14176, N'Clerapliq Eye Drops Solution', 36, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13995, N'Clovimed Co- Amoxiclav 250 mg Powder for Suspension', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14179, N'Co-Amoxisaph 250 Co Amoxiclav 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13955, N'COATSHINE COAT SUPPLEMENT BOTTLE', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14155, N'COMPEHENSIVE PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13924, N'COMPREHENSIVE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14305, N'Comprehensive Profiel (Blood chem) Blood Chem Rotor', 11, N'10/20/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14236, N'Comprehensive Profile Blood Chem Rotor', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14071, N'COMPREHENSIVE PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14084, N'COMPREHENSIVE PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14011, N'CRITICAL CARE BLOOD CHEM rotor', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14006, N'CYTOPOINT 10MG LOKIVETMAB SOLUTION', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14189, N'Cytopoint 10mg Lokivetmab vial', 4, N'12/15/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14238, N'Cytopoint 20mg Lokivetmab Vial', 2, N'09/28/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14008, N'CYTOPOINT 20MG LOKIVETMAB VIAL', 1, N'01/05/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14009, N'CYTOPOINT 30MG LOKIVETMAB VIAL', 2, N'09/22/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14139, N'CYTOPOINT 40MG LOKIVETMAB VIAL', 1, N'09/22/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14139, N'CYTOPOINT 40MG LOKIVETMAB VIAL', 2, N'05/09/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14332, N'Defensor (Anti-Rabies) Anti-Rabies vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14140, N'DEFENSOR ANTI-RABIES VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14082, N'DEFENSOR ANTI-RABIES VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14096, N'DEFENSOR ANTI-RABIES VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13923, N'DEFENSOR ANTI-RABIES VIALS', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14073, N'Dental Kit Oxyfresh KIt', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14108, N'Dentastix  Small Dentastix 75g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14125, N'Dentastix Large Dentastix 112g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14110, N'Dentastix Large Dentastix pack', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14109, N'Dentastix Medium Dentastix 98g', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14124, N'Dentastix Puppy Dentastix 56g', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14123, N'Dentastix Toy Dentastix 60g', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14319, N'Depo-medrol 0.5ml Methylprednisolone Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14320, N'Depo-medrol 1ml Methylprednisolone Solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14157, N'Dermacomfort Mini 3kg Dog Food Dry 3kg', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14060, N'DERMACOMFORT MINI DOG FOOD 3KG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13985, N'Derpson Prednisone 5mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14286, N'Diacef 250mg caps Cefalexin 250mg caps 250mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14291, N'Diacef 250mg Susp Cefalexin 250mg Susp 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13988, N'Diacef Cefalexin 250 mg Capsule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13999, N'Diahist Diphenhydramine 12.5 mg syrup', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14130, N'Diahist Diphenhydramine Hydrochloride 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13928, N'DIGESTIVE ENZYMES PER PIECE PROBIOTIC TABLET', 78, N'05/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14051, N'DIGESTIVE ENZYMES PER PIECE PROBIOTICS TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14262, N'Digestive Enzymes Probiotic 180g bottle', 1, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14263, N'Digestive Enzymes Probiotic 180g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14264, N'Digestive Enzymes Probiotic 180g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14285, N'Dogui Oral Bad Breath Masker 100ml', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14251, N'Doxycycline Hyclate Doxyvet 10 syrup 100ml', 13, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13941, N'DOXYVET 10 CAPLET DOXYCYCLINE CAPLET', 371, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14211, N'Doxyvet 10 caps Doxycyline Hyclate 100mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14252, N'Doxyvet 10 capsule Doxycyline Hyclate capsule 100mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14074, N'Doxyvet 10 Doxycycline Hyclate 100 mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13913, N'DOXYVET 10 SYRUP DOXYCYCLINE 100ML SYRUP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14076, N'Doxyvet 10 Syrup Doxycyline Hyclate Anti-infective Syrup 100ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14213, N'Doxyvet 10 Syrup Doxycyline Hyclate Anti-infective Syrup 100ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14193, N'Drontal 1 1/2 tab (dewormer) Febantel tablet', 50, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14192, N'Drontal 1 tab (dewormer) Febantel tablet', 80, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14191, N'Drontal 1/2 tab (dewormer) Febantel tablet', 90, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14195, N'Drontal 2 1/2 tabs (dewormer) Febantel Tablet', 30, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14194, N'Drontal 2 tabs (dewormer) Febantel tablet', 40, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14196, N'Drontal 3 tabs (dewormer) Febantel Tablet', 25, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14310, N'Duoderm Extra Thin Wound Dressing Patch', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14298, N'E Collar 10cm E Collar 10cm', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14333, N'E Collar 12.5 cm E Collar 12.5', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14297, N'E Collar 20cm E Collar 20cm', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14204, N'E-collar Size 12.5 E-collar Collar', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14205, N'E-collar size 15 E-collar Collar', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14049, N'EARMITICIDE PYRETHRIN BOTTLE', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14044, N'EHR-BAB-ANA COMBO TEST KIT TEST KIT KITS', 13, N'02/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14329, N'Enmalac Papi Enmalac 60ml', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14244, N'Epinephrine Epinephrine Ampule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14288, N'Exel 500 mg caps Cefalexin 500mg caps 500mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13996, N'Exel Cefalexin 250 mg Powder for Suspension', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13987, N'Exel Cefalexin 250mg Capsule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14133, N'Exel Cefalexin 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13970, N'EYE VITAMIN DROPS EYE DROPS DROPS', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14178, N'Ezaller 10mg Cetirizine 10mg 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14070, N'Felocell CAT 4 in 1 Vaccine', 25, N'06/14/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14154, N'FELOCELL CAT 4 in 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13950, N'FELOCELL CAT 4 IN 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14199, N'Felocell Cat 4 in 1 vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14200, N'Felocell Cat 4 in 1 vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13930, N'FELOCELL RCCP (Cat 4 in1) VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14177, N'Finasia Finasteride 5mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14126, N'Finasia Finasteride 5mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14161, N'Fit 32  2kg Cat Food Dry 2kg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14245, N'Fit 32 2kg Cat Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14343, N'Fit 32 2kg Cat Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14039, N'FIT 32 CAT FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13991, N'Flagex Metronidazole 500 mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14131, N'Flagex Metronidazole 500mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14273, N'Frontline Spray Fipronil 100ml', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14047, N'FRONTLINE LARGE PER PIECE FIPRONIL PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14046, N'FRONTLINE MEDIUM PER PIECE FIPRONIL PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13939, N'FRONTLINE PLUS 10-20KG FIPRONIL PIPETTE', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13940, N'FRONTLINE PLUS 20-40KG FIPRONIL PIPETTE', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13938, N'FRONTLINE PLUS UP TO 10KG FIPRONIL PIPETTE', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14182, N'Frontline Small per box Fipronil  S Methoprene 98mg/ 88mg per ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14181, N'Frontline Small per piece Fipronil  S Methoprene 98. 88mg per ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14045, N'FRONTLINE SMALL PER PIECE FIPRONIL PIPETTE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13971, N'FRONTLINE SPRAY FIPRONIL BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14339, N'Furosemide Diuretic 20mg Furosemide 20mg 20mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14290, N'Fusedex 40mf tabs Furosemide 40mg tabs 40mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14127, N'Fusedex Furosemide 40mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14323, N'Gastro Intestinal Bag Dog Food Dry 2kg', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14314, N'Gastro Intestinal Can Dog Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14295, N'Gastro Intestinal Can Dog Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14249, N'Gastro Intestinal Can Dog Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14037, N'GASTROINTESTINAL  CAN DOG FOOD CAN', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14038, N'GASTROINTESTINAL BAG DOG FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14033, N'GASTROINTESTINAL LOWFAT bag DOG FOOD BAG', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13981, N'GI LOWFAT CAN DOGFOOD CAN', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13984, N'GLUCOSAMIN PLUS JOINT SUPPLEMENT TABLET', 34, N'2023-03-01')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13962, N'GLUCOSAMINE PLUS JOINT SUPPLEMENT BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14010, N'GLUCOSAMINE PLUS JOINT SUPPLEMENT TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13963, N'GLUCOSAMINE PLUS PER PIECE JOINT SUPPLEMENT TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14312, N'Halo Beef Stew Can Dog Food Can 5.5oz', 10, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14234, N'Halo Holistic Beef Stew Dog Food Can 5.5oz', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14233, N'Halo Holistic Chicken and Chicken Liver small breed Dog Food Dry 4lbs', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14235, N'Halo Holistic Lamb Stew Dog Food Can 5.5oz', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14232, N'Halo Holistic White Salmon and Whitefish  small breed Dog Food Dry 4lbs', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14311, N'Halo Lamb Stew Can Dog Food Can 5.5oz', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13961, N'HEALTH LIVER PER PIECE LIVER TONIC BOTTLE', 446, N'09/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13960, N'HEALTHY LIVER LIVER TONIC BOTTLE', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14146, N'Heartgard  Brown Box Ivermectin  Pyrantel 272mcg/227mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14144, N'Heartgard Blue Box Ivermectin  Pyrantel 68 mcg/57mg', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13945, N'HEARTGARD BLUE IVERMECTIN/PYRANTEL CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14254, N'Heartgard Blue Ivermectine Pyrantel 68mcg/57mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14255, N'Heartgard Blue Ivermectine Pyrantel 68mcg/57mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14145, N'Heartgard Blue per Piece Ivermetin  Pyrantel 68 mcg/57mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14012, N'Heartgard Blue/box Ivermectin  Pyrantel Chewable Cube', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14013, N'Heartgard Blue/pc Ivermectin  Pyrantel Chewable Cube', 68, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13947, N'HEARTGARD BROWN IVERMECTIN/PYRANTEL CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14147, N'Heartgard Brown Per Piece Ivermectin  Pyrantel 272mcg/227mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14078, N'HEARTGARD BROWN PER PIECE Ivermectin  Pyrantel CHEWABLE NUGGET', 44, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13946, N'HEARTGARD GREEN IVERMECTIN/PYRANTEL CHEWABLE TABLET', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14015, N'Heartgard Green/ pc Ivermectin  Pyrantel Chewable Cube', 36, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14014, N'Heartgard Green/box Ivermectin  Pyrantel Chewable Cube', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14284, N'Hemacare -Fe Ferrou Sulfate  Folic Acid ??? B12 120ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14065, N'HEMACARE IRON SUPPLEMENT SYRUP', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14276, N'Hepatic 1.5 kg Dog Food Dry 1.5kg', 9, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14163, N'Hepatic 1.5kg Dog Food Dry 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13979, N'HEPATIC 1.5KG DOGFOOD BAG/PELLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14031, N'HEPATIC CAN DOG FOOD CAN', 17, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14294, N'Hepatic Can Dog Food Can 420g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14093, N'Hepatic Canine Dog Food 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14132, N'Histomox Diphenhydramine Hydrochloride 50ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14041, N'HYPOALLERGENIC BAG DOG FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14299, N'IMMITICIDE 1 bottle MELARSOMINE solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14300, N'IMMITICIDE 1/2 bottle MELARSOMINE solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13937, N'IMMUNOL IMMUNOMODULATOR BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14212, N'Immunol Liquid Immunomodulator Anti Infective 100ml', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14250, N'Indoor 27 2kg Cat Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14292, N'Indoor 27 2kg Cat Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14159, N'Indoor 27 2kg Dog Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14059, N'INDOOR 27 CAT FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13908, N'INFLACAM MELOXICAM 10ML SUSPENSION', 1, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14186, N'Inflacam per bottle Meloxicam 1.5mg/ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14064, N'Inflacam per Bottle Meloxicam 10ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14187, N'Inflacam per ml Meloxicam 1.5mg/ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14063, N'Inflacam per ml Meloxicam 10 ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14214, N'Inoclav Co Amoxiclav 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14338, N'Inoclav Co Amoxiclav 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14287, N'Inoclav susp Co Amoxiclav susp 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13936, N'K9 EAR DROPS EAR DROPS BOTTLE', 9, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14138, N'K9 Eye Drops Eye Drops 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13932, N'K9 EYE DROPS EYE DROPS BOTTLE', 12, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13909, N'KAWU CALCIUM TABLET', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14277, N'Kennel Cough (Bronchicine) Kennel Cough vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14278, N'Kennel Cough (Bronchicine) Kennel Cough vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13964, N'KETOCONAZOLE KETOCONAZOLE TABLET', 212, N'08/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14209, N'Ketoconazole Ketoconazole Tablet 200mg bottle', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14306, N'Kidney Profile (Blood chem) Blood Chem Rotor', 4, N'10/19/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14237, N'Kidney Profile Blood Chem Rotor', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13956, N'KIDNEY PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14054, N'KIDNEY PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14075, N'LC Vit Multivitamins ??? A D3, B complex, C  Lysine 120 ml', 18, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14143, N'LC VIt Syrup Multivitamins ??? A D3, B complex, C  Lysine 120ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14043, N'LC-DOX TABLET DOXYCYLINE TABLET', 85, N'10/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13912, N'LC-VIT MULTIVITAMINS SYRUP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13994, N'Lefesone Prednisone 10 mg suspension', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14318, N'Leptospirosis Test kit Lepto test kit testt kit', 10, N'02/09/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14068, N'LIV 52 DROPS LIVER TONIC SUSP.', 10, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13927, N'LOVE DROPS COAT SUPPLEMENT BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14003, N'LOVE DROPS PER PIECE COAT SUPPLEMENT TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14134, N'Luvmox Sf Co Amoxiclav 60ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13929, N'MAMMALIAN LIVER BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14307, N'Mammalian Liver Profile (Blood chem) Blood Chem Rotor', 4, N'11/30/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13959, N'MAMMALIAN LIVER PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14085, N'MAMMALIAN LIVER PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14152, N'MAMMALIAN LIVER PROFILE BLOOD CHEMISTRY ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14317, N'Metrogen Metronidazole 60ml', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14042, N'METROGEN METRONIDAZOLE SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14158, N'Mini Indoor Adult 1.5 kg Dog Food Dry 1.5 kg', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14055, N'MINI INDOOR ADULT DOGFOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14331, N'Mondex D Glucose  MOnohydrate 100g', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14289, N'Moxylor Susp Amoxicillin Susp 60mk', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14062, N'MYCOCIDE SHAMPOO 150ml bottle', 19, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13954, N'MYCOCIDE SHAMPOO BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14188, N'Mycocide Shampoo Miconazole Nitrate 150ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14315, N'Mycocide Shampoo Miconazole Nitrate 150ml', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13942, N'NEFROTEC DS per bottle URINARY ANTISEPTIC BOTTLE', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14282, N'Neovax Anti Inflamatory, Anti Bacterial 20g', 10, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14077, N'Neovax Wound ointment 20 g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14220, N'Neovax Wound Ointment 20g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14067, N'NEOVAX WOUND OINTMENT OINTMENT', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14197, N'Normal Saline Solution IV Fluids Solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13998, N'Novacee Ascorbic Acid 100 mg syrup', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14166, N'Nutri Plus Gel Multivitamins 120,5g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14270, N'Nutri Plus Gel Mutivitamins 120.5g', 19, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13966, N'NUTRICAL CALCIUM SUPPLEMENT BOTTLE', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13910, N'NUTRIPLUS GEL MULTIVITAMINS 120G TUBE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13935, N'NUTRIVET EAR CLEANSE EAR CLEANSE BOTTLE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14098, N'Nutrivet Ear Clease Nutrivet Ear Cleanse 118ml', 7, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13933, N'NUTRIVET EYE RINSE EYE RINSE BOTTLE', 19, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14316, N'Otiko Ear Drops Ear Drops 15ml', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14330, N'Papi OB Syrup Vitamin Minerals 120ml', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14116, N'Pedigree  Adult Chicken Can Dog Food Can 1.15kg', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14120, N'Pedigree  Puppy Chicken  Egg and MIlk Dry Dog Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14119, N'Pedigree  Puppy Milk Flavor Dry Dog Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14115, N'Pedigree Adult Beef Can Dog Food Can 1.15kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14117, N'Pedigree Adult Chicken Can Dog Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14122, N'Pedigree Adult Chicken veg flavor Dry Dog Food Dry 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14121, N'Pedigree Aduly Beef and Veg Flavor Dry Dog Food Dry 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14107, N'Pedigree Beef and Gravy Pouch Dog Food Pouch 80g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14114, N'Pedigree Can Chicken Dog Food Can 700g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14118, N'Pedigree Puppy Liver, Veg and Milk Dry Dog Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14201, N'Pet Passport Pet Passport Passport', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14069, N'Pet Tabs MULTIVITAMINS tablet bottle', 2, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13920, N'PET-TABS MULTIVITAMINS TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14183, N'Petdelyte Dextrose Sodium Bicarbonate 50 grams', 43, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13957, N'PETDELYTE ELECTROLYTES SACHET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14296, N'Petdelyte Water Soluble Powder Electrolutes 50g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14190, N'PetX FIV/FeLV test FIV/FeLV test test kit', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14086, N'PHENOBARBITAL PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14007, N'PHENOBARBITAL PROFILE ROTOR ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14242, N'Phytobas Phytomenadione Ampule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14218, N'Pimodin 1.25mg Pimobendan 1.25mg tablet', 71, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14280, N'PImodin 5mg Pimobendan 5mg', 459, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14219, N'Pimodin 5mg Pimobendan 5mg tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14304, N'Prep Profile (Blood Chem) Blood Chem Rotor', 12, N'02/18/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14308, N'Prep Profile Blood Chem Rotor', 2, N'12/22/21')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13925, N'PREP PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14156, N'PREP PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14260, N'Previcox 227mg Firocoxib 227mg', 77, N'09/01/23')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13951, N'PREVICOX 227MG FIROCOXIB TABLET bottle', 1, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13952, N'PREVICOX 227MG FIROCOXIB TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14150, N'Previcox 57mg Firocoxib 57 mg', 13, N'02/01/24')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13953, N'PREVICOX 57MG FIROCOXIB TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14019, N'PROHEART <5KGS (0.05-0.25ML) MOXIDECTIN SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14021, N'PROHEART 10.1 - 15KG (0.51-0.75ML) MOXIDECTIN SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14022, N'PROHEART 15.1-20KG (0.76-1ML) MOXIDECTIN SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14023, N'PROHEART 20.1-25KG (1.05-1.25ML) MOXIDECTIN SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14020, N'PROHEART 5.1-10 KG (0.26-0.5ML) MOXIDECTIN SUSP', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14165, N'Pug 1.5 kg Dog Food Dry 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14313, N'Pug Adult 1.5 kg Dog Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14004, N'Pupaholic Charcoal Pad LARGE Wiwi Pad Pad', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14005, N'Pupaholic Charcoal Pads SMALL Wiwi Pad Pads', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14056, N'PUPPY PROTECH DOG 300G PUPPY FORMULA 300G', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14261, N'Purevax (4 in 1) Cat 4 in 1 Vial', 17, N'02/07/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14261, N'Purevax (4 in 1) Cat 4 in 1 Vial', 25, N'06/14/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14241, N'Ranirex Ranitidine HCl Ampule', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13976, N'RC CARDIAG 2KG BAG DOGFOOD BAG/PELLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14324, N'RC Hypoallergenic Dog Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14275, N'RC Mini Indoor Adult Dog Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13974, N'RC MINI INDOOR PUPPY DOGFOOD BAG/PELLETS', 9, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13972, N'RC RECOVERY CAN PETFOOD CAN', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13926, N'RC SHIH TZU ADULT 1.5KG DOGFOOD - DRY 1.5KG BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14274, N'RC Shihtzu Adult 1.5kg Dog Food Dry 1.5kg', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14322, N'RC Shihtzu Puppy Dog Food Dry 2kg', 10, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13973, N'RC STARTER MOUSSE DOGFOOD CAN', 8, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13978, N'RC URINARY  S/O 2KG DOGFOOD BAG/PELLET', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13977, N'RC URINARY S/O 2KG DOGFOOD BAG/PELLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14162, N'Recovery  Can 195g Dog Food Can 195g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14216, N'Recovery Can Cat/Dog Food Can 195g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14247, N'Recovery Can Dog Food Can 195g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14341, N'Recovery Can Dog Food Dry 195g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14091, N'Recovery Can Pet Food 195g', 12, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14058, N'RECOVERY PET FOOD 195G', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14028, N'RENAL 2KG DOG FOOD BAG', 8, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14246, N'Renal 2kg Dog Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14321, N'Renal 2kg Dog Food Dry 2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14030, N'RENAL CAN DOG FOOD CAN', 21, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14293, N'Renal Can Dog Food Can 410g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13990, N'Saphzine Cetirizine 10 mg tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14057, N'SHIH TZU ADULT 7.5kg RC 7.5 KG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14040, N'SHIH TZU ADULT DOG FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14034, N'SHIH TZU JUNIOR DOG FOOD BAG', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14164, N'Shihtzu Adult 7.5kg Dog Food Dry 7.5  kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14265, N'Simoarica !0mg Box Sarolaner Chewables 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14336, N'Simoarica !0mg Box Sarolaner Chewables 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14017, N'Simparica  20mg per piece n/a Chewables', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14257, N'Simparica 10mg Box Sarolaner Chewables 10mg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14256, N'Simparica 10mg pc Sarolaner Chewables 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14335, N'Simparica 10mg pc Sarolaner Chewables 10mg', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14002, N'SIMPARICA 10MG PER BOX SAROLANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14137, N'Simparica 10mg per Box Simparica 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14266, N'Simparica 10mg per pc Sarolaner Chewables 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14001, N'SIMPARICA 10MG per piece SAROLANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14136, N'Simparica 10mg per piece SIMPARICA 10mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13918, N'SIMPARICA 10MG SAROLANER CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14016, N'SIMPARICA 20mg per box N/A Chewables', 1, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14169, N'Simparica 20mg per box Sarolaner Chewables 20mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14168, N'Simparica 20mg per piece Sarolaner Chewables 20mg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13943, N'SIMPARICA 20MG SAROLANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13919, N'SIMPARICA 40MG per box SAROLANER CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14171, N'Simparica 40mg per Box Sarolaner Chewables 40mg', 6, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14170, N'Simparica 40mg per piece Sarolaner Chewables 40mg', 20, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14066, N'SIMPARICA 40MG PER PIECE SAROLANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14259, N'Simparica 80mg box Ivermectine Pyrantel 80mg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14258, N'Simparica 80mg pc Sarolaner Chewables 80mg', 9, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14079, N'SIMPARICA 80MG PER BOX SAROLANER CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14080, N'SIMPARICA 80MG PER PIECE SAROLANER CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14072, N'SIMPARICA 80mg per piece SAROLANER TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13949, N'SIMPARICA 80MG SAROLANER CHEWABLE TABLET', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14087, N'T4/CHOL BLOOD CHEM ROTOR', 1, N'12/2/2021')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14268, N'T4/Chol Blood Chem Rotor', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14309, N'T4/Chol blood Chem rotor', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14151, N'T4/CHOL BLOOD CHEMISTRY ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14053, N'T4/CHOL PROFILE BLOOD CHEM ROTOR', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14184, N'Temvet Buprenorphine <0.5ml solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14185, N'Temvet Buprenorphine 0.6 - 1ml solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13917, N'test1 test1 test1', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14018, N'Tolfedine Tolfenamic Acid tablet', 3, N'07/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14083, N'TOLFINE TOLFENAMIC ACID SOLUTION', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13934, N'TRICIN EYE & EAR OINTMENT TUBE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14153, N'Tricin Eye and Ear ointment 4g', 15, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14210, N'Tricin Eye ans Ear Ointment 4g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13958, N'TROY NUTRIPET MULTIVITAMINS TUBE', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14035, N'URINARY S/O CAN DOG FOOD CAN', 22, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13975, N'URINARY S/O CAN DOGFOOD CAN', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13980, N'URINARY S/O FELINE 1.5KG CATFOOD BAG/PELLET', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14092, N'Urinary S/o Feline Cat Food 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14215, N'Urinary S/O Feline Dry Cat Food Dry 1.5 kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14342, N'Urinary S/O Feline Dry Cat Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14061, N'URINARY S/O FELINE POUCH CAT FOOD 85 G', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14032, N'URINARY S/O SMALL DOG DOG FOOD BAG', 21, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14090, N'Urinary SO Canine Small Dog Dog Food 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13992, N'Uromid Furosemide tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14202, N'VANGUARD 5 PLUS L4 L4 (8 in 1) VIAL', 49, N'02/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13931, N'VANGUARD 5 PLUS L4 L4 VIAL', 25, N'03/22/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14303, N'Vanguard Plus 5 CVL (6 in 1) 6 in 1 solution', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14227, N'Vanguard Plus 5 L4 (8 in 1) L4 ( 8 in 1) Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14302, N'Vanguard Plus 5 L4 (8 in 1) L4 (8 in 1) vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14228, N'Vanguard Plus 5 L4 L4 ( 8 in 1) Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14229, N'Vanguard Plus 5 L4 L4 ( 8 in 1) Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14226, N'Vanguard Plus 5 L4 L4 ( 8 in 1) Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14225, N'Vanguard Plus 5/CV-L (6 in 1) 6 in 1 Vial', 25, N'03/01/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14172, N'Vanguard Plus 5/CV-L 6 in 1 vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14097, N'VANGUARD PLUS 5/CVL 6 IN 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14027, N'VANGUARD PLUS 5/CVL 6 in 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14279, N'Vanguard Plus 5/L (5 in 1) 5 in 1 Vial', 37, N'06/14/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13921, N'VANGUARD PLUS 5/L 5 in 1 1ML VIALS', 25, N'10/15/22')
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14081, N'VANGUARD PLUS 5/L 5 IN 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14334, N'Vanguard Plus 5L (5 in 1) 5 in 1 Suspension', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14222, N'Vanguard Plus 5L (5 in 1) 5 in 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14223, N'Vanguard Plus 5L 5 in 1 vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14224, N'Vanguard Plus 5L 5 in 1 vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14173, N'Vanguard Plus 5L 5 in 1 Vial', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14025, N'VANGUARD PLUS 5L4 8 in 1 VIAL', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13948, N'VETNODERM CREAM ANTIBACTERIAL CREAM BOTTLE', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13911, N'VETNODERM HERBAL SOAP HERBAL SOAP SOAP', 5, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14026, N'VETOIL OMEGA-3 FATTY ACIDS SOFT-GEL CAPSULES', 16, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14231, N'Vigor and Sage Ginseng Well Being Regular Adult Dog Food Dry 2kg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14230, N'Vigor and Sage Lily Root Beauty small breed Dog Food Dry 2kg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14129, N'Vonwelt 10mg Prednisone 10mg tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (13986, N'Vonwelt 5mg Prednisone 5mg tablet', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14052, N'WATER ADDITIVE DENTAL CARE SOLUTION', 11, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14099, N'Whiskas  Junior Ocean Fish Dry Cat Food Dry 1.1kg', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14103, N'Whiskas  Junior Tuna Pouch Cat Food Pouch 85g', 17, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14101, N'Whiskas Adult Ocean Fish Dry Cat Food Dry 1.2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14112, N'Whiskas Adult Ocean Fish Dry Cat Food Dry 1.2kg', 4, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14111, N'Whiskas Adult Ocean Fish Pouch Cat Food Pouch 85g', 16, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14104, N'Whiskas Adult Ocean Fish Pouch Cat Food Pouch 85g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14105, N'Whiskas Adult Tuna Can Cat Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14113, N'Whiskas Adult Tuna Dry Cat Food Dry 1.2kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14102, N'Whiskas Junior Mackerel Pouch Cat Food Pouch 85g', 22, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14100, N'Whiskas Junior Ocean Fish Dry Cat Food Dry 1.1kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14326, N'Whiskas Ocean Fish 1.5kg Cat Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14180, N'Whiskas Ocean Fish 400g can Cat Food Can 400g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14106, N'Whiskas Ocean Fish Can Cat Food Can 400g', 3, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14325, N'Whiskas Tuna 1.5 kg Cat Food Dry 1.5kg', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14327, N'Whiskas Tuna Can Cat Food Can 400g', 20, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14281, N'Xiclospor Cyclosporin Opthalmic Ointment 2mg', 15, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14208, N'Xiclospor Cyclosporin Opthalmic Ointment 5g', 0, NULL)
GO
INSERT [dbo].[Pawsome_ImportInventory_20210315] ([Ref NO], [Item], [CurrentInventoryCount], [EXPIRY]) VALUES (14024, N'XICLOSPOR CYCLOSPORINE OINTMENT', 0, NULL)

GO

Declare @Pawsomespace_ID_Company INT = 51
Declare @ItemInventory TABLE (ID_Item INT, CurrentInventoryCount INT, DateExpired DateTime)

INSERT @ItemInventory
SELECT item.ID, ISNULL(record.CurrentInventoryCount,0) ,CONVERT(DATE, record.EXPIRY)
FROM    tItem  item
	INNER JOIN [Pawsome_ImportInventory_20210315] record 
		ON record.[Ref NO] = item.ID
where 
	ID_Company = @Pawsomespace_ID_Company


Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory (
	[Code]
	,[ID_Item]
	,[Quantity] 
	,[UnitPrice]
	,[DateExpired]
	,[BatchNo]
	,[ID_FilingStatus]
	,[ID_Company]
	,[Comment]
	,[IsAddInventory]
)
SELECT  
	'Imported on 2021-03-15 11pm'
   ,ID_Item
   ,CurrentInventoryCount
   ,0.00
   ,DateExpired
   ,NULL
   ,3
   ,@Pawsomespace_ID_Company
   ,'Imported on 2021-03-15 11pm'
   ,1
FROM    @ItemInventory 
WHERE
	CurrentInventoryCount > 0
ORDER BY 
	ID_Item

exec pReceiveInventory @adjustInventory, 1

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pawsome_ImportInventory_20210315]') AND type in (N'U'))
DROP TABLE [dbo].[Pawsome_ImportInventory_20210315]
GO