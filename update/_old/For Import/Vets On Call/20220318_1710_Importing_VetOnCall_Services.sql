DEclare @GUID_Company VARCHAR(MAX) = 'D71B6CCD-1B3B-49D9-B6B3-188D52BD9F35'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company



DECLARE @forImport TABLE
  (
     ID_Item     INT,
     Name_Item   Varchar(MAX),
	 Buying_Price float,
	 Selling_Price float
)

/*ServicesType */
DECLARE @IsActive INT=1 
DECLARE @ID_ItemType INT =1
DECLARE @Comment VARCHAR(MAX)=  'Imported '+ FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1



INSERT @forImport
SELECT DISTINCT NULL,
       dbo.fGetCleanedString([Items]),
	   [BuyingPrice],
	   [SellingPrice]
FROM   dbo.[VetCall_Services]

Update @forImport
SET    ID_Item = item.ID
FROm   @forImport import
       INNER JOIN tItem item
               ON import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company 



SELECT * FROM @forImport
DELETE FROM @forImport WHERE LEN(Name_Item)=0


INSERT INTO [dbo].[tItem]
           ([Name]
           ,[IsActive]
           ,[ID_Company]
           ,[Comment]
           ,[DateCreated]
           ,[DateModified]
           ,[ID_CreatedBy]
           ,[ID_LastModifiedBy]
           ,[ID_ItemType]
		   ,[UnitCost]
		   ,[UnitPrice]
        )
   
SELECT DISTINCT
	 import.Name_Item,
	 @IsActive,
	 @ID_Company,
	 @Comment,
	 GETDATE(),
	 GETDATE(),
	 @ID_CreatedBy,
	 @ID_LastModifiedBy,
	 @ID_ItemType,
	 import.Buying_Price,
	 import.Selling_Price
FROM @forImport import
WHERE  ID_Item IS NULL




SELECT Name_Item,
       COUNT(*) Count
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1


SELECT *
FROM   tItem item
WHERE  ID_Company = @ID_Company


SELECT * 
FROM   tItem
WHERE  Comment LIKE 'Imported 03/18/2022%'
Order  by   Name






