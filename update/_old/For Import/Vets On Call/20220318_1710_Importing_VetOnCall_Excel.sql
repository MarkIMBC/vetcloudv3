
GO
/****** Object:  Table [dbo].[VetCall_Services]    Script Date: 18/03/2022 3:52:10 pm ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VetCall_Services]') AND type in (N'U'))
DROP TABLE [dbo].[VetCall_Services]
GO
/****** Object:  Table [dbo].[VetCall_Items]    Script Date: 18/03/2022 3:52:10 pm ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VetCall_Items]') AND type in (N'U'))
DROP TABLE [dbo].[VetCall_Items]
GO
/****** Object:  Table [dbo].[VetCall_Items]    Script Date: 18/03/2022 3:52:10 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VetCall_Items](
	[ITEM NAME ] [nvarchar](255) NULL,
	[BUYING PRICE ] [nvarchar](255) NULL,
	[SELLING PRICE] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VetCall_Services]    Script Date: 18/03/2022 3:52:10 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VetCall_Services](
	[Items] [nvarchar](255) NULL,
	[BuyingPrice] [float] NULL,
	[SellingPrice] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Cetirizine 10 mg ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Clindamycin 300mg ', NULL, 20)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Co-amox 625mg ', NULL, 15)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Enalapril 10mg ', NULL, 20)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furosemide 20mg/tab ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furosemide 40mg/tab ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Meloxicam 15mg ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Metoclopramide 10mg ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Metronidazole 500mg ', NULL, 15)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Omeprazole 20mg ', NULL, 15)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Omeprazole 40mg ', NULL, 20)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Prednisone 10mg ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Prednisone 5mg ', NULL, 10)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Cefalexin 125mg ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Co-amoxiclav 125mg', NULL, 200)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Co-amoxiclav 457mg', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Metoclopramide', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Coatshine', NULL, 399)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC-Vit ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC Vit OB', NULL, 399)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Refamol', NULL, 499)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Hemacare', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Livotine ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Methiovet 60 tabs', NULL, 20)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC Dox 100 tabs ', NULL, 25)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Otiderm ', NULL, 499)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC Dox 60ml', NULL, 220)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furfect cream ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Tolfenol', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Tritozine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furfect soap Green', NULL, 200)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Prefolic Cee', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Biogenta Drops ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Wormex Tabs', NULL, NULL)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Emerflox ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'MDC with honey ', NULL, 320)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'MDC soap', NULL, 130)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Amitraz soap ', NULL, 130)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Earmite Buster ', NULL, 300)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Metronidazole', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Bromhexine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Multivitamins+Min+ Lysine', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Iron+B complex + Taurine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Appeboost ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Petsure', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Growth Advance ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Dextrolyte 48pcs', NULL, 15)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Dextrolyte Lacta 48pcs', NULL, 25)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Nexgard 2-4kg', NULL, 650)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Nexgard 4-10kg', NULL, 700)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Nexgard 10-25kg', NULL, 750)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Nexgard 25-50kg', NULL, 800)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Broadline 2.5kg', NULL, 700)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Coatshine', NULL, 399)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC-Vit ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC Vit OB', NULL, 399)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Refamol', NULL, 499)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Hemacare', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Livotine ', NULL, 250)

GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Otiderm ', NULL, 499)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'LC Dox 60ml', NULL, 220)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furfect cream ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Tolfenol', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Tritozine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Furfect soap Green', NULL, 200)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Prefolic Cee', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Biogenta Drops ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Wormex Tabs', NULL, NULL)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Emerflox ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'MDC with honey ', NULL, 320)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'MDC soap', NULL, 130)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Amitraz soap ', NULL, 130)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Earmite Buster ', NULL, 300)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Metronidazole', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Bromhexine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Multivitamins+Min+ Lysine', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Iron+B complex + Taurine ', NULL, 280)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Appeboost ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Petsure', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Growth Advance ', NULL, 250)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Dextrolyte 48pcs', NULL, NULL)
GO
INSERT [dbo].[VetCall_Items] ([ITEM NAME ], [BUYING PRICE ], [SELLING PRICE]) VALUES (N'Dextrolyte Lacta 48pcs', NULL, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Consultation ', 350, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Deworming1-5kg', 180, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Deworming 6-10kg', 200, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Deworming 11-15kg', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Deworming 16-20kg', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Deworming 21-30kg', 350, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Anti Rabies Vaccine ', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'5in1 dog', 500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'8in1 dog', 650, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'3in1 cat', 700, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'4in1 cat', 900, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wellness fee (first visit)', 100, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wellness fee (next visit)', 50, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wellness fee with new vetbook', 100, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Emergency Fee', 1800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CBC', 950, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Biochem', 1800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CBC Biochem package ', 2700, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Health certificate ', 1000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'One way ', 1000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Two way ', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Three way ', 1900, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Four way ', 2200, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Fecalysis', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Skin scraping ', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Ear swab ', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Vaginal smear ', 700, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Eye test ', 350, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Fungal test ', 100, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'IV change ', 600, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'IV reinsertion', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wound cleaning no sedation ', 350, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Sedation ', 800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Urinary catheter ', 2800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'INJECTABLES 1-3kg', 150, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'INJECTABLES 3-6kg', 200, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'INJECTABLES 6-12kg', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'INJECTABLES 12-20kg', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'INJECTABLES 20kg and above', 350, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Dog Small ', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Dog Medium ', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Dog Large', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Dog X-Large', 3500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Cat 3kg or less', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Castration Cat 3kg-6kg', 2000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog Small', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog Medium ', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog Large', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog X-Large', 7000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat 3kg or less', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat 3kg-6kg', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat with pyometra', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat with OHE', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog w/ pyometra Small', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog w/ pyometra Medium ', 10000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog w/ pyometra Large', 12000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Dog w/ pyometra X-Large', 14000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat w/ pyometraSmall', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat w/ pyometraMedium ', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat w/ pyometraLarge', 7000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Spay Cat w/ pyometraX-Large', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CESARIAN Small dog', 10000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CESARIAN Medium dog', 12000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CESARIAN Large dog', 15000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CESARIAN XLarge dog', 20000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'CESARIAN Cat', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'PYOMETRA Small dog', 10000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'PYOMETRA Medium dog', 12000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'PYOMETRA Large dog', 15000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'PYOMETRA XLarge dog', 20000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'PYOMETRA Cat', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Anal sac resection', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Auricular hematoma ', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Cystotomy ', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Elbow hygroma (bilateral)', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Elbow hygroma (unilateral)', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Enterectomy ', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Entropion', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Enucleation', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Entropion', 6000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Enucleation', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Gastrotomy ', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Hernia repair umbilical', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Hernia repair inguinal', 7000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Hernia repair perineal', 9000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Intestinal anastomosis', 10000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal Prolapse cat', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal Prolapse dog small', 2000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal Prolapse dog medium', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal Prolapse dog large', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal Prolapse dog xlarge', 3500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Rectal prolapse with anastomosis', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Scrotal Ablation dog', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Scrotal Ablation cat', 2000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Splenectomy cats', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Splenectomy dogs small', 8000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Splenectomy dogs medium', 9000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Splenectomy dogs large', 10000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Splenectomy dogs xlarge', 11000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Tarsorarhapy temporary ', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Tarsorarhapy permanent', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Third eyelid flap and tarsorrhapy ', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Thoracotomy ', 15000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wound repair  Small', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wound repair  Medium ', 3500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wound repair  Large ', 4000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Wound repair  Xlarge', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA local Small', 750, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA local medium-large', 1000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA general Small', 1000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA general medium-large', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA sedative Small', 800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'ANESTHESIA sedative medium-large', 1300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Euthanasia', 2000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dental Prophylaxis Small', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dental Prophylaxis Medium', 2800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dental Prophylaxis Large ', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dental Prophylaxis Xlarge ', 3500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Whelphing First 3 hours ', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Whelphing Succeding hour ', 300, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Extractiob of dead fetus ', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Tail dock newborn ', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Tail dock Adult ', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dewclaw removal ', 1000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dewclaw removal all', 5000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Abscess Drainage ', 800, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Abscess Drainage w/ anesthesia Small', 1500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Abscess Drainage w/ anesthesia Medium ', 2000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Abscess Drainage w/ anesthesia Large ', 2500, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Abscess Drainage w/ anesthesia XL', 3000, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Appeboost ', 143, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Petsure', 125, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Growth Advance ', 150, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dextrolyte 48pcs', 250, NULL)
GO
INSERT [dbo].[VetCall_Services] ([Items], [BuyingPrice], [SellingPrice]) VALUES (N'Dextrolyte Lacta 48pcs', 528, NULL)
GO
