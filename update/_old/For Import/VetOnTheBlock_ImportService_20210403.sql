/****** Object:  Table [dbo].[VetOnTheBlock_ImportService_20210403]    Script Date: 4/3/2021 12:57:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VetOnTheBlock_ImportService_20210403]') AND type in (N'U'))
DROP TABLE [dbo].[VetOnTheBlock_ImportService_20210403]
GO
/****** Object:  Table [dbo].[VetOnTheBlock_ImportService_20210403]    Script Date: 4/3/2021 12:57:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VetOnTheBlock_ImportService_20210403](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Price] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section with OVH (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section with OVH (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section with OVH (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section with OVH (X LARGE 16 kg – above)', NULL, 10000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cystotomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cystotomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cystotomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cystotomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enterotomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enterotomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enterotomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enterotomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Esophagostomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Esophagostomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Esophagostomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Esophagostomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Exploratory Laparotomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Exploratory Laparotomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Exploratory Laparotomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Exploratory Laparotomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Episiotomy / Episioplasty (SMALL 0.5 – 5 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Episiotomy / Episioplasty (MEDIUM 6 - 10 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Episiotomy / Episioplasty (LARGE 11 – 15 kg)', NULL, 5500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Episiotomy / Episioplasty (X LARGE 16 kg – above)', NULL, 6500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Gastrotomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Gastrotomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Gastrotomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Gastrotomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PERIANAL/INGUINAL - Hernia Repair (SMALL 0.5 – 5 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PERIANAL/INGUINAL - Hernia Repair (MEDIUM 6 - 10 kg)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PERIANAL/INGUINAL - Hernia Repair (LARGE 11 – 15 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PERIANAL/INGUINAL - Hernia Repair (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'UMBILICAL - Hernia Repair (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'UMBILICAL - Hernia Repair (MEDIUM 6 - 10 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'UMBILICAL - Hernia Repair (LARGE 11 – 15 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'UMBILICAL - Hernia Repair (X LARGE 16 kg – above)', NULL, 6500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Unilateral - Mastectomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Unilateral - Mastectomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Unilateral - Mastectomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Unilateral - Mastectomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bilateral - Mastectomy (SMALL 0.5 – 5 kg)', NULL, 8500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bilateral - Mastectomy (MEDIUM 6 - 10 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bilateral - Mastectomy (LARGE 11 – 15 kg)', NULL, 10000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bilateral - Mastectomy (X LARGE 16 kg – above)', NULL, 11000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Scrotal Ablation - Mastectomy (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Scrotal Ablation - Mastectomy (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Scrotal Ablation - Mastectomy (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Scrotal Ablation - Mastectomy (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rectal Prolapse - Mastectomy (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rectal Prolapse - Mastectomy (MEDIUM 6 - 10 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rectal Prolapse - Mastectomy (LARGE 11 – 15 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rectal Prolapse - Mastectomy (X LARGE 16 kg – above)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - DOG (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - DOG (MEDIUM 6 - 10 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - DOG (LARGE 11 – 15 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - DOG (X LARGE 16 kg – above)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - CAT (SMALL 0.5 – 5 kg)', NULL, 1000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SPAY - CAT (MEDIUM 6 - 10 kg)', NULL, 1200)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - DOG (SMALL 0.5 – 5 kg)', NULL, 1500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - DOG (MEDIUM 6 - 10 kg)', NULL, 1800)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - DOG (LARGE 11 – 15 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - DOG (X LARGE 16 kg – above)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - CAT (SMALL 0.5 – 5 kg)', NULL, 750)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'NEUTER - CAT (MEDIUM 6 - 10 kg)', NULL, 900)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tumor Removal (SMALL 0.5 – 5 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tumor Removal (MEDIUM 6 - 10 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tumor Removal (LARGE 11 – 15 kg)', NULL, 5500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tumor Removal (X LARGE 16 kg – above)', NULL, 6500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - DOG (SMALL 0.5 – 5 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - DOG (MEDIUM 6 - 10 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - DOG (LARGE 11 – 15 kg)', NULL, 10000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - DOG (X LARGE 16 kg – above)', NULL, 12000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - CAT (SMALL 0.5 – 5 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy with Pyometra - CAT (MEDIUM 6 - 10 kg)', NULL, 5500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - DOG (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - DOG (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - DOG (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - DOG (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - CAT (SMALL 0.5 – 5 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy - CAT (MEDIUM 6 - 10 kg)', NULL, 5500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Zepp Operation (Lateral Ear Resection) (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Zepp Operation (Lateral Ear Resection) (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Zepp Operation (Lateral Ear Resection) (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Zepp Operation (Lateral Ear Resection) (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Amputation of the Leg -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Amputation of the Leg -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Amputation of the Leg -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Amputation of the Leg -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Pinning (case to case basis) -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Pinning (case to case basis) -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Pinning (case to case basis) -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 9000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Pinning (case to case basis) -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 10500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting / SAM Splinting -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting / SAM Splinting -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting / SAM Splinting -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting / SAM Splinting -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 7000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting with Quick Splint -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting with Quick Splint -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting with Quick Splint -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting with Quick Splint -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reduction / Luxation -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reduction / Luxation -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reduction / Luxation -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reduction / Luxation -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Thomas Splinting  -  ORTHOPEDICS (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Thomas Splinting  -  ORTHOPEDICS (MEDIUM 6 - 10 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Thomas Splinting  -  ORTHOPEDICS (LARGE 11 – 15 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Thomas Splinting  -  ORTHOPEDICS (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Package (plus PF 500 / day) (SMALL 0.5 – 5 kg)', NULL, 2000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Package (plus PF 500 / day) (MEDIUM 6 - 10 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Package (plus PF 500 / day) (LARGE 11 – 15 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Package (plus PF 500 / day) (X LARGE 16 kg – above)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Itemized Billing: (SMALL 0.5 – 5 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Itemized Billing: (MEDIUM 6 - 10 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Itemized Billing: (LARGE 11 – 15 kg)', NULL, 800)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CONFINEMENT - Itemized Billing: (X LARGE 16 kg – above)', NULL, 800)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Doctors Fee', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Antibiotic Injections / shot  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Antibiotic Injections / shot  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Antibiotic Injections / shot  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Antibiotic Injections / shot  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GIT Injection  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GIT Injection  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GIT Injection  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GIT Injection  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ornipural / Liver Tonics  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ornipural / Liver Tonics  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ornipural / Liver Tonics  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ornipural / Liver Tonics  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin Injection  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin Injection  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin Injection  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin Injection  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Epinephrine Inj  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Epinephrine Inj  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Epinephrine Inj  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Epinephrine Inj  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 700)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin K3  - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin K3  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin K3  - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vitamin K3  - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Atropine Sulfate   - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Atropine Sulfate   - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Atropine Sulfate   - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Atropine Sulfate   - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Furosemide   - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Furosemide   - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Furosemide   - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Furosemide   - CONFINEMENT (X LARGE 16 kg – above)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reversal Agent   - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reversal Agent   - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reversal Agent   - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 750)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Reversal Agent   - CONFINEMENT (X LARGE 16 kg – above)', NULL, 1000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oral Medications per admin   - CONFINEMENT (SMALL 0.5 – 5 kg)', NULL, 50)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oral Medications per admin  - CONFINEMENT (MEDIUM 6 - 10 kg)', NULL, 100)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oral Medications per admin   - CONFINEMENT (LARGE 11 – 15 kg)', NULL, 150)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oral Medications per admin   - CONFINEMENT (X LARGE 16 kg – above)', NULL, 200)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Fistula with Extraction (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Fistula with Extraction (MEDIUM 6 - 10 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Fistula with Extraction (LARGE 11 – 15 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Fistula with Extraction (X LARGE 16 kg – above)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction Deciduos with Local Anesthetic (SMALL 0.5 – 5 kg)', NULL, 400)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction Deciduos with Local Anesthetic (MEDIUM 6 - 10 kg)', NULL, 600)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction Deciduos with Local Anesthetic (LARGE 11 – 15 kg)', NULL, 700)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction Deciduos with Local Anesthetic (X LARGE 16 kg – above)', NULL, 900)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction with Loosened Tooth (SMALL 0.5 – 5 kg)', NULL, 200)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction with Loosened Tooth (MEDIUM 6 - 10 kg)', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction with Loosened Tooth (LARGE 11 – 15 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Extraction with Loosened Tooth (X LARGE 16 kg – above)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Prophylaxis (requires laboratory prior to conduct) (SMALL 0.5 – 5 kg)', NULL, 2000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Prophylaxis (requires laboratory prior to conduct) (MEDIUM 6 - 10 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Prophylaxis (requires laboratory prior to conduct) (LARGE 11 – 15 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental Prophylaxis (requires laboratory prior to conduct) (X LARGE 16 kg – above)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'MINOR SURGERIES', NULL, NULL)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Single Eye (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Single Eye (MEDIUM 6 - 10 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Single Eye (LARGE 11 – 15 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Single Eye (X LARGE 16 kg – above)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Two Eyes (SMALL 0.5 – 5 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Two Eyes (MEDIUM 6 - 10 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Two Eyes (LARGE 11 – 15 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry Eye Removal - Two Eyes (X LARGE 16 kg – above)', NULL, 6500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Single Ear (SMALL 0.5 – 5 kg)', NULL, 2000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Single Ear (MEDIUM 6 - 10 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Single Ear (LARGE 11 – 15 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Single Ear (X LARGE 16 kg – above)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Two Ears (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Two Ears (MEDIUM 6 - 10 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Two Ears (LARGE 11 – 15 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma - Two Ears (X LARGE 16 kg – above)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Elbow / Leg Hygroma (SMALL 0.5 – 5 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Elbow / Leg Hygroma (MEDIUM 6 - 10 kg)', NULL, 6000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Elbow / Leg Hygroma (LARGE 11 – 15 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Elbow / Leg Hygroma (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Eyeball Enucleation (SMALL 0.5 – 5 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Eyeball Enucleation (MEDIUM 6 - 10 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Eyeball Enucleation (LARGE 11 – 15 kg)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Eyeball Enucleation (X LARGE 16 kg – above)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Entropion (SMALL 0.5 – 5 kg)', NULL, 5000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Entropion (MEDIUM 6 - 10 kg)', NULL, 6000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Entropion (LARGE 11 – 15 kg)', NULL, 7500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Entropion (X LARGE 16 kg – above)', NULL, 8000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Temporary (SMALL 0.5 – 5 kg)', NULL, 1500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Temporary (MEDIUM 6 - 10 kg)', NULL, 2000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Temporary (LARGE 11 – 15 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Temporary (X LARGE 16 kg – above)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Permanent (SMALL 0.5 – 5 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Permanent (MEDIUM 6 - 10 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Permanent (LARGE 11 – 15 kg)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tarsorraphy - Permanent (X LARGE 16 kg – above)', NULL, 4500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Less than 7 days (SMALL 0.5 – 5 kg)', NULL, 200)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Less than 7 days (MEDIUM 6 - 10 kg)', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Less than 7 days (LARGE 11 – 15 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Less than 7 days (X LARGE 16 kg – above)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - 2 to 4 weeks (SMALL 0.5 – 5 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - 2 to 4 weeks (MEDIUM 6 - 10 kg)', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - 2 to 4 weeks (LARGE 11 – 15 kg)', NULL, 300)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - 2 to 4 weeks (X LARGE 16 kg – above)', NULL, 350)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Above 6 weeks (SMALL 0.5 – 5 kg)', NULL, 1500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Above 6 weeks (MEDIUM 6 - 10 kg)', NULL, 2000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Above 6 weeks (LARGE 11 – 15 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking + Dew Claw Removal - Above 6 weeks (X LARGE 16 kg – above)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CBC and Serology (full service)', NULL, NULL)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CBC with Platelet Count', NULL, 500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'3 P (CBC,Liver and Kidney Function)', NULL, 1500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Comprehensive ', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Histopathology/lesion (SMALL 0.5 – 5 kg)', NULL, 2500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Histopathology/lesion (MEDIUM 6 - 10 kg)', NULL, 3000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Histopathology/lesion (LARGE 11 – 15 kg)', NULL, 3500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Histopathology/lesion (X LARGE 16 kg – above)', NULL, 4000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'FIV + FeLV', NULL, 900)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Parvo + Coronavirus', NULL, 900)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Other Test Kits (Single)', NULL, 750)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'3 Way', NULL, 1500)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Direct Fecal Smear', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vaginal Smear', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ultrasound  (SMALL 0.5 – 5 kg)', NULL, 750)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ultrasound  (MEDIUM 6 - 10 kg)', NULL, 800)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ultrasound  (LARGE 11 – 15 kg)', NULL, 1000)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ultrasound (X LARGE 16 kg – above)', NULL, 1200)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinalysis (Test Strips)', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Skin Scrapping', NULL, 250)
GO
INSERT [dbo].[VetOnTheBlock_ImportService_20210403] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bacterial Culture and Sensitivity Testing', NULL, 1000)
GO


DECLARE @ID_Company_VetOnTHeBLOCK INT = 55
DECLARE @ItemServices TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitPrice DECIMAL(18, 4))

INSERT @ItemServices
SELECT  import.Name,
		cat.ID,
		ISNULL(import.[Price],0)
FROM    VetOnTheBlock_ImportService_20210403 import 
LEFT JOIN tItemCategory cat 
	ON UPPER(import.Category) = UPPER(cat.Name) AND cat.ID_ItemType = 1

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  Old_item_id)
SELECT 
  @ID_Company_VetOnTHeBLOCK
  ,ItemName
  ,UnitPrice
  ,1
  , NULL
  , ID_ItemCategory
  , NULL
  , 1
  , NULL
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , NULL
 FROM   @ItemServices

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VetOnTheBlock_ImportService_20210403]') AND type in (N'U'))
DROP TABLE [dbo].[VetOnTheBlock_ImportService_20210403]
GO