DECLARE @GUID_Company VARCHAR(MAX) = 'E1682B9E-22CB-4522-BD4E-2B995FFFD552'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client            INT,
     CustomCode_Client    Varchar(MAX),
     Name_Client          Varchar(MAX),
     Address_Client       Varchar(MAX),
     ContactNumber_Client Varchar(MAX),
     Comment_Client       Varchar(MAX)
  )

UPDATE tClient
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_COmpany = @ID_Company

INSERT @forImport
       (CustomCode_Client,
        Name_Client,
        Address_Client,
        ContactNumber_Client,
        Comment_Client)
SELECT dbo.fGetCleanedString([RECORD NO.]),
       dbo.fGetCleanedString([CLIENT NAME]),
       dbo.fGetCleanedString([ADDRESS]),
       dbo.fGetCleanedString([CONTACT NO.]),
       CASE
         WHEN LEN(dbo.fGetCleanedString([SINCE])) > 0 THEN 'Registered Since: '
                                                           + dbo.fGetCleanedString([SINCE])
         ELSE ''
       END
FROm   [Animasolutions INC - Client - 2]

Update @forImport
SET    Comment_Client = Comment_Client
                        + CASE
                            WHEN LEN(Comment_Client) > 0 THEN CHAR(13)
                            ELSE ''
                          END
                        + 'Imported '
                        + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

DELETE FROM @forImport
where  LEN(Name_Client) = 0

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
                  and import.CustomCode_Client = client.CustomCode
WHERE  ID_Company = @ID_Company


Update @forImport SET ContactNumber_Client = REPLACE(ContactNumber_Client, '{', '')
Update @forImport SET ContactNumber_Client = REPLACE(ContactNumber_Client, '}', '')

Update @forImport SET ContactNumber_Client = '0' + ContactNumber_Client
WHERE ContactNumber_Client LIKE '9%'

SELECT *
FROM   @forImport


Update tClient
SET   
       ContactNumber = ContactNumber_Client
FROM   tClient client
       inner join @forImport import
               on import.ID_Client = client.ID
WHERE  ID_Company = @ID_Company 






