
GO

DECLARE @GUID_Company VARCHAR(MAX) = 'E1682B9E-22CB-4522-BD4E-2B995FFFD552'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tItem
Set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

DECLARE @preImport TABLE
  (
     [Product/Service Name] varchar(500),
     [SKU]                  varchar(500),
     [Purchase Cost]        DECIMAL(18, 2),
     TotalInventoryCount    INT,
     TotalStockPrice        DECIMAL(18, 2),
     ComputedUnitCOst       DECIMAL(18, 2),
     [GOOD STOCK]           INT,
     [NEAR EXPIRY]          INT,
     [EXPIRED]              INT,
     [T. GD STOCK]          DECIMAL(18, 2),
     [T NEAR EXPRY]         DECIMAL(18, 2),
     [TOTAL EXPRD]          DECIMAL(18, 2),
     [EXPIRATION DATE]      DateTime,
     [LOCATION]             varchar(500)
  )

INSERT @preImport
       ([Product/Service Name],
        [SKU],
        [Purchase Cost],
        [GOOD STOCK],
        [NEAR EXPIRY],
        [EXPIRED],
        [T. GD STOCK],
        [T NEAR EXPRY],
        [TOTAL EXPRD],
        [EXPIRATION DATE],
        [LOCATION])
SELECT dbo.fGetCleanedString([Product/Service Name]),
       dbo.fGetCleanedString([SKU]),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString([Purchase Cost])),
       TRY_CONVERT(INT, dbo.fGetCleanedString(REPLACE([GOOD STOCK], ',', ''))),
       TRY_CONVERT(INT, dbo.fGetCleanedString(REPLACE([NEAR EXPIRY], ',', ''))),
       TRY_CONVERT(INT, dbo.fGetCleanedString(REPLACE([EXPIRED], ',', ''))),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString(REPLACE([T. GD STOCK], ',', ''))),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString(REPLACE([T NEAR EXPRY], ',', ''))),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString(REPLACE([TOTAL EXPRD], ',', ''))),
       TRY_PARSE(dbo.fGetCleanedString([EXPIRATION DATE] )
                 + '/2022' AS DATETIME),
       [LOCATION]
FROM   [ANIMASOLUTIONS  Inventoriable]

DECLARE @forImportDuplicate TABLE
  (
     Name_Item VARCHAR(MAX)
  )

INSERT @forImportDuplicate
SELECT [Product/Service Name]
FROm   @preImport
GROUP  BY [Product/Service Name]
HAVING COUNT(*) > 1

Update @preImport
SET    [Product/Service Name] = [Product/Service Name]
                                + CASE
                                    WHEN LEN([SKU]) > 0 THEN ' - ' + [SKU]
                                    ELSE ''
                                  END
WHERE  [Product/Service Name] IN (SELECT NamE_Item
                                  FROM   @forImportDuplicate)

DELETE FROM @forImportDuplicate

Update @preImport
SET    [EXPIRATION DATE] = NULL
WHERE  [EXPIRATION DATE] = '1900-01-01 00:00:00.000'

Update @preImport
SET    TotalInventoryCount = ISNULL([GOOD STOCK], 0)
                             + ISNULL([NEAR EXPIRY], 0)
                             + ISNULL([EXPIRED], 0)

Update @preImport
SET    TotalStockPrice = ISNULL([T. GD STOCK], 0)
                         + ISNULL([T NEAR EXPRY], 0)
                         + ISNULL([TOTAL EXPRD], 0)

Update @preImport
SET    ComputedUnitCOst = TotalStockPrice / TotalInventoryCount
where  TotalStockPrice > 0
       and TotalInventoryCount > 0

DECLARE @forImport TABLE
  (
     ID_Item               INT,
     SKU_Item              Varchar(MAX),
     Name_Item             Varchar(MAX),
     UnitCost              Decimal(18, 2),
     UnitPrice             Decimal(18, 2),
     DateExpiration        DateTime,
     CurrentInventoryCount INT,
     Comment               VARCHAR(MAX)
  )
DECLARE @IsActive INT=1
DECLARE @Items_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1

INSERT @forImport
       (SKU_Item,
        Name_Item,
        UnitCost,
        UnitPrice,
        DateExpiration,
        CurrentInventoryCount,
        Comment)
SELECT [SKU],
       [Product/Service Name],
       [Purchase Cost],
       ComputedUnitCOst,
       [EXPIRATION DATE],
       TotalInventoryCount,
       CASE
         WHEN LEN(LOCATION) > 0 THEN 'Location: ' + LOCATION
         ELSE ''
       END
FROM   @preImport

Update @forImport
SET    Comment = Comment
                 + CASE
                     WHEN LEN(Comment) > 0 THEN CHAR(13) + CHAR(13) + 'Imported '
                                                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
                     ELSE ''
                   END

SELECT Name_Item,
       COUNT(*)
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType
       and IsActive = 1

Update tItem
SET    IsActive = 0
WHERE  ID NOT IN (SELECT ID_Item
                  FRom   @ForImport)
       AND ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType 

SELECT *
FROm   @forImport
Order  by Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             SKU,
             [ID_ItemType],
             UnitCost,
             OtherInfo_DateExpiration,
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                import.SKU_Item,
                @Items_ID_ItemType,
                import.UnitCost,
                DateExpiration,
                @IsActive,
                @ID_Company,
                Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy
FROM   @forImport import
WHERE  ID_Item IS NULL

SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL

SELECT import.*
FROM   @forImport import
       inner JOIN @forImportDuplicate tbl
               on import.Name_Item = tbl.Name_Item

SELECT Name_Item
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType
       and IsActive = 1

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession Int

select TOP 1 @ID_UserSession = ID
FROm   tUserSession
where  ID_User = 10
ORDER  BY ID DESC

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import From Excel File',
       import.ID_Item,
       CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FRom   @forImport import
WHERE  ISNULL(import.CurrentInventoryCount, 0) > 0

SELECT *
FROM   @adjustInventory

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession 
