DECLARE @GUID_Company VARCHAR(MAX) = 'E1682B9E-22CB-4522-BD4E-2B995FFFD552'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client            INT,
     CustomCode_Client    Varchar(MAX),
     Name_Client          Varchar(MAX),
     Address_Client       Varchar(MAX),
     ContactNumber_Client Varchar(MAX)
  )

UPDATE tClient
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_COmpany = @ID_Company

INSERT @forImport
       (ID_Client,
        CustomCode_Client,
        Name_Client,
        Address_Client,
        ContactNumber_Client)
SELECT NULL,
       dbo.fGetCleanedString([CODE]),
       dbo.fGetCleanedString([CLIENT NAME]),
       dbo.fGetCleanedString([ADDRESS]),
       dbo.fGetCleanedString([NUMBER])
FROm   [ANIMASOLUTIONS-CLIENT-MASTERLIST]

Update @forImport
SET    ContactNumber_Client = REPLACE(ContactNumber_Client, '`', '')

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company

DELETE FROM @forImport
where  LEN(Name_Client) = 0

INSERT dbo.tClient
       (ID_Company,
        CustomCode,
        Name,
        ContactNumber,
        Address,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                CustomCode_Client,
                Name_Client,
                ContactNumber_Client,
                Address_Client,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
where  ID_Client IS NULL

Update @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.CustomCode_Client = client.CustomCode
WHERE  ID_Company = @ID_Company

SELECT *
FROM   @forImport
where  Name_Client IN (SELECT Name_Client
                       FROM   @forImport
                       GROUP  BY Name_Client
                       HAVING COUNT(*) > 1)
ORDER  BY Name_Client 
