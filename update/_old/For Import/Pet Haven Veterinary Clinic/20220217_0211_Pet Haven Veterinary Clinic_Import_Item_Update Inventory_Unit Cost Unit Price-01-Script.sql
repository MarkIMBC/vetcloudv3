DEclare @GUID_Company VARCHAR(MAX) = 'E4E573E8-5883-4CE2-AE37-8E1039BCC87B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item           int,
     ID_ItemCategory   Int,
     Name_Item         varchar(500),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2),
     DateExpiration    DateTime,
     Quantity          Int,
     Name_ItemCategory varchar(500)
  )

uPDATE tItemCategory
set    nAME = [dbo].[fGetCleanedString](Name)
WHERE  ID_Company = @ID_Company

INSERT @forImport
       (Name_Item,
        UnitCost,
        UnitPrice,
        DateExpiration,
        Quantity,
        Name_ItemCategory)
SELECT [dbo].[fGetCleanedString]([ITEM'S NAME]),
       CASE
         WHEN LEN([dbo].[fGetCleanedString]([BUYING PRICE])) = 0 THEN '0'
         ELSE [dbo].[fGetCleanedString]([BUYING PRICE])
       END,
       CASE
         WHEN LEN([dbo].[fGetCleanedString]([SELLING PRICE])) = 0 THEN '0'
         ELSE [dbo].[fGetCleanedString]([SELLING PRICE])
       END,
       TRY_CONVERT(DateTime, [dbo].[fGetCleanedString]([EXPIRY DATE])),
       [dbo].[fGetCleanedString]([INVENTORY DATE]),
       [dbo].[fGetCleanedString]([CATEGORY])
FROM   [Pet_Haven_Feb_16_2022_Item]

Update @forImport
SET    DateExpiration = NULL
WHERE  DateExpiration = '1900-01-01 00:00:00.000'

Update @forImport
SET    ID_ItemCategory = itmCat.ID
FROM   @forImport import
       inner join tItemCategory itmCat
               on import.Name_ItemCategory = itmCat.Name
Where  itmCat.ID_ItemType = 2

DELETE FROM @forImport
Where  Quantity = 0

Update @forImport
SET    ID_Item = item.ID
FROm   @forImport import
       inner join tItem item
               on import.Name_Item = item.Name
Where  IsActive = 1
       and ID_Company = @ID_Company 

INSERT tItem
       (ID_Company,
        Name,
        ID_ItemType,
        ID_ItemCategory,
		OtherInfo_DateExpiration,
		UnitCost,
		UnitPrice,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company,
       Name_Item,
       2,
       ID_ItemCategory,
	   DateExpiration,
	   UnitCost,
	   UnitPrice,
       1,
       'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @forImport record
WHERE  ID_Item IS NULL

Update @forImport
SET    ID_Item = item.ID
FROm   @forImport import
       inner join tItem item
               on import.Name_Item = item.Name
Where  IsActive = 1
       and ID_Company = @ID_Company 

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.Quantity,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
		'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport import inner join tItem item on import.ID_Item = item.ID
WHERE  Quantity > 0 
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
 @ID_UserSession


SELECT * FROm @forImport
