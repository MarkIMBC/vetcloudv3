if OBJECT_ID('dbo.[Pet_Haven_Feb_16_2022_Item]') is not null
  BEGIN
      DROP TABLE [Pet_Haven_Feb_16_2022_Item]
  END

GO

CREATE TABLE [dbo].[Pet_Haven_Feb_16_2022_Item]
  (
     [ITEM'S NAME]    varchar(500),
     [BUYING PRICE]   varchar(500),
     [SELLING PRICE]  varchar(500),
     [EXPIRY DATE]    varchar(500),
     [INVENTORY DATE] varchar(500),
     [CATEGORY]       varchar(500)
  )

GO

INSERT INTO [dbo].[Pet_Haven_Feb_16_2022_Item]
            ([ITEM'S NAME],
             [BUYING PRICE],
             [SELLING PRICE],
             [EXPIRY DATE],
             [INVENTORY DATE],
             [CATEGORY])
SELECT 'ADVOCATE FOR CATS UP TO 4KG',
       '297',
       '500',
       '',
       '0',
       ''
UNION ALL
SELECT 'ADVOCATE FOR DOGS UP TO  4KG',
       '',
       '500',
       '3/1/2022',
       '11',
       ''
UNION ALL
SELECT 'ACTIVATED CHARCOAL - per capsules',
       '1',
       '5',
       '1/1/2023',
       '800',
       ''
UNION ALL
SELECT 'ALPHA VIT',
       '114',
       '250',
       '5/1/2022',
       '1',
       ''
UNION ALL
SELECT 'ALLERGY-EZE (NV)',
       '14',
       '18',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANTI-DIARRHEA LIQUID (AS)',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANTI-ANEMIA CAPSULES',
       '',
       '',
       '10/1/2022',
       '13',
       ''
UNION ALL
SELECT 'ARTHROPET',
       '6',
       '13',
       '',
       '0',
       ''
UNION ALL
SELECT 'ASPIRIN (Scheeprin)',
       '',
       '25',
       '3/1/2022',
       '80',
       ''
UNION ALL
SELECT 'BARFY MULTIVATAMINS SYRUP',
       '',
       '250',
       '',
       '0',
       ''
UNION ALL
SELECT 'BIOPIROX SPRAY',
       '588',
       '680',
       '',
       '0',
       ''
UNION ALL
SELECT 'BIOVER 100ml',
       '',
       '480',
       '01/14/2023',
       '6',
       ''
UNION ALL
SELECT 'BLADDER CONTROL (AS)',
       '14',
       '20',
       '',
       '0',
       ''
UNION ALL
SELECT 'BREWERS YEAST CHEWABLES (NV)',
       '2',
       '5',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRONCURE',
       '185',
       '350',
       '8/1/2024',
       '9',
       ''
UNION ALL
SELECT 'CALMIVET 1ML',
       '13',
       '150',
       '',
       '0',
       ''
UNION ALL
SELECT 'CALCIUM LACTATE + CHOLECALCIFEROL (VIT D3)',
       '',
       '',
       '01/31/2024',
       '742',
       ''
UNION ALL
SELECT 'CANIMAX',
       '163',
       '250',
       '',
       '0',
       ''
UNION ALL
SELECT 'CARODYL',
       '59',
       '100',
       '1/1/2022',
       '41',
       ''
UNION ALL
SELECT 'CATAGON',
       '220',
       '350',
       '5/1/2024',
       '4',
       ''
UNION ALL
SELECT 'CEFALEXIN CAPSULE 250mg',
       '2',
       '15',
       '2/1/2023',
       '100',
       ''
UNION ALL
SELECT 'CEFALEXIN CAPSULE 500mg',
       '3',
       '20',
       '3/1/2024',
       '61',
       ''
UNION ALL
SELECT 'CEPHALEXINE SUSPENSION (Exel)',
       '28',
       '250',
       '1/1/2024',
       '20',
       ''
UNION ALL
SELECT 'CEPHALEXINE SUSPENSION (Riflexine)',
       '190',
       '250',
       '8/1/2024',
       '1',
       ''
UNION ALL
SELECT 'CELMANASX',
       '430',
       '550',
       '',
       '0',
       ''
UNION ALL
SELECT 'CETIRIZINE TABLET (A20:I28',
       '1',
       '10',
       '9/1/2024',
       '7',
       ''
UNION ALL
SELECT 'CETIRIZINE  SYRUP (Reax)',
       '35',
       '200',
       '10/1/2023',
       '15',
       ''
UNION ALL
SELECT 'CIPROFLOXACIN (CIPROSAN)',
       '',
       '',
       '3/1/2022',
       '83',
       ''
UNION ALL
SELECT 'CLINDAMYIN CAPSULE (300mg)',
       '',
       '',
       '9/1/2023',
       '69',
       ''
UNION ALL
SELECT 'CO-AMOXICLAV TABLET (RANICLAV)',
       '14',
       '30',
       '10/1/2022',
       '114',
       ''
UNION ALL
SELECT 'CO-AMOXICLAV SYRUP (Co-amoxisaph250)',
       '173.87',
       '350',
       '7/1/2023',
       '5',
       ''
UNION ALL
SELECT 'CO-AMOXICLAV SYRUP (Solclav)',
       '165',
       '350',
       '6/1/2023',
       '15',
       ''
UNION ALL
SELECT 'COTRIMOXAZOLE SYRUP',
       '55',
       '250',
       '',
       '0',
       ''
UNION ALL
SELECT 'COTRIMOXAZOLE TAB 960mg (Kathrex)',
       '',
       '',
       '3/1/2024',
       '100',
       ''
UNION ALL
SELECT 'DELTACAL',
       '',
       '400',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOXYCYCLINE CAPSULE',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOXYCYCLINE SYRUP (LYMEDOX)',
       '188',
       '330',
       '2/1/2024',
       '1',
       ''
UNION ALL
SELECT 'EAR DROPS (K9)',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'ENER G',
       '150',
       '350',
       '7/1/2024',
       '7',
       ''
UNION ALL
SELECT 'ENFABOOST',
       '215',
       '300',
       '8/1/2024',
       '1',
       ''
UNION ALL
SELECT 'ENROFLOXACIN SYRUP (EMERFLOX)',
       '',
       '350',
       '9/1/2024',
       '24',
       ''
UNION ALL
SELECT 'ENROFLOXACIN 50mg TAB',
       '12.6',
       '50',
       '3/1/2024',
       '100',
       ''
UNION ALL
SELECT 'ENMALAC',
       '175',
       '325',
       '5/1/2024',
       '8',
       ''
UNION ALL
SELECT 'EXCELVIT',
       '5',
       '12',
       '',
       '0',
       ''
UNION ALL
SELECT 'EYE VITAMIN',
       '502',
       '650',
       '',
       '0',
       ''
UNION ALL
SELECT 'EYE DROPS (K9)',
       '340',
       '600',
       '',
       '0',
       ''
UNION ALL
SELECT 'EYE RINSE LIQUID (NV)',
       '440',
       '650',
       '',
       '0',
       ''
UNION ALL
SELECT 'FLAVET METRONIDAZOLE SUSP.',
       '90',
       '240',
       '',
       '0',
       ''
UNION ALL
SELECT 'FUR BABY 100gm pack',
       '138',
       '200',
       '9/1/2023',
       '92',
       ''
UNION ALL
SELECT 'FURGRANCE MADRE DE CACAO OINTMENT',
       '150',
       '200',
       '',
       '0',
       ''
UNION ALL
SELECT 'FUROSEMIDE 40mg tab',
       '1',
       '25',
       '2/1/2023',
       '100',
       ''
UNION ALL
SELECT 'GOAT''S MILK',
       '150',
       '250',
       '10/1/2022',
       '8',
       ''
UNION ALL
SELECT 'HEALTHY COAT (AS)',
       '9',
       '12',
       '',
       '0',
       ''
UNION ALL
SELECT 'HEPATOSURE',
       '197',
       '26',
       '10/1/2022',
       '1',
       ''
UNION ALL
SELECT 'HIMPYRIN',
       '395',
       '650',
       '',
       '0',
       ''
UNION ALL
SELECT 'HYDROCORTISONE SPRAY',
       '835',
       '1050',
       '',
       '0',
       ''
UNION ALL
SELECT 'IMMUNE HEALTH',
       '5',
       '7',
       '',
       '0',
       ''
UNION ALL
SELECT 'IMMUNOL 120ml',
       '',
       '',
       '1/1/2024',
       '1',
       ''
UNION ALL
SELECT 'IMMUNOL TAB',
       '6.5',
       '15',
       '6/1/2023',
       '180',
       ''
UNION ALL
SELECT 'IMMUNO ACTIVE 100ML',
       '340',
       '520',
       '',
       '0',
       ''
UNION ALL
SELECT 'IMMUNO ACTIVE TAB',
       '9',
       '20',
       '9/1/2021',
       '164',
       ''
UNION ALL
SELECT 'IOZIN SPRAY',
       '205',
       '355',
       '7/1/2024',
       '24',
       ''
UNION ALL
SELECT 'K9 ASPIRIN (AS)',
       '7',
       '15',
       '',
       '0',
       ''
UNION ALL
SELECT 'LACTULOSE',
       '90',
       '230',
       '',
       '0',
       ''
UNION ALL
SELECT 'LIV 52 TAB',
       '6',
       '15',
       '1/1/2023',
       '56',
       ''
UNION ALL
SELECT 'LIV 52 SUSPENSION - bottles',
       '370',
       '600',
       '3/1/2021',
       '6',
       ''
UNION ALL
SELECT 'LIVERATOR SYRUP 30 ml',
       '150',
       '250',
       '1/1/2024',
       '5',
       ''
UNION ALL
SELECT 'LOC TEARS (EYE DROPS) -  boxes',
       '232',
       '600',
       '8/1/2023',
       '16',
       ''
UNION ALL
SELECT 'MEGADERM 4ml ( <10kg)',
       '23',
       '45',
       '9/1/2022',
       '94',
       ''
UNION ALL
SELECT 'MEGADERM 8ml (>10kg)',
       '29.7',
       '55',
       '',
       '0',
       ''
UNION ALL
SELECT 'MENADIONE TAB (10mg)',
       '0.6',
       '15',
       '6/1/2021',
       '30',
       ''
UNION ALL
SELECT 'METOCLOPRAMIDE SYRUP (Motillex)',
       '19',
       '150',
       '7/1/2024',
       '15',
       ''
UNION ALL
SELECT 'METOCLOPRAMIDE TAB (10mg)',
       '1',
       '25',
       '7/1/2023',
       '390',
       ''
UNION ALL
SELECT 'METRONIDAZOLE 500mg TAB (Flagex)',
       '1',
       '50',
       '12/1/2022',
       '259',
       ''
UNION ALL
SELECT 'METRONIDAZOLE SYRUP (Metrozole)',
       '21',
       '240',
       '8/1/2023',
       '14',
       ''
UNION ALL
SELECT 'MICRO FLOX (ENROFLOXACIN)',
       '320',
       '380',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOXIFLOXACIN (AS HYDROCLORIDE)',
       '193.2',
       '380',
       '10/1/2022',
       '21',
       ''
UNION ALL
SELECT 'MOXI/DEXA (4 QUIN-DX EYE DROPS)',
       '220',
       '500',
       '4/1/2022',
       '1',
       ''
UNION ALL
SELECT 'MOXI/ DEXA (ENDEMOX- DX)',
       '207',
       '500',
       '11/1/2023',
       '22',
       ''
UNION ALL
SELECT 'NEPRO',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'NACALVIT-C (120ml)',
       '156',
       '290',
       '9/1/2023',
       '27',
       ''
UNION ALL
SELECT 'NASTY HABIT (NV)',
       '10',
       '15',
       '',
       '0',
       ''
UNION ALL
SELECT 'NEFROTEC',
       '6',
       '15',
       '2/1/2022',
       '26',
       ''
UNION ALL
SELECT 'NERVE PLEX',
       '285',
       '400',
       '',
       '0',
       ''
UNION ALL
SELECT 'NUTRIPLUS GEL',
       '517',
       '650',
       '5/1/2023',
       '7',
       ''
UNION ALL
SELECT 'OPTHACURE',
       '195',
       '350',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI DOXY SYRUP',
       '179',
       '330',
       '7/1/2023',
       '17',
       ''
UNION ALL
SELECT 'PAPI DOXY TAB',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI IRON',
       '182',
       '332',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI MVP SYRUP',
       '83',
       '265',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI MVP TABLETS',
       '8',
       '25',
       '3/1/2022',
       '534',
       ''
UNION ALL
SELECT 'PAPI OB (120 ml)',
       '165',
       '300',
       '4/1/2023',
       '7',
       ''
UNION ALL
SELECT 'PAPI PIRANTEL',
       '30',
       '100',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI SCOUR',
       '110',
       '260',
       '6/1/2023',
       '5',
       ''
UNION ALL
SELECT 'PET CURE (DOXYCLINE TAB)',
       '10',
       '25',
       '1/1/2023',
       '194',
       ''
UNION ALL
SELECT 'PET EASE CHEWS (NV)',
       '10',
       '14',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET REBOOST 120ML',
       '393',
       '700',
       '6/1/2024',
       '11',
       ''
UNION ALL
SELECT 'PET REBOOST 30ML',
       '214',
       '365',
       '',
       '0',
       ''
UNION ALL
SELECT 'PREDNISONE 5mg TAB',
       '',
       '10',
       '',
       '4',
       ''
UNION ALL
SELECT 'PREDNISONE 10mg TAB',
       '',
       '15',
       '7/1/2023',
       '79',
       ''
UNION ALL
SELECT 'PREDNISONE SUSPENSION (Lefesone)',
       '60',
       '250',
       '2/1/2023',
       '17',
       ''
UNION ALL
SELECT 'PROBAC 100ml',
       '220',
       '380',
       '7/2/2022',
       '12',
       ''
UNION ALL
SELECT 'PROBIOTICS CAPSULES',
       '16',
       '25',
       '',
       '0',
       ''
UNION ALL
SELECT 'PROPYRINE SYRUP',
       '250',
       '380',
       '',
       '0',
       ''
UNION ALL
SELECT 'PUPPY BOOST',
       '54',
       '200',
       '',
       '0',
       ''
UNION ALL
SELECT 'RANITIDINE tab 150mg (Ranitein)',
       '',
       '25',
       '2/1/2022',
       '188',
       ''
UNION ALL
SELECT 'RAPIDAX',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'REMEND',
       '600',
       '850',
       '6/1/2022',
       '2',
       ''
UNION ALL
SELECT 'RENACURE',
       '',
       '',
       '7/1/2024',
       '27',
       ''
UNION ALL
SELECT 'SMP 500 TABLETS',
       '',
       '',
       '6/1/2024',
       '12',
       ''
UNION ALL
SELECT 'SYNTEMAX EAR SOLUTION',
       '195',
       '450',
       '3/1/2023',
       '0',
       ''
UNION ALL
SELECT 'THROMB BEAT',
       '650',
       '900',
       '9/1/2023',
       '7',
       ''
UNION ALL
SELECT 'TICK+FLEA AWAY (AS)',
       '5',
       '7',
       '3/1/2023',
       '0',
       ''
UNION ALL
SELECT 'TRITOZINE (Cotri[trime+sulfa] + Kaolin&Pectin)',
       '192',
       '250',
       '8/1/2024',
       '1',
       ''
UNION ALL
SELECT 'TOBRAMYCIN',
       '90',
       '250',
       '',
       '0',
       ''
UNION ALL
SELECT 'TOBRADEX',
       '122',
       '350',
       '',
       '0',
       ''
UNION ALL
SELECT 'TOBRASON',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'TOLFENAMIC ACID SUSPENSION (Tolfenol)',
       '193',
       '250',
       '9/1/2024',
       '21',
       ''
UNION ALL
SELECT 'TOPICURE',
       '200',
       '350',
       '1/1/2024',
       '6',
       ''
UNION ALL
SELECT 'TOPI DERM',
       '166',
       '316',
       '10/1/2023',
       '4',
       ''
UNION ALL
SELECT 'ULTRALITE PLUS',
       '15',
       '35',
       '2/1/2024',
       '55',
       ''
UNION ALL
SELECT 'VENOMA',
       '280',
       '580',
       '7/1/2023',
       '1',
       ''
UNION ALL
SELECT 'VITACHEWS',
       '',
       '15',
       '6/1/2022',
       '24',
       ''
UNION ALL
SELECT 'ZIST O',
       '',
       '450',
       '6/1/2024',
       '5',
       ''
UNION ALL
SELECT 'AMITRAZ ANTIMAGE SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'BARFY HERBAL MAINTENANCE SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'BIOLINE EAR CARE',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'BIOLINE PET STYPTIC POWDER',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'BIOLINE TEAR STAIN REMOVER',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHARCOAL SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'COAT MAITENANCE HERBAL SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOL AND CLEAN CAT LITTER',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'DENTAL HYGINE (NUTRI VET)',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'FELINE FRESH CAT LITTER',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'FURGRANCE OATMEAL SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'HAIR GROWER SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'HERBAL DOG SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'HERBAL OIL',
       '170',
       '350',
       '9/1/2023',
       '9',
       ''
UNION ALL
SELECT 'MADRE DE CACAO ANTIMAGE SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'MADRE DE CACAO HERB OIL',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'NUTRI CHUNKS SLOW FEEDING DISH',
       '',
       '',
       '',
       '3',
       ''
UNION ALL
SELECT 'NUTRI CHUNKS WATER DISPENSER',
       '',
       '',
       '',
       '1',
       ''
UNION ALL
SELECT 'PAPI BOTANICAL PET SHAMPOO 500ml',
       '',
       '',
       '5/1/2023',
       '9',
       ''
UNION ALL
SELECT 'PAPI BOTANICAL PET SHAMPOO 250ml',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAPI PURE ABACA EXTRACT SOAP',
       '',
       '150',
       '9/1/2023',
       '26',
       ''
UNION ALL
SELECT 'PAPI ANTI TICK AND FLEA SOAP',
       '',
       '150',
       '9/1/2023',
       '13',
       ''
UNION ALL
SELECT 'PAPI COAT MAINTENANCE',
       '',
       '150',
       '5/1/2022',
       '11',
       ''
UNION ALL
SELECT 'PET HAIR REMOVAL POWDER',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET''S LOVE SHAMPOO RED 1 LITTER',
       '',
       '',
       '9/1/2022',
       '9',
       ''
UNION ALL
SELECT 'PET''S LOVE SHAMPOO BLUE 1 LITTER',
       '',
       '',
       '9/1/2022',
       '13',
       ''
UNION ALL
SELECT 'PET''S LOVE SHAMPOO GREEN 1 LITTER',
       '',
       '',
       '9/1/2022',
       '3',
       ''
UNION ALL
SELECT 'PET OPTIONS GROOMING SHAMPOO',
       '143',
       '300',
       '2/5/2023',
       '9',
       ''
UNION ALL
SELECT 'PET OPTIONS ANTIFUNGAL SHAMPOO',
       '200',
       '350',
       '9/18/2023',
       '5',
       ''
UNION ALL
SELECT 'PET OPTIONS TICK & FLEA SHAMPOO',
       '214.28',
       '350',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET PALS GROOMING SHAMPOO',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET PALS TICK AND FLEA SHAMPOO',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET SMART 12IN1 SHAMPOO',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET SMART OINTMENT',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET SMART ORGANIC BAR SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'PET SMART PET SPRAY',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'ROYAL TAIL ESSENTIALS (SUMMER FIESTA)',
       '60',
       '150',
       '9/1/2024',
       '1',
       ''
UNION ALL
SELECT 'SDN SOAP (MINT)',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'SDN SOAP (PUPPIES)',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'SDN SOAP (SHAMPOO AND CONDITIONER)',
       '',
       '150',
       '6/1/2023',
       '4',
       ''
UNION ALL
SELECT 'SDN SOAP (5in1)',
       '',
       '150',
       '9/1/2023',
       '9',
       ''
UNION ALL
SELECT 'TICK AND FLEA DOG AND HOME SPRAY',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'VET CORE HERBAL DOG SOAP',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'VET CORE TICK AND FLEA SPRAY',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOG MUZZLE (S)',
       '',
       '380',
       '',
       '5',
       'Accessories'
UNION ALL
SELECT 'COLLAR BELL (PUPPY/KITTEN)',
       '15',
       '50',
       '',
       '4',
       'Accessories'
UNION ALL
SELECT 'COLLARBELL 5/8',
       '20',
       '60',
       '',
       '11',
       'Accessories'
UNION ALL
SELECT 'COLLARBELL WITH LEASH',
       '60',
       '300',
       '',
       '3',
       'Accessories'
UNION ALL
SELECT 'COLLAR (GARRISON 1inch)',
       '17',
       '75',
       '',
       '10',
       'Accessories'
UNION ALL
SELECT 'COLLAR (GARRISON 1 & 1/4inch)',
       '19',
       '100',
       '',
       '4',
       'Accessories'
UNION ALL
SELECT 'ROUND LEASH',
       '89',
       '300',
       '',
       '10',
       'Accessories'
UNION ALL
SELECT 'TRAINING LEASH',
       '81',
       '250',
       '',
       '1',
       'Accessories'
UNION ALL
SELECT 'HARNESS WITH LEASH',
       '132',
       '400',
       '',
       '1',
       'Accessories'
UNION ALL
SELECT 'CHICKEN TOY',
       '',
       '50',
       '',
       '1',
       'Accessories'
UNION ALL
SELECT '',
       '',
       '150',
       '11/01/2024',
       '80',
       'Injectible'
UNION ALL
SELECT 'ATROPINE PER ML',
       '13',
       '150',
       '04/01/2022',
       '6',
       'Injectible'
UNION ALL
SELECT 'BC XYLASED (Xylazine) BY ML',
       '',
       '',
       '01/26/2024',
       '115',
       'Injectible'
UNION ALL
SELECT 'CO-AMOX (BIOVETAMOX)',
       '10',
       '150',
       '05/13/2022',
       '342',
       'Injectible'
UNION ALL
SELECT 'CANGLOB P (Antibody)',
       '110.8',
       '350',
       '01/01/2023',
       '6',
       'Injectible'
UNION ALL
SELECT 'CANGLOB D (Antibody)',
       '',
       '',
       '07/01/2020',
       '21',
       'Injectible'
UNION ALL
SELECT 'CERENIA BY ML',
       '316',
       '450',
       '05/01/2024',
       '18',
       'Injectible'
UNION ALL
SELECT 'CIPROFLOXACIN',
       '',
       '',
       '12/01/2021',
       '50',
       'Injectible'
UNION ALL
SELECT 'COFORTA BY ML',
       '',
       '',
       '08/01/2021',
       '15',
       'Injectible'
UNION ALL
SELECT 'DCM (CALCUIM INJ)',
       '',
       '150',
       '06/01/2022',
       '70',
       'Injectible'
UNION ALL
SELECT 'DEXAMENTHASONE',
       '',
       '150',
       '06/06/2021',
       '3',
       'Injectible'
UNION ALL
SELECT 'DORMINAL (EUTHANASIA) UP TO 10 KG)',
       '',
       '1500',
       '11/01/2023',
       '190',
       'Injectible'
UNION ALL
SELECT 'DOXYCYCLINE',
       '',
       '',
       '03/01/2021',
       '70',
       'Injectible'
UNION ALL
SELECT 'DOXY-PET MONO',
       '',
       '',
       '',
       '',
       'Injectible'
UNION ALL
SELECT 'DUPHALYTE BY ML',
       '',
       '150',
       '06/18/2022',
       '970',
       'Injectible'
UNION ALL
SELECT 'ENROFLOXACIN',
       '',
       '150',
       '8/20/23',
       '30',
       'Injectible'
UNION ALL
SELECT 'EPINEPPHRINE',
       '',
       '150',
       '10/01/2023',
       '29',
       'Injectible'
UNION ALL
SELECT 'FERCOBSANG',
       '',
       '150',
       '04/25/2021',
       '75',
       'Injectible'
UNION ALL
SELECT 'HYOSCINE',
       '35',
       '150',
       '11/01/2022',
       '9',
       'Injectible'
UNION ALL
SELECT 'INTRAFER-200 B12 BY ML',
       '',
       '',
       '05/01/2021',
       '97',
       'Injectible'
UNION ALL
SELECT 'KETAMIN 100 25ml',
       '',
       '',
       '',
       '',
       'Injectible'
UNION ALL
SELECT 'LIDOCAINE',
       '',
       '',
       '02/01/2021',
       '85',
       'Injectible'
UNION ALL
SELECT 'MARBOFLOXACIN BY ML',
       '',
       '',
       '05/01/2014',
       '60',
       'Injectible'
UNION ALL
SELECT 'METOCLOPRAMIDE INJECTABLE',
       '11',
       '150',
       '04/01/2024',
       '29',
       'Injectible'
UNION ALL
SELECT 'OXYTOCIN 50ML',
       '',
       '',
       '08/01/2021',
       '65',
       'Injectible'
UNION ALL
SELECT 'POTASSIUM CHLORIDE',
       '',
       '',
       '08/01/2022',
       '360',
       'Injectible'
UNION ALL
SELECT 'RANITIDINE INJECTABLE',
       '6',
       '150',
       '03/01/2022',
       '25',
       'Injectible'
UNION ALL
SELECT 'SEPTOTRYL PER ML',
       '',
       '',
       '10/20/2020',
       '45',
       'Injectible'
UNION ALL
SELECT 'TOLFEDINE',
       '',
       '',
       '',
       '',
       'Injectible'
UNION ALL
SELECT 'TOLFINE BY ML',
       '',
       '',
       '04/12/2021',
       '97',
       'Injectible'
UNION ALL
SELECT 'TRAMADOL',
       '',
       '',
       '07/01/2022',
       '24',
       'Injectible'
UNION ALL
SELECT 'VERMIFUGE 60ML',
       '',
       '',
       '',
       '',
       'Injectible'
UNION ALL
SELECT 'VINCRISTINE',
       '',
       '',
       '09/01/2022',
       '6',
       'Injectible'
UNION ALL
SELECT 'ZOLITEL 50 (Anesthesia) 5ml',
       '',
       '',
       '11/01/2021',
       '',
       'Injectible'
UNION ALL
SELECT 'BIOCAN PUPPY',
       '137',
       '400',
       '',
       '',
       'Vaccination'
UNION ALL
SELECT 'BIOCAN R',
       '',
       '',
       '',
       '',
       'Vaccination'
UNION ALL
SELECT 'BIOCAN DhPPi + LR (6in1)',
       '179.8',
       '650',
       '',
       '',
       'Vaccination'
UNION ALL
SELECT 'BIOFEL PCHR (for cats)',
       '',
       '900',
       '07/01/2023',
       '19',
       'Vaccination'
UNION ALL
SELECT 'FELOCELL',
       '510',
       '750',
       '08/16/2022',
       '29',
       'Vaccination'
UNION ALL
SELECT 'KENNEL COUGH VACCINE',
       '',
       '500',
       '04/24/2024',
       '5',
       'Vaccination'
UNION ALL
SELECT 'NOVIBAC 2in1',
       '',
       '',
       '',
       '',
       'Vaccination'
UNION ALL
SELECT 'NOVIBAC 8IN1',
       '185',
       '450',
       '10/01/2023',
       '76',
       'Vaccination'
UNION ALL
SELECT 'VANGUARD PLUS 5L4 (8in1)',
       '',
       '',
       '',
       '0',
       'Vaccination'
UNION ALL
SELECT 'VANGUARD PLUS 5 (5 in 1)',
       '',
       '',
       '',
       '0',
       'Vaccination'
UNION ALL
SELECT 'ZOETIS DEFENSOR BY ML',
       '',
       '250',
       '09/13/2022',
       '42',
       'Vaccination'
UNION ALL
SELECT 'ALBENDAZOLE TAB',
       '',
       '100',
       '08/21/2022',
       '198',
       'Dewormers'
UNION ALL
SELECT 'CANIVERM PASTE',
       '',
       '',
       '',
       '',
       'Dewormers'
UNION ALL
SELECT 'CANIVERM PUPPY',
       '38',
       '100',
       '07/14/2023',
       '86',
       'Dewormers'
UNION ALL
SELECT 'CANIVERM ADULT',
       '50',
       '100',
       '05/24/2023',
       '27',
       'Dewormers'
UNION ALL
SELECT 'DRONTAL 1ML SYRUP',
       '7',
       '100',
       '11/01/2021',
       '96',
       'Dewormers'
UNION ALL
SELECT 'DRONTAL TAB',
       '92',
       '150',
       '',
       '0',
       'Dewormers'
UNION ALL
SELECT 'MEBENDAZOLE',
       '',
       '',
       '',
       '0',
       'Dewormers'
UNION ALL
SELECT 'PRAZILEX',
       '35',
       '100',
       '',
       '0',
       'Dewormers'
UNION ALL
SELECT 'VOLAPETS',
       '136',
       '250',
       '',
       '0',
       'Dewormers'
UNION ALL
SELECT 'WORMRID',
       '36',
       '100',
       '',
       '0',
       'Dewormers'
UNION ALL
SELECT 'BRAVECTO 2kg-4.5kg',
       '',
       '1350',
       '09/01/2021',
       '7',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'BRAVECTO 4.5kg-10kg',
       '',
       '1550',
       '',
       '0',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'BRAVECTO 10kg-20kg',
       '',
       '1850',
       '10/01/2022',
       '11',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'BRAVECTO 20kg-40kg',
       '',
       '2000',
       '',
       '0',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'HEARTGUARD (BLUE) 11 KG',
       '',
       '300',
       '02/01/2023',
       '7',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'HEARTGUARD (BROWN)  23 KG. - 45 KG',
       '',
       '320',
       '01/01/2023',
       '23',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'HEARTGUARD (GREEN) 12 KG. - 22 KG',
       '',
       '340',
       '01/01/2023',
       '11',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA S (5.6-11.0) violet',
       '406',
       '550',
       '11/01/2023',
       '9',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA M (11.1-22.0)brown',
       '439',
       '600',
       '09/01/2023',
       '3',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA L (22.1- 44.0) blue',
       '451',
       '650',
       '12/01/2022',
       '8',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA XL (44.1-88.0) green',
       '494',
       '700',
       '09/01/2023',
       '7',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA TRIO VIOLET (2.5-5kgs)',
       '517',
       '660',
       '03/01/2023',
       '10',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA TRIO BROWN (5-10kgs)',
       '550',
       '720',
       '03/02/2023',
       '0',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA TRIO BLUE (10-20kgs)',
       '583',
       '760',
       '02/01/2023',
       '8',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'SIMPARICA TRIO GREEN (20-40kgs)',
       '616',
       '810',
       '02/02/2023',
       '6',
       'Tick & Flea Prevention'
UNION ALL
SELECT 'BLOOD ADMINISTRATION SET',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'CATHETER',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'COTTON',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'GAUZE PADS',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'NASAL OXYGEN CANNULA',
       '30',
       '50',
       '',
       '10',
       'Clinic Use'
UNION ALL
SELECT 'NEEDLE SYRINGE SIZE ___',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'NEEDLE SYRINGE SIZE ___',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'NEEDLE SYRINGE SIZE ___',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'SYRINGE 1 CC',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'SYRINGE 3 CC',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'MICRONIZED ALUMINUM SPRAY',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'SELF ADHERING WRAP',
       '',
       '',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'UNDERPADS',
       '18',
       '0',
       '',
       '',
       'Clinic Use'
UNION ALL
SELECT 'VIOLET 26G',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'YELLOW 24G',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'BLUE 22G',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'PINK 20G',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT '0.9% SODIUM CHLORIDE (NSS)',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT '5% DEXTROSE LRS',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'LACTATED RINGER''S',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT '',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'CHROMIC 2-O',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'NYLON 0',
       '21',
       '350',
       '',
       '',
       ''
UNION ALL
SELECT 'PGA 0',
       '95',
       '350',
       '',
       '',
       ''
UNION ALL
SELECT 'PGA 2-O',
       '95',
       '350',
       '',
       '',
       ''
UNION ALL
SELECT 'PGA 3-O',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'POLIGLECAPRONE 4-O',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'POLYDIOXANONE 2-O',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'POLYGLACTIN 3-O',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SILK 2-O (braided)',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SILK 2-O (braided) 3 metric',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SILK 3-O (braided)',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SILK 4-O (braided)',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SIZE 11',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SIZE 15',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SIZE 20',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'SIZE 21',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'BABESIA TEST',
       '',
       '550',
       '12/01/2021',
       '11',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'CANINE PARVOVIRUS TEST (CPV)',
       '200',
       '550',
       '03/18/2023',
       '28',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'CANINE DISTEMPER VIRUS TEST (CDV)',
       '200',
       '550',
       '06/22/2022',
       '47',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'EHRLICHIA TEST',
       '242',
       '700',
       '',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'HEARTWORM TEST',
       '208',
       '600',
       '01/01/2023',
       '39',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'FIV/FELV (FELINE/CAT)',
       '325',
       '750',
       '03/08/2023',
       '8',
       'Laboratory Test & Kits'
UNION ALL
SELECT '4 WAY TEST KIT (SNAP 4Dx)',
       '',
       '1700',
       '02/24/2022',
       '2',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'EHR+ANA+BAB',
       '542',
       '1200',
       '11/01/2022',
       '47',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'CPV+EHR+GIA',
       '475',
       '1200',
       '',
       '',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'EHR-ANA TEST',
       '',
       '',
       '06/01/2022',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'PARVO + DISTEMPER TEST (CPV+CDV)',
       '317',
       '1000',
       '',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'PREGNANCY TEST (RELAXIN)',
       '380',
       '750',
       '',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'SNAP LEPTO',
       '',
       '1500',
       '',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'SNAP PARVO',
       '',
       '1200',
       '',
       '0',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'SCHIRMER TEAR TEST strips',
       '',
       '',
       '',
       '100',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'FLURESCEIN SODIUM OPTHALMIC STRIPS',
       '',
       '',
       '',
       '98',
       'Laboratory Test & Kits'
UNION ALL
SELECT 'BONE TREATS',
       '',
       '',
       '',
       '',
       'Pet Food'
UNION ALL
SELECT 'BEEF PRO PUPPY BY KILO',
       '',
       '',
       '',
       '31',
       'Pet Food'
UNION ALL
SELECT 'ROYAL CANINE RECOVERY',
       '',
       '',
       '',
       '0',
       'Pet Food'
UNION ALL
SELECT 'SDN PUPPY/ADULT BY KILO',
       '',
       '200',
       '',
       '13',
       'Pet Food'
UNION ALL
SELECT 'SPECIAL DOG',
       '',
       '95',
       '',
       '4',
       'Pet Food'
UNION ALL
SELECT 'SPECIAL CAT',
       '',
       '',
       '',
       '0',
       'Pet Food'
UNION ALL
SELECT 'WOFFY ADULT BY KILO',
       '76',
       '100',
       '',
       '95',
       'Pet Food'
UNION ALL
SELECT 'WOOFY PUPPY BY KILO',
       '100',
       '135',
       '',
       '89',
       'Pet Food'

GO 
