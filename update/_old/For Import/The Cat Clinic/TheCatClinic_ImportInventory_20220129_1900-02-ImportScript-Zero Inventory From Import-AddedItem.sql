DECLARE @ID_Company INT = 219

DECLARE @ForInsert Table (Name Varchar(MAX))

INSERT INTO @ForInsert 
VALUES
('CHEM 17'),
('CL-EAR Cleaner'),
('Filpronil  FRONTLINE 100ml (DO NOT RESTOCK)'),
('Gauze ROLL'),
('GILLET SHAVER'),
('GLOVES LATEX #8'),
('GLOVES#6 unimex'),
('LAB FORM'),
('LINT ROLLER'),
('LITTER BOX'),
('MEOW MIX SALMON&CRAB'),
('MEOW MIX TENDER FAVORITES TUNA &SHRIMP'),
('MEOW MIX TUNA'),
('Micropore Tape 0.5 IN'),
('NEEDLE HOLDER'),
('ORANGE NEEDLE'),
('Ornipural 100ML'),
('PET HAIR REMOVER'),
('Portable Skin Analyzer Lamp'),
('RAZOR BLADE'),
('RECHARGEABLE HOT COMPRESS'),
('STATEMENT OF ACOUNT'),
('STERILIZATION POUCHES'),
('SURGICAL GLOVES #7'),
('SYRINGE #50CC'),
('TREATS (CIAO)) 50''S'),
('U.V Lamp'),
('UNIMEX SURGICAL GLOVES'),
('VET SOLUTION MONGE  WET RECOVERY')

INSERT tItem
       (ID_Company,
        Name,
        ID_ItemType,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company,
       Name,
       2,
	   1,
       'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @ForInsert record