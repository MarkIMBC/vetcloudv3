DECLARE @ID_Company INT = 219
DECLARE @forImport TABLE
  (
     ID_Item   Int,
     Name_Item Varchar(MAX),
     Quantity  int
  )

INSERT @forImport
       (ID_Item,
        Name_Item,
        Quantity)
SELECT DISTINCT NULL,
                LTRIM(RTRIM(REPLACE([Item Name], '  ', ''))),
                [Quantity]
FROM   tTCC_20220130_0712
WHERE  Quantity > 0

Update @forImport
SET    ID_Item = item.ID
FROM   @forImport import
       inner join tItem item
               on LTRIM(RTRIM(REPLACE(import.Name_Item, '  ', ' '))) = LTRIM(RTRIM(REPLACE(item.Name, '  ', ' ')))

DELETE FROM @forImport
WHERE  ID_Item IS NOT NULL

Update @forImport
SET    ID_Item = [Item_ID]
FROM   @forImport import
       inner join [ITEM_CDO_Uptown_Branch_reference] itemRef
               on LTRIM(RTRIM(REPLACE([Item from import file], '  ', ' '))) = LTRIM(RTRIM(REPLACE(import.Name_Item, '  ', ' ')))
WHERE  ID_Item IS NULL

DELETE FROM @forImport
WHERE  ID_Item IS NOT NULL

INSERT tItem
       (ID_Company,
        Name,
        ID_ItemType,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        CurrentInventoryCount)
SELECT @ID_Company,
       record.Name_Item,
       2,
	   1,
       'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1,
       record.Quantity
FROM   @forImport record

Update @forImport
SET    ID_Item = item.ID
FROM   @forImport import
       inner join tItem item
               on LTRIM(RTRIM(REPLACE(import.Name_Item, '  ', ' '))) = LTRIM(RTRIM(REPLACE(item.Name, '  ', ' ')))

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.Quantity,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
		'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport import 
WHERE  Quantity > 0 
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
 @ID_UserSession
 
SELECT * FROM @adjustInventory

SELECT ID_Item,
       LTRIM(RTRIM(REPLACE(Name_Item, '  ', ''))),
       Quantity
FROM   @forImport
ORDER  BY LTRIM(RTRIM(REPLACE(Name_Item, '  ', ''))) 
