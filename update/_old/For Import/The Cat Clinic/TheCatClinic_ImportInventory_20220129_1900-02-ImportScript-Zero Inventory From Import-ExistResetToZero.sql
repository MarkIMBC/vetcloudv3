if OBJECT_ID('dbo.[TheCatClinic_ImportInventory_ResetToZero.xlsx]') is not null
  BEGIN
      DROP TABLE [TheCatClinic_ImportInventory_ResetToZero.xlsx]
  END

GO

CREATE TABLE [dbo].[TheCatClinic_ImportInventory_ResetToZero.xlsx]
  (
     [ID]    INT,
     [Name]  varchar(500),
     [Count] INT
  )

GO

INSERT INTO [dbo].[TheCatClinic_ImportInventory_ResetToZero.xlsx]
            ([ID],
             [Name],
             [Count])
SELECT '84312',
       '2/0 Surgical Suture Metric 3 (polydioxanone) violet',
       '0'
UNION ALL
SELECT '84036',
       'Amilyte-C 100ML',
       '0'
UNION ALL
SELECT '84070',
       'CANESTEN SOL  (20ML)',
       '0'
UNION ALL
SELECT '84223',
       'CATHULA IV CANNULA',
       '0'
UNION ALL
SELECT '84212',
       'CO-AMOXICLAV RAFONEX',
       '0'
UNION ALL
SELECT '84213',
       'CO-AMOXICLAV SUNVAX vials',
       '2'
UNION ALL
SELECT '84234',
       'DAwnsail FBNP (test kit)',
       '6'
UNION ALL
SELECT '84235',
       'Dawnsail FT4 (test kit) (DO NOT RESTOCK)',
       '16'
UNION ALL
SELECT '84236',
       'Dawnsail SAA (test kit) (DO NOT RE STOCK)',
       '0'
UNION ALL
SELECT '84262',
       'ETHICON VIOLET',
       '0'
UNION ALL
SELECT '84268',
       'Felivet FCOV (test kit) (DO NOT RE STOCK)',
       '0'
UNION ALL
SELECT '84187',
       'Gastrointestinal Monge Pharmacy WET',
       '0'
UNION ALL
SELECT '84276',
       'GLYCERIN (LAXATIVE)',
       '0'
UNION ALL
SELECT '84279',
       'HeMacare - FE 60ml',
       '0'
UNION ALL
SELECT '84076',
       'HIMALAYA IMMUNOL SYRUP GREEN 100ML',
       '0'
UNION ALL
SELECT '84285',
       'Himalaya Nefrotec DS',
       '3'
UNION ALL
SELECT '84082',
       'Hydrogen Peroxide',
       '3'
UNION ALL
SELECT '84081',
       'HYPODERMIC NEEDLE DISPOSABLE 100PS',
       '2'
UNION ALL
SELECT '84097',
       'IMIDOCARB DIPROPIONATE (IMIZOL)',
       '0'
UNION ALL
SELECT '84252',
       'JOJOBA Ear Cleansing',
       '0'
UNION ALL
SELECT '84111',
       'Lactated Ringer Solution 1000ml',
       '0'
UNION ALL
SELECT '84113',
       'LACTULOSE SYRUP 120ML (DUHLAX)',
       '0'
UNION ALL
SELECT '84106',
       'Lactulose(ACCELLAC) SYRUP 120ML',
       '3'
UNION ALL
SELECT '102615',
       'MEGADERM',
       '0'
UNION ALL
SELECT '84129',
       'METRONIDAZOLE DARYL 60ml',
       '0'
UNION ALL
SELECT '84127',
       'Mondex 100G',
       '6'
UNION ALL
SELECT '84144',
       'Nobivac Rabies 1 Dose',
       '0'
UNION ALL
SELECT '84150',
       'NUTRI VET EAR CLEANSER',
       '0'
UNION ALL
SELECT '84147',
       'Nutriblend',
       '0'
UNION ALL
SELECT '84146',
       'Nutrical 60ml',
       '0'
UNION ALL
SELECT '84161',
       'POLYNERV 12O ML',
       '0'
UNION ALL
SELECT '84195',
       'RC GASTROTESTINAL DRY',
       '0'
UNION ALL
SELECT '84186',
       'Royal Canin Urinary S/o Pharmacy',
       '0'
UNION ALL
SELECT '84181',
       'Royal Canin Urinary Wet Pharmacy',
       '0'
UNION ALL
SELECT '84306',
       'TOBRAMYCIN  (OPTOB)',
       '0'
UNION ALL
SELECT '84305',
       'TOBRAMYCIN (RAMITOB)',
       '0'
UNION ALL
SELECT '84318',
       'VETALEXIN ORAL 60ML',
       '3'
UNION ALL
SELECT '84317',
       'Vetopic 50',
       '0'
UNION ALL
SELECT '84320',
       'whole blood seperator IDEXX',
       '0'

GO 




DECLARE @ID_Company INT = 219
DECLARE @forImport TABLE
  (
     ID_Item   Int,
     Name_Item Varchar(MAX),
     Quantity  int
  )

INSERT @forImport
       (ID_Item,
        Name_Item,
        Quantity)
SELECT DISTINCT ID,
                LTRIM(RTRIM(REPLACE([Name], '  ', ''))),
                Count
FROM   [TheCatClinic_ImportInventory_ResetToZero.xlsx]


Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       item.CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
		'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       0
FROM   @forImport import inner join tItem item on import.ID_Item = item.ID
WHERE  item.CurrentInventoryCount > 0 
ORDER  BY ID_Item
 
exec pReceiveInventory
  @adjustInventory,
 @ID_UserSession