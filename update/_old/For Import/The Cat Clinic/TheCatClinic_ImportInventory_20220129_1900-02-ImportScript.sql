DECLARE @Table as TAble
  (
     ID_Company   INT,
     DateModified Datetime
  )

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPatient_SOAP soap
       inner join vCompanyActive c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateApproved, soap.DateCreated)
FROm   tPatient_SOAP soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tPatient_SOAP soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateDone, soap.DateCreated)
FROm   tPatient_SOAP soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPatient soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tClient soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tBillingInvoice soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateApproved, soap.DateCreated)
FROm   tBillingInvoice soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tBillingInvoice soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPatient_Confinement soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateDischarge, soap.DateCreated)
FROm   tPatient_Confinement soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tPatient_Confinement soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPatient_Wellness soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tPatient_Wellness soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPurchaseOrder soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateApproved, soap.DateCreated)
FROm   tPurchaseOrder soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tPurchaseOrder soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCancelled, soap.DateCreated)
FROm   tPurchaseOrder soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT rr.ID_Company,
       rr.DateCreated
FROm   tReceivingReport rr
       inner join tCompany c
               on rr.ID_Company = c.ID
			   
INSERT @Table
SELECT rr.ID_Company,
       rr.DateModified
FROm   tReceivingReport rr
       inner join tCompany c
               on rr.ID_Company = c.ID
			   
INSERT @Table
SELECT rr.ID_Company,
       rr.DateApproved
FROm   tReceivingReport rr
       inner join tCompany c
               on rr.ID_Company = c.ID

INSERT @Table
SELECT rr.ID_Company,
       rr.DateCanceled
FROm   tReceivingReport rr
       inner join tCompany c
               on rr.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPaymentTransaction soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateApproved, soap.DateCreated)
FROm   tPaymentTransaction soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateCanceled, soap.DateCreated)
FROm   tPaymentTransaction soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tItem soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tPatientAppointment soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT soap.ID_Company,
       ISNULL(soap.DateModified, soap.DateCreated)
FROm   tSupplier soap
       inner join tCompany c
               on soap.ID_Company = c.ID

INSERT @Table
SELECT u.ID_Company,
       soap.Date
FROm   tAuditTrail soap
       inner join vUser u
               on u.ID = soap.ID_User
       inner join tCompany c
               on u.ID_Company = c.ID

INSERT @Table
SELECT invtTrail.ID_Company,
       invtTrail.DateCreated
FROm   tInventoryTrail invtTrail 
       inner join tCompany c
               on invtTrail.ID_Company = invtTrail.ID_Company

INSERT @Table
SELECT creditLog.ID_Company,
       creditLog.DateCreated
FROm   tClient_CreditLogs creditLog 
       inner join tCompany c
               on creditLog.ID_Company = creditLog.ID_Company


INSERT @Table
SELECT payable.ID_Company,
       payable.DateCreated
FROm   tPayable payable 
       inner join tCompany c
               on payable.ID_Company = payable.ID_Company


INSERT @Table
SELECT payable.ID_Company,
       payable.DateModified
FROm   tPayable payable 
       inner join tCompany c
               on payable.ID_Company = payable.ID_Company
			   

INSERT @Table
SELECT payablePayment.ID_Company,
       payablePayment.DateCreated
FROm   tPayablePayment payablePayment 
       inner join tCompany c
               on payablePayment.ID_Company = payablePayment.ID_Company


SELECT c.Name,
       Max(t.DateModified) DateModified
FROm   @Table t
       INNER JOIN vCompanyActive c
               on t.ID_Company = c.ID
GROUP  by Name
ORDER  BY Max(t.DateModified) DESC

/*Company Usage Count Per Day*/
DECLARE @CompanyUsageCountPerDay TABLE
  (
     ID_Company      INT,
     Name_Company    Varchar(MAX),
     Date            Date,
     ClinicUsedCount Int
  )
DECLARE @Dates Table
  (
     Date Date
  )

INSERT @Dates
SELECT Date
FROM   dbo.fGetDatesByDateCoverage('2021-01-01', GETDATE())

SELECT datePerDate.Date,
       ISNULL(TotalClinicUsed, 0) ClinicUsedCount
FROm   (SELECT tbl.LastDateUsage DateUsage,
               Count(*)          TotalClinicUsed
        FROM   (SELECT DISTINCT Convert(Date, t.DateModified) LastDateUsage
                FROm   @Table t
                       INNER JOIN vCompanyActive c
                               on t.ID_Company = c.ID
                WHERE  c.ID <> 1) tbl
        GROUP  BY tbl.LastDateUsage) tblGrand
       RIGHT JOIN @Dates datePerDate
               ON datePerDate.Date = tblGrand.DateUsage
Order  BY datePerDate.Date DESC

/*Company Usage List Pivot*/
SELECT Name_Company,
       [2022-01-01],
       [2022-01-02],
       [2022-01-03],
       [2022-01-04],
       [2022-01-05],
       [2022-01-06],
       [2022-01-07],
       [2022-01-08],
       [2022-01-09],
       [2022-01-10],
       [2022-01-11],
       [2022-01-12],
       [2022-01-13],
       [2022-01-14],
       [2022-01-15],
       [2022-01-16],
       [2022-01-17],
       [2022-01-18],
       [2022-01-19],
       [2022-01-20],
       [2022-01-21],
       [2022-01-22],
       [2022-01-23],
       [2022-01-24],
       [2022-01-25],
       [2022-01-26],
       [2022-01-27],
       [2022-01-28],
       [2022-01-29],
       [2022-01-30],
       [2022-01-31]
FROM   (SELECT Distinct c.ID                          ID_Company,
                        c.Name                        Name_Company,
                        COnvert(date, t.DateModified) DateUsage,
                        'Yes'                         IsUsed
        FROM   @Table t
               Inner join vCompanyActive c
                       on c.ID = t.ID_Company
               RIGHT JOIN @Dates dateDaily
                       on COnvert(date, t.DateModified) = COnvert(date, dateDaily.Date)) AS SourceTable
       PIVOT ( MAX(IsUsed)
             FOR DateUsage IN ( [2022-01-01],
                                [2022-01-02],
                                [2022-01-03],
                                [2022-01-04],
                                [2022-01-05],
                                [2022-01-06],
                                [2022-01-07],
                                [2022-01-08],
                                [2022-01-09],
                                [2022-01-10],
                                [2022-01-11],
                                [2022-01-12],
                                [2022-01-13],
                                [2022-01-14],
                                [2022-01-15],
                                [2022-01-16],
                                [2022-01-17],
                                [2022-01-18],
                                [2022-01-19],
                                [2022-01-20],
                                [2022-01-21],
                                [2022-01-22],
                                [2022-01-23],
                                [2022-01-24],
                                [2022-01-25],
                                [2022-01-26],
                                [2022-01-27],
                                [2022-01-28],
                                [2022-01-29],
                                [2022-01-30],
                                [2022-01-31] ) ) AS PivotTable;

Declare @Ids_CompanyInUse TABLE
  (
     ID_Company INT,
     Date       Date
  )

INSERT @Ids_CompanyInUse
SELECT DISTINCT ID_Company,
                t.DateModified
FROM   @Table t
WHERE  Convert(Date, t.DateModified) = Convert(Date, DATEADD(DAY, -1, GETDATE()))

SELECT DISTINCT cacvtive.Name,
                Date
FROm   @Ids_CompanyInUse ids
       INNER JOIN vCompanyActive cacvtive
               on ids.ID_Company = cacvtive.ID
Order  By cacvtive.Name ASC

SELECT cacvtive.Name
FROm   vCompanyActive cacvtive
WHERE  ID NOT IN (SELECT ID_Company
                  FROM   @Ids_CompanyInUse)
Order  By cacvtive.Name ASC 
