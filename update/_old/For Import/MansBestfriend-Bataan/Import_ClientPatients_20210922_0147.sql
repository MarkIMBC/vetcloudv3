DELETE FROM Bataanimportmansbestfriend
where  F2 IS NULL

--drop TABLE Bataanimportmansbestfriend
DECLARE @DuplicateClientName TABLE
  (
     Name Varchar(MAX)
  )
DECLARE @Table TABLE
  (
     Code           VARCHAR(1000),
     ClientName     VARCHAR(1000),
     ContactNumber  VARCHAR(1000),
     BillingAddress VARCHAR(1000),
     Pets           VARCHAR(1000)
  )
DECLARE @ImportCLientPatient TABLE
  (
     Code           VARCHAR(1000),
     ClientName     VARCHAR(1000),
     ContactNumber  VARCHAR(1000),
     BillingAddress VARCHAR(1000),
     Pets           VARCHAR(1000)
  )

INSERT @DuplicateClientName
select F2
FROm   Bataanimportmansbestfriend
GROUP  BY F2
HAVING COUNT(*) > 1
ORDER  BY F2

INSERT @Table
select [F1],
       LTRIM(RTRIM([F2])),
       cast(CAST([F3] AS bigint) AS varchar(50)),
       [F4],
       REPLACE(ISNULL([F5], ''), '-', '')
FROm   Bataanimportmansbestfriend
ORDER  BY F2

DECLARE @ID_Company INT = 109
DECLARE @Code VARCHAR(1000)
DECLARE @ClientName VARCHAR(1000)
DECLARE @ContactNumber VARCHAR(1000)
DECLARE @BillingAddress VARCHAR(1000)
DECLARE @Pets VARCHAR(1000)
DECLARE db_cursor CURSOR FOR
  SELECT Code,
         ClientName,
         ContactNumber,
         BillingAddress,
         Pets
  FROM   @Table

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Code,
                               @ClientName,
                               @ContactNumber,
                               @BillingAddress,
                               @Pets

WHILE @@FETCH_STATUS = 0
  BEGIN
      Declare @_Pets Table
        (
           Name VARCHAR(MAX)
        )
      DECLARE @ID_Client INT = 0

      IF CHARINDEX('/', @Pets) > 0
        INSERT @_Pets
        SELECT Part
        FROM   dbo.fGetSplitString(@Pets, '/')
      ELSE IF CHARINDEX(',', @Pets) > 0
        INSERT @_Pets
        SELECT RTRIM(LTRIM(Part))
        FROM   dbo.fGetSplitString(@Pets, ',')
      ELSE
        INSERT @_Pets
        SELECT @Pets

      INSERT @ImportCLientPatient
      SELECT @Code,
             @ClientName,
             @ContactNumber,
             @BillingAddress,
             Name
      FROM   @_Pets
      where  LEN(Name) > 0

      SET @ContactNumber = ISNULL(@ContactNumber, '')

      IF LEN(@ContactNumber) = 10
        SET @ContactNumber = '0' + @ContactNumber
		
      INSERT dbo.tClient
             (ID_Company,
              tempID,
			  CustomCode,
              Code,
              Name,
              Address,
              ContactNumber,
              IsActive,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              Comment)
      VALUES( @ID_Company,
              @Code,
			  @Code,
              @Code,
              @ClientName,
              @BillingAddress,
              @ContactNumber,
              1,
              GETDATE(),
              GETDATE(),
              1,
              1,
              'Imported '
              + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'))

      SET @ID_Client = @@IDENTITY

      INSERT tPatient
             (ID_Company,
              ID_Client,
              Name,
              Comment,
              IsActive,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy)
      SELECT DIStinct @ID_Company,
                      @ID_Client,
                      REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(Name)), CHAR(13), ''), CHAR(10), ''), CHAR(9), ''),
                      'Imported '
                      + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                      1,
                      GETDATE(),
                      GETDATE(),
                      1,
                      1
      FROM   @_Pets
      WHERE  LEN(Name) > 0

      DELETE FROM @_Pets

      FETCH NEXT FROM db_cursor INTO @Code,
                                     @ClientName,
                                     @ContactNumber,
                                     @BillingAddress,
                                     @Pets
  END

CLOSE db_cursor

DEALLOCATE db_cursor
 