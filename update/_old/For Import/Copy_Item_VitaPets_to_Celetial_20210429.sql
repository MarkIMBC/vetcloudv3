DECLARE @ID_Company_VitapetsAnimalClinicandPetSupplies INT = 47
DECLARE @ID_Company_Celestials_Animal_Clinic INT = 50
 
INSERT INTO [dbo].[tItem]
            ([Code],
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [MinInventoryCount],
             [MaxInventoryCount],
             [UnitCost],
             [UnitPrice],
             [CurrentInventoryCount],
             [Old_item_id],
             [Old_procedure_id],
             [OtherInfo_DateExpiration],
             [ID_InventoryStatus])
SELECT [Code],
       [Name],
       [IsActive],
       @ID_Company_Celestials_Animal_Clinic,
       [Comment],
       [DateCreated],
       [DateModified],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [ID_ItemType],
       [ID_ItemCategory],
       0,
       0,
       [UnitCost],
       [UnitPrice],
       0,
       [Old_item_id],
       [Old_procedure_id],
       [OtherInfo_DateExpiration],
       [ID_InventoryStatus]
FROM   tItem
WHERE  ID_Company = @ID_Company_VitapetsAnimalClinicandPetSupplies
       AND IsActive = 1

GO 
