DECLARE @ID_Company_PetAdventure INT = 164
DECLARE @forImportClient TABLE
  (
     Name_Client    VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX),
     EmailAddress   VARCHAR(MAX),
     tempID         VARCHAR(MAX),
     Address_Client VARCHAR(MAX),
     Patients       VARCHAR(MAX)
  )

INSERT @forImportClient
select [Last Name] + ', ' + [First Name],
       CONVERT(BIGINT, [Phone]),
       '',
       ISNULL([Email], ''),
       [Customer Id],
       ISNULL([Address Line 1], '') + ' '
       + ISNULL([Address Line 2], '') + ' '
       + ISNULL([City], '') + ' ' + ISNULL([State], '')
       + ' ' + ISNULL([Postal Code], ''),
       ISNULL([Tags], '')
FROm   PetAdventureClients

INSERT dbo.tClient
       (ID_Company,
        Name,
        Address,
        ContactNumber,
        ContactNumber2,
        Email,
        tempID,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT @ID_Company_PetAdventure,
       Name_Client,
             MAX(Address_Client),
       '0' + MAX(ContactNumber),
       MAX(ContactNumber2),
       MAX(EmailAddress),
       MAX(tempID),
       1,
       Getdate(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @forImportClient
where  Name_Client is NOT NULL
GROUP  BY Name_Client
ORDER BY Name_Client

DECLARE @ID_Client    INT,
        @Name_Patient VARCHAR(MAX)= ''
DECLARE @forImportPatient TABLE
  (
     ID_Client    INT,
     Name_Patient VARCHAR(MAX)
  )
DECLARE import_cursor CURSOR FOR
  SELECT MAX(client.ID),
         importCLient.Patients
  FROm   tClient client
         inner join @forImportClient importCLient
                 on client.Name = importCLient.Name_Client
  where  ID_Company = @ID_Company_PetAdventure
         AND LEN(LTRIM(RTRIM(importCLient.Patients))) > 0
  GROUP  BY client.Name,
            importCLient.Patients

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @ID_Client, @Name_Patient

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT @forImportPatient
             (ID_Client,
              Name_Patient)
      SELECT @ID_Client,
             ParT
      FROM   dbo.fGetSplitString(@Name_Patient, ',')

      FETCH NEXT FROM import_cursor INTO @ID_Client, @Name_Patient
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company_PetAdventure,
       ID_Client,
       Name_Patient,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @forImportPatient
/*
	DECLARE @ID_Company_PetAdventure INT = 164
	DELETE FROM tClient where ID_Company = @ID_Company_PetAdventure
	DELETE FROM tPatient where ID_Company = @ID_Company_PetAdventure
*/
