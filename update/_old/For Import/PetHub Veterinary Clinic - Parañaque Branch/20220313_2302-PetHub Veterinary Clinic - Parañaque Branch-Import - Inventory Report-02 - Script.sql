DEclare @GUID_Company VARCHAR(MAX) = '7C0A3310-E268-4712-8E7A-7DB0D065790C'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

DECLARE @forImport TABLE
  (
     ID_Item   int,
     Name_Item varchar(500),
     UnitCost  DECIMAL(18, 2),
     UnitPrice DECIMAL(18, 2),
     Quantity  Int
  )

INSERT @forImport
       (Name_Item,
        UnitCost,
        UnitPrice,
        Quantity)
SELECT dbo.fGetCleanedString([Item]),
       TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString([Buying Price])),
       TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString([Selling Price])),
       [Quantity]
FROM   [20220313_2302-PetHub Veterinary Clinic - Para�aque Branch-Import - Inventory Report]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company

DELETE FROM @forImport
WHERE  LEN(Name_Item) = 0

Update @forImport
set    UnitCost = ISNULL(UnitCost, 0)

Update @forImport
set    UnitPrice = ISNULL(UnitPrice, 0)

Update tItem
SET    UnitCost = import.UnitCost,
       UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @ForImport import
               on import.ID_Item = item.ID
WHERE  ID_Company = @ID_Company 


SELECT * FROm @ForImport WHERE ID_Item IS NULL
