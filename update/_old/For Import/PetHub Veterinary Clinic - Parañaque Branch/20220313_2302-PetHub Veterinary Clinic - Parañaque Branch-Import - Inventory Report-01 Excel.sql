if OBJECT_ID('dbo.[20220313_2302-PetHub Veterinary Clinic - Para�aque Branch-Import - Inventory Report]') is not null
  BEGIN
      DROP TABLE [20220313_2302-PetHub Veterinary Clinic - Para�aque Branch-Import - Inventory Report]
  END

GO

CREATE TABLE [dbo].[20220313_2302-PetHub Veterinary Clinic - Para�aque Branch-Import - Inventory Report]
  (
     [Item]          varchar(500),
     [Buying Price]  varchar(500),
     [Selling Price] varchar(500),
     [Quantity]      int,
     [Status]        varchar(500)
  )

GO

INSERT INTO [dbo].[20220313_2302-PetHub Veterinary Clinic - Para�aque Branch-Import - Inventory Report]
            ([Item],
             [Buying Price],
             [Selling Price],
             [Quantity],
             [Status])
SELECT '0.9 SODIUM CHLORIDE (NSS-GREEN)',
       '75',
       '',
       0,
       ''
UNION ALL
SELECT '1 cc SYRINGE',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT '10 CC SYRINGE',
       '320',
       '0',
       0,
       ''
UNION ALL
SELECT '25 CC SYRINGE',
       '550',
       '0',
       0,
       ''
UNION ALL
SELECT '3 CC SYRINGE',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT '5 CC SYRINGE',
       '300',
       '0',
       0,
       ''
UNION ALL
SELECT 'ACEPROMAZINE',
       '720',
       '0',
       0,
       ''
UNION ALL
SELECT 'ADVOCATE FOR CATS 4kg and Up',
       '0',
       '0',
       0,
       ''
UNION ALL
SELECT 'ADVOCATE FOR CATS up to 4kg',
       '891',
       '0',
       0,
       ''
UNION ALL
SELECT 'ALCOHOL GALLON',
       '450',
       '0',
       0,
       ''
UNION ALL
SELECT 'AMITRAZ',
       '320',
       '0',
       0,
       ''
UNION ALL
SELECT 'AMOXYCYILLIN 150 LA, 100 mL Inj',
       '689',
       '0',
       0,
       ''
UNION ALL
SELECT 'AMPICILLIN 250mg',
       '100',
       '0',
       0,
       ''
UNION ALL
SELECT 'AMPICILLIN 500mg',
       '80',
       '0',
       0,
       ''
UNION ALL
SELECT 'Arthropet',
       '0',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS ANTI-COPROPHAGIC',
       '700',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS HEALTHY COAT',
       '700',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS HEALTHY HEART',
       '700',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS HEALTHY LIVER CHEWS',
       '935',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS HEALTHY VISION',
       '935',
       '25',
       0,
       ''
UNION ALL
SELECT 'AS K-9 EAR DROPS',
       '460',
       '550',
       0,
       ''
UNION ALL
SELECT 'AS K-9 EYE DROPS',
       '460',
       '550',
       0,
       ''
UNION ALL
SELECT 'ATOM BALL MEDIUM',
       '60',
       '150',
       0,
       ''
UNION ALL
SELECT 'BIG LEASH TWIST LONG',
       '240',
       '550',
       0,
       ''
UNION ALL
SELECT 'BIOGENTA DROPS 10 mL',
       '168',
       '550',
       0,
       ''
UNION ALL
SELECT 'BIOVAC RABIES VACCINE 10Ds',
       '537',
       '0',
       0,
       ''
UNION ALL
SELECT 'BLACK ARMOUR PET',
       '464',
       '700',
       0,
       ''
UNION ALL
SELECT 'BRAVECTO 10kg - 20kg',
       '1005',
       '1350',
       0,
       ''
UNION ALL
SELECT 'BRAVECTO 20kg and Up',
       '1005',
       '1420',
       0,
       ''
UNION ALL
SELECT 'BRAVECTO 2kg - 4.5kg',
       '1005',
       '1200',
       0,
       ''
UNION ALL
SELECT 'BRAVECTO 4kg - 10kg',
       '1005',
       '1270',
       0,
       ''
UNION ALL
SELECT 'BREWER''S YEAST GARLIC (1000T)',
       '1',
       '0',
       0,
       ''
UNION ALL
SELECT 'BROADLINE FOR CATS (LARGE)',
       '0',
       '700',
       0,
       ''
UNION ALL
SELECT 'BROADLINE FOR CATS (SMALL)',
       '0',
       '580',
       0,
       ''
UNION ALL
SELECT 'BROMHEXINE INJECTABLE',
       '356',
       '0',
       0,
       ''
UNION ALL
SELECT 'BRONCHICINE (KENNEL COUGH)',
       '200',
       '450',
       0,
       ''
UNION ALL
SELECT 'BRONCURE 60mL',
       '185',
       '300',
       0,
       ''
UNION ALL
SELECT 'CANINE PREGNANCY TEST 10s',
       '290',
       '850',
       0,
       ''
UNION ALL
SELECT 'CAT CATHETER',
       '300',
       '1000',
       0,
       ''
UNION ALL
SELECT 'CCV+CPV+GIA 3 WAY TEST KIT 10s',
       '300',
       '1500',
       0,
       ''
UNION ALL
SELECT 'CDV TEST KIT 10s',
       '200',
       '1000',
       0,
       ''
UNION ALL
SELECT 'CERENIA SOLUTION 10mg 20mL',
       '7327',
       '0',
       0,
       ''
UNION ALL
SELECT 'CESAR LAMB 100g',
       '61',
       '85',
       0,
       ''
UNION ALL
SELECT 'CETIRIZINE SYRUP',
       '41',
       '220',
       0,
       ''
UNION ALL
SELECT 'CETIRIZINE TABLETS',
       '250',
       '20',
       0,
       ''
UNION ALL
SELECT 'Charcoal Vet',
       '10',
       '650',
       0,
       ''
UNION ALL
SELECT 'CHLORASONE',
       '450',
       '600',
       0,
       ''
UNION ALL
SELECT 'CHROMIC CATGUT 2/0 CUTTING',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'CHROMIC CATGUT 3/0 CUTTING',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'CHROMIC CATGUT 3/0 ROUND',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'CHROMIC CATUGUT 2/0 ROUND',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'CO-AMOXCLAV 125mg',
       '95',
       '270',
       0,
       ''
UNION ALL
SELECT 'CO-AMOXICLAV 250mg',
       '150',
       '270',
       0,
       ''
UNION ALL
SELECT 'CO-AMOXICLAV TABLETS',
       '150',
       '35',
       0,
       ''
UNION ALL
SELECT 'COATSHINE 120 mL',
       '244',
       '380',
       0,
       ''
UNION ALL
SELECT 'COFORTA INJECTABLE',
       '764',
       '0',
       0,
       ''
UNION ALL
SELECT 'COTRIMOXAZOLE SYRUP',
       '18',
       '250',
       0,
       ''
UNION ALL
SELECT 'COTTON',
       '180',
       '0',
       0,
       ''
UNION ALL
SELECT 'COTTON APPLICATOR (BUDS)',
       '120',
       '0',
       0,
       ''
UNION ALL
SELECT 'COVER SLIPS',
       '35',
       '0',
       0,
       ''
UNION ALL
SELECT 'CPV TEST KIT',
       '200',
       '1000',
       0,
       ''
UNION ALL
SELECT 'CPV+CDV TEST KIT',
       '300',
       '1000',
       0,
       ''
UNION ALL
SELECT 'D5LRS FLUID (PINK)',
       '75',
       '0',
       0,
       ''
UNION ALL
SELECT 'D5WATER (RED)',
       '75',
       '0',
       0,
       ''
UNION ALL
SELECT 'Dematting Comb',
       '150',
       '220',
       0,
       ''
UNION ALL
SELECT 'DERMALCLENS CREAM',
       '385',
       '500',
       0,
       ''
UNION ALL
SELECT 'DERMALCLENS SPRAY',
       '485',
       '600',
       0,
       ''
UNION ALL
SELECT 'DEXAMETHASONE INJECTABLE 100 mL',
       '850',
       '0',
       0,
       ''
UNION ALL
SELECT 'DEXTROSE POWDER 100g',
       '40',
       '90',
       0,
       ''
UNION ALL
SELECT 'Diaper Female Extra Small',
       '10.5',
       '25',
       0,
       ''
UNION ALL
SELECT 'Diaper Female Small',
       '12',
       '30',
       0,
       ''
UNION ALL
SELECT 'Diaper Male Wrap Large',
       '24',
       '40',
       0,
       ''
UNION ALL
SELECT 'Diaper Male Wrap Small',
       '16',
       '30',
       0,
       ''
UNION ALL
SELECT 'Diaper Male Wrap XS',
       '14',
       '25',
       0,
       ''
UNION ALL
SELECT 'Dog Snack Jar',
       '0',
       '220',
       0,
       ''
UNION ALL
SELECT 'DONTRAL SUSPENSION 30 mL',
       '1005',
       '0',
       0,
       ''
UNION ALL
SELECT 'DOXYCYCLINE INJECTABLE',
       '640',
       '0',
       0,
       ''
UNION ALL
SELECT 'DRONTAL PUPPY SUSPENSION 50 mL',
       '358',
       '0',
       0,
       ''
UNION ALL
SELECT 'DUPHALYTE 500 mL',
       '596',
       '0',
       0,
       ''
UNION ALL
SELECT 'DUPLOCILLIN 100 mL',
       '568',
       '0',
       0,
       ''
UNION ALL
SELECT 'E. COLLAR # 3',
       '90',
       '250',
       0,
       ''
UNION ALL
SELECT 'E. COLLAR # 4',
       '75',
       '200',
       0,
       ''
UNION ALL
SELECT 'E. COLLAR # 5',
       '65',
       '180',
       0,
       ''
UNION ALL
SELECT 'E. COLLAR # 6',
       '50',
       '150',
       0,
       ''
UNION ALL
SELECT 'E. COLLAR # 7',
       '45',
       '100',
       0,
       ''
UNION ALL
SELECT 'E.COLLAR # 1',
       '135',
       '350',
       0,
       ''
UNION ALL
SELECT 'E.COLLAR # 2',
       '110',
       '300',
       0,
       ''
UNION ALL
SELECT 'EDA TUBE PURPLE 3mL',
       '350',
       '0',
       0,
       ''
UNION ALL
SELECT 'EDTA MICROTAINER',
       '550',
       '0',
       0,
       ''
UNION ALL
SELECT 'EHR+ANA+BAB 3 WAY TEST KIT',
       '320',
       '1500',
       0,
       ''
UNION ALL
SELECT 'EMERFLOX 60 mL',
       '170',
       '400',
       0,
       ''
UNION ALL
SELECT 'EMERPLEX 120 mL',
       '182',
       '400',
       0,
       ''
UNION ALL
SELECT 'EMERVIT 120 mL',
       '125',
       '270',
       0,
       ''
UNION ALL
SELECT 'ENER-G 60mL',
       '150',
       '270',
       0,
       ''
UNION ALL
SELECT 'ENFABOOST 120 mL',
       '215',
       '350',
       0,
       ''
UNION ALL
SELECT 'Enmalac',
       '220',
       '350',
       0,
       ''
UNION ALL
SELECT 'ENROFLOXACIN INJECTABLE',
       '600',
       '0',
       0,
       ''
UNION ALL
SELECT 'EPINEPHRINE AMPULE',
       '234',
       '0',
       0,
       ''
UNION ALL
SELECT 'FACE MASK',
       '100',
       '0',
       0,
       ''
UNION ALL
SELECT 'FERCOBSANG 100 mL INJECTABLE',
       '668',
       '0',
       0,
       ''
UNION ALL
SELECT 'FIP Antibody Test',
       '250',
       '850',
       0,
       ''
UNION ALL
SELECT 'FIV-FeLV Test',
       '300',
       '1000',
       0,
       ''
UNION ALL
SELECT 'FPV Test',
       '250',
       '650',
       0,
       ''
UNION ALL
SELECT 'FRONTLINE PLUS 10kg - 20kg (Dogs)',
       '997',
       '550',
       0,
       ''
UNION ALL
SELECT 'FRONTLINE PLUS 20kg - 40kg (Dogs)',
       '1050',
       '630',
       0,
       ''
UNION ALL
SELECT 'FRONTLINE PLUS for Cats',
       '934',
       '450',
       0,
       ''
UNION ALL
SELECT 'FRONTLINE PLUS up to 10kg (Dogs)',
       '943',
       '450',
       0,
       ''
UNION ALL
SELECT 'FRUDIX (FUROSEMIDE)',
       '800',
       '35',
       0,
       ''
UNION ALL
SELECT 'FURFECT SHAMPOO',
       '270',
       '450',
       0,
       ''
UNION ALL
SELECT 'FURFECT WOUND CREAM 15g',
       '140',
       '250',
       0,
       ''
UNION ALL
SELECT 'FUROSEMIDE AMPULE',
       '60',
       '0',
       0,
       ''
UNION ALL
SELECT 'GAUZE PADS',
       '150',
       '0',
       0,
       ''
UNION ALL
SELECT 'GAUZE ROLLS',
       '650',
       '0',
       0,
       ''
UNION ALL
SELECT 'GENTAMCIN AMPULE',
       '40',
       '0',
       0,
       ''
UNION ALL
SELECT 'GLASS SLIDES',
       '60',
       '0',
       0,
       ''
UNION ALL
SELECT 'Glucose Test',
       '25',
       '50',
       0,
       ''
UNION ALL
SELECT 'GREEN CAP TUBE 3mL',
       '450',
       '0',
       0,
       ''
UNION ALL
SELECT 'HEARTGARD PLUS 12kg to 22kg',
       '883',
       '350',
       0,
       ''
UNION ALL
SELECT 'HEARTGARD PLUS 23kg to 43kg',
       '1001',
       '450',
       0,
       ''
UNION ALL
SELECT 'HEARTGARD PLUS up to 11kg',
       '765',
       '250',
       0,
       ''
UNION ALL
SELECT 'HEARTWORM TEST KIT 10s',
       '220',
       '850',
       0,
       ''
UNION ALL
SELECT 'HEMACLEAN SOLUTION',
       '2850',
       '0',
       0,
       ''
UNION ALL
SELECT 'HEPATOSURE 60 mL',
       '197',
       '320',
       0,
       ''
UNION ALL
SELECT 'HIPRADOG 5in1 VACCINE',
       '200',
       '400',
       0,
       ''
UNION ALL
SELECT 'HM5 REAGENT PACK',
       '24200',
       '0',
       0,
       ''
UNION ALL
SELECT 'HYDROGEN PEROXIDE GALLON',
       '750',
       '0',
       0,
       ''
UNION ALL
SELECT 'INFLACAM 10 mL',
       '295',
       '400',
       0,
       ''
UNION ALL
SELECT 'IV CATHETER gauge 22 (Blue)',
       '1500',
       '0',
       0,
       ''
UNION ALL
SELECT 'IV CATHETER gauge 24 (Yellow)',
       '1750',
       '0',
       0,
       ''
UNION ALL
SELECT 'IV CATHETER gauge 26 (Violet)',
       '1750',
       '0',
       0,
       ''
UNION ALL
SELECT 'IV Fluids',
       '75',
       '200',
       0,
       ''
UNION ALL
SELECT 'IV LINE ADULT',
       '15',
       '0',
       0,
       ''
UNION ALL
SELECT 'IV LINE PEDIA',
       '15',
       '50',
       0,
       ''
UNION ALL
SELECT 'IVOMEC 100 mL Injectable',
       '1650',
       '0',
       0,
       ''
UNION ALL
SELECT 'JERKY STICKS',
       '75',
       '150',
       0,
       ''
UNION ALL
SELECT 'KETADINE MEDICATED SHAMPOO 250mL',
       '480',
       '750',
       0,
       ''
UNION ALL
SELECT 'KETOCONAZOLE TABLET (BOTTLE)',
       '1890',
       '50',
       0,
       ''
UNION ALL
SELECT 'Knot Bone',
       '10',
       '25',
       0,
       ''
UNION ALL
SELECT 'LAKTRAZINE TABS',
       '165',
       '15',
       0,
       ''
UNION ALL
SELECT 'LATEX GLOVES LARGE',
       '380',
       '0',
       0,
       ''
UNION ALL
SELECT 'LATEX GLOVES MEDIUM',
       '380',
       '0',
       0,
       ''
UNION ALL
SELECT 'LATEX GLOVES SMALL',
       '400',
       '0',
       0,
       ''
UNION ALL
SELECT 'LC-VIT 120 mL',
       '114',
       '270',
       0,
       ''
UNION ALL
SELECT 'LEPTOSPIROSIS Ag TEST KIT',
       '280',
       '850',
       0,
       ''
UNION ALL
SELECT 'LIDOCAINE',
       '46',
       '0',
       0,
       ''
UNION ALL
SELECT 'LIV 52 HIMALAYA 60 tabs',
       '400',
       '15',
       0,
       ''
UNION ALL
SELECT 'LRS FLUID (BLUE)',
       '75',
       '0',
       0,
       ''
UNION ALL
SELECT 'LYMEDOX 60 mL',
       '188',
       '350',
       0,
       ''
UNION ALL
SELECT 'MACROCAL TABS',
       '206',
       '15',
       0,
       ''
UNION ALL
SELECT 'MARBOCYL 100 mL',
       '2577',
       '0',
       0,
       ''
UNION ALL
SELECT 'METOCLOPROMIDE 5mg Ampule',
       '80',
       '0',
       0,
       ''
UNION ALL
SELECT 'METOCLOPROMIDE AMPULES',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT 'METOCLOPROMIDE SYRUP',
       '19',
       '170',
       0,
       ''
UNION ALL
SELECT 'METOCLOPROMIDE TABLETS',
       '5',
       '25',
       0,
       ''
UNION ALL
SELECT 'METRONIDAZOLE INJECTABLE',
       '19',
       '0',
       0,
       ''
UNION ALL
SELECT 'METRONIDAZOLE SYRUP',
       '19',
       '170',
       0,
       ''
UNION ALL
SELECT 'METRONIDAZOLE TABLETS',
       '250',
       '20',
       0,
       ''
UNION ALL
SELECT 'MICONATE SHAMPOO',
       '285',
       '450',
       0,
       ''
UNION ALL
SELECT 'MICROPORE 3M 1inch',
       '450',
       '0',
       0,
       ''
UNION ALL
SELECT 'MILKCARE (MILK REPLACER)',
       '185',
       '350',
       0,
       ''
UNION ALL
SELECT 'Muzzle',
       '0',
       '125',
       0,
       ''
UNION ALL
SELECT 'MYCOCIDE SHAMPOO',
       '370',
       '450',
       0,
       ''
UNION ALL
SELECT 'NACALVIT-C 120mL',
       '156',
       '250',
       0,
       ''
UNION ALL
SELECT 'NAIL CUTTER A12',
       '110',
       '250',
       0,
       ''
UNION ALL
SELECT 'NATURIS ADULT 1.2kg',
       '441',
       '550',
       0,
       ''
UNION ALL
SELECT 'NATURIS PUPPY 3kg',
       '1097',
       '150',
       0,
       ''
UNION ALL
SELECT 'NEFROTEC DS 60 tabs',
       '548',
       '20',
       0,
       ''
UNION ALL
SELECT 'NEMATOCIDE SYRUP 15 mL',
       '79',
       '0',
       0,
       ''
UNION ALL
SELECT 'NEMATOCIDE SYRUP 60 mL',
       '258',
       '0',
       0,
       ''
UNION ALL
SELECT 'NEXGARD EXTRA LARGE 25kg - 50kg',
       '1413',
       '680',
       0,
       ''
UNION ALL
SELECT 'NEXGARD LARGE 10-25kg',
       '1300',
       '630',
       0,
       ''
UNION ALL
SELECT 'NEXGARD MEDIUM 4-10kg',
       '1233',
       '580',
       0,
       ''
UNION ALL
SELECT 'NEXGARD SMALL 2-4kg',
       '1185',
       '550',
       0,
       ''
UNION ALL
SELECT 'NUERONERVE (B-COMPLEX)',
       '358',
       '15',
       0,
       ''
UNION ALL
SELECT 'NURSING KIT PLAIN FEEDING BOTTLE',
       '65',
       '170',
       0,
       ''
UNION ALL
SELECT 'NUTRICAL 120 mL',
       '137',
       '350',
       0,
       ''
UNION ALL
SELECT 'NV EAR CLEANSE LIQUID',
       '668',
       '750',
       0,
       ''
UNION ALL
SELECT 'NV EYE RINSE LIQUID',
       '714',
       '800',
       0,
       ''
UNION ALL
SELECT 'NV PET-EASE CHEWS',
       '823',
       '35',
       0,
       ''
UNION ALL
SELECT 'NV PROBIOTICS CAPSULES',
       '1394',
       '35',
       0,
       ''
UNION ALL
SELECT 'NYLON 2/0 CUTTING',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT 'NYLON 3/0 CUTTING',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT 'OMEPRAZOLE CAPSULE',
       '70',
       '40',
       0,
       ''
UNION ALL
SELECT 'OMEPRAZOLE VIAL',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT 'OTIDERM EAR DROPS',
       '346',
       '450',
       0,
       ''
UNION ALL
SELECT 'OXYTOCIN INJECTABLE',
       '480',
       '0',
       0,
       ''
UNION ALL
SELECT 'PADDED HARNESS SET 3.0',
       '290',
       '550',
       0,
       ''
UNION ALL
SELECT 'PAPI IRON 120 mL',
       '186',
       '370',
       0,
       ''
UNION ALL
SELECT 'PAPI LIVEWELL SUSP 60mL',
       '204',
       '320',
       0,
       ''
UNION ALL
SELECT 'PAPI MULTIVITAMINS 120 mL',
       '117',
       '270',
       0,
       ''
UNION ALL
SELECT 'PAPI OB',
       '165',
       '350',
       0,
       ''
UNION ALL
SELECT 'Papi Soap Anti tick & flea',
       '0',
       '120',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE ADULT 20kg',
       '1952',
       '145',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE CAN BEEF 400g',
       '80',
       '110',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE CAN BEEF 700g',
       '120',
       '180',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE CAN CHICKEN 400g',
       '80',
       '110',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE CAN CHICKEN 700g',
       '120',
       '180',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE CAN PUPPY 400g',
       '84',
       '120',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE DENTASTIX LARGE',
       '78',
       '110',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE DENTASTIX MEDIUM',
       '78',
       '110',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE DENTASTIX SMALL',
       '68',
       '100',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE MEAT JERKY STX 60g',
       '68',
       '110',
       0,
       ''
UNION ALL
SELECT 'PEDIGREE PUPPY 15kg',
       '1858',
       '155',
       0,
       ''
UNION ALL
SELECT 'PET COMB SET',
       '115',
       '200',
       0,
       ''
UNION ALL
SELECT 'PET COMB SET A12',
       '110',
       '220',
       0,
       ''
UNION ALL
SELECT 'PET CURE DOXYCYCLINE TABS',
       '7.14',
       '25',
       0,
       ''
UNION ALL
SELECT 'Petpyrin',
       '250',
       '350',
       0,
       ''
UNION ALL
SELECT 'PHYTOMENADIONE AMPULE',
       '143',
       '0',
       0,
       ''
UNION ALL
SELECT 'PIMOBENDAN 1.25mg',
       '2300',
       '0',
       0,
       ''
UNION ALL
SELECT 'PLASTIC HANDLE SINGLE COMB',
       '65',
       '180',
       0,
       ''
UNION ALL
SELECT 'Playpets Shampoo',
       '0',
       '400',
       0,
       ''
UNION ALL
SELECT 'PNEUMODOG VACCINE',
       '220',
       '450',
       0,
       ''
UNION ALL
SELECT 'POLYGLACTIN 2/0 CUTTING',
       '850',
       '0',
       0,
       ''
UNION ALL
SELECT 'POLYGLACTIN 2/0 ROUND',
       '850',
       '0',
       0,
       ''
UNION ALL
SELECT 'POLYGLACTIN 3/0 CUTTING',
       '850',
       '0',
       0,
       ''
UNION ALL
SELECT 'POLYGLACTIN 3/0 ROUND',
       '850',
       '0',
       0,
       ''
UNION ALL
SELECT 'POVIDONE IODINE GALLON',
       '760',
       '0',
       0,
       ''
UNION ALL
SELECT 'PREDNISONE SYRUP',
       '51',
       '270',
       0,
       ''
UNION ALL
SELECT 'PREDNISONE TABLETS',
       '550',
       '20',
       0,
       ''
UNION ALL
SELECT 'PREFOLIC CEE 120mL',
       '202',
       '450',
       0,
       ''
UNION ALL
SELECT 'PREVICOX 57mg',
       '1571',
       '50',
       0,
       ''
UNION ALL
SELECT 'Prevomax',
       '50',
       '100',
       0,
       ''
UNION ALL
SELECT 'PROHEART SR-12',
       '14878',
       '0',
       0,
       ''
UNION ALL
SELECT 'PROXANTEL TABLETS',
       '1940',
       '0',
       0,
       ''
UNION ALL
SELECT 'PULMOQUIN SYRUP 60 mL',
       '98',
       '170',
       0,
       ''
UNION ALL
SELECT 'PUREVAX FELINE 4 (4in1)',
       '470',
       '1000',
       0,
       ''
UNION ALL
SELECT 'RABISIN 10Ds',
       '260',
       '300',
       0,
       ''
UNION ALL
SELECT 'RECOMBITEK C6CV 6IN1',
       '192',
       '550',
       0,
       ''
UNION ALL
SELECT 'RENACURE',
       '185',
       '300',
       0,
       ''
UNION ALL
SELECT 'RIFLEXINE 60 mL',
       '140',
       '385',
       0,
       ''
UNION ALL
SELECT 'ROUND LEASH MEDIUM',
       '140',
       '270',
       0,
       ''
UNION ALL
SELECT 'RTE Citrus Twist Talc 100g',
       '110',
       '250',
       0,
       ''
UNION ALL
SELECT 'RTE Fruity Fresh Talc 100g',
       '110',
       '250',
       0,
       ''
UNION ALL
SELECT 'RTE MDC Fresh Mint Shampoo 500 mL',
       '265',
       '350',
       0,
       ''
UNION ALL
SELECT 'RTE MDC Odor Absorber 500 mL',
       '265',
       '350',
       0,
       ''
UNION ALL
SELECT 'RTE MDC Puppy with Oat 500 mL',
       '265',
       '350',
       0,
       ''
UNION ALL
SELECT 'RTE MDC Sweet Apple Shampoo 500 mL',
       '265',
       '350',
       0,
       ''
UNION ALL
SELECT 'RTE Perfumee De Paris Cologne 100 mL',
       '170',
       '250',
       0,
       ''
UNION ALL
SELECT 'RTE Rainforest Burst Cologne 100 mL',
       '170',
       '250',
       0,
       ''
UNION ALL
SELECT 'RTE Sweet Talk Cologne 100 mL',
       '170',
       '250',
       0,
       ''
UNION ALL
SELECT 'SILK 2/0 CUTTING',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'SILK 3/0 CUTTING',
       '240',
       '0',
       0,
       ''
UNION ALL
SELECT 'SINGLE COLLAR 1.5',
       '40',
       '100',
       0,
       ''
UNION ALL
SELECT 'SOCKS MEDIUM',
       '55',
       '150',
       0,
       ''
UNION ALL
SELECT 'STAINLESS BOWL MEDIUM',
       '70',
       '150',
       0,
       ''
UNION ALL
SELECT 'STAINLESS BOWL SMALL',
       '49',
       '100',
       0,
       ''
UNION ALL
SELECT 'STEEL COMB LARGE',
       '80',
       '160',
       0,
       ''
UNION ALL
SELECT 'STEEL COMB MEDIUM',
       '75',
       '150',
       0,
       ''
UNION ALL
SELECT 'STERILE GLOVE 7.5',
       '900',
       '0',
       0,
       ''
UNION ALL
SELECT 'STERILE GLOVES 6.5',
       '900',
       '0',
       0,
       ''
UNION ALL
SELECT 'STYPTIC POWDER',
       '230',
       '0',
       0,
       ''
UNION ALL
SELECT 'SURGICAL DRAPES',
       '2500',
       '0',
       0,
       ''
UNION ALL
SELECT 'TEST ITEM',
       '150',
       '0',
       0,
       ''
UNION ALL
SELECT 'THERMOMETER',
       '80',
       '0',
       0,
       ''
UNION ALL
SELECT 'TOLFENOL 30 mL',
       '193',
       '320',
       0,
       ''
UNION ALL
SELECT 'TOLFINE 100 mL',
       '2210',
       '0',
       0,
       ''
UNION ALL
SELECT 'TRANEX INJECTABLE',
       '250',
       '0',
       0,
       ''
UNION ALL
SELECT 'TRANEXAMIC ACID AMPULE',
       '5',
       '0',
       0,
       ''
UNION ALL
SELECT 'TRANEXAMIC ACID TABLETS',
       '514',
       '0',
       0,
       ''
UNION ALL
SELECT 'TRICIN',
       '430',
       '600',
       0,
       ''
UNION ALL
SELECT 'TRITOZINE 60 mL',
       '192',
       '300',
       0,
       ''
UNION ALL
SELECT 'TUBE RED CAP 3mL',
       '350',
       '0',
       0,
       ''
UNION ALL
SELECT 'UNDERPADS',
       '150',
       '0',
       0,
       ''
UNION ALL
SELECT 'VD GASTROINTESTINAL 400g',
       '165',
       '270',
       0,
       ''
UNION ALL
SELECT 'VD HEPATIC DOG CAN 420g',
       '165',
       '270',
       0,
       ''
UNION ALL
SELECT 'VD RECOVERY FELINE 195g',
       '160',
       '230',
       0,
       ''
UNION ALL
SELECT 'VD RENAL DOG CAN 410g',
       '165',
       '260',
       0,
       ''
UNION ALL
SELECT 'VD RENAL S/O FELINE 85g',
       '83',
       '150',
       0,
       ''
UNION ALL
SELECT 'VD URINARY S/O DOGS 400g',
       '165',
       '260',
       0,
       ''
UNION ALL
SELECT 'VETNODERM SOAP',
       '78',
       '160',
       0,
       ''
UNION ALL
SELECT 'WHISKAS DRY ADULT OCFISH 1..2kg',
       '252',
       '350',
       0,
       ''
UNION ALL
SELECT 'WHISKAS DRY ADULT TUNA 1.2kg',
       '252',
       '350',
       0,
       ''
UNION ALL
SELECT 'WHISKAS DRY JR OCFISH 1.1kg',
       '252',
       '350',
       0,
       ''
UNION ALL
SELECT 'WHISKAS POUCH JR TUNA 80g',
       '28',
       '50',
       0,
       ''
UNION ALL
SELECT 'WHISKAS POUCH TUNA 80g',
       '27',
       '50',
       0,
       ''
UNION ALL
SELECT 'YELLOW CAP TUBE 3mL',
       '550',
       '0',
       0,
       ''
UNION ALL
SELECT 'ZOLETIL 50 INJECTABLE',
       '1450',
       '0',
       0,
       ''
UNION ALL
SELECT '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT '',
       '',
       '',
       '',
       ''

GO 
