DEclare @GUID_Company VARCHAR(MAX) = '7C0A3310-E268-4712-8E7A-7DB0D065790C'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item   int,
     Name_Item varchar(500),
     UnitPrice DECIMAL(18, 2)
  )
DECLARE @SURGICALPROCEDURE varchar(500) = ''
DECLARE @Small varchar(500) = ''
DECLARE @Medium varchar(500) = ''
DECLARE @Large varchar(500) = ''
DECLARE @ExtraLardge varchar(500) = ''
DECLARE emp_cursor CURSOR FOR
  SELECT dbo.fGetCleanedString([SURGICAL PROCEDURE]),
         [Small (1-8 kg)],
         [Meduim (8.1 kg to 14 kg)],
         [LARGE (14.1 kg to 24 kg)],
         [EXTRA LARGE (24.1 kg and Up)]
  FROM   [Pet-Hub-Paranaque-Surgery-Price-list]

OPEN emp_cursor

FETCH NEXT FROM emp_cursor INTO @SURGICALPROCEDURE,
                                @Small,
                                @Medium,
                                @Large,
                                @ExtraLardge

WHILE @@FETCH_STATUS = 0
  BEGIN
      if( @SURGICALPROCEDURE = 'Tail Docking + Dew Claw Removal' )
        BEGIN
            INSERT @forImport
                   (Name_Item,
                    UnitPrice)
            SELECT 'Tail Docking + Dew Claw Removal per puppy',
                   TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString(@Small))
        END
      ELSE
        BEGIN
            INSERT @forImport
                   (Name_Item,
                    UnitPrice)
            SELECT @SURGICALPROCEDURE + ' Small (1-8 kg)',
                   TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString(@Small))
            UNION ALL
            SELECT @SURGICALPROCEDURE
                   + ' Medium (8.1 kg to 14 kg)',
                   TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString(@Medium))
            UNION ALL
            SELECT @SURGICALPROCEDURE
                   + ' Large (14.1 kg to 24 kg)',
                   TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString(@Large))
            UNION ALL
            SELECT @SURGICALPROCEDURE
                   + ' Extra Large (24.1 kg and Up)',
                   TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString(@ExtraLardge))
        END

      FETCH NEXT FROM emp_cursor INTO @SURGICALPROCEDURE,
                                      @Small,
                                      @Medium,
                                      @Large,
                                      @ExtraLardge
  END

CLOSE emp_cursor;

DEALLOCATE emp_cursor;

SELECT ID,
       Name,
       REPLACE(Name, '(14.1 k to 24 kg)', '(14.1 kg to 24 kg)')
FROM   tItem
WHERE  ID_Company = @ID_Company
       AND Name LIKE '%(14.1 k to 24 kg)%'

SELECT ID,
       Name,
       REPLACE(Name, '(24.1 and Up)', '(24.1 kg and Up)')
FROM   tItem
WHERE  ID_Company = @ID_Company
       AND Name LIKE '%(24.1 and Up)%'

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

Update tItem
SET    Name = REPLACE(Name, '(14.1 k to 24 kg)', '(14.1 kg to 24 kg)')
WHERE  ID_Company = @ID_Company

Update tItem
SET    Name = REPLACE(Name, '(24.1 and Up)', '(24.1 kg and Up)')
WHERE  ID_Company = @ID_Company

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company

SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL

Update tItem
SET    UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @ForImport import
               on import.ID_Item = item.ID
WHERE  ID_Company = @ID_Company



