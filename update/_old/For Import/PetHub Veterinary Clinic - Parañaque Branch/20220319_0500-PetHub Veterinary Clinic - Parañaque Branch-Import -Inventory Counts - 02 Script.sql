DEclare @GUID_Company VARCHAR(MAX) = '7C0A3310-E268-4712-8E7A-7DB0D065790C'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

DECLARE @forImport TABLE
  (
     ID_Item   int,
     Name_Item varchar(500),
     UnitCost  DECIMAL(18, 2),
     UnitPrice DECIMAL(18, 2),
     Quantity  Int
  )

INSERT @forImport
       (Name_Item,
        UnitCost,
        UnitPrice,
        Quantity)
SELECT dbo.fGetCleanedString([Item]),
       TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString([Buying Price])),
       TRY_CONVERT(decimal(18, 2), dbo.fGetCleanedString([Selling Price])),
       [Quantity]
FROM   [InventorySummaryReport-eb7f0886-a7c8-4d86-8182-54b5f49de371]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company

DELETE FROM @ForImport
WHERE  LEN(Name_Item) = 0

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       item.CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       0
FROM   @forImport import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  import.ID_Item IS NOT NULl
       and item.CurrentInventoryCount > 0
ORDER  BY ID_Item

 

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

DELETE FROM @adjustInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.Quantity,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  import.Quantity > 0
       AND import.ID_Item IS NOT NULl
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession
 

SELECT *
FROM   @forImport
WHERE  ID_Item IS NULL Order BY Name_Item	
SELECT *
FROM   @forImport
WHERE  ID_Item IS NOT NULL Order BY Name_Item	
