if OBJECT_ID('dbo.[Pet-Hub-Paranaque-Surgery-Price-list]') is not null
  BEGIN
      DROP TABLE [Pet-Hub-Paranaque-Surgery-Price-list]
  END

GO

CREATE TABLE [dbo].[Pet-Hub-Paranaque-Surgery-Price-list]
  (
     [SURGICAL PROCEDURE]           varchar(500),
     [Small (1-8 kg)]               varchar(500),
     [Meduim (8.1 kg to 14 kg)]     varchar(500),
     [LARGE (14.1 kg to 24 kg)]     varchar(500),
     [EXTRA LARGE (24.1 kg and Up)] varchar(500)
  )

GO

INSERT INTO [dbo].[Pet-Hub-Paranaque-Surgery-Price-list]
            ([SURGICAL PROCEDURE],
             [Small (1-8 kg)],
             [Meduim (8.1 kg to 14 kg)],
             [LARGE (14.1 kg to 24 kg)],
             [EXTRA LARGE (24.1 kg and Up)])
SELECT 'Aural Hematoma (Unilateral)',
       '4000',
       '6500',
       '8500',
       '12000'
UNION ALL
SELECT 'Aural Hematoma (Bilateral)',
       '5000',
       '8000',
       '12000',
       '16000'
UNION ALL
SELECT 'Caesarian Section (Dog)',
       '9000',
       '12000',
       '17000',
       '23000'
UNION ALL
SELECT 'Caesarian Section (Cat)',
       '9000',
       '11000',
       '',
       ''
UNION ALL
SELECT 'Caesarian Section + Ovariohysterectomy',
       '13000',
       '16000',
       '21000',
       '27000'
UNION ALL
SELECT 'Castration (Dog)',
       '2500',
       '5000',
       '7000',
       '10000'
UNION ALL
SELECT 'Castration (Cat)',
       '2500',
       '4500',
       '',
       ''
UNION ALL
SELECT 'Cherry Eye Removal',
       '3500',
       '5000',
       '7000',
       ''
UNION ALL
SELECT 'Cystotomy',
       '8000',
       '12000',
       '15000',
       '18000'
UNION ALL
SELECT 'Enterectomy',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'Eye Enucleation',
       '5000',
       '7000',
       '10000',
       '14000'
UNION ALL
SELECT 'Gastrotomy',
       '10000',
       '15000',
       '20000',
       '25000'
UNION ALL
SELECT 'Hernia (Umbilical)',
       '6500',
       '9000',
       '12000',
       '16000'
UNION ALL
SELECT 'Hernia (Inguinal-Unilateral)',
       '8500',
       '12000',
       '15000',
       '18000'
UNION ALL
SELECT 'Hernia (Inguinal- Bilateral)',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'Prolapse (Rectal)',
       '5000',
       '8000',
       '15000',
       '18000'
UNION ALL
SELECT 'Prolapse (Vaginal)',
       '6000',
       '8000',
       '15000',
       '18000'
UNION ALL
SELECT 'Pyometra',
       '8000',
       '10000',
       '15000',
       '20000'
UNION ALL
SELECT 'Spay (Dog)',
       '3500',
       '6000',
       '8000',
       '11000'
UNION ALL
SELECT 'Spay (Cat)',
       '2500',
       '5500',
       '',
       ''
UNION ALL
SELECT 'Tail Docking + Dew Claw Removal',
       '250/puppy',
       '',
       '',
       ''
UNION ALL
SELECT 'Tarsorrhaphy',
       '3000',
       '5000',
       '7000',
       '10000'
UNION ALL
SELECT 'Urethrostomy',
       '12000',
       '16000',
       '20000',
       '25000'
UNION ALL
SELECT 'Wound Repair',
       '3000',
       '5000',
       '8000',
       '11000'
UNION ALL
SELECT 'Whelping Assistance + 300 per live puppy/kitten',
       '4500',
       '5500',
       '6500',
       ''

GO 
