if OBJECT_ID('dbo.[PANACEA_ITEMS]') is not null
  BEGIN
      DROP TABLE [PANACEA_ITEMS]
  END

GO

CREATE TABLE [dbo].[PANACEA_ITEMS]
  (
     [Name]     varchar(500),
     [Cost]     varchar(500),
     [Price]    varchar(500),
     [In stock] varchar(500)
  )

GO

INSERT INTO [dbo].[PANACEA_ITEMS]
            ([Name],
             [Cost],
             [Price],
             [In stock])
SELECT 'Activo Milk Replacement 150g',
       '131',
       '250',
       '11'
UNION ALL
SELECT 'Advocate Spot-on for Cats',
       '296.87',
       '560',
       '0'
UNION ALL
SELECT 'Bearing Dry Shampoo 150g',
       '145',
       '270',
       '1'
UNION ALL
SELECT 'Bearing Dry Shampoo 300g',
       '195',
       '350',
       '0'
UNION ALL
SELECT 'Bearing Shampoo All Dogs 300ml',
       '165',
       '285',
       '-1'
UNION ALL
SELECT 'Bearing Shampoo Long Hair 300ml',
       '165',
       '285',
       '7'
UNION ALL
SELECT 'Bearing Shampoo Small Breeds 300ml',
       '165',
       '285',
       '5'
UNION ALL
SELECT 'Bearing Shampoo Smelly Hair 300ml',
       '165',
       '285',
       '0'
UNION ALL
SELECT 'Bearing Shampoo White Hair 300ml',
       '165',
       '285',
       '3'
UNION ALL
SELECT 'Bearing Soap All Breeds',
       '56',
       '110',
       '3'
UNION ALL
SELECT 'Bearing Soap Puppies',
       '56',
       '110',
       '2'
UNION ALL
SELECT 'Bearing Soap Small Breeds',
       '56',
       '110',
       '0'
UNION ALL
SELECT 'Bearing Soap Smelly Hair',
       '56',
       '110',
       '0'
UNION ALL
SELECT 'Bearing Tick and Flea Powder 150g',
       '145',
       '260',
       '2'
UNION ALL
SELECT 'Best Clean Cat Litter 10L',
       '220',
       '330',
       '9'
UNION ALL
SELECT 'Bioline Ear Care 50ml',
       '115',
       '270',
       '9'
UNION ALL
SELECT 'Bioline Earmite 30ml',
       '150',
       '280',
       '6'
UNION ALL
SELECT 'Bioline Eye Care',
       '115',
       '240',
       '2'
UNION ALL
SELECT 'Bioline Styptic Powder',
       '135',
       '300',
       '2'
UNION ALL
SELECT 'Bioline Tearstain Remover 50ml',
       '115',
       '280',
       '9'
UNION ALL
SELECT 'Cat Collar Flea',
       '50',
       '150',
       '3'
UNION ALL
SELECT 'Cat Toy',
       '60',
       '130',
       '-1'
UNION ALL
SELECT 'Cat Toy Feather Big',
       '16',
       '80',
       '10'
UNION ALL
SELECT 'Cat Toy Feather Sml',
       '16',
       '60',
       '11'
UNION ALL
SELECT 'Coatshine 120ml',
       '240',
       '350',
       '1'
UNION ALL
SELECT 'Collar w/ Bell',
       '33',
       '95',
       '12'
UNION ALL
SELECT 'Collar w/ Leash',
       '68',
       '180',
       '1'
UNION ALL
SELECT 'Combinex Wound Spray 86g',
       '205',
       '360',
       '0'
UNION ALL
SELECT 'Cosi Pet''s Milk',
       '130',
       '180',
       '0'
UNION ALL
SELECT 'Crancy Dental Bites 90g',
       '50',
       '100',
       '4'
UNION ALL
SELECT 'Crancy Dental Snack Mini 110g',
       '64.5',
       '130',
       '11'
UNION ALL
SELECT 'Crancy Dog Fresh Beef Sticks 30g',
       '39',
       '90',
       '28'
UNION ALL
SELECT 'Crancy I Love Cat 60g',
       '47',
       '85',
       '6'
UNION ALL
SELECT 'Crancy I Love Dog 60g',
       '47',
       '95',
       '7'
UNION ALL
SELECT 'Crancy Puffy Dog 60g',
       '47.13',
       '95',
       '4'
UNION ALL
SELECT 'Dog Collar Tick & Flea',
       '50',
       '150',
       '4'
UNION ALL
SELECT 'Dog Toothpaste',
       '120',
       '220',
       '5'
UNION ALL
SELECT 'Dog Treats (3pcs)',
       '20',
       '40',
       '3'
UNION ALL
SELECT 'Doggie Biscuit Big',
       '90',
       '180',
       '4'
UNION ALL
SELECT 'Doggie Biscuits Round 80g',
       '50',
       '95',
       '4'
UNION ALL
SELECT 'DONO F Diaper (L)',
       '15',
       '40',
       '15'
UNION ALL
SELECT 'DONO F Diaper (M)',
       '12.5',
       '35',
       '54'
UNION ALL
SELECT 'DONO F Diaper (S)',
       '10',
       '30',
       '-4'
UNION ALL
SELECT 'DONO F Diaper (XS)',
       '8.61',
       '25',
       '47'
UNION ALL
SELECT 'DONO Male Wrap (S)',
       '11.6',
       '40',
       '56'
UNION ALL
SELECT 'DONO Male Wrap (XS)',
       '10',
       '30',
       '46'
UNION ALL
SELECT 'Dono Male Wrap M',
       '14',
       '45',
       '43'
UNION ALL
SELECT 'Dr. Clauder''s Hair and Skin',
       '200',
       '260',
       '36'
UNION ALL
SELECT 'Duck Face Muzzle L',
       '50',
       '240',
       '7'
UNION ALL
SELECT 'Duck Face Muzzle S',
       '50',
       '200',
       '0'
UNION ALL
SELECT 'E-Collar Size 1',
       '0',
       '550',
       ''
UNION ALL
SELECT 'E-Collar Size 2',
       '0',
       '450',
       ''
UNION ALL
SELECT 'E-Collar Size 3',
       '0',
       '400',
       ''
UNION ALL
SELECT 'E-Collar Size 4',
       '0',
       '350',
       ''
UNION ALL
SELECT 'E-Collar Size 5',
       '0',
       '300',
       ''
UNION ALL
SELECT 'E-Collar Size 6',
       '0',
       '250',
       ''
UNION ALL
SELECT 'E-Collar Size 7',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Fur Magic 300ml Blue',
       '175',
       '280',
       '0'
UNION ALL
SELECT 'Fur Magic 300ml Pink',
       '175',
       '280',
       '0'
UNION ALL
SELECT 'Fur Magic 300ml Violet',
       '175',
       '280',
       '1'
UNION ALL
SELECT 'Fur Magic Shampoo 1000ml Pink',
       '395',
       '650',
       '0'
UNION ALL
SELECT 'Fur Magic Shampoo 1000ml Violet',
       '395',
       '650',
       '0'
UNION ALL
SELECT 'Fur Magic Shampoo Blue 1000ml',
       '395',
       '650',
       '0'
UNION ALL
SELECT 'Fur Magic Soap',
       '95',
       '160',
       '1'
UNION ALL
SELECT 'HeartGard Blue (Small)',
       '118.66',
       '200',
       '-3'
UNION ALL
SELECT 'HeartGard Green (Medium)',
       '136.16',
       '230',
       '3'
UNION ALL
SELECT 'HeartGard Orange (Large)',
       '153.66',
       '260',
       '4'
UNION ALL
SELECT 'Howbone Assorted 1 pack',
       '140',
       '270',
       '6'
UNION ALL
SELECT 'Hushpet Diaper (L)',
       '18',
       '35',
       '2'
UNION ALL
SELECT 'Hushpet Diaper (M)',
       '14.16',
       '30',
       '0'
UNION ALL
SELECT 'Hushpet Diaper (S)',
       '13.33',
       '25',
       '0'
UNION ALL
SELECT 'Hushpet Diaper (XL)',
       '18',
       '35',
       '8'
UNION ALL
SELECT 'Hushpet Male Wrap (L)',
       '18',
       '40',
       '5'
UNION ALL
SELECT 'Hushpet Male Wrap (M)',
       '18',
       '40',
       '-1'
UNION ALL
SELECT 'Hushpet Male Wrap (S)',
       '12',
       '30',
       '10'
UNION ALL
SELECT 'Hushpet Male Wrap (XS)',
       '12',
       '25',
       '-1'
UNION ALL
SELECT 'Injectable Medications Ceftriaxone',
       '12.95',
       '200',
       '40'
UNION ALL
SELECT 'Injectable Medications: Acetylcysteine (Paracetamol Toxicity)',
       '0',
       '1800',
       ''
UNION ALL
SELECT 'Injectable Medications: Aminophylline 25 mg/ml',
       '0',
       '450',
       ''
UNION ALL
SELECT 'Injectable Medications: Amoxicillin',
       '6.3',
       '150',
       '70'
UNION ALL
SELECT 'Injectable Medications: Ampicillin 50mg/ml (Vial)',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Injectable Medications: Atropine Sulfate',
       '75',
       '450',
       '100'
UNION ALL
SELECT 'Injectable Medications: Bromhexine 0.4%/ml',
       '10',
       '180',
       '100'
UNION ALL
SELECT 'Injectable Medications: Calcium Borogluconate',
       '10',
       '250',
       '179'
UNION ALL
SELECT 'Injectable Medications: Canglob D 6ml Vial',
       '0',
       '1600',
       ''
UNION ALL
SELECT 'Injectable Medications: Canglob P 6ml Vial',
       '750',
       '1600',
       '1'
UNION ALL
SELECT 'Injectable Medications: Cefazolin',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Injectable Medications: Cerenia (10 to 15kgs)',
       '0',
       '1600',
       ''
UNION ALL
SELECT 'Injectable Medications: Cerenia (5 to 10kgs)',
       '370',
       '1000',
       ''
UNION ALL
SELECT 'Injectable Medications: Cerenia (up to 5 kgs)',
       '370',
       '600',
       '3.43'
UNION ALL
SELECT 'Injectable Medications: Coforta 1-5kgs',
       '8.97',
       '150',
       '112'
UNION ALL
SELECT 'Injectable Medications: Coforta 11-15kgs',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Injectable Medications: Coforta 15-20kgs',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Injectable Medications: Coforta 6-10kgs',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Injectable Medications: Convenia 1-5 kgs',
       '1350',
       '1500',
       '8.5'
UNION ALL
SELECT 'Injectable Medications: Convenia 6-10 kgs',
       '0',
       '2400',
       ''
UNION ALL
SELECT 'Injectable Medications: Dexamethasone 0.1%',
       '10',
       '200',
       '96'
UNION ALL
SELECT 'Injectable Medications: Diphenhydramine Inj (Hypersensitivity)',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Injectable Medications: Doxycycline 1-5kgs',
       '10',
       '150',
       '88'
UNION ALL
SELECT 'Injectable Medications: Enrofloxacin 1-5 kgs',
       '10',
       '200',
       '89'
UNION ALL
SELECT 'Injectable Medications: Epinephrine Ampule',
       '18',
       '450',
       '20'
UNION ALL
SELECT 'Injectable Medications: Fercobsang-Iron',
       '7.2',
       '250',
       '90'
UNION ALL
SELECT 'Injectable Medications: Fluconazole',
       '0.8',
       '450',
       '189'
UNION ALL
SELECT 'Injectable Medications: Flunixin Meglumine ampule',
       '75',
       '350',
       '10'
UNION ALL
SELECT 'Injectable Medications: Furosemide Ampule',
       '5.5',
       '250',
       '27'
UNION ALL
SELECT 'Injectable Medications: Hyoscine N-Butyl Bromide Ampule',
       '14.5',
       '150',
       '45'
UNION ALL
SELECT 'Injectable Medications: Ivermectin/Demodex Treatment',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Injectable Medications: Local Anaesthetic/Lidocaine',
       '1',
       '350',
       '398'
UNION ALL
SELECT 'Injectable Medications: Marbofloxacin 1-5 kgs',
       '40',
       '150',
       ''
UNION ALL
SELECT 'Injectable Medications: Marbofloxacin 11-15kgs',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Injectable Medications: Marbofloxacin 6-10kgs',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Injectable Medications: Meloxicam 1-5kgs',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Injectable Medications: Meloxicam 11-15kgs',
       '0',
       '400',
       ''
UNION ALL
SELECT 'Injectable Medications: Meloxicam 6-10kgs',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Injectable medications: Meropenem Vial',
       '217',
       '700',
       '1'
UNION ALL
SELECT 'Injectable Medications: Metoclopramide Ampule',
       '15',
       '180',
       ''
UNION ALL
SELECT 'Injectable Medications: Omeprazole Ampule',
       '30',
       '350',
       '8'
UNION ALL
SELECT 'Injectable Medications: Ornipural',
       '7.99',
       '300',
       '79'
UNION ALL
SELECT 'Injectable Medications: Oxytocin',
       '10',
       '300',
       '81'
UNION ALL
SELECT 'Injectable Medications: Potassium Chloride 20ml',
       '45',
       '400',
       '14'
UNION ALL
SELECT 'Injectable Medications: Procaine Penicillin G',
       '10',
       '200',
       '92'
UNION ALL
SELECT 'Injectable Medications: Ranitidine Ampule',
       '0',
       '180',
       ''
UNION ALL
SELECT 'Injectable Medications: Septotryl 1-5kgs',
       '7.2',
       '150',
       '117'
UNION ALL
SELECT 'Injectable Medications: Septotryl 11-15kgs',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Injectable Medications: Septotryl 6-10kgs',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Injectable Medications: Tolfenamic Acid 1-5 kgs',
       '26.87',
       '180',
       '-10.2'
UNION ALL
SELECT 'Injectable Medications: Tolfenamic Acid 11-15 kgs',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Injectable Medications: Tolfenamic Acid 16-20 kgs',
       '0',
       '360',
       ''
UNION ALL
SELECT 'Injectable Medications: Tolfenamic Acid 21-25 kgs',
       '0',
       '420',
       ''
UNION ALL
SELECT 'Injectable Medications: Tolfenamic Acid 6-10kgs',
       '0',
       '240',
       ''
UNION ALL
SELECT 'Injectable Medications: Tramadol Ampule',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Injectable Medications: Tranexamic Acid Ampule',
       '12.5',
       '350',
       '11'
UNION ALL
SELECT 'Injectable Medications: Vincristine Sulfate Vial',
       '1150',
       '2300',
       '2'
UNION ALL
SELECT 'Injectable Medications: Vit. K/Phytomenadione',
       '10.1',
       '250',
       '26'
UNION ALL
SELECT 'Iozin Wound Spray',
       '170',
       '340',
       '0'
UNION ALL
SELECT 'IV Fluid D5LRS',
       '50',
       '250',
       '9'
UNION ALL
SELECT 'IV Fluid D5NS',
       '50',
       '250',
       '0'
UNION ALL
SELECT 'IV Fluid LRS',
       '50',
       '250',
       '0'
UNION ALL
SELECT 'IV Fluid NSS',
       '50',
       '250',
       '7'
UNION ALL
SELECT 'IV Fluid Set',
       '0',
       '850',
       ''
UNION ALL
SELECT 'IV Line Infusion Set',
       '17',
       '80',
       '21'
UNION ALL
SELECT 'Jerhigh Dog Treat Any Flavor',
       '75',
       '140',
       '3'
UNION ALL
SELECT 'Jojoba Pet Herbal Soap',
       '63',
       '135',
       '26'
UNION ALL
SELECT 'Leash w/ Collar Large (Assorted Colors)',
       '120',
       '280',
       '8'
UNION ALL
SELECT 'Leash w/ Harness Large (Assorted Colors)',
       '140',
       '320',
       '7'
UNION ALL
SELECT 'Leash w/ Harness Medium (Assorted Colors)',
       '85',
       '220',
       '6'
UNION ALL
SELECT 'Leash w/ Harness Small (Assorted Colors)',
       '80',
       '180',
       '7'
UNION ALL
SELECT 'Leash w/ Harness XL',
       '180',
       '360',
       '-1'
UNION ALL
SELECT 'Leash w/Collar Small (Pink)',
       '45',
       '150',
       '8'
UNION ALL
SELECT 'Lori Amitraz',
       '65',
       '140',
       '5'
UNION ALL
SELECT 'Lori Green',
       '65',
       '135',
       '2'
UNION ALL
SELECT 'Lori MDC',
       '65',
       '140',
       '1'
UNION ALL
SELECT 'Lori Red',
       '60',
       '135',
       '0'
UNION ALL
SELECT 'Med Supplies Nelaton Catheter 8FR',
       '60',
       '400',
       '9'
UNION ALL
SELECT 'Med Supplies: Cat Catheter',
       '250',
       '800',
       ''
UNION ALL
SELECT 'Med Supplies: D50% 50ml',
       '20',
       '150',
       ''
UNION ALL
SELECT 'Med Supplies: Digital Thermometer',
       '60',
       '200',
       ''
UNION ALL
SELECT 'Med Supplies: Heparin Lock',
       '9',
       '50',
       '61'
UNION ALL
SELECT 'Med Supplies: Instant Cold Pack',
       '25',
       '80',
       ''
UNION ALL
SELECT 'Med Supplies: Microchip',
       '0',
       '850',
       ''
UNION ALL
SELECT 'Med Supplies: NG Tube 5FR',
       '120',
       '400',
       '2'
UNION ALL
SELECT 'Med Supplies: NG Tube 8FR',
       '150',
       '400',
       '2'
UNION ALL
SELECT 'Med Supplies:¬†Silicone Foley Catheter 6FR',
       '150',
       '700',
       '2'
UNION ALL
SELECT 'Med Supplies: Silicone Foley Catheter 8FR',
       '150',
       '700',
       '0'
UNION ALL
SELECT 'Med Supplies: Surgical Blade',
       '2.8',
       '40',
       '196'
UNION ALL
SELECT 'Med Supplies: Surgical Gloves',
       '15',
       '50',
       '46'
UNION ALL
SELECT 'Med Supplies: Suture; Chromic Catgut',
       '20.83',
       '180',
       '0'
UNION ALL
SELECT 'Med Supplies: Suture; Chromic Catgut 4-0',
       '20.83',
       '180',
       '16'
UNION ALL
SELECT 'Med Supplies: Suture; Polyglactin',
       '150',
       '400',
       '20'
UNION ALL
SELECT 'Med Supplies: Umbilical Cord Clamp',
       '3',
       '35',
       '98'
UNION ALL
SELECT 'Migliorcane Prestige Pink 400g',
       '91.25',
       '220',
       '4'
UNION ALL
SELECT 'Migliorcane Prestige Violet 400g',
       '91.25',
       '220',
       '6'
UNION ALL
SELECT 'Migliorcane Unico Lamb 100g',
       '24.25',
       '65',
       '2'
UNION ALL
SELECT 'Migliorcane Unico Veal 100g',
       '24.25',
       '65',
       '3'
UNION ALL
SELECT 'Migliorgatto Unico Turkey 85g',
       '27',
       '65',
       '22'
UNION ALL
SELECT 'Migliorgatto Unico Veal 85g',
       '27',
       '65',
       ''
UNION ALL
SELECT 'Milky Biscuits Liver with Milk 200g',
       '95',
       '160',
       '0'
UNION ALL
SELECT 'Milky Biscuits Liver with Milk 70g',
       '45',
       '95',
       '0'
UNION ALL
SELECT 'Millk Care 150g',
       '0',
       '220',
       '13'
UNION ALL
SELECT 'Miocane Sensitive 3kgs',
       '465',
       '750',
       '1'
UNION ALL
SELECT 'Mondex 340g',
       '85',
       '200',
       ''
UNION ALL
SELECT 'Mondex Pack 100g',
       '52',
       '120',
       '24'
UNION ALL
SELECT 'Morando Can Cat',
       '45',
       '100',
       '8'
UNION ALL
SELECT 'Morando Professional Cat 2kgs (Beef and Chicken)',
       '199',
       '360',
       '6'
UNION ALL
SELECT 'MP Pate with Veal 400g',
       '0',
       '100',
       '32'
UNION ALL
SELECT 'MP Cat Pate Adult Tuna and Salmon 400g',
       '39.25',
       '65',
       '34'
UNION ALL
SELECT 'MP Cat Pate Chicken and Turkey 100g',
       '25',
       '60',
       '25'
UNION ALL
SELECT 'MP Cat Pate Salmon 100g',
       '25',
       '60',
       '30'
UNION ALL
SELECT 'MP Dog Pate Lamb 400g',
       '37.25',
       '95',
       '35'
UNION ALL
SELECT 'MP Dog Pate Veal Junior 150g',
       '35',
       '75',
       '5'
UNION ALL
SELECT 'Mycocide Shampoo',
       '265',
       '390',
       '1'
UNION ALL
SELECT 'Nail Clipper Large',
       '125',
       '225',
       '0'
UNION ALL
SELECT 'Nail Clipper Medium',
       '115',
       '195',
       '1'
UNION ALL
SELECT 'Nail Clipper Sml',
       '85',
       '190',
       '6'
UNION ALL
SELECT 'Net Muzzle L',
       '0',
       '250',
       '1'
UNION ALL
SELECT 'Net Muzzle S',
       '0',
       '200',
       '5'
UNION ALL
SELECT 'Net Muzzle XL',
       '0',
       '300',
       '0'
UNION ALL
SELECT 'Nexguard 10-25kgs',
       '439',
       '650',
       '-11'
UNION ALL
SELECT 'Nexguard 2-4kgs',
       '373',
       '520',
       '-7'
UNION ALL
SELECT 'Nexguard 25-50kgs',
       '430',
       '680',
       '-3'
UNION ALL
SELECT 'Nexguard 4-10kgs',
       '387',
       '550',
       '2'
UNION ALL
SELECT 'Nexguard Spectra Large',
       '677',
       '900',
       '0'
UNION ALL
SELECT 'Nexguard Spectra Small',
       '677',
       '800',
       ''
UNION ALL
SELECT 'Nursing Bottle',
       '60',
       '170',
       '8'
UNION ALL
SELECT 'Oeral Medications: Refamol Soft Gel Btl 30pcs',
       '',
       '480',
       ''
UNION ALL
SELECT 'Oral Medications CoAmoxiclav Suspension 125mg/31.25mg/5ml 60ml',
       '73',
       '240',
       '31'
UNION ALL
SELECT 'Oral Medications: Doxofylline tablets',
       '20',
       '75',
       '20'
UNION ALL
SELECT 'Oral Medications: Iron Syrup 60ml',
       '30',
       '220',
       '38'
UNION ALL
SELECT 'Oral Medications: Acetylcysteine 200mg Powder',
       '5',
       '150',
       '30'
UNION ALL
SELECT 'Oral Medications: Acetylcysteine 600mg Powder',
       '21',
       '200',
       '30'
UNION ALL
SELECT 'Oral Medications: Allopurinol 100mg Tab',
       '0.55',
       '35',
       '170'
UNION ALL
SELECT 'Oral Medications: AlOHMgOH 300mg Tablet',
       '3',
       '20',
       '499'
UNION ALL
SELECT 'Oral Medications: Amoxicillin 250mg Cap',
       '0.73',
       '15',
       '42'
UNION ALL
SELECT 'Oral Medications: Amoxicillin 500mg Cap',
       '1.08',
       '20',
       '180'
UNION ALL
SELECT 'Oral Medications: Amoxicillin Suspension 125mg/5ml 60ml',
       '11.75',
       '230',
       '84'
UNION ALL
SELECT 'Oral Medications: Amoxicillin Suspension 250mg/5ml 60ml',
       '13.7',
       '260',
       '67'
UNION ALL
SELECT 'Oral Medications: Anti- Diarrhea Liquid',
       '320',
       '550',
       '5'
UNION ALL
SELECT 'Oral Medications: Azithromycin 500mg Tablet',
       '16',
       '90',
       '30'
UNION ALL
SELECT 'Oral Medications: Azithromycin Dihydrate 200mg/5ml',
       '43',
       '350',
       '11'
UNION ALL
SELECT 'Oral Medications: B-Complex Tablet',
       '0',
       '15',
       ''
UNION ALL
SELECT 'Oral Medications: BioCare Doxycycline',
       '9',
       '30',
       '67'
UNION ALL
SELECT 'Oral Medications: Black Armour',
       '442',
       '750',
       '0'
UNION ALL
SELECT 'Oral Medications: Bromhexine Syrup',
       '14.75',
       '250',
       '35'
UNION ALL
SELECT 'Oral Medications: Calcium Lactate 325mg Tab',
       '0.68',
       '15',
       '136'
UNION ALL
SELECT 'Oral Medications: Cani-Vit Multivitamins',
       '11',
       '25',
       '120'
UNION ALL
SELECT 'Oral Medications: Canipril Tablet 5mg',
       '34.5',
       '90',
       '63'
UNION ALL
SELECT 'Oral Medications: Carbocisteine Syrup125mg',
       '15',
       '230',
       '13'
UNION ALL
SELECT 'Oral Medications: Cefalexin 250mg Capsule',
       '1.47',
       '20',
       '108'
UNION ALL
SELECT 'Oral Medications: Cefalexin 500mg Capsule',
       '2.25',
       '25',
       '90'
UNION ALL
SELECT 'Oral Medications: Cefalexin Suspension 125mg/5ml 60ml',
       '14',
       '220',
       '54'
UNION ALL
SELECT 'Oral Medications: Cefalexin Suspension 250mg/5ml 60ml',
       '19.5',
       '260',
       '60'
UNION ALL
SELECT 'Oral Medications: Cetirizine Syrup 5mg/5ml 60ml',
       '18.75',
       '240',
       '100'
UNION ALL
SELECT 'Oral Medications: Cetirizine Tablets 10mg',
       '0.55',
       '15',
       '2264'
UNION ALL
SELECT 'Oral Medications: Chloramphenicol 125mg Susp.',
       '26',
       '300',
       '10'
UNION ALL
SELECT 'Oral Medications: Clindamycin 300mg Capsule',
       '3.63',
       '50',
       '60'
UNION ALL
SELECT 'Oral Medications: Clonidine 150mg Tablet',
       '3.4',
       '25',
       '100'
UNION ALL
SELECT 'Oral Medications: Clopidogrel 75mg Tablet',
       '1.55',
       '30',
       '200'
UNION ALL
SELECT 'Oral Medications: CoAmoxiclav 250mg Tablet',
       '29',
       '80',
       '0'
UNION ALL
SELECT 'Oral Medications: CoAmoxiclav 50mg Tablet',
       '10',
       '50',
       '88'
UNION ALL
SELECT 'Oral Medications: CoAmoxiclav 625mg Tablet',
       '7.1',
       '60',
       '3'
UNION ALL
SELECT 'Oral Medications: CoAmoxiclav Suspension 250mg/62.5mg/5ml 60ml',
       '107',
       '320',
       '19'
UNION ALL
SELECT 'Oral Medications: Colchicine 500mcg Tablet',
       '1.05',
       '25',
       '100'
UNION ALL
SELECT 'Oral Medications: Colimoxyn',
       '130',
       '300',
       '0'
UNION ALL
SELECT 'Oral Medications: Cotrimoxazole 480mg/5ml Susp',
       '20.15',
       '270',
       '73'
UNION ALL
SELECT 'Oral Medications: Cotrimoxazole 960mg Tablet',
       '1.15',
       '30',
       '119'
UNION ALL
SELECT 'Oral Medications: Cotrimoxazole Suspension 200mg/40mg/5ml 60ml',
       '23.25',
       '250',
       '60'
UNION ALL
SELECT 'Oral Medications: Deltacal Calcium Supplement',
       '3.6',
       '20',
       '200'
UNION ALL
SELECT 'Oral Medications: Dewormer Tablet',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Oral Medications: Dexamethasone 500mg Tablet',
       '0.47',
       '20',
       '223'
UNION ALL
SELECT 'Oral Medications: Dextromethorphan HBr 10mg tab',
       '0',
       '18',
       '8'
UNION ALL
SELECT 'Oral Medications: Dextromethorphan Syrup',
       '50',
       '290',
       '4'
UNION ALL
SELECT 'Oral Medications: Dicycloverine 10mg Tablet',
       '0.22',
       '25',
       '260'
UNION ALL
SELECT 'Oral Medications: Diphenhydramine 12.5mg Syrup',
       '11.85',
       '240',
       '136'
UNION ALL
SELECT 'Oral Medications: Diphenhydramine 50mg Capsule',
       '0.65',
       '15',
       '290'
UNION ALL
SELECT 'Oral Medications: Domperidone (Dompy) 1mg/ml Syrup',
       '75',
       '320',
       '19'
UNION ALL
SELECT 'Oral Medications: Domperidone 10mg Tablet',
       '0.45',
       '25',
       '480'
UNION ALL
SELECT 'Oral Medications: Doxycycline 100mg Capsule',
       '3',
       '30',
       '158'
UNION ALL
SELECT 'Oral Medications: Ener-G',
       '120',
       '280',
       '0'
UNION ALL
SELECT 'Oral Medications: Enmalac',
       '141',
       '280',
       '0'
UNION ALL
SELECT 'Oral Medications: Enrocare Oral Suspension',
       '250',
       '420',
       '4'
UNION ALL
SELECT 'Oral Medications: Enrofloxacin Tablets',
       '30',
       '50',
       '300'
UNION ALL
SELECT 'Oral Medications: Ferrous Sulfate Syrup 60ml',
       '11.5',
       '230',
       '11'
UNION ALL
SELECT 'Oral Medications: Ferrous Sulfate tablets',
       '0',
       '20',
       '24'
UNION ALL
SELECT 'Oral Medications: Finasteride 5mg Tablet',
       '3.95',
       '45',
       '90'
UNION ALL
SELECT 'Oral Medications: Fluconazole Capsule 150mg',
       '0',
       '90',
       '45'
UNION ALL
SELECT 'Oral Medications: Furosemide 20mg Tablet',
       '0.41',
       '30',
       '155'
UNION ALL
SELECT 'Oral Medications: Furosemide 40 mg Tablet',
       '0.42',
       '25',
       '70'
UNION ALL
SELECT 'Oral Medications: Gabapentin Capsule 300mg',
       '8',
       '40',
       '30'
UNION ALL
SELECT 'Oral Medications: Hyoscine 10mg Tablet',
       '2.6',
       '25',
       '100'
UNION ALL
SELECT 'Oral Medications: Hyoscine Syrup 10mg/5ml 60ml',
       '43',
       '280',
       '13'
UNION ALL
SELECT 'Oral Medications: ImmunoCare Syrup',
       '260',
       '420',
       '12'
UNION ALL
SELECT 'Oral Medications: Immunol Syrup',
       '300',
       '750',
       '3'
UNION ALL
SELECT 'Oral Medications: Immunol Tablets',
       '5.5',
       '17',
       '173'
UNION ALL
SELECT 'Oral Medications: Itraconazole Tablet 100mg',
       '0',
       '75',
       '-105'
UNION ALL
SELECT 'Oral Medications: Ketoanalogues+Amino Acid Tablets',
       '15',
       '60',
       '85'
UNION ALL
SELECT 'Oral Medications: Lactulose',
       '67.5',
       '350',
       '7'
UNION ALL
SELECT 'Oral Medications: Lagundi 300mg Tablets',
       '0.91',
       '40',
       '536'
UNION ALL
SELECT 'Oral Medications: Lagundi Syrup 120ml',
       '46',
       '320',
       '13'
UNION ALL
SELECT 'Oral Medications: LC Scour',
       '97',
       '240',
       '15'
UNION ALL
SELECT 'Oral Medications: LC-Vit Cat Multivitamins 120ml',
       '115',
       '230',
       '9'
UNION ALL
SELECT 'Oral Medications: LC-Vit Multivitamins 120ml',
       '115',
       '200',
       '0'
UNION ALL
SELECT 'Oral Medications: LC-Vit Multivitamins 60ml',
       '85',
       '120',
       '0'
UNION ALL
SELECT 'Oral Medications: Leash w/ Collar Small (Red)',
       '45',
       '150',
       '0'
UNION ALL
SELECT 'Oral Medications: Levocetirizine 5mg Tablet',
       '2.5',
       '30',
       '80'
UNION ALL
SELECT 'Oral Medications: Levothyroxine 50mcg',
       '1.54',
       '30',
       '90'
UNION ALL
SELECT 'Oral Medications: Liv52 Forte tablets',
       '4.66',
       '15',
       '0'
UNION ALL
SELECT 'Oral Medications: Liv52 Pet Liquid 200ml',
       '280',
       '450',
       '0'
UNION ALL
SELECT 'Oral Medications: Liver Gard',
       '187',
       '280',
       '0'
UNION ALL
SELECT 'Oral Medications: Livotine 120ml',
       '168',
       '280',
       '0'
UNION ALL
SELECT 'Oral Medications: Loperamide 2mg Capsule',
       '0.46',
       '20',
       '280'
UNION ALL
SELECT 'Oral Medications: Meloxicam 15mg Tablet',
       '2.1',
       '30',
       '254'
UNION ALL
SELECT 'Oral Medications: Menadione Sodium Tablet 10mg',
       '1',
       '25',
       '188'
UNION ALL
SELECT 'Oral Medications: Methiovet',
       '10',
       '40',
       '32'
UNION ALL
SELECT 'Oral Medications: Methozine',
       '140',
       '280',
       '1'
UNION ALL
SELECT 'Oral Medications: Methylergometrine tablets',
       '10',
       '40',
       '120'
UNION ALL
SELECT 'Oral Medications: Metoclopramide 10mg Tablet',
       '0.7',
       '12',
       '198'
UNION ALL
SELECT 'Oral Medications: Metoclopramide Syrup 5mg/ml',
       '14.25',
       '200',
       '157'
UNION ALL
SELECT 'Oral Medications: Metronidazole IV 100ml',
       '13.25',
       '300',
       '3'
UNION ALL
SELECT 'Oral Medications: Metronidazole Suspension 125mg/5ml 60ml',
       '13.95',
       '260',
       '44'
UNION ALL
SELECT 'Oral Medications: Metronidazole Tablet 500mg',
       '0.95',
       '20',
       '158'
UNION ALL
SELECT 'Oral Medications: Microzole',
       '130',
       '220',
       '4'
UNION ALL
SELECT 'Oral Medications: Multivitamins + BComplex Tablet',
       '3',
       '12',
       '25'
UNION ALL
SELECT 'Oral Medications: Nefrotec DS',
       '4.41',
       '15',
       '72'
UNION ALL
SELECT 'Oral Medications: Nematocide 15ml',
       '85',
       '180',
       '0'
UNION ALL
SELECT 'Oral Medications: Nematocide 60ml',
       '257',
       '400',
       '4'
UNION ALL
SELECT 'Oral Medications: Nepro 60ml',
       '260',
       '450',
       '16'
UNION ALL
SELECT 'Oral Medications: Nutrical 120ml',
       '136',
       '240',
       '1'
UNION ALL
SELECT 'Oral Medications: Nystatin Drops 10k units/12ml',
       '48',
       '240',
       '10'
UNION ALL
SELECT 'Oral Medications: Omega-3/Fishoil',
       '1.15',
       '5',
       '200'
UNION ALL
SELECT 'Oral Medications: Omeprazole 20mg Capsule',
       '2.05',
       '10',
       '200'
UNION ALL
SELECT 'Oral Medications: Omeprazole 40mg Capsule',
       '4',
       '15',
       '140'
UNION ALL
SELECT 'Oral Medications: Papi Doxy 25mg/ml',
       '144',
       '280',
       '0'
UNION ALL
SELECT 'Oral Medications: Papi Iron Plus 120ml',
       '145',
       '320',
       '0'
UNION ALL
SELECT 'Oral Medications: Papi Multi Vitamins 120ml',
       '95',
       '220',
       '2'
UNION ALL
SELECT 'Oral Medications: Papi Nacalvit-C 120ml',
       '121',
       '240',
       '5'
UNION ALL
SELECT 'Oral Medications: Papi OB 120ml',
       '132',
       '250',
       '2'
UNION ALL
SELECT 'Oral Medications: Papi Scour',
       '91',
       '200',
       '1'
UNION ALL
SELECT 'Oral Medications: Pawell Bromhexine 120ml',
       '190',
       '320',
       '2'
UNION ALL
SELECT 'Oral Medications: Pawell Iron+B complex 120ml',
       '180',
       '300',
       '2'
UNION ALL
SELECT 'Oral Medications: Pawell Metronidazole 120ml',
       '170',
       '320',
       '2'
UNION ALL
SELECT 'Oral Medications: Pawell Multivitamins 120ml',
       '160',
       '260',
       '6'
UNION ALL
SELECT 'Oral Medications: Pawell OB 120ml',
       '175',
       '280',
       '4'
UNION ALL
SELECT 'Oral Medications: Pet Care Multi Vitamins 120ml',
       '140',
       '220',
       '18'
UNION ALL
SELECT 'Oral Medications: Petpyrin 60ML',
       '270',
       '420',
       '11'
UNION ALL
SELECT 'Oral Medications: Pimobendan 5mg',
       '50',
       '110',
       '90'
UNION ALL
SELECT 'Oral Medications: Pimobendan Tablet 1.25mg',
       '35',
       '90',
       '46'
UNION ALL
SELECT 'Oral Medications: Pizotifen Maleate Syrup',
       '37',
       '320',
       '48'
UNION ALL
SELECT 'Oral Medications: Potassium Citrate (Alkalinse)',
       '5',
       '20',
       '60'
UNION ALL
SELECT 'Oral Medications: Prednisolone Syrup 15mg/5ml',
       '84',
       '450',
       '0'
UNION ALL
SELECT 'Oral Medications: Prednisone 10mg Tablet',
       '0.65',
       '15',
       '490'
UNION ALL
SELECT 'Oral Medications: Prednisone 5mg Tablet',
       '0.42',
       '10',
       '2005'
UNION ALL
SELECT 'Oral Medications: Prednisone Syrup 60ml',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Oral Medications: Pregabalin 75mg Capsule',
       '13.41',
       '50',
       '30'
UNION ALL
SELECT 'Oral Medications: Refamol Soft Gel',
       '9.5',
       '25',
       '20'
UNION ALL
SELECT 'Oral Medications: Renacure Kidney Supplement 60ml',
       '148',
       '300',
       '2'
UNION ALL
SELECT 'Oral Medications: Renal Care Syrup',
       '182',
       '250',
       '0'
UNION ALL
SELECT 'Oral Medications: Reprovit OB',
       '135',
       '270',
       '3'
UNION ALL
SELECT 'Oral Medications: Salbutamol 2mg Tablet Ventomax',
       '0.21',
       '25',
       '146'
UNION ALL
SELECT 'Oral Medications: Salbutamol Syrup 2mg/5ml 60ml',
       '10.25',
       '200',
       '34'
UNION ALL
SELECT 'Oral Medications: Sambong 250mg Tablet',
       '1.3',
       '15',
       '300'
UNION ALL
SELECT 'Oral Medications: Sildenafil 100mg Tablet',
       '13',
       '70',
       '10'
UNION ALL
SELECT 'Oral Medications: Sildenafil 50mg Tablet',
       '10.5',
       '50',
       '8'
UNION ALL
SELECT 'Oral Medications: Simparica Trio 10.1-20kg',
       '602',
       '920',
       '0'
UNION ALL
SELECT 'Oral Medications: Simparica Trio 2.6-5kg',
       '536',
       '780',
       '0'
UNION ALL
SELECT 'Oral Medications: Simparica Trio 20-40kg',
       '661',
       '990',
       '6'
UNION ALL
SELECT 'Oral Medications: Simparica Trio 5.1-10kg',
       '587',
       '850',
       '0'
UNION ALL
SELECT 'Oral Medications: Tranexamic Acid 500mg Capsule',
       '4.1',
       '30',
       '35'
UNION ALL
SELECT 'Oral Medications: Ursodeoxycholic Acid 25mg Capsule',
       '16.25',
       '0',
       '40'
UNION ALL
SELECT 'Oral Medications: Vitamin B Complex Tablet',
       '0.92',
       '15',
       '80'
UNION ALL
SELECT 'Oral Medications: Vitamin K 10mg Tablet',
       '0',
       '25',
       '77'
UNION ALL
SELECT 'Oral Medications" Bacillus clausii/Probiotics',
       '0',
       '60',
       '16'
UNION ALL
SELECT 'Oral Medicatons: Ascorbic Acid Syrup',
       '12',
       '160',
       '12'
UNION ALL
SELECT 'Oral Rehydration Dehydrosol',
       '1.88',
       '15',
       '62'
UNION ALL
SELECT 'Padded Collar Sml',
       '68',
       '160',
       '7'
UNION ALL
SELECT 'PANACEA Veterinary Clinic Record Book',
       '80',
       '250',
       ''
UNION ALL
SELECT 'Paracetamol Toxicity Antidote/Vial',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Paw Smart Collapsible Cage Size 1',
       '530',
       '900',
       ''
UNION ALL
SELECT 'Paw Smart Collapsible Cage Size 2',
       '780',
       '1300',
       ''
UNION ALL
SELECT 'Paw Smart Collapsible Cage Size 3',
       '1230',
       '1800',
       ''
UNION ALL
SELECT 'Paw Smart Collapsible Cage Size 4',
       '1430',
       '2100',
       ''
UNION ALL
SELECT 'Paw Smart Toothbrush Set',
       '110',
       '220',
       '-3'
UNION ALL
SELECT 'Pawdel Cologne Newborn',
       '200',
       '340',
       '0'
UNION ALL
SELECT 'Pawdel Cologne Puppy',
       '200',
       '340',
       '0'
UNION ALL
SELECT 'Pawdel Cologne Sweet Dreams',
       '200',
       '340',
       '0'
UNION ALL
SELECT 'Pawell All in One Shampoo 500ml',
       '235',
       '350',
       '4'
UNION ALL
SELECT 'Pawell Charcoal Shampoo 500ml',
       '235',
       '350',
       '4'
UNION ALL
SELECT 'Pawell Mite Buster 60ml',
       '220',
       '350',
       '0'
UNION ALL
SELECT 'Pawell Organic Spray 120ml',
       '240',
       '350',
       '3'
UNION ALL
SELECT 'Pd Denta Sticks Medium',
       '80',
       '140',
       '1'
UNION ALL
SELECT 'Pd Denta Sticks Puppy',
       '73',
       '130',
       '0'
UNION ALL
SELECT 'Pd Denta Sticks Small',
       '75',
       '135',
       '0'
UNION ALL
SELECT 'Pd Meat Jerky Smoky Beef (Stix)',
       '73',
       '150',
       '-1'
UNION ALL
SELECT 'Pd Meat Jerky Smoky Beef (Strap)',
       '0',
       '0',
       '0'
UNION ALL
SELECT 'Pd Meat Jerky Grilled Liver (Strap)',
       '73',
       '150',
       '0'
UNION ALL
SELECT 'Pee Pads (L)',
       '17.31',
       '40',
       '14'
UNION ALL
SELECT 'Pee Pads (M)',
       '8.98',
       '25',
       '63'
UNION ALL
SELECT 'Pee Pads (S)',
       '4.66',
       '15',
       '243'
UNION ALL
SELECT 'Pet Armor Ear Mite & Tick (Cat)',
       '235',
       '440',
       '4'
UNION ALL
SELECT 'Pet Armor Ear Mite & Tick (Dog)',
       '240',
       '440',
       '1'
UNION ALL
SELECT 'Pet Bed Lrg',
       '360',
       '650',
       '-1'
UNION ALL
SELECT 'Pet Bed Med',
       '250',
       '550',
       '2'
UNION ALL
SELECT 'Pet Bed Sml',
       '220',
       '450',
       '3'
UNION ALL
SELECT 'Pet Toothbrush',
       '26',
       '65',
       '8'
UNION ALL
SELECT 'Plastic Muzzle Size 1',
       '50',
       '160',
       '0'
UNION ALL
SELECT 'Plastic Muzzle Size 2',
       '60',
       '180',
       '1'
UNION ALL
SELECT 'Plastic Muzzle Size 3',
       '70',
       '200',
       '1'
UNION ALL
SELECT 'Play Pets Cologne',
       '235',
       '350',
       '11'
UNION ALL
SELECT 'Play Pets Powder',
       '140',
       '250',
       '9'
UNION ALL
SELECT 'Play Pets Soap Anti Tick and Flea',
       '75',
       '135',
       '2'
UNION ALL
SELECT 'Prama Dog Snack',
       '80',
       '120',
       '35'
UNION ALL
SELECT 'ProBioCare Powder',
       '180',
       '380',
       '6'
UNION ALL
SELECT 'Puppy Love Milk Replacement 300g',
       '250',
       '360',
       '5'
UNION ALL
SELECT 'Puppy Love Milk Replacemnet 600g',
       '550',
       '700',
       '0'
UNION ALL
SELECT 'RC Recovery Liquid',
       '0',
       '400',
       ''
UNION ALL
SELECT 'Remedy Recovery Medicated Hotspot Spray',
       '230',
       '550',
       '0'
UNION ALL
SELECT 'Rope with Collar Small (Assorted Color)',
       '55',
       '180',
       '4'
UNION ALL
SELECT 'Royal Canin Gastrointestinal Diet',
       '0',
       '240',
       ''
UNION ALL
SELECT 'Royal Canin Recovery Diet',
       '0',
       '260',
       ''
UNION ALL
SELECT 'Rubber Muzzle Size 2',
       '55',
       '160',
       '3'
UNION ALL
SELECT 'Rubber Muzzle Size 3',
       '68',
       '200',
       '0'
UNION ALL
SELECT 'Rubber Muzzle Size 5',
       '93',
       '260',
       '-1'
UNION ALL
SELECT 'Saint Roche Cologne Happiness 125ml',
       '200',
       '320',
       '0'
UNION ALL
SELECT 'Saint Roche Cologne Heaven Scent 125ml',
       '200',
       '320',
       '0'
UNION ALL
SELECT 'Saint Roche Shampoo 1000ml Happiness',
       '383',
       '600',
       '0'
UNION ALL
SELECT 'Saint Roche Shampoo 1000ml Heaven Scent',
       '383',
       '600',
       '5'
UNION ALL
SELECT 'Saint Roche Shampoo 1000ml Mother Nature',
       '383',
       '600',
       '5'
UNION ALL
SELECT 'Saint Roche Shampoo 1000ml Sweet Embrace',
       '383',
       '600',
       '0'
UNION ALL
SELECT 'Saint Roche Shampoo Happiness 250ml',
       '143',
       '280',
       '5'
UNION ALL
SELECT 'Saint Roche Shampoo Heaven Scent 250ml',
       '143',
       '280',
       '5'
UNION ALL
SELECT 'Saint Roche Shampoo Mother Nature 250ml',
       '143',
       '280',
       '5'
UNION ALL
SELECT 'Saint Roche Shampoo Sweet Embrace 250 ml',
       '143',
       '280',
       '0'
UNION ALL
SELECT 'Salbutamol Nebule',
       '17',
       '60',
       '18'
UNION ALL
SELECT 'Slicker Brush L',
       '110',
       '275',
       '0'
UNION ALL
SELECT 'Slicker Brush M',
       '60',
       '150',
       '-2'
UNION ALL
SELECT 'Slicker Brush XL',
       '150',
       '320',
       '1'
UNION ALL
SELECT 'Special Dog Can Beef Tripe',
       '55',
       '95',
       '0'
UNION ALL
SELECT 'Special Dog Can Junior 400g',
       '55',
       '95',
       '2'
UNION ALL
SELECT 'Special Dog Can Lamb and Turkey',
       '55',
       '95',
       '6'
UNION ALL
SELECT 'Special Dog Can Lamb Tripe',
       '55',
       '95',
       '5'
UNION ALL
SELECT 'Special Dog Can Veal and Veg',
       '55',
       '95',
       '6'
UNION ALL
SELECT 'Test Kit: 3way CPVCCVGIA',
       '350',
       '1500',
       '19'
UNION ALL
SELECT 'Test Kit: 4way EHR/ANA/BAB/CHW Rapid Test',
       '450',
       '1800',
       '12'
UNION ALL
SELECT 'Test Kit: Canine Distemper Virus',
       '100',
       '1000',
       '17'
UNION ALL
SELECT 'Test Kit: Canine Parvovirus',
       '100',
       '850',
       '17'
UNION ALL
SELECT 'Test Kit: Canine Pregnancy Test (Relaxin Assay)',
       '600',
       '1500',
       '5'
UNION ALL
SELECT 'Test Kit: Ehrlichia Ab Rapid Test',
       '250',
       '1200',
       '8'
UNION ALL
SELECT 'Test Kit: Feline Panleukopenia/FPV',
       '150',
       '1000',
       '15'
UNION ALL
SELECT 'Test Kit: Rabies Ag',
       '150',
       '900',
       '3'
UNION ALL
SELECT 'Test Kits: Canine Corona Virus',
       '150',
       '900',
       '0'
UNION ALL
SELECT 'Test Kits: FIV/FeLV Test',
       '430',
       '1400',
       '5'
UNION ALL
SELECT 'Test Kits: Heartworm Ag Rapid Test',
       '250',
       '1200',
       '10'
UNION ALL
SELECT 'Test Kits: Leptospirosis Ab Test',
       '2500',
       '1400',
       '12'
UNION ALL
SELECT 'Tooth Extraction (per tooth)',
       '0',
       '150',
       ''
UNION ALL
SELECT 'Topical Medications/Drops: Betamethasone Valerate Cream 1mg/g 5g',
       '19',
       '180',
       '21'
UNION ALL
SELECT 'Topical Medications/Drops: Ketaconazole Cream',
       '30',
       '160',
       '10'
UNION ALL
SELECT 'Topical Medications/Drops: Mupirocin Ointment',
       '56',
       '230',
       '39'
UNION ALL
SELECT 'Topical medications/Drops: PND Ear Drops',
       '92',
       '450',
       '19'
UNION ALL
SELECT 'Topical Medications/Drops: Scavon Cream',
       '250',
       '400',
       '1'
UNION ALL
SELECT 'Topical Medications/Drops: Tacrolimus Ointment',
       '250',
       '450',
       '1'
UNION ALL
SELECT 'Topical Medications/Drops: Tobramycin Dexamethasone Eye Drops',
       '88',
       '320',
       '31'
UNION ALL
SELECT 'Topical Medicatons/Drops: Cyclosporine Opthalmic Ointment',
       '400',
       '650',
       '12'
UNION ALL
SELECT 'Vaccination Record',
       '0',
       'variable',
       ''
UNION ALL
SELECT 'Vaccination: Anti-Rabies',
       '26',
       '300',
       '100'
UNION ALL
SELECT 'Vaccination: DHLPPi (5in1)',
       '124.28',
       '450',
       '132'
UNION ALL
SELECT 'Vaccination: DHLPPi+CCV (6in1)',
       '153.57',
       '600',
       '158'
UNION ALL
SELECT 'Vaccination: Feline Vaccine/Tricat (CRP)',
       '520',
       '900',
       '40'
UNION ALL
SELECT 'Vaccination: Kennel Cough',
       '215.81',
       '550',
       '25'
UNION ALL
SELECT 'Vaccination: Proheart Injection (1-5kgs)',
       '469',
       '1400',
       ''
UNION ALL
SELECT 'Vaccination: Proheart Injection (11-15kgs)',
       '0',
       '1850',
       ''
UNION ALL
SELECT 'Vaccination: Proheart Injection (16-20kgs)',
       '0',
       '2400',
       ''
UNION ALL
SELECT 'Vaccination: Proheart Injection (6-10kgs)',
       '0',
       '1600',
       ''
UNION ALL
SELECT 'VC10 Recovery Paste for Dog 120g',
       '465',
       '780',
       '2'
UNION ALL
SELECT 'VC11 Recovery Paste for Cat 120g',
       '465',
       '780',
       '2'
UNION ALL
SELECT 'VC13 Lysine for Cat 100ml',
       '1500',
       '2500',
       '1'
UNION ALL
SELECT 'VC33 Iron Oral Solution 100ml',
       '555',
       '900',
       '3'
UNION ALL
SELECT 'Venoma Spray',
       '223',
       '360',
       '0'
UNION ALL
SELECT 'Vetnoderm Cream',
       '138.46',
       '280',
       '1'
UNION ALL
SELECT 'Vetnoderm Soap (Medicated Herbal)',
       '69.23',
       '140',
       '2'
UNION ALL
SELECT 'Vetnoderm Soap (Sulfur+VCO)',
       '78.46',
       '160',
       '-5'
UNION ALL
SELECT 'Virbac Nutriplus Gel',
       '450',
       '600',
       '0'
UNION ALL
SELECT 'VKC1 Rx Liquid - Kidney for Cat 250g',
       '300',
       '450',
       '12'
UNION ALL
SELECT 'VLC1 Liver Diet for cat 250ml',
       '300',
       '450',
       '12'
UNION ALL
SELECT 'VRC1 Rx Liquid Recovery for Cat 250g',
       '300',
       '450',
       '-1'
UNION ALL
SELECT 'VUCI Urinary Diet for Cat 250ml',
       '300',
       '450',
       '7'
UNION ALL
SELECT 'Water Nozzle (Drinker)',
       '90',
       '180',
       '3'

GO 
