DEclare @GUID_Company VARCHAR(MAX) = '5A1BF348-6302-41D6-946B-A54F6E967F8F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Item   INT,
     Name_Item varchar(500),
     UnitCost  Decimal(18, 2),
     UnitPrice Decimal(18, 2),
     Quantity  int
  )

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

INSERT @ForImport
       (Name_Item,
        UnitCost,
        UnitPrice,
        Quantity)
SELECT dbo.fGetCleanedString([Name]),
       TRY_CONVERT(Decimal(18, 2), [Cost]),
       TRY_CONVERT(Decimal(18, 2), [Price]),
       TRY_CONVERT(INT, [In stock])
FROM   dbo.[PANACEA_ITEMS]
order  by [In stock]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
where  ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = 2

INSERT tItem
       (ID_Company,
        Name,
        ID_ItemType,
        UnitCost,
        UnitPrice,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company,
       Name_Item,
       2,
       UnitCost,
       UnitPrice,
       'Imported on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt' ),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @ForImport
WHERE  ID_Item IS NULL
       AND LEN(ISNULL(Name_Item, '')) > 0
ORDER  BY Name_Item ASC

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
where  ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = 2

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10
 
INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.Quantity,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
		'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport import inner join tItem item on import.ID_Item = item.ID
WHERE  ISNULL(Quantity,0) > 0 
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
 @ID_UserSession

SELECT * FROM tItem where ID_Company  = 246 and ID_ItemType = 2