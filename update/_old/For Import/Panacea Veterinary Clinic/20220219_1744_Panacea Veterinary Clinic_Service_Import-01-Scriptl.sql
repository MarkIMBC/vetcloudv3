DEclare @GUID_Company VARCHAR(MAX) = '5A1BF348-6302-41D6-946B-A54F6E967F8F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Item   INT,
     Name_Item varchar(500),
     UnitCost  Decimal(18, 2),
     UnitPrice Decimal(18, 2),
     Quantity  int
  )

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

INSERT @ForImport
       (Name_Item,
        UnitCost,
        UnitPrice,
        Quantity)
SELECT dbo.fGetCleanedString([SERVICES]),
       TRY_CONVERT(Decimal(18, 2), [Buying Price]),
       TRY_CONVERT(Decimal(18, 2), [service Cost]),
       TRY_CONVERT(INT, [Quantity])
FROM   dbo.[PANACEA_Services]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
where  ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = 1

INSERT tItem
       (ID_Company,
        Name,
        ID_ItemType,
        UnitCost,
        UnitPrice,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company,
       Name_Item,
       1,
       UnitCost,
       UnitPrice,
       'Imported on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt' ),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @ForImport
WHERE  ID_Item IS NULL
       AND LEN(ISNULL(Name_Item, '')) > 0
ORDER  BY Name_Item ASC

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
where  ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = 1

SELECT * FROM tItem where ID_Company  = 246 and ID_ItemType = 1