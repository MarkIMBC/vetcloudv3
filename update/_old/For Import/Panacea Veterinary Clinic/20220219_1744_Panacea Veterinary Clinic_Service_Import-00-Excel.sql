if OBJECT_ID('dbo.[PANACEA_Services]') is not null
  BEGIN
      DROP TABLE [PANACEA_Services]
  END

GO

CREATE TABLE [dbo].[PANACEA_Services]
  (
     [SERVICES]     varchar(500),
     [Buying Price] varchar(500),
     [service Cost] varchar(500),
     [Quantity]     varchar(500)
  )

GO

INSERT INTO [dbo].[PANACEA_Services]
            ([SERVICES],
             [Buying Price],
             [service Cost],
             [Quantity])
SELECT 'Boarding Day Care 1-3hrs up to 5kgs',
       '0',
       '100',
       ''
UNION ALL
SELECT 'Boarding/Day 1-5kgs',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Boarding/Day 11-15kgs',
       '0',
       '700',
       ''
UNION ALL
SELECT 'Boarding/Day 16-20kgs',
       '0',
       '800',
       ''
UNION ALL
SELECT 'Boarding/Day 6-10kgs',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Broadline Spot on 2.5-7.5kg',
       '0',
       '650',
       ''
UNION ALL
SELECT 'Broadline Spot-on upto 2.5kgs',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Confinement/Day <5kgs',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Confinement/Day 11-15 kgs',
       '0',
       '700',
       ''
UNION ALL
SELECT 'Confinement/Day 16-20 kgs',
       '0',
       '800',
       ''
UNION ALL
SELECT 'Confinement/Day 21-25',
       '0',
       '900',
       ''
UNION ALL
SELECT 'Confinement/Day 6-10 kgs',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Grooming Services Bath and Blow dry 1-5kgs',
       '30',
       '300',
       ''
UNION ALL
SELECT 'Grooming Services Bath and Blow Dry 11-15kgs',
       '55',
       '550',
       ''
UNION ALL
SELECT 'Grooming Services: Bath and Blow Dry 16-20 kgs',
       '35',
       '600',
       ''
UNION ALL
SELECT 'Grooming Services: Anal Sac Expression',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Grooming Services: Bath and Blow Dry 6-10 kgs',
       '45',
       '450',
       ''
UNION ALL
SELECT 'Grooming Services: Dematting 1-5kgs',
       '35',
       '350',
       ''
UNION ALL
SELECT 'Grooming Services: Dematting 6-10 kgs',
       '45',
       '450',
       ''
UNION ALL
SELECT 'Grooming Services: Ear Cleaning 1-5 kgs',
       '0',
       '80',
       ''
UNION ALL
SELECT 'Grooming Services: Ear Cleaning 11-15 kgs',
       '0',
       '120',
       ''
UNION ALL
SELECT 'Grooming Services: Ear Cleaning 16-20 kgs',
       '0',
       '200',
       ''
UNION ALL
SELECT 'Grooming Services: Ear Cleaning 6-10 kgs',
       '0',
       '100',
       ''
UNION ALL
SELECT 'Grooming Services: Face Trim',
       '0',
       '100',
       ''
UNION ALL
SELECT 'Grooming Services: Full Groom 1-5kgs',
       '50',
       '450',
       ''
UNION ALL
SELECT 'Grooming Services: Full Groom 20-25kgs Thick Fur',
       '0',
       '900',
       ''
UNION ALL
SELECT 'Grooming Services: Full Groom 21-25 kgs',
       '0',
       '800',
       ''
UNION ALL
SELECT 'Grooming Services: Full Grooming 10-15kgs',
       '65',
       '600',
       ''
UNION ALL
SELECT 'Grooming Services: Full Grooming 15-20kgs',
       '80',
       '700',
       ''
UNION ALL
SELECT 'Grooming Services: Full Grooming 5-10kgs',
       '50',
       '500',
       ''
UNION ALL
SELECT 'Grooming Services: Full Grooming Cats',
       '60',
       '600',
       ''
UNION ALL
SELECT 'Grooming Services: Nail Clip 1-5kgs',
       '0',
       '80',
       ''
UNION ALL
SELECT 'Grooming Services: Nail Clip 11-15kgs',
       '0',
       '120',
       ''
UNION ALL
SELECT 'Grooming Services: Nail Clip 16-20kgs',
       '0',
       '150',
       ''
UNION ALL
SELECT 'Grooming Services: Nail Clip 6-10kgs',
       '0',
       '100',
       ''
UNION ALL
SELECT 'Grooming Services: Paw Trim',
       '0',
       '100',
       ''
UNION ALL
SELECT 'Grooming Servies: Sanitary Trim',
       '0',
       '150',
       ''
UNION ALL
SELECT 'Laboratory Services: Basic Chemistry',
       '350',
       '900',
       ''
UNION ALL
SELECT 'Laboratory Services: CBC+Chemistry Package',
       '570',
       '1350',
       ''
UNION ALL
SELECT 'Laboratory Services: Complete Blood Count (CBC)',
       '250',
       '550',
       ''
UNION ALL
SELECT 'Laboratory Services: Comprehensive Chemistry',
       '900',
       '1600',
       ''
UNION ALL
SELECT 'Laboratory Services: Ear Swab/Microscopy',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Laboratory Services: Fecalysis/Microscopy',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Laboratory Services: Flourescent Dye Test',
       '1',
       '300',
       '81'
UNION ALL
SELECT 'Laboratory Services: Impression Smear',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Laboratory Services: Progesterone Test',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Laboratory Services: Schirmer Tear Test (2 Strips)',
       '21',
       '350',
       '50'
UNION ALL
SELECT 'Laboratory Services: Skin Scraping with Microscopy',
       '0',
       '350',
       ''
UNION ALL
SELECT 'Laboratory Services: Smear Test',
       '0',
       '450',
       ''
UNION ALL
SELECT 'Laboratory Services: Staining/Microscopy',
       '0',
       '350',
       ''
UNION ALL
SELECT 'Laboratory Services: Urinalysis (4 Parameters)',
       '3',
       '200',
       '99'
UNION ALL
SELECT 'Laboratory Services: Urine Microscopy (Crystals)',
       '200',
       '350',
       ''
UNION ALL
SELECT 'Surgery Hernia Repair',
       '0',
       '5000',
       ''
UNION ALL
SELECT 'Surgery: Aural Hematoma',
       '0',
       '2000',
       ''
UNION ALL
SELECT 'Surgery: C-Section 1-5kgs',
       '0',
       '7000',
       ''
UNION ALL
SELECT 'Surgery: C-Section 10-15kgs',
       '0',
       '12000',
       ''
UNION ALL
SELECT 'Surgery: C-Section 15-20kgs',
       '0',
       '14000',
       ''
UNION ALL
SELECT 'Surgery: C-Section 21-30kgs',
       '0',
       '16000',
       ''
UNION ALL
SELECT 'Surgery: C-Section 6-10kgs',
       '0',
       '9000',
       ''
UNION ALL
SELECT 'Surgery: Cherry Eye Removal',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Surgery: Corneal Ulceration (Third Eyelid Flap+Tarsorrhapy)',
       '0',
       '3500',
       ''
UNION ALL
SELECT 'Surgery: Cystotomy 5-10kgs',
       '0',
       '7000',
       ''
UNION ALL
SELECT 'Surgery: Cystotomy1-5kgs',
       '0',
       '6000',
       ''
UNION ALL
SELECT 'Surgery: Debridement and Stitch Up (per inch)',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Surgery:¬†Enucleation',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Surgery: Exploratory Laparotomy 1 to 5 kgs',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Surgery: Neuter 1-5kgs',
       '0',
       '2000',
       ''
UNION ALL
SELECT 'Surgery: Neuter 11-15kgs',
       '0',
       '3000',
       ''
UNION ALL
SELECT 'Surgery: Neuter 15-20kgs',
       '0',
       '3500',
       ''
UNION ALL
SELECT 'Surgery: Neuter 6-10kgs',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Surgery: Neuter Cat',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Surgery: Pyometra (addtl)',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Surgery: Rectal Prolapse 1-5kgs (Purse String)',
       '0',
       '3000',
       ''
UNION ALL
SELECT 'Surgery: Rectal Prolapse Repair',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Surgery: Rectal Resection and Anastomosis 1-5kgs',
       '0',
       '6000',
       ''
UNION ALL
SELECT 'Surgery: Spay 1-5kgs',
       '0',
       '3000',
       ''
UNION ALL
SELECT 'Surgery: Spay 10-15kgs',
       '0',
       '5000',
       ''
UNION ALL
SELECT 'Surgery: Spay 16-20kgs',
       '0',
       '6000',
       ''
UNION ALL
SELECT 'Surgery: Spay 20-25kgs',
       '0',
       '7000',
       ''
UNION ALL
SELECT 'Surgery: Spay 5-10kgs',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Surgery: Spay w/ Pyometra (addtl)',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Surgery: Surgery Misc 1-5kgs',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Surgery: Surgery Misc 10-15kgs',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Surgery: Surgery Misc 15-20kgs',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Surgery: Surgery Misc 5-10kgs',
       '0',
       '400',
       ''
UNION ALL
SELECT 'Surgery: Tail Amputation',
       '0',
       '2000',
       ''
UNION ALL
SELECT 'Surgery: Tail Docking <2 weeks',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Surgery: Tumor Excision',
       '0',
       'variable',
       ''
UNION ALL
SELECT 'Veterianary Services: Ear Cleaning with Infection',
       '0',
       '450',
       ''
UNION ALL
SELECT 'Veterianry Services: Urine Draining',
       '0',
       '1000',
       ''
UNION ALL
SELECT 'Veterinary Services: Abortion up to 10 kgs (Additional)',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Veterinary Services: Anesthesia/Sedation (10 to 15kgs)',
       '0',
       '2200',
       ''
UNION ALL
SELECT 'Veterinary Services: Anesthesia/Sedation (15 to 20 kgs)',
       '0',
       '2800',
       ''
UNION ALL
SELECT 'Veterinary Services: Anesthesia/Sedation (20 to 25 kgs)',
       '0',
       '3200',
       ''
UNION ALL
SELECT 'Veterinary Services: Anesthesia/Sedation (5 to 10 kgs))',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Veterinary Services: Anesthesia/Sedation (up to 5kgs)',
       '0',
       '800',
       ''
UNION ALL
SELECT 'Veterinary Services: Cadaver Disposal Fee',
       '500',
       '1500',
       ''
UNION ALL
SELECT 'Veterinary Services: Consultation/Professional Fee',
       '0',
       '350',
       ''
UNION ALL
SELECT 'Veterinary Services: Electrocautery',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Veterinary Services: Emergency Fee',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Veterinary Services: Emesis (Induction) 1-5 kgs',
       '20',
       '1000',
       ''
UNION ALL
SELECT 'Veterinary Services: Emesis (Induction) 11-15 kgs',
       '0',
       '2000',
       ''
UNION ALL
SELECT 'Veterinary Services: Emesis (Induction) 5 to 10 kgs',
       '0',
       '1500',
       ''
UNION ALL
SELECT 'Veterinary Services: Enema',
       '0',
       '800',
       ''
UNION ALL
SELECT 'Veterinary Services: Euthanasia',
       '0',
       '2500',
       ''
UNION ALL
SELECT 'Veterinary Services: House Call/Home Service within 4 kms',
       '0',
       '500',
       ''
UNION ALL
SELECT 'Veterinary Services: Incubator Use / hr',
       '0',
       '80',
       ''
UNION ALL
SELECT 'Veterinary Services: Microchip Installation',
       '0',
       '550',
       ''
UNION ALL
SELECT 'Veterinary Services: Urinary Catheter Installation',
       '0',
       '600',
       ''
UNION ALL
SELECT 'Veterinary Services: Veterinary Health Certificate',
       '0',
       '400',
       ''
UNION ALL
SELECT 'Veterinary Services: Whelp Assist (Per Puppy Delivered)',
       '0',
       '250',
       ''
UNION ALL
SELECT 'Veterinary Services: Whelping Assistance 1-5kgs',
       '0',
       '2000',
       ''
UNION ALL
SELECT 'Veterinary Services: Whelping Assistance 11-15kgs',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Veterinary Services: Whelping Assistance 16-20 kgs',
       '0',
       '5000',
       ''
UNION ALL
SELECT 'Veterinary Services: Whelping Assistance 6-10kgs',
       '0',
       '3000',
       ''
UNION ALL
SELECT 'Veterinary Services: Wound Cleaning 1-2 inches',
       '0',
       '300',
       ''
UNION ALL
SELECT 'Dental Prophylaxis 1-5kgs',
       '0',
       '3000',
       ''
UNION ALL
SELECT 'Dental Prophylaxis 11-15 kgs',
       '0',
       '5000',
       ''
UNION ALL
SELECT 'Dental Prophylaxis 15-20 kgs',
       '0',
       '6000',
       ''
UNION ALL
SELECT 'Dental Prophylaxis 6-10 kgs',
       '0',
       '4000',
       ''
UNION ALL
SELECT 'Deworming (1-5kgs)',
       '16',
       '200',
       ''
UNION ALL
SELECT 'Deworming (11-15kgs)',
       '48',
       '400',
       ''
UNION ALL
SELECT 'Deworming (16-20kgs)',
       '65',
       '600',
       ''
UNION ALL
SELECT 'Deworming (6-10kgs)',
       '32',
       '250',
       ''
UNION ALL
SELECT 'Deworming 21 to 25 kgs',
       '0',
       '750',
       ''

GO 
