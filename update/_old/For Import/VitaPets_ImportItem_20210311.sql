
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VitaVet_ImportItem_20210311'']') AND type in (N'U'))
DROP TABLE [dbo].[VitaVet_ImportItem_20210311']
GO

CREATE TABLE [dbo].[VitaVet_ImportItem_20210311'](
	[Name of Medicine ] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[SRP] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nutriblend Gel', N'Drugs/Medicine', 500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC-Vit 60ml', N'Drugs/Medicine', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC-Vit 120ml', N'Drugs/Medicine', 200)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC-Dox 60ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Hemacare 60ml', N'Drugs/Medicine', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nematocide 15ml', N'Drugs/Medicine', 165)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC Scour 60ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Livotine 120ml', N'Drugs/Medicine', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Coatshine 60ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nutrical 60ml', N'Drugs/Medicine', 165)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nematocide 60ml', N'Drugs/Medicine', 450)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nacalvit-C 120ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Barfy Multivitamins 120ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Barfy Multivitamins 60ml', N'Drugs/Medicine', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Broncure 60ml', N'Drugs/Medicine', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Volapets 60ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ener-G 60ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Livwell 60ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Scour 60ml', N'Drugs/Medicine', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Doxy 60ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Enmalac 120ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Iron 120ml', N'Drugs/Medicine', 240)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi OB ', N'Drugs/Medicine', 220)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Renal Care 120ml', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Drontal', N'Drugs/Medicine', 550)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'VCO Hofmann 100ml', N'Drugs/Medicine', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Liverator', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pyranex', N'Drugs/Medicine', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Petreboost', N'Drugs/Medicine', 300)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pet C syrup 60ml', N'Drugs/Medicine', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Sorvit 120ml', N'Drugs/Medicine', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Metronidazole 125mg/5ml', N'Drugs/Medicine', 65)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Co-amoxiclav 125mg/60ml', N'Drugs/Medicine', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cefalexin 125mg/60ml', N'Drugs/Medicine', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Metoclopramide 5mg/60ml', N'Drugs/Medicine', 80)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Novacee 120ml', N'Drugs/Medicine', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nutriplus gel', N'Drugs/Medicine', 530)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cotrimoxazole 240mg/5ml', N'Drugs/Medicine', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Colimoxyyn 60ml', N'Drugs/Medicine', 260)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC dox 120ml', N'Drugs/Medicine', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Methiovet', N'Drugs/Medicine', 20)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC Dox', N'Drugs/Medicine', 25)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'LC Vit OB', N'Drugs/Medicine', 380)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Enrofloxacin', N'Drugs/Medicine', 20)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nexgard 25-50kg Red', N'Drugs/Medicine', 680)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nexgard 4-10kg Blue', N'Drugs/Medicine', 570)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nexgard up to 4kgs Orange', N'Drugs/Medicine', 520)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nexgard 10-25kg Purple', N'Drugs/Medicine', 650)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Prazilex Dewormer', N'Drugs/Medicine', 50)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Petcure Doxycycline', N'Drugs/Medicine', 20)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Freximol 1 bot', N'Drugs/Medicine', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Neurovet 1 bot', N'Drugs/Medicine', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Itraconazole 100mg', N'Drugs/Medicine', 40)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Metronidazole 500mg', N'Drugs/Medicine', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cetirizine 10mg', N'Drugs/Medicine', 10)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Tramadol 50mg', N'Drugs/Medicine', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Omeprazole 20mg', N'Drugs/Medicine', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Prednisone 5mg', N'Drugs/Medicine', 10)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Metoclopramide 10mg', N'Drugs/Medicine', 10)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Brewer''s Yeast per tab', N'Drugs/Medicine', 3)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Brewer''s Yeast 1 bot', N'Drugs/Medicine', 850)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'wormrid', N'Drugs/Medicine', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Amoxicillin K9 Amox', N'Drugs/Medicine', 10)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Co-amoxiclav 625mg', N'Drugs/Medicine', 40)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cetyl-M', N'Drugs/Medicine', 25)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'bravecto 2-4.5kg', N'Drugs/Medicine', 1300)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'bravecto 4.5-10kg', N'Drugs/Medicine', 1400)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'bravecto 10-20kg', N'Drugs/Medicine', 1500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'bravecto 20-50kg', N'Drugs/Medicine', 1500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Otiderm 15ml', N'Medical Supply', 450)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Papi Powder', N'Medical Supply', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Dextrose Powder 100g', N'Medical Supply', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Dextrose Powder 300g', N'Medical Supply', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Advocate Cats up to 4kg', N'Medical Supply', 500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Advocate Cats up to 4kg', N'Medical Supply', 500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Topicure', N'Medical Supply', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Mondex 100g', N'Medical Supply', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Advocate Cats up to 4kg', N'Medical Supply', 500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Jojoba Ear Cleanser liquid', N'Medical Supply', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'VS Flea & Tick Defense collar M', N'Medical Supply', 600)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'VS Flea & Tick Defense collar L', N'Medical Supply', 700)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'As k9 eye drops', N'Medical Supply', 450)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Easotic', N'Medical Supply', 850)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Vanguard Food Supplement', N'Medical Supply', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Organic madre de cacao ointment', N'Medical Supply', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Terminator 3ml', N'Medical Supply', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Terminator 5ml', N'Medical Supply', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Terminator 7ml', N'Medical Supply', 200)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Madrecure ointment', N'Medical Supply', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Madrecure cologne', N'Medical Supply', 90)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pyramid Hill Lamb & Beef', N'Pet Foods', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Hofmann Beef w/ Linseed dog', N'Pet Foods', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Hofmann Beef Pate for dogs', N'Pet Foods', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Hofmann Beef w/ linseed cats', N'Pet Foods', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Bosch Puppy Milk Replacer 200g ', N'Pet Foods', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Hofmann chicken w/ salmon oil cats', N'Pet Foods', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pedigree puppy chunks 130g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pedigree beef chunks 130g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'pedigree chicken chunks', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pedigree 5 kinds 400g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'whiskas tuna 85g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'pedigree smokey beef 80g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Magnus biscuts 400g', N'Pet Foods', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Empress Cat litter  5L', N'Others', 280)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Empress Cat litter  10L', N'Others', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Best Clean 10L', N'Others', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Biocatolet 12L', N'Others', 500)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'feeding bowl plastic Agrimex', N'Others', 40)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cat litter box S', N'Others', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cat litter box M', N'Others', NULL)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Frog bowl stainless S', N'Others', 140)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'frog bowl stainless M', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Double bowl w/ stainless', N'Others', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'stainless bowl S', N'Others', 70)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'stainless bowl M', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'stainless bowl L', N'Others', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'stainless bowl XL', N'Others', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'food/water dispenser ', N'Others', 400)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'plastic matting black', N'Others', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Plastic matting blue', N'Others', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pull-out tray  L', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pull-out tray  M', N'Others', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Pull-out tray  S', N'Others', 130)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'collapsible cage S', N'Others', 1100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'collapsible cage M', N'Others', 1400)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'collapsible cage L', N'Others', 1600)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'collapsible cage XL', N'Others', 1800)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 7', N'Others', 70)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 6', N'Others', 80)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 5', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 4', N'Others', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 3', N'Others', 140)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 2', N'Others', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Ecollar 1', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Diaper XS', N'Others', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Diaper S', N'Others', 25)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Diaper M', N'Others', 30)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Diaper L', N'Others', 35)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cat toy 3pcs. rope mouse with feather', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cat toy 6pcs. transparent ball', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cat Toy 2pcs. Tennis ball', N'Others', 90)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Female Diaper S', N'Others', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Female Diaper L', N'Others', 25)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Male Diaper S', N'Accessories', 15)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Dog Sock S', N'Accessories', 50)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Dental Care Set Oral Care toothpaste Beef', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'I-Clicker With Whistle', N'Others', 90)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Yaho Cat Snack Pack Mixed', N'Pet Foods', 65)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Round Handle Chain 2.0', N'Accessories', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Round Handle Chain 2.5', N'Accessories', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Round Handle Chain 3.0', N'Accessories', 170)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Round Handle Chain 3.5', N'Accessories', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'H/L Assorted Design 2.0', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Boley Water Feeder', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Darling Pet Soap All Breeds', N'Detergents', 50)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'H/L  Printed Color 2.0', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'H/L  Printed Color 2.5', N'Others', 230)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Dr Pet Toothpaste Flavour Peanut', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Plastic Bussal #1', N'Others', 60)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Plastic Bussal #4', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Leash Round 2 Way Plain', N'Others', 200)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Padded Handle Big Chain 4.0x1.3M', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Padded Collar Plain 2.0', N'Others', 80)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Padded Collar Plain 2.5', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Bioline Toothpaste Single Mint Beef 100g', N'Pet Foods', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'3pcs. Mini Tennis Ball', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cable Leash 120cm', N'Others', 200)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Cable Leash 7.5 120cm', N'Others', 350)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'I LOve Pets Slicker Brush M', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Oral Care Toothpaste Grapes', N'Others', 50)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Squeaky Toy Big Mac', N'Others', 60)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Nursing kit Plain', N'Clinic Use', 90)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Poley Slicker Brush Pink & Blue (M)', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Stainless Bowl Plain (M)', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Stainless Bowl Plain (L)', N'Others', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Stainless Bowl Plain (XL)', N'Others', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #1', N'Others', 180)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #2', N'Others', 160)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #3', N'Others', 140)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #4', N'Others', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #5', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #6', N'Others', 80)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'E-collar Transparent #7', N'Others', 70)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Harness K9 (M)', N'Others', 400)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Harness K9 (L)', N'Others', 450)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'H/L Anchor 1.0', N'Others', 100)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'H/L Anchor 1.5', N'Others', 120)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'C/L Army Assorted Design With Reflector 1.5 ', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'C/L Army Reflector 1.5', N'Others', 150)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'C/L Army Reflector 2.0', N'Others', 220)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'C/L Army Reflector 2.5', N'Others', 280)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Padded Chain 6.0x120m', N'Others', 250)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Padded Chain 10x120m', N'Others', 450)
GO
INSERT [dbo].[VitaVet_ImportItem_20210311'] ([Name of Medicine ], [Category], [SRP]) VALUES (N'Bioline Toothpaste Single Mint 100g', N'Others', 150)
GO

Declare @VitaPet_ID_COmpany INT  = 47
Declare @Record Table (ItemName Varchar(MAX), ID_ItemCategory INT, UnitPrice Decimal)

INSERT @Record
SELECT  LTRIM(RTRIM(record.[Name of Medicine ]))
	   ,MAX(ic.ID)
	   ,record.SRP
FROM    [VitaVet_ImportItem_20210311'] record LEFT JOIN tItemCategory ic on record.Category = ic.Name 
GROUP BY 
	LTRIM(RTRIM(record.[Name of Medicine ]))
   ,record.SRP
ORDER BY 
	 LTRIM(RTRIM(record.[Name of Medicine ]))

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  CurrentInventoryCount,
  Old_item_id)
SELECT 
   @VitaPet_ID_COmpany
  ,LTRIM(RTRIM(ItemName)) 
  ,CONVERT(Decimal, UnitPrice)
  ,2
  , NULL
  , ID_ItemCategory
  , NULL
  , 1
  , 'Imported on 2021-03-11 07pm'
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , 0
  , NULL
FROM    @Record ORDER BY ItemName

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VitaVet_ImportItem_20210311'']') AND type in (N'U'))
DROP TABLE [dbo].[VitaVet_ImportItem_20210311']
GO
