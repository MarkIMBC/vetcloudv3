IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportClientPatiens_Petstacular_20210228]') AND type in (N'U'))
DROP TABLE [dbo].[ImportClientPatiens_Petstacular_20210228]
GO

CREATE TABLE [dbo].[ImportClientPatiens_Petstacular_20210228](
	[Client ID] [float] NULL,
	[Owner's Name] [nvarchar](255) NULL,
	[Pet's Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Contact Number(s)] [nvarchar](255) NULL,
	[Email Address] [nvarchar](255) NULL,
	[Best time to call] [nvarchar](255) NULL,
	[RowIndex] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170001, N'Armando Gomez', N'Melon / Peach', N'Abogado, Paniqui, Tarlac', N'(0999)8890234', NULL, NULL, 1)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170002, N'Victor Cadiente', N'Zoe', N'Bacao, Paniqui, Tarlac', N'(0906)3831066', NULL, NULL, 2)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170003, N'Charlie Mercado', N'Ashly / Summer', N'Poblacion Norte, Paniqui, Tarlac', N'(0905)8384851', NULL, NULL, 3)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170004, N'Marivi de Aquino ', N'Trixie/Princess/ Alden ', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'913-2360', NULL, NULL, 4)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170005, N'Marissa De Gracia', N'Sassy /Choco /Chesnut', N'Quezon St. Tablang, Paniqui, Tarlac', N'(0977)8214922', NULL, NULL, 5)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170006, N'Cezar Techico', N'Baldo', N'MH Del Pilar, Pob. Norte, Paniqui, Tarlac', N'(0910)7858807', NULL, NULL, 6)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170007, N'Emily Maguire', N'Koko', N'Tara Home, Linsao Subd, Brgy. Abogado, Paniqui, Tarlac', N'(045)6066721/(0945)7291548', NULL, NULL, 7)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170008, N'Mary Anne Naces', N'Baxter', N'Magaspac, Gerona, Tarlac', N'(0905)3524126', NULL, NULL, 8)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170009, N'Stacey Caoile', N'Kakashi', N'San Vicente, San Manuel, Tarlac', N'9107015262', NULL, NULL, 9)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170010, N'Danalyn M. Tolentino', N'Pray', N'Brgy. Oloybuaya, Gerona, Tarlac', N'(0915)6588363', NULL, NULL, 10)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170011, N'Janne P. Roxas', N'Ashley', N'Bugallon St.,Estacion, Paniqui, Tarlac', N'(0905)5455128', NULL, NULL, 11)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170012, N'Marla Visaya', N'Aki', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0905)4378522', NULL, NULL, 12)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170013, N'Ma. Julie Hernandez', N'Patie', N'Estacion, Paniqui, Tarlac', N'(0926)4697935', NULL, NULL, 13)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170014, N'Jimmer Yandan', N'Luckie', N'Poblacion Norte, Paniqui, Tarlac', N'(0909)2093616', NULL, NULL, 14)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170015, N'Brenda Hao', N'Shawi', N'Sta. Lucia East, Moncada, Tarlac', NULL, NULL, NULL, 15)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170016, N'Melany Bigornia', N'Paige', N'Brgy. Canan, Paniqui, Tarlac', N'(0995)9350777', NULL, NULL, 16)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170017, N'Jeny Anne Madamba', NULL, N'Poblacion Norte, Paniqui, Tarlac', N'(0917)5077886 / 931-0129', NULL, NULL, 17)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170018, N'Maribel Obcena', N'Slerpy', N'Coral, Paniqui Tarlac', N'(045)9256909/(0921)3615229', NULL, NULL, 18)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170019, N'Catherine Abagat', N'Yoongi', N'Salomague, Paniqui, Tarlac', N'9177087412', N'cc.abagat@gmail.com', NULL, 19)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170020, N'Liza Linsao', N'Cocoy / Macoy', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0925)3102156 / 493-0195', NULL, NULL, 20)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170021, N'Ramon Simon', N'Digong / Rudrigo / Duterte', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 21)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170022, N'Ariel Borja', N'Chocollato / Nova / Naki / Kina', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 22)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170023, N'Imelda U. Salon', N'Chelsea', N'Sta. Lucia East, Moncada, Tarlac', N'(0926)7026766', NULL, NULL, 23)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170024, N'Josie Esteban', N'Poche', N'Sta. Maria, Moncada, Tarlac', N'(0916)6481379', NULL, NULL, 24)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170025, N'Jessie Jhon Piayo', N'Dove', N'Nipaco, Paniqui, Tarlac', N'(0926)5943797', NULL, NULL, 25)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170026, N'Norman Ventura', N'Kendall', N'424 Pasuquin St., Paniqui, Tarlac', N'(0916)1582485', N'aleckzieness@gmail.com', NULL, 26)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170027, N'Rowena C. Eugenio', N'Chi-Chi', N'Pob 3, Pura, Tarlac', N'(0907)1904677', N'wenaeugenio22@gmail.com', NULL, 27)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170028, N'Jenny Domingo', N'Krunch', N'Cuyapo, Nueva Ecija', N'9155212928', NULL, NULL, 28)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170029, N'Jane A. Culipapa', N'Sydney / Butchokoy', N'Lacayanga Subd., Paniqui, Tarlac', N'(0908)4743769', NULL, NULL, 29)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170030, N'Cristina San Valentine', N'Crystal', N'260 Carino, Paniqui, Tarlac', N'(0929)5780586', NULL, NULL, 30)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170031, N'Krisaint Godlyne B. Portin', N'Chow Bella', N'Abogado, Paniqui, Tarlac', N'(0921)3154672', N'ween_azmira@yahoo.com', NULL, 31)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170032, N'Aldous Doctolero', N'Bambie', N'Brgy. Cayanga, Paniqui, Tarlac', N'(0950)2758755', NULL, NULL, 32)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170033, N'MJ Laxamana', N'Happi', N'Samput, Paniqui, Tarlac', N'(0909)4591363', NULL, NULL, 33)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170034, N'Mark Dasin', N'Tin Tin / Bulldog', N'Canan, Paniqui, Tarlac', N'(0915)1137463', NULL, NULL, 34)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170035, N'Jaycar Mayers', N'Princess', N'410A Villa Nadine Subd., Paniqui, Tarlac', N'(0905)4122223', N'marc.mercado1996@gmail.com', NULL, 35)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170036, N'Fely Ongsico', N'Boninay', N'464 Brgy. Abogado, Paniqui, Tarlac', N'(0918)6833469', NULL, NULL, 36)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170037, N'Ana Duenas', N'Panda / Bolt', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0949)4111296', N'anagracielladuenas@gmail.com', NULL, 37)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170038, N'Rolliz Lambino', N'Amigo', N'Canan, Paniqui, Tarlac', N'(0946)4521663', NULL, NULL, 38)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170039, N'Lorena Lomboy', N'Mutya', N'MH Del Pilar, Pob. Norte, Paniqui, Tarlac', N'(0920)3393156', NULL, NULL, 39)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170040, N'Sheerween Cacanando', N'Georgina', N'14 Sta. Rosa St., Brgy. Poblacion Sur, Paniqui, Tarlac', N'(0917)5758403', N's.e.cancanando@gmail.com', NULL, 40)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170041, N'Jennifer C. Padapat', N'Bambam / Prince', N'Purok Maligaya, Brgy. Abogado, Lavitoria Subd., Paniqui, Tarlac', N'(0916)2135332', NULL, NULL, 41)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170042, N'Janrex Galatierra', N'Neggy', N'238 Cabayaoasan, Paniqui, Tarlac', N'(0946)5205408', N'janrex_avila123@yahoo.com', NULL, 42)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170043, N'Kriskim Q. Kim', N'Mudan', N'Angeles, Pampanga', N'(0921)8179078', NULL, NULL, 43)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170044, N'Jan Patrick E. Tabunda', N'Cloud', N'Gomez St., Brgy. Polacion Sur, Paniqui, Tarlac', NULL, NULL, NULL, 44)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170045, N'Ma. Mica Shaine A. Mariano', N'Boom-Boom', N'Cojuanco St., YC, Brgy. Abugado, Paniqui, Tarlac', N'(0966)7617252', NULL, NULL, 45)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170046, N'Mark Aiem Pasion', NULL, N'Brgy. Salomague, Paniqui, Tarlac', N'(0905)7325650', NULL, NULL, 46)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170047, N'Girlie S. David', N'Zoey', N'Brgy. Dapdap, Paniqui, Tarlac', N'(0948)3741617', NULL, NULL, 47)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170048, N'Andrie Turato', N'Penny', N'Gerona, Tarlac', N'(0945)3196755', NULL, NULL, 48)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170049, N'Lolita Punzalan', N'Cheelah / Chooloo', N'Brgy. Abogado, Paniqui, Tarlac', N'(0920)6280570', NULL, NULL, 49)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170050, N'Charie Macadangdang', N'Nico / Nica', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0995)4504439', NULL, NULL, 50)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170051, N'Mario E. Santos', N'Trixie', N'63 Palarca St., Paniqui, Tarlac', N'(045)9311654', NULL, NULL, 51)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170052, N'Francis Albert Cardona', N'Blue', N'Sta. Rosa St., Paniqui, Tarlac', N'(0905)3601264 / (045)4932718', N'albert.cardona.1985@gmail.com', NULL, 52)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170053, N'Orlan Montallana', N'Bonzai', N'Apulid, Paniqui, Tarlac', N'(0947)6408969', NULL, NULL, 53)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170054, N'Marcelina C. Mariano', N'Pandak', N'Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0930)2357597', NULL, NULL, 54)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170055, N'May Belle R. Balaba', N'Twilight', N'Canan, Baraka, Paniqui, Tarlac', N'(0942)3571913', N'maybellebalaba@gmail.com', NULL, 55)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170056, N'Maggie Galeon', N'Likolet', N'Public Market, Paniqui, Tarlac', NULL, NULL, NULL, 56)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170057, N'Roma Ylarde', N' Wacks', N'93 Luna St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0910)4126300', NULL, NULL, 57)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170058, N'Alona Albarracin', N'Olivia', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0998)2461819', NULL, NULL, 58)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170059, N'Laurentino Agres', N'Lobo', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0946)7624242', NULL, NULL, 59)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170060, N'Angeline Ofrecio', N'Mochi', N'Anao, Tarlac', N'(0927)9418559', N'adofrecio@gmail.com', NULL, 60)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170061, N'Jovita Dean', N'Missy / Chico / Raffy', N'Sta. Maria, Moncada, Tarlac', N'(0906)4811662', NULL, NULL, 61)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170062, N'Amel Jane Guevarra', N'Staicy', N'Rang- Ayan, Paniqui, Tarlac', N'(0997)2932188', NULL, NULL, 62)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170063, N'Marlo Miguel', N'Yondu', N'464 Estacion, Paniqui, Tarlac', N'(0917)5146676', NULL, NULL, 63)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170064, N'Christine Grace Cabucana', N'Xander', N'Abogado, Paniqui, Tarlac', N'(0995)4634627', NULL, NULL, 64)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170065, N'Shiela Mae B. Rombaoa', N'Muffin', N'Ventinilla, Paniqui, Tarlac', N'(0910)0841679', NULL, NULL, 65)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170066, N'Princess Tena', N'Cham-Cham/Luigi', N'Samput, Paniqui, Tarlac', N'(0950)2400861', NULL, NULL, 66)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170067, N'Rosaly B. Reyes', N'Dubra', N'Poblacion North, Ramos, Tarlac', N'(0916)9435701', NULL, NULL, 67)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170068, N'Venus Parazo', N'Puddy', N'Abagon, Gerona, Tarlac', N'(0927)7851883', NULL, NULL, 68)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170069, N'Kristine Joy Lucas', N'Hatchico', N'Canan, Paniqui, Tarlac', N'(0930)2692977', NULL, NULL, 69)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170070, N'Jea Balaoing', N'Luna', N'705 Caburnay St., Paniqui, Tarlac', N'(0947)5323460', NULL, NULL, 70)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170071, N'Ma. Christina Grande', N'Fairy', N'San Jose South, Anao, Tarlac', N'9502799566', NULL, NULL, 71)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170072, N'Cherry Jane Gacutan', N'Cleo', N'Poblacion 1, Gerona, Tarlac', N'(045)4915341 / (0920)9594084', NULL, NULL, 72)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170073, N'Loida B. Corpuz', N'Vanilla', N'144 M. H. Del Pilar St., Paniqui, Tarlac', N'(045)4708483 / (0915)3791013', NULL, NULL, 73)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170074, N'Ma. Cristina Roxas', N'Aura / Ginger', N'Bugallon St.,Estacion, Paniqui, Tarlac', N'(0977)0993000', NULL, NULL, 74)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170075, N'Sharysse Pearl J. Acosta', N'Chuck', N'0872 Purok Maligaya, Paniqui, Tarlac', N'(0906)5008015', N'sharysseacosta@gmail.com', NULL, 75)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170076, N'Fernando N. Madamba', N'Misty', N'Cayanga, Paniqui', N'(0928)2834823', NULL, NULL, 76)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170077, N'Allan Yebes', N'max', N'Manaois, Paniqui, Tarlac', N'(045)4915713 / (0920)5427816', NULL, NULL, 77)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170078, N'Keh Lee Lacayanga', N'Furby', N'RA Fernandez Square Bldg. H, Baltazar, Paniqui, Tarlac', N'(0921)2769434', N'kehleelacayanga.klgl@gmail.com', NULL, 78)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170079, N'Rosalinda Calma', N'Digong', N'Tangonan St., Cariño, Paniqui, Tarlac', N'(0975)8146717', NULL, NULL, 79)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170080, N'Leonora F. Domingo', N'Herculez', N'225 Canan, Paniqui, Tarlac', N'(045)9310671', NULL, NULL, 80)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170081, N'Kurt Adrian Galatierra', N'Toffee', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0926)0114070', NULL, NULL, 81)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170082, N'Remedios Gruspe', N'Mojo', N'94 Luna St., Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0919)3011194', NULL, NULL, 82)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170083, N'Mylette Arce', N'Potchi', N'Tablang, Paniqui, Tarlac', N'(045)3260524', NULL, NULL, 83)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170084, N'Ana Balaba', N'Panda / Letbu', N'Abugado, Paniqui, Tarlac', N'(0939)9184950', NULL, NULL, 84)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170085, N'Garry Laguna', N'Alfa / Lucas', N'San Julian, Moncada, Tarlac', N'(0915)5644434', NULL, NULL, 85)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170086, N'Lourdes Valix', N'Furriam', N'203 Carino, Paniqui, Tarlac', N'(0949)9954548', NULL, NULL, 86)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170087, N'Jose Basco', N'Tsuri', N'Purok Aksiyon, Coral, Ramos, Tarlac', N'(0920)3057240', NULL, NULL, 87)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170088, N'Analyn Bumanglag', N'Brix', N'Abagon, Gerona, Tarlac', N'(0948)1736940', NULL, NULL, 88)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170089, N'Margie Constante', N'Yong-Yong', N'San Julian, Moncada, Tarlac', N'(0921)6346483', NULL, NULL, 89)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170090, N'Paulo C. Roxas', N'Psyche', N'Estacion, Paniqui, Tarlac', N'(0977)4654423', NULL, NULL, 90)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170091, N'Michael Alan B. Marquez', N'Juno / Jona', N'DFL Valle, Estacion, Paniqui, Tarlac', N'(0907)2206411', NULL, NULL, 91)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170092, N'Alfred Nacpil', N'Jayce', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0977)1788200', N'eunicecabidoy97@gmail.com', NULL, 92)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170093, N'Eugene Cabidoy', N'Sammy', N'Santa Rosa, Poblacion Sur, Paniqui, Tarlac', N'(0927)6673048', NULL, NULL, 93)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170094, N'Romina Olonan', N'Mojako', N'Matalapitap, Paniqui, Tarlac', N'(0939)9213186', NULL, NULL, 94)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170095, N'Benedict Dioquino', N'Tuffy', N'Samput, Paniqui, Tarlac', N'(0948)0108888', NULL, NULL, 95)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170096, N'Lara Villahermoso', N'Sipol', N'367 Samput, Paniqui, Tarlac', N'(0947)1871922', NULL, NULL, 96)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170097, N'Pastor Dagman', N'Patchie', N'174 Del Valle St., Estacion, Paniqui, Tarlac', N'(0977)4101895/(0926)0318535', NULL, NULL, 97)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170098, N'Eduardo / Marieta Supnet', N'Buddy / Pao-Pao / Sweden / Scarlet', N'110 Matalapitap, Paniqui, Tarlac', N'(0927)2529526', NULL, NULL, 98)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170099, N'Arnin Kaye V. Calpito', N'Inuh', N'Sta. Ines, Paniqui, Tarlac', N'(045)9255664 / (0999)4895905', NULL, NULL, 99)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170100, N'Shellah Gragasin', N'Oreo', N'Pasuquin St., Paniqui, Tarlac', N'(045)9312791 / (0910)5786654', NULL, NULL, 100)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170101, N'Che Mallorca', N'Maui / Moana', N'Moncada, Tarlac', N'(0918)6460052', NULL, NULL, 101)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170102, N'Blaine M. Dela Cruz', NULL, N'Magaspac, Gerona, Tarlac', N'(045)4915723 / (0905)5778937', NULL, NULL, 102)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170103, N'Jefer C. Piedad', N'Hunter', N'Samput, Paniqui, Tarlac', N'(0928)7379856', NULL, NULL, 103)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170104, N'Katrina Rose Anne Daileg', N'Liley', N'Guiteb, Ramos, Tarlac', N'(0927)9795418', N'katrinaroseanne@yahoo.com', NULL, 104)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170105, N'Jesusa Cruz', N'Snow', N'957 Ramos Subd, Paniqui, Tarlac', N'(0915)6713631', NULL, NULL, 105)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170106, N'Marvi De Guia', N'Cubeir', N'Poblacion Norte, Paniqui, Tarlac', N'(0916)3577914', NULL, NULL, 106)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170107, N'Imelda David', N'Popoy', N'Poblacion Norte, Paniqui, Tarlac', N'(0910)0352549', NULL, NULL, 107)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170108, N'Kimberly Cardenas', N'Toby', N'San Julian, Moncada, Tarlac', N'(0907)3744133', NULL, NULL, 108)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170109, N'Ernesto Patajo', N'John Lloyd / Barrac', N'389 Estacion, Paniqui, Tarlac', N'(0977)6786951', NULL, NULL, 109)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170110, N'Dra. Jesusa Tanedo', N'Popoy', N'4 Baltazar Subd., Paniqui, Tarlac', N'(045)9312088 / (0998)5437192', NULL, NULL, 110)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170111, N'Benson Villanueva', N'Indang', N'Aringin, Moncada Tarlac', N'(0919)6521700', N'benson31081@yahoo.com ', NULL, 111)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170112, N'Thannia Mae Sarmiento', N'Toothless', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0926)3634673', N'thanniamaes@gmail.com', NULL, 112)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170113, N'Ardel Steve Varias', N'Karl', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0936)0381595', N'stevevarias@gmail.com', NULL, 113)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170114, N'Pauline P. Soriano', N'Kiefer', N'Calle Abo, Estacion, Paniqui, Tarlac', N'(0915)8802007', N'paulinep.soriano@gmail.com', NULL, 114)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170115, N'Jellica May M. Aguilar', N'Prince Choco', N'Sta. Rosa St., Paniqui, Tarlac', N'(0917)8492209', NULL, NULL, 115)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170116, N'Diana Rose L. Mico', N'Putot', N'45 Abogado, Paniqui, Tarlac', N'(045)9312597 / (0956)6256940', NULL, NULL, 116)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170117, N'Milleon Vistan', N'Bacon', N'203 Tablang, Paniqui, Tarlac', N'(045)6282280 / (0915)2578707', NULL, NULL, 117)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170118, N'Celia Alomia', N'Choco', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0997)9319793', NULL, NULL, 118)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170119, N'Jesus Agabao', N'Primo / Roche', N'Tampog, Bayambang, Pangasinan', N'(0908)6799123', NULL, NULL, 119)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170120, N'Princess Mae M. Millado', N'Shamskie', N'Magaspac, Gerona, Tarlac', N'(0927)6653919', NULL, NULL, 120)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170121, N'Khim A. Viray', N'Pochoychoy', N'Poblacion Center, Ramos, Tarlac', N'(0906)1742813', NULL, NULL, 121)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170122, N'Harold L. Teodoro', N'Miya', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0956)8068260', NULL, NULL, 122)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170123, N'Ramona Rayos', N'Kookie', N'Estacion, Paniqui, Tarlac', N'(045)9311828 / (0917)8158831', NULL, NULL, 123)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170124, N'Elise Jade Tagubuan-David', N'Endra', N'#76 Licor St. Poblacion Norte, Paniqui, Tarlac', N'4915290/(0977)1055014', NULL, NULL, 124)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170125, N'Shane Ann M. Gonzaga', N'Chu-yu', N'654 Caburnay St., Pob. Sur, Paniqui, Tarlac', N'(0926)9207863', NULL, NULL, 125)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170126, N'Anna May Lucas', N'Twix', N'Coral, Paniqui Tarlac', N'(045)4915787', NULL, NULL, 126)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170127, N'Marithe Jersey Pabro', N'Kyle', N'Pance, Ramos, Tarlac', N'9178280162', NULL, NULL, 127)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170128, N'Susie Palarca', N'Marco', N'Canan, Paniqui, Tarlac', N'9076669561', NULL, NULL, 128)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170129, N'Elvira Cachin', N'Xian', N'357 Brgy. Samput, Paniqui, Tarlac', N'(045)4912837', NULL, NULL, 129)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170130, N'Lizeth Alfaro', NULL, N'286 Estacion, Paniqui, Tarlac', N'(0946)9829683', NULL, NULL, 130)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170131, N'Reenzey Mae C. Ildefonso', N'Shortie', N'Pob. 1, Moncada, Tarlac', N'(0905)5015665', NULL, NULL, 131)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170132, N'Rondell Villas', N'Athena', N'Moncada, Tarlac', N'(0920)7319058', NULL, NULL, 132)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170133, N'Trixie Anne Karylle Cruz', N'Sky', N'Salumague, Paniqui, Tarlac', N'9173293724', NULL, NULL, 133)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170134, N'Editha A. Sumandre', N'Miles', N'Paniqui, Tarlac', N'(0936)4469124', NULL, NULL, 134)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170135, N'Judith Tuazon ', N'Zeus', N'Samput, Paniqui, Tarlac', N'9125088416', NULL, NULL, 135)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170136, N'Rowena Fuertes', N'Zoe / Coco', N'Baluyot Cmpd., Brgy. Carino, Paniqui, Tarlac', N'(045)9313162 / (0956)9168649', NULL, NULL, 136)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170137, N'Myla Sacramento', N'Candy', N'San Julian, Moncada, Tarlac', N'9511706916', NULL, NULL, 137)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170138, N'Nathaniel Gaspar', N'Max', N'Apulid, Paniqui, Tarlac', N'(0956)8534457', NULL, NULL, 138)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170139, N'Joel Nool', N'Max', N'San Julian, Moncada, Tarlac', N'9304190664', NULL, NULL, 139)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170140, N'Cathy De Guia', N'Lucia', N'Baltazar Subd, Paniqui, Tarlac', NULL, NULL, NULL, 140)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170141, N'Mylene P. Albalushi', N'Mic-Mic', N'Abogado, Paniqui, Tarlac', N'(0999)9222687', NULL, NULL, 141)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170142, N'Mayvellene De Aquino', N'Covee', N'Coral, Paniqui Tarlac', N'9954654556', NULL, NULL, 142)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170143, N'Joann Lopez', N'Rocky ', N'Apulid, Paniqui, Tarlac', N'9271209592', NULL, NULL, 143)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170144, N'John Lloyd Vallo', N'Bella', N'Carino, Paniqui, Tarlac', N'9615564459', NULL, NULL, 144)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170145, N'Alvin John Guzman', N'Matcha', N'Brgy. Tablang, Paniqui, Tarlac', N'(0917)6251735', NULL, NULL, 145)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170146, N'Michelle Dela Cruz', N'Copper', N'Baltazar Subd, Paniqui, Tarlac', N'9457784036', NULL, NULL, 146)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170147, N'Jhino Lorenzo', N'Grizzly', N'Abogado, Paniqui, Tarlac', N'9292585674', NULL, NULL, 147)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170148, N'Analyn Balmores', N'Kalbee', N'Guiteb, Ramos, Tarlac', N'9506567816', NULL, NULL, 148)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170149, N'Ladylyn Obcena', N'Snowball', N'Poblacion Norte, Paniqui, Tarlac', N'9453486378', NULL, NULL, 149)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170150, N'Irene V. Velasco', N'Bamboo', N'Sinigpit, Paniqui, Tarlac', N'(0921)7714466', NULL, NULL, 150)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170151, N'Jenny Claire N. Sapon', N'Sassy', N'San Julian, Moncada, Tarlac', N'(0917)6804410', N'jennyclaire27@yahoo.com', NULL, 151)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170152, N'Keane Edelbert B. Balingit', N'Urice / Chammy/ Bella / Blue / Bubbles', N'500 Samput, Paniqui, Tarlac', N'(0915)8336933', NULL, NULL, 152)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170153, N'Zarina Rebolledo', N'Hershey', N'693 Carino, Paniqui, Tarlac', N'(045)9312614 / (0929)8226123', NULL, NULL, 153)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170154, N'Edwin Glenn Romabaoa', N'Hershey', N'Salomague, Paniqui, Tarlac', N'9478926024', N'glenn.rombaoa@gmail.com', NULL, 154)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170155, N'Rodrigo D. Mariano Jr.', N'Macky', N'44 Ohio St., Apulid, Paniqui, Tarlac', N'(0906)4139268', NULL, NULL, 155)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170156, N'Francis Robin Papina', N'moona', N'Sitio Bulog Santa Maria, Moncada, Tarlac', N'(0906)3910667', NULL, NULL, 156)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170157, N'Gre-Loserence Ramos', N'Mocha', N'Colibangbang, Paniqui, Tarlac', N'(0906)3045452', NULL, NULL, 157)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170158, N'Walie M. Chua', N'Perkie', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(045)9250613 / (0947)4124343', NULL, NULL, 158)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170159, N'Renerose Acosta', N'Kuku', N'209 Carapdapan St., Paniqui, Tarlac', N'(0905)3972598', NULL, NULL, 159)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170160, N'Alex Melchor', N'Cosmo', N'Carino, Paniqui, Tarlac', N'(045)923-1061 / (0945)7285275', NULL, NULL, 160)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170161, N'Lawrence Dela Cruz', N'Chopper', N'Nampicuan, Nueva Ecija', N'(0906)4025166', NULL, NULL, 161)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170162, N'Rosalie M. Dabe', N'Kobe', N'Purok Uno, Samput, Paniqui, Tarlac', N'(0999)9041633', NULL, NULL, 162)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170163, N'Marsha Magtalas', N'Potpot', N'209 Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'(045)3260583 / (0921)2521547', NULL, NULL, 163)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170164, N'Justin Jay Bulatao', N'Villi', N'90 Salomague, Paniqui, Tarlac', N'(0947)0979776', NULL, NULL, 164)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170165, N'John Paul Chu', N'Biggy / Brutus', N'Patalan, Paniqui, Tarlac', N'(0995)9747235', NULL, NULL, 165)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170166, N'Rhea Melinda L. Cancio', N'Woori / Ghost', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'(045)9313267 / (0977)3337301', N'rhengg@gmail.com', NULL, 166)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170167, N'Joe Mhar Buenavista', N'Namuro', N'Aringin, Moncada Tarlac', N'9618356669', NULL, NULL, 167)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170168, N'Kayla Jane Tawatao', N'Chanel', N'38 M. H. Del Pilar St., Estacion, Paniqui, Tarlac', N'(0955)7092588', N'kaylajane57@gmail.com', NULL, 168)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170169, N'Merlita Ibarra', N'Jack', N'431 Poblacion 1, Gerona, Tarlac', N'(045)4915385 / (0917)5145486', NULL, NULL, 169)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170170, N'Analiza L. Castillo', N'Rocky / Suzy', N'112-B Villa Nadine Subd., Acocolao, Paniqui, Tarlac', N'(0928)6229600', NULL, NULL, 170)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170171, N'Maria Yanga', N'Skye', N'850 Purok Maligaya, Poblacion Norte, Paniqui, Tarlac', N'(045)4910026 / (0909)6733601', NULL, NULL, 171)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170172, N'Carina Ragual', N'Molly', N'211-B Villa Nadine, Acocolao, Paniqui, Tarlac', N'(0918)5044895', NULL, NULL, 172)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170173, N'Harvey Neil Santos', N'Chippie', N'116 Abogado, Paniqui, Tarlac', N'(0910)1404370', NULL, NULL, 173)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170174, N'Phaebe Cris Roman', N'Lucio', N'Sitio Pisong, Pinasling, Gerona, Tarlac', N'(0977)7040876', NULL, NULL, 174)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170175, N'Neil Santomin', NULL, N'Poblacion Sur, Paniqui, Tarlac', N'(0917)5080458', NULL, NULL, 175)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170176, N'Jazzmin Sherry R. Baluyot', N'Peppa', N'Canan, Paniqui, Tarlac', N'(0908)9722396', NULL, NULL, 176)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170177, N'Zandra V. Teoxon', N'Zoe / Panda', N'Sitio Maligaya, Licor, Poblacion Norte, Paniqui, Tarlac', NULL, NULL, NULL, 177)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170178, N'Winnie Ann V. Madayag', N'Chip', N'Calamay, Moncada, Tarlac', N'(0929)7060085', NULL, NULL, 178)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170179, N'William G. Honrado', N'Jaja / Minmin / Yanyan', N'Padapada, Gerona', N'(0919)7562200', NULL, NULL, 179)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170180, N'Jay Isidro Ferrer', N'Line / Bennet', N'68 Luna St., Brgy. Poblacion Norte, Paniqui, Tarla', N'(0949)8274731', NULL, NULL, 180)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170181, N'John Israel Mallari', N'Maxy', N'Poblacion Center, Ramos, Tarlac', N'(0999)7166345', NULL, NULL, 181)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170182, N'Mencie Bautista', N'Lucky / Rose', N'Ablang Sapang, Moncada, Tarlac', N'(0956)7854082', N'toksi14cie@gmail.com', NULL, 182)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170183, N'Jessica Roberto', N'Akashi', N'433 Asuncion St., Brgy. Poblacion Norte, Mayantoc, Tarlac', N'(0919)9911857', NULL, NULL, 183)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170184, N'Edward Aguilar', N'Cali / Scarlet', N'Estacion, Paniqui, Tarlac', N'(0910)3318543 / (0926)0464350', NULL, NULL, 184)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170185, N'Louis Xander Torres', NULL, N'Poblacion North, Ramos, Tarlac', N'(0917)4769953', NULL, NULL, 185)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170186, N'Anne Juliane Soluta', NULL, N'408-A Villa Nadine Subd., Acocolao, Paniqui, Tarlac', N'(0966)1083828', NULL, NULL, 186)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170187, N'Aarone Orceo', N'Chacha', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'(0998)5903321', N'aaorceo@gmail.com', NULL, 187)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170188, N'Florante M. Agustin', N'Jacko', N'121 Licor St., Paniqui, Tarlac', N'(0932)1850238', NULL, NULL, 188)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170189, N'Donny Apol Ayuyao', N'Sam', N'Caturay, Gerona, Tarlac', NULL, NULL, NULL, 189)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170190, N'Christopher P. Gonzales', N'Choji', N'250 Carriedo St., Paniqui, Tarlac', N'(0915)9711387', NULL, NULL, 190)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170191, N'Evarie L. Booc', N'Moon', N'258 Tablang, Paniqui, Tarlac', N'(0910)4701509', N'franzzel.austria@gmail.com', NULL, 191)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170192, N'Maria Isabel D. Balcoria', N'Fuel ', N'Cuyapo, Nueva Ecija', N'(0948)2032705', NULL, NULL, 192)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170193, N'Jerome Vinluan', N'Snow', N'Coral, Paniqui Tarlac', N'(0905)4417812', NULL, NULL, 193)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170194, N'Michael Lagmay', NULL, N'Quezon St., Paniqui, Tarlac', N'(0905)2501444', NULL, NULL, 194)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170195, N'Wendy Sing Mayo', N'Bullet / Harley', N'Canan, Paniqui, Tarlac', N'(045)9310679 / (0929)1398943', NULL, NULL, 195)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170196, N'Nemwel C. Padilla', N'Bruce', N'76 Poblacion, Anao, Tarlac', N'(0995)1553324', N'nemz0214@gmail.com', NULL, 196)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170197, N'Joselyn Galang', N'Lucky', N'Samput, Paniqui, Tarlac', N'(0927)8801651', NULL, NULL, 197)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170198, N'Gina Inocando', N'mojo', N'Brgy. Rizal, Moncada, Tarlac', N'(0916)4141367', NULL, NULL, 198)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170199, N'Alexis Lopez', N'Lexie', N'213 Sta. Rosa St., Paniqui, Tarlac', N'(045)6066006 / (0915)0754929', NULL, NULL, 199)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170200, N'Susan Millado', N'Fallen', N'Caturay, Gerona, Tarlac', N'(0942)7290104', NULL, NULL, 200)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170201, N'Vincy Kitz Nuguid', N'Sha-Sha/Lacy ', N'196 Tablang, Paniqui, Tarlac', N'(0995)2225371', NULL, NULL, 201)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170202, N'Aizha/Vanessa Ramos', N'Natsu', N'Buenlag, Gerona, Tarlac', N'(0915)9700981 / (0907)8656035', NULL, NULL, 202)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170203, N'Kimi Lanuza', NULL, N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0906)4073283', NULL, NULL, 203)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170204, N'Charie G. Cesario', N'Whitey', N'Colibangbang, Paniqui, Tarlac', N'(0907)8480521', NULL, NULL, 204)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170205, N'Mary Rose Tacdol', N'Lucky', N'Calapan, Moncada, Tarlac', N'(0921)3054943', NULL, NULL, 205)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170206, N'Toti Matias', NULL, N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(045)9311983 / (0917)9344150', NULL, NULL, 206)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170207, N'Russel Sean Grande Quiambao', N'Happy', N'Balaoang, Paniqui, Tarlac', N'(045)4708026 / (0921)1272506', N'sheila_0513@yahoo.com', NULL, 207)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170208, N'Ma. Elizabeth Flores', N'Bell', N'Brgy. Sta. Maria, Moncada, Tarlac', N'(0917)5432107', NULL, NULL, 208)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170209, N'Melanie Navarro ', N'Snow', N'Canan, Paniqui, Tarlac', N'9951260015', NULL, NULL, 209)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170210, N'Cristine Mae Taeza', N'Chop', N'San Leon, Moncada, Tarlac', N'9773658180', NULL, NULL, 210)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170211, N'Genica Mia Guzman', N'Maxi', N'West Central Poblacion, Nampicuan, Nueva Ecija', N'9971927507', N'gmdguzman29@gmail.com', NULL, 211)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170212, N'Teodora Magtalas', N'Luchi / Lucio', N'Sta. Lucia East, Moncada, Tarlac', N'(0919)8924682 / (0932)4764989', NULL, NULL, 212)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170213, N'Ellaine Lamorena ', N'Bella', N'Burot, Tarlac City', N'9776897260', N'ellainelamorena56@gmail.com', NULL, 213)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170214, N'Denmark Peralta', N'Cedrick', N'Tablang, Paniqui, Tarlac', N'9972257562', NULL, NULL, 214)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170215, N'Rose Mary Facun', N'Butchukoy', N'Pob.2 Moncada Tarlac', N'9493737752', NULL, NULL, 215)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170216, N'Michelle A. Loyaga', N'Coco', N'234 Baltazar Subd., Paniqui, Tarlac', N'(0917)5129817', N'franzzel.austria@gmail.com', NULL, 216)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170217, N'Winfield Teodoro', N'Elle', N'Gomez St., Brgy. Polacion Sur, Paniqui, Tarlac', N'(0918)3101445', NULL, NULL, 217)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170218, N'Paul Galviero', N'Ruffa', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0948)3292567', NULL, NULL, 218)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170219, N'Dianne Gana', N'Mino / Basti', N'Abogado, Paniqui, Tarlac', N'(045)6066786 / (0916)4220829', NULL, NULL, 219)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170220, N'Agnes Javier', N'Sugar', N'30 West Poblacion, Nampicuan, Nueva Ecija', N'(0956)1763632', NULL, NULL, 220)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170221, N'Lerma Musngi', N'Benedict', N'Estipona, Gerona, Tarlac', N'(0916)7526270', NULL, NULL, 221)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170222, N'Maila Sanders', N'Frida', N'San Antonio, Gerona, Tarlac', N'9667643991', NULL, NULL, 222)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170223, N'Aljen T. Bongon', N'Mia', N'Coral, Paniqui Tarlac', N'(0946)3397171', NULL, NULL, 223)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170224, N'Aljhon Rod Alvarez', N'Alki', N'Samput, Paniqui, Tarlac', N'(0950)2758038', N'majesty.mhars@yahoo.com', NULL, 224)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170225, N'John Paul Carino', N'Kingking', N'Poblacion Norte, Paniqui, Tarlac', N'(0917)4014866', NULL, NULL, 225)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170226, N'Jaja Guerrero', NULL, N'708 Cabayaoasan, Paniqui, Tarlac', N'(045)9312275 / (0917)5250283', N'jajaguerrero12@gmail.com', NULL, 226)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170227, N'Mya Geriene Pascua', N'Mocha / Mochi', N'Salud Papa, Poblacion 1, Gerona, Tarlac', N'(0956)6076554', NULL, NULL, 227)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170228, N'Harvey John Ray Oabel', N'Kylo / Dalisay', N'402 Luna St., Poblacion Norte, Paniqui, Tarlac', N'(045)9313258 / (0915)7655979', NULL, NULL, 228)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170229, N'Mark Ryan Valix', N'Charmaine / Geno', N'Brgy. Abugado, Paniqui, Tarlac', N'(045)606-0827 / (0977)1452591', NULL, NULL, 229)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170230, N'Edna Lampitoc', N'Toby', N'Brgy. Abugado, Paniqui, Tarlac', N'(045)9310025', NULL, NULL, 230)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170231, N'Ma. Fiona Gallano', N'Kobe', N'East Central Poblacion, Nampicuan, Nueva Ecija', N'9206515303', NULL, NULL, 231)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170232, N'Harold A. Martinez', N'Minari', N'Northwest Poblacion, Nampicuan, Nueve Ecija', N'(0915)7846380', NULL, NULL, 232)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170233, N'Alva Gonzales', N'Pepper', N'Bacao, Paniqui, Tarlac', N'(0917)5635002/(0927)5821558', N' ', NULL, 233)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170234, N'Marites Dancel', N'Mikey', N'Brgy. Burgos, Paniqui, Tarlac', N'(0999)9222662', NULL, NULL, 234)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170235, N'Jenddy Mostoles', N'Nala', N'San Julian, Moncada, Tarlac', N'9391750214', NULL, NULL, 235)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170236, N'Rolando Asanion', N'Bamba', N'Poblacion Norte, Paniqui, Tarlac', N'9304183931', NULL, NULL, 236)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170237, N'Jaselle Lacanlale Bautista', N'Elsa / Cuty', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0949)6308602', NULL, NULL, 237)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170238, N'Noel Nicolas', N'Umber', N'Pura, Tarlac', N'9776781968', NULL, NULL, 238)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170239, N'Rosenna Ocampo', N'Bonna', N'Purok 1, Samput, Paniqui, Tarlac', N'(0921)6562114', NULL, NULL, 239)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170240, N'Rosemarie Banedo', N'Malia', N'Carino, Paniqui, Tarlac', N'(0912)4424528', NULL, NULL, 240)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170241, N'Benedict Jay De Guzman', N'Saiber', N'West Central Poblacion, Nampicuan, Nueva Ecija', N'9773661485', N'bjguzman35@gmail.com', NULL, 241)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170242, N'Jenny Rei Magbalot', N'Hapee / Hershey', N'15 Villapaz Subd., Camangaan East, Moncada, Tarlac', N'(0977)2585750', N'jenny.rei06@gmail.com', NULL, 242)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170243, N'Chito Del Rosario', N'Tobi ', N'Tablang, Paniqui, Tarlac', N'9661670948', NULL, NULL, 243)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170244, N'Ancieta Gacayan', NULL, N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0995)1389838', NULL, NULL, 244)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170245, N'Franz Zel Austria', N'Blake', N'027 Patalan, Paniqui, Tarlac', N'(0917)8457829', N'franzzel.austria@gmail.com', NULL, 245)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170246, N'Kian Shane Roxas', N'Oreo', N'Salumague, Paniqui, Tarlac', N'9190054734', NULL, NULL, 246)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170247, N'May Flor Beltran', N'Sarah/Bruno ', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0909)4657219', N'mayflorbeltran@gmail.com', NULL, 247)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170248, N'Wenona Camatcho', NULL, N'Paniqui, Tarlac', N'9089448100', NULL, NULL, 248)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170249, N'Princess Joy Villacista', N'Sky', N'Camp. 1 Norte, Moncada, Tarlac', N'9159700962', NULL, NULL, 249)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170250, N'Karl Angelo Gil', N'Bruno', N'San Pedro, Moncada, Tarlac', N'9977217173', NULL, NULL, 250)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170251, N'Ayesha Claire Paragas', N'Kho Kho', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9277805432', NULL, NULL, 251)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170252, N'Harold Roxas', N'Tiger', N'Canan, Paniqui, Tarlac', N'(0908)8828034', NULL, NULL, 252)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170253, N'Loreta Palilio', N'Coby', N'Nampicuan, Nueva Ecija', N'9270140779', NULL, NULL, 253)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170254, N'John Edward S. Estayo', N'Champ', N'Magallanes St., Paniqui, Tarlac', N'(0998)7915215', N'john_edward1105@yahoo.com', NULL, 254)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170255, N'Avelina C. Ramos', N'Mallows / Choco', N'Baltazar Subd, Paniqui, Tarlac', N'(0943)0426097/ (045)9312695', NULL, NULL, 255)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170256, N'Glen Mariano', N'Nastya', N'Abogado, Paniqui, Tarlac', N'9218260539', N'GMARIANO@ME.COM', NULL, 256)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170257, N'Michael A. Marquez', N'Moana / Miggy', N'Nampicuan, Nueva Ecija', N'(0906)2283285', NULL, NULL, 257)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170258, N'Alexa Brandy Chuahong', N'Wine ', N'San Felipe, San Manuel , Tarlac  ', N'9230902094', NULL, NULL, 258)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170259, N'Aldrin Mendoza', N'Beatrice', NULL, N'(0915)9707147', NULL, NULL, 259)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170260, N'Marilou Mayores', N'Jacob', N'Poblacion 2, Moncada, Tarlac ', N'9497389928', NULL, NULL, 260)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170261, N'Benedict Santillan', N'Britney', N'Poblacion Sur, Paniqui, Tarlac', N'9309350965', NULL, NULL, 261)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170262, N'Jovy Nuguid ', N'Pooper', N'Bugallon St.,Estacion, Paniqui, Tarlac', N'9205314315', N'jovynuguid@gmail.com', NULL, 262)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170263, N'Michelle Villanueva', N'Chewy', N'Gerona, Tarlac', N'(0943)5750399', NULL, NULL, 263)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170264, N'Maribell Sales', N'Princess', N'San Juan, Moncada, Tarlac', N'(0932)0826364', NULL, NULL, 264)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170265, N'Filipina Labasan', N'Cuore', N'Purok 6, Samput, Paniqui, Tarlac', N'(0939)8207173', NULL, NULL, 265)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170266, N'Isabelle Quintos', N'Fluffy', N'Villa Nadine, 109-C, Acocolao, Paniqui, Tarlac', N'(0916)2573386', NULL, NULL, 266)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170267, N'Shanna Alyssa Bruno', N'Chuchay / Aubrey', N'San Julian, Moncada, Tarlac', N'(0906)3823843', N'alyssabruno8@gmail.com', NULL, 267)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170268, N'Nichelle Plata', N'Joji', N'Lacayanga Subd., Paniqui, Tarlac', N'9273892716', NULL, NULL, 268)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170269, N'Fatima Limos', N'Coco', N'East Central Poblacion, Nampicuan, Nueva Ecija', N'(0947)4537613', NULL, NULL, 269)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170270, N'Roselle Roxas', N'Lucky', N'850 Ramos Subd., Paniqui, Tarlac', N'(0966)4385122', NULL, NULL, 270)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170271, N'Senjie Reguindin', N'Sakura', N'Matindeg, Cuyapo, Nueva Ecija', N'9351111790', N'yoshisakura@gmail.com', NULL, 271)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170272, N'Patricia  Graneta Lagasca', N'Cinder/Happy/Luffy ', N'138 Plastado, Gerona, Tarlac', N'(0910)3754018/(0912)9078677', NULL, NULL, 272)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170273, N'Nora Pascual', N'Malia', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 273)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170274, N'Mary Jean Agliam', N'Carol', N'Toledo, Ramos, Tarlac', N'9516639898', NULL, NULL, 274)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170275, N'Mylene B. De Jesus ', N'Zeus ', N'13 Sta. Rosa St. Poblacion Sur, Paniqui, Tarlac ', N'(045) 4708227/ (0906)3964002', NULL, NULL, 275)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170276, N'Evelyn Iban', N'Lily', N'San Vicente, San Manuel, Tarlac', N'9303803410', N'lynnealonzo948@gmail.com', NULL, 276)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170277, N'MJ Pascual', N'Madame Lupita', N'Moncada, Tarlac', N'9228000800', N'maryjoypascual@yahoo.com', NULL, 277)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170278, N'Jayson P. Beltran', N'Shinie', N'Rang- Ayan, Paniqui, Tarlac', N'(0907)9812962', NULL, NULL, 278)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170279, N'Joan Lazaro', N'Gucci', N'Almaville Subd, Camanggan East, Moncada, Tarlac', N'9338103096', NULL, NULL, 279)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170280, N'Shaira Andrea Dela Cruz', N'Magic', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9163408010', NULL, NULL, 280)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170281, N'Annie Marcelo ', N'Edsa', N'Samput, Paniqui, Tarlac', N'(045)9310866/(0930)3381570', NULL, NULL, 281)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170282, N'Arjay Corpuz', N'Santino', N'Ventinilla, Paniqui, Tarlac', N'9484368618', NULL, NULL, 282)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170283, N'Honey Tess B. Alcantara', N'Maxie', N'Carino, Paniqui, Tarlac', N'(0998)2256050', NULL, NULL, 283)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170284, N'Santos Rombaoa', N'Chokee', N'Amacalan, Gerona, Tarlac', N'9106876331', NULL, NULL, 284)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170285, N'Marissa Obcena', N'Husky', N'Samput, Paniqui, Tarlac', N'9166241549', N'marissaobcena@gmail.com', NULL, 285)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170286, N'Adelaida Garcia', N'kingkong', N'Estacion, Paniqui, Tarlac', N'(0956)7086891', NULL, NULL, 286)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170287, N'Ligaya Pascasio', N'Shigggy', N'Paniqui, Tarlac', N'9668359742', NULL, NULL, 287)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170288, N'Cristy A. Carbonel', N'Kenlee', N'Cuyapo, Nueva Ecija', N'(0919)2536721', NULL, NULL, 288)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170289, N'Janette D. Jacob', N'Cookie', N'Coral, Ramos, Tarlac', N'(0933)8534997', NULL, NULL, 289)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170290, N'Zyndrix L. Calla', N'Keziah', N'Samput, Paniqui, Tarlac', N'(0930)9350965', NULL, NULL, 290)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170291, N'May Danjurie Degracia', N'Nana', N'Manaois, Paniqui, Tarlac', N'9073743691', NULL, NULL, 291)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170292, N'Ma. Fe Sarmiento', N'Belle', N'251 Carriedo St., Paniqui, Tarlac', N'(0916)8258879', NULL, NULL, 292)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170293, N'Sheena M. Pagaduan', N'Sammy', N'Samput, Paniqui, Tarlac', N'(0977)1714502', NULL, NULL, 293)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170294, N'Nina Santos', N'Tammy', N'Coral, Paniqui Tarlac', N'(0930)6873923', NULL, NULL, 294)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170295, N'Wendy V. Abaratigue', N'320', N'Rang- Ayan, Paniqui, Tarlac', N'(0916)5747035', NULL, NULL, 295)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170296, N'Carmelita Cabanit', N'Niro', N'Cuyapo, Nueva Ecija', N'9060474666', NULL, NULL, 296)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170297, N'Khristel Anne Caspe', N'Snowy', N'Ventinilla, Paniqui, Tarlac', N'9513223914', NULL, NULL, 297)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170298, N'Princess Danica Raneses', N'Bat Girl', N'Maluac, Moncada, Tarlac', N'9308488295', NULL, NULL, 298)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170299, N'April Joy Manuel', N'Lily', N'Balaoang, Paniqui, Tarlac', N'9364261031', NULL, NULL, 299)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170300, N'Steffi P. Bautista', NULL, N'Caturay, Gerona, Tarlac', N'(0930)0254108', NULL, NULL, 300)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170301, N'Analiza Duduaco', N'Itlog', N'Nampicuan, Nueva Ecija', N'9218656608', NULL, NULL, 301)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170302, N'Marilou G. Garingo', N'Candy', N'119 Samput, Paniqui, Tarlac', N'(045)9312497/(0916)3363544', NULL, NULL, 302)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170303, N'Imelda Nunag', N'Oreo', N'Samput, Paniqui, Tarlac', N'9175348328', NULL, NULL, 303)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170304, N'Emelyn C. Mendoza', N'Yuri', N'Sitio Vega, Licor St, Paniqui, Tarlac', N'(0932)22088360', NULL, NULL, 304)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170305, N'Richard Ongsico', N'Bulan', N'Abogado, Paniqui, Tarlac', N'(0917)3202534', N'ongsicorichard@gmail.com', NULL, 305)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170306, N'Angelo D. Sembrano', N'Mikuro', N'Buenlag, Gerona, Tarlac', N'(0946)9830264', NULL, NULL, 306)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170307, NULL, NULL, NULL, NULL, NULL, NULL, 307)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170308, N'Juvy P. Roxas', N'Panda', N'Estacion, Paniqui, Tarlac', N'(0917)8625087', N'juvyroxas05@yahoo.com', NULL, 308)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170309, N'Vincent  Q. Cunanan', N'Mavi', N'Coral, Paniqui Tarlac', N'(045)9311742/(0927)4333493', N'jaevee_15@yahoo.com', NULL, 309)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170310, N'Micah Joy Sagabaen ', N'Bugak', N'Carino, Paniqui, Tarlac', N'(0907)9701056', N'micah_joy0315@yahoo.com', NULL, 310)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170311, N'Shane Lopez', N'Brusco', N'167 Lacayanga Subd. Paniqui, Tarlac', N'(0956)6797642', NULL, NULL, 311)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170312, N'Jodinel Ladines', N'Lucky/ Basil', N'Poblacion Norte, Paniqui, Tarlac', N'(0905)1316393', NULL, NULL, 312)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170313, N'Robert Bernal', N'Pepper', N'Poblacion 1, Moncada, Tarlac', N'9166343424', NULL, NULL, 313)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170314, N'Charvey Tuazon ', N'Aeris', N'San Isidro Village, Carapdapan, Paniqui, Tarlac', N'(0906)64391679', N'markdamil02@gmail.com ', NULL, 314)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170315, N'Lian Agamao', N'Archie', N'Poblacion 2, Moncada, Tarlac ', N'9208829306', NULL, NULL, 315)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170316, N'Romel Velasco', N'Lonzo', N'Rang- Ayan, Paniqui, Tarlac', N'9457085958', N'romelvelasco15@gamil.com', NULL, 316)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170317, N'Marion Ver Cunanan', N'Hershey', N'Dalayoan Subd., Paniqui, Tarlac', N'9163355540', NULL, NULL, 317)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170318, N'Ailene Santiago', N'Paopao', N'West Central Poblacion, Nampicuan, Nueva Ecija', N'9062145279', NULL, NULL, 318)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170319, N'Florencia M. Pazcoguin', N'Jugle', N'460 Estacion, Paniqui, Tarlac', N'(045)9310064', NULL, NULL, 319)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170320, N'Melody Dumlao', N'Basha ', N'Paniqui, Tarlac', N'(0950)4184245', N'nicoleuycoco@gmail.com ', NULL, 320)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170321, N'Dra. Charito Abalos', N'Luan', N'Paniqui, Tarlac', N'9562464804', NULL, NULL, 321)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170322, N'Ann Berly Trinidad', N'Pocholo', N'Patalan, Paniqui, Tarlac', N'9773021150', NULL, NULL, 322)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170323, N'Jerome Vijiga', N'Calix', N'Sitio Mauplas, Brgy. Pinasling, Gerona, Tarlac', N'(0915)9556504', N' ', NULL, 323)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170324, N'Zendi Benitez', N'Hala', N'Poblacion Sur, Paniqui, Tarlac', N'9396331333', NULL, NULL, 324)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170325, N'Severa Cuchapin', N'Princess', N'Burgos st. Paniqui, Tarlac', NULL, NULL, NULL, 325)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170326, N'Carl Vincent Crisostomo', N'Pepsi', N'Coral, Paniqui Tarlac', N'(0955)3007824', NULL, NULL, 326)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170327, N'Renato Torres ', N'Shandy', N'Nipaco, Paniqui, Tarlac', N'(045)9255784', NULL, NULL, 327)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170328, N'Lyka Sarmiento', N'Skippy', N'254 Carapdapan, Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0908)6184950', NULL, NULL, 328)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170329, N'Christian Jayson Narciso', N'Buknoy/Fifi', N'Brgy. Cabayaosan, Paniqui, Tarlac', N'(0915)0547647', N'nomadsph@gmail.com ', NULL, 329)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170330, N'Rogelio Dugay ', N'Cool ', N'Buenlag, Gerona, Tarlac', N'(0915)8965996', NULL, NULL, 330)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170331, N'Jerome Mercado', N'Ashi', N'Poblacion 1, Moncada, Tarlac', N'9179084830', NULL, NULL, 331)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170332, N'Erah Sagabaen ', N'Oreo', N'Carino, Paniqui, Tarlac', N'(0916)7140490', NULL, NULL, 332)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170333, N'Lalaine Chichoco', N'Scarlet', N'Villa Nadine, Acocolao, Paniqui, Tarlac', N'(0916)5759470', NULL, NULL, 333)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170334, N'Christian C. Benito', N'Bela', N'Lacayanga Subd., Paniqui, Tarlac', N'(0927)0712333', NULL, NULL, 334)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170335, N'Evangeline Milla', N'Serchi', N'Apulid, Paniqui, Tarlac', N'(045)9256126', NULL, NULL, 335)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170336, N'Jumar Morales', N'Axcel Boy ', N'Brgy. Samput', N'(0928)6809909', NULL, NULL, 336)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170337, N'Angela Marie Pascual', N'Chappie/Viber', N'Sitio Carapdapan, Brgy. Pob. Norte, Paniqui, Tarlac', N'(0908)8206168', NULL, NULL, 337)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170338, N'Marcos Duduaco', NULL, N'Nampicuan, Nueva Ecija', N'(0997)3441375', NULL, NULL, 338)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170339, N'Eduard Jocel Villaflores', N'Snowy', N'Coral, Ramos, Tarlac', N'9951784407', NULL, NULL, 339)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170340, N'Mayumi Buenaobra', N'Bruno/Lokie', N'Abogado, Paniqui, Tarlac', N'9178912759', NULL, NULL, 340)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170341, N'Ana Manuel', N'Hatchico / Cheena', N'Coral, Paniqui Tarlac', N'(0917)1330279', NULL, NULL, 341)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170342, N'Johnbert Valdez', N'Jiro', N'Aduas, Paniqui, Tarlac', N'(0910)3914297', NULL, NULL, 342)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170343, N'Claire Merced Buentipo', N'Kali', N'Acocolao, Paniqui, Tarlac', N'9612449138', N'buentipo.claire@gmail.com', NULL, 343)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170344, N'Jimbo Lacanlale', N'Nike ', N'37 M.H. Del Pilar brgy. Estacion Paniqui, Tarlac', N'(0917)4349830', NULL, NULL, 344)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170345, N'Rowena Ramos', NULL, N'Poblacion 1, Gerona, Tarlac', N'(0950)5062663', NULL, NULL, 345)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170346, N'Rea Alyssa Dela Cruz', N'Lance', N'Cabayaoasan, Paniqui, Tarlac', N'9489300862', NULL, NULL, 346)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170347, N'Miguel Abalos ', N'Basha/Popoy', N'11 Burgos St. Paniqui, Tarlac', N'(045)4913762/(0926)7029411', NULL, NULL, 347)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170348, N'Janine Delim', N'Riley', N'Poblacion Sur, Paniqui, Tarlac', N'9774044454', N'Delim.janine97@yahoo.com', NULL, 348)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170349, N'Luz Butac Solibet', N'William ', N'Rang- Ayan, Paniqui, Tarlac', N'(0956)8535122', N'sweet16_shae@yahoo.com ', NULL, 349)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170350, N'Luz P. Ventura', N'Lebron ', N'Brgy. Abagon, Gerona, Tarlac', N'(0917)2792751', NULL, NULL, 350)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170351, N'Decelyn Batcho Luarez', N'Allison', N'Patalan, Paniqui, Tarlac', N'(0919)5166555', NULL, NULL, 351)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170352, N'Norma Lantin', N'Brownie ', N'Baltazar Subd, Paniqui, Tarlac', N'9175123457', NULL, NULL, 352)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170353, N'Melanie Ganzagan', N'Maui', N'Sapang, Moncada, Tarlac', N'9481737757', NULL, NULL, 353)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170354, N'Citadel Garcia', N'Marseille', N'Mabini, Moncada, Tarlac', N'9394463391', NULL, NULL, 354)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170355, N'Geraldine Joy E. Guico', N'Eggsy', N'Poblacion 4, Moncada, Tarlac', N'(0927)8070772', N'gjguico@gmail.com ', NULL, 355)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170356, N'Marlene C. Cayago', NULL, N'Abogado, Paniqui, Tarlac', N'(0927)3665512', N'marlenecayago@yahoo.com ', NULL, 356)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170357, N'Diwata De Vera', N'Miho ', N'201 Abogado, Paniqui, Tarlac', N'(045)6060851', NULL, NULL, 357)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170358, N'Christian Jay Santiago', N'Boa', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0906)2756432', NULL, NULL, 358)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170359, N'Melanie Colcol', N'Mumay', N'Capas, Tarlac', N'9463534106', NULL, NULL, 359)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170360, N'Ivory Jusain', N'Lemon', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0977)6658581', N'aybsmadriaga03@gmail.com ', NULL, 360)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170361, N'Ericka Joy S. Gajes', N'Gucci', N'Nampicuan, Nueva Ecija', N'(0950)9234862', NULL, NULL, 361)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170362, N'Dennis Bacaron', N'Sarah', N'Mabini, Moncada, Tarlac', N'(0929)588430', NULL, NULL, 362)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170363, N'Mark Francis Antolin', N'Chili', N'Sta. Ines, Paniqui, Tarlac', N'9517347279', N'tom.antolin@yahoo.com', NULL, 363)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170364, N'Christine Joy Anastacio', N'Sophie ', N'Poblacion Sur, Paniqui, Tarlac', N'9197932440', N'christineanastacio08@gmail.com', NULL, 364)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170365, N'Henry Perez Jr.', N'Brandon', N'Buenlag, Gerona, Tarlac', N'9269508665', NULL, NULL, 365)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170366, N'Ricky James Versoza', N'Alkene', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9100323431', NULL, NULL, 366)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170367, N'Vir / Mary Galzote', N'Chin Chin', N'San Leon, Moncada, Tarlac', N'9531509090', NULL, NULL, 367)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170368, N'Mylyn Teodoro', N'Franco', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0917)5165503', NULL, NULL, 368)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170369, N'Kimberly Andres', N'Luna ', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'9618359699', NULL, NULL, 369)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170370, N'Erwin Florendo', N'Chi-Chi/Bruno', N'Magallanes St., Paniqui, Tarlac', N'(045)4910429/(0947)5015129', NULL, NULL, 370)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170371, N'Dainelle Nicola Vitales', N'Quinee', N'98 Burgos St. Paniqui, Tarlac', N'(045)9311028/(0916)4384255', NULL, NULL, 371)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170372, N'Chit Tudla', N'Threcey', N'109-A Villa Nadine Acocolao, Paniqui, Tarlac', NULL, NULL, NULL, 372)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170373, N'Maria Cristina Nool', N'Smootch ', N'Acocolao, Paniqui, Tarlac', N'9175526235', NULL, NULL, 373)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170374, N'Princess Kaye Lim ', N'Glyky', N'Casili, Anao Tarlac', N'96776522465', NULL, NULL, 374)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170375, N'Kaye Ann Alegado', N'Brix ', N'Cabayaoasan, Paniqui, Tarlac', N'9472836937', NULL, NULL, 375)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170376, N'Raul Christopher Abubo', N'Kiray', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9451269351', NULL, NULL, 376)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170377, N'Kyla Mitch De Ramos', N'Hamster', N'Poblacion Sur, Paniqui, Tarlac', N'9092733309', NULL, NULL, 377)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170378, N'Rogelio A. Carino', N'Alex', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0912)5104036', NULL, NULL, 378)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170379, N'Arnold b. Adaoag', N'Sasha/Scarlet', N'Pasuquin St. Poblacion Sur, Paniqui, Tarlac', N'(045)9312229 / (0923)1969349', NULL, NULL, 379)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170380, N'Mary Anne Apuan ', N'Bobby ', N'San Jose South, Anao, Tarlac', N'9274376751', NULL, NULL, 380)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170381, N'Melissa Yamaguchi ', N'Skype ', N'Poblacion, San Manuel, Tarlac', N'9063788339', N'melissa.obillo@gmail.com', NULL, 381)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170382, N'Marhen Grace C. Vijiga', N'Uno', N'Pinasling, Gerona, Tarlac', N'(0995)9663777/(0946)8126630', NULL, NULL, 382)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170383, N'Hazel Obillo', N'Dumbo', N'San Manuel, Tarlac', N'6061561', NULL, NULL, 383)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170384, N'Agnes Tablo', N'Bruno', N'Lapsing, Moncada Tarlac', N'9481738199', NULL, NULL, 384)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170385, N'Patrick Inocencio', N'Bubbles', N'#006 Oloybuya Gerona, Tarlac', N'(0906)2436361', NULL, NULL, 385)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170386, N'Carlota Marquez', N'Ewoks', N'#163 Del Valle St., Paniqui, Tarlac', N'(0916)1993117', N'carlotabautistamarquez@gmail.com', NULL, 386)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170387, N'Frederick Malonga', N'Mika/Bullet', N'#02 Obillo St. Poblacion 1, Moncada, Tarlac', N'(045)6063443/(0921)7435950', N'mumay_1622@yahoo.com ', NULL, 387)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170388, N'Richmon Villamayor', N'Maggie', N'Bularit, Gerona, Tarlac', N'9325503615', NULL, NULL, 388)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170389, N'Rachelle Anne Lomboy', N'Chelsa ', N'Balite, Pura, Tarlac', N'9485525519', NULL, NULL, 389)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170390, N'KC Ortelano', N'Kia', N'Ventinilla, Paniqui, Tarlac', N'9305643812', N'kcortelano@gmail.com', NULL, 390)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170391, N'Christian Alfred Cuchapin', N'Pixie', N'Poblacion Norte, Paniqui, Tarlac', N'9175630422', NULL, NULL, 391)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170392, N'Mary Grace Valix', N'Blackee', N'Campo Santo 1 Sur, Moncada, Tarlac', N'9128529131', N'marygracevalix@yahoo.com', NULL, 392)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170393, N'Jomar Dizon', N'Oreo', N'Balaoang, Paniqui, Tarlac', N'9165742925', NULL, NULL, 393)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170394, N'Ricardo Marquez', NULL, N'Del Valle St., Paniqui, Tarlac', NULL, NULL, NULL, 394)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170395, N'Lyka Loren Lagamon', N'Pebbles', N'Tablang, Paniqui, Tarlac', N'(0927)4477721', NULL, NULL, 395)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170396, N'Reina Ramos', N'Sky', N'District 5, Cuyapo, Nueva Ecija', N'9453742592', NULL, NULL, 396)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170397, N'Melissa Mae Favis', N'Echo', N'Poblacion 4, Moncada, Tarlac', N'9218998840', N'assilemeamsivaf@gmail.com', NULL, 397)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170398, N'Catherine Lagarza', N'Iska ', N'Burgos, Moncada, Tarlac', N'9369659630', N'catherinelagarza@gmail.com', NULL, 398)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170399, N'Nheng Sabado', N'Dog', N'San Manuel, Tarlac', N'9275573749', N'nhengsabado@gmail.com', NULL, 399)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170400, N'Mary Ann Veloria', N'Yuki', N'Nancamarinan, Paniqui, Tarlac', N'(0916)2573251', NULL, NULL, 400)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170401, N'Michael Mark Baltazar', N'Lexus', N'Baltazar Subd, Paniqui, Tarlac', N'(0917)9953655', NULL, NULL, 401)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170402, N'Lea Bondoc ', N'Rabbit', N'Samput, Paniqui, Tarlac', N'9517347274', NULL, NULL, 402)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170403, N'Michelle Cortez', N'Gunner', N'Abogado, Paniqui, Tarlac', N'9204912132', NULL, NULL, 403)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170404, N'Juvy Molina Sambo', N'Tommie', N'226 Mac Arthur St. Samput, Paniqui, Tarlac', N'(0946)9356054/(0930)6352649', NULL, NULL, 404)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170405, N'Eunice Tabije', N'Twixy', N'Cuyapo, Nueva Ecija', N'(0955)3010456', NULL, NULL, 405)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170406, N'Venice Enriquez', N'Guenivere', N'Poblacion 1, Gerona, Tarlac', N'9958259577', NULL, NULL, 406)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170407, N'Jhonalyn Natividad', N'Princess', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0945)3195737', NULL, NULL, 407)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170408, N'Joan Marie Florentino', N'Pexie', N'Toledo, Ramos, Tarlac', N'9380194353', NULL, NULL, 408)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170409, N'Via Bernadete Aquino', N'Muncher/Clover ', N'Cabayaoasan, Paniqui, Tarlac', N'(0907)4112099', NULL, NULL, 409)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170410, N'Khamille Q. Mina', N'Clyde', N'Guiteb, Ramos, Tarlac', N'(0936)9645726', NULL, NULL, 410)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170411, N'Ma. Aileen Esteban', N'Jeene', N'Poblacion 1, Gerona, Tarlac', N'(0943)5426949', NULL, NULL, 411)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170412, N'Jhon Rey Bañaga', N'Bunso', N'Abogado, Paniqui, Tarlac', NULL, NULL, NULL, 412)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170413, N'Cynthia Marquez', N'Bella', N'518 Sitio Basio, Poblacion Norte, Paniqui, Tarlac', N'(045)3260794/ (0917)3444408', NULL, NULL, 413)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170414, N'Kathleen Dizon ', N'Zap', N'Poblacion Norte, Paniqui, Tarlac', N'(0966)8699459', NULL, NULL, 414)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170415, N'Leonardo Yabut', N'Chocolate ', N'Ventinilla, Paniqui, Tarlac', N'9196283740', NULL, NULL, 415)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170416, N'Mel Paolo D. Estavillo', N'Tummy/ Sirius', N'Pura, Tarlac', N'(0998)3311481', NULL, NULL, 416)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170417, N'Vianca Figueroa', N'Copper', N'Sapang, Moncada, Tarlac', N'9610415117', NULL, NULL, 417)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170418, N'Helen Krutsch', N'Koko', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(045)4912170/(0927)0483765', NULL, NULL, 418)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170419, N'Elena S. Quibuyen', N'Pochie/Heaven ', N'San Francisco West, Anao, Tarlac', N'(0921)6143015', NULL, NULL, 419)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170420, N'Aimee Camille Gimeno', N'Akiro', N'Nampicuan, Nueva Ecija', N'(0917)1523440', NULL, NULL, 420)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170421, N'Lea Mae Marquez', N'Roxy', N'Poblacion 2, Moncada, Tarlac ', N'9383459181', NULL, NULL, 421)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170422, N'Francisco Ragma', N'Sergio ', N'Victoria, Tarlac', N'9092129593', NULL, NULL, 422)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170423, N'Ramon Aquino', N'Cottonmouth', N'Moncada, Tarlac', N'9175304980', NULL, NULL, 423)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170424, N'Jeremy Gabriel', N'Maggi ', N'Sta. Rosa St., Paniqui, Tarlac', N'9175548914', NULL, NULL, 424)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170425, N'Zyvel Alip Manlansing', N'Tala', N'Poblacion South, Ramos, Tarlac', N'9086265504', NULL, NULL, 425)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170426, N'Emma Alexandra Matundan', N'Amber', N'#67 South Central Pob. Nampicuan Nueva Ecija', N'(0999)9664050', NULL, NULL, 426)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170427, N'Meliza Domingo', N'Mushu', N'Baltazar Subd, Paniqui, Tarlac', N'9457883270', NULL, NULL, 427)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170428, N'Rodel John Pagaduan', N'Chico', N'Poblacion 1, Pura Tarlac', N'9304049415', NULL, NULL, 428)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170429, N'Maricel Sabado', N'Peanut', N'Salumague, Paniqui, Tarlac', N'9062082991', NULL, NULL, 429)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170430, N'Oliver Oleo', N'Covida', N'San Jacinto, San Manuel, Tarlac', N'9187011201', NULL, NULL, 430)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170431, N'Purita Marzan', N'Ashley ', N'Rizal, Moncada, Tarlac', N'606-5275', NULL, NULL, 431)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170432, N'Kimberly Ramos', N'Zimba', N'Canan, Paniqui, Tarlac', N'9750852567', NULL, NULL, 432)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170433, N'Armie Grace Briones', N'Alexander', N'Paniqui, Tarlac', N'9062086173', NULL, NULL, 433)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170434, N'Shen Quibuyen', N'Katniss', N'San Julian, Moncada, Tarlac', N'9076684044', NULL, NULL, 434)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170435, N'Roy Mayo', N'Cosa', N'22 Camanggan East, Moncada, Tarlac', N'(0945)8036393', NULL, NULL, 435)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170436, N'Sheena Marie P. Baluyot', N'Tommy', N'Brgy. Cayanga, Paniqui, Tarlac', N'(0917)1045982', NULL, NULL, 436)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170437, N'Raymond Duco', N'Nana', N'Capayaoan, Moncada, Tarlac', N'(0966)3380780', NULL, NULL, 437)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170438, N'Allan Velano', N'Odette', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'(0995)7221730', NULL, NULL, 438)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170439, N'Mia Cabanting', N'Negi', N'District 5, Cuyapo, Nueva Ecija', N'(0995)9673694', NULL, NULL, 439)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170440, N'Ronie Villa', N'Sheevah', N'Apsayan, Gerona, Tarlac', N'9661888007', NULL, NULL, 440)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170441, N'Julie Ann Veloria', N'Mico', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9109113853', NULL, NULL, 441)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170442, N'Chrisostome Valete', N'Charm', N'Poblacion 3, Gerona, Tarlac', N'9295368183', NULL, NULL, 442)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170443, N'Ligaya Dela Rosa', N'Ichi', N'560 Bugallion St., Paniqui, Tarlac', N'(0916)6449594', NULL, NULL, 443)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170444, N'Juan Parcasio', N'Max', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0949)1515210', NULL, NULL, 444)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170445, N'William Sargado', N'Batak', N'Burgos st. Paniqui, Tarlac', N'(0915)8679537', NULL, NULL, 445)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170446, N'Jeaine Maxine Marquez', N'Zoey', N'Brgy. Southwest Pob. Nampicuan, Nueva Ecija', N'(0916)2530925', NULL, NULL, 446)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170447, N'Elsa G. Acayayan', N'Mccoy/Oni', N'190 Salumgaue, Paniqui, Tarlac', N'(0923)3652557', NULL, NULL, 447)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170448, N'Joseph/Elvie De Guzman', N'Venus', N'New Salem, Gerona, Tarlac', N'(0920)8112273', NULL, NULL, 448)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170449, N'Josephine Limos', N'Izza', N'15 Manaois, Paniqui, Tarlac', N'(0916)7909651', NULL, NULL, 449)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170450, N'Criselda Dungca', N'Izzy', N'Nampicuan, Nueva Ecija', N'(0995)1553576', NULL, NULL, 450)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170451, N'Rossana Vicente', N'Marley/Lexie', N'Poblacion 1, Moncada, Tarlac', N'(0905)2618792', NULL, NULL, 451)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170452, N'Alfredo Malonga', N'Lola', N'Poblacion 1, Moncada, Tarlac', N'(0942)4928571', NULL, NULL, 452)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170453, N'Irish Ane Joy Diamsay', N'Areum', N'San Juan, Ramos, Tarlac', N'9496331188', NULL, NULL, 453)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170454, N'Princess Diane Estanislao', N'Stanley', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0916)6852148', NULL, NULL, 454)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170455, N'Leonalyn Flake', N'Burgera/Kuyang', N'Burgos, Moncada, Tarlac', N'(0945)5799070', NULL, NULL, 455)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170456, N'Zengene Sarmiento', N'Tucker', N'Cuyapo, Nueva Ecija', N'(0927)3002033', NULL, NULL, 456)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170457, N'Jonathan Ramos', N'Ziggy', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0915)9937721', NULL, NULL, 457)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170458, N'Ian Carlos Adriano', N'Hayan', N'Apulid, Paniqui, Tarlac', N'9057515180', NULL, NULL, 458)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170459, N'Paolo Louis Artajos', N'Brutus', N'Sitio Maligaya, Licor, Poblacion Norte, Paniqui, Tarlac', N'(0932)2778986', NULL, NULL, 459)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170460, N'Fritzi Mei Alonzo', N'Pochi', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0936)9665259', NULL, NULL, 460)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170461, N'Monica Gonzales', N'Ar-Ar', N'Samput, Paniqui, Tarlac', N'9386031614', NULL, NULL, 461)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170462, N'Leah B. Villanueva', N'Mik-Mik', N'Mabini, Moncada, Tarlac', N'(0917)5355001', NULL, NULL, 462)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170463, N'Ricky Dasalla', N'Erik', N'Pob. North, Ramos, Tarlac', N'(0927)3515447', NULL, NULL, 463)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170464, N'Gian Peter Gonzales', N'Kiefer', N'110 Samput, Paniqui, Tarlac', N'(0910)1170142', NULL, NULL, 464)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170465, N'Princess Regine Reyla', N'Puppy', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9563813750', NULL, NULL, 465)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170466, N'Margaret Anievas', N'Cody', N'1278 Baltazar Subd., Pob. Norte, Paniqui, Tarlac', N'(045)9310729/(0916)7172584', NULL, NULL, 466)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170467, N'Jewell Fernandez', N'Dwayne', N'Poblacion Norte, Paniqui, Tarlac', N'9167749126', NULL, NULL, 467)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170468, N'Archie Facunla', N'Honey', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0917)5631420', NULL, NULL, 468)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170469, N'Jonna  Dela Cruz', N'Kenzo/Cody', N'Burgos, Moncada, Tarlac', N'(0947)5258551', NULL, NULL, 469)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170470, N'Nelson Dela Cruz', N'Ganda', N'San Jose South, Anao, Tarlac', N'(0921)2107990', NULL, NULL, 470)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170471, N'Jhona Mae Tiamson', N'Ziggy', N'Poblacion South, Ramos, Tarlac', N'9304418325', NULL, NULL, 471)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170472, N'Annie May Co Ting', N'Chanel ', N'San Antonio, Gerona, Tarlac', N'9178938485', NULL, NULL, 472)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170473, N'Maria Christina Rimas', N'Fudge ', N'Pob 3. Pura, Tarlac', N'9154943623', NULL, NULL, 473)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170474, N'Eva Esteban', N'Murphy', N'Maluac, Moncada, Tarlac', N'9773263017', NULL, NULL, 474)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170475, N'Lyzel L. Tan', N'Choco', N'Magaspac, Gerona, Tarlac', N'(0923)9594450', NULL, NULL, 475)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170476, N'Eden Cortez', N'Onyx', N'Cariño, Paniqui, Tarlac', N'9389072990', NULL, NULL, 476)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170477, N'Glorie Ann Doctolera', N'Raba', N'Magaspac, Gerona, Tarlac', N'(0998)9508606', NULL, NULL, 477)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170478, N'Brynhild Yabut', N'Sugar', N'Dalayoan Subd., Paniqui, Tarlac', N'9478936582', NULL, NULL, 478)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170479, N'Anna Marie Pascua', N'Luna ', N'Cariño, Paniqui, Tarlac', N'9667955769', NULL, NULL, 479)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170480, N'John Carlo Dela Cruz', N'Loki', N'Camangaan West, Moncada Tarlac', N'9457525431', NULL, NULL, 480)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170481, N'Zylla Tumampos', N'Kisses', N'Acocolao, Paniqui, Tarlac', N'(0955)8306556', NULL, NULL, 481)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170482, N'Erwin Agamata', N'Ashly', N'Maligaya, Licor, Paniqui, Tarlac', N'(0946)754268', NULL, NULL, 482)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170483, N'Ruby Tapispisan ', N'Jun ', N'Poblacion Sur, Paniqui, Tarlac', N'9667613007', NULL, NULL, 483)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170484, N'Danette Pitpit', N'Akon', N'San Julian, Moncada, Tarlac', N'9208038157', NULL, NULL, 484)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170485, N'Bernadette Mariano', N'Cloud', N'Balite, Pura, Tarlac', N'9275642650', NULL, NULL, 485)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170486, N'Charlotte Melchor', N'Sky', N'Sta. Lucia East, Moncada, Tarlac', NULL, NULL, NULL, 486)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170487, N'Dominic Quirong', N'Jordan', N'Matalapitap, Paniqui, Tarlac', N'9777379815', NULL, N' ', 487)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170488, N'Mario Soliman', N'Budang', N'Abogado, Paniqui, Tarlac', N'9167354502', NULL, NULL, 488)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170489, N'Geronimo Mahor', N'Pot-Pot', N'Sta. Maria, Moncada, Tarlac', N'(0995)2229529', NULL, NULL, 489)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170490, N'Aileen Tena', NULL, N'74 Canan, Paniqui, Tarlac', N'(0920)4984548', NULL, NULL, 490)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170491, N'Rosalinda Palaganas', N'Oreo', N'Lacayanga Subd., Paniqui, Tarlac', N'9485351003', NULL, NULL, 491)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170492, N'Maricris Samoy', N'Stacey', N'Purok Dalisay, Guiteb, Ramos, Tarlac', N'(09164975796', NULL, NULL, 492)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170493, N'Rodolfo Datu', N'Labzky', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'9237373980', NULL, NULL, 493)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170494, N'Allen Jay Cariazo', N'Nami', N'San Julian, Moncada, Tarlac', N'(0965)3348218', NULL, NULL, 494)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170495, N'Leeann Gascon', N'Kode', N'San Julian, Moncada, Tarlac', N'(0927)9367450', NULL, NULL, 495)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170496, N'Genesis Perez', N'Naya', N'Buenlag, Gerona, Tarlac', N'9451465199', NULL, NULL, 496)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170497, N'Arthur Babas', N'Shane', N'Villa Socorro, Poblacion Norte, Paniqui, Tarlac', N'9065133910', NULL, NULL, 497)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170498, N'Cyrille Roxas', N'Lambo', N'Acocolao, Paniqui, Tarlac', N'9177077832', N'cyrille.tienzo@yahoo.com', NULL, 498)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170499, N'Rafael Milan', N'Natoy', N'Samput, Paniqui, Tarlac', N'9756725094', NULL, NULL, 499)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170500, N'Lea Perdigon', NULL, N'Bulala, Cuyapo, Nueva Ecija ', N'9952651511', N' ', NULL, 500)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170501, N'Jennifer Tablada', N'Boo', N'Cuyapo, Nueva Ecija', N'(0927)4335800', NULL, NULL, 501)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170502, N'Pinky Almazan', N'Mucho', N'Baltazar Subd, Paniqui, Tarlac', N'9452092302', NULL, NULL, 502)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170503, N'Michael De Jesus', N'Finn', N'11 F. Cojuangco St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0916)2292035', NULL, NULL, 503)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170504, N'Rosalyn Wage ', N'Coco', N'Cayanga, Paniqui, Tarlac', N'9072206280', NULL, NULL, 504)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170505, N'Marvil Eligio ', N'Oppa', N'Camangaan East, Moncada Tarlac', N'9273704218', NULL, NULL, 505)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170506, N'Jackielyn Duran', N'Athena', N'Pance, Ramos, Tarlac', N'9484577248', NULL, NULL, 506)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170507, N'Kevin Palasigue', N'Cooper', N'Sinigpit, Paniqui, Tarlac', N'9159637498', NULL, NULL, 507)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170508, N'Rovelyn Viray', N'Che-Che', N'Blk 4, Lot 14, Creative St., Alexandre Hts, Samput, Paniqui, Tarlac', N'(0995)4489366', NULL, NULL, 508)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170509, N'Sirafin Yanguas', N'Ackie', N'Cariño, Paniqui, Tarlac', N'9611608005', NULL, NULL, 509)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170510, N'Arlette Gonzales', N'Jannafe', N'Coral, Paniqui Tarlac', N'9056159608', NULL, NULL, 510)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170511, N'Willy Gaculais', N'Belgian', N'Matalapitap, Paniqui, Tarlac', N'(045)9312839', NULL, NULL, 511)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170512, N'Allea Macaraeg', N'Finn', N'90 Samput, Paniqui, Tarlac', N'(045)6063550/(0997)4495026', NULL, NULL, 512)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170513, N'Marie Paz Jimenez', N'Roofy', N'Samput, Paniqui, Tarlac', N'9973578580', NULL, NULL, 513)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170514, N'Bryan Kelly Agustin', N'Tucker', N'Latap, Cuyapo Nueva Ecija', N'9664511712/9162194103', NULL, NULL, 514)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170515, N'Renato Rosario', N'Choco', N'Canan, Paniqui, Tarlac', N'9088828034', NULL, NULL, 515)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170516, N'Christian Paul Melegrito', N'Dyrroth', N'San Antonio, Gerona, Tarlac', N'9152840705', NULL, NULL, 516)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170517, N'Rosalinda Mariano', N'Katie ', N'Poblacion Center, Ramos, Tarlac', N'9435920070', NULL, NULL, 517)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170518, N'Mariella Cabansag', N'Sasha', N'Maluac, Moncada, Tarlac', N'(0915)9705900', NULL, NULL, 518)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170519, N'Rina Jacob', N'Barak', N'Apulid, Paniqui, Tarlac', N'(0907)8405543', NULL, NULL, 519)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170520, N'Rita Napuran', N'Botchog/Prince', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0910)6535354', NULL, NULL, 520)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170521, N'Albert Lim', N'Tetchie', N'646 Cabayaosan, Paniqui, Tarlac', N'(0909)6184699', NULL, NULL, 521)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170522, N'Melissa Dela Cruz', N'Milo', N'Samput, Paniqui, Tarlac', N'9958156688', NULL, NULL, 522)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170523, N'Chelsie Liwanag Cachero', N'Shaggy ', N'Estacion, Paniqui, Tarlac', N'9455799019', NULL, NULL, 523)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170524, N'Jayson Danganan', N'Lakandula', N'Samput, Paniqui, Tarlac', N'9064149612', NULL, NULL, 524)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170525, N'Jhannary Villacisneros', N'Winter', N'San Pedro, Moncada, Tarlac', N'9771693438', NULL, NULL, 525)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170526, N'Hanz Maverick Ayuyao', NULL, N'Poblacion 1, Gerona, Tarlac', N'(0977)0120502', NULL, NULL, 526)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170527, N'Alexis Esguerra', N'Panusa', N'Brgy. Pantol, Bayambang, Pangasinan', N'(0956)8009124', NULL, NULL, 527)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170528, N'Yolanda Vega', N'Shashi', N'Brgy. Salumague, Paniqui, Tarlac', N'(045)6285127', NULL, NULL, 528)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170529, N'Jane Santos', N'Chuck', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'(0916)5176116', NULL, NULL, 529)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170530, N'Khristine Gacayan', N'Jawhead', N'Coral, Ramos, Tarlac', N'9362848906', NULL, NULL, 530)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170531, N'Gabriel Paul Resurreccion', N'Burn', N'Mabini, Moncada, Tarlac', N'(0912)1812201', NULL, NULL, 531)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170532, N'Daryl Cabanes', N'Koffee', N'San Antonio, Gerona, Tarlac', N'9454616', NULL, NULL, 532)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170533, N'Rochelle Pagaling', N'Flurppie', N'Caturay, Gerona, Tarlac', N'9105075778', NULL, NULL, 533)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170534, N'Christian Tanglao', N'Coco', N'Sta. Rosa St., Paniqui, Tarlac', N'(0945)8744626', N'Black Male Pug Aug 17, 2018', NULL, 534)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170535, N'Mercy Escalada', N'Buddy-Bam', N'Parsolingan, Gerona, Tarlac', N'9496693263', NULL, NULL, 535)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170536, N'Jesusa Lambino', N'Yukine', N'Canan, Paniqui, Tarlac', N'(0909)6485409', NULL, NULL, 536)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170537, N'Marie Jean Fernandez', N'Noah', N'Coral, Paniqui Tarlac', N'9618423233 / 628-1840', NULL, NULL, 537)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170538, N'Dana Cabral', N'Sophie ', N'Pob.4, Moncada, Tarlac', N'9102082629', NULL, NULL, 538)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170539, N'Ferdinand Quibuyen', NULL, N'Brgy. Sta Lucia East, Moncada, Tarlac', N'(0930)8811439', NULL, NULL, 539)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170540, N'Melody Maranion', N'Miya', N'Ramos, Tarlac', N'(0939)2018846', NULL, NULL, 540)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170541, N'Shella Mae Pre', N'Messy', N'Abogado, Paniqui, Tarlac', N'9669122345', NULL, NULL, 541)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170542, N'Guia Oliver', N'Maldita', N'Pob. Norte, Paniqui, Tarlac', N'(0995)1866131', NULL, NULL, 542)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170543, N'Debbie Cayago', N'Smokey', N'401 Lacayanga Subd., Paniqui, Tarlac', N'(0905)2987161', NULL, NULL, 543)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170544, N'Guia Pineda', N'Daisy ', N'56 Licor St. Paniqui, Tarlac', N'(0921)0907705', NULL, NULL, 544)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170545, N'Carolyn Peralta', N'Buddy ', N'Capayaoan, Moncada, Tarlac', N'9462624216', NULL, NULL, 545)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170546, N'Joanna Marie Mamaclay', N'Lucky', N'168 Nilasin 2nd, Pura, Tarlac', N'(0906)2518871', NULL, NULL, 546)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170547, N'Regine Fernandez', N'Chamsi', N'Moncada, Tarlac', N'9496394948', NULL, NULL, 547)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170548, N'Esper Louie Rollon', N'Tenten', N'Canan, Paniqui, Tarlac', NULL, NULL, NULL, 548)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170549, N'Jev Joson', N'Zero', N'Matalapitap, Paniqui, Tarlac', N'(0919)8182861', NULL, NULL, 549)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170550, N'Peter Castillo', N'Chloei', N'Dapdap,Paniqui, Tarlac', N'(0910)7251410', NULL, NULL, 550)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170551, N'Jo Carlo Ebalan', N'Rocky', N'Brgy. Mabini, Moncada, Tarlac', N'(0995)1716078/(045)6066394', NULL, NULL, 551)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170552, N'Jessie Farrales', N'Yoriko', N'Cabayaoasan, Paniqui, Tarlac', N'(0909)1164259', NULL, NULL, 552)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170553, N'Tina Chichioco', N'Bently', N'Bugallon St.,Estacion, Paniqui, Tarlac', N'(0906)2425949', NULL, NULL, 553)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170554, N'Francine Lorie A. Narciso', N'Cooper', N'Dapdap,Paniqui, Tarlac', N'(0995)2301858/(0938)6032009', N'limjanine97@yahoo.com ', NULL, 554)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170555, N'Kevin Ray Sabado', N'Luke', N'Apulid, Paniqui, Tarlac', N'(0945)3794079', NULL, NULL, 555)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170556, N'Connie Villena', N'Scarlet', N'Blk 21, Lot 4, Respect St. Alexandre Hts, Samput Paniqui, Tarlac', N'(0905)2642738', NULL, NULL, 556)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170557, N'Jean Samson', N'Railey', N'Palarca St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(045)9312238/(0920)2526652', NULL, NULL, 557)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170558, N'Suzette Hilario', N'Kimchi', N'Poblacion 2, Moncada, Tarlac ', N'9175145980', NULL, NULL, 558)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170559, N'Mary Grazielle Roxas', N'Scar', N'Dalayoan Subd., Paniqui, Tarlac', N'9663246364', NULL, NULL, 559)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170560, N'Louise Leanne Manuel', N'Kana Chan', N'Pob Norte, Paniqui, Tarlac', N'(0967)73694480/ (0907)7550611', NULL, NULL, 560)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170561, N'Gay Macaraeg', N'Athna', N'231 Baltazar Subd., Paniqui, Tarlac', N'(045)9310002/(0917)8220819', NULL, NULL, 561)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170562, N'Ymelie Galapia', N'Tammy', N'Poblacion, San Manuel, Tarlac', N'9267383346', NULL, NULL, 562)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170563, N'Lucky Lyn Tabera', N'Kofi', N'Moncada, Tarlac', N'9665718370', NULL, NULL, 563)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170564, N'Mark Tolentino', N'Austin', N'Anao, Tarlac', N'9176531966', NULL, NULL, 564)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170565, N'Maurice Menor', N'Motchi', N'Singat, Pura, Tarlac', N'9453175983', NULL, NULL, 565)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170566, N'Karl Malone Acebu', NULL, N'Brgy. Hernando, Anao, Tarlac', N'(045)6061279/(0930)3609148', NULL, NULL, 566)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170567, N'Mary Lourdes Velasco', NULL, N'Matalapitap, Paniqui, Tarlac', N'(0926)3008640', NULL, NULL, 567)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170568, N'Ibrahim Marohombsar', N'Bambina', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0917)5180185', NULL, NULL, 568)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170569, N'Chester Honorio', N'Keeper', N'Brgy. Hernando, Anao, Tarlac', N'(045)6069027/(0947)5004306', NULL, NULL, 569)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170570, N'Elisa Padaca', N'Tayra', N'450 Bantog, Carino, Paniqui, Tarlac', N'(045)9312271/(0916)5039273', NULL, NULL, 570)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170571, N'Jerico Cervantes', N'Tootsie', N'Brgy. Patalan, Paniqui, Tarlac', N'(0905)2543697', NULL, NULL, 571)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170572, N'Richard Ambrocio', N'Lebron ', N'Moncada, Tarlac', N'9771958319', NULL, NULL, 572)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170573, N'Danica Lambino', N'Sky', N'Salumague, Paniqui, Tarlac', N'(0946)4612408', NULL, NULL, 573)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170574, N'Norhylein Teves', N'Trexie', N'Ventinilla, Paniqui, Tarlac', N'9202744434', NULL, NULL, 574)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170575, N'Bryan Michael Gabriel ', N'Roy', N'268  Ohio St., Apulid, Paniqui, Tarlac', N'(0936)0832917', NULL, NULL, 575)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170576, N'Evelyn Garay', N'Skye', N'Bautista, Pangasinan', N'9165139499', NULL, NULL, 576)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170577, N'Gil Mendoza', N'Yangchow', N'Brgy. Estacion, Nampicuan, Nueva Ecija', N'(0925)3828984', N'nampicuanchristianacademy@gmail.com', NULL, 577)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170578, N'Jewel Ching', N'Jacob', N'Rizal, Anao, Tarlac', N'9777443148', NULL, NULL, 578)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170579, N'Gwyneth Page', N'Onyx', N'Paniqui, Tarlac', N'9333541816', NULL, NULL, 579)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170580, N'Emily Daileg', N'Bella', N'Dalayoan Subd., Paniqui, Tarlac', N'9152628085', NULL, NULL, 580)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170581, N'Bryan James Castro', N'Sky', N'1799 Baltazar Subd., Paniqui, Tarlac', N'(0995)1478742', NULL, NULL, 581)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170582, N'Zaldy De Guzman', N'Gibson', N'Poblacion Sur, Paniqui, Tarlac', N'(045)9311002/(0915)1777005', NULL, NULL, 582)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170583, N'Brianne Lara Perez', N'Mosh', N'Paniqui, Tarlac', NULL, NULL, NULL, 583)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170584, N'Carlos Abraham', NULL, N'Poblacion Norte, Paniqui, Tarlac', N'(045)9256122', NULL, NULL, 584)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170585, N'Edison Tongol', N'Seve', N'Abogado, Paniqui, Tarlac', N'9055628323', NULL, NULL, 585)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170586, N'Sherma Ventura', N'Tikky', N'194 Rasig, Acocolao, Paniqui, Tarlac', N'(0907)6809575', NULL, NULL, 586)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170587, N'Jordan Butolan', N'Aisha', N'Cayanga, Paniqui, Tarlac', N'9272282791', NULL, NULL, 587)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170588, N'Leny Bagayan', N'Choco', N'Coral, Paniqui Tarlac', N'9186319550', NULL, NULL, 588)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170589, N'Venus C. Caspe', N'Bailey', N'Salumague, Paniqui, Tarlac', N'(0947)9903307', NULL, NULL, 589)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170590, N'Cecilia Caranto', N'Aoie', N'Coral, Paniqui Tarlac', N'9127114345', NULL, NULL, 590)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170591, N'Nestor Magbitang', N'Sam', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'9231968711', NULL, NULL, 591)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170592, N'Ferdinand Navarro', NULL, N'Canan, Paniqui, Tarlac', N'(0948)6319074', NULL, NULL, 592)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170593, N'Justiniano Alviar', N'Bella', N'Matalapitap, Paniqui, Tarlac', N'(0977)6001577', NULL, NULL, 593)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170594, N'Andre Monique Virgino', N'Hoyshie', N'Matalapitap, Paniqui, Tarlac', N'9266562458', NULL, NULL, 594)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170595, N'Trinidad Rosal', N'Pipper', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', NULL, NULL, NULL, 595)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170596, N'Roma Amor Parayno', N'Matthew', N'Pob. North, Ramos, Tarlac', N'(0909)9735858', N'romaparayno@gmail.com ', NULL, 596)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170597, N'Arby Pablo', N'Cookie', N'Baltazar Subd, Paniqui, Tarlac', N'(0922)8758431/(0922)31758431', N'arby_pablo@yahoo.com ', NULL, 597)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170598, N'Marilou Maniego', N'Princess Chewy', N'Poblacion Center, Ramos, Tarlac', N'(045)4917758/(0966)9746656', NULL, NULL, 598)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170599, N'Remedios Torio', N'Chiki', N'Tablang, Paniqui, Tarlac', N'9465723303', NULL, NULL, 599)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170600, N'Leonard Gamarcha', N'Chivas', N'Calapan, Moncada, Tarlac', N'(0995)3971879', NULL, NULL, 600)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170601, N'Karen Fernandez', N'Susie', N'JRG, Baltazar Subd., Paniqui, Tarlac', N'(0939)4279890', NULL, NULL, 601)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170602, N'Dexter Z. Ancheta', N'Mercedez/Ford', N'Mabini, Moncada, Tarlac', N'(0947)3012055', NULL, NULL, 602)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170603, N'Jingle Napuran', N'Chichi', N'Moncada, Tarlac', N'(0916)6512270', NULL, NULL, 603)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170604, N'Sarah Mei Quinto', N'Aki', N'Cabayaoasan, Paniqui, Tarlac', N'(0907)6386038', NULL, NULL, 604)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170605, N'Dennis Lamorena', N'Henry', N'Cabayaoasan, Paniqui, Tarlac', N'9476040653', NULL, NULL, 605)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170606, N'May Mallari', NULL, N'Camposanto Sur, Moncada, Tarlac', N'(0927)9688187', NULL, NULL, 606)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170607, N'Jeremy Munoz', N'Shadow', N'Paniqui, Tarlac', N'9311541', NULL, NULL, 607)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170608, N'Ruden Palasigue', N'MingMing', N'Sinigpit, Paniqui, Tarlac', N'(0919)4663547', NULL, NULL, 608)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170609, N'Eudenfil Atmosfera', N'Brock', N'Cayanga, Paniqui, Tarlac', N'9386441398', NULL, NULL, 609)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170610, N'Josephine DC Luis', N'Choco', N'Brgy. Burgos, Moncada, Tarlac', N'(0928)2132386', NULL, NULL, 610)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170611, N'Alberto Mendoza', N'Ligaya', N'Ventinilla, Paniqui, Tarlac', N'9208887843', NULL, NULL, 611)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170612, N'Rhiam Gamboa', NULL, N'Poblacion South, Ramos, Tarlac', N'9502884768', NULL, NULL, 612)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170613, N'Rioanne Sunshine Gallarda', N'Shinejenn', N'Tolega Norte, Moncada, Tarlac', N'(0998)4581818', NULL, NULL, 613)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170614, N'Warren Pangan', N'Kammuri', N'Sitio Basio, Poblacion Norte, Paniqui, Tarlac', NULL, NULL, NULL, 614)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170615, N'Princess Sarah Pagaduan', N'Matcha', N'Buenlag, Gerona, Tarlac', N'9558858961', NULL, NULL, 615)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170616, N'Albert Nelson Yasay', N'Jumbo', N'Cayanga, Paniqui, Tarlac', N'(0917)9122386', NULL, NULL, 616)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170617, N'Danica Bautista', N'Bimby', N'Guiteb, Ramos, Tarlac', N'(0956)3464669', NULL, NULL, 617)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170618, N'Marietta Ligsay Lacbayan', NULL, N'San Jose North, Anao, Tarlac', NULL, NULL, NULL, 618)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170619, N'Mara Gaculais', N'Misty', N'Matalapitap, Paniqui, Tarlac', N'(0907)3317984', NULL, NULL, 619)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170620, N'Edgardo Gabaldon', N'Pompom', N'Abogado, Paniqui, Tarlac', N'(0917)3443335', NULL, NULL, 620)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170621, N'Marilou Joly', N'Poppie', N'Sinigpit, Paniqui, Tarlac', N'9262161694', NULL, NULL, 621)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170622, N'Arvin Diaz', N'Rusty/Shawi', N'Capayaoan, Moncada, Tarlac', N'(0922)8895383', NULL, NULL, 622)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170623, N'Nora Barcela', NULL, N'Cabayaoasan, Paniqui, Tarlac', N'(045)9311868/(0927)6674809', NULL, NULL, 623)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170624, N'Celina Marie Cuchapin', N'Thunder', N'Capaoayan, Moncada, Tarlac', N'9129997183', NULL, NULL, 624)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170625, N'Adrian Harvey Ayes', N'Coco', N'0686 Maligaya St.,Poblacion Norte, Paniqui, Tarlac', N'(0906)4162910', NULL, NULL, 625)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170626, N'April Soriano', N'Ganda / Pogi', N'Camp.1 Norte, Moncada, Tarlac', N'9950219996', NULL, NULL, 626)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170627, N'Ale Concepcion ', N'Toothless', N'Pob. 3, Moncada, Tarlac', N'9062202698', NULL, NULL, 627)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170628, N'Patrizia Lucindo', N'Bruiser', N'Pob 2, Moncada, Tarlac', N'(0908)8818284', NULL, NULL, 628)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170629, N'Divine Advincula ', N'Yuki', N'Camposanto 2, Moncada, Tarlac', N'9304183765', NULL, NULL, 629)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170630, N'Karl Angelo Kho', N'Uno', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'9662768013', NULL, NULL, 630)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170631, N'Kit Dexter Osio', N'Berna', N'Sinense, Anao, Tarlac', N'(0946)6618691', NULL, NULL, 631)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170632, N'Ronald Daileg', N'Scott', N'Guiteb, Ramos, Tarlac', N'(0929)5878977', NULL, NULL, 632)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170633, N'Leah Parcia', N'Spike', N'Brgy. Plastado, Gerona, Tarlac', N'(045)4709856/(0977)4654922', NULL, NULL, 633)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170634, N'Jenny Ann Paracha', N'Blue', N'South East, Poblacion, Nampicuan, Nueva Ecija', N'(0948)2571859', NULL, NULL, 634)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170635, N'Jhonas Tayag', N'Cinnamon', N'Canan, Paniqui, Tarlac', N'9178329982', NULL, NULL, 635)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170636, N'Kate Angie Ann Rodriguez', N'Moji', N'Cayanga, Paniqui, Tarlac', N'(0997)2450719', NULL, NULL, 636)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170637, N'Joffrey Vinluan', NULL, N'Salumague, Paniqui, Tarlac', N'(0906)2222211', NULL, NULL, 637)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170638, N'Ma. Michaela Pinongcos', N'Kiara', N'Sta. Maria, Moncada, Tarlac', N'9663709957', NULL, NULL, 638)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170639, N'Apolonia Sy', N'Burdagul', N'Maluac, Moncada, Tarlac', N'9073968193', NULL, NULL, 639)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170640, N'Jeffrey Saplaco', N'Bella', N'Tablang, Paniqui, Tarlac', N'9204912096', NULL, NULL, 640)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170641, N'Marianelle Gatchalian', N'Maze', N'Poblacion North, Ramos, Tarlac', N'9454892023', NULL, NULL, 641)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170642, N'Camille Caberos', N'Barney', N'21 Abogado, Paniqui, Tarlac', N'(0917)8915864', NULL, NULL, 642)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170643, N'Almelyn  Lacamento', N'Hershey', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'(0945)3200079', NULL, NULL, 643)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170644, N'Charmie Crisostomo', N'Zoey', N'Poblacion Norte, Paniqui, Tarlac', N'9485816480', NULL, NULL, 644)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170645, N'Eddie Esteban', N'Baby Boy', N'348 Calle Abo St. , Estacion, Paniqui, Tarlac', N'(0939)2614401', NULL, NULL, 645)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170646, N'Elyssa Cuchapin Anito', N'Georgia', N'Poblacion 1, Moncada, Tarlac', N'9175053753', NULL, NULL, 646)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170647, N'Dang Dela Cruz', N'Coffee ', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9276668400', NULL, NULL, 647)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170648, N'Princes Rimocal', N'Juno', N'Abogado, Paniqui, Tarlac', N'(0927)9764186', NULL, NULL, 648)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170649, N'Juanita Santos', N'Buck ', N'Canan, Paniqui, Tarlac', N'9773786628', NULL, NULL, 649)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170650, N'Marie Anne Ocampo', N'Oreo', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'9323403431', NULL, NULL, 650)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170651, N'Juan Gabriel Ramos', N'Uno', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(045)93102019/(0956)6336449', NULL, NULL, 651)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170652, N'Krystal Mae Aquino', N'Pampam', N'San Felipe, San Manuel , Tarlac  ', N'9663128877', NULL, NULL, 652)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170653, N'Analyn Balignasay', N'Apple', N'Camposanto 1 Sur, Moncada, Tarlac', N'(0977)6272074', NULL, NULL, 653)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170654, N'Enrique Chichioco', N'Lilo', N'328 Del Rosario Subdivision, Paniqui, Tarlac', N'(045)9312870/(0918)9151356/(045)4931145', NULL, NULL, 654)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170655, N'Alma Cadiente', N'Jack', N'Rang - Ayan, Paniqui, Tarlac', N'9551096171', NULL, NULL, 655)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170656, N'Princes Larioza', N'Monty', N'Sta. Monica, Moncada, Tarlac', N'9276652881', NULL, NULL, 656)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170657, N'Erecka Piga', N'Bella ', N'Canan, Paniqui, Tarlac', N'9568528892', NULL, NULL, 657)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170658, N'Glandies Edmalin', N'Julia', N'Poblacion Norte, Paniqui, Tarlac', N'9176556051', NULL, NULL, 658)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170659, N'Ivan Kirby Baluyut', N'Misty', N'Poblacion Norte, Paniqui, Tarlac', N'9175086169', NULL, NULL, 659)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170660, N'Trixia  Andres', N'Kokoy/Kakay', N'216 Rizal St., District VIII, Cuyapo, Nueva Ecija', N'(0916)9539178', NULL, NULL, 660)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170661, N'Flordeliza Ancheta', N'Bogart', N'Florida St., Apulid, Paniqui, Tarlac', N'(0930)9160284', NULL, NULL, 661)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170662, N'Verjel Gonzales', N'Trina', N'Amacalan, Gerona, Tarlac', N'9386443828', NULL, NULL, 662)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170663, N'Cristieca May Andres', N'Chambe', N'217 Rizal St., District 8, Cuyapo, Nueva Ecija', N'(044)9514525/(0977)0826438', N'cristieca.andres@gmail.com ', NULL, 663)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170664, N'Brendon Ong', N'Moshi/Chuchi', N'Brgy. Tablang, Paniqui, Tarlac', N'(0946)8380672', NULL, NULL, 664)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170665, N'Angelika M. Ilingan', N'Akia', N'Brgy. Tablang, Paniqui, Tarlac', N'(0927)1294304', NULL, NULL, 665)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170666, N'Andreca Joy Andres', N'Coco/Luckie', N'217 Rizal St., District 8, Cuyapo, Nueva Ecija', N'(0906)2761500', NULL, NULL, 666)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170667, N'Princess Katherine Vergara', N'Olga', N'118 Camanggan East, Moncada, Tarlac', N'(0998)8574465', NULL, NULL, 667)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170668, N'Abigail Fernando', N'Paksiw', N'Plastado, Gerona, Tarlac', N'(0939)5810639', NULL, NULL, 668)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170669, N'Frank Tansingco', N'Shan', N'Poblacion 1, Moncada, Tarlac', N'9069612399', NULL, NULL, 669)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170670, N'Maricar Bascos', N'Nike ', N'Carriedo, Paniqui, Tarlac', N'(0927)1340170', NULL, NULL, 670)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170671, N'Rizalina Obcena', N'Chatsie', N'Apulid, Paniqui, Tarlac', N'9614005509', NULL, NULL, 671)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170672, N'Charmaine Salvador', N'Mucho', N'Balite, Pura, Tarlac', N'(0917)1767088', NULL, NULL, 672)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170673, N'John Robert Atibo', N'Spark', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9952023895', NULL, NULL, 673)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170674, N'Roderick Ramos', N'Toby', N'929 Zamora St. Purok  II, Poblacion Sur, Paniqui, Tarlac', N'(0966)3600347', NULL, NULL, 674)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170675, N'Mark Julius Calso', N'Tiny', N'Brgy. Abogado, Paniqui, Tarlac', N'(0923)4715392', NULL, NULL, 675)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170676, N'Jeric Villanueva', N'Brutus', N'Samput, Paniqui, Tarlac', N'(0912)9051167', NULL, NULL, 676)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170677, N'Lenardson Malonga', N'Samson', N'28 Poblacion 4, Moncada, Tarlac', N'(0936)8081302', NULL, NULL, 677)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170678, N'Jowell Antonio', N'Kenshi', N'Singat, Gerona, Tarlac', N'(0950)2869349', NULL, NULL, 678)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170679, N'Julie Ann Acuna', N'Pods', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0999)3564522', NULL, NULL, 679)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170680, N'Paul Stuart Fry', N'Dodger', N'Baquero North, Moncada, Tarlac', N'(0977)2336968', N'croconell@yahoo.com.uk', NULL, 680)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170681, N'Erlinda Vinluan', N'Cassandra', N'Cayanga, Paniqui, Tarlac', N'(0956)8537421', NULL, NULL, 681)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170682, N'Angela Chichioco', N'Nimbus', N'208B Villa Nadine, Acocolao, Paniqui, Tarlac', N'(045)9230650/(0910)9598404', NULL, NULL, 682)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170683, N'Mariz T. Flores', N'Toby', N'San Juan, Anao, Tarlac', N'(0916)5183285', NULL, NULL, 683)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170684, N'Jerille Navarro', N'Hash', N'Almaville Subd, Camanggan East, Moncada, Tarlac', N'(0947)7829330', NULL, NULL, 684)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170685, N'Andrea Obcena', N'Shalulu', N'Coral, Paniqui Tarlac', N'(0998)5585104', NULL, NULL, 685)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170686, N'Benny Pangilinan', N'Cutie', N'Campo Santo 1 Sur, Moncada, Tarlac', N'(0921)5098081', NULL, NULL, 686)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170687, N'Mark Kent Damasing', N'Lhinno Vladimir', N'Purok 3, Ventinilla, Paniqui, Tarlac', N'(0927)6393994', N'mckl_7@yahoo.com.ph', NULL, 687)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170688, N'Flordeliza Macaraeg', N'Snow', N'Coral, Ramos, Tarlac', N'(045)6284271/ (0921)7435854', NULL, NULL, 688)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170689, N'Grace Guinto', N'Shaya', N'Caturay, Gerona, Tarlac', N'(0916)4378658', N'msg_guinto15@yahoo.com', NULL, 689)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170690, N'Alexandria Yna Cudiamat', N'Lala/ Arleigh ', N'Capayaoan, Moncada, Tarlac', N'(0921)5905933/(0927)2809605', N'ynacudiamart@gmail.com ', NULL, 690)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170691, N'John Granil', N'Charlot', N'Caturay, Gerona, Tarlac', NULL, NULL, NULL, 691)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170692, N'Jenny O. Ular', N'Goldy', N'Brgy. Poblacion Sur, Paniqui, Tarlac', N'(0910)7803369', NULL, NULL, 692)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170693, N'Mary Rose Gatchalian ', N'Chou/Bruno/Harley', N'Tablang, Paniqui, Tarlac', N'(0995)2794731', NULL, NULL, 693)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170694, N'Daisy Mae Pangilinan', NULL, N'Del Pilar St., Paniqui, Tarlac', NULL, NULL, NULL, 694)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170695, N'Christian Joel Valdez', N'Yuki', N'Carriedo, Paniqui, Tarlac', N'(0950)3006843', NULL, NULL, 695)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170696, N'Carlo Ian Paras', N'Willa/Cassius', N'Maasin, Pura, Tarlac', N'(0928)5738187/(0995)689699', NULL, NULL, 696)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170697, N'Carolyn G. Carable', N'Summer', N'Moncada, Tarlac', N'(0916)5742514', NULL, NULL, 697)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170698, N'Johnny Fernandez', N'Drago', N'Nipaco, Paniqui, Tarlac', N'(045)6066100/ (0928)3206550', NULL, NULL, 698)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170699, N'Christopher Paligutan', N'Sarah', N'Camp 1, Norte, Moncada Tarlac', N'(0909)510698', NULL, NULL, 699)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170700, N'Chona Lambino', N'Maisie', N'Canan, Paniqui, Tarlac', N'(045)6061051/ (0921)6347624', NULL, NULL, 700)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170701, N'Lambert Lavitoria', N'Toby', N'179 Legazpi St., Paniqui, Tarlac', N'(0927)0894418', N'lavitorialambert@gmail.com ', NULL, 701)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170702, N'Roda Suniga', N'Clawney', N'Serrano, Coral, Ramos, Tarlac', N'(0917)9373501', NULL, NULL, 702)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170703, N'Desiree F. Po', N'Jimin', N'Brgy. Pinasling, Gerona, Tarlac', N'(0995)8261541', NULL, NULL, 703)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170704, N'Chester P. Chua', NULL, N'Ramos Subdivision, Paniqui, Tarlac', N'(0938)3462634', NULL, NULL, 704)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170705, N'Jesse Pagala', NULL, N'427 Burgos, Moncada, Tarlac', N'(0923)3937436', NULL, NULL, 705)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170706, N'Diana Caoctoy', N'Trixie', N'Moncada, Tarlac', N'(0905)7304218', NULL, NULL, 706)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170707, N'Mary Joy Ilano', N'Primo', N'Cabaducan East, Nampicuan, Nueva Ecija', N'(0930)0226587', NULL, NULL, 707)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170708, N'Charo Gonzales Oca', N'Quene', N'Samput, Paniqui, Tarlac', N'(0907)3315075', NULL, NULL, 708)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170709, N'Chloe Tagubuan', N'Cutie', N'109 B Villa Nadine Subd, Acocolao, Paniqui, Tarlac', N'(0928)1538629', NULL, NULL, 709)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170710, N'Diana Ayson', N'Lil Cookie', N'Camp #2, Moncada, Tarlac', N'(0917)5142367', NULL, NULL, 710)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170711, N'Adrian Panogan', N'Tsino', N'74 Cabaducan East, Nampicuan, Nueva Ecija', N'(0936)9170987', NULL, NULL, 711)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170712, N'Regine Salvador', N'Krypton', N'156 Nampicuan, Nueva Ecija', N'(0935)4564323', NULL, NULL, 712)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170713, N'Jasmin Melchor', N'Mocha', N'Abogado, Paniqui, Tarlac', N'(0998)1862360', NULL, NULL, 713)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170714, N'Marmie Mamucod', N'Yonggi', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0999)2233497', NULL, NULL, 714)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170715, N'Mercy Valdez', N'Tory/Tommy', N'Sitio Lubas, Sinigpit, Paniqui, Tarlac', N'(0939)5230788', NULL, NULL, 715)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170716, N'Charyn Brigitte Juan', N'Potchi', N'Canan, Paniqui, Tarlac', N'(0916)9177339', N'chachabelle13@yahoo.com ', NULL, 716)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170717, N'Jay Ryan Junio', N'Hershey', N'Manaois, Paniqui, Tarlac', N'(0906)2586783', NULL, NULL, 717)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170718, N'John Brix Silva', N'Ersa', N'Salumague, Paniqui, Tarlac', N' ', NULL, NULL, 718)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170719, N'Sean Enders Tinasas', N'John Snow', N'Estacion, Nampicuan, Nueva Ecija', N'(0908)2954187', NULL, NULL, 719)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170720, N'George Pagala III', N'Boss', N'Poblacion 4, Moncada, Tarlac', N'(0909)7888465 ', NULL, NULL, 720)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170721, N'Patricia Santiago', N'Maui', N'Patalan, Paniqui, Tarlac', N'(0908)8100719', NULL, NULL, 721)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170722, N'Aileen Marie Cabotaje', N'Kobie/Jacob', N'324 Brgy. Sta. Maria, Moncada, Tarlac', N'(0906)3364709', NULL, NULL, 722)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170723, N'Samantha Fhaye Garcia', N'Lucky', N'Ramos St., Carino, Paniqui, Tarlac', N'(0906)3906659', NULL, NULL, 723)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170724, N'Evan Saldua', N'Lexie', N'230 Del Valle St. , Estacion, Paniqui, Tarlac', N'(0998)2848562', NULL, NULL, 724)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170725, N'Tessie Mejia', NULL, N'Luna St. Paniqui, Tarlac', N'(0933)1049489', NULL, NULL, 725)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170726, N'Jessie Balmores', N'Pheobe', N'Moncada, Tarlac', NULL, NULL, NULL, 726)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170727, N'Marvin Castillo', N'Heart', N'20 Quezon St., Paniqui, Tarlac', N'(0916)9365460', NULL, NULL, 727)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170728, N'David Facun Jr.', NULL, N'Tagumbao, Gerona, Tarlac', N'(0946)5620489', NULL, NULL, 728)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170729, N'Omar Rufino', N'Zander', N'Canan, Paniqui, Tarlac', N'(0929)3839584', NULL, NULL, 729)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170730, N'Vilma Alejandrino', N'Khasper', N'Salumague, Paniqui, Tarlac', N'(0946)2187985', N'vialejandrino@gmail.com', NULL, 730)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170731, N'Alexa Rain Apoli', N'Izzy', N'Burgos st. Paniqui, Tarlac', NULL, NULL, NULL, 731)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170732, N'Adelaida Mejia', N'Potpot', N'Almaville Subd, Camanggan East, Moncada, Tarlac', N'(0917)5143690', N'aidamejia_mud@yahoo.com ', NULL, 732)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170733, N'Arcelli De Vera', N'Amaya/Chloe', N'Guiteb, Ramos, Tarlac', N'(0910)4351326/(0956)3810759', NULL, NULL, 733)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170734, N'Joylyn Eugenio', N'Indang', N'Salumague, Paniqui, Tarlac', N'(0946)7477850', NULL, NULL, 734)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170735, N'Clariss Cruz', N'Coco', N'Abogado, Paniqui, Tarlac', N'(0936)63839233', NULL, NULL, 735)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170736, N'Jeane Ofrecio', N'Chowder', N'Acocolao, Paniqui, Tarlac', N'(0908)1474631', NULL, NULL, 736)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170737, N'Mary Grace Maglalang', N'Sam', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0947)1039064', NULL, NULL, 737)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170738, N'Joanne Generao', N'Kyro', N'San Isidro, Paniqui, Tarlac', N'(0917)225981', NULL, NULL, 738)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170739, N'Cristina Bernal', N'Churvee', N'Moncada, Tarlac', N'(0917)1409289', NULL, NULL, 739)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170740, N'Dean Pagatpatan', N'Chase', N'Maybubon, Guimba, Nueva Ecija ', N'(0909)1826308(0948)2015828', NULL, NULL, 740)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170741, N'Jenica Valdez', N'Robin', N'Sinigpit, Paniqui, Tarlac', N'(0956)7268722', N'enziesotto@gmail.com', NULL, 741)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170742, N'Reynan De Guzman', N'Richie', N'0333 Carapdapan, Poblacion Norte, Paniqui, Tarlac', N'(0948)5293884', NULL, NULL, 742)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170743, N'Maricel Soto', N'Alice', N'Brgy. San Jose North Anao, Tarlac', N'(0948)9757474', NULL, NULL, 743)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170744, N'Reynaldo Roxas', N'Putot', N'Abogado, Paniqui, Tarlac', N'(0946)6090566', NULL, NULL, 744)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170745, N'Roldan P. De Vera', N'Casper', N'Maybubon, Guimba, Nueva Ecija ', N'(0938)8606463', NULL, NULL, 745)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170746, N'Marilyn Ramos', NULL, N'Acocolao, Paniqui, Tarlac', NULL, NULL, NULL, 746)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170747, N'Kathrina C. Reyes', N'Basha', N'Pob. Center, Ramos, Tarlac', N'(0908)9723794', NULL, NULL, 747)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170748, N'Gloria Marie Lou Panay', N'Tisay', N'04 Quezon St., Paniqui, Tarlac', N'(0906)9610128', NULL, NULL, 748)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170749, N'Mery Rose Orcino', N'Bela ', N'Apulid, Paniqui, Tarlac', N'(0998)2296416', NULL, NULL, 749)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170750, N'Romeo Mamucod', NULL, N'Samput, Paniqui, Tarlac', N'(0956)0070758', NULL, NULL, 750)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170751, N'Rhodora Vigilia', N'Lily', N'San Francisco West, Anao, Tarlac', NULL, NULL, NULL, 751)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170752, N'Emilia Cabaris/Edgar Scott', NULL, N'Salumague, Paniqui, Tarlac', N'(0929)4862251', NULL, NULL, 752)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170753, N'Dakila Tiu', N'Brandy', N'Pob 3, Moncada, Tarlac', N'(0915)64599090', NULL, NULL, 753)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170754, N'Rosa Ann Macadangdang', N'Homer', N'Patalan, Paniqui, Tarlac', N'(0932)2211013', NULL, NULL, 754)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170755, N'Alma Ramos', N'Kisshia', N'Coral , Ramos , Tarlac', N'(0917)1309422', NULL, NULL, 755)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170756, N'Catherine Castillo', N'Happy', N'Brgy. Tablang, Paniqui, Tarlac', N'(0955)3363395', NULL, NULL, 756)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170757, N'Alejo Pacunayen', N'Diego', N'Tolega Norte, Moncada, Tarlac', N'(0908)7330787', NULL, NULL, 757)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170758, N'Lisa Palasigue', N'Miyawie', N'59 Sinigpit St., Paniqui, Tarlac', N'(0925)6572328', NULL, NULL, 758)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170759, N'Jackyline Antonio', N'Catriona', N'0203 Luna St., Poblacion Norte , Paniqui, Tarlac', N'(0906)2047549', NULL, NULL, 759)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170760, N'Arionn Dale Galang', N'Brandy', N'105 Villa Socorro, Paniqui, Tarlac', N'(0945)1177162', NULL, NULL, 760)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170761, N'Gene Peridas', N'Cloe', N'Samput, Paniqui, Tarlac', N'(0921)7162571', NULL, NULL, 761)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170762, N'Rocky Simbre Jr.', N'Speedot', N'Balaoang, Paniqui, Tarlac', N'(0966)7958194', NULL, NULL, 762)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170763, N'Lizel Basa', N'Phiona', N'99 Colorado St., Apulid, Paniqui, Tarlac', N'(0995)2376093', NULL, NULL, 763)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170764, N'Princess Erica Martinez', N'Maxx/Guiget', N'Carino, Paniqui, Tarlac', N'(0995)8512509', NULL, NULL, 764)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170765, N'Melanie Limbo', N'JhoJho', N'Brgy. Lapsing, Moncada, Tarlac', N'(0910)0361348', NULL, NULL, 765)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170766, N'Micah Ruth Galang', N'Max', N'Nagmisahan, Cuyapo , Nueva Ecija', N'(0936)7342069', NULL, NULL, 766)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170767, N'Angelo Zonio', N'Empoy', N'Cayanga, Paniqui, Tarlac', N'(0997)5801390', NULL, NULL, 767)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170768, N'Noel Esteban', N'Spike', N'San Felipe, San Manuel , Tarlac  ', N'(0917)8142299', NULL, NULL, 768)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170769, N'Irvin Gerard Chanchico', N'Henry', N'Sta. Ines, Paniqui, Tarlac', N'(0977)8208940', NULL, NULL, 769)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170770, N'Bernadine Bartolo', N'Candy', N'Villa Sto. Nino Subd., Sta Cruz, Tarlac City', N'(045)6281945/ (0910)0340123', N'bartbern15@yahoo.com', NULL, 770)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170771, N'Aileen S. Santiago', N'Choco', N'07 Purok Sunflower, Brgy. Plastado, Gerona, Tarlac', N'(0917)7933520', NULL, NULL, 771)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170772, N'Sherie Anne Conde', N'Coco', N'Nampicuan, Nueva Ecija', N'(0966)1802512', NULL, NULL, 772)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170773, N'Joana Marie Carrasco', N'Kitty', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0930)5610553', NULL, NULL, 773)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170774, N'Jasper Clein Marquez', N'Masha', N'Estacion, Paniqui, Tarlac', N'(0956)8538566', NULL, NULL, 774)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170775, N'Antonio Rombaoa', N'Panda', N'Patalan, Paniqui, Tarlac', N'(0927)2532964', NULL, NULL, 775)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170776, N'Valerie Mila', N'Icy/ Hamham', N'Sitio Caniogan, Matalapitap, Paniqui, Tarlac ', N'(0933)8668210', NULL, NULL, 776)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170777, N'Amelia Bulandi', N'Choco', N'Nampicuan, Nueva Ecija', N'(0908)9932034', NULL, NULL, 777)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170778, N'Minerva Lambino', N'Scarlet', N'Canan, Paniqui, Tarlac', N'(0922)4234371', NULL, NULL, 778)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170779, N'Michelle Gapasin ', N'TJ', N'Tablang, Paniqui, Tarlac', N'(0919)1912930', NULL, NULL, 779)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170780, N'Rechelle Astrero', N'Roxxy', N'Sta. Rosa St., Paniqui, Tarlac', N'(0945)4155690', NULL, NULL, 780)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170781, N'Christian Joseph Protacio', N'Akiro', N'San Vicente De Amor, Tarlac City', N'(0977)4637463', NULL, NULL, 781)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170782, N'Letty Santos', N'Snow White', N'131 Burgos St. Tablang, Paniqui, Tarlac', N'(0998)9187413', NULL, NULL, 782)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170783, N'Erika Rose David', N'Tyler', N'Pob 1, Moncada, Tarlac', N'(0905)4412899', NULL, NULL, 783)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170784, N'Francis Arandia', N'Amboy', N'Dapdap,Paniqui, Tarlac', N'(0928)8554347', NULL, NULL, 784)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170785, N'Dangill Valdez', N'Uno', N'Mabini, Moncada, Tarlac', N'(0912)6787203', NULL, NULL, 785)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170786, N'Maiko Gowen Rebolledo', N'Elli', N'Cayanga, Paniqui, Tarlac', N'(0945)7782272', NULL, NULL, 786)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170787, N'Nica Mangabat', N'Mia', N'Samput, Paniqui, Tarlac', N'(0946)6069559', NULL, NULL, 787)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170788, N'Gloria Estanoco', N'Bruno', N'Tablang, Paniqui, Tarlac', N'(0935)5758987', N'khine.estanoco@gmail.com', NULL, 788)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170789, N'Malou Chu', N'Summer', N'167 MH Del Pilar, Paniqui, Tarlac', N'(0943)2660481', NULL, NULL, 789)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170790, N'Alyssa Vidaira', N'Lixi', N'Canan, Paniqui, Tarlac', N'(0927)6949005', NULL, NULL, 790)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170791, N'Gena Grospe', N'Bruno', N'Balaoang, Paniqui, Tarlac', N'(0905)7512709', N'joseph_anotnio@yahoo.com', NULL, 791)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170792, N'Elgin Sales', N'Snappy', N'South West, Poblacion, Nampicuan, Nueva Ecija', N'(0926)6105031', NULL, NULL, 792)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170793, N'Jeddah Pena', N'Minnie', N'Guiteb, Ramos, Tarlac', N'(0966)3723919', NULL, NULL, 793)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170794, N'Dan Sarsagat', N'Autumn', N'Salumague, Paniqui, Tarlac', N'(0915)9708640', NULL, NULL, 794)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170795, N'Hallmark Ramos', N'Mucho', N'Brgy. Acocolao, Paniqui, Tarlac', N'(0915)6120575', NULL, NULL, 795)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170796, N'Kim Martinez', N'Happy', N'Carino, Paniqui, Tarlac', N'(045)9312794/(0909)7053784', NULL, NULL, 796)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170797, N'Rhommell Gabriel', N'Voopoo', N'Poblacion South, Ramos, Tarlac', N'(0929)3371822', NULL, NULL, 797)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170798, N'Juris Garcia', N'Willow', NULL, N'(045)6069844/(0919)5736641', NULL, NULL, 798)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170799, N'Kristel Joy Galamay', N'Charlie', N'Ayson, Gerona, Tarlac ', N'(0936)5636982', NULL, NULL, 799)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170800, N'Aldwin Nicole Tungol', N'London', N'35 Nancamarinan, Paniqui, Tarlac', N'(0907)7878080', NULL, NULL, 800)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170801, N'Elizer Lagman', N'Teemo', N'Abogado, Paniqui, Tarlac', N'(0916)5042878', NULL, NULL, 801)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170802, N'Lyka Mendoza', N'Fifi', N'80 Acocolao, Paniqui, Tarlac', N'(0906)1546123', NULL, NULL, 802)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170803, N'Marcelino De Jesus', N'Ember', N'Brgy. North Poblacion, Nampicuan, Nueva Ecija', N'(0995)2946438', NULL, NULL, 803)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170804, N'Bryan Sy', N'Maxene', N'Pob. South, Ramos, Tarlac', N'(0945)2459686', NULL, NULL, 804)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170805, N'Apple May Abril', N'Lucky', N'Guiteb, Ramos, Tarlac', N'(0956)8532082', NULL, NULL, 805)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170806, N'Edward Reginaldo', N'Boomer', N'Pance, Ramos, Tarlac', N'(0920)9465079', NULL, NULL, 806)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170807, N'Sarine Casupanan', N'Chase', N'Abogado, Paniqui, Tarlac', N'(0919)5099638', NULL, NULL, 807)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170808, N'Angelica Tionghoy', N'Toby', N'Samput, Paniqui, Tarlac', N'(0909)2751547', NULL, NULL, 808)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170809, N'Joana Marie Obena', N'Zoey', N'Pob. South, Ramos, Tarlac', N'(0926)6254672', NULL, NULL, 809)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170810, N'Joel Almazar', N'Polong', N'Del Pilar St., Paniqui, Tarlac', N'(045)4709320/(0998)5444641', NULL, NULL, 810)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170811, N'Jessica Macaraeg', N'Chloie', N'Gomez St, Pob Norte, Paniqui, Tarlac', N'(045)4708633/ (0997)2450199', NULL, NULL, 811)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170812, N'Ivern Dela Cruz', N'Molly', N'1721 Garcia Subd. Pob Norte, Paniqui, Tarlac', N'(0956)0756487', NULL, NULL, 812)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170813, N'Ginabeth Bazar', N'Kovi', N'Nampicuan, Nueva Ecija', N'(0917)3221377', NULL, NULL, 813)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170814, N'Maria Crista Morano', N'Lyra', N'Burgos st. Paniqui, Tarlac', N'(0906)2127048', NULL, NULL, 814)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170815, N'Bryan Milla', N'Toni', N'Gerona, Tarlac', N'(0965)4063582', NULL, NULL, 815)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170816, N'Ma. Kri-zel E. Macaraeg', N'Yolo', N'441 Carapdapan St., Poblacion Norte, Paniqui, Tarlac', N'(0995)8263266', NULL, NULL, 816)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170817, N'Danilo Juan Jr.', NULL, N'Apulid, Paniqui, Tarlac', N'(0999)7737566', NULL, NULL, 817)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170818, N'Ma. Eloisa Dante', N'Oreo', N'Purok Aksiyon, Coral, Ramos, Tarlac', N'(0912)9655550', N'eloisadante25@gmail.com', NULL, 818)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170819, N'Mario Gaña', N'Alix/Red', N'Samput, Paniqui, Tarlac', N'(0947)8909354', NULL, NULL, 819)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170820, N'Ma. Theresa Dejesa', N'Maxy', N'Rang - Ayan, Paniqui, Tarlac', N'(0905)3046998', NULL, NULL, 820)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170821, N'Karen Joyce Ramos', N'Stacey', N'Balite, Pura, Tarlac', N'(0915)91721014', NULL, NULL, 821)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170822, N'Jeralyn De Guzman', N'Charls', N'Moncada, Tarlac', N'(0916)5752649', N'djeralyn75@yahoo.com', NULL, 822)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170823, N'Eduardo Daus', N'Bela ', N'Coral, Paniqui Tarlac', N'(0945)5749330', N'arnedenluvcd@gmail.com', NULL, 823)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170824, N'Alma Tan', N'Cha-Cha', N'San Isidro, Paniqui, Tarlac', N'(0916)7546203', N'jerometan789@gmail.com', NULL, 824)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170825, N'Ryan Kylle Malanot', N'Hershey', N'Maliwalo, Lumina Homes, Tarlac', N'(0977)3242926', N'ryangania20@gmail.com', NULL, 825)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170826, N'Angel Baret', N'Browner', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0906)5220095', N'baretangel@yahoo.com', NULL, 826)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170827, N'Jessie Gonzales', N'Echo', N'Paniqui, Tarlac', N'(0946)0558118', NULL, NULL, 827)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170828, N'John Lacar', N'Snow Delo', N'40 Brgy. Carmen, Anao, Tarlac', N'(0915)2818625', NULL, NULL, 828)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170829, N'Jose Lagman', N'Claude', N'175 Tablang, Paniqui, Tarlac', N'(0909)7807907', NULL, NULL, 829)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170830, N'Mark Anthony Alarcon/Faye Ladislao', N'Bumblee Bee', N'Tolega Sur, Moncada, Tarlac', N'(0910)0352311', NULL, NULL, 830)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170831, N'Christine Joy Soriano', N'Misty', N'Pob. North, Ramos, Tarlac', N'(0916)5890349', NULL, NULL, 831)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170832, N'Cherilyn Bontog', N'Buchi', N'Aringin, Moncada Tarlac', N'(0935)6910091', NULL, NULL, 832)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170833, N'Anna Marissa Balisalisa', N'Soondol', N'252 Samput, Paniqui, Tarlac', N'(045)9310339/(0915)7158125', NULL, NULL, 833)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170834, N'Dina Rivera', N'Gucci', N'Acocolao, Paniqui, Tarlac', N'(0998)4435477', NULL, NULL, 834)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170835, N'Rhea O. Dela Cruz', N'Princess ', N'Camp 1, Sur, Moncada Tarlac', N'(0921)3052276', NULL, NULL, 835)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170836, N'Janet D. Imlano', N'Pluto', N'San Julian, Moncada, Tarlac', N'(0946)7995081', NULL, NULL, 836)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170837, N'Reynato Tayag Cruz', N'Good', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0948)448998', NULL, NULL, 837)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170838, N'Leinahtan Sarmiento', N'Nala', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0966)6142635', N'Leisarmiento17@yahoo.com', NULL, 838)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170839, N'Jimber t. Bongon', N'Storm', N'Coral , Paniqui, Tarlac', N'(0915)3985734', NULL, NULL, 839)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170840, N'Francis Bacani', N'Blackie ', N'Paiqui, Tarlac', NULL, NULL, NULL, 840)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170841, N'Nelson Veloria', N'Tammy', N'Sinigpit, Paniqui, Tarlac', N'(0939)1306955', NULL, NULL, 841)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170842, N'Joy Ann D. Javier', N'Molly', N'Samput, Paniqui, Tarlac', N'(0946)9808049', NULL, NULL, 842)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170843, N'Aurora Maristela Duya', NULL, N'M. H. Del Pilar St., Estacion, Paniqui, Tarlac', N'(045)9310316', NULL, NULL, 843)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170844, N'Jonses Arrozal', N'Kenly', N'Gerona, Tarlac', N'(0997)5022218', NULL, NULL, 844)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170845, N'Marife Allauigan', N'Hamper', N'Apulid, Paniqui, Tarlac', N'(0905)1430112', NULL, NULL, 845)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170846, N'Hannah Mei Labasan', N'Maxx', N'Rang - Ayan, Paniqui, Tarlac', N'(0909)6509059', NULL, NULL, 846)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170847, N'Michelle Lamorena', N'Mateo', N'Cabayaoasan, Paniqui, Tarlac', N'(045)9312267/(0920)4788759', NULL, NULL, 847)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170848, N'Carmencita Macatangay', N'Shiki/Takeshi', N'99 Purok 3, Salumague, Paniqui, Tarlac', N'(045)6285264/(0915)7875739', N'carmencitamacatangay@yahoo.com', NULL, 848)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170849, N'Mary Ann Ramos', N'Yuri', N'Almaville Subd, Camanggan East, Moncada, Tarlac', N'(0920)2182012', NULL, NULL, 849)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170850, N'Alma P. Banaag', N'Tank', N'Tablang, Paniqui, Tarlac', N'(0920)6067939', NULL, NULL, 850)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170851, N'Kamyll Manzano', N'Molly/Cooper', N'Manaois, Paniqui, Tarlac', N'(0939)7891331', N'kirstiemanzano09@gmail.com', NULL, 851)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170852, N'Madilyn Bona Canlas', N'Jacob', N'42 Legazpi St. Paniqui, Tarlac', N'(0910)6976688', NULL, NULL, 852)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170853, N'Marichris Carballo', N'Luna/Sanha', N'170 Carapdapan St., Poblacion Norte', N'(0906)1877075', N'titanitey@gmail.com', NULL, 853)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170854, N'Nicole Arabella Abandiano', N'Adonis', N'677 Cabayaoasan, Paniqui, Tarlac', N'(0977)3513765', N'nicoleabandiano@yahoo.com', NULL, 854)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170855, N'Ellen Grace Palacbayan', NULL, N'Poblacion 3 , Moncada, Tarlac', N'(045)6063298/(0912)9989394', NULL, NULL, 855)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170856, N'Jennie Dean', N'Sparky', N'Sta. Maria, Moncada, Tarlac', N'(0908)8629004', NULL, NULL, 856)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170857, N'Steffanie Lising', N'Kim', N'Salumague, Paniqui, Tarlac', N'(0927)4417479', NULL, NULL, 857)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170858, N'Jessica Nool', N'Mochi', N'Carino, Paniqui, Tarlac', N'(0915)5934844', NULL, NULL, 858)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170859, N'Louisse Julianne Bronio', N'Pancho', N'Burgos st. Moncada, Tarlac', N'(0917)8799005', NULL, NULL, 859)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170860, N'Debby Rae San Miguel', N'Missy', N'414 Villa Nadine Subd. Brgy. Acocolao, Paniqui, Tarlac', N'(0945)3348474', NULL, NULL, 860)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170861, N'Jun Tigon', N'Noah', N'Ventinilla, Paniqui, Tarlac', N'(0921)8910202', NULL, NULL, 861)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170862, N'Alma Roxas', N'Riona', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'(0917)6763968', NULL, NULL, 862)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170863, N'Sheena Jane Aguila', N'Blessing', N'41 A Ancheta St. Brgy. Latap, Cuyapo, Nueva Ecija', N'(0933)5366505', N'sheenajane_aguila@gmail.com', NULL, 863)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170864, N'Kyle Nicole Libunao', N'Oreo', N'Sinigpit, Paniqui, Tarlac', N'(0936)8674755', NULL, NULL, 864)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170865, N'Joyce Favis', N'Acee', N'Camp 1, Sur, Moncada Tarlac', N'(0995)7527836', NULL, NULL, 865)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170866, N'Melvrine Jeric Medrano', N'Rico', N'Carino, Paniqui, Tarlac', N'(0938)6585359', N'melvrinejericmedrano@yahoo.com', NULL, 866)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170867, N'Kristel Reyes', N'Kianna', N'Canan, Paniqui, Tarlac', N'(0935)2933274', N'reyeskristel128@gmail.com', NULL, 867)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170868, N'Danielle Capinding', N'Skye', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'(0965)7467925', NULL, NULL, 868)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170869, N' Von Aganon', N'Vivo', N'Purok Maligaya, Poblacion Norte, Paniqui, Tarlac', N'(0947)4809164', N'whiterascalskingsspade@gmail.com', NULL, 869)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170870, N'Renato Basco', N'Kisses', N'Coral, Paniqui Tarlac', N'(0966)3708527', NULL, NULL, 870)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170871, N'Mark Uriel Macadangdang', N'Mauwi', N'Green Meadows, Samput, Paniqui, Tarlac', N'(0995)9809254', NULL, NULL, 871)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170872, N'Ma. Theresa Santos', N'Yuki', N'Anao, Tarlac', N'(0906)5501188', NULL, NULL, 872)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170873, N'Romeo Ardael', N'Dot', N'Centro Acocolao, Paniqui, Tarlac', N'(045)4709959/ (0948)3759804', NULL, NULL, 873)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170874, N'Maribeth Millado', N'Yassie', N'Ramos, Tarlac', N'(0998)5818756', NULL, NULL, 874)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170875, N'Dhon Gabriel', N'Bella', N'Apulid, Paniqui, Tarlac', N'(0936)8621805', NULL, NULL, 875)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170876, N'Christine Caro', N'Gecko', N'Rizal, Moncada, Tarlac', N'(045)6066458/ (0956)9725222', NULL, NULL, 876)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170877, N'Marife Villanueva', N'Itlog', N'Coral, Paniqui Tarlac', N'(0912)1014483', NULL, NULL, 877)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170878, N'Richard Allan Tan', N'Heaven', N'108 Coral, Paniqui, Tarlac', N'(0950)9058881', NULL, NULL, 878)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170879, N'Caridad Mahor', N'Right', N'Capaoyan, Moncada, Tarlac', NULL, NULL, NULL, 879)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170880, N'Diosdado Asio', N'Covid', N'Bugallon Estacion, Paniqui, Tarlac', N'(0920)6222436', NULL, NULL, 880)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170881, N'Mark Anthony Primero', N'Blue Hailey', N'37 Poblacion, Anao, Tarlac', N'(0998)2849831', NULL, NULL, 881)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170882, N'Ma. Angela Aranzaso', N'Luna Belle', N'Cabayaoasan, Paniqui, Tarlac', N'(0917)8217165', N'may_5780@yahoo.com', NULL, 882)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170883, N'Jonalyn Fontanillas', N'Puppy', N'Pance, Ramos, Tarlac', N'(0930)5557036', NULL, NULL, 883)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170884, N'Elsie Nuguid', N'Ashley', N'357 Samput, Paniqui, Tarlac', NULL, NULL, NULL, 884)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170885, N'Rizza Fernandez', N'Summer', N'147 Matalapitap, Paniqui, Tarlac', N'9563865372', NULL, NULL, 885)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170886, N'Denia Benicta', N'jack', N'Acocolao, Paniqui, Tarlac', N'(0917)7340862', NULL, NULL, 886)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170887, N'Janine Lapurga', N'Thalia Baileys', N'Bonifacio St., San Julian, Moncada, Tarlac', N'(0915)3928328', N'ZTRaDENINAJ_27@yahoo.com', NULL, 887)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170888, N'Ronjie Bucasas', N'Arf-Arf', N'Dicolor, Gerona, Tarlac', N'(0918)4679267/ (0918)9990046', NULL, NULL, 888)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170889, N'Aldrene Jay Alfaro', N'Volt', N'Cayanga, Paniqui, Tarlac', N'(0932)4060133', NULL, NULL, 889)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170890, N'Sean Francis Facun', N'Jarvis', N'Nancamarinan, Paniqui, Tarlac', N'(0938)8787397/(0918)5071475', NULL, NULL, 890)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170891, N'Joanne Kaye Mercado', N'Maze', N'119 Vigilia St. Sta. Maria, Moncada, Tarlac', N'(045)9234200/(0926)0921263', NULL, NULL, 891)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170892, N'Jenifer Beltran', N'Mickey', N'Patalan, Paniqui, Tarlac', N'(0910)8910025', NULL, NULL, 892)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170893, N'Raquel M. Alegado', N'Cooper', N'167 Ventinilla, Paniqui, Tarlac', N'(0998)2235563', NULL, NULL, 893)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170894, N'Ramon Briones', N'Rosey', N'Abogado, Paniqui, Tarlac', N'(0921)2205956', NULL, NULL, 894)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170895, N'Shiena De Guzman', N'Koobie', N'Colibangbang, Paniqui, Tarlac', N'(0917)1398107', NULL, NULL, 895)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170896, N'Lauren Carpio', N'Monday', N'Lacayanga Subd., Paniqui, Tarlac', N'(0956)9921039', NULL, NULL, 896)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170897, N'Neriza Enmocino', N'Masha', N'Cabaducan East, Nampicuan, Nueva Ecija', N'(0995)7460651', NULL, NULL, 897)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170898, N'Hessie San Diego', N'Zoe', N'MH Del Pilar St. Paniqui, Tarlac', N'(0946)0541457', NULL, NULL, 898)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170899, N'Hanna Esteban', NULL, N'123 San Francisco West, Anao Tarlac', N'(0998)0010063', NULL, NULL, 899)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170900, N'Antonine Palancia', N'Karry', N'Pob. South, Ramos, Tarlac', N'(0933)8164191', NULL, NULL, 900)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170901, N'Christopher M. Obcena', N'Axel', N'Poblacion Norte, Paniqui, Tarlac', N'(0995)2226330', NULL, NULL, 901)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170902, N'Rommel/Janine Abril', N'Jarvis', N'St. Bernard Subd., Abogado, Paniqui, Tarlac', N'(0905)9744118', NULL, NULL, 902)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170903, N'Michael Jeorge Arellano', N'Zoey', N'Poblacion #1, Moncada, Tarlac', N'(0936)9659615', NULL, NULL, 903)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170904, N'Noemi Castillo', N'Oreo', N'Purok 3, Samput, Paniqui, Tarlac', N'(0907)8597541', NULL, NULL, 904)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170905, N'Joceyn Patriarca', N'Neggy', N'646 Caburnay St. Paniqui, Tarlac', N'(0999)5066243', NULL, NULL, 905)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170906, N'Ryan Ondevilla', NULL, N'Brgy. Cayanga, Paniqui, Tarlac', N'(0906)3293056', NULL, NULL, 906)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170907, N'Jeoffrey Diza', N'Popoii', N'Poblacion, San Manuel, Tarlac', N'(0936)5116679', NULL, NULL, 907)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170908, N'Redentor Bacaron', NULL, N'Mabini, Moncada, Tarlac', N'(0927)0617365', NULL, NULL, 908)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170909, N'Jon Rafael Pazcoguin', N'Choullo', N'Estacion, Paniqui, Tarlac', N'(0935)0443531', N'jonrafaelpazcoguin@yahoo.com', NULL, 909)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170910, N'Launel Tiria', N'Magtanggol', N'Apulid, Paniqui, Tarlac', N'(0936)8856078', NULL, NULL, 910)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170911, N'Marian De Lara', N'Tyzon/Beauty', N'418 Estacion, Paniqui, Tarlac', N'(0977)8134965', NULL, NULL, 911)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170912, N'Laarni L. Viray', N'Tommy', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0918)6584614', NULL, NULL, 912)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170913, N'Carmelita Quines', N'Bruno', N'Brgy. Culabot, San Manuel, Tarlac', N'(0917)5588622', NULL, NULL, 913)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170914, N'Marilyn Alonzo', N'George', N'Poblacion Norte, Paniqui, Tarlac', N'(0922)445218', NULL, NULL, 914)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170915, N'Patricia Grace Medina', N'Kalani', N'Tolega Sur, Moncada, Tarlac', N'(0930)0229638', NULL, NULL, 915)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170916, N'Grace Valdez', N'Conney', N'Apulid, Paniqui, Tarlac', N'(0977)2692393', NULL, NULL, 916)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170917, N'Jeanibeth Yebes', N'Fudge', N'Purok 7,Manaois, Paniqui, Tarlac', N'(0946)7460527', NULL, NULL, 917)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170918, N'Glenda Caseria', N'Gray', N'Estacion, Paniqui, Tarlac', N'(0919)9836022', NULL, NULL, 918)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170919, N'Lovely Joy Marquez', N'Yami', N'Pasuquin St., Paniqui, Tarlac', N'(0956)6085649', NULL, NULL, 919)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170920, N'Karol Naval', N'Royce', N'Ramos, Tarlac', N'(045)4917343/(0917)8567842', NULL, NULL, 920)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170921, N'Marco David', N'Godard', N'Green Meadows, Samput, Paniqui, Tarlac', NULL, NULL, NULL, 921)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170922, N'Kristina Cassandra Romeo', N'Twinkie', N'Samput, Paniqui, Tarlac', N'(0936)8558238', NULL, NULL, 922)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170923, N'Kaye Belarmino', N'Magi', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'(045)9310955/(0956)4173424', NULL, NULL, 923)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170924, N'Ma. Katrina Verceles', N'Luna/Lucho', N'Poblacion, Moncada, Tarlac', N'(0927)3530169', NULL, NULL, 924)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170925, N'Janine Facun', N'Shake', N'269 Samput, Paniqui, Tarlac', N'(0946)6421477', NULL, NULL, 925)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170926, N'Sherwin Santillan', NULL, N'Coral, Paniqui Tarlac', N'(0926)0990505', NULL, NULL, 926)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170927, N'Mellany Malanot', N'Caddy/Hershey', N'Blk. 9, Lot 24, Lumina Homes, Tarlac City', N'(0906)0287697', NULL, NULL, 927)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170928, N'Analyn Arcega', N'Chunley', N'Apulid, Paniqui, Tarlac', N'(0995)1938218', NULL, NULL, 928)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170929, N'Christine Dangpas', N'Ged', N'San Vicente, San Manuel, Tarlac', N'(0927)2159164', NULL, NULL, 929)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170930, N'Anna Loraine B. De Guzman', N'Scarlett', N'#50 Carriedo St. Paniqui', N'(0945)7783601', NULL, NULL, 930)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170931, N'Keith Ann Rochelle D. Castro', N'Snow white', N'Abagon, Gerona, Tarlac', N'9984941400', NULL, NULL, 931)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170932, N'Christian/Grace Ramos', N'Kyla', N'#75 Corpuz St. Dist. 07 Cuyapo, Nueva Ecija', N'(0995)5227029', NULL, NULL, 932)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170933, N'Gemmelyn D. Rombaoa', N'Myley', N'47 Salomague, Paniqui Tarlac', N'(0920)9467798', NULL, NULL, 933)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170934, N'Chester M. Tiedad', N'Nigel', N'Samput, Paniqui, Tarlac', N'(0948)9959011', NULL, NULL, 934)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170935, N'Nenita Valdez', N'Spider', N'Pob. # 1 Moncada Tarlac', N'(0939)6138681', NULL, NULL, 935)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170936, N'Godwin Ivan A. Atencio', N'Harith', N'Banaong West, Moncada Tarlac', N'(0926)7404354', NULL, NULL, 936)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170937, N'Kristel Venice Somera', N'Red', N'Baltazar Subd, Paniqui, Tarlac', N'(0956)6910492', NULL, NULL, 937)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170938, N'Marisol Cardel', N'Junior', N'Campo Sto. 1 Norte Moncada Tarlac', N'(0950)0375273', NULL, NULL, 938)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170939, N'Chichioco Olive', N'Wada', N'309 Estacion Paniqui Tarlac', N'(0918)2419019', NULL, NULL, 939)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170940, N'Cristobal M. De Leon', N'Blessie', N'Caturay, Gerona, Tarlac', N'(0999)1525606', NULL, NULL, 940)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170941, N'Jobell Ann D. Dumayag', N'Harry ', N'Cuyapo, Nueva Ecija', N'(0916)2099360', NULL, NULL, 941)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170942, N'Jessybel Cedeno', N'Kitkat', N'Coral, Paniqui Tarlac', N'(0950)3019831', NULL, NULL, 942)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170943, N'Chelsi Pagala', N'Charlie', N'Burgos, Moncada, Tarlac', N'(0938)7757977', NULL, NULL, 943)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170944, N'Madeliene Mamaril', N'Tibers', N'Abogado, Maligaya Paniqui Tarlac', N'(0999)3562495/(0921)9524622', NULL, NULL, 944)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170945, N'Reynaldo F. Andaya', N'Oreo', N'83 M.H Del Pilar St. Paniqui Tarlac', N'(0936)0460110', NULL, NULL, 945)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170946, N'Geraldine A. Espejo', N'Brownie', N'Brgy. Salomague, Paniqui, Tarlac', N'(0929)8045060/931-1534', NULL, NULL, 946)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170947, N'Angelica Acosta', N'Buster', N'Baltazar Subd, Pob. Norte Paniqui, Tarlac', N'(0936)8674376', NULL, NULL, 947)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170948, N'Monique Roxas', N'Daiki', N'III Estacion, Paniqui Tarlac', N'(0927)5803448', NULL, NULL, 948)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170949, N'Princess Ann L. Patricio', N'Cookie', N'Sta. Lucia West, Moncada, Tarlac', N'(0905)7518957', NULL, NULL, 949)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170950, N'Beverly Grace B. Rojo', N'Pancho', N'Del Valle Estacion, Paniqui Tarlac', N'(0977)2860601/931-1153', NULL, NULL, 950)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170951, N'Micaela P. Betis', N'Sansa', N'101 Rizal St. Cuyapo, Nueva Ecija', NULL, NULL, NULL, 951)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170952, N'Angelo Rosario', N'Prada', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0950)9057990/470-8968', NULL, NULL, 952)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170953, N'Hazel Grace Barrozo', N'Kiray', N'Sta. Rosa St., Paniqui, Tarlac', N'(0915)7582119', N'hazelracebarroro@gmail.com', NULL, 953)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170954, N'Dayanara Caldona', N'Small/Tiny', N'Brgy. Sapang, Moncada Tarlac', N'(0930)6349424', N'cnsgame666@gmail.com', NULL, 954)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170955, N'Howell Dela Cruz', N'Sniffy', N'Camposanto #1 Sur, Moncada Tarlac', NULL, NULL, NULL, 955)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170956, N'Rowena Pascua', N'Thyson', N'Brgy. Samput Paniqui Tarlac', N'(0932)1398015', NULL, NULL, 956)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170957, N'Janette Amuenda', N'Thunder', N'310 Coral Paniqui, Tarlac', N'(0948)7078365', NULL, NULL, 957)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170958, N'Maria Lourdes Cabuslay', N'Chi-Chi', N'Ace Apartment, Carapdapan Paniqui, Tarlac', N'(0947)7918769', NULL, NULL, 958)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170959, N'Irene L. Almuete', N'Oreo', N'Brgy. Loob Cuyapo, Nueva Ecija', N'(0995)1513824', NULL, NULL, 959)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170960, N'Frances Kiana K. Molina', N'Thirdee', N'232 Samput, Paniqui Tarlac', N'(0916)6316874', N'inkianmolinao@gmail.com', NULL, 960)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170961, N'Pastor C. Villar', N'Bulldog', N'256 Camp. 1 Norte, Moncada Tarlac', N'(0956)8790989', NULL, NULL, 961)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170962, N'Bong Payad', N'Randy', N'Samput, Paniqui, Tarlac', N'(0949)6038745', NULL, NULL, 962)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170963, N'John Michael Balgos', NULL, N'Moncada, Tarlac', NULL, NULL, NULL, 963)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170964, N'Cholo F. Tuazon', N'Tobi', N'Coral, Paniqui Tarlac', N'(0966)7306212', NULL, NULL, 964)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170965, N'Harris Dalayoan', N'Frances', N'Coral, Paniqui Tarlac', N'(0928)6635678', NULL, NULL, 965)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170966, N'Neo Oliveros', N'Hana', N'Dalayoan, Subd. Paniqui Tarlac', N'(0966)2662147', NULL, NULL, 966)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170967, N'Paul Andre Villanueva', N'Pillar', N'Capaoyan, Moncada, Tarlac', N'(0917)9053121', NULL, NULL, 967)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170968, N'Maribel R. Lactaotao', N'Jijel', N'Purok 4, Dapdap Paniqui Tarlac', N'(0947)3344116', NULL, NULL, 968)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170969, N'Win-Ann Untalan', N'Oreo', N'Samput, Paniqui, Tarlac', N'(0935)6108616', NULL, NULL, 969)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170970, N'Cristy Joy J. Ibay', N'Winter', N'#552 Magallanes St. Paniqui Tarlac', N'(0942)0810327', NULL, NULL, 970)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170971, N'Charlie B. Gamitano', N'Laika', N'Sta. Ines, Paniqui, Tarlac', N'(0930)5765025', NULL, NULL, 971)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170972, N'Willie Asuncion', N'Tobie', N'#50 Dapdap, Paniqui Tarlac', N'(0919)6967760', NULL, NULL, 972)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170973, N'Jellicoe Maine C. Asuncion', N'Loydie', N'191 Cabayaoasan, Paniqui Tarlac', N'(0943)4291755', NULL, NULL, 973)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170974, N'Frances Penelope Recto', N'Chewie', N'Carino, Paniqui, Tarlac', N'(0915)2592184', NULL, NULL, 974)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170975, N'Lyndon M. De Leon', N'Budoy', N'#93 Carapdapan, Pob. Norte, Paniqui Tarlac', N'(0908)4515843', NULL, NULL, 975)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170976, N'Maybell B. Caibigan', N'KD', N'Pob. 4, Moncada Paniqui, Tarlac', N'(0998)9127211', NULL, NULL, 976)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170977, N'Ivy Rose Kristine Roque', N'Moana', N'Lapsing, Moncada Tarlac', N'(0910)0346392', NULL, NULL, 977)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170978, N'Vanessa Bautista', N'Pitou', N'394 Coral, Paniqui, Tarlac', N'(0935)6599085', NULL, NULL, 978)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170979, N'Jefferson Sonza', N'Snow', N'#04 Luna St. Pob. Sur, Paniqui, Tarlac', N'(0921)9302785', NULL, NULL, 979)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170980, N'Rochille T. Austria', N'Riley', N'1652 M.H Del Pilar St. Paniqui Tarlac', N'(0915)7006815', NULL, NULL, 980)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170981, N'Aiza Lomboy', N'Cucu', N'Samput, Paniqui, Tarlac', N'(0938)8612088/(0956)9169463', NULL, NULL, 981)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170982, N'Zyrusfil C. Ariz', N'Koko', N'Samput, Paniqui, Tarlac', N'(0915)9452763', NULL, NULL, 982)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170983, N'Mark Mendiola', N'Sanji', N'Samput, Paniqui, Tarlac', N'(0919)5188977', NULL, NULL, 983)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170984, N'Nika G. Aguilar', N'Chim-Chim', N'Carino, Paniqui, Tarlac', N'(0945)9811059', NULL, NULL, 984)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170985, N'Louise Penamante', N'Snow', N'Pinasling, Gerona, Tarlac', N'(0956)3815667', N'louisepenamante96@gmail.com', NULL, 985)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170986, N'Marionne Estanislao', N'Nikita', N'Magallanes Pob. Sur Paniqui, Tarlac', N'(0912)8590095', NULL, NULL, 986)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170987, N'Paulina Lacayanga', N'Pot-Pot', N'Ayson, Gerona, Tarlac ', NULL, NULL, NULL, 987)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170988, N'Virginia B. Ison', N'Ember', N'Samput, Paniqui, Tarlac', N'(0919)8208888', NULL, NULL, 988)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170989, N'Bernadith B. Victoria', N'Lovey', N'Del Pilar annex St., Paniqui, Tarlac', N'(0999)3125495', NULL, NULL, 989)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170990, N'Brylle Jan V. Linsao', N'Kitting', N'Pob. South, Ramos, Tarlac', N'(0912)1015474', NULL, NULL, 990)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170991, N'Ian Roy Mercado', N'Amaia', N'103 Brgy. San Roque, Anao Tarlac', N'(0917)8904594/(0977)1039234', NULL, NULL, 991)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170992, N'Alexander/Wilma Sumaoang', N'Chirra', N'Purok 7, Brgy. Samput, Paniqui Tarlac', N'(0950)2795554', NULL, NULL, 992)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170993, N'Edward V. Tolentino', N'Blue', N'Burgos, Moncada, Tarlac', N'(0917)3206192', NULL, NULL, 993)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170994, N'Mark Daniel P. Fernando', N'Oreo', N'Capaoyan, Moncada, Tarlac', N'(0915)9172375', NULL, NULL, 994)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170995, N'Adelaide B. Macarayo', N'Sunshine', N'#94 Cabaducan, East Nampicuan, Nueva Ecija', N'(0995)5524630/044-803-8590', NULL, NULL, 995)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170996, N'Teng O. Galvadorez', N'Hailey', N'Guiteb, Ramos, Tarlac', N'(0938)4403463/9255724', NULL, NULL, 996)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170997, N'Jhaemee Rose Dela Cruz', N'Yuri', N'Lacayanga Subd., Paniqui, Tarlac', N'(0938)6436659', NULL, NULL, 997)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170998, N'Pacita Corpuz', N'King', N'Purok 1, Samput, Paniqui, Tarlac', N'(0950)2483556', NULL, NULL, 998)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20170999, N'Cherry V. Melchor', N'Chuchay', N'Carino, Paniqui, Tarlac', N'(0930)1684744', NULL, NULL, 999)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171000, N'Jonathan Miaco', N'Yara', N'Samput, Paniqui, Tarlac', N'(0998)4729179', NULL, NULL, 1000)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171001, N'Marites A. Elardo', N'Sugar', N'Del Valle Estacion, Paniqui Tarlac', N'(0927)4051828', NULL, NULL, 1001)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171002, N'Ester De Guia', N'Rambo', N'1737 Baltazar Paniqui Tarlac', N'(0906)2257126', NULL, NULL, 1002)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171003, N'Jan Krisman M. Contreras', N'Yuri', N'Abogado, Paniqui, Tarlac', N'(0909)2896551', NULL, NULL, 1003)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171004, N'Tessan Kiok', N'Q.T', N'Del Pilar Str. Paniqui, Tarlac', N'(0955)7729872/491-1792', NULL, NULL, 1004)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171005, N'Karla Chargmane Unabia', N'Chi-Chi', N'268 Samput, Paniqui, Tarlac', N'(0927)0623938', NULL, NULL, 1005)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171006, N'Doreen Lavitoria', N'Chi-Chi', N'Bugallon Estacion, Paniqui, Tarlac', N'(09198)9300175', NULL, NULL, 1006)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171007, N'Jemimah Agpaoa', N'Inno,Coco,Dos', N'Coral, Paniqui Tarlac', N'(0915)9160641', NULL, NULL, 1007)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171008, N'Shelly Ann Lopez', N'Sydney', N'Rizal, Moncada, Tarlac', N'(0919)2516496', NULL, NULL, 1008)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171009, N'Junjie Arucha', N'Tam-Tam', N'Sinigpit, Paniqui, Tarlac', N'(0966)3708546', NULL, NULL, 1009)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171010, N'Elaine R. Diaz', N'Shanti', N'Nampicuan, Nueva Ecija', N'(0917)4354058/(0977)0245665', NULL, NULL, 1010)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171011, N'Charlene Cachero', N'Macky', N'M.H Del Pilar St. Brgy. Estacion Paniqui, Tarlac', N'(0956)6086962', NULL, NULL, 1011)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171012, N'Normita P. Catap', N'Mango', N'423 Abogado, Paniqui, Tarlac', N'(0977)3195617', NULL, NULL, 1012)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171013, N'Gelli Jen Bulosan', N'Zoey', N'Tablang, Paniqui, Tarlac', N'(0916)3335416', NULL, NULL, 1013)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171014, N'Nathaniel Angelo Macayanan', N'Harry', N'41 Fernando, Anao, Tarlac', N'(0977)8124991', NULL, NULL, 1014)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171015, N'Clarisse D. Marcos', N'Hachiko', N'Legaspi St. Paniqui, Tarlac', N'(0975)9560499', NULL, NULL, 1015)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171016, N'Shiela Delos Reyes', N'Zooey', N'Ramos Subd. Villasoccoro, Pob. Norte, Paniqu, Tarlac', N'(0977)3277221', NULL, NULL, 1016)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171017, N'Mica Angela Rosaros', N'Ash', N'Manaois, Paniqui, Tarlac', N'(0921)2181126', NULL, NULL, 1017)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171018, N'Sonia & Philip Dela Cruz', N'Wackee & Byte', N'#05 Cabayaoasan, Paniqui, Tarlac', N'(0977)2391259/(0917)6496701', NULL, NULL, 1018)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171019, N'Zandro E. Sarmiento', N'Iska, Kulasa, Mini', N'# 150, Dalayaoan, Subd. Paniqui Tarlac', N'(0946)2232023', NULL, NULL, 1019)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171020, N'Aurora V. Ibarra', N'Elmo', N'Samput, Paniqui, Tarlac', N'931-16-56', NULL, NULL, 1020)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171021, N'Alice Fernandez', N'Kayang-Kayang', N'Samput, Paniqui, Tarlac', N'(0949)9986454', NULL, NULL, 1021)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171022, N'Hex David', N'Pochi', N'Legaspi St. Paniqui, Tarlac', N'(0936)6394566', NULL, NULL, 1022)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171023, N'Nicole Roxas', N'Summer', N'Estacion, Paniqui, Tarlac', N'(0995)5168978', NULL, NULL, 1023)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171024, N'Joy Pascual', N'2 puppies', N'Canan, Paniqui, Tarlac', N'(0966)7263957/045-470-8334', NULL, NULL, 1024)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171025, N'Ryan Arayan', N'Akira', N'# 395 Rosal, St. Carino Paniqui, Tarlac', N'(0938)8531622', NULL, NULL, 1025)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171026, N'Jomark L. Manaloto', N'Daniela', N'Rang - Ayan, Paniqui, Tarlac', N'(0949)5757176', NULL, NULL, 1026)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171027, N'Rowena Bautista', N'Maxx', N'Lacayanga Subd., Paniqui, Tarlac', N'(0917)5002600', NULL, NULL, 1027)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171028, N'Andrea Kisher Mostrales', N'Hustler', N'Guiteb, Ramos, Tarlac', N'(0938)4403463/925-57-24', NULL, NULL, 1028)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171029, N'Jose C. Yu', N'Brownie', N'Carriedo, Paniqui, Tarlac', N'(0946)2129416', NULL, NULL, 1029)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171030, N'Joane Candari', N'Ponsy', N'Sitio Balsa, San Isidro Paniqui, Tarlac', N'(0928)4503307', NULL, NULL, 1030)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171031, N'Abegail Mae Dulay', N'Buffy', N'Sitio Balsa, San Isidro Paniqui, Tarlac', N'(0927)4819813', NULL, NULL, 1031)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171032, N'Henry Caerlang', N'Ella', N'Camia St. Cayanga, Paniqui, Tarlac', N'(0956)5672899', NULL, NULL, 1032)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171033, N'Lucena Palarca', N'Whity', N'Purok 2, Samput, Paniqui, Tarlac', N'(0939)8388630', NULL, NULL, 1033)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171034, N'Joel Abalos', N'Scarlet', N'Bugallon Estacion, Paniqui, Tarlac', NULL, NULL, NULL, 1034)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171035, N'Genaro Melchor', N'Rambo', N'Baquero Sur, Moncada, Tarlac', N'(0927)2769002', NULL, NULL, 1035)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171036, N'Jonathan Quilapio', N'Miyaka, Shiroi, Mighty', N'Balaoang, Paniqui, Tarlac', N'(0950)8461363', NULL, NULL, 1036)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171037, N'Shainon Irvin G. Lising', N'Choi', N'Villa socorro, Paniqui, Tarlac', N'(0905)7150405', NULL, NULL, 1037)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171038, N'Andrew Mendoza', N'Lunox', N'458 Magallanes Paniqui, Tarlac', N'(0905)2800349', NULL, NULL, 1038)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171039, N'Cyber Domingo', N'Sundae ', N'Gomez St. Camiling, Tarlac', N'(0919)5165487/(045)-491-5270', NULL, NULL, 1039)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171040, N'Marianne Claire Gallema', N'Moana', N'529 Bugallon St. Brgy. Estacion, Paniqui, Tarlac', N'(0926)3054858', N'Jhiannedayrit@gmail.com', NULL, 1040)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171041, N'Joycel Mae Villaluz', N'Valerian', N'Quezon St.Paniqui, Tarlac', N'(0912)481287179', NULL, NULL, 1041)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171042, N'Mark S. Kasilag', N'Tagpi', N'#86 Dalayoan Subd. Pob Sur, Paniqui Tarlac', N'(0917)7171510', N'kasilag_mark09@yahoo.com', NULL, 1042)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171043, N'Juvelyn P. Elivado', N'Santina', N'Villa Paz Camangaan East, Moncada', N'(0927)1852063', NULL, NULL, 1043)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171044, N'Khenard Arlantico/Kyle Aguas', N'Zorro', N'Tolega Norte, Moncada, Tarlac', N'(0995)1552349', N'narol_arlantico@yahoo.com', NULL, 1044)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171045, N'Brenda M. Gines', N'Chewy', N'Carino, Paniqui, Tarlac', N'(0919)2408506', NULL, NULL, 1045)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171046, N'Gretchen U. Austria', N'Snowie', N'Samput, Paniqui, Tarlac', N'(0955)3011288', NULL, NULL, 1046)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171047, N'Cecilia Baladad', N'HAiley', N'Matalapitap, Paniqui, Tarlac', N'(0905)4451703', NULL, NULL, 1047)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171048, N'William C. Ibay, Jr.', N'Robbie', N'Carino, Paniqui, Tarlac', N'(0936)2065710', NULL, NULL, 1048)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171049, N'John Kevin G. Clemente', N'Molly', N'#463 Magallanes St. Paniqui Tarlac', N'(0927)6628982', NULL, NULL, 1049)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171050, N'Rina Bernal', N'Peanuts', N'Poblacion 1, Moncada, Tarlac', N'(0932)9861271', N'rinabernal888@gmail.com', NULL, 1050)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171051, N'Ferdinand Clarin', N'Cyan', N'Pob. Anao, Paniqui, Tarlac', N'(0917)8818277', NULL, NULL, 1051)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171052, N'Glenn Belarmino', N'Coco/Erp', N'Quezon St. Paniqui, Tarlac', N'(0945)8740606/(0956)4173424', NULL, NULL, 1052)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171053, N'Rachel Traspe', N'Max', N'Anao, Tarlac', N'(0915)5490927', NULL, NULL, 1053)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171054, N'Yshie Cobsin', N'Sophie', N'Moncada, Tarlac', N'(0995)1058854', NULL, NULL, 1054)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171055, N'Jhun Abraham', N'Princess ', N'Pob 1, Moncada, Tarlac', N'(0950)5070441', NULL, NULL, 1055)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171056, N'Jennie Aquino ', N'Myuki', N'Abogado, Paniqui, Tarlac', N'(0936)5365205', NULL, NULL, 1056)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171057, N'John Ednyl S. Acosta', N'Buddy/Blake', N'Patalan, Paniqui, Tarlac', N'(0966)9122726', NULL, NULL, 1057)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171058, N'Arleen P. Ragasa', N'Kairo', N'Aguilar St. District 1, Cuyapo Nueva Ecija', N'(0917)6278011', NULL, NULL, 1058)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171059, N'Maricel De Vera', N'Scarlette', N'#50 Purok Saranay, Coral, Ramos', N'(0908)7152792/(0995)2930286', NULL, NULL, 1059)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171060, N'Jennifer/Fernando Vaquilar', N'Hercules', N'Lot 4, 5, 6, 7 Block 3, Brgy. Abogado, Paniqui, Tarlac', N'(0927)6664143/045-925 4820', NULL, NULL, 1060)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171061, N'Eduardo E. Garcia', N'Mira', N'Caturay, Gerona, Tarlac', N'(0907)2556395', NULL, NULL, 1061)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171062, N'Joshua  G. Ibuos', N'Choco', N'San Juan, Moncada, Tarlac', N'(0977)4042306', NULL, NULL, 1062)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171063, N'Leah/Andy Castro', N'Milo', N'# 13, Dhalia St. Brgy. Cayanga, Paniqui, Tarlac', N'(0909)4710303', NULL, NULL, 1063)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171064, N'Julie Roxas', N'Maximus', N'San Julian, Moncada, Tarlac', N'(0967)2373144', NULL, NULL, 1064)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171065, N'Princess Caberto ', N'Ella', N'Coral, Ramos, Tarlac', N'(0966)7306667', NULL, NULL, 1065)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171066, N'Ma. Corazon Adams', N'Cooper', N'Tablang, Paniqui, Tarlac', N'(0917)7531115', NULL, NULL, 1066)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171067, N'Melsie Tiu', N'Muffin', N'Poblacion Norte, Paniqui, Tarlac', N'(0921)4126777', NULL, NULL, 1067)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171068, N'Rob Louise Rosales', N'Sky', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0961)1669897', NULL, NULL, 1068)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171069, N'Clarissa Uycoco', N'E-Yohgurt', N'Poblacion 1, Moncada, Tarlac', N'(0935)4214434', NULL, NULL, 1069)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171070, N'Ma. Cristiana Santos', N'Banana', N'151 Magallanes St. Pob. Sur, Paniqui, Tarlac', N'(0956)4105995', N'mariacristianasantos@gmail.com', NULL, 1070)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171071, N'Eloise Taguiran', N'Callie', N'Magallanes Pob. Sur Paniqui, Tarlac', N'(0956)6194240', NULL, NULL, 1071)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171072, N'Yrvin Paul Sanguyo', NULL, N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0956)3704926', NULL, NULL, 1072)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171073, N'Joy B. Roque', N'Storm', N'Lapsing, Moncada Tarlac', N'(0977)3365762', NULL, NULL, 1073)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171074, N'Lianna Patricia Tolentino', N'Peanut', N'423 Abogado, Paniqui, Tarlac', N'(0936)7855344', NULL, NULL, 1074)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171075, N'Mariva Mariano', N'Candy', N'Bulala, Cuyapo, Nueva Ecija ', N'(0927)2868195', NULL, NULL, 1075)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171076, N'Crisnel Exiomo ', N'Toffee', N'Acocolao, Paniqui, Tarlac', N'(0950)3533787', NULL, NULL, 1076)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171077, N'Luvy Daus', N'Akira', N'Lamorena Subd. Coral Paniqui, Tarlac', N'(0946)2185866', NULL, NULL, 1077)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171078, N'Jenie Benitez', N'Coco', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0977)4688620', NULL, NULL, 1078)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171079, N'Joy Decipeda', N'Marsh', N'Purok Maligaya, Poblacion Norte, Paniqui, Tarlac', N'470-9043/09056157511', NULL, NULL, 1079)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171080, N'Erwin Saraza', N'Hachico', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0956)6602451', NULL, NULL, 1080)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171081, N'Allen / Cynthia Gan', N'Happy', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'931-2250/09165457880', NULL, NULL, 1081)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171082, N'Jeffrey Dulce', N'Martini', N'Aringin, Moncada Tarlac', N'(0939)5128083', NULL, NULL, 1082)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171083, N'Myrna Montano', N'Cindy', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 1083)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171084, N'Meek Resngit', N'Atom', N'Mandaluyong', N'(0921)8114773', NULL, NULL, 1084)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171085, N'Deanette Michaela Duque', N'Princess', N'Mabini, Moncada, Tarlac', N'(0921)5974733', NULL, NULL, 1085)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171086, N'Felix Caseria', N'Fiona/Fatima', N'Salumague, Paniqui, Tarlac', N'(0998)4113197', NULL, NULL, 1086)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171087, N'Cathlene Joy Dela Cruz', N'Summer', N'53 South East Pob. Nampicuan, Nueva Ecija', N'(0928)7070353', NULL, NULL, 1087)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171088, N'Barbara F. Perez', N'Pitz', N'Bugallon Estacion, Paniqui, Tarlac', N'(0920)8119049', NULL, NULL, 1088)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171089, N'Arceli Macaraeg', N'Panchang', N'Pob. Centro, Ramos Tarlac', N'(0910)2060055', NULL, NULL, 1089)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171090, N'Rodrigo Pica Jr.', N'Kolokoy', N'Sta. Ines Paniqui, Tarlac', N'(0928)3525533', NULL, NULL, 1090)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171091, N'Jay-Ar Dungca', N'Fiona', N'Tablang, Paniqui, Tarlac', N'(0915)5066875', NULL, NULL, 1091)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171092, N'Dhanica Faye Ramos', N'Shaira', N'Gerona, Tarlac', N'(0905)4718878', NULL, NULL, 1092)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171093, N'Edison Pascasio', N'Riley', N'Pob. Norte, Paniqui, Tarlac', N'491-7089/09219571280', NULL, NULL, 1093)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171094, N'Cheryl V. Niegos', N'Storm', N'Camangaan East, Moncada Tarlac', N'(0930)3666748', NULL, NULL, 1094)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171095, N'Juiem Galang', N'Archie', N'13 Southwest Poblacion, Nampicuan Nueva Ecija', N'(0917)7094261', NULL, NULL, 1095)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171096, N'Ailene M. Libunao', N'Cloud', N'676 Caburnay St. Pob. Sur Paniqui, Tarlac', N'(0946)6573299 / (0950)3029799', NULL, NULL, 1096)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171097, N'Mitchell Tan', N'Elda', N'Anao, Tarlac', N'(0927)8600373', NULL, NULL, 1097)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171098, N'Cindy Dumrique', N'Ash', N'Tablang, Paniqui, Tarlac', N'(0926)0982459', NULL, NULL, 1098)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171099, N'Maureen Pandaraoan', N'Chloe', N'44 Cabayaoasan Paniqui, Tarlac', N'931-2031 / (0975)4184925', NULL, NULL, 1099)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171100, N'Gina V. Obrero', N'Ted', N'Palarca St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0942)25719801', NULL, NULL, 1100)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171101, N'Alyssa Joy Oliveros', N'Cody', N'60 Plaridel St. Brgy. Latap Cuyapo Nueva Ecija', N'(0906)5173996', NULL, NULL, 1101)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171102, N'Olea Shineha Tuazon', N'Oreo', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(045)931-1615 / 09302284716', NULL, NULL, 1102)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171103, N'May Anne Calosing', N'Coockie', N'Sinigpit, Paniqui, Tarlac', N'(0956)6910558', NULL, NULL, 1103)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171104, N'Ramon Soriano', N'Alliyha', N'Dicolor, Gerona, Tarlac', N'(0929)3480403', NULL, NULL, 1104)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171105, N'Niña Ann Maribeth Gabriel', N'Hailey', N'Cuyapo, Nueva Ecija', N'(0935)7107584', NULL, NULL, 1105)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171106, N'Orpha Navarro', N'Pongoy', N'Abogado, Paniqui, Tarlac', N'(0906)5743963', NULL, NULL, 1106)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171107, N'Kisster P. Peridas', N'Baby', N'Burgos st. Paniqui, Tarlac', N'(0947)8462323', NULL, NULL, 1107)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171108, N'Renz Jason Sumalbog', N'Baba', N'Brgy. Latap, Cuyapo Nueva Ecija', N'(0927)4065717 / 09974800109', NULL, NULL, 1108)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171109, N'Kevin Ladica', N'Botchoy', N'Abogado, Paniqui, Tarlac', N'(0935)8242247', NULL, NULL, 1109)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171110, N'Shen B. Quibuyen ', N'Katniss ', N'San Julian, Moncada, Tarlac', N'(0907)6684044 (0906)4876357', NULL, NULL, 1110)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171111, N'Kim Antonio', N'Oreo', N'Malvar St., Camiling, Tarlac ', N'(0939)8883478', NULL, NULL, 1111)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171112, N'Sarah Jean Bacay', N'KingKong ', N'Sitio Umangan, Patalan, Paniqui, Tarlac ', N'(0999)8106284', N'azhaiabacay@gmail.com', NULL, 1112)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171113, N'Annabel Caravaca', N'Puppy', N'Buenlag, Gerona, Tarlac', N'(0930)1476998', NULL, NULL, 1113)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171114, N'Rolan Nerbes', N'Pepper', N'Maura St. Poblacion 1 Pura, Tarlac', N'(0927)7827259', NULL, NULL, 1114)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171115, N'Ruel Madayag', N'Tadashi', N'Sulipa, Gerona, Tarlac', N'(0907)6304838', NULL, NULL, 1115)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171116, N'Benjamin Villanueva', N'Stormy', N'Samput, Paniqui, Tarlac', N'(0922)406481', NULL, NULL, 1116)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171117, N'Anna Clariza Morales', N'Marcial/Tracker', N'Purok 6 Matapitap, Gerona Tarlac', N'(0917)7173287', N'accolomorales@gmail.com', NULL, 1117)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171118, N'Rholex Apelado', N'Pepito', N'Sta.Maria San Manuel Tarlac', N'09064824848/09773025011', NULL, NULL, 1118)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171119, N'Joel Tarifa', N'Milo', N'Camposanto 1 Sur, Moncada, Tarlac', N'(0910)8380120', N'joeltarifa05147@gmail.com', NULL, 1119)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171120, N'Giel Denise Ramos', N'Paw', N'929 Maligaya 2 Zamora St. Poblacion Sur, Paniqui Tarlac', N'(0906)3882912', N'ramosgieldenise@gmail.com', NULL, 1120)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171121, N'Marc Jasper Castillo', N'Sushi', N'Blk 65 Abundance St. Alexannre Heights', N'(0956)8431524', N'marcjasper0323@icloud.com', NULL, 1121)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171122, N'Joan Paragas', N'Penpen', N'268 Clark St. Paniqui Tarlac', N'931-2259/09977944107', NULL, NULL, 1122)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171123, N'Alvin Vallente', N'Shan Cai', N'Nancamarinan, Paniqui, Tarlac', N'(0912)2193377', NULL, NULL, 1123)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171124, N'Amando Caerlang', N'Choco', N'Cayanga, Paniqui, Tarlac', N'628-8561/09272022150', N'acaerlang@yahoo.com.ph', NULL, 1124)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171125, N'Rosa Abarra', N'Sonia/Jodi', N'90 main st. Dalayoan Subd. Pob.Sur Paniqui, Tarlac', N'491-8959', NULL, NULL, 1125)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171126, N'Gerald Cokiangco/ Grazielle Vicente', N'Cairo', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'09184254248/09982701689', N'gerald.cokiangco@deped.gov.ph', NULL, 1126)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171127, N'Jennieca Manalo', N'Snow', N'Pob.Sur Paniqui, Tarlac', N'(0933)8138917', NULL, NULL, 1127)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171128, N'Wilfredo Soriano', N'Max', N'411 Purok Pagasa, Guiteb Ramos Tarlac', N'(0998)2215810', NULL, NULL, 1128)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171129, N'Joaquin Guiam Jr.', NULL, N'28 Brgy. Rizal Anao Tarlac ', N'(0925)8270608', NULL, NULL, 1129)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171130, N'Tam Santillan', N'Hachi', N'Dapdap,Paniqui, Tarlac', N'(0966)1500492', NULL, NULL, 1130)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171131, N'Anacar/Christopher Tayag', N'Bubba Jing', N'37 East Central Poblacion, Nampicuan Nueva Ecija', N'(0977)4898588', NULL, NULL, 1131)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171132, N'Katrina Esteban', N'Hong Kong', N'Anao, Tarlac', N'09174113171/09065090648', NULL, NULL, 1132)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171133, N'Margaux Audrey Ramil', N'Bello', N'Carino, Paniqui, Tarlac', N'925-7929/09396347816', NULL, NULL, 1133)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171134, N'Jesus/Hazel Domingo', N'Whisky/Mochi', N'JMD Apartment Clark St. Poblacion Norte Paniqui', N'(0917)7045295', NULL, NULL, 1134)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171135, N'Victor Puyaoan ', N'Akiro ', N'417 Abogado, Paniqui, Tarlac ', N'(0923)4703223', NULL, NULL, 1135)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171136, N'April Pesimo', N'Cat Cat ', N'0432 Luna St. Pob Norte, Paniqui, Tarlac ', N'(0907)9060460', NULL, NULL, 1136)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171137, N'Jennet Longaza', N'Vania ', N'116 Gonzales St., Cuyapo, Nueva Ecija', N'(0956)4179719', NULL, NULL, 1137)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171138, N'Kaye Arciaga', N'Cece', N'Sulipa, Gerona, Tarlac', N'(0966)3273634', NULL, NULL, 1138)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171139, N'Mark Joseph Divina ', N'Melbourne ', N'Cuyapo, Nueva Ecija', N'(0927)6127437', NULL, NULL, 1139)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171140, N'Miguela Bergonia ', N'Digong', N'Curva, Cuyapo, Nueva Ecija ', N'(0917)6182378', NULL, NULL, 1140)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171141, N'Anna Rose Tabigue', N'Marimar', N'Samput, Paniqui, Tarlac', N'(0938)88951720', NULL, NULL, 1141)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171142, N'Jonella Mae Garcia', N'Zoro', N'2000 Don Fausto St., Blk 5, San Roque, Tarlac City ', N'(0966)9476952', NULL, NULL, 1142)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171143, N'Arlene Gonzales ', N'Apple ', N'Brgy. Abogado, Paniqui, Tarlac', N'(0936) 5311296', NULL, NULL, 1143)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171144, N'Aaron Sagum', N'Simba/ Nalla ', N'482 Sta. Rosa St., Paniqui, Tarlac ', N'(0909)9083827', NULL, NULL, 1144)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171145, N'Julien Layug ', N'Maggie', N'Nipaco, Paniqui, Tarlac', N'(0996)7830774', NULL, NULL, 1145)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171146, N'Lysse Dina Ellis', N'Master Yoda ', N'Brgy. Patalan, Paniqui, Tarlac', N'(045)9856208/ (0917)5059414', NULL, NULL, 1146)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171147, N'Joyce Anne Gragasin ', N'Toby', N'Colibangbang, Paniqui, Tarlac', N'(0938)4411120', NULL, NULL, 1147)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171148, N'Sonia Dela Cruz', N'Kaden ', N'Carino, Paniqui, Tarlac', N'(0999)3829119', NULL, NULL, 1148)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171149, N'Anngellica Socias', N'Hacko', N'Maligaya St., Pob. Norte, Paniqui, Tarlac ', N'(0926)7025712', NULL, NULL, 1149)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171150, N'Rica Sanda Rivera', N'Sebastianne ', N'Sta. Rosa St., Poblacion Sur, Paniqui, Tarlac', N'(0965)4151246', NULL, NULL, 1150)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171151, N'Gladys Aquino ', N'Kitto', N'Alexanre Heights, Samput, Paniqui, Tarlac', N'(045)9255456/(0910)3158358', NULL, NULL, 1151)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171152, N'Robert Fabia', N'Voodly ', N'Lapsing, Moncada Tarlac', N'(0908)2108911', NULL, NULL, 1152)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171153, N'Cesar Paragas II', N'Rambo', N'268 Clark St. Paniqui Tarlac', N'(045)3260400/(0906)2045324', NULL, NULL, 1153)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171154, N'Gemma Dispo', N'Gohan ', N'Green Meadows, Samput, Paniqui, Tarlac', N'(0928)7393688', NULL, NULL, 1154)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171155, N'Elvie Navarro ', N'Pogi', N'Salumague, Paniqui, Tarlac', NULL, NULL, NULL, 1155)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171156, N'Ma. Asuncion Hipolita Silao', N'Blackie ', N'Cuyapo, Nueva Ecija', N'(0955)8859708', NULL, NULL, 1156)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171157, N'Virginia Misarcia', N'Bantay', N'Samput, Paniqui, Tarlac', N'(0948)8058127', NULL, NULL, 1157)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171158, N'Ricalyn Joy Pagalingan ', N'Thea ', N'San Isidro, Paniqui, Tarlac', N'(0950)0387999/ (0912)9989372', NULL, NULL, 1158)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171159, N'Arjay Fernandez', N'Colai', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0906)5114176', N'arjayfernandez@gmail.com', NULL, 1159)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171160, N'Mark Ayvan James Tadeo', N'Belgian', N'Malvid, Victoria, Tarlac', N'(0929)6135385', NULL, NULL, 1160)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171161, N'Jessa Nicolle Pacia', N'Calli', N'Capaoayan Compound, Moncada, Tarlac', N'(0999)6684779', NULL, NULL, 1161)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171162, N'Chi- Chi Olonan Garcia', N'Kourtney', N'Pasuquin St., Paniqui, Tarlac', N'(0921)3693012', NULL, NULL, 1162)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171163, N'Joshua Jhae Daileg', N'Lucky', N'Centro Patalan, Paniqui, Tarlac ', N'(0909)1817991', NULL, NULL, 1163)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171164, N'John Roden Claunan', N'Abdul/ Tokyo', N'Poblacion Center, Ramos, Tarlac', N'(0998)1851968', NULL, NULL, 1164)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171165, N'Spunky Tayag', N'King Podo/Callie', N'Sta. Ines Paniqui, Tarlac', N'(0917)5145754', NULL, NULL, 1165)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171166, N'Diana Joy Nuguid', N'Bebeng', N'Bugallon Estacion, Paniqui, Tarlac', N'(0966)9327620', NULL, NULL, 1166)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171167, N'Ervin Astudillo', N'Candice', N'455 Villasio Nino, Sta. Cruz, Tarlac', N'(0945)8828268', N'ervin.amianan@gmail.com', NULL, 1167)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171168, N'Dalisay Manuel', N'Chu-Chu', N'Guiteb, Ramos, Tarlac', N'(0956)8532082', N'DalisayManuel@yahoo.com', NULL, 1168)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171169, N'Lita Aquino', N'Hanna/Susi/Chippy', N'Poblacion 1, Moncada, Tarlac', N'(0917)5140875/(0907)1767849', NULL, NULL, 1169)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171170, N'Amelia Serrano', N'Mochi', N'Canan, Paniqui, Tarlac', N'(0998)2883529', N'bethyessa@gmail.com', NULL, 1170)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171171, N'Mylene Navarro', N'Thunder', N'Apulid, Paniqui, Tarlac', N'(0939)8249204', NULL, NULL, 1171)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171172, N'Tina Grospe', N'Thunder/Akira', N'Ablang Sapang, Moncada, Tarlac', N'(0919)5378640', NULL, NULL, 1172)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171173, N'Aldwin Tulagan', N'Storm', N'Cayanga, Paniqui, Tarlac', N'(0912)0597517', NULL, NULL, 1173)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171174, N'Minerva Guttierez', N'Kobi', N'Purok 6, Samput, Paniqui, Tarlac', N'6067417/ (0950)6897137', NULL, NULL, 1174)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171175, N'Maureen Aquino', N'Riley', N'Maligaya, Abogado, Paniqui, Tarlac', N'(0946)2045015', NULL, NULL, 1175)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171176, N'Vanessa Soriano', N'Jake', N'Pob. 4, Moncada, Tarlac', N'(0916)3715188', NULL, NULL, 1176)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171177, N'Jenalyn Tabifrancia', N'Trixie', N'Cabayaoasan, Paniqui, Tarlac', N'(0917)6233337', NULL, NULL, 1177)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171178, N'Candy Precious Pardo', N'Kino', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'(0916)7877610', N'jrarcandypardo17@gmail.com', NULL, 1178)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171179, N'Anjelee Zairyl Villanueva', N'Toby', N'Magaspac, Gerona, Tarlac', N'(0926)0980259', NULL, NULL, 1179)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171180, N'Kathleen Kate Borreros', N'Luna', N'Carino, Paniqui, Tarlac', N'(0906)3821291', NULL, NULL, 1180)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171181, N'Marivic Delos Reyes', N'Kiekie', N'Samput, Paniqui, Tarlac', N'(045)9312586/ (0956)3815380', NULL, NULL, 1181)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171182, N'Walter Wong', N'Siomai', N'Abogado, Paniqui, Tarlac', N'(0917)8149123', NULL, NULL, 1182)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171183, N'Eduardo Traspe', NULL, N'Cabaducan West , Nampicuan, Nueva Ecija', N'(0995)8739515', NULL, NULL, 1183)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171184, N'Jhun Tacmo', N'Snow', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0932)6080579', NULL, NULL, 1184)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171185, N'Jan Michael Fernandez', N'Hei-Hei', N'M. H. Del Pilar St., Estacion, Paniqui, Tarlac', N'(0927)9292340/ (0936)9939366', NULL, NULL, 1185)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171186, N'Jovelyn Del Rosario', N'Cooper', N'Carino, Paniqui, Tarlac', N'(0939)4391413', NULL, NULL, 1186)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171187, N'Ma. Corazon Viuya', N'Lily', N'Estacion, Paniqui, Tarlac', N'(0908)5961696', NULL, NULL, 1187)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171188, N'Ryan Mendoza', N'Kinny', N'Gomez St., Paniqui, Tarlac', N'9255615', NULL, NULL, 1188)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171189, N'Cheryl Mariano', N'Ruth', N'Salumague, Paniqui, Tarlac', N'(0930)8088480', NULL, NULL, 1189)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171190, N'Filipina Ramos', N'Peachy', N'Coral, Ramos, Tarlac', N'(0929)7059634', NULL, NULL, 1190)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171191, N'Reenhard Agamata', N'Choco', N'Patalan, Paniqui, Tarlac', N'(0947)3329441', N'areenhard@yahoo.com ', NULL, 1191)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171192, N'Elaine Joy Austria', N'Shun', N'M. H. Del Pilar St., Estacion, Paniqui, Tarlac', N'(0930)8592337', NULL, NULL, 1192)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171193, N'Mary Joy Saptang', N'Prince', N'Balite, Pura, Tarlac', N'(0943)4792590', NULL, NULL, 1193)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171194, N'Karen Mae Dulay', N'Sky', N'Sitio Balsa, San Isidro Paniqui, Tarlac', N'(0907)3449808', NULL, NULL, 1194)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171195, N'Charito Melchor', N'Whitey', N'Florida St., Apulid, Paniqui, Tarlac', N'(0928)9014189', NULL, NULL, 1195)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171196, N'Kharyn Salarzon', N'Mia', N'303 Balaoang, Paniqui, Tarlac', N'(0909)1564255', N'meandmykhing@gmail.com', NULL, 1196)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171197, N'Aileen Michelle Santos', N'Margaux', N'1752 Baltazar Subd., Paniqui, Tarlac', N'(0909)1909558', NULL, NULL, 1197)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171198, N'Lerma Tongoy', N'Porkchop', N'Poblacion 2, Moncada, Tarlac ', N'(0998)5107328', NULL, NULL, 1198)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171199, N'Anna Princess Fuller', N'Alpha', N'Carino, Paniqui, Tarlac', N'(0930)9561320', N'anna231fuller@gmail.com', NULL, 1199)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171200, N'Van Allen Ruiz', N'Scarlet', N'Samput, Paniqui, Tarlac', N'(0927)6672081', NULL, NULL, 1200)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171201, N'Tin Carballo', N'Harry/Elvis', N'Acocolao, Paniqui, Tarlac', N'(0929)5846233', NULL, NULL, 1201)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171202, N'Rose Avedano', N'Nina', N'Canan, Paniqui, Tarlac', N'(0948)0102970', NULL, NULL, 1202)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171203, N'Brayan Sacramento', N'Shadow', N'Mapalad, Coral, Ramos', N'(0910)1878402', NULL, NULL, 1203)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171204, N'Betty Castro', N'Kobie', N'Camp #1 Sur, Moncada, Tarlac', N'(0907)1767849', NULL, NULL, 1204)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171205, N'Jonathan Tuazon', N'Zig', N'23 Samput, Paniqui, Tarlac', N'(0933)8693709', NULL, NULL, 1205)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171206, N'Clarise Cardona', N'Ace', N'1729 Baltazar Subd., Poblacion Norte, Paniqui, Tarlac', N'(0966)2686821', NULL, NULL, 1206)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171207, N'Angel Laica Dela Cruz', N'Maximus', N'Aringin, Moncada Tarlac', N'(0933)9181071', NULL, NULL, 1207)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171208, N'Hidie Reyes', N'Penguin', N'Coral, Ramos, Tarlac', N'(0905)7519126', NULL, NULL, 1208)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171209, N'Joann Lii', N'Angela', N'M. H. Del Pilar St., Estacion, Paniqui, Tarlac', N'(0905)5477988', NULL, NULL, 1209)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171210, N'Lovely Ann Catubay', N'Lady Sansa', N'San Francisco East , Anao, Tarlac', N'(0998)7727969', NULL, NULL, 1210)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171211, N'Aldrin Wilson Co', NULL, N'Estacion, Paniqui, Tarlac', N'(0975)5846752', NULL, NULL, 1211)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171212, N'Jhonalyn Fontillas', N'Jack', N'Pance, Ramos, Tarlac', N'9105804127', NULL, NULL, 1212)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171213, N'Dakila Melchor', N'Kobe', N'Patalan, Paniqui, Tarlac', N'(0927)0421215', NULL, NULL, 1213)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171214, N'Fatima Lising', N'Margo', N'Abogado, Paniqui, Tarlac', NULL, NULL, NULL, 1214)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171215, N'Joseph Borja', N'Jelly Bean ', N'Poblacion Norte, Paniqui, Tarlac', N'(0908)6698228', NULL, NULL, 1215)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171216, N'Jenebie Clamor', N'Opium', N'Patalan, Paniqui, Tarlac', N'(0916)9197042', NULL, NULL, 1216)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171217, N'Ruderick Basanez', N'Metoy', N'Coral, Ramos, Tarlac', N'(0950)3535897', NULL, NULL, 1217)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171218, N'Gretchen Zuniga', N'Febie', N'Sta. Maria, Moncada, Tarlac', N'(0922)7510', NULL, NULL, 1218)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171219, N'Lorelie Salinas', N'Cassey', N'Tubectubang, Moncada, Tarlac', N'(0919)7419806', NULL, NULL, 1219)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171220, N'Adlai Bonilla', N'Aubrey', N'San Julian, Moncada, Tarlac', N'(0955)4396245', NULL, NULL, 1220)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171221, N'Jericho Manuel', N'Nezuko', N'Poblacion 1 , Moncada, Tarlac', N'(0965)4898183', NULL, NULL, 1221)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171222, N'Frances Aguinaldo', N'Boogie', N'Baltazar Subd., Paniqui, Tarlac ', N'(0926)7254300', NULL, NULL, 1222)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171223, N'Jasmin Ann Juto', N'Pomsi', N'Acocolao, Paniqui, Tarlac', N'(0966)7495244', NULL, NULL, 1223)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171224, N'Jerone Cayabyab', N'Febie', N'Sitio Basio, Poblacion Norte, Paniqui, Tarlac', N'(0966)1992889', NULL, NULL, 1224)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171225, N'Princess Angela De Jesus', N'Lycan', N'Tablang, Paniqui, Tarlac', N'(0977)9025414', NULL, NULL, 1225)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171226, N'Xyryll Santiago', N'Bela', N'Gonzales St., Cuyapo, Nueva Ecija', N'(0956)3343193', NULL, NULL, 1226)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171227, N'Nicole Reserva', N'Cookie', N'Camangaan East, Moncada Tarlac', N'(0917)1403942', NULL, NULL, 1227)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171228, N'Jennifer Pueda', N'Chacha', N'Sulipa, Gerona, Tarlac', N'(0917)3238900', NULL, NULL, 1228)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171229, N'Patrick Aban', N'Q-Pee', N'Bantog, Anao, Tarlac', N'(0956)538138', NULL, NULL, 1229)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171230, N'Kim Valeriano', N'Malty', N'Bugallon Estacion, Paniqui, Tarlac', N'(0995)4504838', NULL, NULL, 1230)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171231, N'Marvin Velano', NULL, N'Bersamin, Alcala, Pangasinan', N'(0945)6809107', NULL, NULL, 1231)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171232, N'Roldan Mendoza', N'Star', N'Estacion, Paniqui, Tarlac', N'(0939)6139318', NULL, NULL, 1232)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171233, N'Severa Ibuan', N'Klaree', N'Dalayaoan Subd., Paniqui, Tarlac', N'(0921)7412129', NULL, NULL, 1233)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171234, N'Jonahan Dela Cruz', NULL, N'Concepcion, Tarlac', N'(0917)1108936', NULL, NULL, 1234)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171235, N'Eduardo Ryan Osorio', N'Ely', N'Almaville, Moncada, Tarlac', N'(0956)0474812', NULL, NULL, 1235)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171236, N'Jhen/ Cris Pidot', N'Oreo', N'Pob. South, Ramos, Tarlac', N'(0915)4597637', NULL, NULL, 1236)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171237, N'John Troy Vallejos', N'Kendra', N'Rizal, Moncada, Tarlac', N'(0921)8114031', NULL, NULL, 1237)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171238, N'Joanna Mayo', N'Jack', N'Casabea Hotel & Resort Poblacion 4, Mon. Tar', N'(0929)4839039', NULL, NULL, 1238)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171239, N'Doris Tiangsing', N'Daffy', N'Pob.2, Moncada, Tarlac', N'(0998)2235076', NULL, NULL, 1239)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171240, N'Cristine Joy Reyes', N'Coco', N'Canan, Paniqui, Tarlac', N'(0910)7614389', NULL, NULL, 1240)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171241, N'Mard Elwin Banag', N'Sassy/Sally/Sky', N'Pob. North, Ramos, Tarlac', N'(0995)7553367', NULL, NULL, 1241)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171242, N'Mardja Corazon Uycoco', N'Leyra', N'Tablang, Paniqui, Tarlac', N'(0933)8533248', NULL, NULL, 1242)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171243, N'Maychel Manalili', NULL, N'Apulid, Paniqui, Tarlac', N'(0923)0844450', NULL, NULL, 1243)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171244, N'Marilou Pera', NULL, N'Patalan, Paniqui, Tarlac', N'(0917)4014752', NULL, NULL, 1244)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171245, N'Jessie Chua', N'Yorme', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0928)1355837', NULL, NULL, 1245)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171246, N'Ginalyn Facun', N'Gretchen', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0946)9898584', NULL, NULL, 1246)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171247, N'Maritess Duran', N'Tucker', N'Samput, Paniqui, Tarlac', N'(0927)5430620', NULL, NULL, 1247)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171248, N'Kiel Arciaga', N'Einstein', N'Balaoang, Paniqui, Tarlac', N'(0995)8659701', NULL, NULL, 1248)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171249, N'Eduardo Arce', N'Misha', N'Tablang, Paniqui, Tarlac', N'(0915)8338739', NULL, NULL, 1249)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171250, N'Lallaine Cajulao', N'Amber/Brioche', N'Poblacion 1, Moncada, Tarlac', N'(0995)7642607', NULL, NULL, 1250)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171251, N'Angelica Obenario', N'Brody/Bailey', N'Balaoang, Paniqui, Tarlac', N'(09567405441', NULL, NULL, 1251)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171252, N'Diana Laurena', N'Kobe', N'Victoria, Tarlac', N'(0930)9401924', NULL, NULL, 1252)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171253, N'Aries Alvior', N'Dagul', N'Sulipa, Gerona, Tarlac', N'(0956)6910094', N'bustosranzly@gmail.com', NULL, 1253)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171254, N'Juanito Lomboy', N'Tommy', N'Coral, Paniqui Tarlac', N'(0929)1971298', NULL, NULL, 1254)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171255, N'Agnes Agustin', N'Meijie', N'San francisco East, Anao, Tarlac ', N'(0907)0731572', NULL, NULL, 1255)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171256, N'Jastine Paragas', N'Sandin', N'Abogado, Paniqui, Tarlac', NULL, NULL, NULL, 1256)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171257, N'Merry Grace Juan', N'Moller', N'415 B Villa Nadine, Acocolao, Paniqui, Tarlac', N'(0938)4411706', NULL, NULL, 1257)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171258, N'Jannea Aranzaso', N'Chloe', N'Samput, Paniqui, Tarlac', N'(0912)1999265', NULL, NULL, 1258)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171259, N'Jun Francis Torres', N'Riley', N'Balaoang, Paniqui, Tarlac', N'(0915)8735467', NULL, NULL, 1259)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171260, N'Olga Theresa Cesa', N'Jack', N'Samput, Paniqui, Tarlac', N'(0950)4187729', NULL, NULL, 1260)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171261, N'John Patrick Corpuz', N'Hiro', N'Patalan, Paniqui, Tarlac', N'(045)9231090', NULL, NULL, 1261)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171262, N'Maribel Ortega', N'Nanu', N'Samput, Paniqui, Tarlac', N'(0938)8787601', NULL, NULL, 1262)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171263, N'Marife Villanueva', N'Itlog', N'Coral, Paniqui Tarlac', N'(0912)1014483', NULL, NULL, 1263)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171264, N'Marianne Bondoc', N'Basha', N'St. Bernard Subd., Abogado, Paniqui, Tarlac', N'(0998)1738943', NULL, NULL, 1264)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171265, N'Ellen Dizon', N'Ganda', N'Matalapitap, Paniqui, Tarlac', N'(0917)3053329', N'elenadizon.sweetstems@yahoo.com', NULL, 1265)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171266, N'Sultana Cortez', N'Ice', N'Villa Socorro St., Pob. Norte, Paniqui, Tarlac', N'(0995)2216844', NULL, NULL, 1266)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171267, N'Gabriel Tan', N'Boomer', N'Moncada, Tarlac', N'(0928)3926972', NULL, NULL, 1267)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171268, N'Gloria Bala', N'Bella', N'Magaspac, Gerona, Tarlac', N'(0927)3029791', NULL, NULL, 1268)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171269, N'Rommel Doliente', NULL, N'801 Caburnay St., Poblacion Sur, Paniqui, Tarlac', N'(0917)1236899', NULL, NULL, 1269)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171270, N'Jhoy Manalo', N'Chow', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0912)1334244', NULL, NULL, 1270)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171271, N'Aldrin Kid Montemayor', N'Chuchay', N'Pob. South, Ramos, Tarlac', N'(0926)0990804', NULL, NULL, 1271)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171272, N'Javanne Ballobar', NULL, N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'(0950)0388207', NULL, NULL, 1272)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171273, N'Benneth Montesar', N'Lala', N'Moncada, Tarlac', N'(0915)5625011', NULL, NULL, 1273)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171274, N'Raneza Joy Buerano', N'Bella', N'Nilasin 1st, Pura, Tarlac', N'(0995)8850918', NULL, NULL, 1274)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171275, N'Sheena Joy Bongala', N'Bailey', N'Gomez St. Pob. Sur Paniqui, Tarlac', N'(0906)2545100', NULL, NULL, 1275)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171276, N'Mc Jexter Lambino', NULL, N'San Juan , Ramos, Tarlac', N'(0939)6475487', NULL, NULL, 1276)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171277, N'Marlo Ramos', N'Maximus', N'Carino, Paniqui, Tarlac', N'(0918)6207018', NULL, NULL, 1277)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171278, N'Justine Angelo Bulosan', N'Bella', N'Nipaco, Paniqui, Tarlac', N'(0945)2462102', NULL, NULL, 1278)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171279, N'Teresita Quines', N'Alfonso', N'Sta. Maria, Moncada, Tarlac', N'(0909)6728392', NULL, NULL, 1279)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171280, N'Ruzlyn Andaya', N'Stella', N'Guiteb, Ramos, Tarlac', N'(0927)0626263', NULL, NULL, 1280)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171281, N'Dhane Gabriel Santos', N'Dania', N'178 Coral, Paniqui, Tarlac', N'(0966)7615875', NULL, NULL, 1281)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171282, N'Joel Orille', N'Claudia', N'Abogado, Paniqui, Tarlac', N'(0912)7541785', NULL, NULL, 1282)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171283, N'Bea Clarisse Lamorena', N'Covid', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'(0908)7121694', NULL, NULL, 1283)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171284, N'Anarica Araos', N'Saske', N'Tablang, Paniqui, Tarlac', N'(0946)2402163', NULL, NULL, 1284)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171285, N'Christian Sunglao', N'Olive ', N'Sta. Rosa St. , Poblacion Sur, Paniqui, Tarlac', N'(0967)2110101', NULL, NULL, 1285)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171286, N'Myla Cacayuran', N'Ban', N'Coral, Ramos, Tarlac', NULL, NULL, NULL, 1286)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171287, N'Mary Joy Cayabyab', N'Zola', N'Sitio Basio, Poblacion Norte, Paniqui, Tarlac', N'(0966)2611113/(0909)5241181', NULL, NULL, 1287)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171288, N'Aldwin Ashley Abon', N'Jose', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'(0977)3039220', NULL, NULL, 1288)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171289, N'Kareen Jane Lampa', N'Valentina', N'281 Tablang, Paniqui, Tarlac', N'(0915)7285002', NULL, NULL, 1289)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171290, N'Clarissa Jane Allas', N'Ippo', N'Tablang, Paniqui, Tarlac', N'(0906)2718194', NULL, NULL, 1290)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171291, N'Marivic Dasalla', N'Cadie', N'Samput, Paniqui, Tarlac', N'(0921)2024003', N'kBryan092@gmail.com', NULL, 1291)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171292, N'Rowena Linsao', N'Erocks', N'Cayanga, Paniqui, Tarlac', N'(0918)6079820/(0915)5492407', NULL, NULL, 1292)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171293, N'Rhodalyn Naungayan', N'Cody', N'Del Valle Estacion, Paniqui Tarlac', N'(0997)2450573', NULL, NULL, 1293)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171294, N'Gabriel Baldonado', N'French', N'San Juan, Ramos, Tarlac', N'(0910)580424', NULL, NULL, 1294)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171295, N'Jr Madez', N'Bullet', N'Coral, Ramos, Tarlac', N'(0977)3196109', NULL, NULL, 1295)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171296, N'Bliss Pascasio', N'Hugs', N'Nipaco, Paniqui, Tarlac', N'(0998)8681026', NULL, NULL, 1296)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171297, N'Kathleen Ann Bautista', N'Maggie', N'Pob. North, Ramos, Tarlac', N'(0977)7550765', NULL, NULL, 1297)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171298, N'Alyanna Roxas', N'Apollo', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'(0998)0839851', NULL, NULL, 1298)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171299, N'Sylvester Rana', N'Milo', N'Samput, Paniqui, Tarlac', N'(0908)1335295', NULL, NULL, 1299)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171300, N'Mark Mercado', N'Cloud', N'Sta. Lucia East, Moncada, Tarlac', N'(0905)4404570', NULL, NULL, 1300)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171301, N'Feliciano Arada Jr.', N'Zona', N'Cabatuan, Cuyapo, Nueva Ecija', N'(0966)7682690', NULL, NULL, 1301)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171302, N'Arlene Gonzales ', N'Feather', N'Buenavista, Pura, Tarlac', N'(0909)5120344', NULL, NULL, 1302)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171303, N'Mark Joel Caranto', N'Bella', N'Buenlag, Gerona, Tarlac', N'(0915)4973150', NULL, NULL, 1303)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171304, N'Jocelyn Abenoja', N'Bruno', N'Maluac, Moncada, Tarlac', N'(0966)3273407', NULL, NULL, 1304)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171305, N'Rojan Faye Mina', N'Hachi', N'Samput, Paniqui, Tarlac', N'(0956)0642426', NULL, NULL, 1305)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171306, N'Paz Capinpin', NULL, N'Purok 1, Buenavista, Pura, Tarlac', N'(0920)9381559', NULL, NULL, 1306)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171307, N'Kenneth Bryan Bartolome', N'Yuri', N'West Poblacion, Nampicuan, Nueva Ecija', N'(0948)4284993', NULL, NULL, 1307)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171308, N'Maricel Campo', N'Pogita', N'Poblacion 4, Moncada, Tarlac', N'(0919)8990572/ (0909)6595839', NULL, NULL, 1308)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171309, N'Shiela Mae Zamora', N'Sany', N'South West, Poblacion, Nampicuan, Nueva Ecija', N'(0907)3118839', NULL, NULL, 1309)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171310, N'Rogelio Palad ', N'Laica/Miya', N'Maluac, Moncada, Tarlac', N'(0927)1208003', NULL, NULL, 1310)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171311, N'Bernard Caser ', N'Choco', N'Cabayaoasan, Paniqui, Tarlac', N'9393561324', NULL, NULL, 1311)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171312, N'Marian Sera', N'Winnie', N'Pob 2, Moncada, Tarlac', NULL, NULL, NULL, 1312)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171313, N'Rhian Velasco', N'Zoe', N'Sitio Rasig, Acocolao, Paniqui, Tarlac', N'(0921)6594437', NULL, NULL, 1313)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171314, N'Espie Gonzales', N'Storm', N'Tablang, Paniqui, Tarlac', N'(0995)4605907', NULL, NULL, 1314)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171315, N'Kezza Jane Lenon', N'Ginger', N'Poblacion South, Ramos, Tarlac', N'(0995)1784498', NULL, NULL, 1315)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171316, N'Chona Pacunayen', N'Lia', N'Tolega Norte, Moncada, Tarlac', N'(0998)3803242', NULL, NULL, 1316)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171317, N'Edna Lobacz', N'Harley', N'Samput, Paniqui, Tarlac', N'(0939)1903445', NULL, NULL, 1317)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171318, N'Jairebill Mora', NULL, N'Tablang, Paniqui, Tarlac', N'(045)9255692/ (0909)7668000', NULL, NULL, 1318)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171319, N'Kath Victoria', N'Cutie', N'Estacion, Paniqui, Tarlac', N'(0917)9271965', NULL, NULL, 1319)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171320, N'Carlita Agbayani', N'Ganda/Unknown', N'Poblacion South, Ramos, Tarlac', N'(0922)5885398', NULL, NULL, 1320)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171321, N'Precious Arceli Corpuz', N'Yuri', N'Guiteb, Ramos, Tarlac', N'(0948)7624902', NULL, NULL, 1321)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171322, N'Dolores Castillo', NULL, N'128 Carino, Paniqui, Tarlac', N'(0961)2783423', NULL, NULL, 1322)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171323, N'Eduardo Espiritu', N'Bro', N'Carino, Paniqui, Tarlac', NULL, NULL, NULL, 1323)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171324, N'Jusine Chzeilo Santiago', N'Leni', N'Carino, Paniqui, Tarlac', N'(0916)9192159', NULL, NULL, 1324)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171325, N'Billy Tamayo', N'Molly', N'Balite, Pura, Tarlac', N'(0905)4404354', NULL, NULL, 1325)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171326, N'Eggie Mae Balanon', N'Jungkok', N'Sitio Mascota, Capaoayan, Moncada, Tarlac', NULL, NULL, NULL, 1326)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171327, N'Laurano Ramos', N'Ariana', N'Carino, Paniqui, Tarlac', NULL, NULL, NULL, 1327)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171328, N'April Joy Nunag', N'Mochi', N'Carino, Paniqui, Tarlac', N'(0909)4842225', NULL, NULL, 1328)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171329, N'Imelda Hilario', N'Juke', N'42 Quezon St., Tablang, Paniqui, Tarlac', N'(0977)1785880', NULL, NULL, 1329)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171330, N'Caesar Siababa', N'Jedz', N'Canarem, Victoria, Tarlac', N'(0912)0562739', NULL, NULL, 1330)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171331, N'Julie Pascua', N'Bella', N'Apulid, Paniqui, Tarlac', N'(0946)0298677', NULL, NULL, 1331)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171332, N'Aurea Bardolla', N'Wambo', N'Ventinilla, Paniqui, Tarlac', N'(0918)4678433', NULL, NULL, 1332)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171333, N'Camille Villanueva', N'Ai/Heinz', N'San Nicolas, Victoria, Tarlac', N'(0906)4213971', NULL, NULL, 1333)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171334, N'Leo Panim', N'Chase', N'Gomez St., Poblacion Sur, Paniqui, Tarlac', N'(045)4705082', NULL, NULL, 1334)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171335, N'Emerlita Puyaoan', N'Eli', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0933)8164262', NULL, NULL, 1335)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171336, N'Porcia Macaraeg', N'Sabrina', N'Sitio Basio, Poblacion Norte, Paniqui, Tarlac', N'639951785', NULL, NULL, 1336)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171337, N'Randy Lastimosa', N'Copper', N'Capayaoan Compound, Moncada, Tarlac', N'(0917)7047381', NULL, NULL, 1337)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171338, N'Mariz Sta. Marina', N'Pudge', N'Purok 7, Singat, Gerona, Tarlac', N'(0995)54568948', NULL, NULL, 1338)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171339, N'Jeanet Dy', N'Sachiko', N'13 Magsaysay St., Cuyapo, Nueva Ecija', N'(0917)5940250', NULL, NULL, 1339)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171340, N'Khalil Angelika Fernandez', N'Bella', N'Samput, Paniqui, Tarlac', N'(0977)3039689', NULL, NULL, 1340)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171341, N'Mark Carpetero', N'Liza/Yuka', N'362 Carino, Paniqui, Tarlac ', N'(0939)5439564', NULL, NULL, 1341)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171342, N'Leah Elazegui', N'Eevah', N'7 Baltazar Subdivision, Paniqui, Tarlac', N'(0915)5072764', NULL, NULL, 1342)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171343, N'Maan Cachuela', N'Cooper', N'Purok Paraiso, Ramos, Tarlac', N'(0943)4014280', NULL, NULL, 1343)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171344, N'Gilbong Favis', N'Princess', N'San Julian, Moncada, Tarlac', N'(0907)3310946', NULL, NULL, 1344)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171345, N'Carrisa Joy Meneses', N'Basha/Popoy', N'Sulipa, Gerona, Tarlac', N'(0906)8731790', NULL, NULL, 1345)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171346, N'Tanedo Natividad', N'Stanley', N'104 Balite, Pura, Tarlac', N'(0947)8573744', NULL, NULL, 1346)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171347, N'Marjorie Manalo', N'Toshi/Lexi', N'Canan, Paniqui, Tarlac', N'(0930)9502484', NULL, NULL, 1347)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171348, N'Rosita Ambrocio', N'Angel', N'Matindog, Cuyapo, Nueva Ecija ', NULL, NULL, NULL, 1348)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171349, N'Keirstine Dangaran', N'Luffy', N'Sitio Riverside, Carino, Paniqui, Tarlac', N'(0927)6452146', NULL, NULL, 1349)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171350, N'Rhoda Mae Balatbat', N'Crunchy', N'Matalapitap, Paniqui, Tarlac', N'(0995)7680900', NULL, NULL, 1350)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171351, N'Marlon Gomez', NULL, N'Sta. Maria, Moncada, Tarlac', N'(0998)3332970', NULL, NULL, 1351)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171352, N'Elpidio Reguindin', N'Dong', N'Pob. North, Ramos, Tarlac', N'(0919)5187627', NULL, NULL, 1352)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171353, N'Aurelio Bragado', N'Whitie', N'Carriedo St., Paniqui, Tarlac', N'(', NULL, NULL, 1353)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171354, N'Gemma Theresa De Guzman', N'Lucas/Lucia', N'Poblacion 1, Moncada, Tarlac', N'(0927)3308801', NULL, NULL, 1354)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171355, N'Arthur Blas', N'Ngisit', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0999)7197291', NULL, NULL, 1355)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171356, N'Angela Milla', N'Snow', N'Colibangbang, Paniqui, Tarlac', N'(0915)4842608', NULL, NULL, 1356)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171357, N'Lish Jeanne Culaton', N'Zach', N'Sta. Maria, Moncada, Tarlac', N'(0945)7881110', NULL, NULL, 1357)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171358, N'Juancho Tuazon', N'Casey', N'Nancamarinan, Paniqui, Tarlac', N'(0922)8938878', NULL, NULL, 1358)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171359, N'Myra Quitaleg', NULL, N'Canan, Paniqui, Tarlac', N'(045)4709469', NULL, NULL, 1359)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171360, N'Gerry Hiloma', N'Flashy', N'256 Integrity St., Alexannre Heights, Paniqui, Tarlac', N'(0998)2216027', NULL, NULL, 1360)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171361, N'Elmer Mamaradlo', N'Butchocoy', N'Guiteb, Ramos, Tarlac', N'(0920)6206599', NULL, NULL, 1361)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171362, N'Regina Tabian', N'Prince/Queenie', N'Luna St., Carapdapan, Poblacion Norte, Paniqui, Tarlac', N'(0918)6802167', NULL, NULL, 1362)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171363, N'Janina Aquino', N'Noona', N'Capayaoan Compound, Moncada, Tarlac', N'(0943)3548865', NULL, NULL, 1363)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171364, N'Maria Rowena Modales', N'Jordan', N'Carino, Paniqui, Tarlac', N'(0926)7338675', NULL, NULL, 1364)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171365, N'Nestor Palencia', N'Miya', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'(0928)5735077', NULL, NULL, 1365)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171366, N'Joven Lobo', N'Peppa', N'Abogado, Paniqui, Tarlac', N'(0918)6692898', NULL, NULL, 1366)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171367, N'Maximo Corpuz ', NULL, N'49 Samput, Paniqui, Tarlac', NULL, NULL, NULL, 1367)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171368, N'Sheena Galapate ', N'Bambi', N'San Raymundo, Ramos, Tarlac ', N'(0910)0988927', NULL, NULL, 1368)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171369, N'Angelica Marie Beltran', N'Toffee', N'310 Magallanes St., Paniqui, Tarlac', N'(0947)6491869', NULL, NULL, 1369)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171370, N'Seagel Cala', N'Mocha', N'Moncada, Tarlac', NULL, NULL, NULL, 1370)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171371, N'Macel Fernando', N'Barba', N'Pob. #3, Moncada, Tarlac', N'(0912)0593289', NULL, NULL, 1371)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171372, N'Hernalyn Quinto', N'Zoey', N'Alexanre Heights, Samput, Paniqui, Tarlac', N'(0908)9989604', NULL, NULL, 1372)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171373, N'Janeina Pauline Saguyod', N'Anton', N'Sitio Caniogan, Matalapitap, Paniqui, Tarlac ', N'(0906)5027290', NULL, NULL, 1373)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171374, N'Rosalie Ibera', NULL, N'Canan, Paniqui, Tarlac', N'(0949)8162971', NULL, NULL, 1374)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171375, N'Geraldine Rasos', N'Callie', N'Coral, Ramos, Tarlac', N'(0921)9238326', NULL, NULL, 1375)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171376, N'Lee Jay Carino', N'Bruno', N'Baltazar Subd., Paniqui, Tarlac ', N'(0912)2440895', NULL, NULL, 1376)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171377, N'Magnolia Pagaduan', NULL, N'Poblacion Norte, Paniqui, Tarlac', N'(0932)7324796', NULL, NULL, 1377)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171378, N'Juvylyn Mayo', N'Diego', N'Sta. Maria, Moncada, Tarlac', N'(0917)8078144', NULL, NULL, 1378)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171379, N'Ana Mia Cajulao', N'Lucky', N'Alexanre Heights, Samput, Paniqui, Tarlac', N'(0932)6308101', NULL, NULL, 1379)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171380, N'Jackie Lou Moya', NULL, N'Rizal, Moncada, Tarlac', N'(0912)9988326', NULL, NULL, 1380)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171381, N'Rufina Baldoria', N'Smidgit', N'Mabini, Moncada, Tarlac', N'(0910)4518471', NULL, NULL, 1381)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171382, N'Meliza Sabado', N'Pong -Pong', N'Balaoang, Paniqui, Tarlac', N'(0977)7867338', NULL, NULL, 1382)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171383, N'Freddie Galleon Jr.', N'Misha', N'Dapdap,Paniqui, Tarlac', N'(0917)3019662', NULL, NULL, 1383)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171384, N'Imelda Alavazo', N'Chenggay', N'Camangaan West, Moncada Tarlac', N'(045)606925/(0917)8967039', NULL, NULL, 1384)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171385, N'Eijhay Cabuslay', N'Catzu', N'294 Canan, Paniqui, Tarlac', N'(0915)9710830', NULL, NULL, 1385)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171386, N'Flortesa Luis', N'Tres/ Kwatro', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'(0927)0440175', NULL, NULL, 1386)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171387, N'Kiana Chelsea Miranda', N'Hershey', N'Coral, Paniqui Tarlac', N'(0930)3291927', NULL, NULL, 1387)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171388, N'Tom Litosquen Jr.', N'Shamy', N'Villa Socorro, Pob.Norte, Paniqui, Tarlac', N'(0906)5133910', NULL, NULL, 1388)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171389, N'Lyra Dey Bustos', N'Sky/BongSoon', N'Purok Maligaya, Brgy. Abogado, Paniqui, Tarlac', N'(0999)9275924', NULL, NULL, 1389)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171390, N'Josefina Anastacio', N'Chi-Che', N'Don Ramon, Anao, Tarlac', N'(0919)8681406', NULL, NULL, 1390)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171391, N'Kimmy Valentin', N'Kola', N'Baltazar Subdivision, Paniqui, Tarlac', N'(045)9310394/(0939)9168897', NULL, NULL, 1391)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171392, N'Mundong Roxas ', N'Piolo', N'Abogado, Paniqui, Tarlac', NULL, NULL, NULL, 1392)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171393, N'Guila Baldonado', N'Scotty', N'Capayaoan Compound, Moncada, Tarlac', N'(0927)1719136', NULL, NULL, 1393)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171394, N'Alden De Aquino', N'Pablo', N'Baltazar Subdivision, Paniqui, Tarlac', N'(0917)8520910', NULL, NULL, 1394)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171395, N'Sheina Estacio', N'Yoshi', N'San Manuel, Tarlac', N'(0995)1643766', NULL, NULL, 1395)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171396, N'Suzette Corpuz', N'Annie', N'Almaville, Moncada, Tarlac', N'(0917)1598810', NULL, NULL, 1396)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171397, N'Dhemar Asidera', N'G Car', N'398 District 5, Ventinilla, Paniqui, Tarlac', N'(0938)9076618', N'Dhemar05@gmail.com', NULL, 1397)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171398, N'Abyss Daliva', N'Collie', N'678 Carino, Paniqui, Tarlac', N'(0926)8272724', NULL, NULL, 1398)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171399, N'Mary Ann Gison', N'Twerky', N'Camanggaan West, Moncada, Tarlac', N'(0917)6543237', NULL, NULL, 1399)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171400, N'Marianne Miguel', N'Luna', N'Estacion, Paniqui, Tarlac', N'(0912)0393330', NULL, NULL, 1400)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171401, N'Lauren Mariz Quines', N'Cloudy', N'49 San Francisco East, Anao, Tarlac', N'(045)606654/(0977)1344498', NULL, NULL, 1401)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171402, N'John Rey Manuel', N'Chumi/Lexie', N'65 Manaois, Paniqui, Tarlac', NULL, NULL, NULL, 1402)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171403, N'Argenta Reyes', N'Bella', N'Mabini, Moncada, Tarlac', N'(045)6066636/(0977)1420388', NULL, NULL, 1403)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171404, N'Rowena Sevilla', N'Aswad', N'61 San Roque, Anao, Tarlac', N'(0916)4592023', NULL, NULL, 1404)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171405, N'Resty Dumalay', N'Starla', N'027 Legaspi St. Poblacion Sur, Paniqui, Tarlac', N'(0910)5793322', NULL, NULL, 1405)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171406, N'Bryan Apol Policarpio', N'Kurt', N'San Francisco, Anao, Tarlac', N'(0930)1378313', NULL, NULL, 1406)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171407, N'Margarette Iglesias', N'Bucky', N'Caturay, Gerona, Tarlac', N'(0950)3491960', NULL, NULL, 1407)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171408, N'Arwin Tadeo', NULL, N'Camp Macabulos, Tarlac City', N'(0915)7213219', NULL, NULL, 1408)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171409, N'Micah Ross', N'Rum', N'Del Valle Estacion, Paniqui Tarlac', N'(0945)7886000', NULL, NULL, 1409)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171410, N'Aljen Bongon', N'Ahdi', N'Coral, Paniqui Tarlac', N'(0907)1992450', NULL, NULL, 1410)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171411, N'Dolores Vetus', N'Syrie', N'San Raymundo, Ramos, Tarlac ', N'(0948)4455221', NULL, NULL, 1411)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171412, N'Juliet Flores', N'Tammy', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'(0936)2073679', NULL, NULL, 1412)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171413, N'Gilda Tolentino', N'Duke', N'San Julian, Moncada, Tarlac', N'(0947)7645233', NULL, NULL, 1413)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171414, N'Angelica Abella', N'Cindy', N'Cabawangan, Nampicuan, Nueva Ecija', N'(0917)8284268', NULL, NULL, 1414)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171415, N'Nicsan Castro De Francia ', N'Nicolas', N'Paniqui, Tarlac', N'(0930)9348075', NULL, NULL, 1415)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171416, N'Leticia Adaoag', N'Bambi', N'Pasuquin St., Paniqui, Tarlac', N'(0926)0045198', NULL, NULL, 1416)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171417, N'Glorie Ramos', N'Cookie', N'Poblacion Sur, Paniqui, Tarlac', N'(0916)3446072', NULL, NULL, 1417)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171418, N'Christian Jay Tandang', N'Sunsun', N'Alexanre Heights, Samput, Paniqui, Tarlac', N'(0950)2315242', NULL, NULL, 1418)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171419, N'Joel Balaba', N'Bravo', N'Lapsing, Moncada Tarlac', N'(0929)1429640', N'balaba.joel@yahoo.com', NULL, 1419)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171420, N'Cora S. Gundran', N'Chuckie', N'220 Samput, Paniqui, Tarlac', N'(0918)24555563', NULL, NULL, 1420)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171421, N'Mauricio Galang', N'Verna', N'Southeast Poblacion, Nampicuan, Nueva Ecija', N'(0977)1776291', N'galang.mauricio@gmail.com', NULL, 1421)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171422, N'RR Melegrito', N'Bingo', N'336 Mabini, Moncada, Tarlac', N'(0915)8419697', NULL, NULL, 1422)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171423, N'Jhon Matthew Isunza', N'Sky', N'Canan, Paniqui, Tarlac', N'(0945)3347274', NULL, NULL, 1423)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171424, N'Boyet Ting', N'Mikoy', N'Carino, Paniqui, Tarlac', N'(0999)5244240', NULL, NULL, 1424)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171425, N'Jaypee Dela Pena', N'Gab', N'Abogado, Paniqui, Tarlac', N'(0995)8715997', NULL, NULL, 1425)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171426, N'Nwelorine Maloles', N'Tiny', N'Gerona, Tarlac', N'(0917)6257595', NULL, NULL, 1426)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171427, N'Daicy Gacusan', N'Loonee', N'Samput, Paniqui, Tarlac', N'(0938)9074793', NULL, NULL, 1427)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171428, N'Michelle Ganzon', N'Daiso', N'Rang- Ayan, Paniqui, Tarlac', N'(0977)4763722', NULL, NULL, 1428)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171429, N'Jerry Asanion', N'Misha', N'TDK Estacion, Paniqui, Tarlac', N'(0955)7729872', NULL, NULL, 1429)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171430, N'Christopher Aguilar/ Manilyn Crisostomo', N'Cody', N'Guimba, Nueva Ecija', N'(0929)8530944', NULL, NULL, 1430)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171431, N'Krishia Mae De Ramos', N'Summer', N'240 Carriedo St., Paniqui, Tarlac', N'(0915)27900059', NULL, NULL, 1431)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171432, N'Jovan Soriano', N'Stan/Seth/Boy', N'Moncada, Tarlac', N'(0917)7921583', NULL, NULL, 1432)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171433, N'Benedick Mendoza', N'Tisoy', N'Sta. Maria, Moncada, Tarlac', N'(0948)2207891', NULL, NULL, 1433)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171434, N'Jennifer Atencio', N'Wacko/Gucci', N'Moncada, Tarlac', N'(0915)56276179', NULL, NULL, 1434)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171435, N'Bethzaida Dadural', N'Sophie', N'Sto. Tomas Batangas', N'(0977)8056719', NULL, NULL, 1435)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171436, N'Irene Gregorio', N'Lucky', N'Nipaco, Paniqui, Tarlac', N'(0948)6068582', NULL, NULL, 1436)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171437, N'Mary Ann Dangla', N'Toby', N'Abogado, Paniqui, Tarlac', N'(0915)5056252', NULL, NULL, 1437)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171438, N'Jillian Alvarez', N'Potchi', N'Zamora St. Paniqui, Tarlac', NULL, NULL, NULL, 1438)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171439, N'Sheila Marquez', N'Cassie', N'Capaoayan Compound, Moncada, Tarlac', N'(0929)4538023', NULL, NULL, 1439)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171440, N'Gladeliene Mamaril', N'Tiger', N'Abogado, Paniqui, Tarlac', N'(0930)9001355', NULL, NULL, 1440)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171441, N'Elizabeth Nicdao', N'Yabang', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'491-7747', NULL, NULL, 1441)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171442, N'Eugene Cabidoy', N'Spongie', N'Sta. Rosa, Pob. Sur, Paniqui Tarlac', N'(0950)1588634', NULL, NULL, 1442)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171443, N'Raven Joy De Jesus', N'Lexie', N'Salumague, Paniqui, Tarlac', N'045-6285369/09236933595', NULL, NULL, 1443)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171444, N'Dr. Manalastas', NULL, N'Samput, Paniqui, Tarlac', NULL, NULL, NULL, 1444)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171445, N'Mike Ryvert Astrera', N'Cooper', N'Samput, Paniqui, Tarlac', N'(0927)2066062', NULL, NULL, 1445)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171446, N'Cristine Bueno', N'Buddy', N'Oloybuaya, Gerona, Tarlac', N'(0917)8854214', NULL, NULL, 1446)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171447, N'Dane Jowart Javier', N'Botchok', N'Calle 11 Pob. 1 Gerona', N'(0927)2187823', NULL, NULL, 1447)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171448, N'Analyn Sabado', N'Brandy', N'Pob. Center, Ramos, Tarlac', N'(0919)8753793', NULL, NULL, 1448)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171449, N'Catherine/Jayson Gaspar', N'Pipa', N'Purok 2 Pance Ramos', N'(0995)4875170', NULL, NULL, 1449)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171450, N'Cesar Garcia', N'Chivas', N'San Manuel, Tarlac', N'(0977)6110387', NULL, NULL, 1450)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171451, N'Amie Bamba', N'Bimbi', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'(0916)3333923', NULL, NULL, 1451)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171452, N'Cherry Rose Patricio', N'Zoie', N'Camangaan East, Moncada Tarlac', N'(0967)2373803', NULL, NULL, 1452)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171453, N'Leah Africa', N'Prestone', N'Nampicuan, Nueva Ecija', N'(0955)5634345', NULL, NULL, 1453)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171454, N'Dianne Angel', N'Loki', N'Purok 4 Maungib, Pura, Tarlac', N'(0945)5749047', NULL, NULL, 1454)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171455, N'Lani Llana', N'Bubbles', N'Poblacion 3, Gerona, Tarlac', N'(0915)3536833', NULL, NULL, 1455)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171456, N'Dyan Techico Avila', N'Mini Mini', N'Moncada, Tarlac', N'6065319', NULL, NULL, 1456)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171457, N'Rodel Pagco', N'Zoe', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 1457)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171458, N'Randy Alfaro', NULL, N'Moncada, Tarlac', N'(0946)4199666', NULL, NULL, 1458)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171459, N'Julie Ann Estoesta', N'ChumChum', N'Sulipa, Gerona, Tarlac', N'(0935)43471647', NULL, NULL, 1459)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171460, N'Madonna Daquigan', N'L', N'Sinigpit, Paniqui, Tarlac', N'(0929)7183428', NULL, NULL, 1460)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171461, N'Elisha Nuezca', N'Mozzart', N'Cabayaoasan, Paniqui, Tarlac', N'(0928)1972811', NULL, NULL, 1461)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171462, N'Marvie Locadine', N'Sprinkles', N'Alexanre Heights, Samput, Paniqui, Tarlac', N'(0916)6682251', NULL, NULL, 1462)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171463, N'Mila Abubo', N'Cloe', N'Abagon, Gerona, Tarlac', N'(0909)9966263', NULL, NULL, 1463)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171464, N'Ommar Cali', N'Amira', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'9483412936', NULL, NULL, 1464)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171465, N'Maricel Lapada', NULL, N'Poblacion #4, Moncada, Tarlac', N'(0933)0811450', NULL, NULL, 1465)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171466, N'Aida Vargas', NULL, N'Camp 1 Sur, Moncada, Tarlac', N'(0938)8611005', NULL, NULL, 1466)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171467, N'Princess Avi Gamboa', N'Potchi', N'Abogado, Paniqui, Tarlac', N'(0945)7883773', NULL, NULL, 1467)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171468, N'Bernard Pabalan', N'Lemon', N'Lanat, San Manuel, Tarlac', N'9190059531', NULL, NULL, 1468)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171469, N'Cecilio Mendoza Jr.', N'Landa', N'12 Dahlia St., Cayanga, Paniqui, Tarlac', N'(0998)2851688', NULL, NULL, 1469)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171470, N'Rosemarie Astrera', N'Roxy', N'Samput, Paniqui, Tarlac', N'(0921)2246885', NULL, NULL, 1470)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171471, N'Karla Mikka Terrado', N'Xanique', N'Cabawangan, Nampicuan, Nueva Ecija', N'(0916)3341950', NULL, NULL, 1471)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171472, N'Lady Brenlie Bernardino', N'LeeShu/Starla', N'Sulipa, Gerona, Tarlac', N'(0997)9584131', NULL, NULL, 1472)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171473, N'Lailani Salunga', N'Quinsy', N'San Vicente, Tarlac', N'(0939)3771877', NULL, NULL, 1473)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171474, N'Novelyn Daileg', N'Mayumi', N'Nampicuan, Nueva Ecija', N'(0927)4335800', NULL, NULL, 1474)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171475, N'Agnes Garcia', N'Kimi', N'Rizal, Moncada, Tarlac', N'(0939)9101150', NULL, NULL, 1475)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171476, N'Zyna Marie Alnas', N'Groover', N'Poblacion 1 , Pura, Tarlac', N'(0912)0293937', NULL, NULL, 1476)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171477, N'Alvin Feria', N'Ice ', N'1806 Baltazar Subd., Paniqui, Tarlac', N'(045)6060933/(0905)2305604', NULL, NULL, 1477)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171478, N'Hiera Kim Narbay', N'Lucas', N'80 Ambassador Alzate Village, Nampicuan, Nueva Ecija', N'(0950)2579412', NULL, NULL, 1478)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171479, N'Piolo Ayson', N'Zed ', N'Pob. Norte, Paniqui, Tarlac', N'(0967)6735213', NULL, NULL, 1479)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171480, N'Reinne Supan', N'Bella', N'30 Canan, Paniqui, Tarlac', N'(0922)2011240/(0917)1446317', NULL, NULL, 1480)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171481, N'Liberty Santos', N'Cindy', N'Cuyapo, Nueva Ecija', N'(0921)5149082', NULL, NULL, 1481)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171482, N'Mark Sacatropez', N'Primo', N'Coral, Ramos, Tarlac', N'(0995)7680688', NULL, NULL, 1482)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171483, N'Jocelyn Garcia', N'Dambo ', N'Caturay, Gerona, Tarlac', N'(0947)2385065', NULL, NULL, 1483)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171484, N'Beannette Cayton', N'Ka Bebang', N'San Jose North, Anao, Tarlac', N'(0922)5906022', NULL, NULL, 1484)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171485, N'Cesar Falguera', N'Popoy', N'Alexandre Heights, Samput, Paniqui, Tarlac', N'(0918)3094281', NULL, NULL, 1485)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171486, N'Milagros Rodriguez', N'Eldon', N'Brgy. Ambassador, Nampicuan, Nueva Ecija', N'(0917)1296230/(0945)3394598', NULL, NULL, 1486)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171487, N'Christian Ed Yago ', N'Bella', N'Mabini, Moncada, Tarlac', N'(0966)3271735', NULL, NULL, 1487)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171488, N'Juan Paolo Galeon', N'Beatriz', N'Moncada, Tarlac', N'(0909)6717167', NULL, NULL, 1488)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171489, N'Maricris Mapanao', N'Mercedes', N'Salumague, Paniqui, Tarlac', N'(0995)4606250', NULL, NULL, 1489)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171490, N'Edward Joseph Mamucod', N'Tiffany', N'Salumague, Paniqui, Tarlac', N'8006838/(0995)4606250', NULL, NULL, 1490)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171491, N'Riza Pabia', N'Una', N'Patalan, Paniqui, Tarlac', N'(0995)7847597', NULL, NULL, 1491)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171492, N'Ailyn Pagaduan', N'Maicee', N'Dicolor, Gerona, Tarlac', N'(0950)2988214', NULL, NULL, 1492)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171493, N'Belinda Jeresano', N'Coco', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'(0949)8636944', NULL, NULL, 1493)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171494, N'Edwin Buenaobra', N'Cash', N'Pob. North, Ramos, Tarlac', N'(0908)1933564', NULL, NULL, 1494)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171495, N'Marie Dayrit', N'Thor', N'Linsao Subd., Abogado, Paniqui, Tarlac', N'(0917)1245823', NULL, NULL, 1495)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171496, N'Dianne Coliamco', N'Stark', N'Estacion, Paniqui, Tarlac', N'(0977)4638658', NULL, NULL, 1496)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171497, N'Bing Balignasay', N'Chacha', N'128 Samput, Paniqui, Tarlac ', N'(0999)9042881', NULL, NULL, 1497)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171498, N'Agnes Buenaflor ', N'Parker', N'Cabaducan East, Nampicuan, Nueva Ecija', NULL, NULL, NULL, 1498)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171499, N'Lorna Manalili', N'Rojo', N'Villa Socorro , Pob. Norte, Paniqui, Tarlac', N'(0921)4450691', NULL, NULL, 1499)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171500, N'Celine Poco', N'Mocha/Snow', N'Canan, Paniqui, Tarlac', N'045-985-6295/09502882086', NULL, NULL, 1500)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171501, N'Daniela Joy Macaraeg', N'Lulu', N'Abogado, Paniqui, Tarlac', N'(0918)2405563', NULL, NULL, 1501)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171502, N'Richard Orpilla', N'Grezy', N'Lapsing, Moncada Tarlac', N'(0956)6905225', NULL, NULL, 1502)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171503, N'Winnie Ann Madayag', N'Mocha', N'Calamay, Moncada, Tarlac', N'(0929)7060085', NULL, NULL, 1503)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171504, N'Shielanie Fabia', NULL, N'Aglipay St. Campo Santo 1, Moncada, Tarlac', N'(0927)41711746', NULL, NULL, 1504)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171505, N'Daniel Gian Dizon', N'Bambu', N'Estacion, Paniqui, Tarlac', N'045-931-1847/09369505469', NULL, NULL, 1505)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171506, N'Richard Yap Perez', N'Daphne', N'Bugallon Estacion, Paniqui, Tarlac', N'951-1783/09954634646', NULL, NULL, 1506)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171507, N'Sofia Ann Tabago', N'Prince', N'Sapang, Moncada Tarlac', N'(0916)5742672', NULL, NULL, 1507)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171508, N'Nelly Javier', N'Khobe', N'Alamania, Nampicuan, Nueva Ecija', NULL, NULL, NULL, 1508)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171509, N'Norbelyn Hernandez Madrona', N'Zoey', N'Abogado, Paniqui, Tarlac', N'(0956)6085529', NULL, NULL, 1509)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171510, N'Richard Palarca', N'Mocha', N'Ramos Subd. Villasoccoro, Pob. Norte, Paniqu, Tarlac', N'(0948)4696277', NULL, NULL, 1510)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171511, N'Teresita Manuel', N'Ella', N'Cayanga, Paniqui, Tarlac', N'(0998)2474892', NULL, NULL, 1511)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171512, N'Lorna Domingo', N'Saver', N'West Poblacion, Nampicuan, Nueva Ecija', N'(0948)0227966', NULL, NULL, 1512)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171513, N'Maria Theresa Roxas', NULL, N'Salumague, Paniqui, Tarlac', N'(0995)1962796', NULL, NULL, 1513)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171514, N'Ray Mark Solomon', N'Dora', N'Pob. Norte, Paniqui, Tarlac', N'(0906)5674726', NULL, NULL, 1514)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171515, N'Zuen Tayao', N'Angel', N'Manaois, Paniqui, Tarlac', N'(0999)5821738', NULL, NULL, 1515)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171516, N'Genesis Alcantara', N'Beauty', N'Tolega Sur, Moncada, Tarlac', N'(0909)2029051', NULL, NULL, 1516)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171517, N'Ma. Rosario Rowena Estanislao', NULL, N'Zamora St. Paniqui, Tarlac', N'045-9310041/09237308271', NULL, NULL, 1517)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171518, N'Renzon  Despi', N'Charm ', N'Poblacion 2, Moncada, Tarlac ', N'(0967)6387070', NULL, NULL, 1518)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171519, N'Josephine Molina', N'Twixy', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'(0916)3149782', NULL, NULL, 1519)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171520, N'Ethel Grace Santos', N'Copper', N'Patalan, Paniqui, Tarlac', N'(0917)3037528', NULL, NULL, 1520)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171521, N'Jayson Obenario', NULL, N'Balaoang, Paniqui, Tarlac', N'(0966)3356680', NULL, NULL, 1521)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171522, N'Maria Karisma Mercado', N'Febby', N'Landig, Cuyapo. Nueva Ecija', N'(0919)8686520', NULL, NULL, 1522)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171523, N'Edwin Palarca', N'Cactus', N'Apulid, Paniqui, Tarlac', N'(0999)7853060', NULL, NULL, 1523)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171524, N'Rolando Pimentel', N'Jill', N'Danzo, Gerona Tarlac', N'(0922)2588269', NULL, NULL, 1524)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171525, N'Karla Villapando', N'Faith', N'Del Valle Estacion, Paniqui Tarlac', N'(0995)4694979', NULL, NULL, 1525)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171526, N'Joel Saberola', N'Flaxy', N'Villa Socorro St., Pob. Norte, Paniqui, Tarlac', N'0945-276-2073', NULL, NULL, 1526)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171527, N'Dante David', N'Billy', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0910)6976868', NULL, NULL, 1527)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171528, N'Ralph Allyne Mendoza', N'Sachi', N'Apulid, Paniqui, Tarlac', NULL, NULL, NULL, 1528)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171529, N'Olivia De Castro', N'Kela', N'Manaois, Paniqui, Tarlac', N'(0917)3261203', NULL, NULL, 1529)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171530, N'Nora Deuna', N'Koda', N'Poblacion Sur, Camiling, Tarlac', N'(0905)6641741', NULL, NULL, 1530)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171531, N'Marcosa Castillo', N'Cielo ', N'Samput, Paniqui, Tarlac', N'(0949)5755373', NULL, NULL, 1531)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171532, N'Syiin Cordero', N'Happy', N'Samput, Paniqui, Tarlac', N'(0917)9493065', NULL, NULL, 1532)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171533, N'Wendell Bien Carballo', N'Bruno', N'Luna St., Carapdapan, Poblacion Norte, Paniqui, Tarlac', N'(0995)9510109', NULL, NULL, 1533)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171534, N'Criselda & Raymond Mahor', N'Choy-Choy/ Tammy', N'Poblacion #4, Moncada, Tarlac', N'(09282591175', NULL, NULL, 1534)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171535, N'Gerald Bautista', N'A- Conce', N'Plastado, Gerona, Tarlac', N'(0945)4254637', NULL, NULL, 1535)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171536, N'Janna Paragas', N'Problem', N'Balsa, Paniqui, Tarlac', N'(0919)7424252', NULL, NULL, 1536)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171537, N'Chona David', N'O'' nneal', N'264 San Isidro, Paniqui, Tarlac', N'(0919)4031785', NULL, NULL, 1537)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171538, N'Leny Oliveros', N'Chab-Chab', N'Carmen, Anao, Tarlac', N'(0919)4244355', NULL, NULL, 1538)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171539, N'Virginia Abad', N'Russi', N'Aringin, Moncada Tarlac', N'(0966)7307090', NULL, NULL, 1539)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171540, N'Robert Ordonio', N'Tin- Tin', N'695 Caburnay St. Paniqui, Tarlac', N'(0948)7936762', NULL, NULL, 1540)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171541, N'Jason Mendoza', N'Barkley', N'Del Carmen St., Lanat, San Manuel, Tarlac', N'(0925)7730791', NULL, NULL, 1541)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171542, N'Eugine Jimenez', N'Bruce', N'Samput, Paniqui, Tarlac', N'(0977)3280901', NULL, NULL, 1542)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171543, N'Marife Tabita', N'Eros', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'(0918)3897710', NULL, NULL, 1543)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171544, N'Ferdinand Manalo', N'Browty', N'Canan, Paniqui, Tarlac', N'(0933)8091908', NULL, NULL, 1544)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171545, N'Lloyd Lagasca', N'Mia', N'Plastado, Gerona, Tarlac', N'(0977)3252535', NULL, NULL, 1545)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171546, N'Melody Parazo', N'Coby', N'Pob. 2, Moncada, Tarlac', N'(0930)02130050', NULL, NULL, 1546)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171547, N'Queenie Arvian Ramirez', N'Chewie', N'San Juan, Moncada, Tarlac', N'(0936)5633426', NULL, NULL, 1547)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171548, N'Althea Kayla Ramos', N'Bailey', N'Carino, Paniqui, Tarlac', N'(0912)8606534', NULL, NULL, 1548)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171549, N'Charisma Velasco', N'Goku', N'Manaois, Paniqui, Tarlac', N'(0975)6461842', NULL, NULL, 1549)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171550, N'Danilla Weng Martinez', N'Bela', N'Camposanto #2, Moncada, Tarlac', N'(0932)7162787', NULL, NULL, 1550)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171551, N'Elena Antonio/Jamella Carpio', N'Beejay', N'Villa Socorro, Poblacion Norte, Paniqui, Tarlac', N'(0939)1940790', NULL, NULL, 1551)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171552, N'Rachelle Abizar', N'RR', N'San Julian, Moncada, Tarlac', N'(0928)5631053', NULL, NULL, 1552)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171553, N'Vanessa Yongco', N'Paw-Paw', N'Rizal, Moncada, Tarlac', N'(0916)4975735', NULL, NULL, 1553)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171554, N'Jannah Paculguin', N'Cassy', N'Cabawangan, Nampicuan, Nueva Ecija', N'(0977)3668161', NULL, NULL, 1554)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171555, N'Nerissa Tangalin', N'Taguro', N'North Poblacion, Nampicuan Nueva Ecija', N'(0905)2727300', NULL, NULL, 1555)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171556, N'Richell Valdez', N'Ulap', N'Sinigpit, Paniqui, Tarlac', N'(0955)1288005', NULL, NULL, 1556)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171557, N'Marigel Maniego', N'Toothless', N'Ramos Tarlac', N'(0995)5408578', NULL, NULL, 1557)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171558, N'Agnes Chan', N'Skater', N'Samput, Paniqui, Tarlac', NULL, NULL, NULL, 1558)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171559, N'Charles Spain / Nila Ferrer', N'Buddy', N'Sta. Maria, Moncada, Tarlac', N'(0916)5019111', NULL, NULL, 1559)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171560, N'Camia Calonge', N'Kaydee', N'Latap, Cuyapo Nueva Ecija', N'(0906)3814429', NULL, NULL, 1560)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171561, N'Diosa Diza', N'Toby', N'Poblacion, San Manuel, Tarlac', N'(0927)2230509', NULL, NULL, 1561)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171562, N'Lyra Inocencio', N'Chewee', N'Pob. Norte, Paniqui, Tarlac', N'(0939)9543439', NULL, NULL, 1562)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171563, N'Chris Sabat', N'Ash', N'Matalapitap, Paniqui, Tarlac', N'(0917)5260120', NULL, NULL, 1563)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171564, N'Madeleine Roxas', N'Panpan', N'Pob. 3 Moncada Tarlac', N'(0908)1473368', NULL, NULL, 1564)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171565, N'Yeng Lavitoria', N'Polar', N'Samput, Paniqui, Tarlac', N'(0977)1955366', NULL, NULL, 1565)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171566, N'Jazline Jade Bernal', N'Choco', N'Pob. 1 Moncada Tarlac', NULL, NULL, NULL, 1566)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171567, N'Jenny Diza', N'Tootsie Roll', N'Poblacion San Manuel', N'(0923)0909284', NULL, NULL, 1567)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171568, N'Prince Roland Agustin', N'Ross', N'Del Valle Estacion, Paniqui Tarlac', N'(0947)4214270', NULL, NULL, 1568)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171569, N'Maui Castillo', N'Mimo', N'District 2 Cuyapo Nueva Ecija', N'(0977)0826438', NULL, NULL, 1569)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171570, N'Kim Alyana Castañeda', N'Daphne', N'Rizal, Moncada, Tarlac', N'(0995)4636646', NULL, NULL, 1570)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171571, N'Marites Ostertag', N'Tommy', N'Ramos, Tarlac', N'(0977)6901032', NULL, NULL, 1571)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171572, N'Vivian Andres', N'Thor', N'District 8 Cuyapo Nueva Ecija', N'(0995)2351955', NULL, NULL, 1572)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171573, N'Editha Garingo', N'Minnie', N'Samput, Paniqui, Tarlac', N'(0906)5591298', NULL, NULL, 1573)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171574, N'Kim Adriel Capinpin', N'Browna', N'Coral, Paniqui Tarlac', N'(0998)4089609', NULL, NULL, 1574)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171575, N'Lorenza Castillo', NULL, N'Camiling Tarlac', N'(0939)1165106', NULL, NULL, 1575)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171576, N'Mary Kristine Briones', N'Uno', N'San Vicente, Victoria, Tarlac', NULL, NULL, NULL, 1576)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171577, N'Jonathan B. Delos Santos', N'Paper', N'Cabaducan West, Nampicuan, Nueva Ecija', N'(0956)9477764', NULL, NULL, 1577)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171578, N'Sonia Uy Dy', N'Krusher', N'Samput, Paniqui, Tarlac', N'(0917)5145689', NULL, NULL, 1578)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171579, N'Sweeth Angel Egay', N'Stormii', N'Coral, Paniqui Tarlac', N'(0910)5318348', NULL, NULL, 1579)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171580, N'Vanessa Jane Llamas', N'Pot-pot/Chloe', N'Abogado, Paniqui, Tarlac', N'(0995)1201163', NULL, NULL, 1580)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171581, N'Cecille Gueco', N'Alpha', N'Paniqui, Tarlac', N'(0977)4644065', NULL, NULL, 1581)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171582, N'Ronald Calpito', N'Miya', N'Camposanto 1 Sur, Moncada, Tarlac', N'(0998)9862215', NULL, NULL, 1582)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171583, N'Joana Bernaldez', N'Uno', N'Lacayanga Subd., Paniqui, Tarlac', N'(0905)5550714', NULL, NULL, 1583)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171584, N'Raymart Gonzales', N'Rock', N'Guiteb, Ramos, Tarlac', N'(0977)6788791', NULL, NULL, 1584)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171585, N'Dennis Bryian Flores', N'Noshi', N'San Leon, Moncada, Tarlac', N'(0948)6220879', NULL, NULL, 1585)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171586, N'Cesar Espiritu', N'Lucas', N'Canan, Paniqui, Tarlac', N'(0921)5309902', NULL, NULL, 1586)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171587, N'Marianne Balignasay', N'Mucho', N'Moncada, Tarlac', N'(0938)1640020', NULL, NULL, 1587)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171588, N'Carla Gison', N'Coco', N'Gerona, Tarlac', N'491-5341/09228559972', NULL, NULL, 1588)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171589, N'Gerald Jay Bayno', N'Trump', N'Balaoang, Paniqui, Tarlac', N'(0956)4403127', NULL, NULL, 1589)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171590, N'Mercy Manalese', N'Mochi', N'Apulid, Paniqui, Tarlac', N'(0966)9329775', NULL, NULL, 1590)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171591, N'Lady Regina Brillante', N'Aecha', N'Magaspac, Gerona, Tarlac', N'(0907)1702437', NULL, NULL, 1591)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171592, N'Christopher Garcia', N'Basyang', N'Marawi, Camiling Tarlac', N'(0919)5260690', NULL, NULL, 1592)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171593, N'Ryan Zinampan', N'Hyden', N'San Jose, Cuyapo Nueva Ecija', N'09952103301/0917869902', NULL, NULL, 1593)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171594, N'Camille De Lara', N'Chuchay', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'(0956)9109561', NULL, NULL, 1594)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171595, N'Dennis Grande', N'Shady', N'San Pedro, Moncada, Tarlac', N'(0949)4688888', NULL, NULL, 1595)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171596, N'Michelle Simeon', N'Brandy', N'Apsayan Gerona Tarlac', N'09230217979/09496735349', NULL, NULL, 1596)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171597, N'Randolph Ongpauco', N'Saki', N'Estacion, Paniqui, Tarlac', N'09954224217/09177483132', NULL, NULL, 1597)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171598, N'Hermina Garcia', N'Kobe', N'Lacayanga Subd., Paniqui, Tarlac', N'(0926)7342838', NULL, NULL, 1598)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171599, N'David Bren Macaraeg', N'Dobie', N'Pob. Norte, Paniqui, Tarlac', N'(0906)1017009', NULL, NULL, 1599)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171600, N'Jay Ian Pascual', N'Appa', N'Cabaducan East, Nampicuan, Nueva Ecija', N'(0927)6834412', NULL, NULL, 1600)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171601, N'Pamela Grace Lagasca', N'Chase/ Holly', N'Canan, Paniqui, Tarlac', N'9167866646', NULL, NULL, 1601)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171602, N'Emilbert Mina', N'Hershee', N'Poblacion South, Ramos, Tarlac', N'(0906)2059950', NULL, NULL, 1602)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171603, N'Tina Marie DL. Belton', N'Tubby', N'Rizal, Moncada, Tarlac', N'9293895891', NULL, NULL, 1603)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171604, N'Andria Viluan', N'Chiboy', N'Coral, Paniqui Tarlac', N'9560669426', NULL, NULL, 1604)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171605, N'Princess Gail Suniga', N'Auzee', N'Apulid, Paniqui, Tarlac', N'9984376365', NULL, NULL, 1605)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171606, N'Ludy M. Corpuz', N'Bella', N'Sitio Rasig, Acocolao, Paniqui, Tarlac', N'9298279483', NULL, NULL, 1606)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171607, N'Jomeldy C. Gomez', N'Aye', N'Lacayanga Subd., Paniqui, Tarlac', N'9672374371', NULL, NULL, 1607)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171608, N'Candy Alcantara', N'Chichi', N'Samput, Paniqui, Tarlac', N'9463979088', NULL, NULL, 1608)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171609, N'Sammy Sapad', N'Max', N'Plastado, Gerona, Tarlac', N'09669307645/09175418925', NULL, NULL, 1609)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171610, N'Ruthly M. Tan', N'Muchi', N'San Francisco East , Anao, Tarlac', N'9178714060', NULL, NULL, 1610)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171611, N'Ajhon Khier Cruz', N'Cassey', N'Salumague, Paniqui, Tarlac', N'9062889019', NULL, NULL, 1611)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171612, N'Leandro Cortez', N'Ninja', N'Samput, Paniqui, Tarlac', N'9485938666', NULL, NULL, 1612)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171613, N'Kathleen Bartolome', N'Chay- Chay', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'94699033950', NULL, NULL, 1613)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171614, N'Carmela Andres', N'Angel/Cassey', N'Abogado, Paniqui, Tarlac', N'9479942280', NULL, NULL, 1614)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171615, N'Maricris Lagman', N'Simon', N'Abogado, Paniqui, Tarlac', N'9267889351', NULL, NULL, 1615)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171616, N'Jomer Grande', N'Mia Khalipa', N'Carmen, Anao, Tarlac', N'97710272265', NULL, NULL, 1616)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171617, N'Ma. Socorro Barbon', N'Zia', N'San Roque, Anao', N'9100280811', NULL, NULL, 1617)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171618, N'Patricia Joanne Arauilo', N'Makey', N'Samput, Paniqui, Tarlac', N'9777791399', NULL, NULL, 1618)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171619, N'Jenielyn Aquillo', N'Beauty', N'Coral, Paniqui Tarlac', N'9062048398', NULL, NULL, 1619)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171620, N'Mayumi Lopez', N'Lucky', N'Coral, Paniqui Tarlac', N'9430770442', NULL, NULL, 1620)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171621, N'Sheina Manalo', N'Coltton', N'Samput, Paniqui, Tarlac', N'99230966', NULL, NULL, 1621)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171622, N'Katrine Shaira Pazcoguin', N'Tokyo', N'Estacion, Paniqui, Tarlac', N'9310064', NULL, NULL, 1622)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171623, N'Lito Dominic Calvero', N'Cooper', N'Pob. Norte, Paniqui, Tarlac', N'9197424319', NULL, NULL, 1623)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171624, N'Kyla Aquino', N'Uno', N'Coral, Paniqui Tarlac', N'9394978428', NULL, NULL, 1624)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171625, N'Rustom Obcena', N'Milo', N'Tablang, Paniqui, Tarlac', N'9569103203', NULL, NULL, 1625)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171626, N'Margie Lomboy', N'Marvy', N'Samput, Paniqui, Tarlac', N'9393795926', NULL, NULL, 1626)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171627, N'Noel Christian Lazo', N'Evana', N'Mangandingan, San Manuel, Tarlac', N'9469908114', NULL, NULL, 1627)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171628, N'Renato Basco', N'Twix', N'Coral, Paniqui Tarlac', N'9669708527', NULL, NULL, 1628)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171629, N'Christine Joy Corral', N'Peachy', N'Pob. Norte, Paniqui, Tarlac', N'9062045019', NULL, NULL, 1629)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171630, N'Bonifacio Hilario', N'Harbor', N'Salumague, Paniqui, Tarlac', N'9151943088', NULL, NULL, 1630)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171631, N'Gerald Luis Mene', N'Cesar', N'Apulid, Paniqui, Tarlac', N'9177791682', NULL, NULL, 1631)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171632, N'Vherna Mae Banag', N'Reign', N'Pob. North, Ramos, Tarlac', N'9204981150', NULL, NULL, 1632)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171633, N'Rodolfo Abuan jr.', N'Akira', N'Baquero Sur, Moncada, Tarlac', N'9869700723', NULL, NULL, 1633)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171634, N'Jose Erasto Antonio Tayag', NULL, N'Sta. Ines Paniqui, Tarlac', N'9152476755', NULL, NULL, 1634)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171635, N'Jeniffer Apostol', N'Santino', N'Bugallon st. paniqui, Tarlac', N'9054474406', NULL, NULL, 1635)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171636, N'Joselito Mapuyan', N'Primo', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'9338211529', NULL, NULL, 1636)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171637, N'Darline Janeth Triste', NULL, N'Dalayaoan Subd., Paniqui, Tarlac', N'9328587576', NULL, NULL, 1637)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171638, N'Charmaine Rodriguez', N'Naruto', N'Salumague, Paniqui, Tarlac', N'9208887877', NULL, NULL, 1638)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171639, N'Genwilito Bala', N'Boomer', N'Magaspac, Gerona, Tarlac', N'9122209927', NULL, NULL, 1639)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171640, N'Christine Joy Santiago', N'Luffy', N'Villa Socorro, Poblacion Norte, Paniqui, Tarlac', N'(0917)5660705', NULL, NULL, 1640)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171641, N'Jonelle Visaya', N'Logan', N'Dicolor, Gerona, Tarlac', N'9982728180', NULL, NULL, 1641)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171642, N'Rosalie Salinas', N'Kobe', N'Aduas, Paniqui, Tarlac', N'9206114482', NULL, NULL, 1642)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171643, N'Rose Manzano', N'Koby', N'Abogado, Paniqui, Tarlac', N'9270617345', NULL, NULL, 1643)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171644, N'Elmer Pagco jr.', N'Gucci', N'M.H. Del Pilar St. Paniqui, Tarlac', N'9105318829', NULL, NULL, 1644)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171645, N'Manilyn Lumague', N'Namie', N'Pob. Norte, Paniqui, Tarlac', N'9166317508', NULL, NULL, 1645)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171646, N'Melanie Prado', NULL, N'Paniqui, Tarlac', N'9985580356', NULL, NULL, 1646)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171647, N'Prince Jeffrey Facunla', N'Hershey', N'Palarca St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9125098185', NULL, NULL, 1647)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171648, N'Frecie Gruspe', N'Milo', N'Balaong Paniqui, Tarlac', N'9358892580', NULL, NULL, 1648)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171649, N'Donna Panaligan', N'Milo', N'Bugallon st. paniqui, Tarlac', N'9567835164', NULL, NULL, 1649)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171650, N'Loreta Baniqued', N'Brutus', N'Balaoang, Paniqui, Tarlac', N'9270641179', NULL, NULL, 1650)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171651, N'Joven Saranilla', N'Chuchay', N'Purok 2 Samput, Paniqui, Tarlac', N'9611836287', NULL, NULL, 1651)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171652, N'Charlie Cadayan', N'Macmac', N'Carino, Paniqui, Tarlac', N'9166667636', NULL, NULL, 1652)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171653, N'Jona Flores', N'Master Bruce', N'Canan, Paniqui, Tarlac', N'9302280340', NULL, NULL, 1653)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171654, N'Kristine Joy Sudaria', N'Kobie', N'Mabilang, Paniqui, Tarlac', N'9503456369', NULL, NULL, 1654)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171655, N'Hazel Tangonan', NULL, N'Patalan, Paniqui, Tarlac', N'9950911836', NULL, NULL, 1655)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171656, N'Herminia Lagman', N'Coco', N'Dalayaoan Subd., Paniqui, Tarlac', N'910139308', NULL, NULL, 1656)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171657, N'Jake Uycoco', NULL, N'Poblacion 1, Moncada, Tarlac', N'9666510077', NULL, NULL, 1657)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171658, N'Melissa Krutsch', N'Chuchay', N'Pob. North, Ramos, Tarlac', N'9313733/09713770416', NULL, NULL, 1658)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171659, N'Javen Drew Elliott', N'Cody', N'Rang- Ayan, Paniqui, Tarlac', NULL, NULL, NULL, 1659)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171660, N'Noel Mariano', N'Dalisay', N'Samput, Paniqui, Tarlac', NULL, NULL, NULL, 1660)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171661, N'Candy Alcantara', N'Babsie', N'Samput, Paniqui, Tarlac', N'9463979088', NULL, NULL, 1661)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171662, N'Evelina Lambino', N'Beely', N'Canan, Paniqui, Tarlac', N'9669783263', NULL, NULL, 1662)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171663, N'Ivy Jane Arcega', N'Kael', N'Sitio Timbagan Patalan Paniqui Tarlac', N'9054428356', NULL, NULL, 1663)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171664, N'Mercedita Pablo Lapada', N'Shierraz', N'Paniqui, Tarlac', N'9489965878', NULL, NULL, 1664)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171665, N'Diana Rose Simpao', N'Akira', N'Tablang, Paniqui, Tarlac', N'95095597419', NULL, NULL, 1665)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171666, N'Vernalyn Dacara', N'Kassie', N'Pob. Norte, Paniqui, Tarlac', N'9197444007', NULL, NULL, 1666)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171667, N'Emmery Balbas', N'Butchog', N'San Juan Moncada Tarlac', N'9291928359', NULL, NULL, 1667)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171668, N'Karen Christy Linsao', N'Kenjie', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9293951978', NULL, NULL, 1668)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171669, N'Jovy De Jesus', NULL, N'Apulid, Paniqui, Tarlac', N'9774423255', NULL, NULL, 1669)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171670, N'Kathrina Joy Bautista', N'Kuki', N'Plastado, Gerona, Tarlac', N'9952374521', NULL, NULL, 1670)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171671, N'Melanie Nicolas', N'Brownie&Rex', N'Abogado, Paniqui, Tarlac', N'9182940409', NULL, NULL, 1671)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171672, N'Marcelina Conde', N'Popoy', N'Cabaoawasan, Paniqui, Tarlac', N'09178640182/09151300182', NULL, NULL, 1672)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171673, N'Audrey Fuentes', N'Marsha', N'Coral, Paniqui Tarlac', N'9393986539', NULL, NULL, 1673)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171674, N'Cherry Pie Delos Santos', N'Luna', N'Anao, Tarlac', N'916511979', NULL, NULL, 1674)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171675, N'Ferdinand Santiago', N'Sky', N'District 5, Cuyapo, Nueva Ecija', N'9952650593', NULL, NULL, 1675)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171676, N'Mark Castillo', N'Chico', N'Samput, Paniqui, Tarlac', N'9503454913', NULL, NULL, 1676)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171677, N'Glen Ramos', N'Joyce', N'Samput, Paniqui, Tarlac', N'9179178972', NULL, NULL, 1677)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171678, N'Cherrylyn Gaspar', N'Peach', N'Abogado, Paniqui, Tarlac', N'9469911281', NULL, NULL, 1678)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171679, N'Mark Louise Macaraeg', N'Gazini', N'Poblacion North,Ramos, Tarlac', N'9278046983', NULL, NULL, 1679)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171680, N'Grace Maranion', N'Bruno', N'Ramos, Tarlac', N'9465059833', NULL, NULL, 1680)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171681, N'Sarah Ramiscal', N'Harold', N'Toledo, Ramos Tarlac', N'9062203465', NULL, NULL, 1681)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171682, N'Willy Balaba', NULL, N'Norzagaray, Bulacan', N'9263466511', NULL, NULL, 1682)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171683, N'Kathleen Toledo', N'Covi', N'Magallanes Pob. Sur Paniqui, Tarlac', N'9054451872', NULL, NULL, 1683)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171684, N'Evitha Flores', N'Nicki', N'San Julian, Moncada, Tarlac', N'9361625669', NULL, NULL, 1684)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171685, N'Allyza Daileg', N'Narinci', N'San Julian, Moncada, Tarlac', N'9175025884', NULL, NULL, 1685)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171686, N'Ramon Bernal', N'Tyron', N'Poblacion 1, Moncada, Tarlac', N'9481212099', NULL, NULL, 1686)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171687, N'Jeff De Leon', NULL, N'Abagon, Gerona, Tarlac', N'9177916223', NULL, NULL, 1687)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171688, N'Fredalyn Abril', N'Khovee', N'Del Valle Estacion, Paniqui Tarlac', N'9182012121', NULL, NULL, 1688)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171689, N'Paula May Payumo', N'Yokai', N'Poblacion Sur, Paniqui, Tarlac', N'9107087131', NULL, NULL, 1689)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171690, N'Jay Patrick Sambilay', N'Skype', N'Sta. Ines Paniqui, Tarlac', N'9985860094', NULL, NULL, 1690)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171691, N'Maricar Ronquillo', N'Mapho', N'Rang- Ayan, Paniqui, Tarlac', N'9501541862', NULL, NULL, 1691)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171692, N'Lucky Torres ', N'Yoshi', N'Rizal, Moncada, Tarlac', N'9504463771', NULL, NULL, 1692)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171693, N'Joshua Favis', N'Red ', N'Burgos st. Paniqui, Tarlac', N'9958576081', N'joshuafavis@yahoo.com', NULL, 1693)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171694, N'Alejandro Galvadores', N'Burgo', N'Ventinilla, Paniqui, Tarlac', N'9307183568', NULL, NULL, 1694)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171695, N'Chrizelle Glynyrich Solomon', N'Ace', N'Abogado, Paniqui, Tarlac', N'9759560469', NULL, NULL, 1695)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171696, N'Khareema Puyaoan', N'Yvo', N'Abogado, Paniqui, Tarlac', N'9295737633', NULL, NULL, 1696)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171697, N'Angielee Rivera', N'Shophia', N'Ventinilla, Paniqui, Tarlac', N'9260973369', NULL, NULL, 1697)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171698, N'Arnel Ilingan', N'Bullpei', N'Matalapitap, Paniqui, Tarlac', N'9122221112', N'arnelilingan@gmail.com', NULL, 1698)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171699, N'Rosela Rombaoa', N'Yeyow', N'Carino, Paniqui, Tarlac', N'9277445143', NULL, NULL, 1699)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171700, N'Jocelyn Icoy', N'Ollie', N'Balaoang, Paniqui, Tarlac', N'(0995)8659469', NULL, NULL, 1700)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171701, N'Lito Morazo', N'Arira', N'Coral, Paniqui Tarlac', N'(0910)1798489', NULL, NULL, 1701)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171702, N'Dairo Jones De Roxas', N'Mino', N'Poblacion, Moncada, Tarlac', N'(0927)8268549', NULL, NULL, 1702)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171703, N'Christine Canoy', N'Twinkle', N'Acocolao, Paniqui, Tarlac', N'(0936)0460024', NULL, NULL, 1703)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171704, N'Glenn Hailar', N'Amber', N'Bantog, Anao, Tarlac', N'9275313820', N'hailar.glenn89@gmail.com', NULL, 1704)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171705, N'Carina Lynn Austria', N'Sugar', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9667785881', NULL, NULL, 1705)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171706, N'Glenn Tracy Ignacio', N'RG', N'Rizal, Anao Tarlac', N'9277737919', NULL, NULL, 1706)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171707, N'Irish Yacynth Noveda', N'Ashy Fall', N'Poblacion North,Ramos, Tarlac', N'9773334617', N'aerishnoveda@gmail.com', NULL, 1707)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171708, N'Richel V. Coralde', N'Mochi', N'Cabayaoasan, Paniqui, Tarlac', N'9669124578', N'lehcircoralde24@gmail.com', NULL, 1708)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171709, N'Desiree Krina Doria', N'Mona', N'Matindeg, Pura, Tarlac', N'9171124103', N'krinadoria@gmail.com', NULL, 1709)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171710, N'Sharmaine Joy Esteban', N'Healer', N'Carino, Paniqui, Tarlac', N'9669788235', N'sjae0423@icloud.com', NULL, 1710)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171711, N'Jelli Bautista', N'Katsha', N'Poblacion 2, Moncada, Tarlac ', N'9128132033', NULL, NULL, 1711)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171712, N'Anthony Marcelo Eclaera', N'Oreo', N'Estacion, Paniqui, Tarlac', N'9171600927', N'eclaeraanthony@gmail.com', NULL, 1712)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171713, N'Mary Joy Uycoco', N'Swifty', N'Poblacion 1, Moncada, Tarlac', N'9357720094', NULL, NULL, 1713)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171714, N'Abelardo Bautista', N'Bruce', N'Bulala, Cuyapo, Nueva Ecija ', N'9071964243', NULL, NULL, 1714)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171715, N'Leslie Bacarro', N'Bolt', N'Cabayaoasan, Paniqui, Tarlac', N'9455518469', NULL, NULL, 1715)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171716, N'Josefino Faustino', N'Thunder', N'Magallanes Pob. Sur Paniqui, Tarlac', N'9183128377', NULL, NULL, 1716)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171717, N'Aj Garcia', NULL, N'Mabini, Moncada, Tarlac', N'9505263010', NULL, NULL, 1717)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171718, N'Christine Joy Lacanlale', N'Thowvy', N'Salumague, Paniqui, Tarlac', N'9481736725', NULL, NULL, 1718)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171719, N'Claudin Chan', N'Skipper', N'Samput, Paniqui, Tarlac', N'9975117580', NULL, NULL, 1719)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171720, N'Jeriel Manaloto', N'Molly', N'Forbes Hall, Sampaloc, Manila', N'9661957145', NULL, NULL, 1720)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171721, N'Joselle Dela Cruz', N'Ezra', N'Villa, Moncada, Tarlac', N'9089534334', NULL, NULL, 1721)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171722, N'Clarence Alcantara', N'Bubbles', N'Atencio, Moncada, Tarlac', N'9198750019', NULL, NULL, 1722)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171723, N'Al Alquizaras', N'Joey', N'Poblacion 1, Moncada, Tarlac', N'9770845776', NULL, NULL, 1723)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171724, N'Monica Ramos', N'Pucholo', N'Acocolao, Paniqui, Tarlac', N'9053326920', NULL, NULL, 1724)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171725, N'Leo Perez', N'Pupa', N'Abogado, Paniqui, Tarlac', N'9564104136', NULL, NULL, 1725)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171726, N'Jeaneam Ballobar', N'Bruno', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9364250608', NULL, NULL, 1726)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171727, N'Roan Gale Ofrecio', N'Chut-Chut', N'Acocolao, Paniqui, Tarlac', N'9062059444', NULL, NULL, 1727)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171728, N'Sheena Babes Barrozo', N'Milo', N'Paniqui, Tarlac', N'9774045606', NULL, NULL, 1728)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171729, N'Jayvee Natividad', N'Gabby', N'Camangaan West, Moncada Tarlac', N'9125088040', NULL, NULL, 1729)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171730, N'Marc Joshua Manalo', N'Happy', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9453356641', NULL, NULL, 1730)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171731, N'Jesse Bautista', N'Russell', N'Lacayanga Subd., Paniqui, Tarlac', N'9176261656', NULL, NULL, 1731)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171732, N'Rafael Atchico', N'Mara', N'Tablang, Paniqui, Tarlac', N'9958156670', N'atchico.r@yahoo.com', NULL, 1732)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171733, N'Angelica Mae Capinpin', N'Addy', N'Purok 1, Buenavista, Pura, Tarlac', N'9175144097', N'capinpinangel@gmail.com', NULL, 1733)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171734, N'CJ Cacho', N'Maya', N'Poblacion, San Manuel, Tarlac', N'9128566234', NULL, NULL, 1734)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171735, N'Lemuel Gregorio', N'Maze', N'Nipaco, Paniqui, Tarlac', N'9167646090', NULL, NULL, 1735)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171736, N'Kenneth Isla', N'White', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'9275821558', NULL, NULL, 1736)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171737, N'Celia Espiritu', N'Lucy', N'Cabaducan, Nueva Ecija', N'9267221135', NULL, NULL, 1737)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171738, N'Avegil Gabuya', N'Molly', N'Guiteb, Ramos, Tarlac', N'9208029615', N'avegilgabuya@gmail.com', NULL, 1738)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171739, N'Luzviminda Martinez', N'Siolo', N'Carino, Paniqui, Tarlac', N'9124905629', NULL, NULL, 1739)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171740, N'Michael Baltazar', N'Mickey', N'Samput, Paniqui, Tarlac', N'9072196029', N'mfbalatazar@yahoo.com', NULL, 1740)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171741, N'Aries Agustin', N'Chiko', N'Poblacion Center, Ramos, Tarlac', N'9957646653', N'ariesahustin49@gmail.com', NULL, 1741)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171742, N'Jason Benedick Yu', N'Cat', N'Paniqui, Tarlac', N'9107440427', NULL, NULL, 1742)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171743, N'Lea Makisig', N'Lily', N'San Francisco West, Anao, Tarlac', N'9073291209', NULL, NULL, 1743)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171744, N'Mary Grace Gonzales', N'Shihtzy', N'Villa socorro, Paniqui, Tarlac', N'9164801713', NULL, NULL, 1744)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171745, N'Jay Aquino', N'Gringo', N'Matalapitap, Paniqui, Tarlac', N'916526855', NULL, NULL, 1745)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171746, N'Charisse Maniacop', N'Mallows', N'Matalapitap, Paniqui, Tarlac', N'9079900808', NULL, NULL, 1746)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171747, N'Rona Miranda', N'Skye', N'Maligaya St., Pob. Norte, Paniqui, Tarlac ', N'9669907680', NULL, NULL, 1747)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171748, N'Kristian Delantar', N'Hanzo', N'Poblacion Sur, Paniqui, Tarlac', N'9174048736', NULL, NULL, 1748)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171749, N'Jemaica Mora Mandi', N'Sevi', N'Tablang, Paniqui, Tarlac', N'9061758768', NULL, NULL, 1749)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171750, N'Jeannette Calaycay', N'Luna ', N'Poblacion 2, Moncada, Tarlac ', N'9182429332', NULL, NULL, 1750)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171751, N'Liberty Itliong', N'Rocky', N'Samput, Paniqui, Tarlac', N'9109064751', NULL, NULL, 1751)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171752, N'Maria Carla Marquez', N'Kobe', N'San Nicolas, Victoria, Tarlac', N'9321799648', NULL, NULL, 1752)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171753, N'Marissa Bautista', N'Ming Ming ', N'M.H. Del Pilar St. Paniqui, Tarlac', N'9553795403', NULL, NULL, 1753)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171754, N'Joanne Manzano', N'Freya', N'Buenlag, Gerona, Tarlac', N'9121999238', NULL, NULL, 1754)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171755, N'Leah Granadozin', N'Pepper', N'Tolega Sur, Moncada, Tarlac', N'9566503290', NULL, NULL, 1755)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171756, N'Nicole Kathleen Ibay', N'Maxwell', N'Poblacion Norte, Paniqui, Tarlac', N'9182008534', NULL, NULL, 1756)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171757, N'Jhonvy Bacani', N'Starky', N'Apulid, Paniqui, Tarlac', N'9274916249', NULL, NULL, 1757)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171758, N'E. Mananes', N'Browny', N'Dalayaoan Subd., Paniqui, Tarlac', N'9203537566', NULL, NULL, 1758)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171759, N'Emer Cadalso', N'Pangasinan', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'9124897590', NULL, NULL, 1759)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171760, N'Sepraina Carandang', N'Chipchip', N'Ventinilla, Paniqui, Tarlac', N'9327744580', NULL, NULL, 1760)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171761, N'Mary Grace Rosario', N'Max', N'Pance, Ramos, Tarlac', N'9178720912', N'marygrathysr@gmail.com', NULL, 1761)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171762, N'Olivia Martina', NULL, N'Paniqui, Tarlac', N'9169365460', NULL, NULL, 1762)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171763, N'Ronneth Furio', N'Loki', N'Tablang, Paniqui, Tarlac', N'9950730397', N'ronnethfurio@gmail.com', NULL, 1763)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171764, N'Lyann Quidangen', N'Lolo', N'Moncada, Tarlac', N'9182889713', NULL, NULL, 1764)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171765, N'Paula Gamurot', N'Jumby', N'Calamay, Moncada, Tarlac', N'9956597886', N'vasquezpaula0790@gmail.com', NULL, 1765)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171766, N'Jellie Hernandez', N'Tyler', N'Abogado, Paniqui, Tarlac', N'9107585161', NULL, NULL, 1766)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171767, N'Kristina Martin', N'Thor', N'Toledo, Ramos Tarlac', N'9278248086', N'baconloves.001@gmail.com', NULL, 1767)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171768, N'Lorenzo Apada', N'Jackson', N'Sapang, Moncada Tarlac', N'9323651080', NULL, NULL, 1768)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171769, N'Terence Ipac', N'Sharon', N'Balaoang, Paniqui, Tarlac', N'9161542655', N'sheilaipac@gmail.com', NULL, 1769)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171770, N'Gellie Quismundo', N'Roro', N'Moncada, Tarlac', N'9674288473', NULL, NULL, 1770)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171771, N'Ericka Daileg', N'Sora', N'Buenvista, Pura, Tarlac', N'9062054661', NULL, NULL, 1771)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171772, N'Fregelyn Bulatao', N'Hannah', N'Samput, Paniqui, Tarlac', N'9289655219', NULL, NULL, 1772)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171773, N'Chephren Agustin / Roanne Angeles', N'Anima', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9167354681', NULL, NULL, 1773)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171774, N'Marvin Jay Manuel', N'Belle', N'Balaoang, Paniqui, Tarlac', N'9075167954', NULL, NULL, 1774)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171775, N'Andrei Joshua Clemente', N'Bogart', N'Poblacion Norte, Paniqui, Tarlac', N'9155931550', N'ajhclemente@gmail.com', NULL, 1775)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171776, N'Byer Singian', N'Rocket', N'Poblacion South, Ramos, Tarlac', N'9565418351', NULL, NULL, 1776)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171777, N'Jhanine De Guzman', N'Luffy', N'Camangaan West, Moncada Tarlac', N'9300229625', NULL, NULL, 1777)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171778, N'Janranni Adlaon', N'Oreo', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9956481523', N'888janranni@gmail.com', NULL, 1778)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171779, N'Ella Joy Peña', N'Conan', N'Pob. Center, Ramos, Tarlac', N'9450889689', NULL, NULL, 1779)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171780, N'Mark Ian Ramos', N'Blackie', N'Nipaco, Paniqui, Tarlac', N'9266107805', N'ianramos12@yahoo.com', NULL, 1780)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171781, N'Robles Garcia', N'Tart', N'San Julian, Moncada, Tarlac', N'9667300663', NULL, NULL, 1781)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171782, N'Shaina Guiang', N'Stitch', N'Coral, Ramos, Tarlac', N'9457288049', N'shine_03@yahoo.com', NULL, 1782)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171783, N'Narcisa Tubaces', N'Toby', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9282321112', N'tubacesnarcisa@gmail.com', NULL, 1783)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171784, N'Allysa Banag', N'Kulub', N'Poblacion North, Ramos, Tarlac', N'9155028520', NULL, NULL, 1784)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171785, N'Presious Tiaba', N'Midnight', N'Abogado, Paniqui, Tarlac', N'9178311529', N'pjuu21@gmail.com', NULL, 1785)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171786, N'Jayme Mae Fernandez', N'Amber', N'Paniqui, Tarlac', N'9567837725', N'fjaymemae@yahoo.com', NULL, 1786)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171787, N'Noreen Jade Fernandez', N'Happy', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9063187128', N'noreenjade16@gmail.com', NULL, 1787)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171788, N'Althea Criziel Gabriel', N'Glacier Bless', N'Paniqui, Tarlac', N'9480448090', N'gabrielzephyr07@gmail.com', NULL, 1788)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171789, N'Boy Obrique', N'Covid', N'Lacayanga Subd., Paniqui, Tarlac', N'9393702663', NULL, NULL, 1789)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171790, N'Jodel Valencia', N'Dapple', N'Tablang, Paniqui, Tarlac', N'9654136708', NULL, NULL, 1790)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171791, N'Mary Joyce Pagala', N'Popol', N'San Julian, Moncada, Tarlac', N'9653168961', N'pagalamaryjoyce@gmail.com', NULL, 1791)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171792, N'Pierce Cajigal', N'Nimbus', N'Abogado, Paniqui, Tarlac', N'9126273213', N'pierz_13@outlook.com', NULL, 1792)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171793, N'Gemma Dela Cruz', N'Tobii', N'Anao, Tarlac', N'9304213682', NULL, NULL, 1793)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171794, N'Micaella Bagsic', N'Mholi', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'9264392003', NULL, NULL, 1794)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171795, N'Lea Andres', N'Putot', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'9193265851', NULL, NULL, 1795)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171796, N'Christopher Hilario', N'Snowpy', N'Samput, Paniqui, Tarlac', N'9307297930', N'topherrhilario@gmail.com', NULL, 1796)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171797, N'Edna Aguinaldo', N'Neo', N'Cuyapo, Nueva Ecija', N'9770180261', N'edna.esquejo.aguinaldo@gmail.com', NULL, 1797)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171798, N'Ivy Marie Lopez', N'Ada', N'Cuyapo, Nueva Ecija', N'9955522855', NULL, NULL, 1798)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171799, N'Arjhon Khier Cruz', N'Samson', N'Salumague, Paniqui, Tarlac', N'9062585514', NULL, NULL, 1799)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171800, N'Ricardo Belmonte', N'Tyler', N'Lacayanga Subd., Paniqui, Tarlac', N'9228312214', NULL, NULL, 1800)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171801, N'Christelle May Sarmiento', N'Bullet', N'Cabayaoasan, Paniqui, Tarlac', N'9958260442', NULL, NULL, 1801)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171802, N'Millet Dulatre', N'Zibby', N'Samput, Paniqui, Tarlac', N'9129449338', NULL, NULL, 1802)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171803, N'Brendalyn Delin', N'Chloe', N'Sta. Rosa, Pob. Sur, Paniqui Tarlac', N'9297897523', NULL, NULL, 1803)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171804, N'Myrna Lopez', N'Ellie', N'Salumague, Paniqui, Tarlac', N'9152955335', NULL, NULL, 1804)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171805, N'Efren Garcia', N'Kassy', N'Acocolao, Paniqui, Tarlac', N'9173175452', N'egj1616@gmail.com', NULL, 1805)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171806, N'Prisco Gonzales Jr.', N'Hachi', N'Guiteb, Ramos, Tarlac', N'mc', NULL, NULL, 1806)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171807, N'Jolly Mae Salgado', N'Sky', N'San Manuel, Tarlac', N'9952290797', NULL, NULL, 1807)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171808, N'Brian Munar', N'Jonna', N'Abogado, Paniqui, Tarlac', N'9498623892', NULL, NULL, 1808)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171809, N'Princess Ann Cabaltera', N'Achilles', N'Guiteb, Ramos, Tarlac', N'9273773536', N'princess2cabaltera@gmail.com', NULL, 1809)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171810, N'Alden Yamar', N'Eunice', N'Abogado, Paniqui, Tarlac', N'9305089262', NULL, NULL, 1810)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171811, N'Medy Abril', N'KC', N'Abogado, Paniqui, Tarlac', N'9951644062', N'abrilmedy@yahoo.com', NULL, 1811)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171812, N'Christine Joy Carbonel', N'Charlie', N'Cuyapo, Nueva Ecija', N'9055855157', NULL, NULL, 1812)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171813, N'Anne Valenzuela', N'Samson', N'Samput, Paniqui, Tarlac', N'9193785323', N'ankeithleen@gmail.com', NULL, 1813)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171814, N'Lorna Dacuma', N'Cheska', N'Samput, Paniqui, Tarlac', N'9999987839', N'dacumalorna@gmail.com', NULL, 1814)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171815, N'Anatalia Dulay', N'Black', N'Balza, Paniqui, Tarlac', N'9124571710', NULL, NULL, 1815)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171816, N'Pamela Viernes', N'Cracker', N'Rang- Ayan, Paniqui, Tarlac', N'9275492455', N'pmlviernes@gmail.com', NULL, 1816)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171817, N'Emma Flores', N'Bruno', N'San Julian, Moncada, Tarlac', N'9275852488', NULL, NULL, 1817)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171818, N'Agapito Seril Jr.', N'Angela', N'Capaoayan Compound, Moncada, Tarlac', N'9107096636', NULL, NULL, 1818)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171819, N'Diomedes Fernandez Jr. ', N'Pippin', N'San Leon, Moncada, Tarlac', N'9991098326', NULL, NULL, 1819)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171820, N'Sheila Ipac', N'Logan', N'Balaoang, Paniqui, Tarlac', N'9161542655', NULL, NULL, 1820)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171821, N'Grosele Paule', N'COco', N'San Francisco West, Anao, Tarlac', N'9094593022', N'grosellepaule@gmail.com', NULL, 1821)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171822, N'Aiko Dy', N'Sky', N'Samput, Paniqui, Tarlac', N'9770120068', NULL, NULL, 1822)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171823, N'Mark Abner Anijo', N'Bella', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9176184078', NULL, NULL, 1823)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171824, N'Voltaire Roxas', N'Pippa', N'Baltazar Subd., Paniqui, Tarlac ', N'9179251833', N'voltsroxas@gmail.com', NULL, 1824)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171825, N'Ruby Jane / Joey Naval', N'Zabrina ', N'Mabini, Moncada, Tarlac', N'9328795240', N'rubyjane.naval@gmail.com', NULL, 1825)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171826, N'Rizalina Yebes', N'Coffee', N'Manaois, Paniqui, Tarlac', N'9772904739', NULL, NULL, 1826)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171827, N'Michael / Dairen Perez', N'Coco', N'Abogado, Paniqui, Tarlac', N'9386450165', NULL, NULL, 1827)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171828, N'Ian Duro', N'Potchie', N'Poblacion, Anao, Tarlac', N'9175174260', N'ianduro23@gmail.com', NULL, 1828)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171829, N'Domingo Galvadores', N'Max', N'Burgos , Moncada, Tarlac', N'9302093581', NULL, NULL, 1829)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171830, N'Arnelio Hernandez Jr.', N'Aisha', N'Poblacion Norte, Paniqui, Tarlac', N'9771916022', N'vpi.alhernandez@gmail.com', NULL, 1830)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171831, N'Ana Karenina Perez', N'Taylor', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9175117621', NULL, NULL, 1831)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171832, N'Jinky Carpio', N'Chico', N'Apulid, Paniqui, Tarlac', N'9123190431', N'jinkycarpio1208@gmail.com', NULL, 1832)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171833, N'Rodean Janus Raña', N'Alpha', N'Baquero Norte, Moncada, Tarlac', N'9296196758', N'rodeanjanus10@gmail.com', NULL, 1833)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171834, N'Cyla Ilingan', N'Milo', N'Apulid, Paniqui, Tarlac', N'9155162805', NULL, NULL, 1834)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171835, N'Ian Geneses Quilet', NULL, N'Poblacion 4, Moncada, Tarlac', N'9166012704', NULL, NULL, 1835)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171836, N'Victor Aquino', N'Toby', N'Abogado, Paniqui, Tarlac', N'9672373147', NULL, NULL, 1836)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171837, N'Gilbert Calumpiano', N'Cahaya', N'Sta. Lucia, Gerona, Tarlac', N'9164309099', NULL, NULL, 1837)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171838, N'Juancho Perez', N'Wuhan', N'Ramos, Tarlac', N'9774697144', NULL, NULL, 1838)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171839, N'Raiza Cassandra Lee', N'Kitkat', N'Samput, Paniqui, Tarlac', N'9478609208', NULL, NULL, 1839)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171840, N'Kimberly Keith Cruz', N'Ley Chang ', N'Poblacion Sur, Paniqui, Tarlac', N'9567869455', NULL, NULL, 1840)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171841, N'Noime Rosario', N'JC Boy', N'Pance, Ramos, Tarlac', N'9129982344', NULL, NULL, 1841)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171842, N'May Elena Paringit', N'Kittens', N'Salumague, Paniqui, Tarlac', N'9159030522', NULL, NULL, 1842)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171843, N'Jeniffer Conde', N'Pinky', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'9388611010', NULL, NULL, 1843)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171844, N'Harlene Burbano', N'Cookies', N'Poblacion South, Ramos, Tarlac', N'9774600272', NULL, NULL, 1844)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171845, N'Nica Lacambra', N'Lexie', N'Abogado, Paniqui, Tarlac', N'9773284599', NULL, NULL, 1845)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171846, N'Kenneth Guillermo / Michaella Serrano', N'Kaiden', N'Balaoang, Paniqui, Tarlac', N'9060593511', N'guillermokenneth05@yahoo.com', NULL, 1846)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171847, N'Melani Machado', N'Rid', N'Manaois, Paniqui, Tarlac', N'9206978241', NULL, NULL, 1847)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171848, N'Keziah Jemimah Santomin', N'Luna ', N'Dalayaoan Subd., Paniqui, Tarlac', N'9175037760', N'kjemimah.santomin@gmail.com', NULL, 1848)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171849, N'Nielsen Techico', N'Milk', N'M.H. Del Pilar St. Paniqui, Tarlac', N'9672817194', N'nieltechico00@gmail.com', NULL, 1849)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171850, N'Juliet Adams', N'Toffee', N'Tablang, Paniqui, Tarlac', N'9294256455', NULL, NULL, 1850)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171851, N'Clarisse Maniacop', N'Prince', N'Matalapitap, Paniqui, Tarlac', N'9386441394', NULL, NULL, 1851)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171852, N'Kharel Paragas', N'Macey', N'Sta. Maria, Moncada, Tarlac', N'9666408741', NULL, NULL, 1852)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171853, N'Dennis Roxas', N'Melo', N'Salumague, Paniqui, Tarlac', N'9174294015', NULL, NULL, 1853)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171854, N'Kristine Billedo', N'Jacky', N'Anao, Tarlac', N'9566121005', N'archerqueentin@gmail.com', NULL, 1854)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171855, N'Marla Estanislao', N'Cinnamon', N'Zamora St. Paniqui, Tarlac', N'9270622411', NULL, NULL, 1855)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171856, N'Mary Rose Mandapat', N'Two', N'Burgos, Moncada, Tarlac', NULL, NULL, NULL, 1856)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171857, N'Reynaldo Lambino', N'Hanzel', N'Poblacion North, Ramos, Tarlac', N'9460249032', NULL, NULL, 1857)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171858, N'Florinda Villanos', N'Ellie', N'Poblacion 1, Moncada, Tarlac', N'9270364328', NULL, NULL, 1858)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171859, N'Maureen Cuchapin', N'Wine ', N'Acocolao, Paniqui, Tarlac', N'9369422118', N'maureencuchapin@yahoo.com', NULL, 1859)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171860, N'Arcelia Galingan', N'Khing', N'Palarca St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9663402321', NULL, NULL, 1860)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171861, N'Gelyn Cesario', N'Coco', N'Ventinilla, Paniqui, Tarlac', N'9196422173', NULL, NULL, 1861)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171862, N'Rowena Campos', N'Hiro', N'Poblacion, San Manuel, Tarlac', N'9192219768', NULL, NULL, 1862)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171863, N'Jeiel Obillo', N'Peppert', N'San Manuel, Tarlac', N'9284212548', NULL, NULL, 1863)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171864, N'Dhienalyn Robles', N'Milky', N'San Miguel, San Manuel Tarlac', N'9238688201', NULL, NULL, 1864)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171865, N'Jennifer Paragas', N'Bailey', N'Lacayanga Subd., Paniqui, Tarlac', N'9178942750', NULL, NULL, 1865)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171866, N'Marvic Prado', N'Hachi', N'Rang- Ayan, Paniqui, Tarlac', N'9103596876', NULL, NULL, 1866)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171867, N'Randy Garcia', N'Maly', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'9171106447', NULL, NULL, 1867)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171868, N'Vic Soriano', N'Uno', N'Coral, Ramos, Tarlac', N'9278796262', NULL, NULL, 1868)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171869, N'Raymund Riparip', N'Kobe', N'San Rafael, Tarlac', N'9064325205', NULL, NULL, 1869)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171870, N'Jeron Rafael Magbalot', N'HAppy', N'Sta. Maria, Moncada, Tarlac', N'9494198800', NULL, NULL, 1870)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171871, N'Maria Cleofe Garcia', N'Jillian', N'Rizal, Anao Tarlac', N'9564717687', NULL, NULL, 1871)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171872, N'Kim Michael Reyes', N'Pupu', N'Nampicuan, Nueva Ecija', N'9057001580', NULL, NULL, 1872)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171873, N'Maribel Tung', N'Koko', N'West Poblacion, Nampicuan, Nueva Ecija', N'9175351098', NULL, NULL, 1873)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171874, N'Aira Cheena Fernandez', N'Motchi', N'Rang- Ayan, Paniqui, Tarlac', N'9569476973', NULL, NULL, 1874)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171875, N'Maricel Barcala / Bernard Mendoza', N'Mikkel', N'Poblacion 1, Gerona, Tarlac', N'9338545981', NULL, NULL, 1875)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171876, N'Bhong Madamba', N'Pepper', N'Carino, Paniqui, Tarlac', N'9291500520', NULL, NULL, 1876)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171877, N'Jun Gonzales', N'Gypsy', N'Poblacion South, Ramos, Tarlac', N'9669928953', NULL, NULL, 1877)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171878, N'Mark Anthony Duran', N'Athena', N'Pance, Ramos, Tarlac', N'9505091121', NULL, NULL, 1878)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171879, N'Janelle Rosales', N'Chowie', N'Tablang, Paniqui, Tarlac', N'9669122722', N'janellerosalesgallardo@gmail.com', NULL, 1879)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171880, N'Charmie Reign Dela Cruz', N'Snow', N'Burgos, Moncada, Tarlac', N'9187857881', NULL, NULL, 1880)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171881, N'Dianna Soriano', N'Parker', N'Gomez St., Poblacion Sur, Paniqui, Tarlac', N'9152790092', NULL, NULL, 1881)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171882, N'Ina Nool', N'Pipay', N'Plastado, Gerona, Tarlac', N'9199750393', NULL, NULL, 1882)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171883, N'Donna GB Cadalina', N'Bella', N'San Julian, Moncada, Tarlac', N'9454718799', NULL, NULL, 1883)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171884, N'Jealeen Potente', N'Claudio', N'Burgos, Moncada, Tarlac', N'9484995421', NULL, NULL, 1884)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171885, N'Gina Obena ', N'Snooky', N'Guiteb, Ramos, Tarlac', N'9367851448', NULL, NULL, 1885)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171886, N'Mary Grace Mariñas', N'Blu', N'Nampicuan, Nueva Ecija', N'9107128415', NULL, NULL, 1886)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171887, N'Anne Pacheco', N'Fiona', N'Maligaya St. Abogado, Paniqui Tarlac', N'9458997035/9177017886', NULL, NULL, 1887)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171888, N'Aiza Flores', N'Ginger', N'Balza, Paniqui, Tarlac', N'9507818335/9507818335', NULL, NULL, 1888)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171889, N'Benjamin Bulatao', N'Storm', N'Baltazar Subd., Paniqui, Tarlac ', N'931-0119', NULL, NULL, 1889)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171890, N'Mary May Breganio', N'Shine ', N'Moncada, Tarlac', N'9163126758', NULL, NULL, 1890)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171891, N'Jyrah Baniqued', N'MOchi', N'Samput, Paniqui, Tarlac', N'9264604038', NULL, NULL, 1891)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171892, N'Bart Angelo Jonatas', N'Max', N'Coral, Ramos, Tarlac', N'9650938690', NULL, NULL, 1892)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171893, N'Angelito Linsao', N'Shailohh', N'Poblacion Sur, Paniqui, Tarlac', N'9167546048', NULL, NULL, 1893)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171894, N'Jhulie Ann Patio', N'Chippy', N'Canan, Paniqui, Tarlac', N'9164220920', NULL, NULL, 1894)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171895, N'Arceli Carrera', N'Aly', N'San Julian, Moncada, Tarlac', N'9196661439', NULL, NULL, 1895)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171896, N'Teresita Bulosan', N'A Thor', N'Nipaco, Paniqui, Tarlac', N'9177384261', NULL, NULL, 1896)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171897, N'Frances Borje', N'Pepper', N'Anao, Tarlac', N'9772400410', NULL, NULL, 1897)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171898, N'Danmark Gajes', N'Oreo', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'9190000096', NULL, NULL, 1898)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171899, N'Bryan Keith Taccad', N'Rufo', N'Maligaya, Taumuni, Isabela', N'9351563539', NULL, NULL, 1899)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171900, N'Jamee Loida Ramos', N'Hakie', N'Maybubon, Guimba, Nueva Ecija ', N'9383352864', NULL, NULL, 1900)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171901, N'Dina Dela Cruz', N'Shimi Shimi ', N'Moncada, Tarlac', N'9159720734', NULL, NULL, 1901)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171902, N'Arianna Marie Chichioco', N'Achilles', N'Matalapitap, Paniqui, Tarlac', N'9163138852', NULL, NULL, 1902)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171903, N'Marievic Jimenez', N'Tacia', N'Samput, Paniqui, Tarlac', N'9973578580', NULL, NULL, 1903)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171904, N'Shaina Rimando', N'Chewy', N'Sta. Maria, Moncada, Tarlac', NULL, NULL, NULL, 1904)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171905, N'Maria Leonores', N'Harley', N'Poblacion 2, Moncada, Tarlac ', N'9610278187', NULL, NULL, 1905)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171906, N'Marifel Barrozo', N'Elmo', N'Burgos st. Paniqui, Tarlac', N'9991560459', NULL, NULL, 1906)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171907, N'Mauie Chichioco', N'Baecon', N'Matalapitap, Paniqui, Tarlac', N'9065278266', NULL, NULL, 1907)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171908, N'Pauline Gonzales', N'Jazzmin', N'Villa socorro, Paniqui, Tarlac', N'9227873472', NULL, NULL, 1908)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171909, N'Allen Oween Caberto', N'Pugsly / Torn', N'Coral, Ramos, Tarlac', N'9179184309', NULL, NULL, 1909)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171910, N'Penelope Flores', N'Biboy', N'San Julian, Moncada, Tarlac', N'9078272142', NULL, NULL, 1910)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171911, N'Dorothy Joy Breganio', N'Jimmy', N'Camangaan West, Moncada Tarlac', N'9179992068', NULL, NULL, 1911)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171912, N'Ana Mae Benitez', N'Kobe ', N'Nipaco, Paniqui, Tarlac', N'9284155431', N'b.anamae@ymail.com', NULL, 1912)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171913, N'Roderick Oril', N'Binggo / Missy', N'Luna, Cuyapo, Nueva Ecija', N'9953611024', NULL, NULL, 1913)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171914, N'Jerome Gil', N'Tormund Giantsbane', N'Capaoayan Compound, Moncada, Tarlac', N'9296298373', N'ziehzieh13@yahoo.com', NULL, 1914)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171915, N'Brian Angel Fuertes', N'Oreo', N'Quezon St., Brgy. Tablang,Paniqui, Tarlac', N'9054488368', N'brian_fuertes06@yahoo.com', NULL, 1915)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171916, N'Lovely Rose Lagman', N'Luna', N'Patalan, Paniqui, Tarlac', N'9995068713', N'lovelylagman90@gmail.com', NULL, 1916)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171917, N'Marson Dela Torre', N'Turbo', N'Salumague, Paniqui, Tarlac', N'9065496209', NULL, NULL, 1917)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171918, N'Julieta Bautista', N'Mickey', N'Poblacion 2, Moncada, Tarlac ', N'9771201351', NULL, NULL, 1918)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171919, N'Jefren Aguilar', N'Brusco', N'San Miguel, San Manuel Tarlac', N'9171193135', N'jefren.aguilar@gmail.com', NULL, 1919)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171920, N'Glenn Allen Encomienda', N'Jisoo', N'San Juan, Moncada, Tarlac', N'9190060068', N'glenn.encomienda@outlook.com', NULL, 1920)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171921, N'Essell Joyce Ganuelas', N'Calvin', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'9186701499', N'2113546essell@gmail.com', NULL, 1921)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171922, N'Bryant Ayuyao', NULL, N'Poblacion 1, Gerona, Tarlac', N'9125641130', NULL, NULL, 1922)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171923, N'Greg Obcena', N'Ion', N'Cabayaoasan, Paniqui, Tarlac', N'9455109199', N'georgegreg1988@gmail.com', NULL, 1923)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171924, N'Arianne Zorilla', N'Tokyo', N'Apulid, Paniqui, Tarlac', N'9955197944', NULL, NULL, 1924)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171925, N'Natalia Tagavilla', N'Desha', N'Poblacion, Anao, Tarlac', N'9073892922', NULL, NULL, 1925)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171926, N'Psalm David Absolor', N'Oreo', N'Mabilang, Paniqui, Tarlac', N'9993116910', NULL, NULL, 1926)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171927, N'Jarrel May Quitaleg ', N'Chelsea', N'Burgos, Moncada, Tarlac', N'9054445056', NULL, NULL, 1927)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171928, N'Larry Paez', NULL, N'Burgos st. Paniqui, Tarlac', NULL, NULL, NULL, 1928)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171929, N'Allan Dandasan / Lyan Francisco', N'Hiraya', N'Colibangbang, Paniqui, Tarlac', N'9163141352', NULL, NULL, 1929)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171930, N'Mara Soliman ', N'Coco', N'Abogado, Paniqui, Tarlac', N'9459907958', N'maraeysoliman.1998@gmail.com', NULL, 1930)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171931, N'Grace Gomez', N'Jaja', N'Lamorena Subd. Ramos Tarlac', N'9300241816', NULL, NULL, 1931)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171932, N'Ma. Socorro Capinpin', N'Kiki / Nini', N'Pura, Tarlac', N'9550835405', N'mhamc.28@yahoo.com', NULL, 1932)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171933, N'Jeanne Estevon', N'Margaux / Ganda', N'Magaspac, Gerona, Tarlac', N'9171561624', NULL, NULL, 1933)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171934, N'Rochelle Francisco', N'Choco', N'Camiling Tarlac', N'9155237735', N'rdfrancisco@one-bosco.org', NULL, 1934)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171935, N'Micaela De Guzman', N'Mikoy', N'Cabayaoasan, Paniqui, Tarlac', N'9615565795', N'michaella1229@icloud.com', NULL, 1935)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171936, N'Jobeel Diaz', N'Maxx', N'North Poblacion, Nampicuan Nueva Ecija', N'9979443548', NULL, NULL, 1936)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171937, N'Clarence Jonathan Santiago', N'Angela ', N'Poblacion Norte, Paniqui, Tarlac', N'9175660705', N'noobguy15gt@gmail.com', NULL, 1937)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171938, N'Jefferson Domingo', N'Sam', N'San Francisco West, Anao, Tarlac', N'9063140502', NULL, NULL, 1938)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171939, N'Marites Cajan', N'Shadow', N'Coral, Paniqui Tarlac', N'9199239614', NULL, NULL, 1939)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171940, N'Alexandra Garin ', N'King', N'Estacion, Paniqui, Tarlac', N'9655602820', NULL, NULL, 1940)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171941, N'Christian Honera / Gilbert Isla', N'Gillian / Gian', N'Amacalan, Gerona, Tarlac', N'9392744078', NULL, NULL, 1941)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171942, N'John Erwin Pacheco', N'Luka', N'Estacion, Paniqui, Tarlac', N'9669787623', NULL, NULL, 1942)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171943, N'Joan Melody Valenton', N'Pepper', N'Estacion, Paniqui, Tarlac', N'9168856828', NULL, NULL, 1943)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171944, N'Disadhel Bueno', N'Maxzin', N'Apulid, Paniqui, Tarlac', N'9197416361', NULL, NULL, 1944)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171945, N'Ariel Villanueva', N'Kuro', N'Buenlag, Gerona, Tarlac', N'9497213322', NULL, NULL, 1945)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171946, N'Krizzia Garcia', NULL, N'Apulid, Paniqui, Tarlac', NULL, NULL, NULL, 1946)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171947, N'Julius Geoffrey Tangonan', N'Hero', N'Carino, Paniqui, Tarlac', N'9173842729', NULL, NULL, 1947)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171948, N'Lourdes Salazar', N'Jaguar', N'Tablang, Paniqui, Tarlac', N'9454675241', NULL, NULL, 1948)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171949, N'CJ Oquendo', N'Toby', N'San Juan, Ramos, Tarlac', N'9109104065', NULL, NULL, 1949)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171950, N'Denny Sabado', N'Oreo', N'Buenlag, Gerona, Tarlac', N'9166561803', NULL, NULL, 1950)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171951, N'Ma. Theresa Calipjo', N'Coco', N'Burgos, Moncada, Tarlac', N'9154591328', NULL, NULL, 1951)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171952, N'Marie Camille Villanueva', NULL, N'Abogado, Paniqui, Tarlac', N'9155854820', NULL, NULL, 1952)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171953, N'Kathleen May Ramilo', N'Covie', N'Estacion, Paniqui, Tarlac', N'9759558179', NULL, NULL, 1953)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171954, N'Mikki Marquez', N'Tokyo', N'West Poblacion, Nampicuan, Nueva Ecija', N'9167661415', NULL, NULL, 1954)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171955, N'Xyrene Escandor', N'Sofie', N'San Felipe, San Manuel , Tarlac  ', N'9457885658', NULL, NULL, 1955)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171956, N'Joy Mendoza', N'Wisky', N'San Juan, Moncada, Tarlac', N'9162534969', NULL, NULL, 1956)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171957, N'Maria Teresa Quibuyen', N'Spyke', N'San Julian, Moncada, Tarlac', N'9076308434', NULL, NULL, 1957)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171958, N'Ma. Avelene Rillo', N'Mochi', N'Matalapitap, Paniqui, Tarlac', N'9957680900', NULL, NULL, 1958)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171959, N'Antonette Gammaru', N'Rio/Mochi', N'Carmen, Anao, Tarlac', N'9056302633', NULL, NULL, 1959)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171960, N'Mylene Ragual', N'MidKnight ', N'San Jose South, Anao, Tarlac', N'9058654761', NULL, NULL, 1960)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171961, N'TinTin Paz', N'Noah', N'Maluac, Moncada, Tarlac', N'9495891664', NULL, NULL, 1961)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171962, N'Jennylyn Briones', N'Maxi', N'Apulid, Paniqui, Tarlac', N'9174684871', NULL, NULL, 1962)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171963, N'Camille Calubana', N'Jolly', N'Camiling Tarlac', N'9297942504', NULL, NULL, 1963)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171964, N'Ma. Pauline Carreon', N'Bella', N'Salomague, Paniqui, Tarlac', N'9568063997', NULL, NULL, 1964)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171965, N'Edna Villanos', N'Blitz', N'Mabini, Moncada, Tarlac', N'9058185260', NULL, NULL, 1965)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171966, N'Carol Abril', N'Bruno', N'Guiteb, Ramos, Tarlac', N'9356090168', NULL, NULL, 1966)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171967, N'Christian Directo', N'Ayena', N'San Vicente, San Manuel, Tarlac', N'9485996287', NULL, NULL, 1967)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171968, N'Charlotte Pagaduan', N'Spot', N'Samput, Paniqui, Tarlac', N'9469908383', NULL, NULL, 1968)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171969, N'Jan Pasion', N'Kobe', N'Matalapitap, Paniqui, Tarlac', NULL, NULL, NULL, 1969)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171970, N'Julius Benavides', N'Mak-Mak', N'Sto. Domingo, Anao, Tarlac', N'9469351047', NULL, NULL, 1970)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171971, N'Robert Abliter', N'Zoe', N'Burgos, Paniqui, Tarlac', N'9456657428', NULL, NULL, 1971)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171972, N'Zyrille Art Rombaoa', N'Jon Snow', N'Poblacion Norte, Paniqui, Tarlac', N'9153006153', NULL, NULL, 1972)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171973, N'Suena Del Rosario', N'Potpotsie', N'Pob. Norte, Paniqui, Tarlac', N'9394461801', NULL, NULL, 1973)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171974, N'Mary Grace Fontanilla', N'Willow', N'Sulipa, Gerona, Tarlac', N'9455749740', NULL, NULL, 1974)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171975, N'Ellenor Valdez', N'Muffin', N'Salumague, Paniqui, Tarlac', N'9204983413', NULL, NULL, 1975)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171976, N'Jennelyn Magaoay', N'Joaquin', N'Cuyapo, Nueva Ecija', N'9274258694', NULL, NULL, 1976)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171977, N'Jean Villanueva', N'Patchicoy', N'Ramos, Tarlac', N'9128624573', NULL, NULL, 1977)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171978, N'Ren Ren Basco', NULL, N'Coral, Paniqui Tarlac', N'9266871927', NULL, NULL, 1978)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171979, N'Rowena Rubilla', N'Sophie', N'Anao, Tarlac', NULL, NULL, NULL, 1979)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171980, N'Kar Angelo Nacpil', N'Pharsa', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', NULL, NULL, NULL, 1980)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171981, N'Van Angelo Calalo', N'Akira Luna Randall', N'Poblacion Sur, Paniqui, Tarlac', N'9386033453', NULL, NULL, 1981)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171982, N'Sherilyn Beltran', N'Chichay', N'Burgos, Moncada, Tarlac', N'9155593207', NULL, NULL, 1982)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171983, N'Rhee Arciaga', N'Hershey', N'Poblacion 3, Gerona, Tarlac', N'9152611785', NULL, NULL, 1983)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171984, N'Maria Juvielyn Idio', N'Peter', N'Poblacion Norte, Paniqui, Tarlac', N'9167865912', NULL, NULL, 1984)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171985, N'Mark Anthony Andino', N'Taylor', N'Poblacion North, Ramos, Tarlac', N'9081422803', NULL, NULL, 1985)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171986, N'Leslie Anne Pascual', N'Mocha', N'Baltazar Subd. Poblacion Norte, Paniqui, Tarlac', N'9774263899', NULL, NULL, 1986)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171987, N'Collyne Andrea Pariñas', N'Hikoa', N'Paniqui, Tarlac', N'9455416140', NULL, NULL, 1987)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171988, N'Jan Nathaniel Lagasca', N'Hope', N'Gerona, Tarlac', N'9359288032', NULL, NULL, 1988)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171989, N'Emily Orcino', N'Axelle', N'Ventinilla, Paniqui, Tarlac', N'9076320810', NULL, NULL, 1989)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171990, N'Lucky Bagsic', NULL, N'Victoria, Tarlac', N'9175141327', NULL, NULL, 1990)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171991, N'Maureene Joyce Legaspi', N'Winter/Snow/Amber', N'Sta. Lucia, Gerona, Tarlac', N'9773282716', NULL, NULL, 1991)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171992, N'Mawie Melchor ', N'Nipper', N'Poblacion Norte, Paniqui, Tarlac', N'9959187218', NULL, NULL, 1992)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171993, N'Areli Garcia', N'George', N'Rizal, Moncada, Tarlac', N'9307374379', NULL, NULL, 1993)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171994, N'Dominga Baba', N'Jery / Carla', N'Balite, Pura, Tarlac', N'9105209055', NULL, NULL, 1994)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171995, N'Mary Abigail Modales', N'Felize', N'Paniqui, Tarlac', N'9360415864', NULL, NULL, 1995)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171996, N'Jennyvi Gamundoy ', N'Bella ', N'Mabini, Moncada, Tarlac', N'9612876060', NULL, NULL, 1996)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171997, N'Noriefe Yasay', N'Sam', N'Atencio, Moncada, Tarlac', N'9097822254', NULL, NULL, 1997)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171998, N'Sharon Poco', N'Keena', N'Coral, Ramos, Tarlac', N'9216521801', NULL, NULL, 1998)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20171999, N'Tiffany Elaine Arellano', N'Sandy', N'Cabayaoasan, Paniqui, Tarlac', N'9162879204', NULL, NULL, 1999)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172000, N'Diana Rose Cabacungan', N'Kikay', N'Patalan, Paniqui, Tarlac', N'9304188153', NULL, NULL, 2000)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172001, N'Pio Sebastian Chichioco', N'Snow', N'Matalapitap, Paniqui, Tarlac', N'9155560240', NULL, NULL, 2001)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172002, N'Cheryl Esteban ', N'Tabachingching', N'Sta.Rosa Street, Paniqui, Tarlac', N'9395056212', NULL, NULL, 2002)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172003, N'Gary Hilario', N'Digong', N'Coral, Paniqui Tarlac', N'9183351830', NULL, NULL, 2003)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172004, N'Jena Rose Eberio', N'Buffy', N'Samput, Paniqui, Tarlac', N'9279798869', NULL, NULL, 2004)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172005, N'Bernard  Calderas', N'Summer', N'Cabayaoasan, Paniqui, Tarlac', N'9988672747', NULL, NULL, 2005)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172006, N'Rheavette Dagdagan', N'Stella', N'Rizal, Moncada, Tarlac', N'9071253156', NULL, NULL, 2006)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172007, N'Cherrylane Obcena', N'Zaxhie', N'Coral, Paniqui, Tarlac', N'9090550418', NULL, NULL, 2007)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172008, N'Jamie Contreras', N'Primo', N'Matalapitap, Paniqui, Tarlac', N'9472697155', NULL, NULL, 2008)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172009, N'Jenn Clarin', N'Jacob ', N'Pob. Norte, Paniqui, Tarlac', N'9305643763', NULL, NULL, 2009)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172010, N'Marica Chiara Velasquez', N'Drei', N'Carino, Paniqui, Tarlac', N'9998996722', NULL, NULL, 2010)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172011, N'Kimberly Baluyut', N'Inah', N'Samput, Paniqui, Tarlac', N'9089727906', NULL, NULL, 2011)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172012, N'Loren Amistozo', N'Athena', N'San Leon, Moncada, Tarlac', N'9213031909', NULL, NULL, 2012)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172013, N'Isidora Lopez', N'Sucheol', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9099723738', NULL, NULL, 2013)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172014, N'Angela Abraham', N'Chuchi', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9457658314', NULL, NULL, 2014)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172015, N'Arly Irish Gozum', N'Nala', N'Paniqui, Tarlac', N'9171873575', NULL, NULL, 2015)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172016, N'Sam Ladrero', N'Tyler', N'San Leon, Moncada, Tarlac', N'9176551501', NULL, NULL, 2016)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172017, N'Maricho Rimando', N'Panena', N'Moncada, Tarlac', N'9453356386', NULL, NULL, 2017)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172018, N'Narciso Baquisal', N'Thor', N'San Francisco West, Anao, Tarlac', N'9396063798', NULL, NULL, 2018)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172019, N'Lea Mabanglo', N'Ping Pong', N'Samput, Paniqui, Tarlac', N'9478481236', NULL, NULL, 2019)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172020, N'Imelda Puyaoan', N'Milo / Lucky', N'Abogado, Paniqui, Tarlac', N'9564334715', NULL, NULL, 2020)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172021, N'Marivic Caseria', N'Zayah', N'Sta. Ines, Paniqui, Tarlac', N'9062229959', NULL, NULL, 2021)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172022, N'Luke Ivan Nogaliza', N'Sky', N'Rizal, Moncada, Tarlac', N'9286881220', NULL, NULL, 2022)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172023, N'Nelma Mendoza', N'Maria', N'San Roque, Moncada, Tarlac', N'9072218131', NULL, NULL, 2023)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172024, N'Maria Isabel Gabriel', N'Aiko', N'Maluac, Moncada, Tarlac', N'9185349868', NULL, NULL, 2024)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172025, N'Dea Rose Gabriel ', N'Coffee', N'Pob. South, Ramos, Tarlac', N'9654898571', NULL, NULL, 2025)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172026, N'Carlo Roi Jaycent Cusi', N'Primo', N'Abogado, Paniqui, Tarlac', N'9162705695', NULL, NULL, 2026)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172027, N'Napoleon Pamintuan', N'Kiko', N'Poblacion 4, Moncada, Tarlac', N'9393904640', NULL, NULL, 2027)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172028, N'Jhon Nuguid ', N'Bella', N'Abogado, Paniqui, Tarlac', N'9331408834', NULL, NULL, 2028)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172029, N'Charito Reguindin', N'Toby', N'Poblacion Norte, Paniqui, Tarlac', N'9988527467', NULL, NULL, 2029)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172030, N'Analiza Burgos', N'Saber / Summer', N'Cuyapo, Nueva Ecija', N'9165017688', NULL, NULL, 2030)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172031, N'April Jem Punzalan', NULL, N'Apulid, Paniqui, Tarlac', N'9662663245', NULL, NULL, 2031)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172032, N'Phryncess Mhary Cacal', N'Clo-e', N'Balaoang, Paniqui, Tarlac', N'9469970657', NULL, NULL, 2032)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172033, N'Romnick Viernes', N'Simba ', N'Baquero Sur, Moncada, Tarlac', N'9068789726', NULL, NULL, 2033)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172034, N'Bea Nichole Bagamaspad ', N'Mochi', N'San Pedro, Moncada, Tarlac', N'9669925175', NULL, NULL, 2034)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172035, N'Eduardo Isidro', N'Cardo', N'Guiteb, Ramos, Tarlac', N'9669905312', NULL, NULL, 2035)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172036, N'Cheska Lacson / Hubert Mamucod', N'Peanut ', N'Burgos st. Paniqui, Tarlac', N'9471042611', NULL, NULL, 2036)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172037, N'Shara Mae Tejada', N'Elly ', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9669927319', NULL, NULL, 2037)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172038, N'Carlo De Leon', N'Wacky', N'Cuyapo, Nueva Ecija', N'9367587801', NULL, NULL, 2038)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172039, N'John Raphael Sanchez', N'Chico', N'Poblacion Sur, Paniqui, Tarlac', N'9959747275', NULL, NULL, 2039)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172040, N'Annabelle Takahashi', N'Maximus', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'9666453240', NULL, NULL, 2040)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172041, N'Jeff Vincent Diemsen', N'Yohan', N'Samput, Paniqui, Tarlac', N'9065251609', NULL, NULL, 2041)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172042, N'Aries Gammaru', N'Kimchi', N'Carmen, Anao, Tarlac', N'9773276640', NULL, NULL, 2042)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172043, N'Jose Libatique', N'Oreo', N'Abogado, Paniqui, Tarlac', N'9193143883', NULL, NULL, 2043)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172044, N'Angelica Bonifacio', N'Chamlee', N'Abogado, Paniqui, Tarlac', N'9065001234', NULL, NULL, 2044)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172045, N'Kristina Bernadette Yambao', N'Chopper', N'Tablang, Paniqui, Tarlac', N'9669330690', NULL, NULL, 2045)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172046, N'Gerard Quizon', N'Nima', N'Coral, Paniqui, Tarlac', N'9195945141', NULL, NULL, 2046)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172047, N'Domingo Laña ', N'Coco', N'Tolega Sur, Moncada, Tarlac', N'9568765328', NULL, NULL, 2047)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172048, N'Roderick Nera', N'Akira', N'Ventinilla, Paniqui, Tarlac', N'9091827566', NULL, NULL, 2048)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172049, N'Christian Tulabot', N'Tiger', N'Lacayanga Subd., Paniqui, Tarlac', N'9395125640', NULL, NULL, 2049)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172050, N'Julie Ann Tacdol', N'Miley', N'Rizal, Moncada, Tarlac', N'9395128295', NULL, NULL, 2050)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172051, N'Jimmy Villanueva', N'Chay-Chay', N'Patalan, Paniqui, Tarlac', N'9129528701', NULL, NULL, 2051)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172052, N'Esther Grace Samson', N'Bogart', N'Abogado, Paniqui, Tarlac', N'9127146866', NULL, NULL, 2052)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172053, N'Hasmin Melegrito', N'Mayson', N'Dalayaoan Subd., Paniqui, Tarlac', N'9129312779', NULL, NULL, 2053)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172054, N'Aurora Mañalac', N'Spot', N'Ramos, Tarlac', N'9309561470', NULL, NULL, 2054)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172055, N'Ismael Formoso', N'Bela', N'Camposanto 2, Moncada, Tarlac', N'9267031561', NULL, NULL, 2055)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172056, N'Judge Daril Palarca', N'Shiro', N'Carino, Paniqui, Tarlac', N'9566692461', NULL, NULL, 2056)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172057, N'Stephanie Radovan', N'Francesca', N'Samput, Paniqui, Tarlac', N'9983727285', NULL, NULL, 2057)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172058, N'Madel Beltran', N'Shiny', N'Paniqui, Tarlac', N'9495752625', NULL, NULL, 2058)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172059, N'Reynaldo Camat', N'Puti', N'Paniqui, Tarlac', N'9482240123', NULL, NULL, 2059)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172060, N'Glendel Daileg', N'Chukie', N'Guiteb, Ramos, Tarlac', N'9263839243', NULL, NULL, 2060)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172061, N'Bernadette  Galapon', N'Gucci / Chanel', N'Pinsaling, Gerona, Tarlac', N'9156951769', NULL, NULL, 2061)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172062, N'Louinelle Tiangsing', N'Ginger', N'Lacayanga Subd., Paniqui, Tarlac', N'9487489747', NULL, NULL, 2062)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172063, N'Lorenzo Chuateco', N'George ', N'Salumague, Paniqui, Tarlac', N'9487576771', NULL, NULL, 2063)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172064, N'Flora Capinpin', N'Black', N'Abogado, Paniqui, Tarlac', N'9558858238', NULL, NULL, 2064)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172065, N'Princess Belle Acob', N'Shiro', N'Buenlag, Gerona, Tarlac', N'9950731674', NULL, NULL, 2065)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172066, N'Rosario Dasalla', N'Princess', N'Pob. Norte, Paniqui, Tarlac', N'9052575342', NULL, NULL, 2066)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172067, N'Rodilyn Lopez', N'Lebron', N'Camp. 1 Norte, Moncada, Tarlac', N'9669617479', NULL, NULL, 2067)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172068, N'Jake Roxas', N'Jacob ', N'Baltazar Subd. Poblacion Norte, Paniqui, Tarlac', N'9173511001', NULL, NULL, 2068)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172069, N'Jebby Gonzales', N'Covid', N'Poblacion Sur, Paniqui, Tarlac', N'9473331373', NULL, NULL, 2069)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172070, N'Kim Ercy Lagmay', N'Grey', N'Carino, Paniqui, Tarlac', N'9993122917', NULL, NULL, 2070)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172071, N'Emmanuel Fronda', N'Bella', N'Pasig City', N'9354354064', NULL, NULL, 2071)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172072, N'Richelle Ariaga', N'Apollo/Pilar', N'Anao, Tarlac', N'9776457714', NULL, NULL, 2072)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172073, N'Gryfel Joy Ronquillo', N'Loki', N'Nipaco, Paniqui, Tarlac', N'9083670050', NULL, NULL, 2073)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172074, N'Sheridan Nestor Santillan', N'Nimbus', N'Coral, Paniqui Tarlac', N'9089228314', NULL, NULL, 2074)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172075, N'Geneveive Beltran', N'Zia', N'Patalan, Paniqui, Tarlac', N'9669788206', NULL, NULL, 2075)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172076, N'Genmark Rodriguez', N'Alley', N'Abogado, Paniqui, Tarlac', N'9667786035', NULL, NULL, 2076)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172077, N'Rygie Carulla', N'Tichalla', N'Maungib, Pura, Tarlac', N'9166860671', NULL, NULL, 2077)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172078, N'JC Louise Aquino', N'Toshi', N'Rizal, Moncada, Tarlac', N'9985725270', NULL, NULL, 2078)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172079, N'Crisanto Cortez', N'Chelsea', N'Carino, Paniqui, Tarlac', N'9487754774', NULL, NULL, 2079)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172080, N'David Bala', N'King', N'Samput, Paniqui, Tarlac', N'9985854121', NULL, NULL, 2080)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172081, N'Myrna Dela Cruz', N'Akihiro', N'Carino, Paniqui, Tarlac', N'9201273876', NULL, NULL, 2081)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172082, N'Bobby Madamba', N'Maggie', N'Poblacion Norte, Paniqui, Tarlac', N'9152705538', NULL, NULL, 2082)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172083, N'Arnel Sadang', N'Cheesecake', N'Camposanto 1, Moncada, Tarlac', N'9472833886', NULL, NULL, 2083)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172084, N'Joana Krista Tan', N'Sushi', N'Cuyapo, Nueva Ecija', N'9175083281', NULL, NULL, 2084)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172085, N'Jesusa Guansing', N'Munaynay', N'Nampicuan, Nueva Ecija', N'9955657132', NULL, NULL, 2085)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172086, N'Dana Keith Benito', N'Cooki', N'Apulid, Paniqui, Tarlac', N'9279403387', NULL, NULL, 2086)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172087, N'Vincent Daileg ', N'Hanzo', N'Tablang, Paniqui, Tarlac', N'9105327251', NULL, NULL, 2087)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172088, N'Regine Palarca ', N'Georgia', N'Canan, Paniqui, Tarlac', N'9171344628', NULL, NULL, 2088)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172089, N'Rina Paculguin', N'Bless', N'Nampicuan, Nueva Ecija', N'9301376272', NULL, NULL, 2089)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172090, N'Alexis Kiok', N'Boots', N'M.H. Del Pilar St. Paniqui, Tarlac', N'9173119131', NULL, NULL, 2090)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172091, N'Elma Paras', N'Yhumie', N'Dalayap, Tarlac City', N'9503305935', NULL, NULL, 2091)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172092, N'Janeth Dameg', N'Shargy', N'Acocolao, Paniqui, Tarlac', N'9485681878', NULL, NULL, 2092)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172093, N'Kenneth Mabanglo', N'Oni', N'Samput, Paniqui, Tarlac', N'9667799593', NULL, NULL, 2093)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172094, N'Edgar Cayanan', N'Jack', N'Amacalan, Gerona, Tarlac', N'9467603661', NULL, NULL, 2094)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172095, N'Aucel Gaculais', N'Champ', N'Matalapitap, Paniqui, Tarlac', N'9473370022', NULL, NULL, 2095)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172096, N'Ashley Jane Marquez', N'Hinata', N'San Francisco West, Anao, Tarlac', N'9155061312', NULL, NULL, 2096)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172097, N'Evangeline Soriano', N'Gigi', N'Guiteb, Ramos, Tarlac', N'9273772001', NULL, NULL, 2097)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172098, N'Judy Rose Luis', N'Loca', N'Pob.Sur, Paniqui, Tarlac', N'9772669995', NULL, NULL, 2098)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172099, N'Jayson Garcia', N'Katniss', N'Apulid, Paniqui, Tarlac', N'9532592435', NULL, NULL, 2099)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172100, N'Justine Doreen Valencia', N'Mason', N'Patalan, Paniqui, Tarlac', N'9953381871', NULL, NULL, 2100)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172101, N'Luis Magdangal', N'Pam-Pam', N'Canan, Paniqui, Tarlac', N'985-5595', NULL, NULL, 2101)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172102, N'Cathy Llamas', N'Putipot', N'Samput, Paniqui, Tarlac', N'9451178513', NULL, NULL, 2102)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172103, N'Mark Neil Abella', N'Atlas', N'Cabawangan, Nampicuan, Nueva Ecija', N'9456224847', NULL, NULL, 2103)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172104, N'Neil Butalid ', N'Cham-Cham', N'Samput, Paniqui, Tarlac', N'9466498729', NULL, NULL, 2104)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172105, N'Cynthia Rebolledo', N'Beau', N'Cayanga, Paniqui, Tarlac', N'9215714817', NULL, NULL, 2105)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172106, N'Mark Angelo Buscaino', NULL, N'Nampicuan, Nueva Ecija', N'9974494254', NULL, NULL, 2106)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172107, N'Vernaflor Molo', N'King', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9984944156', NULL, NULL, 2107)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172108, N'Casey Michaela Sapon', N'Chip', N'Sta. Maria, Moncada, Tarlac', N'9369416562', NULL, NULL, 2108)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172109, N'Rosanna Castro', NULL, N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9079708129', NULL, NULL, 2109)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172110, N'Mabelle Ladines', N'Snow', N'Sinense, Anao, Tarlac', N'9952276902', NULL, NULL, 2110)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172111, N'Roze Viera Cachin', N'Domino/Doritos/Ruffles', N'Samput, Paniqui, Tarlac', N'9395612066 / 491-12837', NULL, NULL, 2111)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172112, N'Shaina Novenario', N'Cholo', N'Tablang, Paniqui, Tarlac', N'9121979440', NULL, NULL, 2112)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172113, N'Justine Delos Reyes', N'Zoey', N'Rang- Ayan, Paniqui, Tarlac', N'9096431621', NULL, NULL, 2113)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172114, N'Wilfredo Sudaria', N'Luna', N'Ventinilla, Paniqui, Tarlac', N'9155902057', NULL, NULL, 2114)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172115, N'Lilet Ann Santiago', N'Loki', N'Poblacion 3, Gerona, Tarlac', N'9171244738', NULL, NULL, 2115)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172116, N'Vince Elizarde', N'Tokyo', N'Ramos, Tarlac', N'9190899809', NULL, NULL, 2116)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172117, N'Amalia Corpuz ', N'Biday', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9177071905', NULL, NULL, 2117)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172118, N'Gilbert Malazo', N'Princess Amber', N'San Manuel, Tarlac', N'9465625520', NULL, NULL, 2118)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172119, N'Virgilio Pioquinto', N'Shadow', N'Maybubon, Guimba, Nueva Ecija ', N'9107587154', NULL, NULL, 2119)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172120, N'Evangeline Cariño', N'Cutie ', N'Pob. Norte, Paniqui, Tarlac', N'9052371485', NULL, NULL, 2120)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172121, N'Wilner Casica', N'Kane', N'San Manuel, Tarlac', N'9264817931', NULL, NULL, 2121)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172122, N'Pelita Padalla', N'Lily', N'Cuyapo, Nueva Ecija', N'9752942629', NULL, NULL, 2122)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172123, N'May Ann Beltran', N'Sachi', N'Baltazar Subd. Poblacion Norte, Paniqui, Tarlac', N'9484158398', NULL, NULL, 2123)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172124, N'May Macadangdang', N'Oreo', N'Santiago, Gerona, Tarlac', N'9277088648', NULL, NULL, 2124)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172125, N'Trisha Nool', N'Woopie', N'Cayanga, Paniqui, Tarlac', N'9614811101', NULL, NULL, 2125)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172126, N'Olga Flores', N'Maliah', N'Cuyapo, Nueva Ecija', N'9172777589', NULL, NULL, 2126)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172127, N'Dina Buenaventura', N'Chiddy', N'Estacion, Paniqui, Tarlac', N'9055038459', NULL, NULL, 2127)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172128, N'Diana Lactao / Ria Casco', N'Roan', N'Nancamarinan, Paniqui, Tarlac', N'9652303506', NULL, NULL, 2128)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172129, N'Marygold Castillo', N'Happy', N'Poblacion Norte, Paniqui, Tarlac', N'9650939703', NULL, NULL, 2129)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172130, N'Mareon Claris Gezef Vergara', N'Milka', N'Poblacion 1, Moncada, Tarlac', N'9771962289', NULL, NULL, 2130)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172131, N'Ryan Magaoay', N'Peachy', N'Almaville Subd, Camanggan East, Moncada, Tarlac', N'9275306512', NULL, NULL, 2131)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172132, N'Merlyn Diemsen ', N'Mollie ', N'Acocolao, Paniqui, Tarlac', N'9151667263', NULL, NULL, 2132)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172133, N'Freddie Manuel ', N'Proxy', N'Estacion, Paniqui, Tarlac', N'9283531002', NULL, NULL, 2133)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172134, N'Marizel Dameg', N'Thirdy', N'Acocolao, Paniqui, Tarlac', N'9129975096', NULL, NULL, 2134)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172135, N'Christopher Banania', N'Cobie', N'Abogado, Paniqui, Tarlac', N'9301823884', NULL, NULL, 2135)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172136, N'Regina Serquinia', N'Chowfun', N'San Manuel, Tarlac', N'9676421294', NULL, NULL, 2136)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172137, N'Roanne Sales', N'Zoe', N'Abogado, Paniqui, Tarlac', N'9632138865', NULL, NULL, 2137)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172138, N'Adrian Breganio', N'Spencer', N'Abogado, Paniqui, Tarlac', N'9562683186', NULL, NULL, 2138)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172139, N'Ma. Karen Lopez', N'Mogli', N'Coral, Paniqui, Tarlac', N'9380899024', NULL, NULL, 2139)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172140, N'Genalyn Taygon', NULL, N'Patalan, Paniqui, Tarlac', N'9052580985', NULL, NULL, 2140)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172141, N'Rosemarie Guzman', N'Bailey', N'West Central Poblacion, Nampicuan, Nueva Ecija', N'9175724959', NULL, NULL, 2141)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172142, N'Rhaizel Shane Bactad', N'Toffee', N'Sta. Ignacia, Tarlac', N'9163138711', NULL, NULL, 2142)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172143, N'Jenny Fabros', N'Mat-Mat', N'Coral, Paniqui Tarlac', N'9123172010', NULL, NULL, 2143)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172144, N'Bryan Sacramento', N'Chloe', N'Coral, Ramos, Tarlac', N'9308490281', NULL, NULL, 2144)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172145, N'Michelle Angoluan', N'Luna', N'Burgos, Moncada, Tarlac', N'9196437352', NULL, NULL, 2145)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172146, N'Elaiza Egipto', N'Pepper', N'Gerona, Tarlac', N'9957205585', NULL, NULL, 2146)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172147, N'Czarina Mei Esteban', N'Tofu ', N'Estacion, Paniqui, Tarlac', N'9266898380', NULL, NULL, 2147)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172148, N'Noel Mangabat', N'Akhi', N'Salumague, Paniqui, Tarlac', N'9338149377', NULL, NULL, 2148)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172149, N'Ronaldo Cabaya', N'Maxen', N'San Manuel, Tarlac', N'9659556655', NULL, NULL, 2149)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172150, N'Marinella Bacsa', N'Laycan', N'Balaoang, Paniqui, Tarlac', N'9457163660', NULL, NULL, 2150)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172151, N'Cheyz Kyle Silvestre', N'Billy', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'9309105916', NULL, NULL, 2151)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172152, N'Ma. Elaine Zinampan', N'Cash', N'Poblacion Norte, Paniqui, Tarlac', N'9513242067', NULL, NULL, 2152)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172153, N'Marvin Evangelista', N'Chuchay', N'Camiling Tarlac', N'9999041080', NULL, NULL, 2153)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172154, N'Pamela Katrice Novelozo', N'Summer', N'Poblacion 1, Moncada, Tarlac', N'9560941037', NULL, NULL, 2154)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172155, N'Xaira Ann Magday', N'Winter', N'Rasig, Paniqui, Tarlac', N'9088743155', NULL, NULL, 2155)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172156, N'Lynee Nichole Galvez', N'Wina', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9615561912', NULL, NULL, 2156)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172157, N'Theresa Napoles', N'Mochi', N'Canan, Paniqui, Tarlac', N'9153400076', NULL, NULL, 2157)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172158, N'Alexandra Beltran', NULL, N'Coral, Paniqui, Tarlac', N'9667966371', NULL, NULL, 2158)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172159, N'Mac Natural', N'Lilo', N'Del Pilar, Estacion, Paniqui, Tarlac', N'9478407366', NULL, NULL, 2159)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172160, N'Erica Asprer', N'Mallows', N'Poblacion 2, Moncada, Tarlac ', N'9270890561', NULL, NULL, 2160)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172161, N'Erika Joy Belga', N'Bizmac', N'Poblacion North, Ramos, Tarlac', N'9613571669', NULL, NULL, 2161)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172162, N'Raymond Kasilag', N'Bornok', N'Dalayoan Subd. Pob. Sur, Paniqui, Tarlac', N'9175761040', NULL, NULL, 2162)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172163, N'Jelix Garcia', NULL, N'Estipona, Pura, Tarlac', N'9771206320', NULL, NULL, 2163)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172164, N'Ma. Honey Grace ibay', N'Snow', N'Samput, Paniqui, Tarlac', NULL, NULL, NULL, 2164)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172165, N'Gumercindo Bravo', N'Bella', N'Estacion, Paniqui, Tarlac', N'9062586471', NULL, NULL, 2165)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172166, N'Amelia Robles', N'Lilo', N'Cabayaoasan, Paniqui, Tarlac', N'9105386314', NULL, NULL, 2166)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172167, N'Fresel Jane Lanuza', N'Chowy ', N'San Jose, Tarlac', N'9201766390', NULL, NULL, 2167)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172168, N'Melanie Tomas', NULL, N'Balaoang, Paniqui, Tarlac', N'9159159542', NULL, NULL, 2168)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172169, N'Jun Pineda ', N'Yhan-Yhan', N'Cayanga, Paniqui, Tarlac', N'9999049277', NULL, NULL, 2169)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172170, N'Danyel Semanero', N'Harry', N'Pob. Norte, Paniqui, Tarlac', N'9279798314', NULL, NULL, 2170)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172171, N'Desiree Dela Cruz', N'Zeki', N'Balaoang, Paniqui, Tarlac', N'9457951745', NULL, NULL, 2171)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172172, N'Anna Marie Soriano', N'Chuchu', N'Burgos, Moncada, Tarlac', N'9472828997', NULL, NULL, 2172)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172173, N'Cenon Lim', N'Sky', N'Poblacion 2, Moncada, Tarlac ', N'9477720444', NULL, NULL, 2173)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172174, N'Cynthia Saranilla', N'Bella', N'Samput, Paniqui, Tarlac', N'9268295029', NULL, NULL, 2174)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172175, N'Anne Julianne Chu', N'Creeper', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'9273770816', NULL, NULL, 2175)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172176, N'Maricel Tenorio', N'Sheikah', N'Mabini, Moncada, Tarlac', N'9196490198', NULL, NULL, 2176)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172177, N'Alyzza Gayll Gonzales', N'Yuno', N'Ramos, Tarlac', N'9159597341', NULL, NULL, 2177)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172178, N'Julianne Crissa Rosete', N'Love', N'San Juan, Moncada, Tarlac', N'9457284702', NULL, NULL, 2178)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172179, N'Cary Gabriel Daus', N'Lucy', N'Coral, Paniqui, Tarlac', N'9682903725', NULL, NULL, 2179)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172180, N'Patricia Melchor', N'Bam', N'Carino, Paniqui, Tarlac', N'9469688500', NULL, NULL, 2180)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172181, N'Micaela Betis', N'Sana', N'Cuyapo, Nueva Ecija', N'9059539665', NULL, NULL, 2181)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172182, N'Lorena Austria', NULL, N'Abogado, Paniqui, Tarlac', N'9612269984', NULL, NULL, 2182)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172183, N'Edgar Amar', N'Ranger', N'Oloybuaya, Gerona, Tarlac', N'9484941873', NULL, NULL, 2183)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172184, N'Frances Gabriel', N'Kiko', N'Paniqui, Tarlac', N'9397969944', NULL, NULL, 2184)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172185, N'Noel Melegrito', N'Meiji', N'Anao, Tarlac', N'9099687145', NULL, NULL, 2185)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172186, N'Jerome Marzan', N'Ginger', N'San Francisco West, Anao, Tarlac', N'9192220092', NULL, NULL, 2186)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172187, N'Jackielyn Faller', N'Hershey', N'Poblacion Sur, Paniqui, Tarlac', N'9165159418', NULL, NULL, 2187)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172188, N'RJ De Jesus', N'Cody', N'Salumague, Paniqui, Tarlac', N'9950748629', NULL, NULL, 2188)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172189, N'Janelle Cailing', N'Lexie', N'Cuyapo, Nueva Ecija', N'9271224965/9454227695', NULL, NULL, 2189)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172190, N'Lilibeth Galatierra ', N'Hearty', N'Coral, Paniqui, Tarlac', N'9439188652', NULL, NULL, 2190)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172191, N'Ear John Lamorena', N'Choco', N'Sampaloc, Manila ', N'9065142718', NULL, NULL, 2191)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172192, N'Elisha May Feraro', N'Dino', N'San Antonio, Gerona, TArlac', N'9394375935', NULL, NULL, 2192)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172193, N'Razel Ramas', N'Pepper', N'Matalapitap, Paniqui, Tarlac', N'9109083726', NULL, NULL, 2193)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172194, N'Jasmin Navaira', N'Jersey', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'9208004343', NULL, NULL, 2194)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172195, N'Alexis Tamayo', N'Rango', N'Pob. 4, Moncada, Tarlac', N'998847465', NULL, NULL, 2195)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172196, N'Valerie Medoza', N'Docker', N'Moncada, Tarlac', N'9167916804', NULL, NULL, 2196)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172197, N'Michelle Bautista', N'Lucy', N'Salumague, Paniqui, Tarlac', N'9957553373', NULL, NULL, 2197)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172198, N'Joraine Gallardo ', N'Bubbles', N'Ambassador Alzate Village, Nampicuan, Nueva Ecija ', N'9499035003', NULL, NULL, 2198)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172199, N'Telesforo Lipana', N'Biboy', N'Poblacion Sur, Paniqui, Tarlac', N'9310271', NULL, NULL, 2199)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172200, N'Jop Perez', N'Bless', N'Abogado, Paniqui, Tarlac', N'9273772983', NULL, NULL, 2200)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172201, N'Rochelle Castillo', N'Athina', N'Poblacion Sur, Paniqui, Tarlac', N'9267881466', NULL, NULL, 2201)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172202, N'Yesha Denise Tawatao', N'Hiroshi', N'Coral, Ramos, Tarlac', N'9452534771', NULL, NULL, 2202)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172203, N'Jiro Franco Santos', N'Blizzard', N'Tolega Norte, Moncada, Tarlac', N'9395013796', NULL, NULL, 2203)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172204, N'Angelika Olaño', N'Buddy ', N'Balayang, Victoria Tarlac', N'9105289687', NULL, NULL, 2204)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172205, N'Menchie Ergeno', N'Whitie', N'Carino, Paniqui, Tarlac', N'9287788064', NULL, NULL, 2205)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172206, N'Paul Joefrey Gruspe', N'Luna', N'Balaoang, Paniqui, Tarlac', N'9770838350', NULL, NULL, 2206)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172207, N'Ian Joefrey Cariño ', N'Arya', N'Patalan, Paniqui, Tarlac', N'9264847338', NULL, NULL, 2207)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172208, N'Jamaica Ragsac', N'Toby', N'Sinigpit, Paniqui, Tarlac', N'9159637498', NULL, NULL, 2208)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172209, N'Ralvin Raposas', N'Coco', N'Sinigpit, Paniqui, Tarlac', N'9953546083', NULL, NULL, 2209)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172210, N'Mark Jhon Paul Gorospe', N'Oceans', N'Samput, Paniqui, Tarlac', N'9954417345', NULL, NULL, 2210)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172211, N'Anne Marie Tolete', N'Quinnie', N'Carino, Paniqui, Tarlac', N'9209326639', NULL, NULL, 2211)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172212, N'Marilou Montoya', N'Pinang', N'Matalapitap, Paniqui, Tarlac', N'9283622514', NULL, NULL, 2212)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172213, N'Jayzel Anne Romero', N'Tyson', N'San Manuel, Tarlac', N'9672358899', NULL, NULL, 2213)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172214, N'Michael Agustin', N'Chix ', N'Anao, Tarlac', N'9384695675', NULL, NULL, 2214)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172215, N'Marianne Macantan', N'Yumi', N'Camp. 1 Norte, Moncada, Tarlac', N'9663710602', NULL, NULL, 2215)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172216, N'John Arnold Delos Santos', N'Rabbit', N'San Manuel, Tarlac', N'9458338763', NULL, NULL, 2216)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172217, N'Tiffany Beatriz Garcia', N'Murphy', N'Maluac, Moncada, Tarlac', N'9167268299', NULL, NULL, 2217)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172218, N'Fredalyn Abril', N'Lucas', N'Del Valle Estacion, Paniqui Tarlac', N'9182012121', NULL, NULL, 2218)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172219, N'Virgie Diza', N'Lucky', N'Sta. Maria, Moncada, Tarlac', N'9668359808', NULL, NULL, 2219)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172220, N'Allan Marquez', N'Thatiana', N'Purok Maligaya, Pob. Norte, Paniqui, Tarlac', N'9457883688', NULL, NULL, 2220)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172221, N'Charles Valix', N'Dos', N'Licor St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9190847235', NULL, NULL, 2221)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172222, N'Ria Cuadro', N'Gucci', N'Carapdapan, Pob.Norte, Paniqui, Tarlac', N'9618423384', NULL, NULL, 2222)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172223, N'Mary Joy Molina', N'Putol', N'Cuyapo, Nueva Ecija', N'9275496480', NULL, NULL, 2223)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172224, N'Julie Ann Florendo', N'Nari', N'Poblacion Sur, Paniqui, Tarlac', N'9239389924', NULL, NULL, 2224)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172225, N'Lyra May Quibuyen', N'Murphy', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9453347208', NULL, NULL, 2225)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172226, N'Renato Fernandez', N'Kobe', N'Poblacion Norte, Paniqui, Tarlac', N'9999204244', NULL, NULL, 2226)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172227, N'Julius Diamsay', N'Gavin', N'Canan, Paniqui, Tarlac', N'9490910408', NULL, NULL, 2227)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172228, N'Andrea Ancheta', N'Ollie', N'Del Valle Estacion, Paniqui Tarlac', N'9100341231', NULL, NULL, 2228)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172229, N'Arlene Beltran ', N'Tasha', N'Estacion, Paniqui, Tarlac', N'9172038866', NULL, NULL, 2229)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172230, N'Virgilio Tiangco', N'Troy', N'Clark St., Poblacion Sur, Paniqui, Tarlac', N'9324854728', NULL, NULL, 2230)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172231, N'Ralfie Lapinosa', N'Suzie', N'Samput, Paniqui, Tarlac', N'9511563804', NULL, NULL, 2231)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172232, N'Jenever Zorilla', N'Panda', N'Pob. 3, Moncada, Tarlac', N'9095048246', NULL, NULL, 2232)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172233, N'Marvin Jayson Vinluan', N'Maki', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9054847199', NULL, NULL, 2233)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172234, N'Minerva Quinio', N'Jolly', N'Poblacion Norte, Paniqui, Tarlac', N'9216583591', NULL, NULL, 2234)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172235, N'Camille Noelle Linsao', N'Purrcy ', N'Abogado, Paniqui, Tarlac', N'9177800728', NULL, NULL, 2235)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172236, N'Grace Villanueva', N'Sushi', N'Southwest Poblacion, Nampicuan, Nueva Ecija', N'9399047380', NULL, NULL, 2236)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172237, N'Karen Villanueva', N'Mochie', N'Balza, Paniqui, Tarlac', N'9395954242', NULL, NULL, 2237)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172238, N'John Fabian', N'Iro', N'Colibangbang, Paniqui, Tarlac', N'9505258845', NULL, NULL, 2238)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172239, N'Dionel Laurente', N'Sky', N'Burgos, Moncada, Tarlac', N'9771193314', NULL, NULL, 2239)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172240, N'Ezra Lambino', N'Rain', N'Paniqui, Tarlac', N'9102569851', NULL, NULL, 2240)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172241, N'Juvy Capilo', N'Skye', N'Salumague, Paniqui, Tarlac', N'9156241089', NULL, NULL, 2241)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172242, N'Sidonie Ashley Lopez', N'Samantha', N'Estacion, Paniqui, Tarlac', N'9092127519', NULL, NULL, 2242)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172243, N'Josephine Kadir ', NULL, N'Ermita, Manila', N'9175121111', NULL, NULL, 2243)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172244, N'Dharyl Caratiquit', N'Kisses', N'Nilasin 1st, Pura, Tarlac', N'9503454439', NULL, NULL, 2244)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172245, N'Christian Ray Ubay', N'Coco', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9270893588', NULL, NULL, 2245)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172246, N'Maricel Bumanlag', N'Damboo', N'Del Pilar, Estacion, Paniqui, Tarlac', N'9500605681', NULL, NULL, 2246)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172247, N'Ayka Angela Chichioco', N'Aki', N'Lacayanga Subd., Paniqui, Tarlac', N'9560071012', NULL, NULL, 2247)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172248, N'Jessie Balbas', N'Kyrie', N'Sta. Lucia East, Moncada, Tarlac', N'9214448176', NULL, NULL, 2248)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172249, N'Aeriel Jane Torio', N'Aki', N'Palarca St., Brgy. Poblacion Norte, Paniqui, Tarlac', N'9462216598', NULL, NULL, 2249)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172250, N'Aiza Ramos', N'Yzay', N'Samput, Paniqui, Tarlac', N'9515256245', NULL, NULL, 2250)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172251, N'Robertson Ferrer', N'Oreo', N'Brillante, Paniqui, Tarlac', N'9955176564', NULL, NULL, 2251)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172252, N'Princess Angel Santos', N'Cholo', N'Carino, Paniqui, Tarlac', N'9561267449', NULL, NULL, 2252)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172253, N'Leny Lagmay', N'Banysang', N'Carino, Paniqui, Tarlac', N'9187984808', NULL, NULL, 2253)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172254, N'Rose Ann Ramos', N'Lucy', N'Carino, Paniqui, Tarlac', N'9182424208', NULL, NULL, 2254)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172255, N'Jillian Cabarero', N'Scarlet', N'Poblacion South, Ramos, Tarlac', N'9455797712', NULL, NULL, 2255)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172256, N'Reggie Perez', N'Pitz / Kobi', N'Estacion, Paniqui, Tarlac', N'9611367191', NULL, NULL, 2256)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172257, N'Joanna Marie Magtalas', N'Lyn', N'Sta. Lucia East, Moncada, Tarlac', N'9212521547', NULL, NULL, 2257)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172258, N'Kata Facunla', N'Bimby', N'Poblacion Center, Ramos, Tarlac', NULL, NULL, NULL, 2258)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172259, N'Joe Mon', N'Axe', N'Paniqui, Tarlac', N'9662288118', NULL, NULL, 2259)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172260, N'Mariel Garrubo', N'Bella', N'Carino, Paniqui, Tarlac', N'9122885464', NULL, NULL, 2260)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172261, N'John Michael Palacio', N'Oreo', N'Poblacion Norte, Paniqui, Tarlac', N'9157415482', NULL, NULL, 2261)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172262, N'Angel Chloe Alcantara', N'Chanel', N'Estacion, Paniqui, Tarlac', N'9270892857', NULL, NULL, 2262)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172263, N'Mia Lim-ang', N'Stormi', N'Nampicuan, Nueva Ecija', N'9568860966', NULL, NULL, 2263)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172264, N'Trishia Mae De Vera', N'Kiro', N'Acocolao, Paniqui, Tarlac', N'907286612', NULL, NULL, 2264)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172265, N'Ryan Rempillo', N'Maxx', N'Rizal, Moncada, Tarlac', N'9285729713', NULL, NULL, 2265)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172266, N'Charise Diaz', N'Chucky', N'Sitio Balsa, San Isidro Paniqui, Tarlac', N'9216561065', NULL, NULL, 2266)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172267, N'Dr. Rex Santos', N'Bruno', N'Canan, Paniqui, Tarlac', N'9185594710 / 9312856', NULL, NULL, 2267)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172268, N'Willy Dulay', N'Haliah', N'Sta. Lucia West, Moncada, Tarlac', N'9107105536', NULL, NULL, 2268)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172269, N'Jeanny Cristina Gallema', N'Kuting', N'Estacion, Paniqui, Tarlac', N'9497755644', NULL, NULL, 2269)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172270, N'Ric Daileg', N'Sinag', N'Guiteb, Ramos, Tarlac', NULL, NULL, NULL, 2270)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172271, N'Editha Claunan', N'Lucy', N'Poblacion North, Ramos, Tarlac', N'9422831708', NULL, NULL, 2271)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172272, N'Wilfredo Valeriano', N'Buster', N'Estacion, Paniqui, Tarlac', N'9276605060', NULL, NULL, 2272)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172273, N'Amada Tomas', N'Tobi', N'Estacion, Paniqui, Tarlac', N'9563817670', NULL, NULL, 2273)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172274, N'Gen Aicetil Callejo', N'Flynn', N'Burgos, Moncada, Tarlac', N'9060798551', NULL, NULL, 2274)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172275, N'Ma. Gelene Uson', N'Tobby ', N'Poblacion Sur, Paniqui, Tarlac', N'9099863673', NULL, NULL, 2275)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172276, N'Rinna Macadangdang', N'Dos', N'Carino, Paniqui, Tarlac', N'9206151927', NULL, NULL, 2276)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172277, N'Jerome Gaudia', N'Sky', N'Guiteb, Ramos, Tarlac', N'9563026642', NULL, NULL, 2277)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172278, N'Mark Joseph Cornejo', N'Alee', N'Moncada, Tarlac', N'9199237018', NULL, NULL, 2278)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172279, N'Christian Jay Pagaduan', N'Brutos', N'Samput, Paniqui, Tarlac', N'9463162430', NULL, NULL, 2279)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172280, N'Rufina Mandap', N'Bambam', N'Samput, Paniqui, Tarlac', N'9302821148', NULL, NULL, 2280)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172281, N'Camille Jade Villanueva', N'Dumbo', N'Patalan, Paniqui, Tarlac', N'9198751999', NULL, NULL, 2281)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172282, N'Charito Toledo', N'Kitkat', N'Anao, Tarlac', N'9063785721', NULL, NULL, 2282)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172283, N'Esli May Ismael', N'Lucy', N'Pantol, Bayambang, Pangasinan', N'9190928823', NULL, NULL, 2283)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172284, N'Rezeine Esteban', N'Kwak', N'Estacion, Paniqui, Tarlac', N'9190792235', NULL, NULL, 2284)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172285, N'Arnel Del Rosario', N'Chi-Chi', N'Poblacion Sur, Paniqui, Tarlac', N'9669946397', NULL, NULL, 2285)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172286, N'Melanie Dumlao', N'Basya', N'Tolega Sur, Moncada, Tarlac', N'9468908311', NULL, NULL, 2286)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172287, N'Hannie Ruth Garcia', N'Brusse', N'San Juan, Moncada, Tarlac', N'9279532126', NULL, NULL, 2287)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172288, N'Romeo Santiago Jr.', N'Britt', N'Estacion, Paniqui, Tarlac', N'9389074907', NULL, NULL, 2288)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172289, N'Jhoy Baltazar', N'Star', N'Samput, Paniqui, Tarlac', N'9102569773', NULL, NULL, 2289)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172290, N'Jane Salinas', N'Morning', N'San Manuel, Tarlac', N'9173051478', N' ', NULL, 2290)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172291, N'Noli Delos Reyes', NULL, N'Nancamarinan, Paniqui, Tarlac', N'9304418250 / 09094595559', NULL, NULL, 2291)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172292, N'William Mendoza', N'Kobe', N'Camposanto 1 Sur, Moncada, Tarlac', N'9665637325', NULL, NULL, 2292)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172293, N'Claidz Sadiaza', N'Olaf', N'Poblacion Norte, Paniqui, Tarlac', N'9285722095', NULL, NULL, 2293)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172294, N'Krizia Anne Dela Cruz', N'LV', N'San Jose South, Anao, Tarlac', N'9151661737', NULL, NULL, 2294)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172295, N'Bee Jay Lagbo', N'Tala', N'Magaspac, Gerona, Tarlac', N'995768428', NULL, NULL, 2295)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172296, N'Gerdelle John Mori', N'Momo', N'Buenlag, Gerona, Tarlac', N'9094959269', NULL, NULL, 2296)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172297, N'Maryon Angelo Rejuso', N'Yuna', N'Salumague, Paniqui, Tarlac', N'9959435415', NULL, NULL, 2297)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172298, N'Ralph Philip Pagaduan', N'Jacqui / Vito', N'Aringin, Moncada Tarlac', N'9088756526', NULL, NULL, 2298)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172299, N'Alpha Tangalin', N'Orange / Kitty', N'North Poblacion, Nampicuan Nueva Ecija', N'91072338353', NULL, NULL, 2299)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172300, N'Edison Aviguetero', N'Panpan', N'Nancamarinan, Paniqui, Tarlac', N'9094720037', NULL, NULL, 2300)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172301, N'Donita Rose Gahum', N'Max', N'Burgos, Moncada, Tarlac', N'9952288927', NULL, NULL, 2301)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172302, N'Roldan Capinding', N'Skye', N'Villa Nadine Subd., Brgy. Acocolao, Paniqui, Tarlac', N'9957866030', NULL, NULL, 2302)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172303, N'Vhince Dylan Dumantay', N'Hachi', N'San Julian, Moncada, Tarlac', N'9167494550', NULL, NULL, 2303)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172304, N'Jennifer Anne Kaine ', N'Ozzy', N'San Jose South, Anao, Tarlac', N'9363330599', NULL, NULL, 2304)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172305, N'Rhonalyn Loreto', N'Paquito', N'Cabayaoasan, Paniqui, Tarlac', N'9516779965', NULL, NULL, 2305)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172306, N'James Andrei Melivo', N'Andy', N'Poblacion Sur, Paniqui, Tarlac', N'9277093946', NULL, NULL, 2306)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172307, N'Behn Buentipo', N'Kopi', N'Acocolao, Paniqui, Tarlac', N'9612449138', NULL, NULL, 2307)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172308, N'Alyssa Rose Alcantara', N'Mobe', N'Lacayanga Subd., Paniqui, Tarlac', N'9688535391', NULL, NULL, 2308)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172309, N'Marilou Gruspe', N'Nutella', N'Balaoang, Paniqui, Tarlac', N'9773989082', NULL, NULL, 2309)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172310, N'Samantha Rose Gamboa', N'Rayon', N'Nampicuan, Nueva Ecija', N'9973583765', NULL, NULL, 2310)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172311, N'Elna Navarro', N'Luna / Shine', N'San Miguel, San Manuel Tarlac', N'9771984331', NULL, NULL, 2311)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172312, N'Robert Gallema', N'Sophia', N'Estacion, Paniqui, Tarlac', N'9162169936', NULL, NULL, 2312)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172313, N'Genesis Tamayo', N'Tim/Hamster/Goldey', N'Paniqui, Tarlac', N'9771712550', NULL, NULL, 2313)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172314, N'Edgar Polintan', N'Kobe', N'Manaois, Paniqui, Tarlac', N'9273447830', NULL, NULL, 2314)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172315, N'Dorabel Abarra', N'Lexus', N'Sta. Lucia West, Moncada, Tarlac', N'9059747209', NULL, NULL, 2315)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172316, N'Maricel Mejia', N'Willie', N'Abogado, Paniqui, Tarlac', N'9298212932', NULL, NULL, 2316)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172317, N'Carl James Ofrecio', N'Ayumi', N'Coral, Paniqui Tarlac', N'9466259385', NULL, NULL, 2317)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172318, N'Modesto Gascon ', N'Grey', N'Ablang Sapang, Moncada, Tarlac', N'9204908422', NULL, NULL, 2318)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172319, N'Hanna Allisa Esteban', N'Marshmallows', N'San Francisco West, Anao, Tarlac', N'9980010063', NULL, NULL, 2319)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172320, N'Edna Ongcal', N'Kirah', N'Rang- Ayan, Paniqui, Tarlac', N'9232778795', NULL, NULL, 2320)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172321, N'Ma. Rosario Fuentinegra', N'Riona ', N'Poblacion 2, Gerona, Tarlac', N'9064255817', NULL, NULL, 2321)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172322, N'Janice Cabigting', N'Pogi ', N'Anao, Tarlac', N'9184069883', NULL, NULL, 2322)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172323, N'Renato Vistan', N'Daisy', N'Poblacion Norte, Paniqui, Tarlac', N'9173485983', NULL, NULL, 2323)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172324, N'Junicar Javier', N'Freya', N'Pob. 1, Moncada, Tarlac', N'9397902091', NULL, NULL, 2324)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172325, N'Ayld Fernandez', N'Balbon', N'Paniqui, Tarlac', N'9451921557', NULL, NULL, 2325)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172326, N'Dennis Abuan', NULL, N'Moncada, Tarlac', N'9078620767', NULL, NULL, 2326)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172327, N'Noah Cariño', N'Millie ', N'Cuyapo, Nueva Ecija', N'9982516369', NULL, NULL, 2327)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172328, N'Dhes Enriquez', N'Cola ', N'Carriedo St., Paniqui, Tarlac', N'9954606439', NULL, NULL, 2328)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172329, N'Prince John Manalili', N'Chinchin', N'Apulid, Paniqui, Tarlac', N'9950788229', NULL, NULL, 2329)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172330, N'Faith Cacho', N'Timone', N'Carriedo St., Paniqui, Tarlac', N'9053370150', NULL, NULL, 2330)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172331, N'Ronald Molera', N'Bullet ', N'Poblacion Norte, Paniqui, Tarlac', N'9087678221', NULL, NULL, 2331)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172332, N'Rafael Gallieto', N'Luna', N'Victoria, Tarlac', N'9511707563', NULL, NULL, 2332)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172333, N'Jessa Lumague', N'Paula', N'Paniqui, Tarlac', N'9511707563', NULL, NULL, 2333)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172334, N'Allan Paul Sadaran', N'Haraki', N'Nampicuan, Nueva Ecija', N'9388264765', NULL, NULL, 2334)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172335, N'Lucky Garcia', N'Chuchay', N'Tarlac City', N'9484991969', NULL, NULL, 2335)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172336, N'Rosa Briones Gamboa', N'Harloth', N'Estacion, Paniqui, Tarlac', N'9753022710', NULL, NULL, 2336)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172337, N'Jolina Sagabaen', N'Toby', N'Ablang Sapang, Moncada, Tarlac', N'9634386642', NULL, NULL, 2337)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172338, N'Aljean Dela Cruz', N'Daisy', N'Ablang Sapang, Moncada, Tarlac', N'9634386642', NULL, NULL, 2338)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172339, N'Roselle Organes', N'Blaite', N'Baquero Norte, Moncada, Tarlac', N'9126276949', NULL, NULL, 2339)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172340, N'Joel Sicat Jr.', N'Bruno', N'Magallanes Pob. Sur Paniqui, Tarlac', N'9978991196', NULL, NULL, 2340)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172341, N'Emily Isidro', N'ChaCha', N'Guiteb, Ramos, Tarlac', N'9216137766', NULL, NULL, 2341)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172342, N'John Edmer Ilumin', N'Milo', N'Matalapitap, Paniqui, Tarlac', N'9302543963', NULL, NULL, 2342)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172343, N'Kozta Myka Tolentino', N'Martha Willow', N'Poblacion Norte, Paniqui, Tarlac', N'9453348519', NULL, NULL, 2343)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172344, N'Victor Ymnnuel Cortez', N'Winter', N'Buenlag, Gerona, Tarlac', N'9096476963', NULL, NULL, 2344)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172345, N'Jovita Nuguid ', N'Kali', N'Bugallon st. paniqui, Tarlac', N'9205314315', NULL, NULL, 2345)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172346, N'Allan Paul Lagman', N'Oreo', N'Tablang, Paniqui, Tarlac', N'9167546226', NULL, NULL, 2346)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172347, N'Glen Paez', N'Aji', N'Burgos st. Paniqui, Tarlac', N'92849934', NULL, NULL, 2347)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172348, N'Rowella Pararuan', N'Zhen ', N'San Felipe, San Manuel , Tarlac  ', N'9951786290', NULL, NULL, 2348)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172349, N'Nicole Andrey Bolos', N'Dale ', N'Carino, Paniqui, Tarlac', N'9310153273', NULL, NULL, 2349)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172350, N'James Palafox', N'Kim Chiu/Kim Chi', N'Poblacion 1, Moncada, Tarlac', N'9167910734', NULL, NULL, 2350)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172351, N'Kristine Javier', N'Nanu', N'Pacpaco, San Manuel, Tarlac', N'9456984728', NULL, NULL, 2351)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172352, N'Derlie Dela Cruz', N'Koji', N'Nampicuan, Nueva Ecija', N'9285019601', NULL, NULL, 2352)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172353, N'Mary Grace Tolosa', N'Chia', N'Camposanto 1 Norte, Moncada', N'9178051708', NULL, NULL, 2353)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172354, N'Bea Sinohin', N'Steward ', N'Guiteb, Ramos, Tarlac', N'9266895809', NULL, NULL, 2354)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172355, N'Kayle Galvez', N'Brodi', N'Poblacion Sur, Paniqui, Tarlac', N'9282588580', NULL, NULL, 2355)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172356, N'Geraldine Estabillo', N'Fritzie', N'Matapitap, Gerona, Tarlac', N'9988671148', NULL, NULL, 2356)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172357, N'Jenmarc Ilano', N'Oreo', N'San Julian, Moncada, Tarlac', N'9367645307', NULL, NULL, 2357)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172358, N'Mylene Pascual', N'Chuchai', N'Canan, Paniqui, Tarlac', N'9294333876', NULL, NULL, 2358)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172359, N'Recymay Rallos', N'Coco', N'Acocolao, Paniqui, Tarlac', N'9101192210', NULL, NULL, 2359)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172360, N'Gerlie Gamboa', N'Cassie', N'Baltazar Subd. Poblacion Norte, Paniqui, Tarlac', N'9218552216', NULL, NULL, 2360)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172361, N'Roselyn Rivera', N'Trixie', N'Moncada, Tarlac', N'9088814364', NULL, NULL, 2361)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172362, N'Santiago Calla', NULL, N'Rang- Ayan, Paniqui, Tarlac', N'9212271286', NULL, NULL, 2362)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172363, N'Hannah Esteban', N'Happy ', N'Anao, Tarlac', N'9980010063', NULL, NULL, 2363)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172364, N'Sarah Jane Dela Cruz', N'Bukch/Bella/Olaf', N'Rizal, Anao, Tarlac', N'9350548351', NULL, NULL, 2364)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172365, N'Pherlyn Palaganas', N'Cobby', N'Cayanga, Paniqui, Tarlac', N'9074571628', NULL, NULL, 2365)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172366, N'Maria Helena Evangelista', N'Storm', N'Caburnay St. Pob.Sur, Paniqui, Tarlac', N'9066400823', NULL, NULL, 2366)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172367, N'Benito Molina', N'Tobi', N'Cuyapo, Nueva Ecija', N'9176227259', NULL, NULL, 2367)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172368, N'Pia Rowena Recto', N'Bella', N'Poblacion Sur, Paniqui, Tarlac', N'9127127033', NULL, NULL, 2368)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172369, N'Cleofe Cedenio', N'Chaze', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9634386209', NULL, NULL, 2369)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172370, N'Philip John Tapaya', N'Luffy', N'Balbaloto, Victoria, Tarlac', N'09098384778/09511238689', NULL, NULL, 2370)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172371, N'Bambi Argel', N'Skittles', N'Samput, Paniqui, Tarlac', N'9498622739', NULL, NULL, 2371)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172372, N'Pacita Alejo', N'Valerie/Biden/Trump', N'Ventinilla, Paniqui, Tarlac', N'9082731517', NULL, NULL, 2372)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172373, N'Gian Carlo Lanorio', N'George/Dos', N'Abogado, Paniqui, Tarlac', N'9165633911', NULL, NULL, 2373)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172374, N'Arlester Sanchez', N'Bri', N'Estacion, Paniqui, Tarlac', N'9173048420', NULL, NULL, 2374)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172375, N'Alvin Fernando', N'Loki', N'Mabini, Moncada, Tarlac', N'9561667675', NULL, NULL, 2375)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172376, N'Jire Mae Ibay', N'Stacie', N'Carino, Paniqui, Tarlac', N'9508768924', NULL, NULL, 2376)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172377, N'Marvin Sible', N'Pey-Pey', N'Coral, Paniqui Tarlac', N'9184240193', NULL, NULL, 2377)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172378, N'Matt Rayver Lagrana', N'Astro', N'Cuyapo, Nueva Ecija', N'9062233285', NULL, NULL, 2378)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172379, N'Diona Louise Ofrecio', N'Yem-Yem', N'Coral, Paniqui, Tarlac', N'9466259385', NULL, NULL, 2379)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172380, N'Dennis Baylon', N'Bailey ', N'Poblacion Norte, Paniqui, Tarlac', N'9958563814', NULL, NULL, 2380)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172381, N'Jackielyn Tuazon', N'Snow', N'Coral, Paniqui, Tarlac', N'9615563481', NULL, NULL, 2381)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172382, N'Lerma Salonga', N'Jelai', N'Abogado, Paniqui, Tarlac', N'9218626497', NULL, NULL, 2382)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172383, N'Jerick Tolentino', N'Cindy', N'Samput, Paniqui, Tarlac', NULL, NULL, NULL, 2383)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172384, N'Ryan Tamayo', N'Bela', N'Balite, Pura, Tarlac', N'9163520010', NULL, NULL, 2384)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172385, N'Lujanna Mendoza', N'Tiny', N'Estacion, Paniqui, Tarlac', N'9190810242', NULL, NULL, 2385)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172386, N'Angelo Ang', N'Gummy', N'Carbonel, Gerona, Tarlac', N'9171568404', NULL, NULL, 2386)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172387, N'Archie Sanchez', N'Dark/Matcha', N'Estacion, Paniqui, Tarlac', N'9971688965', NULL, NULL, 2387)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172388, N'Joy Cariaga', N'Balbon', N'Carino, Paniqui, Tarlac', N'9092888250', NULL, NULL, 2388)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172389, N'Gemma Agnes Dumandan', N'Keana', N'Cuyapo, Nueva Ecija', N'9532076707', NULL, NULL, 2389)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172390, N'Christian Arfie Austria', N'Choco', N'Poblacion, Anao, Tarlac', N'9958803411', NULL, NULL, 2390)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172391, N'Cyrill Alwyn Aquino', NULL, N'Sta. Monica, Moncada, Tarlac', N'9663218710', NULL, NULL, 2391)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172392, N'Christine Jane Framil', N'Chico', N'Nagmisaan, Paniqui, Tarlac', N'9457849504', NULL, NULL, 2392)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172393, N'Alexander Dayrit', N'Bubbles', N'Del Valle Estacion, Paniqui Tarlac', N'9292673462', NULL, NULL, 2393)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172394, N'Cristine April Velasco', N'Sky', N'San Narciso, San Manuel, Tarlac', N'9154614020', NULL, NULL, 2394)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172395, N'Flora Grace Quiaoit', N'Akesha', N'Nampicuan, Nueva Ecija', N'9433080391', NULL, NULL, 2395)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172396, N'Belinda Mañalac', N'Kendra', N'Moncada, Tarlac', N'9568702288', NULL, NULL, 2396)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172397, N'Eric Balilia', N'Babsy', N'Sta. Lucia West, Moncada, Tarlac', N'9983555899', NULL, NULL, 2397)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172398, N'Mackie Andi', N'Bella/Uno', N'Balaoang, Paniqui, Tarlac', N'9301820474', NULL, NULL, 2398)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172399, N'Gary Aban', N'Stitch', N'San Manuel, Tarlac', N'9502845748', NULL, NULL, 2399)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172400, N'Julius Romar Lagman', N'Julia', N'Nampicuan, Nueva Ecija', N'9278604043', NULL, NULL, 2400)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172401, N'Car Lester Abaya', N'Zoey', N'Aringin, Moncada Tarlac', N'9106985651', NULL, NULL, 2401)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172402, N'Eddrey Shane Santos', N'Nami', N'Cayanga, Paniqui, Tarlac', N'9982215817', NULL, NULL, 2402)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172403, N'Gianella Padilla', N'Sven', N'Anao, Tarlac', N'9274368748', NULL, NULL, 2403)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172404, N'Fortunato Fernandez', NULL, N'San Jose North, Anao, Tarlac', N'9196341171', NULL, NULL, 2404)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172405, N'Sean Andrei Acasio', N'Loki', N'Poblacion 4, Moncada, Tarlac', N'9453227719', NULL, NULL, 2405)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172406, N'Liza Capones', N'Douglas', N'Colibangbang, Paniqui, Tarlac', N'9951072181', NULL, NULL, 2406)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172407, N'Adrian Paul Espolong', N'Logan', N'Dicolor, Gerona, Tarlac', N'9082696048', NULL, NULL, 2407)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172408, N'Julie Abraham', NULL, N'Bugallon St. Estacion, Paniqui, Tarlac', N'9664074029', NULL, NULL, 2408)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172409, N'Lady Preza Carbonell', N'Brandy', N'Cuyapo, Nueva Ecija', N'9973441015', NULL, NULL, 2409)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172410, N'Maria Romelyn Marquez', N'Briana ', N'Cuyapo, Nueva Ecija', N'9952376146', NULL, NULL, 2410)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172411, N'Catherine Niolar', N'Sanrio', N'Poblacion 1, Moncada, Tarlac', N'9488317930', NULL, NULL, 2411)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172412, NULL, NULL, NULL, NULL, NULL, NULL, 2412)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172413, N'Rolly Gaspar ', N'Peanut', N'Pance, Ramos, Tarlac', N'9557247419', NULL, NULL, 2413)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172414, N'Antonette Centeno', N'Pebbles', N'Sitio Balsa, San Isidro Paniqui, Tarlac', N'9635195563', NULL, NULL, 2414)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172415, N'Ryan Santiago', N'Peewee', N'Estacion, Paniqui, Tarlac', NULL, NULL, NULL, 2415)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172416, N'Francisco Caguioa Jr.', N'Yuzomi', N'Coral, Ramos, Tarlac', N'9270113280', NULL, NULL, 2416)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172417, N'Ailyn Acob', N'Gray', N'Padapada, Gerona, Tarlac', N'9287809400', NULL, NULL, 2417)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172418, N'Janrex Galatierra', N'Johnny ', N'Cabayaoasan, Paniqui, Tarlac', N'9465205408', NULL, NULL, 2418)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172419, NULL, NULL, NULL, NULL, NULL, NULL, 2419)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172420, N'Andrea Wong ', N'Ming', N'Sta Rosa, Paniqui, Tarlac', N'9278115952', NULL, NULL, 2420)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172421, N'Kwain Barlahan', N'Millow', N'Maluac, Moncada, Tarlac', N'9177333316', NULL, NULL, 2421)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172422, N'Patricia Celine Canonizado', N'Belle', N'Maluac, Moncada, Tarlac', N'9156409169', NULL, NULL, 2422)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172423, N'RJ Corpuz', N'Nitro', N'Maluac, Moncada, Tarlac', N'9177333341', NULL, NULL, 2423)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172424, N'Alex Laurente ', N'Shenzi', N'Carbonel, Gerona, Tarlac', N'9082294091', NULL, NULL, 2424)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172425, N'Brian Madamba', N'Uno', N'Carino, Paniqui, Tarlac', N'9088656192', NULL, NULL, 2425)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172426, N'Myra Rivera', N'Boomer', N'Canan, Paniqui, Tarlac', N'9178535505', NULL, NULL, 2426)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172427, N'Marijoice Lazaro', N'Muning', N'Apulid, Paniqui, Tarlac', N'9566075497', NULL, NULL, 2427)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172428, N'Kathleen Ann Abaya', N'Sachi', N'Pinasling, Gerona, Tarlac', N'9164160634', NULL, NULL, 2428)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172429, N'Jenelyn Dagdag', N'Hachiko', N'District VI, Cuyapo, Nueva Ecija', N'9217621410', NULL, NULL, 2429)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172430, N'Jacquelyn Bautista', N'Mini ', N'Nampicuan, Nueva Ecija', N'9506678153', NULL, NULL, 2430)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172431, N'Alma Flores', N'Claowe', N'Carino, Paniqui, Tarlac', N'9503533824', NULL, NULL, 2431)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172432, N'Calvin Edward Chichioco', N'Chloe', N'Estacion, Paniqui, Tarlac', N'9151525136', NULL, NULL, 2432)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172433, N'Victoria Gabon', N'Lucio', N'San Julian, Moncada, Tarlac', N'9184644209', NULL, NULL, 2433)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172434, N'Franceska Erese', N'Orenji', N'District IV, Cuyapo, Nueva Ecija', N'9455299182', NULL, NULL, 2434)
GO
INSERT [dbo].[ImportClientPatiens_Petstacular_20210228] ([Client ID], [Owner's Name], [Pet's Name], [Address], [Contact Number(s)], [Email Address], [Best time to call], [RowIndex]) VALUES (20172435, N'Stephanie Eberio', N'Spark', N'Alexannre Heights, Samput, Paniqui, Tarlac', N'9052304524', NULL, NULL, 2435)
GO


	Declare @DuplicateNames Table (ClientName varchar(MAX))
	Declare @ID_Company_Petstacular INT = 45

	INSERT @DuplicateNames 
	SELECT  LTRIM(RTRIM(ISNULL(LOWER( [Owner's Name]), ''))) 
	FROM    [ImportClientPatiens_Petstacular_20210228]
	GROUP BY LTRIM(RTRIM(ISNULL(LOWER( [Owner's Name]), ''))) 
	HAVING count(*)  > 1

	INSERT dbo.tClient (
		ID_Company,
		Code,
		Name,
		Address,
		ContactNumber,
		Email,
		IsActive,
		Comment,
		DateCreated,
		DateModified,
		ID_CreatedBy,
		ID_LastModifiedBy,
		ContactNumber2,
		tempID
	)
	SELECT @ID_Company_Petstacular,
		  Convert(DECIMAL, record.[Client ID]),
		  record.[Owner's Name], 
		  record.Address,
		  record.[Contact Number(s)],
		  record.[Email Address],
		  1,
		  NUll,
		  GEtDate(),
		  GEtDate(),
		  1,
		  1,
		  NULL,
		  COnvert(VARCHAR(400), GETDATE(), 102) + ':' + COnvert(VARCHAR(400), COnvert(DECIMAL, RowIndex))
	FROM    [ImportClientPatiens_Petstacular_20210228] record
	WHERE LTRIM(RTRIM(ISNULL(LOWER( record.[Owner's Name]), ''))) NOT IN (SELECT LTRIM(RTRIM(ISNULL(LOWER(ClientName), ''))) FROM @DuplicateNames) 
 
DECLARE @ImportClientPet TABLE(ID_Client INT, PetNames VARCHAR(MAX))

INSERT @ImportClientPet
SELECT client.ID, record.[Pet's Name]
FROM    [ImportClientPatiens_Petstacular_20210228] record LEFT JOIN tClient client 
	on COnvert(VARCHAR(400), GETDATE(), 102) + ':' + COnvert(VARCHAR(400), COnvert(DECIMAL, RowIndex)) = client.tempID
WHERE client.ID_Company = @ID_Company_Petstacular   AND record.[Pet's Name] NOT LIKE '%/%'


DECLARE @ID_Client VARCHAR(MAX)
DECLARE @PetName VARCHAR(MAX)

DECLARE db_cursor CURSOR FOR SELECT client.ID, record.[Pet's Name]
FROM    [ImportClientPatiens_Petstacular_20210228] record LEFT JOIN tClient client 
	on COnvert(VARCHAR(400), GETDATE(), 102) + ':' + COnvert(VARCHAR(400), COnvert(DECIMAL, RowIndex)) = client.tempID
WHERE client.ID_Company = @ID_Company_Petstacular   AND record.[Pet's Name] LIKE '%/%'


OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName


WHILE @@FETCH_STATUS = 0
BEGIN
 
INSERT @ImportClientPet (ID_Client, PetNames)
  SELECT
    @ID_Client
   ,Part
  FROM dbo.fGetSplitString(@PetName, '/') gss

FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName
END

CLOSE db_cursor
DEALLOCATE db_cursor


INSERT tPatient (ID_Company,
  ID_Client,
  Code,
  Name,
  Old_patient_id,
  IsNeutered,
  IsDeceased,
  Comment,
  Species,
  ID_Gender,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy,
  FirstName,
  LastName,
  MiddleName,
  Email,
  FullAddress,
  ID_Country,
  ContactNumber)
SELECT
    @ID_Company_Petstacular
  ,ID_Client
  , NULL
 ,PetNames
  ,NULL
  ,0
  ,0
  ,'Imported on 02/12/2021'
  ,''
  ,NULL,1,GETDATE(),GETDATE(),1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL
FROM @ImportClientPet
UPDATE tPatient 
SET
	Code = pCode.Code
FROM tPatient p INNER JOIN @PatientCode pCode 
ON p.ID = pCode.ID_Patient


UPDATE tDocumentSeries SET Counter = (SELECT COUNT(*) FROM @PatientCode) + 1 
WHERE 
	ID_Company = @ID_Company AND
	ID_Model = @Oid_Model

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportClientPatiens_Petstacular_20210228]') AND type in (N'U'))
DROP TABLE [dbo].[ImportClientPatiens_Petstacular_20210228]
GO

