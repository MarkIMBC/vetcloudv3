﻿if OBJECT_ID('ForImport.dbo.[JCPAWSCLAWS-Service]') is not null
  BEGIN
      DROP TABLE ForImport.dbo.[JCPAWSCLAWS-Service]
  END

GO

CREATE TABLE ForImport.[dbo].[JCPAWSCLAWS-Service]
  (
     [SERVICE NAME ] varchar(500),
     [CATEGORY]      varchar(500),
     [SERVICE FEE]   varchar(500)
  )

GO

if OBJECT_ID('ForImport.dbo.[JCPAWSCLAWS-Item]') is not null
  BEGIN
      DROP TABLE ForImport.dbo.[JCPAWSCLAWS-Item]
  END

GO

CREATE TABLE ForImport.[dbo].[JCPAWSCLAWS-Item]
  (
     [ITEM NAME ]    varchar(500),
     [CATEGORY]      varchar(500),
     [BUYING PRICE]  varchar(500),
     [SELLING PRICE] varchar(500)
  )

GO

INSERT INTO ForImport.[dbo].[JCPAWSCLAWS-Service]
            ([SERVICE NAME ],
             [CATEGORY],
             [SERVICE FEE])
SELECT 'Consultation Fee/ Check up Fee',
       'Doctor’s Fee',
       ''
UNION ALL
SELECT 'Antibiotics 1-10kgs',
       'Injectable Medications',
       '200'
UNION ALL
SELECT 'Antibiotics 11-15kgs',
       'Injectable Medications',
       '250'
UNION ALL
SELECT 'Antibiotics 16-20kgs',
       'Injectable Medications',
       '300'
UNION ALL
SELECT 'Antibiotics 21-30kgs',
       'Injectable Medications',
       '350'
UNION ALL
SELECT 'Antibiotics 31 and up',
       'Injectable Medications',
       '400'
UNION ALL
SELECT 'Vitamins 1-10kgs',
       'Injectable Medications',
       '200'
UNION ALL
SELECT 'Vitamins 11-15kgs',
       'Injectable Medications',
       '250'
UNION ALL
SELECT 'Vitamins 16-20kgs',
       'Injectable Medications',
       '300'
UNION ALL
SELECT 'Vitamins 21-30kgs',
       'Injectable Medications',
       '350'
UNION ALL
SELECT 'Vitamins 31 and up',
       'Injectable Medications',
       '400'
UNION ALL
SELECT 'Anti-Inflam 1-10kgs',
       'Injectable Medications',
       '200'
UNION ALL
SELECT 'Anti-Inflam 11-15kgs',
       'Injectable Medications',
       '250'
UNION ALL
SELECT 'Anti-Inflam 16-20kgs',
       'Injectable Medications',
       '300'
UNION ALL
SELECT 'Anti-Inflam 21-30kgs',
       'Injectable Medications',
       '350'
UNION ALL
SELECT 'Anti-Inflam 31 and up',
       'Injectable Medications',
       '400'
UNION ALL
SELECT 'Other Inj 1-10kgs',
       'Injectable Medications',
       '200'
UNION ALL
SELECT 'Other Inj 11-15kgs',
       'Injectable Medications',
       '250'
UNION ALL
SELECT 'Other Inj 16-20kgs',
       'Injectable Medications',
       '300'
UNION ALL
SELECT 'Other Inj 21-30kgs',
       'Injectable Medications',
       '350'
UNION ALL
SELECT 'Other Inj 31 and up',
       'Injectable Medications',
       '400'
UNION ALL
SELECT 'Ivermectin INJ 1-10kgs',
       'Injectable Medications',
       '200'
UNION ALL
SELECT 'Ivermectin INJ 11-15kgs',
       'Injectable Medications',
       '250'
UNION ALL
SELECT 'Ivermectin INJ 16-20kgs',
       'Injectable Medications',
       '300'
UNION ALL
SELECT 'Ivermectin INJ 21-30kgs',
       'Injectable Medications',
       '350'
UNION ALL
SELECT 'Ivermectin INJ 26-30kgs',
       'Injectable Medications',
       '400'
UNION ALL
SELECT 'Ivermectin INJ 31 and up',
       'Injectable Medications',
       '450'
UNION ALL
SELECT 'CBC',
       'LABORATORIES',
       '500'
UNION ALL
SELECT 'Blood Chem',
       'LABORATORIES',
       '2000'
UNION ALL
SELECT 'Fecalysis',
       'LABORATORIES',
       '150'
UNION ALL
SELECT 'Skin Scraping',
       'LABORATORIES',
       '250'
UNION ALL
SELECT 'Vaginal Smear',
       'LABORATORIES',
       '300'
UNION ALL
SELECT 'Ear Swabbing',
       'LABORATORIES',
       '150'
UNION ALL
SELECT 'NAIL CLIPPING Small Breed',
       'Services',
       '50'
UNION ALL
SELECT 'NAIL CLIPPING Medium Breed',
       'Services',
       '75'
UNION ALL
SELECT 'NAIL CLIPPING Large Breed',
       'Services',
       '100'
UNION ALL
SELECT 'Ear Cleaning',
       'Services',
       '100'
UNION ALL
SELECT 'Ear Cleaning w/ Otitis',
       'Services',
       '200'
UNION ALL
SELECT 'IV Insertion',
       'Services',
       '500'
UNION ALL
SELECT 'IV Insertion w/ vit',
       'Services',
       '600'
UNION ALL
SELECT 'CONFINEMENT per day',
       'Confinement (Package)',
       ''
UNION ALL
SELECT 'Boarding Fee',
       'Boarding Services',
       ''
UNION ALL
SELECT 'Tarsorrhapy Dog 1-5kgs',
       'Surgery',
       '2000'
UNION ALL
SELECT 'Tarsorrhapy Dog 6-10kgs',
       'Surgery',
       '2500'
UNION ALL
SELECT 'Tarsorrhapy Dog 11-15kgs',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Tarsorrhapy Dog 16 and up',
       'Surgery',
       '3500'
UNION ALL
SELECT 'Tarsorrhapy Cat',
       'Surgery',
       '1000'
UNION ALL
SELECT 'Enucleation Dog 1-5kgs',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Enucleation Dog 6-10kgs',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Enucleation Dog 11-15kgs',
       'Surgery',
       '6500'
UNION ALL
SELECT 'Enucleation Dog 16 ↑',
       'Surgery',
       '8000'
UNION ALL
SELECT 'Enucleation Cat',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Castration Dog 1-5kgs',
       'Castrations',
       '2500'
UNION ALL
SELECT 'Castration Dog 6-10kgs',
       'Castrations',
       '3000'
UNION ALL
SELECT 'Castration Dog 11-15kgs',
       'Castrations',
       '3500'
UNION ALL
SELECT 'Castration Dog 16-20kgs',
       'Castrations',
       '4000'
UNION ALL
SELECT 'Castration Cat',
       'Castrations',
       '1000'
UNION ALL
SELECT 'Hysterectomy / OVH Dog 1-5kgs',
       'Surgery',
       '3500'
UNION ALL
SELECT 'Hysterectomy / OVH Dog 6-10kgs',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Hysterectomy / OVH Dog 11-15kgs',
       'Surgery',
       '4500'
UNION ALL
SELECT 'Hysterectomy / OVH Dog 16 and up',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Hysterectomy / OVH Cat',
       'Surgery',
       '2500'
UNION ALL
SELECT 'Caesarean Small Breed 1-5kgs',
       'Surgery',
       '7000'
UNION ALL
SELECT 'Caesarean Small Breed 6-10kgs',
       'Surgery',
       '8000'
UNION ALL
SELECT 'Caesarean Medium Breed 11-15kgs',
       'Surgery',
       '10000'
UNION ALL
SELECT 'Caesarean Large Breed 16 -20kgs',
       'Surgery',
       '12000'
UNION ALL
SELECT 'Caesarean Large Breed 21 and up',
       'Surgery',
       '15000'
UNION ALL
SELECT 'Whelping Assistance Small Breed DOG',
       'Vet Services',
       '2000'
UNION ALL
SELECT 'Whelping Assistance Medium Breed DOG',
       'Vet Services',
       '3000'
UNION ALL
SELECT 'Whelping Assistance Large Breed DOG',
       'Vet Services',
       '4000'
UNION ALL
SELECT 'Whelping Assistance CAT',
       'Vet Services',
       '2000'
UNION ALL
SELECT 'Cystotomy 1-10kgs',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Cystotomy 11-15kgs',
       'Surgery',
       '6000'
UNION ALL
SELECT 'Cystotomy 16-20kgs',
       'Surgery',
       '7000'
UNION ALL
SELECT 'Cystotomy 21 ↑',
       'Surgery',
       '8000'
UNION ALL
SELECT 'Ear Cropping 3-4 kgs',
       'Surgery',
       '2500'
UNION ALL
SELECT 'Ear Cropping 4-5 kgs',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Ear Cropping 5-6 kgs',
       'Surgery',
       '3500'
UNION ALL
SELECT 'Ear Cropping 6-7 kgs',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Ear Cropping 7-8 kgs',
       'Surgery',
       '4500'
UNION ALL
SELECT 'Ear Cropping 8-9 kgs',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Ear Cropping 9-10 kgs',
       'Surgery',
       '5500'
UNION ALL
SELECT 'Ear Cropping 10- 12 kgs',
       'Surgery',
       '6700'
UNION ALL
SELECT 'Ear Cropping 12-15 kgs',
       'Surgery',
       '7500'
UNION ALL
SELECT 'Explore Lap 1-10kgs DOG',
       'Surgery',
       '6000'
UNION ALL
SELECT 'Explore Lap 11-15kgs DOG',
       'Surgery',
       '7000'
UNION ALL
SELECT 'Explore Lap 16 ↑ DOG',
       'Surgery',
       '8000'
UNION ALL
SELECT 'Explore Lap CAT',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Pyometra 1-5kgs DOG',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Pyometra 6-10kgs DOG',
       'Surgery',
       '6000'
UNION ALL
SELECT 'Pyometra 11-15kgs DOG',
       'Surgery',
       '8000'
UNION ALL
SELECT 'Pyometra 16-20kgs DOG',
       'Surgery',
       '10000'
UNION ALL
SELECT 'Pyometra 21 and up DOG',
       'Surgery',
       '12500'
UNION ALL
SELECT 'Pyometra CAT',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Umbilical Hernia Repair 1-5kgs DOG',
       'Surgery',
       '2000'
UNION ALL
SELECT 'Umbilical Hernia Repair 6-10kgs DOG',
       'Surgery',
       '3500'
UNION ALL
SELECT 'Umbilical Hernia Repair 11-15kgs DOG',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Umbilical Hernia Repair 16 and up DOG',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Umbilical Hernia Repair 1-5kgs CAT',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Umbilical Hernia Repair 6-10kgs CAT',
       'Surgery',
       '3500'
UNION ALL
SELECT 'Umbilical Hernia Repair 11-15kgs CAT',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Umbilical Hernia Repair 16 and up CAT',
       'Surgery',
       '4500'
UNION ALL
SELECT 'Wound Repair 1-5kgs DOG',
       'Surgery',
       '2000'
UNION ALL
SELECT 'Wound Repair 6-10kgs DOG',
       'Surgery',
       '2500'
UNION ALL
SELECT 'Wound Repair 11-15kgs DOG',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Wond Repair 16 and up DOG',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Wound Repair CAT',
       'Surgery',
       '1500'
UNION ALL
SELECT 'Tail Docking Less than 1  week',
       'Surgery',
       '150'
UNION ALL
SELECT 'Tail Docking 2 weeks old - 1 month',
       'Surgery',
       '300'
UNION ALL
SELECT 'Tail Docking up to 10 kgs',
       'Surgery',
       '1500'
UNION ALL
SELECT 'Tail Docking 11 and up',
       'Surgery',
       '2000'
UNION ALL
SELECT 'Euthanasia 1-5kgs',
       'Euthanasia',
       '800'
UNION ALL
SELECT 'Euthanasia 6-10kgs',
       'Euthanasia',
       '1200'
UNION ALL
SELECT 'Euthanasia 11-15kgs',
       'Euthanasia',
       '1600'
UNION ALL
SELECT 'Euthanasia 16 and up',
       'Euthanasia',
       '2000'
UNION ALL
SELECT 'Euthanasia for Cat',
       'Euthanasia',
       '600'
UNION ALL
SELECT 'Cherry Eye 1-10 Kgs DOG',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Cherry Eye 11-15 kgs DOG',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Cherry Eye 16 ↑ DOG',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Auricular Hematoma 1-5kgs',
       'Surgery',
       '3000'
UNION ALL
SELECT 'Auricular Hematoma 6-10kgs',
       'Surgery',
       '4000'
UNION ALL
SELECT 'Auricular Hematoma 11-15kgs',
       'Surgery',
       '5000'
UNION ALL
SELECT 'Auricular Hematoma 16 ↑',
       'Surgery',
       '6500'
UNION ALL
SELECT 'Auricular Hematoma for Cats',
       'Surgery',
       '2000'
UNION ALL
SELECT 'Wound Dressing Small Breed DOG',
       'Surgery',
       '300'
UNION ALL
SELECT 'Wound Dressing Medium Breed DOG',
       'Surgery',
       '400'
UNION ALL
SELECT 'Wound Dressing Large Breed DOG',
       'Surgery',
       '500'
UNION ALL
SELECT 'Wound Dressing for Cats',
       'Surgery',
       '200'
UNION ALL
SELECT 'Dental Surgery',
       'Surgery',
       '0'
UNION ALL
SELECT 'Dental Cleaning',
       'Surgery',
       '0'

GO

INSERT INTO ForImport.[dbo].[JCPAWSCLAWS-Item]
            ([ITEM NAME ],
             [CATEGORY],
             [BUYING PRICE],
             [SELLING PRICE])
SELECT 'Nexgard 2-4',
       'Anti-Tick & Flea',
       '408',
       '550'
UNION ALL
SELECT 'Nexgard 4-10',
       'Anti-Tick & Flea',
       '431',
       '650'
UNION ALL
SELECT 'Nexgard 10-25',
       'Anti-Tick & Flea',
       '450',
       '750'
UNION ALL
SELECT 'Nexard 25-50',
       'Anti-Tick & Flea',
       '484',
       '850'
UNION ALL
SELECT 'Nexgard Spectra 2-3.5',
       'Anti-Tick & Flea',
       '492',
       '650'
UNION ALL
SELECT 'Nexgard Spectra 3.5-7.5',
       'Anti-Tick & Flea',
       '557',
       '750'
UNION ALL
SELECT 'Nexgard Spectra 7.5-15',
       'Anti-Tick & Flea',
       '610',
       '850'
UNION ALL
SELECT 'Nexgard Spectra 15-30',
       'Anti-Tick & Flea',
       '670',
       '950'
UNION ALL
SELECT 'Nexgard Spectra 30-60',
       'Anti-Tick & Flea',
       '735',
       '1050'
UNION ALL
SELECT 'Simparica TRIO 2-5kg',
       'Anti-Tick & Flea',
       '569',
       '750'
UNION ALL
SELECT 'Simparica TRIO 5-10',
       'Anti-Tick & Flea',
       '622',
       '850'
UNION ALL
SELECT 'Simparica TRIO 10-20',
       'Anti-Tick & Flea',
       '639',
       '950'
UNION ALL
SELECT 'Simparica TRIO 20-40',
       'Anti-Tick & Flea',
       '700',
       '1050'
UNION ALL
SELECT 'Frontline 0-10',
       'Anti-Tick & Flea',
       '330',
       '500'
UNION ALL
SELECT 'Frontline 10-20',
       'Anti-Tick & Flea',
       '360',
       '600'
UNION ALL
SELECT 'Frontline Cats',
       'Anti-Tick & Flea',
       '327',
       '500'
UNION ALL
SELECT 'Advocate Cats',
       'Anti-Tick & Flea',
       '353',
       '550'
UNION ALL
SELECT 'Bravecto 2-4.5',
       'Anti-Tick & Flea',
       '1035',
       '1250'
UNION ALL
SELECT 'Bravecto 4.5-10',
       'Anti-Tick & Flea',
       '1035',
       '1300'
UNION ALL
SELECT 'Bravecto 10-20',
       'Anti-Tick & Flea',
       '1035',
       '1350'
UNION ALL
SELECT 'Bravecto 20-40',
       'Anti-Tick & Flea',
       '1035',
       '1400'
UNION ALL
SELECT 'Coatshine 120ml',
       'Drug/Medicine',
       '252',
       '380'
UNION ALL
SELECT 'Coatshine 60ml',
       'Drug/Medicine',
       '158',
       '250'
UNION ALL
SELECT 'Colymoxyn',
       'Drug/Medicine',
       '135',
       '300'
UNION ALL
SELECT 'Deltacal',
       'Drug/Medicine',
       '181',
       '450/10'
UNION ALL
SELECT 'Hemacare FE',
       'Drug/Medicine',
       '185',
       '350'
UNION ALL
SELECT 'Hemacare FE',
       'Drug/Medicine',
       '122',
       '250'
UNION ALL
SELECT 'LC Dox Tablet',
       'Drug/Medicine',
       '810',
       '1900'
UNION ALL
SELECT 'Methiovet',
       'Drug/Medicine',
       '530',
       '850'
UNION ALL
SELECT 'LC Vit 120ml',
       'Drug/Medicine',
       '120',
       '250'
UNION ALL
SELECT 'LC Vit 60ml',
       'Drug/Medicine',
       '72',
       '180'
UNION ALL
SELECT 'Livotine 120ml',
       'Drug/Medicine',
       '142',
       '300'
UNION ALL
SELECT 'Livotine 60ml',
       'Drug/Medicine',
       '133.3',
       '250'
UNION ALL
SELECT 'Nutrical 120ml',
       'Drug/Medicine',
       '141',
       '300'
UNION ALL
SELECT 'Nutrical 60ml',
       'Drug/Medicine',
       '94',
       '180'
UNION ALL
SELECT 'Pet Tabs',
       'Drug/Medicine',
       '1350',
       '3500'
UNION ALL
SELECT 'Refamol',
       'Drug/Medicine',
       '285 (10)',
       '400/20'
UNION ALL
SELECT 'LC Dox 60ML',
       'Drug/Medicine',
       '125',
       '250'
UNION ALL
SELECT 'LC Dox 120 ml',
       'Drug/Medicine',
       '173',
       '350'
UNION ALL
SELECT 'Petto AI- Travel Crates #1',
       'Others',
       '398',
       '1000'
UNION ALL
SELECT 'Petto AI- Travel Crates #2',
       'Others',
       '733',
       '1500'
UNION ALL
SELECT 'Petto AI- Travel Crates #3',
       'Others',
       '1309',
       '2500'
UNION ALL
SELECT 'Petto AI- Travel Crates #4',
       'Others',
       '1937',
       '3000'
UNION ALL
SELECT 'Petto AI- Collapsible Cage #1',
       'Others',
       '542',
       '1000'
UNION ALL
SELECT 'Petto AI- Collapsible Cage #2',
       'Others',
       '1029',
       '1500'
UNION ALL
SELECT 'Petto AI- Collapsible Cage #3',
       'Others',
       '',
       '2000'
UNION ALL
SELECT 'Petto AI- Collapsible Cage #4',
       'Others',
       '1762',
       '2500'
UNION ALL
SELECT 'Pet Crates (Collapsible Cage) #1',
       'Others',
       '520',
       '1000'
UNION ALL
SELECT 'Pet Crates (Collapsible Cage) #2',
       'Others',
       '',
       '1500'
UNION ALL
SELECT 'Pet Crates (Collapsible Cage) #3',
       'Others',
       '1120',
       '2000'
UNION ALL
SELECT 'Pet Crates (Collapsible Cage) #4',
       'Others',
       '1530',
       '2500'
UNION ALL
SELECT 'Ordinary Collapsible Cage  Medium',
       'Others',
       '390',
       '800'
UNION ALL
SELECT 'Ordinary Collapsible Cage  Large',
       'Others',
       '560',
       '1000'
UNION ALL
SELECT 'Ordinary Collapsible Cage  XL',
       'Others',
       '695',
       '1200'
UNION ALL
SELECT 'Ordinary Collapsible Cage  XXL',
       'Others',
       '870',
       '1500'
UNION ALL
SELECT 'Alluspray',
       'Medical Supply',
       '584',
       '800'
UNION ALL
SELECT 'Canicee',
       'Medical Supply',
       '52',
       '250'
UNION ALL
SELECT 'Canicef',
       'Medical Supply',
       '108',
       '350'
UNION ALL
SELECT 'Canitrim',
       'Medical Supply',
       '60',
       '300'
UNION ALL
SELECT 'Canixetin',
       'Medical Supply',
       '78',
       '300'
UNION ALL
SELECT 'Omega Sea Plus',
       'Medical Supply',
       '175',
       '400'
UNION ALL
SELECT 'Optazol',
       'Medical Supply',
       '152',
       '400'
UNION ALL
SELECT 'Canibrom',
       'Medical Supply',
       '64',
       '250'
UNION ALL
SELECT 'Bioline Ear Care',
       'Others',
       '106',
       '250'
UNION ALL
SELECT 'Bioline Tearstain Remover',
       'Others',
       '106',
       '250'
UNION ALL
SELECT 'Boley Water Feeder',
       'Others',
       '45',
       '150'
UNION ALL
SELECT 'Cat Harness Fish',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Cat Harness Maong Friend',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Cat Harness Nylon Dot',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Cat Harness Sport',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Cat Litter Box Enclosed',
       'Others',
       '1355',
       '2000'
UNION ALL
SELECT 'Cat Scratcher',
       'Others',
       '123',
       '200'
UNION ALL
SELECT 'Collar & Harness Leash Cotton Handle 1.5 (CP918NNCHL-1.5-1182)',
       'Others',
       '121',
       '250'
UNION ALL
SELECT 'Collar & Harness Leash Cotton Handle 2.0 (CP918NNCHL-2.0-1453)',
       'Others',
       '148',
       '350'
UNION ALL
SELECT 'Collar & Harness Leash Cotton Handle 2.5 (CP918NNCHL-2.5-1684)',
       'Others',
       '171',
       '450'
UNION ALL
SELECT 'Detangler (CPDCL-1121)',
       'Others',
       '115',
       '250'
UNION ALL
SELECT 'Detangler (CPDCS-991)',
       'Others',
       '138',
       '300'
UNION ALL
SELECT 'Flat Bone',
       'Others',
       '41',
       '100'
UNION ALL
SELECT 'Green Spik Bone',
       'Others',
       '68',
       '150'
UNION ALL
SELECT 'Harness Padded Stripe 3.0 (PSH30-1201)',
       'Others',
       '123',
       '250'
UNION ALL
SELECT 'Harness Padded Stripe 4.0 (PSH40-1550)',
       'Others',
       '158',
       '300'
UNION ALL
SELECT 'Nail Clipper (5020-431)',
       'Others',
       '46',
       '100'
UNION ALL
SELECT 'Nail Clipper (A19-581)',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Nail Clipper (A-22-581)',
       'Others',
       '61',
       '150'
UNION ALL
SELECT 'Nail Clipper (RMC-1021)',
       'Others',
       '63',
       '200'
UNION ALL
SELECT 'Portable Transparent Travel Bottle 245ml',
       'Others',
       '48',
       '250'
UNION ALL
SELECT 'Portable Transparent Travel Bottle 450ml',
       'Others',
       '61',
       '350'
UNION ALL
SELECT 'Puppy Saucer L ( SRB-360L-2522)',
       'Others',
       '258',
       '450'
UNION ALL
SELECT 'Puppy Saucer M (SRB-359M-1851)',
       'Others',
       '188',
       '380'
UNION ALL
SELECT 'Puppy Saucer S (SRB-358S-1490)',
       'Others',
       '152',
       '300'
UNION ALL
SELECT 'Rope Toy (CPRT-002-781)',
       'Others',
       '81',
       '170'
UNION ALL
SELECT 'Rope Toy (CPRT-117-704)',
       'Others',
       '73',
       '150'
UNION ALL
SELECT 'Rope Toy (CPRT-131)',
       'Others',
       '71',
       '150'
UNION ALL
SELECT 'Rope Toy (CPRT-133-981)',
       'Others',
       '',
       '200'
UNION ALL
SELECT 'Square Water Feeder',
       'Others',
       '91',
       '250'
UNION ALL
SELECT 'Squeeky Toy Burger',
       'Others',
       '28',
       '70'
UNION ALL
SELECT 'Squeeky Toy Dumbell w/ Paws and Bone',
       'Others',
       '24',
       '70'
UNION ALL
SELECT 'Stainless Bowl L (FD355-1153)',
       'Others',
       '118',
       '250'
UNION ALL
SELECT 'Stainless Bowl M (FD354-853)',
       'Others',
       '88',
       '200'
UNION ALL
SELECT 'Stainless Bowl S (FD353-653)',
       'Others',
       '68',
       '150'
UNION ALL
SELECT 'Stainless Bowl XL (F356)',
       'Others',
       '148',
       '300'
UNION ALL
SELECT 'Stainless Bowl XS (FD352-455)',
       'Others',
       '48',
       '100'
UNION ALL
SELECT 'Stainless Bowl XXL (FD 357-1636)',
       'Others',
       '266',
       '400'
UNION ALL
SELECT 'Three Ring',
       'Others',
       '91',
       '180'
UNION ALL
SELECT 'Vest Leash Ribbon L (ILRVL-1050)',
       'Others',
       '108',
       '300'
UNION ALL
SELECT 'Vest Leash Ribbon M (ILRVL-950)',
       'Others',
       '98',
       '250'
UNION ALL
SELECT 'Vest Leash Ribbon S (ILRVL-860)',
       'Others',
       '89',
       '200'
UNION ALL
SELECT 'RMC 1057',
       'Others',
       '65',
       '250'
UNION ALL
SELECT 'Beef Pro per Bag',
       'Pet Food',
       '3152',
       '3450'
UNION ALL
SELECT 'Beef Pro per KG',
       'Pet Food',
       '139',
       '155'
UNION ALL
SELECT 'SDN 15kgs per BAG',
       'Pet Food',
       '2550',
       '3300'
UNION ALL
SELECT 'SDN 15kgs per KG',
       'Pet Food',
       '170',
       '225'
UNION ALL
SELECT 'SDN 5Kgs',
       'Pet Food',
       '857',
       '1195'
UNION ALL
SELECT 'Pedigree P. Per Bag',
       'Pet Food',
       '1991',
       '2250'
UNION ALL
SELECT 'Pedigree P. Per KG',
       'Pet Food',
       '133',
       '150'
UNION ALL
SELECT 'Pedigree A.',
       'Pet Food',
       '',
       ''
UNION ALL
SELECT 'Nutrichunks P. 20Kgs per BAG',
       'Pet Food',
       '2599',
       '2750'
UNION ALL
SELECT 'Nutrichunks P. 20Kgs per KG',
       'Pet Food',
       '125',
       '140'
UNION ALL
SELECT 'Nutrichunks P. 10kgs',
       'Pet Food',
       '1250',
       '1375'
UNION ALL
SELECT 'Nutrichunks A. 20Kgs per BAG',
       'Pet Food',
       '1771',
       '2245'
UNION ALL
SELECT 'Nutrichunks A. 20Kgs per KG',
       'Pet Food',
       '89',
       '115'
UNION ALL
SELECT 'Nutrichunks L. 20Kgs per BAG',
       'Pet Food',
       '2135',
       '2345'
UNION ALL
SELECT 'Nutrichunks L. 20Kgs per KG',
       'Pet Food',
       '107',
       '120'
UNION ALL
SELECT 'Nutrichunks L 10Kgs',
       'Pet Food',
       '1068',
       '1175'
UNION ALL
SELECT 'Nutrichunks Coatshine 20kgs per BAG',
       'Pet Food',
       '2499',
       '2750'
UNION ALL
SELECT 'Nutrichunks Coatshine 20kgs per KG',
       'Pet Food',
       '125',
       '140'
UNION ALL
SELECT 'Special Dog A. Per BAG',
       'Pet Food',
       '903',
       '1150'
UNION ALL
SELECT 'Special Dog A. Per KG',
       'Pet Food',
       '101',
       '130'
UNION ALL
SELECT 'Special Dog P. Per BAG',
       'Pet Food',
       '945',
       '1250'
UNION ALL
SELECT 'Special Dog P. Per KG',
       'Pet Food',
       '105',
       '140'
UNION ALL
SELECT 'Top Breed A. Per BAG',
       'Pet Food',
       '1210',
       '1480'
UNION ALL
SELECT 'Top Breed A. Per KG',
       'Pet Food',
       '61',
       '75'
UNION ALL
SELECT 'Top Breed P. Per BAG',
       'Pet Food',
       '1515',
       '1700'
UNION ALL
SELECT 'Top Breed P. Per KG',
       'Pet Food',
       '76',
       '90'
UNION ALL
SELECT 'Special Cat Food per BAG',
       'Pet Food',
       '735',
       '920'
UNION ALL
SELECT 'Special Cat Food per KG',
       'Pet Food',
       '105',
       '135'
UNION ALL
SELECT 'Whiskas 7kgs per BAG',
       'Pet Food',
       '937.25',
       ''
UNION ALL
SELECT 'Whiskas 7kgs per KG',
       'Pet Food',
       '134',
       '150'
UNION ALL
SELECT 'Princess Cat 22.73kg per BAG',
       'Pet Food',
       '2370',
       '2900'
UNION ALL
SELECT 'Princess Cat 22.73kg per KG',
       'Pet Food',
       '105',
       '130'
UNION ALL
SELECT 'Power Cat Adult per BAG',
       'Pet Food',
       '1050',
       '1280'
UNION ALL
SELECT 'Power Cat Adult per KG',
       'Pet Food',
       '131.25',
       '160'
UNION ALL
SELECT 'Power Dog Can Adult',
       'Pet Food',
       '63',
       '95'
UNION ALL
SELECT 'Power Dog Can Puppy',
       'Pet Food',
       '68',
       '100'
UNION ALL
SELECT 'Pedigree Pouch Puppy',
       'Pet Food',
       '36',
       '45'
UNION ALL
SELECT 'Pedigree Pouch Adult',
       'Pet Food',
       '33',
       '40'
UNION ALL
SELECT 'Special Dog Can A',
       'Pet Food',
       '55',
       '85'
UNION ALL
SELECT 'Special Dog Can P',
       'Pet Food',
       '55',
       '85'
UNION ALL
SELECT 'Special Cat Can',
       'Pet Food',
       '65',
       '95'
UNION ALL
SELECT 'Whiskas Pouch jr. Mackerel',
       'Pet Food',
       '29',
       '45'
UNION ALL
SELECT 'Whiskas Pouch jr. Tuna',
       'Pet Food',
       '29',
       '45'
UNION ALL
SELECT 'Whiskas Pouch Tuna Adult',
       'Pet Food',
       '28',
       '40'
UNION ALL
SELECT 'Pedigree 400 g Adult',
       'Pet Food',
       '81',
       '100'
UNION ALL
SELECT 'Pedigree 400 g 700g',
       'Pet Food',
       '121',
       '200'
UNION ALL
SELECT 'Pedigree 400 g 1.15kg',
       'Pet Food',
       '178',
       '300'
UNION ALL
SELECT 'Pedigree 400 g Puppy',
       'Pet Food',
       '92.6',
       '100'
UNION ALL
SELECT 'Emerflox',
       'Medical Supply',
       '142',
       '400'
UNION ALL
SELECT 'Emerplex',
       'Medical Supply',
       '152',
       '350'
UNION ALL
SELECT 'Emervit',
       'Medical Supply',
       '104',
       '300'
UNION ALL
SELECT 'Enfaboost',
       'Medical Supply',
       '180',
       '400'
UNION ALL
SELECT 'Hepatosure',
       'Medical Supply',
       '165',
       '400'
UNION ALL
SELECT 'Prefolic Cee',
       'Medical Supply',
       '170',
       '400'
UNION ALL
SELECT 'Wormexx',
       'Medical Supply',
       '15',
       '100'
UNION ALL
SELECT 'Ocoxin',
       'Medical Supply',
       '660',
       '850'
UNION ALL
SELECT 'Folrex',
       'Medical Supply',
       '575',
       '750'
UNION ALL
SELECT 'Viusid',
       'Medical Supply',
       '650',
       '850'
UNION ALL
SELECT 'Furfect Biosulfur MDC Shampoo',
       'Medical Supply',
       '300',
       '500'
UNION ALL
SELECT 'Furfect Biosulfur MDC Soap',
       'Medical Supply',
       '110',
       '170'
UNION ALL
SELECT 'Gentin',
       'Eye & Ear Drops',
       '450',
       '286'
UNION ALL
SELECT 'Tobramycin',
       'Eye & Ear Drops',
       '350',
       '110'
UNION ALL
SELECT 'Just Tears',
       'Eye & Ear Drops',
       '350',
       '196'
UNION ALL
SELECT 'Otocin',
       'Eye & Ear Drops',
       '350',
       '196'
UNION ALL
SELECT 'Serum',
       'Eye & Ear Drops',
       '350',
       '100'
UNION ALL
SELECT 'Biover',
       'Treats',
       '285',
       '500'
UNION ALL
SELECT 'Cuddle',
       'Treats',
       '60',
       '75'
UNION ALL
SELECT 'Doxypet 100mg',
       'Treats',
       '17',
       '30'
UNION ALL
SELECT 'Doxypet 200mg',
       'Treats',
       '22',
       '35'
UNION ALL
SELECT 'Doxytic Syrup',
       'Treats',
       '300',
       '550'
UNION ALL
SELECT 'Emepet',
       'Treats',
       '350',
       '550'
UNION ALL
SELECT 'Gen Pet Shampoo',
       'Shampoo and Soap',
       '260',
       '350'
UNION ALL
SELECT 'Gen Pet Shampoo VCO',
       'Shampoo and Soap',
       '210',
       '350'
UNION ALL
SELECT 'Gen Pet Soap',
       'Shampoo and Soap',
       '100',
       '150'
UNION ALL
SELECT 'Imidocarb',
       'Treats',
       '650 (150/ml)',
       '300(min.)'
UNION ALL
SELECT 'Immuno Active',
       'Treats',
       '340',
       '500'
UNION ALL
SELECT 'Kiwof Puppy',
       'Treats',
       '215',
       '15'
UNION ALL
SELECT 'Kiwof Small',
       'Treats',
       '85',
       '200'
UNION ALL
SELECT 'Kiwof XL',
       'Treats',
       '225',
       '600'
UNION ALL
SELECT 'Metaflam 1.5mg',
       'Treats',
       '340',
       '550'
UNION ALL
SELECT 'Nepro',
       'Treats',
       '220',
       '480'
UNION ALL
SELECT 'Nerveplex',
       'Treats',
       '285',
       '500'
UNION ALL
SELECT 'Opthacure',
       'Treats',
       '195',
       '400'
UNION ALL
SELECT 'Propyrin',
       'Treats',
       '250',
       '400'
UNION ALL
SELECT 'Proticure',
       'Treats',
       '195',
       '400'
UNION ALL
SELECT 'Rkleen',
       'Treats',
       '800',
       '1200'
UNION ALL
SELECT 'Thrombeat',
       'Tick & Flea Prevention',
       '690',
       '1000'
UNION ALL
SELECT 'XYLAZINE 30ml',
       'Injectible',
       '1200',
       '50'
UNION ALL
SELECT 'Laktrazine',
       'Drug/Medicine',
       '165',
       '450'
UNION ALL
SELECT 'Neurovet',
       'Drug/Medicine',
       '300',
       '500'
UNION ALL
SELECT 'Prazinate 15ML',
       'Drug/Medicine',
       '94',
       '200'
UNION ALL
SELECT 'Prazinate 60ml',
       'Drug/Medicine',
       '263',
       '500'
UNION ALL
SELECT 'Pulmoquin',
       'Drug/Medicine',
       '98',
       '300'
UNION ALL
SELECT 'Sorvit',
       'Drug/Medicine',
       '139',
       '300'
UNION ALL
SELECT 'Miconate',
       'Drug/Medicine',
       '285',
       '500'
UNION ALL
SELECT 'Laktrazine per piece',
       'Drug/Medicine',
       '8.25',
       '25'
UNION ALL
SELECT 'Neurovet per piece',
       'Drug/Medicine',
       '15',
       '30'
UNION ALL
SELECT 'Allergy Ease',
       'Pet Supplies',
       '754.8',
       '1400/25'
UNION ALL
SELECT 'Anti Pruritic Spray',
       'Pet Supplies',
       '404.8',
       '700'
UNION ALL
SELECT 'Anti-Coprophagic',
       'Pet Supplies',
       '564.8',
       ''
UNION ALL
SELECT 'Bladder Control',
       'Pet Supplies',
       '796.8',
       '1500'
UNION ALL
SELECT 'Breweres Yeast',
       'Pet Supplies',
       '483.1',
       '850'
UNION ALL
SELECT 'Def- Puppy & Miniature',
       'Pet Supplies',
       '991.3',
       '1500'
UNION ALL
SELECT 'Ear Cleanse Liquid',
       'Pet Supplies',
       '485',
       '700'
UNION ALL
SELECT 'Eye Rinse Liquid',
       'Pet Supplies',
       '518',
       '700'
UNION ALL
SELECT 'Healthy Coat',
       'Pet Supplies',
       '508.3',
       '1000'
UNION ALL
SELECT 'Healthy Liver Chewables',
       'Pet Supplies',
       '677.52',
       '1300'
UNION ALL
SELECT 'Healthy Vision',
       'Pet Supplies',
       '752.8',
       '1500'
UNION ALL
SELECT 'Immune Health',
       'Pet Supplies',
       '544.32',
       '1000'
UNION ALL
SELECT 'K-9 Calcium Chewables',
       'Pet Supplies',
       '439',
       '800'
UNION ALL
SELECT 'Probiotics Capsules',
       'Pet Supplies',
       '1199',
       '2000'
UNION ALL
SELECT 'Vetpramide',
       'Pet Supplies',
       '484',
       '850'
UNION ALL
SELECT 'Wormrid Tablet',
       'Pet Supplies',
       '35',
       '200'
UNION ALL
SELECT 'Allergy Ease per piece',
       'Pet Supplies',
       '12.6',
       '25'
UNION ALL
SELECT 'Anti-Coprophagic per piece',
       'Pet Supplies',
       '9.4',
       '20'
UNION ALL
SELECT 'Bladder Control per piece',
       'Pet Supplies',
       '13.3',
       '25'
UNION ALL
SELECT 'Breweres Yeast per piece',
       'Pet Supplies',
       '16',
       '20'
UNION ALL
SELECT 'Def- Puppy & Miniature per piece',
       'Pet Supplies',
       '331',
       '500'
UNION ALL
SELECT 'Healthy Coat per piece',
       'Pet Supplies',
       '8.5',
       '20'
UNION ALL
SELECT 'Healthy Liver Chewables per piece',
       'Pet Supplies',
       '11.3',
       '25'
UNION ALL
SELECT 'Healthy Vision per piece',
       'Pet Supplies',
       '12.5',
       '25'
UNION ALL
SELECT 'Immune Health per piece',
       'Pet Supplies',
       '4.5',
       '20'
UNION ALL
SELECT 'K-9 Calcium Chewables per piece',
       'Pet Supplies',
       '4.4',
       '20'
UNION ALL
SELECT 'Probiotics Capsules per piece',
       'Pet Supplies',
       '12',
       '25'
UNION ALL
SELECT 'Vetpramide per piece',
       'Pet Supplies',
       '4',
       '20'
UNION ALL
SELECT 'Britcare recovery',
       'Drug/Medicine',
       '200',
       '300'
UNION ALL
SELECT 'Co- Amox 625mg',
       'Drug/Medicine',
       '16',
       '35'
UNION ALL
SELECT 'Co- Amox Syrup',
       'Drug/Medicine',
       '124',
       '250'
UNION ALL
SELECT 'Detick Plus',
       'Drug/Medicine',
       '55',
       '120'
UNION ALL
SELECT 'Forza',
       'Drug/Medicine',
       '350',
       '230'
UNION ALL
SELECT 'GIT',
       'Drug/Medicine',
       '190',
       '260'
UNION ALL
SELECT 'Immunol',
       'Drug/Medicine',
       '230',
       '700'
UNION ALL
SELECT 'Micoderm',
       'Drug/Medicine',
       '126',
       '300'
UNION ALL
SELECT 'Microzole',
       'Drug/Medicine',
       '139',
       '250'
UNION ALL
SELECT 'Mondex Big',
       'Drug/Medicine',
       '96',
       '180'
UNION ALL
SELECT 'Mondex Small',
       'Drug/Medicine',
       '50',
       '100'
UNION ALL
SELECT 'Mycocide',
       'Drug/Medicine',
       '260',
       '500'
UNION ALL
SELECT 'Natrakelp',
       'Drug/Medicine',
       '215',
       '400'
UNION ALL
SELECT 'Nefrotec DS',
       'Drug/Medicine',
       '340',
       '800'
UNION ALL
SELECT 'Nematocide 15ml',
       'Drug/Medicine',
       '80',
       '200'
UNION ALL
SELECT 'Nutrich',
       'Drug/Medicine',
       '395',
       '800'
UNION ALL
SELECT 'Nutriplus Gel',
       'Drug/Medicine',
       '545',
       '700'
UNION ALL
SELECT 'Oridermyl',
       'Drug/Medicine',
       '660',
       '850'
UNION ALL
SELECT 'Protect Plus Gold',
       'Drug/Medicine',
       '1373',
       ''
UNION ALL
SELECT 'Puppy Love 600g',
       'Drug/Medicine',
       '500',
       '750'
UNION ALL
SELECT 'Refamol',
       'Drug/Medicine',
       '285',
       '450'
UNION ALL
SELECT 'Nefrotec DS per pc',
       'Drug/Medicine',
       '6',
       '25'
UNION ALL
SELECT 'Refamol per pc',
       'Drug/Medicine',
       '10',
       '20'
UNION ALL
SELECT 'Broncure',
       'Treats',
       '142',
       '300'
UNION ALL
SELECT 'Doxy Syrup',
       'Treats',
       '138',
       '300'
UNION ALL
SELECT 'Doxy Tab',
       'Treats',
       '15',
       ''
UNION ALL
SELECT 'EnerG',
       'Treats',
       '117',
       '250'
UNION ALL
SELECT 'Enmalac',
       'Treats',
       '136',
       '300'
UNION ALL
SELECT 'Enroflox',
       'Treats',
       '10',
       '20'
UNION ALL
SELECT 'Flavet',
       'Treats',
       '73',
       '250'
UNION ALL
SELECT 'Iozin',
       'Treats',
       '160',
       '400'
UNION ALL
SELECT 'Iron',
       'Treats',
       '140',
       '300'
UNION ALL
SELECT 'Livewell',
       'Treats',
       '157',
       '300'
UNION ALL
SELECT 'MVP',
       'Treats',
       '89',
       '220'
UNION ALL
SELECT 'Nacalvit C',
       'Treats',
       '108',
       '250'
UNION ALL
SELECT 'Papi OB',
       'Treats',
       '127',
       '280'
UNION ALL
SELECT 'Papi Pet Powder',
       'Treats',
       '91',
       '250'
UNION ALL
SELECT 'Papi Scour',
       'Treats',
       '85.42',
       '220'
UNION ALL
SELECT 'Papi Topiderm',
       'Treats',
       '128',
       '250'
UNION ALL
SELECT 'Proxantel',
       'Treats',
       '1970',
       ''
UNION ALL
SELECT 'Venoma',
       'Treats',
       '220',
       '400'
UNION ALL
SELECT 'Vermifuge',
       'Treats',
       '154',
       '350'
UNION ALL
SELECT 'Metronidazole',
       'Drug/Medicine',
       '177',
       '500'
UNION ALL
SELECT 'Mother and Puppy Paste',
       'Medical Supply',
       '451',
       '700'
UNION ALL
SELECT 'Singen Milk',
       'Medical Supply',
       '457',
       '700'
UNION ALL
SELECT 'Renal Paste',
       'Medical Supply',
       '605',
       '850'
UNION ALL
SELECT 'Liquid Kidney',
       'Medical Supply',
       '315',
       '500'
UNION ALL
SELECT 'Recovery Paste for Dog/Cat',
       'Medical Supply',
       '445.35',
       '700'
UNION ALL
SELECT 'Fur and Skin Diet',
       'Medical Supply',
       '180',
       '300'
UNION ALL
SELECT 'Iron Oral Solution',
       'Medical Supply',
       '530',
       '800'
UNION ALL
SELECT 'Actea Oral',
       'Medical Supply',
       '594',
       '800'
UNION ALL
SELECT 'Pet Reboost',
       'Medical Supply',
       '300',
       '500'
UNION ALL
SELECT 'Liverator',
       'Medical Supply',
       '150',
       '350'
UNION ALL
SELECT 'Bacterid',
       'Medical Supply',
       '38',
       '150'
UNION ALL
SELECT 'gabay S',
       'Pet Supplies',
       '30',
       '100'
UNION ALL
SELECT 'GABAY M',
       'Pet Supplies',
       '55',
       '130'
UNION ALL
SELECT 'GABAY L',
       'Pet Supplies',
       '65',
       '150'
UNION ALL
SELECT 'Boley Water Feeder',
       'Pet Supplies',
       '71',
       '150'
UNION ALL
SELECT 'Square Water Feeder',
       'Pet Supplies',
       '106',
       '250'
UNION ALL
SELECT 'DONO MALE WRAP XS',
       'Pet Supplies',
       '10',
       '20'
UNION ALL
SELECT 'DONO MALE WRAP S',
       'Pet Supplies',
       '14',
       '25'
UNION ALL
SELECT 'Nail Clipper (pink&Blue)',
       'Pet Supplies',
       '50',
       '150'
UNION ALL
SELECT 'SQUEEKY TOY S',
       'Pet Supplies',
       '40',
       '80'
UNION ALL
SELECT 'SQUEEKYT TOY L',
       'Pet Supplies',
       '50',
       '120'
UNION ALL
SELECT 'WHITE COMB',
       'Pet Supplies',
       '35',
       '150'
UNION ALL
SELECT 'Nursing Kit S',
       'Pet Supplies',
       '58',
       '100'
UNION ALL
SELECT 'Nursing Kit B',
       'Pet Supplies',
       '90',
       '200'
UNION ALL
SELECT 'Tennis Ball',
       'Pet Supplies',
       '20',
       '50/pc'
UNION ALL
SELECT 'Squeaky Big Mac',
       'Pet Supplies',
       '35',
       '100'
UNION ALL
SELECT 'Atom Ball',
       'Pet Supplies',
       '102',
       '250'
UNION ALL
SELECT 'Mondex',
       'Pet Supplies',
       '52',
       '100'
UNION ALL
SELECT 'Collar w/ Bell',
       'Pet Supplies',
       '20',
       '50'
UNION ALL
SELECT 'Harness w/ Leash',
       'Pet Supplies',
       '25',
       '100'
UNION ALL
SELECT 'Show Leash',
       'Pet Supplies',
       '170',
       '250'
UNION ALL
SELECT 'Slow Feeder',
       'Pet Supplies',
       '208',
       '250'
UNION ALL
SELECT 'E-collar 1',
       'Pet Supplies',
       '130',
       '300'
UNION ALL
SELECT 'E-collar 2',
       'Pet Supplies',
       '108',
       '250'
UNION ALL
SELECT 'E-collar 3',
       'Pet Supplies',
       '90',
       '200'
UNION ALL
SELECT 'E-collar 4',
       'Pet Supplies',
       '66',
       '150'
UNION ALL
SELECT 'E-collar 5',
       'Pet Supplies',
       '58',
       '100'
UNION ALL
SELECT 'E-collar 6',
       'Pet Supplies',
       '46',
       ''
UNION ALL
SELECT 'E-collar 7',
       'Pet Supplies',
       '',
       ''
UNION ALL
SELECT 'Bioline Tooth Brush Set',
       'Pet Supplies',
       '168',
       '380'
UNION ALL
SELECT 'Bioline Tooth Paste',
       'Pet Supplies',
       '88',
       '180'
UNION ALL
SELECT 'WIWIPAD Small',
       'Pet Supplies',
       '358',
       ''
UNION ALL
SELECT 'WIWIPAD Medium',
       'Pet Supplies',
       '388',
       ''
UNION ALL
SELECT 'WIWIPAD Large',
       'Pet Supplies',
       '408',
       ''
UNION ALL
SELECT 'WIWIPAD XL',
       'Pet Supplies',
       '399',
       ''
UNION ALL
SELECT 'DIAPER FEMALE XS',
       'Pet Supplies',
       '141.35',
       '360'
UNION ALL
SELECT 'DIAPER FEMALE S',
       'Pet Supplies',
       '141.35',
       '400'
UNION ALL
SELECT 'DIAPER FEMALE M',
       'Pet Supplies',
       '162.29',
       '450'
UNION ALL
SELECT 'DIAPER FEMALE XS per piece',
       'Pet Supplies',
       '8',
       '20'
UNION ALL
SELECT 'DIAPER FEMALE S per piece',
       'Pet Supplies',
       '9',
       '25'
UNION ALL
SELECT 'DIAPER FEMALE M per piece',
       'Pet Supplies',
       '12',
       '30'
UNION ALL
SELECT 'Saint Roche 250ml',
       'Shampoo and Soap',
       '134',
       '250'
UNION ALL
SELECT 'Saint Roche 628ml',
       'Shampoo and Soap',
       '302',
       '450'
UNION ALL
SELECT 'Saint Roche 1050ml',
       'Shampoo and Soap',
       '385',
       '600'
UNION ALL
SELECT 'Bearing 150ml',
       'Shampoo and Soap',
       '135',
       '200'
UNION ALL
SELECT 'Bearing 300ml',
       'Shampoo and Soap',
       '220',
       '320'
UNION ALL
SELECT 'Bearing 600ml',
       'Shampoo and Soap',
       '295',
       '450'
UNION ALL
SELECT 'SNT.ROCHE 500ML',
       'Shampoo and Soap',
       '302',
       '450'
UNION ALL
SELECT 'FUR MAGIC 300ML',
       'Shampoo and Soap',
       '168',
       '250'
UNION ALL
SELECT 'FUR MAGIC 600ML',
       'Shampoo and Soap',
       '315',
       '500'
UNION ALL
SELECT 'TOFU CAT LITTER',
       'Others',
       '290',
       '400'
UNION ALL
SELECT 'Starter Mousse',
       'Medical Supply',
       '115',
       '200'
UNION ALL
SELECT 'Puppy Protech Milk',
       'Medical Supply',
       '252',
       '450'
UNION ALL
SELECT 'Persian Pouch',
       'Medical Supply',
       '54',
       '80'
UNION ALL
SELECT 'Recover Liquid Dog/ Cat',
       'Medical Supply',
       '249',
       '400'
UNION ALL
SELECT 'Renal Liquid Dog',
       'Medical Supply',
       '249',
       '400'
UNION ALL
SELECT 'Mini Indoor Puppy',
       'Medical Supply',
       '647',
       '800'
UNION ALL
SELECT 'CPV',
       'Laboratory Test & Kits',
       '150',
       '700'
UNION ALL
SELECT 'CDV',
       'Laboratory Test & Kits',
       '150',
       '700'
UNION ALL
SELECT 'E. Canis',
       'Laboratory Test & Kits',
       '225',
       '700'
UNION ALL
SELECT 'Pregnancy',
       'Laboratory Test & Kits',
       '290',
       '550'
UNION ALL
SELECT '2 Way test (CPV/CDV)',
       'Laboratory Test & Kits',
       '250',
       '1000'
UNION ALL
SELECT '3 Way Test ( CCV/CPV/GIA)',
       'Laboratory Test & Kits',
       '410',
       '1300'
UNION ALL
SELECT '4 Way Test (CHW/ANA/BAB/EHR)',
       'Laboratory Test & Kits',
       '440',
       '1500'
UNION ALL
SELECT 'Jerhigh',
       'Treats',
       '70',
       '120'
UNION ALL
SELECT 'Sleeky Chew',
       'Treats',
       '66',
       '90'
UNION ALL
SELECT 'How Bone',
       'Treats',
       '120',
       '250'
UNION ALL
SELECT 'Tasty Bites',
       'Treats',
       '83',
       '110'
UNION ALL
SELECT 'John''s Farm',
       'Treats',
       '100',
       '150'
UNION ALL
SELECT 'Delicio',
       'Treats',
       '100',
       '150'
UNION ALL
SELECT 'Dentastix S',
       'Treats',
       '48.45',
       '100'
UNION ALL
SELECT 'Dentastix M',
       'Treats',
       '66.2',
       '110'
UNION ALL
SELECT 'Dentastix L',
       'Treats',
       '78.45',
       '120'
UNION ALL
SELECT 'Temptation',
       'Treats',
       '85.15',
       '130'
UNION ALL
SELECT '5 in 1',
       'Vaccines',
       '150',
       '400'
UNION ALL
SELECT '6 in 1',
       'Vaccines',
       '210',
       '450'
UNION ALL
SELECT '8 in 1',
       'Vaccines',
       '200',
       '500'
UNION ALL
SELECT 'ARV',
       'Vaccines',
       '50',
       '200'
UNION ALL
SELECT 'KC',
       'Vaccines',
       '215',
       '500'
UNION ALL
SELECT 'Pro Heart',
       'Vaccines',
       '195',
       '1000'
UNION ALL
SELECT 'HIPRA 2IN1',
       'Vaccines',
       '140',
       '400'
UNION ALL
SELECT 'Hipra 7in1',
       'Vaccines',
       '200',
       '450'
UNION ALL
SELECT 'Feline 4 in 1',
       'Vaccines',
       '560',
       '900'
UNION ALL
SELECT '',
       '',
       '',
       ''

GO 
