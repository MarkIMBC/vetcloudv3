GO

DECLARE @GUID_Company VARCHAR(MAX) = 'AC35C0AC-5C1D-4559-BFE6-1C8E8988FB29'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2)
  )

INSERT @forImport
       (Name_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice)
SELECT dbo.fGetCleanedString([ITEM NAME ]),
       dbo.fGetCleanedString([CATEGORY]),
       TRY_PARSE(dbo.fGetCleanedString([BUYING PRICE]) as decimal(18, 2)),
       TRY_PARSE(dbo.fGetCleanedString([SELLING PRICE]) as decimal(18, 2))
FROM   ForImport.[dbo].[JCPAWSCLAWS-Item]

Update @forImport
SET    ID_ItemCategory = category.ID
FROm   @forImport import
       LEFT JOIN tItemCategory category
              on import.Name_ItemCategory = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType

Update @forImport
SET    ID_ItemCategory = category.ID
FROm   @forImport import
       LEFT JOIN tItemCategory category
              on import.Name_ItemCategory = category.Name
WHERE  category.ID_ItemType = 1

SELECT *
FROm   @forImport
WHERE  ID_ItemCategory IS NULL

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             UnitCost,
             UnitPrice,
             ID_ItemCategory,
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                import.UnitCost,
                import.UnitPrice,
                import.ID_ItemCategory,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0 
