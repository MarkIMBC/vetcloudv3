GO

DECLARE @GUID_Company VARCHAR(MAX) = '9AB6327D-BAE6-402F-AE72-97BB3FDFF9C2'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Service_ID_ItemType INT = 1
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitPrice         DECIMAL(18, 2)
  )

INSERT @forImport
       (Name_Item,
        Name_ItemCategory,
        UnitPrice)
SELECT dbo.fGetCleanedString([SERVICE NAME ]),
       dbo.fGetCleanedString([CATEGORY]),
       TRY_PARSE(dbo.fGetCleanedString([SERVICE FEE]) as decimal(18, 2))
FROM   ForImport.[dbo].[JCPAWSCLAWS-Service]

Update @forImport
SET    ID_ItemCategory = category.ID
FROm   @forImport import
       LEFT JOIN tItemCategory category
              on import.Name_ItemCategory = category.Name
WHERE  category.ID_ItemType = @Service_ID_ItemType

Update @forImport
SET    Name_ItemCategory = 'Doctor''s Fee'
WHERE  Name_ItemCategory = 'Doctor�s Fee'

Update @forImport
SET    Name_ItemCategory = 'Service'
WHERE  Name_ItemCategory = 'Services'

Update @forImport
SET    ID_ItemCategory = category.ID
FROm   @forImport import
       LEFT JOIN tItemCategory category
              on import.Name_ItemCategory = category.Name
WHERE  category.ID_ItemType = 1

SELECT DISTINCT Name_ItemCategory
FROm   @forImport
WHERE  ID_ItemCategory IS NULL

SELECT DISTINCT Name_ItemCategory
FROm   @forImport
WHERE  ID_ItemCategory IS NULL

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             UnitPrice,
             ID_ItemCategory,
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                @Service_ID_ItemType,
                import.UnitPrice,
                import.ID_ItemCategory,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0 


	   GO
	    