/****** Object:  Table [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]    Script Date: 3/25/2021 3:39:54 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]') AND type in (N'U'))
DROP TABLE [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]
GO
/****** Object:  Table [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]    Script Date: 3/25/2021 3:39:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [float] NULL,
	[Selling Price] [float] NULL,
	[Current Inventory Count] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rabisin (Biocan)', N'Vaccination', NULL, 300, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Kennel Cough (Bronchicine)', N'Vaccination', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'5in1 (Vanguard  L5)', N'Vaccination', NULL, 350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'6in1 (Eurican DHPP12L)', N'Vaccination', NULL, 550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'4in1 (Biofel PCH + R)', N'Vaccination', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canine Parvo Virus', N'Laboratory Test & Kits', 160, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canine Distemper', N'Laboratory Test & Kits', 160, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canine Heartworm', N'Laboratory Test & Kits', 220, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canine Ehrlichia', N'Laboratory Test & Kits', 290, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CPV + CCV + Rota', N'Laboratory Test & Kits', 340, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CHW + BAB + ANA', N'Laboratory Test & Kits', 400, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FPL + FCOV + Guardia', N'Laboratory Test & Kits', 355, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CPV + CDV + Guardia', N'Laboratory Test & Kits', 558, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fiprovet Plus Small (1-11kg)', N'Injectible', 198, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fiprovet Plus medium (11-25kg)', N'Injectible', 231, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fiprovet Plus Large (26-40kg)', N'Injectible', 265, 575, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fiprovet Plus Xlarge (41-60kg)', N'Injectible', 298, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Defense Puppy & Miniature', N'Injectible', 401, 550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Defense Small Dog', N'Injectible', 407, 595, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Practic Small (4.5-11kg)', N'Injectible', 250, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Practic Medium (11-22kg)', N'Injectible', 333, 595, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline Small (1-10kg)', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline Medium (10-20kg)', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline Large (20-40kg)', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline Xlarge (40-60kg)', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate Small (1-4kg)', N'Injectible', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate Medium (4-10kg)', N'Injectible', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate Large (10-20kg)', N'Injectible', NULL, 850, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate Xlarge (20-40kg)', N'Injectible', NULL, 950, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prazinate Bottle', N'Drug/Medicine', 330, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prazinate ', N'Drug/Medicine', NULL, 200, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Milbemax Small', N'Drug/Medicine', 75, 275, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Milbemax Large', N'Drug/Medicine', 100, 375, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet-C', N'Drug/Medicine', 130, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Scourvet', N'Drug/Medicine', 200, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sorvit 60ml', N'Drug/Medicine', 115, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sorvit 120ml', N'Drug/Medicine', 144, 380, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doggy-Doggy 60ml', N'Drug/Medicine', 115, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doggy-Doggy 120ml', N'Drug/Medicine', 144, 380, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Iron B-Complex', N'Drug/Medicine', 182, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Barfy 60ml', N'Drug/Medicine', 60, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nacalvit-C', N'Drug/Medicine', 140, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-Vit 60ml', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi-Livwell', N'Drug/Medicine', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Paracetamol', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pulmoquin', N'Drug/Medicine', NULL, 380, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Energy 60ml', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ferrous Sulfate (Iron)', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Broncure', N'Drug/Medicine', NULL, 285, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enrofloxacin 100ml', N'Drug/Medicine', NULL, 590, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Gravidamin', N'Drug/Medicine', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enmalac', N'Drug/Medicine', NULL, 380, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi-OB', N'Drug/Medicine', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Syrup 100ml', N'Drug/Medicine', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv 52 Syrup', N'Drug/Medicine', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline Syrup 60ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DR.C''S Soap', N'Others', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Jojoba Soap', N'Others', NULL, 165, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dermovet', N'Others', NULL, 145, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dr.C''s Others', N'Others', NULL, 375, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bearing Others 150ml', N'Others', NULL, 195, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bearing Others 300ml', N'Others', NULL, 420, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bearing Others 600ml', N'Others', NULL, 850, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furmagic 3000ml', N'Others', NULL, 285, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furmagic 500ml', N'Others', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furmagic 1000ml', N'Others', NULL, 580, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Saint Roche 250ml', N'Others', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Saint roche 628ml', N'Others', NULL, 580, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Saint Roche 1 Gallon', N'Others', NULL, 2100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Jojoba Organic 500ml', N'Others', NULL, 295, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline Drug/Medicine / Pcs (Biocure)', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline Drug/Medicine / Bottle (Biocure', N'Drug/Medicine', NULL, 480, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Laktrazine / Pcs (Co-Trimazine) ', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Laktrazine / Bottle (Co-Trimazine)', N'Drug/Medicine', NULL, 480, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Brewers Yeast / Pcs', N'Drug/Medicine', NULL, 5, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Allergy Ease / Pcs', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9-Ease (Chamomile) / Pcs', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy Liver', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 MultiDrug/Medicine Drug/Medicine', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Glucosamine', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy Vision', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Ease', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Probiotics Capsules', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Macrocal / Pcs', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Macrocal / Bottle', N'Drug/Medicine', NULL, 700, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Kawu', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Kawu / Bottle', N'Drug/Medicine', NULL, 900, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nefrotec / Pcs', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nefrotec / Bottle', N'Drug/Medicine', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Freximol / Pcs Capsule', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Freximol / Bottle Capsule', N'Drug/Medicine', NULL, 480, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Apoquel / Pcs', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Itraconazole', N'Drug/Medicine', NULL, 35, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dexamethasone', N'Drug/Medicine', NULL, 18, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cetirizine', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone', N'Drug/Medicine', NULL, 18, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv-52 Drug/Medicine / Pcs', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv-52 Drug/Medicine / Bottle', N'Drug/Medicine', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Drug/Medicine / Pcs', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Drug/Medicine/ Bottle', N'Drug/Medicine', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cephalexin 60ml', N'Drug/Medicine', NULL, 295, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin 60ml', N'Drug/Medicine', NULL, 295, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dextrose Powder 100g', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dextrose Powder 340g', N'Drug/Medicine', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ear Cleanser Dr.C''s', N'Medical Supply', NULL, 375, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ear Cleanser Nutrivet', N'Medical Supply', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Otidex 15ml', N'Medical Supply', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Eye Cleanser Nutrivet', N'Medical Supply', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Opticare (Gentamicin)', N'Medical Supply', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Wound Cream Dr.C''s', N'Drug/Medicine', NULL, 375, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Anti-Itch Spray Nutrivet', N'Drug/Medicine', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Venoma Herbal Spray', N'Drug/Medicine', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Puppy Vite Nutrivet', N'Drug/Medicine', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutri-tierGel', N'Drug/Medicine', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard 25-40kg', N'Drug/Medicine', NULL, 950, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 2.5-4.5kg', N'Drug/Medicine', NULL, 1450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 4.5-10kg', N'Drug/Medicine', NULL, 1550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 10-20kg', N'Drug/Medicine', NULL, 1650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 20-40kg', N'Drug/Medicine', NULL, 1750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra 3.5- 7.5kg', N'Drug/Medicine', NULL, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra 7.5- 15kg', N'Drug/Medicine', NULL, 1550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra 15-30kg', N'Drug/Medicine', NULL, 1750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra 30-60kg', N'Drug/Medicine', NULL, 1950, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Flea + Tick Defense Small (Vetri-Science)', N'Merchandise & Accessories', NULL, 450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Flea + Tick Defense Medium (Vetri-Science)', N'Merchandise & Accessories', NULL, 550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Flea + Tick Defense Large (Vetri-Science)', N'Merchandise & Accessories', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Flea + Tick Defense For Cats', N'Merchandise & Accessories', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dentastix', N'Pet Food', NULL, 110, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Breath Bites', N'Pet Food', NULL, 95, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Goodies', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Knotted Bone Small', N'Pet Food', NULL, 10, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Knotted Bone Medium', N'Pet Food', NULL, 15, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Knotted Bone Large', N'Pet Food', NULL, 50, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Milko Pro (Milk)', N'Pet Food', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Puppy Love', N'Pet Food', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Pouch', N'Pet Food', NULL, 60, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cesar', N'Pet Food', NULL, 90, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Whiskas Pouch', N'Pet Food', NULL, 45, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat / Kilo Kitten', N'Pet Food', NULL, 180, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Sack Kitten', N'Pet Food', NULL, 1400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Chicken / Kilo', N'Pet Food', NULL, 180, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Chicken Sack', N'Pet Food', NULL, 1400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Green / Kilo (Ocean Fish)', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Green Sack (Ocean Fish)', N'Pet Food', NULL, 1320, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Blue / Kilo (Tuna)', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powercat Blue Sack (Tuna)', N'Pet Food', NULL, 1320, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Puppy / Kilo', N'Pet Food', NULL, 135, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Puppy Sack', N'Pet Food', NULL, 2100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Adult Small Breed / Kilo ', N'Pet Food', NULL, 130, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Lamb Adult Large Breed / Kilo', N'Pet Food', NULL, 130, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Beef Adult Large Breed / Kilo', N'Pet Food', NULL, 130, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Lamb Adult Large Breed / Sack', N'Pet Food', NULL, 2000, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog beef Adult Large Breed / Sack', N'Pet Food', NULL, 2000, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Small Breed Adult / Sack', N'Pet Food', NULL, 2000, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Lamb Adult Large Breed 3 kg', N'Pet Food', NULL, 400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Beef Adult Large Breed 3 kg', N'Pet Food', NULL, 400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powerdog Lamb Adult Small Breed 3kg', N'Pet Food', NULL, 400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vitality Puppy / Kilo', N'Pet Food', NULL, 167, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vitality Puppy / Sack', N'Pet Food', NULL, 2450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vitality Adult / Kilo', N'Pet Food', NULL, 165, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vitality Adult Sack', N'Pet Food', NULL, 2400, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Puppy Chicken / Kilo', N'Pet Food', NULL, 160, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Puppy Chicken Sack 15kg', N'Pet Food', NULL, 2300, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Small Breed / Kilo', N'Pet Food', NULL, 155, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Small Breed / Sack 20kg', N'Pet Food', NULL, 2700, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Beef & Veg Adult / Kilo', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Adult 10kg Sack', N'Pet Food', NULL, 1450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Dog Puppy / Kilo', N'Pet Food', NULL, 155, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Dog Puppy /Sack', N'Pet Food', NULL, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Dog Adult / Kilo', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Dog Adult / Sack', N'Pet Food', NULL, 1300, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Maxime Elite Puppy / Kilo', N'Pet Food', NULL, 190, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Treatment Small Breed', N'Injectible', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Treatment Medium Dog', N'Injectible', NULL, 450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Treatment Large Breed', N'Injectible', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Microfelaria Test', N'Laboratory Test & Kits', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clinic Medical Supply', N'Medical Supply', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'IV Set', N'Medical Supply', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Oxygen Small Breed', N'Medical Supply', NULL, 350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Oxygen Large Breed', N'Medical Supply', NULL, 600, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'D5LRS', N'Medical Supply', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cathetirization', N'Medical Supply', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Wound Cleaning', N'Medical Supply', NULL, 350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cerenia .1ml', N'Injectible', NULL, 80, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob P .1ml', N'Injectible', NULL, 35, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob D .1ml', N'Injectible', NULL, 35, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories XS', N'Merchandise & Accessories', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories Small', N'Merchandise & Accessories', NULL, 28, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories Large', N'Merchandise & Accessories', NULL, 30, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories Xlarge', N'Merchandise & Accessories', NULL, 35, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories 2XL', N'Merchandise & Accessories', NULL, 35, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dress Small', N'Merchandise & Accessories', NULL, 375, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dress Large', N'Merchandise & Accessories', NULL, 475, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Water Feeder', N'Merchandise & Accessories', NULL, 195, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Feeding Bottle', N'Merchandise & Accessories', NULL, 175, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Toothbrush', N'Merchandise & Accessories', NULL, 65, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dental Care Set', N'Merchandise & Accessories', NULL, 295, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Smart Bottle', N'Merchandise & Accessories', NULL, 110, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ball Toys Small', N'Merchandise & Accessories', NULL, 100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ball Toys Large', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bone Toys w/ Spike Small', N'Merchandise & Accessories', NULL, 175, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bone Toys Small', N'Merchandise & Accessories', NULL, 135, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bone Toys Large', N'Merchandise & Accessories', NULL, 285, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Steel Comb Small', N'Merchandise & Accessories', NULL, 185, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Steel Comb Large', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Slicker Brush Small', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Slicker Brush Large', N'Merchandise & Accessories', NULL, 285, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bags Small', N'Merchandise & Accessories', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bags Medium', N'Merchandise & Accessories', NULL, 750, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bags Large', N'Merchandise & Accessories', NULL, 950, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bags Xlarge', N'Merchandise & Accessories', NULL, 1000, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage XSmall', N'Merchandise & Accessories', NULL, 900, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage Small Non-Collapsible', N'Merchandise & Accessories', NULL, 1100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage Medium Non-Collapsible', N'Merchandise & Accessories', NULL, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage Small Collapsible', N'Merchandise & Accessories', NULL, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage Large Collapsible', N'Merchandise & Accessories', NULL, 2100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cage Medium Collapsible', N'Merchandise & Accessories', NULL, 1550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cat Litter Sand 2kls', N'Merchandise & Accessories', NULL, 165, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cat Litter Sand bag', N'Merchandise & Accessories', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cat Litter Sand Sack', N'Merchandise & Accessories', NULL, 1350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Travel Crates Small', N'Merchandise & Accessories', NULL, 2100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Travel Crates Large', N'Merchandise & Accessories', NULL, 3000, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amitraz per ML', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Deworming Puppy Small', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Whiskas 1.1kg', N'Pet Food', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bandage', N'Medical Supply', NULL, 350, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cat Mouse Toys', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aluminum Bowl Small', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aluminum Bowl Medium', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aluminum Bowl Large', N'Merchandise & Accessories', NULL, 450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aluminum Bowl Xlarge', N'Merchandise & Accessories', NULL, 495, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #1', N'E-Merchandise & Accessories', NULL, 450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #2', N'E-Merchandise & Accessories', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #3', N'E-Merchandise & Accessories', NULL, 295, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #4', N'E-Merchandise & Accessories', NULL, 225, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #5', N'E-Merchandise & Accessories', NULL, 175, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #6', N'E-Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-Merchandise & Accessories #7', N'E-Merchandise & Accessories', NULL, 110, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories w/ Bell', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories 4.0', N'Merchandise & Accessories', NULL, 330, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories 1.5', N'Merchandise & Accessories', NULL, 125, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Merchandise & Accessories 3.0', N'Merchandise & Accessories', NULL, 285, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Harness Merchandise & Accessories 4.0', N'Merchandise & Accessories', NULL, 550, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Harness Merchandise & Accessories 2.5', N'Merchandise & Accessories', NULL, 275, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Merchandise & Accessories 2.0', N'Merchandise & Accessories', NULL, 195, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Harness 1.0', N'Merchandise & Accessories', NULL, 185, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Harness 2.0', N'Merchandise & Accessories', NULL, 330, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Harness 2.5', N'Merchandise & Accessories', NULL, 395, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Harness 4.0', N'Merchandise & Accessories', NULL, 650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leash Merchandise & Accessories 1.5', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Plastic Bowl', N'Merchandise & Accessories', NULL, 225, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Chain Leash', N'Merchandise & Accessories', NULL, 530, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Chain Merchandise & Accessories 2.0', N'Merchandise & Accessories', NULL, 100, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Chain Merchandise & Accessories 3.5', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Chain Merchandise & Accessories 3.0', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pill Gun', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Necktie', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Royal Canine For Cat', N'Pet Food', NULL, 1450, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Royal Canine For Dog', N'Pet Food', NULL, 1650, NULL)
GO
INSERT [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Royal Canine For Cat Pouch', N'Pet Food', NULL, 120, NULL)
GO


DECLARE @TheAnimalDoctorClinicAndGroomingServices_ID_Company INT = 46

Declare @Record Table (ItemName Varchar(MAX), ID_ItemCategory INT, UnitCost Decimal, UnitPrice Decimal)

INSERT @Record(
	ItemName,
	ID_ItemCategory,
	UnitCost,
	UnitPrice
)
SELECT  
		LTRIM(RTRIM(import.Name)), 
		cat.ID,
		ISNULL([Buying Price], 0),
		ISNULL([Selling Price], 0)
FROM    [TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25] import 
	LEFT JOIN tItemCategory  cat 
		ON TRIM(LOWER(cat.Name)) = TRIM(LOWER(import.Category)) AND cat.ID_ItemType = 2

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  CurrentInventoryCount,
  Old_item_id)
SELECT 
   @TheAnimalDoctorClinicAndGroomingServices_ID_Company
  ,LTRIM(RTRIM(ItemName)) 
  ,UnitPrice
  ,2
  , UnitCost
  , ID_ItemCategory
  , NULL
  , 1
  , 'Imported on 2021-03-25 3pm'
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , 0
  , NULL
FROM @Record ORDER BY ItemName

exec pUpdateItemCurrentInventory


/****** Object:  Table [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]    Script Date: 3/25/2021 2:17:43 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]') AND type in (N'U'))
DROP TABLE [dbo].[TheAnimalDoctorClinicAndGroomingservices_ImportItems_2021-03-25]
GO





