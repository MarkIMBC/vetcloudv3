GO

DECLARE @GUID_Company VARCHAR(MAX) = '58E820FD-9002-4651-B2B4-F8B38276DFE2'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @preImport TABLE
  (
     RowIndex             INT,
     ID_Client            INT,
     Name_Client          VARCHAR(MAX),
     Address_Client       VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     Email_Client         VARCHAR(MAX),
     ID_Patient           VARCHAR(MAX),
     Name_Patient         VARCHAR(MAX),
     ID_Gender            VARCHAR(MAX),
     DateBirth_Patient    VARCHAR(MAX),
     Species_Patient      VARCHAR(MAX),
     Weight_Patient       VARCHAR(MAX),
     Color_Patient        VARCHAR(MAX)
  )

INSErt @preImport
       (RowIndex,
        Name_Client,
        Address_Client,
        ContactNumber_Client,
        Email_Client,
        Name_Patient,
        ID_Gender,
        DateBirth_Patient,
        Species_Patient,
        Weight_Patient,
        Color_Patient)
SELECT RowIndex,
       dbo.fGetCleanedString([NAME]),
       dbo.fGetCleanedString([ADDRESS]),
       dbo.fGetCleanedString([MOBILE NO.]),
       dbo.fGetCleanedString([EMAIL]),
       dbo.fGetCleanedString([PET NAME]),
       dbo.fGetCleanedString([GENDER]),
       dbo.fGetCleanedString([BIRTHDATE]),
       dbo.fGetCleanedString( [PET TYPE]) + ' - '
       + dbo.fGetCleanedString( [PET BREED]),
       dbo.fGetCleanedString([WEIGHT]),
       dbo.fGetCleanedString([COLOR])
FROM   ForImport.[dbo].[GALPAVET_CLIENT_DATABASE_9.15.22(1)]

SELECT *
FROM   @preImport