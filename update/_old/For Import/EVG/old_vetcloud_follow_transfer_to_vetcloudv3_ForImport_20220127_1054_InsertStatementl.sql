DECLARE @EVGJuicoPetCarCenter_ID_Company INT = 183
DECLARE @patietWellness TABLE
  (
     return_date           DATETIME,
     [ID_Client]           INT,
     [ID_Patient]          INT,
     ContactNumber         VARCHAR(MAX),
     Email                 VARCHAR(MAX),
     [AttendingPhysician]  VARCHAR(MAX),
     old_return_patient_id VARCHAR(MAX),
     reason                VARCHAR(MAX),
     client_id             VARCHAR(MAX),
     c_pet_id              VARCHAR(MAX)
  )
DECLARE @client_id INT
DECLARE @pet_id INT
DECLARE @rp_id INT = 0
DECLARE @return_date DateTime
DECLARE @u_vet_name VARCHAR(MAX) = ''
DECLARE @client_name VARCHAR(MAX) = ''
DECLARE @pet_name VARCHAR(MAX) = ''
DECLARE @c_mobile VARCHAR(MAX) = ''
DECLARE @c_email VARCHAR(MAX) = ''
DECLARE @reason VARCHAR(MAX) = ''
DECLARE @c_status VARCHAR(MAX) = ''

UPDATE [evg_from_old_vetcloud_followup]
SET    c_mobile = ''
WHERE  c_mobile = 'NULL'

UPDATE [evg_from_old_vetcloud_followup]
SET    c_mobile = ''
WHERE  c_mobile = 'NULL'

UPDATE [evg_from_old_vetcloud_followup]
SET    [vetname] = ''
WHERE  [vetname] = 'NULL'

UPDATE [evg_from_old_vetcloud_followup]
SET    reason = ''
WHERE  reason = 'NULL'

DECLARE import_cursor CURSOR FOR
  SELECT returnPatient.[rp_id],
         returnPatient.[return_date],
         returnPatient.[vetname],
         returnPatient.[client_name],
         returnPatient.[pet_name],
         returnPatient.[c_mobile],
         returnPatient.[c_email],
         returnPatient.[reason],
         returnPatient.[c_status],
         returnPatient.[client_id],
         returnPatient.[pet_id]
  FROM   [evg_from_old_vetcloud_followup] returnPatient
  WHERE  returnPatient.[rp_id] NOT IN (SELECT old_return_patient_id
                                       FROM   tPatient_Wellness
                                       WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
                                              AND old_return_patient_id IS NOT NULL)
  ORDER  BY CONVERT(DATETIME, returnPatient.[return_date]) ASC

OPEN import_cursor

/*  8138
8139 */
FETCH NEXT FROM import_cursor INTO @rp_id,
                                   @return_date,
                                   @u_vet_name,
                                   @client_name,
                                   @pet_name,
                                   @c_mobile,
                                   @c_email,
                                   @reason,
                                   @c_status,
                                   @client_id,
                                   @pet_id

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Patient_Wellness INT = 0
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      SELECT @ID_Client = ID
      FROM   tClient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_client_id = @client_id

      IF( ISNULL(@ID_Client, 0) = 0 )
        BEGIN
            INSERT INTO [dbo].[tClient]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [ContactNumber],
                         [Email],
                         Old_client_id)
            VALUES      ( @client_name,
                          1,
                          @EVGJuicoPetCarCenter_ID_Company,
                          '',
                          GetDate(),
                          GetDate(),
                          1,
                          1,
                          @c_mobile,
                          @c_email,
                          @client_id )

            SET @ID_Client = @@IDENTITY
        END

      SELECT @ID_Patient = ID
      FROM   tPatient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_patient_id = @pet_id

      IF( ISNULL(@ID_Patient, 0) = 0 )
        BEGIN
            INSERT INTO [dbo].[tPatient]
                        (ID_Client,
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         Old_patient_id)
            VALUES      (@ID_Client,
                         @pet_name,
                         1,
                         @EVGJuicoPetCarCenter_ID_Company,
                         '',
                         GetDate(),
                         GetDate(),
                         1,
                         1,
                         @pet_id)

            SET @ID_Patient = @@IDENTITY
        END

      INSERT @patietWellness
             (return_date,
              [ID_Client],
              [ID_Patient],
              ContactNumber,
              Email,
              [AttendingPhysician],
              old_return_patient_id,
              reason,
              client_id,
              c_pet_id)
      VALUES (@return_date,
              @ID_Client,
              @ID_Patient,
              @c_mobile,
              @c_email,
              @u_vet_name,
              @rp_id,
              @reason,
              @client_id,
              @pet_id )

      INSERT INTO [dbo].[tPatient_Wellness]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Date],
                   [ID_Client],
                   [ID_Patient],
                   [ID_FilingStatus],
                   [AttendingPhysician],
                   old_return_patient_id)
      VALUES      ( 1,
                    @EVGJuicoPetCarCenter_ID_Company,
                    NULL,
                    GetDate(),
                    GetDate(),
                    1,
                    1,
                    @return_date,
                    @ID_Client,
                    @ID_Patient,
                    1,
                    @u_vet_name,
                    @rp_id )

      SET @ID_Patient_Wellness = @@IDENTITY

      INSERT INTO [dbo].[tPatient_Wellness_Schedule]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   [Date],
                   Comment)
      VALUES      (1,
                   @EVGJuicoPetCarCenter_ID_Company,
                   GetDate(),
                   GetDate(),
                   1,
                   1,
                   @ID_Patient_Wellness,
                   @return_date,
                   @reason )

      INSERT INTO [dbo].[tPatient_Wellness_Detail]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   CustomItem)
      VALUES      (1,
                   @EVGJuicoPetCarCenter_ID_Company,
                   GetDate(),
                   GetDate(),
                   1,
                   1,
                   @ID_Patient_Wellness,
                   @reason)

      FETCH NEXT FROM import_cursor INTO @rp_id,
                                         @return_date,
                                         @u_vet_name,
                                         @client_name,
                                         @pet_name,
                                         @c_mobile,
                                         @c_email,
                                         @reason,
                                         @c_status,
                                         @client_id,
                                         @pet_id
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

SELECT h.*,
       client.Name,
       client.ContactNumber,
       patient.Name
FROM   @patietWellness h
       INNER JOIN TClient client
               ON h.ID_Client = client.ID
       INNER JOIN tPatient patient
               ON h.ID_Patient = patient.ID 
