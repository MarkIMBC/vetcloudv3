DECLARE @EVGJuicoPetCarCenter_ID_Company INT = 183
DECLARE @patietWellness TABLE
  (
     return_date           DATETIME,
     [ID_Client]           INT,
     [ID_Patient]          INT,
     [AttendingPhysician]  VARCHAR(MAX),
     old_return_patient_id VARCHAR(MAX),
     reason                VARCHAR(MAX)
  )
DECLARE @return_date DATETIME
DECLARE @client_name VARCHAR(MAX) = ''
DECLARE @pet_name VARCHAR(MAX) = ''
DECLARE @c_mobile VARCHAR(MAX) = ''
DECLARE @c_email VARCHAR(MAX) = ''
DECLARE @reason VARCHAR(MAX) = ''
DECLARE @u_vet_name VARCHAR(MAX) = ''
DECLARE @client_id FLOAT = 0
DECLARE @c_pet_id FLOAT = 0
DECLARE @c_vet_id FLOAT = 0
DECLARE @c_pet_id1 FLOAT = 0
DECLARE @c_plan_id FLOAT = 0
DECLARE @c_return_id FLOAT = 0
DECLARE @Isexist VARCHAR(MAX) = ''
DECLARE @Remarks VARCHAR(MAX) = ''

UPDATE evg_from_20211101_return_patient
SET    c_mobile = ''
WHERE  c_mobile = 'NULL'

UPDATE evg_from_20211101_return_patient
SET    c_mobile = ''
WHERE  c_mobile = 'NULL'

UPDATE evg_from_20211101_return_patient
SET    u_vet_name = ''
WHERE  u_vet_name = 'NULL'

UPDATE evg_from_20211101_return_patient
SET    reason = ''
WHERE  reason = 'NULL'

DECLARE import_cursor CURSOR FOR
  SELECT CONVERT(DATETIME, returnPatient.[return_date]),
         returnPatient.client_name,
         returnPatient.pet_name,
         returnPatient.c_mobile,
         returnPatient.c_email,
         returnPatient.reason,
         returnPatient.u_vet_name,
         returnPatient.client_id,
         returnPatient.c_pet_id,
         returnPatient.c_vet_id,
         returnPatient.c_pet_id1,
         returnPatient.c_plan_id,
         returnPatient.c_return_id,
         [Is exist],
         Remarks
  FROM   evg_from_20211101_return_patient returnPatient
         INNER JOIN evg_from_20211101_for_clientpatient clientpatient
                 ON returnPatient.client_id = clientpatient.client_id
                    AND returnPatient.c_pet_id = clientpatient.c_pet_id
  WHERE  returnPatient.return_date IN ( '2021-11-15', '2021-11-16' )

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @return_date,
                                   @client_name,
                                   @pet_name,
                                   @c_mobile,
                                   @c_email,
                                   @reason,
                                   @u_vet_name,
                                   @client_id,
                                   @c_pet_id,
                                   @c_vet_id,
                                   @c_pet_id1,
                                   @c_plan_id,
                                   @c_return_id,
                                   @Isexist,
                                   @Remarks

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Patient_Wellness INT = 0
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      SELECT @ID_Client = ID
      FROM   tClient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_client_id = @client_id

      SELECT @ID_Patient = ID
      FROM   tPatient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_patient_id = @c_pet_id

      IF( ISNULL(@ID_Client, 0) = 0 )
        BEGIN
            INSERT INTO [dbo].[tClient]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [ContactNumber],
                         [Email],
                         Old_client_id)
            VALUES      ( @client_name,
                          1,
                          @EVGJuicoPetCarCenter_ID_Company,
                          '',
                          GetDate(),
                          GetDate(),
                          1,
                          1,
                          @c_mobile,
                          @c_email,
                          @client_id )

            SET @ID_Client = @@IDENTITY
        END

      IF( ISNULL(@ID_Patient, 0) = 0 )
        BEGIN
            INSERT INTO [dbo].[tPatient]
                        (ID_Client,
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         Old_patient_id)
            VALUES      (@ID_Client,
                         @pet_name,
                         1,
                         @EVGJuicoPetCarCenter_ID_Company,
                         '',
                         GetDate(),
                         GetDate(),
                         1,
                         1,
                         @c_pet_id)

            SET @ID_Patient = @@IDENTITY
        END

      FETCH NEXT FROM import_cursor INTO @return_date,
                                         @client_name,
                                         @pet_name,
                                         @c_mobile,
                                         @c_email,
                                         @reason,
                                         @u_vet_name,
                                         @client_id,
                                         @c_pet_id,
                                         @c_vet_id,
                                         @c_pet_id1,
                                         @c_plan_id,
                                         @c_return_id,
                                         @Isexist,
                                         @Remarks
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

