
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'ImportItem_ClinicaVeterinariaDeFigura_20210226'))
BEGIN

	Drop TABLE [ImportItem_ClinicaVeterinariaDeFigura_20210226]
END


GO
CREATE TABLE [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226](
	[Code] [float] NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [float] NULL,
	[Selling Price] [float] NULL,
	[Current Inventory Count] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (100, N'RC Gastrointestinal', N'Pet Food', 147.25, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (101, N'RC Starter moosse', N'Pet Food', 110, 170, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (102, N'DR Clauders', N'Pet Food', 144, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (103, N'Pyramid hill beef', N'Pet Food', 70, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (104, N'Pyramid hill lamb', N'Pet Food', 70, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (111, N'Eukanuba Adult', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (112, N'Eukanuba Puppy', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (113, N'Hepatic can', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (114, N'Holistic Adult ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (115, N'Holistic puppy', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (116, N'I/D canine', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (117, N'I/D feline ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (118, N'Iams Adult', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (119, N'Iams Kitten', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (120, N'K/D canine', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (121, N'K/D FELINE', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (122, N'L/D canine ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (123, N'L/D feline', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (124, N'Meal time', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (125, N'my foodie can', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (126, N'Pyramid Beef', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (127, N'Pyramid Lamb', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (128, N'RC Hepatic pellet 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (129, N'RC Renal pellet 2kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (130, N'RC Shihtzu Adult 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (131, N'RC Shihtzu Jr 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (132, N'Renal can ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (133, N'SD Hairball 1kg ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (134, N'SD Science diet 2kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (135, N'SD Small Bites Adult 1kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (136, N'SD Small Bites puppy 1kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (137, N'Urinary S/O canine can', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (138, N'Valuemeal ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (139, N'Vitality Classic 15/1', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (140, N'Vitality High Energy 15/1', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (141, N'Whiskas Adult', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (142, N'Whiskas puppy ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (143, N'Deodorizing biscuit 100g', N'Pet Food', 47, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (144, N'Dog biscuit ', N'Pet Food', 220, 350, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (147, N'Dentastix Green tea 75g ', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (148, N'Dentastix Med/Large 86g', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (149, N'Dentastix Puppy 56g', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (150, N'Dentastix  Toy small 75g', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (151, N'Jerhigh spinach ', N'Pet Food', 80.8, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (152, N'Jerhigh banana', N'Pet Food', 80.8, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (153, N'Jerhigh blueberry', N'Pet Food', 80.8, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (154, N'Jerhigh milky', N'Pet Food', 80.8, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (155, N'White bone 6"', N'Pet Food', 31.25, 50, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (156, N'Banana bone 2.5"', N'Pet Food', 7.5, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (157, N'Curly stick BBQ', N'Pet Food', 2, 5, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (158, N'Curly stick Strawberry', N'Pet Food', 3, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (159, N'Knotted bone 2.5" milk', N'Pet Food', 5, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (160, N'Knotted bone 4.5" milk', N'Pet Food', 16, 30, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (161, N'Knotted bone 2.5" apple', N'Pet Food', 6, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (162, N'Knotted bone 2.5" BBQ', N'Pet Food', 4, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (182, N'Squidgy Mini dumbell ', N'Accessories', 18, 65, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (183, N'Squidgy spike ball  ', N'Accessories', 25, 75, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (184, N'Poop bag plain 12''s', N'Accessories', 7, 20, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (185, N'Poop bag with case ', N'Accessories', 40, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (186, N'Adidas with hood medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (187, N'Adidas with hood small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (188, N'Adidas with hood XL', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (189, N'Bag cotton medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (190, N'Bag cotton small ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (191, N'Bag Net medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (192, N'Bag Net small ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (193, N'Bag sling medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (194, N'Bag sling small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (195, N'Biscuit small (charles)', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (196, N'Bowl Double silicon', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (197, N'Bowl Frosty', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (198, N'Bowl Paw design', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (199, N'Bowl plastic Double transparent', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (200, N'Bowl stainless  colored large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (201, N'Bowl stainless  colored medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (202, N'Bowl stainless  double with stand small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (203, N'Bowl stainless  with stand large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (204, N'Bowl stainless colored small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (205, N'Bowl stainless colored XL', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (206, N'Bowl stainless double with stand large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (207, N'Bowl stainless double with stand medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (209, N'Bowl stainless Xl', N'Accessories', 105, 285, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (210, N'Bowl stainless large ', N'Accessories', 78, 205, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (211, N'Bowl stainless  medium', N'Accessories', 50, 185, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (212, N'Bowl stainless  small', N'Accessories', 31, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (213, N'Brush comb all size', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (214, N'Busal Duck large', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (215, N'Busal Duck medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (216, N'Busal Duck small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (217, N'Busal Leather ( all size)', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (218, N'Busal Nylon #1 ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (219, N'Busal Nylon #2', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (220, N'Busal Nylon #3', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (221, N'Busal Nylon #4', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (222, N'Busal Nylon #5', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (223, N'Busal Nylon #6', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (224, N'Busal Nylon #7', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (225, N'Busal plastic #1', N'Accessories', 105, 285, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (226, N'Busal plastic #2', N'Accessories', 78, 205, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (227, N'Busal plastic #3', N'Accessories', 50, 185, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (228, N'Busal plastic #4', N'Accessories', 31, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (229, N'Busal plastic #5', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (230, N'Busal plastic #6', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (231, N'Busal plastic #7', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (232, N'Cat Litter 5L', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (233, N'Cat toy with feather', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (234, N'Cat toy with scratch pad ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (235, N'Calling pad', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (236, N'Clicker ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (237, N'Collar white paw Bell', N'Accessories', 25, 80, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (238, N'Bell character', N'Accessories', 20, 60, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (239, N'Collar nylon plain single 1.0', N'Accessories', 18, 60, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (240, N'Collar christmass with bell', N'Accessories', 52, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (241, N'Collar padded Large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (242, N'Collar padded medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (243, N'Collar padded small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (244, N'Collar padded XL', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (245, N'Collar nylon plain single 1.5 ', N'Accessories', 20, 80, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (246, N'Collar with necktie small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (247, N'Comb Detangler', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (248, N'Comb stainless large', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (249, N'Comb stainless medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (250, N'Comb stainless small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (251, N'Comb with Blade ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (252, N'Comb with handle double side ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (253, N'Comb with handle single side ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (254, N'Darling ear powder ', N'Accessories', 115, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (255, N'Diaper large 10''s', N'Accessories', 13, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (256, N'Diaper medium 10''s', N'Accessories', 12, 35, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (257, N'Diaper small 10''s', N'Accessories', 8.5, 30, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (258, N'Diaper XL 10''s', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (259, N'Diaper XS 10''s ', N'Accessories', 6.5, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (260, N'Diaper medium 18''s', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (261, N'Diaper small 18''s', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (262, N'Diaper XL 18''s', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (263, N'Diaper XS 18''s ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (264, N'Diaper XXL 18''s', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (265, N'Dress cotton small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (266, N'Dono diaper male medium 10''s', N'Accessories', 15, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (267, N'Dono diaper male small 10''s', N'Accessories', 15, 35, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (268, N'E-collar colored #0', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (269, N'E-collar colored all size', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (270, N'E-collar cotton pad small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (271, N'E-Collar cotton pad medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (272, N'E-collar cotton pad large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (273, N'E-collar printed #6', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (274, N'E-collar printed with design #8', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (275, N'E-collar printed with design #10', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (276, N'E-collar printed with design #11 ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (277, N'E-collar printed with design #12', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (278, N'E-collar printed with design #13', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (279, N'E-collar printed with design #14', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (280, N'E-collar transparent #3', N'Accessories', 69, 350, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (281, N'E-collar transparent #2', N'Accessories', 80, 450, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (282, N'E-collar transparent  #1', N'Accessories', 108, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (283, N'E-collar transparent  #7', N'Accessories', 70, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (284, N'E-collar transparent  #5', N'Accessories', 90, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (285, N'Elizabeth collar medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (286, N'Elizabeth collar Large', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (287, N'Finger Toothbrush ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (288, N'Finger Toothbrush (2pcs)', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (289, N'Foldable cage', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (290, N'Round hnadle chain 2.0', N'Accessories', 85, 180, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (291, N'Round hnadle chain 2.5', N'Accessories', 95, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (292, N'Round handle chain 3.0', N'Accessories', 108, 290, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (293, N'Harnesh Bag with leash large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (294, N'Harnesh & Leash Army Large', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (295, N'Harnesh & Leash bee design ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (296, N'Harnesh & Leash Jumbo', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (297, N'Harnesh & Leash Large B', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (298, N'Harnesh & Leash 2.5', N'Accessories', 95, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (299, N'Harnesh & Leash  1.0', N'Accessories', 45, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (300, N'Harnesh & Leash  1.5', N'Accessories', 55, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (301, N'Padded collar plain 2.5', N'Accessories', 50, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (302, N'Padded collar plain 4.0', N'Accessories', 75, 180, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (303, N'Harnesh & Leash  round small', N'Accessories', 109, 280, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (304, N'Harnesh & Leash  round medium', N'Accessories', 178, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (305, N'Harnesh & Leash  round large ', N'Accessories', 215, 480, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (306, N'Harnesh & Leash  round Xlarge ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (307, N'Harnesh & Leash Neon cotton 1.5', N'Accessories', 108, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (308, N'Harnesh & Leash Neon cotton 2.0', N'Accessories', 139, 400, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (309, N'Collar & Leash 1.5', N'Accessories', 55, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (310, N'Dog teether triangle', N'Accessories', 33, 100, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (311, N'Harnesh & Leash reflect small B', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (312, N'Harnesh & Leash small B', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (313, N'Harnesh & Leash stripe flat small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (314, N'Harnesh & Leash with pearl small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (315, N'Harnesh & Leash Crumpled', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (316, N'Jumper Large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (317, N'Jumper medium', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (318, N'Jumper small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (319, N'Jumper XL', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (320, N'Cat toy bell', N'Accessories', 30, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (321, N'Leash Cat', N'Accessories', 55, 180, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (322, N'Dog mug', N'Accessories', 105, 350, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (323, N'Leash Large 2way with handle ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (324, N'Leash Neon short', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (325, N'Leash Rainbow round Large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (326, N'Leash Retractable Large  ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (327, N'Leash Retractable medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (328, N'Leash Retractable small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (329, N'Nail cutter Big', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (330, N'Nail cutter small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (331, N'Necktie', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (332, N'Neem Powder ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (333, N'water feeder bottle', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (334, N'Nursing kit  bottle small', N'Accessories', 45, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (335, N'Pet Brush ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (336, N'Pet Hair cleaner ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (337, N'Pet Ribbon ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (338, N'Pet Towel', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (339, N'Pet wipes ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (340, N'Pill Gun', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (341, N'Play fence ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (342, N'Rope toy twist', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (343, N'Sando #1', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (344, N'Sando #2', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (345, N'Sando #3', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (346, N'Sando #4', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (347, N'Sando #5', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (348, N'Scented Toy rainbow round', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (349, N'Scratch pad (3dots)', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (350, N'Scratch pad (mouse) ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (351, N'Shoes (all size)', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (352, N'Comb wood double red handle', N'Accessories', 55, 160, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (353, N'Comb wood single side ', N'Accessories', 45, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (354, N'Slicker brush boley', N'Accessories', 85, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (355, N'Comb wood double ', N'Accessories', 45, 120, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (356, N'Slicker brush transparent ', N'Accessories', 37, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (357, N'Toothbrush set 12''s', N'Accessories', 10, 50, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (358, N'Toothpaste bioline ', N'Accessories', 79, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (359, N'Toothpaste Dental Care', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (360, N'Toothpaste Dr Pet', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (361, N'Toothpaste Enzymatic ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (362, N'Travel Cage large ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (363, N'Travel Cage medium ', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (364, N'Travel cage small', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (365, N'Water feeder with bottle', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (366, N'Water feeder without bottle', N'Accessories', 55, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (367, N'garbage plastic', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (368, N'Catnip powder', N'Accessories', 15, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (369, N'Styptic stopper', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (370, N'Nail clipper  A2', N'Accessories', 90, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (371, N'Nail clipper A07', N'Accessories', 48, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (372, N'Basin', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (373, N'Feeding Bottle', N'Accessories', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (374, N'Dog bed all size', N'Accessories', 165, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2001, N'Colimoxyn 60ml', N'Drug/Medicine', 129.9, 280, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2002, N'Amoxicillin 250mg capsule (vhellox)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2005, N'Ascorbic Acid Biocee 500mg tab', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2006, N'Ascorbic acid syrup ', N'Drug/Medicine', 22, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2007, N'Arthrox ', N'Drug/Medicine', 21.6, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2008, N'Canisep cream ', N'Drug/Medicine', 298, 500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2009, N'Anicine cream ', N'Drug/Medicine', 438, 600, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2010, N'Cefalexin 500mg capsule (falteria)', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2011, N'Cefalexin 250mg suspension', N'Drug/Medicine', 27, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2012, N'Ketoconazole tablet', N'Drug/Medicine', 18, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2013, N'Cetirizine syrup ', N'Drug/Medicine', 22, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2015, N'Ceterizine 10mg tab', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2014, N'Metergine', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2016, N'Chlorphenamine syrup 60ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2017, N'Chlorphenamine 4mg tablet ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2018, N'Cimetidine 400mg tablet ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2019, N'Co-amoxiclav 125mg suspension ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2020, N'Co-amoxiclav 250mg suspension ', N'Drug/Medicine', 150, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2021, N'Co-amoxiclav 325mg tablet (nahaltin)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2022, N'Co-amoxiclav 625mg tablet ', N'Drug/Medicine', 10.72, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2023, N'Cotrimoxazole 200mg capsule', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2024, N'Cotrimoxazole 200mg suspension ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2025, N'Cotrimoxazole 400mg capsule ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2026, N'Cotrimoxazole 240mg suspension (kathrex)', N'Drug/Medicine', 26, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2027, N'Cotrimoxazole 400mg tablet kathrex', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2028, N'Cotrimoxazole 800mg tablet ', N'Drug/Medicine', 1.6, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2029, N'Tolfedine 60mg ', N'Drug/Medicine', 47, 70, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2033, N'Dexamethasone tablet', N'Drug/Medicine', 1, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2034, N'Doxycycline 100mg capsule', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2035, N'Doxyvet 100mg syrup', N'Drug/Medicine', 315, 500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2036, N'Doxyvet 100mg tablet ', N'Drug/Medicine', 7.95, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2037, N'Methergine 125 mcg tab ', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2038, N'Furazolidone syrup 60ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2039, N'Furosemide 20mg tab (diurin)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2040, N'Lactulose', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2041, N'Menadione Sodium Bisulfate 10mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2042, N'Medbron 120ml syrup', N'Drug/Medicine', 42, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2043, N'Neovax ointment ', N'Drug/Medicine', 414, 650, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2044, N'Methozine syrup', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2045, N'Metoclopramide syrup ', N'Drug/Medicine', 18, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2046, N'Metoclopramide 10mg tablet ', N'Drug/Medicine', 0.35, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2047, N'Metronidazole 125mg suspension ', N'Drug/Medicine', NULL, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2048, N'Metronidazole 500mg tablet ', N'Drug/Medicine', 0.94, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2054, N'Tranexamic Acid 500mg capsule', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2058, N'Papi iron 120ml syrup', N'Drug/Medicine', 182, 320, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2059, N'Refamol 500mg bottle', N'Drug/Medicine', 324, 650, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2060, N'Refamol 500mg per piece', N'Drug/Medicine', 11, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2061, N'Pet tabs 180 tabs', N'Drug/Medicine', 6.1, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2062, N'Coatshine syrup', N'Drug/Medicine', 237.6, 400, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2063, N'Septoplex gel', N'Drug/Medicine', 281, 450, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2064, N'LC vit syrup 120ml', N'Drug/Medicine', 113.89, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2065, N'LC Vit OB bottle ', N'Drug/Medicine', 229.68, 400, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2066, N'LC Vit OB tablet', N'Drug/Medicine', 7.6, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2067, N'Megaderm 8ml', N'Drug/Medicine', 28, 60, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2068, N'Megaderm 4ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2071, N'Immunol syrup', N'Drug/Medicine', 560, 850, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2072, N'Immunol tablet  bottle', N'Drug/Medicine', 440, 700, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2073, N'Immunol tablet  piece', N'Drug/Medicine', 7.3, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2079, N'LC Vit syrup', N'Drug/Medicine', 113.89, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2080, N'Liv-52 tablet  bottle', N'Drug/Medicine', 400, 700, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2081, N'Liv-52 tablet  piece', N'Drug/Medicine', 6.6, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2083, N'Liverolin syrup', N'Drug/Medicine', 195, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2084, N'Love drop ', N'Drug/Medicine', 6.5, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2086, N'Methiovet 500mg battle ', N'Drug/Medicine', 435, 750, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2087, N'Methiovet 500mg per tablet', N'Drug/Medicine', 16, 30, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2088, N'Mondex big ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2089, N'Mondex small ', N'Drug/Medicine', 46, 85, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2090, N'Nutrical tablets', N'Drug/Medicine', 3.6, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2091, N'Nefrotec bottle', N'Drug/Medicine', 400, 700, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2092, N'Nefrotec tablet ', N'Drug/Medicine', 6.6, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2093, N'Neuronerve tablet ', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2094, N'Nutrical syrup 120ml ', N'Drug/Medicine', 151.2, 350, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2095, N'Deltacal tablets', N'Drug/Medicine', 3.6, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2096, N'Deltacal bottle ', N'Drug/Medicine', 180, 400, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2097, N'Papi Iron', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2099, N'Papi OB', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2102, N'Puppy Love 1.8kg', N'Drug/Medicine', 214.16, 350, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2103, N'Puppy Love 600g', N'Drug/Medicine', NULL, 600, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2104, N'Ranitidine HCI 150mg (ranitein)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2108, N'Glucosamine tablet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2110, N'Advocate Dog 10-25kgs', N'Drug/Medicine', 349.6, 600, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2111, N'Advocate Cat', N'Drug/Medicine', 296.87, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2112, N'Advocate Dog medium 4-10kg', N'Drug/Medicine', 305.91, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2113, N'Advocate Dog small 4kg to less', N'Drug/Medicine', 282.7, 500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2114, N'Advocate Dog 25 up', N'Drug/Medicine', 496.12, 700, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2115, N'Bravecto Large 10-20kg', N'Drug/Medicine', 928, 1500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2116, N'Bravecto medium 4.5-10kg', N'Drug/Medicine', 928, 1500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2117, N'Bravecto small 2-4.5kg', N'Drug/Medicine', 928, 1500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2118, N'Bravecto XL', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2119, N'Frontline spot Large', N'Drug/Medicine', 341.3, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2120, N'Frontline spot on medium ', N'Drug/Medicine', 324, 500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2121, N'Frontline spot on small', N'Drug/Medicine', 307, 450, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2122, N'Frontline  Cat', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2123, N'Frontline spray 100ml', N'Drug/Medicine', 540, 850, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2124, N'Frontline spray 250ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2125, N'Heartgard Large ', N'Drug/Medicine', 158.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2126, N'Heartgard medium', N'Drug/Medicine', 139.8, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2127, N'Heartgard small', N'Drug/Medicine', 121.6, 200, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2129, N'Nexgard 25-50kls', N'Drug/Medicine', 360.6, 600, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2130, N'Nexgard  10-25kls', N'Drug/Medicine', 406.6, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2131, N'Nexgard  4-10kls ', N'Drug/Medicine', 381.3, 500, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2132, N'Nexgard 2-4kls ', N'Drug/Medicine', 342.6, 450, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2133, N'Chloramphenicol eye (vistachlor)', N'Drug/Medicine', 104, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2134, N'Chloramphenicol Otic (chloro-V)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2135, N'Aurizon ear drop', N'Drug/Medicine', 709, 850, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2136, N'Ear Doctor', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2137, N'Earmiticide', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2138, N'Eye Doctor', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2139, N'Gentamicin Eye/Ear ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2140, N'Mebradex eye drops', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2141, N'Neodex-V', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2142, N'Nutri-Vet Ear Cleanse', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2143, N'Nutri-Vet Eye Rinse', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2144, N'Orbidex Eye drops ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2145, N'Otiderm ear drop ', N'Drug/Medicine', 308, 450, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2146, N'Tobramicin eye drops', N'Drug/Medicine', 84, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2147, N'Tobramicin+Dexa eye drops (Orbidex)', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2148, N'Naturvet earwash 118ml ', N'Drug/Medicine', 345, 650, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aurizon ear drop', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2149, N'Vetnoderm soap ', N'Others', 80, 160, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2150, N'Pet cologne deodorant ', N'Others', 288, 400, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2151, N'Mycocide shampoo', N'Others', 320, 550, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2152, N'Bearing 150ml red', N'Others', 107.5, 225, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2153, N'Bearing 150ml green', N'Others', 107.5, 225, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2154, N'Bearing 150ml pink', N'Others', 107.5, 225, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2155, N'Bearing 150ml orange', N'Others', 107.5, 225, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2156, N'Bearing 150ml white', N'Others', 107.5, 225, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2157, N'Bearing 300ml red', N'Others', 167.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2158, N'Bearing 300ml green', N'Others', 167.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2159, N'Bearing 300ml pink', N'Others', 167.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2160, N'Bearing 300ml orange ', N'Others', 167.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2161, N'Bearing 300ml white', N'Others', 167.5, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2162, N'Bearing soap orange', N'Others', 75, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2163, N'Bearing soap pink ', N'Others', 75, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2164, N'Nova pink 100ml ', N'Others', 294, 480, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2165, N'Grooming shampoo', N'Others', 725, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2166, N'Top tail MDC fresh mint soap ', N'Others', 90, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2167, N'Top tail MDC sweet apple soap ', N'Others', 90, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2168, N'Top tail MDC oatmeal soap ', N'Others', 90, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2169, N'Top tail MDC shine and soft soap ', N'Others', 90, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2170, N'Top tail MDC odor absorber soap ', N'Others', 90, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2171, N'Top tail MDC mint fresh shampoo', N'Others', 265, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2172, N'Top tail MDC sweet apple shampoo', N'Others', 265, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2173, N'Top tail MDC oatmeal shampoo', N'Others', 265, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2174, N'Top tail MDC shine and soft shampoo', N'Others', 265, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (2175, N'Top tail MDC odor absorber shampoo', N'Others', 265, 380, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1001, N'Atrosite 50ml injectable ', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1002, N'Atropine  ampule', N'Injectible', 12.5, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1003, N'Belamyl 100ml', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1004, N'Calcium 100ml injectable', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1005, N'Coforta 100ml injectable', N'Injectible', 960, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1006, N'Vimekat 100ml injectable ', N'Injectible', 845, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1007, N'D50 50ml injectable ', N'Injectible', 60, 150, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1008, N'Dectomax', N'Injectible', 665, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1009, N'Doxycycline HCI injectable', N'Injectible', 495, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1010, N'Duphalyte 500ml injectable', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1011, N'Enrofloxacin 100ml injectable ', N'Injectible', 2680, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1012, N'Enrofloxacin 50ml injectable ', N'Injectible', 650, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1013, N'Epinephrine ampule 10''s', N'Injectible', 25, 70, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1014, N'Furosemide ampule 10''s', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1015, N'Hepavita 100ml injectable', N'Injectible', 420, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1016, N'Lidocaine HCI 50ml', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1017, N'Forcobsa 100ml injectable ', N'Injectible', 719, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1018, N'Metoclopramide ampule 10''s', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1019, N'Metronidazole IV 100ml', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1020, N'Ornipural 100ml injectable ', N'Injectible', 799, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1021, N'Phytomenadione ampule 10''s', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1022, N'Ranitidine ampule 10''s', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1023, N'Septotryl 100ml injectable ', N'Injectible', 717, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1024, N'Tolfenamic Acid 100ml injectable', N'Injectible', 2687, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1025, N'Tranexamic ampule 10''s', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1026, N'Vitamin A, D3,E 100ml injectable', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1027, N'Vitamin K 50ml', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1028, N'Vitamin K ampule', N'Injectible', 1280, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1029, N'Zoletil-50 ', N'Injectible', 1280, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1030, N'Dexamethasone 100ml', N'Injectible', 687, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1031, N'Capropain tab', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1032, N'Ascorbic ampule ', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1033, N'Xylazine injectable', N'Injectible', 3500, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1034, N'Oxytocin ampule', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1036, N'Adhesive tape tube ', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1037, N'Alcohol Gallon', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1038, N'Catheter Dog', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1039, N'Catheter Cat without stylet ', N'Medical Supply', 230, 750, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1040, N'Chromic 1/0 (without needle)', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1041, N'Chromic 2/0', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1042, N'Cotton roll', N'Medical Supply', 160, 160, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1043, N'Clipper oil', N'Medical Supply', 100, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1044, N'Dextrose ( red)', N'Medical Supply', 75, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1045, N'Dextrose (green)', N'Medical Supply', 75, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1046, N'Dextrose (orange)', N'Medical Supply', 75, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1047, N'Dextrose (pink)', N'Medical Supply', 75, 300, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1048, N'Dextrose (violet)', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1049, N'Edtha tube 3ml 100''s', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1050, N'Ethicon all size', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1051, N'Face mask', N'Medical Supply', 0.75, 5, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1052, N'Lube ', N'Medical Supply', 150, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1053, N'Gauze roll', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1054, N'Gloves small', N'Medical Supply', 180, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1055, N'Hydrogen Peroxide Gallon', N'Medical Supply', 350, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1056, N'IV Cannula violet', N'Medical Supply', 17, 50, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1057, N'IV Cannula yellow', N'Medical Supply', 15, 45, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1058, N'IV Cannula blue', N'Medical Supply', 17, 50, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1059, N'Micropore 1 inch', N'Medical Supply', 420, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1060, N'Mineral Oil', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1061, N'Chromic gut 0', N'Medical Supply', 23, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1062, N'Polyglactin 0/0', N'Medical Supply', 84, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1063, N'Polyglactin 2/0 ', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1064, N'Polyglactin 4/0 ', N'Medical Supply', 84, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1065, N'Povidone Iodine Gallon ', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1066, N'Silk 1/0', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1067, N'Silk 2/0', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1068, N'Slides', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1069, N'Surgical Blade #15', N'Medical Supply', 3, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1070, N'Surgical Blade #10', N'Medical Supply', 3, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1071, N'Surgical Cap ', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1072, N'Syringe 10ml', N'Medical Supply', 1.75, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1073, N'Syringe 1ml ', N'Medical Supply', 1.75, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1074, N'IV Line adult', N'Medical Supply', 15, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1075, N'Syringe 3ml', N'Medical Supply', 1.75, 10, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1076, N'Thermometer', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1077, N'Umbilical cord', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1078, N'IV Line pedia ', N'Medical Supply', 15, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1079, N'Underpads 10''s 60/90', N'Medical Supply', 15, 25, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1080, N'Pee pad ', N'Medical Supply', NULL, 15, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1081, N'Vicryl 1/0', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1082, N'Vicryl 2/0', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1083, N'Vicryl all types', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1084, N'Cotton swab ', N'Medical Supply', 35, 130, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1085, N'Betadine 1 gal ', N'Medical Supply', 680, 680, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1087, N'Nematocide syrup 15ml ', N'Drug/Medicine', 87.12, 250, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1088, N'Nematocide syrup 60ml ', N'Drug/Medicine', 284.4, 480, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1089, N'Proxantel bottle ', N'Drug/Medicine', 1944, 3000, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1090, N'Proxantel tablet', N'Drug/Medicine', 39, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1091, N'Canine wormer tablet', N'Drug/Medicine', 17, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1092, N'Drontal suspension', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1093, N'Drontal tablet ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1095, N'Biocan M', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1096, N'Biofel 3in1', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1097, N'Bronchicine ', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1098, N'Canglob Distemper ', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1099, N'Canglob Parvo', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1100, N'5in1  vaccine', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1101, N'Nobivac 4in1 ', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1102, N'6n1 vaccine', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1103, N'Anti Rabies (nobivac)', N'Vaccination', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1106, N'Parvo test kit 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1107, N'Dermathophytes test 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1108, N'Distemper test kit 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1109, N'Ehrlichia test kit 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1110, N'Heartworm test kit 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (1111, N'Corona/Parvo test kit 10''s', N'Laboratory Test & Kits', NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[ImportItem_ClinicaVeterinariaDeFigura_20210226] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO

DECLARE @ClinicaVeterinaria_ID_Company INT = 44

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  CurrentInventoryCount,
  Old_item_id)
SELECT 
   @ClinicaVeterinaria_ID_Company
  ,LTRIM(RTRIM(record.Name))  
  ,record.[Selling Price]         
  ,2
  ,record.[Buying Price]  
  , ic.ID
  , NULL
  , 1
  , 'Imported on 2021-02-26 04pm'
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , record.[Current Inventory Count]
  , NULL
FROM [ImportItem_ClinicaVeterinariaDeFigura_20210226] record LEFT JOIN tItemCategory ic on record.Category = ic.Name 
WHERE 
	ic.ID_ItemType = 2 ORDER BY record.Name 


Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory (
	[Code]
	,[ID_Item]
	,[Quantity] 
	,[UnitPrice]
	,[DateExpired]
	,[BatchNo]
	,[ID_FilingStatus]
	,[ID_Company]
	,[Comment]
	,[IsAddInventory]
)
SELECT  'Imported on 2021-02-26 4:30pm'
		,ID
		,CurrentInventoryCount
		,UnitPrice
		,OtherInfo_DateExpiration
		,NULL
		,3
		,@ClinicaVeterinaria_ID_Company
		,'Imported on 2021-02-26 4:30pm'
		,1
FROM    titem WHERE ID_Company = @ClinicaVeterinaria_ID_Company and  Comment  = 'Imported on 2021-02-26 4:30pm'
AND ISNULL(CurrentInventoryCount,0) > 0
 

exec pReceiveInventory @adjustInventory, 1



IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'ImportItem_ClinicaVeterinariaDeFigura_20210226'))
BEGIN

	Drop TABLE [ImportItem_ClinicaVeterinariaDeFigura_20210226]
END


