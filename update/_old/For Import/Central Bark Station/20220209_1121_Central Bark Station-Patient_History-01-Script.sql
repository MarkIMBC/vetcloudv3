DEclare @GUID_Company VARCHAR(MAX) = '182C8145-DE36-4986-8A93-F86CE52BA062'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImportSOAP TABLE
  (
     ID_Patient_SOAP    INT,
     ID_Patient         INT,
     ID_Client          INT,
     Old_pet_id         varchar(500),
     Old_history_id     varchar(500),
     Name_Patient       varchar(500),
     Date               DateTime,
     AttendingPersonnel varchar(500),
     History            varchar(MAX)
  )

INSERT @forImportSOAP
       (Old_pet_id,
        Old_history_id,
        Name_Patient,
        Date,
        AttendingPersonnel,
        [History])
SELECT [pet_id],
       [history_id],
       [pet_name],
       [datetime],
       [vet_name],
       [history]
FROM   [CentralBarkStation]

Update @forImportSOAP
SET    ID_Patient_SOAP = patientSOAP.ID
FROm   @forImportSOAP import
       inner join tPatient_SOAP patientSOAP
               on import.Old_history_id = patientSOAP.old_history_id
WHERE  ID_Company = @ID_Company

Update @forImportSOAP
SET    ID_Patient = patient.ID,
       ID_Client = patient.ID_Client
FROm   @forImportSOAP import
       inner join tPatient patient
               on import.Old_pet_id = patient.Old_patient_id
WHERE  ID_Company = @ID_Company

INSERT INTO [dbo].[tPatient_SOAP]
            ([ID_Company],
             [ID_Patient],
             [ID_Client],
             old_history_id,
             [History],
             [Date],
             [ID_SOAPType],
             [ID_FilingStatus],
             [IsActive],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT @ID_Company,
       ID_Patient,
       ID_Client,
       Old_history_id,
       History,
       Date,
       NULL,
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   @forImportSOAP
WHERE  ID_Patient_SOAP IS NULL

SELECT *
FROM   @forImportSOAP 
