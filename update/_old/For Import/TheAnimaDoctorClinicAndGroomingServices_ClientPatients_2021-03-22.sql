/****** Object:  Table [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]    Script Date: 3/22/2021 5:35:39 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]') AND type in (N'U'))
DROP TABLE [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]
GO


CREATE TABLE [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322](
	[Owner's ID / Code] [nvarchar](255) NULL,
	[Owner's Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Contact Number] varchar(MAX) NULL,
	[Email Address] [nvarchar](255) NULL,
	[Pet's Name (Pet 1/Pet 2)] [nvarchar](255) NULL,
	[tempRowID] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N' Lomboy Rosario', NULL, 9194097863, NULL, N'Mai-Mai', 1)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N' Te Sharon', N'Buhangin, Baler Aurora', 9394824246, NULL, N'Suri', 2)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abacan Jois', NULL, 9972893366, NULL, N'Cali', 3)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abacan Jois', NULL, 9972893366, NULL, N'Dariko', 4)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abad Vangie', NULL, 9173280617, NULL, N'Bella/ Mochi/ Kiwi', 5)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abalos Liberty', NULL, 9757055110, NULL, N'Wion/ William/ Lucio', 6)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abanilla Esther', N'Dinadiawan, Dipaculao Aurora', 9194091574, NULL, N'Tank', 7)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abdelrezek Sarah', NULL, 9618321025, NULL, N'Cassie/Clyde/ Mochi', 8)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abenone Charity', NULL, 9465799161, NULL, N'Waffle/ Zoey/ Toffy', 9)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abion Olga', NULL, 9186795802, NULL, N'Rowan/ Lyan', 10)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abul Tricia', N'Brgy. Sabang, Baler Aurora', 9065757467, NULL, N'Forest', 11)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Abutog Ronald', NULL, 9104714740, NULL, N'Bagani', 12)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acilla Jericho', N'Baler aurora', 9395693002, NULL, N'Princezz', 13)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acosta Jazzelle', NULL, NULL, NULL, N'Tam-Tam', 14)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acosta Leni', N'Dinadiawan, Dipaculao Aurora', 9995585099, NULL, N'Max/ Khaiza', 15)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acosta Robert', NULL, 9090841534, NULL, N'Tristan/ Freya', 16)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acuarez Lorna', NULL, 9104975150, NULL, N'Kesha/ Zeus/ Daisy', 17)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Acueza Divine', NULL, 9385641594, NULL, N'Nike', 18)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Adanza Justin', NULL, 9278467689, NULL, N'Kendall/ Varuz', 19)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ade Bernadeth', NULL, 9196262791, NULL, N'Burito/ Nachoz', 20)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ade Nerlita', NULL, 9270853041, NULL, N'Kulot/ Dave', 21)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Adriano April', NULL, 9473218911, NULL, N'Coco/ Twinkle/ Bella', 22)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Afos Aileen', NULL, 9985721709, NULL, N'Miro/ Coco/ Bunso/ Ginger/ Luna', 23)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aganad Anthony', NULL, 9383713226, NULL, N'Flec/ Rain/ Coco', 24)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agbayani Merly', NULL, 9206169200, NULL, N'Taffie/Sophie/ Goofie/ Maffie', 25)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agpoon Jaycel', NULL, 9354427156, NULL, N'Tucker', 26)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agtong Reynante', NULL, 9474251843, NULL, N'Jake/ Jack', 27)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguila Mark', NULL, 9055161682, NULL, N'Black/ White', 28)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguila Wilma', NULL, 9997133105, NULL, N'Axle/ Tan-Tan', 29)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguilar Dona', NULL, 9481741593, NULL, N'Kisses/ Coco', 30)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguilar Ricky', NULL, 9488212643, NULL, N'Rocky/ Yumi/ Mawi', 31)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguirre Jhee', NULL, 9171061717, NULL, N'Chow2x', 32)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguja Alely', NULL, 9610187494, NULL, N'Muggy', 33)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aguja Michelle', NULL, 9498398377, NULL, N'Max', 34)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agustin Hazel', NULL, 9213825429, NULL, N'Chippy', 35)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agustin Kaye', NULL, 9152477981, NULL, N'Gucci', 36)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Agustin Ronelyn ', NULL, 9152196181, NULL, N'Spike/ ', 37)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alanes Ralph John', NULL, 9303272969, NULL, N'Henry', 38)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alarcon Jack', NULL, 9997430527, NULL, N'Kisses', 39)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alarcon Maricar', NULL, 9656629221, NULL, N'Gutchi', 40)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alatro Nerize', NULL, 9997139689, NULL, N'Daffy', 41)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alban Margie', NULL, 9216052704, NULL, N'Pocco', 42)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcaide Amor', NULL, 9354111751, NULL, N'Shawn', 43)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcalde', NULL, NULL, NULL, N'Conan', 44)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcalde Daisy', NULL, 9507164580, NULL, N'Hailey', 45)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Alianah', NULL, 9302614841, NULL, N'Pretty', 46)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Amele', NULL, 9095126515, NULL, N'Blue', 47)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Donnah', NULL, 9988923232, NULL, N'Fionah', 48)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Harold', NULL, 9087878171, NULL, NULL, 49)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Leomar', NULL, 9282228535, NULL, N'Choey/ Chu-Chu/ Shibu', 50)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcantara Parciso', NULL, NULL, NULL, N'Zion/ Zia', 51)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alcarez Shane', NULL, NULL, NULL, N'Moana', 52)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alejo June', NULL, 9077597104, NULL, N'Kitty/ Poy', 53)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alfa Kate', NULL, 9108885988, NULL, N'Lucky', 54)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alfaro Angelica', NULL, 9653769879, NULL, N'Hershey/ Pepper', 55)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alhambra Leo', N'Baler aurora', 9124077572, NULL, N'Yatchi', 56)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alibin Chester', NULL, 9103464704, NULL, N'Milo', 57)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alipio Rhaine Frances', NULL, 9983503839, NULL, N'Snow', 58)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Almariego Elizabeth', NULL, 9391692311, NULL, N'Keanu/ Kisha', 59)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Almero April', NULL, 9086066445, NULL, N'Muffin', 60)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alona Patricia', NULL, 9509861122, NULL, N'Timon/ Max', 61)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alshelahi Laila', NULL, 9219255485, NULL, N'Maxi/ Charlie', 62)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alvarez Thelma', NULL, 9195132355, NULL, N'Belle', 63)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Alviar Amilito', NULL, 9083356852, NULL, N'Soju', 64)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amarillo Aries', NULL, 9178150583, NULL, N'Moe', 65)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amat Edralyn', NULL, 9998214235, NULL, N'Happy', 66)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amat Melita', NULL, 9467035617, NULL, N'Snow White', 67)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amat Noel', NULL, 9255584932, NULL, N'Althea', 68)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amatorio Flora', NULL, 9123957272, NULL, N'Mimi/ Khen', 69)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amatorio Gloria', NULL, 9206360161, NULL, N'Warrior', 70)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amatorio Kenneth', NULL, 9166103116, NULL, N'Shana', 71)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amatorio Sherwin', NULL, NULL, NULL, N'Zion ', 72)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amazona Allen', NULL, NULL, NULL, N'Labitha', 73)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amazona Flordeliza', NULL, 9998441185, NULL, N'Eumi', 74)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amazona Vicmar', NULL, 9287967558, NULL, N'Prada', 75)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ambol Alcia', NULL, 9369534195, NULL, N'Candy/ Princess/ Pocholo', 76)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amor John April', NULL, 9568014964, NULL, N'Chi-chi', 77)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Amparo Jonas', NULL, 9228069071, NULL, N'Yumi', 78)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ampatin Erlinda', NULL, 9127457356, NULL, N'Ming-go', 79)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Anastacio Jennifer', N'Maria Aurora, Aurora', 9297047410, NULL, N'Snapy/ Spark', 80)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Christine Joy', N'Brgy. Suclayin, Baler Aurora', 9108084285, NULL, N'Balbon', 81)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Irene', NULL, 9126882282, NULL, N'Zyzy', 82)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Michael joseph', NULL, 9187118396, NULL, N'Buster/ Callie/ Max', 83)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Michael joseph', NULL, 9178547850, NULL, N'Goro', 84)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Mike', NULL, 9382286611, NULL, N'Kookie', 85)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Miraflor', NULL, 9203796265, NULL, N'Nono', 86)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ancheta Patrick', NULL, 9198382679, NULL, N'Cassey/ Roldan', 87)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andal Jennifer', NULL, NULL, NULL, N'Skye', 88)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Anding Kazumi', NULL, 9989915571, NULL, N'Asuna', 89)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andrada Timn ', NULL, 9666308103, NULL, N'Enchong/ Toby/ Lambina', 90)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andrei Mel', NULL, 9509828435, NULL, N'Tres', 91)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andres Alfred', NULL, 9122431999, NULL, N'Bimbo/ Bitsy', 92)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andres Leizel', NULL, 9074881029, NULL, N'Siera', 93)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andres Rovelyn', NULL, 9399218741, NULL, N'Majaco', 94)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Andres Ruby', NULL, 9395338449, NULL, N'Liksi', 95)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ang Lacson', NULL, 9397591234, NULL, N'Misty', 96)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ang Lacson', NULL, 9993672643, NULL, N'Enzo/ Lucy/ Nala', 97)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ang Xian', NULL, 9397591234, NULL, N'Zion/Kobe', 98)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ang Zhirome Cheryle', NULL, 9494531380, NULL, N'Moana', 99)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Anne', NULL, 9173411089, NULL, N'Sophia', 100)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Aura', NULL, 9206614125, NULL, N'Red', 101)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Badjeng', NULL, NULL, NULL, N'Puppy James', 102)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Ely', NULL, 9150714537, NULL, N'Moriseth', 103)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Eusebio', NULL, NULL, NULL, N'Matana/ Shishi', 104)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Karen', NULL, 9478919217, NULL, N'Mio', 105)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Rabbi', N'Dipaculao Aurora', 9185107039, NULL, NULL, 106)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Win Joy', N'San Luis Aurora', 9778361876, NULL, N'Maui/ Bruno', 107)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angara Zeroida ', NULL, 9186949053, NULL, N'Mimi', 108)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angeles Alyana Marie', NULL, 9167820043, NULL, N'Pengu/ Liit', 109)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angeles Marilyn ', NULL, 9293135362, NULL, N'Amber', 110)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angelo Avie Leah', NULL, 9217599949, NULL, N'Mikki', 111)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angnano Edwin', NULL, 9994551280, NULL, N'Spyke/ Maxie', 112)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Angyab Julie Anne', NULL, 9472931791, NULL, N'Digong/ Bailey/ Yasha/ H-puppies', 113)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ansalme Mary Ann', NULL, NULL, NULL, N'Goochi', 114)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Anselmo Arnel', NULL, 9501577109, NULL, N'Amihan', 115)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Anthony Joseph ', NULL, 9178545226, NULL, N'Shasha', 116)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Antigua sarah', NULL, 9092112404, NULL, N'Hapy', 117)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Antojado Rolly', NULL, 9215690570, NULL, N'Phinks', 118)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Antolin Mary Abigail', NULL, 9770944267, NULL, N'Rain', 119)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Antonio Aeron', NULL, 9482670330, NULL, N'Chico', 120)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Antonis Donnalyn', NULL, 9074003218, NULL, N'Maxi', 121)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Apalis Emil', NULL, 9276763730, NULL, NULL, 122)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Apigo Yohan', NULL, 9289267888, NULL, N'Limon/ Marica', 123)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Apolonio Georgina', NULL, 9099494131, NULL, N'Maktum', 124)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Apostol Jemimah', NULL, 9237255100, NULL, N'Mocha', 125)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aquino Aldrin', NULL, 9491440135, NULL, N'Fhiepi', 126)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aquino Grace', NULL, 9666614906, NULL, N'Lilia', 127)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aquino Helen', NULL, 9429870659, NULL, N'Britch', 128)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aquino Liliano', NULL, 9286370619, NULL, N'Beagle', 129)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Abriell', NULL, 9770768207, NULL, N'Mangue', 130)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Ariel', NULL, 9294017298, NULL, N'Kelly', 131)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Ariel', NULL, 9294017298, NULL, N'Dino', 132)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Miland', NULL, 9489627070, NULL, N'Kilmen', 133)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Mildred', NULL, NULL, NULL, N'Mikey', 134)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Pepito', NULL, 9294676503, NULL, N'Sophia', 135)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Rey', NULL, 9988596725, NULL, N'Polar', 136)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aragon Reynaldo', NULL, 9288275924, NULL, N'Budo-budo', 137)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Arao Maryjane', NULL, 9061075264, NULL, N'Prince', 138)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Arcilla Hadjie', NULL, 9954087877, NULL, N'Bubbles', 139)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Arguson Emil', NULL, 9083210334, NULL, N'Carson', 140)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ariglado Rhosie', NULL, 9954747479, NULL, N'Charlie', 141)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aril Mary Jane', NULL, 9568015399, NULL, NULL, 142)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Arimbuyutan Ronalyn', NULL, 9560176339, NULL, N'Chantel', 143)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aronzado  Sheila', N'Suklayin, Baler Aurora', 9666415506, NULL, N'JC', 144)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Asan Samson', NULL, 9399154379, NULL, N'Dunchess', 145)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Asilo Ericson', NULL, 9396007716, NULL, N'16', 146)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Asilo Ericson', NULL, 9396007716, NULL, NULL, 147)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Asne Aiza', NULL, 9159585804, NULL, N'Dolche/ Gabana', 148)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aspillaga Rosemarie', NULL, 9187682227, NULL, N'Alpha', 149)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aspillasa Juvy', NULL, 9127874575, NULL, N'Bella', 150)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Asuncion Andrea', NULL, 9183752658, NULL, N'Ichie', 151)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Atienza Lito', NULL, 9568263913, NULL, N'Zyruz/ Chow', 152)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ato Grecelyn', NULL, 9091660513, NULL, N'G.C', 153)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ato Jannah', N'Maria Aurora, Aurora', 9452054335, NULL, N'Creamy/ Cookies/ Rambo', 154)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Atun Giovanni', NULL, 9298255243, NULL, N'Kopi', 155)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Auilo MILo', NULL, NULL, NULL, N'Toothless/ Bench', 156)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Aurora Leonard', N'San Luis Aurora', 9171347896, NULL, N'Pork', 157)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda christian', NULL, 9459658551, NULL, N'Bailey', 158)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Clinton', NULL, 9464918335, NULL, N'Samuel', 159)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Kheem', NULL, 9199912797, NULL, N'Mochow', 160)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Lawrence', NULL, 9483193808, NULL, N'Kimmy/ Toffy', 161)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Mary Grace', NULL, 9338613195, NULL, N'Cobi / Chini', 162)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Maynard', NULL, 9281849918, NULL, N'Chacha', 163)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Rachelle ', NULL, 9077041071, NULL, N'Starlee Thor', 164)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellaneda Rheem', NULL, 9485224239, NULL, N'Yumo', 165)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avellano Jomar', NULL, 9074867454, NULL, N'Kobe', 166)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avenilla Arjan', N'Baler aurora', 9085782542, NULL, N'Boggie / Lila', 167)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Avenilla Arnold', NULL, 9064971510, NULL, N'Harley/ Sunday/ Bixby', 168)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ay-yad Norie', NULL, 9288018882, NULL, N'Kia/ Pan-pan/ Love/ Ice/ Tanos', 169)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Azmatorio Emilio', NULL, 9123592322, NULL, N'Buddy/ Chelsea', 170)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Babastro Dinah', NULL, 9060827066, NULL, N'Golden', 171)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bacquel Anita', NULL, NULL, NULL, N'Potchi', 172)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Baculpo Liu Altair', NULL, 9951865434, NULL, N'Labamba', 173)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Badua jeffrey', NULL, 9384555429, NULL, NULL, 174)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bajas Kristine', NULL, 9776022103, NULL, N'Cashmere', 175)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Balanga Sonny', NULL, 9956107553, NULL, N'Pola', 176)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Balbuena Cynthia', NULL, 9982467716, NULL, N'Trouble', 177)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Baltazar Yasmin', NULL, 9071521648, NULL, N'Alexxis', 178)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Balubal Rizza Joy', NULL, 9123589300, NULL, N'Ping', 179)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Banan Jemima', NULL, 9460401541, NULL, N'Jela', 180)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bandilla Samuel', NULL, 9292586443, NULL, N'Neggy/ Lino/ Diana', 181)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Baniqued Marie', NULL, 9431341473, NULL, N'Oreo', 182)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barbiera Honey Grace', NULL, 9460626745, NULL, N'Sofie', 183)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barcenas Cherwin', NULL, 9124238126, NULL, N'Girl', 184)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barrantes Carylle', N'Baler aurora', 9177756552, NULL, N'Skyflakes', 185)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barro Godfrey', NULL, 9288266545, NULL, N'Pussy/ Samantha', 186)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barrogo Mina', NULL, 9054640674, NULL, N'Jammin', 187)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Barz Lorna', N'Reserva, Baler Aurora', NULL, NULL, N'Kholet/ Spike/ Sam', 188)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Basbas Janneth ', NULL, 9215953633, NULL, N'Chuchay', 189)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Basbas Liway', N'Ditumabo, San Luis Aurora', 9382280336, NULL, N'Brouw', 190)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Basbas Patricia', N'Brgy. Suclayin, Baler Aurora', 9178419620, NULL, N'Potchi', 191)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Basilio Wilson', NULL, 9052276863, NULL, N'Mestisa', 192)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bata Manuel', N'San Luis Aurora', 9999940334, NULL, N'Azhala Grey', 193)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Baturi Tiffany Anne', NULL, 9388058726, NULL, N'Serra', 194)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Aiza', NULL, 9501684741, NULL, N'Marimar', 195)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Connie Grace', N'Baler aurora', 9055635437, NULL, N'Chuchay', 196)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Dianne', NULL, 9178370712, NULL, N'Pona', 197)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Fannie', NULL, 9611375795, NULL, N'Champ', 198)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Jovelyn', NULL, 9085746287, NULL, N'Xao-Xao', 199)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Lorena', NULL, 9383273033, NULL, N'Max/ Chloe', 200)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Mark Thomas', NULL, 9283255184, NULL, NULL, 201)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Mina ', NULL, 9385616147, NULL, N'Ford', 202)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bautista Ryan', N'Suklayin, Baler Aurora', 9178458759, NULL, N'Berta', 203)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bayani Gemma', NULL, 9122341393, NULL, NULL, 204)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bayudan Analyn', N'Maria Aurora, Aurora', 9088903909, NULL, N'Nima', 205)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Belen Gelli Jane', NULL, 9202446140, NULL, N'Makisig', 206)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Belen Lindon', NULL, 9176395989, NULL, N'Coco', 207)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Belgar Sherloin', NULL, 9484358597, NULL, N'Liza', 208)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Berbenzana Ghelyn', NULL, 9150513701, NULL, N'Rexor', 209)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bergantintos Vivienne', NULL, 9153137746, NULL, N'Sassy/ Molly', 210)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Berjay Editha', NULL, 9288690559, NULL, N'Ysabelle', 211)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bermudez Rose', NULL, 9617750286, NULL, N'Bless', 212)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bermudo Warlita', NULL, 9501265859, NULL, N'Chloe', 213)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bernabe Jay-Anne', NULL, 9466717991, NULL, N'Mik-Mik', 214)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bernal Jolina', N'Buhangin, Baler Aurora', 9203881492, NULL, N'Chocolate', 215)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Beronilla Vanessa ', NULL, 9089050176, NULL, N'Sherlock', 216)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Berza Jefferson', N'San Luis Aurora', 9288709035, NULL, N'Tom', 217)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bihasa Marjorie', NULL, 9072898239, NULL, N'Lactum', 218)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bihasa Vivian', NULL, 9209678144, NULL, N'Skies/ Snow/ Sussy', 219)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bihasa Xyrene', NULL, 9997042577, NULL, N'Tsum-Tsum', 220)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Binarao Pitz', N'Brgy. Zabali, Baler Aurora', 9396549855, NULL, NULL, 221)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Binarao Pitz', NULL, 9209032163, NULL, N'Elly/ Kelly/ Clipper/ Jackels', 222)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bitong Adriano Jr.', NULL, 9077529545, NULL, N'Hero Brine', 223)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bitong Sandara', NULL, 9463090014, NULL, N'Chuchai', 224)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bitong Seychelles', NULL, 9565501858, NULL, NULL, 225)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Blasco ZLT', NULL, 9175758007, NULL, N'Porky', 226)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bobes Regie', NULL, 9985620705, NULL, NULL, 227)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bombase Karissa', N'Brgy. Zabali, Baler Aurora', 9173026058, NULL, N'Aurora', 228)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bombeo Lucille', NULL, 9987916379, NULL, N'Ranger/ Scout', 229)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bonamy Vhie', NULL, 9088187954, NULL, N'Jack', 230)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bongabong Rowena', N'Dinalungan Aurora', 9087122695, NULL, N'Min-Min', 231)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bongat Charmaine', NULL, 9982647781, NULL, N'Skip', 232)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bonifacio Jillian', NULL, 9182463512, NULL, N'Odie', 233)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bordo Mary Jane', NULL, 9101912052, NULL, N'Pepper', 234)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bornes Syrie', NULL, 9982760370, NULL, N'Stephanie', 235)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bravo Marivi', NULL, 9199912797, NULL, N'Lily', 236)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Briones Kennelyn ', NULL, 9202443689, NULL, NULL, 237)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Brogueza Jorryll ', NULL, 9650475479, NULL, NULL, 238)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Broqueza Ramon', N'Kinalapan, Baler Aurora', 9985532679, NULL, N'Lebron/ Trixie', 239)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bucud Jun ', NULL, 9198555116, NULL, N'Margaus/ Max', 240)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buenaventura Akhie', N'Brgy. Suclayin, Baler Aurora', 9216051124, NULL, NULL, 241)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buenaventura Jeena', NULL, 9324666528, NULL, N'Ginger/ Pepper', 242)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buencamino Estela', NULL, 9752061909, NULL, N'Hakyuen', 243)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buencamino Ma. Estella', NULL, 9752061909, NULL, N'Yurri', 244)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buenconsejo Annabel', NULL, 9213937712, NULL, N'Blessed', 245)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buenconsejo Grace', NULL, 9218353246, NULL, N'Chuchi', 246)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buenconsejo Jaye Ann', N'Calabuanan, Baler Aurora', 9101456628, NULL, N'Artemis/ Apollo/ Athena', 247)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buendicho Josephine', NULL, 9773917928, NULL, N'Sachi', 248)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bulaclac Ada', NULL, 9751464563, NULL, N'Rowan', 249)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bulado Marlon', NULL, 9481739039, NULL, N'Snarky', 250)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bulambao', NULL, 9212903955, NULL, N'Anggus', 251)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Buscalo Jessa Mae', NULL, 9209045454, NULL, N'Koko/ Blackie', 252)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bustamante Jeth', NULL, 9664668213, NULL, N'Kobey', 253)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Bustamante Ram', NULL, 9171423083, NULL, N'Sai-Sai', 254)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Caballes jogielyn', NULL, 9387111455, NULL, N'Tobias', 255)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabanayan Zener', N'Suklayin, Baler Aurora', 9298251201, NULL, N'Mik', 256)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabangan Lyka', NULL, 9151921982, NULL, N'Mikmik', 257)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabansag Eunice', NULL, 9187641018, NULL, N'Shibo', 258)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabayanan Zener', NULL, 9097792813, NULL, N'Euro', 259)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabebe Sharmaine', NULL, 9561328691, NULL, N'Dave', 260)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabling Joseph', NULL, 9190037252, NULL, N'Tommy', 261)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabrales Consuelo', NULL, 9128111650, NULL, N'Jeje', 262)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabunoc Jella', NULL, 9383713444, NULL, N'Lollipop', 263)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cabuyoc Bernard', NULL, 9166677125, NULL, N'Khendra', 264)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cacanindin Arnold', NULL, 9207042125, NULL, N'Plaffy', 265)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cacanindin Lerma', NULL, 9959450927, NULL, N'Pickels', 266)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cacanindin Liza', N'San Luis Aurora', 9508663407, NULL, N'Bom-Bom', 267)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cacanindin Ylayza', NULL, 9213264849, NULL, N'Georgeena', 268)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cachuela Zharieka', N'Dipaculao Aurora', 9284058928, NULL, N'Bailey', 269)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cagbay Loida', NULL, 9463988631, NULL, N'Bella', 270)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Calderon Rinalyn', NULL, 9105008821, NULL, N'Dodo/ Dada', 271)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Calonge Cecille', NULL, 9505321881, NULL, N'Koi', 272)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Calugtong Jay', NULL, 9099299612, NULL, N'Cyber', 273)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'camacho Gemalyn', NULL, 9273795016, NULL, N'Neutron', 274)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Camading Evelyn', N'Dipaculao Aurora', 9499019042, NULL, N'Mikay/ Gummy', 275)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Camara Hannelore', N'Buhangin, Baler Aurora', 9489467118, NULL, N'Ashraf', 276)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Campos Rosenda', NULL, 9993996709, NULL, N'Blessie/ Gwapo', 277)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Canete Sydney', N'Baler aurora', 9295370793, NULL, N'Sam', 278)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Canibaling Rosabel', NULL, 9165754505, NULL, N'Panda', 279)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Canibiran Ramon', NULL, 9491415162, NULL, N'Purple/ Kayser', 280)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Capin Annie', NULL, 9297859350, NULL, N'Toothless/ Ella/ Egzie', 281)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cara Mark Bryan', NULL, 9383438807, NULL, N'Myungjin', 282)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Carpio Santiago', NULL, 9216097790, NULL, N'Hammer', 283)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Carrasco Jeff', NULL, 9273760120, NULL, N'Mucho', 284)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Carriaga John Paul', NULL, 9153966755, NULL, N'Belgie', 285)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Carriedo Rowena', NULL, 9218352783, NULL, N'Sovay', 286)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Carter Mary Grace', N'Buhangin, Baler Aurora', 9298675698, NULL, N'Oreo', 287)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Caruluan Hannalyn', NULL, 9669685888, NULL, N'Jackie ', 288)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cas Jonabeth', NULL, 9485354592, NULL, N'Sunget', 289)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Casandy Rima', NULL, 9195519652, NULL, N'Max', 290)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Casannis Jomar', NULL, 9107864793, NULL, N'Yuki', 291)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Casilig Angela', NULL, 9560959891, NULL, N'Miya', 292)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Castillo Moly', NULL, NULL, NULL, N'Kulingling', 293)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Catipon Jonalyn', NULL, 9052782460, NULL, N'Chinchin', 294)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Caytap Lloise', NULL, 9457146555, NULL, N'Conan', 295)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Celebre Ana Marie', N'San Luis Aurora', NULL, NULL, N'Misty', 296)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Celindro Mary Grace', NULL, 9667228567, NULL, N'Chow', 297)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cerezo Julius', NULL, 9365438361, NULL, N'ParuParu', 298)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cervantes Randylyn', NULL, 9085854716, NULL, N'Patchi', 299)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Chan Lani', N'Nonong, San Luis Aurora', 9392345087, NULL, N'Bean', 300)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Chaves Yasmin', NULL, 9455338162, NULL, N'Sporky', 301)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Clara Myra', N'Brgy. Suclayin, Baler Aurora', 9128802461, NULL, N'Bruce', 302)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Claro Marie', N'San luis Aurora', 9294079413, NULL, N'Harry', 303)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Clemente Catherine', N'Pingit, Baler Aurora', 9994109795, NULL, N'Jongga ,Janggo', 304)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Clemente Raymond', N'Brgy. Suclayin, Baler Aurora', 9397424171, NULL, N'Brandy', 305)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Climatu Edison', NULL, 9204971642, NULL, N'Yassy', 306)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Climatu Emerson', NULL, 9357504552, NULL, N'Kc', 307)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cole Joseph', NULL, 9088692485, NULL, N'Gotze', 308)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Collado Dave', NULL, 9487898307, NULL, N'Butegteg', 309)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Collado Roberto', NULL, NULL, NULL, N'Tawkil', 310)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Combis Czarina', N'Dipaculao Aurora', 9165036956, NULL, N'Puppy', 311)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Combis Vergilio', NULL, 9123453864, NULL, N'Kelly', 312)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Concepcion Kevin', NULL, 9669686030, NULL, N'Mocha', 313)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cordial Eunhimhar', NULL, 9289784720, NULL, N'Monschi', 314)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cornelio Yolanda', NULL, 9106554360, NULL, N'Frisky', 315)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Corpuz Lolita', NULL, 9176485556, NULL, N'Disney', 316)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Corpuz Sofronio', N'Maria Aurora, Aurora', 9056618746, NULL, N'Bardy', 317)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cosa Mercy', NULL, 9216835561, NULL, N'Akhira', 318)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Costa Ma. Azileira', NULL, 9295944360, NULL, N'Skye', 319)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Crayo Lenie', N'San luis Aurora', 9202309948, NULL, N'Kimber ', 320)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cristobal Emily', NULL, 9175952307, NULL, N'Lyzky', 321)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cruz Patricia', NULL, 9980993665, NULL, N'Maggy', 322)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cryo Lenie', NULL, 9665848757, NULL, N'Cassy', 323)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cuerdo Winnie', NULL, 9197303724, NULL, N'Black,Taba', 324)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Cuntaday Demie', NULL, 9187247618, NULL, N'Zeb', 325)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Curitana Cristine', NULL, 9097966197, NULL, N'GG', 326)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Curitana Xcian', NULL, 9274044240, NULL, N'Angela', 327)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dacgdag Larrydel', NULL, 9482669691, NULL, N'Nammy', 328)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dacquel Edelle ', NULL, 9979538472, NULL, N'Puppies', 329)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dacquel Jedidiah', NULL, 9486552172, NULL, N'Black ', 330)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dacumos Mary Ann', NULL, 9338235553, NULL, N'Yoki', 331)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dacusa Remedios', NULL, 9052646889, NULL, N'Loki', 332)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dadea Yonie', NULL, 9395171001, NULL, N' Milo', 333)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Daguman Carmen', NULL, 9277482919, NULL, N'Gizmo', 334)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Daguman Digna', NULL, 9203452213, NULL, N'Panda', 335)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Daguman Fiona', NULL, 9178622911, NULL, N'Shaper', 336)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Daguman Myrna', NULL, NULL, NULL, N'Uno', 337)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dalson Rose', NULL, 9357120464, NULL, N'Slymer', 338)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dangco Marla', NULL, 9394823984, NULL, N'Oreo', 339)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dasalla Nona', NULL, 9283842707, NULL, N'Dexter', 340)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dasalla Nona Flor', N'Dipaculao Aurora', 9474574188, NULL, N'Taba', 341)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'David Ardee', NULL, 9070643033, NULL, N'Paupau', 342)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'David Cristine', NULL, NULL, NULL, N'Tutchie', 343)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'David Julius', NULL, 9662207275, NULL, N'Maxi', 344)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'David Realyn', NULL, 9382003708, NULL, N'Oyo Boy', 345)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dayot Vanessa', NULL, 9954320354, NULL, N'Natnat', 346)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Asis Erwin', NULL, 919253417, NULL, N'Snow', 347)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'DE Dios Famela', NULL, 9219250072, NULL, N'Jamie', 348)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Guzman Izi', NULL, 9302516475, NULL, N'Gucci', 349)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Guzman Nestor', NULL, 9186328633, NULL, N'Catriona G.', 350)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Guzman Nora', NULL, 9163373048, NULL, N'Veorgie', 351)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'DE Guzman Rosalee', NULL, 9485406025, NULL, N'Shitsui', 352)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Leon Aj', NULL, 9461148618, NULL, N'Bryner', 353)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Leon Florence', NULL, 9993032949, NULL, N'Nam Kee', 354)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Leon Kristine', NULL, 9291934059, NULL, N'Puppies', 355)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Leon Suzuth', NULL, 9300587145, NULL, N'Chooey', 356)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Luna Queenie', NULL, 9293473991, NULL, N'Molly', 357)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Luna Shiela Mia', NULL, 9281852214, NULL, N'Bruno', 358)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Mesa Val', NULL, NULL, NULL, N'Nuggets', 359)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Monte Myreen', NULL, 9193255596, NULL, N'Akirho', 360)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Pablo Pia Framchesca', NULL, 9302786834, NULL, N'Smidget', 361)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Tagle Miel', NULL, 9175486956, NULL, N'Blacky', 362)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Vera John', N'Ditumabo, San Luis Aurora', 9055578021, NULL, N'Lucky', 363)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Villa Nestor', NULL, 9205646589, NULL, N'Kody', 364)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'De Vina Alicia', N'Maria Aurora, Aurora', 9481121113, NULL, N'Loki', 365)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deauna Colyn', NULL, 9452653399, NULL, N'Sausage', 366)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dejoras Flos', NULL, 9392882930, NULL, N'Sweety', 367)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Del Rosario April', NULL, 9757700379, NULL, N'Ryle', 368)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Del Rosario Geraldine', NULL, 9125459337, NULL, N'Ganda', 369)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Angelo', NULL, 9354677877, NULL, N'Nido', 370)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Donald', N'Casiguran Aurora', 93936122759, NULL, N'Dambo', 371)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Donato', NULL, 9088666512, NULL, N'Chelsea', 372)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Irene', N'Brgy. Suclayin, Baler Aurora', 9287575372, NULL, N'Milo', 373)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Jocelyn', NULL, 9071032468, NULL, N'Puppies', 374)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Raffa Catherine', NULL, 9084519533, NULL, N'Akira', 375)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Cruz Sylvia', NULL, 961755662, NULL, N'Blacky', 376)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Pena Margarita', N'Brgy. Suclayin, Baler Aurora', 9280394611, NULL, N'Rue', 377)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Pena Tristan', NULL, 9988891090, NULL, N'Miho', 378)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Rosa Cynthia', NULL, 9754657952, NULL, N'TomJones', 379)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre', NULL, 9274411884, NULL, N'Pupies', 380)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre Allan', NULL, 9296778134, NULL, N'Tony', 381)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre Jazreel', NULL, 9555489598, NULL, N'Coca', 382)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre Marry Joy', NULL, 9128854682, NULL, N'Dice', 383)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre Nora', NULL, 9983537680, NULL, N'Eva', 384)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Torre Raymond', NULL, 9672281123, NULL, N'Sausage ', 385)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dela Vega Veneranda', NULL, 9182906035, NULL, N'Snow', 386)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delante Steven Melvin II', NULL, 9562318092, NULL, N'Whity', 387)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deleon Cevilla', NULL, 9173967888, NULL, N'Choco', 388)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deleon Charo', NULL, 9072899905, NULL, N'Nami', 389)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deleon Cristine', N'Sabang, Baler Aurora', 9507244002, NULL, NULL, 390)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deleon Rogel', NULL, 9983428639, NULL, N'Pixie', 391)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delfin Gretchen', NULL, 9125800413, NULL, N'Robin', 392)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deliso Evelyn', NULL, 9083936417, NULL, N'Jela', 393)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Reyes Allan', NULL, 9178163338, NULL, N'Ginger', 394)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Reyes Jose', NULL, 9178530179, NULL, NULL, 395)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Reyes Rodessa', NULL, 9615505993, NULL, N'Nengski', 396)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Agnes', NULL, 9308920631, NULL, N'Blacky', 397)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Aira', NULL, 9501582686, NULL, N'Aso ', 398)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Ariane', NULL, 9513425606, NULL, N'Sherlock', 399)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Daniel', NULL, 9288690179, NULL, N'Sophie', 400)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Dyza', NULL, 9999968243, NULL, N'Toby', 401)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Imee', NULL, 9508128979, NULL, N'Chuchay', 402)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Joseph', NULL, 9107752794, NULL, N'Shky', 403)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Judy Ann', NULL, 9667312695, NULL, N'Fujiko', 404)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Remedios', NULL, 9499968743, NULL, N'Shiny', 405)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Remz', NULL, 9999968243, NULL, N'Cookie', 406)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Delos Santos Richard', N'Maria Aurora, Aurora', 9065764785, NULL, N'Travis', 407)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Denzo Anne', N'Baler aurora', 9288710172, NULL, NULL, 408)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deri Denalyn', NULL, 9953440639, NULL, N'Benden', 409)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Deri Marites', NULL, 9294599369, NULL, N'Sabana', 410)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Desiree', N'Baler Aurora', 9106087190, NULL, N'Shadow', 411)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Desiree Lara', NULL, 9178416626, NULL, N'Ryle', 412)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Alexandra', NULL, 9494599160, NULL, N'Pompom', 413)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Beth', NULL, 9091666227, NULL, N'Clover', 414)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Diane', NULL, 9105288994, NULL, N'Cobie', 415)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Joshua', NULL, 9950336341, NULL, N'Sharla', 416)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Myra', NULL, 9076428643, NULL, N'Kisses', 417)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Paulene', NULL, 9651503935, NULL, N'Shawie', 418)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Devera Vicente', NULL, 9288771501, NULL, N'Wacky', 419)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diaz Denisse Nicolle', NULL, 9203070909, NULL, N'Scarlet', 420)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diaz Jahleen', NULL, 9063529738, NULL, N'Tommy', 421)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diaz Jerome', NULL, 9175554361, NULL, N'Cassie', 422)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diaz Mercy', NULL, 9301616758, NULL, N'Princeky', 423)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diaz Nicolle', NULL, 9951376144, NULL, N'Shiro', 424)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dida Leodegano', NULL, NULL, NULL, N'Apple', 425)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Diferia May', N'Pingit, Baler Aurora', 9276838084, NULL, N'Gabo', 426)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dimacutac Franklin', NULL, 9267466905, NULL, N'Scarlet', 427)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dimalumio Yolanda', NULL, NULL, NULL, N'Spike', 428)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dizon Ian Jay', NULL, 9473474783, NULL, N'Inu', 429)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dizon Kyle Gavin', NULL, 9154714157, NULL, N'Chestnut', 430)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dizon Loraine', N'San luis Aurora', 9183018078, NULL, N'Bruno', 431)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dizon Mariano', NULL, 9123715864, NULL, N'Flexi', 432)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Docena Harriet', NULL, 9054991982, NULL, N'Pogi/Shogo', 433)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Anton', NULL, 9668673766, NULL, N'Hurley', 434)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Barbara', NULL, 9189127496, NULL, N'Browny', 435)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Edgardo', NULL, 9177798237, NULL, N'Lucky', 436)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Jay-Ar', NULL, 9753725442, NULL, N'Oreo', 437)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Kristine Rose', NULL, 9281857675, NULL, N'Traus', 438)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Uera', NULL, 9399086647, NULL, N'Pylor', 439)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Vera', N'Maria Aurora, Aurora', 9399086047, NULL, N'Tamey', 440)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domingo Willie', NULL, 9283903518, NULL, N'Jake', 441)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Domino Josefina', NULL, 9183759669, NULL, NULL, 442)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Donato Christine', NULL, 9062029398, NULL, N'Mallows', 443)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Donato Jaymar', NULL, 9108081075, NULL, N'Buck', 444)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Donato Lhen', NULL, 9305594749, NULL, N'Jasper', 445)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Donato Vida', NULL, 9216052158, NULL, N'Snow', 446)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dorado Gerald', NULL, 9997349990, NULL, N'Samu', 447)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Doringo Angel', NULL, NULL, NULL, NULL, 448)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Doringo Seychelles', NULL, NULL, NULL, N'Milo', 449)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Drine/Kc', NULL, 9190740544, NULL, N'Shadow', 450)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duarte Jessica', NULL, 9773280921, NULL, N'Gabby', 451)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duarte May', NULL, 9101343146, NULL, N'Trixie', 452)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duaso Entico', NULL, NULL, NULL, N'Uno', 453)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duaso Evelyn', NULL, 9127467855, NULL, N'Kira', 454)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duaso Joanne', NULL, 9212629261, NULL, N'Rambro', 455)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duaso Loruel', NULL, 9953346165, NULL, N'Chase', 456)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ducusin Marife', NULL, 9995266970, NULL, N'Princess', 457)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dukha Camille', N'San Luis Aurora', 9175473384, NULL, N'Diggy', 458)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dukha Emilia', NULL, 9204356919, NULL, N'Yoco', 459)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulay Agnes', NULL, 9090841546, NULL, NULL, 460)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulay Anita', NULL, 9287967121, NULL, N'Bon-Bon', 461)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulay Jackielyn', NULL, 9128829479, NULL, N'Bob', 462)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulay Melvin', NULL, 9461805920, NULL, N'Sheera', 463)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulla June', NULL, 9097123170, NULL, NULL, 464)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dulnuan Bryan', NULL, 9754657460, NULL, N'Eg, Paslan', 465)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumandal Joepher', NULL, 9153780092, NULL, N'Dorie', 466)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumandal Johanna joy', NULL, 9951294263, NULL, N'Willow', 467)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumandal JoJo Jr.', NULL, NULL, NULL, N'Haru', 468)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumandal Jonathan', NULL, 9207586557, NULL, N'Cookie', 469)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumandal Maritz', NULL, 9169248502, NULL, N'Pirincess', 470)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumlao Denmer', NULL, 9208669070, NULL, N'Oreo', 471)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumlao Gloria', NULL, 9102028736, NULL, NULL, 472)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dumpit elysha', NULL, 9101910232, NULL, N'Lexie', 473)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Duran Rhey', NULL, 9368301815, NULL, N'Tinky', 474)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dusayen Daisery', NULL, 9215607671, NULL, N'Giro', 475)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Dusayen Regiena', NULL, 9109238362, NULL, N'Ramen', 476)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Edica Grace', NULL, 9107650241, NULL, N'Maxie', 477)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Eduardo Rose Ann', NULL, 9125864770, NULL, N'Baby Rambo', 478)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Enriquez Breanna', N'Castillo, Baler Aurora', 9183987168, NULL, N'Pandak', 479)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Enriquez Breanna', NULL, 9183987168, NULL, N'Loopy', 480)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Eriksen Liza', NULL, 9175035030, NULL, N'Kidya', 481)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Escobar Kyle', NULL, 9351221747, NULL, N'Raven', 482)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Escoto Victor', NULL, 9954706481, NULL, N'Butchie', 483)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esguerra Gennie', NULL, 9084377195, NULL, N'Olie', 484)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esmundo Jp', N'San luis Aurora', 9107156536, NULL, N'Merideth', 485)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espaja Kamia', NULL, NULL, NULL, N'Lala', 486)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espana Kamia', NULL, 9064775923, NULL, N'Sexie', 487)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esparis John Paul', NULL, 9487896314, NULL, N'Coby', 488)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espina Audrey', N'Baler aurora', 9456653000, NULL, N'Cookie', 489)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espina Girlie', NULL, 9288193830, NULL, N'Imo', 490)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espinosa Allen', NULL, 9276856960, NULL, N'Gabrielle', 491)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espinosa Catalina', N'Baler aurora', 9178430993, NULL, N'Chochat', 492)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espinosa Zea', NULL, 9178042332, NULL, N'Winston', 493)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Espiritu Arian', NULL, 9230847907, NULL, N'Marky', 494)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esquivez Leni', NULL, 9995585099, NULL, N'Snowy', 495)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esteban Grace', NULL, 9516949139, NULL, N'Roxii', 496)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Esteban Mike', N'Ditumabo, San Luis Aurora', 9392341192, NULL, N'Crinckles', 497)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Estrella Paulo', NULL, 9989305482, NULL, N'Dicxie', 498)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Evangelista Nancy', NULL, 9951378665, NULL, N'Chie', 499)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fabricante Jelmar', N'Dilasag Aurora', 9474545674, NULL, N'Maxx', 500)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fabrigas Erlyn', N'Buhangin, Baler Aurora', 9215622680, NULL, N'Sceiny', 501)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fabrigas Melvin', N'Dipaculao Aurora', NULL, NULL, N'Trish', 502)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fajardo Kendi', N'Reserva, Baler Aurora', 9099848381, NULL, N'Sky', 503)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fajardo Pilimon', NULL, 9107263277, NULL, N'Bogs', 504)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Familleza John', NULL, 9272462326, NULL, N'Van', 505)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Faraon Jovanne ', NULL, 9164747796, NULL, N'Twix/Hersheys', 506)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Feliciano Glenn', N'Pingit, Baler Aurora', 9264415954, NULL, N'Spy', 507)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Felix Sydney Claire', N'Calabuanan, Baler Aurora', 9096826889, NULL, N'Ponggok', 508)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Feres Rodelin', N'Pingit, Baler Aurora', 9105287733, NULL, N'Gobgob', 509)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernandez Daisy', N'Maria Aurora, Aurora', 9475901067, NULL, N'Stanley', 510)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernandez Kath', NULL, 9052112392, NULL, N'Emma', 511)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernandez Mikaela', NULL, NULL, NULL, N'Manuk', 512)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernandez Regie', NULL, 9498038158, NULL, N'Gabby', 513)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernandez Richard', N'Dipaculao Aurora', 9187457035, NULL, N'Puppy', 514)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fernando Rodolfo', N'Suklayin, Baler Aurora', 9279524391, NULL, NULL, 515)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferre Pie', NULL, NULL, NULL, N'Bob', 516)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferrer Eilyn', NULL, 9073694839, NULL, N'Molly', 517)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferrer Michael ', NULL, 9369907492, NULL, N'Sydney', 518)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferrera Jenalyn', N'Buhangin, Baler Aurora', 9483712786, NULL, N'Mellow', 519)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferreras Emma', NULL, 9094384222, NULL, N'Panda', 520)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferreras Iina', NULL, 9216706101, NULL, N'Jopay', 521)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferreras Mary Anne', NULL, 9166103196, NULL, N'Toothless', 522)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ferreras Raphaela', NULL, 9502306082, NULL, N' Mauwie', 523)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flec', N'Brgy. Buhangin, Baler Aurora', 9493514090, NULL, N'Toblerone', 524)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flora Che', NULL, 9172596544, NULL, N'Waffles', 525)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flora Dennis', NULL, 9091861265, NULL, N'Cutie', 526)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flora Diane', NULL, 9152287815, NULL, N'Maby', 527)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Christian', NULL, NULL, NULL, N'Hughug', 528)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Joseph', N'Setan, Baler Aurora', 9667312673, NULL, N'Max', 529)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Marilyn', NULL, 9186626166, NULL, N'Carmelita', 530)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Merrydios', NULL, 9292038031, NULL, N'Kring', 531)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Robert Jr.', NULL, 9266571470, NULL, NULL, 532)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Flores Rolando', NULL, 9071556347, NULL, N'Forest', 533)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fontanilla Gretchen', NULL, 9493224108, NULL, N'Chuchay', 534)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fontanilla Gretchen', NULL, 9289317509, NULL, N'Mavie', 535)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fontanilla Shiena', NULL, 9352166983, NULL, N'Myla', 536)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fontanos Meilin', NULL, 9273225190, NULL, N'Selena', 537)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Formento Arnel', NULL, 9288757943, NULL, N'Nami', 538)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Formento Zylene', N'Brgy. Suclayin, Baler Aurora', 9099466381, NULL, N'Chuchay', 539)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Francisco Jerome', NULL, 9568015700, NULL, N'Coco', 540)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Francisco Rona', NULL, 9499281613, NULL, N'Maggy', 541)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Frias Joanne', N'Buhangin, Baler Aurora', 9399125232, NULL, N'Corona', 542)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Alyssa', NULL, 9565301645, NULL, N'Sherek', 543)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Carolle', NULL, 9473218753, NULL, NULL, 544)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Ceron', NULL, 9127948243, NULL, N'Coby', 545)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Hermie', NULL, 9994343820, NULL, N'Gin', 546)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Noel', N'Sabang, Baler Aurora', 9060818186, NULL, N'Zoe', 547)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Friginal Noel Jr.', NULL, 9060818186, NULL, N'Pharsa', 548)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Froilan Laurio', NULL, 9263471660, NULL, N'Boom Boom', 549)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Fulgar Ove', NULL, 9399071271, NULL, N'Luna', 550)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gahol Paolo', NULL, 9612885060, NULL, N'Summer', 551)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Galicia Heleina', NULL, 9989899858, NULL, N'Sofie', 552)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Galingan Arnald', NULL, 9338121363, NULL, N'Peanut', 553)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Galzote Pia', NULL, 9454800326, NULL, N'Monne', 554)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garcia Anna', NULL, 9482665540, NULL, NULL, 555)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garcia Blandino', NULL, 9205791166, NULL, N'Yappu', 556)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garcia Heaven', NULL, 9275534081, NULL, N'Donald', 557)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garcia Jerry', N'Sabang, Baler Aurora', 9482665540, NULL, N'Coby', 558)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garcia Jilson', NULL, 9276830891, NULL, N'C2', 559)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Garrobo Tyra', NULL, 9166972312, NULL, N'XO', 560)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gatalena Kimberly', NULL, 9298267930, NULL, N'Sahdra', 561)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gatchalian Jeil', NULL, 9122091323, NULL, N'Kuro', 562)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gavia Mik', NULL, 9362861759, NULL, N'Rara', 563)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gavilan Danica', NULL, 9203884294, NULL, N'Happy', 564)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Genzola Lalaine', NULL, 9122448670, NULL, N'Primo', 565)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Glemao Lorna', NULL, 9481739017, NULL, N'Nathan', 566)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Glemao Lorna', NULL, 9481739017, NULL, N'Brupi', 567)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gloria Vem', NULL, 9457608487, NULL, N'Sophia', 568)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Glorioso Sherwin', NULL, 9207039900, NULL, N'Sophie', 569)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'GMA FARM', NULL, NULL, NULL, N'Uno', 570)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Go Juliebeth', NULL, 9204812313, NULL, N'Mocha', 571)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Go Rona', NULL, 9283434760, NULL, N'Chiechie', 572)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gomez Dennis', N'Brgy. Zabali, Baler Aurora', 9993663895, NULL, N'Liear', 573)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gomez Dominador', NULL, NULL, NULL, N'Hachi', 574)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gomez Gerwin', NULL, 9456287136, NULL, N'Starla', 575)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Edna', NULL, 9202715302, NULL, N'Marga', 576)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Flor', NULL, 9296112607, NULL, N'Boboy', 577)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Jojo', NULL, 9123332999, NULL, N'Brownie', 578)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Jojo', NULL, 9123332999, NULL, N'Tobby', 579)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Krizzy', NULL, 9504577230, NULL, N'Ira', 580)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzales Mary Grace', NULL, 9498656430, NULL, N'Gelo', 581)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gonzalez Delsa', NULL, 9396144106, NULL, N'Bogira', 582)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Grefalda Julius', N'Brgy. Pingit, Baler Aurora', 9508803050, NULL, N'Bumpy', 583)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guardiana Johnrex', NULL, 9293474080, NULL, NULL, 584)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guerrero Diane', NULL, 9984940988, NULL, N'Lucky Dog', 585)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guerrero Dovie', NULL, 9281851947, NULL, N'Scarlet', 586)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guerrero wella', NULL, 9093031144, NULL, N'Luna', 587)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guevarra Nemencio', NULL, 9757033068, NULL, N'Apolo', 588)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guia Annie', NULL, 9999377332, NULL, N'Kulot', 589)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gusilatar Rizza', NULL, 9385685168, NULL, N'Enzo', 590)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gusilatar Ronald', NULL, 9071033174, NULL, N'Cloud', 591)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gusilatar Stephanie', NULL, 9455338181, NULL, N'Sansa', 592)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gutierrez Jay Ar', NULL, 9306482651, NULL, N'Sophie', 593)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Gutierrez Tatz', NULL, 9152281873, NULL, NULL, 594)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Gillian', NULL, 9455143144, NULL, N'Gingers Siblings', 595)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Jeanna', NULL, 9382298425, NULL, N'Maui', 596)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Myrna', NULL, 9483909944, NULL, N'Chou', 597)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Reymir Jan', NULL, NULL, NULL, N'Alexa', 598)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Vanessa', NULL, 9503258717, NULL, N'Junior', 599)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guttierez Zenaida', NULL, 9274202904, NULL, N'Jisoo', 600)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Guzman Sherma', NULL, 9291091443, NULL, N'Julio', 601)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hagita Rommel', NULL, 9099294887, NULL, N'Cookie', 602)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hamada Jennie', NULL, 9214201260, NULL, N'Husky', 603)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hamtig Enrico', NULL, 9128110134, NULL, N'Deigo', 604)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Herminigildo Louie', NULL, 9108088786, NULL, N'Max', 605)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hermo Rodolfo', NULL, 9281849425, NULL, N'Uno', 606)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hermozara Glenn', NULL, 9212297348, NULL, NULL, 607)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hernandez Mikaela', NULL, 9073962967, NULL, N'Bougart', 608)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hernandez Nenitas', NULL, 9219310202, NULL, N'Nathalia', 609)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hernandez Victorino', NULL, 9125426705, NULL, N'Starla', 610)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hertazuela Greta', NULL, 9983528420, NULL, N'Milo', 611)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hidalgo Jhun', NULL, 9205658095, NULL, N'Shemie', 612)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hinosa April', NULL, 9173084471, NULL, N'Mahliah', 613)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hokam Joana', NULL, 9051134620, NULL, N'Hera', 614)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hokam Julie', NULL, 9501086354, NULL, N'Daga', 615)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Holopainen Michelle', NULL, 9987522528, NULL, N'Barrel', 616)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hong Tanya', NULL, 9175005059, NULL, N'Apa', 617)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hong Tanya', NULL, 9175005059, NULL, N'Che', 618)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hora Vinz', NULL, 9084859664, NULL, N'Peanut', 619)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hubahib Marsha', NULL, 9092262119, NULL, N'Bruno', 620)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hulipas Jinkie', NULL, NULL, NULL, N'Jedai', 621)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Hungriano Violy', NULL, 9076017697, NULL, N'Draco', 622)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ico Mark Christian', NULL, 9388068660, NULL, N'Chickie', 623)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ico Mark Christian', NULL, 9070487689, NULL, N'Kurnel', 624)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Imperial Jeia', NULL, 9456723525, NULL, N'Cotton', 625)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Imperial Mylene ', NULL, 9193243906, NULL, N'Asher', 626)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Imperial Rogimer', NULL, 9457608410, NULL, N'Chichi', 627)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Imperial Wilma', NULL, 9105288171, NULL, N'Ashleigh', 628)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Inocillas Ramir', NULL, 9219191462, NULL, N'Sadam', 629)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ivy joy', NULL, 9168665262, NULL, N'Cotton', 630)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Jacob Patrick Harris', NULL, 9976722602, NULL, N'Bobba', 631)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Jandoc Anne', NULL, 9777535465, NULL, N'Sparky', 632)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Javar Jefren', N'Brgy. Suclayin, Baler Aurora', 9502306353, NULL, N'Goddy', 633)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Jerusalem Jenifer', NULL, 9454432344, NULL, N'Sonen', 634)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Jimenez Edward', NULL, 9451429660, NULL, N'Zoe', 635)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Joed', NULL, 9162566719, NULL, N'Bebot', 636)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Joven Denalyn', NULL, 9953440639, NULL, N'Gray', 637)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Julian Angel', NULL, 9982760764, NULL, N'Uzzi', 638)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Julian Shamaiah', NULL, 9383714486, NULL, N'Snoopy', 639)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Julio Irene', NULL, 9061547461, NULL, N'Danaya', 640)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Katrina', NULL, 9122090846, NULL, N'Chichi', 641)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Kenn Alvin', NULL, 9071573496, NULL, N'Rash', 642)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'La Pena Charmel', NULL, 9274397565, NULL, N'Max ', 643)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'La Pena Janielin', NULL, 9569300465, NULL, N'Lucky', 644)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'La Pena Janielyn', NULL, 9569300465, NULL, N'Hanney', 645)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Labarre Marlou', NULL, 9085063721, NULL, N'Choco', 646)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Labusta Donna', NULL, NULL, NULL, N'Rhina', 647)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lacambra Jellie', NULL, 9516954974, NULL, N'Chibi', 648)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lachica Eunice', NULL, 9282270405, NULL, N'Kobe', 649)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lacson Ang', NULL, NULL, NULL, N'Hershey', 650)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lacuata Jela', NULL, 9107802153, NULL, N'Misty', 651)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ladiero Kate', NULL, 9302043831, NULL, N'Ratics', 652)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lagman Johanne', NULL, 9164747796, NULL, N'Toby', 653)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lamia Queencel', NULL, 9467110001, NULL, N'Rick', 654)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lamoste Cecile', NULL, 9308554462, NULL, N'Chloe', 655)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Langkit MIla', NULL, NULL, NULL, N'Dyno', 656)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Languido Jorge', NULL, 9091371214, NULL, NULL, 657)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lanuza Cecil', NULL, 9568016120, NULL, N'Pam', 658)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lanzanida Jenny', NULL, 9218349226, NULL, N'Mochi', 659)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lapig Joana', NULL, 9666614877, NULL, N'Bella/Kidlat', 660)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lapig John Winter', NULL, NULL, NULL, N'Sam', 661)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lapig Precious', NULL, 9214481697, NULL, NULL, 662)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lapsot Dionisio', NULL, 9078853123, NULL, N'Spanky', 663)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lara Deseree', NULL, 9178416626, NULL, NULL, 664)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lara Jericho', N'Maria Aurora, Aurora', 9189475116, NULL, N'Jax', 665)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laranang Michelle', N'Dinalungan Aurora', 9467094396, NULL, N'Covid', 666)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Larioza Edward', NULL, 9506503149, NULL, NULL, 667)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laroza Donn', NULL, NULL, NULL, N'Java', 668)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lastimosa Mary Anne', NULL, 9167946075, NULL, N'Scruggs', 669)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laurel Patricia', NULL, 9062528475, NULL, N'Bigmax', 670)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laurel Polynn', NULL, 9062528475, NULL, N'Angel', 671)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laureles Khazzie', NULL, 9084372929, NULL, N'Mimi', 672)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laureles Teodoro', NULL, NULL, NULL, N'Chloe', 673)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lauren Ralph', NULL, 9367540458, NULL, N'Lucas', 674)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laurente Mark John', NULL, 9612929358, NULL, N'Chichi', 675)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lauresta Lennie', NULL, 9983097117, NULL, N'Pixie', 676)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Laureta Grace', N'Calabuanan, Baler Aurora', 9385625937, NULL, N'Covid', 677)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Leal Kimberly', NULL, 9091651520, NULL, N'Skipter', 678)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Leander Naty', NULL, 9079402652, NULL, N'Lovely', 679)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Leander Zindel', NULL, 9178238948, NULL, N'Ginger', 680)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lee Roberto', NULL, 9652504463, NULL, N'Vel', 681)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lee Sheena', NULL, 9259273699, NULL, N'Trixie', 682)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lejano Erlinda', NULL, 9283865018, NULL, NULL, 683)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Leonardo Naraha', NULL, 9479802186, NULL, N'Maggy', 684)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Leynes Henry', NULL, 9216855281, NULL, N'Basha', 685)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Liacer Bianca', NULL, 9481748052, NULL, N'Barbie', 686)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Licad Yolanda', NULL, 9495616911, NULL, N'Cobi', 687)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Licay Gina', N'Diteki, San Luis Aurora', 9157360299, NULL, N'Flerken', 688)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Liggayo Marilyn', N'San Luis Aurora', 9213418427, NULL, NULL, 689)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Linda Carlota', NULL, 9104292319, NULL, N'Kobe', 690)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Llagas FL', NULL, 9612174424, NULL, N'Una', 691)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lo Christine', NULL, 9190050939, NULL, N'Belle', 692)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lobo Jun', NULL, 9469271950, NULL, N'Britney', 693)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lomboy Jovito', NULL, 9062732320, NULL, N'Vivor', 694)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lombres Corrine', NULL, 9663742733, NULL, N'Joahra', 695)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lombres Elmer', NULL, NULL, NULL, N'Peng peng', 696)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lombres Jemima', NULL, 9154569908, NULL, N'Gabo', 697)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lombres Rhealyn', NULL, 9088981300, NULL, NULL, 698)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Longdason Charlyn', NULL, 9103075074, NULL, N'Scout Ranger', 699)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lopez Elisa', NULL, 9566019362, NULL, N'Chestnut', 700)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lopez Florante', NULL, 9195337578, NULL, N'Fiel', 701)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lopez Maricel', N'Pingit, Baler Aurora', 9191331497, NULL, N'Callie', 702)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lorilla Mervin', NULL, 9079512584, NULL, N'Lockdown', 703)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lorilla Mervin', NULL, 9381230480, NULL, N'Mona', 704)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Loverez Alexis', NULL, 9502331714, NULL, N'Bubba', 705)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Loza TJ', NULL, NULL, NULL, N'Riley', 706)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lozano Novelynda', NULL, 9171258495, NULL, N'Bullet', 707)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lozano Venus', NULL, 9993472608, NULL, N'Hiro', 708)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lubriave Rechel', NULL, 9198736266, NULL, NULL, 709)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumabas Kyle', NULL, NULL, NULL, N'Weed', 710)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumalang Wendel', NULL, 9099253851, NULL, N'Yoggy', 711)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumasac Andrea joy', NULL, 9083628257, NULL, N'Hunter', 712)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumasac Joyce', NULL, 9216765260, NULL, N'Mally', 713)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumasac Kris', NULL, 9957323960, NULL, N'Nica', 714)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumasac Marilen', NULL, 9481748177, NULL, N'Baghatur', 715)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Lumasac Romnick', NULL, 9203883967, NULL, N'Buddy', 716)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mabanes Benigno', NULL, 9203881526, NULL, N'Ace', 717)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Macabinguil danica Jane', NULL, 9208794316, NULL, N'Choco', 718)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Macatangay Grace', N'Ditumabo, San Luis Aurora', 9392341203, NULL, N'Ember', 719)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Macatiag Aldrin', NULL, 9099285303, NULL, N'Anaia', 720)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Macatiag Zenaida', NULL, 9283216194, NULL, N'Potpot', 721)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Madrazo Janice', NULL, 9568012400, NULL, N'Niko', 722)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mago Jemuel', N'Obligacion', 9984676411, NULL, N'Kiki', 723)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mamisao Nona', NULL, 9661742603, NULL, N'Bella ', 724)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Manangan Jenelyn ', NULL, 9266739602, NULL, N'Blue', 725)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Manantan Mark Julius', NULL, 9064467334, NULL, N'Sophie', 726)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mangaba Nonita', NULL, 9189475158, NULL, N'Kretta', 727)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Maniaol Anthony', NULL, 9672852178, NULL, N'Ashleigh', 728)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Manubay Precy', NULL, 9751230185, NULL, N'Matcha', 729)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Manzano Catherine', NULL, 9175874262, NULL, N'Baron', 730)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mapindan Rey', NULL, 9209069037, NULL, N'Era', 731)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mapindin Marlon', NULL, 9209060316, NULL, N'Walter', 732)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mariano Jenn', NULL, 9079152790, NULL, NULL, 733)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mariel Precious', NULL, 9975800900, NULL, N'Basha', 734)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Martin Dean', NULL, 9174956477, NULL, N'Tophy', 735)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Martin Isidro ', NULL, 9513426590, NULL, NULL, 736)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Martinez Christine Joy', NULL, 9295328669, NULL, N'Hiro', 737)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Martinez Nornaly', NULL, 9982465953, NULL, N'Fluffy', 738)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Marzan Jerome', NULL, 9062021264, NULL, N'Codie/Chloe', 739)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Marzan Jessa', NULL, 9385648333, NULL, N'Choco', 740)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Marzan Mar', NULL, 9489617449, NULL, N'Oreo', 741)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Marzan Marilyn', NULL, 9519950369, NULL, N'Covi', 742)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Marzan Michelle', NULL, 9077838718, NULL, N'Pepsi', 743)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Masibay Fulgencio', NULL, NULL, NULL, N'Shasha', 744)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Masibay JM', NULL, 9163134093, NULL, N'Lulu', 745)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mata Florante', NULL, 9204477239, NULL, N'Sharon', 746)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mella Carla Xenova', NULL, 9218821477, NULL, N'Xander', 747)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendije Jay-Anne ', NULL, 9203880444, NULL, N'Starlight', 748)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Christine', NULL, 9556758871, NULL, N'Bella', 749)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Eduardo', NULL, 9161235582, NULL, N'Yoshi', 750)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Jericho', NULL, 9159586239, NULL, N'Kiffer', 751)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Jewel Faith', NULL, 9501677339, NULL, N'Putchu', 752)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Reinabelle', NULL, 9178051619, NULL, N'Talikakas', 753)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mendoza Rolando', NULL, 9196183004, NULL, N'Sulao', 754)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Meneses Jaizzen', N'Brgy. Suclayin, Baler Aurora', 9213919117, NULL, NULL, 755)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mercado Slash', NULL, 9127418995, NULL, N'Cooper', 756)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Miana Eleaner', NULL, 9393738356, NULL, N'Pepper', 757)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Miana Eleanor', NULL, 9393738356, NULL, N'Cliff', 758)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Miana Hernald', NULL, 9292210121, NULL, N'Cookie', 759)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Miguel Carlos', NULL, 9205096849, NULL, N'Tobby', 760)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mijares Marychu', NULL, 9273967114, NULL, N'Bela', 761)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mijares Norman', NULL, NULL, NULL, NULL, 762)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Millan Max', NULL, 9988685638, NULL, N'Luffy', 763)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mingua Fredz', NULL, 9059280082, NULL, N'Pooh', 764)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Mintac Manny', NULL, 9499862469, NULL, N'Zoe', 765)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Miranda Ariel', NULL, 9305258948, NULL, N'Siti', 766)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Moises Jarameel', NULL, 9260687563, NULL, N'Ice/Burtus', 767)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Molina Casandra', NULL, 9393778878, NULL, N'Max', 768)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Molina Johanna', NULL, 9460590672, NULL, N'Buchi', 769)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Molina Joy', NULL, 9253762011, NULL, N'Squibb', 770)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Molina Juspher', NULL, 9481739457, NULL, N'Bella ', 771)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Molina Sol Fern', NULL, 9074003292, NULL, N'Kael', 772)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Montalban Alexis', NULL, 9072629888, NULL, N'Kuro', 773)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Montez Lorena', NULL, NULL, NULL, N'Magie', 774)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Morada Joan', NULL, 9758202975, NULL, N'Sophia', 775)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Morada Kenny', NULL, 9071663914, NULL, N'Buddy', 776)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Moral Beverly', NULL, 9479824214, NULL, N'SO', 777)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Morden Nolito', NULL, 9560068754, NULL, N'Railey', 778)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Morillo Merlyn', NULL, 9380490749, NULL, N'Bella', 779)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Motia King Harry', NULL, 9999080902, NULL, N'Basil', 780)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Moyo Luzviminda', NULL, 9399390867, NULL, N'Rooky', 781)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nabo Dan Andrew', NULL, 9993423124, NULL, N'Prince', 782)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nabutel Anna', NULL, 9288709469, NULL, N'Sizzy', 783)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nabutel Cynthia', NULL, 9399590848, NULL, N'Lucky', 784)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nacino Aryza Mari', NULL, 9178058136, NULL, N'Whity', 785)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nacino Ben', NULL, 9183221467, NULL, N'Lucky', 786)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nacino Felicitus', NULL, 9071769335, NULL, N'Muymuy', 787)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nacino Josua', NULL, 9187681890, NULL, N'Twyx', 788)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nacino Miguel', NULL, 9189177767, NULL, N'Puppies', 789)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Naguid Vincent', NULL, 9478348441, NULL, N'Yasha', 790)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nakar Joseph', NULL, 9284791406, NULL, N'Kokoy', 791)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nalu Foreigner', NULL, NULL, NULL, N'Hairo', 792)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nalupa Gil', NULL, 9091660906, NULL, NULL, 793)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nalupa Ma.Jean', NULL, 9291739983, NULL, N'Luigi', 794)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Namoro Jowenal', NULL, 9999939936, NULL, N'Primo', 795)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Naquid Vince', NULL, 9478348441, NULL, N'Moana', 796)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Naraja Nardo', NULL, 95115.41291, NULL, N'cobi', 797)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Narbonita Rey', NULL, 9451724101, NULL, N'Jackson', 798)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Navarro Kristi', NULL, 9166393278, NULL, N'Loxi', 799)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nazareno Raymond', NULL, 9064868786, NULL, NULL, 800)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nazario Eloiza', NULL, 9305597341, NULL, N'Tiny', 801)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nefulda Dennis', NULL, 9985800295, NULL, NULL, 802)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nicer Jerome', NULL, 9192612496, NULL, N'Jimbo', 803)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Niera Danielle', NULL, 9953660295, NULL, NULL, 804)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nipa ricky', NULL, 9073294221, NULL, N'Maxi', 805)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nisperos Analie', NULL, 9669686409, NULL, N'Hector', 806)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nolasco Flor', NULL, 9954194766, NULL, N'Ponimy', 807)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Noora Jake', NULL, 9984841678, NULL, N'Britney', 808)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nortez Zairalyn', N'Nonong, San Luis Aurora', 9156796688, NULL, N'Cooper', 809)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nudalo', NULL, NULL, NULL, N'Luna', 810)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Nuval Pricess Anne', NULL, 9236902174, NULL, NULL, 811)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Obra Jenefer', NULL, 9398550082, NULL, N'Jigs', 812)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ocampo Mark', NULL, 9084225054, NULL, N'Simba', 813)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ochoco Mylene', NULL, 9292409229, NULL, NULL, 814)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Oco Jillian', NULL, 9498721305, NULL, N'Axe', 815)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Oco Milan', NULL, 9498721305, NULL, N'Macie', 816)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Oica Ronell', NULL, 9173095382, NULL, N'Kc', 817)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Olivar Joie', NULL, 9058196254, NULL, N'Zeus', 818)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Oliver Rita', NULL, 9177285533, NULL, N'Beno', 819)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Olonan Ernest Frank', NULL, 9771688498, NULL, N'Skyie', 820)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Omania Jerome', NULL, 9953390790, NULL, N'Beasty', 821)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ong Amy', NULL, 9162711676, NULL, N'Dope', 822)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ong Donna Jane', NULL, 9164939511, NULL, N'Uno', 823)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ong Eva', N'Baler aurora', 9283919579, NULL, N'Sensei', 824)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ong Pedrito', NULL, 9162711676, NULL, N'Tanya', 825)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ong Romer', NULL, 9164939511, NULL, N'Copper', 826)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Opalda Chrystalene', NULL, 9085763621, NULL, N'Tootsie', 827)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Openiano Rica Mae', NULL, 9099992265, NULL, N'Shield', 828)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Opol Rafael', NULL, 9561936697, NULL, N'Storm', 829)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Optiona Riecette', NULL, 9050253000, NULL, N'Puppies', 830)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Orena Nieves', NULL, 9395020067, NULL, N'Misha', 831)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Orena Romelle', NULL, 9083237742, NULL, NULL, 832)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Orogo Bernard', NULL, 9292991670, NULL, N'Murphy', 833)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Orogo Freddie', NULL, 9473888953, NULL, N'Coby', 834)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Orolfo Mae', NULL, 9568258286, NULL, N'Rapa', 835)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ortomio Julius Ray', NULL, 9272765897, NULL, N'Persie', 836)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ortomio Vito', NULL, 9293177650, NULL, N'Jack', 837)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Osias Bessie', NULL, 9497552916, NULL, N'Max', 838)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paano Marites', NULL, 9564491237, NULL, N'Hanter', 839)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pabillo EJ', NULL, 9194713361, NULL, NULL, 840)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pablo Klein', NULL, 9338149065, NULL, NULL, 841)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pacasio Menard', NULL, 9179430107, NULL, N'Booboo', 842)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Padilla Joy', NULL, 9124622365, NULL, N'Smoke', 843)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Padua Ezra', NULL, 9299585025, NULL, N'Lucy', 844)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Padua Lorie', NULL, 9386490767, NULL, N'Coco', 845)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Padua Raul', NULL, 9776138645, NULL, N'Bogi', 846)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pagaduan Jericha', NULL, 9513272966, NULL, N'Chica', 847)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pagao Jeniffer', NULL, 9495996846, NULL, N'Starla', 848)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pagkaliwangan Suzaine', NULL, 9993284983, NULL, N'Bella', 849)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pagoguin Edwin', NULL, 9513414439, NULL, N'Pia', 850)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pajaroja Ma. Liza', NULL, 9193034485, NULL, N'Memosa', 851)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Palo Sirlie', NULL, 9053964863, NULL, N'Crumpy', 852)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Palto Divina', NULL, 9072564366, NULL, N'Puppies', 853)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pangilinan Reshelle', NULL, 9120264409, NULL, N'Chloe', 854)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pangue Myrna', N'Zabali, Baler Aurora', 9083628257, NULL, N'Ash', 855)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pantalunan Marites', N'Kinalapan, Baler Aurora', 9128599302, NULL, N'Coffee', 856)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Parica Jonathan', NULL, 9338678337, NULL, N'Kiana', 857)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Parreno Rowena', NULL, 9182783143, NULL, N'Copper', 858)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascasio Mary elf', NULL, 9505745206, NULL, N'Amira', 859)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascion Kimberlyn ', NULL, 9166898707, NULL, N'Jolly', 860)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascua Aimee Rose', NULL, NULL, NULL, N'Chaw', 861)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascua Benny', NULL, 9993533677, NULL, N'Cobhy', 862)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascua Gladys', NULL, 9159586264, NULL, N'Sparky', 863)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascua Mary Grace', NULL, 9995944088, NULL, N'Coco', 864)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pascua Roderick', NULL, 9161234816, NULL, N'Cheffy', 865)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paug Marjerie', NULL, 9178116861, NULL, N'Maliah', 866)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Catherine Joy', NULL, 9060634153, NULL, N'Cream', 867)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Clarize', NULL, 9273754813, NULL, N'Tata', 868)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Jolly', NULL, 9090634153, NULL, N'Thara', 869)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Jolly', NULL, 9090634153, NULL, N'King', 870)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Tim', NULL, 9164762842, NULL, N'Nami', 871)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Paulino Valerie', NULL, 9384530103, NULL, N'Doobie', 872)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pecasio Nasreen', NULL, 9399218723, NULL, N'Fayta', 873)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pecasio Rio Ann', NULL, 93590007373, NULL, N'Blacky', 874)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pedroso Jetsky', NULL, NULL, NULL, N'Zoey', 875)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pedrozo Spencer', NULL, NULL, NULL, N'Angel', 876)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pelaez Lucia', NULL, 9480226837, NULL, N'Puppies', 877)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pelaez Mhei', NULL, 9555509714, NULL, NULL, 878)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pelaez Sam Claren', NULL, 9569300503, NULL, NULL, 879)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pelayo Krista', NULL, 9060277342, NULL, N' Ron-it', 880)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pena Evangeline', NULL, 9506869102, NULL, N'Akie', 881)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pena Gemma ', NULL, 9552863658, NULL, N'Ming', 882)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pena Jimmy', NULL, 9203186167, NULL, N'Rocky', 883)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pena John Oliver', NULL, NULL, NULL, N'Kura', 884)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Penaloza Jessa', NULL, 9391693227, NULL, N'Chowkie', 885)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Peneyra Franchesca', NULL, 9099294874, NULL, N'tommy', 886)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Peralta Neil Jann', NULL, 9067574677, NULL, N'Krissy', 887)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Perez Angelo', NULL, 9091876045, NULL, N'Nala', 888)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pescado Jaypee', N'Brgy. Suclayin, Baler Aurora', 9497152282, NULL, NULL, 889)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Peslado Charlou', NULL, 9503937699, NULL, N'Ghost', 890)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Petallo Sharon', N'Brgy. Buhangin, Baler Aurora', 9266528155, NULL, N'Olaf', 891)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'PFC Veterinary Clinic', NULL, 9456198347, NULL, N'trouble', 892)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Phong Nguyen', NULL, 9773917041, NULL, N'Potpot', 893)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Picart Hazel', N'Maria Aurora, Aurora', 9124489840, NULL, N'Dambo', 894)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Picart Jeruel Jr.', N'Maria Aurora, Aurora', 9207790392, NULL, N'Candy', 895)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Picart Sheren', NULL, 9564421727, NULL, N'tiny', 896)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pimentel Clea', NULL, 9463932062, NULL, N'Gotsi', 897)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pimentel Franchesca', N'Castillo, Baler Aurora', NULL, NULL, N'Sky ', 898)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pimentel Joseph', NULL, 9071556563, NULL, N'Cookie', 899)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pimentel Lhey', NULL, 9072894901, NULL, N'Jeore', 900)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pineda Marry Clenn', N'Casiguran Aurora', 9088621668, NULL, N'Nami', 901)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pinile Divina', N'Dipaculao Aurora', 9062344594, NULL, N'Tookie', 902)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Pizarra Grace', NULL, 9569301122, NULL, N'Jammeer', 903)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Cesar', N'San Isidro, San luis Aurora', 9301599324, NULL, N'Chocolate', 904)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Charley', NULL, 9465491223, NULL, N'3 Puppies', 905)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Everlyn A.', N'Sabang, Baler Aurora', 9106827090, NULL, N'Sausage', 906)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Ferdinand', N'Poblacion 2, San luis Aurora', 9989708662, NULL, N'Hashi', 907)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Kelly sheen', N'San jose, San luis Aurora', 9178592954, NULL, N'Cj', 908)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Nathan Joy', NULL, 9070284771, NULL, NULL, 909)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Poblete Rowena', NULL, 9061182389, NULL, N'Labrador', 910)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Porquerino Kris', NULL, 9177718040, NULL, N'Adam', 911)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Portera Mylin', NULL, 9758801274, NULL, N'Jack/Jill', 912)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Postor Famela', NULL, 9292410298, NULL, N'Sam', 913)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Postor Farah Faith', NULL, 9481740531, NULL, N'Peenee', 914)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Prado Geraldine', N'Baler aurora', 9162868156, NULL, N'King', 915)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Punzal Selina', N'Maria Aurora, Aurora', 9279281362, NULL, N'PIa', 916)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Punzalan James', N'Taguig', NULL, NULL, N'Toby', 917)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quambao Kenneth', NULL, 9456904669, NULL, N'Whitey', 918)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Arlene G.', N'Buhangin, Baler Aurora', NULL, NULL, N'Alfred', 919)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Carmelo', NULL, 9083456315, NULL, N'Lala', 920)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Ciara May E.', N'Zabali, Baler Aurora', 9953661619, NULL, N'Jopay', 921)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Glacy', NULL, 9260231345, NULL, N'Butler', 922)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Jamaica', N'Castillo, Baler Aurora', 9301530635, NULL, N'Lano', 923)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Lara', NULL, 9469730964, NULL, N'Asi', 924)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Lea Mae', NULL, 9190027657, NULL, N'Sam', 925)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Lory', N'Sabang, Baler Aurora', 9089618073, NULL, N'Jojo', 926)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Querijero Maynard', NULL, 9477940584, NULL, N'Moggli', 927)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quevedo Alexandra', NULL, 9953315627, NULL, N'Sophie', 928)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quiambao Juanito', NULL, 9203881560, NULL, N'Chino', 929)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quiambo Gloria', N'Brgy. Matuyo', 9301528324, NULL, N'Niklaus', 930)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quiambo Praise', N'Brgy. 4, San luis Aurora', 9480641807, NULL, N'Bongbong', 931)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quiap Mazahiro', NULL, 9192617091, NULL, N'Macko', 932)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quiap Ruel A.', NULL, 9488004112, NULL, N'Dyno', 933)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quibuyen Roderick', N'Suklayin, Baler Aurora', 9086096399, NULL, N'Cerset', 934)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Quintos Jonalyn', N'Sabang, Baler Aurora', 9275890479, NULL, N'Pepper', 935)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Racoma Joan', NULL, 9077172338, NULL, N'Creamy', 936)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rada Grace', N'Caltex', 9176376261, NULL, NULL, 937)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rada Marites', NULL, 9209815327, NULL, N'Farrah', 938)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramirez Leila', N'Baler aurora', 9479661603, NULL, NULL, 939)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Amador', NULL, 9299674121, NULL, N'Bobby', 940)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Ariel C.', N'Maria Aurora, Aurora', 9219253867, NULL, N'Bee Bow', 941)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Christina V.', N'Norong, San luis Aurora', 9199689693, NULL, N'Bruno', 942)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Evelyn', N'Reserva, Baler Aurora', 9219960092, NULL, N'Lucky', 943)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Jessal', N'Castillo, Baler Aurora', 9488574851, NULL, N'Skyler Love', 944)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Kim Adrian', N'Maria Aurora, Aurora', 9061342940, NULL, N'Coffee', 945)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos Rechelle', NULL, 9150513669, NULL, N'Maya', 946)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ramos, Pipoy', N'Maria Aurora, Aurora', 9753549301, NULL, N'Walden', 947)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ranillo Glen', N'Suklayin, Baler Aurora', 932063095, NULL, N'Summer', 948)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ranillo Rajiv', N'Buhangin, Baler Aurora', NULL, NULL, N'Max', 949)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ranillo Samantha', N'San Luis Aurora', 9398255888, NULL, NULL, 950)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rapetti Ariane', N'Baler aurora', 9452440185, NULL, N'Aurora', 951)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rashid Elisan', NULL, 9159802796, NULL, N'Zoey', 952)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Raymundo Sherly', NULL, 9214630617, NULL, N'Bubu', 953)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reamucio Danfort', NULL, 9204393854, NULL, N'Sammie', 954)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rebutes Rachelie', NULL, 9383273059, NULL, N'Ivana', 955)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Redustes Rachelle', NULL, 9631596224, NULL, N'Hachie', 956)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Regis Allan ', NULL, 9982743792, NULL, N'Happy', 957)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rei John', NULL, 9305160890, NULL, N'Febby', 958)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reopta Mary Rose', NULL, 9084841264, NULL, N'Potchie', 959)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Resplandor Lean Anthony', N'Reserva, Baler Aurora', 9217601345, NULL, N'Lucker', 960)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Resueno Jovy', N'San Luis Aurora', 9194351657, NULL, N'Mary Rod', 961)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Revilla Shaira Mae', N'Baler aurora', 9128974547, NULL, N'Shitzu', 962)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Annabelle', N'San Luis Aurora', 9997038085, NULL, N'Sushi', 963)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Denan', N'Sabang, Baler Aurora', 9205434311, NULL, NULL, 964)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Jordan', NULL, 9459658585, NULL, N'Luno', 965)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Kat', NULL, 9055741625, NULL, N'Joy', 966)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Michelle', N'Dipaculao Aurora', 9297773196, NULL, N'Hunter', 967)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Michelle', NULL, 9106812149, NULL, NULL, 968)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Patricia', NULL, 9176526655, NULL, NULL, 969)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Queen', N'Maria Aurora, Aurora', 9218985689, NULL, N'Coffee', 970)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Reyes Syndelle', N'Baler aurora', 9300488498, NULL, N'Ulysses', 971)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rigdao Tricia', NULL, 9063208611, NULL, N'Ulysses', 972)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rigor Iris', NULL, 9094429085, NULL, N'Black ', 973)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rigor Jess', N'San Luis Aurora', 9284207654, NULL, N'Simba', 974)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rimando Dempsey', NULL, 9171927404, NULL, N'Celery', 975)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rinonos Karen', NULL, 9978114624, NULL, N'Shillow', 976)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rioflorida Dominic', NULL, 9999942919, NULL, N'Dos', 977)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rioflorido Rowel', N'Baler aurora', 9999942919, NULL, N'Whity', 978)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritual Allen', NULL, 9128891928, NULL, N'Eva', 979)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritual Anazarina', NULL, 9515415814, NULL, N'Lola', 980)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritual Jerome', N'San Luis Aurora', 9277561128, NULL, N'Behati', 981)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritual Karizza', N'Baler aurora', 9773591491, NULL, N'Kevin', 982)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritual Ricardo', NULL, 9506896897, NULL, N'Sophie', 983)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ritzmond Reyo', NULL, 9176683191, NULL, N'Batman', 984)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Eczen', N'Dipaculao Aurora', 9389443145, NULL, N'Bebe', 985)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera edwin', NULL, 9950884795, NULL, N'Oreo,Choco', 986)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Frances', NULL, 9173127367, NULL, N'Luffy', 987)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Jedrack', NULL, 9612635892, NULL, N'Sonic', 988)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Mariflo', NULL, 9079444823, NULL, N'Shiro', 989)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Merlie', NULL, 9219574050, NULL, N'Choco', 990)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Rennie', NULL, 9338184537, NULL, N'Yuri', 991)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rivera Rosalina', NULL, 9293159192, NULL, N'Argo', 992)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Roasales Saujena', NULL, 9499049425, NULL, N'Mocha', 993)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Robles Jeff', NULL, 9208314479, NULL, N'Diego', 994)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rodelas Jenirose', NULL, 9268121461, NULL, N'Kitty', 995)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rodica Pauline Denise', NULL, 9194131510, NULL, NULL, 996)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rodriguez Geraldin', NULL, 9176486377, NULL, N'B2', 997)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rodriguez Zenaida', N'Casiguran Aurora', 9985672240, NULL, N'Arthur', 998)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rojo Lj', N'Suklayin, Baler Aurora', 9193289399, NULL, N'Chappie', 999)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Roldalo Joana R.', N'Florida, Maria Aurora', 9287908396, NULL, N'Maxpein', 1000)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Romano Lita', NULL, 9187916107, NULL, N'Loki', 1001)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Romero Irish', N'Buenavista', 9072736698, NULL, N'Dalgom', 1002)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Romero Karyle', NULL, 9351860849, NULL, N'Max', 1003)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Romero Ria', NULL, 9209829998, NULL, N'Sabrina', 1004)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Romero Vanessa', N'Brgy. 2, Baler Aurora', 9151474055, NULL, N'J-Cov', 1005)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Roque Dhanmark Paul', N'Maria Aurora, Aurora', 9071885038, NULL, N'Jack', 1006)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Roque Dulce', NULL, 9295729238, NULL, N'Zoe ', 1007)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosala Bobby', NULL, NULL, NULL, N'Achi', 1008)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosales Gerald', N'Suklayin, Baler Aurora', 9152668326, NULL, N'Aso', 1009)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosario Ronald', NULL, 9151705611, NULL, N'Billy', 1010)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosas Abie', NULL, 9212479590, NULL, N'Princess', 1011)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosas Aubrey', N'Brgy. 5, Baler Aurora', 9212479590, NULL, N'Hotdog', 1012)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rosas Zyra', N'Brgy. 2, Bonifacio st.', 9121286973, NULL, N'Lucie', 1013)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rotaquio Rommel Kenneth', N'Nonong, San Luis Aurora', 9505742032, NULL, N'Ketty', 1014)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rotaquio Vincent', NULL, 9090776959, NULL, N'Aviona', 1015)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rubio Aeron', N'Buhangin, Baler Aurora', 9161039735, NULL, N'Bubba', 1016)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rubio Issandra Mae', N'Suklayin, Baler Aurora', 9271761639, NULL, N'Jam ', 1017)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Rubio Romar', N'Sitio Kinalapan', 9998336360, NULL, N'Layca', 1018)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ruiz Kimberly', NULL, 9525884068, NULL, N'Pepper', 1019)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ruiz Paolo', NULL, 9612885060, NULL, N'Momu', 1020)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ruzol Della', N'Castillo, Baler Aurora', 9185721323, NULL, N'Hanzel', 1021)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sabatin Harold', N'Puangi, Dipaculao Aurora', 9473443691, NULL, NULL, 1022)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sacramento Francis', NULL, 9178165062, NULL, N'Ganda', 1023)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sacro Charlie', NULL, 9478833041, NULL, N'Oreo//Zowie', 1024)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sagampod Aira', N'Dike, Baler Aurora', 9474572876, NULL, N'Skyro', 1025)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sagampod Alvin', N'Pingit', 9297610434, NULL, N'Lakas', 1026)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sagun Shane Abygail', N'Maria Aurora, Aurora', 9093293754, NULL, NULL, 1027)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salamanca Jonjon', N'Florida, Maria Aurora', 9474435541, NULL, N'Benny', 1028)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salamanca Leila', N'Dimanpudso, Maria Aurora', 9104302086, NULL, N'Ten-Ten', 1029)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salas Macon', NULL, 9158285129, NULL, N'Biomele', 1030)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salaya Joel', NULL, 9498542251, NULL, N'Choe', 1031)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salazar Jessa', NULL, 9517088847, NULL, N'Paoebus', 1032)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salipsip Mark Anthony', N'Suklayin, Baler Aurora', 9171246445, NULL, N'Nicole', 1033)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salmo Lyzel', N'Dinadiawan, Dipaculao Aurora', 9209634112, NULL, N'Pompom', 1034)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Saludo Xyriel', NULL, 9286828012, NULL, N'Akira', 1035)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salunga Elizabeth', NULL, 9192734373, NULL, N'Cashmere', 1036)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Salunga Oscar', NULL, 9472361158, NULL, N'Toby', 1037)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samaniego Bernadette', N'Suklayin, Baler Aurora', 9178131913, NULL, N'Coally', 1038)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samano Erik', N'Suklayin, Baler Aurora', 9294803871, NULL, N'Diana', 1039)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samatra Rica', NULL, 9362858008, NULL, N'Wyte', 1040)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samonte Karen', NULL, 9998800928, NULL, N'Jopay', 1041)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samonte Mel', NULL, 9175112264, NULL, N'Moochow', 1042)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Samonte Princess Yunyzah', N'Baler Aurora', 9106838798, NULL, N'Bavin', 1043)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'San Pedro Lloyd', NULL, 9151707185, NULL, N'Courtney', 1044)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'San Pedro Marvin', NULL, 9151707185, NULL, N'Sophie', 1045)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'San Pedro Vic', NULL, 9285025908, NULL, N'Slim', 1046)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sanchez Christopher', NULL, 9277264717, NULL, N'Chico', 1047)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sanchez Kimar', NULL, 9186626166, NULL, N'Juanba', 1048)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sanchez Ronald', N'Baler aurora', 9219250307, NULL, N'Loki', 1049)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santiago Cathy', N'Dipaculao Aurora', 9065677594, NULL, N'Ginger', 1050)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos Edwin', NULL, 9202867357, NULL, N'Brenda', 1051)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos Imelda', N'San Isidro', 9302795625, NULL, N'Jhowie', 1052)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos James', N'Dipaculao Aurora', 9457668817, NULL, N'Shev', 1053)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos Lance', NULL, 9218913044, NULL, N'xity', 1054)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos Liliaso', NULL, 9307252672, NULL, N'Belle', 1055)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'santos Patricia', NULL, 9078895355, NULL, N'Floopy', 1056)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Santos Roselle', N'Brgy. 1, Maria Auora', 9488415727, NULL, N'Hamper', 1057)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sarenas Rosalie', N'San luis Aurora', 9288355838, NULL, N'Clark', 1058)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sareno Ynah', NULL, 9757661966, NULL, N'Rambo', 1059)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sarimo Miraflor', N'Maria Aurora, Aurora', 9212676860, NULL, N'Moana', 1060)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sarmiento Enrico', NULL, 9104542607, NULL, NULL, 1061)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sarmiento Jacelle', N'Dipaculao Aurora', 9155307132, NULL, N'chiqui', 1062)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sarmiento Norcelle', N'Maria Aurora, Aurora', 9212676860, NULL, N'Pomzky', 1063)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Saturno Sheryl', NULL, 9503202659, NULL, N'Moana', 1064)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Saulo Jona Vie', NULL, 9508129205, NULL, N'Van/Nami', 1065)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Saura Zylene', NULL, 9496889880, NULL, N'Roxy', 1066)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Saynes Julius', NULL, 9383273252, NULL, N'Sam', 1067)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Scott Cherrybel', N'Sitio Castillo', 9203884093, NULL, N'Candies', 1068)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sean Anthony', NULL, 9472365738, NULL, N'Rico', 1069)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Senoja Rengelyn ', NULL, 9513275888, NULL, N'Bella', 1070)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sensires Maria', NULL, 9452412675, NULL, N'Keeper', 1071)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Serdena Samson', NULL, 9186066761, NULL, N'Atheena', 1072)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sereena Marie', N'Baler aurora', 9451692744, NULL, N'Moica', 1073)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Serna Analyn', NULL, 9216834288, NULL, N'Garrie Asilo', 1074)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sernal John', N'Kinalapan, Baler', 9293549415, NULL, N'Emma', 1075)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sernal Lodrigo', N'Debucao, Maria Aurora', 9305821792, NULL, N'Russel', 1076)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sernes Magdalena', NULL, 9186854411, NULL, N'Waffie', 1077)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sia Fatima', NULL, 9475215698, NULL, N'Sophia', 1078)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Siapno Rodelin', N'Dinadiawan, Dipaculao Aurora', 9171299268, NULL, N'Spark', 1079)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sibayan Orland M.', N'Dipaculao Aurora', 9305577099, NULL, NULL, 1080)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sicat Raquel', N'Maria Aurora, Aurora', 9072651720, NULL, N'Martina', 1081)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sigua Jacquel', NULL, 9099989380, NULL, N'Milo', 1082)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sigun Ian James', NULL, 97712416602, NULL, N'Ar-die', 1083)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Silao Raffy', NULL, 9669067039, NULL, N'Bacon', 1084)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Silva Francisco', N'Gloria st., Baler Aurora', 9122090745, NULL, N'Rolly', 1085)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Silva Jen', NULL, 9175585728, NULL, N'Pow', 1086)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Silva Justine', N'Baclaran Baler', 9175985728, NULL, N'Kina', 1087)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Silvestre Rhiza', NULL, 9338523114, NULL, N'Stitch', 1088)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Simon Ginalyn', NULL, 9381440947, NULL, N'Darcko', 1089)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Simon Jessely', NULL, 9982716582, NULL, N'Stochy', 1090)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Bernard G.', N'Calabuanan, Baler Aurora', 9051010343, NULL, N'Tootsie', 1091)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Billy Jae', N'Suklayin, Baler Aurora', 9293754766, NULL, N'Bruce', 1092)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Eve', NULL, 9178155822, NULL, N'Fifi', 1093)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Josephine', NULL, 9097975007, NULL, N'Roman', 1094)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Lei', NULL, 9481748032, NULL, N'Nacho', 1095)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Leila', NULL, 9481748032, NULL, N'Anya', 1096)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Rommel P.', N'Sabang, Baler Aurora', 9215080508, NULL, N'Rufus', 1097)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Rosie', NULL, 9567955346, NULL, N'Dolores', 1098)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sindac Shirley', NULL, 9078894934, NULL, N'Nonoma', 1099)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sison April', NULL, NULL, NULL, NULL, 1100)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sison Dominador', N'Sabang, Baler Aurora', NULL, NULL, N'Briana', 1101)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sison Emil', N'Zabali, Baler Aurora', 9216811037, NULL, N'Maxie', 1102)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sison Rosalinda', N'San Luis Aurora', 9176264144, NULL, N'Teddy', 1103)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Smith Laarni', N'Baler aurora', 9178453359, NULL, N'Witty', 1104)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Smith Laarni', NULL, 9178453359, NULL, NULL, 1105)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soliven Joseph', NULL, 9958241583, NULL, N'Sasha', 1106)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soliwen Noly', N'Maria Aurora, Aurora', 9288778130, NULL, N'Chloe', 1107)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sollege Evelyn', N'Sabang, Baler Aurora', 9123690975, NULL, N'Cali ', 1108)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sollege Marites', NULL, 9500782427, NULL, N'Pawlo', 1109)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Solomon Eunice', N'Suklayin, Baler Aurora', 9773142115, NULL, N'Angela', 1110)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Solomon Gisell', N'Maria Aurora, Aurora', 9287224341, NULL, N'Malliah', 1111)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Solomon Mark Anthony', NULL, 9153136837, NULL, NULL, 1112)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Solomon Shara', N'Nonong, San Luis Aurora', 9501582642, NULL, N'Katkat', 1113)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Somera Jessica', NULL, 9102025455, NULL, N'Blaze', 1114)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Somera Kristine', NULL, 9108130767, NULL, N'Creamy', 1115)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Somera Marirose', NULL, 9993663371, NULL, N'Bella', 1116)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soria Lenny', N'Dinalungan Aurora', NULL, NULL, N'Anoacks', 1117)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soriano Carmelita', N'San Luis Aurora', 9295320214, NULL, N'Miehou', 1118)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soriano Donel', N'Suklayin, Baler Aurora', 9509556451, NULL, N'Ysabella', 1119)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soriano Ladimer', N'San Luis Aurora', 9951294034, NULL, NULL, 1120)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soriano Raul', NULL, 9479706148, NULL, N'Luci ', 1121)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Soriao Rosemarie', N'San Luis Aurora', 9123047536, NULL, N'Hastag', 1122)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sotero Arlene', NULL, 9212078714, NULL, N'Chuchay', 1123)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sotero Deck', N'Baler aurora', 9276694720, NULL, N'Leela', 1124)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sotero Marissa', NULL, 9501379736, NULL, NULL, 1125)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Spencer David ', NULL, 9178935877, NULL, N'Tobi', 1126)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Spitez Manuel', N'Buhangin, Baler Aurora', 9206366061, NULL, N'Max', 1127)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Stephen', NULL, NULL, NULL, N'Fee-fee', 1128)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sua Sonny', N'Zabali, Baler Aurora', 9087988167, NULL, N'Bailey', 1129)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Suaverdez Paula Joy', N'Baler aurora', 9984102027, NULL, NULL, 1130)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Subad Heizel ', NULL, 9300392208, NULL, N'Hercules', 1131)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Subad Shenna', NULL, 9480808923, NULL, N'Hele', 1132)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Subad Suzette', NULL, 9152590227, NULL, N'Liksi', 1133)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sudario Diana', N'Maria Aurora, Aurora', 9158143223, NULL, N'Sandra', 1134)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sudario Dianna', NULL, 9178102506, NULL, N'Choco', 1135)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sudario Mark Anthony', N'Maria Aurora, Aurora', 9306484636, NULL, N'Jasper', 1136)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sudario Vilma', NULL, 9454785213, NULL, N'MIKa', 1137)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Sunida Michelle', NULL, 9395000656, NULL, N'Rara', 1138)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Supnad Marifel', NULL, 9053913938, NULL, N'HAchikoDagul', 1139)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Supnet Anna', NULL, 9672114904, NULL, N'Sweety', 1140)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Supnet Jessa Mae', N'Dipaculao Aurora', 9305576986, NULL, N'Woffie//Mimi', 1141)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Supsup Jaja', NULL, 9203384846, NULL, NULL, 1142)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Suril Mark', N'Baler aurora', 9151727909, NULL, N'Bolty', 1143)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tabelisima Nio', N'Maria Aurora, Aurora', 9089838387, NULL, N'Stephie', 1144)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tabias Rica May', NULL, 9168972603, NULL, N'Yori', 1145)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tablang Shayne', NULL, 9998435400, NULL, N'Tutsi', 1146)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tablazon Leny', NULL, 9182783143, NULL, N'Lexus', 1147)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tablazon Rowena ', NULL, 9291252523, NULL, N'Pitbull', 1148)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tadeo George', N'Dipaculao Aurora', 9103797345, NULL, N'Daisy', 1149)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Talento Macy Ann', NULL, 9267218866, NULL, N'Spike', 1150)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tamayo Althea R.', N'Smart Beach House', 9067571293, NULL, N'Kuyangi', 1151)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tamayo Win', NULL, 9194719532, NULL, N'Ina', 1152)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tambalo Josue', N'Calabuanan, Baler Aurora', 9064491101, NULL, N'Suchi', 1153)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tambis Len', N'Suklayin, Baler Aurora', 9090843654, NULL, N'Carmie', 1154)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tan Henry', N'Baler Aurora', 9478990373, NULL, N'Spendy', 1155)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tan Julian Peter', N'Baler Aurora', 9104103053, NULL, N'Hatchi', 1156)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tan Julius Peter', NULL, 9776943123, NULL, N'Volvs', 1157)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tandas Kristian', NULL, 9179151017, NULL, N'Skud', 1158)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tandiaman Karen', N'Buhangin, Baler Aurora', 9101450588, NULL, N'Ranger', 1159)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tanghal Juanito Jr.', N'Sabang, Baler Aurora', 9394473583, NULL, N'Courtney', 1160)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tangson Baby Jane', N'Buhangin, Baler Aurora', 9072174397, NULL, N'Charlie', 1161)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tangson Christine', NULL, 9301526275, NULL, N'Kisses', 1162)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tangson Jovie', NULL, 9292587598, NULL, N'Klifer', 1163)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tangson Ma.Elizabeth ', NULL, 9494712403, NULL, N'Hammet/salma', 1164)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tapucol Micko', N'Bazal, Maria Aurora', 9184586180, NULL, N'Simang', 1165)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tayag Samantha', NULL, 9156383045, NULL, N'Pipay', 1166)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tayo Mario', NULL, 9151727806, NULL, N'Cody', 1167)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tayoan Judelyn', NULL, 9395693415, NULL, N'Chloe', 1168)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Te Corazon ', N'San Luis Aurora', 9086068471, NULL, N'Tenten/Sese', 1169)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Te Gina', N'Recto st., Baler Aurora', 9121320933, NULL, N'Chummy', 1170)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Te Ma. Isabel P.', N'Recto st., Baler Aurora', 9297732074, NULL, NULL, 1171)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Te Maria Isabel', NULL, 9297732074, NULL, N'Dremone', 1172)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Te Nicole', N'Maria Aurora, Aurora', 9193245469, NULL, NULL, 1173)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tecson Joseph', NULL, 9469730645, NULL, NULL, 1174)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tena Marvie', NULL, 9568412506, NULL, N'Sophia', 1175)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tena Menchu', NULL, 9436568393, NULL, N'Akkeo', 1176)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tenioso Kristian', NULL, 9497531062, NULL, N'Cardo', 1177)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Teniozo Edriane', N'Buhangin, Baler Aurora', 9996806140, NULL, N'Voj/Luna', 1178)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'The John', NULL, 9508555175, NULL, N'Totchie', 1179)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tilan Laarni', N'Dipaculao Aurora', 9480226532, NULL, N'Hophop', 1180)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tinguban Myra', NULL, 9397472429, NULL, N'Ken', 1181)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tinio Carla', NULL, 9052134560, NULL, N'Zoy', 1182)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tinio Jeremy', NULL, 9214723661, NULL, N'James', 1183)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tipon Menard', N'Calabuanan, Baler Aurora', 9105107761, NULL, NULL, 1184)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tobelisima Ivy', N'Maria Aurora, Aurora', NULL, NULL, N'Clover', 1185)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tobelisima Shekinah', NULL, 9474857131, NULL, N'Carah', 1186)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Dann', NULL, 9198280912, NULL, N'MIlo', 1187)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Ishmael ', NULL, 9284204822, NULL, N'Eisntein', 1188)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Noel', NULL, 9189859029, NULL, N'Potpot', 1189)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Patricia', NULL, 9666871938, NULL, N'Chuchi', 1190)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Ryan', N'Dipaculao Aurora', 9186825747, NULL, N'Lea', 1191)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tolentino Weng', NULL, 9185896401, NULL, N'Rain', 1192)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torrejos Arnie', NULL, 9385641818, NULL, N'Tom', 1193)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torrena Marlon', N'Nonong, San Luis Aurora', 9100620550, NULL, N'Rovi', 1194)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torres Alyssa', N'Castillo, Baler Aurora', 9483976173, NULL, N'Nicky', 1195)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torres Daniel', NULL, 9473218988, NULL, N'Mija', 1196)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torres Karissa', N'Suklayin, Baler Aurora', 9061208593, NULL, N'Roxanne', 1197)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torres Mary', NULL, 9203874401, NULL, N'Indie', 1198)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Torres Winnie', NULL, 9565938534, NULL, N'Haram', 1199)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tradio Alfayne', N'Dilasag Aurora', 9069607221, NULL, N'Olryt', 1200)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Trajano Michelle', N'Pingit, Baler Aurora', 9193270101, NULL, N'Rajen//Emme', 1201)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tresvalles Regine', NULL, 9073892569, NULL, N'Stiff', 1202)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tricilla April Joy', NULL, 9195516887, NULL, N'Stifler', 1203)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Trinidad essin', NULL, 9772820797, NULL, N'Floki', 1204)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Trinidad Louie', NULL, 9287908537, NULL, N'Gizmo', 1205)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Trucilla Violeta', N'Buhangin, Baler Aurora', 9212918118, NULL, N'Shadow', 1206)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tualla Jose', N'Baler aurora', 9270213499, NULL, N'Pebbles', 1207)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tubeje Gelina', NULL, 9216231285, NULL, N'Azi', 1208)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Turaja Raquel', N'Sabang, Baler Aurora', 9208720475, NULL, N'Chi-Chi', 1209)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Turgo Winjoy', NULL, 9453344919, NULL, N'Shintaron', 1210)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Turingan John', NULL, 9060148329, NULL, N'Trixie', 1211)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tuzon Jez', N'Buhangin, Baler Aurora', 9999446472, NULL, N'Bulley', 1212)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Tuzon Nancy', NULL, 9195516716, NULL, N'Putol', 1213)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Umali Katrina', N'San luis Aurora', 9284653151, NULL, N'Shannel', 1214)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Urbino Euice', NULL, 9338164121, NULL, N'Hunter', 1215)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Urbino Roi', NULL, 9338164114, NULL, N'Teptep/Tadashi', 1216)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Urpiano Marita', N'Maria Aurora, Aurora', 9296865375, NULL, N'Haba', 1217)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Uy Choco', N'Suklayin, Baler Aurora', 9479661519, NULL, N'Jumper', 1218)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valdez Christine', NULL, 9382368876, NULL, N'Seth', 1219)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valdez Reina Fiel', NULL, 9662037972, NULL, N'Winnie', 1220)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valentin Jan', NULL, 9174471314, NULL, N'Sniper', 1221)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Alelie', N'Reserva, Baler Aurora', 9990191626, NULL, N'HAppy', 1222)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Anthony', N'Baler Aurora', 9300488081, NULL, N'Bulati', 1223)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Athena', N'Reserva, Baler Aurora', 9481745356, NULL, N'Rocky', 1224)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Chona', NULL, 9178340409, NULL, N'Max', 1225)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Clive', N'San Luis Aurora', 9507164789, NULL, N'Digong', 1226)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valenzuela Elena', N'Sabang, Baler Aurora', 9128567677, NULL, N'Tuts', 1227)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valero Gabriela', N'Buhangin, Baler Aurora', 9183777336, NULL, N'Tobby', 1228)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Valin Ma. Rowena', NULL, 9125239252, NULL, N'Unli', 1229)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vallejo Aurora', NULL, 9151891968, NULL, N'Allie', 1230)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vanzuela Fidel ', NULL, 9297949045, NULL, N'Dollar', 1231)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vargas Gail', NULL, 9560799719, NULL, N'Whitey', 1232)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vasquez Edward', NULL, NULL, NULL, N'Putz', 1233)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Velasco Dianne', NULL, 9990232319, NULL, N'Clipper', 1234)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ventura Gabriel', NULL, 9125831434, NULL, N'Mimi', 1235)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ventura Joanna', NULL, 9981965842, NULL, N'Menchie', 1236)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Verana Alma', NULL, 9466712123, NULL, N'Limpi', 1237)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vermudez Andy', NULL, 9178157866, NULL, N'Tiny', 1238)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vertudez Andy', NULL, 9184357126, NULL, N'Wigan/Kima', 1239)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viado Apple', N'Buhangin, Baler Aurora', 9285204070, NULL, NULL, 1240)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Adrian', N'Dipaculao Aurora', 9999234984, NULL, N'Sophie', 1241)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Edison', NULL, 9399014529, NULL, N'Kassy', 1242)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Janelle', N'Suklayin, Baler Aurora', 9126726975, NULL, N'Oreo', 1243)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Keith', NULL, 9298186555, NULL, NULL, 1244)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Vanessa', NULL, 9501608702, NULL, N'Minho', 1245)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viernes Von', NULL, 9496293808, NULL, N'Skyler Love', 1246)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vila Jefferson', N'Suklayin, Baler Aurora', 9991558815, NULL, N'Max', 1247)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villa Jonathan', NULL, 9192909993, NULL, N'Cardo', 1248)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villabeza Janeth', NULL, NULL, NULL, N'Athena/Kratos', 1249)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villaflor Sumilang', NULL, 9209671997, NULL, N'Jumbo', 1250)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villagra Ice', NULL, 9190016939, NULL, N'Bella', 1251)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villaluz Jewel', NULL, 9222246928, NULL, N'Chico', 1252)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villamar Zharieka', NULL, 9284058928, NULL, N'Baguio', 1253)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villamora Quenie', NULL, 9359602023, NULL, N'Kinitoom', 1254)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva Emely ', NULL, 9103399670, NULL, N'Chongke', 1255)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva John', N'Maria Aurora, Aurora', 9204031690, NULL, N'Sky', 1256)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva Lolita', N'Maria Aurora, Aurora', 9216857323, NULL, N'Seung Yu', 1257)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva Lou', NULL, 9173124367, NULL, N'Jar', 1258)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva Verjie', NULL, 9288018633, NULL, N'Mikay', 1259)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villanueva Veronica', NULL, NULL, NULL, N'Mutya', 1260)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villapa ken', N'Maria Aurora, Aurora', 9496292995, NULL, N'Trixie', 1261)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villar Danzen', NULL, 9360588629, NULL, NULL, 1262)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villareal Raquel', NULL, 9667311750, NULL, N'Rie-Rie', 1263)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villarial Khaycee', N'San Luis Aurora', 9382292818, NULL, N'Francis', 1264)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villaros Constancia', N'Maria Aurora, Aurora', 9471569028, NULL, N'Tippy', 1265)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Villaros Kim Harlyn', NULL, 9127225338, NULL, N'Tippy', 1266)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viloria Naty', N'Maria Aurora, Aurora', 9101432800, NULL, N'Ginger', 1267)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viloria sofia', NULL, 9052492302, NULL, N'Roman', 1268)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Vinarao Janelle', N'Zabali, Baler Aurora', 9396549855, NULL, N'Toby//Fronia', 1269)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viray Rowena', N'Baler aurora', 9102042441, NULL, N'Akira Myki', 1270)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viron Cesar', NULL, 9067571720, NULL, N'Timy', 1271)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Virrey Cheryl', NULL, 9190047231, NULL, N'Kit ', 1272)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Virrey Mary Jane', NULL, 9464601881, NULL, N'Sarah', 1273)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Virrey Ritchel', NULL, 9650475227, NULL, N'Nena', 1274)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Viz colleen', NULL, 9186362032, NULL, N'Coco', 1275)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Warr April Shane', N'Zabali, Baler Aurora', 9984909176, NULL, N'Zack', 1276)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Yambot Ricardo', N'Maria Aurora, Aurora', 9394673258, NULL, N'Bam -Bam', 1277)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Yamsuan Aldin', NULL, 9079115535, NULL, N'Candies', 1278)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Ydia Randy', N'Maria Aurora, Aurora', 9481756419, NULL, NULL, 1279)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Zabat Josell', N'Sabang, Baler Aurora', 9105695486, NULL, N'Shishi', 1280)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Zamora Rechelle', N'Maria Aurora, Aurora', 9282330011, NULL, N'Chooky', 1281)
GO
INSERT [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322] ([Owner's ID / Code], [Owner's Name], [Address], [Contact Number], [Email Address], [Pet's Name (Pet 1/Pet 2)], [tempRowID]) VALUES (NULL, N'Zapanta Minay', N'Baler aurora', 9192056966, NULL, N'Sky', 1282)
GO

Declare @TheAnimaDoctorClinicAndGroomingServices_ID_Company INT = 46

DECLARE @ClientRecord TABLE (ID_Client INT, Name Varchar(MAX), Address  Varchar(MAX), ContactNumber Varchar(MAX), EmailAddress  Varchar(MAX), PatientNames VARCHAR(MAX) ,tempID  Varchar(MAX))
DECLARE @ClientPatients TABLE(ID_Client INT, PatientNames VARCHAR(MAX))

insert @ClientRecord 
(
	Name
   ,Address
   ,ContactNumber
   ,EmailAddress
   ,tempID
   ,PatientNames
)
SELECT  [Owner's Name]
	   ,[Address]
	   ,CONVERT(DECIMAL, [Contact Number])
	   ,[Email Address]
	   ,[tempRowID]
	   ,[Pet's Name (Pet 1/Pet 2)]
FROM    [TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]
 
INSERT dbo.tClient (
	ID_Company,
	Code,
	Name,
	Address,
	ContactNumber,
	Email,
	IsActive,
	Comment,
	DateCreated,
	DateModified,
	ID_CreatedBy,
	ID_LastModifiedBy,
	ContactNumber2,
	tempID
)
SELECT  	
	@TheAnimaDoctorClinicAndGroomingServices_ID_Company
   ,NULL
   ,Name
   ,Address
   ,ContactNumber
   ,EmailAddress	
   ,1
   ,'Import 2021-03-22 8pm'
   ,GETDATE()
   ,GETDATE()
   ,1
   ,1
   ,NULL
   ,tempID
FROM    @ClientRecord

UPDATE @ClientRecord
	SET ID_Client = c.id
FROM @ClientRecord record
INNER JOIN tClient c
	ON c.tempID = record.tempID
WHERE
	ID_Company = @TheAnimaDoctorClinicAndGroomingServices_ID_Company


INSERT @ClientPatients
SELECT  
	ID_Client, 
	ISNULL(PatientNames,'')
FROM  @ClientRecord
WHERE
	 ISNULL(PatientNames,'') NOT LIKE '%/%' AND
	 ISNULL(PatientNames,'') <> ''


DECLARE @ID_Client VARCHAR(MAX)
DECLARE @PetName VARCHAR(MAX)

DECLARE db_cursor CURSOR FOR SELECT  
								ID_Client, 
								ISNULL(PatientNames,'')
							 FROM  @ClientRecord
							 WHERE
									ISNULL(PatientNames,'') LIKE '%/%' AND
									ISNULL(PatientNames,'') <> ''

OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName

WHILE @@FETCH_STATUS = 0
BEGIN
 
INSERT @ClientPatients (ID_Client, PatientNames)
  SELECT
    @ID_Client
   ,Part
  FROM dbo.fGetSplitString(@PetName, '/') gss

FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName
END

CLOSE db_cursor
DEALLOCATE db_cursor

INSERT tPatient (ID_Company,
  ID_Client,
  Code,
  Name,
  Old_patient_id,
  IsNeutered,
  IsDeceased,
  Comment,
  Species,
  ID_Gender,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy,
  FirstName,
  LastName,
  MiddleName,
  Email,
  FullAddress,
  ID_Country,
  ContactNumber)
SELECT
  @TheAnimaDoctorClinicAndGroomingServices_ID_Company
  ,ID_Client
  , NULL
  ,PatientNames
  ,NULL
  ,0
  ,0
  ,'Import 2021-03-22 8pm'
  ,''
  ,NULL,1,GETDATE(),GETDATE(),1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL
FROM @ClientPatients


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]') AND type in (N'U'))
DROP TABLE [dbo].[TheAnimaDoctorClinicAndGroomingServices_ClientPatients_20210322]
GO