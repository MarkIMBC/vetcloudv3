DECLARE @GUID_Company VARCHAR(MAX) = '804E6163-18D4-484D-8981-9EB2E1910925'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

Update ForImport.[dbo].[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]
SET    [Items] = 'Ampicilin Vial',
       [QTY.] = 1
WHERE  [QTY.] = '1 vial'

Update ForImport.[dbo].[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]
SET    [Items] = 'RABISIN Vial',
       [QTY.] = 5
WHERE  [QTY.] = '5 vials'

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item                      INT,
     Name_Item                    VARCHAR(MAX),
     Import_BrandSupplierCategory VARCHAR(MAX),
     CurrentInventoryCount        INT,
     GUID                         VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        Import_BrandSupplierCategory,
        CurrentInventoryCount,
        GUID)
select dbo.fGetCleanedString([ITEMS]),
       dbo.fGetCleanedString([Category/Brand/Supplier]),
       dbo.fGetCleanedString([QTY.]),
       [GIUD]
FROM   ForImport.[dbo].[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]

DELETE FROM @forImport
WHERE  Name_Item IN (SELECT DIstinct Import_BrandSupplierCategory
                     FROM   @forImport)

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

-- Remove Duplicate: dr clauders intestinal diet	OTHERS	0
DELETE @forImport
WHERE  GUID = '999A3B61-40A8-44E6-A15F-137360F5CDEA-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

Update @forImport
SET    Name_Item = 'Furosemide (INJECTABLES)'
WHERE  GUID = '51BDC776-F5C5-4CC3-A404-354383A8135E-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

Update @forImport
SET    Name_Item = 'Furosemide (MEDS)'
WHERE  GUID = '40748D82-2F4D-4230-BF22-0243523F160B-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

--goclin 150	OTHERS	0
DELETE @forImport
WHERE  GUID = 'EBF219BA-6C65-446B-99D2-43ECAB9415A8-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

Update @forImport
SET    Name_Item = 'Tranexamic Acid (INJECTABLES)'
WHERE  GUID = '535329B9-A7B7-4DFC-B751-4D7112904645-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

Update @forImport
SET    Name_Item = 'Tranexamic Acid (MEDS)'
WHERE  GUID = '776D08C9-8B26-42E3-ACA8-FEDB659C017C-[Petlink)POTRERO-STOCK-INVENTORY(1)_20220810_2257]'

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

SELECT *
FROM   @forImport

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

SELECT *
FROM   @forImport

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Import inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  ISNULL(import.CurrentInventoryCount, 0) > 0
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT *
FROM   @adjustInventory 
