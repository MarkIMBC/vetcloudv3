
/****** Object:  Table [dbo].[GSC PETCARE_Services]    Script Date: 7/14/2021 4:57:26 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GSC PETCARE_Services]') AND type in (N'U'))
DROP TABLE [dbo].[GSC PETCARE_Services]
GO
/****** Object:  Table [dbo].[GSC PETCARE_Products]    Script Date: 7/14/2021 4:57:26 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GSC PETCARE_Products]') AND type in (N'U'))
DROP TABLE [dbo].[GSC PETCARE_Products]
GO
/****** Object:  Table [dbo].[GSC PETCARE_Products]    Script Date: 7/14/2021 4:57:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GSC PETCARE_Products](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [nvarchar](255) NULL,
	[Selling Price] [float] NULL,
	[Current Inventory Count] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GSC PETCARE_Services]    Script Date: 7/14/2021 4:57:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GSC PETCARE_Services](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Price] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TetraDecanol Complex Soft Gel', N'Drug/Medicine', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hello-Pet 2-Sided Pin Brush', N'Accessories', NULL, 215, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Aller-eze', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ALT', N'Laboratory Test & Kits', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ambroxol 15mg/5ml', N'Drug/Medicine', NULL, 80, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ambroxol Tablet', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amitraz', N'Tick & Flea Prevention', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin Suspension 60 ml', N'Drug/Medicine', NULL, 90, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin Capsule 250 mg', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin Capsule 500 mg', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin Drops 10 ml', N'Drug/Medicine', NULL, 55, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxicillin LA (inj)', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amoxyvet 50', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ampicillin', N'Injectible', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Animal Sci Enzymatic Toothpaste', N'Merchandise & Accessories', NULL, 390, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Animal Science Anti Mange 100ml', N'Merchandise & Accessories', NULL, 800, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Animycin', N'Drug/Medicine', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Apoquel 16mg tab', N'Drug/Medicine', NULL, 290, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Apoquel 5.4mg tab', N'Drug/Medicine', NULL, 190, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Arthropet Tablet', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ascorbic Acid tab', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Atropine Sulfate', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'B-complex Tablet', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bandage Gauze 4x4', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Barfy Herbal Soap', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Barfy Multivitamins', N'Drug/Medicine', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Barfy Shampoo 200mL', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Beef Meal Adult Bag', N'Pet Food', NULL, 3400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Beef Meal Adult 1kg', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Beef Meal Puppy Bag', N'Pet Food', NULL, 3500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Beef Meal Puppy 1kg', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Biocatalin', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Biocure Doxycycline (Retail)', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bisacodyl Suppository', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bitch supplement', N'Drug/Medicine', NULL, 210, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bitter Bandage', N'Merchandise & Accessories', NULL, 370, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bladder Control', N'Drug/Medicine', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Blood Bag', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Blood Transfusion Set', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 10-20kg', N'Tick & Flea Prevention', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 20-40kg', N'Tick & Flea Prevention', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 2-4.5 kg', N'Tick & Flea Prevention', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto 4.5-10kg', N'Tick & Flea Prevention', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto Cats', N'Tick & Flea Prevention', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Brewer''s Yeast Retail', N'Drug/Medicine', NULL, 4, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Broadline 2.5kg', N'Tick & Flea Prevention', NULL, 540, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Broadline 2.5kg-7.5kg', N'Tick & Flea Prevention', NULL, 605, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bromhexine Inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Broncure 60 ml', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BUN', N'Laboratory Test & Kits', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Calcium Borogluconate (CBG)', N'Injectible', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Calcium Chewables K-9', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Calphos D3', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob D', N'Injectible', NULL, 300, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob P', N'Injectible', NULL, 300, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canibrom  60 ml', N'Drug/Medicine', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canicee  60 ml', N'Drug/Medicine', NULL, 160, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Carbocisteine 250/5ml syrup', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Carprofen 50mg', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CDV + CPV Test Kit', N'Laboratory Test & Kits', NULL, 1200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefalexin 500mg Capsule', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefalexin 250mg Capsule', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefalexin Suspension 125mg', N'Drug/Medicine', NULL, 90, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefalexin Suspension 250mg', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cefavet', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cetirizine 10mg tab', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cetirizine 5mg/5ml syrup', N'Drug/Medicine', NULL, 140, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Classic Pets Puppy Milk 1kg', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Classic Pets Puppy Milk 10kg', N'Pet Food', NULL, 1700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clens Wound Spray', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clindamycin', N'Drug/Medicine', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clumping Cat Litter Sand 10L', N'Merchandise & Accessories', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clumping Cat Litter Sand 5L', N'Merchandise & Accessories', NULL, 300, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Co-amoxiclav 500mg/125 mg Tablet', N'Drug/Medicine', NULL, 35, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Co-amoxiclav 250mg/62.5mg/5ml Suspension', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coatshine 120 ml', N'Drug/Medicine', NULL, 310, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coatshine 60ml', N'Drug/Medicine', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coforta inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole 800mg/160mg Tablet', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole  400mg/80mg Tablet', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole Inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cotrimoxazole suspension', N'Drug/Medicine', NULL, 80, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CPV + CCV Test Kit', N'Laboratory Test & Kits', NULL, 1200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Crea', N'Laboratory Test & Kits', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cuties Cat Tuna', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cuties Cat Tuna Bag 22kg', N'Pet Food', NULL, 3400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'D5LRS 1000 ml', N'Medical Supply', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Deltacal Retail', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dexamethasone inj.', N'Injectible', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dextrose Powder 100g', N'Medical Supply', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Distemper test kit', N'Laboratory Test & Kits', NULL, 900, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dish 16 oz.', N'Merchandise & Accessories', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dish 24 oz', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dish 32 oz.', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dish 64 oz.', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dish 96 oz', N'Merchandise & Accessories', NULL, 360, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dog Dress', N'Merchandise & Accessories', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doramectin inj', N'Injectible', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxyvet-10 Caplet', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Drontal Bot', N'Drug/Medicine', NULL, 650, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Drontal tab', N'Drug/Medicine', NULL, 275, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Duphafral inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Duphalyte inj', N'Injectible', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutri-Vet Ear Cleanse', N'Eye & Ear Drops', NULL, 517, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Ear Drops', N'Eye & Ear Drops', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 1', N'Merchandise & Accessories', NULL, 370, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 2', N'Merchandise & Accessories', NULL, 330, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 3', N'Merchandise & Accessories', NULL, 210, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 4', N'Merchandise & Accessories', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 5', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 6', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'E-collar size 7', N'Merchandise & Accessories', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ecolmin Inj', N'Injectible', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ehrlichia Test Kit', N'Laboratory Test & Kits', NULL, 900, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ener-G  60 ml bot', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enmalac bot', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enrofloxacin (papi) tablet', N'Drug/Medicine', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Enrofloxacin inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Epinephrine vials', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Excelvit Tab', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ever Fluffy 1 Liter- Strawberry', N'Merchandise & Accessories', NULL, 370, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ever Fluffy 1Liter - Bubblegum', N'Merchandise & Accessories', NULL, 370, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EverFluffy 250ml (Bubblegum)', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EverFluffy 250ml (Strawberry)', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EverFluffy 500ml (Bubblegum)', N'Merchandise & Accessories', NULL, 160, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EverFluffy 500ml (Strawberry)', N'Merchandise & Accessories', NULL, 160, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Eye Drops', N'Eye & Ear Drops', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutri-Vet Eye Rinse', N'Eye & Ear Drops', NULL, 630, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fe + B complex inj', N'Injectible', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fercobsang', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline 0-10kg.', N'Tick & Flea Prevention', NULL, 480, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline 10-20kg', N'Tick & Flea Prevention', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline 20-40kg.', N'Tick & Flea Prevention', NULL, 540, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furosemide 40 mg Tablet', N'Drug/Medicine', NULL, 14, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furosemide inj.', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Furosemide 20mg Tablet', N'Drug/Medicine', NULL, 5, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Madre de Cacao Soap', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Shampoo - Strawberry', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Shampoo- Melon', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Shampoo - Lemon', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Shampoo - Green Apple', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FurVet Shampoo - Red Apple', N'Merchandise & Accessories', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Gentin', N'Eye & Ear Drops', NULL, 450, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Glucosamine', N'Drug/Medicine', NULL, 40, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Healthy Liver', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Heartworm test', N'Laboratory Test & Kits', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hemacare FE 120 ml', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hemacare FE 60ml', N'Drug/Medicine', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Holistic - Adult bag', N'Pet Food', NULL, 3000, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Holistic - Adult retail', N'Pet Food', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Holistic - Puppy bag', N'Pet Food', NULL, 3200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Holistic - Puppy retail', N'Pet Food', NULL, 210, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Imizol 10ml', N'Injectible', NULL, 300, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immune Health Animal Science', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Suspension', N'Drug/Medicine', NULL, 800, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol tab retail', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Iodine Povidone', N'Medical Supply', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Iozin wound spray', N'Merchandise & Accessories', NULL, 320, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Advanced Multivitamins', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Amoxicillin', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ketoprofen injection', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lc Dox Syrup 120ml', N'Drug/Medicine', NULL, 400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Dox Syrup 60ml', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC DOX tab 100 mg', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lc Scour', N'Drug/Medicine', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Vit Syrup 120ml', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Vit OB Susp 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Vit OB Susp 60ml', N'Drug/Medicine', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Vit Plus Syrup 60mL', N'Drug/Medicine', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC Vit Syrup 60mL', N'Drug/Medicine', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-Vit OB Retail', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Leptotest kit', N'Laboratory Test & Kits', NULL, 900, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv. 52 Forte Retail', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liverator 30ml', N'Drug/Medicine', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Livotine 120ml', N'Drug/Medicine', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Livotine 60ml', N'Drug/Medicine', NULL, 160, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lutalyse Inj 30ml/bot', N'Injectible', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Marbofloxacin Inj.', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Matting (Black)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Matting (Blue)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Matting (Green)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Matting (Red)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Matting (White)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metaprime inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Methiovet tab', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metoclopramide 10 mg', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metoclopramide inj.', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metoclopramide Syrup 60ml', N'Drug/Medicine', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole 500mg', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole infusion', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metronidazole Suspension 60ml', N'Drug/Medicine', NULL, 80, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Milk Replacer Furbaby''s retail', N'Merchandise & Accessories', NULL, 210, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K9 Multivitamins', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mupirocin Ointment', N'Medical Supply', NULL, 200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Muzzle - Adjustable (L)', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Muzzle - Adjustable (M)', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Muzzle - Adjustable (S)', N'Merchandise & Accessories', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mycocide Shampoo 150ml', N'Merchandise & Accessories', NULL, 450, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nacalvit-C 120ml', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nasty Habit Retail', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Natural Litter Sand 4kg', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nefrotec DS Retail', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nematocide 15ml', N'Drug/Medicine', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nematocide 60ml', N'Drug/Medicine', NULL, 540, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NexGard 10-25kg', N'Tick & Flea Prevention', NULL, 600, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NexGard 25-50kg', N'Tick & Flea Prevention', NULL, 650, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NexGard Spectra L (15-30kg)', N'Tick & Flea Prevention', NULL, 950, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra Med (7.5-15kg)', N'Tick & Flea Prevention', NULL, 850, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard spectra Small(3.5-7.5)', N'Tick & Flea Prevention', NULL, 780, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra XL(30-60kg)', N'Tick & Flea Prevention', NULL, 1025, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nexgard Spectra XS (2-3.5Kg)', N'Tick & Flea Prevention', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac  5in1', N'Vaccination', NULL, 390, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac 4in1', N'Vaccination', NULL, 360, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac R', N'Vaccination', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac 6in1', N'Vaccination', NULL, 590, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac Syringe 2.5cc', N'Vaccination', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac Tricat', N'Vaccination', NULL, 890, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrical 120ml', N'Drug/Medicine', NULL, 310, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrical 60ml', N'Drug/Medicine', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutriplus gel Virbac', N'Merchandise & Accessories', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutrivet Milk Replacement', N'Merchandise & Accessories', NULL, 210, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Optazol', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ornipural inj. 100ml/bot', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Otiderm', N'Drug/Medicine', NULL, 490, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Oxytetracycline inj', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Oxytocin', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Amitraz - Anti Mange 250mL', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Amitraz- Anti Mange 500 mL', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Amitraz soap', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Anti Tick&Flea Soap', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Barfy Soap', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Doxy Suspension', N'Drug/Medicine', NULL, 270, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Doxy Tab 100mg', N'Drug/Medicine', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Iron Plus', N'Drug/Medicine', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi MVP 120 ml', N'Drug/Medicine', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi OB', N'Drug/Medicine', NULL, 230, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Pet Cologne Spray (Blue)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Pet Cologne Spray (Pink)', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Pet Powder', N'Tick & Flea Prevention', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Papi Pure Madre de Cacao Soap', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Parvo-Distemper Kit', N'Laboratory Test & Kits', NULL, 1200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Parvotest kit', N'Laboratory Test & Kits', NULL, 800, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Beef Chunks', N'Pet Food', NULL, 60, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Chicken & Egg', N'Pet Food', NULL, 160, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Chicken Gravy', N'Pet Food', NULL, 60, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree Dentastix 3-12 mos', N'Pet Food', NULL, 115, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree DentaStix 5-10kg', N'Pet Food', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree DentaStix 10-25kg', N'Pet Food', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Ease', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Reboost 30ml', N'Drug/Medicine', NULL, 350, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Shampoo Bar Fragrance Free', N'Merchandise & Accessories', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Shampoo Bar w/Citronella', N'Merchandise & Accessories', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Grass', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Phytomenadione inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pimobendan 1.25mg', N'Drug/Medicine', NULL, 30, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pimobendan 5mg', N'Drug/Medicine', NULL, 90, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vermifuge Suspension', N'Drug/Medicine', NULL, 500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PNSS 1000ml', N'Medical Supply', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Polymyxin + Neomycin + Dexa', N'Drug/Medicine', NULL, 380, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powder Coated Cage L', N'Merchandise & Accessories', NULL, 2800, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powder Coated Cage M', N'Merchandise & Accessories', NULL, 2300, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powder Coated Cage S', N'Merchandise & Accessories', NULL, 1900, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Powder Coated Cage XS', N'Merchandise & Accessories', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prazilex Tab', N'Drug/Medicine', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisolone inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone 20mg Tablet', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone 5mg Tablet', N'Drug/Medicine', NULL, 5, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Prednisone 10mg Tablet', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Previcox 227mg', N'Drug/Medicine', NULL, 220, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Previcox 57mg', N'Drug/Medicine', NULL, 70, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Princess cat bag 22.7kg', N'Pet Food', NULL, 3780, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Princess Cat retail', N'Pet Food', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Probiotic (Digestive Enzymes)', N'Pet Food', NULL, 25, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ProCyte Dx Reagent Kit', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ProCyte Dx Stain Pack', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Propofol', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Propoxur', N'Tick & Flea Prevention', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pyramid Hill - Beef', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pyramid Hill - Lamb', N'Pet Food', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vermifuge Tablet', N'Drug/Medicine', NULL, 90, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ranitidine Injectable', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Refamol retail', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Reflex Urinary Bag 15kg', N'Pet Food', NULL, 3500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Reflex Urinary Retail 1kg', N'Pet Food', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Remend Corniel Gel 3ml', N'Drug/Medicine', NULL, 1200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Royal Puppy Dewormer 30ml', N'Drug/Medicine', NULL, 199, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Salbutamol 2mg tab', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Salbutamol 2mg/5ml', N'Drug/Medicine', NULL, 80, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Salbutamol/Guaifenesin syrup', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Senior moments', N'Drug/Medicine', NULL, 230, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Septotryl inj. (cotri inj)', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'SNAP 4Dx Plus 5T', N'Laboratory Test & Kits', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'SNAP Parvo Test Kit', N'Laboratory Test & Kits', NULL, 900, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sodago', N'Drug/Medicine', NULL, 1000, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sodium Bicarbonate (tab)', N'Drug/Medicine', NULL, 10, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sodium Bicarbonate inj', N'Drug/Medicine', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Spasmovet Inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Adult Lamb &Rice 9.07kg', N'Pet Food', NULL, 1530, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Adult Lamb& Rice Retail', N'Pet Food', NULL, 170, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Cat 7kg bag', N'Pet Food', NULL, 1260, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Cat Retail', N'Pet Food', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Puppy Lamb & Rice9.07kg', N'Pet Food', NULL, 1620, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Special Puppy Lamb& Rice Retail', N'Pet Food', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tetanus Anti Toxin (TAT)', N'Injectible', NULL, 150, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tobramycin Dexamethasone', N'Drug/Medicine', NULL, 400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tolfedine Retail', N'Drug/Medicine', NULL, 60, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tolfine inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tranexamic Acid Capsule', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tranexamic Acid Inj', N'Injectible', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Travel Crates 1001', N'Merchandise & Accessories', NULL, 1000, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Travel Crates 1002', N'Merchandise & Accessories', NULL, 1500, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Travel Crates 1004', N'Merchandise & Accessories', NULL, 4200, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vaccination Card', N'Vaccination', NULL, 100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Edta Tube', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCheck cProgesterone', N'Laboratory Test & Kits', NULL, 890, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCheck CPV Ag', N'Laboratory Test & Kits', NULL, 800, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCheck cTSH', N'Laboratory Test & Kits', NULL, 1100, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCheck T4', N'Laboratory Test & Kits', NULL, 990, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCO 1000ml', N'Merchandise & Accessories', NULL, 350, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCO 350ml', N'Merchandise & Accessories', NULL, 250, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VCO 500ml', N'Merchandise & Accessories', NULL, 180, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Venoma herbal spray', N'Merchandise & Accessories', NULL, 380, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetnoderm Bio Sulfur+VCO', N'Merchandise & Accessories', NULL, 130, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetnoderm Cream', N'Merchandise & Accessories', NULL, 280, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetnoderm Madre de cacao', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetnoderm Medicated Herbal Soap', N'Merchandise & Accessories', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetnoderm Medicated Shampoo', N'Merchandise & Accessories', NULL, 320, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vincristine', N'Medical Supply', NULL, NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VitaBeef bag', N'Pet Food', NULL, 1400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VitaBeef retail', N'Pet Food', NULL, 140, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VitaLamb bag', N'Pet Food', NULL, 1400, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VitaLamb retail', N'Pet Food', NULL, 140, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Whiskas Tuna Adult', N'Pet Food', NULL, 50, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Whiskas Tuna Jr', N'Pet Food', NULL, 50, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Worm Rid Animal Science', N'Drug/Medicine', NULL, 120, NULL)
GO
INSERT [dbo].[GSC PETCARE_Products] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Worm Rid Suspension', N'Drug/Medicine', NULL, 700, NULL)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Amputation', NULL, 5000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Draining', NULL, 120)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anesthesia - General Injectable', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anesthesia - Sedative', NULL, 600)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anesthesia - GAS (General)', NULL, 2500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anesthesia- Local', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Auricular Hematoma extraction', NULL, 700)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Barium', NULL, 100)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bitter Bandage Dressing', NULL, 370)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Blood Transfusion', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Pinning', NULL, 10000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Bone Splints', NULL, NULL)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Interment Fee', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Carnassial Tooth Removal', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Casting', NULL, 5000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Castration Cat', NULL, 760)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Castration Dog- 10kg Above', NULL, 3260)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Castration Dog- 10kg Below', NULL, 1260)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'catheterization', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'CBC', NULL, 850)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Certificate of examination', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ceasarian Section', NULL, 6000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ceasarian Section + Panhysterectomy', NULL, 7500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Chemotherapy', NULL, 800)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cherry eye removal', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Confinement Fee- Large', NULL, 400)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Confinement Fee- Medium', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Confinement Fee- Small', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cystotomy', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'D5LRS + IV set', NULL, 450)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery assistance/natural del', NULL, 2500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Dental prophylaxis', NULL, 2500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'dewclaw removal 6week up', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Deworming', NULL, 120)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'DFS (Direct Fecal Smear)', NULL, 120)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Drainage', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning with Otitis', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear swabbing', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Taping (excluding tape)', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Emergency PF', NULL, 700)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enterotomy', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Entropion', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Enucleation', NULL, 3500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Euthanasia - Cat', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Euthanasia - Dog', NULL, 750)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Facial Reconstruction Surgery', NULL, 15000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Food charges', NULL, 60)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Gastrotomy', NULL, 3500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Exploratory Laparotomy', NULL, 6000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Extraction of dead fetus', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Glucose test', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Health Certificate', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Hernia Repair', NULL, 2500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'House call', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Laparotomy', NULL, 5000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Laser therapy', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Lavage', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Mamectomy', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Mastectomy', NULL, 3000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'MCF (microfilaria test)', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Medical Certificate', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Microchip', NULL, 600)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail clipping', NULL, 100)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail clipping w/ ingrown nails', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Operating Room Fee (Major Surgeries)', NULL, 1000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy (Spaying) Cat', NULL, 1260)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Panhysterectomy w/ pyometra', NULL, 7000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Orthopedic Surgery', NULL, 10000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oxygen', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Penile Resection', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Peritoneal Paracenthesis', NULL, 1000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Professional Fee', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PF for deworming', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PF for dextrose', NULL, 120)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Proptosis Repair', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Puppy care', NULL, 100)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rectal Prolapse (Purse String)', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Removal of Bone pin/s', NULL, 5000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Repair of Lumbar Vertebrae', NULL, 8000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Repair Of Jaw', NULL, 6000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Skin scraping', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Scrotal ablation', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Spaying - Dog 10kg Above', NULL, 3260)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Spaying - Dog 10kg Below', NULL, 1260)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Sperm count', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Splint', NULL, 250)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Surgical Reconstructive Repair', NULL, 2000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Suture Removal', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail docking', NULL, 550)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking ( 6weeks up)', NULL, 1000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tail Docking (1week old)', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Third eyelid excision', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Thoracotomy', NULL, 2500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Tumor Removal', NULL, 6000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'TVT smear test', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ultrasound', NULL, 800)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethra Repair', NULL, 4000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urethrostomy', NULL, 6000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinalysis', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary catherization', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Paracentesis', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Vaginal smear', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Warts Removal', NULL, 1500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Woods Lamp Test', NULL, 150)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound dressing', NULL, 350)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Repair and/or Dressing', NULL, 1000)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Xray reading', NULL, 200)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'XRay (Large)', NULL, 500)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'XRay (Small)', NULL, 300)
GO
INSERT [dbo].[GSC PETCARE_Services] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Xray Reading', NULL, 200)
GO


DECLARE @GSCPetCareCenterVeterinaryHospital_ID_Company INT = 67

Declare @Record Table
  (
     ItemName        Varchar(MAX),
     ID_ItemCategory INT,
     UnitCost        Decimal,
     UnitPrice       Decimal,
     ID_ItemType     INT
  )

INSERT @Record
       (ItemName,
        ID_ItemCategory,
        UnitCost,
        UnitPrice,
        ID_ItemType)
SELECT LTRIM(RTRIM(import.Name)),
       cat.ID,
       ISNULL([Buying Price], 0),
       ISNULL([Selling Price], 0),
       2
FROM   [GSC PETCARE_Products] import
       LEFT JOIN (SELECT MAX(ID) ID,
                         Name
                  FROM   tItemCategory category
                  WHERE  ID_ItemType = 2
                  GROUP  BY NAME) cat
              ON TRIM(LOWER(cat.Name)) = TRIM(LOWER(import.Category))

INSERT @Record
       (ItemName,
        ID_ItemCategory,
        UnitCost,
        UnitPrice,
        ID_ItemType)
SELECT LTRIM(RTRIM(import.Name)),
       cat.ID,
       0,
       ISNULL([Price], 0),
       1
FROM   [GSC PETCARE_Services] import
       LEFT JOIN (SELECT MAX(ID) ID,
                         Name
                  FROM   tItemCategory category
                  WHERE  ID_ItemType = 1
                  GROUP  BY NAME) cat
              ON TRIM(LOWER(cat.Name)) = TRIM(LOWER(import.Category))

 
SELECT  Count(*)
FROM   @Record 
DELETE @Record
FROM   @Record importRecord
				  INNER JOIN tItem item ON   LTRIM(RTRIM(importRecord.ITEMName)) = LTRIM(RTRIM(item.Name)) and  
				   importRecord.ID_ITemType = item. ID_ITemType
				 WHERE ID_Company = @GSCPetCareCenterVeterinaryHospital_ID_Company
               AND ISACTIVE = 1

SELECT   Count(*) 
FROM   @Record 
SELECT   *
FROM   @Record   order by ItemName


INSERT tItem
       (ID_Company,
        Name,
        UnitPrice,
        ID_ItemType,
        UnitCost,
        ID_ItemCategory,
        Code,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        CurrentInventoryCount,
        Old_item_id)
SELECT @GSCPetCareCenterVeterinaryHospital_ID_Company,
       LTRIM(RTRIM(ItemName)),
       UnitPrice,
       ID_ITemType,
       UnitCost,
       ID_ItemCategory,
       NULL,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1,
       0,
       NULL V 
FROM   @Record
ORDER  BY ItemName
 

 DROP TABLE [dbo].[GSC PETCARE_Products]
DROP TABLE [dbo ].[GSC PETCARE_Services]




