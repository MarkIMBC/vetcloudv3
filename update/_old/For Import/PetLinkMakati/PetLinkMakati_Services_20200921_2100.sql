UPDATE tItem 
SET IsActive=0
WHERE ID_Company=153 AND ID_ItemType=1


DECLARE @Is_Active AS INT
DECLARE @ID_Company AS INT
DECLARE @Comment AS VARCHAR(300)
DECLARE @ID_CreatedBy AS INT
DECLARE @ID_ModifiedBy AS INT
DECLARE @ID_ItemType AS INT
DECLARE @Min_InventoryCount AS INT
DECLARE @Max_InventoryCount AS INT
DECLARE @Unit_Cost AS INT
DECLARE @ID_InventoryStatus AS INT

SET @Is_Active = 1
SET @ID_Company = 153
SET @ID_CreatedBy = 1
SET @ID_ModifiedBy = 1
SET @ID_ItemType = 1
SET @Min_InventoryCount = NULL
SET @Max_InventoryCount = NULL
SET @Unit_Cost = NULL
SET @ID_InventoryStatus =1
SET @Comment ='Imported 09/20/2021 09:30 PM'

INSERT INTO [dbo].[tItem]
           ([Code]
           ,[Name]
           ,[IsActive]
           ,[ID_Company]
           ,[Comment]
           ,[DateCreated]
           ,[DateModified]
           ,[ID_CreatedBy]
           ,[ID_LastModifiedBy]
           ,[ID_ItemType]
           ,[ID_ItemCategory]
           ,[MinInventoryCount]
           ,[MaxInventoryCount]
           ,[UnitCost]
           ,[UnitPrice]
           ,[CurrentInventoryCount]
           ,[Old_item_id]
           ,[Old_procedure_id]
           ,[OtherInfo_DateExpiration]
           ,[ID_InventoryStatus]
           ,[BarCode]
           ,[CustomCode]
           ,[_tempSupplier])

SELECT 
			NULL,
			import.[Services],
			@Is_Active,
			@ID_Company,
			@Comment,
			GETDATE(),
			GETDATE(),
			@ID_CreatedBy,
			@ID_ModifiedBy,
			@ID_ItemType,
			import.[Category],
			@Min_InventoryCount,
			@Max_InventoryCount,
			@Unit_Cost,
			import.[Price],
			0,
			NULL,
			NULL,
			NULL,
			@ID_InventoryStatus,
			NULL,
			NULL,
			NULL


FROM  petlinkmakati_services import

 



SELECT * FROM tItem WHERE ID_Company=153 AND ID_ItemType=1 
