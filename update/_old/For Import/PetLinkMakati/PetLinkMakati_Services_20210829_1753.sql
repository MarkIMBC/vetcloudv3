DECLARE @ID_Company_PetlinkMakati INT = 153
DECLARE @ItemServices TABLE
  (
     ItemName        VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 4)
  )

INSERT @ItemServices
SELECT import.Name,
       cat.ID,
       ISNULL(import.[Price], 0)
FROM   PetLinkMakati_Services_20210829_1753 import
       LEFT JOIN (SELECT MAX(ID) ID,
                         Name
                  FROM   tItemCategory
                  WHERE  ID_ItemType = 1 GROUP BY Name) cat
              ON UPPER(import.Category) = UPPER(cat.Name)

INSERT tItem
       (ID_Company,
        Name,
        UnitPrice,
        ID_ItemType,
        UnitCost,
        ID_ItemCategory,
        Code,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Old_item_id)
SELECT @ID_Company_PetlinkMakati,
       ItemName,
       UnitPrice,
       1,
       NULL,
       ID_ItemCategory,
       NULL,
       1,
       NULL,
       GETDATE(),
       GETDATE(),
       1,
       1,
       NULL
FROM   @ItemServices

GO 
