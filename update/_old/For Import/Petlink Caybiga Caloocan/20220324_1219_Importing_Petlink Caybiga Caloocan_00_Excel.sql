
GO
/****** Object:  Table [dbo].[PetLink_Caybiga_Services]    Script Date: 24/03/2022 12:17:41 pm ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PetLink_Caybiga_Services]') AND type in (N'U'))
DROP TABLE [dbo].[PetLink_Caybiga_Services]
GO
/****** Object:  Table [dbo].[PetLink_Caybiga_Items]    Script Date: 24/03/2022 12:17:41 pm ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PetLink_Caybiga_Items]') AND type in (N'U'))
DROP TABLE [dbo].[PetLink_Caybiga_Items]
GO
/****** Object:  Table [dbo].[PetLink_Caybiga_Items]    Script Date: 24/03/2022 12:17:41 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetLink_Caybiga_Items](
	[Category] [nvarchar](255) NULL,
	[Item] [nvarchar](255) NULL,
	[Option] [nvarchar](255) NULL,
	[Price] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PetLink_Caybiga_Services]    Script Date: 24/03/2022 12:17:42 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetLink_Caybiga_Services](
	[CATEGORY] [nvarchar](255) NULL,
	[Name2] [nvarchar](255) NULL,
	[SERVICES] [nvarchar](255) NULL,
	[SERVICE FEE] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Holistic', N'Hollistic Adult 15kg', 2925)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Holistic', N'Holistic Adult 1kg Repack', 215)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Holistic', N'Holistic Puppy 1kg Repack', 205)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Holistic', N'Hollistic Puppy 15kg', 3075)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'Pet Food', N'VITALITY', N'ADULT 1KG REPACK', 160)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'Pet Food', N'VITALITY', N'PUPPY 1KG REPACK', 175)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Lactulose 120ml', NULL, 230)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Renal N Paste', NULL, 1150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Singen Liquid Kidney for Cats', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Oxyfresh Water Additive 500ml', NULL, 1250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cystocure', NULL, 55)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'METROGEN 60ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Bisacodyl tab', NULL, 10)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'3', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'4', 110)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'5', 95)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'6', 85)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'7', 80)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'1', 170)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET E COLLAR', N'2', 155)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Colmoc', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Petzyme Spray 330ml', NULL, 195)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Bromivet', NULL, 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Poenophen', NULL, 160)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Proticure', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Otishield', NULL, 280)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cotrimed', NULL, 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Prednisone syrup', NULL, 185)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Neovax', NULL, 575)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Omni Hyal 14mg', NULL, 800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Canidox', N'120ml', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PAPI AMITRAZ SOAP', NULL, 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PAPI AMITRAZ 500ML', NULL, 349)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PAPI AMITRAZ 250ML', NULL, 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PETS OWN', N'CAT&KITTEN', 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PETS OWN', N'DOG&PUPPY', 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'ZERT DENTACARE', N'PISTACIO GELATO', 165)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Goats Milk', NULL, 625)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enterovet', N'tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Amproxol Hcl', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Play pets', N'1L', 320)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'TOP BREED', N'ADULT 1KG REPACK', 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'TOP BREED', N'PUPPY 1KG REPACK', 85)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Lysine for Cats', NULL, 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Nutridrops 15ml', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enfaboost', NULL, 285)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Heat Pad', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'CO-AMOXICLAV 125MG', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cefuroxime tab', NULL, 45)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Tobramycn', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Petmedin 1pt25mg', NULL, 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Petmedin 5mg', NULL, 110)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Goclin 150', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Ambroxol tab', N'tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Topicure', NULL, 380)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Vet Naturals Anti Fungal', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Vet Naturals Anti Parasitic', NULL, 170)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Vat Naturals Wound', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Furosemide tab', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FURFECT MADRE DE CACAO SOAP', NULL, 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Metoclopramide tab', NULL, 10)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Emerflox', NULL, 280)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Comoxi', NULL, 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'BeSame 100', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Actea Oral', NULL, 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Florentero Paste', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Microcyn Spray 3oz', NULL, 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Lactulose 60ml', NULL, 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Laktrazine', N'Tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Laktrazine', N'Bottle', 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Hepatosure', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Whiskas Hairball Control', NULL, 330)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'WHISKAS', N'MACKEREL POUCH', 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'WHISKAS', N'TUNA 400G', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL DOG', N'ADULT 9KG', 1050)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL DOG', N'PUPPY 9KG', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL DOG', N'ADULT 1pt5KG', 220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Topbreed', N'Puppy', 85)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Topbreed', N'Adult', 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'GASTRO INTESTINAL LOW FAT CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RECOVERY CAN 195G', 240)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Dr Clauders Kidney diet', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Dr Clauders Liver Diet', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Dr  Clauders Hair&Fur diet', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Ketoconazole', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Lc scour', NULL, 290)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'CO AMOX TAB 625MG', NULL, 40)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Apoquel 16mg', N'16mg', 225)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Monodox', N'syrup', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Eye Vitamin', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Lymedox', NULL, 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Gabapentin 100mg tab', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'TOPTAILS SHAMPOO', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Shampoo-Fresh Mint 500mL', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Amoxivet', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Apoquel 5pt4mg', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'AURIZON 10ML', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'BRONCURE 60ML', NULL, 275)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Biocure', N'Tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Biocure', N'Bottle', 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Bonazil', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Canixetin', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'CEFAID 60ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cefavet', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'CETIRIAID 60ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cetirizine Tab', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cflex', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Chlorphenamine Tablet', NULL, 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Cimetidine', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Clindamycin', NULL, 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'CO-AMOXICLAV 250MG', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'DOXYVET', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Dexamethasone Tab', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Diphenhydramine Cap', NULL, 75)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Doxy Adult (50mg)', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Doxy Puppy (25mg)', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enroflox', N'Box', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enroflox', N'Piece', 30)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enalapril 10mg', NULL, 30)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Enalapril 5mg', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Gentin', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'GASTROAID 60ML', NULL, 220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'IOZIN SPRAY 120ML', NULL, 325)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'MARBOCYL 80MG', NULL, 220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'MEDALEM 60ml', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'MEDALEM 120ml', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CHEM15', NULL, 2300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'NOVIBAC 8IN1', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Petmedin 2pt5mg', NULL, 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Pet Reboost', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Besame 200', NULL, 95)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Renal Paste', NULL, 900)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Trigenna', N'tab 500g', 64)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Prednisone 5mg', NULL, 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Tawa tawa cap', NULL, 45)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Monodox tab', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Megaderm 4ml', NULL, 30)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Megaderm 8ml', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'JOJOBA', N'PET CREAM 50G', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'TOPTAILS SOAP', NULL, 115)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Soap-Fresh Mint', 115)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Soap-Puppy w Oatmeal', 115)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Soap-Odor Absorber', 115)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Soap-Soft & Shine', 115)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'BOSCH MILK 400G', NULL, 425)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'CATSAN', N'ULTRA 5L', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'CATSAN', N'ULTRA 10L', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'DENTASTIX LARGE', 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'DENTASTIX MEDIUM', 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'DENTASTIX PUPPY', 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'DENTASTIX SMALL', 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'DENTASTIX TOY DOG', 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Shampoo-Sweet Apple 500mL', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Shampoo-Puppy w Oatmeal 500mL', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Toptails', N'Shampoo-Odor Absorber 500mL', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'CHKN 700G', 170)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FURPECT FIPRONIL SHAMPOO', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FURBABY MILK REPLACER', N'Sachet', 0)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FURBABY MILK REPLACER', N'Box', 0)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FELINE FRESH', N'LEMON 10L', 320)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'FELINE FRESH', N'LEMON 5L', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Happy Fur Charcoal', NULL, 155)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Happy Fur Moisturizer', NULL, 155)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'JOJOBA', N'EAR CLEANSER 120ML', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Jojoba Pet Herbal Soap', NULL, 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'JOJOBA', N'ORGANIC SHAMPOO 1L', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'JOJOBA', N'ORGANIC SHAMPOO 500ML', 320)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PAPI HAIR GROWER SOAP', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PAPI MADRE DE CACAO SOAP', NULL, 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET BREATHE', NULL, 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PET SPRAY', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PETZYME COLOGNE', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Papi', N'Amitraz', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Patimax Premium', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'5 KINDS OF MEAT 400G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'BEEF 400G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'CHKN 500G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'PUPPY 400G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Pedigree Chicken & Tuna Pouch', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Meat Jerky', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'PEDIGREE', N'GRIL LIVER POUCH', 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CA', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CBC', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CHEM10', NULL, 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CHOL', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'CREA', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'LYTE4', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'NSAID6', NULL, 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + 3P (BUN-CREA-ALT)', 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + CHEM10', 2750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + CHEM 15', 3050)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + LYTE 4', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + NSAID6', 2350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + Critical Care', 2400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + Comprehensive Diagnostic Profile', 2600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'Chem17', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'PACKAGES', N'CBC + Chem 17', 3400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'SDMA', NULL, 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'T4', NULL, 1750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'TBIL', NULL, 200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'TP', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'TRIG', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'UPC', NULL, 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'BLOOD TEST CLIPS', N'URIC', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Kitten 1kg', 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Puppy Starter 10kgs', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Aozi Dog Lamb & Apple Adult Pellette 1kg', 175)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Aozi Dog Beef Puppy Pellette 1kg', 175)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Aozi Dog Lamb & Apple Puppy Pellette 1kg', 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Chicken & Beef 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Chicken & Crab Stick 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Chicken & Lamb 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Chicken & Prawn 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Chicken & Salmon 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Tuna & Crab 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Tuna & Scallop 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Tuna & Shrimp 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Kitcat deboned', N'Kitcat Tuna and Chicken 80g', 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'MINI 1KG REPACK', 160)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Trio 2pt5-5kg', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Trio 5-10kg', NULL, 800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Trio 10-20kg', NULL, 900)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Trio 20-40kg', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'OTHERS', N'Coffin Box', N'Small', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'OTHERS', N'Coffin Box', N'Medium', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'OTHERS', N'Coffin Box', N'Large', 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Biomilk 100G', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Biomilk 500G', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Bosch', N'Sensitive 1kg', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'DOUBLEFISH&SHRIMP 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'KITTEN MOUSSE 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'SARDINE&CHKN 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&ANCHOVY 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&CHKN 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&CRAB 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&KATSUOBUSHI 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&MACKEREL 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&PRAWN 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&SALMON 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'KITCAT', N'TUNA&WHITE 400G', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Royal Canin', N'CARDIAC CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Royal Canin', N'HEPATIC CAN CANINE 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Royal Canin', N'GASTRO INTESTINAL CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Bosch', N'MILK 250G', 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'AOZI', N'DOG STARTER 1KG REPACK', 215)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'AOZI', N'CAT CHKN 400G', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'AOZI', N'CAT TUNA&CHKN 400G', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'ADULT 1KG REPACK', 140)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'PUPPY 1KG REPACK', 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'ADULT 20KG', 2400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Bosch', N'High Premium Adult', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'PUPPY 20KG', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'BOSCH', N'REPRODUCTION 7pt5KG', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'CHKN&TURKEY 1KG REPACK', 140)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'CHKN&TURKEY 7KG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'CHKN&TURKEY 1pt5KG', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica XSmall 2pt5-5KG', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Small 5pt1-10KG', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Medium 10pt1-20KG', NULL, 700)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Simparica Large 20pt1-40KG', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Frontline Spray 250ml', NULL, 1350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Immiticide 11-20kg', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Immiticide 5-10kg', NULL, 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Michlor', NULL, 775)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Immiticide <5kg', NULL, 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'ADVOCATE UP TO 4KG', NULL, 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'ADVOCATE 4-10KG', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'ADVOCATE 10-25KG', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'ADVOCATE 25-40KG', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'ADVOCATE CATS', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Allermyl', NULL, 775)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'BRAVECTO 4pt5-10KG', NULL, 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'BRAVECTO 10-20KG', NULL, 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'BRAVECTO 20-40KG', NULL, 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Sebolytic', NULL, 775)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Broadline 2pt5-7pt5kgs', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Broadline <2pt5kg', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'FRONTLINE UP TO 10kg', N'S', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'FRONTLINE 10-20kg', N'M', 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'FRONTLINE 20-40kg', N'L', 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'FRONTLINE CATS', N'CATS', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Frontline Spray 100ml', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'HEARTGARD UP TO 11KG', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'HEARTGARD 11-22KG', NULL, 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'HEARTGARD 22-45KG', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Ivermectin Tab', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Ketazole', NULL, 800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Mycocide', NULL, 480)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD 2-4KG', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD 4-10KG', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD 10-25KG', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD 25-50KG', NULL, 700)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD SPECTRA 2-3pt5KG', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD SPECTRA 3pt5-7pt5KG', NULL, 800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD SPECTRA 7pt5-15KG', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD SPECTRA 15-30KG', NULL, 900)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'NEXGARD SPECTRA 30-60KG', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'FLAVET 60ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'PROHEART SR', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Pyoderm', NULL, 725)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'Sebohex', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PARASITICIDES', N'VENOMA 120ML', NULL, 380)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Methozine', NULL, 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Otiderm', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Optazol', NULL, 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Opticare', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'ORIDERMYL 10G', NULL, 900)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Otidex', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'PAPI DOXY SYRUP 60ML', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Papi Doxy tab', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'PAPI SCOUR 60ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Prednisone 10mg', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'PULMOQUIN', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Remend Eye Gel', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Topiderm', NULL, 275)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'TOLFEDINE', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Tranexamic Acid', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Unicefa', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Unimeto', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Unimoxazole', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Unimoxicol', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'ANTIBIOTICS & OTHERS', N'Unipara', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'STARTER MOUSSE 195G', 180)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY SO CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'HYPOALLERGENIC CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RENAL CANINE CAN 410G', 260)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RENAL FELINE POUCH 85G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY POUCH FELINE 85G', 130)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'REHYDRATION POUCH 29G', 0)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RECOVERY LIQUID 0pt2L', 380)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'SKINCARE JR SMALLDOG 2KG', 1710)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY FELINE SO 1pt5KG', 1210)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'INSTINCTIVE 85G', 105)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'INTENSE BEAUTY 85G', 105)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'KITTEN INSTINCTIVE 85G', 105)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'CAT GASTRO INTESTINAL 2KG', 1480)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RENAL CANINE 2KG', 1430)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'FIT 32 2KG', 1050)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'CARDIAC CANINE 2KG', 1410)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY CARE 400G', 270)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY CARE 2KG', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'URINARY CANINE 2KG', 1410)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'HEPATIC CANINE 1pt5KG', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'BABYCAT MILK 300G', 1870)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'BABYDOG MILK 400G', 1760)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'HYPOALLERGENIC CANINE 2KG', 1680)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'GASTRO INTESTINAL 2KG', 1380)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'SKIN SUPPORT DOG 2KG', 1750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'RENAL FELINE 2KG', 1590)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'SATIETY DOG 1pt5KG', 1300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'MINI STARTER M&B 1KG', 530)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'HAIR & SKIN 2KG', 1110)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'MINI INDOOR PUPPY 1pt5KG', 830)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'MINI INDOOR Adult 1pt5KG', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'SHIHTZU PUPPY 1pt5KG', 900)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'SHIHTZU ADULT 1pt5KG', 860)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'DERMACOMFORT MINI 3KG', 1490)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Singen Recovery for Dogs', NULL, 375)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Dr Clauders Hypersensitive 35kg', NULL, 2300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'FIT32 10KG', 3810)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'PERSIAN 10KG', 4640)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'GOLDEN RETRIEVER PUPPY 3KG', 1360)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'KITTEN36 2KG', 1160)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'KITTEN PERSIAN 2KG', 1340)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'CHIHUAHUA 1pt5KG', 880)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'CHIHUAHUA JR 1pt5KG', 910)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'FRENCH BULLDOG 3KG', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'FRENCH BULLDOG JR 3KG', 1530)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'LABRADOR 3KG', 1220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'LABRADOR JR 3KG', 1330)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'POODLE 1pt5KG', 865)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'POODLE JR 500G', 370)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'PUG 1pt5KG', 865)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'PUG PUPPY 500G', 370)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'PERSIAN 2KG', 1220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'BABYCAT 400G', 330)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'INDOOR27 2KG', 1050)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'INDOOR27 10KG', 3930)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'KITTEN36 10KG', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VET DIET', N'Royal Canin', N'HAIRBALL CARE 2KG', 1080)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Calciubest', NULL, 10)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Gembifer', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Biover', NULL, 280)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'DOCTOR POOCH', N'VCO', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Coatshine 120ml', NULL, 320)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nutrich tab', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Vitabest', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Pet Pro', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Puppy Boost', N'box', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'SAMe 100mg', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Brewers Yeast', NULL, 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Pet-Ease', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Anti-Coprophagic', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Eye Rinse Liquid', NULL, 720)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liverator', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Arthropet', NULL, 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Dextrolyte Pack', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Iron Oral Solution 100ml', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Cardi Excel', N'tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nutrical 60ml', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Kawu', N'bottle', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Kawu', N'tab', 10)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Love Drops', NULL, 12)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liv52 Forte', N'Bottle', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liv52 Forte', N'Tab', 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Dextrolyte', N'5g', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nutrical 120ml', NULL, 220)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Emervit', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'BIO DART HAN PROBIOTIC', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Appeboost', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Black Armour', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Bio-Genta Drop', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Growth Advance', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Brewer’s Yeast', N'Tablet', 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Brewer’s Yeast', N'Bottle', 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Coatex', NULL, 35)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Colostrovet', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'COMPLIVIT 50G', N'per pump', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Cystaid', NULL, 40)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'METHIOVET', NULL, 40)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'ENER-G 60ML', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Emerplex', NULL, 380)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Prefolic Cee', NULL, 320)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'ENMALAC 120ML', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Freximol', N'CAPSULE', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Freximol', N'BOTTLE', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Immunol', N'SYRUP', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Immunol', N'TABLET', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Immunol', N'TABLET BOTTLE', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liv52', N'TABLET', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liv52', N'TABLET BOTTLE', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Liv52', N'DROPS', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'LIVEROLIN', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'LivTone', NULL, 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'LIVWELL 60ML', NULL, 275)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Mondex Big', NULL, 170)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Mondex Small', NULL, 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Macrocal', NULL, 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Inflacam 32ml', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nefrotec', N'tab', 20)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nefrotec', N'bottle', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nefrotec DS', N'tab', 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nefrotec DS', N'bottle', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Neurovet', N'tab', 25)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Neurovet', N'Bottle', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'NACALVIT C 120ML', NULL, 275)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Nutriplus Gel', NULL, 800)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Papi Calcium', N'TABLET', 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Papi Calcium', N'BOTTLE', 600)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PAPI IRON 120ML', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PAPI MVP', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PAPI MULTI TAB', NULL, 15)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PAPI OB 120ML', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PET DEFENSE', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Petsure', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PROMAX S', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PROMAX M', NULL, 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'PROMAX L', NULL, 1300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Puppy Boost', N'per pump', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'RENACURE 60ML', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Synoquin L', NULL, 80)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Synoquin M', NULL, 70)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Synoquin S', NULL, 65)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'UNIVIT C', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'Urinaid', NULL, 60)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'VET OIL', N'CAPSULE', 5)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'VITAMINS & MINERALS SUPPLEMENTS', N'VET OIL', N'BOTTLE', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Pedigree Rodeo', NULL, 85)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET SUPPLIES', N'Pedigree Tasty Bites', NULL, 120)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL DOG', N'ADULT 400G', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL DOG', N'PUPPY 1pt5KG', 230)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'URINARY 7KG', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'URINARY 1KG REPACK', 195)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'SPECIAL CAT', N'MOUSSE BEEF&LIV', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Dog - Salmon (Can)', 80)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Dog - Chicken (Can)', 80)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Dog - Beef (Can)', 80)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Adult 1kg', 165)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Puppy 1kg', 175)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Cat - Beef (Can)', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Cat - Tuna & Beef (Can)', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Cat 1kg', 170)
GO
INSERT [dbo].[PetLink_Caybiga_Items] ([Category], [Item], [Option], [Price]) VALUES (N'PET FOOD', N'Aozi', N'Lamb & Apple Puppy 1kg', 180)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 1-3 kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 3-7kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 7-15kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 15-20kg with CBC + 3P', 14750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 20-30kg with CBC + 3P', 15250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 30&Above with CBC + 3P', 16250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 3-7 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 7-15 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 15-20 kg with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 20-30 kg with Meds', 13500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'Under Gas Anes 30kg&Above with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'3-7kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'20-30kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'30&Above with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'3-7kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'7-15kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'15-20kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'20-30kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'EAR CANAL ABLATION', N'30&Above with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 1-3 kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 3-7kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 7-15kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 15-20kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 20-30kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 1-3 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 3-7 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 7-15 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 15-20 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 20-30 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'1-3 kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'3-7kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'7-15kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'15-20kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'20-30kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'1-3 kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'3-7kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'7-15kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'15-20kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'20-30kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'MAMMECTOMY', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 1-3 kg with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 3-7kg with CBC + 3P', 16250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 7-15kg with CBC + 3P', 19250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 15-20kg with CBC + 3P', 22250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 20-30kg with CBC + 3P', 23250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 30&Above with CBC + 3P', 24250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 1-3 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 3-7 kg with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 7-15 kg with Meds', 17500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 15-20 kg with Meds', 20500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 20-30 kg with Meds', 21500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'Under Gas Anes 30kg&Above with Meds', 22500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'1-3 kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'3-7kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'7-15kg with CBC + 3P', 16750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'15-20kg with CBC + 3P', 19750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'20-30kg with CBC + 3P', 20750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'30&Above with CBC + 3P', 21750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'3-7kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'7-15kg with Meds', 15000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'15-20kg with Meds', 18000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'20-30kg with Meds', 19000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'BONE PINNING', N'30&Above with Meds', 20000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'PROMO', N'Papi Doxy Promo', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'PROMO', N'Low Cost Neuter', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'PROMO', N'Low Cost Spay', NULL, 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Recatheter', NULL, 1750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Laser Therapy', N'3 sessions', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SERVICES', N'Pet Pick Up', N'per KM', 150)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Scrotal Ablation', NULL, 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Tail Dock (Puppy)', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'1-3kgs', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'3-7kgs', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'7-15kgs', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'15-20kgs', 1250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'20-30kgs', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Pyometra', NULL, 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gas Anesthesia', N'After first 2 hrs', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Enema', N'30kgs & Above', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cryptorchidism', NULL, 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'1-3kgs', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'3-7kgs', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'7-15kgs', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'15-20kgs', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'20-30kgs', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Blood Transfusion', N'30kgs & Above', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 15-20kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 20-30kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 30&Above with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 15-20 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 20-30 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'Under Gas Anes 30kg&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'15-20kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'15-20kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'20-30kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair (Multiple Severe)', N'30&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 15-20kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 3-7 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 7-15 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 15-20 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 20-30 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'Under Gas Anes 30kg&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'1-3 kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'3-7kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'7-15kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'15-20kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'20-30kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'30&Above with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'1-3 kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'3-7kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'7-15kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'15-20kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'20-30kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Repair', N'30&Above with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 1-3 kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 3-7kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 7-15kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 15-20kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 20-30kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 30&Above with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 1-3 kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 3-7 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 7-15 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 15-20 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 20-30 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'Under Gas Anes 30kg&Above with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'1-3 kg with CBC + 3P', 3750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'3-7kg with CBC + 3P', 4250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'7-15kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'15-20kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'20-30kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'30&Above with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'1-3 kg with Meds', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'3-7kg with Meds', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'7-15kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'15-20kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'20-30kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Wound Closure', N'30&Above with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 3-7kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 7-15kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 15-20kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 30&Above with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 1-3 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 3-7 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 7-15 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 15-20 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'Under Gas Anes 30kg&Above with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'1-3 kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'3-7kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'7-15kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'15-20kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'30&Above with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'1-3 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'3-7kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'7-15kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'15-20kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Urethrostomy', N'30&Above with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 15-20kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 20-30kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 30&Above with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 15-20 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 20-30 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'Under Gas Anes 30kg&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'15-20kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'15-20kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'20-30kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Umbilical Hernia Repair', N'30&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 1-3 kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 3-7kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 7-15kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 15-20kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 30&Above with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 1-3 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 3-7 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 7-15 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 15-20 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'Under Gas Anes 30kg&Above with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'1-3 kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'3-7kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'7-15kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'15-20kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'30&Above with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'1-3 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'3-7kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'7-15kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'15-20kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tumor Removal', N'30&Above with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 30&Above with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 3-7 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 7-15 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 15-20 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 20-30 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'Under Gas Anes 30kg&Above with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'1-3 kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'3-7kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'7-15kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'15-20kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'20-30kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'30&Above with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'1-3 kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'3-7kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'7-15kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'15-20kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'20-30kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tarsorrhapy', N'30&Above with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 15-20kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 20-30kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 30&Above with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 15-20 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 20-30 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'Under Gas Anes 30kg&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'15-20kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'15-20kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'20-30kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Tail DockRepair', N'30&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 30&Above with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 3-7 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 7-15 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 15-20 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 20-30 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'Under Gas Anes 30kg&Above with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'1-3 kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'3-7kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'7-15kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'15-20kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'20-30kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'30&Above with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'1-3 kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'3-7kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'7-15kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'15-20kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'20-30kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Rectal Prolpase', N'30&Above with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 1-3 kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 3-7kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 7-15kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 15-20kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 20-30kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 3-7 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 7-15 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 15-20 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 20-30 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'3-7kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'7-15kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'15-20kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'20-30kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'3-7kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'7-15kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'15-20kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'20-30kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Inguinal Hernia Repair', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 15-20kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 20-30kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 30&Above with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 15-20 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 20-30 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'Under Gas Anes 30kg&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'15-20kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'15-20kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'20-30kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Glossectomy', N'30&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'FBO Removal', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 1-3 kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 3-7kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 7-15kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 15-20kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 20-30kg with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 30&Above with CBC + 3P', 14750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 1-3 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 3-7 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 7-15 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 15-20 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 20-30 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'Under Gas Anes 30kg&Above with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'15-20kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'20-30kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'30&Above with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'3-7kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'7-15kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'15-20kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'20-30kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enucleation', N'30&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Enterotomy', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 1-3 kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 3-7kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 15-20kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 20-30kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 30&Above with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 1-3 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 3-7 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 15-20 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 20-30 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'Under Gas Anes 30kg&Above with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'1-3 kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'3-7kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'20-30kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'30&Above with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'1-3 kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'3-7kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'15-20kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'20-30kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog OVH', N'30&Above with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 30&Above with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 3-7 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 7-15 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 15-20 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 20-30 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'Under Gas Anes 30kg&Above with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'1-3 kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'3-7kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'7-15kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'15-20kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'20-30kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'30&Above with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'1-3 kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'3-7kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'7-15kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'15-20kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'20-30kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dog Castration', N'30&Above with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 3-7kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 7-15kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 15-20kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 20-30kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 30&Above with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 3-7 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 7-15 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 15-20 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 20-30 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'Under Gas Anes 30kg&Above with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'1-3 kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'3-7kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'7-15kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'15-20kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'20-30kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'30&Above with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'1-3 kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'3-7kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'7-15kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'15-20kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'20-30kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Dental Prophylaxis', N'30&Above with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cystotomy', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 1-3 kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 3-7kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 15-20kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 20-30kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 30&Above with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 1-3 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 3-7 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 15-20 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 20-30 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'Under Gas Anes 30kg&Above with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'30&Above with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'3-7kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'15-20kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'20-30kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair (both eyes)', N'30&Above with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 30&Above with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 3-7 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 7-15 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 15-20 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 20-30 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'Under Gas Anes 30kg&Above with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'1-3 kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'3-7kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'7-15kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'15-20kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'20-30kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'30&Above with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'1-3 kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'3-7kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'7-15kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'15-20kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'20-30kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cherry Eye Repair', N'30&Above with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 1-3 kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 3-7kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 7-15kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 1-3 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 3-7 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'Under Gas Anes 7-15 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'1-3 kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'3-7kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'1-3 kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'3-7kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat OVH', N'7-15kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 1-3 kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 7-15kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 1-3 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 3-7 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'Under Gas Anes 7-15 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'1-3 kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'3-7kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'7-15kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'1-3 kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'3-7kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Cat Castration', N'7-15kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 1-3 kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 3-7kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 7-15kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 15-20kg with CBC + 3P', 14750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 20-30kg with CBC + 3P', 15750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 30&Above with CBC + 3P', 16750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 3-7 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 7-15 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 15-20 kg with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 20-30 kg with Meds', 14000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'Under Gas Anes 30kg&Above with Meds', 15000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'3-7kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'7-15kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'15-20kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'20-30kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'C-section', N'30&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 1-3 kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 3-7kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 15-20kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 20-30kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 30&Above with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 1-3 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 3-7 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 15-20 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 20-30 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'Under Gas Anes 30kg&Above with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'1-3 kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'3-7kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'15-20kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'20-30kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'30&Above with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'1-3 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'3-7kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'15-20kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'20-30kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Blepharoplasty (per eye)', N'30&Above with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 1-3 kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 3-7kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 7-15kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 20-30kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 30&Above with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 1-3 kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 3-7 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 7-15 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 20-30 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'Under Gas Anes 30kg&Above with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'1-3 kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'3-7kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'7-15kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'20-30kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'30&Above with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'1-3 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'3-7kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'7-15kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'20-30kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma (both ears)', N'30&Above with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 1-3 kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 3-7kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 7-15kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 20-30kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 30&Above with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 1-3 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 3-7 kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 7-15 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 15-20 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 20-30 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'Under Gas Anes 30kg&Above with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'1-3 kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'3-7kg with CBC + 3P', 6250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'7-15kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'15-20kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'20-30kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'30&Above with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'1-3 kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'3-7kg with Meds', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'7-15kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'15-20kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'20-30kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Aural Hematoma', N'30&Above with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 1-3 kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 3-7kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 7-15kg with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 15-20kg with CBC + 3P', 15250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 20-30kg with CBC + 3P', 16250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 30&Above with CBC + 3P', 17250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 1-3 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 3-7 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 7-15 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 15-20 kg with Meds', 13500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 20-30 kg with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'Under Gas Anes 30kg&Above with Meds', 15500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'3-7kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'7-15kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'15-20kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'20-30kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'30&Above with CBC + 3P', 14750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'3-7kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'7-15kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'15-20kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'20-30kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Anastamosis', N'30&Above with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 1-3 kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 3-7kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 7-15kg with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 15-20kg with CBC + 3P', 15250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 20-30kg with CBC + 3P', 16250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 30&Above with CBC + 3P', 17250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 1-3 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 3-7 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 7-15 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 15-20 kg with Meds', 13500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 20-30 kg with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'Under Gas Anes 30kg&Above with Meds', 15500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'1-3 kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'3-7kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'7-15kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'15-20kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'20-30kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'30&Above with CBC + 3P', 14750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'3-7kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'7-15kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'15-20kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'20-30kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Amputation', N'30&Above with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 3-7 kg with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 7-15 kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 15-20 kg with Meds', 6500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 20-30 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 30kg&Above with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 3-7kg with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 7-15kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 15-20kg with CBC + 3P', 8250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 20-30kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'Under Gas Anes 30&Above with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'1-3 kg with CBC + 3P', 4250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'3-7kg with CBC + 3P', 4750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'7-15kg with CBC + 3P', 5250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'15-20kg with CBC + 3P', 5750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'20-30kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'30&Above with CBC + 3P', 7250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'1-3 kg with Meds', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'3-7kg with Meds', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'7-15kg with Meds', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'15-20kg with Meds', 4000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'20-30kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Abscess Draining', N'30&Above with Meds', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 1-3 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 3-7 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 7-15 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 15-20 kg with Meds', 13500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 20-30 kg with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 30kg&Above with Meds', 15500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 1-3 kg with CBC + 3P', 12200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 3-7kg with CBC + 3P', 13200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 7-15kg with CBC + 3P', 14200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 15-20kg with CBC + 3P', 15200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 20-30kg with CBC + 3P', 16200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'Under Gas Anes 30&Above with CBC + 3P', 17200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'1-3 kg with CBC + 3P', 9700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'3-7kg with CBC + 3P', 10700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'7-15kg with CBC + 3P', 11700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'15-20kg with CBC + 3P', 12700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'20-30kg with CBC + 3P', 13700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'30&Above with CBC + 3P', 14700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'3-7kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'7-15kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'15-20kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'20-30kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Complete)', N'30&Above with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 1-3 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 3-7 kg with Meds', 13500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 7-15 kg with Meds', 14500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 15-20 kg with Meds', 15500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 20-30 kg with Meds', 16500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 30kg&Above with Meds', 17500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 1-3 kg with CBC + 3P', 14200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 3-7kg with CBC + 3P', 15200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 7-15kg with CBC + 3P', 16200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 15-20kg with CBC + 3P', 17200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 20-30kg with CBC + 3P', 18200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'Under Gas Anes 30&Above with CBC + 3P', 19200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'1-3 kg with CBC + 3P', 11700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'3-7kg with CBC + 3P', 12700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'7-15kg with CBC + 3P', 13700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'15-20kg with CBC + 3P', 14700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'20-30kg with CBC + 3P', 15700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'30&Above with CBC + 3P', 16700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'3-7kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'7-15kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'15-20kg with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'20-30kg with Meds', 14000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Splenectomy (Partial)', N'30&Above with Meds', 15000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 1-3 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 3-7 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 7-15 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 15-20 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 20-30 kg with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 30kg&Above with Meds', 13000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 12200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 3-7kg with CBC + 3P', 12700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 7-15kg with CBC + 3P', 13200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 15-20kg with CBC + 3P', 13700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 20-30kg with CBC + 3P', 14200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'Under Gas Anes 30&Above with CBC + 3P', 14700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'1-3 kg with CBC + 3P', 9700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'3-7kg with CBC + 3P', 10200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'7-15kg with CBC + 3P', 10700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'15-20kg with CBC + 3P', 11200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'20-30kg with CBC + 3P', 11700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'30&Above with CBC + 3P', 12200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'1-3 kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'3-7kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'7-15kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'15-20kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'20-30kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Mandibulectomy', N'30&Above with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 3-7 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 7-15 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 15-20 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 20-30 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 1-3 kg with CBC + 3P', 11700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 3-7kg with CBC + 3P', 12200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 7-15kg with CBC + 3P', 12700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 15-20kg with CBC + 3P', 13200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 20-30kg with CBC + 3P', 13700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'Under Gas Anes 30&Above with CBC + 3P', 14200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'1-3 kg with CBC + 3P', 9200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'3-7kg with CBC + 3P', 9700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'7-15kg with CBC + 3P', 10200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'15-20kg with CBC + 3P', 10700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'20-30kg with CBC + 3P', 11200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'30&Above with CBC + 3P', 11700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'3-7kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'7-15kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'15-20kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'20-30kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'GDV', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'1-3kgs', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'3-7kgs', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'7-15kgs', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'15-20kgs', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'20-30kgs', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Necropsy Report', N'30kg & Above', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Exploratory Laparotomy', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 1-3 kg with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 3-7 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 7-15 kg with Meds', 11000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 15-20 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 20-30 kg with Meds', 12000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 1-3 kg with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 3-7kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 7-15kg with CBC + 3P', 12750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 15-20kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 20-30kg with CBC + 3P', 13750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 3-7 kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 7-15 kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 15-20 kg with Meds', 10500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 20-30 kg with Meds', 11500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 30kg&Above with Meds', 12500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 3-7kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 7-15kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 15-20kg with CBC + 3P', 12250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 20-30kg with CBC + 3P', 13250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'Under Gas Anes 30&Above with CBC + 3P', 14250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'1-3 kg with CBC + 3P', 9250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'3-7kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'7-15kg with CBC + 3P', 10250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'15-20kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'20-30kg with CBC + 3P', 11250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'1-3 kg with Meds', 7500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'3-7kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'7-15kg with Meds', 8500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'15-20kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'20-30kg with Meds', 9500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Perineal Hernia Repair', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'1-3 kg with CBC + 3P', 6750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'3-7kg with CBC + 3P', 7750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'7-15kg with CBC + 3P', 8750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'15-20kg with CBC + 3P', 9750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'20-30kg with CBC + 3P', 10750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'30&Above with CBC + 3P', 11750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'30&Above with Meds', 10000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'1-3 kg with Meds', 5000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'3-7kg with Meds', 6000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'7-15kg with Meds', 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'15-20kg with Meds', 8000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Gastrotomy', N'20-30kg with Meds', 9000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Flourescein Dye', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SURGERY', N'Prolapse Urethral', NULL, 7000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Catheterization', NULL, 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Full Assessment', NULL, 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Incubator', NULL, 650)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Infusion Pump', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Patient Monitoring', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Nebulizer', N'12hrs', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Casting', N'Medium', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Casting', N'Large', 5500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Casting', N'Small', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'SERVICES', N'Pet Services', N'Additional for Distemper Suspect Patient', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Deticking', NULL, 400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Nursing Care Package', N'Confinement - Feeding - Incubator', 650)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Critical care Package', N'Px monitoring - Nebulizer - Oxygen', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package D (Wo Distemper Tk)', N'Large (4&more Meds)', 1750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package D (Wo Distemper Tk)', N'Medium (4&more Meds)', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package D (Wo Distemper Tk)', N'Small (4&more Meds)', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package C (With Distemper Tk)', N'Large (4&more Meds)', 2550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package C (With Distemper Tk)', N'Medium (4&more Meds)', 2200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package C (With Distemper Tk)', N'Small (4&more Meds)', 1900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package B (Wo Distemper Tk)', N'Large (1-3Meds)', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package B (Wo Distemper Tk)', N'Medium (1-3Meds)', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package B (Wo Distemper Tk)', N'Small (1-3Meds)', 900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package A (With Distemper Tk)', N'Small (1-3Meds)', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package A (With Distemper Tk)', N'Medium (1-3Meds)', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'CONFINEMENT', N'Package A (With Distemper Tk)', N'Large (1-3Meds)', 2250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'DOWN PAYMENT', N'DOWN (CASH RECEIVED)', NULL, 1)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'DOWN PAYMENT', N'DOWN (REDUCTION IN TOTAL)', NULL, -1)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Sanitary Puppy', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Calciovet', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Calciovet', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Calciovet', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Bio Amino Fort', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Bio Amino Fort', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Bio Amino Fort', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Flavicel', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Flavicel', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Flavicel', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Vetmectin', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Vetmectin', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Vetmectin', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Nova Amox LA', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Nova Amox LA', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Nova Amox LA', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cytopoint', N'40mg', 4200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phytobas', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phytobas', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phytobas', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'23-25 kgs', 3900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'25-27 kgs', 4300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'10-13 kgs', 1900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'13-16 kgs', 2300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'16-18kgs', 2700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'18-20kgs', 3100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'20-23 kgs', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cerenia', N'25kgs & above', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cerenia', N'Large (15-25kgs)', 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cerenia', N'Medium (7-15kgs)', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Omeprazole', NULL, 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxycycline', N'Small up to 7kgs', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cytopoint', N'10mg', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tribivet', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tribivet', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tribivet', N'Small (up to 7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxorubicin up to 10kg', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxorubicin 10-20', NULL, 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'3-5 kgs', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'5-7 kgs', 1050)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Convenia', N'7-10 kgs', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Imidocarb', N'1 - 10 kgs', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Imidocarb', N'11 - 20 kg', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Imidocarb', N'21 up kgs', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'CANGLOB', N'PARVO 26-30kg', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'CANGLOB', N'DISTEMPER 26-30kg', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'CANGLOB', N'PARVO 21-25kg', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'CANGLOB', N'DISTEMPER 21-25kg', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cytopoint', N'20mg', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Cytopoint', N'30mg', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Fercobsang', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Fercobsang', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ferosemide', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ferosemide', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Diphenhydramine', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Diphenhydramine', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxycycline', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxycycline', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Energidex', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Energidex', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Enrofloxacin', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Enrofloxacin', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Bicogen', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Bicogen', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Biodyl', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Biodyl', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'D50', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'D50', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Dexamethasone', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Dexamethasone', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Acepromazine', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Acepromazine', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Alfalfer', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Alfalfer', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Amilyte', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Amilyte', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ampicilin', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ampicilin', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'B-Complex', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'B-Complex', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tolfine', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tolfine', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tramadol', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tramadol', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tranexamic Acid', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tranexamic Acid', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phylumox', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phylumox', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ranitidine', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ranitidine', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Septotryl', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Septotryl', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Spasmovet', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Spasmovet', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Marbocyl', N'Medium (7-15kgs)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Marbocyl', N'Large (15&above)', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Prednisolone', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Prednisolone', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metoclopramide', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metoclopramide', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metronidazole', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metronidazole', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ornipural', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ornipural', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytetracycline', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytetracycline', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytocin', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytocin', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Neurobe', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Neurobe', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Epinephrine', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Epinephrine', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Furosemide', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Furosemide', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Iron', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Iron', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ivermectin', N'Large (15&above)', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ivermectin', N'Medium (7-15kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Neurobe', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Epinephrine', N'Small (1-7kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Furosemide', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Iron', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ivermectin', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Marbocyl', N'20mg tab', 90)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Prednisolone', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Marbofloxacin', N'Small (1-7kgs)', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metoclopramide', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Metronidazole', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ornipural', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytetracycline', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Oxytocin', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Phylumox', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Prednisone', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Ranitidine', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Septotryl', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Spasmovet', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tolfine', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tramadol', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Tranexamic Acid', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Vincristine up to 10kg', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Vincristine 10-20kg', NULL, 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Bronchicine', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Biocan M', NULL, 480)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'PARVO <5 kg', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'PARVO 5-10 kg', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'PARVO 11-20 kg', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'DISTEMPER <5 kg', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'DISTEMPER 5-10 kg', 1100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Canglob', N'DISTEMPER 11-20 kg', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'DEWORMING', N'1-10 kg', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'DEWORMING', N'11 kg onwards', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Purevax', NULL, 700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Felocell', NULL, 700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'VANGUARD 8IN1', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'ZOLETIL', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Tolfenol', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'ALT', NULL, 400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'BUN', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Abdominocentesis', N'Small', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Abdominocentesis', N'Medium', 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Abdominocentesis', N'Large', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Antibody Check', N'Canine Distemper', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Antibody Check', N'Canine Parvovirus', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CHECK-UP CONSULTATION', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'1-3kgs', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'3-7kgs', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'7-15kgs', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'ECG', N'Surgical Use', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'ECG', N'Consult', 900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Ear Mite Test', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Emergency Fee', NULL, 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Emesis', NULL, 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Euthanasia', N'1-5 kg', 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Euthanasia', N'6-10 kg', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Euthanasia', N'10 kg onwards', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Fecalysis', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Gram Stain', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'IV Fluids', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'IV Line Replacement', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Microchip', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Oxygen', N'12 hrs', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Passport + Microchip', NULL, 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Pet Passport', NULL, 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Professional Fee', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Skin Scrape', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Smear Test', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath + Full Groom', N'Small', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath + Full Groom', N'Medium', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath + Full Groom', N'Large', 1250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'With Styles', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Toothbrush', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Dematting', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Organic Full Grooming', N'XLarge Breed', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Organic Full Grooming', N'XxLarge Breed', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Anal Sac Draining', N'All Breeds', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Bath Only', N'Small Breed', 150)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Bath Only', N'Medium Breed', 200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Bath Only', N'Large Breed', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Cat Grooming', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Cut Only', N'Small Breed', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Cut Only', N'Medium Breed', 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Cut Only', N'Large Breed', 400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Ear Cleaning only', NULL, 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Nail Cut', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Organic Full Grooming', N'Small Breed', 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath', N'Small Breed', 600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath', N'Medium Breed', 700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Medicated Bath', N'Large Breed', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Organic Full Grooming', N'Medium Breed', 550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Organic Full Grooming', N'Large Breed', 700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'GROOMING', N'Sanitary Shave', NULL, 150)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'D50', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Dexamethasone', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Diphenhydramine', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Dorminal', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Doxtcycline HCl', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Energidex', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Enrofloxacin', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Epoetin', NULL, 450)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'INJECTABLES', N'Fercobsang', N'Small (1-7kgs)', 250)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Rabisin', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'NOVIBAC 8IN1', NULL, 600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'15-20kgs', 600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'20-30kgs', 850)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'CONFINEMENT PER DAY', N'30kgs&Above', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Recombitek 6in1', NULL, 550)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 6-10kg', 2600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 11-15kg', 2900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 16-20kg', 3200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 21-25kg', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 26-30kg', 3800)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VACCINES & DEWORMING', N'Heartworm Prevention', N'Proheart 31-35kg', 4100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'PETX FPV-FCOV-GIA', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'PETX CDV AG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'ANIGEN FPV AG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'ANIGEN RABIES AG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'ANIGEN CHW AG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CTNL', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail FPV Ag', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CRP', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail T4', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail FPL', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CPL', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail FTSH', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'PETX CDV-CAV-CIV', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'PETX CPV-CCV-GIARDIA-CRV', 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV+CDV AB', 1300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV-CDV AB + CPV-CDV AG', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'A Pet Care CDV-CPV', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'A Pet Care CPV-CCV-EHR-ANA', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'A Pet Care CPV-CCV-CDV-EHR', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'A Pet Care EHR-ANA-BAB', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Lepto Testkit', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Babesia', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'DIANOTECH PROGESTERONE', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'A Pet Care CPV-CDV-EHR', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CPC', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail P4', 900)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail RLN', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CPV Ag', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Dawnsail CDV Ag', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CDV Ag', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'SNAP 4DX', 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Snap Lepto', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Snap Combo', 2200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Snap Parvo', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV-CCV-GIARDIA', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CDV-CAV-CIV', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Ehrli-Ana-Bab', 1400)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Ehrlichia-Anaplasma', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'FeLV-FPV', 2000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'HEARTWORM Ag', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'FPV', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Pregnancy Test Kit', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV AG', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'EhrAnaBabChw', 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Rabies testkit', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Feline Corona Virus', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV-CDV', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV-CCV', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Snap fPL', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Snap cPL', 1500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'FIV-FELV', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Ehrlichia Ab', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'DianoTech CPL', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'DianoTech FPL', 1200)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'CPV-CCV-GIARDIA-CRV', 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'FPV 4way', 1600)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Giardia', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'FCV-GIA-FCOV-FPV', 1700)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Test Kits', N'Progesterone Test', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Xray print out', NULL, 50)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Tooth Extraction', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Ultrasound', NULL, 950)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Urinalysis', N'Complete', 1000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Urinalysis', N'Urine Strip', 750)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Urinalysis', N'Microscopy', 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'VIP Card', NULL, 850)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Veterinary Health Certificate', NULL, 500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wellness Check-up', N'Full Assessment', 3000)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wellness Check-up', N'Wellness 1', 2300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wellness Check-up', N'Wellness 2', 1800)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wellness Check-up', N'Urinary Function Test', 4500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Whelping', N'Per Puppy', 100)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Whelping', N'Whelping', 3500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Woods Lamp', NULL, 300)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wound Cleaning', NULL, 350)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'Wound Closure', NULL, 2500)
GO
INSERT [dbo].[PetLink_Caybiga_Services] ([CATEGORY], [Name2], [SERVICES], [SERVICE FEE]) VALUES (N'VET SERVICES', N'X-ray', NULL, 950)
GO
