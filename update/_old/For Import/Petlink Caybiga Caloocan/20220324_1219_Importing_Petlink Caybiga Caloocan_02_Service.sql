DEclare @GUID_Company VARCHAR(MAX) = 'EDDF9513-0735-45FB-8F43-8F3C67B1A4DF'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item           INT,
     Name_Item         Varchar(MAX),
     UnitPrice         float,
     ID_ItemCategory   INT,
     Name_ItemCategory Varchar(MAX)
  )
DECLARE @IsActive INT=1
DECLARE @Service_ID_ItemType INT = 1
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1
 

INSERT @forImport
       (Name_Item,
        Name_ItemCategory,
        UnitPrice)
SELECT dbo.fGetCleanedString([Name2])
       + CASE
           WHEN dbo.fGetCleanedString(ISNULL([SERVICES], '')) = '' THEN ''
           ELSE ' - ' + dbo.fGetCleanedString(ISNULL([SERVICES], ''))
         END,
       CATEGORY,
       [SERVICE FEE]
FROM   dbo.[PetLink_Caybiga_Services] import


UPdate @ForImport
set    Name_ItemCategory = 'Service'
WHERE  Name_ItemCategory = 'SERVICES'

UPdate @ForImport
set    Name_ItemCategory = 'Surgery'
WHERE  Name_ItemCategory = 'SURGERY'


UPdate @ForImport
set    Name_ItemCategory = 'Grooming Services'
WHERE  Name_ItemCategory = 'GROOMING'

UPdate @ForImport
set    Name_ItemCategory = 'Confinement (Package)'
WHERE  Name_ItemCategory = 'CONFINEMENT'

UPdate @ForImport
set    Name_ItemCategory = 'Injectable Medications'
WHERE  Name_ItemCategory = 'INJECTABLES'

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_ItemCategory = tbl.ID
FROM   @ForImport import
       inner join (SELECT MAX(ID) ID,
                          Name
                   FROM   tItemCategory
                   WHERE  ID_ItemType = @Service_ID_ItemType
                          AND IsActive = 1
                   GROUP  BY Name) tbl
               on import.Name_ItemCategory = tbl.Name

SELECT DISTINCT Name_ItemCategory
FROm   @forImport
WHERE  ID_ItemCategory IS NULL

SELECT *
FROM   @ForImport Order by Name_Item 

INSERT INTO [dbo].[tItem]
            ([Name],
             ID_ItemCategory,
             [ID_ItemType],
             [UnitPrice],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                import.ID_ItemCategory,
                @Service_ID_ItemType,
                import.UnitPrice,
                @IsActive,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy
FROM   @forImport import
WHERE  ID_Item IS NULL


SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL



SELECT Name_Item,
       COUNT(*) Count
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1