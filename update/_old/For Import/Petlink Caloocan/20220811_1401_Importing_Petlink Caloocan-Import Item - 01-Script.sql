DECLARE @GUID_Company VARCHAR(MAX) = '77F3B4CF-6521-4CCF-AA71-2A1FC83EFF81'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([ITEM NAME]),
       dbo.fGetCleanedString([SELLING PRICE]),
       dbo.fGetCleanedString([CATEGORY]),
       [GIUD]
FROM  ForImport.[dbo].[Petlink-Caloocan-ITEMS_20220811_1150]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    Import_Category = 'ANTIBIOTICS & OTHERS'
WHERE  Import_Category = 'ANTIBIOTIC & OTHERS'

Update @forImport
SET    Import_Category = 'VITAMINS & MINERALS SUPPLEMENTS'
WHERE  Import_Category = 'VITAMINS & MINERALS SUPPLEMENT'

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType
       and category.IsActive = 1

SELECT DISTINCT Import_Category
FROM   @forImport
WHERE  ID_ItemCategory IS NULL

--Kitcat can Double Fish & Shrimp 400g	90.00
DELETE FROM @forImport
WHERE  GUID = '4EBD4AB2-299E-408E-990D-280B2DB5E0AF-[Petlink-Caloocan-ITEMS_20220811_1150]'

--Oridermyl
DELETE FROM @forImport
WHERE  GUID = '01C5D636-7241-48D9-AF93-A2C64F3335BD-[Petlink-Caloocan-ITEMS_20220811_1150]'

--TopBreed Adult 1kg - PET FOOD
DELETE FROM @forImport
WHERE  GUID = '710293EA-676F-40AC-B598-5971EC40C864-[Petlink-Caloocan-ITEMS_20220811_1150]'

--TTopBreed Puppy 1kg - PET FOOD
DELETE FROM @forImport
WHERE  GUID = 'D3053CA2-CFF7-4F4E-94B6-815ADF378DBB-[Petlink-Caloocan-ITEMS_20220811_1150]'

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 
