DECLARE @GUID_Company VARCHAR(MAX) = '77F3B4CF-6521-4CCF-AA71-2A1FC83EFF81'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Service_ID_ItemType INT =1
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([SERVICE NAME])
       + CASE
           WHEN LEN(dbo.fGetCleanedString(SER)) > 0 THEN ' ' + dbo.fGetCleanedString(SER)
           ELSE ''
         END,
       dbo.fGetCleanedString([SERVICE FEE]),
       dbo.fGetCleanedString([CATEGORY]),
       [GIUD]
FROM   ForImport.[dbo].[Petlink-Caloocan-Services_20220811_1150]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    Import_Category = 'Confinement (Package)'
WHERE  Import_Category = 'CONFINEMENT'

Update @forImport
SET    Import_Category = 'Grooming Services'
WHERE  Import_Category = 'GROOMING'

Update @forImport
SET    Import_Category = 'Injectable Medications'
WHERE  Import_Category = 'INJECTABLES'

Update @forImport
SET    Import_Category = 'Service'
WHERE  Import_Category = 'SERVICES'

-- Delete Furosemide Medium (7-15kgs)
Delete FROM @forImport
WHERE  GUID = 'CAF9CBC6-ED99-4FB6-9D71-CA61CB5A624F-[Petlink-Caloocan-Services_20220811_1150]'

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Service_ID_ItemType
       and category.IsActive = 1

SELECT DISTINCT Import_Category
FROM   @forImport
WHERE  ID_ItemCategory IS NULL

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Service_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROm   tItemCategory
WHERE  ID_ItemType = 1
Order  by Name 
