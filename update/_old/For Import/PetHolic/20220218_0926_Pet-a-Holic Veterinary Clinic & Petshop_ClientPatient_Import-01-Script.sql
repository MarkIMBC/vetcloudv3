DEclare @GUID_Company VARCHAR(MAX) = 'E20EEA77-74ED-427C-B8DC-45937F9AA40D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @PreImport TABLE
  (
     RowIndex       int IDENTITY(1, 1),
     CustomCode     VARCHAR(MAX),
     Lastname       VARCHAR(MAX),
     Firstname      VARCHAR(MAX),
     Patients       VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX)
  )
DECLARE @Import TABLE
  (
     ID_Client             INT,
     ID_Patient            INT,
     RowIndex_PreImport    int,
     CustomCode_Client     VARCHAR(MAX),
     LastName_Client       VARCHAR(MAX),
     FirstName_Client      VARCHAR(MAX),
     Name_Client           VARCHAR(MAX),
     Name_Patient          VARCHAR(MAX),
     ContactNumber_Client  VARCHAR(MAX),
     ContactNumber2_Client VARCHAR(MAX)
  )

INSERT @PreImport
       (CustomCode,
        Lastname,
        Firstname,
        Patients,
        ContactNumber,
        ContactNumber2)
SELECT dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([ID], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Lastname], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Firstname], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Patient], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([ContantNumber], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([ContantNumber2], '{', '')), '}', ''))
FROM   [Petaholic Client Patient]

Update tClient
SET    Name = dbo.fGetCleanedString(NaME)
WHERE  ID_Company = @ID_Company

Update tPatient
SET    Name = dbo.fGetCleanedString(NaME)
WHERE  ID_Company = @ID_Company

Update @PreImport
SET    ContactNumber = '0' + ContactNumber
WHERE  LEN(ContactNumber) > 0
       and LEN(ContactNumber) < 11

----------------------------------------------------------------------------------------------------------
DECLARE @SplittedPatients TABLE
  (
     RowIndex     Int,
     Name_Patient VARCHAR(MAX)
  )
DECLARE @Counter INT
DECLARE @MaxCounter INT

SET @Counter=1

SELECT @MaxCounter = MAX(RowIndex)
FROM   @PreImport

WHILE ( @Counter <= @MaxCounter )
  BEGIN
      DECLARE @__RowIndex INT = 0
      DECLARE @__Patient VARCHAR(MAX) = ''

      SELECT @__Patient = Patients,
             @__RowIndex = RowIndex
      FROM   @PreImport
      WHERE  RowIndex = @Counter

      INSERT @SplittedPatients
      SELECT @__RowIndex,
             Part
      FROM   dbo.fGetSplitString(@__Patient, '/')

      SET @Counter = @Counter + 1
  END

---------------------------------------------------------------------------------------------------------------------------
INSERT @Import
       (RowIndex_PreImport,
        CustomCode_Client,
        LastName_Client,
        FirstName_Client,
        Name_Client,
        Name_Patient,
        ContactNumber_Client,
        ContactNumber2_Client)
SELECT preimport.RowIndex,
       preimport.CustomCode,
       preimport.Lastname,
       preimport.Firstname,
       preimport.Lastname
       + CASE
           WHEN LEN(preimport.Lastname) > 0
                AND LEN(preimport.Firstname) > 0 THEN ', '
           ELSE ''
         END
       + preimport.Firstname,
       splitedPatient.Name_Patient,
       preimport.ContactNumber,
       preimport.ContactNumber2
FROM   @PreImport preimport
       inner join @SplittedPatients splitedPatient
               on preimport.RowIndex = splitedPatient.RowIndex

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.LastName_Client + ' '
                  + import.FirstName_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.FirstName_Client + ' '
                  + import.LastName_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.FirstName_Client = client.Name
WHERE  ID_Company = @ID_Company
       and LEN(LastName_Client) = 0

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.LastName_Client = client.Name
WHERE  ID_Company = @ID_Company
       and LEN(FirstName_Client) = 0

Update @Import
SET    ID_Client = patient.ID_Client,
       ID_Patient = patient.ID
FROM   @Import import
       inner join tPatient patient
               on import.Name_Patient = patient.Name
                  and import.ID_Client = patient.ID_Client
WHERE  patient.ID_Company = @ID_Company

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        ContactNumber2,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber_Client,
                ContactNumber2_Client,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @Import
where  ID_Client IS NULL  AND Len(Name_Client) > 0

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.LastName_Client + ' '
                  + import.FirstName_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.FirstName_Client + ' '
                  + import.LastName_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.FirstName_Client = client.Name
WHERE  ID_Company = @ID_Company
       and LEN(LastName_Client) = 0

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.LastName_Client = client.Name
WHERE  ID_Company = @ID_Company
       and LEN(FirstName_Client) = 0

Update @Import
SET    ID_Client = patient.ID_Client,
       ID_Patient = patient.ID
FROM   @Import import
       inner join tPatient patient
               on import.Name_Patient = patient.Name
                  and import.ID_Client = patient.ID_Client
WHERE  patient.ID_Company = @ID_Company 

INSERT dbo.tPatient
       (ID_Company,
        ID_Client,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @Import
where  ID_Client IS NOT NULL
       AND ID_Patient IS NULL AND Len(Name_Patient) > 0

SELECT ID_Client,  name_Client, ID_Patient, Name_Patient
FROM   @Import
--SELECT * FROm tClient where ID_Company = 170 Order by Comment , Name
--DELETE FROM tClient where ID_Company = 170 and Comment = 'Imported 02/18/2022 08:32 AM'
--SELECT * FROm tPatient where ID_Company = 170
