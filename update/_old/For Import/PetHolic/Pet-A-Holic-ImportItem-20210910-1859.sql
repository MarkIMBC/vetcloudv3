DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @ID_Company INT = 170

INSERT tItem
       (ID_Company,
	   BarCode,
	   CustomCode,
        Name,
        ID_ItemType,
        UnitPrice,
        UnitCost,
        ID_ItemCategory,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT @ID_Company,
	   [BARCODE],
	   [PRODUCT CODE],
       [DESCRIPTION],
       @ID_ItemType_Inventoriable,
       0,
       0,
       0,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   PRODUCT_LISTING_REPORT_09102021
