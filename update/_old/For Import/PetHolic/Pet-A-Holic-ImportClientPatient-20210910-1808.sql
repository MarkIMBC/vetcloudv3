DECLARE @Name_Client    VARCHAR(MAX)= '',
        @Name_Patient   VARCHAR(MAX)= '',
        @ContactNumber  VARCHAR(MAX)= '',
        @ContactNumber2 VARCHAR(MAX)= ''
DECLARE @_Temp_Name_Client VARCHAR(MAX)= ''
DEclare @ID_Company INT = 170
DEclare @PETAHOLICCLIENTSFORIMPORTING TABLE
  (
     Name_Client    VARCHAR(MAX),
     Name_Patient   VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX)
  )
DECLARE import_cursor CURSOR FOR
  select CASE
           WHEN LEN(ISNULL([SURNAME], '')) > 0 THEN LTRIM(RTRIM(ISNULL([SURNAME], ''))) + ', '
           ELSE ''
         END
         + LTRIM(RTRIM(ISNULL([FIRST NAME], ''))),
         ISNULL([CY], ''),
         CONVERT(BIGINT, ISNULL([CP #], '')),
         CONVERT(BIGINT, ISNULL([Telephone], ''))
  FROM   PETAHOLICCLIENTSFORIMPORTING
  WHERE  [SURNAME] IS NOT NULL
         AND [FIRST NAME] IS NOT NULL
  ORDER  BY CASE
              WHEN LEN(ISNULL([SURNAME], '')) > 0 THEN LTRIM(RTRIM(ISNULL([SURNAME], ''))) + ', '
              ELSE ''
            END
            + LTRIM(RTRIM(ISNULL([FIRST NAME], '')))

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @Name_Patient = REPLACE(@Name_Patient, ',', '/')

      INSERT @PETAHOLICCLIENTSFORIMPORTING
             (Name_Client,
              Name_Patient,
              ContactNumber,
              ContactNumber2)
      VALUES ( @Name_Client,
               @Name_Patient,
               LTRIM(RTRIM(@ContactNumber)),
               LTRIM(RTRIM(@ContactNumber2)) )

      FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

/**/
DEclare @ClientPatient TABLE
  (
     Name_Client    VARCHAR(MAX),
     Name_Patient   VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX)
  )
DECLARE import_cursor CURSOR FOR
  select Name_Client,
         Name_Patient,
         ContactNumber,
         ContactNumber2
  FROM   @PETAHOLICCLIENTSFORIMPORTING

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2

WHILE @@FETCH_STATUS = 0
  BEGIN
      if( @ContactNumber = '0' )
        SET @ContactNumber = ''

      if( @ContactNumber2 = '0' )
        SET @ContactNumber2 = ''

      if( @_Temp_Name_Client <> @Name_Client )
        BEGIN
            SET @_Temp_Name_Client = @Name_Client

            INSERT @ClientPatient
                   (Name_Client,
                    Name_Patient,
                    ContactNumber,
                    ContactNumber2)
            VALUES ( @Name_Client,
                     @Name_Patient,
                     @ContactNumber,
                     @ContactNumber2 )
        END
      ELSE
        BEGIN
            Update @ClientPatient
            SET    Name_Patient = Name_Patient + '/' + @Name_Patient,
                   ContactNumber = REPLACE(ContactNumber, @ContactNumber, '')
                                   + '/' + @ContactNumber,
                   ContactNumber2 = REPLACE(ContactNumber2, @ContactNumber2, '')
                                    + '/' + @ContactNumber2
            WHERE  Name_Client = @Name_Client
        END

      FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

Update @ClientPatient
SET    ContactNumber = REPLACE(ContactNumber, '/////', '/'),
       ContactNumber2 = REPLACE(ContactNumber2, '/////', '/')

Update @ClientPatient
SET    ContactNumber = REPLACE(ContactNumber, '////', '/'),
       ContactNumber2 = REPLACE(ContactNumber2, '////', '/')

Update @ClientPatient
SET    ContactNumber = REPLACE(ContactNumber, '///', '/'),
       ContactNumber2 = REPLACE(ContactNumber2, '///', '/')

Update @ClientPatient
SET    ContactNumber = REPLACE(ContactNumber, '//', '/'),
       ContactNumber2 = REPLACE(ContactNumber2, '//', '/')

Update @ClientPatient
SET    ContactNumber = LEFT(ContactNumber, LEN (ContactNumber) - 1)
WHERE  ContactNumber LIKE '/%'

Update @ClientPatient
SET    ContactNumber = RIGHT(ContactNumber, LEN (ContactNumber) - 1)
WHERE  ContactNumber LIKE '%/'

Update @ClientPatient
SET    ContactNumber2 = LEFT(ContactNumber2, LEN (ContactNumber2) - 1)
WHERE  ContactNumber2 LIKE '/%'

Update @ClientPatient
SET    ContactNumber2 = RIGHT(ContactNumber2, LEN (ContactNumber2) - 1)
WHERE  ContactNumber2 LIKE '%/'

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        ContactNumber2,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
       Name_Client,
       '0' + ContactNumber,
       '0' + ContactNumber2,
       1,
       Getdate(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @ClientPatient


/**/
DEclare @ImportClientPatient TABLE
  (
     Name_Client    VARCHAR(MAX),
     Name_Patient   VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX)
  )
DECLARE import_cursor CURSOR FOR
  select Name_Client,
         Name_Patient,
         ContactNumber,
         ContactNumber2
  FROM   @ClientPatient

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT @ImportClientPatient
             (Name_Client,
              Name_Patient,
              ContactNumber,
              ContactNumber2)
      SELECT DISTINCT @Name_Client,
                      ParT,
                      @ContactNumber,
                      @ContactNumber2
      FROM   dbo.fGetSplitString(@Name_Patient, '/')
      WHERE  LEN(PART) > 0

      FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber, @ContactNumber2
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company,
       client.ID,
       Name_Patient,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @ImportClientPatient _import
       INNER JOIN tClient client
               on _import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company 


Update tClient set ContactNumber2  = ''
WHERE ContactNumber2  = '0'