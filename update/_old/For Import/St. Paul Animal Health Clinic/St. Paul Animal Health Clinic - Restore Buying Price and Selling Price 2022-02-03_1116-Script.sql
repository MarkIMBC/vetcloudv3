DECLARE @ID_Company INT = 22
DECLARE @Items TABLE
  (
     ID_Item   int,
     Name      VARCHAR(MAX),
     UnitCost  Decimal(18, 2),
     UnitPrice Decimal(18, 2)
  )




INSERT @Items
SELECT CONVERT(int, ID),
       [Name],
       CONVERT(decimal(18, 2), REPLACE([Buying Price], ',', '')),
       CONVERT(decimal(18, 2), REPLACE([Selling Price], ',', ''))
FROm   [dbo].[oldPriceStPallAnimalHealth]


SELECT item.Name, item.UnitCost, item.UnitPrice, lastRecord.UnitCost, lastRecord.UnitPrice
FROM   tItem item
       inner join @Items lastRecord
               on item.ID = lastRecord.ID_Item
WHERE  ID_Company = @ID_Company and (item.UnitCost <> lastRecord.UnitCost or item.UnitPrice <> lastRecord.UnitPrice)


Update tItem
SET    UnitCost = lastRecord.UnitCost,
       UnitPrice = lastRecord.UnitPrice
FROM   tItem item
       inner join @Items lastRecord
               on item.ID = lastRecord.ID_Item
WHERE  ID_Company = @ID_Company and (item.UnitCost <> lastRecord.UnitCost or item.UnitPrice <> lastRecord.UnitPrice)



SELECT *
FROm   @Items 
