if OBJECT_ID('dbo.[Pet Express X Vet Street Services]') is not null
  BEGIN
      DROP TABLE [Pet Express X Vet Street Services]
  END

GO

CREATE TABLE [dbo].[Pet Express X Vet Street Services]
  (
     [PARTICULARS] varchar(500),
     [SRP]         decimal(18, 2),
     [UNIT COST]   decimal(18, 2)
  )

GO

INSERT INTO [dbo].[Pet Express X Vet Street Services]
                           ([PARTICULARS],[SRP],[UNIT COST])
                SELECT 'CONSULTATION FEE',                 350.00 ,                 280.00  UNION ALL SELECT 'EAR CLEANING W/EAR INFECT',                 750.00 ,                 600.00  UNION ALL SELECT 'WOUND CLEANING',                 250.00 ,                 200.00  UNION ALL SELECT 'DEWORMING  5kgs',                 175.00 ,                 140.00  UNION ALL SELECT 'DEWORMING 6-10kgs',                 250.00 ,                 200.00  UNION ALL SELECT 'SEDATION      5kgs',             1000.00 ,                 800.00  UNION ALL SELECT 'SEDATION 6-10kgs',             1200.00 ,                 960.00  UNION ALL SELECT 'SEDATION 11-20kgs',             1500.00 ,             1200.00  UNION ALL SELECT 'SEDATION 21-40kgs',             2000.00 ,             1600.00  UNION ALL SELECT 'CYSTOTOMY DOG 11-20kgs',             8500.00 ,             6800.00  UNION ALL SELECT 'INTESTINAL ANASTOMOSIS  S',             9000.00 ,             7200.00  UNION ALL SELECT 'INTESTINAL ANASTOMOSIS CO',          12000.00 ,             9600.00  UNION ALL SELECT 'SURGICAL WOUND REPAIR',             3500.00 ,             2800.00  UNION ALL SELECT 'CAUDECTOMY   10 DAYS DOGS',             4000.00 ,             3200.00  UNION ALL SELECT 'INGUINAL HERNIA REPAIR',             7500.00 ,             6000.00  UNION ALL SELECT 'SCROTAL ABLATION',             4000.00 ,             3200.00  UNION ALL SELECT 'RECTAL PROLAPSE  10 kgs',             5000.00 ,             4000.00  UNION ALL SELECT 'RECTAL PROLAPSE 11-20kgs',             6000.00 ,             4800.00  UNION ALL SELECT 'RECTAL PROLAPSE 21kgs up',             7000.00 ,             5600.00  UNION ALL SELECT 'ESOPHAGOSTOMY',             4500.00 ,             3600.00  UNION ALL SELECT 'AURICULAR HEMATOMA W/O ANESTHESIA',             4000.00 ,             3200.00  UNION ALL SELECT 'SPAYING DOG 21kgs UP',          10000.00 ,             8000.00  UNION ALL SELECT 'PERMANENT TARSORAPHY',             5000.00 ,             4000.00  UNION ALL SELECT 'TEMPORARY TARSORAPHY',             3500.00 ,             2800.00  UNION ALL SELECT 'SPAYING CAT  5kgs',             3000.00 ,             2400.00  UNION ALL SELECT 'SPAYING CAT 6kgs up',             4000.00 ,             3200.00  UNION ALL SELECT 'SPAYING   IN HEAT CAT',             3500.00 ,             2800.00  UNION ALL SELECT 'SPAYING   PREGNANT CAT',             4000.00 ,             3200.00  UNION ALL SELECT 'SPAYING   PYOMETRA CAT',             5000.00 ,             4000.00  UNION ALL SELECT 'ANAL SAC RESECTION',             4000.00 ,             3200.00  UNION ALL SELECT 'CAESAREAN SECTION DOG  10',             9000.00 ,             7200.00  UNION ALL SELECT 'CAESAREAN SECTION DOG 11-',          11000.00 ,             8800.00  UNION ALL SELECT 'CAESAREAN SECTION DOG 21k',          15000.00 ,          12000.00  UNION ALL SELECT 'CAESAREAN   SPAYING DOG',          12000.00 ,             9600.00  UNION ALL SELECT 'CAESAREAN SECTION CAT  5k',             5000.00 ,             4000.00  UNION ALL SELECT 'CAESAREAN SECTION CAT 6kg',             6500.00 ,             5200.00  UNION ALL SELECT 'CAESAREAN   SPAYING CAT',             7500.00 ,             6000.00  UNION ALL SELECT 'ENUCLEATION',             5000.00 ,             4000.00  UNION ALL SELECT 'MASTECTOMY DOG  10kgs',             6000.00 ,             4800.00  UNION ALL SELECT 'MASTECTOMY DOG 11-20kgs',             7000.00 ,             5600.00  UNION ALL SELECT 'MASTECTOMY DOG 21kgs up',             9000.00 ,             7200.00  UNION ALL SELECT 'MASTECTOMY CAT',             4000.00 ,             3200.00  UNION ALL SELECT 'TAIL DOCKING  5days  10kg',                 400.00 ,                 320.00  UNION ALL SELECT 'TAIL DOCKING   5days 21kg',             3500.00 ,             2800.00  UNION ALL SELECT 'TAIL DOCKING  5days 11-20',                 500.00 ,                 400.00  UNION ALL SELECT 'EAR HEMATOMA REMOVAL',             3000.00 ,             2400.00  UNION ALL SELECT 'SMALL TUMOR / CYST REMOVA',             4000.00 ,             3200.00  UNION ALL SELECT 'DEWCLAW REMOVAL DOG  5day',                 300.00 ,                 240.00  UNION ALL SELECT 'DEWCLAW REMOVAL DOG  5day',             3000.00 ,             2400.00  UNION ALL SELECT 'DECLAW CAT',             5500.00 ,             4400.00  UNION ALL SELECT 'PATELLAR LUXATION REPAIR DOG  10kgs',          12000.00 ,             9600.00  UNION ALL SELECT 'PATELLAR LUXATION REPAIR DOG 11-20 kgs',          14000.00 ,          11200.00  UNION ALL SELECT 'PATELLAR LUXATION REPAIR DOG 21kgs up',          16000.00 ,          12800.00  UNION ALL SELECT 'ACL REPAIR DOG  10kgs',          12000.00 ,             9600.00  UNION ALL SELECT 'ACL REPAIR DOG 11-20kgs',          16000.00 ,          12800.00  UNION ALL SELECT 'ACL REPAIR DOG 21kgs up',          18000.00 ,          14400.00  UNION ALL SELECT 'FEMORAL HEAD OSTECTOMY DOG  10kgs',             8000.00 ,             6400.00  UNION ALL SELECT 'FEMORAL HEAD OSTECTOMY DOG 11-20kgs',          10000.00 ,             8000.00  UNION ALL SELECT 'FEMORAL HEAD OSTECTOMY DOG 21kgs up',          12000.00 ,             9600.00  UNION ALL SELECT 'LEG AMPUTATION  10kgs',             7000.00 ,             5600.00  UNION ALL SELECT 'LEG AMPUTATION 11-20kgs',             8000.00 ,             6400.00  UNION ALL SELECT 'LEG AMPUTATION 21kgs up',          10000.00 ,             8000.00  UNION ALL SELECT 'ENTROPION UNILATERAL',             3000.00 ,             2400.00  UNION ALL SELECT 'ENTROPION BILATERAL',             4500.00 ,             3600.00  UNION ALL SELECT 'ISSUANCE OF HEALTH CERT',                 400.00 ,                 320.00  UNION ALL SELECT 'ADD''L PUPPY BOOK CHARGE',                 100.00 ,                    80.00  UNION ALL SELECT 'MICROCHIP APPLICATION',                 150.00 ,                 120.00  UNION ALL SELECT 'Hygroma ( Drain )',             3500.00 ,             2800.00  UNION ALL SELECT 'Hygroma ( Drain ) 2nd  visit and 3rd visit',                 750.00 ,                 600.00  UNION ALL SELECT 'Anal sac draining ( preventive)',                 450.00 ,                 360.00  UNION ALL SELECT 'Anal sac draining (infected)',             1250.00 ,             1000.00  UNION ALL SELECT 'UMBILICAL HERNIA REPAIR',             6500.00 ,             5200.00  UNION ALL SELECT 'DEWORMING 11-20kgs',                 350.00 ,                 280.00  UNION ALL SELECT 'DEWORMING 21-30kgs',                 450.00 ,                 360.00  UNION ALL SELECT 'DEWORMING 31-50kgs',                 550.00 ,                 440.00  UNION ALL SELECT 'CASTRATION CATS  5kgs',             1800.00 ,             1440.00  UNION ALL SELECT 'CASTRATION CATS 6kgs up',             2000.00 ,             1600.00  UNION ALL SELECT 'CASTRATION DOGS  10kgs',             3500.00 ,             2800.00  UNION ALL SELECT 'CASTRATION DOGS 11-20kgs',             4000.00 ,             3200.00  UNION ALL SELECT 'CASTRATION DOGS 21kgs up',             5000.00 ,             4000.00  UNION ALL SELECT 'SPAYING   IN HEAT DOG  10k',             5000.00 ,             4000.00  UNION ALL SELECT 'SPAYING   IN HEAT DOG 10K UP',             6000.00 ,             4800.00  UNION ALL SELECT 'SPAYING   PREGNANT DOG  10KG',             5500.00 ,             4400.00  UNION ALL SELECT 'SPAYING   PREGNANT DOG 10KG UP',             6500.00 ,             5200.00  UNION ALL SELECT 'SPAYING   PYOMETRA DOG  10K',             6500.00 ,             5200.00  UNION ALL SELECT 'SPAYING   PYOMETRA DOG 10K UP',             8500.00 ,             6800.00  UNION ALL SELECT 'SPAYING DOG  10kgs',             4500.00 ,             3600.00  UNION ALL SELECT 'SPAYING DOG 10-kg up',             6500.00 ,             5200.00  UNION ALL SELECT 'WHELPING ASSISTANCE',             2500.00 ,             2000.00  UNION ALL SELECT 'EAR CANAL ABLATION',             4500.00 ,             3600.00  UNION ALL SELECT 'CYSTOTOMY DOG 21kgs up',             9000.00 ,             7200.00  UNION ALL SELECT 'CYSTOTOMY CAT',             5000.00 ,             4000.00  UNION ALL SELECT 'CYSTOTOMY  DOG  10kgs',             7000.00 ,             5600.00  UNION ALL SELECT 'Catheterization',             3500.00 ,             2800.00  UNION ALL SELECT 'O2 supplementation /hr',                 300.00 ,                 240.00  UNION ALL SELECT 'Gas Anaesthesia small',             1500.00 ,             1200.00  UNION ALL SELECT 'Gas Anaesthesia Medium',             2500.00 ,             2000.00  UNION ALL SELECT 'Gas Anaesthesia Large',             3500.00 ,             2800.00  UNION ALL SELECT 'Gas Anaesthesia additiona',             1000.00 ,                 800.00  UNION ALL SELECT 'CHERRY EYE TUCK IN BILATE',             7000.00 ,             5600.00  UNION ALL SELECT 'GASTROTOMY  5KG',             9000.00 ,             7200.00  UNION ALL SELECT 'GASTROPEXY  5KG',             8500.00 ,             6800.00  UNION ALL SELECT 'ENTEROTOMY  5KG',             9000.00 ,             7200.00  UNION ALL SELECT 'PROFESSIONAL DENTAL CLEANING (11-20kgs)',             3500.00 ,             2800.00  UNION ALL SELECT 'PROFESSIONAL DENTAL CLEANING (1-10kgs)',             3000.00 ,             2400.00  UNION ALL SELECT 'PROFESSIONAL DENTAL CLEANING (21-30kgs)',             4000.00 ,             3200.00  UNION ALL SELECT 'PROFESSIONAL DENTAL CLEANING (31kgs above)',             4500.00 ,             3600.00  UNION ALL SELECT 'TOOTH EXTRACTION SIMPLE',                 300.00 ,                 240.00  UNION ALL SELECT 'DENTAL EXTRCTN (PER TOOTH',                 700.00 ,                 560.00  UNION ALL SELECT 'Vaccine 5-in-1',                 500.00 ,                 400.00  UNION ALL SELECT 'Vaccine anti rabies',                 350.00 ,                 280.00  UNION ALL SELECT 'Vaccine kennel cough',                 525.00 ,                 420.00  UNION ALL SELECT 'Vaccine 6-in-1',                 675.00 ,                 540.00  UNION ALL SELECT 'Tricat vaccine',                 900.00 ,                 720.00  UNION ALL SELECT 'RCCP vaccine',                 950.00 ,                 760.00  UNION ALL SELECT 'CBC',                 500.00 ,                 400.00  UNION ALL SELECT 'HRTWRM ANTGN TST KT',                 750.00 ,                 600.00  UNION ALL SELECT 'SKIN SCRAPPING',                 250.00 ,                 200.00  UNION ALL SELECT 'LEPTOSPIROSIS TEST KIT',                 975.00 ,                 780.00  UNION ALL SELECT 'VULVAR SMEAR',                 500.00 ,                 400.00  UNION ALL SELECT 'EHRLICHIA TEST',             1000.00 ,                 800.00  UNION ALL SELECT 'FUNGAL CULTURE',                 700.00 ,                 560.00  UNION ALL SELECT 'PARVOVIRUS ANTIGEN TEST KIT',                 700.00 ,                 560.00  UNION ALL SELECT 'DISTEMPER ANTIGEN TEST KIT',                 800.00 ,                 640.00  UNION ALL SELECT 'XRAY 4 SML DOG',                 800.00 ,                 640.00  UNION ALL SELECT 'XRAY 4 MED DOG',                 900.00 ,                 720.00  UNION ALL SELECT 'XRAY 4 LRG DOG',             1200.00 ,                 960.00  UNION ALL SELECT 'BACTERIA & ANTIBIOTIC SEN',             1500.00 ,             1200.00  UNION ALL SELECT 'T4 TEST',             2000.00 ,             1600.00  UNION ALL SELECT 'IMMUNOCOMB ANTBDY TSTKIT',             1400.00 ,             1120.00  UNION ALL SELECT 'IMMUNOCOMB ANTBDY TSTKT 4',             1400.00 ,             1120.00  UNION ALL SELECT 'PARVOVIRUS AND CORONAVIRUS',             1000.00 ,                 800.00  UNION ALL SELECT 'EAR SWAB EXAM',                 350.00 ,                 280.00  UNION ALL SELECT 'WOODS LAMP EXAM',                 200.00 ,                 160.00  UNION ALL SELECT 'GLUCOSE CHECK',                 250.00 ,                 200.00  UNION ALL SELECT 'URINALYSIS   MICROSCOPIC',                 600.00 ,                 480.00  UNION ALL SELECT 'INTESTINAL PARASITE CHECK',                 250.00 ,                 200.00  UNION ALL SELECT 'DIAGNOSTIC HEALTH PROFILE',             2600.00 ,             2080.00  UNION ALL SELECT 'IDEXX 4DX',             1800.00 ,             1440.00  UNION ALL SELECT '3P (ALT. BUN. CREA.)   CB',             1850.00 ,             1480.00  UNION ALL SELECT 'Sperm Analysis',                 350.00 ,                 280.00  UNION ALL SELECT 'SKIN IMPRESSION SMEAR',                 350.00 ,                 280.00  UNION ALL SELECT 'GENERAL HEALTH PROFLE',             2500.00 ,             2000.00  UNION ALL SELECT 'URINALYSIS (Strip)',                 350.00 ,                 280.00  UNION ALL SELECT 'INJ PENSTREP 400',                 250.00 ,                 200.00  UNION ALL SELECT 'INJ DOXYCLNE/mL',                 250.00 ,                 200.00  UNION ALL SELECT 'INJ MARBOCYL/mL',                 400.00 ,                 320.00  UNION ALL SELECT 'ampicillin vial 250/ml/vi',                 250.00 ,                 200.00  UNION ALL SELECT 'ampicillin vial 500/ml/vi',                 450.00 ,                 360.00  UNION ALL SELECT 'amoxicillin vial 250/ml/v',                 300.00 ,                 240.00  UNION ALL SELECT 'amoxicillin vial 500/ml/v',                 500.00 ,                 400.00  UNION ALL SELECT 'Septotryl Injection',                 250.00 ,                 200.00  UNION ALL SELECT 'INJ ATROSITE/ML',                 500.00 ,                 400.00  UNION ALL SELECT 'INJ ZOLETIL/ML',                 500.00 ,                 400.00  UNION ALL SELECT 'INJ OXYTOCIN/ML',                 500.00 ,                 400.00  UNION ALL SELECT 'Epinephrine/ml',                 500.00 ,                 400.00  UNION ALL SELECT 'Omeprazole/ml ( proton pu',                 600.00 ,                 480.00  UNION ALL SELECT 'Metoclopramide vial 1ml',                 300.00 ,                 240.00  UNION ALL SELECT 'Furosemide vial',                 400.00 ,                 320.00  UNION ALL SELECT 'INJ FERCOBSANG',                 300.00 ,                 240.00  UNION ALL SELECT 'INJ INTROVIT BCOMPLEX/ML',                 280.00 ,                 224.00  UNION ALL SELECT 'INJ PREDNISOLONE',                 350.00 ,                 280.00  UNION ALL SELECT 'TOLFIDINE INJ',                 350.00 ,                 280.00  UNION ALL SELECT 'ENERGIDEX',                 250.00 ,                 200.00  UNION ALL SELECT 'COFORTA ROBORANT',                 300.00 ,                 240.00  UNION ALL SELECT 'ORNIPURAL',                 250.00 ,                 200.00  UNION ALL SELECT 'CALCIUM',                 250.00 ,                 200.00  UNION ALL SELECT 'DEXAMETHASONE INJ',                 350.00 ,                 280.00  UNION ALL SELECT 'MELOXICAM INJ',                 300.00 ,                 240.00  UNION ALL SELECT 'METHYL PREDNISOLONE INJ',                 400.00 ,                 320.00  UNION ALL SELECT 'METHYL PREDNISOLONE INJ 5',                 600.00 ,                 480.00  UNION ALL SELECT 'ECOLMIN INJ',                 300.00 ,                 240.00  UNION ALL SELECT 'Tramadol Hydrochloride in',                 250.00 ,                 200.00  UNION ALL SELECT 'Ranitidne 25mg/ml (2 ml a',                 200.00 ,                 160.00  UNION ALL SELECT 'BOARDING REG/DAY W/O FOOD',                 500.00 ,                 400.00  UNION ALL SELECT 'BOARDING REG/DAY W/O FOOD',                 600.00 ,                 480.00  UNION ALL SELECT 'PET FOOD FOR BOARDING/ DA',                 100.00 ,                    80.00  UNION ALL SELECT 'IV FLUIDS',                 400.00 ,                 320.00  UNION ALL SELECT 'IV TUBE',                 170.00 ,                 136.00  UNION ALL SELECT 'IV CATHETER',                 200.00 ,                 160.00  UNION ALL SELECT 'TOM CATHETER',                 450.00 ,                 360.00  UNION ALL SELECT 'MAJOR SURGERY ANESTHETIC 0-10KG',             1500.00 ,             1200.00  UNION ALL SELECT 'MAJOR SURGERY ANESTHETIC 11-20KG',             2000.00 ,             1600.00  UNION ALL SELECT 'MAJOR SURGERY ANESTHETIC 21KG - 30KG',             2500.00 ,             2000.00  UNION ALL SELECT 'MAJOR SURGERY ANESTHETIC 31KG UP',             3000.00 ,             2400.00  UNION ALL SELECT 'BURIAL 0-10kg',                 700.00 ,                 560.00  UNION ALL SELECT 'BURIAL 11-20 KG',             1500.00 ,             1200.00  UNION ALL SELECT 'BURIAL 20 KG UP',             2000.00 ,             1600.00  UNION ALL SELECT 'Schirmer Tear Test',                 250.00 ,                 200.00  UNION ALL SELECT 'Fluorescein Dye test',                 250.00 ,                 200.00  UNION ALL SELECT 'Microfillaria Smear',                 250.00 ,                 200.00  UNION ALL SELECT 'Microchip   Application',             1500.00 ,             1200.00  UNION ALL SELECT 'Ear Mite Check',                 250.00 ,                 200.00  UNION ALL SELECT 'Microchip Package',                 550.00 ,                 440.00  UNION ALL SELECT 'Follow up Consultation',                 200.00 ,                 160.00  UNION ALL SELECT 'Partial Shave and Cleanin',                 100.00 ,                    80.00  UNION ALL SELECT 'Earmite Infested Cleaning',                 150.00 ,                 120.00  UNION ALL SELECT 'Anal Gland Expression ( R',                 300.00 ,                 240.00  UNION ALL SELECT 'Anal Gland Expression (In',                 600.00 ,                 480.00  UNION ALL SELECT 'Earmite Selamectin Treatment  5kg',                 450.00 ,                 360.00  UNION ALL SELECT 'Earmite Selamectin Treatment 5-15kg',                 550.00 ,                 440.00  UNION ALL SELECT 'Earmite Selamectin Treatment  15kg',                 700.00 ,                 560.00  UNION ALL SELECT 'Doctor''s Fee',                 350.00 ,                 280.00  UNION ALL SELECT 'Vaccine 8in1',                 650.00 ,                 520.00  UNION ALL SELECT 'biocan M',                 900.00 ,                 720.00  UNION ALL SELECT 'Pregnancy Relaxin Test',             1500.00 ,             1200.00  UNION ALL SELECT 'Progesterone Ovulation Te',             1500.00 ,             1200.00  UNION ALL SELECT 'Bacterial Culture and Ant',             2000.00 ,             1600.00  UNION ALL SELECT 'Babesia PCR',             3200.00 ,             2560.00  UNION ALL SELECT 'Ultrasound',                 700.00 ,                 560.00  UNION ALL SELECT 'Selamectin Drops S',                 479.75 ,                 383.80  UNION ALL SELECT 'Selamectin Drops M',                 549.75 ,                 439.80  UNION ALL SELECT 'Selamectin Drops L',                 599.75 ,                 479.80  UNION ALL SELECT 'Selamectin Drops XL',                 699.75 ,                 559.80  UNION ALL SELECT 'WOOD''S LAMP TEST',                 350.00 ,                 280.00  UNION ALL SELECT 'TRICAT VACCINE',             1200.00 ,                 960.00  UNION ALL SELECT 'ENEMA',                 500.00 ,                 400.00  UNION ALL SELECT 'ABSCESS DRAINING',                 600.00 ,                 480.00  UNION ALL SELECT 'CPV-CCV-GIARDIA TEST',             1400.00 ,             1120.00  UNION ALL SELECT 'FIV-FELV TEST KIT',             1200.00 ,                 960.00  UNION ALL SELECT 'FPV TEST KIT',             1000.00 ,                 800.00  UNION ALL SELECT '4-IN-1 DOG VACCINE',                 500.00 ,                 400.00  UNION ALL SELECT 'DHPP+Lepto',                 500.00 ,                 400.00  UNION ALL SELECT 'Milbemax 0.5 - 5 Tx',                 300.00 ,                 240.00  UNION ALL SELECT 'Milbemax 5-25 Tx',                 450.00 ,                 360.00  UNION ALL SELECT 'Vincristine 1mg/0.1ml',                 160.00 ,                 128.00  UNION ALL SELECT 'Convenia /0.1 ml',                 500.00 ,                 400.00  UNION ALL SELECT 'Azi-clovir Tx',                 700.00 ,                 560.00  UNION ALL SELECT 'Aciclovir + Vit Tx',                 350.00 ,                 280.00  UNION ALL SELECT 'Depomedrol inj /0.1 ml',                 400.00 ,                 320.00  UNION ALL SELECT 'FIP Test',             2500.00 ,             2000.00  UNION ALL SELECT 'Complete Blood Count',                 800.00 ,                 640.00  UNION ALL SELECT 'Chem 17 Canine GHP',             3000.00 ,             2400.00  UNION ALL SELECT 'K9 Chem 17 + CBC',             3500.00 ,             2800.00  UNION ALL SELECT 'F9 Chem 15 GHP',             2700.00 ,             2160.00  UNION ALL SELECT 'F9 Chem 15 + CBC',             3200.00 ,             2560.00  UNION ALL SELECT 'Chem 10 Pre-Anes Pannel',             2500.00 ,             2000.00  UNION ALL SELECT 'ElectoLYTE 4',             1100.00 ,                 880.00  UNION ALL SELECT 'Catalyst T4',             2200.00 ,             1760.00  UNION ALL SELECT 'Catalyst Fructosamine',             1600.00 ,             1280.00  UNION ALL SELECT 'Catalyst UPC Ratio',             1300.00 ,             1040.00  UNION ALL SELECT 'Catalyst Phenobarbital',             1600.00 ,             1280.00  UNION ALL SELECT 'Vaccicheck',             1600.00 ,             1280.00  UNION ALL SELECT 'SDMA',             1900.00 ,             1520.00  UNION ALL SELECT 'Papi Topi-Derm',                 249.00 ,                 199.20  UNION ALL SELECT 'Keto-Chlorhex Shampoo ( T',                 349.00 ,                 279.20  UNION ALL SELECT 'Papi Ultralite Plus',                    29.00 ,                    23.20  UNION ALL SELECT 'PROHEART INJ 5-10 KG',             2500.00 ,             2000.00  UNION ALL SELECT 'PROHEART INJ 10-15 KG',             3500.00 ,             2800.00  UNION ALL SELECT 'PROHEART INJ 15-20 KG',             4500.00 ,             3600.00  UNION ALL SELECT 'PROHEART INJ 20-25 KG',             6500.00 ,             5200.00  UNION ALL SELECT 'PROHEART INJ 25-30',             7500.00 ,             6000.00  UNION ALL SELECT 'PROHEART INJ 30-35',             8500.00 ,             6800.00  UNION ALL SELECT 'PROHEART INJ 35-40',             9500.00 ,             7600.00  UNION ALL SELECT 'Skin Mite Selamectin Treatment 5kg',                 450.00 ,                 360.00  UNION ALL SELECT 'Skin Mite Selamectin Treatment 5-15kg',                 550.00 ,                 440.00  UNION ALL SELECT 'Skin Mite Selamectin Treatment 15kg',                 700.00 ,                 560.00  UNION ALL SELECT 'Tick Prevention/Treatment  5kg',                 450.00 ,                 360.00  UNION ALL SELECT 'Tick Prevention/Treatment 5-10 kg',                 550.00 ,                 440.00  UNION ALL SELECT 'Tick Prevention/Treatment 15kg',                 700.00 ,                 560.00  UNION ALL SELECT 'Cytopoint 10 mg',             3000.00 ,             2400.00  UNION ALL SELECT 'Cytopoint 20 mg',             4000.00 ,             3200.00  UNION ALL SELECT 'Cytopoint 30mg',             5000.00 ,             4000.00  UNION ALL SELECT 'Cytopoint 40 mg',             6000.00 ,             4800.00  UNION ALL SELECT 'CERENIA (MAROPITANT CITRA',                 600.00 ,                 480.00  UNION ALL SELECT 'PROHEART INJ 5 KG',             1500.00 ,             1200.00  UNION ALL SELECT 'PROHEART INJ 5-10 KG',             2500.00 ,             2000.00  UNION ALL SELECT 'PROHEART INJ 10-15 KG',             3500.00 ,             2800.00  UNION ALL SELECT 'PROHEART INJ 15-20 KG',             4500.00 ,             3600.00  UNION ALL SELECT 'Cytopoint 20 mg',             3999.75 ,             3199.80  UNION ALL SELECT 'ONLINE CONSULTATION FEE',                 350.00 ,                 280.00  UNION ALL SELECT 'Antibiotic inj  <5 kg',                 300.00 ,                 240.00  UNION ALL SELECT 'Antibiotic inj 5-10 kg',                 350.00 ,                 280.00  UNION ALL SELECT 'Antibiotic inj  10-20 kg',                 450.00 ,                 360.00  UNION ALL SELECT 'Antibiotic inj  20-30 kg',                 600.00 ,                 480.00  UNION ALL SELECT 'Antibiotic inj  30-40 kg',                 800.00 ,                 640.00  UNION ALL SELECT 'Vit K inj',                 350.00 ,                 280.00  UNION ALL SELECT 'Multivitamin inj up to 5k',                 300.00 ,                 240.00  UNION ALL SELECT 'Multivitamin inj 5-10 kg',                 400.00 ,                 320.00  UNION ALL SELECT 'Multivitamin Inj 10-20',                 500.00 ,                 400.00  UNION ALL SELECT 'Multivitamin Inj 20-40',                 700.00 ,                 560.00  UNION ALL SELECT 'Triancinolone Inj 0.1 ml',                 200.00 ,                 160.00  UNION ALL SELECT 'Woodslamp Check',                 250.00 ,                 200.00  UNION ALL SELECT 'Skin Cytology',                 350.00 ,                 280.00  UNION ALL SELECT 'Ear Cytology',                 350.00 ,                 280.00  UNION ALL SELECT 'Direct Ophthalmoscope',                 500.00 ,                 400.00  UNION ALL SELECT 'FIV/FHV/FCoV/FPV',             2000.00 ,             1600.00  UNION ALL SELECT 'Urinalysis Vet Central',                 700.00 ,                 560.00  UNION ALL SELECT 'Tonometer',             1300.00 ,             1040.00  UNION ALL SELECT 'Eye Check Slit Lamp + Ton',             2500.00 ,             2000.00  UNION ALL SELECT 'Parvo Corona Giardia Test',             1500.00 ,             1200.00  UNION ALL SELECT 'Subcutneous Fluids <10 kg',                 300.00 ,                 240.00  UNION ALL SELECT 'SubQ Fluids 10-20',                 400.00 ,                 320.00  UNION ALL SELECT 'SubQ Fluid < 20 kg',                 500.00 ,                 400.00  UNION ALL SELECT 'Anti mange inj /5 kg',                 350.00 ,                 280.00 
GO

if OBJECT_ID('dbo.[Pet Express X Vet Street Items]') is not null
  BEGIN
      DROP TABLE [Pet Express X Vet Street Items]
  END

GO

CREATE TABLE [dbo].[Pet Express X Vet Street Items]
  (
     [PARTICULARS] varchar(500),
     [SRP]         decimal(18, 2),
     [COST]        decimal(18, 2)
  )

GO

INSERT INTO [dbo].[Pet Express X Vet Street Items]
            ([PARTICULARS],
             [SRP],
             [COST])
SELECT 'Pet One Natural Response 120 Tab',
       3960.00,
       3168.00
UNION ALL
SELECT 'Pet One Natural Response 1 tab',
       36.00,
       28.80
UNION ALL
SELECT 'Iron Aid',
       360.00,
       288.00
UNION ALL
SELECT 'Inflacam 32ml',
       840.00,
       672.00
UNION ALL
SELECT 'Tefrosol Forte',
       240.00,
       192.00
UNION ALL
SELECT 'Previcox 57mg Tab',
       84.00,
       67.20
UNION ALL
SELECT 'Previcox 227mg Tab',
       180.00,
       144.00
UNION ALL
SELECT 'Pet Sure 120ml',
       276.00,
       220.80
UNION ALL
SELECT 'Pet Sure 60ml',
       240.00,
       192.00
UNION ALL
SELECT 'Hemacare 60ml',
       264.00,
       211.20
UNION ALL
SELECT 'Hemacare 120ml',
       336.00,
       268.80
UNION ALL
SELECT 'Methiovet 60 tabs',
       1020.00,
       816.00
UNION ALL
SELECT 'Methiovet 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'LC Dox 60ml',
       300.00,
       240.00
UNION ALL
SELECT 'LC Dox 120ml',
       420.00,
       336.00
UNION ALL
SELECT 'LC Dox 100 Tabs',
       1800.00,
       1440.00
UNION ALL
SELECT 'LC Dox 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'Scourvet 60ml',
       360.00,
       288.00
UNION ALL
SELECT 'LC Scour 60ml',
       360.00,
       288.00
UNION ALL
SELECT 'Micoderm 15ml',
       300.00,
       240.00
UNION ALL
SELECT 'Apex Ear Drops Otiderm 15ml',
       660.00,
       528.00
UNION ALL
SELECT 'Livtone 60ml',
       384.00,
       307.20
UNION ALL
SELECT 'Livotone 120ml',
       480.00,
       384.00
UNION ALL
SELECT 'NV Brewers Yeast Chewables Bottle',
       1020.00,
       816.00
UNION ALL
SELECT 'NV Brewers Yeast Chewables 1 tab',
       12.00,
       9.60
UNION ALL
SELECT 'NV Probiotics Capsules 1 bottle',
       1920.00,
       1536.00
UNION ALL
SELECT 'NV Probiotics 1 Capsule',
       36.00,
       28.80
UNION ALL
SELECT 'NV Eye Rinse Liquid',
       780.00,
       624.00
UNION ALL
SELECT 'AS K9 Multi Chewables 1 bottle',
       900.00,
       720.00
UNION ALL
SELECT 'AS K9 Multi Chewable 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'AS K9 Eye Drops',
       636.00,
       508.80
UNION ALL
SELECT 'AS K9 Ear Drops',
       636.00,
       508.80
UNION ALL
SELECT 'AS Healthy Liver Chewable 1 bottle',
       1020.00,
       816.00
UNION ALL
SELECT 'AS Healthy Liver Chewable 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'AS Bladder Control 1 bottle',
       1020.00,
       816.00
UNION ALL
SELECT 'AS Bladder Control 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'AS K9 Calcium Chewables 1 bottle',
       900.00,
       720.00
UNION ALL
SELECT 'AS K9 Calcium Chewables 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'AS Anti Mange Spray',
       720.00,
       576.00
UNION ALL
SELECT 'AS Tick and Flea Away 1 bottle',
       960.00,
       768.00
UNION ALL
SELECT 'AS Tick and Flea Away 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'AS Immune Health 1 bottle',
       900.00,
       720.00
UNION ALL
SELECT 'AS Immune Health 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'AS Anti Coprophagic 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'Nefrotec Tabs 60s',
       660.00,
       528.00
UNION ALL
SELECT 'Nefrotec 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'Immunol Tab 60s',
       720.00,
       576.00
UNION ALL
SELECT 'Immunol Tab 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'Liv52 Tab 60s',
       660.00,
       528.00
UNION ALL
SELECT 'Liv52 1 tab',
       18.00,
       14.40
UNION ALL
SELECT 'Nefrotec DS Tab 60s',
       900.00,
       720.00
UNION ALL
SELECT 'Nefrotec DS 1 Tab',
       24.00,
       19.20
UNION ALL
SELECT 'Baytril 50mg Tab',
       114.00,
       91.20
UNION ALL
SELECT 'Aluminum OH + Magnesium OH Chewable Zilgam',
       12.00,
       9.60
UNION ALL
SELECT 'Cefalexin Monohydrate Cap 250mg Diacef',
       18.00,
       14.40
UNION ALL
SELECT 'Cefalexin Monohydrate Cap 250mg Falteria',
       18.00,
       14.40
UNION ALL
SELECT 'Cefalexin Monohydrate Cap 500mg Exel',
       18.00,
       14.40
UNION ALL
SELECT 'Ceterizine HCl Syr 5mg/5ml Reax 60ml',
       180.00,
       144.00
UNION ALL
SELECT 'Cetirizine HCl Syrup 2.5mg/ml 60ml',
       180.00,
       144.00
UNION ALL
SELECT 'Cetirizine HCL tab 10mg Cetzy-10',
       18.00,
       14.40
UNION ALL
SELECT 'Ciprofloxacin tab 500mg Cyfrox',
       18.00,
       14.40
UNION ALL
SELECT 'Clarithromycin Susp 125mg/5ml Clariwell 60ml',
       300.00,
       240.00
UNION ALL
SELECT 'Co-Amoxiclav FC Tab 625mg Asiclav 625',
       48.00,
       38.40
UNION ALL
SELECT 'Co-Amoxiclav Susp 250mg/62.5mg/5ml Clovimed 60ml',
       360.00,
       288.00
UNION ALL
SELECT 'Co-Amoxiclav Tab 250mg/125mg Myclav 375',
       36.00,
       28.80
UNION ALL
SELECT 'Cotrimoxazole Tab 400mg/80mg Kathrex',
       18.00,
       14.40
UNION ALL
SELECT 'Cotrimoxazole Tab 800mg/160mg Zolbach',
       18.00,
       14.40
UNION ALL
SELECT 'Diphenhydramine HCL Syrup 12.5mg/5ml Hiztasyn 60ml',
       120.00,
       96.00
UNION ALL
SELECT 'Ferrous Sulfate+Folic+Vit B Complex Cap Foralivit',
       18.00,
       14.40
UNION ALL
SELECT 'Furosemide Tab 20mg',
       24.00,
       19.20
UNION ALL
SELECT 'Lactulose Syrup 3.35g Accelac 60ml',
       360.00,
       288.00
UNION ALL
SELECT 'Metoclopramide HCl Susp 5mg/5ml Motillex 60ml',
       120.00,
       96.00
UNION ALL
SELECT 'Metoclopramide HCl Tab 10mg',
       24.00,
       19.20
UNION ALL
SELECT 'Metronidazole Susp 125mg/5ml Ambidazol 60ml',
       144.00,
       115.20
UNION ALL
SELECT 'Metronidazole Tab 500mg Flagex',
       18.00,
       14.40
UNION ALL
SELECT 'Omeprazole Delated Released cap 20mg Ometift',
       30.00,
       24.00
UNION ALL
SELECT 'Prednisolone Syrup 15mg/5ml Impresol 60ml',
       240.00,
       192.00
UNION ALL
SELECT 'Prednisone Susp 10mg/5ml Lefesone 60ml',
       180.00,
       144.00
UNION ALL
SELECT 'Prednisone Tab 10mg',
       30.00,
       24.00
UNION ALL
SELECT 'Prednisone Tab 5mg',
       24.00,
       19.20
UNION ALL
SELECT 'Ranitidine FC Tab 150mg Sriranit',
       24.00,
       19.20
UNION ALL
SELECT 'Vitamin B1 + Vitamin B6 + Vitamin B12 Tab Myrevit B',
       18.00,
       14.40
UNION ALL
SELECT 'Ursodeoxycholic Acid 300mg Tab Urcid',
       72.00,
       57.60
UNION ALL
SELECT 'Emerflox (Enrofloxacin 20%) 60ml',
       360.00,
       288.00
UNION ALL
SELECT 'Tolfenol (Tolfenamic Acid Syrup)',
       360.00,
       288.00
UNION ALL
SELECT 'Pomisol Ear Drops',
       360.00,
       288.00
UNION ALL
SELECT 'Love Drops 200 tabs',
       2400.00,
       1920.00
UNION ALL
SELECT 'Love Drops 1 tab',
       30.00,
       24.00
UNION ALL
SELECT 'Prednil 1 tab',
       24.00,
       19.20
UNION ALL
SELECT 'Ketaconazole 1 tab',
       36.00,
       28.80
UNION ALL
SELECT 'Pulmoquin Syrup 60ml',
       240.00,
       192.00
UNION ALL
SELECT 'Xiclospor Eye Ointment',
       660.00,
       528.00
UNION ALL
SELECT 'E-Collar 7.5cm',
       240.00,
       192.00
UNION ALL
SELECT 'E-Collar 10cm',
       300.00,
       240.00
UNION ALL
SELECT 'E-Collar 12.5cm',
       360.00,
       288.00
UNION ALL
SELECT 'E-Collar 15cm',
       480.00,
       384.00
UNION ALL
SELECT 'E-collar 20cm',
       600.00,
       480.00
UNION ALL
SELECT 'E-Collar 30cm',
       720.00,
       576.00

GO 
