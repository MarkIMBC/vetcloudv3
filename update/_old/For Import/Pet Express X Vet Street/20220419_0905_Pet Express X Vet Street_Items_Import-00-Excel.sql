DEclare @GUID_Company VARCHAR(MAX) = 'DF396BC2-5751-4B1C-9393-D39B4F5CB07D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item   INT,
     Name_Item Varchar(MAX),
     UnitCost  Decimal(18, 2),
     UnitPrice Decimal(18, 2)
  )
DECLARE @IsActive INT=1
DECLARE @Items_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1

INSERT @forImport
       (Name_Item,
        UnitCost,
        UnitPrice)
SELECT dbo.fGetCleanedString([PARTICULARS]),
       [COST] ,
       [SRP] 
FROM   dbo.[Pet Express X Vet Street Items] import

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType
       and IsActive = 1

SELECT *
FROm   @forImport

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             UnitCost,
             [UnitPrice],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                @Items_ID_ItemType,
                import.UnitCost,
                import.UnitPrice,
                @IsActive,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy
FROM   @forImport import
WHERE  ID_Item IS NULL

SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL

SELECT Name_Item,
       COUNT(*) Count
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1 
