DEclare @GUID_Company VARCHAR(MAX) = '36CC1E14-8E80-4ABE-ADFA-D687D6B262FF'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

Update tSupplier
SET    Name = dbo.fGetCleanedString(Name)

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @_TempForImport TABLE
  (
     ID_Item_Supplier   INT,
     ID_Supplier        INT,
     ID_Item            INT,
     Name_Supplier      Varchar(MAX),
     Name_Item          Varchar(MAX),
     BuyingPrice        Decimal(18, 2),
     BuyingPRice_Import Varchar(MAX)
  )


DECLARE @ForImport TABLE
  (
     ID_Item_Supplier   INT,
     ID_Supplier        INT,
     ID_Item            INT,
     Name_Supplier      Varchar(MAX),
     Name_Item          Varchar(MAX),
     BuyingPrice        Decimal(18, 2),
     BuyingPRice_Import Varchar(MAX)
  )
DECLARE @ForItemsWithSupplier TABLE
  (
     ID_Item       INT,
     SupplierCount INT
  )
DECLARE @DuplicateItemNames TABLE
  (
     ID_Item   INT,
     Name_Item Varchar(MAX)
  )

INSERT @_TempForImport
       (Name_Item,
        Name_Supplier,
        BuyingPRice_Import)
SELECT dbo.fGetCleanedString([Item Name]),
       dbo.fGetCleanedString([Supplier Name]),
       REPLACE(REPLACE([Buying], '}', ''), '{', '')
FROm   [HAPPYVETBUYING]

UPDATE @_TempForImport
SET    Name_Supplier = 'BEDAN ANIMALCARE'
WHERE  Name_Supplier = 'BEDAN ANIMAL CARE INC.'

UPDATE @_TempForImport
SET    Name_Supplier = 'DAB VETERINARY PRODUCTS'
WHERE  Name_Supplier = 'DAB VETERINARY DRUG STORE'

UPDATE @_TempForImport
SET    Name_Supplier = 'ELI PETCARE INC'
WHERE  Name_Supplier = 'ELI PETCARE MARKETING'

UPDATE @_TempForImport
SET    Name_Supplier = 'NUTRIVET MARKETING'
WHERE  Name_Supplier = 'NUTRIVET'

UPDATE @_TempForImport
SET    Name_Supplier = 'MS.ARLENE'
WHERE  Name_Supplier = 'MS.ARLENE MARKETINGS'

UPDATE @_TempForImport
SET    Name_Supplier = 'SIDNEY AND CAIRNS'
WHERE  Name_Supplier = 'SIDNEY AND CAIRNS PET ACCESSORIES'

UPDATE @_TempForImport
SET    Name_Supplier = 'SIMON ENTERPRISE'
WHERE  Name_Supplier = 'SIMON AGRIBUSINESS CORP'

UPdate @_TempForImport
SET    BuyingPRice_Import = '0'
WHERE  BuyingPRice_Import = ''

UPdate @_TempForImport
SET    BuyingPRice_Import = '20'
WHERE  BuyingPRice_Import = '20/bot'

UPdate @_TempForImport
SET    BuyingPRice_Import = '25'
WHERE  BuyingPRice_Import = '25/bot'

UPdate @_TempForImport
SET    BuyingPRice_Import = '464'
WHERE  BuyingPRice_Import = '464/bot'

UPdate @_TempForImport
SET    BuyingPRice_Import = '80'
WHERE  BuyingPRice_Import = '80/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '120'
WHERE  BuyingPRice_Import = '120/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '150'
WHERE  BuyingPRice_Import = '150/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '200'
WHERE  BuyingPRice_Import = '200/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '220'
WHERE  BuyingPRice_Import = '220/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '250'
WHERE  BuyingPRice_Import = '250/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '280'
WHERE  BuyingPRice_Import = '280/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '300'
WHERE  BuyingPRice_Import = '300/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '960'
WHERE  BuyingPRice_Import = '960/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '1000'
WHERE  BuyingPRice_Import = '1000/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '1500'
WHERE  BuyingPRice_Import = '1,500/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '2295'
WHERE  BuyingPRice_Import = '2,295/box'

UPdate @_TempForImport
SET    BuyingPRice_Import = '650'
WHERE  BuyingPRice_Import = '650/gal'

UPdate @_TempForImport
SET    BuyingPRice_Import = '184.6'
WHERE  BuyingPRice_Import = '184.6-200'

UPdate @_TempForImport
SET    BuyingPRice_Import = '200'
WHERE  BuyingPRice_Import = '200-220'

UPdate @_TempForImport
SET    BuyingPRice_Import = '500'
WHERE  BuyingPRice_Import = '500-550'

UPdate @_TempForImport
SET    BuyingPRice_Import = '250'
WHERE  BuyingPRice_Import = '250-300'

UPdate @_TempForImport
SET    BuyingPRice_Import = '150'
WHERE  BuyingPRice_Import = '150/pack'

UPdate @_TempForImport
SET    BuyingPRice_Import = '320'
WHERE  BuyingPRice_Import = '320/pack'

UPdate @_TempForImport
SET    BuyingPRice_Import = '350'
WHERE  BuyingPRice_Import = '350/pack'

UPdate @_TempForImport
SET    BuyingPRice_Import = '550'
WHERE  BuyingPRice_Import = '550/pack'

UPdate @_TempForImport
SET    BuyingPRice_Import = '1000'
WHERE  BuyingPRice_Import = '1000/roll'

UPdate @_TempForImport
SET    BuyingPRice_Import = '700'
WHERE  BuyingPRice_Import = '700/tube'

UPdate @_TempForImport
SET    BuyingPRice_Import = '0'
WHERE  BuyingPRice_Import = 'free'


INSERT @ForImport
SELECT DISTINCT * FROm @_TempForImport

Update @ForImport
SET    ID_Supplier = supplier.ID
FROM   @ForImport import
       inner join tSupplier supplier
               on dbo.fGetCleanedString(import.Name_Supplier) = dbo.fGetCleanedString(supplier.Name)
WHERE  ID_Company = @ID_Company

Update @ForImport
SET    ID_Item_Supplier = itmSupplier.ID
FROM   @ForImport import
       inner join tItem item
               on dbo.fGetCleanedString(import.Name_Item) = dbo.fGetCleanedString(item.Name)
       INNER JOIN vItem_Supplier itmSupplier
               on itmSupplier.Name_Supplier = import.Name_Supplier
                  and item.ID = itmSupplier.ID_Item
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = 2

Update @ForImport
SET    ID_Item = item.ID,
       BuyingPrice = item.UnitCost
FROM   @ForImport import
       inner join tItem item
               on dbo.fGetCleanedString(import.Name_Item) = dbo.fGetCleanedString(item.Name)
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = 2


INSERT @DuplicateItemNames
SELECT ID_Item,
       Name_Item
FROM   @ForImport
GROUP  BY ID_Item,
          Name_Item
HAVING Count(*) > 1
Order  BY Name_Item

INSERT @ForItemsWithSupplier
       (ID_Item,
        SupplierCount)
SELECT import.ID_Item,
       ISNULL(SupplierCount, 0) SupplierCount
FROm   @ForImport import
       LEFT join (SELECT import.ID_Item,
                         COUNT(*) SupplierCount
                  FROm   @ForImport import
                         inner join tItem_Supplier itemSupplier
                                 on import.ID_Item = itemSupplier.ID_Item
                         inner join tItem item
                                 on item.ID = itemSupplier.ID_Item
                  WHERE  ID_Company = @ID_Company
                  GROUP  BY import.ID_Item) tbl
              on import.ID_Item = tbl.ID_Item
WHERE  import.ID_Item IS NOT NULL

SELECT import.ID_Item_Supplier,
       import.ID_Supplier,
       import.ID_Item,
       import.Name_Supplier,
       import.Name_Item,
       MAX(import.BuyingPRice_Import) BuyingPRice_Import
FROM   tItem item
       inner join @ForImport import
               on item.iD = import.ID_Item
WHERE  Name_Item NOT IN (SELECT Name_Item
                         FROm   @DuplicateItemNames)
       and ID_Item_Supplier IS NULL
GROUP  BY import.ID_Item_Supplier,
          import.ID_Supplier,
          import.ID_Item,
          import.Name_Supplier,
          import.Name_Item

Update tItem
SET    UnitCost = BuyingPRice_Import
FROM   tItem item
       inner join (SELECT DISTINCT ID_Item
                   FROm   tItem_Supplier) itmSuppler
               on item.ID = itmSuppler.ID_Item
       inner join @ForImport import
               on item.iD = import.ID_Item
WHERE  Name_Item NOT IN (SELECT Name_Item
                         FROm   @DuplicateItemNames)
       and ISNUMERIC(BuyingPRice_Import) = 1
       and ID_Item_Supplier IS NULL

SELECT DIStinct *
FROM   @ForImport
WHERE  Name_Item IN (SELECT Name_Item
                     FROm   @DuplicateItemNames)
Order  by Name_Item

SELECT  *
FROM   @ForImport
Order by Name_Item, ISNULL(ID_Item_Supplier, 0) DESC


--SELECT *
--FROM   tItem_Supplier
--exec _pAddModelProperty
--  'tItem_Supplier',
--  'UnitCost',
--  3
--EXEC _pRefreshAllViews 
SELECT *
FROm   @ForItemsWithSupplier 
