﻿/* BEWARE Reexecuting this query may result of duplicate records on tPatient*/

EXEC _pBackUpDatabase

GO

IF (EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'LourdesVeterinaryClinic_AddNewItem_20210212')
  )
BEGIN

  DROP TABLE dbo.[LourdesVeterinaryClinic_AddNewItem_20210212]
END

GO

/****** Object:  Table [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212]    Script Date: 2/12/2021 7:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] (
  [Item] [nvarchar](255) NULL
 ,[UnitPrice] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'ADVOCATE', 700)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'AMBIMOX', 5)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'APPE BOOST', 180)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'ARTHROX', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BEARING DRY SHAMPOO', 280)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BORIC ACID+BORAX EQUISINE REGULAR', 140)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BRAVECTO (2-4.5KG)', 1500)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BRAVECTO (4.5-10KG)', 1500)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BRAVECTO (20-40 KG) ', 1500)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'BUBBLE BATH', 350)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'CANINE DISTEMPER TEST', 850)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'CEPHALEXIN CFLEX', 200)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'CO-AMOXICLAV AXALAV', 25)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'CO-AMOXICLAV COMOXI', 200)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'CYPERMETHRIN', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DE TICK PLUS(2cc)', 250)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DE TICK PLUS(1cc)', 170)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DELTACAL ', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DEXAMETHASONE SYNTHEMAX', 250)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DEXTROLYTE', 10)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DIAHIST ANTIHISTAMINE', 80)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DOGGIE BISCUITS', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DOXYCYCLINE HCI LC-DOX', 320)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DOXYCYCLINE HYCLATE', 10)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DOXYCYCLINE HYCLATE PAPIDOXY', 220)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'DOXYVET 10', 15)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'FIDOW', 200)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'FIDOW DRY SHAMPOO', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'FLAGEX METRONIDAZOLE', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'GENTAMICIN OPTAZOL', 350)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'HEMACARE-FE', 220)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'HYROMELLOSE EQUISINE MOIST', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'LC-SCOUR', 250)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'LIVEROLIN', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MADRE DE CACAO HERBAL CREAM', 200)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MAJOR D LIQUID DISINFECTANT', 130)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MAJOR D LIQUID DISINFECTANT', 30)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MAXI COAT', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'METRONIDAZOLE METROZOLE', 150)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MOXYLOR', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MUPIROCIN MUPIREX', 150)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'MYCOCIDE', 400)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEPRO', 200)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD SPECTRA (2-3.5KG)', 650)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD PLAIN (2-4KG)', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD SPECTRA (3.5-7.5KG)', 700)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD  PLAIN (10-25KG)', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD PLAIN (25-50KG)', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD PLAIN (4-10KG)', 700)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD SPECTRA (15-30KG)', 750)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NEXGARD SPECTRA (7.5-15KG)', 800)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'NUTRI PLUS GEL ', 600)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'ODORLESS PROBIOTIC', 1100)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'OFLOXACIN OFLOBIZ', 220)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PAPI (LIV WELL)', 180)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PAPI RENA CURE', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PARVO/CORONA VIRUS TEST', 1000)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PETSURE', 180)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PET TABS', 15)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PREDNISONE BETPRED', 150)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PREDNISONE CORTICOSTEROID', NULL)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'PUPPY LOVE MILK', 300)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'RAPID TEST', 850)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'ROYAL CANIN URINARY S/O', 230)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'SULFUR OINTMENT', 100)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'TOBRAMYCIN RAMITOB', 180)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VET NATURALS (KAKAWATE+KALAMANSI', 180)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VITAMIN E', 5)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE (5IN1-VANGUARD)', 500)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE (5IN1-RECOMBITEK)', 500)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE (8IN1-VANGUARD)', 600)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE (ANTI-RABIES)', 300)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE FOR CATS(3IN1)', 700)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE FOR CATS(4IN1)', 700)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'VACCINE (KENNEL COUGH)', 550)
GO
INSERT [dbo].[LourdesVeterinaryClinic_AddNewItem_20210212] ([Item], [UnitPrice])
  VALUES (N'WORMRID', 200)
GO

DECLARE @LourdesVeterinaryClinic_ID_Company INT = 27


INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  Old_item_id)
SELECT 
  @LourdesVeterinaryClinic_ID_Company
  ,Item
  ,UnitPrice
  ,2
  , NULL
  , NULL
  , NULL
  , 1
  , NULL
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , NULL
 FROM   [LourdesVeterinaryClinic_AddNewItem_20210212]


GO
IF (EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'LourdesVeterinaryClinic_AddNewItem_20210212')
  )
BEGIN

  DROP TABLE dbo.[LourdesVeterinaryClinic_AddNewItem_20210212]
END

GO