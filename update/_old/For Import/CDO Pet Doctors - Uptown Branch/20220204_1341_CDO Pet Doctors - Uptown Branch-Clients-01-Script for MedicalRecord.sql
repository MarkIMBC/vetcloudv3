DEclare @GUID_Company VARCHAR(MAX) = '1DF6BF02-A694-425E-8A05-AB0AD5C9B1DD'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Patient_SOAP                     INT,
     ID_Patient_SOAP_Plan                INT,
     ID_Client                           INT,
     _tempID                             Varchar(MAX),
     Name_Client                         Varchar(MAX),
     ContactNumber                       Varchar(MAX),
     ContactNumber2                      Varchar(MAX),
     Address                             Varchar(MAX),
     ID_Patient                          Int,
     Name_Patient                        Varchar(MAX),
     DateBirth                           DateTime,
     Marking                             Varchar(MAX),
     Species                             Varchar(MAX),
     Date_MedicalRecord                  DateTime,
     Remark_MedicalRecord                Varchar(MAX),
     Weight_MedicalRecord                Varchar(MAX),
     DateNextSchedule_Plan_MedicalRecord DateTime,
     Appointment_Plan_MedicalRecord      Varchar(MAX)
  )
DECLARE @forImportClient TABLE
  (
     ID_Client      INT,
     Name_Client    Varchar(MAX),
     ContactNumber  Varchar(MAX),
     ContactNumber2 Varchar(MAX),
     Address        Varchar(MAX)
  )
DECLARE @forImportPatient TABLE
  (
     ID_Client    INT,
     ID_Patient   Int,
     Name_Patient Varchar(MAX),
     DateBirth    DateTime,
     Marking      Varchar(MAX),
     Species      Varchar(MAX)
  )
DECLARE @forImportMedicalRecord TABLE
  (
     _tempID                             Varchar(MAX),
     ID_Patient_SOAP                     INT,
     ID_Patient_SOAP_Plan                INT,
     ID_Client                           INT,
     ID_Patient                          INT,
     Date_MedicalRecord                  DateTime,
     Remark_MedicalRecord                Varchar(MAX),
     Weight_MedicalRecord                Varchar(MAX),
     DateNextSchedule_Plan_MedicalRecord DateTime,
     Appointment_Plan_MedicalRecord      Varchar(MAX)
  )

INSERT @forImport
       (_tempID,
        Name_Client,
        ContactNumber,
        ContactNumber2,
        Address,
        Name_Patient,
        DateBirth,
        Marking,
        Species,
        Date_MedicalRecord,
        Remark_MedicalRecord,
        Weight_MedicalRecord,
        DateNextSchedule_Plan_MedicalRecord,
        Appointment_Plan_MedicalRecord)
SELECT dbo.fGetCleanedString([Asset ID]),
       dbo.fGetCleanedString([Client Name]),
       dbo.fGetCleanedString([Contact No]),
       dbo.fGetCleanedString([Tel No]),
       dbo.fGetCleanedString([Address]),
       dbo.fGetCleanedString([PetID]),
       dbo.fGetCleanedString([Born]),
       dbo.fGetCleanedString([Marking]),
       dbo.fGetCleanedString([Breed]),
       dbo.fGetCleanedString([Date]),
       dbo.fGetCleanedString([Remark]),
       dbo.fGetCleanedString([Weight]),
       dbo.fGetCleanedString([NextSched]),
       dbo.fGetCleanedString([Appointment])
FROM   [CDO-UPTOWNOldClients]

UPdate @forImport
SET    Name_Patient = Species
WHERE  LEN(Name_Patient) = 0
       AND LEN(Species) > 0

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, ',', ', ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, '.', '. ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, ' , ', ', ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, '  ', ' ')

UPdate @forImport
SET    Name_Client = NULL
WHERE  Name_Client = '`'

UPdate @forImport
SET    Name_Client = NULL
WHERE  Name_Client = '-'

UPdate @forImport
SET    DateBirth = NULL
WHERE  DateBirth = '1900-01-01 00:00:00.000'

UPdate @forImport
SET    Date_MedicalRecord = NULL
WHERE  Date_MedicalRecord = '1900-01-01 00:00:00.000'

UPdate @forImport
SET    DateNextSchedule_Plan_MedicalRecord = NULL
WHERE  DateNextSchedule_Plan_MedicalRecord = '1900-01-01 00:00:00.000'

DELETE @forImport
WHERE  LEN(Name_Client) = 0
       AND LEN(Name_Patient) = 0

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

Update @forImport
SET    ID_Patient = patient.ID
FROm   @forImport import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
                  and patient.ID_Client = client.ID
WHERE  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

Update @forImport
SET    ID_Patient_SOAP = soap.ID
FROM   @forImport import
       inner join tPatient_SOAP soap
               ON import._tempID = soap._tempID
                  AND import.ID_Client = soap.ID_Client
                  AND import.ID_Patient = soap.ID_Patient
WHERE  ID_Company = @ID_Company

Update @forImport
SET    ID_Patient_SOAP_Plan = soapPlan.ID
FROM   @forImport import
       inner join tPatient_SOAP_Plan soapPlan
               on soapPlan.ID_Patient_SOAP = import.ID_Patient_SOAP
                  AND import.DateNextSchedule_Plan_MedicalRecord = soapPlan.DateReturn
                  And import.Appointment_Plan_MedicalRecord = soapPlan.Comment
       inner join tPatient_SOAP soap
               on soap.ID = import.ID_Patient_SOAP
WHERE  ID_Company = @ID_Company

INSERT @forImportMedicalRecord
       (_tempID,
        ID_Patient_SOAP,
        ID_Patient_SOAP_Plan,
        ID_Client,
        ID_Patient,
        Date_MedicalRecord,
        Remark_MedicalRecord,
        Weight_MedicalRecord,
        DateNextSchedule_Plan_MedicalRecord,
        Appointment_Plan_MedicalRecord)
SELECT _tempID,
       ID_Patient_SOAP,
       ID_Patient_SOAP_Plan,
       ID_Client,
       ID_Patient,
       Date_MedicalRecord,
       Remark_MedicalRecord,
       Weight_MedicalRecord,
       DateNextSchedule_Plan_MedicalRecord,
       Appointment_Plan_MedicalRecord
FROM   @forImport

INSERT tPatient_SOAP
       (ID_Company,
        _tempID,
        ID_Client,
        ID_Patient,
        Date,
        History,
        ClinicalExamination,
        Comment)
SELECT @ID_Company,
       _tempID,
       ID_Client,
       ID_Patient,
       Date_MedicalRecord,
       Remark_MedicalRecord,
       CASE
         WHEN LEN(ISNULL(Weight_MedicalRecord, '')) > 0 THEN 'Weight: ' + Weight_MedicalRecord
         ELSE ''
       END Weight_MedicalRecord,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @forImportMedicalRecord
WHERE  ID_Patient_SOAP IS NULL
       AND Date_MedicalRecord IS NOT NULL
       AND ID_Client IS NOT NULL
       AND ID_Patient IS NOT NULL

Update @forImportMedicalRecord
SET    ID_Patient_SOAP = soap.ID
FROM   @forImportMedicalRecord import
       inner join tPatient_SOAP soap
               ON import._tempID = soap._tempID
                  AND import.ID_Client = soap.ID_Client
                  AND import.ID_Patient = soap.ID_Patient
WHERE  ID_Company = @ID_Company

INSERT tPatient_SOAP_Plan
       (ID_Patient_SOAP,
        DateReturn,
        Comment)
SELECT ID_Patient_SOAP,
       DateNextSchedule_Plan_MedicalRecord,
       Appointment_Plan_MedicalRecord
FROm   @forImportMedicalRecord
WHERE  ID_Patient_SOAP_Plan IS NULL
       AND ID_Patient_SOAP IS NOT NULL
       AND DateNextSchedule_Plan_MedicalRecord IS NOT NULL
       and LEN(ISNULL(Appointment_Plan_MedicalRecord, '')) > 0

Update @forImportMedicalRecord
SET    ID_Patient_SOAP_Plan = soapPlan.ID
FROM   @forImportMedicalRecord import
       inner join tPatient_SOAP_Plan soapPlan
               on import.ID_Patient_SOAP = soapPlan.ID_Patient_SOAP
                  AND import.DateNextSchedule_Plan_MedicalRecord = soapPlan.DateReturn
                  And import.Appointment_Plan_MedicalRecord = soapPlan.Comment
       inner join tPatient_SOAP soap
               on soap.ID = import.ID_Patient_SOAP
WHERE  ID_Company = @ID_Company

Update @forImport
SET    ID_Patient_SOAP = soap.ID
FROM   @forImport import
       inner join tPatient_SOAP soap
               ON import._tempID = soap._tempID
                  AND import.ID_Client = soap.ID_Client
                  AND import.ID_Patient = soap.ID_Patient
WHERE  ID_Company = @ID_Company

Update @forImport
SET    ID_Patient_SOAP_Plan = soapPlan.ID
FROM   @forImport import
       inner join tPatient_SOAP_Plan soapPlan
               on soapPlan.ID_Patient_SOAP = import.ID_Patient_SOAP
                  AND import.DateNextSchedule_Plan_MedicalRecord = soapPlan.DateReturn
                  And import.Appointment_Plan_MedicalRecord = soapPlan.Comment
       inner join tPatient_SOAP soap
               on soap.ID = import.ID_Patient_SOAP
WHERE  ID_Company = @ID_Company

/*
DELETE FROM tPatient_SOAP_Plan
WHERE  ID_Patient_SOAP IN (SELECT ID
                           FROm   tPatient_SOAP
                           WHERE  Comment LIKE '%Imported 02/05/2022%')

DELETE FROM tPatient_SOAP
WHERE  Comment LIKE '%Imported 02/05/2022%'
*/
SELECT DISTINCT client.Name,
                client.ContactNumber,
                client.ContactNumber2,
                client.Address
FROM   @forImport import
       INNER JOIN tClient client
               on client.ID = import.ID_Client

SELECT DISTINCT patient.Name_Client,
                patient.Name,
                patient.DateBirth,
                patient.Comment,
                patient.Species
FROM   @forImport import
       INNER JOIN vPatient patient
               on patient.ID = import.ID_Patient

SELECT soap.Code,
       soap._tempID,
       soap.Date,
       soap.Name_Client,
       soap.Name_Patient,
       History,
       ClinicalExamination,
       soapPlan.DateReturn,
       soapPlan.Comment
FROM   @forImport import
       inner join vPatient_SOAP soap
               on import.ID_Patient_SOAP = soap.ID
       INNER JOIN tPatient_SOAP_Plan soapPlan
               on soapPlan.ID = import.ID_Patient_SOAP_Plan

SELECT _tempID [Asset ID],
       Name_Client,
       ContactNumber,
       ContactNumber2,
       Address,
       Name_Patient,
       DateBirth,
       Marking,
       Species,
       Date_MedicalRecord,
       Remark_MedicalRecord,
       Weight_MedicalRecord,
       DateNextSchedule_Plan_MedicalRecord,
       Appointment_Plan_MedicalRecord
FROm   @forImport
WHERE  ID_Patient_SOAP IS NULL 
