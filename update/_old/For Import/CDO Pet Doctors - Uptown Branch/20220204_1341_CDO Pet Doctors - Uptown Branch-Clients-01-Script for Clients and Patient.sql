DEclare @GUID_Company VARCHAR(MAX) = '1DF6BF02-A694-425E-8A05-AB0AD5C9B1DD'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client                           INT,
     _tempID                             Varchar(MAX),
     Name_Client                         Varchar(MAX),
     ContactNumber                       Varchar(MAX),
     ContactNumber2                      Varchar(MAX),
     Address                             Varchar(MAX),
     ID_Patient                          Int,
     Name_Patient                        Varchar(MAX),
     DateBirth                           DateTime,
     Marking                             Varchar(MAX),
     Species                             Varchar(MAX),
     Date_MedicalRecord                  DateTime,
     Remark_MedicalRecord                Varchar(MAX),
     Weight_MedicalRecord                Varchar(MAX),
     DateNextSchedule_Plan_MedicalRecord DateTime,
     Appointment_Plan_MedicalRecord      Varchar(MAX)
  )
DECLARE @forImportClient TABLE
  (
     ID_Client      INT,
     Name_Client    Varchar(MAX),
     ContactNumber  Varchar(MAX),
     ContactNumber2 Varchar(MAX),
     Address        Varchar(MAX)
  )
DECLARE @forImportPatient TABLE
  (
     ID_Client    INT,
     ID_Patient   Int,
     Name_Patient Varchar(MAX),
     DateBirth    DateTime,
     Marking      Varchar(MAX),
     Species      Varchar(MAX)
  )

INSERT @forImport
       (_tempID,
        Name_Client,
        ContactNumber,
        ContactNumber2,
        Address,
        Name_Patient,
        DateBirth,
        Marking,
        Species,
        Date_MedicalRecord,
        Remark_MedicalRecord,
        Weight_MedicalRecord,
        DateNextSchedule_Plan_MedicalRecord,
        Appointment_Plan_MedicalRecord)
SELECT dbo.fGetCleanedString([Asset ID]),
       dbo.fGetCleanedString([Client Name]),
       dbo.fGetCleanedString([Contact No]),
       dbo.fGetCleanedString([Tel No]),
       dbo.fGetCleanedString([Address]),
       dbo.fGetCleanedString([PetID]),
       dbo.fGetCleanedString([Born]),
       dbo.fGetCleanedString([Marking]),
       dbo.fGetCleanedString([Breed]),
       dbo.fGetCleanedString([Date]),
       dbo.fGetCleanedString([Remark]),
       dbo.fGetCleanedString([Weight]),
       dbo.fGetCleanedString([NextSched]),
       dbo.fGetCleanedString([Appointment])
FROM   [CDO-UPTOWNOldClients]

UPdate @forImport
SET    Name_Patient = Species
WHERE  LEN(Name_Patient) = 0
       AND LEN(Species) > 0

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, ',', ', ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, '.', '. ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, ' , ', ', ')

UPdate @forImport
SET    Name_Client = REPLACE(Name_Client, '  ', ' ')

UPdate @forImport
SET    Name_Client = NULL
WHERE  Name_Client = '`'

UPdate @forImport
SET    Name_Client = NULL
WHERE  Name_Client = '-'

UPdate @forImport
SET    DateBirth = NULL
WHERE  DateBirth = '1900-01-01 00:00:00.000'

UPdate @forImport
SET    Date_MedicalRecord = NULL
WHERE  Date_MedicalRecord = '1900-01-01 00:00:00.000'

UPdate @forImport
SET    DateNextSchedule_Plan_MedicalRecord = NULL
WHERE  DateNextSchedule_Plan_MedicalRecord = '1900-01-01 00:00:00.000'

DELETE @forImport
WHERE  LEN(Name_Client) = 0
       AND LEN(Name_Patient) = 0

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

Update @forImport
SET    ID_Patient = patient.ID
FROm   @forImport import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
                  and patient.ID_Client = client.ID
WHERE  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

INSERT @forImportClient
SELECT DISTINCT ID_Client,
                Name_Client,
                MAX(ContactNumber),
                MAX(ContactNumber2),
                MAX(Address)
FROM   @forImport
WHERE  LEn(Name_Client) > 0
       AND ID_Client IS NULL
GROUP  BY ID_Client,
          Name_Client

SELECT Name_Client,
       Count(*)
FROm   @forImportClient
GROUP  BY Name_Client
HAVING Count(*) > 1

SELECT Name_Client,
       ContactNumber,
       Count(*)
FROm   @forImportClient
GROUP  BY Name_Client,
          ContactNumber
HAVING Count(*) > 1

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        ContactNumber2,
        Address,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber,
                ContactNumber2,
                Address,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImportClient
WHERE  ID_Client IS NULL
       AND LEN(Name_Client) > 0

Update @forImport
SET    ID_Client = client.ID
FROm   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company

INSERT @forImportPatient
       (ID_Client,
        ID_Patient,
        Name_Patient,
        DateBirth,
        Marking,
        Species)
SELECT ID_Client,
       ID_Patient,
       Name_Patient,
       MAX(DateBirth),
       MAX(Marking),
       MAX(Species)
FROM   @forImport
WHERE  LEn(Name_Client) > 0
       AND ID_Client IS NOT NULL
GROUP  BY ID_Client,
          ID_Patient,
          Name_Patient

SELECT ID_Client,
       ID_Patient,
       Name_Patient,
       COUNT(*) Count
FROM   @forImportPatient
GROUP  BY ID_Client,
          ID_Patient,
          Name_Patient
HAVING Count(*) > 1

INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Species,
        DateBirth,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                Species,
                DateBirth,
                CASE
                  WHEN LEN(Marking) > 0 THEN 'Markings :' + Marking + CHAR(13) + CHAR(13)
                  ELSE ''
                END
                + 'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImportPatient
WHERE  LEN (Name_Patient) > 0
       and ID_Patient IS NULL

Update @forImportPatient
SET    ID_Patient = patient.ID
FROm   @forImportPatient import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.ID_Client = client.ID
                  and patient.ID_Client = client.ID
WHERE  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

Update tPatient
SEt    DateBirth = import.DateBirth
FROM   tPatient patient
       INNER JOIN @forImportPatient import
               on patient.ID = import.ID_Patient
WHERE  ID_Company = @ID_Company

SELECT *
FROM   tClient
WHERE  ID_Company = @ID_Company

SELECT ID,
       Name_Client,
       Name
FROM   vPatient
WHERE  ID_Company = @ID_Company
/*
DELETE FROM tClient WHERE Comment LIKE '%Imported 02/04/2022%' AND ID_Company  = 245
DELETE FROM tPatient WHERE Comment LIKE '%Imported 02/04/2022%%' AND  ID_Company  = 245

DELETE FROM tClient WHERE Comment LIKE '%Imported 02/05/2022%' AND  ID_Company  = 245
DELETE FROM tPatient WHERE Comment LIKE '%Imported 02/05/2022%%'  AND  ID_Company  = 245
*/
