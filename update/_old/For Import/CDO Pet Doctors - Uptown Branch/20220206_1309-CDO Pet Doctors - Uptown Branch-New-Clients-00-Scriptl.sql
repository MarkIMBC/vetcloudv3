DEclare @GUID_Company VARCHAR(MAX) = '1DF6BF02-A694-425E-8A05-AB0AD5C9B1DD'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Client     INT,
     ID_Patient    INT,
     Name_Client   Varchar(MAX),
     ContactNumber Varchar(MAX),
     Name_Patient  Varchar(MAX)
  )
DECLARE @ForImportClient TABLE
  (
     ID_Client     INT,
     ID_Patient    INT,
     Name_Client   Varchar(MAX),
     ContactNumber Varchar(MAX),
     Name_Patient  Varchar(MAX)
  )

INSERT @ForImport
       (Name_Client,
        ContactNumber,
        Name_Patient)
SELECT dbo.fGetCleanedString([Client Name]),
       dbo.fGetCleanedString([Mobile Phone]),
       dbo.fGetCleanedString([Pet Name])
FROM   [CDO Pet Doctors - Uptown Branch New]

Update @ForImport
SET    ContactNumber = REPLACE(ContactNumber, '-', '')

Update @ForImport
SET    ContactNumber = REPLACE(ContactNumber, '/', ' / ')

UPdate @ForImport
SET    ID_Client = client.ID
FROm   @ForImport import
       inner join tClient client
               on import.Name_Client = client.Name
where  ID_Company = @ID_Company

UPdate @ForImport
SET    ID_Client = client.ID,
       ID_Patient = patient.ID
FROm   @ForImport import
       inner join tClient client
               on import.Name_Client = client.Name
       INNER JOIN tPatient patient
               on import.ID_Client = patient.ID_Client
                  AND import.Name_Patient = patient.Name
where  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

SELECT *
FROm   @ForImport

INSERT @ForImportClient
       (ID_Client,
        Name_Client,
        ContactNumber)
SELECT ID_Client,
       Name_Client,
       MAX(ContactNumber)
FROm   @ForImport
GROUP  BY ID_Client,
          Name_Client
Order  by Name_Client

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @ForImportClient
WHERE  ID_Client IS NULL
       AND LEN (Name_Client) > 0

UPdate @ForImport
SET    ID_Client = client.ID
FROm   @ForImport import
       inner join tClient client
               on import.Name_Client = client.Name
where  ID_Company = @ID_Company

INSERT @ForImportClient
       (ID_Client,
        Name_Client,
        ContactNumber)
SELECT ID_Client,
       Name_Client,
       MAX(ContactNumber)
FROm   @ForImport
GROUP  BY ID_Client,
          Name_Client
Order  by Name_Client

INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @forImport
WHERE  LEN (Name_Patient) > 0
       AND ID_Client IS NOT NULL
       and ID_Patient IS NULL

UPdate @ForImport
SET    ID_Client = client.ID,
       ID_Patient = patient.ID
FROm   @ForImport import
       inner join tClient client
               on import.Name_Client = client.Name
       INNER JOIN tPatient patient
               on import.ID_Client = patient.ID_Client
                  AND import.Name_Patient = patient.Name
where  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

SELECT *
FROm   @ForImport

SELECT *
FROm   tClient
WHERE  ID_Company = @ID_Company
/*
	DELETE FROM tClient WHERE Comment LIKE '%Imported 02/06/2022%' AND  ID_Company = 245
	DELETE FROM tPatient WHERE Comment LIKE '%Imported 02/06/2022%' AND  ID_Company = 245
*/
