DECLARE @GUID_Company VARCHAR(MAX) = 'F13C585D-2D21-401F-8B58-BCDAF68DE516'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @IsActive INT=1
DECLARE @Service_ID_ItemType INT =1
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_ItemCategory INT,
     ID_Item         INT,
     [Category]      varchar(MAX),
     [Name_Item]     varchar(MAX),
     [UnitCost]      varchar(500),
     [UnitPrice]     varchar(500)
  )

INSERT @ForImport
       ([Category],
        [Name_Item],
        [UnitPrice],
        [UnitCost])
SELECT dbo.[fGetCleanedString]([Category]),
       dbo.[fGetCleanedString]([Item]),
       TRY_CONVERT(DECIMAL(18, 2), dbo.[fGetCleanedString]([Price])),
       TRY_CONVERT(DECIMAL(18, 2), dbo.[fGetCleanedString]([Cost]))
FROM   [Noahs Ark Pet Wellness Center - Caloocan_Services_to_Upload_Vet_Cloud]

UPdate @ForImport
set    Category = 'Grooming Services'
WHERE  Category = '01 Grooming'

UPdate @ForImport
set    Category = 'Wellness'
WHERE  Category = '02 Wellness'

UPdate @ForImport
set    Category = 'LABORATORIES'
WHERE  Category = '03 Laboratory & Diagnostics'

UPdate @ForImport
set    Category = 'Injectable Medications'
WHERE  Category = '04 Injectable Treatment'

UPdate @ForImport
set    Category = 'Injectable Medications'
WHERE  Category = '04 Injectable Treatment'

UPdate @ForImport
set    Category = 'SP with confine'
WHERE  Category = '05 SP with confine'

UPdate @ForImport
set    Category = 'SP without confine'
WHERE  Category = '06 SP without confine'

UPdate @ForImport
set    Category = 'Confinement & Lodging'
WHERE  Category = '07 Confinement & Lodging'

UPdate @ForImport
set    Category = 'Others'
WHERE  Category = '12 Other Services'

UPdate @ForImport
set    Category = 'Others'
WHERE  Category IN ( '12 Other Services', '13 Misc' )

Update tItem
SET    IsActive = 0
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType

Update @ForImport
SET    ID_ItemCategory = tbl.ID
FROM   @ForImport import
       inner join (SELECT MAX(ID) ID,
                          Name
                   FROM   tItemCategory
                   WHERE  ID_ItemType = @Service_ID_ItemType
                          AND IsActive = 1
                   GROUP  BY Name) tbl
               on import.Category = tbl.Name

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       and IsActive = 1

SELECT *
FROM   @ForImport

INSERT INTO [dbo].[tItem]
            ([Name],
             ID_ItemCategory,
             [ID_ItemType],
             [UnitCost],
             [UnitPrice],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                import.ID_ItemCategory,
                @Service_ID_ItemType,
                import.UnitCost,
                import.UnitPrice,
                @IsActive,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy
FROM   @forImport import
WHERE  ID_Item IS NULL

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

SELECT Name_Item,
       COUNT(*)
FROM   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

exec pUpdateItemCurrentInventory 
