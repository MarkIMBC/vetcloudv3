if OBJECT_ID('dbo.[Noahs Ark Pet Wellness Center - Caloocan_Services_to_Upload_Vet_Cloud]') is not null
  BEGIN
      DROP TABLE [Noahs Ark Pet Wellness Center - Caloocan_Services_to_Upload_Vet_Cloud]
  END

GO

CREATE TABLE [dbo].[Noahs Ark Pet Wellness Center - Caloocan_Services_to_Upload_Vet_Cloud]
  (
     [#]        varchar(500),
     [Category] varchar(500),
     [Item]     varchar(500),
     [Price]    varchar(500),
     [Cost]     varchar(500)
  )

GO

INSERT INTO [dbo].[Noahs Ark Pet Wellness Center - Caloocan_Services_to_Upload_Vet_Cloud]
            ([#],
             [Category],
             [Item],
             [Price],
             [Cost])
SELECT '7',
       '01 Grooming',
       '01 Full Grooming Up to 10Kgs',
       '400',
       ''
UNION ALL
SELECT '8',
       '01 Grooming',
       '01 Full Grooming 10 to 20Kgs',
       '550',
       ''
UNION ALL
SELECT '9',
       '01 Grooming',
       '01 Full Grooming 20 to 30KGs',
       '650',
       ''
UNION ALL
SELECT '10',
       '01 Grooming',
       '01 Full Grooming 30Kgs and Up',
       '750',
       ''
UNION ALL
SELECT '11',
       '01 Grooming',
       '02 Full Grooming + Styling Up to 10Kgs',
       '550',
       ''
UNION ALL
SELECT '12',
       '01 Grooming',
       '02 Full Grooming + Styling 10 to 20Kgs',
       '700',
       ''
UNION ALL
SELECT '13',
       '01 Grooming',
       '02 Full Grooming + Styling 20 to 30KGs',
       '800',
       ''
UNION ALL
SELECT '14',
       '01 Grooming',
       '02 Full Grooming + Styling 30Kgs and Up',
       '900',
       ''
UNION ALL
SELECT '15',
       '01 Grooming',
       '03 Wash & Go Up to 10Kgs',
       '250',
       ''
UNION ALL
SELECT '16',
       '01 Grooming',
       '03 Wash & Go 10 to 20Kgs',
       '350',
       ''
UNION ALL
SELECT '17',
       '01 Grooming',
       '03 Wash & Go 20 to 30KGs',
       '450',
       ''
UNION ALL
SELECT '18',
       '01 Grooming',
       '03 Wash & Go 30Kgs and Up',
       '500',
       ''
UNION ALL
SELECT '19',
       '01 Grooming',
       '04 Cat Grooming',
       '400',
       ''
UNION ALL
SELECT '20',
       '01 Grooming',
       '05 Ala Carte Face Trim',
       '80',
       ''
UNION ALL
SELECT '21',
       '01 Grooming',
       '05 Ala Carte Nail & Pad Clipping',
       '50',
       ''
UNION ALL
SELECT '22',
       '01 Grooming',
       '05 Ala Carte Ear Cleaning',
       '50',
       ''
UNION ALL
SELECT '23',
       '01 Grooming',
       '05 Ala Carte Toothbrushing',
       '80',
       ''
UNION ALL
SELECT '24',
       '01 Grooming',
       '05 Ala Carte Eye Cleaning',
       '80',
       ''
UNION ALL
SELECT '25',
       '01 Grooming',
       '05 Ala Carte Anal Gland Expression',
       '100',
       ''
UNION ALL
SELECT '26',
       '01 Grooming',
       '05 Ala Carte Butt Trim',
       '80',
       ''
UNION ALL
SELECT '27',
       '01 Grooming',
       '05 Ala Carte Sanitary Trim',
       '240',
       ''
UNION ALL
SELECT '28',
       '01 Grooming',
       '06 Medicated Bath',
       '100',
       ''
UNION ALL
SELECT '29',
       '02 Wellness',
       'Professional Fee Initial',
       '300',
       ''
UNION ALL
SELECT '30',
       '02 Wellness',
       'Professional Fee Follow Up',
       '50',
       ''
UNION ALL
SELECT '1087',
       '12 Other Services',
       'Euthanasia 5-10Kgs',
       '1700',
       ''
UNION ALL
SELECT '1088',
       '12 Other Services',
       'Euthanasia 10-15Kgs',
       '2300',
       ''
UNION ALL
SELECT '1089',
       '12 Other Services',
       'Euthanasia 15-20Kgs',
       '2900',
       ''
UNION ALL
SELECT '1090',
       '12 Other Services',
       'Euthanasia 20-30Kgs',
       '4200',
       ''
UNION ALL
SELECT '1091',
       '12 Other Services',
       'Euthanasia 30-40Kgs',
       '5500',
       ''
UNION ALL
SELECT '1092',
       '12 Other Services',
       'Dextrose Connection',
       '300',
       ''
UNION ALL
SELECT '1093',
       '12 Other Services',
       'Medical Certificate',
       '300',
       ''
UNION ALL
SELECT '1094',
       '12 Other Services',
       'Oxygen Administration',
       '300',
       ''
UNION ALL
SELECT '1095',
       '12 Other Services',
       'Microchip Implant',
       '1100',
       '299'
UNION ALL
SELECT '1103',
       '13 Misc',
       'Wound Cleaning and Treatment',
       '300',
       ''
UNION ALL
SELECT '63',
       '03 Laboratory & Diagnostics',
       'Package CBC + Blood Chemistry � Prep Profile',
       '2150',
       ''
UNION ALL
SELECT '64',
       '03 Laboratory & Diagnostics',
       'Package CBC+ Blood Chemistry � Comprehensive Diagnostic Pr',
       '2600',
       ''
UNION ALL
SELECT '65',
       '03 Laboratory & Diagnostics',
       'Package Distemper + Parvo Test (Antibody) � Vcheck',
       '1200',
       ''
UNION ALL
SELECT '66',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure CBC',
       '850',
       ''
UNION ALL
SELECT '67',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure CBC � Follow Up',
       '500',
       ''
UNION ALL
SELECT '68',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Blood Chemistry � Prep Profile',
       '1550',
       ''
UNION ALL
SELECT '69',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Blood Chemistry � Comprehensive Diagnostic Profile',
       '2200',
       ''
UNION ALL
SELECT '70',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Ultrasound',
       '750',
       ''
UNION ALL
SELECT '71',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Fecalysis',
       '300',
       ''
UNION ALL
SELECT '72',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Skin Test Scraping',
       '350',
       ''
UNION ALL
SELECT '73',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Ear Swab (for ear mites)',
       '300',
       ''
UNION ALL
SELECT '74',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Staining',
       '350',
       ''
UNION ALL
SELECT '75',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Urine Sedimentation',
       '300',
       ''
UNION ALL
SELECT '76',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Urinalysis',
       '300',
       ''
UNION ALL
SELECT '77',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Flourescein Dye Test',
       '250',
       ''
UNION ALL
SELECT '78',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Biopsy Test',
       '1900',
       ''
UNION ALL
SELECT '79',
       '03 Laboratory & Diagnostics',
       'Diagnostic Procedure Leptospirosis',
       '1500',
       ''
UNION ALL
SELECT '80',
       '03 Laboratory & Diagnostics',
       'Vcheck Progesterone',
       '800',
       ''
UNION ALL
SELECT '81',
       '03 Laboratory & Diagnostics',
       'Vcheck T4',
       '900',
       ''
UNION ALL
SELECT '82',
       '03 Laboratory & Diagnostics',
       'Vcheck Pancreatitis cPL',
       '950',
       ''
UNION ALL
SELECT '83',
       '03 Laboratory & Diagnostics',
       'Vcheck Pancreatitis fPL',
       '1100',
       ''
UNION ALL
SELECT '84',
       '03 Laboratory & Diagnostics',
       'Vcheck Distemper Test (Antibody)',
       '800',
       ''
UNION ALL
SELECT '85',
       '03 Laboratory & Diagnostics',
       'Vcheck Parvo Test (Antibody)',
       '800',
       ''
UNION ALL
SELECT '86',
       '03 Laboratory & Diagnostics',
       'Vcheck Distemper Test (Antigen)',
       '800',
       ''
UNION ALL
SELECT '87',
       '03 Laboratory & Diagnostics',
       'Vcheck Parvo Test (Antigen)',
       '800',
       ''
UNION ALL
SELECT '88',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - Canivet 1 Way CPV',
       '800',
       ''
UNION ALL
SELECT '89',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - Canivet 1 Way CDV',
       '800',
       ''
UNION ALL
SELECT '90',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - Canivet 2 Way CDV + CPV',
       '1200',
       ''
UNION ALL
SELECT '91',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - Canivet 3 Way CPV + CCV + Giardia',
       '1400',
       ''
UNION ALL
SELECT '92',
       '03 Laboratory & Diagnostics',
       'Test Kit Heart Worm � Test Kit: Canine Heartworm Ag Test',
       '1000',
       ''
UNION ALL
SELECT '93',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - Canivet 3 Way Blood Parasite: Ana+Ehr+Bab',
       '1400',
       ''
UNION ALL
SELECT '94',
       '03 Laboratory & Diagnostics',
       'Test Kit Canine - A Pet Care 4 Way Ehrlichia - Anaplasma -',
       '1500',
       ''
UNION ALL
SELECT '95',
       '03 Laboratory & Diagnostics',
       'Test Kit Feline - Felivet 2 Way FIV + FELV',
       '1200',
       ''
UNION ALL
SELECT '96',
       '03 Laboratory & Diagnostics',
       'Test Kit Feline - Felivet 4 Way FPV + FCV + FHV + FCOV',
       '1800',
       ''
UNION ALL
SELECT '97',
       '03 Laboratory & Diagnostics',
       'Test Kit Relaxin Rapid Test Kit',
       '1500',
       ''
UNION ALL
SELECT '98',
       '04 Injectable Treatment',
       'Digestive Tx Up to 10Kgs',
       '400',
       ''
UNION ALL
SELECT '99',
       '04 Injectable Treatment',
       'Digestive Tx 10-20Kgs',
       '450',
       ''
UNION ALL
SELECT '100',
       '04 Injectable Treatment',
       'Digestive Tx 20-30Kgs',
       '550',
       ''
UNION ALL
SELECT '101',
       '04 Injectable Treatment',
       'Digestive Tx 30Kgs and Up',
       '650',
       ''
UNION ALL
SELECT '102',
       '04 Injectable Treatment',
       'Eyes  ENT Tx Up to 10Kgs',
       '400',
       ''
UNION ALL
SELECT '103',
       '04 Injectable Treatment',
       'Eyes  ENT Tx 10-20Kgs',
       '450',
       ''
UNION ALL
SELECT '104',
       '04 Injectable Treatment',
       'Eyes  ENT Tx 20-30Kgs',
       '550',
       ''
UNION ALL
SELECT '105',
       '04 Injectable Treatment',
       'Eyes  ENT Tx 30Kgs and Up',
       '650',
       ''
UNION ALL
SELECT '106',
       '04 Injectable Treatment',
       'Reproductive Tx Up to 10Kgs',
       '550',
       ''
UNION ALL
SELECT '107',
       '04 Injectable Treatment',
       'Reproductive Tx 10-20Kgs',
       '600',
       ''
UNION ALL
SELECT '108',
       '04 Injectable Treatment',
       'Reproductive Tx 20-30Kgs',
       '700',
       ''
UNION ALL
SELECT '109',
       '04 Injectable Treatment',
       'Reproductive Tx 30Kgs and Up',
       '800',
       ''
UNION ALL
SELECT '110',
       '04 Injectable Treatment',
       'Respiratory Tx Up to 10Kgs',
       '400',
       '243'
UNION ALL
SELECT '111',
       '04 Injectable Treatment',
       'Respiratory Tx 10-20Kgs',
       '450',
       '243'
UNION ALL
SELECT '112',
       '04 Injectable Treatment',
       'Respiratory Tx 20-30Kgs',
       '550',
       '435'
UNION ALL
SELECT '113',
       '04 Injectable Treatment',
       'Respiratory Tx 30Kgs and Up',
       '650',
       '685'
UNION ALL
SELECT '114',
       '04 Injectable Treatment',
       'Skin Tx Up to 10Kgs',
       '400',
       '242'
UNION ALL
SELECT '115',
       '04 Injectable Treatment',
       'Skin Tx 10-20Kgs',
       '450',
       '773'
UNION ALL
SELECT '116',
       '04 Injectable Treatment',
       'Skin Tx 20-30Kgs',
       '550',
       '750'
UNION ALL
SELECT '117',
       '04 Injectable Treatment',
       'Skin Tx 30Kgs and Up',
       '650',
       '415'
UNION ALL
SELECT '118',
       '04 Injectable Treatment',
       'Urinary Tx Up to 10Kgs',
       '450',
       '940'
UNION ALL
SELECT '119',
       '04 Injectable Treatment',
       'Urinary Tx 10-20Kgs',
       '500',
       '600'
UNION ALL
SELECT '120',
       '04 Injectable Treatment',
       'Urinary Tx 20-30Kgs',
       '600',
       ''
UNION ALL
SELECT '121',
       '04 Injectable Treatment',
       'Urinary Tx 30Kgs and Up',
       '700',
       ''
UNION ALL
SELECT '122',
       '04 Injectable Treatment',
       'Poison Tx Up to 10Kgs',
       '550',
       ''
UNION ALL
SELECT '123',
       '04 Injectable Treatment',
       'Poison Tx 10-20Kgs',
       '600',
       ''
UNION ALL
SELECT '124',
       '04 Injectable Treatment',
       'Poison Tx 20-30Kgs',
       '700',
       ''
UNION ALL
SELECT '125',
       '04 Injectable Treatment',
       'Poison Tx 30Kgs and Up',
       '800',
       ''
UNION ALL
SELECT '126',
       '04 Injectable Treatment',
       'Skeletal Tx Up to 10Kgs',
       '450',
       ''
UNION ALL
SELECT '127',
       '04 Injectable Treatment',
       'Skeletal Tx 10-20Kgs',
       '500',
       ''
UNION ALL
SELECT '128',
       '04 Injectable Treatment',
       'Skeletal Tx 20-30Kgs',
       '600',
       ''
UNION ALL
SELECT '129',
       '04 Injectable Treatment',
       'Skeletal Tx 30Kgs and Up',
       '700',
       ''
UNION ALL
SELECT '130',
       '04 Injectable Treatment',
       'General Tx Up to 10Kgs',
       '400',
       ''
UNION ALL
SELECT '131',
       '04 Injectable Treatment',
       'General Tx 10-20Kgs',
       '450',
       ''
UNION ALL
SELECT '132',
       '04 Injectable Treatment',
       'General Tx 20-30Kgs',
       '550',
       ''
UNION ALL
SELECT '133',
       '04 Injectable Treatment',
       'General Tx 30Kgs and Up',
       '650',
       ''
UNION ALL
SELECT '134',
       '04 Injectable Treatment',
       'Emergency Tx Up to 10Kgs',
       '450',
       ''
UNION ALL
SELECT '135',
       '04 Injectable Treatment',
       'Emergency Tx 10-20Kgs',
       '500',
       ''
UNION ALL
SELECT '136',
       '04 Injectable Treatment',
       'Emergency Tx 20-30Kgs',
       '600',
       ''
UNION ALL
SELECT '137',
       '04 Injectable Treatment',
       'Emergency Tx 30Kgs and Up',
       '700',
       ''
UNION ALL
SELECT '138',
       '04 Injectable Treatment',
       'Vitamins  Supplements Up to 10Kgs',
       '150',
       ''
UNION ALL
SELECT '139',
       '04 Injectable Treatment',
       'Vitamins  Supplements 10-20Kgs',
       '200',
       ''
UNION ALL
SELECT '140',
       '04 Injectable Treatment',
       'Vitamins  Supplements 20-30Kgs',
       '250',
       ''
UNION ALL
SELECT '141',
       '04 Injectable Treatment',
       'Vitamins  Supplements 30Kgs and Up',
       '300',
       ''
UNION ALL
SELECT '142',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 1-5Kgs',
       '500',
       ''
UNION ALL
SELECT '143',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 5-10Kgs',
       '900',
       ''
UNION ALL
SELECT '144',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 10-15Kgs',
       '1300',
       ''
UNION ALL
SELECT '145',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 15-20Kgs',
       '1700',
       ''
UNION ALL
SELECT '146',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 20-25Kgs',
       '2100',
       ''
UNION ALL
SELECT '147',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 25-30Kgs',
       '2500',
       ''
UNION ALL
SELECT '148',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 30-35Kgs',
       '2900',
       ''
UNION ALL
SELECT '149',
       '04 Injectable Treatment',
       'Vinion Vincristine Sulfate 35-40Kgs',
       '3300',
       ''
UNION ALL
SELECT '150',
       '04 Injectable Treatment',
       'Canglob Parvo 1-5Kgs',
       '500',
       ''
UNION ALL
SELECT '151',
       '04 Injectable Treatment',
       'Canglob Parvo 5-10Kgs',
       '750',
       ''
UNION ALL
SELECT '152',
       '04 Injectable Treatment',
       'Canglob Parvo 10-15Kgs',
       '1000',
       ''
UNION ALL
SELECT '153',
       '04 Injectable Treatment',
       'Canglob Parvo 15-20Kgs',
       '1250',
       ''
UNION ALL
SELECT '154',
       '04 Injectable Treatment',
       'Canglob Parvo 20-25Kgs',
       '1500',
       ''
UNION ALL
SELECT '155',
       '04 Injectable Treatment',
       'Canglob Parvo 25-30Kgs',
       '1750',
       ''
UNION ALL
SELECT '156',
       '04 Injectable Treatment',
       'Canglob Parvo 30-35Kgs',
       '2000',
       ''
UNION ALL
SELECT '157',
       '04 Injectable Treatment',
       'Canglob Parvo 35-40Kgs',
       '2250',
       ''
UNION ALL
SELECT '158',
       '04 Injectable Treatment',
       'Convenia 1-5Kgs',
       '1300',
       ''
UNION ALL
SELECT '159',
       '04 Injectable Treatment',
       'Convenia 5-10Kgs',
       '1900',
       ''
UNION ALL
SELECT '160',
       '04 Injectable Treatment',
       'Convenia 10-15Kgs',
       '2500',
       ''
UNION ALL
SELECT '161',
       '04 Injectable Treatment',
       'Convenia 15-20Kgs',
       '3100',
       ''
UNION ALL
SELECT '162',
       '04 Injectable Treatment',
       'Convenia 20-25Kgs',
       '3600',
       ''
UNION ALL
SELECT '163',
       '04 Injectable Treatment',
       'Convenia 25-30Kgs',
       '4100',
       ''
UNION ALL
SELECT '164',
       '04 Injectable Treatment',
       'Convenia 30-35Kgs',
       '4600',
       ''
UNION ALL
SELECT '165',
       '04 Injectable Treatment',
       'Convenia 35-40Kgs',
       '5100',
       ''
UNION ALL
SELECT '166',
       '04 Injectable Treatment',
       'Cerenia 1-5Kgs',
       '800',
       ''
UNION ALL
SELECT '167',
       '04 Injectable Treatment',
       'Cerenia 5-10Kgs',
       '1100',
       ''
UNION ALL
SELECT '168',
       '04 Injectable Treatment',
       'Cerenia 10-15Kgs',
       '1400',
       ''
UNION ALL
SELECT '169',
       '04 Injectable Treatment',
       'Cerenia 15-20Kgs',
       '1700',
       ''
UNION ALL
SELECT '170',
       '04 Injectable Treatment',
       'Cerenia 20-25Kgs',
       '2000',
       ''
UNION ALL
SELECT '171',
       '04 Injectable Treatment',
       'Cerenia 25-30Kgs',
       '2200',
       ''
UNION ALL
SELECT '172',
       '04 Injectable Treatment',
       'Cerenia 30-35Kgs',
       '2400',
       ''
UNION ALL
SELECT '173',
       '04 Injectable Treatment',
       'Cerenia 35-40Kgs',
       '2700',
       ''
UNION ALL
SELECT '174',
       '05 SP with confine',
       'Custom Description',
       '1',
       ''
UNION ALL
SELECT '175',
       '05 SP with confine',
       'Catheterization (Cats) 1-5Kgs',
       '3900',
       ''
UNION ALL
SELECT '176',
       '05 SP with confine',
       'Catheterization (Cats) 5kgs Above',
       '4200',
       ''
UNION ALL
SELECT '177',
       '05 SP with confine',
       'Abdominocentesis 1-5Kgs',
       '3500',
       ''
UNION ALL
SELECT '178',
       '05 SP with confine',
       'Abdominocentesis 5-10Kgs',
       '4000',
       ''
UNION ALL
SELECT '179',
       '05 SP with confine',
       'Abdominocentesis 10-15Kgs',
       '4400',
       ''
UNION ALL
SELECT '180',
       '05 SP with confine',
       'Abdominocentesis 16-20Kgs',
       '4700',
       ''
UNION ALL
SELECT '181',
       '05 SP with confine',
       'Abdominocentesis 20-30Kgs',
       '5400',
       ''
UNION ALL
SELECT '182',
       '05 SP with confine',
       'Abdominocentesis 30-40Kgs',
       '6300',
       ''
UNION ALL
SELECT '183',
       '05 SP with confine',
       'Auricular Hematoma (1) 1-5Kgs',
       '5000',
       ''
UNION ALL
SELECT '184',
       '05 SP with confine',
       'Auricular Hematoma (1) 5-10Kgs',
       '5600',
       ''
UNION ALL
SELECT '185',
       '05 SP with confine',
       'Auricular Hematoma (1) 10-15Kgs',
       '6300',
       ''
UNION ALL
SELECT '186',
       '05 SP with confine',
       'Auricular Hematoma (1) 16-20Kgs',
       '6800',
       ''
UNION ALL
SELECT '187',
       '05 SP with confine',
       'Auricular Hematoma (1) 20-30Kgs',
       '7900',
       ''
UNION ALL
SELECT '188',
       '05 SP with confine',
       'Auricular Hematoma (1) 30-40Kgs',
       '9400',
       ''
UNION ALL
SELECT '189',
       '05 SP with confine',
       'Auricular Hematoma (2) 1-5Kgs',
       '6700',
       ''
UNION ALL
SELECT '190',
       '05 SP with confine',
       'Auricular Hematoma (2) 5-10Kgs',
       '7400',
       ''
UNION ALL
SELECT '191',
       '05 SP with confine',
       'Auricular Hematoma (2) 10-15Kgs',
       '8000',
       ''
UNION ALL
SELECT '192',
       '05 SP with confine',
       'Auricular Hematoma (2) 16-20Kgs',
       '8500',
       ''
UNION ALL
SELECT '193',
       '05 SP with confine',
       'Auricular Hematoma (2) 20-30Kgs',
       '9600',
       ''
UNION ALL
SELECT '194',
       '05 SP with confine',
       'Auricular Hematoma (2) 30-40Kgs',
       '11100',
       ''
UNION ALL
SELECT '195',
       '05 SP with confine',
       'Caesarian Delivery 1-5Kgs',
       '8300',
       ''
UNION ALL
SELECT '196',
       '05 SP with confine',
       'Caesarian Delivery 5-10Kgs',
       '9100',
       ''
UNION ALL
SELECT '197',
       '05 SP with confine',
       'Caesarian Delivery 10-15Kgs',
       '9900',
       ''
UNION ALL
SELECT '198',
       '05 SP with confine',
       'Caesarian Delivery 16-20Kgs',
       '10500',
       ''
UNION ALL
SELECT '199',
       '05 SP with confine',
       'Caesarian Delivery 20-30Kgs',
       '11800',
       ''
UNION ALL
SELECT '200',
       '05 SP with confine',
       'Caesarian Delivery 30-40Kgs',
       '13500',
       ''
UNION ALL
SELECT '201',
       '05 SP with confine',
       'Castration (Cats) 1-5Kgs',
       '3700',
       ''
UNION ALL
SELECT '202',
       '05 SP with confine',
       'Castration (Cats) 5-10Kgs',
       '4100',
       ''
UNION ALL
SELECT '203',
       '05 SP with confine',
       'Castration (Dogs) 1-5Kgs',
       '4200',
       ''
UNION ALL
SELECT '204',
       '05 SP with confine',
       'Castration (Dogs) 5-10Kgs',
       '4700',
       ''
UNION ALL
SELECT '205',
       '05 SP with confine',
       'Castration (Dogs) 10-15Kgs',
       '5100',
       ''
UNION ALL
SELECT '206',
       '05 SP with confine',
       'Castration (Dogs) 16-20Kgs',
       '5400',
       ''
UNION ALL
SELECT '207',
       '05 SP with confine',
       'Castration (Dogs) 20-30Kgs',
       '6100',
       ''
UNION ALL
SELECT '208',
       '05 SP with confine',
       'Castration (Dogs) 30-40Kgs',
       '7000',
       ''
UNION ALL
SELECT '209',
       '05 SP with confine',
       'Catherization (Dogs) 1-5Kgs',
       '4200',
       ''
UNION ALL
SELECT '210',
       '05 SP with confine',
       'Catherization (Dogs) 5-10Kgs',
       '4500',
       ''
UNION ALL
SELECT '211',
       '05 SP with confine',
       'Catherization (Dogs) 10-15Kgs',
       '5000',
       ''
UNION ALL
SELECT '212',
       '05 SP with confine',
       'Catherization (Dogs) 16-20Kgs',
       '5200',
       ''
UNION ALL
SELECT '213',
       '05 SP with confine',
       'Catherization (Dogs) 20-30Kgs',
       '5900',
       ''
UNION ALL
SELECT '214',
       '05 SP with confine',
       'Catherization (Dogs) 30-40Kgs',
       '6800',
       ''
UNION ALL
SELECT '215',
       '05 SP with confine',
       'Cherry Eye Removal (1) 1-5Kgs',
       '3900',
       ''
UNION ALL
SELECT '216',
       '05 SP with confine',
       'Cherry Eye Removal (1) 5-10Kgs',
       '4300',
       ''
UNION ALL
SELECT '217',
       '05 SP with confine',
       'Cherry Eye Removal (1) 10-15Kgs',
       '4600',
       ''
UNION ALL
SELECT '218',
       '05 SP with confine',
       'Cherry Eye Removal (1) 16-20Kgs',
       '4900',
       ''
UNION ALL
SELECT '219',
       '05 SP with confine',
       'Cherry Eye Removal (1) 20-30Kgs',
       '5600',
       ''
UNION ALL
SELECT '220',
       '05 SP with confine',
       'Cherry Eye Removal (1) 30-40Kgs',
       '6500',
       ''
UNION ALL
SELECT '221',
       '05 SP with confine',
       'Cherry Eye Removal (2) 1-5Kgs',
       '5000',
       ''
UNION ALL
SELECT '222',
       '05 SP with confine',
       'Cherry Eye Removal (2) 5-10Kgs',
       '5400',
       ''
UNION ALL
SELECT '223',
       '05 SP with confine',
       'Cherry Eye Removal (2) 10-15Kgs',
       '5700',
       ''
UNION ALL
SELECT '224',
       '05 SP with confine',
       'Cherry Eye Removal (2) 16-20Kgs',
       '6100',
       ''
UNION ALL
SELECT '225',
       '05 SP with confine',
       'Cherry Eye Removal (2) 20-30Kgs',
       '6700',
       ''
UNION ALL
SELECT '226',
       '05 SP with confine',
       'Cherry Eye Removal (2) 30-40Kgs',
       '7600',
       ''
UNION ALL
SELECT '227',
       '05 SP with confine',
       'Cystocentesis 1-5Kgs',
       '3300',
       ''
UNION ALL
SELECT '228',
       '05 SP with confine',
       'Cystocentesis 5-10Kgs',
       '3700',
       ''
UNION ALL
SELECT '229',
       '05 SP with confine',
       'Cystocentesis 10-15Kgs',
       '4200',
       ''
UNION ALL
SELECT '230',
       '05 SP with confine',
       'Cystocentesis 16-20Kgs',
       '4500',
       ''
UNION ALL
SELECT '231',
       '05 SP with confine',
       'Cystocentesis 20-30Kgs',
       '5200',
       ''
UNION ALL
SELECT '232',
       '05 SP with confine',
       'Cystocentesis 30-40Kgs',
       '6100',
       ''
UNION ALL
SELECT '233',
       '05 SP with confine',
       'Cystotomy 1-5Kgs',
       '8400',
       ''
UNION ALL
SELECT '234',
       '05 SP with confine',
       'Cystotomy 5-10Kgs',
       '9000',
       ''
UNION ALL
SELECT '235',
       '05 SP with confine',
       'Cystotomy 10-15Kgs',
       '10400',
       ''
UNION ALL
SELECT '236',
       '05 SP with confine',
       'Cystotomy 16-20Kgs',
       '10900',
       ''
UNION ALL
SELECT '237',
       '05 SP with confine',
       'Cystotomy 20-30Kgs',
       '13400',
       ''
UNION ALL
SELECT '238',
       '05 SP with confine',
       'Cystotomy 30-40Kgs',
       '14900',
       ''
UNION ALL
SELECT '239',
       '05 SP with confine',
       'Eye Enucleation (1) 1-5Kgs',
       '6000',
       ''
UNION ALL
SELECT '240',
       '05 SP with confine',
       'Eye Enucleation (1) 5-10Kgs',
       '6600',
       ''
UNION ALL
SELECT '241',
       '05 SP with confine',
       'Eye Enucleation (1) 10-15Kgs',
       '7300',
       ''
UNION ALL
SELECT '242',
       '05 SP with confine',
       'Eye Enucleation (1) 16-20Kgs',
       '7800',
       ''
UNION ALL
SELECT '243',
       '05 SP with confine',
       'Eye Enucleation (1) 20-30Kgs',
       '8900',
       ''
UNION ALL
SELECT '244',
       '05 SP with confine',
       'Eye Enucleation (1) 30-40Kgs',
       '10400',
       ''
UNION ALL
SELECT '245',
       '05 SP with confine',
       'Eye Enucleation (2) 1-5Kgs',
       '8500',
       ''
UNION ALL
SELECT '246',
       '05 SP with confine',
       'Eye Enucleation (2) 5-10Kgs',
       '9100',
       ''
UNION ALL
SELECT '247',
       '05 SP with confine',
       'Eye Enucleation (2) 10-15Kgs',
       '9800',
       ''
UNION ALL
SELECT '248',
       '05 SP with confine',
       'Eye Enucleation (2) 16-20Kgs',
       '10300',
       ''
UNION ALL
SELECT '249',
       '05 SP with confine',
       'Eye Enucleation (2) 20-30Kgs',
       '11400',
       ''
UNION ALL
SELECT '250',
       '05 SP with confine',
       'Eye Enucleation (2) 30-40Kgs',
       '12900',
       ''
UNION ALL
SELECT '251',
       '05 SP with confine',
       'Foreign Body Removal 1-5Kgs',
       '8700',
       ''
UNION ALL
SELECT '252',
       '05 SP with confine',
       'Foreign Body Removal 5-10Kgs',
       '9300',
       ''
UNION ALL
SELECT '253',
       '05 SP with confine',
       'Foreign Body Removal 10-15Kgs',
       '10000',
       ''
UNION ALL
SELECT '254',
       '05 SP with confine',
       'Foreign Body Removal 16-20Kgs',
       '10500',
       ''
UNION ALL
SELECT '255',
       '05 SP with confine',
       'Foreign Body Removal 20-30Kgs',
       '11600',
       ''
UNION ALL
SELECT '256',
       '05 SP with confine',
       'Foreign Body Removal 30-40Kgs',
       '13100',
       ''
UNION ALL
SELECT '257',
       '05 SP with confine',
       'Gastrotomy 1-5Kgs',
       '8700',
       ''
UNION ALL
SELECT '258',
       '05 SP with confine',
       'Gastrotomy 5-10Kgs',
       '9300',
       ''
UNION ALL
SELECT '259',
       '05 SP with confine',
       'Gastrotomy 10-15Kgs',
       '10000',
       ''
UNION ALL
SELECT '260',
       '05 SP with confine',
       'Gastrotomy 16-20Kgs',
       '10500',
       ''
UNION ALL
SELECT '261',
       '05 SP with confine',
       'Gastrotomy 20-30Kgs',
       '11600',
       ''
UNION ALL
SELECT '262',
       '05 SP with confine',
       'Gastrotomy 30-40Kgs',
       '13100',
       ''
UNION ALL
SELECT '263',
       '05 SP with confine',
       'Hernia Repair 1-5Kgs',
       '4300',
       ''
UNION ALL
SELECT '264',
       '05 SP with confine',
       'Hernia Repair 5-10Kgs',
       '4700',
       ''
UNION ALL
SELECT '265',
       '05 SP with confine',
       'Hernia Repair 10-15Kgs',
       '5000',
       ''
UNION ALL
SELECT '266',
       '05 SP with confine',
       'Hernia Repair 16-20Kgs',
       '5300',
       ''
UNION ALL
SELECT '267',
       '05 SP with confine',
       'Hernia Repair 20-30Kgs',
       '6000',
       ''
UNION ALL
SELECT '268',
       '05 SP with confine',
       'Hernia Repair 30-40Kgs',
       '6900',
       ''
UNION ALL
SELECT '269',
       '05 SP with confine',
       'Ovario Hysterectomy Cat 1-5Kgs',
       '4800',
       ''
UNION ALL
SELECT '270',
       '05 SP with confine',
       'Ovario Hysterectomy Cat 5-10Kgs',
       '5100',
       ''
UNION ALL
SELECT '271',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 1-5Kgs',
       '5100',
       ''
UNION ALL
SELECT '272',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 5-10Kgs',
       '5400',
       ''
UNION ALL
SELECT '273',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 10-15Kgs',
       '5800',
       ''
UNION ALL
SELECT '274',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 16-20Kgs',
       '6100',
       ''
UNION ALL
SELECT '275',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 20-30Kgs',
       '6800',
       ''
UNION ALL
SELECT '276',
       '05 SP with confine',
       'Ovario Hysterectomy Dog 30-40Kgs',
       '7700',
       ''
UNION ALL
SELECT '277',
       '05 SP with confine',
       'Perineal Hernia 1-5Kgs',
       '7100',
       ''
UNION ALL
SELECT '278',
       '05 SP with confine',
       'Perineal Hernia 5-10Kgs',
       '7500',
       ''
UNION ALL
SELECT '279',
       '05 SP with confine',
       'Perineal Hernia 10-15Kgs',
       '7800',
       ''
UNION ALL
SELECT '280',
       '05 SP with confine',
       'Perineal Hernia 16-20Kgs',
       '8200',
       ''
UNION ALL
SELECT '281',
       '05 SP with confine',
       'Perineal Hernia 20-30Kgs',
       '8900',
       ''
UNION ALL
SELECT '282',
       '05 SP with confine',
       'Perineal Hernia 30-40Kgs',
       '9800',
       ''
UNION ALL
SELECT '283',
       '05 SP with confine',
       'Pyometra 1-5Kgs',
       '7100',
       ''
UNION ALL
SELECT '284',
       '05 SP with confine',
       'Pyometra 5-10Kgs',
       '7400',
       ''
UNION ALL
SELECT '285',
       '05 SP with confine',
       'Pyometra 10-15Kgs',
       '7800',
       ''
UNION ALL
SELECT '286',
       '05 SP with confine',
       'Pyometra 16-20Kgs',
       '8100',
       ''
UNION ALL
SELECT '287',
       '05 SP with confine',
       'Pyometra 20-30Kgs',
       '8600',
       ''
UNION ALL
SELECT '288',
       '05 SP with confine',
       'Pyometra 30-40Kgs',
       '9500',
       ''
UNION ALL
SELECT '289',
       '05 SP with confine',
       'Scrotal Ablation 1-5Kgs',
       '5700',
       ''
UNION ALL
SELECT '290',
       '05 SP with confine',
       'Scrotal Ablation 5-10Kgs',
       '6000',
       ''
UNION ALL
SELECT '291',
       '05 SP with confine',
       'Scrotal Ablation 10-15Kgs',
       '6300',
       ''
UNION ALL
SELECT '292',
       '05 SP with confine',
       'Scrotal Ablation 16-20Kgs',
       '6700',
       ''
UNION ALL
SELECT '293',
       '05 SP with confine',
       'Scrotal Ablation 20-30Kgs',
       '7400',
       ''
UNION ALL
SELECT '294',
       '05 SP with confine',
       'Scrotal Ablation 30-40Kgs',
       '8300',
       ''
UNION ALL
SELECT '295',
       '05 SP with confine',
       'Vaginal Prolapse Excision 1-5Kgs',
       '6700',
       ''
UNION ALL
SELECT '296',
       '05 SP with confine',
       'Vaginal Prolapse Excision 5-10Kgs',
       '7000',
       ''
UNION ALL
SELECT '297',
       '05 SP with confine',
       'Vaginal Prolapse Excision 10-15Kgs',
       '7400',
       ''
UNION ALL
SELECT '298',
       '05 SP with confine',
       'Vaginal Prolapse Excision 16-20Kgs',
       '7700',
       ''
UNION ALL
SELECT '299',
       '05 SP with confine',
       'Vaginal Prolapse Excision 20-30Kgs',
       '8400',
       ''
UNION ALL
SELECT '300',
       '05 SP with confine',
       'Vaginal Prolapse Excision 30-40Kgs',
       '9300',
       ''
UNION ALL
SELECT '301',
       '05 SP with confine',
       'Wound Laceration 1-5Kgs',
       '4000',
       ''
UNION ALL
SELECT '302',
       '05 SP with confine',
       'Wound Laceration 5-10Kgs',
       '4400',
       ''
UNION ALL
SELECT '303',
       '05 SP with confine',
       'Wound Laceration 10-15Kgs',
       '4700',
       ''
UNION ALL
SELECT '304',
       '05 SP with confine',
       'Wound Laceration 16-20Kgs',
       '5100',
       ''
UNION ALL
SELECT '305',
       '05 SP with confine',
       'Wound Laceration 20-30Kgs',
       '5800',
       ''
UNION ALL
SELECT '306',
       '05 SP with confine',
       'Wound Laceration 30-40Kgs',
       '6600',
       ''
UNION ALL
SELECT '307',
       '05 SP with confine',
       'Tumor Removal 1-5Kgs',
       '8000',
       ''
UNION ALL
SELECT '308',
       '05 SP with confine',
       'Tumor Removal 5-10Kgs',
       '8600',
       ''
UNION ALL
SELECT '309',
       '05 SP with confine',
       'Tumor Removal 10-15Kgs',
       '9300',
       ''
UNION ALL
SELECT '310',
       '05 SP with confine',
       'Tumor Removal 16-20Kgs',
       '9800',
       ''
UNION ALL
SELECT '311',
       '05 SP with confine',
       'Tumor Removal 20-30Kgs',
       '10900',
       ''
UNION ALL
SELECT '312',
       '05 SP with confine',
       'Tumor Removal 30-40Kgs',
       '12400',
       ''
UNION ALL
SELECT '313',
       '06 SP without confine',
       'Custom Description',
       '1',
       ''
UNION ALL
SELECT '314',
       '06 SP without confine',
       'Abdominocentesis 1-5Kgs',
       '2500',
       ''
UNION ALL
SELECT '315',
       '06 SP without confine',
       'Abdominocentesis 5-10Kgs',
       '3000',
       ''
UNION ALL
SELECT '316',
       '06 SP without confine',
       'Abdominocentesis 10-15Kgs',
       '3400',
       ''
UNION ALL
SELECT '317',
       '06 SP without confine',
       'Abdominocentesis 16-20Kgs',
       '3700',
       ''
UNION ALL
SELECT '318',
       '06 SP without confine',
       'Abdominocentesis 20-30Kgs',
       '4400',
       ''
UNION ALL
SELECT '319',
       '06 SP without confine',
       'Abdominocentesis 30-40Kgs',
       '5300',
       ''
UNION ALL
SELECT '320',
       '06 SP without confine',
       'Auricular Hematoma (1) 1-5Kgs',
       '4000',
       ''
UNION ALL
SELECT '321',
       '06 SP without confine',
       'Auricular Hematoma (1) 5-10Kgs',
       '4600',
       ''
UNION ALL
SELECT '322',
       '06 SP without confine',
       'Auricular Hematoma (1) 10-15Kgs',
       '5300',
       ''
UNION ALL
SELECT '323',
       '06 SP without confine',
       'Auricular Hematoma (1) 16-20Kgs',
       '5800',
       ''
UNION ALL
SELECT '324',
       '06 SP without confine',
       'Auricular Hematoma (1) 20-30Kgs',
       '6900',
       ''
UNION ALL
SELECT '325',
       '06 SP without confine',
       'Auricular Hematoma (1) 30-40Kgs',
       '8400',
       ''
UNION ALL
SELECT '326',
       '06 SP without confine',
       'Auricular Hematoma (2) 1-5Kgs',
       '5700',
       ''
UNION ALL
SELECT '327',
       '06 SP without confine',
       'Auricular Hematoma (2) 5-10Kgs',
       '6400',
       ''
UNION ALL
SELECT '328',
       '06 SP without confine',
       'Auricular Hematoma (2) 10-15Kgs',
       '7000',
       ''
UNION ALL
SELECT '329',
       '06 SP without confine',
       'Auricular Hematoma (2) 16-20Kgs',
       '7500',
       ''
UNION ALL
SELECT '330',
       '06 SP without confine',
       'Auricular Hematoma (2) 20-30Kgs',
       '8600',
       ''
UNION ALL
SELECT '331',
       '06 SP without confine',
       'Auricular Hematoma (2) 30-40Kgs',
       '10100',
       ''
UNION ALL
SELECT '332',
       '06 SP without confine',
       'Caesarian Delivery 1-5Kgs',
       '7300',
       ''
UNION ALL
SELECT '333',
       '06 SP without confine',
       'Caesarian Delivery 5-10Kgs',
       '8100',
       ''
UNION ALL
SELECT '334',
       '06 SP without confine',
       'Caesarian Delivery 10-15Kgs',
       '8900',
       ''
UNION ALL
SELECT '335',
       '06 SP without confine',
       'Caesarian Delivery 16-20Kgs',
       '9500',
       ''
UNION ALL
SELECT '336',
       '06 SP without confine',
       'Caesarian Delivery 20-30Kgs',
       '10800',
       ''
UNION ALL
SELECT '337',
       '06 SP without confine',
       'Caesarian Delivery 30-40Kgs',
       '12500',
       ''
UNION ALL
SELECT '338',
       '06 SP without confine',
       'Castration (Cats) 1-5Kgs',
       '2700',
       ''
UNION ALL
SELECT '339',
       '06 SP without confine',
       'Castration (Cats) 5-10Kgs',
       '3100',
       ''
UNION ALL
SELECT '340',
       '06 SP without confine',
       'Castration (Dogs) 1-5Kgs',
       '3200',
       ''
UNION ALL
SELECT '341',
       '06 SP without confine',
       'Castration (Dogs) 5-10Kgs',
       '3700',
       ''
UNION ALL
SELECT '342',
       '06 SP without confine',
       'Castration (Dogs) 10-15Kgs',
       '4100',
       ''
UNION ALL
SELECT '343',
       '06 SP without confine',
       'Castration (Dogs) 16-20Kgs',
       '4400',
       ''
UNION ALL
SELECT '344',
       '06 SP without confine',
       'Castration (Dogs) 20-30Kgs',
       '5100',
       ''
UNION ALL
SELECT '345',
       '06 SP without confine',
       'Castration (Dogs) 30-40Kgs',
       '6000',
       ''
UNION ALL
SELECT '346',
       '06 SP without confine',
       'Catherization (Dogs) 1-5Kgs',
       '3200',
       ''
UNION ALL
SELECT '347',
       '06 SP without confine',
       'Catherization (Dogs) 5-10Kgs',
       '3500',
       ''
UNION ALL
SELECT '348',
       '06 SP without confine',
       'Catherization (Dogs) 10-15Kgs',
       '4000',
       ''
UNION ALL
SELECT '349',
       '06 SP without confine',
       'Catherization (Dogs) 16-20Kgs',
       '4200',
       ''
UNION ALL
SELECT '350',
       '06 SP without confine',
       'Catherization (Dogs) 20-30Kgs',
       '4900',
       ''
UNION ALL
SELECT '351',
       '06 SP without confine',
       'Catherization (Dogs) 30-40Kgs',
       '5800',
       ''
UNION ALL
SELECT '352',
       '06 SP without confine',
       'Cherry Eye Removal (1) 1-5Kgs',
       '2900',
       ''
UNION ALL
SELECT '353',
       '06 SP without confine',
       'Cherry Eye Removal (1) 5-10Kgs',
       '3300',
       ''
UNION ALL
SELECT '354',
       '06 SP without confine',
       'Cherry Eye Removal (1) 10-15Kgs',
       '3600',
       ''
UNION ALL
SELECT '355',
       '06 SP without confine',
       'Cherry Eye Removal (1) 16-20Kgs',
       '3900',
       ''
UNION ALL
SELECT '356',
       '06 SP without confine',
       'Cherry Eye Removal (1) 20-30Kgs',
       '4600',
       ''
UNION ALL
SELECT '357',
       '06 SP without confine',
       'Cherry Eye Removal (1) 30-40Kgs',
       '5500',
       ''
UNION ALL
SELECT '358',
       '06 SP without confine',
       'Cherry Eye Removal (2) 1-5Kgs',
       '4000',
       ''
UNION ALL
SELECT '359',
       '06 SP without confine',
       'Cherry Eye Removal (2) 5-10Kgs',
       '4400',
       ''
UNION ALL
SELECT '360',
       '06 SP without confine',
       'Cherry Eye Removal (2) 10-15Kgs',
       '4700',
       ''
UNION ALL
SELECT '361',
       '06 SP without confine',
       'Cherry Eye Removal (2) 16-20Kgs',
       '5100',
       ''
UNION ALL
SELECT '362',
       '06 SP without confine',
       'Cherry Eye Removal (2) 20-30Kgs',
       '5700',
       ''
UNION ALL
SELECT '363',
       '06 SP without confine',
       'Cherry Eye Removal (2) 30-40Kgs',
       '6600',
       ''
UNION ALL
SELECT '364',
       '06 SP without confine',
       'Cystocentesis 1-5Kgs',
       '2300',
       ''
UNION ALL
SELECT '365',
       '06 SP without confine',
       'Cystocentesis 5-10Kgs',
       '2700',
       ''
UNION ALL
SELECT '366',
       '06 SP without confine',
       'Cystocentesis 10-15Kgs',
       '3200',
       ''
UNION ALL
SELECT '367',
       '06 SP without confine',
       'Cystocentesis 16-20Kgs',
       '3500',
       ''
UNION ALL
SELECT '368',
       '06 SP without confine',
       'Cystocentesis 20-30Kgs',
       '4200',
       ''
UNION ALL
SELECT '369',
       '06 SP without confine',
       'Cystocentesis 30-40Kgs',
       '5100',
       ''
UNION ALL
SELECT '370',
       '06 SP without confine',
       'Cystotomy 1-5Kgs',
       '7400',
       ''
UNION ALL
SELECT '371',
       '06 SP without confine',
       'Cystotomy 5-10Kgs',
       '8000',
       ''
UNION ALL
SELECT '372',
       '06 SP without confine',
       'Cystotomy 10-15Kgs',
       '9400',
       ''
UNION ALL
SELECT '373',
       '06 SP without confine',
       'Cystotomy 16-20Kgs',
       '9900',
       ''
UNION ALL
SELECT '374',
       '06 SP without confine',
       'Cystotomy 20-30Kgs',
       '12400',
       ''
UNION ALL
SELECT '375',
       '06 SP without confine',
       'Cystotomy 30-40Kgs',
       '13900',
       ''
UNION ALL
SELECT '376',
       '06 SP without confine',
       'Dental Prophylaxis 1-5Kgs',
       '2500',
       ''
UNION ALL
SELECT '377',
       '06 SP without confine',
       'Dental Prophylaxis 5-10Kgs',
       '2600',
       ''
UNION ALL
SELECT '378',
       '06 SP without confine',
       'Dental Prophylaxis 10-15Kgs',
       '2800',
       ''
UNION ALL
SELECT '379',
       '06 SP without confine',
       'Dental Prophylaxis 16-20Kgs',
       '2900',
       ''
UNION ALL
SELECT '380',
       '06 SP without confine',
       'Dental Prophylaxis 20-30Kgs',
       '3200',
       ''
UNION ALL
SELECT '381',
       '06 SP without confine',
       'Dental Prophylaxis 30-40Kgs',
       '3500',
       ''
UNION ALL
SELECT '382',
       '06 SP without confine',
       'Enema 1-5Kgs',
       '1500',
       ''
UNION ALL
SELECT '383',
       '06 SP without confine',
       'Enema 5-10Kgs',
       '1800',
       ''
UNION ALL
SELECT '384',
       '06 SP without confine',
       'Enema 10-15Kgs',
       '2100',
       ''
UNION ALL
SELECT '385',
       '06 SP without confine',
       'Enema 16-20Kgs',
       '2500',
       ''
UNION ALL
SELECT '386',
       '06 SP without confine',
       'Enema 20-30Kgs',
       '2800',
       ''
UNION ALL
SELECT '387',
       '06 SP without confine',
       'Enema 30-40Kgs',
       '3100',
       ''
UNION ALL
SELECT '388',
       '06 SP without confine',
       'Eye Enucleation (1) 1-5Kgs',
       '5000',
       ''
UNION ALL
SELECT '389',
       '06 SP without confine',
       'Eye Enucleation (1) 5-10Kgs',
       '5600',
       ''
UNION ALL
SELECT '390',
       '06 SP without confine',
       'Eye Enucleation (1) 10-15Kgs',
       '6300',
       ''
UNION ALL
SELECT '391',
       '06 SP without confine',
       'Eye Enucleation (1) 16-20Kgs',
       '6800',
       ''
UNION ALL
SELECT '392',
       '06 SP without confine',
       'Eye Enucleation (1) 20-30Kgs',
       '7900',
       ''
UNION ALL
SELECT '393',
       '06 SP without confine',
       'Eye Enucleation (1) 30-40Kgs',
       '9400',
       ''
UNION ALL
SELECT '394',
       '06 SP without confine',
       'Eye Enucleation (2) 1-5Kgs',
       '7500',
       ''
UNION ALL
SELECT '395',
       '06 SP without confine',
       'Eye Enucleation (2) 5-10Kgs',
       '8100',
       ''
UNION ALL
SELECT '396',
       '06 SP without confine',
       'Eye Enucleation (2) 10-15Kgs',
       '8800',
       ''
UNION ALL
SELECT '397',
       '06 SP without confine',
       'Eye Enucleation (2) 16-20Kgs',
       '9300',
       ''
UNION ALL
SELECT '398',
       '06 SP without confine',
       'Eye Enucleation (2) 20-30Kgs',
       '10400',
       ''
UNION ALL
SELECT '399',
       '06 SP without confine',
       'Eye Enucleation (2) 30-40Kgs',
       '11900',
       ''
UNION ALL
SELECT '400',
       '06 SP without confine',
       'Foreign Body Removal 1-5Kgs',
       '7700',
       ''
UNION ALL
SELECT '401',
       '06 SP without confine',
       'Foreign Body Removal 5-10Kgs',
       '8300',
       ''
UNION ALL
SELECT '402',
       '06 SP without confine',
       'Foreign Body Removal 10-15Kgs',
       '9000',
       ''
UNION ALL
SELECT '403',
       '06 SP without confine',
       'Foreign Body Removal 16-20Kgs',
       '9500',
       ''
UNION ALL
SELECT '404',
       '06 SP without confine',
       'Foreign Body Removal 20-30Kgs',
       '10600',
       ''
UNION ALL
SELECT '405',
       '06 SP without confine',
       'Foreign Body Removal 30-40Kgs',
       '12100',
       ''
UNION ALL
SELECT '406',
       '06 SP without confine',
       'Gastrotomy 1-5Kgs',
       '7700',
       ''
UNION ALL
SELECT '407',
       '06 SP without confine',
       'Gastrotomy 5-10Kgs',
       '8300',
       ''
UNION ALL
SELECT '408',
       '06 SP without confine',
       'Gastrotomy 10-15Kgs',
       '9000',
       ''
UNION ALL
SELECT '409',
       '06 SP without confine',
       'Gastrotomy 16-20Kgs',
       '9500',
       ''
UNION ALL
SELECT '410',
       '06 SP without confine',
       'Gastrotomy 20-30Kgs',
       '10600',
       ''
UNION ALL
SELECT '411',
       '06 SP without confine',
       'Gastrotomy 30-40Kgs',
       '12100',
       ''
UNION ALL
SELECT '412',
       '06 SP without confine',
       'Hernia Repair 1-5Kgs',
       '3300',
       ''
UNION ALL
SELECT '413',
       '06 SP without confine',
       'Hernia Repair 5-10Kgs',
       '3700',
       ''
UNION ALL
SELECT '414',
       '06 SP without confine',
       'Hernia Repair 10-15Kgs',
       '4000',
       ''
UNION ALL
SELECT '415',
       '06 SP without confine',
       'Hernia Repair 16-20Kgs',
       '4300',
       ''
UNION ALL
SELECT '416',
       '06 SP without confine',
       'Hernia Repair 20-30Kgs',
       '5000',
       ''
UNION ALL
SELECT '417',
       '06 SP without confine',
       'Hernia Repair 30-40Kgs',
       '5900',
       ''
UNION ALL
SELECT '418',
       '06 SP without confine',
       'Ovario Hysterectomy Cat 1-5Kgs',
       '3800',
       ''
UNION ALL
SELECT '419',
       '06 SP without confine',
       'Ovario Hysterectomy Cat 5-10Kgs',
       '4100',
       ''
UNION ALL
SELECT '420',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 1-5Kgs',
       '4100',
       ''
UNION ALL
SELECT '421',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 5-10Kgs',
       '4400',
       ''
UNION ALL
SELECT '422',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 10-15Kgs',
       '4800',
       ''
UNION ALL
SELECT '423',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 16-20Kgs',
       '5100',
       ''
UNION ALL
SELECT '424',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 20-30Kgs',
       '5800',
       ''
UNION ALL
SELECT '425',
       '06 SP without confine',
       'Ovario Hysterectomy Dog 30-40Kgs',
       '6700',
       ''
UNION ALL
SELECT '426',
       '06 SP without confine',
       'Perineal Hernia 1-5Kgs',
       '6100',
       ''
UNION ALL
SELECT '427',
       '06 SP without confine',
       'Perineal Hernia 5-10Kgs',
       '6500',
       ''
UNION ALL
SELECT '428',
       '06 SP without confine',
       'Perineal Hernia 10-15Kgs',
       '6800',
       ''
UNION ALL
SELECT '429',
       '06 SP without confine',
       'Perineal Hernia 16-20Kgs',
       '7200',
       ''
UNION ALL
SELECT '430',
       '06 SP without confine',
       'Perineal Hernia 20-30Kgs',
       '7900',
       ''
UNION ALL
SELECT '431',
       '06 SP without confine',
       'Perineal Hernia 30-40Kgs',
       '8800',
       ''
UNION ALL
SELECT '432',
       '06 SP without confine',
       'Pyometra 1-5Kgs',
       '6100',
       ''
UNION ALL
SELECT '433',
       '06 SP without confine',
       'Pyometra 5-10Kgs',
       '6400',
       ''
UNION ALL
SELECT '434',
       '06 SP without confine',
       'Pyometra 10-15Kgs',
       '6800',
       ''
UNION ALL
SELECT '435',
       '06 SP without confine',
       'Pyometra 16-20Kgs',
       '7100',
       ''
UNION ALL
SELECT '436',
       '06 SP without confine',
       'Pyometra 20-30Kgs',
       '7600',
       ''
UNION ALL
SELECT '437',
       '06 SP without confine',
       'Pyometra 30-40Kgs',
       '8500',
       ''
UNION ALL
SELECT '438',
       '06 SP without confine',
       'Rectal Prolapse 1-5Kgs',
       '2500',
       ''
UNION ALL
SELECT '439',
       '06 SP without confine',
       'Rectal Prolapse 5-10Kgs',
       '2800',
       ''
UNION ALL
SELECT '440',
       '06 SP without confine',
       'Rectal Prolapse 10-15Kgs',
       '3300',
       ''
UNION ALL
SELECT '441',
       '06 SP without confine',
       'Rectal Prolapse 16-20Kgs',
       '3600',
       ''
UNION ALL
SELECT '442',
       '06 SP without confine',
       'Rectal Prolapse 20-30Kgs',
       '4300',
       ''
UNION ALL
SELECT '443',
       '06 SP without confine',
       'Rectal Prolapse 30-40Kgs',
       '5200',
       ''
UNION ALL
SELECT '444',
       '06 SP without confine',
       'Scrotal Ablation 1-5Kgs',
       '4700',
       ''
UNION ALL
SELECT '445',
       '06 SP without confine',
       'Scrotal Ablation 5-10Kgs',
       '5000',
       ''
UNION ALL
SELECT '446',
       '06 SP without confine',
       'Scrotal Ablation 10-15Kgs',
       '5300',
       ''
UNION ALL
SELECT '447',
       '06 SP without confine',
       'Scrotal Ablation 16-20Kgs',
       '5700',
       ''
UNION ALL
SELECT '448',
       '06 SP without confine',
       'Scrotal Ablation 20-30Kgs',
       '6400',
       ''
UNION ALL
SELECT '449',
       '06 SP without confine',
       'Scrotal Ablation 30-40Kgs',
       '7300',
       ''
UNION ALL
SELECT '450',
       '06 SP without confine',
       'Tarsorrhaphy (1) 1-5Kgs',
       '2500',
       ''
UNION ALL
SELECT '451',
       '06 SP without confine',
       'Tarsorrhaphy (1) 5-10Kgs',
       '3000',
       ''
UNION ALL
SELECT '452',
       '06 SP without confine',
       'Tarsorrhaphy (1) 10-15Kgs',
       '3400',
       ''
UNION ALL
SELECT '453',
       '06 SP without confine',
       'Tarsorrhaphy (1) 16-20Kgs',
       '3800',
       ''
UNION ALL
SELECT '454',
       '06 SP without confine',
       'Tarsorrhaphy (1) 20-30Kgs',
       '4400',
       ''
UNION ALL
SELECT '455',
       '06 SP without confine',
       'Tarsorrhaphy (1) 30-40Kgs',
       '5300',
       ''
UNION ALL
SELECT '456',
       '06 SP without confine',
       'Tarsorrhaphy (2) 1-5Kgs',
       '3000',
       ''
UNION ALL
SELECT '457',
       '06 SP without confine',
       'Tarsorrhaphy (2) 5-10Kgs',
       '3500',
       ''
UNION ALL
SELECT '458',
       '06 SP without confine',
       'Tarsorrhaphy (2) 10-15Kgs',
       '3900',
       ''
UNION ALL
SELECT '459',
       '06 SP without confine',
       'Tarsorrhaphy (2) 16-20Kgs',
       '4300',
       ''
UNION ALL
SELECT '460',
       '06 SP without confine',
       'Tarsorrhaphy (2) 20-30Kgs',
       '4900',
       ''
UNION ALL
SELECT '461',
       '06 SP without confine',
       'Tarsorrhaphy (2) 30-40Kgs',
       '5800',
       ''
UNION ALL
SELECT '462',
       '06 SP without confine',
       'Tooth Extraction (1)',
       '100',
       ''
UNION ALL
SELECT '463',
       '06 SP without confine',
       'Vaginal Prolapse Excision 1-5Kgs',
       '5700',
       ''
UNION ALL
SELECT '464',
       '06 SP without confine',
       'Vaginal Prolapse Excision 5-10Kgs',
       '6000',
       ''
UNION ALL
SELECT '465',
       '06 SP without confine',
       'Vaginal Prolapse Excision 10-15Kgs',
       '6400',
       ''
UNION ALL
SELECT '466',
       '06 SP without confine',
       'Vaginal Prolapse Excision 16-20Kgs',
       '6700',
       ''
UNION ALL
SELECT '467',
       '06 SP without confine',
       'Vaginal Prolapse Excision 20-30Kgs',
       '7400',
       ''
UNION ALL
SELECT '468',
       '06 SP without confine',
       'Vaginal Prolapse Excision 30-40Kgs',
       '8300',
       ''
UNION ALL
SELECT '469',
       '06 SP without confine',
       'Whelping 1-5Kgs',
       '3300',
       ''
UNION ALL
SELECT '470',
       '06 SP without confine',
       'Whelping 5-10Kgs',
       '3500',
       ''
UNION ALL
SELECT '471',
       '06 SP without confine',
       'Whelping 10-15Kgs',
       '3700',
       ''
UNION ALL
SELECT '472',
       '06 SP without confine',
       'Whelping 16-20Kgs',
       '3900',
       ''
UNION ALL
SELECT '473',
       '06 SP without confine',
       'Whelping 20-30Kgs',
       '4100',
       ''
UNION ALL
SELECT '474',
       '06 SP without confine',
       'Whelping 30-40Kgs',
       '4300',
       ''
UNION ALL
SELECT '475',
       '06 SP without confine',
       'Whelping Per Puppy',
       '100',
       ''
UNION ALL
SELECT '476',
       '06 SP without confine',
       'Wound Laceration 1-5Kgs',
       '3000',
       ''
UNION ALL
SELECT '477',
       '06 SP without confine',
       'Wound Laceration 5-10Kgs',
       '3400',
       ''
UNION ALL
SELECT '478',
       '06 SP without confine',
       'Wound Laceration 10-15Kgs',
       '3700',
       ''
UNION ALL
SELECT '479',
       '06 SP without confine',
       'Wound Laceration 16-20Kgs',
       '4100',
       ''
UNION ALL
SELECT '480',
       '06 SP without confine',
       'Wound Laceration 20-30Kgs',
       '4800',
       ''
UNION ALL
SELECT '481',
       '06 SP without confine',
       'Wound Laceration 30-40Kgs',
       '5600',
       ''
UNION ALL
SELECT '482',
       '06 SP without confine',
       'Wound Suture 1-5Kgs',
       '1500',
       ''
UNION ALL
SELECT '483',
       '06 SP without confine',
       'Wound Suture 5-10Kgs',
       '2000',
       ''
UNION ALL
SELECT '484',
       '06 SP without confine',
       'Wound Suture 10-15Kgs',
       '2400',
       ''
UNION ALL
SELECT '485',
       '06 SP without confine',
       'Wound Suture 16-20Kgs',
       '2700',
       ''
UNION ALL
SELECT '486',
       '06 SP without confine',
       'Wound Suture 20-30Kgs',
       '3400',
       ''
UNION ALL
SELECT '487',
       '06 SP without confine',
       'Wound Suture 30-40Kgs',
       '4300',
       ''
UNION ALL
SELECT '488',
       '06 SP without confine',
       'Tumor Removal 1-5Kgs',
       '7000',
       ''
UNION ALL
SELECT '489',
       '06 SP without confine',
       'Tumor Removal 5-10Kgs',
       '7600',
       ''
UNION ALL
SELECT '490',
       '06 SP without confine',
       'Tumor Removal 10-15Kgs',
       '8300',
       ''
UNION ALL
SELECT '491',
       '06 SP without confine',
       'Tumor Removal 16-20Kgs',
       '8800',
       ''
UNION ALL
SELECT '492',
       '06 SP without confine',
       'Tumor Removal 20-30Kgs',
       '9900',
       ''
UNION ALL
SELECT '493',
       '06 SP without confine',
       'Tumor Removal 30-40Kgs',
       '11400',
       ''
UNION ALL
SELECT '494',
       '07 Confinement & Lodging',
       'Custom Lodging',
       '1',
       ''
UNION ALL
SELECT '495',
       '07 Confinement & Lodging',
       'Custom Confinement',
       '1',
       ''
UNION ALL
SELECT '496',
       '07 Confinement & Lodging',
       'Hourly Lodging 1-5Kgs',
       '40',
       ''
UNION ALL
SELECT '497',
       '07 Confinement & Lodging',
       'Hourly Lodging 5-10Kgs',
       '50',
       ''
UNION ALL
SELECT '498',
       '07 Confinement & Lodging',
       'Hourly Lodging 10-15Kgs',
       '60',
       ''
UNION ALL
SELECT '499',
       '07 Confinement & Lodging',
       'Hourly Lodging 15-20Kgs',
       '70',
       ''
UNION ALL
SELECT '500',
       '07 Confinement & Lodging',
       'Hourly Lodging 20-30Kgs',
       '80',
       ''
UNION ALL
SELECT '501',
       '07 Confinement & Lodging',
       'Hourly Lodging 30-40Kgs',
       '90',
       ''
UNION ALL
SELECT '502',
       '07 Confinement & Lodging',
       'Daily Lodging 1-5Kgs',
       '300',
       ''
UNION ALL
SELECT '503',
       '07 Confinement & Lodging',
       'Daily Lodging 5-10Kgs',
       '400',
       ''
UNION ALL
SELECT '504',
       '07 Confinement & Lodging',
       'Daily Lodging 10-15Kgs',
       '450',
       ''
UNION ALL
SELECT '505',
       '07 Confinement & Lodging',
       'Daily Lodging 15-20Kgs',
       '500',
       ''
UNION ALL
SELECT '506',
       '07 Confinement & Lodging',
       'Daily Lodging 20-30Kgs',
       '600',
       ''
UNION ALL
SELECT '507',
       '07 Confinement & Lodging',
       'Daily Lodging 30-40Kgs',
       '700',
       ''
UNION ALL
SELECT '508',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 1-5Kgs',
       '2500',
       ''
UNION ALL
SELECT '509',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 5-10Kgs',
       '2700',
       ''
UNION ALL
SELECT '510',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 10-15Kgs',
       '2900',
       ''
UNION ALL
SELECT '511',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 15-20Kgs',
       '3100',
       ''
UNION ALL
SELECT '512',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 20-30Kgs',
       '3400',
       ''
UNION ALL
SELECT '513',
       '07 Confinement & Lodging',
       'Confinement Package 1st Day 30-40Kgs',
       '4100',
       ''
UNION ALL
SELECT '514',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 1-5Kgs',
       '1200',
       ''
UNION ALL
SELECT '515',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 5-10Kgs',
       '1400',
       ''
UNION ALL
SELECT '516',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 10-15Kgs',
       '1600',
       ''
UNION ALL
SELECT '517',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 15-20Kgs',
       '1800',
       ''
UNION ALL
SELECT '518',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 20-30Kgs',
       '2100',
       ''
UNION ALL
SELECT '519',
       '07 Confinement & Lodging',
       'Confinement Package Succeeding Day 30-40Kgs',
       '2800',
       ''

GO 
