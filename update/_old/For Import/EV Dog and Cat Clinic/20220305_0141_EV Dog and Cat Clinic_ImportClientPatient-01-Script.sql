DEclare @GUID_Company VARCHAR(MAX) = '4A3DE256-549A-4A02-9818-CEC4E31A6043'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @PreImport TABLE
  (
     ID_PreImport       INT,
     ID_Patient         INT,
     ID_Client          INT,
     Name_Client        VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     CustomCode_Patient VARCHAR(MAX),
     PractoPatient      VARCHAR(MAX),
     ContactNumber      VARCHAR(MAX),
     ContactNumber2     VARCHAR(MAX),
     EmailAddress       VARCHAR(MAX),
     ID_Gender          INT,
     Address_Client     VARCHAR(MAX),
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )
DECLARE @Import TABLE
  (
     ID_PreImport       INT,
     ID_Patient         INT,
     ID_Client          INT,
     Name_Client        VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     CustomCode_Patient VARCHAR(MAX),
     ContactNumber      VARCHAR(MAX),
     ContactNumber2     VARCHAR(MAX),
     EmailAddress       VARCHAR(MAX),
     ID_Gender          INT,
     Address_Client     VARCHAR(MAX),
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )

INSERT @PreImport
       (ID_PreImport,
        CustomCode_Patient,
        PractoPatient,
        ContactNumber,
        ContactNumber2,
        EmailAddress,
        ID_Gender,
        Address_Client,
        DateBirth,
        Species,
        Comment)
SELECT ID_PreImport,
       [Patient Number],
       [Patient Name],
       [Mobile Number],
       [Contact Number]
       + CASE
           WHEN LEN([Secondary Mobile]) > 0 THEN ' ' + [Secondary Mobile]
           ELSE ''
         END,
       [Email Address],
       CASE
         WHEN [Gender] = 'M' THEN 1
         ELSE
           CASE
             WHEN [Gender] = 'F' THEN 2
             ELSE NULL
           END
       END,
       [Address]
       + CASE
           WHEN LEN([Locality]) > 0 THEN ' '
           ELSE ''
         END
       + [Locality]
       + CASE
           WHEN LEN([City]) > 0 THEN ' '
           ELSE ''
         END
       + [City],
       CASE
         WHEN LEN([Date of Birth]) > 0 THEN TRY_CONVERT(DateTime, [Date of Birth])
         ELSE NULL
       END,
       [Groups],
       + CASE
           WHEN LEN([Remarks]) > 0 THEN 'Remarks' + CHAR(13) + CHAR(10) + ' ' + [Remarks]
           ELSE ''
         END
       +
       + CASE
           WHEN LEN([Medical History]) > 0 THEN CHAR(13) + CHAR(10) + 'Medical History'
                                                + CHAR(13) + CHAR(10) + ' ' + [Medical History]
           ELSE ''
         END
       + CASE
           WHEN LEN([Patient Notes]) > 0 THEN CHAR(13) + CHAR(10) + 'Patient Notes' + CHAR(13)
                                              + CHAR(10) + ' ' + [Patient Notes]
           ELSE ''
         END
FROM   (SELECT ID_PreImport,
               dbo.fGetRemovedBegEndSQuatationString([Patient Number])                                        [Patient Number],
               REPLACE(REPLACE(dbo.fGetRemovedBegEndSQuatationString([Patient Name]), '-', ' - '), '  ', ' ') [Patient Name],
               dbo.fGetRemovedBegEndSQuatationString([Mobile Number])                                         [Mobile Number],
               dbo.fGetRemovedBegEndSQuatationString([Contact Number])                                        [Contact Number],
               dbo.fGetRemovedBegEndSQuatationString([Email Address])                                         [Email Address],
               dbo.fGetRemovedBegEndSQuatationString([Secondary Mobile])                                      [Secondary Mobile],
               dbo.fGetRemovedBegEndSQuatationString([Gender])                                                [Gender],
               dbo.fGetRemovedBegEndSQuatationString([Address])                                               [Address],
               dbo.fGetRemovedBegEndSQuatationString([Locality])                                              [Locality],
               dbo.fGetRemovedBegEndSQuatationString([City])                                                  [City],
               dbo.fGetRemovedBegEndSQuatationString([National Id])                                           [National Id],
               dbo.fGetRemovedBegEndSQuatationString([Date of Birth])                                         [Date of Birth],
               dbo.fGetRemovedBegEndSQuatationString([Remarks])                                               [Remarks],
               dbo.fGetRemovedBegEndSQuatationString([Medical History])                                       [Medical History],
               dbo.fGetRemovedBegEndSQuatationString([Groups])                                                [Groups],
               dbo.fGetRemovedBegEndSQuatationString([Patient Notes])                                         [Patient Notes]
        FROM   [dbo].EVDC_ClientPatients) tbl

Update @PreImport
SET    Name_Client = SUBSTRING (PractoPatient, 0, CHARINDEX('-', PractoPatient))

Update @PreImport
SET    Name_Client = LTRIM(RTRIM(dbo.fGetCleanedString(Name_Client)))

Update @PreImport
SET    Name_Patient = SUBSTRING(PractoPatient, CHARINDEX('-', PractoPatient) + 1, LEN(PractoPatient))

Update @PreImport
SET    Name_Patient = LTRIM(RTRIM(dbo.fGetCleanedString(Name_Patient)))

SELECT 'No Client'

SELECT *
FROM   @PreImport
WHERE  LEN(Name_Client) = 0
       AND LEN(Name_Patient) > 0
Order  By LEN(Name_Client),
          LEN(Name_Patient)

SELECT 'Same Value Client - Patient'

SELECT *
FROM   @PreImport
WHERE  PractoPatient = Name_Client + ' - ' + Name_Patient
       and Name_Client = Name_Patient

SELECT 'Client - Patient'

INSERT @Import
       (ID_PreImport,
        ID_Patient,
        ID_Client,
        Name_Client,
        Name_Patient,
        CustomCode_Patient,
        ContactNumber,
        ContactNumber2,
        EmailAddress,
        ID_Gender,
        Address_Client,
        DateBirth,
        Species,
        Comment)
SELECT ID_PreImport,
       ID_Patient,
       ID_Client,
       Name_Client,
       Name_Patient,
       CustomCode_Patient,
       ContactNumber,
       ContactNumber2,
       EmailAddress,
       ID_Gender,
       Address_Client,
       DateBirth,
       Species,
       Comment
FROM   @PreImport
WHERE  LEN(Name_Client) > LEN(Name_Patient)
       AND LEN(Name_Patient) > 0
       AND LEN(Name_Client) > 0
Order  By LEN(Name_Client),
          LEN(Name_Patient)

SELECT *
FROM   @Import

----------------------------------------------------------------------------------------
SELECT 'Patient - Client'

SELECT *
FROM   @PreImport
WHERE  LEN(Name_Client) < LEN(Name_Patient)
       AND LEN(Name_Patient) > 0
       AND LEN(Name_Client) > 0
Order  By LEN(Name_Client),
          LEN(Name_Patient)

		  
SELECT 'Able Import'
SELECT *
FROM   [EVDC_ClientPatients] WHERE ID_PreImport  IN (SELECT ID_PreImport FROM @Import)

SELECT 'Not Able to Import'
SELECT *
FROM   [EVDC_ClientPatients] WHERE ID_PreImport NOT IN (SELECT ID_PreImport FROM @Import)


DROP TABLE IF EXISTS ##PreClient
CREATE TABLE ##PreClient
  (
     Name_Client    VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX),
     EmailAddress   VARCHAR(MAX),
     Address_Client VARCHAR(MAX)
  )
INSERT ##PreClient
       (Name_Client,
        ContactNumber,
        ContactNumber2,
        EmailAddress,
        Address_Client)
Select DISTINCT Name_Client,
                STUFF ((SELECT DISTINCT '/' + ISNULL(ContactNumber, '')
                        FROM   @Import t2
                        WHERE  t2.Name_Client = t1.Name_Client
                        FOR XML PATH('')), 1, 1, '') ContactNumber,
                STUFF ((SELECT DISTINCT '/' + ISNULL(ContactNumber2, '')
                        FROM   @Import t2
                        WHERE  t2.Name_Client = t1.Name_Client
                        FOR XML PATH('')), 1, 1, '') ContactNumber2,
                STUFF ((SELECT DISTINCT '/' + ISNULL(Address_Client, '')
                        FROM   @Import t2
                        WHERE  t2.Name_Client = t1.Name_Client
                        FOR XML PATH('')), 1, 1, '') Address_Client,
                STUFF ((SELECT DISTINCT '/' + ISNULL(EmailAddress, '')
                        FROM   @Import t2
                        WHERE  t2.Name_Client = t1.Name_Client
                        FOR XML PATH('')), 1, 1, '') EmailAddress
FROm   @Import t1
WHERE  LEN(Name_Client) > 0

DROP TABLE IF EXISTS ##PrePatient
CREATE TABLE ##PrePatient
  (
     Name_Client        VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     CustomCode_Patient VARCHAR(MAX),
     ID_Gender          INT,
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )
INSERT ##PrePatient
       (Name_Client,
        Name_Patient,
        CustomCode_Patient,
        ID_Gender,
        DateBirth,
        Species,
        Comment)
SELECT DISTINCT _client.Name_Client,
                Name_Patient,
                STUFF ((SELECT DISTINCT '/' + ISNULL(CustomCode_Patient, '')
                        FROM   @Import t2
                        WHERE  t2.Name_Client = import.Name_Client
                               AND t2.Name_Patient = import.Name_Patient
                        FOR XML PATH('')), 1, 1, ''),
                MAX(ID_Gender),
                MAX(DateBirth),
                MAX(Species),
                STUFF ((SELECT DISTINCT + CHAR(13) + CHAR(10) + ISNULL(Comment, '')
                                         + CHAR(13) + CHAR(10)
                        FROM   @Import t2
                        WHERE  t2.Name_Client = import.Name_Client
                               AND t2.Name_Patient = import.Name_Patient
                        FOR XML PATH('')), 1, 1, '')
FROM   ##PreClient _client
       inner join @Import import
               on _client.Name_Client = import.Name_Client
GROUP  BY _client.Name_Client,
          import.Name_Client,
          Name_Patient



		  
