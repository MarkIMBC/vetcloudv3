ALTER TABLE tClient
  ALTER COLUMN Address varchar(8000);

DEclare @GUID_Company VARCHAR(MAX) = '4A3DE256-549A-4A02-9818-CEC4E31A6043'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company


Update ##PreClient
SET    ContactNumber = REPLACE(ContactNumber, '+639', '09')

Update ##PreClient
SET    ContactNumber2 = REPLACE(ContactNumber2, '+639', '09')

Update ##PrePatient
SET    CustomCode_Patient = 'P' + CustomCode_Patient
where  CustomCode_Patient LIKE 'ET%'

Update ##PrePatient
SET    Comment = '&' + Comment
where  Comment LIKE '#x0D;%'

Update ##PrePatient
SET    Comment = LTRIM(RTRIM(REPLACE(Comment, '&#x0D;', CHAR(13))))

DECLARE @Import TABLE
  (
     ID_Client          INT,
     ID_Patient         INT,
     Name_Client        VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     CustomCode_Patient VARCHAR(MAX),
     ContactNumber      VARCHAR(MAX),
     ContactNumber2     VARCHAR(MAX),
     EmailAddress       VARCHAR(MAX),
     ID_Gender          INT,
     Address_Client     VARCHAR(MAX),
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )

INSERT @Import
       (Name_Client,
        ContactNumber,
        ContactNumber2,
        Address_Client,
        EmailAddress,
        Name_Patient,
        CustomCode_Patient,
        ID_Gender,
        DateBirth,
        Species,
        Comment)
SELECT client.Name_Client,
       client.ContactNumber,
       client.ContactNumber2,
       client.EmailAddress,
       client.Address_Client,
       patient.Name_Patient,
       patient.CustomCode_Patient,
       patient.ID_Gender,
       patient.DateBirth,
       patient.Species,
       patient.Comment
FROM   ##PreClient client
       inner join ##PrePatient patient
               on patient.Name_Client = client.Name_Client

Update @Import
SET    ID_Client = client.ID
FROm   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company;

Update @Import
SET    ID_Patient = patient.ID
FROm   @Import import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

INSERT dbo.tClient
       (ID_Company,
        Name,
        Address,
        ContactNumber,
        ContactNumber2,
        Email,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Client,
                Address_Client,
                ContactNumber,
                ContactNumber2,
                EmailAddress,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @Import  WHERE ID_Client IS NULL 

Update @Import
SET    ID_Client = client.ID
FROm   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company;

INSERT INTO [dbo].[tPatient]
            ([ID_Company],
             [ID_Client],
             [Name],
             [CustomCode],
             [DateBirth],
             [Comment],
             [ID_Gender],
             [Species],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT @ID_Company,
       ID_Client,
       Name_Patient,
       CustomCode_Patient,
       DateBirth,
       Comment,
       ID_Gender,
       Species,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   @Import  WHERE ID_Client IS NOT NULL AND ID_Patient IS NULL

Update @Import
SET    ID_Patient = patient.ID
FROm   @Import import
       INNER JOIN tPatient patient
               on patient.Name = import.Name_Patient
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

SELECT * FROM @Import


SELECT Name, Count(*) FROM tClient WHERE ID_Company = @ID_Company 
GROUP BY Name
HAVING COUNT(*) >1