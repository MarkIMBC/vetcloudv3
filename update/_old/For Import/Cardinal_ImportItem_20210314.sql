/****** Object:  Table [dbo].[Cardinal_ImportTemplate_20210314]    Script Date: 3/14/2021 10:58:44 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cardinal_ImportTemplate_20210314]') AND type in (N'U'))
DROP TABLE [dbo].[Cardinal_ImportTemplate_20210314]
GO

CREATE TABLE [dbo].[Cardinal_ImportTemplate_20210314](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [nvarchar](255) NULL,
	[Selling Price] [nvarchar](255) NULL,
	[Current Inventory Count] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advantix For Dogs Large', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advantix For Dogs Medium', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advantix For Dogs Small', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advantix For Dogs X Large', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate For Dogs Large', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate For Dogs Med', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate For Dogs Mini Small', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate For Small Cats', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Advocate For Very Large Dogs', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Amilyte C', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Anti Diarrhea', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Anupco Ancomoxin 150 La', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Appeboost Syr 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Antimange Spray Dogs/Cats', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Antimicrobial Wound Spray', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Healthy Liver', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Healthy Vision', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Heartworm Remedy', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Immune Health', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As K9 Ear Drops', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'As Liquid Wormer', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Asan Easy Test Ecanis', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Atrosite', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Baciflora', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Beloxicam Susp 15ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Biocan 5in1', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bio-Genta Drops 10ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bionote Ecanis Ab Test', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto Large', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto Medium', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Bravecto Small', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob Distemper 6ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canglob Parvo 6ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Canisep 30g', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Caniverm Adult 10kg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cardinal Syr 1ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cardinal Syr 3ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Carpropain', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cathy G24 Iv Cannula', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cathy G26 Iv Cannula', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cbg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cerenia 20ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cflex 250mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clindagold', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clovimed 250', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Clovimed 400', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Co-Amoxisaph-125', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Coforta', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Complivit 150g', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Convenia Inj', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Corsin 300', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Cover Glass 22x22', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dawnsail Cpv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dawnsail Ccv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dexat', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dexavet 500', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dextrolyte Reco Pack 5gx7', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doggy Doggy Syrup 120 Ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxycycline Inj 100mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxypet 100', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxypet 200', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxyvet 10 Syrup', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Doxyvet 10 Tab', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Dr Cs Nat Pet Soap', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Drontal Sus For Puppies', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ear Wash Naturevet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Emerplex 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Emervit 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Endclav 625', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Epibbas', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Euromet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fatrocortin', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fel O Cell Vaccine', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline >10kg <20kg Med Dogs', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline >20kg <40kg Large Dogs', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline >8 Wks <10kg Small Dogs', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Frontline Plus >8 Wks <10kg Cats', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Gastroaid', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Gembifer 30ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Gentin Drops', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Growth Advance 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hexacare Underpads', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Hipra 5l3', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Syr', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Immunol Tab', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Indoplas', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Indoplas F Mask', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ivomec 100ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ivomec 50ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Kawu', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ketoconazole 200mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv 52 Tab Forte Ds', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liv52 Drops', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Live Free Underpads', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Liverolin', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Luvmox Dsf', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Lymedox', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Marbocyl P 5mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Marbocyl P 80mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mc Nitrile Gloves L', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Mc Nitrile Gloves M', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Meto', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metrex', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Metrogen', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Moxylor', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Myrevit B', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nefrotec Ds', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac 5l4', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nobivac Kc', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nutripet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nv Brewers Yeast 300', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Nv Ear Cleanse 4oz', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Opticare 7ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Oridermyl 10mg', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ormet Microset', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ornipural', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Otiderm', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Partners Gauze Pad 4x4 Non Sterile', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Defense 100gms', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Nutridrops 15ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Tabs 180''s', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pet Tabs 60''s', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Petsure 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Phylumox', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pneumodog', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Povidone 120 Ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Promax Medium', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Promax Small', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Protect Plus Gold', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Purevax', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Quantum 6in1', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rafonex 625', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ranirex', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Ranitein', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rapigen Cdv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Recombitek 5in1', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Remend', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Scourvet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sorvit Syr 120ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Suregetech Glass Slide Plain', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sureguard Swab', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sureguard Syringe 1cc', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Surgical Mask', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Surgitech Blades #20', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sylvite', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Teodor Silk 2/0 Cutting', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Terumo', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Terumo Needle G25x5/8" ', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Terumo Syringe 1ml', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Thromb Beat Syr 100', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tolfedine 60mg 16oz', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Trance', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tricin Eye & Ear Ointment', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tudor 2/0', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tudor 2/0 Cutting', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Tudor 3/0 Cutting', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Uni Shampoo I', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Unimoxicol', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Unigard', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Urovet', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vantage', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Cdv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Chw Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Cpv+Ccv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Cpv+Cdistemp Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Ehr+Ana+Bab Ab', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vet Dx Fiv+Felv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetaid Combo Test Kit Bea Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetaid Combo Test Kit Cpv+Ccv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vetaid Ehrlichia Test Kit', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Wonderfuri Gold', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Worm Protek Plus', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Wormguard Tab', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Xylazine', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Zoletil', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Zr Bio Fiv+Felv Ag', N'Drug/Medicine', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Kitten Food Chicken  1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Salmon', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Hairball  Salmon 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Urinary 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Choosy 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Salmon  15kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Cat Food Chicken 1.5kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food Mini Small', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Chicken 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food Mini Small  Salmon 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food Med Large  Lamb&Rice 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food Med Large', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Salmon 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food Med Large', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Salmon 15kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food High Energy 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Adult Dog Food High Energy 15kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Junior Dog Food Mini Small Chicken 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Junior Dog Food Med Large Lamb&Rice 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Rp Senior Dog Food Med Large Lamb&Rice 3kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Fcn Urinary Care 2 Kg', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Baby Dog Milk 400g', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Puppy Pro Tech Dog 300g 16c', N'Pet Food', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Gastrointestinal 400gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Hepatic Dog Can 420gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Hepatic Canine 1.5kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Hypoallergenic C9 400gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Hypoallergenic Feline 400g', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Hypo Spec Small Dog 1kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Recovery Fel/Can 195gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Renal Dog Can 410gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Renal Canine 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Urinary Canine 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Skin Care Adult Dog 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Skin Care Sd Adult Sd 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Skin Care Jr Small Dog 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Urinary Dog Can 410g X 12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sh Mini Junior (Puppy) 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Sh Medium Junior (Puppy) 1kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Shn Mini Starter M&B 1kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vdiet Satiety Dog 6kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vdiet Satiety Dog 1.5kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Shn Mini Adult 2kg', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Urinary S/O Feline 100gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Vd Urinary S/O Feline 85gx12', N'Others', NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Cardinal_ImportTemplate_20210314] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, NULL, NULL, NULL, NULL, NULL)
GO

DECLARE @Cardinal_ID_Company INT = 14

Declare @Record Table (ItemName Varchar(MAX), ID_ItemCategory INT)

INSERT @Record
SELECT  LTRIM(RTRIM(import.Name)), cat.ID
FROM    [Cardinal_ImportTemplate_20210314] import 
	LEFT JOIN tItemCategory  cat 
		ON TRIM(LOWER(cat.Name)) = TRIM(LOWER(import.Category))
WHERE 
	cat.ID_ItemType = 2

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  CurrentInventoryCount,
  Old_item_id)
SELECT 
   @Cardinal_ID_Company
  ,LTRIM(RTRIM(ItemName)) 
  ,0
  ,2
  , NULL
  , ID_ItemCategory
  , NULL
  , 1
  , 'Imported on 2021-03-14 11am'
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , 0
  , NULL
FROM @Record ORDER BY ItemName

exec pUpdateItemCurrentInventory

/****** Object:  Table [dbo].[Cardinal_ImportTemplate_20210314]    Script Date: 3/14/2021 10:58:44 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cardinal_ImportTemplate_20210314]') AND type in (N'U'))
DROP TABLE [dbo].[Cardinal_ImportTemplate_20210314]
GO
