DECLARE @GUID_Company VARCHAR(MAX) = 'B34739F0-F0AE-45C6-AF6B-DC0185A0AB59'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')


SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item   INT,
     Name_Item Varchar(MAX),
     UnitPrice Decimal(18, 2)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice)
SELECT dbo.fGetCleanedString( [Products])
       + CASE
           WHEN LEN([Unit]) > 0 then ' - ' + [Unit]
           ELSE ''
         END,
       [Unit Price]
FROm   [GOLDWINGS VETERINARY CLINIC - Items] 

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = 2
       and IsActive = 1
	   
SELECT *
FROm   @forImport

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [UnitPrice],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                2,
                import.UnitPrice,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1
FROM   @forImport import
WHERE  ID_Item IS NULL

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = 2
       and IsActive = 1

SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL