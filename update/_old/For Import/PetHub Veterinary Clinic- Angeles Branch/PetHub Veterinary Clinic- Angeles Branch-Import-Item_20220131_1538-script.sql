GO

CREATE OR
ALTER FUNCTION fGetCleanedString(@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      RETURN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@Value, '  ', ' '), CHAR(9), ''), CHAR(10), ''), CHAR(13), '')))
  END

GO

DECLARE @Record TABLE
  (
     CustomCode_Item   varchar(500),
     Name_Item         varchar(500),
     UnitCost          decimal(18, 2),
     MinInventoryCount int
  )
DECLARE @PetHubVeterinaryClinicAngelesBranch_ID_Company INT = 246

SELECT ID, Name FROM vCompanyActive WHERE ID = @PetHubVeterinaryClinicAngelesBranch_ID_Company

INSERT @Record
       (CustomCode_Item,
        Name_Item,
        UnitCost,
        MinInventoryCount)
SELECT dbo.fGetCleanedString([Code]),
       dbo.fGetCleanedString([NAME]),
       Convert(decimal(18, 2), [UNIT PRICE]),
       Convert(int, [MIN. INVENTORY])
FROM   [PetHub_New]

SELECT *
FROm   @Record

INSERT tItem
       (ID_Company,
        CustomCode,
        Name,
        ID_ItemType,
        UnitCost,
        MinInventoryCount,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @PetHubVeterinaryClinicAngelesBranch_ID_Company,
       CustomCode_Item,
       Name_Item,
       2,
       UnitCost,
       MinInventoryCount,
       'Imported on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt' ),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @Record
ORDER  BY Name_Item ASC 


SELECT * FRom tItem WHERE ID_Company = 246