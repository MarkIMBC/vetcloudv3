DEclare @GUID_Company VARCHAR(MAX) = '4A0E491E-5B07-4436-BB68-6D79D42AD35B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @Import TABLE
  (
     ID_Item         int,
     [Name_Item]     varchar(500),
     [Buying Price]  decimal,
     [Selling Price] decimal,
     [Quantity]      int
  )

INSERT @Import
       ([Name_Item],
        [Buying Price],
        [Selling Price],
        [Quantity])
SELECT [Item],
       [Buying Price],
       [Selling Price],
       [Quantity]
FROM   [inventoryPetHub_Angeles]

Update @Import
SET    ID_Item = item.ID
FROM   @Import import
       inner join tItem item
               on import. Name_Item = item.Name
WHERE  ID_Company = @ID_Company

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset inventory '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       item.CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Reset inventory '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       0
FROM   @Import import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  item.CurrentInventoryCount > 0
       AND ID_Company = @ID_Company
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

DELETE FROM @adjustInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import inventory from file on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       import.Quantity,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Import inventory from file on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @Import import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  import.Quantity > 0
       AND ID_Company = @ID_Company
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

Update tItem
SET    UnitCost = import.[Buying Price],
       UnitPrice = import.[Selling Price]
FROM   tItem item
       inner join @Import import
               on item.ID = import.ID_Item
WHERE  ID_Company = @ID_Company 
