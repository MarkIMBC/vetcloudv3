DEclare @GUID_Company VARCHAR(MAX) = '4A0E491E-5B07-4436-BB68-6D79D42AD35B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex  INT NOT NULL IDENTITY PRIMARY KEY,
     ID_Item   INT,
     Name_Item varchar(500),
     UnitPrice Decimal(18, 8),
     Comment   varchar(500)
  )

INSERT @ForImport
       (Name_Item,
        UnitPrice,
        Comment)
SELECT dbo.fGetCleanedString(REPLACE(REPLACE([Services], '{', ''), '}', '')),
       [Service Fee],
       dbo.fGetCleanedString(REPLACE(REPLACE(ISNULL([Notes], ''), '{', ''), '}', ''))
FROm   [PETHUB Services ANGELES]

INSERT dbo.tItem
       (ID_Company,
        Name,
        UnitPrice,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT DISTINCT @ID_Company,
                Name_Item,
                UnitPrice,
                CASE
                  WHEN LEN(Comment) > 0 THEN Comment + CHAR(13)+ CHAR(13)
                  ELSE ''
                END  + 'Imported '
                                             + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
                1,
                Getdate(),
                GETDATE(),
                1,
                1
FROm   @ForImport 
order BY Name_Item


SELECT * FROM @ForImport
where Name_Item IN (

'Aural Hematoma (Unilateral) Extra Large (24.1 and Up)',
'Aural Hematoma (Unilateral) Large (14.1 k to 24 kg)',
'Aural Hematoma (Unilateral) Medium (8.1 kg to 14 kg)',
'Aural Hematoma (Unilateral) Small (1-8 kg)'
)
order BY Name_Item
