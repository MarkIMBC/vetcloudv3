/****** Object:  Table [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]    Script Date: 4/12/2021 12:57:02 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]') AND type in (N'U'))
DROP TABLE [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]
GO
/****** Object:  Table [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]    Script Date: 4/12/2021 12:57:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Buying Price] [nvarchar](255) NULL,
	[Selling Price] [float] NULL,
	[Current Inventory Count] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-SOUR ', N'Drug/Medicine', NULL, 250, 17)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'COLIMOXYN', N'Drug/Medicine', NULL, 250, 21)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CANITRIM', N'Drug/Medicine', NULL, 200, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EMERFLOX', N'Drug/Medicine', NULL, 300, 13)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TILIMICOSIN', N'Drug/Medicine', NULL, 250, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BRONCURE', N'Drug/Medicine', NULL, 350, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TEFROSOL', N'Drug/Medicine', NULL, 350, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HEMACARE ( 120ml )', N'Drug/Medicine', NULL, 400, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HEMACARE ( 60ml )', N'Drug/Medicine', NULL, 250, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-DOX ( 120ml )', N'Drug/Medicine', NULL, 450, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LIVOTINE ( 120ml)', N'Drug/Medicine', NULL, 350, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LIVOTINE ( 60ml)', N'Drug/Medicine', NULL, 250, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'MELOXICAM ', N'Drug/Medicine', NULL, 550, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'METHIOVET ( 60Tabs )', N'Drug/Medicine', NULL, 720, 81)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-DOX ( 100Tabs )', N'Drug/Medicine', NULL, 1500, 264)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEFROTEC ( 60Tabs )', N'Drug/Medicine', NULL, 720, 14)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LIV 52 ( 60Tabs )', N'Drug/Medicine', NULL, 12, 36)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LIV 52 ( 60Tabs )', N'Drug/Medicine', NULL, 12, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'IMMUNOL ( 60Tabs )', N'Drug/Medicine', NULL, 12, 46)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PREVICOX ( 60Tabs ) Big', N'Drug/Medicine', NULL, 150, 57)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PREVICOX ( 60Tabs ) Small', N'Drug/Medicine', NULL, 50, 19)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'GENTIN ', N'Drug/Medicine', NULL, 400, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EYESHIELD', N'Drug/Medicine', NULL, 400, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'EAR WASH w/tea oil', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PROTICURE EAR DROP', N'Drug/Medicine', NULL, 420, 9)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'OTIDERM ', N'Drug/Medicine', NULL, 420, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K-9 EAR DROPS', N'Drug/Medicine', NULL, 650, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ORIDERMYL', N'Drug/Medicine', NULL, 800, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'MONDEX ( 340 grms )', N'Drug/Medicine', NULL, 150, 19)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'MONDEX ( 100 grams )', N'Drug/Medicine', NULL, 80, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'IMMUNE HEALTH (120)', N'Drug/Medicine', NULL, 15, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HEALTHY HEART (60)', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HEALTHY LIVER (60)', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HEALTHY VISION (60)', N'Drug/Medicine', NULL, 25, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRI VET (60)', N'Drug/Medicine', NULL, 20, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'V-GARD VITAMINS', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRI PLUS GEL', N'Drug/Medicine', NULL, 550, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VC-10 RECOVERY PASTE )', N'Drug/Medicine', NULL, 650, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ROYAL CANIN URINARY SO', N'Drug/Medicine', NULL, 350, 17)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'RECOVERY', N'Drug/Medicine', NULL, 300, 23)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CANINE ARMOR', N'Drug/Medicine', NULL, 650, 9)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TOPECURE', N'Drug/Medicine', NULL, 380, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VENOMA', N'Drug/Medicine', NULL, 380, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'WOUND BUSTER', N'Drug/Medicine', NULL, 250, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ORAL CARE', N'Drug/Medicine', NULL, 150, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'SINGEN CARE Cat', N'Drug/Medicine', NULL, 650, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'SINGEN CARE DOG', N'Drug/Medicine', NULL, 650, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PUP GAIN PLUS', N'Drug/Medicine', NULL, 350, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ROYAL CAIN ( PERSIAN )', N'Drug/Medicine', NULL, 1300, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'KITTEN', N'Drug/Medicine', NULL, 1000, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'URINARY S0 ( CAT )', N'Drug/Medicine', NULL, NULL, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'URINARY S0 ( DOG )', N'Drug/Medicine', NULL, 1500, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NACALVIT-C', N'Drug/Medicine', NULL, 220, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'COATSHINE', N'Drug/Medicine', NULL, 400, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-VIT SYRUP 60ml', N'Drug/Medicine', NULL, 150, 17)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-VIT SYRUP 120 ml', N'Drug/Medicine', NULL, 250, 14)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LC-VIT PLUS 120ml', N'Drug/Medicine', NULL, 250, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ALPHA VIT', N'Drug/Medicine', NULL, NULL, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRICAL', N'Drug/Medicine', NULL, 350, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PET REBOOST', N'Drug/Medicine', NULL, 400, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LIVERATOR', N'Drug/Medicine', NULL, 300, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BUSTERID', N'Drug/Medicine', NULL, NULL, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ARTHROPET', N'Drug/Medicine', NULL, 20, 31)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'MIDOX', N'Drug/Medicine', NULL, NULL, 26)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'K-BOOST', N'Drug/Medicine', NULL, NULL, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VITAMINS B-12', N'Drug/Medicine', NULL, NULL, 43)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CEFALEXIN/FALTERIA (500)', N'Drug/Medicine', NULL, NULL, 30)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PIMOBENDAN', N'Drug/Medicine', NULL, NULL, 100)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CLINDAMYCIN', N'Drug/Medicine', NULL, NULL, 88)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'REMEND', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ENROFLOXACIN', N'Drug/Medicine', NULL, NULL, 55)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TRIMETHOPRIM', N'Drug/Medicine', NULL, NULL, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'JUROLAV 50ml', N'Drug/Medicine', NULL, NULL, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'JUROLAV 250ml', N'Drug/Medicine', NULL, NULL, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CANIVERM', N'Drug/Medicine', NULL, 200, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TICKS AWAY (150 ml)', N'Detergents', NULL, 300, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TICKS AWAY (250 ml)', N'Detergents', NULL, 480, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ANTI-MANGE SHAMPOO', N'Detergents', NULL, 150, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ANTI-TICK BATH SHAMPOO', N'Detergents', NULL, NULL, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PLAY PET SHAMPOO', N'Detergents', NULL, 250, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'JAPANESE BLOSSOM', N'Detergents', NULL, 300, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BABY BLOSSOM', N'Detergents', NULL, 300, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FENOBUCARB', N'Detergents', NULL, 150, 20)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VANGARD FLEA POWER', N'Detergents', NULL, 100, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'V-GARD ORGANIC', N'Detergents', NULL, 100, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VANGARD PET SOAP', N'Detergents', NULL, 100, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LORI SOAP ( NON TOXIC )', N'Detergents', NULL, 120, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'LORI SOAP ( AMITRAS)', N'Detergents', NULL, 120, 11)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DOGGIES CHOICE HERBAL', N'Detergents', NULL, 120, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DOGGIES CHOICE TICK FLEA', N'Detergents', NULL, 120, 20)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'WUFF PET SOAP', N'Detergents', NULL, 120, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEEDING BOUL', N'Detergents', NULL, 60, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HONEY', N'Pet Food', NULL, 150, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'WHISKAS ', N'Pet Food', NULL, 40, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BEEF JERKY', N'Pet Food', NULL, 180, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DELOCIO SALMON', N'Pet Food', NULL, 180, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DELOCIO SALMON BBQ', N'Pet Food', NULL, 180, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE BEEF GRAVY', N'Pet Food', NULL, 45, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE BEEF VEGETABLES', N'Pet Food', NULL, 45, NULL)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DENTALSTIX 01/30', N'Pet Food', NULL, 90, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DENTAL CHEW STRAWBERRY', N'Pet Food', NULL, 20, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DENTAL CHEW APPLE', N'Pet Food', NULL, 20, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DENTAL ORANGE', N'Pet Food', NULL, 20, 15)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'DENTAL DIGESTIVE SUPPORT', N'Pet Food', NULL, 20, 12)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HIGH ENERGY 3kilos', N'Pet Food', NULL, 610, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'HIGH ENERGY 1kilos', N'Pet Food', NULL, 225, 9)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VALUE MEAL PUPPY 3kilos', N'Pet Food', NULL, 510, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VALUE MEAL PUPPY 1kilos', N'Pet Food', NULL, 200, 9)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VALUE MEAL ADULT 3kilos', N'Pet Food', NULL, 410, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'VALUE MEAL ADULT 1kilos', N'Pet Food', NULL, 150, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRI CHUNKS 10kg', N'Pet Food', NULL, 1600, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRI CHUNKS PUPPY ', N'Pet Food', NULL, 300, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NUTRI CHUNKS ADULT ', N'Pet Food', NULL, 250, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'CATSUN', N'Pet Food', NULL, 250, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD SPECTRA 3.5-7.5kg', N'Drug/Medicine', NULL, 750, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD SPECTRA 2-3.5kg', N'Drug/Medicine', NULL, 700, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD SPECTRA 7.5-15kg', N'Drug/Medicine', NULL, 800, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD SPECTRA 15-30kg', N'Drug/Medicine', NULL, 850, 2)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD SPECTRA 30-60kg', N'Drug/Medicine', NULL, 950, 6)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD PLAIN 2-4kg', N'Drug/Medicine', NULL, 550, 9)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD PLAIN 4-10kg', N'Drug/Medicine', NULL, 600, 18)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD PLAIN 10-25kg', N'Drug/Medicine', NULL, 650, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'NEXGARD PLAIN 25-50kg', N'Drug/Medicine', NULL, 700, 12)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ADVOCATE up to 4kg', N'Drug/Medicine', NULL, NULL, 1)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ADVOCATE 10-25kg', N'Drug/Medicine', NULL, NULL, 3)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BROADLINE 2.5-7.5kg', N'Drug/Medicine', NULL, NULL, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'BROADLINE 2.5kg', N'Drug/Medicine', NULL, NULL, 10)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'TOPI DERM', N'Drug/Medicine', NULL, NULL, 26)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'ROYAL CAIN KITTEN 01/29', N'Drug/Medicine', NULL, 80, 35)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'MICROZOLE 01/30', N'Drug/Medicine', NULL, NULL, 8)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE BEEF 01/30', N'Pet Food', NULL, 45, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE GRILLED BEEF', N'Pet Food', NULL, 45, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE SIMMERED BEEF', N'Pet Food', NULL, 45, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PEDIGREE BEEF CAN', N'Pet Food', NULL, 120, 7)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'Pedigree 5 KIND OF MEAT', N'Pet Food', NULL, 120, 0)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FURFECT FIPRONIL SOAP', N'Pet Food', NULL, 120, 14)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'FURFECT WOUND HEALING', N'Pet Food', NULL, NULL, 13)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PRIMA FOOD ENERGY', N'Pet Food', NULL, 160, 4)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PRIMA LAMM & RIES', N'Pet Food', NULL, 140, 5)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PYRAMID HILL (LAMB)', N'Pet Food', NULL, 120, 21)
GO
INSERT [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246] ([Code], [Name], [Category], [Buying Price], [Selling Price], [Current Inventory Count]) VALUES (NULL, N'PYRAMID HILL (BEEF)', N'Pet Food', NULL, 120, 22)
GO

DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @ID_Company_CausinVeterinaryClini INT = 57
DECLARE @InitInventoryImport TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitCost DECIMAL(18, 4) , UnitPrice DECIMAL(18, 4), CurrentInventoryCount int)
DECLARE @InventoryImport TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitCost DECIMAL(18, 4) , UnitPrice DECIMAL(18, 4), CurrentInventoryCount int, ID_Item INT)

INSERT @InitInventoryImport
SELECT  import.Name,
		cat.ID,
		ISNULL(import.[Buying Price],0),
		ISNULL(import.[Selling Price],0),
		ISNULL(import.[Current Inventory Count],0)
FROM    [CausinVeterinaryClinic_ImportItem_20210412_1246] import 
LEFT JOIN tItemCategory cat 
	ON UPPER(import.Category) = UPPER(cat.Name) AND cat.ID_ItemType = 2

INSERT @InventoryImport
SELECT  ItemName, MIN(ID_ItemCategory), UnitCost, UnitPrice, CurrentInventoryCount, 0
FROM    @InitInventoryImport
GROUP BY ItemName, UnitCost, UnitPrice, CurrentInventoryCount


/******************************************************************************************************************************/
 
INSERT tItem (
  ID_Company,
  Name,
  ID_ItemType,
  UnitPrice,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  Comment)
SELECT 
   @ID_Company_CausinVeterinaryClini
  ,ItemName
  ,@ID_ItemType_Inventoriable
  ,UnitPrice
  , UnitCost
  , ID_ItemCategory
  , NULL
  , 1
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , 'Imported on 2021-04-12 1pm'
 FROM   @InventoryImport

UPDATE @InventoryImport
SET
	ID_Item = item.ID
FROM @InventoryImport import 
	INNER JOIN tItem item on item.Name = import.ItemName
WHERE ID_Company = @ID_Company_CausinVeterinaryClini AND Comment = 'Imported on 2021-04-12 1pm'


Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory (
	[Code]
	,[ID_Item]
	,[Quantity] 
	,[UnitPrice]
	,[DateExpired]
	,[BatchNo]
	,[ID_FilingStatus]
	,[ID_Company]
	,[Comment]
	,[IsAddInventory]
)
SELECT  
	'Imported on 2021-04-12 1pm'
   ,ID_Item
   ,CurrentInventoryCount
   ,0.00
   ,NULL
   ,NULL
   ,3
   ,@ID_Company_CausinVeterinaryClini
   ,'Imported on 2021-04-12 1pm'
   ,1
FROM    @InventoryImport 
WHERE
	CurrentInventoryCount > 0
ORDER BY 
	ID_Item

exec pReceiveInventory @adjustInventory, 1

GO

/****** Object:  Table [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]    Script Date: 4/12/2021 12:57:02 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]') AND type in (N'U'))
DROP TABLE [dbo].[CausinVeterinaryClinic_ImportItem_20210412_1246]
GO