/****** Object:  Table [dbo].[ITEMIMPORTCONSULTAVET]    Script Date: 6/8/2021 9:26:17 PM ******/
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[ITEMIMPORTCONSULTAVET]')
                  AND type in ( N'U' ))
  DROP TABLE [dbo].[ITEMIMPORTCONSULTAVET]

GO

/****** Object:  Table [dbo].[ITEMIMPORTCONSULTAVET]    Script Date: 6/8/2021 9:26:17 PM ******/
SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE TABLE [dbo].[ITEMIMPORTCONSULTAVET]
  (
     [Code]                    [nvarchar](255) NULL,
     [Name]                    [nvarchar](255) NULL,
     [Category]                [nvarchar](255) NULL,
     [Buying Price]            [nvarchar](255) NULL,
     [Selling Price]           [nvarchar](255) NULL,
     [Current Inventory Count] [nvarchar](255) NULL
  )
ON [PRIMARY]

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Appeboost (60mL)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Activated Charcoal',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Allertsoft',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Amoxicillin- Vhellox (500mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Aspirin (80mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Aurizon',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Boost All Capsule',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Canexitin',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Canibrom',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Carprofen (25mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Carprofen (50mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Carpropain (50mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Cflex (250mg/5mL) 60mL',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Chlorhexidine- Dermstat',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Clindamycin- Acresil  (150mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Clindamycin- Clin-Gen (300mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Clotrimazole Cream ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Co-Amoxiclav  Syrup ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Co-Amoxiclav Tab. (625mg) ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Coatshine',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Dexamethasone- Impodex (500mcg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Dextrolyte',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Diacef Cefalexin (250mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Diacef Cefalexin (500mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Dr. Clauder''s Multiderm',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Enalapril- Scheepril (10mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Enalapril- Scheepril (5mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Enroxxa (50mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Exel (125mg/5ml) 60mL',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Exel (250mg/5mL) 60ml',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Freximol',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Furosemide (40mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Hemacare',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Iron Aid',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'K-Boost',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Ketoconazole cream',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Lactulose',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Laktrazine',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC- Cal',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC- Dox Syrup (120ml)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC- Dox Tablet Bottle',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC Scour',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC- Vit',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC- Vit Plus (Cat)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LC-Vit OB',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Levofloxacin',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Levothyroxine (50mcg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Livergard (120mL)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Macrocal Tablet',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Melmag (60mL) ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Methiovet ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Metoclopramide (10mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Metronidazole- Flagex (500mg/tab)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Metronidazole- Metrozole (60ml)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Neovax',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nephroflush Forte Bottle',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Neuromax',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Neurovet',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexpet (30mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nutriblend gel',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nutrical ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nutri-Kat Lysine ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nutri-trier Gel',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pet Defense',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pet Doxy Plus ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pet Fever Go',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pet Power-C Boost ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PetLiv Tablet',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PetSure- Ascorbic acid+Zinc (60ml)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pimodendan (5mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Prednisone (10mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Prednisone (5mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Prednisone 10mg/5ml (60ml)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Previcox ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Pulmoquin',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Ranitidine (150mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Renal Care',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Scourvet',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Silver Sulfadiazine cream ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SingenVet Cardi Excel',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SingenVet Joint Relief',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SingenVEt Uriease',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Sorvit ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Tracen Ceterizine (10mg)',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Vermitraz ',
        N'Drug/Medicine',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Advocate Up to 4kg (Green)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Advocate 4kg- 10kg (Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Advocate 10kg- 20kg (Red)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Advocate 25kg- 40kg (Dark Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Bravecto 4.5kg - 10kg (Orange)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Bravecto 10kg- 20kg (Graan)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Bravecto 20kg- 40kg (Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Broadline Up to 2.5kg (Gray)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Broadline 2.5kg - 7.5kg (Green)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Frontline Up to 10kg (Orange)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Frontline 10kg to 20kg (Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Frontline 20kg- 40kg (Violet)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Heartgard Up to 11kg (Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Heartgard 12kg to 22kg (Green)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Heartgard 23kg to 45kg (Brown)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexgard Spectra 2kg to 3.5kg (Orange)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexgard Spectra 3.5kg to 7.5kg (Yellow)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexgard Spectra 7.5kg to 15kg (Green)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexgard Spectra 15kg to 30kg (Violet)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Nexgard Spectra 30kg to 60kg (Red)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Simparica 2.6kg to 5kg (Violet)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Simparica 5.1kg to 10kg (Orange)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Simparica 10.1kg to 20kg (Blue)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Simparica 20.1kg to 40kg (Green)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Seresto Small (<8kg)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Seresto Small Large (>8kg)',
        N'Tick & Flea Prevention',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BATH  WIPES',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BREATH SPRAY FOR DOGS (118ML)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BUBBLE BATH SOFT & SHINY (500ML)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BUTTON E. COLLAR (LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BUTTON E. COLLAR (SMALL)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BUTTON E. COLLAR (X-LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BUTTON E. COLLAR (X-SMALL)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CARPET REFRESHER',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CHARLES PET BONE ACCESSORY',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CHARLES PET GROOMING COMB',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CHLORHEX',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CUDDLY COLOGNE HER',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CUDDLY COLOGNE HIM',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DELTA CARE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTAL WIPES',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DERMATIC SHAMPOO',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DOG BREEDER ORG. SHAMPOO 1L',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DOG BREEDER ORG. SHAMPOO 20L',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. PETS SOAP TICK AND FLEA',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EAR , MITE & TICK CONTROL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EAR WIPES',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EYE WIPES',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FLEXIA CAT SAND 10L',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'GLAM ORG. SHAMP. FOREVER LOVE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'GLAM ORG. SHAMP. SWEET HEAVEN',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'GLAM ORGANIC SHAMP. ADORABLE ANGEL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HAPPY PETS FRENZY FOR HER (PINK) 250ML',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HARTZ EAR CLEANER',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HOUSEBREAKING AID',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSH PET TRAINING PADS (BLUE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSH PET TRAINING PADS (PINK)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSH PET TRAINING PADS (PUPPY)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET DIAPER LARGE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET DIAPER MEDIUM',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET DIAPER SMALL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET DIAPER XLARGE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET DIAPER XSMALL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET MALE WRAPS LARGE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET MALE WRAPS MEDIUM',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HUSHPET MALE WRAPS SMALL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HYDROCORTISONE LOTION',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'INFLATTABLE E. COLLAR (LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'INFLATTABLE E. COLLAR (MEDIUM)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LEASH HARNESS SET STRIPE (MEDIUM)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LEASH HARNESS SET STRIPE (LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LEASH HARNESS SET (X-LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LIQUID BANDAGE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LORI  SOAP HERBAL & MINERAL (RED)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LORI SOAP AMITRAZ (VIOLET)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LORI SOAP GUAVA & BANANA (BROWN)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LORI SOAP HERBAL & MAINTENANCE (GREEN)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAJOR D DISINFECTANT',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MICHIKO FLEA COMB',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MICHIKO NYLON LEASH SET (LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MICONATE SHAMPOO',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MICROCYN AH',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET BOWL CERAMIC ',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET BOWL STAINLESS (SMALL)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET FEEDER DOGS AND CATS',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET FEEDING BOTTLE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET INC. TRAINING PADS (LARGE)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PET WATER NUZZLE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PLAQUE OFF CAT',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS COAT DRY SHAM. FLORAL SCENT',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS COAT DRY SHAM. HERBAL',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS COAT DRY SHAM. PURPLE DEW',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS COAT DRY SHAM. SUMMER',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS PET ALL BREED OATMEAL SOAP',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS PET HERBITRAZ',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRECIOUS PET VET & HERBS',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ROCKIE PET TRAINING PADS (SMALL)',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SEBOHEX SHAMPOO',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SERGEANTS SKIP FLEA',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SIR DOG SHAMPOO- BLACK',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SIR DOG SHAMPOO- WHITE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'STOP BLEEDING',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'TEAR STAIN REMOVER',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'VIRBAC C.E.T. ENZYMATIC TOOTHPASTE',
        N'Accessories',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'AMPICILLIN SODIUM BOTTLE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BELAMYL BOTTLE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BICOGEN ',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'BLOOD TRANSFUSION SET',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CALCIUM BOROGLUCONATE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CALMIVET ',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CATHETER',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CEFUROXIME',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CERENIA',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'COFORTA',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'COTTON',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'COTTON TIP APPLICATOR',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'COVER SLIDES',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'COVER SLIP',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DEXAMATHASONE AMPULE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DEXAMATHASONE BOTTLE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Disposable Cath. 2.6',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Disposable Catheter Cat 1',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Disposable Catheter Cat 1.3 ',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DOG CATHETER 4Frx20',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DOG CATHETER 6Frx20',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DOXYCYCLINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EDTA TUBES (VIOLET)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ENROFLOXACIN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EPINEPHRINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'EYE PADS',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FACEMASK',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FAMOTIDINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FLOU DYE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Folley Catheter Latex',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FUROSEMIDE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'GENTAMICIN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'GLASS SLIDES',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HEPARIN TUBES (GREEN)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'HYDROPEROXIDE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V FLUIDS  BLUE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V FLUIDS  PINK',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V FLUIDS GREEN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V. CANNULA 22G (BLUE)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V. CANNULA 24G (YELLOW)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V. CANNULA 26G (VIOLET)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I.V. LINE SET ',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ICE BAG',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'IVERMECTIN (IVOMEC)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LEUKOPLAST',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'LIDOCAINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MARBOCYL',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'METOCLOPRAMIDE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'METRONIDAZOLE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MICROPORE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MINERAL OIL',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NEDDLE 18G',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NEDDLE 22G',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NEDDLE 23G',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NEDDLE 25G',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NON STERILE GAUZE SWAB',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'OXYTOCIN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PENICILLIN STREPTOMYCIN BOTTLE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PLASTER OF PARIS',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 0/0 CURVED',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 0/0 ROUND',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 2/0 CURVED',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 2/0 ROUND',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 3/0 CURVED',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POLYGLACTIN 3/0 ROUND',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POTASSIUM CHLORIDE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'POVIDINE IODINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PREDNISOLONE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'RANITIIDINE',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'RUBBER SUCTION',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SALBUTAMOL',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SPASMOVET',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'STERILE GAUZE SWAB',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL  CAP',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL BLADES # 11',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL BLADES # 20',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL GLOVES 7.5',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL GLOVES LATEX 6.5',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SURGICAL GOWN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SUTURE CHROMIC BROWN 2/0 CURVED',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SUTURE CHROMIC BROWN 2/0 ROUND',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SUTURES NYLON 2/0 CUTTING',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'TOLFENAMIC',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'TONGUE DEPRESSOR',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'TRANEXAMIC ACID',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'URINE COLLECTOR',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'VASOTRAN',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'VET WRAP',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WADDING SHEET',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ZINC OXIDE PLASTER',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ZOLETIL',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALA CARTE LAMB&RICE 18KG (PINK)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALA CARTE LAMB&RICE/ KILO',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALA CARTE SALMON&POTATO 18KG (BLUE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALA CARTE SALMON&POTATO/ KILO',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALL CATS 3KG.',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'ALL DOGS 3KG.',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTASTIX LARGE',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTASTIX MEDIUM',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTASTIX PUPPY',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTASTIX SMALL',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DENTASTIX TOY',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. CLAUDERS INTESTINAL (Orange)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. CLAUDERS KIDNEY CATS (Maroon)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. CLAUDERS KIDNEY DOGS (Maroon)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. CLAUDERS LIVER (Light Blue)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'DR. CLAUDERS SKIN (Dark Blue)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I AMS ADULT DOG (GREEN) 1.5KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I AMS ADULT SMALL BREED (RED) 1.5KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'I AMS MOMMY & PUPPY (YELLOW) 1.5KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME ELITE ADULT 1.5KG (WHITE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME ELITE ADULT 10KG (WHITE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME ELITE ADULT 3KG (WHITE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME ELITE PUPPY 1.5KG (WHITE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME ELITE PUPPY 3KG (WHITE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME PRIME ADULT 1.5KG (BLUE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME PRIME PUPPY 1.5KG (BLUE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MAXIME PRME ADULT 3KG (BLUE)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MEAT JERKY BACON STIX',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MEAT JERKY GRILLED LIVER',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MEAT JERKY HAM & CHEESE',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MEAT JERKY ROASTED LAMB',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'MILKY BITCH',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC BLUE ADULTO 2KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC BLUE ADULTO 5KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC BLUE CACHORRO 2KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC BLUE CACHORRO 8KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC PINK ADULTO 2KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC PINK ADULTO 8 KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC PINK CACHORRO 2KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'NUPEC PINK CACHORRO 8KG',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE CAN ADULT BEEF FLVR.',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE CAN PUPPY HOMESTYLE',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE POUCH BEEF & VEGETABLES',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE POUCH CHICKEN IN GRAVY',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE POUCH GRILLED LIVER',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PEDIGREE POUCH PUPPY JR.',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Heart Care (h/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Kidney Care (k/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Skin/Food Sen. (d/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Urgent Care (a/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Urinary Care Cat (c/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'PRESCRIPTION DIET Urinary Care Dog (s/d)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'RECOVERY PASTE FOR CATS',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'RECOVERY PASTE FOR DOGS',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SINGENVET GI TRACT (Dog)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SINGENVET KIDNEY DIET (Cat)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SINGENVET RECOVERY DIET (Cat)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SINGENVET RECOVERY DIET (Dog)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'SINGENVET URINARY DIET (Cat)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS CAN OCEAN FISH (400g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS CAN TUNA (400g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS DRY MACKEREL (7kg)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS DRY OCEAN FISH JR. (1.2kg)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS DRY TUNA (1.2kg)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS POUCH ADLT TUNA (85g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS POUCH ADLT TUNA&WHITEFISH (85g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS POUCH ADULT MACKEREL (85g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'WHISKAS POUCH KITTEN (85g)',
        N'Pet Food',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'8in1 (Novibac)',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'8in1 (Vanguard)',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Canine Wormer',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Drontal Syrup Bottle',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FelloCell ',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Prazel Syrup',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Proxantel',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Rabies (Defensor)/ bottle',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Rabies (Novibac)/ bottle',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Rabisin / bottle',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'TriCat (Purevax)',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Wormectin Bottle',
        N'Vaccination',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Canine Distemper Rapid Test Kit',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Canine Heartworm Rapid Test kit',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'CPV, CCV, Giardia Test Kit',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Dianotech (Canine Parvovirus)',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Ehrlichia Rapid Test Kit',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'FeLv Ag, FIV Ab Rapid Test kit',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Rapid Vet-D',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Ciprofloxacin ',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Hypromellose',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Opticare',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Otidex',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Polymixin B + Neomycin 5mL (Ears)',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Polymixin B + Neomycin 7.5mL (Eyes)',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Sodago Gel',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Sodago Serum',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Tobramycin',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Tobramycin Dexamethasone',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Xiclospor (Opthalmic)',
        N'Eye & Ear Drops',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Syringe 1mL 1cc',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Syringe 3mL 3cc',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Syringe 5mL 5cc',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

INSERT [dbo].[ITEMIMPORTCONSULTAVET]
       ([Code],
        [Name],
        [Category],
        [Buying Price],
        [Selling Price],
        [Current Inventory Count])
VALUES (NULL,
        N'Syringe 10mL 10cc',
        N'Laboratory Test & Kits',
        NULL,
        NULL,
        NULL)

GO

GO

DECLARE @ConsultAVet_ID_Company INT = 99
Declare @Record Table
  (
     ItemName        Varchar(MAX),
     ID_ItemCategory INT,
     UnitCost        Decimal,
     UnitPrice       Decimal
  )

INSERT @Record
       (ItemName,
        ID_ItemCategory,
        UnitCost,
        UnitPrice)
SELECT LTRIM(RTRIM(import.Name)),
       cat.ID,
       ISNULL([Buying Price], 0),
       ISNULL([Selling Price], 0)
FROM   ITEMIMPORTCONSULTAVET import
       LEFT JOIN (SELECT MAX(ID) ID,
                         Name
                  FROM   tItemCategory category
                  WHERE  ID_ItemType = 2
                  GROUP  BY NAME) cat
              ON TRIM(LOWER(cat.Name)) = TRIM(LOWER(import.Category))

INSERT tItem
       (ID_Company,
        Name,
        UnitPrice,
        ID_ItemType,
        UnitCost,
        ID_ItemCategory,
        Code,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        CurrentInventoryCount,
        Old_item_id)
SELECT @ConsultAVet_ID_Company,
       LTRIM(RTRIM(ItemName)),
       UnitPrice,
       2,
       UnitCost,
       ID_ItemCategory,
       NULL,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1,
       0,
       NULL
FROM   @Record
ORDER  BY ItemName

GO

/****** Object:  Table [dbo].[ITEMIMPORTCONSULTAVET]    Script Date: 6/8/2021 9:26:17 PM ******/
IF EXISTS (SELECT *
           FROM   sys.objects
           WHERE  object_id = OBJECT_ID(N'[dbo].[ITEMIMPORTCONSULTAVET]')
                  AND type in ( N'U' ))
  DROP TABLE [dbo].[ITEMIMPORTCONSULTAVET]

GO 
