/****** Object:  Table [dbo].[ServiceImportTemplate_VetsOnTheBlock]    Script Date: 3/30/2021 12:34:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ServiceImportTemplate_VetsOnTheBlock]') AND type in (N'U'))
DROP TABLE [dbo].[ServiceImportTemplate_VetsOnTheBlock]
GO
/****** Object:  Table [dbo].[ServiceImportTemplate_VetsOnTheBlock]    Script Date: 3/30/2021 12:34:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceImportTemplate_VetsOnTheBlock](
	[Code] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Price] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Consultation Fee (Associates)', N'Consultation Charge', 350)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Professional Fee (Senior Veterinarian) Max of 3 patients', N'Consultation Charge', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EMERGENCY FEE (SMALL 0.5 – 5 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EMERGENCY FEE (MEDIUM 6 - 10 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EMERGENCY FEE (LARGE 11 – 15 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EMERGENCY FEE (X LARGE 16 kg – above)', N'Vet Services', 2000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'HOUSE CALL (Walking Distance)', N'Vet Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'HOUSE CALL (1km - 3km)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'HOUSE CALL (4km - 7km)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'HOUSE CALL (10km - above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'PET WELLNESS PROGRAM', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Deworming (SMALL 0.5 – 5 kg)', N'Deworming', 300)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Deworming (MEDIUM 6 - 10 kg)', N'Deworming', 400)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Deworming (LARGE 11 – 15 kg)', N'Deworming', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Deworming (X LARGE 16 kg – above)', N'Deworming', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Heartworm Prevention (SMALL 0.5 – 5 kg)', N'Vaccination', 200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Heartworm Prevention (MEDIUM 6 - 10 kg)', N'Vaccination', 300)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Heartworm Prevention (LARGE 11 – 15 kg)', N'Vaccination', 400)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Heartworm Prevention (X LARGE 16 kg – above)', N'Vaccination', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Add On:  Vitamin B Complex Injection', N'Vaccination', 200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Rabies Vaccination', N'Vaccination', 300)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'DHLPPi', N'Vaccination', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'DHLPPi + Corona', N'Vaccination', 600)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Feline Vaccine', N'Vaccination', 700)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'LOCAL - ANESTHETICS AND SEDATIONS (SMALL 0.5 – 5 kg)', N'Vet Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'LOCAL - ANESTHETICS AND SEDATIONS  (MEDIUM 6 - 10 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'LOCAL - ANESTHETICS AND SEDATIONS  (LARGE 11 – 15 kg)', N'Vet Services', 900)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'LOCAL - ANESTHETICS AND SEDATIONS  (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SEDATIVE - ANESTHETICS AND SEDATIONS (SMALL 0.5 – 5 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SEDATIVE - ANESTHETICS AND SEDATIONS  (MEDIUM 6 - 10 kg)', N'Vet Services', 900)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SEDATIVE - ANESTHETICS AND SEDATIONS  (LARGE 11 – 15 kg)', N'Vet Services', 1200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'SEDATIVE - ANESTHETICS AND SEDATIONS  (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GENERAL - ANESTHETICS AND SEDATIONS (SMALL 0.5 – 5 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GENERAL - ANESTHETICS AND SEDATIONS  (MEDIUM 6 - 10 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GENERAL - ANESTHETICS AND SEDATIONS  (LARGE 11 – 15 kg)', N'Vet Services', 2000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'GENERAL - ANESTHETICS AND SEDATIONS  (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Drainage with Antibiotic Injection (SMALL 0.5 – 5 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Drainage with Antibiotic Injection  (MEDIUM 6 - 10 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Drainage with Antibiotic Injection  (LARGE 11 – 15 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Drainage with Antibiotic Injection  (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - CAT (SMALL 0.5 – 5 kg)', N'Vet Services', 2500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - CAT (MEDIUM 6 - 10 kg)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - DOG (SMALL 0.5 – 5 kg)', N'Vet Services', 3500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - DOG (MEDIUM 6 - 10 kg)', N'Vet Services', 4500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - DOG (LARGE 11 – 15 kg)', N'Vet Services', 5000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Urinary Catheterization - DOG (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning (Regular) with Medications (SMALL 0.5 – 5 kg)', N'Grooming Services', 350)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning (Regular) with Medications (MEDIUM 6 - 10 kg)', N'Grooming Services', 350)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning (Regular) with Medications (LARGE 11 – 15 kg)', N'Grooming Services', 450)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning (Regular) with Medications (X LARGE 16 kg – above)', N'Grooming Services', 450)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning with Otitis (SMALL 0.5 – 5 kg)', N'Grooming Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning with Otitis (MEDIUM 6 - 10 kg)', N'Grooming Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning with Otitis (LARGE 11 – 15 kg)', N'Grooming Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Ear Cleaning with Otitis (X LARGE 16 kg – above)', N'Grooming Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EUTHANASIA (SMALL 0.5 – 5 kg)', N'Vet Services', 1200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EUTHANASIA (MEDIUM 6 - 10 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EUTHANASIA (LARGE 11 – 15 kg)', N'Vet Services', 2000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'EUTHANASIA (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Health Certificate', N'Vet Services', 250)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (Regular Hours) (SMALL 0.5 – 5 kg)', N'Vet Services', 3500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (Regular Hours) (MEDIUM 6 - 10 kg)', N'Vet Services', 4500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (Regular Hours) (LARGE 11 – 15 kg)', N'Vet Services', 6000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (Regular Hours) (X LARGE 16 kg – above)', N'Vet Services', 8000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (After Clinic Hours) (SMALL 0.5 – 5 kg)', N'Vet Services', 4500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (After Clinic Hours) (MEDIUM 6 - 10 kg)', N'Vet Services', 6000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (After Clinic Hours) (LARGE 11 – 15 kg)', N'Vet Services', 8000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Delivery Assistance (After Clinic Hours) (X LARGE 16 kg – above)', N'Vet Services', 10000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail Clipping (add local anesthetic for ingrown toe nails) (SMALL 0.5 – 5 kg)', N'Grooming Services', 200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail Clipping (add local anesthetic for ingrown toe nails) (MEDIUM 6 - 10 kg)', N'Grooming Services', 200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail Clipping (add local anesthetic for ingrown toe nails) (LARGE 11 – 15 kg)', N'Grooming Services', 250)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nail Clipping (add local anesthetic for ingrown toe nails) (X LARGE 16 kg – above)', N'Grooming Services', 250)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Nebulization / per session inclusive of meds', N'Vet Services', 350)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Oxygenation (per hour)', N'Vet Services', 300)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Paracentesis / Cystocentesis (+ Cost of Sedation/Anesthetics) (SMALL 0.5 – 5 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Paracentesis / Cystocentesis (+ Cost of Sedation/Anesthetics) (MEDIUM 6 - 10 kg)', N'Vet Services', 2000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Paracentesis / Cystocentesis (+ Cost of Sedation/Anesthetics) (LARGE 11 – 15 kg)', N'Vet Services', 3000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Paracentesis / Cystocentesis (+ Cost of Sedation/Anesthetics) (X LARGE 16 kg – above)', N'Vet Services', 4000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Antibiotic Injection (SMALL 0.5 – 5 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Antibiotic Injection (MEDIUM 6 - 10 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Antibiotic Injection (LARGE 11 – 15 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Antibiotic Injection (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Suturing and Local Anesthetic (SMALL 0.5 – 5 kg)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Suturing and Local Anesthetic (MEDIUM 6 - 10 kg)', N'Vet Services', 2000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Suturing and Local Anesthetic (LARGE 11 – 15 kg)', N'Vet Services', 2500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Wound Dressing With Suturing and Local Anesthetic (X LARGE 16 kg – above)', N'Vet Services', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Internment / Burial Services - Local Service (SMALL 0.5 – 5 kg)', N'Vet Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Internment / Burial Services - Local Service (MEDIUM 6 - 10 kg)', N'Vet Services', 700)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Internment / Burial Services - Local Service (LARGE 11 – 15 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Internment / Burial Services - Local Service (X LARGE 16 kg – above)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Medicated (fungal, mites, bacterial treatment shampoos) - Bath (SMALL 0.5 – 5 kg)', N'Vet Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Medicated (fungal, mites, bacterial treatment shampoos) - Bath (MEDIUM 6 - 10 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Medicated (fungal, mites, bacterial treatment shampoos) - Bath (LARGE 11 – 15 kg)', N'Vet Services', 1000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Medicated (fungal, mites, bacterial treatment shampoos) - Bath (X LARGE 16 kg – above)', N'Vet Services', 1500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding (without food) - Bath (SMALL 0.5 – 5 kg)', N'Boarding Services', 200)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding (without food) - Bath (MEDIUM 6 - 10 kg)', N'Boarding Services', 250)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding (without food) - Bath (LARGE 11 – 15 kg)', N'Boarding Services', 300)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding (without food) - Bath (X LARGE 16 kg – above)', N'Boarding Services', 400)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding with Food (Clinics Discretion) (SMALL 0.5 – 5 kg)', N'Boarding Services', 350)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding with Food (Clinics Discretion) (MEDIUM 6 - 10 kg)', N'Boarding Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding with Food (Clinics Discretion) (LARGE 11 – 15 kg)', N'Boarding Services', 500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Boarding with Food (Clinics Discretion) (X LARGE 16 kg – above)', N'Boarding Services', 750)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Resection - MAJOR SURGERIES (SMALL 0.5 – 5 kg)', N'Surgery', 5000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Resection - MAJOR SURGERIES  (MEDIUM 6 - 10 kg)', N'Surgery', 5500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Resection - MAJOR SURGERIES  (LARGE 11 – 15 kg)', N'Surgery', 6000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anal Sac Resection - MAJOR SURGERIES (X LARGE 16 kg – above)', N'Surgery', 7500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anastomosis - MAJOR SURGERIES (SMALL 0.5 – 5 kg)', N'Surgery', 6500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anastomosis - MAJOR SURGERIES  (MEDIUM 6 - 10 kg)', N'Surgery', 7000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anastomosis - MAJOR SURGERIES  (LARGE 11 – 15 kg)', N'Surgery', 7500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Anastomosis - MAJOR SURGERIES (X LARGE 16 kg – above)', N'Surgery', 9000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - DOG (SMALL 0.5 – 5 kg)', N'Surgery', 6500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - DOG (MEDIUM 6 - 10 kg)', N'Surgery', 8000)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - DOG (LARGE 11 – 15 kg)', N'Surgery', 10500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - DOG (X LARGE 16 kg – above)', N'Surgery', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - CAT (SMALL 0.5 – 5 kg)', N'Surgery', 4500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - CAT (MEDIUM 6 - 10 kg)', N'Surgery', 4500)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - CAT (LARGE 11 – 15 kg)', N'Surgery', NULL)
GO
INSERT [dbo].[ServiceImportTemplate_VetsOnTheBlock] ([Code], [Name], [Category], [Price]) VALUES (NULL, N'Cesarean Section - CAT (X LARGE 16 kg – above)', N'Surgery', NULL)
GO

DECLARE @ID_Company_VetOnTHeBLOCK INT = 55
DECLARE @ItemServices TABLE (ItemName VARCHAR(MAX), ID_ItemCategory INT, UnitPrice DECIMAL(18, 4))

INSERT @ItemServices
SELECT  import.Name,
		cat.ID,
		ISNULL(import.[Price],0)
FROM    [ServiceImportTemplate_VetsOnTheBlock] import 
LEFT JOIN tItemCategory cat 
	ON UPPER(import.Category) = UPPER(cat.Name) AND cat.ID_ItemType = 1

INSERT tItem (
  ID_Company,
  Name,
  UnitPrice,
  ID_ItemType,
  UnitCost,
  ID_ItemCategory,
  Code,
  IsActive,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy, 
  Old_item_id)
SELECT 
  @ID_Company_VetOnTHeBLOCK
  ,ItemName
  ,UnitPrice
  ,1
  , NULL
  , ID_ItemCategory
  , NULL
  , 1
  , NULL
  , GETDATE()
  , GETDATE()
  , 1
  , 1 
  , NULL
 FROM   @ItemServices

/****** Object:  Table [dbo].[ServiceImportTemplate_VetsOnTheBlock]    Script Date: 3/30/2021 12:34:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ServiceImportTemplate_VetsOnTheBlock]') AND type in (N'U'))
DROP TABLE [dbo].[ServiceImportTemplate_VetsOnTheBlock]
GO