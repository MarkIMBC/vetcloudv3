DEclare @GUID_Company VARCHAR(MAX) = 'F5BA7DF7-38AD-4F3B-A9F1-27A5D0263A69'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Client            INT,
     ID_Patient           INT,
     Name_Client          varchar(500),
     ContactNumber_Client varchar(500),
     Address_Client       varchar(500),
     Name_Patient         varchar(500),
     Name_Gender          varchar(500),
     ID_Gender            INT,
     Species_Patient      varchar(500),
     Age_Patient          varchar(500),
     DateBirth_Patient    DateTime
  )

uPDATE tClient
set    nAME = [dbo].[fGetCleanedString](Name)
WHERE  ID_Company = @ID_Company

uPDATE tPatient
set    nAME = [dbo].[fGetCleanedString](Name)
WHERE  ID_Company = @ID_Company

INSERT @forImport
       (Name_Client,
        ContactNumber_Client,
        Address_Client,
        Name_Patient,
        Species_Patient,
        Age_Patient,
        ID_Gender)
SELECT [dbo].[fGetCleanedString]([CLIENT NAME]),
       [dbo].[fGetCleanedString]([CONTACT NO.]),
       [dbo].[fGetCleanedString]([ADDRESS]),
       [dbo].[fGetCleanedString]([PET'S NAME]),
       [dbo].[fGetCleanedString]([BREED])
       + CASE
           WHEN LEN([dbo].[fGetCleanedString]([BREED])) > 0
                AND LEN([dbo].[fGetCleanedString]([SPECIES])) > 0 THEN ' - '
           ELSE ''
         END
       + [dbo].[fGetCleanedString]([SPECIES]),
       [dbo].[fGetCleanedString]([DOB/AGE]),
       CASE
         WHEN [GENDER] = 'M' THEN 1
         ELSE
           CASE
             WHEN [GENDER] = 'F' THEN 2
             ELSE NULL
           END
       END
FROm   [Immanuel Animal Clinic Pet Supply and Grooming Services Clients Patient]

Update @forImport
SET    DateBirth_Patient = TRY_CONVERT(Datetime, Age_Patient)

Update @forImport
SET    DateBirth_Patient = NULL
WHERE  DateBirth_Patient = '1900-01-01 00:00:00.000'

Update @forImport
SET    ContactNumber_Client = '0' + ContactNumber_Client
WHERE  LEN(ContactNumber_Client) < 11
       anD LEN(ContactNumber_Client) > 0

DELETE FROM @forImport
WHERE  LEN(Name_Client) = 0
       AND LEN(Name_Patient) = 0

UPDATE @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
where  ID_Company = @ID_Company

UPDATE @forImport
SET    ID_Patient = patient.ID
FROM   @forImport import
       inner join tClient client
               on import.ID_Client = client.ID
       inner join tPatient patient
               on import.Name_Patient = patient.Name
                  and client.ID = patient.ID_Client
where  client.ID_Company = @ID_Company
       AND patient.ID_Company = @ID_Company

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        Address,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber_Client,
                Address_Client,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @forImport
WHERE  ID_Client IS NULL
       AND LEN(Name_Client) > 0

UPDATE @forImport
SET    ID_Client = client.ID
FROM   @forImport import
       inner join tClient client
               on import.Name_Client = client.Name
where  ID_Company = @ID_Company

INSERT dbo.tPatient
       (ID_Company,
        ID_Client,
        Name,
        Species,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                Species_Patient,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
				CASE WHEN LEN(Age_Patient) > 0 and DateBirth_Patient IS NULL then 'Date Birth: ' + Age_Patient + CHAR(13) + CHAR(13) ELSE '' END +
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @forImport
WHERE  ID_Client IS NOT NULL
       AND ID_Patient IS NULL
       AND LEN(Name_Patient) > 0

SELECT *
FROM   @forImport 
