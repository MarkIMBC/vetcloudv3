﻿/* BEWARE Reexecuting this query may result of duplicate records on tPatient*/

exec _pBackUpDatabase

Go


IF (EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'CardinalPetRecord_ImportClientPatient_20210212')
  )
BEGIN

  DROP TABLE dbo.[CardinalPetRecord_ImportClientPatient_20210212]
END

/****** Object:  Table [dbo].[CardinalPetRecord_ImportClientPatient_20210212]    Script Date: 2/12/2021 6:10:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardinalPetRecord_ImportClientPatient_20210212] (
  [ID] [nvarchar](255) NULL
 ,[First] [nvarchar](255) NULL
 ,[Last] [nvarchar](255) NULL
 ,[Address] [nvarchar](255) NULL
 ,[email] [nvarchar](255) NULL
 ,[Mobile] [float] NULL
 ,[Telephone] [float] NULL
 ,[Pets Name] [nvarchar](255) NULL
 ,[RowIndex] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AA4022', N'Ariel', N'Acuna', N'25 Sampaguita St Gliria V Subd Novaliches QC', N'yoyacuna@gmail.com', 9178464022, NULL, N'Cholo', 1)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AB0435', N'Aimee', N'Balaguer', N'B14L13 Sierra Vista Novaliches QC', N'aimee.balaguer@gmail.com', 9178540435, NULL, N'Louie', 2)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AB4333', N'Al', N'Belmonte', N'1724 Joel St Jordan Plains Novaliches QC', NULL, 9164834333, NULL, N'MaiMai', 3)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AC5250', N'April Joy', N'Colarina', N'54 S Francisco St San Bartolome Nova QC', N'april_joy.colarina@yahoo.com', 9457935250, NULL, N'Zenithzu/Nora/Rocketship', 4)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AC6309', N'Ashley', N'Compacion', N'Goodwill Homes 2 St Mark St Bagbag Nova QC', NULL, 9656076309, NULL, N'YangYang', 5)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AC6724', N'Abie E', N'Carreon', N'625 Quirino Highway Bagbag QC', NULL, 9236096724, 9364504, N'AJ', 6)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AD0500', N'April Joy', N'dela Cruz', N'809 St Matthew St Maries Vill San Bartolome Nova QC', N'aprildelacruz007@gmail.com', 9062730500, NULL, N'Cleo', 7)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AD3507', N'Abegail', N'Dadivas', NULL, N'abhie.dadivas@yahoo.com', 9563593507, NULL, N'Nairobi', 8)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AD4688', N'Allan', N'Desbaro', N'A15 Dupax Cmpd Bagbag Nova QC', NULL, 9173124688, NULL, N'Sweety', 9)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AD6603', N'Aeva', N'Davis', N'15 P Castillo St Pansol QC', NULL, 9278616603, NULL, N'Summer/Panther/Oreo/Coco/Fudge', 10)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AD9116', N'Adrian', N'de Jesus', N'15 Venus St Remarville Subd Bagbag Nova QC', NULL, 9162059116, NULL, N'Pokwang', 11)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AE5328', N'Amy', N'Escleto', N'L4B4 Grand Mango Homes 3 Carron St Bagbag Nova QC', NULL, 9258905328, NULL, N'Shinny Atom', 12)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AE9025', N'Angel', N'Espiritu', N'25 Parokya ng Pagkabuhay Rd Bagbag Nova QC', N'ahespiritu7@gmail.com', 9989649025, NULL, N'Scout', 13)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AF6707', N'Arjayne', N'Ferrer', N'B4 L14 St Antony San Perdo 7', N'drarferrer@gmail.com', 9178976707, NULL, N'Flogii Cong', 14)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AF9497', N'Aileen L ', N'Ferrancullo', N'092 Virginia St Gulod Nova Q', NULL, 9665099497, NULL, N'Maverick', 15)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AG2004', N'Anjela Polla', N'Gonzalo', NULL, N'anjelapollagonzalo@gmail.com', 9658392004, NULL, N'Polito', 16)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AI5760', N'Anthony', N'Ignacio', N'Bagbag Nova QC', N'chubbythonyfoodservices@gmail.com', 9664165760, NULL, N'Yoona Pimpim', 17)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AK8899', N'Arnhil', N'Kho', N'10 Ramon Magsaysay Ave San Bartolome Nova QC', N'arnhillongkho@yahoo.com.ph', 9267128899, NULL, N'Lucky', 18)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AL0080', N'Yulo', N'Lao', N'627 Abbey Rd Bagbag Nova QC', N'laoangelica5@gmail.com', 9150280080, 9350796839, N'Bravo', 19)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AL3262', N'Analyn', N'Labita', N'12C Rd 3 Don Julio Gregorio Bagbag Nova QC', NULL, 9667033262, NULL, N'Bruno 2', 20)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AL4771', N'Arabela', N'Lalata', N'Sauyo Rd 3C8 Bagbg Nova QC', NULL, 9081587471, NULL, N'Hershey', 21)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM0835', N'Alberto', N'Mistades', N'B3 LA Princess Homes Bagbag Nova QC', NULL, 9399350835, NULL, N'Choco/3 pups', 22)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM0968', N'Aaron', N'Matucad', N'3 Waling waling St Noman 2 San Bartolome', N'A12matucad@gmail.com', 9565540968, NULL, N'Samy', 23)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM1644', N'Arlyn S', N'Martinez', N'B10 L13 Kingdom I Subd Nova QC', N'arlynmartinez33@yahoo.com', 9057501644, NULL, N'Sky', 24)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM3868', N'Andrei', N'Mortel', N'Pasadena Drive San Juan', NULL, 9178373868, NULL, N'Paquito/Apollo D', 25)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AO0588', N'Alyssa', N'Ong', N'B6 L34 Metrogreen Vill San Bartolome Nova QC', NULL, 9566090588, NULL, N'Oreo', 26)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AO6822', N'Ailee T', N'Olveda', N'160 Fernando Cmpd Sauyo Bagbag Nova QC', N'aileentalagog@gmail.com', 9498486822, NULL, N'Kreamy', 27)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AP5047', N'Aljune', N'Pascot', N'20 Chico St Bagbag Nova QC', NULL, 9310115047, NULL, N'Diosa', 28)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AP5910', N'Angela O', N'Piquero', N'14 Capricorn St Remarville Subd Bagbag Nova QC', N'lablabgola@gmail.com', 9158755910, NULL, N'Shaa', 29)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AP8208', N'Avegail', N'Pigtain', N'B3L28 North Point Subd San Bartolome Novaliches QC', N'apigtain310@gmail.com', 9272788208, NULL, N'Kylo', 30)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AR5920', N'Anna', N'Rebebes', N'39 Condar St RMS II San Bartolome Nova QC', N'akarebebes@gmail.com', 9283665920, NULL, N'Chubby/Charlie', 31)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS1212', N'Alex', N'Saulo', N'B3 L10 Eagle St Rolling Meadows 2 San Bartolome', N'jessyvillrente@yahoo.com', 9328841212, NULL, N'Shadow', 32)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS3931', N'Alberto S', N'Santiago', N'24 Apollo St Remarville Subd Bagbag', NULL, 9177083931, NULL, N'Flash', 33)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS5142', N'Althea', N'Saure', N'512 Ruby St Greenheights Subd', NULL, 9156525142, NULL, N'Macs', 34)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS8138', N'Alfredo', N'Silva', N'615B Callejan Cruz Bagbag Novaliches QC', NULL, 9958708138, NULL, N'Barney/Banjo', 35)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS8481', N'Arlene', N'Samaniego', N'3 Old Paliguan St Remarville Subd', N'len_ael@yahoo.com', 9563168481, NULL, N'Cloud', 36)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS8648', N'Anita', N'Salutan', N'E20 Abbey Rd Bagbag Nova QC', NULL, 9306348648, NULL, N'Sky/Pongky/Toby', 37)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AT0000', N'Angel', N'Tan', N'625 Biglang Awa Cmpd', NULL, NULL, NULL, N'Maxi', 38)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AT1339', N'Anna Marie B', N'Teves', N'178 K Jojo St Baesa', NULL, 9175031339, NULL, N'Skye', 39)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AT3227', N'Anne', N'Tangonan', N'185C King Frederic St Kingspoint Subd', NULL, 9172463227, NULL, N'Gucci/Olex', 40)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AT8601', N'Ann', N'Turtal', N'120 Sauyo Rd Nova QC', NULL, 9171298601, 9295659231, N'Max F', 41)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AU0000', N'Amelia Lea', N'Usi', N'B6 L23 San Pedro Subd', NULL, NULL, NULL, N'Sissy', 42)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BA0000', N'Betsy', N'Adora', N'Remarville Bagbag Remarville QC', NULL, 0, NULL, N'Stephie', 43)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BA0453', N'Beth', N'Altea', N'2 St Anthony Rockville Bagbag Nova QC', NULL, 9359980453, 89389563, N'Buchikoy/Scarlet', 44)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BB4746', N'Benedick', N'Benedictos', N'43 Ero St Bagbag Novaliches QC', N'nedbenedictos.hyc@gmail.com', 9399144846, NULL, N'Bea', 45)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BC0053', N'Brixcel james M', N'Cabrera', N'637 Carreon St Bagbag Nova QC', N'rbreez.006@gmail.com', 9776800053, NULL, N'BM', 46)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BC1965', N'Belen V', N'Cruz', N'Int 16 Armando QC', N'digong@yahoo.com', 9175681965, 4192212, N'Digong', 47)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BC7268', N'Brian Carl', N'Chiu', N'315 K P dela Cruz St', N'briancarlchiu@yahoo.com', 9673167268, NULL, N'Buchog', 48)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BC9166', N'Bing', N'Corro', N'6 Onyx St Rockville Subd San Bartolome Nova QC', NULL, 9983619166, NULL, N'Zion', 49)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BD3124', N'Bennete Mae', N'Dice', N'B15 L1 Phs 1 Noah St cor Abraham St Goodwill 2 Nova QC', N'dice.benette@gmail.com', 9196023124, 9178758758, N'Amber/Shine', 50)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BD4838', N'Bryan Jethro', N'de Mesa', N'B12 L5 March St Goodwill Homes II ', N'bryanjethro.demesa@outlook.com', 9975924838, NULL, N'Toby', 51)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BD5591', N'Binaca', N'del Mundo', N'152 Sauyo Rd Nova QC ', N'delmundo.naida@gmail.com', 9216765591, NULL, N'Keso/Cheese', 52)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BR5922', N'Bambi', N'Rina', N'9 Evangelista St San Bartolome Nova QC', N'bambi.rina@yahoo.com', 9190045922, NULL, N'Ruffie/Rocky', 53)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BS0361', N'Binca', N'Salvador', N'48 Gamma St RTG Vill Nova QC', NULL, 9053770361, NULL, N'Harvey', 54)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA0654', N'Cherry', N'Argallon', N'12 Armando St Bagbag Novaliches QC', NULL, 9777120654, NULL, N'River', 55)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA2771', N'Connie', N'Acejas', N'L6B3 franco St Bagbag Novaliches QC', NULL, 9988582771, NULL, N'Almond', 56)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CB6181', N'Charles', N'Bruan', N'36 Urbano St Bagbag Nova QC', N'nauthycarl@yahoo.com', 9058916181, NULL, N'No Pet Name', 57)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CC4133', N'Charisse', N'Cruz', N'9E Esperanza St Bagbag Nova QC', N'chayeez02@gmail.com', 9669184133, NULL, N'Togo', 58)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CC7582', N'Claudine', N'Castro', N'43 Ero St Seminary Rd Bagbag Nova QC', N'claudinegcastro@gmail.com', 9983620762, 9517157582, N'Max', 59)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CC8664', N'Carl Bryan V', N'Cruz', N'14 Urbano St Bagbag Nova QC', N'carlcruz045@gmail.com', 9266188664, NULL, N'Chuchay', 60)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CD0086', N'Cheryl', N'del Rosario', N'B25 L9 Bistek Nova QC', NULL, 9055260086, NULL, N'Booh', 61)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CD1417', N'Chris', N'del Rosario', N'42 Saminary Rd Bagbag Nova QC', NULL, 9282941417, NULL, N'Perci', 62)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CF1160', N'Clint M', N'Fernandez', N'L5 B4 Sta Barbara Ave Sta Barbara Place Phase 1 Tandang Sora QC', NULL, 9195961160, NULL, N'Paree', 63)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CF2045', N'Carlo', N'Fajardo', N'L5 B13 kathleen Place 4 Nova QC', N'carlo.fajardo.wat2015@gmail.com', 9056632045, 3986733, N'Chow', 64)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CJ0131', N'Cathy', N'Jovilo', N'141 Magsaysay Ext Dona Faustina', NULL, 9999470131, NULL, N'Chichay/chuchay', 65)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CL3434', N'Consolacion R', N'Lopez', N'B2 L23 Capricorn Ave Bagbag Nova QC', NULL, 9102883434, NULL, N'Coycia', 66)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CM3340', N'Christian Dave', N'Mangali', N'B4 L1 St Bernadette St James Nova QC', N'mangali.dave@yahoo.com', 9233543340, NULL, N'Pem', 67)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CO6585', N'Chan', N'Orque', N'16 Job St Goodwill ', NULL, 9293466585, NULL, N'Bingo', 68)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CR0758', N'Charles Nichole O', N'Ramos', N'131 Ibong Pilak St Sta Monica Nova QC', NULL, 9062520758, NULL, N'Maggie', 69)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CR8710', N'Cristine Joy', N'Roque', N'Bagbag Nova QC', NULL, 9777348710, NULL, N'Kisses', 70)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CR9465', N'Chris Evert', N'Resurreccion', N'6 Diamond St Goldhill Homes 599 Quirino Highway', NULL, 9178669465, NULL, N'Zacky', 71)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CS3611', N'Concepcion', N'Sotto', N'L36 B1 Tangili St RP5 Subd Bagbag', NULL, 929683611, NULL, N'Madi/Bella/Brandy/Coco', 72)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CS5820', N'Christian', N'Santos', N'Miracle Homes Kingspoint Subd', N'bhoxzsanto@gmail.com', 9455475820, NULL, N'Sweet', 73)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CT6044', N'Carmena', N'Tomies', N'Paquita St Gulod', NULL, 9391116044, 9260662825, N'Zevei', 74)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CV4739', N'Cerjohn', N'Velasquez', N'19 Marianito St Gulod Nova QC', N'velasquezceejay@gmail.com', 9512254739, 9982840739, N'Jaycee', 75)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CV9687', N'Christine', N'Valenzuela', N'11 Isaiah St Goodwill Homes', NULL, NULL, 9379687, N'Thalia', 76)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CY1972', N'Cejay', N'Yabut', N'L3 B4A Rainbow Homes San Bartolome', NULL, 9178911972, NULL, N'Chibi', 77)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CZ0637', N'Catherine M', N'Zorca', N'7 Belen St San Bartolome', NULL, 9430070637, NULL, N'Atom', 78)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DA4693', N'Daniela', N'Acero', N'Bagbag Novaliches QC', NULL, 9209234693, NULL, N'Bellat/Bailey', 79)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DA9430', N'Darren', N'Anonuevo', N'0625 Kingspoint Rd Bagbag Novaliches QC', N'darrenanonuevo@gmail.com', 9558519430, 9057031070, N'Sanji', 80)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DC0208', N'Derick', N'Calicdan', N'422 Diamond Ave Greenheights Subd San Bartolome Nova QC', N'clc.9207@gmail.com', NULL, 89305848, N'Balot D', 81)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DC7528', N'Dairene', N'Cuya', N'438 C Franco St Bagbag Nova QC', N'dairen.cuya@axa.com.ph', NULL, 89367528, N'Oreo', 82)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DC8740', N'Domingo', N'Cordial', N'625-33 Parokya Bagbag Nova QC', NULL, 9194488740, NULL, N'MingMing', 83)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DD3895', N'Daniel T', N'Discaya', N'B2 L12 Phase 2 Princess Homes', NULL, 9227903895, NULL, N'Rafa', 84)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DD4488', N'Dennis', N'de Jesus', N'23 Remarville Ave Bagbag Nova QC', NULL, 9991044488, NULL, N'Chimmy', 85)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DF7516', N'Dynah', N'Franco', N'Remarville Ave Bagbag Nova QC', N'dynahfranco7@gmail.com', 9065007516, 9466233387, N'Mommy', 86)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DF8259', N'Desiree Joy', N'Fernandez', N'9 St Francisco St San Bartolome Nova QC', N'hashiken1690@gmail.com', 9972278259, NULL, N'Polar', 87)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DG7105', N'Darell', N'Garcia', N'6 Rome Rd San Bartolome Nova QC', N'jamesdarellgarcia@gmail.com', 9178457105, NULL, N'Arwen', 88)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DM1521', N'Dr', N'Mendoza', N'Roque Cmpd Rockville Bagbag Nova QC', NULL, 9173261821, NULL, N'Elvis/Fudge', 89)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DP0270', N'Delex', N'Pharma', N'L4B4 Carnation cor Magnolia St Sauyo QC', NULL, NULL, 4260270, N'Duke', 90)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DP1720', N'Don Jorge', N'Porte', N'Goodwill 2 Bagbag Nova QC', NULL, 9155461720, NULL, N'Chandler/Patty', 91)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DS0281', N'Deanne', N'Soriano', N'15 Pascual Ave Remarville Bagbag QC', N'dean_0222@yahoo.com', 9178030281, NULL, N'Mamba', 92)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DS2414', N'Denise Cathy', N'Sanchez', N'9 Gerry St Remarville Bagbag', N'denisecathy.sanchez@gmail.com', 9086372414, 9454811171, N'Charlie', 93)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DS2783', N'Danny', N'Sy', N'10 San Bartolome', NULL, NULL, 9362783, N'Casper/Louie', 94)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EA0047', N'Euberly', N'Agsaulio', N'29 Babina Cmpd King Christopher', N'eubs_agsaulio@gmail.com', 9398490047, NULL, N'Railey', 95)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EA0451', N'Ellen', N'Aguilar', N'146 G Maligaya St Bagbag Novaliches QC', N'itsajbalbiiin@gmail.com', 9218620451, NULL, N'Stephie/Corona/Covid', 96)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EA7235', N'Elsie', N'Abejuala', N'9 Capricorn St Remarville Bagbag QC', NULL, NULL, NULL, NULL, 97)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EB0530', N'Edison D', N'Buhain', N'B2L4Metrogreen Village Novaliches QC', N'buhainelaine68@gmail.com', 9208970530, NULL, N'Hero', 98)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EB1314', N'Eunice Princess', N'Baladjay', N'5 Lapu Lapu St Doña Faustina Nova QC', N'dok_eunice03@yahoo.com', 9088930314, NULL, N'Star', 99)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EB9158', N'Erica', N'Basilio', N'439 Int 23 Camp Grezar', NULL, 9955019158, NULL, N'Yana', 100)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EC0221', N'Essan', N'Calajatan', N'G51 Abbey Rd Bagbag Nova QC', N'emcalajatan@gmail.com', 9352350221, NULL, N'Avria', 101)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EC2120', N'Eduardo R', N'Catiis Jr', N'9 Sapphire St Rockville 2 Nova QC', N'ercatiis@gmail.com', 9217462120, 8379741, N'Luna', 102)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ED5594', N'Emerson', N'Dawang', N'6 Zircon St Rockville 2 San Bartolome Nova QC', N'emzdawang@gmail.com', 9230825594, NULL, N'Basil', 103)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ED7512', N'Ellalaine', N'dela Cruz', N'031 P dela Cruz St Sitio Gitna Nova QC', N'delacruzellalaine@gmail.com', 9770157512, NULL, N'Mamon', 104)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EF7277', N'Elleonor A', N'Fernando', N'L4A B41 King Rudolf St Kingspoint Subd Nova QC', NULL, NULL, 89367277, N'Luna/Phoebe', 105)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EF8091', N'Emmamuel Joseph A', N'Faigo', N'B11L18K Alibangbang St Amparo Subd Nova QC', N'emmanuelfaigao@gmail.com', 9393900923, 9354098091, N'James', 106)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EG8020', N'Erlinda', N'Garlitos', N'36 Scorpio alley Bagbag Nova QC', NULL, 9953918020, 77456071, N'Coffee', 107)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EM0841', N'Elona', N'Mission', N'Bagbag Nova QC', N'mission_elona@yahoo.com', 9972410841, NULL, N'Stephen', 108)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EM3266', N'Ellaine', N'Magdurulan', N'Gulod Nova QC', N'laine.magdurulan@gmail.com', 9054073266, NULL, N'Koko', 109)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EM4396', N'Evelyn', N'Musngi', N'17 E Aguinaldo St Doña Faustina', N'evelyn_musngi@yahoo.com', 9989514896, 89311196, N'Jack/Andy', 110)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EO1045', N'El John Ray T', N'Oba', N'15 Emerald St GoldHill Homes Bagbag Nova QC', NULL, 9165301045, NULL, N'Honey/Sugar', 111)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EP0383', N'Edyline Gloria M', N'Perez', N'B1 L31 Abraham cor Esay St Goodwill Homes 2 Bagbag Nova QC', N'edsperez325@yahoo.com', 9177150383, NULL, N'Zoey', 112)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EP7055', N'Ellen', N'Padunan', N'L6 B3 Richland V Subd Bagbag', NULL, 9176927055, NULL, N'Chichi', 113)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EP8733', N'Erinnea', N'Pascual', N'L4 B8 Pisces St remarville Subd Bagbag Nnova QC', N'erinnea.pascual@gmail.com', 9353848733, NULL, N'Spike/Marky', 114)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ER0132', N'Ester', N'Rivera', N'190 ACF Homes Holy Cross Nova QC', NULL, 9979050132, NULL, N'Fumashi', 115)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ER0885', N'Elizabeth A', N'Roque', N'4275 St Anthony St Bernardino Cmpd Nova QC Semnary Rd', NULL, 9178119885, NULL, N'Olive', 116)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ER2989', N'Edna', N'Reyes', N'007 Villareal St Gulod Nova QC', N'devy_reyes@yahoo.com', 9216762989, NULL, N'Lilo', 117)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ES0000', N'Erlinda', N'Sabino', N'625 Calejon St Bagbag', NULL, NULL, NULL, N'Collie/Alex', 118)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ES6543', N'Edel', N'Salazar', N'121 Reparo Bagong Lote', NULL, 9661896543, NULL, N'Max', 119)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ES6885', N'Edison', N'Santos', N'25 Aquarius St Remarville Subd', NULL, 9355686885, NULL, N'Rhia', 120)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ES7891', N'Evelyn Samonte', N'Samonte', N'8 Robina Rd Nova QC', NULL, 9382837891, NULL, NULL, 121)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ES9695', N'Erlin', N'Sanchez', N'L14 B6 Sampaguita St Nomar II Subd', NULL, 9985849695, 9053079718, N'Max/Orange', 122)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ET2181', N'Edison', N'Torres', N'701 Sapphire St Greenheights Subd', N'jelorres@gmail.com', 9178452181, NULL, N'Chipper', 123)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EV0447', N'Ezekiel B', N'Valle', N'Arty 1 St Talipapa ', NULL, 9558220447, NULL, N'Ampon', 124)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FA0711', N'Florenda', N'Aragon', N'L1B7 Asteroid St Remarville Novaliches QC', NULL, 9273950711, NULL, N'Caramel/Toby', 125)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FA1444', N'Fe', N'Alejandro', N'439 Camp Grezar St 25D Bagbag Novaliches QC', NULL, 9564331444, NULL, N'Barry', 126)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FA2997', N'Francis', N'Asistin', N'B3L3 Remarville Bagbag Nova QC', N'asiskiko4@gmail.com', 9190012997, NULL, N'Jhaebie', 127)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FC6791', N'Fiona Amanda', N'Coronado', N'Bagbag Nova QC', N'fiona.amanda.corodano17@yahoo.com', 9263356791, 83712279, N'PotPot', 128)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FC6939', N'Fernando A', N'Comillo', N'49 Bernardino Cmpd Bagbag Nova QC', N'waku.ztci@gmail.com', 9272666939, NULL, N'Snow', 129)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FC9226', N'Faye', N'Canton', N'639 Quirino Highway Bagbag Nova QC', N'felicefaye@yahoo.com', 9167569226, 9175770358, N'Chelsea/Charlie/Mochie', 130)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FD2584', N'Frendo', N'Duyao', N'B1L8 Santos Cmpd Cam Grezar St Sauyo QC', NULL, 9195812584, NULL, N'Pororo', 131)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FI1673', N'Fe', N'Ibatuan', N'581 Wings Cmpd Kingspoint Bagbag Nova QC', NULL, 9301541673, NULL, N'Sunshine', 132)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FJ2757', N'Fidel', N'Jimenez', N'7 Ruby St Goldhill Homes Sauyo Nova QC', NULL, 9205572757, 89358271, N'Tyler', 133)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FR5590', N'Florante A', N'Ramos', N'699 Quirino Highway San Bartolome Nova QC', NULL, 9287435590, 4523967, N'Kiki', 134)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FS4825', N'Francis', N'Santos', N'100 P Tupaz St Dona Rosario Nova', NULL, 9321254825, NULL, N'Pepper', 135)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FT1008', N'Ferdinand', N'Tolentino', N'Remarville Bagbag', NULL, 9175501008, NULL, N'Whitey/Whitier/Brownie/Junior', 136)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FT8595', N'Frederick', N'Tabungan', N'Grand Monaco 3 Carreon St', N'erick.tabungan22@gmail.com', 9615148595, NULL, N'Vida', 137)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GB1115', N'Geomer', N'Bornasal', N'Kathleen Place 4 San Bartolome Novaliches QC', N'jepp111589@gmail.com', 9171421115, NULL, N'Lucas', 138)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GB4028', N'Gregian Paul', N'Baltes', N'B12L3 Joshua St Goodwill Homes 2 Bagbag Novaliches QC', N'borriquitos17@gmail.com', 9355544028, NULL, N'Chacha', 139)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GD0152', N'Gie', N'del Mundo', N'Talipapa Nova QC', NULL, 9218700152, NULL, N'Coco', 140)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GE1721', N'Gregdale', N'Edralin', N'6 Gold St Rockville Subd ', NULL, 9063551721, NULL, N'Yoshi', 141)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GF5876', N'Gemma N', N'Ferreria', N'B5 L5 Iajan Townhomes San Bartolome Nova QC', N'gemma@vgmcomputer.com', 9277065876, NULL, N'Scarlet', 142)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GJ4664', N'Jake Robert', N'de Guzman', N'R-C King Christopher St Kingspoint Subd Bagbag Novaliches QC', N'jaroaustdegu@gmail.com', 9083004664, 89529216, N'Nichi/Kiyo/Pluto', 143)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GM3863', N'Grace', N'Morato', N'737 Quirino Highway San Bartolome', NULL, 9185033863, 85420889, N'Covid/Virus', 144)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GR1925', N'Gracel', N'Romero', N'635 Quirino Highway Bagbag Novaliches QC', NULL, 9773091925, NULL, N'Billy/Hershey/Nuggets', 145)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GR2699', N'Gabriella', N'Ramos', N'23 Gold St RockvilleSubd Nova QC', N'gabriellavictorine26@gmail.com', 9171862699, NULL, N'Tenten', 146)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GR4611', N'Gracel', N'Romero', N'635 Quirino Highway Bagbag Novaliches QC', NULL, 9177654611, NULL, N'Billy/Hershey/Nuggets', 147)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GR8416', N'Gloria D', N'Razon', N'17 Emerald St Gold Hill Homes', NULL, 9174848416, NULL, N'Charcoal/Cuchie', 148)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GS4951', N'Gigi', N'Silvestre', N'11 Remarville Ave Bagbag', NULL, 9998874951, NULL, N'Loki', 149)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HB2806', N'Harlene Rose R', N'Bunag', N'B3 L71 Pisces Alley St Goodwill Townhomes Bagbag Nova QC', N'harlenerosebunag@gmail.com', 9178212806, NULL, N'Rogie', 150)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HB4811', N'Harold', N'Borja', N'St james Otelco', NULL, 9223914811, NULL, N'Zoey', 151)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HP8425', N'Haze', N'Perez', NULL, NULL, NULL, 89628425, N'Lady/Basha/Cholo', 152)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HR3866', N'Hahna', N'Rodriquez', N'G47 Abbey Rd Bagbag Novaliches QC', N'hannarodriguez030@gmail.com', 9126433866, NULL, N'Oliver/Choichoi', 153)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HS0408', N'Heidi', N'Start', N'51 G Felix St Remarville Ave', N'grinmachine62799@gmail.com', 92931140408, NULL, N'Luna/Lily', 154)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HS5169', N'Hershey', N'Sotero', N'Ibayo 2 Bagbag', N'babyshy030512@gmail.com', 9086355169, NULL, N'Jamela', 155)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HV7815', N'Helen', N'Villa Carlos', N'5 Bonifacio St Dona Faustino', N'lennec12@yahoo.com', 9178477815, NULL, N'Zyra', 156)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ID8389', N'Ivan', N'del Mundo', N'24 Gamma St RTG Village Nova QC', N'ivangeraldd@gmail.com', 9276688389, NULL, N'Coco', 157)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'II0113', N'Ike', N'Ildefonso', N'383 Mariposa St Gulod', N'kenneth.ildefonso222@gmail.com', 9064680113, NULL, N'Waku', 158)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'IR0411', N'Ivan', N'Ricafort', N'C13 Don Julio Gregorio Bagbat Nova QC', N'chrizzy_jaeym@yahoo.com', 9171940411, 9162377969, N'Coco/Chichi/Boo', 159)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'IR1191', N'Ishi', N'Reyes', N'21 King Henry Kingspoint Bagbag Nova QC', NULL, 9171301191, NULL, N'Kiro/Chucky/Enzo', 160)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'IR9471', N'Ian', N'Rivera', N'190 P dela Cruz San Bartolome Nova QC', NULL, 9274059471, NULL, N'Kobe/Cholo', 161)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'IS1331', N'Irene D', N'Salazar', N'615 Int G Callejon Cruz Bagbag Nova QC', N'irenedjsalazar@yahoo.com', 9178171331, NULL, N'Ayka', 162)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA0000', N'John Edgar', N'Abdon', N'106 Seminary Rd Bagbag Novaliches QC', NULL, 9000000000, NULL, N'Bantay', 163)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA0127', N'John Jodel', N'Antonio', N'15 B Zodiac St Remarville Subd Bagbag QC', N'johnjodel.antonio@gmail.com', 9664420127, NULL, N'Oreo', 164)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA2369', N'John Edison', N'Asistin', N'5 Zodiac St Remarville Subd Bagbag Novaliches QC', N'delrosario.joyce1495@yahoo.com', 9356718352, NULL, N'Louise', 165)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA2490', N'Jade', N'Abaloyan', N'595 Blas Roque Cmpd Bagbag', N'abaloyanjade@yahoo.com', 9214672490, NULL, N'Ellie/Zoey/Cleo/Zoey', 166)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA3791', N'Jeanette', N'Arguelles', N'B3L4 John St Goodwill Homes Nova QC', N'jean_july20@yahoo.com', 9281713791, NULL, N'Daphne', 167)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA7826', N'Jennylyn', N'Aballar', N'5222 Ugong Valenzuela City', NULL, 9219797826, NULL, NULL, 168)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA8274', N'Jennifer', N'Asor', N'Sto Nino Talipapa Novaliches QC', NULL, 9300128274, NULL, N'Rockie', 169)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA9802', N'Jorrell', N'Ang', N'17 Carlos St Montville Novaliches QC', NULL, 9770159802, NULL, N'5 puppies', 170)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB0872', N'Jaime', N'Biglang awa', N'Bagbag', NULL, 9228700872, 9175327788, N'Mitch', 171)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB1898', N'Jahred', N'Bistayan', N'Capricorn St Remarville Subd Nova QC', N'tutsgaming@gmail.com', 9510781898, NULL, N'Ryuta', 172)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB2234', N'Jocelyn', N'Ballon', N'595 Blas Roque St Bagbag Novaliches QC', N'josh20licos@gmail.com', 9282992234, 9167709824, N'Gnocchi', 173)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB2457', N'Joyce Mirasol', N'Boco', N'Bistek Village Novaliches QC', N'mirasolboco0@gmail.com', 9566102457, NULL, N'Sugar', 174)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB2656', N'Joyce', N'Bacquiano', N'B3L18 Philip J North Point Basic P dela Cruz St San Bartolome', NULL, 9175232656, NULL, N'Ginger', 175)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB5978', N'Jhoanalyn', N'Baruzo', N'B4L6 Princess Homes Villa Hermano 3 San Bartolome Novaliches QC', N'joanbaruzo@yahoo.com', 9984545978, NULL, N'Sushi', 176)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB9289', N'Jose Lyn', N'Balbino', N'19 Urcia St Bagbag Novaliches QC', N'nhebalbino03@gmail.com', 9177059289, NULL, N'Patty', 177)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC0714', N'Jose', N'Cabajar', N'Seminaryo Bagbag Nova QC', N'jonevegho@gmail.com', 9653380714, NULL, N'Bruno', 178)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC2515', N'Jerome', N'Cribello', N'Greenheights Subd San Bartolome Nova QC', NULL, 9275952515, NULL, N'Aliah', 179)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC2586', N'Jasmin', N'Cruz', N'2 Damong Maliit Rd Nova QC', N'jasminccruz16@gmail.com', 9177052586, NULL, N'Siby', 180)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC4996', N'Jose P', N'Clemente', N'6 Remarville Ave Bagbag Nova QC', NULL, 9178847817, 9175794996, N'Butter', 181)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC5413', N'Jessica I', N'Costuna', N'190 P dela Cruz St San Bartolome Nova QC', N'costunajessica0627@gmail.com', 9489222407, 9568735413, N'Mia', 182)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC6476', N'Justine', N'Cabanag', N'B44L4 Ph 3 M Roxas St Casimiro Deparo Caloocan City', N'justinecabanag23@gmail.com', 9771406476, NULL, N'Lexie', 183)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC7990', N'Joy', N'Calanoga', N'G9 Abbey Rd Bagbag Nova QC', NULL, 9367557990, NULL, N'Snoopy', 184)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC8749', N'Justin', N'Caguindagan', N'B2L1 Richland Phase V Bagbag Nova QC', NULL, 9166084438, 4560427, N'Sam', 185)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC9783', N'Juele', N'Clemente', N'B9 L20 North Green Subd Nova QC', NULL, 9950839783, NULL, N'Chief', 186)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JD3158', N'Jennelyn Valencia', N'Dizon', N'L1 Apollo St Bagbag Nova QC', NULL, 9273163158, NULL, N'Bandet', 187)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JD4452', N'Jasper angelo', N'Dolhinog', N'30 Sta Maxima Gulod Nova QC', N'jasperangelo.dohinog.GE2019@gmail.com', 9509824452, NULL, N'Kyrie', 188)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JD5663', N'Jojelyn L', N'Damasco', N'20 Pascual Sauyo Nova QC', NULL, 9053675663, NULL, N'Zoey', 189)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JE8446', N'Jessie L', N'Elle', N'L19A B6 Phase 3 Esther St', N'choco.gatas04@gmail.com', 9477168446, NULL, N'Panda', 190)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JF1212', N'Jun', N'Frias', N'13 san Juan St RP Gulod Nova QC', N'friasmaylene@yahoo.com', 9196411212, NULL, N'Ashie', 191)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JF3274', N'Josephine', N'Felongco', N'Bistekville 8 Bagbag Nova QC', NULL, 9773943274, NULL, N'Raffy', 192)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JF7543', N'John Levi', N'Flores', N'7 L17 B9 Goodwill Homes II Daniel St Bagbag Nova QC', NULL, 9210577543, NULL, N'Ramboo', 193)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JG2582', N'Joelle', N'Galapon', N'30 Daisy St San Pedro 9 Novaliches QC', N'joellegalapon@yahoo.com', 9164372582, NULL, N'Margux/Mickey/Bella/Milo', 194)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JG32582', N'Joelle', N'Galapon', N'30 Daisy St San Pedro 9 Nova QC', N'joellegalapon@yahoo.com', 9164372582, NULL, N'Milo/Margaux', 195)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JG4091', N'Jeff', N'Gudelos', N'No. 2 2nd St Carreon Village San Bartolome Novaliches QC', N'jeffgudelos@gmail.com', 9175224091, NULL, N'Goki/Kelvin/Ash/Monmon', 196)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JG5202', N'Josephine', N'Gacutan', N'17 King Christopher Kingspoint', N'joygacutan28.com', 9125645202, NULL, N'Shakira', 197)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JG7401', N'Julie', N'Guinto', N'14 A5 C5 Talipapa Nova QC', N'guintojulie07@gmail.com', 9675467401, NULL, N'Oreo/Roxy', 198)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JJ4793', N'Jessalyn', N'Jagodilla', N'B2 L29 6K Sto Nino Talipapa Nova QC', N'jessalynjagodilla5@gmail.com', 9561824793, NULL, N'Jennie', 199)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JL0011', N'Joana Marie M', N'Lucero', N'28 Maagap St Nova QC', N'seanlucero728@yahoo.com', 9497770011, NULL, N'Piglet/Murit', 200)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JL1323', N'Joan', N'Lumabi', N'A 11 Wings Cmpd Bagbag Nova QC', NULL, 9094311323, NULL, N'Charlie/Coffee', 202)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JL2563', N'Jimmy', N'Llana IV', N'Magno St Seminary Rd Bagbag Nova QC', NULL, 9356002563, NULL, N'Wanda/Mona/Moby/Barbie', 203)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JL2626', N'Justine', N'Lee', N'B11 L14 St Philip san Bartolome Nova QC', NULL, 9173272626, NULL, N'Finesse/Nigo', 204)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JL4601', N'Jacky', N'Lahip', N'23 Apollo Ext Bagbag Nova QC', NULL, 9669014601, NULL, N'Yuki/Sam', 205)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM1216', N'Jonalyn', N'Musngi', N'164 Acme Rd San Bartolome Nova QC', NULL, 9426471216, NULL, N'Thoffer', 206)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM1963', N'Jaysen', N'Morato', N'737 Quirino Highway San Bartolome Nova QC', NULL, 9959241963, 9274731700, N'Asiong/Star/Sky', 207)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM3490', N'Jennily', N'Manuel', N'9 Katipunan Ave California Vill San Bartolome Nova QC', N'jdvy_04@yahoo.com', 9065113490, NULL, N'Pucca', 208)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM3847', N'Jessa', N'Marcellano', N'22 Sapphire St Rockville II Bagbag Nova QC', N'marcellanojessa@gmail.com', 9271503847, NULL, N'Silver', 209)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM4236', N'Judith', N'Mercado', N'10K Tom''s Appt Cleofas St Bagbag Nova QC', N'mercadojudith777@gmail.com', 9456324236, NULL, N'Payat F', 210)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JM5651', N'Jacque', N'Maglalang', N'B4L10 Princess Homes Bagbag Nova QC', NULL, 9228765651, NULL, N'Clark', 211)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP2578', N'Julie', N'Ped', N'625 Biglang Awa Cmpd Bagbag Nova QC', NULL, 9173292578, NULL, N'Summer/Phiphi', 212)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP3456', N'Joy', N'Pelonio', N'Goodwill TownhomesI Bagbag Nova QC', N'joy.pelonio@gmail.com', 9393223456, NULL, N'Lokie', 213)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP3577', N'Jocelyn', N'Peteza', N'Naval St Sauyo Nova QC', NULL, 9077523577, NULL, N'Mucho', 214)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP3675', N'Juliana M', N'Pile', N'L3 B4 Rose St Dona Marciana Subd Ugong Valenzuela City', NULL, NULL, 89212675, N'Coffee', 215)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP4238', N'Jane', N'Pamlilio', N'16 Dona Rosario St Bagbag Nova QC', N'janel0014@gmail.com', 9778374238, NULL, N'Quennie', 216)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP4258', N'Jhun', N'Pacanza', N'427 Victorino St Talipapa', N'jhunpacanza@yahoo.com', 9198174258, NULL, N'Gozim', 217)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP5886', N'Jess', N'Pangan', N'9 Amethyst St Rockville II Bagbag Nova QC', NULL, 9389395886, 89377983, N'Cincy/Cupcake/Bubbles', 218)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP6021', N'Jestoni', N'Perseverndaa', N'G8 Abbey Rd Nova QC', NULL, 9050296021, NULL, N'Kally', 219)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP6967', N'Joy R', N'Paduit', N'45 D susana St Gulod Nova QC', N'djpaduit@yahoo.com', 9184006967, NULL, N'Dachshund', 220)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP7075', N'Jordan', N'Pimentel', N'Unit 1B Osishii5 Rd 7', NULL, 9456797075, NULL, N'Marsha', 221)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP9412', N'Jun', N'Pescante', N'11 sinforosa St Bagbag Nova QC', N'gaupescante2017@gmail.com', 9284779412, NULL, N'Popoy', 222)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JR9305', N'Jennifer J', N'Ramirez', N'16 Gold St Rockville Subd San Bartolome Nova QC', N'jen_jramirez@yahoo.com.ph', 9285059305, 9361467, N'Leumeah/Sydney/Ally', 223)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JS4106', N'Joshoa', N'Situbal', N'Pascualler Vill L2000 Rainbow Homes I Nova', N'situbaljosh@gmail.com', 9955904106, NULL, N'Scott', 224)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JT3163', N'Jecel', N'Tabudlong', N'G L12 El Gine Intervill III', N'asterisk766@gmail.com', 9954503163, NULL, N'Toothless/Ivy', 225)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JT5932', N'Jeffrey', N'Tindugan', N'76 Katipunan St Bagbag', N'jarsen_james12@yahoo.com', 9074675932, NULL, N'Crunchy', 226)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JU2038', N'Jenny P', N'Ubana', N'B2 L6 Princess Homes Bagbag', NULL, 9063382038, NULL, N'Missy/Dos/Lucky ', 227)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JU7944', N'Jessa', N'Udtohan', N'B1 L14 Princess Homes Bagbag', NULL, 9452007944, NULL, N'Yoona/Arlo', 228)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JV6230', N'Joan', N'Villanueva', N'Rockville Ave', NULL, 9366416230, NULL, N'Biyaya/Caloy', 229)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JV9519', N'Jennifer', N'Victorio', N'B25 L21 Efficiency St Goodwill Homes 1 Nova', N'jennifer.victorio78@gmail.com', 9178999519, 9178176577, N'Cha/Yuri', 230)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KA3481', N'Ken', N'Abegonia', N'6 B Magno St Bagbag Nova QC', N'abegoniaken@yahoo.com.ph', 9481293481, NULL, N'Oreo', 231)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KB3532', N'Kristine', N'Balatico', N'626 Quirino Highway Bagbag Novaliches QC', NULL, 9083663532, NULL, N'Covee', 232)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KC0219', N'Kevin', N'Chua', N'85 L43 L46 Rm32 San Bartolome Nova QC', N'kev19hic@yahoo.com', 9165530219, NULL, N'Sky', 233)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KC2850', N'Kristofer', N'Canedo', N'B2 L1 Matthew St Bagbag Nova QC', N'kristofercanedo@gmail.com', 9989752850, NULL, NULL, 234)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KC7581', N'Kristal Gaile', N'Casuncad', N'C-11 Wings Cmpd Bagbag Nova QC', N'kristalgailecasuncad2298@gmail.com', 9199407581, NULL, N'Harley', 235)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KC8457', N'Kriza B', N'Cisnero', N'B3 L24 DH3 Dormitory Nova QC', NULL, 9465735594, 9515948457, N'Fiona', 236)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KD4943', N'Kaye B', N'de Leon', N'0595 Blas Roque Subd Bagbag Nova QC', NULL, 9989394943, NULL, N'Yezcy', 237)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KD7507', N'Kits', N'Diaz', N'2A Onyx Rockville II Subd', N'diaz.kits@gmail.com', 9178227507, NULL, N'Pippa/Pippo', 238)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KE0555', N'Kariz', N'Espeña', N'B1L9 St Michaels Home Subd Bagbag Nova QC', NULL, 9424580555, NULL, N'Ace', 239)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KF9958', N'Karen M', N'Felipe', N'L13 B3 Richland 5 Bagbag Nova QC', NULL, 9084839958, 82850045, N'Snow', 240)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KG5996', N'Kay M', N'Gerobin', N'Kinspoint Subd Bagbag QC', N'kgerobin@gmail.com', 9157735996, NULL, N'Toffee', 241)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KL0877', N'Kurt Louie', N'Lim', N'9 Sagittarius St Remarville Subd Bagbag Nova QC', N'lkurtlouie@gmail.com', 9175400877, 9550349685, N'Covi', 242)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KM3323', N'Kris', N'Monte', N'A23 Dupax Cmpd Bagbag Nova QC', N'krismonte111188@gmail.com', 9476913323, 9217377921, N'Faith', 243)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KO7427', N'Kristina', N'Oliva', N'37 Leon Cleofas St Bagbag Nova QC', N'superkitin@gmail.com', 9087097424, NULL, N'Russell/Loti', 244)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KP4140', N'Katleya M', N'Pamfilo', N'L9 B5 Virgo St Remarville Subd Bagbag Nova QC', NULL, 9988274140, NULL, N'Raffles', 245)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KP7719', N'Kristine Joy', N'Paguinto', N'625 Parokya Rd Bagbag Nova QC', N'kristinejoy.paguinto13@gmail.com', 9129447719, NULL, N'Jolly/Chelsea/Queenie/Trixie', 246)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KR5872', N'Kisses Acie', N'Rueda', N'Gregorio St Biglang Awa Subd Novaliches QC', N'kissesacierueda@gmail.com', 9230815872, NULL, N'Mochie', 247)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KS1139', N'karen de', N'Sagon', N'B9 L1 Daid St Goodwill 2 Bagbag', N'krdesagon@gmail.com', 9565541139, NULL, N'Thalia', 248)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KT2277', N'Kathlee Gail G', N'Tolosa', N'B3 L59 Scorpio Alley Bagbag Nova QC', N'kathleengail_911@yahoo.com', 9956982277, NULL, N'Sky', 249)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KT7352', N'Kathryn May', N'Tomas', N'Good will Homes 2 Phase 1 Bagbag', NULL, 9369497352, NULL, N'Velvet', 250)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KT8589', N'Kris Joy', N'Tabangin', N'19 Venus St Remarville Subd Nova', NULL, 9165888589, NULL, N'Skyler', 251)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KT9722', N'Krisham', N'Tayem', N'B3 L1 Jemar Cmpd Arty II Nova', NULL, 9957469722, 9972687973, N'Chelsea', 252)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KV3289', N'Kristina', N'Valerio', N'14C Magno St Seminary Rd', N'kra.valerio@gmail.com', 9954433289, NULL, N'Yumi', 253)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KY0061', N'Keven Michael M', N'Yadao', N'38 Int Masaya St Gulod', NULL, 9201230061, NULL, N'Stark', 254)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LA6598', N'Leslie', N'Ampo-an', N'19 Venus St Bagbag Novaliches QC', NULL, 9190036598, NULL, N'Masha', 255)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LB2480', N'Leony', N'Bondal', N'B5L4 Phase I Princess Homes Seminary Rd Bagbag', NULL, 9298002480, NULL, N'Covee', 256)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LB6373', N'Leonardo', N'Basmayor', N'B5L2-A King Charles St Kingspoint Subd Nova QC', N'mclplats@gmail.com', 9089666373, NULL, N'JR', 257)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LB8886', N'Lydia', N'Balistoy', N'624 E Quirino Highway Bagbag Novaliches QC', N'balistoy_lydia@yahoo.com', 9675038886, NULL, N'Chloe', 258)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LC8375', N'Loveth Mae', N'Camanga', N'B10A L30 Sapphire St Green Heights QC', NULL, 9209818375, NULL, N'Baby', 259)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LG3184', N'Lester', N'Guiral', N'47 B Don Julio Gregorio St Sauyo Nova QC', N'wonglester593@gmail.com', 9952213184, NULL, N'PowPow', 260)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LG3373', N'Leah', N'Galvez', N'019 A Urcia St Alley II Seminaryo Rd Bagbag Nova QC', N'melay22@gmail.com', 9338203373, NULL, N'Lucky', 261)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LG6824', N'Liezel', N'Gumboc', N'34 King Alexander St Kingspoint Subd Bagbag Nova QC', NULL, 9778216824, NULL, N'Gigi/Michie/Zoomie', 262)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LJ6668', N'Lara', N'Jacinto', N'25 Virgo St Remarville Bagbag Novaliches QC', NULL, 9054616668, NULL, N'Little/Piper/Vibe/Snowy/Milky/Santino/Choco/Helga/Burge/Chariole/Bethoven', 263)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LL2022', N'Lea', N'Lopez', N'B2 L11 St Michael Homes Seminary Rd Bagbag Nova QC', N'lopezleapollen@gmail.com', 9066702022, NULL, N'Sammy/Dua/Mimi', 264)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LP0000', N'Leonard', N'Padrigo', N'595 Blas Roque San Jose Cmpd Bagbag Nova QC', NULL, 0, NULL, N'Chai', 265)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LP0845', N'Leah', N'Pabaya', N'Pabaya Cmpd King Christian Kingspoint', NULL, 9678040845, NULL, N'Ruru/Riri/Piper', 266)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LP9352', N'Lawrence', N'Padilla', N'40 Remarville Bagbag Nova QC', N'yanggols1000@gmail.com', 9772559352, NULL, N'Kana', 267)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LR6106', N'Lorenzo', N'Rosco', N'L6 B15 Rainbow Homes 2 Nova', N'renzchie21@gmail.com', 9667746106, NULL, N'Brandy/Bradpit', 268)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LS3131', N'Leslie', N'Saavedra', N'8 King Christopher Kingspoint', NULL, 9087083131, NULL, N'Pempem', 269)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LS8238', N'Louis Anthony', N'Sanchez', N'572 H Land 5 Bagbag Nova QC', N'louis17sanchez@gmail.com', 9277608238, NULL, N'Pupper', 270)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LS8633', N'Lady Lyn', N'Serdon', N'B4L10 Naval St Sauyo Nova QC', N'ladylynserdon@gmail.com', 9214188633, NULL, N'Mini F', 271)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LT 3314', N'Lordes', N'Tomado', N'Goodwill Homes Bagbag Nova QC', NULL, 9192933314, NULL, N'Julio D', 272)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LT7188', N'Lorden', N'Tiu', N'Sta Barbara Place Tandang Sora QC', N'tiulorden@gmail.com', 9778117188, NULL, N'Jiro', 273)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA1773', N'Melanie', N'Angeles', N'15 Evangelista St Novaliches QC', NULL, 9216371773, NULL, N'Yasou', 274)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA2551', N'Mae Rachel Shaela', N'Apatan', N'6 Eleuteria St gulod Novaliches QC', N'maedingle@gmail.com', 91719902551, NULL, N'Aki', 275)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA2613', N'Michael', N'Alebuche', N'661 Llano Rd Caloocan City', NULL, 9154812613, NULL, N'Zebi', 276)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA3011', N'Mickey', N'Amoncio', N'625-32 Parokya  ng Pagkabuhay Rd Bagbag Novaliches QC', NULL, 9269503011, NULL, N'maxine/Cromie', 277)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA7909', N'Maybellyn', N'Aparato', N'B8L21 Metrogreen Vill San Bartolome Novaliches QC', NULL, 9052217909, NULL, N'Momoca', 278)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MB1444', N'Maxine', N'Bernardino', N'49 Seminary Rd Bagbag Novaliches QC', NULL, 9196701444, NULL, N'Rawstar', 279)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MB3092', N'Melanie', N'Bernardino', N'49 Seminary Rd Bagbag Novaliches QC', NULL, 9178013092, NULL, N'Summer', 280)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MB4150', N'Mark Jhun', N'Balatayo', N'G9 Abbey Rd Bagbag Novaliches QC', NULL, 9291644150, NULL, N'Bruce', 281)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MC2339', N'Mary Ann F', N'Cañero', N'L4 B2 Gozum St Saint James', NULL, 9324562339, NULL, N'Maxine', 282)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MC4347', N'Maggie', N'Cristobal', N'27A Palmera St Villa Florencia Subd', N'mggcrstbl@gmail.com', 9260294347, NULL, N'Vale', 283)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MC5940', N'Ma Deby', N'Calumpag', N'169 Sauyo Rd Nova QC', NULL, 9099345940, NULL, N'Snok', 284)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MC7923', N'Mark', N'Canilao', N'G7 Abbey Rd Bagbag Nova QC', NULL, 9771727923, NULL, N'Vandolph', 285)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD0203', N'Mary Jane', N'de Vera', N'439 Int 24 Camp Grezar Bagbag Nova QC', N'jhanadvera@gmail.com', 9163880203, NULL, N'Jacob/Yohan', 286)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD0355', N'Ma Cecilia', N'Dimas', N'121 King Constantine Ext Kingspoint Subd Bagbag Nova QC', NULL, 9233540355, NULL, N'Chamby', 287)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD4680', N'Mary Grace', N'Diño', N'6 Lung Christian St  Kingspoint Subd Bagbag QC
B50 L5 Rolling Meadows 3B Brgy San Bartolome Nov QC', N'mgracedino@gmail.com', 9178424680, 84176567, N'Chichi', 288)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD4926', N'Matt Daryll', N'de Guzman', N'B7 L22 Dahlia St  Nomar I Nova QC', NULL, 9286764926, NULL, N'Ghost', 289)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD8239', N'Marianne Loraine', N'del Castillo', N'Unit 3 Adelle 4 Santiago Subd Bry Sta Monica Novaliches QC', N'cslanne.dc@gmail.com', 9159308239, NULL, N'Chloei', 290)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD9004', N'Mirasol', N'David', N'Green Heights San Bartolome', NULL, 9953549004, NULL, N'Cloud', 291)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MD9228', N'Ma Jerusalen N', N'de Luna', N'Don Julio Gregorio St Bagbag Nova QC', NULL, 9611819228, NULL, N'Lauran', 292)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MF7256', N'Ma Lourdes A', N'Fullero', N'Rockville I', NULL, NULL, 4197256, N'Kitty', 293)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MF9169', N'Marjorie', N'Francisco', N'500 Karaan St Bagbag Nova QC', N'khimkhaekhim004@gmail.com', 9955879169, NULL, N'Tanya/Bakkie', 294)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MG0000', N'Mel', N'Galecio', N'625-7 Parokya', NULL, NULL, NULL, N'Gooffie', 295)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MI2137', N'Mrgene C', N'Igup', N'595 Blas Roque Cmpd Bagbag Nova QC', N'mergen.igup@gmail.com', 9958252137, NULL, N'Lexi', 296)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML0582', N'Mrs', N'Luzuriaga', N'33 Topaz St Rockville II', NULL, 9998590582, NULL, N'Petunia/Aska/Sky', 297)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML1660', N'Michelle', N'Lamparero', N'4 Magno St Bgbag Nova QC', NULL, 9088971660, NULL, N'Samchie', 298)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML1719', N'Ma Paola C', N'Lema', N'B5 L9 Virgo St Remarville Subd Bagbag Nova QC', N'tayls_19@yahoo.com', 9428311719, 9177213121, N'Chester', 299)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML2933', N'Maylene B', N'Lopez', N'175 0dela Cruz St San Bartolome Nova QC', NULL, 938552933, NULL, N'Thunder', 300)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML3325', N'Michelle', N'Lacadin', N'Gregor Cmpd Bagbag Nova QC', NULL, 9367103325, NULL, N'Choco', 301)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML3463', N'Mercy', N'Labordo', N'B5L5 James St', NULL, 9161663463, NULL, N'Ranch', 302)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML4669', N'Marwyn Albert V', N'Lacabra', N'430 D Ero Bernardino St Bagbag Nova QC', N'wyncg@yahoo.com', 9156474669, 9406937, N'Chichay', 304)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ML7889', N'Mary Doreen', N'Llamas', N'Goodwill 1 Subd San Bartolome Nova QC', N'doreen.llamas09@gmail.com', 9653417889, NULL, N'Boomer', 305)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM0962', N'Milan', N'Mendoza', N'7A Novility St New Capitol Estate II Batasan Hills', N'ts_asiana@yahoo.com', 9267000962, NULL, N'Pancake', 306)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM1582', N'Michael', N'Morales', N'Virgo St Remarville Bagbag Novaliches QC', N'asianightangel@yahoo.com.ph', 9178231581, NULL, NULL, 307)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM3216', N'Marte', N'Megallon', N'No 9 F Biglang Awa St Bagbag Nova QC', N'mary_Megallon@yahoo.com', 9420603216, NULL, N'Milo', 308)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM3334', N'Marianne Leny S', N'Maquiddang', N'6 Carreon Vill San Bartolome Nova QC', N'maquiddangjaerald@gmail.com', 9435443334, NULL, N'Talee', 309)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM5126', N'Mrs', N'Manlapaz', N'57 Kayalaan Rd ', NULL, 9086435126, NULL, N'Chichay Oreo', 310)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM7969', N'Melody', N'Manalang', N'120 Sauyo Rd Nova QC', NULL, 9323744969, NULL, N'Pia', 311)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM8067', N'Marife', N'Morandante', N'B3L3 Goldhill Homes Bagbag Novaliches QC', NULL, 9178598067, NULL, N'Max/Angel/Frankie/Katkat', 312)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM9990', N'Mharu', N'Maliwat', N'C25 Int Bagbag Nova QC', N'mharu02122@gmail.com', 9283979990, NULL, N'Sadie', 313)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MO6388', N'Melanie', N'Osiana', N'7 Gerry St Remarville Bagbag Nova QC', NULL, 9174266388, NULL, N'Blue', 314)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP0234', N'Michael M', N'Pascual', N'39 Leon Cleofas St Bagbag Nova QC', N'michaelpascual@yahoo.com', 9312000234, NULL, N'Gucci', 315)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP8625', N'Marivic B', N'Palomares', N'L8 B7 Virgo St Remarville Subd Bagbag Nova QC', N'palomares_marivic@yahoo.com', 9772338625, NULL, N'Pechay', 316)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP9718', N'Mia', N'Perez', N'B2 L44 Aquarius St Goodwill TH I Bagbag Nova QC', NULL, 9982969718, 9271312056, N'Lilly/Ashley', 317)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP9847', N'Ma Theresa', N'Pineda', N'B2 L28 West Wing Residences North Belton Talipapa Nova QC', N'pinda.theresa@yahoo.om', 9157099847, 8758485, N'Gucci/Hermes/Prada/Namie', 318)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MQ8732', N'Mark', N'Quiambao', N'594 Bagbag Nova QC', N'quiambaomarklouie@gmail.com', 9994108732, NULL, N'Fiery', 319)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR1377', N'Ma Rosie', N'Rosales', N'B3 Cchai St Niño Talipapa Nova QC', N'ra739706@gmail.com', 9424091377, NULL, N'5 puppies', 320)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR1391', N'Maria Rose', N'Rostrata', N'20 Pagkabuhay Rd Bagbag Rovaliches QC', NULL, 9171541391, NULL, N'Pepper/Cotton/Clover', 321)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR6215', N'Michelle', N'Rodriguez', N'99 Sauyo Nova QC', NULL, 9338276215, NULL, N'Powee', 322)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR7083', N'Melody', N'Refran', N'Talipapa Nova QC', N'cauilanmelody@gmail.com', 9177057083, 9260926346, N'Sinag Tala', 323)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR7164', N'Myles Chellsy', N'Rosales', N'38 Sinforosa St Bagbag', N'myttantarou@gmail.com', 9568737164, NULL, N'Gigi', 324)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR7530', N'Mia', N'Ramores', N'635 Seminaryo Lazaro Cmpd Bagbag Nova QC', N'miamengramores@gmail.com', 9979267530, NULL, N'Chuchu', 325)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS1771', N'Michael G', N'Sanchez', N'L9 B1 Blue Bird St Rainbow I Nova QC', N'rtps19ue@gmail.com', 9228091771, NULL, N'Sonjo', 326)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS3566', N'Maimai', N'Sarmiento', N'RT Gonzales Subd ', NULL, 9613383566, NULL, N'Shikoku/Hachiko', 327)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS4162', N'Mary Ann', N'Seguin', N'22 Gozum St QC', N'seguinmaryann@gmail.com', 9178734162, NULL, N'Butter', 328)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS4208', N'Ma Clarissa', N'Sicat', N'34 C Silver St Rockville Subd San Bartolome Novaliches QC', N'clarissasicat13@gmailcom', 9568824208, NULL, N'Snow/Sky', 329)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS4535', N'Mary Grace', N'Serrano', N'35 san Agustin St RP Gulod Nova', N'marygraceserrano99@yahoo.com', 9278314535, NULL, N'Champ/Charlie/Basita/Black/Jack/Mikee', 330)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS4548', N'Maylen', N'Salvador', N'175 P dela Cruz St San Bartolome', N'jamaimj2027@gmail.com', 9261624548, NULL, N'Pepper', 331)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS4840', N'Marc Allen C', N'Salem', N'541 Talipapa Nova QC', NULL, 9387724840, NULL, N'Kitkit/Katkat', 332)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS5953', N'Myrra', N'San Miguel', N'B3 L12 Diamond St Goldhills Bagbag', NULL, 9278015953, NULL, N'Cas', 333)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MT9205', N'Mery Ana', N'Tonio', N'Mary Anne Residences Sauyo', N'mttonio@yahoo.com', 9333739205, NULL, N'Lucky', 334)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MU9325', N'Monette', N'Umlas ', N'12 Seminary Rd Bagbag', NULL, 9154829325, NULL, N'Snowee/Patty', 335)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MV8325', N'Mylle Warlmer', N'Viernes', N'106 Seminary RD Bagbag', N'ramosmylle28@gmail.com', 9198118325, NULL, N'Marshall', 336)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MV8777', N'Melody M', N'Viaña', N'B3 L11 Ruth St Ph 3 Goodwill 2', NULL, 9294168777, NULL, N'Jackie', 337)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NB0917', N'Nicole Dufney', N'Bercadez', NULL, N'dufneybercadez1@gmail.com', 9155100917, 4173674, N'Gigi', 338)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NB3152', N'Nilo', N'Bande', N'105 Remarville Subd Bagbag Novaliches QC', NULL, 9209163152, NULL, N'Loko/Bruno/Batman/Bacon/Cat Stitch', 339)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ND2611', N'Neil Aedrin', N'de Vera', N'2 Marcos St Doña Faustina Nova QC', N'neilaedrindevera@gmail.com', 9560332611, NULL, N'Atlas', 340)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ND3618', N'Norberto', N'Dineros', N'Grand Monaco 3', N'nonoybanez15@gmail.com', 9399363618, NULL, N'Persia', 341)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ND8180', N'Nicole M', N'Dava', N'90 Leon Cleofas St Bagbag Nova QC', NULL, 9296128180, NULL, N'Porsia', 342)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ND9508', N'Niña', N'de Castro', N'B8 L8 Aquarius St Remarville Subd Bagbag Nova QC', N'ninsarahd@yahoo.com', 9177939508, NULL, N'Cobe/Cookie', 343)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NF4242', N'Nico', N'Flores', N'B12 L4 Mocking Bird St Rainbow Homes I', NULL, 9289834242, 9373905, N'Bruno', 344)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NG0434', N'Nemesia G', N'Guanzon', N'15 Libra St ACF Homes Holy Cross Nova QC', NULL, 9214780434, NULL, N'Rio', 345)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NL2761', N'Nestor', N'Lim III', N'Gulod Nova QC', NULL, 9064122761, NULL, N'Cloud', 346)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NP0676', N'Nery Joy', N'Patena', N'625 Biglang Awa St Bagbag Nova QC', NULL, 9190020676, NULL, N'Yuri Yumi Yoshi', 347)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NP1667', N'Norman', N'Pulido', N'48 St Anthony St RP Nova QC', NULL, 9561351667, 9338120996, N'Coca', 348)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NQ1455', N'Nicah', N'Quirante', N'44 Diamond St Goldhill Homes Bagbag Nova QC', N'scottsmarlo@hotmail.com', 9399221455, NULL, N'Milky', 349)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NS4310', N'Niel', N'Santos', N'13 A Bisugo St Nova QC', NULL, 9267254310, NULL, N'Whiley Saturn', 350)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'OT7396', N'Ophelia', N'Talacay', N'L17B1 Dona Thomasa Subd San Bartolome Novaliches QC', N'ophelia.talacay@gmail.com', 9752307396, NULL, N'Kiko', 351)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PA8611', N'Patricia Ann', N'Blas', N'B6L9C Ruth St Goodwill Homes II Bagbag Novaliches QC', N'patriciannblas@gmail.com', 9323058611, 9998788011, N'Kobe', 352)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PA9432', N'Pamela April', N'Aldana', N'B5 L5 Princess Homes Goldrey Bagbag Nova QC', N'aldana.april15@gmail.com', 9156069432, NULL, N'Milo', 353)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PB2434', N'Pia', N'Banaga', N'240 Amethyst Ave Nova QC', N'piaeyemd@gmail.com', 9178972434, NULL, N'Coolit/Tabby', 354)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PB3908', N'Princess', N'Budoso', N'645 Unit P Quirino Highway Bagbag Nova QC', N'yxacesz@gmail.com', 9234573908, NULL, N'Mikko/Yumi', 355)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PC0926', N'Patrick', N'Claviolo', N'8 King Henry St Kingspoint Sauyo Nova QC', N'claviolo@gmail.com', 9177910926, NULL, N'Cookie', 356)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PC3759', N'Paulo', N'Cortez', N'625 Int Biglang Awa Cmpd', NULL, 9216073759, NULL, N'Edward PTS', 357)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PD5581', N'Princess S', N'Dillinger', N'51 B Ilang Ilang St San Pedro 9 Subd', NULL, 9455475581, NULL, N'Snow', 358)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PD6361', N'PJ', N'David', N'Topaz St Rockville 2 Subd Bagbag Nova QC', NULL, 9959616361, NULL, N'Wipe Out Ayuda', 359)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PD6879', N'Paulo', N'Denosta', N'B8 L20 Rolling Meadows I', NULL, 9275646879, NULL, N'Popoy', 360)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PG7690', N'Pretzel  E', N'Guamos', N'42 diamond St GoldHill Homes Bagbag Nova QC', N'pretzel_guamos@yahoo.com', 9491727690, 9491727690, N'Kauri', 361)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PG9201', N'Percy', N'Gupit', N'B5L24 White Duck St rainbow Homes 1 San Bartolome Novaliches QC', NULL, 9552109201, NULL, N'Percy', 362)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PH7075', N'Paul', N'Herrera', N'12 Seminary Rd Bagbag Novaliches QC', NULL, 9973557075, NULL, N'Beany/Beauty', 363)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PL8447', N'Patrick Joshua', N'Lazaro', N'19 Carreon St Bagbag Nova QC', NULL, 9564078447, NULL, N'Charlie', 364)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PL8882', N'Pia', N'Llagado', N'631 Quirino Highway Bagbag Nova QC', NULL, NULL, NULL, N'Pixie', 365)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PQ7836', N'Paulo', N'Quilaton', N'12 Urcia St Seminary RD BAgbag Nova QC', NULL, 9977497836, NULL, N'Sophia', 366)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PS5428', N'Pia', N'Sadora', N'Swallow St St Francis Village San Bartolome', NULL, 9058795428, 9235043365, N'Bugzy/Dafny', 367)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PT1810', N'Patric', N'Teves', N'11 macapagal St San Bartolome ', N'pteves32@gmail.com', 9150771810, NULL, N'Logan', 368)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PV7898', N'Princess Hazel', N'Vargas', N'120 Magnolia St St Dominic 9 Nova QC', N'cessvargas30@gmail.com', 9155437898, NULL, N'Kaspersky/Dyno', 369)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'QC4998', N'Quiel', N'Crescini', N'8 Sta Eleuteria St RP Gulod', NULL, 9063034998, NULL, N'Lita', 370)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA1673', N'Romar', N'Apon', N'220 P dela Cruz St San Bartolome Novaliches QC', NULL, 9303951673, NULL, N'Charvi', 371)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA2243', N'Rowena', N'Abadenis', N'Blas Roque Bagbag Novaliches QC', NULL, 9086722243, NULL, N'Haruko', 372)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA2288', N'Rey', N'Allarey', N'B4L24 Princess Homes 2', N'reyallarey1208@gmail.com', 9214632288, NULL, N'Hunter', 373)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA9413', N'Rey', N'Alejo', N'San Bartolome Novaliches QC', N'meddybscs@gmail.com', 9179089413, NULL, N'RC', 374)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RB2819', N'Roselyn', N'Belen', N'B5L8 C King Charles St Kingspoint Sauyo', NULL, 9352172819, NULL, N'Ella', 375)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RB5251', N'Ria Nicole', N'Balagtas', N'615 Quirino Highway Bagbag Novaliches QC', NULL, 9778435251, 2811072, N'Reese', 376)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RB7282', N'Romenich O', N'Balauag', N'38A Urcia Street Bagbag Novaliches QC', NULL, 9398197282, NULL, N'Sushi', 377)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RB8406', N'Roger Val', N'Bernasor', N'35 Emerald St Goldhill Homes Subd Sauyo Bagbag QC', N'rcbernasor@yahoo.com.ph', 9479808406, 9165573482, N'King', 378)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RC2013', N'Remie', N'Cabral', N'439 Camp Grezar Bagbag Nova QC', NULL, 9597442013, NULL, N'Primo', 379)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RC3710', N'Rowena', N'Castro', N'129 Katipunan Kaliwa Bagbag', NULL, 9094133710, NULL, N'Karuki', 380)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RC5170', N'Ruth', N'Crisostomo', N'4 Carreon St Bagbag Nova QC', NULL, 9397185170, NULL, N'Juna F', 381)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RC7101', N'Renan', N'Cauilan', N'2 Emerald St Odelco Subd San Bartolome Nova QC', N'rscauilan@gmail.com', 9166527101, NULL, N'Aboo', 382)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RC8351', N'Roberto T', N'Chan', N'52 Mangga St Green Acres Subd San Bartolome Nova QC', NULL, 9271628351, NULL, N'Coco', 383)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RD3996', N'Dexter', N'Riamico', N'27 H Magno St Bagbag Nova QC', NULL, 9164963996, NULL, N'Shanel', 384)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RD9758', N'Rolando E', N'dela Cruz', N'1 Ruby St Rockville Subd San Bartolome Nova QC', N'rollydc@gmail.com', 9178209758, 89361104, N'Mocca', 385)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RF5067', N'Richelle', N'Feliciano', N'39 Magsaysay St Doña Faustina San Bartolome Nova QC', NULL, 9064505067, NULL, N'Zoey', 386)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RG3341', N'Rosalee', N'Guevara', N'35 Calumpit St Proj 7 QC', NULL, 9274863341, NULL, N'Ashley', 387)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RG3835', N'Raquel M', N'Gonzales', N'By L4 SPS7 Saint Philip St san Bartolome Nova QC', NULL, 9475893835, NULL, N'Britney', 388)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RG5382', N'Romer Carlo', N'Gacusan', N'B2L47 Goodwill T/H Bagbag Nova QC', NULL, 9054545382, NULL, N'Blink', 389)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RG5754', N'Ruben', N'Guerra', N'A38 Dupax Cmpd Bagbag Nova QC', NULL, 9297055754, NULL, N'Kamukamo', 390)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RG7385', N'Riza', N'Gonzales', N'39C Sinforosa Ext', N'rizacarmelagonzales@gmail.com', 91011397385, NULL, N'Shata', 391)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RI4803', N'Raniev', N'Inocencio', N'San Pedro 7 Subd San Bartolome Nova QC', NULL, 9065743803, NULL, N'Beauty', 392)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RL1630', N'Riza', N'Liton', N'G32 Abbey Rd St Bagbag Nova QC', N'rizaliton@yahoo.com', 9393711630, 9297098919, N'Bimbim/Bonzai/Bunso', 393)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RL1836', N'Roi', N'Laurente', N'B13 L14 Isaiah St Nova QC', NULL, 9194991836, NULL, N'Autumn', 394)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM2096', N'Ronel', N'Moncay', N'14C Magno St Semenary Rd Bagbag Nova QC', NULL, 9271312096, NULL, N'Lucky/Amber', 395)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM3631', N'Rozelgen', N'Manzanero', N'20 Katipunan Kaliwa Bng Nova QC', N'winrosemanzanero', 9612283631, NULL, N'LUna', 396)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM3657', N'Rocky C', N'Miguel Jr', N'B14 L6 Isaiah St Goodwill Homes 2 Nova QC', N'rockymigueljr100987@gmail.com', 9959383657, NULL, N'Jaycel', 397)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM7713', N'Red Idon D', N'Marquez', N'74 Carlos St San Bartolome Nova QC', NULL, 9323987713, NULL, N'Kov', 398)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM8782', N'Ruel', N'Mercado', N'B15 L14 Goodwill Homes PII', NULL, NULL, 9368782, N'Tisay', 399)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RN9487', N'Rizza', N'Nuñez', N'King Christian St Kingspoint Bagbag Nova QC', N'rizzasuarez958@gmail.com', 9204139487, NULL, N'Roby/Moimoi/Sam', 400)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RO6996', N'Regine', N'ordonez', N'39 Maligaya St Nova QC', NULL, 9152336996, NULL, N'Mitchie', 401)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RP0137', N'Remy', N'Pangilinan', N'Unit 3 L1B2 Abraham St Goodwill Homes 2 QC', NULL, 9271580137, NULL, N'Max/Scarlet', 402)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RQ1708', N'Rosalinda M', N'Qiebada', N'B8 L6 Martin St RMS II', N'quebadakyrillie@gmail.com', 9564961708, NULL, N'Agatha', 403)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RR4512', N'Rose Ann', N'Ramoy', N'36 Leon Cleofas St Ibayo I Bagbag Nova QC', N'raramoy@yahoo.com', 9255464512, NULL, N'Bella', 404)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RR9991', N'Rona', N'Royales', N'10 Rita St Goodwill II Bagbag', NULL, 9176749991, NULL, N'Cheenie', 405)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS0000', N'Roy', N'Sestoso', N'2 Florville St Tandang Sora QC', NULL, NULL, NULL, N'Pinggo/Cera', 406)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS0058', N'Robert', N'Samaniego', N'B2 L1 St James St SPS VII', N'Linepriros@yahoo.com', 9176310058, NULL, N'Shea/Oreo', 407)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS0543', N'Ricky', N'Sampoang', N'625-54 Biglang-awa St Bagbag Novaliches QC', NULL, 9063320543, NULL, N'Fendy', 408)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS1886', N'Rica', N'Serrano', N'Rd 2 Don Julio Gregorio St Bagbag Novaliches QC', N'ricapujedaserrano@gmail.com', 9161681886, NULL, N'Lucas', 410)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS1960', N'Rolly', N'Santos', N'San Bartolome Nova', NULL, 9177761960, NULL, N'Clark/Baste/Thor', 411)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS3912', N'Rolly', N'Sy Jr', N'102 Remarville Ave Bagbag', NULL, 9773803912, NULL, N'Choco/Chomita', 412)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS5019', N'Romae', N'San Jose', N'625-20  Isaac Carreon St Bagbag', N'sanjoseromae29@gmail.com', 9293005019, NULL, N'Raiko', 413)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS5372', N'Ryan', N'Sarucam', N'52 Villareal St Gulod Nova', N'sarucamryan@gmail.com', 9672305372, NULL, N'Fendy', 415)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS7302', N'Ruby Ann', N'Santos', N'581 Wings BAgbag', NULL, 9958437302, 9155803286, N'Pepper', 416)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RT1318', N'Rolando', N'Tanglao', N'Sta Quiteria ', NULL, 9176541318, NULL, N'Kobe', 417)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RT8420', N'Ric', N'Tenoso', N'74 Libra St ACF Homes San Bartolome', N'ric.tenoso@yahoo.com', 9178328420, NULL, N'Bane', 418)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RU8190', N'Rnnie', N'Umlas', N'24 Seminary Rd Bagbag', NULL, 9128188190, NULL, N'Doggo/Choco', 419)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RV1056', N'Rosalia', N'Viado', N'582 Quirino Highway Bagbag', NULL, 9616121056, NULL, N'Madie', 420)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RV1751', N'Rachel', N'Vergonia', N'No 2  2nd St Carreon Village Holy Cross', N'rachel.vergonia1@gmail.com', 9190071751, NULL, N'Paquito', 422)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RV4896', N'Rosalina', N'Viray', N'24A Nitang Ave Gulod Novaliches QC', NULL, 9951614896, NULL, N'Patty', 423)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SC2773', N'Sonny', N'Ciano', N'E25 Abbey Rd Bagbag Nova QC', NULL, 9056636938, 9195102773, N'Hero/Hera', 424)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SD2737', N'Sarah', N'Dichosos', N'Joshua St Bagbag Novaliches QC', N'sarahangelica_dichoso@yahoo.com', NULL, NULL, NULL, 425)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SF3144', N'Shaina Dhel', N'Foliente', N'15 Leon Cleofas St Seminary Rd Bagbag Nova QC', N'shanhayey27@gmail.com', 9167843144, NULL, N'Aki', 426)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SL0237', N'Stephen Keith', N'Lubrico', N'1234 The Avenue Residenes Tandang Sora QC', N'stephenkeithlubrico@gmail.com', 9208250237, NULL, N'Hera/Ochie', 427)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SM3716', N'Sonia', N'Miranda', N'B5 L22 Mterogreen Vill san Bartolome Nova QC', NULL, 9272823716, 9196487170, N'Choa/Sugar', 428)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SM4810', N'Shayne', N'Malto', N'595 B1 L11 Blas Roques Bagbag Nova QC', N'shaynemalto@gmail.com', 9203094810, NULL, N'Coco', 429)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SO0494', N'Sharon', N'Ong', N'637 E Carreon St 19 Lazaro Cmpd', NULL, 9182830494, NULL, N'Zoe/Gab', 430)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SP0184', N'Sally', N'Panuda', N'B9L12 Metrogreen San Bartolome Nova QC', NULL, 9189030184, 4170019, N'Sasha', 431)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SP8628', N'Sannshyne', N'Paiso', N'RP Shooters Gulod Nova QC', NULL, 9212188628, 3985870, N'Simba D', 432)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SR1228', N'Shalie', N'Rabida', N'315 P dela Cruz St San Bartolome Nova QC', NULL, 9216361228, NULL, N'Matti', 433)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS1130', N'Salvie', N'Santos', N'25 Aquarius St', N'salviemorales@yahoo.com', 9068751130, NULL, N'Zoey', 434)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS5014', N'Sanora', N'Santos', N'B18 L14 Isaiah St Goodwill 2 PI Bagbag', N'akosialisa@gmail.com', 9173065014, 9366987, N'Pompoms', 435)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS6171', N'Shiela Marie', N'Serrano', N'Unit 10-11 IF Bldg A Bistek Ville 9 nenita St Gulod Nova', N'aleish.02@gmail.com', 9154226171, NULL, N'Eros', 436)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS7405', N'Shaznay P', N'Suva', N'44 S Francisco San Bartolome ', N'shaznaysuva20@gmail.com', 9324227405, NULL, N'Chloe', 437)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS8457', N'Shaira Mae', N'Sansano', N'16 Jonas St Goodwill Novaliches QC', N'sansanoshairamae1@gmail.com', 9363398457, NULL, N'Nada', 438)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS8742', N'Skyla', N'Simeon', N'17 Monarda St St Dominic Subd Talipapa', NULL, 9394108742, 3695785, N'Stark', 439)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SY8162', N'Shane', N'Yabut', N'Ramos Cmpd Holy Cross', N'romeryabut@yahoo.com', 9158448162, 9284033452, N'Summer/Sahaya/Lulu', 440)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TA1980', N'Teng', N'Acosta', NULL, NULL, 9777691980, NULL, NULL, 441)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TB4134', N'Trexie', N'Brillantes', N'120 Sauyo Rd', N'trexbrillantes@gmail.com', 9509404134, NULL, N'Izzy', 442)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TB6709', N'Teresita', N'Bolito', N'B5L39 North Point Subd Pdela Cruz St San Bartolome Nova QC', NULL, 9292146709, NULL, N'Zaldy/Lira/Mono/Mira', 443)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TB7199', N'Teresita', N'Baquilar', N'31 Apollo Ext Remarville Subd Bagbag Novaliches QC', NULL, 9178527199, NULL, N'Douglas', 444)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TB9152', N'Tadeo', N'Babina', N'29 King Christopher Bagbag Novaliches QC', N'tadzky1988@gmail.com', 9616049152, NULL, N'Bella', 445)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TC4813', N'Telly', N'Cacatian', N'B17L9 Faith St goodwill Homes I Novaliches QC', NULL, NULL, 89364813, N'Digong', 446)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TC8616', N'Thor', N'Chico', N'44 Virgo AC F Homes San Bartolome Nova QC', NULL, 96761148616, NULL, N'Molly', 447)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TF3112', N'Thelma', N'Fontanoza', N'13A Blueridge Ave Northwind Subd', N'paulinefonanoza@gmail.com', 9178733112, NULL, N'Phoebe', 448)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TF4835', N'Tricia Leigh', N'Falcutila', N'24 Urcia St Bagbag Nova QC', N'tricialeight.falcutila@gmail.com', 9162974835, NULL, N'Peanut', 449)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TP7231', N'Tony Rose', N'Paulite', N'24 Urcia St Bagbag Nova QC', N'toni.paulite@gmail.com', 9216007231, 9156394113, N'Jiyo/Mikay', 450)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TS0494', N'Tes', N'Semilla', N'620 Bagbag Nova', NULL, 9338510494, NULL, N'Korei', 451)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TS6683', N'Tess', N'Surita', N'L3 B8 Ezekiel St', NULL, 9175623468, 9175116682, N'Pipi/Misty', 452)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VB4286', N'Van Chester', N'Bigyan', N'53 Rockville Subd San Bartolome Novaliches QC', N'bigyanchester047@gmail.com', 9269684286, NULL, N'Kirei', 453)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VF9095', N'Vivian', N'Fajardo', N'11 san Agustin St Gulod Nova QC', NULL, 9228759095, 9433500459, N'Sneeker/Congoi/Bandit/Vito/Colette/Tinay/Gracia', 454)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VO0953', N'Vanity', N'Orejana', N'2 Santan St San Pedro St Bagbag Novaliches QC', N'josephorejana@yahoo.om', 9497360953, NULL, N'Zap', 455)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VS2085', N'Vangie', N'Salazar', N'120 Sauyo Rd Sauyo Nova QC', NULL, 9480182085, NULL, N'Odi', 456)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VT9815', N'Venson', N'Tabora', N'625 F Biglang Awa St Bagbag', N'johannaocaris@yahoo.com.ph', 9758519815, NULL, N'Covee', 457)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VV1305', N'Val', N'Verzano', N'645 Quirino Highway Bagbag', NULL, 9167491305, NULL, N'Maya', 458)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WA6384', N'William', N'Ang Jr', N'B8L12 Joshua St Goodwill Homes 2 Novaliches QC', NULL, NULL, 4416384, N'Hannah', 459)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WC5370', N'Winston Romero', N'Casio', N'53 Bernardino Rd Bagbag Nova QC', NULL, 9177235370, 79874559, N'Abby', 460)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WH1562', N'Wilma', N'Holis', N'North Point Park Nova QC', NULL, 91153291562, NULL, N'Peanut', 461)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WM3221', N'Wendelyn', N'Manuel', N'B4L7A Brgy San Bartolome Novaliches QC', N'windelwynmanuel@gmail.com', 9152883221, NULL, N'Tisoy', 462)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WP6575', N'Wenea Xarisse', N'Portillo', N'Seminaryo Rd Bagbag Nova QC', N'weneaxarisse@gmail.com', 9612536575, 9950983609, N'Cupid/Shila', 463)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'YR8689', N'Yuan', N'Robosa', N'Caloocan City', N'robosajeanyuan@gmail.com', 9060128689, NULL, N'Hiro', 464)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ZM7499', N'Zyrelle Anne', N'Magallanes', N'625 Biglang Awa Cmpd Bagbag Nova QC', N'magallaneszyrelleanne@gmail.com', 9185807499, NULL, N'Tei', 465)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MC1840', N'Maricar', N'Concepcion', N'4163 Feliciano Rd Ugong Valenzuela City', N'maricarcncpcn#gmai.com', 9163361840, NULL, N'Royce/Milko/Lexy', 466)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DA3936', N'Denise Lyka', N'Amago', N'F20 Abbey Rd Bagbag Nova QC', N'deniselyraamago@gmail.com', 9995163936, 70917859, N'Meowzie', 467)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA9275', N'Consuelo', N'Aguilar', N'Talipapa QC', N'cathaguilar28@gmail.com', 9752479275, NULL, N'Hazel/Moana/Krizia', 468)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LT5771', N'Lovely', N'Talusan', N'314 P dela Cruz St San Bartolome Nova QC', N'lovelymanalili@yahoo.com', 9617505771, 9617505771, N'Violet', 469)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA0562', N'Cathia', N'Alejo', N'32A Bernardino Rd Bagbag Nova QC', NULL, 9276790572, NULL, N'Duke', 470)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LE 7315', N'Laarni', N'Eva', N'32 Remarville Ave Bagbag Nova', N'laarnieva18@gmail.com', 9175217315, NULL, N'Pangpong', 471)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA1556', N'Mylene', N'Alegre', N'B7 L13 Goodwill Homes II Bagbag Nova', NULL, 9338181556, 34176949, N'Senorito/Chiqui', 472)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LF5266', N'Lilian', N'Fullero', NULL, NULL, 9954815266, NULL, N'Stevie/Tino/Chaka', 473)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA4659', N'Catleya', N'Alcala', N'49 Seminary Rd Bagbag Nova QC', NULL, 9662714659, NULL, N'Bambi/Bamba', 474)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA3838', N'Rosemarie P', N'Arboleda', N'7 Biglang Awa St Pagkabuhay Rd Bagbag Nova', N'arboledarosemarie@gmail.com', 9274723838, NULL, N'Mikmik/Hany/Kitkat', 475)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TB4724', N'Thesa', N'Baguinguito', N'11 Parokya Rd Bagbag Nova QC', NULL, 9159034724, NULL, N'Beauty', 476)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RB3928', N'Rose', N'Bongco', N'Ero St Seminaryo', NULL, NULL, 74183928, N'Sky', 477)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'WB6675', N'Wilfredo G', N'Bysma Jr', N'13 Rd 2 Don Julio Gregorio St Bagbag Nova QC', N'blysmatic@gmail.com', 9396436675, NULL, N'Chacha', 478)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PD0507', N'Pau', N'de Castro', N'25 Urbano St Bagbag Nova QC', NULL, 9954670507, NULL, N'Toby', 479)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SF3763', N'Stefanny Marie', N'Francisco', N'A Ramirez St Dona Faustina Subd Nova QC', N'smf04297@gmail.com', 9295293763, NULL, N'Chi/Star', 480)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS2725', N'Marilou', N'Sabater', N'Good Heaven Cmpd', NULL, 9563692725, NULL, N'Chichi', 481)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CS9644', N'Charmaine', N'Sambahon', N'24 Leon Cleofas St Seminary Bagbag Nova', NULL, 9271429644, NULL, N'Pepper/Clover', 482)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JS5839', N'Joan/Tony', N'Sison', N'17 Amethyst St Rockville Subd Bagbag Nova QC', NULL, 9083665839, 9985623571, N'Coco', 483)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM9430', N'Marilou', N'Mande', N'121 Remarville Ave Bagbag Nova QC', NULL, 9669849430, NULL, N'Small/Snowman', 484)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CY6850', N'Cristy', N'Yadao', N'Area 5 Naval QC', NULL, 9091666850, NULL, N'Chumpy', 485)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BM5147', N'Bea & Blezy', N'Montances', N'30 San Nicasio St Gulod Nova QC', N'bmntns@gmail.com', 9159875147, NULL, N'Bully', 486)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EQ6968', N'Evelyn', N'Quinday', N'595 Blas Roque Bagbag Nova QC', NULL, 9324636968, NULL, N'Mia', 487)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CA0572', N'Cathia', N'Alejo', N'32A Bernadino Rd Bagbag Nova QC', N'cathiaalejo@gmail.com', 9276790572, 9066265579, N'Duke', 488)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JD4820', N'Jamie', N'Dasmarinas MD', N'B6 L20 Silvergull St Rainbow Hms I San Bartolome Nova QC', N'jamiedominique@yahoo.com', 9185614820, NULL, N'Lady', 489)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FL9170', N'Florence', N'Labarda', N'16 Leon Cleofas Bagbag', N'flor_labarda@yahoo,com', 9751849170, 9066726631, N'Sky', 490)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SA9360', N'Shanice Louie', N'Ardales', N'620 Int Quirino Hiway Bagbag', N'shaniceardales@gmail.com', 9063549360, NULL, N'Simba', 491)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FL6631', N'Florence', N'Labarda', N'16 Cleofas St Bagbag Nova QC', N'flor_labarda@yahoo.com', 9751849170, 9066726631, N'Sky', 492)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JO6633', N'Joyce Ann B', N'Osita', N'124 King Constantine St Kingspoint Bagbag Nova QC', N'annosita03@gmai.com', 9481146633, NULL, N'Dawny', 493)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'OL0131', N'Ofelia', N'Lazaro', N'94 Apollo St Rockville', NULL, NULL, 9450131, N'Chloe', 494)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC1944', N'Joan', N'Ching', N'211 P dela Cruz St San Bartolome', NULL, 9177074643, 83615446, N'Iceman/Panda/Pakyaw/Brownie/Pogi/Whiskey', 495)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC4879', N'Jane', N'Coombs', N'B3 L2 Princess Homes Bagbag Nova QC', NULL, 9957444879, 9988843965, N'Smokey/Tux/Saki/Mona/Ash', 496)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM6465', N'Roden Jhon', N'Mirasol', N'No 08 Gerry St Remarville Bagbag Nova QC', NULL, 9557316465, NULL, N'Louise', 497)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RN1140', N'Ronie', N'Necor', N'459 Camp Grezar St Bagbag Nova QC', N'shonieshiane@gmail.com', 9984231140, NULL, N'Fulgoso', 498)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CK6533', N'Christopher Cyril B', N'Kiumisala Jr', N'30 Leoni Cleofas St Ibayo 1 Bagbag Nova QC', N'christophercyrilK@yahoo.com', 9369996533, NULL, N'Zoey', 499)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM1798', N'Mike', N'Medina', N'Rolling Meadows 3 Nova QC', N'michaelmedina101@gmail.com', 9174681798, NULL, N'Britney', 500)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MV5325', N'Mrk Joey', N'Violeta', N'615 Int O Babag Nova QC', N'joeyknowsvioleta@gmail.com', 9531805325, NULL, N'Jiro', 501)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SS8160', N'Shelly Grace G', N'Semillano', N'5091, sulok St Brgy Ugong, Valenzuela City', N'shellygracesemillano0013@gmail.com', 9451978160, NULL, N'Nana/Kahel/Choleng/Yakin', 502)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MS7516', N'Marfan Yuson', N'Sereneo', N'Kingspoint Subd Bagbag Nova QC', NULL, 9557336516, NULL, N'Hachiko', 503)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NF1364', N'Nelia', N'Felipe', N'625 Biglang Awa Bagbag Nova QC', NULL, 9167691364, NULL, N'Hershey', 504)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ID3834', N'Kathleen L', N'Domingo', N'B7 L5 Enoch St Goodwill Homes II Bagbag Nova QC', NULL, 9268073834, NULL, N'Snow/Gray/Mapsie', 505)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NR1802', N'Nelson B', N'Repe', N'Gulod Nitang Nova QC', N'joshuarepa@yahoo.com', 9665881802, NULL, N'Teo', 506)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'L5016', N'Rosalie D', N'Licudine', N'101-A Capricorn St Remarville Subd Bagbag Nova QC', NULL, 9266755016, NULL, N'Chloe/Fluffy', 507)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MO0825', N'Michael Ronald', N'Opeda', N'51 St Peter Street Marie''s Village San Bartolome Nova QC', NULL, 9173190825, 89361683, N'Dumpy/Pochi/Brownlee/Waffle', 508)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AL6004', N'Andrea', N'Licuanan', N'17 Urbano St Bagbag Nova QC', N'fzaraastrid0818@gmail.com', 9177146004, 9951583038, N'Pocholo/Sasha/Marcial', 509)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SA6191', N'Saturnino', N'Azana', N'A8 Don Jukio Gregorio St Bagbag Nova QC', NULL, 9287336191, 9186634133, N'Miah', 510)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP4432', N'Mario Nimfa', N'Pimentel', N'13 Felipe St Remarville Bagbag Nova QC', NULL, 9092384432, NULL, N'Bruno', 511)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RO6193', N'Rosa Meñano', N'Ortega', N'2339 lamesa St Ugong Valenzuela City', N'menano.rosa08@gmail.com', 9666926193, 9324958360, N'Spotty/Lucky', 512)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MJ1652', N'Marck', N'Juliano', N'Hammet Sauyo QC', N'tacenetworks@gmail.com', 9665831652, NULL, N'Seven', 513)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC5694', N'Jhon Patrick D', N'Cellona', N'A19 Don Julio Gregorio St Bagbag QC', N'jhonpatrickcellona@gmail.com', 9461196594, NULL, N'Scallie', 514)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA6013', N'Mirasol', N'Aparis', N'14 A Ibayo II Bagbag Nova QC', NULL, 9218756013, NULL, N'Patras', 515)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AT4557', N'Audrey', N'Talatala', N'118 A Remarville St Subd Bagbag Nova QC', N'audreystarr25@gmail.com', 9166184557, 9151543504, N'Bossy', 516)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MN2997', N'Mark Anthony', N'Nafarrete', N'B12 L15 A Joshua St Goodwill Homes 2', N'marknafarrete@yahoo.com', 9338152997, NULL, N'Zchatzi', 517)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JI6883', N'Joniva', N'Idorita', N'C25 Wings Comp Bagbag Nova QC', N'jonivaidorita1119@gmail.com', 9101316883, NULL, N'Brown', 518)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JT6942', N'John Paul', N'Tan', N'B2 L15 GK Intl Village Remarville Subd Bagbag Nova QC', N'singkitko_77@yahoo.com', 9231926942, NULL, N'Dribble', 519)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JV6849', N'Janice', N'Vinas', N'595 Blas Roque St Bagbag Nova QC', N'dyanis021379@gmail.com', 9154416849, NULL, N'PowPow', 520)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LD8051', N'Letlet', N'Dalafu', N'637 Int 4 Lazaro Comp Bagbag Nova QC', N'lenidithadalafu@gmail.com', 9672568051, 83523505, N'Tofi', 521)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CJ0013', N'Cristyn', N'Juanillo', N'32 C Bernardino Rd Bagbag Nova QC', N'cristynjuanillo@gmail.com', 9569100013, NULL, N'Rocket', 522)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'IL5089', N'Ian Dominic O', N'Lagman', N'B2 L35 Aquarius St Bagbag Nova QC', N'ckirsten98@gmail.com', 9176285089, 9277474031, N'Gatsby/Bokki', 523)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RA8772', N'Reyn', N'Arante', N'26 B Esau St Nova QC', NULL, 9771248772, NULL, N'Dutches', 524)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FB4734', N'Ferdinand M', N'Banquil Jr', N'C-10 Rd 3 Bagbag Nova QC', N'bobby051991@gmail.com', 9293724734, 9286874306, N'Oreo/Nigga', 525)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CH6513', N'Cherald Joy', N'Hubahib', N'625 3C Padiz Bldg Biglang Awa St Bagbag Nova QC', N'cheraldjoycruz@yahoo.com', 9279186513, NULL, N'Piko', 526)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GR1200', N'Gellie H', N'Ramos', N'B10 L12 Moses St Goodwill 2 Bagbag Nova QC', N'gellieramos26@gmail.com', 9266391200, NULL, N'Potchi', 527)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GM5042', N'Genelyn M', N'Malasarte', N'625 Int 05 Bagbag Parokya ng Pagkabuhay Nova QC', N'genelynmmalasarte@gmail.com', 9197265042, NULL, N'Toby', 528)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EC4260', N'Emmanuel S', N'Cagara', N'B3 L5 St Michael Homes Bagbag Nova QC', N'tss.em206@gmail.com', 9166244260, NULL, N'John/Snow', 529)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'SM5865', N'Sorene M', N'Maquiraya', N'2 Senading St Gulod Nova QC', N'prettysorene05@gmail.com', 9338225865, NULL, N'Dianna', 530)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LJ5276', N'Lhen', N'Jose', N'28A1 Magno St Remarville Bagbag Nova QC', N'lhenjose1986@gmail.com', 9268065276, NULL, N'Miggy/Popoy', 531)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JT7722', N'Jeanie', N'Taasan', N'427 Victorino St Talipapa Nova QC', N'jeanie_taasan@yahoo.com', 9957957722, NULL, N'Suzy', 532)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KO2729', N'Khristine', N'Ontalan', N'5 sapphire St Rockville Subd Phase 2 Nova QC', NULL, 9171672729, NULL, N'Tyson', 533)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CM1725', N'Carla A', N'Magpantay', N'RT Gunzalo San Bartolome Nova QC', N'christianmagpantay_809@yahoo.om', 9088151725, 2745872, N'Cabbie/Morris', 534)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM3649', N'Rain Jade S', N'Mallari', N'53 Leon Cleofas St Bagbag Nova QC', NULL, 9776253649, NULL, N'Ashie', 535)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LM3649', N'Lemmuel', N'Manalanzan', N'Bagbag Nova QC', NULL, 9456203649, NULL, N'Belle', 536)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PM4611', N'Patricia A', N'Manalili', N'49 S Francisco St San Bartolome Nova QC', NULL, 9550544611, NULL, N'Billy', 537)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FM7621', N'Francis', N'Mangahas', N'625 Int C Bagbag Nova QC', N'honiigow@gmail.com', 9186117621, NULL, N'Frea', 538)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LM', N'Lizelle Jasmin B', N'Manansala', N'B10 L15 Northwind Subd', NULL, NULL, NULL, N'Stefanie', 539)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TM2020', N'Teresa', N'Mandapat', N'594 Marides Bagbag Nova QC', NULL, 9087182020, NULL, N'Missy', 540)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM5949', N'aiko J', N'Manganti', N'27 Cadena de Amor St San Pedro Bagbag Nova QC', N'aiko.manganti07@yahoo.com', 9228545949, NULL, N'Yumi', 541)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RM0046', N'Robert', N'Mangilao', N'Seminary Rd', NULL, 9322110046, NULL, N'Happy', 542)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CM7110', N'Cherrie T', N'Manibo', N'B2 L17 CFC Int''l Vill Remarvill Subd Bagbag Nova QC', NULL, 7517110, NULL, N'Chico', 543)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DM9747', N'Dennis', N'Manalastas', N'B9 L38 Metrogreen San Bartolome Nova QC', NULL, NULL, 2949747, N'Blake', 544)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM8661', N'Arlene', N'Manuel', N'11 Villareal St Joan of Arc Gulod Nova QC', N'doc.joelmanuel@yahoo.com', 9420148661, NULL, N'Bella', 545)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MM1971', N'Mary Wyeth', N'Manlusoc', N'149 P dela Cruz St San Bartolome Nova QC', NULL, 9228161971, 9989614, N'Koko', 546)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FM4721', N'Felipe', N'Mansanillo', N'Area 5 A Sitio Cabuyao Sauyo Nova QC', N'geralyndiwan1983@gmail,com', 9058214721, 9224095708, N'Kitkat', 547)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JT4837', N'Jerome', N'Torres', N'35 Bagong Lote St Potrero Malabon', NULL, NULL, 3614837, N'Tyron/Tammy/Trixie/TBolt', 548)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CC6228', N'Christopher R', N'Custodio', N'19 Martirez St Gulod Nova QC', NULL, 9082916228, NULL, N'Hachico/Lucas', 549)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'HB6025', N'Hannah', N'Biglang Awa', N'73 JP Ramoy St Talipapa Nova QC', N'biglangawagenelita@yahoo.com', 9954296025, NULL, N'Quilo', 550)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RV4542', N'Rafaela', N'Velasco', N'B12 L3 Saul St Goodwill Homes 2', NULL, 9087094542, NULL, N'Hatchi', 551)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RP4469', N'Rico', N'Punzalan', N'B7 L36 Metro Green Vill', NULL, 9176184469, NULL, N'Shanel', 552)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GL7818', N'Gerard', N'Luz', N'103 East Fresno St California Vill QC', NULL, 9065657818, NULL, N'Simon', 553)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NT2764', N'Nora', N'Tubiano', N'B10 L21 Guyabano St SM Homes Nova QC', NULL, 9065042764, 9380579, N'Hennesy/Hershey ', 554)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA6098', N'Jerome D', N'Arbiol', N'12C Champaca St Sauyo Nova QC', N'jerome.arbiol.pnp@gmail.com', 9995226098, NULL, N'Kotwa', 555)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ED2911', N'Evelyn', N'Diaz', N'Unit C Rosal cor Sampaguita Gloria V Subd', NULL, 9171892911, NULL, N'Pompom', 556)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ZI3495', N'Zenaida', N'Iligan', N'BB L10 Good Haven Homes II Urbano St', N'iligan.carmelo@yahoo.com', 9157223495, NULL, N'Potchie', 557)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MA3274', N'Marielle', N'Uy Abella', N'B6 L14 Moss St Metro Green Vill San Bartolome', N'maye.uyabella@gmail.com', 9228733274, NULL, N'Soju', 558)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KS0684', N'Krystal Ann', N'Santos', N'B2 L3 Biglang Awa St Biglang Awa Subd Talipapa', N'k.annsantos29@gmail.com', 9953440684, NULL, N'Cookie', 559)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CF9868', N'Clarisse Kyla M', N'Frias', N'B1 L58 King Solomon St Kingspoint', N'clarkaypriyaz@gmail.com', 9553679868, NULL, N'Kiyomi', 560)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JZ5865', N'Jojo', N'Zialcita', N'057 Road 3 Bagbag Sauyo Nova', N'jojo.zialcita@yahoo.com', 9215435865, NULL, N'Puppy', 561)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CF1515', N'Casper P', N'Fegi', N'10 6B Don Julio Gregorio St Bagbag', N'casperfegi@gmail.com', 9127491515, NULL, N'Yuzong', 562)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB5334', N'Jingjing', N'Bade', N'4 Carreon Bagbag', N'jingjing@gmail.com', 9698495334, NULL, N'Aiden', 563)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GS7178', N'Gensha', N'San Jose', N'1 Martilyo Marikina', N'genshasanjose@gmail.com', 9152947178, NULL, N'Lucy/Jassper', 564)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RS1070', N'Reila', N'Superiano', N'595 Blas Roque St Bagbag', N'superianoreila261@gmail.com', 9312081070, NULL, N'Belgian', 565)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MB9354', N'Monaliza', N'Bario', N'C11 Rd3 Don Julio Gregorio St Bagbag', NULL, 9274079354, NULL, N'Balbon', 566)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MB2395', N'Menchie', N'Babina', N'5236 Bgy Ugong Sulog St Valenzuela Caloocan', NULL, 9171652395, NULL, N'Alluka/Unicah/Ade/Sandy/Alex', 567)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KB7018', N'Karen', N'Balistoy', N'23B Coronel Cmpd Bagbag', N'karenbalistoy12ip@gmail.com', 9958317018, NULL, N'Ragnar', 568)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TD0107', N'Trisha Janna A', N'Danganan', N'16 Apollo St Bagbag Nova QC', N'tishhemmings1996@gmail.com', 9190830107, NULL, N'Bluie', 569)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DE2933', N'Dennis', N'Estopacia', N'438 A Quirino Highway Bagbag Nova QC', N'estopacia.dennis@gmail.com', 9992282933, NULL, N'Bingo', 570)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JE8533', N'Jona Ellaine L', N'Equiza', N'12 Gerry St Bagbg Nova QC', N'ejonaellaine@gmail.com', 9451828533, NULL, N'Gucci', 571)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JS5505', N'Janina Nicole', N'Sanchez', N'26 Morning Glory St del Nacia 3 QC', N'jn_sanchez@ymail.com', 9954134469, 9474275505, N'4 pups', 572)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'RL4544', N'Ronalyn', N'Lleva', N'04 San Martin St Gulod Nova QC', N'rhonalleva@gmail.com', 9216334544, NULL, N'Caleb', 573)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EB6057', N'Emmanstar', N'Bernardino', N'720 Quirino Highway San Bartolome Nova QC', N'emmanstar.bernardino@gmail.com', 9263926057, NULL, N'Coffee', 574)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'VA1625', N'Ver', N'Agustin', N'BC L9 Good haven II Urbano St Bagbag Nova QC', N'jonkayana@yahoo.com', 9175701625, NULL, N'Kodi', 575)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NA4443', N'Nicole', N'Ambrosio', N'31 H Joshua St Goodwill Homes II Bagbag Nova QC', N'nic.ambrosio23@gmail.com', 9459974443, NULL, N'Bambi/Toby/Chichi', 576)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BE8863', N'Benjamin', N'Eugenio', N'70 Biglang Awa St Bagbag Nova QC', N'eugenioben@gmail.com', 9178478863, NULL, N'Mayonaise', 577)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JI63521', N'James', N'Ines', N'L39A San Bartolome Metrogreeen Vill', NULL, 9270563521, NULL, N'Huskies', 578)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GG8370', N'Gale Beaumont', N'Gillo', N'Area 2 Ilang ilang St Ext Bgy Capri Nova QC', N'galey.gillo021@gmail.com', 9751828370, 9754985610, N'Cooper', 579)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AC1578', N'Ajay-ann', N'dela Cruz', N'27 F Dona Rosario Subd Nova QC', N'ajayann.delacruz23@gmail.com', 9050251578, NULL, N'Sugar', 580)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JR3391', N'Janin', N'Ronquillo', N'604 Quirino Hiway Bagbag Nova QC', N'ronquillo.jr@pnu.edu.ph', 9979203391, NULL, N'Pompom', 581)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AL4296', N'Ashley Joshua S', N'Laxa', N'B3 Don Jcouo St Bagbag Nova QC', N'joshlaxa04@gmail.com', 9560714296, NULL, N'Saver', 582)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'LC4311', N'Lycka', N'Caguete', N'B11 L27 Kathleen Place 4 San Bartolome Nova QC', N'misslyckamae@yahoo.com', 9175914311, NULL, N'Milo', 583)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KC3967', N'Kayla', N'Cabasal', N'16 King Francis St Kingspoint Subd Nova QC', N'kaylacabasal@gamil.com', 9295523967, NULL, N'Bonbon', 584)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JR0607', N'Jhayson H', N'Romero', N'666 Quirino Highway Bagbag Nova QC', NULL, 9276290607, NULL, N'Tofer', 585)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AP3823', N'Agatha', N'Pamittan', N'70 Anahaw St Proj 7 QC', N'agathapamittan@gmail.com', 9217053823, NULL, N'Bronx', 586)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'GG0168', N'Genevee', N'Go', N'B5 Unit 13 Goldkey Townhouse Waling-waling St Sauyo QC', N'geneveego@gmail.com', 9174470168, NULL, N'Mio', 587)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NP9870', N'Noel', N'Panzo', N'10L Don Julio Gregorio St Bagbag', N'smile_belle2004@yahoo.com', 9296849870, NULL, N'Bragy', 588)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'YS8601', N'Yamenolah', N'Sheik', N'12 Lazaaro Comp T Carreon St Bagbag', N'yamenolahsheik@yahoo.com', 9210958601, NULL, N'Miggy', 589)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AE7839', N'Amiel', N'Eva', N'Virgo St Remarville Subd Bagbag', N'acreva.062197@gmail.com', 9479927839, NULL, N'Caden', 590)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AM0313', N'Anne Miriam R', N'Mercado', N'L10 B14 Isiah St Goodwill Homes 2 Pagkabuhay Bagbag', N'mercadojdr@gmail.com', 9369330313, NULL, N'Blair/Halley/Luffy', 592)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB6239', N'Janelle', N'Buenaventura', N'46 Sinforosa St Bagbag', N'ellesyperez@gmail.com', 9992276239, NULL, N'Ibiza/Malibu', 594)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MH1326', N'Ma Luisa', N'Hofilena', N'B10 L28 Maarigold St RMSI San Bartolome', N'luisaconda@yahoo.com', 9213171326, NULL, N'Kitkat', 596)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'OC9696', N'Orlyn', N'Crosa', N'19B Maningning St Gulod Nova', NULL, 9199729696, NULL, N'Kelly/Chuchu', 597)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PD4389', N'Pedro', N'Domingo', N'26 Omicron St RT Gonzales Vill', NULL, 9391274389, NULL, N'Miyaca/Sprite', 598)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AB5474', N'Argie', N'Barba', N'10A Rockville San Bartolome Nova QC', N'argiebarba@ymail.com', 9155745474, NULL, N'Timmy', 599)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'ND7545', N'Nelfie', N'de Jesus', N'2141 spatio Bernardo Champaca St Sauyo Nova', N'nelfiedejesus90@yahoo.com', 9087317545, NULL, N'Laila', 600)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'YR6709', N'Yolanda', N'Rodriguez', N'85 Sauyo Rd Nova QC', N'itsajbalbiiin@gmail.com', 9491026709, NULL, N'Matteo', 601)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'DL9395', N'Dave', N'Lim', N'9 Urbano St Bagbag', N'dave@nickgenmedia.com', 9322760513, 9155129395, N'Xena', 602)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MR5223', N'Mary Rose', N'Regis', N'595 Blas Roque Bagbag', N'mary126@yahoo.com', 9398155223, NULL, N'Brownie', 603)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'MP2111', N'Mary Jane', N'Parajes', N'615 Interio O Bagbag', N'janeparajes@yahoo.com', 9274072111, NULL, N'Snow', 604)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BG4986', N'Babylyn', N'Geñorga', N'Bagbag Nova QC', N'psychegenorga@yahoo.om', 9167704986, NULL, N'Bailey', 605)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CD6717', N'Corrine Ashley C', N'Dancel', N'9 Gerry St Remarville Ave Bagbag Nova QC', N'dancel.corrineashley@yahoo.com', 9059486717, NULL, N'Craiova/Onion', 606)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'BJ7653', N'Benjie', N'Salavania', N'4th Ero St Seminaryo Nova QC', N'benjie.salavania@gmail.com', 9324606753, NULL, N'Thunder/Sunny/Snowy', 607)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KP8165', N'Kenneth', N'Pua', N'b23l23 Kindness St Goodwill 1', N'puakaypec@gmail.com', 9173878165, NULL, N'Cloudy', 608)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AS0247', N'Aubrey Bryan', N'Sanchez', N'B17L9 Lukes St Goodwill 2 Bagbag Nova QC', N'aubrey.sanchez@yahoo.com', 9178740247, NULL, N'Jimbo', 609)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NC5899', N'Nico', N'Castrence', N'L4 B2 Titanium St Goldhill Homes', N'nicocastrence@gmail.com', 9174425899, NULL, N'Taco', 610)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JA3742', N'John Michael', N'Agupasa', N'21 King Alexis Bagbag', NULL, 9126173742, NULL, N'Portia', 611)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JR4134', N'Jocelyn', N'Rillon', N'44 King Charles St Ext Kingspoint Nova QC', N'jocelynrillon13@gmail.com', 9353304134, NULL, N'Farty Ulap', 612)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'KR2454', N'Khari', N'Ramo', N'003 Alley II Urcia St Seminary Rd Bagbag', N'keifmramo@gmail.com', 9167672454, NULL, N'Duke', 613)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'YB7172', N'Yoshua Yvez G', N'Bondoy', N'5534 Hup St Dona Mariana subd Ugong Val City', N'yvezbondoy@gmail.com', 9219337172, NULL, N'Gon', 614)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'NG1056', N'Noel', N'Garganza', N'C8 Road 3 Don Julio Bagbag', N'noelgarganza123@gmail.com', 9636281056, NULL, N'Cochie', 615)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TR6322', N'Tony', N'Rivera jr', N'B8L6 Abraham St Goodwill Homes II', N'thony014@yahoo.com', 9393916322, NULL, N'Princess', 616)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CM9000', N'Carlo', N'del Mundo', N'59 Old Sauyo Rd Sauyo QC', N'maleendbiog@gmail.com', 9081449000, NULL, N'Max Pogi', 617)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'FR7931', N'Fatima', N'Rimando', N'25 Esperan St Bagbag Nova QC', N'farimando3579@gmail.com', 9064097931, NULL, N'Leslie', 618)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PA0713', N'Patricia', N'Arias', N'B33 L2 Goodwill Homes 1 Nova QC', N'pmarias413@gmail.com', 9177120713, NULL, N'Koji', 619)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'PO9140', N'Pamela', N'Ontay', N'19 Cleofas St Bagbag Nova QC', N'cs101912@gmail.com', 9175139140, NULL, N'Happy Bon', 620)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JB7204', N'Johnmel', N'Buenaagua', N'Bagbag', N'johnmelbuenaagua@gmail.com', 9288487204, NULL, N'Jaguar', 621)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'AP4240', N'Aliza Michelle', N'Palomares', N'B1 L7 St Michael Homes Bagbag Nova', N'palomaresalizamichelle@gmail.com', 9267204240, NULL, N'Kiwi', 622)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JV4441', N'Jean', N'Vacunawa', N'16 Rockville Ave Rockville Subd San Bartolome', N'vacunawa14jean@gmail.com', 9155804441, 9561982036, N'Rami', 623)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'TG0275', N'Mary Grace', N'Tan', N'3 Dizon St, Alfred', N'jobelyntupig174@gmail.com', 9215350275, NULL, N'Celo/Forto/Bullet/Shadow', 624)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'EM1890', N'Erlene B', N'Mercado', N'76 Leon Cleofas St Bagbag', N'erlenemercado1@gmail.com', 9669101890, NULL, N'Tally', 625)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JC4903', N'Joshua', N'Eclarinal', N'23B Coronel Comp Bagbag Nova QC', N'joshua.eclarinal.je@gmail.com', 9496254903, NULL, N'Ellie', 626)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'JP7133', N'Jonalynn', N'Pagao', N'B1 L12 Aquarius St Goodwill T Homes Bagbag Nova QC', N'pagao.jona@gmail.com', 9271567133, NULL, N'Sasha', 627)
GO
INSERT [dbo].[CardinalPetRecord_ImportClientPatient_20210212] ([ID], [First], [Last], [Address], [email], [Mobile], [Telephone], [Pets Name], [RowIndex])
  VALUES (N'CB4982', N'Caitlin', N'Bartolaba', N'46 Sampaguita St San Pedro 9 Subd Nova QC', N'caitlinbartolaba@gmail.com', 9274174982, NULL, N'Noah', 629)
GO

DECLARE @Cardinal_ID_Company INT = 14


DECLARE @record TABLE (
  ID VARCHAR(MAX)
 ,First VARCHAR(MAX)
 ,Last VARCHAR(MAX)
 ,Address VARCHAR(MAX)
 ,email VARCHAR(MAX)
 ,Mobile VARCHAR(MAX)
 ,Telephone VARCHAR(MAX)
 ,PetsNames VARCHAR(MAX)
)

DECLARE @duplicateID TABLE (
  ID VARCHAR(MAX)
)


DECLARE @duplicateName TABLE (
  Name VARCHAR(MAX)
)


INSERT @duplicateID (ID)
  SELECT DISTINCT
    ID
  FROM dbo.CardinalPetRecord_ImportClientPatient_20210212
  GROUP BY ID
  HAVING COUNT(*) > 1


INSERT @duplicateName (Name)
  SELECT
    hed.Last + ' ' + hed.First
  FROM dbo.CardinalPetRecord_ImportClientPatient_20210212 hed
  WHERE ID NOT IN (SELECT
      ID
    FROM @duplicateID)
  GROUP BY hed.Last + ' ' + hed.First
  HAVING COUNT(*) > 1


INSERT @record (ID, First, Last, Address, email, Mobile, Telephone, PetsNames)
  SELECT
    hed.ID
   ,hed.First
   ,hed.Last
   ,hed.Address
   ,hed.email
   ,CONVERT(DECIMAL, hed.Mobile) Mobile
   ,CONVERT(DECIMAL, hed.Telephone) Telephone
   ,hed.[Pets Name]
  FROM dbo.CardinalPetRecord_ImportClientPatient_20210212 hed
  WHERE ID NOT IN (SELECT
      ID
    FROM @duplicateID)
  AND hed.Last + ' ' + hed.First NOT IN (SELECT
      Name
    FROM @duplicateName)
  ORDER BY hed.First, hed.Last, ID

DECLARE @ExistClientCode TABLE (
  Code VARCHAR(MAX)
)

INSERT @ExistClientCode (Code)
  SELECT
    Distinct ISNULL(c.Code,'')
  FROM tClient c
  WHERE c.ID_Company = @Cardinal_ID_Company


INSERT dbo.tClient (Code,
Name,
IsActive,
ID_Company,
Comment,
DateCreated,
DateModified,
ID_CreatedBy,
ID_LastModifiedBy,
ContactNumber,
Email,
Address,
ContactNumber2)
  SELECT
    hed.ID
   ,hed.First + ' ' + hed.Last
   ,1
   ,@Cardinal_ID_Company
   ,'Imported in 2/12/2021'
   ,GETDATE()
   ,GETDATE()
   ,1
   ,1
   ,hed.Mobile
   ,hed.email
   ,hed.Address
   ,hed.Telephone
  FROM @record hed
  WHERE hed.ID NOT IN (SELECT
      Code
    FROM @ExistClientCode)
  ORDER BY hed.Last, ID

DECLARE @ID_Client VARCHAR(MAX)
DECLARE @PetName VARCHAR(MAX)

DECLARE @ImportClientPet TABLE (
  ID_Client INT
 ,PetName VARCHAR(MAX)
)


INSERT @ImportClientPet (ID_Client, PetName)
  SELECT
    c.ID
   ,cpricp.[Pets Name]
  FROM tClient c
  INNER JOIN CardinalPetRecord_ImportClientPatient_20210212 cpricp
    ON c.Code = cpricp.ID
  WHERE c.ID_Company = @Cardinal_ID_Company
  AND cpricp.[Pets Name] NOT LIKE '%/%'


DECLARE db_cursor CURSOR FOR SELECT
  c.ID
 ,cpricp.[Pets Name]
FROM tClient c
INNER JOIN CardinalPetRecord_ImportClientPatient_20210212 cpricp
  ON c.Code = cpricp.ID
WHERE c.ID_Company = @Cardinal_ID_Company
AND cpricp.[Pets Name] LIKE '%/%'


OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName


WHILE @@FETCH_STATUS = 0
BEGIN
 
INSERT @ImportClientPet (ID_Client, PetName)
  SELECT
    @ID_Client
   ,Part
  FROM dbo.fGetSplitString(@PetName, '/') gss

FETCH NEXT FROM db_cursor INTO @ID_Client, @PetName
END

CLOSE db_cursor
DEALLOCATE db_cursor


INSERT tPatient (ID_Company,
  ID_Client,
  Code,
  Name,
  Old_patient_id,
  IsNeutered,
  IsDeceased,
  Comment,
  Species,
  ID_Gender,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy,
  FirstName,
  LastName,
  MiddleName,
  Email,
  FullAddress,
  ID_Country,
  ContactNumber)
SELECT
    @Cardinal_ID_Company
  ,ID_Client
  , NULL
 ,PetName
  ,NULL
  ,0
  ,0
  ,'Imported on 02/12/2021'
  ,''
  ,NULL,1,GETDATE(),GETDATE(),1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL
FROM @ImportClientPet


Go

IF (EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'CardinalPetRecord_ImportClientPatient_20210212')
  )
BEGIN

  DROP TABLE dbo.[CardinalPetRecord_ImportClientPatient_20210212]
END

