DEclare @GUID_Company VARCHAR(MAX) = '22D33175-52C0-48FD-B18C-796858C6AA37'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @Import TABLE
  (
     ID_Client            INT,
     ID_Patient           INT,
     Name_Client          VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     ContactNumber_Email  VARCHAR(MAX),
     Name_Patient         VARCHAR(MAX),
     Microchip_Patient    VARCHAR(MAX),
     DateDeceased_Patient DateTime
  )

INSERT @Import
       (Name_Client,
        ContactNumber_Client,
        ContactNumber_Email,
        Name_Patient,
        Microchip_Patient,
        DateDeceased_Patient)
SELECT dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Client Name], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Mobile Phone], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([E-mail Address], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Pet Name], '{', '')), '}', '')),
       dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Microchip No.], '{', '')), '}', '')),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(REPLACE(dbo.fGetCleanedString(REPLACE([Date Of Deceased], '{', '')), '}', '')))
FROM   [MacasandigClientsPatients]

Update @Import
SET    DateDeceased_Patient = NULL
WHERE  DateDeceased_Patient = '1900-01-01 00:00:00.000'

DELETE @Import
WHERE  LEN(Name_Client) = 0

Update tClient
SET    Name = dbo.fGetCleanedString(NaME)
WHERE  ID_Company = @ID_Company
       and IsActive = 1

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        Email,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber_Client,
                ContactNumber_Email,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @Import
where  ID_Client IS NULL
       AND Len(Name_Client) > 0

Update @Import
SET    ID_Client = client.ID
FROM   @Import import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @ID_Company

Update @Import
SET    ID_Client = patient.ID_Client,
       ID_Patient = patient.ID
FROM   @Import import
       inner join tPatient patient
               on import.Name_Patient = patient.Name
                  and import.ID_Client = patient.ID_Client
WHERE  patient.ID_Company = @ID_Company

INSERT dbo.tPatient
       (ID_Company,
        ID_Client,
        Microchip,
        Name,
        DateDeceased,
        IsDeceased,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Microchip_Patient,
                Name_Patient,
                DateDeceased_Patient,
                CASE
                  WHEN DateDeceased_Patient IS NOT NULL THEN 1
                  ELSE 0
                END,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @Import
where  ID_Client IS NOT NULL
       AND ID_Patient IS NULL
       AND Len(Name_Patient) > 0

Update @Import
SET    ID_Client = patient.ID_Client,
       ID_Patient = patient.ID
FROM   @Import import
       inner join tPatient patient
               on import.Name_Patient = patient.Name
                  and import.ID_Client = patient.ID_Client
WHERE  patient.ID_Company = @ID_Company

SELECT *
FROM   @Import
Order  BY Name_Client,
          Name_Patient 
