if OBJECT_ID('dbo.[MacasandigClientsPatients]') is not null
  BEGIN
      DROP TABLE [MacasandigClientsPatients]
  END

GO

CREATE TABLE [dbo].[MacasandigClientsPatients]
  (
     [Pet Name]         varchar(500),
     [Client Name]      varchar(500),
     [E-mail Address]   varchar(500),
     [Mobile Phone]     varchar(500),
     [Microchip No.]    varchar(500),
     [Check]            varchar(500),
     [Date Of Deceased] varchar(500)
  )

GO

INSERT INTO [dbo].[MacasandigClientsPatients]
            ([Pet Name],
             [Client Name],
             [E-mail Address],
             [Mobile Phone],
             [Microchip No.],
             [Check],
             [Date Of Deceased])
SELECT 'Alaska Flores',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bochok Dublas',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bruno Abrazado',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Padilla',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'Drax Pacatang',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       '16-Jun-21'
UNION ALL
SELECT 'Ganda Bacalan',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'KIMI Ligutan',
       'Bacalan Bacalan',
       '',
       '09363048219',
       '',
       '0',
       ''
UNION ALL
SELECT 'Smokey Bed Barroa',
       'Fionna Andrhea Barroa',
       '',
       '09050332065',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hermes Adil',
       'Abdulla Adil',
       '',
       '09568413261',
       '',
       '0',
       ''
UNION ALL
SELECT 'Babi Mabunay',
       'Abegail Mabunay',
       '',
       '09956278480',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cake Inso',
       'Acerbell Inso',
       '',
       '09152354160',
       '',
       '0',
       ''
UNION ALL
SELECT 'Princess Acman',
       'Acman',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'LIXIE NAVALES',
       'Acquila Mae Navales',
       '',
       '09553973723',
       '',
       '0',
       ''
UNION ALL
SELECT 'STEPHANIE Lerin',
       'Adelaida Lerin',
       '',
       '09177120554',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Moreno',
       'Adhel Moreno',
       '',
       '09654147098',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOBE AND KYRIE deVeyra',
       'Ado deVeyra',
       '',
       '09361367060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gringer Barillo',
       'Adrian Barillo',
       '',
       '09161513699',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lebron Federicos',
       'Agnes Joy Federicos',
       '',
       '09950977309',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Lupoy',
       'Agustin Lupoy',
       '',
       '09434954394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tamtam Timay',
       'Agustin Timay',
       '',
       '09175521060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiger Timay',
       'Agustin Timay',
       '',
       '09175521060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gucci Kang',
       'Ahmana Kang',
       '',
       '09566584300',
       '',
       '0',
       ''
UNION ALL
SELECT 'Phoebe Galenski',
       'Aida Galenski',
       '',
       '09171159685',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHLOE Hussain',
       'Aigene Hussain',
       '',
       '09165522492',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tuffy Dagoc',
       'AIleen Dagoc',
       '',
       '09265708699',
       '',
       '0',
       ''
UNION ALL
SELECT 'Barky Doromal',
       'Aileen Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blacky Doromal',
       'Aileen Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tyne Villaruel',
       'Aileen Villaruel',
       '',
       '09454574912',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Calamba',
       'Aimae Calamba',
       '',
       '09560632165',
       '',
       '0',
       ''
UNION ALL
SELECT 'Frea Vallecera',
       'Aimee S. Vallecera',
       '',
       '09650559635',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toffee Vallecera',
       'Aimee S. Vallecera',
       '',
       '09650559635',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Vallecera',
       'Aimee Vallecera',
       '',
       '09650559635',
       '',
       '0',
       ''
UNION ALL
SELECT 'TinkerBell Vallecera',
       'Aimee Vallecera',
       '',
       '09650559635',
       '',
       '0',
       ''
UNION ALL
SELECT 'Twinkle Vallecera',
       'Aimee Vallecera',
       '',
       '09650559635',
       '',
       '0',
       ''
UNION ALL
SELECT 'Poker Miasco',
       'Aiza Miasco',
       '',
       '09075565523',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kirai Akut',
       'Akut',
       '',
       '09271335207',
       '',
       '0',
       ''
UNION ALL
SELECT 'AULA Banto',
       'Alaisah Abanto',
       '',
       '09218099347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Digong Gines',
       'Alberto Gines',
       '',
       '09355545534',
       '',
       '0',
       ''
UNION ALL
SELECT 'Popoy Panoy',
       'Alberto Panoy',
       '',
       '09174301846',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dar-z Bax',
       'Alex Bax',
       '',
       '09174704545',
       '',
       '0',
       '19-Dec-19'
UNION ALL
SELECT 'Datu Bax',
       'Alex Bax',
       '',
       '09174704545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mayor Bax',
       'Alex Bax',
       '',
       '09174704545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Poppy Bax',
       'Alex Bax',
       '',
       '09174704545',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOOKIE Gonzales',
       'Alexandria Gonzales',
       '',
       '09957127431',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUCI villamil',
       'Alexandria villamil',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Paje',
       'Alexie Paje',
       '',
       '09155092379',
       '',
       '0',
       ''
UNION ALL
SELECT 'Trikay Abragan',
       'Alexis Abragan',
       '',
       '09165567100',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sasha Ernest',
       'Alexis Ernest',
       '',
       '09279535713',
       '',
       '0',
       ''
UNION ALL
SELECT 'IRISH BEAU Alvarez',
       'Alfonso Alvarez',
       '',
       '09977483806',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kobi Suralta',
       'Alfredo Suralta',
       '',
       '09363361163',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chewy Caballos',
       'Alia Caballos',
       '',
       '09778014557',
       '',
       '0',
       ''
UNION ALL
SELECT 'KENNY ESPA�OL',
       'ALICE ESPA�OL',
       '',
       '09265772126',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Eppie',
       'Aliyah Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'MATI Eppie',
       'Aliyah Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milkita Eppie',
       'Aliyah Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tanjiro Eppie',
       'Aliyah Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tyga Oanes',
       'Allen Oanes',
       '',
       '09756261244',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZEKE Ib-ib',
       'Alliah Jane Japlag',
       '',
       '09261688482',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kelly Kiano',
       'Allyn Kiano',
       '',
       '09954276072',
       '',
       '0',
       ''
UNION ALL
SELECT 'Allie Penabella',
       'Allyn Mae penabella',
       '',
       '09675702604',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 3 penabella',
       'Allyn Mae penabella',
       '',
       '09675702604',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jegger Culita',
       'Allysa Mae Culita',
       '',
       '093524112246',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Ilogon',
       'Althea Ilogon',
       '',
       '09065048320',
       '',
       '0',
       ''
UNION ALL
SELECT 'Clooney Ilogon',
       'Althea Ilogon',
       '',
       '09065048320',
       '',
       '0',
       ''
UNION ALL
SELECT 'Clover Ilogon',
       'Althea Ilogon',
       '',
       '09065048320',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Magtrayo',
       'Althea Magtrayo',
       '',
       '09053180456',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tux Gaid',
       'Alyn Gaid',
       '',
       '09678724978',
       '',
       '0',
       ''
UNION ALL
SELECT 'Stiles Cinco',
       'Alyssa Cinco',
       '',
       '090521535586',
       '',
       '0',
       ''
UNION ALL
SELECT 'Igor Clenuar',
       'Alyssa Clenur',
       '',
       '09392976545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Densing',
       'Alyssa Densing',
       '',
       '09066606445',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maui Yanez',
       'Alyssa Yanez',
       '',
       '09393041491',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cash Olivar',
       'Amalia Olivar',
       '',
       '09264411644',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ginger Morin',
       'Amalyn Morin',
       '',
       '09175484777',
       '',
       '0',
       ''
UNION ALL
SELECT 'LESLIE & BRUNO Pelaez',
       'Amar Pelaez',
       '',
       '09103646827',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bianco Ceniza',
       'Amelia Ceniza',
       '',
       '09177121670',
       '',
       '0',
       ''
UNION ALL
SELECT 'Barbie  Erojo',
       'Amor Erojo',
       '',
       '09362889040',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 6 Talaboc',
       'Amy Talaboc',
       '',
       '09974839646',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toothless Uy',
       'Ana Genica Uy',
       '',
       '0995489110',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toothless Uy',
       'Ana Genica Uy',
       '',
       '0995489110',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panda Babia',
       'Ana Marie Babia',
       '',
       '09395948156',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scout Palabrica',
       'Anahira Palabrica',
       '',
       '09190023079',
       '',
       '0',
       ''
UNION ALL
SELECT 'Satoshi Sobremisana',
       'Analou Sobremisana',
       '',
       '09175060521',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy Padecio',
       'Analyn Padecio',
       '',
       '09230820678',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bebot Tecson',
       'Andrea Tecson',
       '',
       '09059671371',
       '',
       '0',
       ''
UNION ALL
SELECT 'Onion Tecson',
       'Andrea Tecson',
       '',
       '09059671371',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Linaac',
       'Andria Lou Linaac',
       '',
       '09560028734',
       '',
       '0',
       ''
UNION ALL
SELECT 'ELSA Ligsanan',
       'Andro Ligasanan',
       '',
       '09551858109',
       '',
       '0',
       ''
UNION ALL
SELECT 'Phillip Ligasanan',
       'Andro Ligasanan',
       '',
       '09551858109',
       '',
       '0',
       ''
UNION ALL
SELECT 'Prince Ligasanan',
       'Andro Ligasanan',
       '',
       '09551858109',
       '',
       '0',
       ''
UNION ALL
SELECT 'BELLA Ranis',
       'Andro Ranis',
       '',
       '09059526306',
       '',
       '0',
       ''
UNION ALL
SELECT 'August Barsalote',
       'Angel Barsalote',
       '',
       '09977207812',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spike Hayag',
       'Angel Lou Hayag',
       '',
       '09050363459',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spot Hayag',
       'Angel Lou Hayag',
       '',
       '09050363459',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHICKO Maagad',
       'Angel Maagad',
       '',
       '09166493468',
       '',
       '0',
       ''
UNION ALL
SELECT 'Reiwa Meneses',
       'Angela Gopez',
       '',
       '09776568682',
       '',
       '0',
       ''
UNION ALL
SELECT 'TootieBoy Gopez',
       'Angela Gopez',
       '',
       '09776568682',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nabi Carralo',
       'Angela Mae Carralo',
       '',
       '09356382076',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tikay Rebamba',
       'Angela Rebamba',
       '',
       '09055649482',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tabby Quevedo',
       'Angelica Quevedo',
       '',
       '09171338692',
       '',
       '0',
       ''
UNION ALL
SELECT 'Raja Galope',
       'Angelie Galope',
       '',
       '09971797557',
       '',
       '0',
       ''
UNION ALL
SELECT 'Eve Galua',
       'Angelina GaluA',
       '',
       '09359825304',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Galua',
       'Angelina GaluA',
       '',
       '09359825304',
       '',
       '0',
       ''
UNION ALL
SELECT 'Matheos Galua',
       'Angelina GaluA',
       '',
       '09359825304',
       '',
       '0',
       ''
UNION ALL
SELECT 'oreo Galua',
       'Angelina GaluA',
       '',
       '09359825304',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zeus Galua',
       'Angelina GaluA',
       '',
       '09359825304',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marshal Galeon',
       'Angelino Galeon',
       '',
       '09363363382',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Meneses',
       'Angelique Meneses',
       '',
       '09164435987',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUNO Naplykit',
       'Angie Naplykit',
       '',
       '09364530639',
       '',
       '0',
       ''
UNION ALL
SELECT 'POCO Abas',
       'Anjelica Abas',
       '',
       '09176221147',
       '',
       '0',
       ''
UNION ALL
SELECT 'Princess Piamonte',
       'Ann Mechelle Piamonte',
       '',
       '09264981987',
       '',
       '0',
       ''
UNION ALL
SELECT 'Haru Gallena',
       'Anna Gallena',
       '',
       '09559011866',
       '',
       '0',
       ''
UNION ALL
SELECT 'MotMot Sabunod',
       'Anna Mae Sabunod',
       '',
       '09350937994',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snowie Navarro',
       'Anne Navarro',
       '',
       '09164330438',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kobi Virtudazo',
       'Anne Vitrudazo',
       '',
       '09056462603',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bella Belagantol',
       'Annie Belagantol',
       '',
       '09260889394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Belagantol',
       'Annie Belagantol',
       '',
       '09260889394',
       '',
       '0',
       ''
UNION ALL
SELECT 'SamSam Belagantol',
       'Annie Belagantol',
       '',
       '09260889394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cardo Alavar',
       'Annie Camille Jacalan',
       '',
       '09978616158',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bambi Abragan',
       'Annie Claire Abragan',
       '',
       '09556488658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snowy Cavite',
       'Anthony Cavite',
       '',
       '09473617228',
       '',
       '0',
       ''
UNION ALL
SELECT 'TIMMY Cavite',
       'Anthony Cavite',
       '',
       '09473617228',
       '',
       '0',
       ''
UNION ALL
SELECT 'Adam Galua',
       'Anton Pear Manseguiao',
       '',
       '09262361370',
       '',
       '0',
       ''
UNION ALL
SELECT 'Beauty Manseguiao',
       'Anton Pear Manseguiao',
       '',
       '09262361370',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kujoe Baclig',
       'Apple Baclig',
       '',
       '09171674401',
       '',
       '0',
       ''
UNION ALL
SELECT 'Riri Rosales',
       'April Marie Alexest Rosales',
       '',
       '09458207089',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Jutic',
       'April Rose Jutic',
       '',
       '09059341649',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toby Lavetoria',
       'Araceli Lavetoria',
       '',
       '09273914882',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sachi Gaabucayan',
       'Archille Gaabucayan',
       '',
       '09358418370',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHEKEE Banez',
       'Ardon Banez',
       '',
       '09750454114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chzena Banez',
       'Ardon Banez',
       '',
       '09750454114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bench Cojuanco',
       'Arjay Cojuanco',
       '',
       '09261776897',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mospet Bayer',
       'Arjo Bayer',
       '',
       '09066623133',
       '',
       '0',
       ''
UNION ALL
SELECT 'SNOOPY ASIADO',
       'ARMAN ASIADO',
       '',
       '09177224840',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mario Singh',
       'Armarjit Singh',
       '',
       '09777524131',
       '',
       '0',
       ''
UNION ALL
SELECT 'Potpot Uba',
       'Arnel Uba',
       '',
       '09975248994',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brooklyn Culanggo',
       'Arnold Culanggo',
       '',
       '09552634085',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHLOE Honcada',
       'ARNOLD Honcada',
       '',
       '09278771117',
       '',
       '0',
       ''
UNION ALL
SELECT 'Asta Luboa',
       'Arnold Luboa',
       '',
       '09971491678',
       '',
       '0',
       ''
UNION ALL
SELECT 'Baste Laguardia',
       'Arra Laguardia',
       '',
       '09166528113',
       '',
       '0',
       ''
UNION ALL
SELECT 'MASTER Roa',
       'Arrow Roa',
       '',
       '09171224231',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pogi Luib',
       'Art Simon Luib',
       '',
       '09277554545',
       '',
       '0',
       ''
UNION ALL
SELECT 'BAILEY & LUNA Bacor',
       'Arvelyn Bacor',
       '',
       '09268217345',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUCKY Tondag',
       'Arvin Rey Tondag',
       '',
       '09368400554',
       '',
       '0',
       ''
UNION ALL
SELECT 'Happy Macarampat',
       'Asnaira Macarampat',
       '',
       '09551256600',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puma Macarampat',
       'Asnaira Macarampat',
       '',
       '09551256600',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spam Estonilo',
       'Athena Estonilo',
       '',
       '09279832666',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zeus Badlon',
       'Audim Badlon',
       '',
       '09173112999',
       '',
       '0',
       ''
UNION ALL
SELECT 'BLUE Canada',
       'Audrey Canda',
       '',
       '09773493499',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHLOE Canada',
       'Audrey Canda',
       '',
       '09773493499',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hazai Qux',
       'Ausun Qux',
       '',
       '09173063086',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blue Peligro',
       'Azeil Peligro',
       '',
       '09069767995',
       '',
       '0',
       ''
UNION ALL
SELECT 'PaoPao Cabanes',
       'Azenith Cabarles',
       '',
       '09989979723',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Rafols',
       'Azley Rafols',
       '',
       '09363046390',
       '',
       '0',
       ''
UNION ALL
SELECT 'Carli Balgua',
       'Balgua Balgua',
       '',
       'no contact',
       '',
       '0',
       ''
UNION ALL
SELECT 'LOLOY Baylon',
       'Barbie Baylon',
       '',
       '09264545530',
       '',
       '0',
       ''
UNION ALL
SELECT 'Isko Estrada',
       'Barbie Estrada',
       '',
       '09059554116',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blondina and Latina Neri',
       'Barbie Neri',
       '',
       '09159019572',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jiger Bax',
       'Bax',
       '',
       '09174704545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lily Bernas',
       'Bea Bernas',
       '',
       '09177177446',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Bernas',
       'Bea Bernas',
       '',
       '09177177446',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dexter Nautan',
       'Bea Nautan',
       '',
       '09069261418',
       '',
       '0',
       ''
UNION ALL
SELECT 'BARBIE Elizaga',
       'Belin Ellizaga',
       '',
       '09177233777',
       '',
       '0',
       ''
UNION ALL
SELECT 'WHITY Bacarro',
       'Ben Bacarro',
       '',
       '09457964996',
       '',
       '0',
       ''
UNION ALL
SELECT 'HATCHI Neri',
       'Ben Neri',
       '',
       '09209102101',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cora Dingal II',
       'Benjamin Dingal II',
       '',
       '09069504293',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba Acedera',
       'Benjo Acedera',
       '',
       '09778062897',
       '',
       '0',
       ''
UNION ALL
SELECT 'Beauty Welter',
       'Berlinda Welter',
       '',
       'no num',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pino BERONIO',
       'bernadette Beronio',
       '',
       '09975369919',
       '',
       '0',
       ''
UNION ALL
SELECT 'CLARK Simbajon',
       'Bernaldo Simbajon',
       '',
       '09652605965',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ringo Ladera',
       'Beth Ladera',
       '',
       '09365007465',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ryder Uy',
       'Beth Uy',
       '',
       '09226432900',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Apostol',
       'Bil Apostol',
       '',
       '09161910084',
       '',
       '0',
       ''
UNION ALL
SELECT 'Daz Famas',
       'Bob Famas',
       '',
       '09755234715',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 2 Famas',
       'Bob Famas',
       '',
       '09755234715',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sunday Pagalan',
       'Boy Pagalan',
       '',
       '09177121040',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max AND MAXIM Virtudazo',
       'Boyet Virtudazo',
       '',
       '09756040029',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAMPAM Pana',
       'Brave Pana',
       '',
       '09264885897',
       '',
       '0',
       ''
UNION ALL
SELECT 'Polar Pana',
       'Brave Pana',
       '',
       '09264885897',
       '',
       '0',
       ''
UNION ALL
SELECT 'GrayGray Barcelona',
       'Brenda Barcelona',
       '',
       '09353186312',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luchee Barcelona',
       'Brenda Barcelona',
       '',
       '09353186312',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yannie Te',
       'Brent Te',
       '',
       '09171736935',
       '',
       '0',
       ''
UNION ALL
SELECT 'Butter Maverick',
       'Bret Maverick',
       '',
       '09663319899',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 5 Dampog',
       'Brian Dampog',
       '',
       '09151277575',
       '',
       '0',
       ''
UNION ALL
SELECT 'TUCKER Dampog',
       'Brian Dampog',
       '',
       '09151277575',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Rabago',
       'Brian Rabago',
       '',
       '09661445902',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mallows Quililan',
       'Camille Quililan',
       '',
       '09179846354',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panda Tenio',
       'Camille tenio',
       '',
       '09068885268',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cotton Vallejos',
       'Careen Mae Vallejos',
       '',
       '09173015849',
       '',
       '0',
       ''
UNION ALL
SELECT 'CUDDLES vallejos',
       'Careen Mae Vallejos',
       '',
       '09173015849',
       '',
       '0',
       ''
UNION ALL
SELECT 'MILEY VALLEJOS',
       'Careen Mae Vallejos',
       '',
       '09173015849',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rusty Co',
       'Carla Co',
       '',
       '09273675251',
       '',
       '0',
       ''
UNION ALL
SELECT 'Betty Santos',
       'Carlo Santos',
       '',
       '09177709188',
       '',
       '0',
       ''
UNION ALL
SELECT 'Freddie Sison',
       'Carmel Sison',
       '',
       '09262234706',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUVVER Ordonia',
       'Carmela Ordonia',
       '',
       '09751539738',
       '',
       '0',
       ''
UNION ALL
SELECT 'Star Duyag',
       'Carmen Duyag',
       '',
       '09458034453',
       '',
       '0',
       ''
UNION ALL
SELECT 'LIIT Guzman',
       'Carmincita Guzman',
       '',
       '09174138479',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dako Bangcong',
       'Caryl Bangcong',
       '',
       '09064744712',
       '',
       '0',
       ''
UNION ALL
SELECT 'RAVI Cruz',
       'Cassius Cruz',
       '',
       '09053395555',
       '',
       '0',
       ''
UNION ALL
SELECT 'CASPER Andrino',
       'Catherine Andrino',
       '',
       '09261494709',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pepa Gamali',
       'Catherine Gamali',
       '',
       '09772871996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Molly, Belamide',
       'Catherine Mabida',
       '',
       '09059037060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Arya Roa',
       'Catherine Mae Roa',
       '',
       '09176280733',
       '',
       '0',
       ''
UNION ALL
SELECT 'Drogo Roa',
       'Catherine Mae Roa',
       '',
       '09176280733',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shaq Roa',
       'Catherine Mae Roa',
       '',
       '09176280733',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sheeba Roa',
       'Catherine Mae Roa',
       '',
       '09176280733',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gasul Manusio',
       'Catherine Manusio',
       '',
       '09569362212',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Paderanga',
       'Catherine Paderanga',
       '',
       '09266967356',
       '',
       '0',
       ''
UNION ALL
SELECT 'Veerus Sajor',
       'Catherine Sajor',
       '',
       '09667042629',
       '',
       '0',
       ''
UNION ALL
SELECT 'Island Eagar',
       'Cathy Eagar',
       '',
       '09178677677',
       '',
       '0',
       ''
UNION ALL
SELECT 'LOKIE EAGAR',
       'Cathy Eagar',
       '',
       '09178677677',
       '',
       '0',
       ''
UNION ALL
SELECT 'Misha Eagar',
       'Cathy Eagar',
       '',
       '09178677677',
       '',
       '0',
       ''
UNION ALL
SELECT 'River Eagar',
       'Cathy Eagar',
       '',
       '09178677677',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cherie Pangan',
       'cecele Pangan',
       '',
       '09177732647',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOCHI Amolar',
       'Cecil Amolar',
       '',
       '09565729139',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kervex Jamis',
       'Cecille Jamis',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Archie Magno',
       'Cesar Magno',
       '',
       '09224970558',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buds Manulat',
       'Cesteeh Manulat',
       '',
       '09265680036',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toffee Moreno',
       'Chantal Moreno',
       '',
       '09363903853',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blesy Agbalog',
       'Charina Agbalog',
       '',
       '09058893810',
       '',
       '0',
       ''
UNION ALL
SELECT 'Illumi Galpo',
       'Charina Galpo',
       '',
       '09350749019',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Galpo',
       'Charina Galpo',
       '',
       '09350749019',
       '',
       '0',
       ''
UNION ALL
SELECT 'Taki Agana',
       'Charisse Agana',
       '',
       '09265527356',
       '',
       '0',
       ''
UNION ALL
SELECT 'Clint Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Clovis/ JACK Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cutie Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Misty Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Randy Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Randy Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiny Lustre',
       'Charito Lustre',
       '',
       '09778553347',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tammy Rockwell',
       'Charity Rockwell',
       '',
       '09756406714',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kingkay Neri',
       'Charleen Neri',
       '',
       '09998823622',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gringgo Estroga',
       'Charlreshiel Estroga',
       '',
       '09151725130',
       '',
       '0',
       ''
UNION ALL
SELECT 'Trigger Estroga',
       'Charlreshiel Estroga',
       '',
       '09151725130',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANGEL Babor',
       'Charmaigne Babor',
       '',
       '09753902567',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pinpin Albo',
       'Chatty Mee Albo',
       '',
       '09665679174',
       '',
       '0',
       ''
UNION ALL
SELECT 'badiday Gabitano',
       'Cherie Gabitano',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'bomber Gabitano',
       'Cherie Gabitano',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ramini Gagnan',
       'Cherie Gabitano',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'SABINA Doque',
       'Cherry Doque',
       '',
       '09667621043',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sabrina Dique',
       'Cherry Doque',
       '',
       '09667621043',
       '',
       '0',
       ''
UNION ALL
SELECT 'Teddy Bear',
       'Cherry Mae Avencena',
       '',
       '09309445325',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pipoy Teneza',
       'Cherry Pir Teneza',
       '',
       '09778027913',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kheno Tuca',
       'Cherry Tuca',
       '',
       '09176222659',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chelseah Alonzo',
       'Chery Lou Alonzo',
       '',
       '09953434505',
       '',
       '0',
       ''
UNION ALL
SELECT 'MACARONI Cruz',
       'Cheryl Love Cruz',
       '',
       '09751915330',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kitten2 Libarnes',
       'Chico Libarnes',
       '',
       '09279716966',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Onate',
       'Chlobeth Onate',
       '',
       '09480115534',
       '',
       '0',
       ''
UNION ALL
SELECT 'Candy Cho',
       'Cho',
       '',
       '09154940647',
       '',
       '0',
       ''
UNION ALL
SELECT 'Red Cho',
       'Cho',
       '',
       '09154940647',
       '',
       '0',
       ''
UNION ALL
SELECT 'Babu Arellano',
       'Chong Arellano',
       '',
       '09368001882',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOWNY Pecasales',
       'Chris Pecasales',
       '',
       '09771572268',
       '',
       '0',
       ''
UNION ALL
SELECT 'MENTOS Pecasales',
       'Chris Pecasales',
       '',
       '09771572268',
       '',
       '0',
       ''
UNION ALL
SELECT 'Teddie Portugal',
       'Chris Portugal',
       '',
       '09351084089',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cassie Abo-Abo',
       'Chrismar Abo-abo',
       '',
       '09169600888',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yano Lumagsao',
       'Chrissoula May Lumagsao',
       '',
       '09356016260',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHACKIE Pontero',
       'Christalyn Pontero',
       '',
       '09317588080',
       '',
       '0',
       ''
UNION ALL
SELECT 'Honey Pamploma',
       'Christian Pamploma',
       '',
       '09778338880',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucy Ferrarez',
       'Christina Therese Ferrarez',
       '',
       '09619862063',
       '',
       '0',
       ''
UNION ALL
SELECT 'ROCKY Bolasco',
       'Christine Bolasco',
       '',
       '09756006075',
       '',
       '0',
       ''
UNION ALL
SELECT 'POLPOL Hangca',
       'Christine Hangca',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'SHINE Martin',
       'Christine Martin',
       '',
       '09690150818',
       '',
       '0',
       ''
UNION ALL
SELECT 'SUN Martin',
       'Christine Martin',
       '',
       '09690150818',
       '',
       '0',
       ''
UNION ALL
SELECT 'COCO Nacua',
       'Christine Nacua',
       '',
       '09682975735',
       '',
       '0',
       ''
UNION ALL
SELECT 'Duchess Vallejos',
       'Christine Vallejos',
       '',
       '09171136543',
       '',
       '0',
       ''
UNION ALL
SELECT 'Callie Ching',
       'Christinity Ching',
       '',
       '09177776135',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Ching',
       'Christinity Ching',
       '',
       '09177776135',
       '',
       '0',
       ''
UNION ALL
SELECT 'GAMORA LEE',
       'Christopher Lee',
       '',
       '09631499685/09706028715',
       '',
       '0',
       ''
UNION ALL
SELECT 'COCOMELON Mejares',
       'Christy Mejares',
       '',
       '09757252165',
       '',
       '0',
       ''
UNION ALL
SELECT 'MILO Mejares',
       'Christy Mejares',
       '',
       '09757252165',
       '',
       '0',
       ''
UNION ALL
SELECT 'YODA Sampado',
       'Christy Sampado',
       '',
       '09359175044',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brown Pandan',
       'Ciryl Pandan',
       '',
       '09173071806',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Dela Cerna',
       'Claudine Dela Cerna',
       '',
       '09569079260',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOKIE Aguilar',
       'Clyde Ian Russel Aguilar',
       '',
       '09053533071',
       '',
       '0',
       ''
UNION ALL
SELECT 'Burrito Cornilla',
       'Coco Cornilla',
       '',
       '09759429007',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Maneja',
       'Coleen Maneja',
       '',
       '09177076550',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snoopy Surdilla',
       'Connie Surdilla',
       '',
       '09263116537',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Radia',
       'Cooky Radia',
       '',
       '09172072399',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paolo Taala',
       'Corazon Taala',
       '',
       '09051628393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gigi Abales',
       'Cristian Abales',
       '',
       '09054875619',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chuckie Timay',
       'Cristine Timay',
       '',
       '09551661291',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tummy Timay',
       'Cristine Timay',
       '',
       '09551661291',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hobi Agana',
       'Cristy Agana',
       '',
       '09178177758',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kookie Agana',
       'Cristy Agana',
       '',
       '09178177758',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mochi Agana',
       'Cristy Agana',
       '',
       '09178177758',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tannie Agana',
       'Cristy Agana',
       '',
       '09178177758',
       '',
       '0',
       ''
UNION ALL
SELECT 'MONA Aguilar',
       'Crystal Aguilar',
       '',
       '09466789430',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pepper Roa',
       'Cybelle Roa',
       '',
       '09177779508',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rocco Roa',
       'Cybelle Roa',
       '',
       '09177779508',
       '',
       '0',
       ''
UNION ALL
SELECT 'DIPSY Engracia',
       'Cyrene Engracia',
       '',
       '09157084346',
       '',
       '0',
       ''
UNION ALL
SELECT 'spike clemente',
       'Czar Clemente',
       '',
       '09279535379',
       '',
       '0',
       ''
UNION ALL
SELECT 'rubi da-abay',
       'Da-abay',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shandy Febra',
       'Daday Febra',
       '',
       '09771829523',
       '',
       '0',
       ''
UNION ALL
SELECT 'Skadi Guyano',
       'Dahlia Guyano',
       '',
       '09068493793',
       '',
       '0',
       ''
UNION ALL
SELECT 'June Romanillos',
       'Daily Romanillos',
       '',
       '09368497387',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kyle Mader',
       'Daise Mader',
       '',
       '09363904372',
       '',
       '0',
       ''
UNION ALL
SELECT 'Leo Morales',
       'Daisy Morales',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chuckie Briones',
       'Dan Briones',
       '',
       '09356529948',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ronin Maghuyop',
       'Dan Maghuyop',
       '',
       '09277922319',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloud Bragat',
       'Dang Bragat',
       '',
       '09277019536',
       '',
       '0',
       ''
UNION ALL
SELECT 'AKI Agdeppa',
       'Danica Reese Agdeppa',
       '',
       '09974853858',
       '',
       '0',
       ''
UNION ALL
SELECT 'Enchong  Yasay',
       'Danica Yasay',
       '',
       '09351518140',
       '',
       '0',
       ''
UNION ALL
SELECT 'koko Flores',
       'Daniela Flores',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tofi Jaranilla',
       'Danielle Justine Jaranilla',
       '',
       '09168451453',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOPPER Tan',
       'Danielle Tan',
       '',
       '09565361346',
       '',
       '0',
       ''
UNION ALL
SELECT 'Denver Mier',
       'Dante Mier',
       '',
       '09556506492',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOCONIDO Reyes',
       'Dareen Reyes',
       '',
       '09774596121',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kopee Irisari',
       'Darleen Irisari',
       '',
       '09772580140',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yam2 and Cloudy Mellijor',
       'Darwin Mellijor',
       '',
       '09168916713',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luffy Unabia',
       'Daryl Unabia',
       '',
       '09677730756',
       '',
       '0',
       ''
UNION ALL
SELECT 'Vogz Alipayo',
       'Dave Alipayo',
       '',
       '09457913392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo Jaranilla',
       'David Jaranilla',
       '',
       '09566296427',
       '',
       '0',
       ''
UNION ALL
SELECT 'Diego Miranda',
       'Dean Adrian Miranda',
       '',
       '09355153116',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 4 Miranda',
       'Dean Adrian Miranda',
       '',
       '09355153116',
       '',
       '0',
       ''
UNION ALL
SELECT 'FORD Amper',
       'Decelyn Amper',
       '',
       '09551505630',
       '',
       '0',
       ''
UNION ALL
SELECT 'FEITAN Copon',
       'Dee Copon',
       '',
       '09350749019',
       '',
       '0',
       ''
UNION ALL
SELECT 'HISOKA Copon',
       'Dee Copon',
       '',
       '09350749019',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peaches Del Castillo',
       'Del Castillo',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chelly Del Puerto',
       'Del Puerto',
       '',
       '09266058619',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chewie Lagumbay',
       'Delia lagumbay',
       '',
       '09176221293',
       '',
       '0',
       ''
UNION ALL
SELECT 'JANSEN DELIGERO',
       'DELIGERO  SHIELA MAE',
       '',
       '094554777336',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Macaibay',
       'deneca Macaibay',
       '',
       '09750494290',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chuchu (CAT) Gomez',
       'Denmark Gomez',
       '',
       '09167631075',
       '',
       '0',
       ''
UNION ALL
SELECT 'Emyat Gomez',
       'Denmark Gomez',
       '',
       '09167631075',
       '',
       '0',
       ''
UNION ALL
SELECT 'Inday Gomez',
       'Denmark Gomez',
       '',
       '09167631075',
       '',
       '0',
       ''
UNION ALL
SELECT 'Parok Gomez',
       'Denmark Gomez',
       '',
       '09167631075',
       '',
       '0',
       ''
UNION ALL
SELECT 'Persian ComCom',
       'Denver Comcom',
       '',
       '09369402980',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Jadulan',
       'Deo Jadulan',
       '',
       '09230820703',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRODY Sacayan',
       'Devie Marie J. Sacayan',
       '',
       '09652602027',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOCHA Sacayan',
       'Devie Marie J. Sacayan',
       '',
       '09652602027',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 2 Leones',
       'Devorah Leones',
       '',
       '09185412345',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mavy Ritchie',
       'Dexy Ian Ritchie',
       '',
       '09359510274',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sassy Samar',
       'Dhafne Samar',
       '',
       '09177190801',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panda Elcamel',
       'Diana Elcamel',
       '',
       '09367208173',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pawpaw Garcia',
       'Ding Garcia',
       '',
       '09175818680',
       '',
       '0',
       ''
UNION ALL
SELECT 'BETCHAY Calihat',
       'Dioscoro Calihat',
       '',
       '09150629876',
       '',
       '0',
       ''
UNION ALL
SELECT 'CARLITO Calihat',
       'Dioscoro Calihat',
       '',
       '09150629876',
       '',
       '0',
       ''
UNION ALL
SELECT 'Magnus Jaranilla',
       'Dj Jaranilla',
       '',
       '09177097313',
       '',
       '0',
       ''
UNION ALL
SELECT 'KIMMPY Salcedo',
       'Dodje Salcedo',
       '',
       '09154270396',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 2 Salcedo',
       'Dodje Salcedo',
       '',
       '09154270396',
       '',
       '0',
       ''
UNION ALL
SELECT 'Storm Caresma',
       'Dominic Caresma',
       '',
       '09276499400',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tuffy Albizo',
       'Donavelle Albizo',
       '',
       '09565360222',
       '',
       '0',
       ''
UNION ALL
SELECT 'golda Sobremisana',
       'Dondon Sobremisana',
       '',
       '09664902754',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pepper Sobremisana',
       'Dondon Sobremisana',
       '',
       '09664902754',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snowball Sobremisana',
       'Dondon Sobremisana',
       '',
       '09664902754',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Gepalago',
       'Donnie Gepalago',
       '',
       '09177217675',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scooby Umel',
       'Doribel Umel',
       '',
       '09058332892',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Roberts',
       'Dream Roberts',
       '',
       '09177724494',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choychoy Rebalocos',
       'Dulce Rebalocos',
       '',
       '09759479920',
       '',
       '0',
       ''
UNION ALL
SELECT 'Thunder Rebalocos',
       'Dulce Rebalocos',
       '',
       '09759479920',
       '',
       '0',
       ''
UNION ALL
SELECT 'Champy Burgos',
       'Dureza Burgos',
       '',
       '09266739296',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ganda Burgos',
       'Dureza Burgos',
       '',
       '09266739296',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sunday Burgos',
       'Dureza Burgos',
       '',
       '09266739296',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nine Buenaflor',
       'Duyle Buenaflor',
       '',
       '09176770466',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hug Hera',
       'Dy Hera',
       '',
       '09173241798',
       '',
       '0',
       ''
UNION ALL
SELECT 'BLU & NAMNAM Cabanubias',
       'Dyesebel Cabarubias',
       '',
       '09354937169',
       '',
       '0',
       ''
UNION ALL
SELECT 'JAVA & CASPER Cabarubias',
       'Dyesebel Cabarubias',
       '',
       '09354937169',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kokai R.H',
       'Eartha R.H',
       '',
       '09457225064',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sophie Ecarma',
       'Ecarma',
       '',
       '09056094153',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coby Baldo',
       'Edita Baldo',
       '',
       '09063730466',
       '',
       '0',
       ''
UNION ALL
SELECT 'Eggi Nagac',
       'Editha Nagac',
       '',
       '09264560739',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Lani',
       'Edwin Lani',
       '',
       '09178539739',
       '',
       '0',
       ''
UNION ALL
SELECT 'LILY Lloren',
       'Efren Lloren',
       '',
       '09174766491',
       '',
       '0',
       ''
UNION ALL
SELECT 'samantha Lloren',
       'Efren Lloren',
       '',
       '09174766491',
       '',
       '0',
       ''
UNION ALL
SELECT 'Noname Pollentes',
       'Efren Pollentes',
       '',
       '09174625476',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tamai Yuson',
       'Eira Yuson',
       '',
       '09166473229',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ganda Bacalan',
       'Eisel Mae Padinit',
       '',
       '09179409062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mercedes Padinit',
       'Eisel Mae Padinit',
       '',
       '09179409062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mercedes Padinit',
       'Eisel Mae Padinit',
       '',
       '09179409062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Thalia Padinit',
       'Eisel Mae Padinit',
       '',
       '09179409062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chay-chay Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chewy Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chiko Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloud Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dazzle Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hail Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'HERSHEY Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lemon remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maya Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Polar Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 5 Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toffee Remolado',
       'Elaine Remolado',
       '',
       '09956271996',
       '',
       '0',
       ''
UNION ALL
SELECT 'Estel Tajor',
       'Eldie Tajor',
       '',
       '09673949590',
       '',
       '0',
       ''
UNION ALL
SELECT 'DonYa Abesamis',
       'Elena Abesamis',
       '',
       '09051729488',
       '',
       '0',
       ''
UNION ALL
SELECT 'Putin Moring',
       'Elena Solima',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zig Reyes',
       'Elijah Reyes',
       '',
       '09275254605',
       '',
       '0',
       ''
UNION ALL
SELECT 'Koko Roa',
       'Elisa Roa',
       '',
       'ask doc mhai',
       '',
       '0',
       ''
UNION ALL
SELECT 'Q-taro Pelicano',
       'Eliza Pauline Pelicano',
       '',
       '09257014905',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cascade Aleria',
       'Elizabeth Aleria',
       '',
       '09955033893',
       '',
       '0',
       ''
UNION ALL
SELECT 'Grey Rivera',
       'Elloi Rivera',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucy Neri',
       'Elna Neri',
       '',
       '09276352256',
       '',
       '0',
       ''
UNION ALL
SELECT 'Draco Capistrano',
       'Elnor Capistrano',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUTTER Haloot',
       'Eloisa Haloot',
       '',
       '09167500158',
       '',
       '0',
       ''
UNION ALL
SELECT 'PEANUT Haloot',
       'Eloisa Haloot',
       '',
       '09167500158',
       '',
       '0',
       ''
UNION ALL
SELECT 'Thunder Haloot',
       'Eloisa Haloot',
       '',
       '09167500158',
       '',
       '0',
       ''
UNION ALL
SELECT 'Missy Alibangbang',
       'Elsa Alibangbang',
       '',
       '09176245801',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ollie Alibangbang',
       'Elsa Alibangbang',
       '',
       '09176245801',
       '',
       '0',
       ''
UNION ALL
SELECT 'skipper alibangbang',
       'Elsa Alibangbang',
       '',
       '09176245801',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tong Adora',
       'Elsie Adora',
       '',
       '09364522026',
       '',
       '0',
       ''
UNION ALL
SELECT 'BEAR Wright',
       'Emil Wright',
       '',
       '09171692119',
       '',
       '0',
       ''
UNION ALL
SELECT 'Merlin Wright',
       'Emil Wright',
       '',
       '09171692119',
       '',
       '0',
       ''
UNION ALL
SELECT 'Monty Wright',
       'Emil Wright',
       '',
       '09171692119',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gem Alegado',
       'Emilyn Alegado',
       '',
       '09651805853',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bobordj Far',
       'Emlyn rose Far',
       '',
       '09354270097',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scout Diaz',
       'emma Diaz',
       '',
       '09177708883',
       '',
       '0',
       ''
UNION ALL
SELECT 'ACE Ejera',
       'Emma Lene Ejera',
       '',
       '09772570295',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHEWY Martinez',
       'Emman Martinez',
       '',
       '09292976474',
       '',
       '0',
       ''
UNION ALL
SELECT 'SPRUCE Tiro',
       'Ena Marie Tiro',
       '',
       '09956275510',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kazuki Tiro',
       'Ena Tiro',
       '',
       '09171593197',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Monique',
       'Erica Monique',
       '',
       '09560188078',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Sanchez',
       'Erlin Sanchez',
       '',
       '09175993936',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jake Tagaro',
       'Erson Tagaro',
       '',
       '09558834330',
       '',
       '0',
       ''
UNION ALL
SELECT 'BLACK PUG Ramos',
       'Erwin Blair Ramos',
       '',
       '09178223665',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rover Galarrita',
       'Ethan Sarge Galarrita',
       '',
       '09778105684',
       '',
       '0',
       ''
UNION ALL
SELECT 'PEW-PEW Langit',
       'Eula Maritoni Langit',
       '',
       '09559720579',
       '',
       '0',
       ''
UNION ALL
SELECT 'Granja Sobremisana',
       'Eunice Sobremisana',
       '',
       '09150476706',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Cho',
       'Eunsook Cho',
       '',
       '09154940647',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chichay Borja',
       'Eva Borja',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Buhay',
       'Eva Buhay',
       '',
       '09393882093',
       '',
       '0',
       ''
UNION ALL
SELECT 'Baby Dags and Papi Galgao',
       'Eva Galgao',
       '',
       '09354594912',
       '',
       '0',
       ''
UNION ALL
SELECT 'BALOO CIRI Olape',
       'Eva Mae Olape',
       '',
       '09954660678',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHUCHAY Olape',
       'Eva Mae Olape',
       '',
       '09954660678',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mowgli Olape',
       'Eva Mae Olape',
       '',
       '09954660678',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Olape',
       'Eva Olape',
       '',
       '09954660678',
       '',
       '0',
       ''
UNION ALL
SELECT 'MAKI AND DABI Padilla',
       'Evangeline Padilla',
       '',
       '09062576474',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Padilla',
       'Evangeline Padilla',
       '',
       '09062576474',
       '',
       '0',
       ''
UNION ALL
SELECT 'RED Padilla',
       'Evangeline Padilla',
       '',
       '09062576474',
       '',
       '0',
       ''
UNION ALL
SELECT 'AKELA Olape',
       'Eve Olape',
       '',
       '09954660678',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lebron Datumulok',
       'Evelyn Datumulok',
       '',
       'ask doc mhai',
       '',
       '0',
       ''
UNION ALL
SELECT 'Daiso Suarez',
       'Evelyn Suarez',
       '',
       '09265574651',
       '',
       '0',
       ''
UNION ALL
SELECT 'Seve Suarez',
       'Evelyn Suarez',
       '',
       '09265574651',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bingo Factura',
       'Factura',
       '',
       '09174464202',
       '',
       '0',
       ''
UNION ALL
SELECT 'Black Boctot',
       'Faith Boctot',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marky Dalidig',
       'Fatima Dalidig',
       '',
       '09053777966',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky and Star Nicdao',
       'Fel Nicdao',
       '',
       '09364527600',
       '',
       '0',
       ''
UNION ALL
SELECT 'Killer Quililan',
       'Fernando Quililan',
       '',
       '09175134957',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiyoshi Yanit',
       'Feuna Yanit',
       '',
       '09266659869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiddy Enoviso',
       'Filma Enoviso',
       '',
       '09360462208',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gigi Namuag',
       'FirstName Lastname',
       '',
       '09052977956',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZORO Fernan',
       'Florence Parson Fernan',
       '',
       '09062970901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pinchy Aba-a',
       'Florian Aba-a',
       '',
       '09228752789',
       '',
       '0',
       ''
UNION ALL
SELECT 'WOW TAN',
       'Frances Tan',
       '',
       '09252632683',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mocha Tan',
       'Francine Tan',
       '',
       '09177063726',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tami Tan',
       'Francine Tan',
       '',
       '09177063726',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucy Serrano',
       'Francis Arnel Serrano',
       '',
       '09458338792',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chico Jandayan',
       'Francis Elmar Jandayan',
       '',
       '091642565851',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Jandayan',
       'Francis Elmar Jandayan',
       '',
       '091642565851',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies5 Jandayan',
       'Francis Elmar Jandayan',
       '',
       '091642565851',
       '',
       '0',
       ''
UNION ALL
SELECT 'BELLA JANDAYAN',
       'FRANCIS ELMER JANDAYAN',
       '',
       '09164265851',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Facturan',
       'Francis Facturan',
       '',
       '09553310486',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 7 Jandayan',
       'FRANCIS JANDAYAN',
       '',
       '09164265851',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cheska Moreto',
       'Francis Moreto',
       '',
       '09363981964',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Frannie',
       'Frannie Manalastas',
       '',
       '09476459195',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panda Manalastas',
       'Frannie Manalastas',
       '',
       '09476459195',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo Magallanes',
       'Franz Magallanes',
       '',
       '09277564921',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Serenio',
       'Frenz Serenio',
       '',
       '09063964029',
       '',
       '0',
       ''
UNION ALL
SELECT 'Onni Serenio',
       'Frenz Serenio',
       '',
       '09063964029',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mini Tidoy',
       'Gabbie Tidoy',
       '',
       '09278463393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Uy',
       'Gabby Uy',
       '',
       '09177048088',
       '',
       '0',
       ''
UNION ALL
SELECT 'MINGKAY Angcog',
       'Gabriel Angcog',
       '',
       '09666528565',
       '',
       '0',
       ''
UNION ALL
SELECT 'TIN-TIN Angcog',
       'Gabriel Angcog',
       '',
       '09666528565',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tuting Gagnan',
       'Gagnan',
       '',
       '09654710421',
       '',
       '0',
       ''
UNION ALL
SELECT 'Layla Anabieza',
       'Garfield Anabieza',
       '',
       '09358541946',
       '',
       '0',
       ''
UNION ALL
SELECT 'KObe Moreno',
       'Gary Moreno',
       '',
       '09363903853',
       '',
       '0',
       ''
UNION ALL
SELECT 'Diego Damasing',
       'Gay Damasing',
       '',
       '09175316392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dora Damasing',
       'Gay Damasing',
       '',
       '09175316392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Swiper Damasing',
       'Gay Damasing',
       '',
       '09175316392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Getuaban',
       'Geisha Mae Getuaban',
       '',
       '09568256265',
       '',
       '0',
       ''
UNION ALL
SELECT 'TEAMORE Plaza',
       'Gemma Plaza',
       '',
       '09972556788',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wisky Ybanez',
       'Gena Ybanez',
       '',
       '09551870041',
       '',
       '0',
       ''
UNION ALL
SELECT 'REX Sumayag',
       'Genevive Sumayag',
       '',
       '09356366042',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hanary Ang',
       'George Ang',
       '',
       '09177061689',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coffee Jandayan',
       'Gerald Jandayan',
       '',
       '09066109729',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sersha Carnaje',
       'Geraldine Carnaje',
       '',
       '09177068644',
       '',
       '0',
       ''
UNION ALL
SELECT 'GIGI Babanto',
       'Germaine Babanto',
       '',
       '09560165790',
       '',
       '0',
       ''
UNION ALL
SELECT 'Taca Dela Pena',
       'Gevy Rose Dela Pena',
       '',
       '09355504140',
       '',
       '0',
       ''
UNION ALL
SELECT 'TALA Dela Pena',
       'Gevy Rose Dela Pena',
       '',
       '09355504140',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloud Estandarte',
       'Gigi Estandarte',
       '',
       '09265565662',
       '',
       '0',
       ''
UNION ALL
SELECT 'GAEO Guzman',
       'Gilian Guzman',
       '',
       '09659022856',
       '',
       '0',
       ''
UNION ALL
SELECT 'GAEO Guzman',
       'Gilian Guzman',
       '',
       '09659022856',
       '',
       '0',
       ''
UNION ALL
SELECT 'chuta2 ipan',
       'ginalou ipan',
       '',
       '09057683933',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucky Escabusa',
       'Giselle Escabusa',
       '',
       '09175158114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Onew Escabusa',
       'Giselle Escabusa',
       '',
       '09175158114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Labial',
       'Gizelle Labial',
       '',
       '09273319605',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chokie Glad',
       'glad',
       '',
       '09658516016',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nala Selerio',
       'Gladys Jane Selerio',
       '',
       '09264866229',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cheekee Khobuntin',
       'Glecelle Khobuntin',
       '',
       '09265089834',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chava Go',
       'Glenn Go',
       '',
       '09756354522',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kalvin Salvacion',
       'Glenn Salvacion',
       '',
       '0997753666',
       '',
       '0',
       ''
UNION ALL
SELECT 'Taurus Bacong',
       'Glicerio Bacong',
       '',
       '09777008001',
       '',
       '0',
       ''
UNION ALL
SELECT 'MELODY Magno',
       'Gloria Mae Magno',
       '',
       '09178884991',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ming-ming Magno',
       'Gloria Mae Magno',
       '',
       '09178884991',
       '',
       '0',
       ''
UNION ALL
SELECT 'FRODO JAVA',
       'GLORIO  JAVA jr.',
       '',
       '09176262914',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUCAS JAVA',
       'GLORIO  JAVA jr.',
       '',
       '09176262914',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shegeyo Java',
       'GLORIO  JAVA jr.',
       '',
       '09176262914',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba  Java',
       'GLORIO  JAVA jr.',
       '',
       '09176262914',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bambam Romerde',
       'GLory Jane Romerde',
       '',
       '09177013507',
       '',
       '0',
       ''
UNION ALL
SELECT 'CLOIE Romerde',
       'GLory Jane Romerde',
       '',
       '09177013507',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dagul Romerde',
       'GLory Jane Romerde',
       '',
       '09177013507',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paquito Aberin',
       'GLory Jane Romerde',
       '',
       '09177013507',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sprincle Lanipa',
       'Glory Lanipa',
       '',
       '09773301666',
       '',
       '0',
       ''
UNION ALL
SELECT 'HERSHEY Murillo',
       'Golda Murillo',
       '',
       '09177701099',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba Larios',
       'Gorby Larios',
       '',
       '09273907186',
       '',
       '0',
       ''
UNION ALL
SELECT 'PIKACHU Bagonsos',
       'Grace Bagonsos',
       '',
       '096650555830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rain Balawag',
       'Grace Balawag',
       '',
       '09059215774',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sun Balawag',
       'Grace Balawag',
       '',
       '09059215774',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Paye',
       'Gries Paye',
       '',
       '09566434240',
       '',
       '0',
       ''
UNION ALL
SELECT 'Quene Divinagracia',
       'Guiller Divinagracia',
       '',
       '09555648850',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brown Acera',
       'Gwen Acera',
       '',
       '09457961422',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 2 Acera',
       'Gwen Acera',
       '',
       '09457961422',
       '',
       '0',
       ''
UNION ALL
SELECT 'white brown ACERA',
       'Gwen Acera',
       '',
       '09457961422',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kitkat Amron',
       'Haipa Amron',
       '',
       '09666606402',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOon Amron',
       'Haipa Amron',
       '',
       '09666606402',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba Mapandi',
       'Hanna Mapandi',
       '',
       '09177054898',
       '',
       '0',
       ''
UNION ALL
SELECT 'ASHTONE Javier',
       'Harold Javier',
       '',
       '09659023066',
       '',
       '0',
       ''
UNION ALL
SELECT 'Claude Estrada',
       'Hazel Barbie Estrada',
       '',
       '09059554116',
       '',
       '0',
       ''
UNION ALL
SELECT 'LeslEy Estrada',
       'Hazel Barbie Estrada',
       '',
       '09059554116',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chichay Jampit',
       'Hazel Jampit',
       '',
       '09061854865',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHIKO Jampit',
       'Hazel Jampit',
       '',
       '09061854865',
       '',
       '0',
       ''
UNION ALL
SELECT 'BOMBER Ares',
       'Herbert Ares',
       '',
       '09175044275',
       '',
       '0',
       ''
UNION ALL
SELECT 'MEEMONG Ares',
       'Herbert Ares',
       '',
       '09175044275',
       '',
       '0',
       ''
UNION ALL
SELECT 'GEORGE Echavez',
       'Hereclou Echavez',
       '',
       '089355173671',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mochi Gighe',
       'Herzi Gighe',
       '',
       '09954747876',
       '',
       '0',
       ''
UNION ALL
SELECT 'Remy Examinada',
       'Hezel Grace Examinada',
       '',
       '09261458365',
       '',
       '0',
       ''
UNION ALL
SELECT 'Remy Examinada',
       'Hezel Grace Examinada',
       '',
       '09261458365',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOBEE Jiminez',
       'Honey Jiminez',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kuney Love',
       'Honey Love',
       '',
       '09276428454',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sugar Pagayan',
       'Hope Pagayan',
       '',
       '09751091009',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANDY Madki',
       'Hussein Madki',
       '',
       '09173156403',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANNE Madki',
       'Hussein Madki',
       '',
       '09173156403',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANNIE Madki',
       'Hussein Madki',
       '',
       '09173156403',
       '',
       '0',
       ''
UNION ALL
SELECT 'ARCHIE Madki',
       'Hussein Madki',
       '',
       '09173156403',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUPIN   MADKI',
       'Hussein Madki',
       '',
       '09173156403',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dukki Balingit',
       'Ian Mark Balingit',
       '',
       '09260739313',
       '',
       '0',
       ''
UNION ALL
SELECT 'mocci Balingit',
       'Ian Mark Balingit',
       '',
       '09260739313',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sammi Balingit',
       'Ian Mark Balingit',
       '',
       '09260739313',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucky Celerimos',
       'Ian Mitch Celerimos',
       '',
       '09518385418',
       '',
       '0',
       ''
UNION ALL
SELECT 'PUPS OF CHOWCHOW Timogan',
       'Iby Timogan',
       '',
       '09177084157',
       '',
       '0',
       ''
UNION ALL
SELECT 'Carter Waga',
       'Ike Waga',
       '',
       '09069287899',
       '',
       '0',
       ''
UNION ALL
SELECT 'Billy Roa',
       'Ina Roa',
       '',
       '09177916597',
       '',
       '0',
       ''
UNION ALL
SELECT 'CASPER Hassanodin',
       'Ingko Hassanodin',
       '',
       '09487721995',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Hassanodin',
       'Ingko Hassanodin',
       '',
       '09487721995',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lang2 Hassanodin',
       'Ingko Hassanodin',
       '',
       '09487721995',
       '',
       '0',
       ''
UNION ALL
SELECT 'Percy Hassanodin',
       'Ingko Hassanodin',
       '',
       '09487721995',
       '',
       '0',
       ''
UNION ALL
SELECT 'UNO Hassanodin',
       'Ingko Hassanodin',
       '',
       '09487721995',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy DeAustria',
       'Inigo deAustria',
       '',
       '09778550176',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chewie deAustria',
       'Inigo deAustria',
       '',
       '09778550176',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Co',
       'Irene Co',
       '',
       '09177122610',
       '',
       '0',
       ''
UNION ALL
SELECT 'OREO Lavina',
       'Irish Lavina',
       '',
       '09057750010',
       '',
       '0',
       ''
UNION ALL
SELECT 'OREO Lavina',
       'Irish Lavina',
       '',
       '09057750010',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hershey Pascua',
       'Irish Pascua',
       '',
       '09069009975',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hershey Pascua',
       'Irish Pascua',
       '',
       '09069009975',
       '',
       '0',
       ''
UNION ALL
SELECT 'Moana Belen',
       'Isabela Belen',
       '',
       '0953481833',
       '',
       '0',
       ''
UNION ALL
SELECT 'Barats Duramparam',
       'Ishei Duramparam',
       '',
       '09676711900',
       '',
       '0',
       ''
UNION ALL
SELECT 'Doodle &  MAUO Duramparam',
       'Ishei Duramparam',
       '',
       '09676711900',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloudy Narvasa',
       'Isidro Narvasa',
       '',
       '09069527550',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zeus Ybanez',
       'Ismael Ybanez',
       '',
       '09267066910',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bro Alago',
       'Jack Alago',
       '',
       '09356778464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Ong',
       'Jacqueline Ong',
       '',
       '09176877146',
       '',
       '0',
       ''
UNION ALL
SELECT 'Eldy Baldos',
       'Jaina Baldos',
       '',
       '09177151841',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ayah Malic',
       'Jalanie Malic',
       '',
       '09069635547',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marimar Malic',
       'Jalanie Malic',
       '',
       '09069635547',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sunday Malic',
       'Jalanie Malic',
       '',
       '09069635547',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chico Baes',
       'Jamel Baes',
       '',
       '09559745379',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jervis Bernardino',
       'James Bernardino',
       '',
       '09954585828',
       '',
       '0',
       ''
UNION ALL
SELECT 'ChowChow Binoya',
       'James Binoya',
       '',
       '09294613098/8573145',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yisha Ignacio',
       'James Ignacio',
       '',
       '09750446110',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sonic Natan',
       'James Smash Natan',
       '',
       '09552872273',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Waga',
       'jAMES Waga',
       '',
       '09260819036',
       '',
       '0',
       ''
UNION ALL
SELECT 'Belly Chio',
       'Jamie Chio',
       '',
       '09177773361',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maxie Chio',
       'Jamie Chio',
       '',
       '09177773361',
       '',
       '0',
       ''
UNION ALL
SELECT 'TEO Chio',
       'Jamie Chio',
       '',
       '09177773361',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rosie Austin',
       'Jandrey Austin',
       '',
       '09177108069',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mochi Lumbay',
       'Jane Lumbay',
       '',
       '09362588230',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiny Lumbay',
       'Jane Lumbay',
       '',
       '09362588230',
       '',
       '0',
       ''
UNION ALL
SELECT 'Potpot Medija',
       'Jane Medija',
       '',
       '09759394753',
       '',
       '0',
       ''
UNION ALL
SELECT 'Krimmy Baldo',
       'Janessa Baldo',
       '',
       '09171662823',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maggi Baldo',
       'Janessa Baldo',
       '',
       '09171662823',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blue Desquito',
       'janet Desquito',
       '',
       '09175485488',
       '',
       '0',
       ''
UNION ALL
SELECT 'Andi Bacang',
       'Janette Bacang',
       '',
       '09773521394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tamtam Baang',
       'Janette Bacang',
       '',
       '09773521394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Boter',
       'Janice Boter',
       '',
       '09060586821',
       '',
       '0',
       ''
UNION ALL
SELECT 'Covi Sumaylo',
       'Janneth Sumaylo',
       '',
       '09350919896',
       '',
       '0',
       ''
UNION ALL
SELECT 'IGOR Villono',
       'Janno Villono',
       '',
       '09457936566',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOBE Roa',
       'jaquelin Roa',
       '',
       '09051877530',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rocky and Nugget Roa',
       'jaquelin Roa',
       '',
       '09051877530',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nala  Gamayon',
       'Jared Gamayon',
       '',
       '09171862306',
       '',
       '0',
       ''
UNION ALL
SELECT 'JD Lauderes',
       'Jarrie Mae Lauderes',
       '',
       '09289703926',
       '',
       '0',
       ''
UNION ALL
SELECT 'WHISKEY Lauderes',
       'Jarrie Mae Lauderes',
       '',
       '09289703926',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nobu Pandi',
       'Jasmin Pandi',
       '',
       '09177065479',
       '',
       '0',
       ''
UNION ALL
SELECT 'YUKI Ching',
       'Jasmine Ching',
       '',
       '09177122768',
       '',
       '0',
       ''
UNION ALL
SELECT 'WENDY CHU',
       'Jason chu',
       '',
       '09953328829',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jassi Sarte',
       'Jason Sarte',
       '',
       '090574651154',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ace Villono',
       'Jason Villono',
       '',
       '09260774464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Admiral Villono',
       'Jason Villono',
       '',
       '09260774464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Argus Villono',
       'Jason Villono',
       '',
       '09260774464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Atlas Villono',
       'Jason Villono',
       '',
       '09260774464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gypsy Villono',
       'Jason Villono',
       '',
       '09260774464',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Janubas',
       'Javia Irie Janubas',
       '',
       '09569060638',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppy Lavina',
       'Jay Lavina',
       '',
       '09452137415',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiki Loyola',
       'Jay Loyola',
       '',
       '09058894857',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Manginsay',
       'Jay Manginsay',
       '',
       '09274288789',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alpha Ormillada',
       'Jay Ormillada',
       '',
       '09066601841',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tokyo Undo',
       'Jay Stephen Undo',
       '',
       '09366654729',
       '',
       '0',
       ''
UNION ALL
SELECT 'ASHER Aclan',
       'Jay-ar Aclan',
       '',
       '09566452908',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Nicolas',
       'Jaymar Nicoca',
       '',
       '09352834750',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bowlsco Nicolas',
       'Jaymar Nicolas',
       '',
       '09177714622',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chiko Nicolas',
       'Jaymar Nicolas',
       '',
       '09177714622',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUCKY Urbano',
       'Jaymie Urbano',
       '',
       '09056002409',
       '',
       '0',
       ''
UNION ALL
SELECT 'Angela Sanchez',
       'Jayno Sanchez',
       '',
       '09265736830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cutie Sanchez',
       'Jayno Sanchez',
       '',
       '09265736830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Empay Sanchez',
       'Jayno Sanchez',
       '',
       '09265736830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gusion Sanchez',
       'Jayno Sanchez',
       '',
       '09265736830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coby/KOBE Doromal',
       'jaypee Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luke Doromal',
       'jaypee Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUKIE Doromal',
       'jaypee Doromal',
       '',
       '09150482588',
       '',
       '1',
       ''
UNION ALL
SELECT 'MAX Doromal',
       'jaypee Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mikay Doromal',
       'jaypee Doromal',
       '',
       '09150482588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Guido Penacoga',
       'Jayquel Penacoga',
       '',
       '09468520150',
       '',
       '0',
       ''
UNION ALL
SELECT 'ARYA Clementir',
       'Jayson Clementir',
       '',
       '09273173566',
       '',
       '0',
       ''
UNION ALL
SELECT 'Togo Toledo',
       'Jb Toledo',
       '',
       '09177747703',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tohu  Jacutin',
       'Jc Jacutin',
       '',
       '09999947650',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zaru Aventurado',
       'Jeah Aventurado',
       '',
       '09554968977',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chi-chi Canoy',
       'Jean Canoy',
       '',
       '09067861506',
       '',
       '0',
       '22-Oct-19'
UNION ALL
SELECT 'Rochii Lagrama',
       'Jean Marrie Lagrama',
       '',
       '09273531948',
       '',
       '0',
       ''
UNION ALL
SELECT 'Willa Ladica',
       'Jeana Ladica',
       '',
       '09356363370',
       '',
       '0',
       ''
UNION ALL
SELECT 'Stella Sumagang',
       'Jeannie Sumagang',
       '',
       '09177161970',
       '',
       '0',
       ''
UNION ALL
SELECT 'Stephanie Sumagang',
       'Jeannie Sumagang',
       '',
       '09177161970',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Daabay',
       'Jefferso Daabay',
       '',
       '09161178356',
       '',
       '0',
       ''
UNION ALL
SELECT 'King Lagbas',
       'Jeffrhey Lagbas',
       '',
       '09171351124',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ehgo Samuya',
       'Jefre Samuya',
       '',
       '09776495392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cashmere Banez',
       'Jel Banez',
       '',
       '09750454114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snoopy Dela Cruz',
       'Jello Dela Cruz',
       '',
       '09177597909',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zoey Dela Cruz',
       'Jemer Dela Cruz',
       '',
       '09178192742',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chubi Acenas',
       'Jenn Marie Acenas',
       '',
       '09362257918',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jekay Magallanes',
       'Jennelyn Magallanes',
       '',
       '09556777691',
       '',
       '0',
       ''
UNION ALL
SELECT 'LOLA Cariado',
       'Jennie Cariado',
       '',
       '09676619981',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pocholo Baldon',
       'Jennifer Baldon',
       '',
       '09154906913',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rover Llubit',
       'Jennifer Llubit',
       '',
       '09177173404',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bernadeth Odi',
       'Jennifer Odi',
       '',
       '09459738539',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maldita Odi',
       'Jennifer Odi',
       '',
       '09459738539',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dolce Ceniza',
       'Jenny Kyle Ceniza',
       '',
       '09979616865',
       '',
       '0',
       ''
UNION ALL
SELECT 'NICOLE Nuada',
       'JENNY NUADA',
       '',
       '09265737232',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Sundol',
       'Jenny Sundol',
       '',
       '09363674818',
       '',
       '0',
       ''
UNION ALL
SELECT 'SHIN Beja',
       'Jennylyn Beja',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloud Tagarda',
       'Jeremy Tagarda',
       '',
       '09953548659',
       '',
       '0',
       ''
UNION ALL
SELECT 'noname Lumbay',
       'Jeric Lumbay',
       '',
       '09362588230',
       '',
       '0',
       ''
UNION ALL
SELECT 'MONA Mendoza',
       'Jericho Mendoza',
       '',
       '09350945743',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gram Requerme',
       'Jerlie Requerme',
       '',
       '09177185881',
       '',
       '0',
       ''
UNION ALL
SELECT 'Irish Requerme',
       'Jerlie Requerme',
       '',
       '09177185881',
       '',
       '0',
       ''
UNION ALL
SELECT 'Samurai Requerme',
       'Jerlie Requerme',
       '',
       '09177185881',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOPER Daabay',
       'Jerome Daabay',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'Golda Daabay',
       'Jerome Daabay',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Daabay',
       'Jerome Daabay',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'ruby daabay',
       'Jerome Daabay',
       '',
       '09973501979',
       '',
       '0',
       ''
UNION ALL
SELECT 'Francisco Lim',
       'Jerome Lim',
       '',
       '09979566419',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pretzel Pabillaran',
       'Jerome Pabillaran',
       '',
       '09177168545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bugsy Soldevilla',
       'Jerome Soldevilla',
       '',
       '09667659247',
       '',
       '0',
       ''
UNION ALL
SELECT 'ChoLOKoy Pulido',
       'Jerry Pulido',
       '',
       '0967483459',
       '',
       '0',
       ''
UNION ALL
SELECT 'Billa Singu',
       'Jerry Singu',
       '',
       '09164003828',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chooey Nagac',
       'Jerryfel Nagac',
       '',
       '09164853715',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mikmik Lomongo',
       'Jesan Lomongo',
       '',
       '09650547552',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cassie Pacorda',
       'Jessa pacorda',
       '',
       '09071585330',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maxus Pesaldero',
       'Jesse Roy Pescaldero',
       '',
       '09988442395',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pablo Galleto',
       'Jeth Galleto',
       '',
       '09057021719',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mickey Cabahug',
       'Jewel Jane Cabahug',
       '',
       '09058873713',
       '',
       '0',
       ''
UNION ALL
SELECT 'TOFU Mertalla',
       'Jeze An Mertalla',
       '',
       '09171449874',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tazz Silva',
       'Jezrel Silva',
       '',
       '09658836289',
       '',
       '0',
       ''
UNION ALL
SELECT 'Meallow Cotongan',
       'Jhasmisah Cotongan',
       '',
       '09121562401',
       '',
       '0',
       ''
UNION ALL
SELECT 'GEOrgina Abrazado',
       'Jhoanna Abrazado',
       '',
       '09163374351',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kobe Abrazado',
       'Jhoanna Abrazado',
       '',
       '09163374351',
       '',
       '0',
       ''
UNION ALL
SELECT 'Atotot Jubane',
       'jhoanne Jubane',
       '',
       '09758834205',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bluey Aput',
       'Jhon Kenneth Aput',
       '',
       '0967653077',
       '',
       '0',
       ''
UNION ALL
SELECT 'Loki Aput',
       'Jhon Kenneth Aput',
       '',
       '0967653077',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bruno Ofiana',
       'Jhon Ofiana',
       '',
       '09051445443',
       '',
       '0',
       ''
UNION ALL
SELECT 'aldous asilan',
       'Jhondy Asilan',
       '',
       '09059037060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mocha Asilan',
       'Jhondy Asilan',
       '',
       '09059037060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chu-chu Zambrano',
       'Jhunjhun Zambrano',
       '',
       '09261488193',
       '',
       '0',
       ''
UNION ALL
SELECT 'KYTE Taala',
       'Jiffer john Taala',
       '',
       '09358377207',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kama-kama Padilla',
       'Jimmy Padilla',
       '',
       '09177167895',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lava Padilla',
       'Jimmy Padilla',
       '',
       '09177167895',
       '',
       '0',
       ''
UNION ALL
SELECT 'PAAKA Padilla',
       'Jimmy Padilla',
       '',
       '09177167895',
       '',
       '0',
       ''
UNION ALL
SELECT 'Xander Padilla',
       'Jimmy Padilla',
       '',
       '09177167895',
       '',
       '0',
       ''
UNION ALL
SELECT 'JCO David',
       'Jing David',
       '',
       '09759408517',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOCHA Go',
       'Jireh Go',
       '',
       '09659023311',
       '',
       '0',
       ''
UNION ALL
SELECT 'Macmac SY',
       'Jiseca Sy',
       '',
       '09177728076',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ashley Bacarro',
       'Jm Bacarro',
       '',
       '09368415784',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Bacarro',
       'Jm Bacarro',
       '',
       '09368415784',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chippy Cabahug',
       'jm cabahug',
       '',
       '09757199061',
       '',
       '0',
       ''
UNION ALL
SELECT 'George Abines',
       'Joan Abines',
       '',
       '09203084471',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luca Capistrano',
       'Joan Capistran',
       '',
       '09778036579',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucy Capistrano',
       'Joan Capistran',
       '',
       '09778036579',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maple Luminarias',
       'Joanalyn Luminarias',
       '',
       '09053942015',
       '',
       '0',
       ''
UNION ALL
SELECT 'Trump Luminarios',
       'Joanalyn Luminarias',
       '',
       '09053942015',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ginger Butalid',
       'Joanna Butalid',
       '',
       '09666528565',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mingkay Butalid',
       'Joanna Butalid',
       '',
       '09666528565',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuko Butalid',
       'Joanna Butalid',
       '',
       '09666528565',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nairobi Suson',
       'Joanna Suson',
       '',
       '09335799988',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tokyo Suson',
       'Joanna Suson',
       '',
       '09335799988',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ashley Monje',
       'JoAnnette Monje',
       '',
       '09276360390',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chingkee Monje',
       'JoAnnette Monje',
       '',
       '09276360390',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lulu Monje',
       'JoAnnette Monje',
       '',
       '09276360390',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bruno Racaza',
       'Job Racaza',
       '',
       '09151699878',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cholo Rabadon',
       'Jocelyn Ramadon',
       '',
       '09971174784',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chacha Tadle',
       'Jocelyn Tadle',
       '',
       '09972372723',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Virgo',
       'Joday S. Virgo',
       '',
       '09556488992',
       '',
       '0',
       ''
UNION ALL
SELECT 'toxido quililan',
       'Joe Fernando Quililan',
       '',
       '09164840455',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cholo Layon',
       'Joe Layon',
       '',
       '09959303099',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUNO DELOSO',
       'JOE MARI DELOSO',
       '',
       '09154634376',
       '',
       '0',
       ''
UNION ALL
SELECT 'SIMBA Deloso',
       'JOE MARI DELOSO',
       '',
       '09154634376',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mong Ryu',
       'Joehun Ryu',
       '',
       '09165567554',
       '',
       '0',
       ''
UNION ALL
SELECT 'Adones Delminguez',
       'Joel Delminguez',
       '',
       '09354760328',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jaguar Delminguez',
       'Joel Delminguez',
       '',
       '09354760328',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ozzy Posadas',
       'Joeram Posadas II',
       '',
       '09175310222',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Sarip',
       'Johanna Sarip',
       '',
       '09455492411',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUNO Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'KIM Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'KIM Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'MARS Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'MINGKAY Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'TIN-TIN Angcog',
       'John Angcog',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy Lu',
       'John Lu',
       '',
       '09176234989',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 3 Paumar',
       'john patrick paumar',
       '',
       '09955895030',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUNA Kwong',
       'John Paul Kwong',
       '',
       '09367874912',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sundae Valdehueza',
       'John Paul Valdehueza',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Langga Samarista',
       'John Samarista',
       '',
       '09209585407',
       '',
       '0',
       ''
UNION ALL
SELECT 'Willow Tecson',
       'John Tecson',
       '',
       '09974120071',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mucho Ermio',
       'Johna Ermio',
       '',
       '09357571816',
       '',
       '0',
       ''
UNION ALL
SELECT 'YUKI Tolibas',
       'Johney Tolibas',
       '',
       '09055508013',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spark Ke-e',
       'Joie Annika Ke-e',
       '',
       '09175591459',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chippy Cabahug',
       'Jojie Mae Cabahug',
       '',
       '09757199061',
       '',
       '0',
       ''
UNION ALL
SELECT 'BYTE Taala',
       'Jon Jacob Taala',
       '',
       '09650557714',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zoe Salazar',
       'jonathan salazar',
       '',
       '09173916301',
       '',
       '0',
       ''
UNION ALL
SELECT 'Noname Alba',
       'Jong Alba',
       '',
       '09355153536',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cham Beatingo',
       'Jonles Beatingo',
       '',
       '09177706727',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paopao & Cookie Leyterio',
       'jordan Leyterio',
       '',
       '09068704663',
       '',
       '0',
       ''
UNION ALL
SELECT 'Noah Bacalan',
       'Jorge Bacalan',
       '',
       '09758726801',
       '',
       '0',
       ''
UNION ALL
SELECT 'Husky Comon',
       'Jorge Comon',
       '',
       '09056981117',
       '',
       '0',
       ''
UNION ALL
SELECT 'LULU Comon',
       'Jorge Comon',
       '',
       '09056981117',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kim Que',
       'Jose Antonio Que',
       '',
       '09177153000',
       '',
       '0',
       ''
UNION ALL
SELECT 'Baby bear Laxina',
       'Jose Jay Laxina',
       '',
       '09067659098',
       '',
       '0',
       ''
UNION ALL
SELECT 'Logan Cabigas',
       'Jose Lorenzo Cabigas',
       '',
       '09173036548',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tank Cabigas',
       'Jose Lorenzo Cabigas',
       '',
       '09173036548',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snigger Ladao',
       'Josefine Ladao',
       '',
       '09262244004',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wooyo and Oreo Choi',
       'Josep Choi',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Balindres',
       'Joseph Balindres',
       '',
       '09650559600',
       '',
       '0',
       ''
UNION ALL
SELECT 'Copper Galorio',
       'Joseph Galorio',
       '',
       '09558011232',
       '',
       '0',
       ''
UNION ALL
SELECT 'SHINJI Pernites',
       'Joseph James Pernites',
       '',
       '09177705708',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sabby Ulbenario',
       'Joseph Ulbenario',
       '',
       '09353195251',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pompom Joshua',
       'Joshua',
       '',
       '09534381832',
       '',
       '0',
       ''
UNION ALL
SELECT 'TANYA Merla',
       'Joshua Merla',
       '',
       '09356787977',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zoro Macabenta',
       'Jovannie Macabenta',
       '',
       '09261914685',
       '',
       '0',
       ''
UNION ALL
SELECT 'Callie Culanggo',
       'Jovel Culanggo',
       '',
       '09363516988',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gummy Ang',
       'Jovelle Jane Ang',
       '',
       '09531875233',
       '',
       '0',
       '18-Nov-21'
UNION ALL
SELECT 'KIMCHI Ang',
       'Jovelle Jane Ang',
       '',
       '09531875233',
       '',
       '0',
       ''
UNION ALL
SELECT 'Prince Ang',
       'Jovelle Jane Ang',
       '',
       '09531875233',
       '',
       '0',
       ''
UNION ALL
SELECT 'VictorIA Ong',
       'Jovert Ong',
       '',
       '09051457338',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paquito Aberin',
       'Jowella Aberin',
       '',
       '09569652280',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peachos Castica',
       'Joy Castica',
       '',
       '09176258256',
       '',
       '0',
       ''
UNION ALL
SELECT 'MESSI neri',
       'Joy Neri',
       '',
       '09209681814',
       '',
       '0',
       ''
UNION ALL
SELECT '',
       'JP Fournter',
       '',
       '09399177113',
       '',
       '',
       ''
UNION ALL
SELECT 'Veeda Abcede',
       'Jubith Abcede',
       '',
       '09950934060',
       '',
       '0',
       ''
UNION ALL
SELECT 'Duck Pacursa',
       'Jucelle Kae Pacursa',
       '',
       '09266579582',
       '',
       '0',
       ''
UNION ALL
SELECT 'Flicker Pacursa',
       'Jucelle Kae Pacursa',
       '',
       '09266579582',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brownies David',
       'Jude David',
       '',
       '09662756142',
       '',
       '0',
       ''
UNION ALL
SELECT 'Drogon Paglinawan',
       'Judith Irene Paglinawan',
       '',
       '09177065295',
       '',
       '0',
       ''
UNION ALL
SELECT 'Broody Gutang',
       'Jufel Gutang',
       '',
       '09778024822',
       '',
       '0',
       ''
UNION ALL
SELECT 'Itzy/POCA Gutang',
       'Jufel Gutang',
       '',
       '09778024822',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tazi/LUFFY Gutang',
       'Jufel Gutang',
       '',
       '09778024822',
       '',
       '0',
       ''
UNION ALL
SELECT 'MELO Bonje',
       'Jules Bonje',
       '',
       '09756231749',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bo Ontalan',
       'Julia Ontalan',
       '',
       '09066007864',
       '',
       '0',
       ''
UNION ALL
SELECT 'yuki Ontalan',
       'Julia Ontalan',
       '',
       '09066007864',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOBE Magsalay',
       'Julia Rae Magsalay',
       '',
       '09177108069',
       '',
       '0',
       ''
UNION ALL
SELECT 'Amari Vacalares',
       'Julia Vacalares',
       '',
       '09656548258',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOJITOS Vacalares',
       'Julia Vacalares',
       '',
       '09656548258',
       '',
       '0',
       ''
UNION ALL
SELECT 'Matmat Labiano',
       'Julie Labiano',
       '',
       '09556500771',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tapi Labiano',
       'Julie Labiano',
       '',
       '09556500771',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tapi Labiano',
       'Julie Labiano',
       '',
       '09556500771',
       '',
       '0',
       ''
UNION ALL
SELECT 'BACHUCHAI Mendoza',
       'Julie Pearl Mendoza',
       '',
       '09164339254',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tommy Roy',
       'Julie Roy',
       '',
       '093591713800',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kobe Ecabande',
       'Julio Ecabande',
       '',
       '09051635920',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tender Garcia',
       'Jun Garcia',
       '',
       '09177797943',
       '',
       '0',
       ''
UNION ALL
SELECT 'zia Garcia',
       'Jun Garcia',
       '',
       '09177797943',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zion Garcia',
       'Jun Garcia',
       '',
       '09177797943',
       '',
       '0',
       ''
UNION ALL
SELECT 'MI-MI maw',
       'Jun Maw',
       '',
       '09350942155',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mimi Mawi, Elyas Dale',
       'Jun Mawi',
       '',
       '09350942155',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Maca-ayong',
       'Junalyn Maca-ayong',
       '',
       '09977194850',
       '',
       '0',
       ''
UNION ALL
SELECT 'FIFTH Lopecillo',
       'Junrey Lopecillo',
       '',
       '09368438490',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Galvez',
       'Jupiter Galvez',
       '',
       '09351920654',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOKIE Cadorna',
       'Jury Mie Cadorna',
       '',
       '09171195171',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOPIE Cardona',
       'Jury Mie Cadorna',
       '',
       '09171195171',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOCHA Cardona',
       'Jury Mie Cadorna',
       '',
       '09171195171',
       '',
       '0',
       ''
UNION ALL
SELECT 'POPSI Cardona',
       'Jury Mie Cadorna',
       '',
       '09171195171',
       '',
       '0',
       ''
UNION ALL
SELECT 'Noname Acera',
       'Justine Acera',
       '',
       '09560637185',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy Badal',
       'Justine Badal',
       '',
       '09178248936',
       '',
       '0',
       ''
UNION ALL
SELECT 'brandy dabay',
       'justine dabay',
       '',
       '09552606375',
       '',
       '0',
       ''
UNION ALL
SELECT 'ALPHA Doromal',
       'Justine Doromal',
       '',
       '09356780967',
       '',
       '0',
       ''
UNION ALL
SELECT 'MAX Doromal',
       'Justine Doromal',
       '',
       '09356780967',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tommy Castro',
       'Juvelyn Castro',
       '',
       '09972054045',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cooper Selibio',
       'Kamille Selibio',
       '',
       '0966042809',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chico Enlawan',
       'Kareel Enlawan',
       '',
       '09353743434',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Sevillano',
       'Kareen Sevillano',
       '',
       '09177039633',
       '',
       '0',
       '3-Oct-21'
UNION ALL
SELECT 'Mia Dacut',
       'Karen Dacut',
       '',
       '09565341920',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chona Gacson',
       'Karen Joy Gacson',
       '',
       '09054135195',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ringgo Octubre',
       'Karenn Octubre',
       '',
       '09362743641',
       '',
       '0',
       ''
UNION ALL
SELECT 'Serapio tigulo',
       'Karl Joaquin Tigulo',
       '',
       '09674361751',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wanda Tigulo',
       'Karl Joaquin Tigulo',
       '',
       '09674361751',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jhonny Martes',
       'Karla Martes',
       '',
       '09368490553',
       '',
       '0',
       ''
UNION ALL
SELECT 'Thor Gamolo',
       'Kathlynn Mae Gamolo',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'TRISHA Mabelin',
       'Kathrina Mae Mabelin',
       '',
       '09178942348',
       '',
       '0',
       ''
UNION ALL
SELECT 'Teebo Montecillo',
       'Katrina Montecillo',
       '',
       '09953840599',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Napone',
       'Katrina Napone',
       '',
       '09779624699',
       '',
       '0',
       ''
UNION ALL
SELECT 'Barry Pabelic',
       'Kayleigh Pabelic',
       '',
       '09452141610',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kendra Banaag',
       'Kean Banaag',
       '',
       '09268788075',
       '',
       '0',
       ''
UNION ALL
SELECT 'CORII Bagares',
       'Keisha Bagares',
       '',
       '09176373480',
       '',
       '0',
       ''
UNION ALL
SELECT 'BINO Cuevas',
       'Kelly Cuevas',
       '',
       '09159931607',
       '',
       '0',
       ''
UNION ALL
SELECT 'Silver Cuevas',
       'Kelly Cuevas',
       '',
       '09159931607',
       '',
       '0',
       ''
UNION ALL
SELECT 'SYVIE Cuevas',
       'Kelly Cuevas',
       '',
       '09159931607',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kenna Suzuki',
       'Kenichiso Suzuki',
       '',
       '09173189924',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hiro Labadan',
       'Kenneth John Labadan',
       '',
       '09657206941',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ara Laurea',
       'Kenneth Laurea',
       '',
       '09177111399',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hunter Laurea',
       'Kenneth Laurea',
       '',
       '09177111399',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Noob',
       'Kenneth Noob',
       '',
       '09356519992',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alaska Flores',
       'Kent Flores',
       '',
       '09177731312',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUBBLE TAP Flores',
       'Kent Flores',
       '',
       '09177731312',
       '',
       '0',
       ''
UNION ALL
SELECT 'ELLIE Esguerra',
       'Kevin Joshua Esguerra',
       '',
       '09270799389',
       '',
       '0',
       ''
UNION ALL
SELECT 'Beagle Pungtod',
       'Kevin Pungtod',
       '',
       '09953539866',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dabbi Amolato',
       'keziah Joy Amolato',
       '',
       '09759394587',
       '',
       '0',
       ''
UNION ALL
SELECT 'Khawi Panya',
       'khawi panya',
       '',
       '09353952161',
       '',
       '0',
       ''
UNION ALL
SELECT 'Elle Linaac',
       'Khereen Linaac',
       '',
       '09268362963',
       '',
       '0',
       ''
UNION ALL
SELECT 'Princess Linaac',
       'Khereen Linaac',
       '',
       '09268362963',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bella Hatamosa',
       'Khyle Hatamosa',
       '',
       '09552457717',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bambam Capinpuyan',
       'Kim Capinpuyan',
       '',
       '09273832405',
       '',
       '0',
       ''
UNION ALL
SELECT 'Laim Hyo Hoon',
       'Kim Hyo Hoon',
       '',
       '09569632439',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tori Hyo Hoon',
       'Kim Hyo Hoon',
       '',
       '09569632439',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jake Valledor',
       'Kim Valledor',
       '',
       '09363565052',
       '',
       '0',
       ''
UNION ALL
SELECT 'Caszy Bienes',
       'Kimberly Bienes',
       '',
       '09058888398',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tinkles Gegjie',
       'Kimberly Gegjie',
       '',
       '09166881724',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Pilapil',
       'Kimberly Pilapil',
       '',
       '09277118673',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peugeot Pungtod',
       'Kirby Jay Pungtod',
       '',
       '09176775133',
       '',
       '0',
       ''
UNION ALL
SELECT 'Raiku Pungtod',
       'Kirby Jay Pungtod',
       '',
       '09176775133',
       '',
       '0',
       ''
UNION ALL
SELECT 'Harley Apdian',
       'Kirk Apdian',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Charlie Delos Arcos',
       'Kirk Daryl Delos Arcos',
       '',
       '09359006503',
       '',
       '0',
       ''
UNION ALL
SELECT 'Putin Moring',
       'Kirt Moring',
       '',
       '09973508808',
       '',
       '0',
       ''
UNION ALL
SELECT 'Putin Moring',
       'Kirt Moring',
       '',
       '09973508808',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rubo Pungtod',
       'Kj Pungtod',
       '',
       '09953539866',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dewey Nagtalon',
       'koleya Rigel Nagtalon',
       '',
       '09177005748',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lucy Roa',
       'koykoy Roa',
       '',
       '09069533654',
       '',
       '0',
       ''
UNION ALL
SELECT 'Boomer Ponce',
       'Krissa Ponce',
       '',
       '09974478231',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo Benosa',
       'Kristian Benosa',
       '',
       '092600668729',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOOEY Lagumbay',
       'Kristine Delle Lagumbay',
       '',
       '09176221293/09260881659',
       '',
       '0',
       ''
UNION ALL
SELECT 'Motoy Eblacas',
       'Kristine Eblacas',
       '',
       '09171179481',
       '',
       '0',
       ''
UNION ALL
SELECT 'KhufRa Intol',
       'Kristine Faith Intol',
       '',
       '09974828188',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hoshi Osin',
       'Kristine Osin',
       '',
       '09277774159',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sasa Ablanque',
       'Kryssa Ablanque',
       '',
       '09058992322',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ben Navarro',
       'Kunan Navarro',
       '',
       '09212322143',
       '',
       '0',
       ''
UNION ALL
SELECT 'Casper Navarro',
       'Kunan Navarro',
       '',
       '09212322143',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alyanna Candilas',
       'Kurt Candilas',
       '',
       '09261145351',
       '',
       '0',
       ''
UNION ALL
SELECT 'MAX Esteves',
       'Kurt Esteves',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hunter Ugmad',
       'Kurt Joel Ugmad',
       '',
       '09190748319',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kora Sevilla',
       'Kyle Joshua Sevilla',
       '',
       '09050380527',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ellie Ybanez',
       'Kyrie Ybanez',
       '',
       '09771051341',
       '',
       '0',
       ''
UNION ALL
SELECT 'Booh Dajao',
       'Lady Ann Dajao',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'ROCHII Lagrama',
       'Lagrama, Jean Marie',
       '',
       '09283531948',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shawie lagrosas',
       'LAGROSAS',
       '',
       '09167619776',
       '',
       '0',
       ''
UNION ALL
SELECT 'Goldie Kitten',
       'Lai Abdulmalic',
       '',
       '09451355149',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lily Pablo',
       'Lance Pablo',
       '',
       '09155443790',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanuts Ebcas',
       'Lara Ebcas',
       '',
       '09177152731',
       '',
       '0',
       ''
UNION ALL
SELECT 'Potpot Ledesma',
       'Lara Ledesma',
       '',
       '09061478176',
       '',
       '0',
       ''
UNION ALL
SELECT 'HARLEY Baclig',
       'Larry Baclig',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hachiko Ritardo',
       'Lawrence Ritardo',
       '',
       '09177073623',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maximus Ritardo',
       'Lawrence Ritardo',
       '',
       '09177073623',
       '',
       '0',
       ''
UNION ALL
SELECT 'Holiday Jala',
       'Lea Jala',
       '',
       '09752056602',
       '',
       '0',
       ''
UNION ALL
SELECT 'COCO Tecson',
       'Lea Tecson',
       '',
       '09152083589',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bella Rodil',
       'Leah Rose Rodil',
       '',
       '09397282310',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo Tudio',
       'LeahAbigail Tudio',
       '',
       '09650556134',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toffee Tudio',
       'LeahAbigail Tudio',
       '',
       '09650556134',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Barriga',
       'Leanne Marie Barriga',
       '',
       '09171758080',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Nuevo',
       'Leika Mea Nuevo',
       '',
       '09358233546',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pakx Nuevo',
       'Leika Mea Nuevo',
       '',
       '09358233546',
       '',
       '0',
       ''
UNION ALL
SELECT 'Trek Nuevo',
       'Leika Mea Nuevo',
       '',
       '09358233546',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOCHA Asilan',
       'Lemon Asilan',
       '',
       '09532612904',
       '',
       '0',
       ''
UNION ALL
SELECT 'PEanut Asilan',
       'Lemon Asilan',
       '',
       '09532612904',
       '',
       '0',
       ''
UNION ALL
SELECT 'LOKI Bautizado',
       'Lemuel Bautizado',
       '',
       '09275939565',
       '',
       '0',
       ''
UNION ALL
SELECT 'SUMMER ODCHIGUE',
       'LENITA ODCHIGUE',
       '',
       '09670273267',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pomy Garcia',
       'Leonardo Garcia',
       '',
       '09177997943',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dario Aguillon',
       'Leonilo Aguillon',
       '',
       '09352483817',
       '',
       '0',
       ''
UNION ALL
SELECT 'Igor Moya',
       'Leslie Moya',
       '',
       '09959774443',
       '',
       '0',
       ''
UNION ALL
SELECT 'Petra Moya',
       'Leslie Moya',
       '',
       '09959774443',
       '',
       '0',
       ''
UNION ALL
SELECT 'TOMATO Moya',
       'Leslie Moya',
       '',
       '09959774443',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo Reyes',
       'Leslie Reyes',
       '',
       '09164083508',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yvonne Reyes',
       'Leslie Reyes',
       '',
       '09164083508',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bubbles Valderama',
       'Lester Valderama',
       '',
       '09652601372',
       '',
       '0',
       ''
UNION ALL
SELECT 'Swaggy Vergara',
       'Lhndy Vergara',
       '',
       '09177133641',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simon Mora',
       'Liana Mora',
       '',
       '09957514073',
       '',
       '0',
       ''
UNION ALL
SELECT 'Smokey Mora',
       'Liana Mora',
       '',
       '09957514073',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chun2x Campanilla',
       'Liezyl Campanilla',
       '',
       '09061674755',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pluto Caresma',
       'Lilibeth Caresma',
       '',
       '09178226211',
       '',
       '0',
       ''
UNION ALL
SELECT 'Raz Paredes',
       'Lilibeth Paredes',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Appa Tigulo',
       'Lilibeth Tigulo',
       '',
       '09952230686',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mocha Serognas',
       'Lily May Serognas',
       '',
       '09176331716',
       '',
       '0',
       ''
UNION ALL
SELECT 'Aw-aw Rabal',
       'Lina Rabal',
       '',
       '09057422933',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sam and Max Rabal',
       'Lina Rabal',
       '',
       '09057422933',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pepper Canguit',
       'Linart Canguit',
       '',
       '09664682024/09062932217',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spooky Galicia',
       'Liramyl Galicia',
       '',
       '09665837277',
       '',
       '0',
       ''
UNION ALL
SELECT 'Xaxi Galicia',
       'Liramyl Galicia',
       '',
       '09665837277',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alpine Roa',
       'Lisa Roa',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Roa',
       'Lisa Roa',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Roa',
       'Lisa Roa',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Siao',
       'Lita Siao',
       '',
       '09358356568',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pshyco Galinada',
       'Lito Galidana',
       '',
       '09655021849',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZEKE Olayon',
       'Lito Olayon',
       '',
       '09351100890',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZEKE Olayon',
       'Lito Olayon',
       '',
       '09351100890',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wacke2 Mercado',
       'Liz Mercado',
       '',
       '09172483846',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wakawaka Mercado',
       'Liz Mercado',
       '',
       '09172483846',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wocko2 Mercado',
       'Liz Mercado',
       '',
       '09172483846',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mercy Chan',
       'Liza Chan',
       '',
       '09176844111',
       '',
       '0',
       ''
UNION ALL
SELECT 'Khylie Malazarte',
       'Liza Lorena Malazarte',
       '',
       '09364897598',
       '',
       '0',
       ''
UNION ALL
SELECT '8 PUPPIES Villamor',
       'Liza May Vilamor',
       '',
       '09178825839',
       '',
       '0',
       ''
UNION ALL
SELECT 'Roxy Ozaraga',
       'Liza Ozaraga',
       '',
       '09178825839',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Bahan',
       'lloyd Bahan',
       '',
       '09059523547',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bailey Bass',
       'Lloyd Bass',
       '',
       '09276300757',
       '',
       '0',
       ''
UNION ALL
SELECT 'DreamBoy Ib-ib',
       'Lloyd Ib-ib',
       '',
       '09477741564',
       '',
       '0',
       ''
UNION ALL
SELECT 'EREN Ib-ib',
       'Lloyd Ib-ib',
       '',
       '09477741564',
       '',
       '0',
       ''
UNION ALL
SELECT 'TeddyBear Ib-ib',
       'Lloyd Ib-ib',
       '',
       '09477741564',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZEKE Ib-ib',
       'Lloyd Ib-ib',
       '',
       '09477741564',
       '',
       '0',
       ''
UNION ALL
SELECT 'Seven Pahayac',
       'Lloyd Pahayac',
       '',
       '09951979511',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Rizada',
       'LLoyd Rizada',
       '',
       '09177077625',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cholo Rosabal',
       'Loigie Rosabal',
       '',
       '09565366424',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sparky Narvasa',
       'Loriefe Narvasa',
       '',
       '09069527535',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pulgoso Dela Vega',
       'Lorifel Dela Vega',
       '',
       '09966026151',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yummy Napone',
       'Lorraine Napone',
       '',
       '09057143272',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chuchu Jo',
       'Lou Brigette Jo',
       '',
       '09169354353',
       '',
       '0',
       ''
UNION ALL
SELECT 'Emiko Alvar',
       'Louie Alvar',
       '',
       '09179844778',
       '',
       '0',
       ''
UNION ALL
SELECT 'Loco Halapit',
       'Louie Halapit',
       '',
       '09176656118',
       '',
       '0',
       ''
UNION ALL
SELECT 'KRYPTO Amatong',
       'LOURAINE JUNE AMATONG',
       '',
       '09959649391',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kendra Mapano',
       'Love Mapano',
       '',
       '09354554865',
       '',
       '0',
       ''
UNION ALL
SELECT 'SCOT Mapano',
       'Love Mapano',
       '',
       '09354554865',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Paayas',
       'Love Paayas',
       '',
       '09364822454',
       '',
       '0',
       ''
UNION ALL
SELECT 'Guinea Pig Cabigon',
       'Lovely Cabigon',
       '',
       'no contact num',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cobby Ladera',
       'Lucille Ladera',
       '',
       '09658836052',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cuan Lim',
       'lucio Lim',
       '',
       '09056666788',
       '',
       '0',
       ''
UNION ALL
SELECT 'WOOHAN Haloot',
       'Luisa Haloot',
       '',
       '09167500158',
       '',
       '0',
       ''
UNION ALL
SELECT 'Penny Pakino',
       'Luke Pakino',
       '',
       '09158044357',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloud Dagum',
       'Luz Marie Dagum',
       '',
       '09208320598',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bailey Bahan',
       'Lynnel Lloyd Bahan',
       '',
       '09059523547',
       '',
       '0',
       ''
UNION ALL
SELECT 'PANDA Dador',
       'Lyzette Diane Marie Dador',
       '',
       '09053661129 /8515160',
       '',
       '0',
       ''
UNION ALL
SELECT 'AnastasiaRose Quintos',
       'M.G Quintos',
       '',
       '09177055411',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marley Sanchez',
       'Ma. Carmela Sanchez',
       '',
       '09977929093',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Bonita',
       'Ma. Cristina Bonita',
       '',
       '09450780976',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pia Sapon',
       'Ma. Fe Evangeline Sapon',
       '',
       '09161519763',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kobe Guitarte',
       'Ma. Jeneza Guitarte',
       '',
       '09051406694',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shipoo Guitarte',
       'Ma. Jeneza Guitarte',
       '',
       '09051406694',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jackie Bolanio',
       'Ma. Lourdes Bolanio',
       '',
       '09178691632',
       '',
       '0',
       ''
UNION ALL
SELECT 'YAMI Salinasal',
       'Ma. Lourdes Salinasal',
       '',
       '09166197966',
       '',
       '0',
       ''
UNION ALL
SELECT 'In-in Lugpatan',
       'Ma. Maybelle Lugpatan',
       '',
       '09777205638',
       '',
       '1',
       ''
UNION ALL
SELECT 'Dos Ong',
       'Ma. Pops Ong',
       '',
       '09758445831',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Ong',
       'Ma. Pops Ong',
       '',
       '09758445831',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tres Ong',
       'Ma. Pops Ong',
       '',
       '09758445831',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Pom Amao',
       'Ma. Teresita Amao',
       '',
       '09171637636',
       '',
       '0',
       ''
UNION ALL
SELECT 'ISAY Dolino',
       'Mackoi Dolino',
       '',
       '09175823344',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maia Dolino',
       'Mackoi Dolino',
       '',
       '09175823344',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rasta Dolino',
       'Mackoi Dolino',
       '',
       '09175823344',
       '',
       '0',
       ''
UNION ALL
SELECT 'Skylar Tagaro',
       'Mae Ann Tagaro',
       '',
       '09659767831',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiki Garcia',
       'Mae Garcia',
       '',
       '09176242517',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zito Garcia',
       'Mae Garcia',
       '',
       '09176242517',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOKIE Macdan',
       'Maelyn Macdan',
       '',
       '09272764814',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kalai Robante',
       'Magdalena Robante',
       '',
       '09362776875',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lendee  Robante',
       'Magdalena Robante',
       '',
       '09362776875',
       '',
       '0',
       ''
UNION ALL
SELECT 'Micro Robante',
       'Magdalena Robante',
       '',
       '09362776875',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lagertha Maghuyop',
       'Maghuyop',
       '',
       '09667041268',
       '',
       '0',
       ''
UNION ALL
SELECT 'Siggy Maghuyop',
       'Maghuyop',
       '',
       '09667041268',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chooe Lim',
       'Mags Lim',
       '',
       '09177957191',
       '',
       '0',
       ''
UNION ALL
SELECT 'Booni Macababat',
       'Maida Macababat',
       '',
       '09209061887',
       '',
       '0',
       ''
UNION ALL
SELECT 'SHUPEE Daclag',
       'Maila A. Daclag',
       '',
       '09151277575',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba Farhana',
       'Mapandi Farhana',
       '',
       '09177054898',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miyuki Bangcong',
       'Marc Vincent Bangcong',
       '',
       '09363224482',
       '',
       '0',
       ''
UNION ALL
SELECT 'NO NAME CAT Amodia',
       'Marco Amodia',
       '',
       '09999900462',
       '',
       '0',
       ''
UNION ALL
SELECT 'daRK n light Plamus',
       'Marco Plamus',
       '',
       '09178280117',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chico Cotejar',
       'Mari Arch Cotejar',
       '',
       '09453113892',
       '',
       '0',
       ''
UNION ALL
SELECT 'Minho Cuenca',
       'Mari Cris Cuenca',
       '',
       '09368802482',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kylie Amigable',
       'Maria Amigable',
       '',
       '09533852889',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gordon Ilogon',
       'Maria Ilogon',
       '',
       '09171452909',
       '',
       '0',
       ''
UNION ALL
SELECT 'NANA Ilogon',
       'Maria Ilogon',
       '',
       '09171452909',
       '',
       '0',
       ''
UNION ALL
SELECT 'NEJI Ilogon',
       'Maria Ilogon',
       '',
       '09171452909',
       '',
       '0',
       ''
UNION ALL
SELECT 'WAN-WAN Ilogon',
       'Maria Ilogon',
       '',
       '09171452909',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUKA Del Mar',
       'Maria Lourdes Del Mar',
       '',
       '09684268861',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panda Maandig',
       'Maria Maandig',
       '',
       '09173110919',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiger Maandig',
       'Maria Maandig',
       '',
       '09173110919',
       '',
       '0',
       ''
UNION ALL
SELECT 'Arya Pangalian',
       'Mariam Nifah Pangalian',
       '',
       '09053713943/ 09774010901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Baby Girl Pangalian',
       'Mariam Nifah Pangalian',
       '',
       '09053713943/ 09774010901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Batchoy Ponferrada',
       'Marianne Ponferrada',
       '',
       '09151699843',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ivana Lucanas',
       'Maricel Lucanas',
       '',
       '09060623734',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maxxi Yongco',
       'Maridel Yongco',
       '',
       '09656608646',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pillow Feliciano',
       'Maridell Feliciano',
       '',
       '09175628146',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alex Pasion',
       'Marie Cris Pasion',
       '',
       '09177122080',
       '',
       '0',
       ''
UNION ALL
SELECT 'Berta Soria',
       'Marie Cris Soria',
       '',
       '09757138963',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peachie Seno',
       'Marie Joe Seno',
       '',
       '09363034487',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zachie Seno',
       'Marie Joe Seno',
       '',
       '09363034487',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toytoy Gaid',
       'Mariefred Gaid',
       '',
       '09177004583',
       '',
       '0',
       ''
UNION ALL
SELECT 'WoobahChan Ang-og',
       'Mariel Ang-og',
       '',
       '09555648166',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hansel Gabales',
       'Mariel Gabales',
       '',
       '09279053751',
       '',
       '0',
       ''
UNION ALL
SELECT 'DEBBIE Jugador',
       'Mariel Jugador',
       '',
       '09176586416',
       '',
       '0',
       ''
UNION ALL
SELECT 'Boss Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Grey Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'kittens 5 Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lydia Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       '5-Sep-20'
UNION ALL
SELECT 'Maggie Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Moon Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       '5-Sep-20'
UNION ALL
SELECT 'StileS Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tricolor Montecillo',
       'Mariel Montecillo',
       '',
       '09273347901',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bella Deloso',
       'Marife Deloso',
       '',
       '09971447403',
       '',
       '0',
       ''
UNION ALL
SELECT 'CurryKent Jaramillo',
       'Marilyn Jaramillo',
       '',
       '09260663747',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jacob Damasing',
       'Mario Dmasing',
       '',
       '09155514134',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rosie Magto',
       'Marion Magto',
       '',
       '09397830008',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jet Bragat',
       'Maripet Bragat',
       '',
       '09235824511/09772925216',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mani Bragat',
       'Maripet Bragat',
       '',
       '09235824511/09772925216',
       '',
       '0',
       ''
UNION ALL
SELECT '',
       'Mariz RAMOS',
       '',
       '',
       '',
       '',
       ''
UNION ALL
SELECT 'JACOB Ramos',
       'Mariz RAMOS',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bigming Silagan',
       'Mariza Silagan',
       '',
       '09153897126',
       '',
       '0',
       ''
UNION ALL
SELECT 'Friday Silagan',
       'Mariza Silagan',
       '',
       '09153897126',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiwi Silagan',
       'Mariza Silagan',
       '',
       '09153897126',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bubok Ohwada',
       'Marjorie Ohwada',
       '',
       'ask doc mhai',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mimi Ohwada',
       'Marjorie Ohwada',
       '',
       'ask doc mhai',
       '',
       '0',
       ''
UNION ALL
SELECT 'KURO Aceso',
       'Mark Aceso',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Micai Dolino',
       'Mark Dolino',
       '',
       '09125823344',
       '',
       '0',
       ''
UNION ALL
SELECT 'GRU PACATANG',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'JACK AND RUSSELL Pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Li Pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'puppies 5 pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scout Pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'WaffleS Pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZAK PACATANG',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zaya Pacatang',
       'Mark Pacatang',
       '',
       '09177113333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hash Montera',
       'Marlon Montera',
       '',
       '09177079266',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tag Montera',
       'Marlon Montera',
       '',
       '09177079266',
       '',
       '0',
       ''
UNION ALL
SELECT 'Serbo Alerre',
       'Marlyn Allere',
       '',
       '09268956208',
       '',
       '0',
       ''
UNION ALL
SELECT 'Boodi Calingasan',
       'Marriane Calingasan',
       '',
       '09171044134',
       '',
       '0',
       ''
UNION ALL
SELECT 'Channel Calingasan',
       'Marriane Calingasan',
       '',
       '09171044134',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choobi Calingasan',
       'Marriane Calingasan',
       '',
       '09171044134',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUCE Paragili',
       'Marrylyn Paragili',
       '',
       '09773906564',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cassie Paragili',
       'Marrylyn Paragili',
       '',
       '09773906564',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lester Paragili',
       'Marrylyn Paragili',
       '',
       '09773906564',
       '',
       '0',
       ''
UNION ALL
SELECT 'Moshie Paragili',
       'Marrylyn Paragili',
       '',
       '09773906564',
       '',
       '0',
       ''
UNION ALL
SELECT 'shasha Paragili',
       'Marrylyn Paragili',
       '',
       '09773906564',
       '',
       '0',
       ''
UNION ALL
SELECT 'cobie nalang',
       'martha nalang',
       '',
       '09155952247',
       '',
       '0',
       ''
UNION ALL
SELECT 'ChiChi Dollaga',
       'Martin Matheww Dollaga',
       '',
       '09065725456',
       '',
       '0',
       ''
UNION ALL
SELECT 'Uno Canoy',
       'Mary Ann Canoy',
       '',
       '09177942319',
       '',
       '0',
       ''
UNION ALL
SELECT 'Morry Batutay',
       'Mary Batutay',
       '',
       '09260922486',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiptip Lumbay',
       'Mary Gee Lumbay',
       '',
       '09171547377',
       '',
       '0',
       ''
UNION ALL
SELECT 'EVEY Padillo',
       'Mary Glou Padillo',
       '',
       '09173931245',
       '',
       '0',
       ''
UNION ALL
SELECT 'GIMLI Padillo',
       'Mary Glou Padillo',
       '',
       '09173931245',
       '',
       '0',
       ''
UNION ALL
SELECT 'KAFKA Padillo',
       'Mary Glou Padillo',
       '',
       '09173931245',
       '',
       '0',
       ''
UNION ALL
SELECT 'KIRIGAN/EIVOR Padillo',
       'Mary Glou Padillo',
       '',
       '09173931245',
       '',
       '0',
       ''
UNION ALL
SELECT 'SEGUNDA Padillo',
       'Mary Glou Padillo',
       '',
       '09173931245',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOKIE Lucagbo',
       'Mary Grace Lucagbo',
       '',
       '09276518372',
       '',
       '0',
       ''
UNION ALL
SELECT 'COOPER Odal',
       'Mary Grace Odal',
       '',
       '09057824342',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUNA Odal',
       'Mary Grace Odal',
       '',
       '09057824342',
       '',
       '0',
       ''
UNION ALL
SELECT 'IVANA Ramiro',
       'Mary Grace RAMIRO',
       '',
       '09051142999',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yumi Edon',
       'Mary Grethel Edon',
       '',
       '09057919595',
       '',
       '0',
       ''
UNION ALL
SELECT 'potpot Coruna',
       'Mary Jan Coruna',
       '',
       '09261144578',
       '',
       '0',
       ''
UNION ALL
SELECT 'EVA Cagas',
       'Mary Jane Cagas',
       '',
       '09156372500',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chewie Eduria',
       'Mary Jane Eduria',
       '',
       '09073274836',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maku Madula',
       'Mary Jane Madula',
       '',
       '09173112317',
       '',
       '0',
       ''
UNION ALL
SELECT 'DUKE Jose',
       'MAry Kaye Jose',
       '',
       '09661727736',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paulken Micobalo',
       'Maurecio Micobalo',
       '',
       '09161513409',
       '',
       '0',
       ''
UNION ALL
SELECT 'Paulken Micobalo',
       'Maurecio Micobalo',
       '',
       '09161513409',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Vallejos',
       'Maureen Vallejos',
       '',
       '09755264292',
       '',
       '0',
       ''
UNION ALL
SELECT 'RIO Momo',
       'Mav Jean Momo',
       '',
       '09350027621',
       '',
       '0',
       ''
UNION ALL
SELECT 'Saby Atay',
       'May Ann Atay',
       '',
       '09177122942',
       '',
       '0',
       ''
UNION ALL
SELECT 'Anna Quijano',
       'Mayette Quijano',
       '',
       '09357727778',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scarlet Auley',
       'Mc Auley',
       '',
       '09262947588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hugo Dichosa',
       'Mechelle, Dichosa',
       '',
       '09176538750',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chief Gio',
       'Melchie Gio',
       '',
       '09262016120',
       '',
       '0',
       ''
UNION ALL
SELECT 'MInggay Gio',
       'Melchie Gio',
       '',
       '09262016120',
       '',
       '0',
       ''
UNION ALL
SELECT 'STARK ALVAREZ',
       'MELEAN ALVAREZ',
       '',
       '09362688579',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blue Salering',
       'Melou Jane Salering',
       '',
       '09756354522',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Salering',
       'Melou Jane Salering',
       '',
       '09756354522',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peachy Salering',
       'Melou Jane Salering',
       '',
       '09756354522',
       '',
       '0',
       ''
UNION ALL
SELECT 'Portia Salering',
       'Melou Jane Salering',
       '',
       '09756354522',
       '',
       '0',
       ''
UNION ALL
SELECT 'Itiroki Canas',
       'Melvar Canas',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Teddy Mangila',
       'Melvie John Mangila',
       '',
       '09177721761',
       '',
       '0',
       ''
UNION ALL
SELECT 'MONKING Chua',
       'Menchu Chua',
       '',
       '09177721477',
       '',
       '0',
       ''
UNION ALL
SELECT 'SATCHIE Chua',
       'Menchu Chua',
       '',
       '09177721477',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sophia Solangon',
       'Mercel Solangon',
       '',
       '09752340659',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gigirl Febra',
       'Merlita Febra',
       '',
       '09771829523',
       '',
       '0',
       ''
UNION ALL
SELECT 'Candy Ocladina',
       'Merly Ocladina',
       '',
       '09277058394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Donut Ocladina',
       'Merly Ocladina',
       '',
       '09277058394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sushi Ocladina',
       'Merly Ocladina',
       '',
       '09277058394',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blanca Sumabat',
       'Merry Cris Sumabat',
       '',
       '09068672081',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bonnie Asio',
       'Merry Grace Asio',
       '',
       '09283217406',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bonnie Asio',
       'Merry Grace Asio',
       '',
       '09283217406',
       '',
       '0',
       ''
UNION ALL
SELECT 'SERAH Abueva',
       'Mia Abueva',
       '',
       '09532767572',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fudge Caina',
       'Mia Caina',
       '',
       '09666604397',
       '',
       '0',
       ''
UNION ALL
SELECT 'BONJING  CORTES',
       'MIA CORTES',
       '',
       '0565345588',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHATTY Abueva',
       'Mia Jade Abueva',
       '',
       '09538767572',
       '',
       '0',
       ''
UNION ALL
SELECT 'BONJING Lodoroz',
       'Mia Lorodoz',
       '',
       '09565345588',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Salvador',
       'Michaella Salvador',
       '',
       '09161513699',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rory Cabilogan',
       'Micheal Cabilogan',
       '',
       '09265955530',
       '',
       '0',
       ''
UNION ALL
SELECT 'Anya Chaves',
       'Micheal Chaves',
       '',
       '09178207310',
       '',
       '0',
       ''
UNION ALL
SELECT 'ArTo Chaves',
       'Micheal Chaves',
       '',
       '09178207310',
       '',
       '0',
       ''
UNION ALL
SELECT 'Azi Chaves',
       'Micheal Chaves',
       '',
       '09178207310',
       '',
       '0',
       ''
UNION ALL
SELECT 'Popol Figura',
       'Micheal Figura',
       '',
       '0997064741',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mike Obregon',
       'Micheal John Obregon',
       '',
       '09551133282',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Bas',
       'Michell Bas',
       '',
       '09562948849',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mona Acenas',
       'Michelle Dawn Acenas',
       '',
       '09268692415',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kaylee Gaabucayan',
       'Michelle Gaabucayan',
       '',
       '09264866229',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fisk Lua',
       'Michelle Lua',
       '',
       '09453813322',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oda Pabillaran',
       'Michelle Pabillaran',
       '',
       '09651685910',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Pabillaran',
       'Michelle Pabillaran',
       '',
       '09651685910',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maui Pelaez',
       'Michelle Pelaez',
       '',
       '09669342148',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pippa Tp Pelaez',
       'Michelle Pelaez',
       '',
       '09669342148',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lia Tan',
       'Michelle Tan',
       '',
       '09178753358',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yorokobi Villasan',
       'MIchelle Villasan',
       '',
       '09751050705',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cloudy Seno',
       'Micole Seno',
       '',
       '09975197541',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scar Montecillo',
       'Miguel Montecillo',
       '',
       '09171366782',
       '',
       '0',
       ''
UNION ALL
SELECT 'Selina Gorra',
       'Mik Gorra',
       '',
       '09266300524',
       '',
       '0',
       ''
UNION ALL
SELECT 'Leng Beatisula',
       'Mikee Beatisula',
       '',
       '09369349227',
       '',
       '0',
       ''
UNION ALL
SELECT 'Draco Villamor',
       'Mikee Villamor',
       '',
       '09569016062',
       '',
       '0',
       ''
UNION ALL
SELECT 'KATHDINE Villamor',
       'Mikee Villamor',
       '',
       '09569016062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lala Villamor',
       'Mikee Villamor',
       '',
       '09569016062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lc Villamor',
       'Mikee Villamor',
       '',
       '09569016062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scarlet Villamor',
       'Mikee Villamor',
       '',
       '09569016062',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Vallejos',
       'Mikha Vallejos',
       '',
       '09173034890',
       '',
       '0',
       ''
UNION ALL
SELECT 'Roo Ramos',
       'Mikhaela Ramos',
       '',
       '09178818745',
       '',
       '0',
       ''
UNION ALL
SELECT 'MATCHA Ormillada',
       'Mikko Ormillada',
       '',
       '09368414117',
       '',
       '0',
       ''
UNION ALL
SELECT 'Amanda Hojas',
       'Mildred Hojas',
       '',
       '09665033936',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tucker Hojas',
       'Mildred Hojas',
       '',
       '09665033936',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wifi Macamay',
       'Mimerua Macamay',
       '',
       '09663819106',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUDDY Artajo',
       'Mimi  Artajo',
       '',
       '09754947354',
       '',
       '0',
       ''
UNION ALL
SELECT 'Macy Artajo',
       'Mimi  Artajo',
       '',
       '09754947354',
       '',
       '0',
       ''
UNION ALL
SELECT 'Berlin Rufin',
       'Minette Rufin',
       '',
       '09107913838',
       '',
       '0',
       ''
UNION ALL
SELECT '3 Puppies Celerinos',
       'Mitch Celerinos',
       '',
       '09171139973',
       '',
       '0',
       ''
UNION ALL
SELECT 'IC Cultura',
       'MJ Cultura',
       '',
       '09176302887',
       '',
       '0',
       ''
UNION ALL
SELECT 'CoCo Gala',
       'Mj Gala',
       '',
       '09171120727',
       '',
       '0',
       ''
UNION ALL
SELECT 'china momongan',
       'Momongan Miguel',
       '',
       '09551668892',
       '',
       '0',
       ''
UNION ALL
SELECT 'covid momongan',
       'Momongan Miguel',
       '',
       '09551668892',
       '',
       '0',
       ''
UNION ALL
SELECT 'ITALY MOMONGAN',
       'Momongan Miguel',
       '',
       '09551668892',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tokyo Momongan',
       'Momongan Miguel',
       '',
       '09551668892',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miggy Jalalon',
       'Mona Jalalon',
       '',
       '09559146085',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chowchow Agsalog',
       'Monaliza Agsalog',
       '',
       '09169652260',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cubee Agsalog',
       'Monaliza Agsalog',
       '',
       '09169652260',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chase Galeon',
       'Monica Galleon',
       '',
       '09177775805',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jacky Galleon',
       'Monica Galleon',
       '',
       '09177775805',
       '',
       '0',
       ''
UNION ALL
SELECT 'Murphy Garcia',
       'Monica Garcia',
       '',
       '09153923511',
       '',
       '0',
       ''
UNION ALL
SELECT 'Xander Israel',
       'Monica Israel',
       '',
       '09177064658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kean Neri',
       'Monique Neri',
       '',
       '09176665709',
       '',
       '0',
       ''
UNION ALL
SELECT 'Smokey Vasquez',
       'Monique Vasquez',
       '',
       '09563472401',
       '',
       '0',
       ''
UNION ALL
SELECT 'coco jameno',
       'Moses Jameno',
       '',
       '09177071490',
       '',
       '0',
       ''
UNION ALL
SELECT 'Beauty Padilla',
       'Myleen Padilla',
       '',
       '09983780214',
       '',
       '0',
       ''
UNION ALL
SELECT 'KOBE Garcia',
       'Myra Garcia',
       '',
       '09972660452',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Cabaron',
       'Myramy Cabaron',
       '',
       '09167351049',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Cabaron',
       'Myramy Cabaron',
       '',
       '09167351049',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pechacho Demecillo',
       'Myrna Demencillo',
       '',
       '09177028283',
       '',
       '0',
       ''
UNION ALL
SELECT 'Penny Demencillo',
       'Myrna Demencillo',
       '',
       '09177028283',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cooper Te',
       'Nadia Te',
       '',
       '09176334343',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mojo Te',
       'Nadia Te',
       '',
       '09176334343',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lilo Perpetua',
       'Nal Nober Perpetua',
       '',
       '09454576752',
       '',
       '0',
       ''
UNION ALL
SELECT 'Andy Malinao',
       'Nancy Malinao',
       '',
       '09105776988',
       '',
       '0',
       ''
UNION ALL
SELECT 'HAILEY Balbon',
       'Natasha Mae Balbon',
       '',
       '09054388333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dax Mangubat',
       'Natasha Mangubat',
       '',
       '09976023975',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiko Sia',
       'Natasha Sia',
       '',
       '09565348051',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHICOO Busano',
       'Nathy Busano',
       '',
       '09550552498',
       '',
       '0',
       ''
UNION ALL
SELECT 'Happy Padilla',
       'Nelson Padilla',
       '',
       '09177921967',
       '',
       '0',
       ''
UNION ALL
SELECT 'ShanShan Padilla',
       'Nelson Padilla',
       '',
       '09177921967',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cooper Papina',
       'Nelson Papina',
       '',
       '09177921967',
       '',
       '0',
       ''
UNION ALL
SELECT 'bunny satur',
       'neneng satur',
       '',
       '0935480855',
       '',
       '0',
       ''
UNION ALL
SELECT 'Snow Galendez',
       'Nepretenre Jane Galendez',
       '',
       '09551540843',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiwi Cortes',
       'Nes Cortes',
       '',
       '09350053955',
       '',
       '0',
       ''
UNION ALL
SELECT 'Astra Tidoy',
       'Nestor Tidoy',
       '',
       '09278463393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blizzard Tidoy',
       'Nestor Tidoy',
       '',
       '09278463393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Storm Tidoy',
       'Nestor Tidoy',
       '',
       '09278463393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiny Tidoy',
       'Nestor Tidoy',
       '',
       '09278463393',
       '',
       '0',
       ''
UNION ALL
SELECT 'Koji Bolijuan',
       'Neszel Rose Bolijuan',
       '',
       '09976082829',
       '',
       '0',
       ''
UNION ALL
SELECT 'Prence Calisris',
       'Nicasia Calisris',
       '',
       '09067950357',
       '',
       '0',
       ''
UNION ALL
SELECT 'Choco Aligno',
       'Nicole Aligno',
       '',
       '09059486561',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOlly Arranguez',
       'Nicole Arranguez',
       '',
       '09363552780',
       '',
       '0',
       ''
UNION ALL
SELECT 'SHOVY Arranguez',
       'Nicole Arranguez',
       '',
       '09363552780',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gothchi Madjos',
       'Nicole Madjos',
       '',
       '09758406166',
       '',
       '0',
       ''
UNION ALL
SELECT 'TINY Balansag',
       'Nino Balansag',
       '',
       '090610154929',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milo Ong',
       'Noe Ong Jr',
       '',
       '09177038874',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zeus DeCastro',
       'Noel DeCastro',
       '',
       '09177065666',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tiger Jagualing',
       'Noel Jagualing',
       '',
       '09656501583',
       '',
       '0',
       ''
UNION ALL
SELECT 'STAR Naybe',
       'Noel Naybe',
       '',
       '09051264544',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mako Cabanaz',
       'Nola Cabanez',
       '',
       '0997742349',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cholo Lupiba',
       'Noloseo Lupiba',
       '',
       '09355175785',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nero Mabida',
       'Nonia Mabida',
       '',
       '0926486229',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kiara Salen',
       'Norma Salen',
       '',
       '09228934777',
       '',
       '0',
       ''
UNION ALL
SELECT 'GARRY mamacotao',
       'Norsheda Mamacotao',
       '',
       '09177057836',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ragmac/ULIK Mamacotao',
       'Norsheda Mamacotao',
       '',
       '09177057836',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bab Nino',
       'Odessa Nino',
       '',
       '09175408055',
       '',
       '0',
       ''
UNION ALL
SELECT 'Black Cat Alcantara',
       'Oliver Alcantara',
       '',
       '09055665055',
       '',
       '0',
       ''
UNION ALL
SELECT 'JOONGKOOK Villegas',
       'Omar Villegas',
       '',
       '09298770009',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kaikai Llonod',
       'Oro Llomod',
       '',
       '09065066327',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kisses Reyes',
       'Orvin Reyes',
       '',
       '09265680535',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pompey Aguda',
       'Oscar Aguda',
       '',
       '09171461378',
       '',
       '0',
       ''
UNION ALL
SELECT 'Aquipos Merin',
       'Paola Merin',
       '',
       '09178791161',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milan Hassanodin',
       'Papalaingko Hassanodin',
       '',
       '09268693425',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bandit Go',
       'Pat John Go',
       '',
       '09261672551',
       '',
       '0',
       ''
UNION ALL
SELECT 'CoyCoy Rebolos',
       'Patricia Rebolos',
       '',
       '09451348652',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kimmy Rebolos',
       'Patricia Rebolos',
       '',
       '09451348652',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lambo Rebolos',
       'Patricia Rebolos',
       '',
       '09451348652',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fluffy Tumarong',
       'Patricia Tumarong',
       '',
       '09069176346',
       '',
       '0',
       ''
UNION ALL
SELECT 'YATCHIE ROSAS',
       'Patrick Rosas',
       '',
       '09356357623',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Redillus',
       'Paul Andre Redillus',
       '',
       '09177151573',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHAVA Dela Cruz',
       'Paul Rizzi Dela Cruz',
       '',
       '09619328949',
       '',
       '0',
       ''
UNION ALL
SELECT 'Luna Gonzales',
       'Pearl Gonzales',
       '',
       '09564711096',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chambe Mendoza',
       'Pearl Mendoza',
       '',
       '09083236999',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chowhei Mendoza',
       'Pearl Mendoza',
       '',
       '09083236999',
       '',
       '0',
       ''
UNION ALL
SELECT 'Praystine Peligro',
       'Peligro Family',
       '',
       '09551661291',
       '',
       '0',
       ''
UNION ALL
SELECT 'Asis Pelisco',
       'Percy Pelisco',
       '',
       '09265907601',
       '',
       '0',
       ''
UNION ALL
SELECT 'Latte Pelisco',
       'Percy Pelisco',
       '',
       '09265907601',
       '',
       '0',
       ''
UNION ALL
SELECT 'MAKISIG Peliscio',
       'Percy Pelisco',
       '',
       '09265907601',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marix Pelisco',
       'Percy Pelisco',
       '',
       '09265907601',
       '',
       '0',
       ''
UNION ALL
SELECT 'Breanne Sablada',
       'Percy Sablada',
       '',
       '09164772445/09358236700',
       '',
       '0',
       ''
UNION ALL
SELECT 'Eyecee Castanares',
       'Peter Castanares',
       '',
       '09188011515',
       '',
       '0',
       ''
UNION ALL
SELECT 'Athena Glore',
       'Peter Glore',
       '',
       '09065059702',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gucci Glore',
       'Peter Glore',
       '',
       '09065059702',
       '',
       '0',
       ''
UNION ALL
SELECT 'PACO Mendoza',
       'Peter Jean Mendoza',
       '',
       '09772543855',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sammy Navarro',
       'Phoebe Navarro',
       '',
       '09359156813',
       '',
       '0',
       ''
UNION ALL
SELECT 'BB Jabines',
       'Pie Jabines',
       '',
       '09177108255',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pzyrck Melallos',
       'PIe Melallos',
       '',
       '09361626968',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maui Pragados',
       'Pragados',
       '',
       '09358560095',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuki Abubacar',
       'Prince Abubacar',
       '',
       '09338229772',
       '',
       '0',
       ''
UNION ALL
SELECT 'Attorney Dalidig',
       'Princess Dalidig',
       '',
       '09053777966',
       '',
       '0',
       ''
UNION ALL
SELECT 'MARIA Bisoy',
       'Princess Mary Bisoy',
       '',
       '09631388841',
       '',
       '0',
       ''
UNION ALL
SELECT 'Patty Bongo',
       'Princess Missy Bongo',
       '',
       '09175704222',
       '',
       '0',
       ''
UNION ALL
SELECT 'Madison Saria',
       'Princess Saria',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'Missy Saria',
       'Princess Saria',
       '',
       'no',
       '',
       '0',
       ''
UNION ALL
SELECT 'JULIA UBAY-UBAY',
       'Princess Ubay-ubay',
       '',
       '09066030040',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hobi Moros',
       'Queenie Moros',
       '',
       '09350915328',
       '',
       '0',
       ''
UNION ALL
SELECT 'Midnight Hirang',
       'Rachelle Hirang',
       '',
       '09173031234',
       '',
       '0',
       ''
UNION ALL
SELECT 'Simba Hirang',
       'Rachelle Hirang',
       '',
       '09173031234',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bran Bonilla',
       'Rachelle Myan Bonilla',
       '',
       '09273021388',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bailey Mangawang',
       'Radshine Mangawang',
       '',
       '09667668723',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bently Mangawang',
       'Radshine Mangawang',
       '',
       '09667668723',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Getueza',
       'Rafael Getueza',
       '',
       '09952262496',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alexei Silagan',
       'Rafael Silagan',
       '',
       '09778444606',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pepper Silagan',
       'Rafael Silagan',
       '',
       '09778444606',
       '',
       '0',
       ''
UNION ALL
SELECT 'BACI Antonio',
       'Rahuel Antonio',
       '',
       '09177043773',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chenney Antonio',
       'Rahuel Antonio',
       '',
       '09177043773',
       '',
       '0',
       ''
UNION ALL
SELECT 'Khalos Antonio',
       'Rahuel Antonio',
       '',
       '09177043773',
       '',
       '0',
       ''
UNION ALL
SELECT 'Spark Antonio',
       'Rahuel Antonio',
       '',
       '09177043773',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHICO Ramirez',
       'Ralph Abegail Ramirez',
       '',
       '09551699122',
       '',
       '0',
       ''
UNION ALL
SELECT 'Skye Neri',
       'Ralph Anthony Neri',
       '',
       '09163619266',
       '',
       '0',
       ''
UNION ALL
SELECT 'Anni Guingao',
       'Ralph Guingao',
       '',
       '09973584500',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rox Guingao',
       'Ralph Guingao',
       '',
       '09973584500',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Rodriguez',
       'Ralph Rodriguez',
       '',
       '09654084543/09173286681',
       '',
       '0',
       ''
UNION ALL
SELECT 'Uno Kei',
       'Ramil Kei',
       '',
       '09068791498',
       '',
       '0',
       ''
UNION ALL
SELECT 'Prince Detalban',
       'Ramon Detalban',
       '',
       '09099609251',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wacowaco Betonio',
       'Randy Betonio',
       '',
       '09652607226',
       '',
       '0',
       ''
UNION ALL
SELECT 'CASSY Pelaez',
       'Rapunzelle Pelaez',
       '',
       '09276618069',
       '',
       '0',
       ''
UNION ALL
SELECT 'Charlotte Saab',
       'Raquel Saab',
       '',
       '09257008241',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lilie Flores',
       'Raul Flores',
       '',
       '09051407142',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Cocamas',
       'Ray Cocamas',
       '',
       '09369003940',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRIGITA Saavedra',
       'Ray Jan Saavedra',
       '',
       '09173829514/09569383235',
       '',
       '0',
       ''
UNION ALL
SELECT 'FOUR Saavedra',
       'Ray Jan Saavedra',
       '',
       '09173829514/09569383235',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jumong Saavedra',
       'Ray Jan Saavedra',
       '',
       '09173829514/09569383235',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nezuko Saavedra',
       'Ray Jan Saavedra',
       '',
       '09173829514/09569383235',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cersei Acosta',
       'Raychelle Acosta',
       '',
       '09178872797',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milky Casas',
       'Raymond Casas',
       '',
       '09977508725',
       '',
       '0',
       ''
UNION ALL
SELECT 'Milow Casas',
       'Raymond Casas',
       '',
       '09977508725',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ellie Llacuna',
       'Raymond Llacuna',
       '',
       '09954735431',
       '',
       '0',
       ''
UNION ALL
SELECT 'White Cat Belyo',
       'Raymund Belco',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'kittens 2 black Belco',
       'Raymund Belyo',
       '',
       '09178207350',
       '',
       '0',
       ''
UNION ALL
SELECT 'kittens 3 Belco',
       'Raymund Belyo',
       '',
       '09178207350',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Llacuna',
       'Raymund Llacuna',
       '',
       '09954735431',
       '',
       '0',
       ''
UNION ALL
SELECT 'Macky Paster',
       'Raymund Paster',
       '',
       '09173695855',
       '',
       '0',
       ''
UNION ALL
SELECT 'Meomeo Paster',
       'Raymund Paster',
       '',
       '09173695855',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sarge Zulita',
       'Raymund Zucita',
       '',
       '09274338449',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dottie Macabalang',
       'Rayyana Macabalang',
       '',
       '09275346509',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brady Delgado',
       'Rebecca Delgado',
       '',
       '09260361891',
       '',
       '0',
       ''
UNION ALL
SELECT 'MOcha Delgado',
       'Rebecca Delgado',
       '',
       '09260361891',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mojo Tabaranza',
       'Rebecca Tabaranza',
       '',
       '09178305417',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miya Rapliza',
       'Regina Rapliza',
       '',
       '09058925568',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miya Rapliza',
       'Regina Rapliza',
       '',
       '09058925568',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scout Rapliza',
       'Regina Rapliza',
       '',
       '09058925568',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shiro Rapliza',
       'Regina Rapliza',
       '',
       '09058925568',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bona Edmilao',
       'Remedios Bona',
       '',
       '09985510052',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toffee Villamor',
       'Remedios Villamor',
       '',
       '09177192968',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cairo Lopez',
       'Remmy Lopez',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Gucci Sumalpong',
       'Ren Sumalpong',
       '',
       '09669345404',
       '',
       '0',
       ''
UNION ALL
SELECT 'Digong Arabiana',
       'Renel Arabiana',
       '',
       '09679667579',
       '',
       '0',
       ''
UNION ALL
SELECT 'JIHOO Taganas',
       'Resmae Rose Taganas',
       '',
       '09159660432',
       '',
       '0',
       ''
UNION ALL
SELECT 'JISOO Taganas',
       'Resmae Rose Taganas',
       '',
       '09159660432',
       '',
       '0',
       ''
UNION ALL
SELECT 'JUNGKOOK Taganas',
       'Resmae Rose Taganas',
       '',
       '09159660432',
       '',
       '0',
       ''
UNION ALL
SELECT 'RAMRAM Agbalog',
       'Rey Agbalog',
       '',
       '09678715111',
       '',
       '0',
       ''
UNION ALL
SELECT 'Doree Bagares',
       'Rey Bagares',
       '',
       '09176788940',
       '',
       '0',
       ''
UNION ALL
SELECT 'DEXIE Calipayan',
       'Rey Calipayan',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bruce Lecaros',
       'Rey Lecaros',
       '',
       '09171940658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rocky Lecaros',
       'Rey Lecaros',
       '',
       '09171940658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tower Lecaros',
       'Rey Lecaros',
       '',
       '09171940658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chibi Balandra',
       'Rey Miguel Balandra',
       '',
       '09778535072',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mirana Balandra',
       'Rey Miguel Balandra',
       '',
       '09778535072',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nana Awit',
       'Reymart Awit',
       '',
       '09958745885',
       '',
       '0',
       ''
UNION ALL
SELECT 'TT Thyrion Thunder Dalogdog',
       'Reynie Dalogdog',
       '',
       'ask doc mhai',
       '',
       '0',
       ''
UNION ALL
SELECT 'BORA Kwong',
       'RG Clyde Kwong',
       '',
       '09050384629',
       '',
       '0',
       ''
UNION ALL
SELECT 'SOSANO Kwong',
       'RG Clyde Kwong',
       '',
       '09050384629',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy Banlos',
       'Rhandy Banlos',
       '',
       '09672237798',
       '',
       '0',
       ''
UNION ALL
SELECT 'Digoy Pelaez',
       'Rhea Pelaez',
       '',
       '09953538152',
       '',
       '0',
       ''
UNION ALL
SELECT 'Elmo Taniza',
       'Rhea Taniza',
       '',
       '09268217345',
       '',
       '0',
       ''
UNION ALL
SELECT 'minggay Waslo',
       'rhea waslo',
       '',
       '09065816808',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kitkat 2 Amron',
       'Rhidaak Amron',
       '',
       '09675703396',
       '',
       '0',
       ''
UNION ALL
SELECT 'pot-pot Dimaano',
       'Ric Dimaano',
       '',
       '09161503093',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rabi Llorca Llorca',
       'Ricardo Llorca',
       '',
       '09494898992',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bruce Abelo',
       'Richard Abelo',
       '',
       '09065051052',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chowchow Abelo',
       'Richard Abelo',
       '',
       '09065051052',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dino Hernandez',
       'Richard Hernandez',
       '',
       '09558102844',
       '',
       '0',
       ''
UNION ALL
SELECT 'Princessa Hernandez',
       'Richard Hernandez',
       '',
       '09558102844',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppy Hernandez',
       'Richard Hernandez',
       '',
       '09558102844',
       '',
       '0',
       ''
UNION ALL
SELECT 'Nala Mancao',
       'Richard Mancao',
       '',
       '09355191514',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sophie Mancao',
       'Richard Mancao',
       '',
       '09355191514',
       '',
       '0',
       ''
UNION ALL
SELECT 'GOLDIE Langit',
       'Rico Rey Langit',
       '',
       '09178299100',
       '',
       '0',
       ''
UNION ALL
SELECT 'PUPPIES 4 Langit',
       'Rico Rey Langit',
       '',
       '09178299100',
       '',
       '0',
       ''
UNION ALL
SELECT 'Maui Tandog',
       'Rigel Tandog',
       '',
       '09755264565',
       '',
       '0',
       ''
UNION ALL
SELECT 'YOSHI Santos',
       'Rina Santos',
       '',
       '09177172030',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wangyu Quidlat',
       'Ritchie Quidlat',
       '',
       '09772803871',
       '',
       '0',
       ''
UNION ALL
SELECT 'Giant Lecaros',
       'Rito Rey Lecaros',
       '',
       '09171940658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mega Lecaros',
       'Rito Rey Lecaros',
       '',
       '09171940658',
       '',
       '0',
       ''
UNION ALL
SELECT 'Behati Jaranilla',
       'Riza Jaranilla',
       '',
       '09161501899',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miranda Jaranilla',
       'Riza Jaranilla',
       '',
       '09161501899',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chunky Vicente',
       'Riza Vicente',
       '',
       '09651529127',
       '',
       '0',
       ''
UNION ALL
SELECT 'CUTTING Prajes',
       'Rizalyn Prajes',
       '',
       '09976440139',
       '',
       '0',
       ''
UNION ALL
SELECT 'Summit Yap',
       'Rizchelle Yap',
       '',
       '09956230606',
       '',
       '0',
       ''
UNION ALL
SELECT 'Max Galleto',
       'Rj Galleto',
       '',
       '09171867908',
       '',
       '0',
       ''
UNION ALL
SELECT 'Xena Galleto',
       'Rj Galleto',
       '',
       '09171867908',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brownie Cruz',
       'Robert Cruz',
       '',
       '09266222100',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pee Chee Panaguiton',
       'Robert Panuguiton',
       '',
       '09464275714',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alloka Gerago',
       'Roberto Gerago',
       '',
       '09759150088',
       '',
       '0',
       ''
UNION ALL
SELECT 'Harvie Ypil',
       'Roberto Ypil',
       '',
       '09255862191',
       '',
       '0',
       ''
UNION ALL
SELECT 'ChumChum Paclibar',
       'Rochelle Mae Paclibar',
       '',
       '09380818752',
       '',
       '0',
       ''
UNION ALL
SELECT 'LILY Micabalo',
       'Rocky San Micabalo',
       '',
       '09178691262',
       '',
       '0',
       ''
UNION ALL
SELECT 'Grooj Valbuena',
       'Rodrigo Valbuena',
       '',
       'no contact',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ivan Almerol',
       'Roel Almerol',
       '',
       '09675702998',
       '',
       '0',
       ''
UNION ALL
SELECT 'Elsa DelPuerto',
       'Rogelio Delpuerto',
       '',
       '09156014966',
       '',
       '0',
       ''
UNION ALL
SELECT 'Darla Gulles',
       'Rogelio Gulles',
       '',
       '09358570813',
       '',
       '0',
       ''
UNION ALL
SELECT 'barbie carting',
       'Roger Carting',
       '',
       'tibasak',
       '',
       '0',
       ''
UNION ALL
SELECT 'TAKI Dayadaya',
       'Rogie John Dayadaya',
       '',
       '09053650781',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cochin Demetrio',
       'Rohnel Demetrio',
       '',
       '09750832838',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tummy Demetrio',
       'Rohnel Demetrio',
       '',
       '09750832838',
       '',
       '0',
       ''
UNION ALL
SELECT 'LUNA Laquites',
       'Rojelly Lasquites',
       '',
       '09161900250',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Pechee Capungon',
       'Roland Capungon',
       '',
       '09764275714',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sugar Capungon',
       'Roland Capungon',
       '',
       '09764275714',
       '',
       '0',
       ''
UNION ALL
SELECT 'Zoey Galgo',
       'Roland Galgo',
       '',
       '09367513348',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Reyes',
       'Roland Reyes',
       '',
       '09656609342',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 4 Bayron',
       'Rolly Bayron',
       '',
       '09650550764',
       '',
       '0',
       ''
UNION ALL
SELECT 'Saske Bayron',
       'Rolly Bayron',
       '',
       '09650550764',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blacky Quipanes',
       'Romel Quipanes',
       '',
       '09055150830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dagol Tondag',
       'Romel Tondag',
       '',
       '09753047175',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sese Padinit',
       'Romeo Padinit',
       '',
       '09064947299',
       '',
       '0',
       ''
UNION ALL
SELECT 'PETER ZOSOBRECADO',
       'ROMEO ZOSOBRECADO',
       '',
       '09360370693',
       '',
       '0',
       ''
UNION ALL
SELECT 'Blue Daniel',
       'Rommel Daniel',
       '',
       '09054648521',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chowee Lim',
       'Ron Lim',
       '',
       '09177102392',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Aba-a',
       'Ronald Aba-a',
       '',
       '09255092001',
       '',
       '0',
       ''
UNION ALL
SELECT 'DAVID Galgo',
       'Ronald Galgo',
       '',
       '09367513348',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lilo  Alinas',
       'Ronals Alinas',
       '',
       '09176220353',
       '',
       '0',
       ''
UNION ALL
SELECT 'Star Magno',
       'Rosa Maria Magno',
       '',
       '09177128355',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pokemon Labastida',
       'Rosalie Labastida',
       '',
       '09355978313',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yllyn Miel',
       'Rosalinda Miel',
       '',
       '09358919167',
       '',
       '0',
       ''
UNION ALL
SELECT 'Miggy Cabunoc',
       'Rosalyn Cabunoc',
       '',
       '09751528889',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kangga Abellana',
       'Rosanna Abellana',
       '',
       '09 no contact',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Abellana',
       'Rosanna Abellana',
       '',
       '09 no contact',
       '',
       '0',
       ''
UNION ALL
SELECT 'PEPPER Tan',
       'Rose Tan',
       '',
       '09667584649',
       '',
       '0',
       ''
UNION ALL
SELECT 'Coco Pabillaran',
       'Rosejean Pabillaran',
       '',
       '09057405509',
       '',
       '0',
       ''
UNION ALL
SELECT 'YURI Tutor',
       'Rosemarie Tutor',
       '',
       '09091077732',
       '',
       '0',
       ''
UNION ALL
SELECT 'Loki Macagaan',
       'Roseraine Macagaan',
       '',
       '09459887333',
       '',
       '0',
       ''
UNION ALL
SELECT 'Rashumi Rafanan',
       'Rosevilla Rafanan',
       '',
       '09153763647',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alex Reyes',
       'Rosielyn Reyes',
       '',
       '09358280726',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marcie Uy',
       'Rosselle uy',
       '',
       '09177121238',
       '',
       '0',
       ''
UNION ALL
SELECT 'dado magalang',
       'Rowell Magalang',
       '',
       '09088749414',
       '',
       '0',
       ''
UNION ALL
SELECT 'David Magalang',
       'Rowell Magalang',
       '',
       '09088749414',
       '',
       '0',
       ''
UNION ALL
SELECT 'lucky, roxas',
       'roxas, julius jayson',
       '',
       '09531466177',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hobi Gacus',
       'Roxette Gacus',
       '',
       '09455660502',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buddy Bernal',
       'Ruben Bernal',
       '',
       '09078828855',
       '',
       '0',
       ''
UNION ALL
SELECT 'Covee Sagaral',
       'Rubie Sagaral',
       '',
       '09954111950',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tsina Alcantara',
       'Ruby Alcantara',
       '',
       '09185158538',
       '',
       '0',
       ''
UNION ALL
SELECT 'BELLA Mainit',
       'Ruby Mainit',
       '',
       '09154167906/06069342419',
       '',
       '0',
       ''
UNION ALL
SELECT 'HUNTER Mainit',
       'Ruby Mainit',
       '',
       '09154167906/06069342419',
       '',
       '0',
       ''
UNION ALL
SELECT 'ZHOWIE Mainit',
       'Ruby Mainit',
       '',
       '09154167906/06069342419',
       '',
       '0',
       ''
UNION ALL
SELECT 'Freddie Malanog',
       'Ruby Malanog',
       '',
       '09399101833',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lara Boctot',
       'Rudy Boctot',
       '',
       '09979146218',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bobie Dantes',
       'Rundy Boy Dantes',
       '',
       '09752324104',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puti Dantes',
       'Rundy Boy Dantes',
       '',
       '09752324104',
       '',
       '0',
       ''
UNION ALL
SELECT 'Killie Biwamoto',
       'Ruth Biwamoto',
       '',
       '09367329504',
       '',
       '0',
       ''
UNION ALL
SELECT 'Junil Madriano',
       'Ruth Madriano',
       '',
       '09977491113',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mako Vallejos',
       'Ruvy Vallejos',
       '',
       '09171592635',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fuji Tisoy',
       'Ryan Tisoy',
       '',
       '09163756138',
       '',
       '0',
       ''
UNION ALL
SELECT 'kitikat Tisoy',
       'Ryan Tisoy',
       '',
       '09163756138',
       '',
       '0',
       ''
UNION ALL
SELECT 'Panpan Tisoy',
       'Ryan Tisoy',
       '',
       '09163756138',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tisay Tisoy',
       'Ryan Tisoy',
       '',
       '09163756138',
       '',
       '0',
       ''
UNION ALL
SELECT 'MAX Mann',
       'Sal Andre Mann',
       '',
       '09177724504',
       '',
       '0',
       ''
UNION ALL
SELECT 'MEAGLE Mann',
       'Sal Andre Mann',
       '',
       '09177724504',
       '',
       '0',
       ''
UNION ALL
SELECT 'MIGHTY Mann',
       'Sal Andre Mann',
       '',
       '09177724504',
       '',
       '0',
       ''
UNION ALL
SELECT 'Berlin Estrella',
       'Sam Estrella',
       '',
       '09473655964',
       '',
       '0',
       ''
UNION ALL
SELECT 'SNOW Said',
       'Sam Said',
       '',
       '09183327428',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ali Painit',
       'Samuel Painit',
       '',
       '09273814975',
       '',
       '0',
       ''
UNION ALL
SELECT 'Caleigh Luna Yburan',
       'Sanreo Yburan',
       '',
       '09364175285',
       '',
       '0',
       ''
UNION ALL
SELECT 'Waki Arante',
       'Santiago Arante',
       '',
       '09053034161',
       '',
       '0',
       ''
UNION ALL
SELECT 'SMOKEY Eppie',
       'Settie Jamaica Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'TINA Eppie',
       'Settie Jamaica Eppie',
       '',
       '09163406398',
       '',
       '0',
       ''
UNION ALL
SELECT 'hatch Elizaga',
       'Shadon Elizaga',
       '',
       '09177060924',
       '',
       '0',
       ''
UNION ALL
SELECT 'Safari Gorgova?',
       'Shaira Gorgova',
       '',
       '09952254183',
       '',
       '0',
       ''
UNION ALL
SELECT 'Carlo Langot',
       'Shaira Langot',
       '',
       '09261967830',
       '',
       '0',
       ''
UNION ALL
SELECT 'Samson Jumawan',
       'Shane Jumawan',
       '',
       '09051276710',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mija Garcia',
       'Shania D. Garcia',
       '',
       '09056123283',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shadow Miculob',
       'Shannen Miculob',
       '',
       '09279825987',
       '',
       '0',
       ''
UNION ALL
SELECT 'Layla Sacriz',
       'Shantz Sacriz',
       '',
       '09177187506',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scooby Tan',
       'Sheila Tan',
       '',
       '09066256238',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pototo Ulbenario',
       'Shelay Ulbenario',
       '',
       'no num',
       '',
       '0',
       ''
UNION ALL
SELECT 'spoOky yap',
       'shella yap',
       '',
       '09663340429',
       '',
       '0',
       ''
UNION ALL
SELECT 'Empress Fajarde',
       'Shena Fajarde',
       '',
       '09750439824',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mamo Lucman',
       'Sherlini Lucman',
       '',
       '09150485789',
       '',
       '0',
       ''
UNION ALL
SELECT '5 ALMAZAN PUPS Almazan',
       'Shernan Almazan',
       '',
       '09156614420',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Tindoy',
       'Sherren Bell Tindoy',
       '',
       '09051415285',
       '',
       '0',
       ''
UNION ALL
SELECT 'Beach Lagrito',
       'Sherwin lagrito',
       '',
       '857-2307',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Villarta',
       'Shezalou Villarta',
       '',
       '09051678550',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cassie Gacus',
       'Shiela Mae Gacus',
       '',
       '09262374882',
       '',
       '0',
       ''
UNION ALL
SELECT 'Layla Benita',
       'Shielovera Benita',
       '',
       '09061642655',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alpha Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHarlie Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'JackJack Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jen Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jen Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'SOPHIA Rodriguez',
       'Shiera Rodriguez',
       '',
       '09565894359',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lemon Sabuera',
       'Shirelyn Sabuera',
       '',
       '09977490630',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yiesha Sabuero',
       'Shirelyn Sabuera',
       '',
       '09977490630',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Hontiveros',
       'Sibelle Hontiveros',
       '',
       '09150476241',
       '',
       '0',
       ''
UNION ALL
SELECT 'Parsky Galarrita',
       'Siegfred Galarrita',
       '',
       '09778105684',
       '',
       '0',
       ''
UNION ALL
SELECT 'black chio',
       'Sigfred Chio',
       '',
       '09667044615',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brown Chio',
       'Sigfred Chio',
       '',
       '09667044615',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUDDY Manduro',
       'SJ Manduro',
       '',
       '09177775069',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kinky Joloyohoy',
       'Socorro Joloyohoy',
       '',
       '09953589828',
       '',
       '0',
       ''
UNION ALL
SELECT 'noname DelaCruz',
       'Solmarie DelaCruz',
       '',
       '09057241959/0936772514',
       '',
       '0',
       ''
UNION ALL
SELECT 'Parker Dela Cruz',
       'Solmarie DelaCruz',
       '',
       '09057241959/0936772514',
       '',
       '0',
       ''
UNION ALL
SELECT 'BlueCollar Tan',
       'Sonny Boy Tan',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brandy Tan',
       'Sonny Boy Tan',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'El Tan',
       'Sonny Boy Tan',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'JunJun Tan',
       'Sonny Boy Tan',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Bonnie Rago',
       'Stefan Rago',
       '',
       '09178124114',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tofu Flores',
       'Stephanie Flores',
       '',
       '09664682024/09062932217',
       '',
       '0',
       ''
UNION ALL
SELECT 'Toby Valmores',
       'Stephanie Valmores',
       '',
       '09652452117',
       '',
       '0',
       ''
UNION ALL
SELECT 'Tonia Daque',
       'Steve Daque',
       '',
       '09178791161',
       '',
       '0',
       ''
UNION ALL
SELECT 'TERRY Tobias',
       'stevie John Tobias',
       '',
       '09971266752',
       '',
       '0',
       ''
UNION ALL
SELECT 'Pogi Gamo',
       'Sugar Gamo',
       '',
       '09669347318',
       '',
       '0',
       ''
UNION ALL
SELECT 'DOBER Singh',
       'Sukhjinder Singh',
       '',
       'Sukhjinder Singh',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hiro Bantique',
       'suseth bantigue',
       '',
       '09656604381',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Tabique',
       'Tabique',
       '',
       '09569029321',
       '',
       '0',
       ''
UNION ALL
SELECT 'Charlie Talaboc',
       'Talaboc',
       '',
       '09166559277',
       '',
       '0',
       ''
UNION ALL
SELECT 'Casper Ciatong',
       'Teddy Ciatong',
       '',
       '09168533634',
       '',
       '0',
       ''
UNION ALL
SELECT 'Theo Duray',
       'Teena Duray',
       '',
       '09565345558',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Teneza',
       'Teneza',
       '',
       '09778027913',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cardo Alavar',
       'Teresita Alavar',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cathy and Garfield Alavar',
       'Teresita Alavar',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'BunkeeShaquilandNitzebeth Garvida',
       'Tess Garvida',
       '',
       'b',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies6 Saliot',
       'Tessie Saliot',
       '',
       '09654701618',
       '',
       '0',
       ''
UNION ALL
SELECT 'PEPPER Caron',
       'Therese Marie Caron',
       '',
       '09177205664',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wosho Gala',
       'Tirsho Gala',
       '',
       '09274090600',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shack Apit',
       'Titus Apit',
       '',
       '09392976545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shake Apit',
       'Titus Apit',
       '',
       '09392976545',
       '',
       '0',
       ''
UNION ALL
SELECT 'Hachi Tiu',
       'Tiu',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Floki Caamino',
       'Toni Caamino',
       '',
       '09065389506',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Chio',
       'Toni Chio',
       '',
       '09953591853',
       '',
       '0',
       ''
UNION ALL
SELECT 'Twinkle Go',
       'Toni Phillip Go',
       '',
       '09265682388',
       '',
       '0',
       ''
UNION ALL
SELECT 'Court Cahanap',
       'Tony Cahanap',
       '',
       '09057722268',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kikiam Salise',
       'Tracy Salise',
       '',
       '09452144390',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fonti Apor',
       'Trans Apor',
       '',
       '09062589847',
       '',
       '0',
       ''
UNION ALL
SELECT 'Smalley Sogoc',
       'Trexie Sogoc',
       '',
       '09360462208',
       '',
       '0',
       ''
UNION ALL
SELECT 'Swalley Sogoc',
       'Trexie Sogoc',
       '',
       '09360462208',
       '',
       '0',
       ''
UNION ALL
SELECT 'Princess Laureta',
       'Trisshia Laureta',
       '',
       '09973022688',
       '',
       '0',
       ''
UNION ALL
SELECT 'Storm Quina',
       'Tristian Quina',
       '',
       '09265574905',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mazu Buot',
       'Twila Arriz Buot',
       '',
       '09560478263',
       '',
       '0',
       ''
UNION ALL
SELECT 'Scarlet, Ubay-ubay',
       'ubay-ubay, princess',
       '',
       '09066030040',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUNO dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cali Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kimmy Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies 4 Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Puppies Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'tyler Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yuri Dejan',
       'Vanessa Dejan',
       '',
       '09054610869',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOCO Maghanoy',
       'Vangelica Maghanoy',
       '',
       '09278398263',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mona Acenas',
       'Vangelica Maghanoy',
       '',
       '09278398263',
       '',
       '0',
       ''
UNION ALL
SELECT 'Clouie Abueva',
       'Venus Abueva',
       '',
       '09655524192',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cheska Bagabaldo',
       'Venus Bagabaldo',
       '',
       '09351671320',
       '',
       '0',
       ''
UNION ALL
SELECT 'Franko Bagabaldo',
       'Venus Bagabaldo',
       '',
       '09351671320',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sabre Entrampas',
       'Venus Entrampas',
       '',
       '09272025473',
       '',
       '0',
       ''
UNION ALL
SELECT 'Birthday Sabejon',
       'Venus Sabejon',
       '',
       '09367267595',
       '',
       '0',
       ''
UNION ALL
SELECT 'Birthday Sabejon',
       'Venus Sabejon',
       '',
       '09367267595',
       '',
       '0',
       ''
UNION ALL
SELECT 'Aldrich Lago',
       'Vergil Lago',
       '',
       '09278679188',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cookie Garces',
       'Via Garces',
       '',
       '09363157566',
       '',
       '0',
       ''
UNION ALL
SELECT 'Teddy Garces',
       'Via Garces',
       '',
       '09363157566',
       '',
       '0',
       ''
UNION ALL
SELECT 'Wave Rivera',
       'VIa rivera',
       '',
       '09976753318',
       '',
       '0',
       ''
UNION ALL
SELECT 'Primo Mangompit',
       'Vicente Mangompit',
       '',
       '09176283682',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sparky Gines',
       'Vicky Gines',
       '',
       '09654010357',
       '',
       '0',
       ''
UNION ALL
SELECT 'Greer Apit',
       'Victor Apit',
       '',
       '09064412023',
       '',
       '0',
       ''
UNION ALL
SELECT 'Jaguar Apit',
       'Victor Apit',
       '',
       '09064412023',
       '',
       '0',
       ''
UNION ALL
SELECT 'George Duray',
       'Victoria Duray',
       '',
       '09154243705',
       '',
       '0',
       ''
UNION ALL
SELECT 'JANDI Vicitacion',
       'Victoria Vicitacion',
       '',
       '09171686473',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chili Madria',
       'Vien Katniss Madria',
       '',
       '09164200422',
       '',
       '0',
       ''
UNION ALL
SELECT 'Lily Bagas',
       'Vilma Bagas',
       '',
       '09502254085',
       '',
       '0',
       ''
UNION ALL
SELECT 'SNOW Mato',
       'Vina Mato',
       '',
       '09759412251',
       '',
       '0',
       ''
UNION ALL
SELECT 'Omi-omi Catipay',
       'Vince Catipay',
       '',
       '09567704844',
       '',
       '0',
       ''
UNION ALL
SELECT 'bots Aguero',
       'Vincent Aguero',
       '',
       '09278142425',
       '',
       '0',
       ''
UNION ALL
SELECT 'Boki Castino',
       'Vincent Castino',
       '',
       '09562455459',
       '',
       '0',
       ''
UNION ALL
SELECT '',
       'Vincent Chuo',
       '',
       '09156787555',
       '',
       '',
       ''
UNION ALL
SELECT 'Travis Acuna',
       'Vincent Evan Acuna',
       '',
       '09957842154',
       '',
       '0',
       ''
UNION ALL
SELECT 'Savannah Saligomba',
       'Vincent Saligomba',
       '',
       '09458024452/09065222832',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mijay Lashley',
       'Virgie Lashley',
       '',
       '09662005399',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOTIE Bongalon',
       'Virgilio Bongalon',
       '',
       '09268970276',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHOTIE Bongalon',
       'Virgilio Bongalon',
       '',
       '09268970276',
       '',
       '0',
       ''
UNION ALL
SELECT 'Sky Bongalon',
       'Virgilio Bongalon',
       '',
       '09268970276',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chloe Dagcuita',
       'Virginia Dagcuita',
       '',
       '09650773368',
       '',
       '0',
       ''
UNION ALL
SELECT 'Brownie Alhara',
       'Vivian Alhara',
       '',
       '09657209739',
       '',
       '0',
       ''
UNION ALL
SELECT 'Molly Alhara',
       'Vivian Alhara',
       '',
       '09657209739',
       '',
       '0',
       ''
UNION ALL
SELECT 'CHUN-CHUN Amores',
       'Vivian Amores',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'CatTy Baygan',
       'Vivirly Rose Baygan',
       '',
       '09533592775',
       '',
       '0',
       ''
UNION ALL
SELECT 'Kelly Sagaral',
       'Walter Sagaral',
       '',
       '09972155435',
       '',
       '0',
       ''
UNION ALL
SELECT 'Oreo and Nicole Trecencio',
       'Warren Trecencio',
       '',
       '09065248200',
       '',
       '0',
       ''
UNION ALL
SELECT 'Buknoy Mabida',
       'Wendalyn Mabida',
       '',
       '09351115662',
       '',
       '0',
       ''
UNION ALL
SELECT 'BUTTER Mabida',
       'Wendalyn Mabida',
       '',
       '09351115662',
       '',
       '0',
       ''
UNION ALL
SELECT 'GREGGY Mabida',
       'Wendalyn Mabida',
       '',
       '09351115662',
       '',
       '0',
       ''
UNION ALL
SELECT 'Peanut Mabida',
       'Wendalyn Mabida',
       '',
       '09351115662',
       '',
       '0',
       ''
UNION ALL
SELECT 'TRIXIE Mabida',
       'Wendalyn Mabida',
       '',
       '09351115662',
       '',
       '0',
       ''
UNION ALL
SELECT 'Dianne De Leon',
       'Wilfred De Leon',
       '',
       '09051725823',
       '',
       '0',
       ''
UNION ALL
SELECT 'Angel Bacalso',
       'Wilfredo Bacalso',
       '',
       '09552604356',
       '',
       '0',
       ''
UNION ALL
SELECT 'Porkchop Bibera',
       'Wilfredo Bibero Jr',
       '',
       '',
       '',
       '0',
       ''
UNION ALL
SELECT 'Porgo Dogoldogol',
       'Wilmer Dogoldogol',
       '',
       '09364898110',
       '',
       '0',
       ''
UNION ALL
SELECT 'LOKI Mamaat',
       'Xian Mamaat',
       '',
       '09953646986',
       '',
       '0',
       ''
UNION ALL
SELECT 'Shooky Apale',
       'Xophia Apale',
       '',
       '09561725101',
       '',
       '0',
       ''
UNION ALL
SELECT 'Vacci Apale',
       'Xophia Apale',
       '',
       '09561725101',
       '',
       '0',
       ''
UNION ALL
SELECT 'Alibaba Mariga',
       'Yasser Mariga',
       '',
       '09274053866',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mocha and Edward Mariga',
       'Yasser Mariga',
       '',
       '09274053866',
       '',
       '0',
       ''
UNION ALL
SELECT 'Mocha Mariga',
       'Yasser Mariga',
       '',
       '09274053866',
       '',
       '0',
       ''
UNION ALL
SELECT 'AMITIEL Elango',
       'Yona Elango',
       '',
       '09171897741',
       '',
       '0',
       ''
UNION ALL
SELECT 'ANGEL Elango',
       'Yona Elango',
       '',
       '09171897741',
       '',
       '0',
       ''
UNION ALL
SELECT 'MONKEY Elango',
       'Yona Jayne Elango',
       '',
       '09171897741',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chuchu Go',
       'Zach Enry Go',
       '',
       '09658839146',
       '',
       '0',
       ''
UNION ALL
SELECT 'Charley Calva',
       'Zaenith Calva',
       '',
       '09177101378',
       '',
       '0',
       ''
UNION ALL
SELECT 'Ziggy Calva',
       'Zaenith Calva',
       '',
       '09177101378',
       '',
       '0',
       ''
UNION ALL
SELECT 'Broody Roque',
       'Zander Roque',
       '',
       '09056548073',
       '',
       '0',
       ''
UNION ALL
SELECT 'Chopper Roque',
       'Zander Roque',
       '',
       '09056548073',
       '',
       '0',
       ''
UNION ALL
SELECT 'HarleY Roque',
       'Zander Roque',
       '',
       '09056548073',
       '',
       '0',
       ''
UNION ALL
SELECT 'Loki Rodriquez',
       'Zedric Rodriquez',
       '',
       '0',
       '',
       '0',
       ''
UNION ALL
SELECT 'Marmar Amper',
       'Zenaida Amper',
       '',
       '09551505630',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yabyab Amper',
       'Zenaida Amper',
       '',
       '09551505630',
       '',
       '0',
       ''
UNION ALL
SELECT 'Yamyam Amper',
       'Zenaida Amper',
       '',
       '09551505630',
       '',
       '0',
       ''
UNION ALL
SELECT 'Aubrey Chio',
       'zenaida chio',
       '',
       '09667044615',
       '',
       '0',
       ''
UNION ALL
SELECT 'Christy Chio',
       'zenaida chio',
       '',
       '09667044615',
       '',
       '0',
       ''
UNION ALL
SELECT 'Merry Chio',
       'zenaida chio',
       '',
       '09667044615',
       '',
       '0',
       ''
UNION ALL
SELECT 'Reyna Gabia',
       'zoe gabia',
       '',
       '09173950749',
       '',
       '0',
       ''
UNION ALL
SELECT 'Taz Gabia',
       'zoe gabia',
       '',
       '09173950749',
       '',
       '0',
       ''
UNION ALL
SELECT 'ASHLEIGH Ricole',
       'Zoe Ricole',
       '',
       '09178768405',
       '',
       '0',
       ''
UNION ALL
SELECT 'BRUNO Mapalad',
       'Zurita Mapalad',
       '',
       '09178452852',
       '',
       '0',
       ''
UNION ALL
SELECT 'Cecee Jovel',
       'Zylee Jovel',
       '',
       '09275891056',
       '',
       '0',
       ''
UNION ALL
SELECT 'Fiona Jovel',
       'Zylee Jovel',
       '',
       '09275891056',
       '',
       '0',
       ''

GO 
