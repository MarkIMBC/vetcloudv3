/****** Object:  Table [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]    Script Date: 5/22/2021 3:26:06 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]') AND type in (N'U'))
DROP TABLE [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]
GO
/****** Object:  Table [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]    Script Date: 5/22/2021 3:26:06 AM ******/

CREATE TABLE [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS](
	[client_temp_id] [float] NULL,
	[Last Name] [nvarchar](255) NULL,
	[First Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Home Phone] [nvarchar](255) NULL,
	[patient_temp_id] [float] NULL,
	[Patient's Name] [nvarchar](255) NULL,
	[Color] [nvarchar](255) NULL,
	[Species] [nvarchar](255) NULL,
	[Breed] [nvarchar](255) NULL,
	[Age] [nvarchar](255) NULL,
	[Sex] [nvarchar](255) NULL,
	[Column2] [nvarchar](255) NULL,
	[Column3] [nvarchar](255) NULL,
	[Column4] [nvarchar](255) NULL,
	[Column1] [nvarchar](255) NULL,
	[History] [nvarchar](255) NULL,
	[Vaccination Record] [nvarchar](255) NULL,
	[Deworming Record] [nvarchar](255) NULL,
	[Date/Time/ Medications/Observations] [nvarchar](255) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (1, N'Abad', N'Vincent', N'112 Tacay Rd. Pinsaoproper', N'09164350293', 1, N'Arlo', N'black brown', N'CANINE', NULL, N'3 YO', N'Male', NULL, NULL, NULL, N'1', N'as', NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (2, N'Abnod', N'Ralph', N'Purok 4 Tam Awan Pinsao Proper', N'', 2, N'Daphne', NULL, NULL, NULL, N'6-7 mos', N'female', NULL, NULL, NULL, N'2', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (3, N'Acacio', N'Elzon', N'223 Tam Awan Pinsao Proper', N'09487172315', 3, N'Plapols', N'White', N'Canine', NULL, N'7 years old', N'Female', NULL, NULL, NULL, N'3', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (4, N'Acosta', N'Dianne', N'362a Tam Awan Baguio City', N'09052293385', 4, N'Wowo', N'Brown', N'Canine', NULL, N'2 YO', N'Female', NULL, NULL, NULL, N'4', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (5, N'Acosta', N'Dianne', N'362a Tam Awan Baguio City', N'09052293385', 5, N'Maya', NULL, N'Canine', NULL, N'2 yo', N'Female', NULL, NULL, NULL, N'4', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (6, N'Akiapat', N'Cecil', N'Atol Long Long, La Trinidad', N'09104920529', 6, N'Molly', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'5', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (7, N'Akiapat', N'Cecil', N'Atol Long Long, La Trinidad', N'09104920529', 7, N'Fox', NULL, N'Canine', NULL, N'3 months', NULL, NULL, NULL, NULL, N'5', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (8, N'Alaman', N'Leuy', N'Benin Rd', N'09493020499', 8, N'whisky', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'6', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (9, N'Alejo', N'Jeremie', N'Guisad Central', N'09382890223', 9, N'Sugar Plum', N'Brown', N'Canine', N'Shih Tzu', N'48 days', N'Female', NULL, NULL, NULL, N'7', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (10, N'Ancheta', N'Abraham', N'Baguing Tinongdan Itogon Benguet', N'09386599881', 10, N'Sky', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'8', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (11, N'Antonio', N'Trisha Mae', N'258 Camp 7, Bower West, Baguio City', N'09984284263', 11, N'Cookie', N'Black/ White', N'Canine', N'Aspin', N'3 mos', N'Female', NULL, NULL, NULL, N'9', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (12, N'Aquisan', N'Amer', N'317 Tam Awan Pinsao Proper', N'09452412440', 12, N'copper', NULL, NULL, NULL, N'6', N'male', NULL, NULL, NULL, N'10', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (13, N'Ardales', N'Marvin', N'Pinsao', N'092712134996', 13, N'cutie pie', N'gray', NULL, NULL, N'3  months', N'female', NULL, NULL, NULL, N'11', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (14, N'Ardales', N'Marvin', N'Pinsao Kalkalan', N'09271454996', 14, N'Kulet', N'White', N'Canine', N'Alaskan Malamute', N'1 year 7 mos', N'Female', NULL, NULL, NULL, N'12', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (15, N'Avecilla', N'Kristine', N'Pinsao Proper', N'09178627629', 15, N'Ali', N'Brown/ White', N'Canine', N'Mix', N'4 yrs old', N'Male', NULL, NULL, NULL, N'13', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (16, N'Ayaman', N'Kurt Michael', N'Atol Long Long, La Trinidad', N'09126199150', 16, N'CHU CHAI', N'WHITE', NULL, N'JAPANESE SPITZ', NULL, N'FEMALE', NULL, NULL, NULL, N'14', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (17, N'Ayaman', N'Kurt Michael', N'Atol Long Long, La Trinidad', N'09126199150', 17, N'RAMBO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'14', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (18, N'Ayaman', N'Kurt Michael', N'Atol Long Long, La Trinidad', N'09126199150', 18, N'BRUNO', N'BROWN', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'14', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (19, N'Ayaman', N'Kurt Michael', N'Atol Long Long, La Trinidad', N'09126199150', 19, N'CHU CHU', N'WHITE', NULL, NULL, N'6 MOS', N'FEMALE', NULL, NULL, NULL, N'14', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (20, N'Ayaman', N'Thymm', N'', N'09955634543', 20, N'SPIKE', N'WHITE', NULL, N'JAPANESE SPITZ', N'7 YO', N'MALE', NULL, NULL, NULL, N'15', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (21, N'Bacullo', N'Kathleen', N'Upper Pinget ', N'09267172388', 21, N'KIRA', N'GRAY WHITE', NULL, N'HUSKY', N'2 MOS', N'FEMALE', NULL, NULL, NULL, N'16', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (22, N'Bacullo', N'Villamor', N'Pinget', N'', 22, N'AKI', NULL, NULL, N'MIXED', NULL, NULL, NULL, NULL, NULL, N'17', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (23, N'Bagaras', N'Eisher Dan', N'59 Pinsao Pilot', N'09663795537', 23, N'ALPHA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'18', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (24, N'Ballayao', N'Nikki Ann', N'26b Pinsao Proper', N'09397638338', 24, NULL, N'White', N'Canine', N'Labrador', N'2 MOS', N'Female', NULL, NULL, NULL, N'19', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (25, N'Bangao', N'Vincent Kristof', N'309 Piraso Rd. Pinsao Proper', N'09998463020', 25, N'CHIPPY', N'WHITE BROWN BLACK', NULL, N'JACK RUSSEL TERRIER', N'8 MOS', N'FEMALE', NULL, NULL, NULL, N'20', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (26, N'Barbacena', N'Dennis Jr.', N'Kalkalan Pinsao', N'09082975635', 26, N'4 PUPS', N'WHITE W/ BLACK MARKS', NULL, N'JAP SPITZ MIX', N'2 WEEKS', N'2 MALE 2 FEMALE', NULL, NULL, NULL, N'21', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (27, N'Barbacena', N'Dennis Jr.', N'Kalkalan Pinsao', N'09082975635', 27, N'CHI CHI', NULL, NULL, N'SHIH TZU', NULL, NULL, NULL, NULL, NULL, N'21', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (28, N'Barrameda', N'Jarren', N'251 Tam Awan ', N'09060330638', 28, N'TALA', NULL, NULL, N'SHIH TZU', N'1 YO', N'MALE', NULL, NULL, NULL, N'22', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (29, N'Basilio', N'Julius Esrael', N'232 Pinsao Proper', N'09503274895', 29, N'JUMP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'23', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (30, N'Basilio', N'Xennon', N'233 B Purok 4 Pinsao Proper', N'', 30, N'PRECIOUS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'24', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (31, N'Basiwag', N'Jochebed Kelly', N'Sinayd Ucab Itogon', N'09295205319', 31, N'CHOWY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'25', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (32, N'Basungit', N'Sonny', N'461 Lucnab', N'09176791109', 32, N'BUDDHA', NULL, NULL, N'LHASA APSO', N'5 MOS', N'MALE', NULL, NULL, NULL, N'26', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (33, N'Bawing', N'Janice', N'323 Lower Tacay', N'09776022036', 33, N'GINGER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'27', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (34, N'Bawing', N'Janice', N'323 Lower Tacay', N'09776022036', 34, N'COFFEE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'27', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (35, N'Bayanay', N'Renz Carl', N'Benin Rd. Tam Awan', N'09052759573', 35, N'MAX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'28', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (36, N'Berad', N'Arvin', N'Km 5 Ltb', N'09772597612', 36, N'ANNA', NULL, NULL, N'EXOTIC BULLY', NULL, N'FEMALE', NULL, NULL, NULL, N'29', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (37, N'Berad', N'Arvin', N'Km 5 Ltb', N'09772597612', 37, N'KITKAT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'29', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (38, N'Berad', N'Arvin', N'Km 5 Ltb', N'09772597612', 38, N'BEAR', N'FAWN', NULL, N'ENGLISH BULLDOG', NULL, N'female', NULL, NULL, NULL, N'29', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (39, N'Berad', N'Arvin', N'Km 5 Ltb', N'09772597612', 39, N'CADEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'29', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (40, N'Bernandino', N'Maria Cristina', N'317 A Flora Spa Tam Awan, Pinsao Proper', N'09985774186', 40, N'Dambes', NULL, N'Feline', N'Persian', N'8 months', N'Female', NULL, NULL, NULL, N'30', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (41, N'Bitamor', N'Louie', N'136 Laksmi Drive ', N'09059094792', 41, N'MIKO', N'BROWN', NULL, N'ASPIN', N'4 MOS', N'MALE', NULL, NULL, NULL, N'31', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (42, N'Buenviaje', N'Johmel', N'160 Prk 5 Scout Barrio', N'09163210595', 42, N'MAGGIE', N'LILAC', NULL, N'EXOTIC BULLY', NULL, N'FEMALE', NULL, NULL, NULL, N'32', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (43, N'Bugnosen', N'Percival', N'312 Piraso Rd Pinsao Proper', N'09186518603', 43, N'LEILA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'33', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (44, N'Bugnosen', N'Percival', N'312 Piraso Rd Pinsao Proper', N'09186518603', 44, N'BANTAY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'33', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (45, N'Buned', N'Bernadette', N'362 Purok 1 Pinsao Proper', N'09996796597', 45, N'BOOTS', N'CREAM', NULL, NULL, N'3 MOS', N'FEMALE', NULL, NULL, NULL, N'34', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (46, N'Cabaiz', N'Marlyn', N'302 Purok 2 Pinsaoproper', N'09070877937', 46, N'FAITH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'35', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (47, N'Cabanas', N'Armando Jr', N'Bsu Compound Ltb', N'09481513823', 47, N'JOLLIBEE', N'black', NULL, N'SHIH TZU', NULL, N'MALE', NULL, NULL, NULL, N'36', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (48, N'Cabanas', N'Armando Jr', N'Bsu Compound Ltb', N'09481513823', 48, N'KATRINA', N'black', NULL, N'SHIH TZU', NULL, N'FEMALE', NULL, NULL, NULL, N'36', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (49, N'Cadalig', N'Max And Rolin', N'', N'', 49, N'TOTOT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'37', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (50, N'Cagay-An', N'Jordan', N'266a Binen Rd. Prk 3 Pinsao Proper', N'09478734880', 50, N'MADDIE', N'BLACK WHITE', NULL, N'SIBERIAN HUSKY', N'5 MOS', N'FEMALE', NULL, NULL, NULL, N'38', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (51, N'Camdas', N'Grace', N'229 Tam Awan Pinsao Proper, Baguio City', N'09999974067', 51, N'Dagul', N'Black and White', N'Canine', N'Mix', N'2 mos', N'Male', NULL, NULL, NULL, N'39', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (52, N'Campos', N'Judd Maximo', N'361 H1 Prk 1 Pinsao Proper', N'09287962445', 52, N'RUFFUS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'40', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (53, N'Capsuyen', N'Aaron', N'Slide Tuding Itogon', N'09060947527', 53, N'YAO', NULL, NULL, NULL, NULL, N'female', NULL, NULL, NULL, N'41', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (54, N'Capuyan', N'Zeny', N'City Limit Tuding Itogon', N'09178343174', 54, N'TANK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'42', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (55, N'Carbonel', N'Christopher', N'Crusher Pinsao Pilot', N'', 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'43', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (56, N'Carlos', N'Chris', N'', N'', 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'44', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (57, N'Carlos', N'Kristoffer Russel', N'71 West Quirino Hill Baguio City', N'09501255728', 57, N'CHUBBY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'45', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (58, N'Castillo', N'William', N'Lucnab Baguio City', N'09087096622', 58, N'RED', N'BLACK AND TAN', NULL, N'ROTTWEILLER', N'3 MOS', N'FEMALE', NULL, NULL, NULL, N'46', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (59, N'Castillo', N'William', N'Lucnab Baguio City', N'09087096622', 59, N'ORANGE', N'BLACK AND TAN', NULL, N'ROTTWEILLER', N'3 MOS', N'male', NULL, NULL, NULL, N'46', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (60, N'Castor', N'Michell', N'362 Rs 11 Prk 1 Pinsao Proper', N'09179365316', 60, N'MALLOWS', N'LILAC', NULL, N'AMERICAN BULLY', N'59 DAYS', N'FEMALE', NULL, NULL, NULL, N'47', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (61, N'Cena', N'Wayne Justine', N'94 Prk 11 Upper Pinget', N'09107039806', 61, N'CHICO', N'BLACK AND WHITE', NULL, N'SIBERIAN HUSKY', NULL, N'MALE', NULL, NULL, NULL, N'48', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (62, N'Chan', N'Princess Anne', N'66 Prk 5 Kalkalan Pinsao Pilot', N'09776342098', 62, N'ICKY', NULL, N'FELINE', N'PERSIAN ', N'6 MOS', N'FEMALE', NULL, NULL, NULL, N'49', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (63, N'Chees', N'Gideon', N'Tam Awan Pinsao Proper', N'09994984226', 63, N'MAKONEG', N'WHITE BROWN', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'50', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (64, N'Chilam', N'Cesar', N'219a Prk 4 Pinsaoproper', N'09993975433', 64, N'BUTCH', NULL, NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'51', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (65, N'Chilam', N'Cesar', N'219a Prk 4 Pinsaoproper', N'09993975433', 65, N'DADO', N'BROWN', NULL, N'MIXED', N'3 MOS', N'MALE', NULL, NULL, NULL, N'51', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (66, N'Chilam', N'Cesar', N'219a Prk 4 Pinsaoproper', N'09993975433', 66, N'RYAN', N'TAN', NULL, N'MIXED', N'3 MOS', N'MALE', NULL, NULL, NULL, N'51', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (67, N'Chinean', N'Freddie', N'', N'', 67, NULL, NULL, NULL, N'HUSKY', N'42 DAYS', NULL, NULL, NULL, NULL, N'52', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (68, N'Ciriano', N'Ida Jances', N'Tuding Itogon', N'09998845067', 68, N'SNOWY', N'WHITE BLACK BROWN', NULL, N'SHIH TZU', N'6 MOS', N'FEMALE', NULL, NULL, NULL, N'53', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (69, N'Codiaman', N'Johnelle', N'372a Pinsao Proper', N'09272427376', 69, N'henessy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'54', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (70, N'Codiaman', N'Johnelle', N'372a Pinsao Proper', N'09272427376', 70, N'MATHILDA', N'BLACK AND WHITE', NULL, NULL, N'1 YO', N'FEMALE', NULL, NULL, NULL, N'54', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (71, N'Cognoden', N'Madeelyn', N'Ucab Itogon', N'09128855795', 71, N'PANDA', N'WHITE BLACK BROWN', NULL, N'MALTESE SHIH TZU', NULL, N'FEMALE', NULL, NULL, NULL, N'55', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (72, N'Cognoden', N'Madeelyn', N'Ucab Itogon', N'09128855795', 72, N'MINAMIE', N'BLACK', NULL, N'HUSKY', NULL, N'FEMALE', NULL, NULL, NULL, N'55', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (73, N'Cognoden', N'Madeelyn', N'Ucab Itogon', N'09128855795', 73, N'HOTCHIKO', N'BROWN', NULL, N'HUSKY', NULL, N'MALE', NULL, NULL, NULL, N'55', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (74, N'Col-Long', N'Carlos', N'Benin Pinsao Proper', N'09362040773', 74, N'AIKA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'56', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (75, N'Cortez', N'Romeo', N'43arvizu St Middle Quezon Hill', N'09985140409', 75, N'AXEL', NULL, NULL, NULL, N'1 YO', NULL, NULL, NULL, NULL, N'57', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (76, N'Custodio', N'Liza Roi', N'94 Prk 2 Bgh Compound', N'09289172637', 76, N'BOGART', NULL, NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'58', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (77, N'Dagang', N'Marvin', N'307 Pinsao Proper', N'09991127681', 77, N'OREO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'59', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (78, N'Datu', N'Miguel', N'361 H Pinsao Proper', N'09152212944', 78, N'COMET', N'WHITE', NULL, N'FRENCH BULLDOG', N'5 MOS', N'FEMALE', NULL, NULL, NULL, N'60', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (79, N'Dayao', N'Shirley', N'Irisan', N'', 79, N'YOYA', N'BLACK AND WHITE', N'CATTLE', N'HOLSTEIN FREISIAN', N'8 MOS', N'MALE', NULL, NULL, NULL, N'61', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (80, N'De Guzman', N'Rochelle Mae', N'250k Purok 4 Pinsao Proper', N'09750158856', 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'62', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (81, N'Dedal', N'Annalyne', N'Western Poblacion, L.T.B', N'09258649566', 81, N'Jacob', N'white', N'Canine', N'Shitszu', N'3.5moths', N'Male', NULL, NULL, NULL, N'63', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (82, N'Delenela', N'', N'', N'', 82, N'LOLA CAT', NULL, N'Feline', NULL, NULL, N'FEMALE', NULL, NULL, NULL, N'64', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (83, N'Deligen', N'Gerry', N'Central Balili', N'09462356491', 83, N'MELAY', NULL, NULL, N'Shih Tzu', N'3 MOS', NULL, NULL, NULL, NULL, N'65', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (84, N'Digmayo', N'Eryll', N'372 E Pinsao Proper', N'09491745445', 84, N'KIMCHI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'66', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (85, N'Digmayo', N'Eryll', N'372 E Pinsao Proper', N'09491745445', 85, N'KIMBAP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'66', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (86, N'Digmayo', N'Roger', N'372 E Pinsao Proper', N'09393609099', 86, N'DJANGO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'67', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (87, N'Digmayo', N'Roger', N'372 E Pinsao Proper', N'09393609099', 87, N'ALYANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'67', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (88, N'Dolinta', N'Irene', N'37b Pinsao Proper', N'09194830914', 88, N'CASIE', N'WHITE BROWN', NULL, N'Shih Tzu', N'8 MOS', N'MALE', NULL, NULL, NULL, N'68', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (89, N'Dolinta', N'Irene', N'37b Pinsao Proper', N'09194830914', 89, N'SPARKY', NULL, N'Feline', N'PERSIAN ', N'2 MOS', N'female', NULL, NULL, NULL, N'68', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (90, N'Doping', N'Ryan Jay', N'210 Atoc Long Long Ltb', N'09302494653', 90, N'AM BULLY', N'GRAY/LILAC', N'CANINE', N'AM BULLY', N'8 WKS', N'FEMALE', NULL, NULL, NULL, N'69', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (91, N'Estrellado', N'Emma', N'Benin Rd', N'09212565083', 91, N'JORDAN', NULL, NULL, N'Shih Tzu', NULL, NULL, NULL, NULL, NULL, N'70', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (92, N'Fallorin', N'Borek', N'Crystal Cave', N'09175111783', 92, N'JAGGER', NULL, N'canine', N'FRENCH BULLDOG', N'9 YRS', NULL, NULL, NULL, NULL, N'71', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (93, N'Faroga', N'Dexter', N'Pinsao Kalkalan', N'09995165539', 93, N'PRINCESS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'72', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (94, N'Faycon', N'Sheila', N'24b Prk 5 Pinsao Pilot', N'', 94, N'BAGIRAH', N'BLACK', N'CANINE', N'MINI PINSCHER', N'3 YO', N'FEMALE', NULL, NULL, NULL, N'73', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (95, N'Fischer', N'Lee Ann', N'', N'', 95, N'ROBINHOOD', N'BLACK AND WHITE', N'Feline', NULL, N'9  MOS', N'MALE', NULL, NULL, NULL, N'74', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (96, N'Fischer', N'Lee Ann', N'', N'', 96, N'JEDI', NULL, N'Feline', NULL, NULL, NULL, NULL, NULL, NULL, N'74', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (97, N'Fischer', N'Lee Ann', N'', N'', 97, N'OLIVER', NULL, N'Feline', NULL, NULL, NULL, NULL, NULL, NULL, N'74', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (98, N'Fontanilla', N'Charity', N'217 Tuding Proper', N'', 98, N'SNOWBALL', N'WHITE', N'Feline', N'PUSPIN', N'4 MOS', N'MALE', NULL, NULL, NULL, N'75', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (99, N'Fung', N'Catherine Ng', N'127 Upper Tacay Pinsao Proper', N'09392600487', 99, N'WENDY', NULL, N'Canine', N'BEAGLE', N'5 YO', N'female', NULL, NULL, NULL, N'76', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (100, N'Galang', N'John Louie', N'Pk 2 Klakalan, Pinsao Proper', N'09386276474', 100, N'Milkita', N'Black and white', N'canine', N'alaskan malamute', N'2 yrs old', N'female', NULL, NULL, NULL, N'77', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (101, N'Gansoen', N'Julienne', N'Tam-Awan Long Long', N'09306989500', 101, N'Peanut', N'white brown', N'canine', N'Shih Tzu', N'2 MOS 1 WK', N'female', NULL, NULL, NULL, N'78', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (102, N'Gasmen', N'Jhon Denver', N'370 A Tam Awan Pinsao Proper', N'09351353067', 102, N'Nastya', N'Brown', N'Canine', NULL, N'Female', NULL, NULL, NULL, NULL, N'79', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (103, N'Gawaen', N'Erwin', N'Purok 1 Upper Pinsao', N'09158508351', 103, N'Grover', N'Black', N'Canine', N'Siberian Husky/ Mix', N'2 mos', N'Male', NULL, NULL, NULL, N'80', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (104, N'Gayundad', N'Fleur De Lis', N'Pinsao Proper', N'', 104, N'Nitro', NULL, N'Canine', NULL, N'5month', N'Male
Male', NULL, NULL, NULL, N'81', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (105, N'Herrera', N'Michelle', N'100 Engineer''s Hill', N'09165513349', 105, N'Holly', NULL, NULL, N'JACK RUSSEL', N'10 MOS', N'female', NULL, NULL, NULL, N'82', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (106, N'Javier', N'Danilo Jr', N'San Luis Extension', N'09081020380', 106, N'CASEY', N'BLACK TRI', NULL, N'ENGLISH BULLDOG', N'2 YO', N'female', NULL, NULL, NULL, N'83', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (107, N'Javier', N'Danilo Jr', N'San Luis Extension', N'09081020380', 107, N'ANTONIA', N'BLACK TRI', NULL, N'ENGLISH BULLDOG', N'2 YO', N'female', NULL, NULL, NULL, N'83', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (108, N'Javier', N'Luis', N'Upper Pinget ', N'09091357969', 108, N'MINUK', N'WHITE', NULL, NULL, N'10 WKS', N'female', NULL, NULL, NULL, N'84', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (109, N'Jimenez', N'Carlos', N'103 Tacay Rd Quezon Hill', N'09183515863', 109, N'CHIME', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'85', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (110, N'Juan', N'Zavadiel', N'Pinsao Proper', N'09385746258', 110, N'SUMO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'86', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (111, N'Juan', N'Zavadiel', N'Pinsao Proper', N'09385746258', 111, N'SNOOKY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'86', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (112, N'Kenniker', N'Peter', N'09 Prk 4 Victoria Village Bc', N'09486871743', 112, N'AMBER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'87', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (113, N'Kenniker', N'Peter', N'09 Prk 4 Victoria Village Bc', N'09486871743', 113, N'CHOLLA', N'BLACK ', NULL, N'Shih Tzu', N'7 MOS', N'FEMALE', NULL, NULL, NULL, N'87', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (114, N'Laban', N'Db', N'43 Purok 5 Pinsao Pilot,', N'09484345692', 114, N'Misty', NULL, N'Feline', N'Persian', N'4 months', N'Female', NULL, NULL, NULL, N'88', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (115, N'Labiang', N'Xyrene', N'278 Prk 3 Benin Rd. Pinsao Proper', N'09564578488', 115, N'SNOWBEAR', N'WHITE BROWN', NULL, N'ASPIN', N'7YO', N'female', NULL, NULL, NULL, N'89', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (116, N'Lamsis', N'Almer', N'362s Pinsao Proper', N'09171070434', 116, N'REXIE', N'GOLD', NULL, N'GOLDEN RETRIEVER', N'5 YO', N'FEMALE', NULL, NULL, NULL, N'90', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (117, N'Lardizabal', N'Katherine', N'372 E Pinsao Proper', N'09491745443', 117, N'NATSU', NULL, NULL, N'SHIH TZU', NULL, NULL, NULL, NULL, NULL, N'91', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (118, N'Layaban', N'Cherry', N'San Luis Extension', N'09464061662', 118, N'NIKAI', N'BLACK BROWN', NULL, N'German Sheperd', N'6 MOS', N'MALE', NULL, NULL, NULL, N'92', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (119, N'Layaban', N'Cherry', N'San Luis Extension', N'09464061662', 119, N'GANDA', NULL, NULL, NULL, NULL, N'female', NULL, NULL, NULL, N'92', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (120, N'Layaban', N'Mylene', N'Pinsao Proper', N'09486905703', 120, N'POGI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'93', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (121, N'Layaban', N'Mylene', N'Pinsao Proper', N'09486905703', 121, N'PUP1  PUP 2 PUP 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'93', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (122, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 122, N'BELLA', N'BLACK', NULL, NULL, N'1 YR 4 MOS', N'FEMALE', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (123, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 123, N'COOPER', N'BROWN', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (124, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 124, N'MAX', N'WHITE', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (125, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 125, N'GREY', N'GRAY/GOLD', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (126, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 126, N'BLACK NOSE', N'GRAY BROWN', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (127, N'Limus', N'Daisy', N'319c Pinsao Proper', N'09177033181', 127, N'DONG YI', NULL, NULL, NULL, NULL, N'female', NULL, NULL, NULL, N'94', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (128, N'Lumi-Ib', N'Chrisanto', N'42 Prk 5 Pinsao Pilot', N'09172401660', 128, NULL, NULL, NULL, N'German Sheperd', NULL, NULL, NULL, NULL, NULL, N'95', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (129, N'Lumiwan', N'Denn Mark', N'26 Prk 7 Sunnyside Fairview', N'', 129, N'KIWI', N'BROWN', NULL, N'MIXED', N'4 MOS', N'MALE', NULL, NULL, NULL, N'96', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (130, N'Lutog', N'Allan', N'364 D Tam Awan Prk 1 Pinsao Proper', N'09386116294', 130, N'GUMMY', NULL, N'1/4 PEKINGESE', N'1 MONTH 27 DSAYS', N'WHITE', N'MALE', NULL, NULL, NULL, N'97', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (131, N'Mahusay', N'Anthony Queen', N'Purok 4tacay Rd Baguio City', N'09070149290', 131, N'winter', N'white brown', N'canine', NULL, N'1 yr 4 mos', N'female', NULL, NULL, NULL, N'98', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (132, N'Mangapan', N'Gwen', N'250 A Purok 4, Benin Rd. Pinsao Proper, Baguio City', N'09565409729', 132, N'Vivor', NULL, N'Canine', N'Chow Mix', N'2 mos', N'Male', NULL, NULL, NULL, N'99', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (133, N'Marcelo', N'Adele Olive', N'Ic-47 Km6 Betag', N'09395454164', 133, N'Tao', NULL, N'Feline', N'Persian/Siamese', N'2 MOS', N'female', NULL, NULL, NULL, N'100', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (134, N'Marekchan', N'Mike', N'Tam-Awan Pinsao Proper', N'09504599164', 134, N'Choxie', N'Brown', N'Canine', N'Mix', N'5 months', N'Female', NULL, NULL, NULL, N'101', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (135, N'Marrero', N'Ryan', N'219 Purok 4, Pinsao Proper', N'09216519669', 135, N'Covid', NULL, N'Canine', NULL, N'10 mos', N'Female', NULL, NULL, NULL, N'102', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (136, N'Masedman', N'Marques', N'32 Purok 5 Pinsao Pilot ', N'09268829385', 136, N'shukran', NULL, N'canine', N'begian malinois', NULL, N'male', NULL, NULL, NULL, N'103', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (137, N'Mateo', N'Rae Lee', N'Tam-Awan Pinsao Proper', N'445671909194141793', 137, N'Maya', N'Black', N'Canine', N'Shih TZu', N'6 mos', N'female', NULL, NULL, NULL, N'104', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (138, N'Melgarejo', N'Wynnford', N'Beleng Tuding', N'09274934740', 138, N'Max', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'105', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (139, N'Mendoza', N'Katrina', N'2 Upper Qm', N'09773916859', 139, N'Piper', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'106', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (140, N'Mendoza', N'Maria Eunice', N'Laksmi Drv, Pinsao Rd,B.C', N'09306144044', 140, N'Nana', N'Tri-color', N'Feline', N'Puspin', N'4months', N'Female', NULL, NULL, NULL, N'107', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (141, N'Mensualuas', N'', N'Pinsao Proper, Tam Awan ', N'09454895927', 141, N'Cheeky', N'Black and white', N'canine', N'Shih Tzu', NULL, N'female', NULL, NULL, NULL, N'108', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (142, N'Mirabueno', N'Lea', N'0456 Dontogan', N'09436560161', 142, N'Siksu', N'Black Brown', N'Canine', N'Yorkshire Terrier', N'2 months', N'Male', NULL, NULL, NULL, N'109', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (143, N'Mirabueno', N'Jeiel Kevin', N'456 Dontogan Baguio', N'09174192646', 143, N'Midnight', NULL, N'Canine', N'French Bulldog', NULL, N'Female', NULL, NULL, NULL, N'110', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (144, N'Monang', N'Cecile B.', N'Md 231-D Long Long', N'09093302934', 144, N'Browny', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'111', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (145, N'Navares', N'Remia', N'Pinsao Pilot', N'09108630132', 145, N'Antonite', N'Black and White', N'Canine', NULL, N'2 yrs', N'Female', NULL, NULL, NULL, N'112', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (146, N'Ocampo', N'Paul', N'', N'', 146, N'Kali', NULL, N'canine', N'German Sheperd', NULL, N'female', NULL, NULL, NULL, N'113', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (147, N'Ocampo', N'Paul Vincent', N'', N'', 147, N'Ace', NULL, N'canine', N'Shih Tzu', NULL, NULL, NULL, NULL, NULL, N'114', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (148, N'Omedio', N'Rona', N'', N'', 148, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'115', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (149, N'On Onod', N'Gabriel', N'Central Guisad', N'09126253523', 149, N'8 Belgian Puppies', NULL, N'Canine', N'Belgian Malinois', NULL, NULL, NULL, NULL, NULL, N'116', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (150, N'Ongyod', N'Krishelle May', N'4015 Kias Baguio City', N'09504579152', 150, N'Winter', N'white', N'canine', N'husky mix', N'4 mos', N'female', NULL, NULL, NULL, N'117', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (151, N'Orchangon', N'Godphrey', N'198 Roothill St, Crystal Cave, Baguio City', N'09461378966', 151, N'Yuna Pups (7)', NULL, N'Canine', N'Beagle', NULL, NULL, NULL, NULL, NULL, N'118', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (152, N'Orcullo', N'Toots', N'12 Tacay Rd Pinsao Proper', N'09357819795', 152, N'Nikichi', NULL, NULL, NULL, N'7 yrs', NULL, NULL, NULL, NULL, N'119', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (153, N'Pacay-Uan', N'Aaron Dale Tiago', N'234 B Pinsao Proper', N'090505112323', 153, N'HUGO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'120', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (154, N'Paderon', N'Alfredo', N'148 Pinsao Proper', N'09123004445', 154, N'SHADOW', N'WHITE TAN', NULL, N'JACK RUSSEL TERRIER', N'2 MOS', NULL, NULL, NULL, NULL, N'121', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (155, N'Padua', N'Gwyneth Pearl', N'Dekkan Subd., Longlong Puguis', N'09196811006', 155, N'Sophia', NULL, N'Canine', N'Shitszu', N'1Month', N'Female', NULL, NULL, NULL, N'122', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (156, N'Pahamotang', N'Kennith', N'Pinsao Proper', N'09562541705', 156, N'Cooper', N'Black/white', N'Canine', N'Shitzu', N'3 months', N'Male', NULL, NULL, NULL, N'123', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (157, N'Palangeo', N'Valerie', N'235 B Tam Awan Pinsao Proper', N'09457704301', 157, N'SHAGGY', N'BROWN', NULL, NULL, N'10 YO', N'MALE', NULL, NULL, NULL, N'124', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (158, N'Palangeo', N'Valerie', N'235 B Tam Awan Pinsao Proper', N'09457704301', 158, N'TIKTOK', N'WHITE', NULL, NULL, N'6 MOS', N'FEMALE', NULL, NULL, NULL, N'124', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (159, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 159, N'CHOW PUP', N'CREAM', NULL, N'CHOW CHOW', N'58 DAYS', N'MALE', NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (160, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 160, N'HUSKY 1 ', N'BROWN', NULL, NULL, N'42 DAYS', NULL, NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (161, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 161, N'HUSKY 2', N'BLACK', NULL, NULL, N'42 DAYS', NULL, NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (162, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 162, N'HUSKY 3', N'RED', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (163, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 163, N'HUSKY 4', N'LIGHT BROWN ', NULL, NULL, NULL, N'FEMALE', NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (164, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 164, N'HUSKY 5', N'BLACK', NULL, NULL, NULL, N'FEMALE', NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (165, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 165, N'GRANDEE', NULL, NULL, N'CHOW CHOW', NULL, NULL, NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (166, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 166, N'CHOW PUP', NULL, NULL, N'CHOW CHOW', NULL, NULL, NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (167, N'Pangod', N'Obrien', N'Lucnab Baguio City', N'09954863601', 167, N'KHABIB', N'BUFF', NULL, N'CHOW CHOW', N'4 MOS', N'MALE', NULL, NULL, NULL, N'125', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (168, N'Pang-Uan', N'Aeron Dale', N'Pinsao Proper, B.C', N'09266555421', 168, N'Hugo', NULL, N'Canine', N'Chihuahua', N'2months', N'Male', NULL, NULL, NULL, N'126', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (169, N'Parantac', N'Erwin', N'Tam Awan Pinsao Proper', N'096626338473', 169, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'127', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (170, N'Pasegon', N'Julia', N'Km22 Atok Benguet', N'', 170, N'12 PUPS ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'128', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (171, N'Pasiwen', N'Kurt Ranson', N'Tam-Awan', N'09070966807', 171, N'BRITNEY PUPS (3)', N'WHITE', NULL, N'MALTESE', N'6 WEEKS', N'FEMALE MALE (2)', NULL, NULL, NULL, N'129', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (172, N'Pedro', N'Bernando', N'Od 472 Banig Tawang Ltb', N'09101604503', 172, N'FLUFFY', NULL, NULL, NULL, N'6  MOS', N'MALE', NULL, NULL, NULL, N'130', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (173, N'Pera', N'Cedrick David', N'230 Benin  Tacay Pinsao Proper', N'09753555292', 173, N'ABRAM', NULL, N'Feline', N'HIMALAYAN/PERSIAN', N'8 MOS', N'MALE', NULL, NULL, NULL, N'131', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (174, N'Prado', N'Christian Dale', N'362 A Long Long Pinsao Proper', N'09059530597', 174, N'BAGELS', NULL, NULL, N'HUSKY', NULL, N'FEMALE', NULL, NULL, NULL, N'132', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (175, N'Puguon', N'Karen Anne', N'Bokawkan Rd. Bc', N'09473181028', 175, N'GIGI', N'BLACK/TAN', NULL, N'TIBETAN MASTIFF', N'3.4 YO', N'FEMALE', NULL, NULL, NULL, N'133', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (176, N'Quibete', N'Cromwel/Merlita', N'16 Prk 5 Pinsao Pilot', N'09282188283', 176, N'4 PUPS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'134', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (177, N'Quibete', N'Cromwel/Merlita', N'16 Prk 5 Pinsao Pilot', N'09282188283', 177, N'DREI', N'BLACK WHITE', NULL, N'Shih TZu', N'1 YO 6 MOS', N'MALE', NULL, NULL, NULL, N'134', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (178, N'Quipse', N'Kristina', N'Pinsao Proper, B.C', N'09353188039', 178, N'Cat', NULL, N'Feline', N'domestic', NULL, NULL, NULL, NULL, NULL, N'135', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (179, N'Resurrecion', N'Agnes', N'San Carlos', N'09486937988', 179, N'LUCKY', N'WHITE BROWN', NULL, N'Shih TZu', N'4 MOS', N'MALE', NULL, NULL, NULL, N'136', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (180, N'Ricafort', N'Aaron', N'1 Benin Pinsao Proper', N'09564436986', 180, N'ALPHA', N'BLACK BROWN', NULL, N'begian malinois', N'2 MOS', N'MALE', NULL, NULL, NULL, N'137', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (181, N'Rico', N'Danielle Louise', N'361 A Tam Awan Long Long Pinsao Proper', N'9955319815', 181, N'BJORK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'138', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (182, N'Sabling', N'Mark Rafael', N'Kias Baguio City', N'09212201577', 182, N'TIGER', NULL, NULL, N'ENGISH BULLDOG', N'2 YO', N'MALE', NULL, NULL, NULL, N'139', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (183, N'Sagun', N'Riza', N'', N'', 183, N'PUP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'140', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (184, N'Sagun', N'Riza', N'', N'', 184, N'HUSKY PUP', NULL, NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'140', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (185, N'Sagun', N'Riza', N'', N'', 185, N'BELLA', NULL, NULL, NULL, N'3 YO', N'FEMALE', NULL, NULL, NULL, N'140', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (186, N'Salud', N'Doris', N'299d Piraso Rd Tam Awan Pinsao Proper', N'09652159437', 186, N'SANTINO', NULL, NULL, N'BEAGLE', NULL, N'MALE', NULL, NULL, NULL, N'141', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (187, N'Salvador', N'Sam', N'99 Tacay Rd. Pinsao Proper', N'091765980092', 187, N'KAI', N'WHITE GREY', NULL, NULL, N'1 YO', N'MALE', NULL, NULL, NULL, N'142', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (188, N'Salvaleon', N'Myra', N'234a Pinsao Proper', N'09151107781', 188, N'SILVER', NULL, N'FELINE', N'PUSPIN', N'1YR 8 MOS', NULL, NULL, NULL, NULL, N'143', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (189, N'Salvaleon', N'Myra', N'234a Pinsao Proper', N'09151107781', 189, N'ROGER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'143', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (190, N'Sanidad', N'John Amado', N'18 St Therese Mirador', N'09276372445', 190, N'MAXENE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'144', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (191, N'Santos', N'Mel', N'Pinsao Proper', N'09173254766', 191, N'Covi', N'Black/Brown', N'Canine', N'Shih Tzu', N'1 month 2 weeks', N'Male', NULL, NULL, NULL, N'145', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (192, N'Sitboy', N'Herline', N'89 Tacay Rd, Pinsao Proper', N'09676847448', 192, N'Che Che', N'Black', N'Canine', N'Aspin', N'4 mos', N'Female', NULL, NULL, NULL, N'146', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (193, N'Somera', N'Leonora', N'171 P5 Laksmi Pinsao Proper', N'09487657667', 193, N'POTCHI', NULL, NULL, N'PUSPIN', NULL, N'MALE', NULL, NULL, NULL, N'147', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (194, N'Soriano', N'Grace', N'Bernadette Drive, Lexber Hgts Camp 7', N'09175071722', 194, N'MOCHA', NULL, NULL, N'CHIHUAHUA', NULL, N'FEMALE', NULL, NULL, NULL, N'148', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (195, N'Soriano', N'Grace', N'Bernadette Drive, Lexber Hgts Camp 7', N'09175071722', 195, N'DONALD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'148', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (196, N'Soriano', N'Grace', N'Bernadette Drive, Lexber Hgts Camp 7', N'09175071722', 196, N'CHOCO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'148', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (197, N'Suan', N'Edralyn', N'235 B Tam Awan Pinsao Proper', N'09215959929', 197, N'FUJI', N'WHITE', NULL, NULL, NULL, N'MALE', NULL, NULL, NULL, N'149', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (198, N'Tabareng', N'Gina', N'309 C Prk 2 Pinsao Proper', N'0921438660', 198, N'BEAR', N'CHOCO', NULL, NULL, N'3 MOS', N'MALE', NULL, NULL, NULL, N'150', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (199, N'Tabas', N'Randell', N'Dps Compound', N'09264486713', 199, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'151', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (200, N'Tabbang', N'Polard', N'Tam Awan Pinsao Proper', N'09754710035', 200, N'BROWNY', NULL, NULL, NULL, N'2 MOS', NULL, NULL, NULL, NULL, N'152', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (201, N'Tagudar', N'Juan Carlos Angelo', N'362a Tam Awan Pinsao Proper', N'09772379129', 201, N'RAVEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'153', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (202, N'Tamayo', N'Richal', N'Irisan', N'09453514074', 202, N'AKIRO', N'BLACK WHITE', N'Canine', N'Siberian Husky', NULL, N'MALE', NULL, NULL, NULL, N'154', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (203, N'Tamlayen', N'Marco', N'', N'', 203, N'"Puppy"', NULL, N'Canine', NULL, N'2 mos', N'Female', NULL, NULL, NULL, N'155', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (204, N'Tangliben', N'Angeli', N'Baguio City', N'09156984584', 204, N'RAGNAR', N'ORANGE ', N'Feline', NULL, N'4 MOS', N'MALE', NULL, NULL, NULL, N'156', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (205, N'Tipa', N'Janet', N'362 Q2 Purok 1 Pinsao Proper', N'', 205, N'Blessy', N'Brown', N'Canine', N'Fox Pomeranian', N'3 months', N'Female', NULL, NULL, NULL, N'157', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (206, N'Toyoken', N'Jordan', N'Prk 23 Middle San Carlos Heights', N'09201153796', 206, N'ZENDY', N'BROWN', NULL, NULL, NULL, N'FEMALE', NULL, NULL, NULL, N'158', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (207, N'Ubagan', N'Shirley', N'173a Bugnay Laksmi Drive Pinsao Proper', N'09062244577', 207, N'JERRY', N'WHITE ORANGE', NULL, NULL, N'2YRS 3MOS', N'MALE', NULL, NULL, NULL, N'159', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (208, N'Ubagan', N'Joemar', N'173a Bugnay Laksmi Drive Pinsao Proper', N'09153323889', 208, N'KITTY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'160', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (209, N'Ubagan', N'Joemar', N'173a Bugnay Laksmi Drive Pinsao Proper', N'09153323889', 209, N'QUILL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'160', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (210, N'Ubagan', N'Joemar', N'173a Bugnay Laksmi Drive Pinsao Proper', N'09153323889', 210, N'KUNING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'160', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (211, N'Ubagan', N'Joemar', N'173a Bugnay Laksmi Drive Pinsao Proper', N'09153323889', 211, N'MUNING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'160', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (212, N'Ugay', N'Lee', N'', N'0927785580', 212, N'SKUNK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'161', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (213, N'Urbien', N'Trisha', N'223 Pinsao Proper, Baguio', N'09774808613', 213, N'Chikchi', N'White/ Brown', N'Canine', N'Shih Tzu', N'3 YRS OLD', N'Female', NULL, NULL, NULL, N'162', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (214, N'Urbien', N'Trisha', N'223 Pinsao Proper, Baguio', N'09774808613', 214, N'Chi Chi', NULL, N'Canine', N'Shih Tzu', NULL, N'Female', NULL, NULL, NULL, N'162', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (215, N'Veloso', N'Aimee Gem', N'38 San Roque Village', N'09955857095', 215, N'Mew', NULL, NULL, NULL, NULL, N'Male', NULL, NULL, NULL, N'163', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (216, N'Ventura', N'Alfonso', N'138 P7 Sunny Side, Fairview, Baguio City', N'09385941318', 216, N'Rabba', N'White', N'Canine', NULL, N'11 months', N'Female', NULL, NULL, NULL, N'164', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (217, N'Villanueva', N'Joas', N'55 Cherry Blossom St. Sunnyside, Quezon Hill', N'09287417602', 217, N'Sia', N'Black', N'Feline', N'Half Siamese', N'2 yrs old', N'Female', NULL, NULL, NULL, N'165', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (218, N'Villanueva', N'Joas', N'55 Cherry Blossom St. Sunnyside, Quezon Hill', N'09287417602', 218, N'Milky', NULL, N'Feline', NULL, NULL, NULL, NULL, NULL, NULL, N'165', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (219, N'Villanueva', N'Joas', N'55 Cherry Blossom St. Sunnyside, Quezon Hill', N'09287417602', 219, N'Pickles', NULL, N'Feline', NULL, NULL, NULL, NULL, NULL, NULL, N'165', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (220, N'Wagayan', N'Alwin', N'Purok 4 Pinsao Proper', N'09953621235', 220, N'Mixie', N'Gray Brown White', N'Canine', N'Maltese X Shih Tzu', N'3 months', N'Female', NULL, NULL, NULL, N'166', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (221, N'Waya-An', N'Jessica', N'Pines Park Subd. La Trinidad', N'09773542825', 221, N'Saint', NULL, N'Canine', N'Siberian Husky', N'2 months', NULL, NULL, NULL, NULL, N'167', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (222, N'Witcher', N'Brian And Luisa', N'L23 B Woodgrove Park Brgy. Del Carmen, San Fernando City, Pampanga', N'09774583529', 222, N'Jasper', N'Grey', N'Canine', N'Mix', N'9 y.o', N'Male', NULL, NULL, NULL, N'168', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (223, N'Witcher', N'Brian And Luisa', N'L23 B Woodgrove Park Brgy. Del Carmen, San Fernando City, Pampanga', N'09774583529', 223, N'Winston', N'Brown', N'Canine', N'Mix', N'3 y.o', N'Male', NULL, NULL, NULL, N'168', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (224, N'Wooden', N'Delson Markis', N'362 C Purok 1 Pinsao Proper', N'09454074889', 224, N'Oscar', NULL, N'Canine', NULL, N'4 months', N'Male', NULL, NULL, NULL, N'169', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (225, N'Wooden', N'Delson Markis', N'362 C Purok 1 Pinsao Proper', N'09454074889', 225, N'Alison', NULL, N'Canine', NULL, NULL, N'Female', NULL, NULL, NULL, N'169', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (226, N'Wooden', N'Delson Markis', N'362 C Purok 1 Pinsao Proper', N'09454074889', 226, N'Millet', NULL, N'Canine', NULL, N'3 months', N'Female', NULL, NULL, NULL, N'169', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (227, N'Yamoyam', N'Glen', N'Sitio Beleng Proper Tuding, Itogon Benguet', N'09085336020', 227, NULL, NULL, NULL, N'Labrador Retriever', NULL, N'Male', NULL, NULL, NULL, N'170', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (228, N'Zarate', N'Shirley', N'', N'', 228, N'Gio', N'Orange Tabby', N'Feline', N'Puspin', NULL, N'Male', NULL, NULL, NULL, N'171', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (229, N'Zita', N'Kathleen', N'361 H Pinsao Proper', N'097556886342', 229, N'Lita', N'Black', N'Canine', N'American Bully', N'4 months', N'Female', NULL, NULL, NULL, N'172', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (230, N'Zita', N'Kathleen', N'361 H Pinsao Proper', N'097556886342', 230, N'Bebang', NULL, N'Canine', N'Pug', NULL, NULL, NULL, NULL, NULL, N'172', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS] ([client_temp_id], [Last Name], [First Name], [Address], [Home Phone], [patient_temp_id], [Patient's Name], [Color], [Species], [Breed], [Age], [Sex], [Column2], [Column3], [Column4], [Column1], [History], [Vaccination Record], [Deworming Record], [Date/Time/ Medications/Observations]) VALUES (231, N'Zita', N'Rowena', N'361 H Pinsao Proper', N'091621920190', 231, N'Bebang (Puppies)', NULL, N'Canine', N'Pug', N'3 weeks old', NULL, NULL, NULL, NULL, N'173', NULL, NULL, NULL, NULL)
GO







Update [FAITHFULFRIENDVCCLIENTSPATIENTS]
SET    [Column1] = NULL

DECLARE @LastName      VARCHAR(MAX)= '',
        @FirstName     VARCHAR(MAX)= '',
        @ContactNumber VARCHAR(MAX)= '',
        @Address       VARCHAR(MAX)= '',
        @tempID        VARCHAR(MAX)= ''
DECLARE @_temp_LastName      VARCHAR(MAX) = '',
        @_temp_FirstName     VARCHAR(MAX) = '',
        @_temp_ContactNumber VARCHAR(MAX) = '',
        @_temp_Address       VARCHAR(MAX) = '',
        @Counter             INT = 0
DECLARE import_cursor CURSOR FOR
  SELECT [Last Name],
         [First Name],
         [Home Phone],
         [Address],
         client_temp_id
  FROM   [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @LastName,
                                   @FirstName,
                                   @ContactNumber,
                                   @Address,
                                   @tempID

WHILE @@FETCH_STATUS = 0
  BEGIN
      IF @_temp_LastName != @LastName
          OR @_temp_FirstName != @FirstName
          OR @_temp_Address != @Address
          OR @_temp_ContactNumber != @ContactNumber
        BEGIN
            SET @_temp_LastName = @LastName
            SET @_temp_FirstName = @FirstName
            SET @_temp_Address = @Address
            SET @_temp_ContactNumber = @ContactNumber
            SET @Counter = @Counter + 1
        END

      Update [FAITHFULFRIENDVCCLIENTSPATIENTS]
      SET    [Column1] = @Counter
      WHERE  client_temp_id = @tempID

      FETCH NEXT FROM import_cursor INTO @LastName,
                                         @FirstName,
                                         @ContactNumber,
                                         @Address,
                                         @tempID
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;

SELECT @Counter

GO

DECLARE @ID_Company INT = 91
Declare @Client TABLE
  (
     Name          VARCHAR(MAX),
     ContactNumber VARCHAR(MAX),
     Address       VARCHAR(MAX),
     tempID        VARCHAR(MAX)
  )

INSERT @Client
SELECT DISTINCT [Last Name]
                + CASE
                    WHEN LEN([First Name]) > 0 THEN ', '
                    ELSE ''
                  END
                + [First Name],
                [Home Phone],
                [Address],
                column1
FROM   [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]

INSERT INTO [dbo].[tClient]
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ContactNumber],
             [Address],
             [tempID])
SELECT Name,
       1,
       @ID_Company,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       GETDATE(),
       GETDATE(),
       1,
       1,
       [ContactNumber],
       [Address],
       tempID
FROM   @Client

INSERT INTO [dbo].[tPatient]
            ([Code],
             [Name],
             [IsActive],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_Gender],
             [ID_Company],
             [Species],
             [ID_Client])
SELECT '',
       CASE
         WHEN LEN(RTRIM(LTRIM(ISNULL([Patient's Name], '')))) > 0 THEN [Patient's Name]
         ELSE '-Not Specified-'
       END,
       1,
       LTRIM(RTRIM(CASE
                     WHEN LEN(ISNULL(TRIM([Color]), '')) > 0 THEN 'Color: ' + [Color]
                     ELSE ''
                   END
                   + CASE
                       WHEN LEN(ISNULL(TRIM([Color]), '')) > 0
                            AND LEN(ISNULL(TRIM(Age), '')) > 0 THEN CHar(13) + CHAR(10)
                       ELSE ''
                     END
                   + CASE
                       WHEN LEN(ISNULL(TRIM(Age), '')) > 0 THEN 'Age: ' + Age
                       ELSE ''
                     END)),
       GetDate(),
       GETDATE(),
       1,
       1,
       CASE
         WHEN LOWER(TRIM([Sex])) = 'male' THEN 1
         ELSE
           CASE
             WHEN LOWER(TRIM([Sex])) = 'female' THEN 2
             ELSE NULL
           END
       END,
       client.ID_Company,
       LTRIM(RTRIM(ISNULL([Species], '')) + ' '
             + ISNULL([Breed], '')),
       client.ID
FROm   tClient client
       inner join [FAITHFULFRIENDVCCLIENTSPATIENTS] importPatient
               on client.[tempID] = importPatient.column1
Where  client.ID_Company = @ID_Company

DECLARE @ID_Client INT = 0
DECLARE @ID_Patient INT = 0
DECLARE client_cursor CURSOR FOR
  SELECT ID
  FROM   [dbo].tClient
  WHERE  LEN(TRIM(ISNULL(Code, ''))) = 0
         AND ID_Company = @ID_Company

OPEN client_cursor

FETCH NEXT FROM client_cursor INTO @ID_Client

WHILE @@FETCH_STATUS = 0
  BEGIN
      EXEC pModel_AfterSaved_Client
        @ID_Client,
        1

      FETCH NEXT FROM client_cursor INTO @ID_Client
  END

CLOSE client_cursor;

DEALLOCATE client_cursor;

DECLARE patient_cursor CURSOR FOR
  SELECT ID
  FROM   [dbo].tPatient
  WHERE  LEN(TRIM(ISNULL(Code, ''))) = 0
         AND ID_Company = @ID_Company

OPEN patient_cursor

FETCH NEXT FROM patient_cursor INTO @ID_Patient

WHILE @@FETCH_STATUS = 0
  BEGIN
      EXEC pModel_AfterSaved_Patient
        @ID_Patient,
        1

      FETCH NEXT FROM patient_cursor INTO @ID_Patient
  END

CLOSE patient_cursor;

DEALLOCATE patient_cursor;

GO
/*
SELECT dbo.fnConvert_TitleCase(UPPER(RTRIM(LTRIM([Last Name])))),
     dbo.fnConvert_TitleCase(UPPER(RTRIM(LTRIM([First Name])))),
     REPLACE(REPLACE(TRIM(ISNULL([Home Phone], '')), ' ', ''), '-', '') ContactNumber,
     dbo.fnConvert_TitleCase([Address])
FROM   [dbo].[FAITHFULFRIENDVCCLIENTS]
GROUP  BY RTRIM(LTRIM([Last Name])),
        RTRIM(LTRIM([First Name])),
        REPLACE(REPLACE(TRIM(ISNULL([Home Phone], '')), ' ', ''), '-', ''),
        [Address]
*/
/*
Update [FAITHFULFRIENDVCCLIENTSPATIENTS]
SET    [Last Name] =  dbo.fnConvert_TitleCase(UPPER(RTRIM(LTRIM(client.[Last Name])))),
       [First Name] =  dbo.fnConvert_TitleCase(UPPER(RTRIM(LTRIM(client.[First Name])))),
       [Address] =  dbo.fnConvert_TitleCase(UPPER(client.[Address])),
       [Home Phone] = REPLACE(REPLACE(TRIM(ISNULL( client.[Home Phone], '')), ' ', ''), '-', '')
FROM   [FAITHFULFRIENDVCCLIENTSPATIENTS] clientPatient
       inner JOIN [FAITHFULFRIENDVCCLIENTS] client
              ON clientPatient.[client_temp_id] = client.[client_temp_id]
*/



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]') AND type in (N'U'))
DROP TABLE [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]
GO
/****** Object:  Table [dbo].[FAITHFULFRIENDVCCLIENTSPATIENTS]    Script Date: 5/22/2021 3:26:06 AM ******/
