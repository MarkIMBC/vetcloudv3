/*
	DECLARE @@ID_Company_NorthernValleyVeterinaryClinic INT = 130
*/
DECLARE @ID_Company_NorthernValleyVeterinaryClinic INT = 130
DECLARE @Table TABLE
  (
     tempClientID VARCHAR(1000),
     ClientName   VARCHAR(1000),
     PhoneNumber  VARCHAR(1000),
     Address      VARCHAR(1000),
     PatientName  VARCHAR(1000),
     Birthday     VARCHAR(1000),
     Sex          VARCHAR(1000),
     ID_Gender    VARCHAR(1000),
     Breed        VARCHAR(1000),
     Comment      VARCHAR(3000)
  )
DECLARE @_TempID VARCHAR(MAX)
DECLARE @___ClientName NVARCHAR(1000)= ''
DECLARE @___PhoneNumber float -- database name 
DECLARE @___Address VARCHAR(1000) -- database name 
DECLARE @ClientName NVARCHAR(1000) -- database name 
DECLARE @PhoneNumber float -- database name 
DECLARE @Address VARCHAR(1000) -- database name 
DECLARE @PatientName VARCHAR(1000) -- database name 
DECLARE @Birthday VARCHAR(1000) -- database name 
DECLARE @Sex VARCHAR(1000) -- database name 
DECLARE @Breed VARCHAR(1000) -- database name 
DECLARE db_cursor CURSOR FOR
  SELECT ISNULL(REPLACE(REPLACE(REPLACE(CASE
                                          WHEN [Client Name] = '-' THEN ''
                                          ELSE [Client Name]
                                        END, CHAR(10), ''), CHAR(13), ''), CHAR(9), ''), ''),
         [Phone Number],
         [Address],
         [Patient Name],
         [Birthday],
         [Sex],
         [Breed]
  FROM   [dbo].[NorthernValleyClientPatients]

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ClientName,
                               @PhoneNumber,
                               @Address,
                               @PatientName,
                               @Birthday,
                               @Sex,
                               @Breed

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @ClientName = ltrim(RTRIM(@ClientName))

      DECLARE @COMMENT VARCHAR(MAX) = ''
      DECLARE @ID_Gender INT = NULL

      if( TRIM(@___ClientName) <> TRIM(@ClientName)
          AND LEN(@ClientName) > 0 )
        BEGIN
            SET @___ClientName = @ClientName
            SET @___PhoneNumber = @PhoneNumber
            SET @___Address = @Address
            SET @_TempID = NEWID()
        END

      IF( ISDATE(@Birthday) = 0 )
        BEGIN
            SET @COMMENT = 'Birthday - ' + @Birthday + CHAR(10) + CHAR(13)
        END

      IF( LOWER(@Sex) IN ( 'm', 'male' ) )
        BEGIN
            SET @ID_Gender = 1
        END
      else if ( LOWER(@Sex) IN ( 'f', 'female' ) )
        BEGIN
            SET @ID_Gender = 1
        END
      ELSE
        SET @COMMENT = 'Gender - ' + ISNULL(@Sex, '') + CHAR(10)
                       + CHAR(13)

      INSERT @Table
      VALUES(@_TempID,
             @___ClientName,
             ISNULL('0'
                    + cast(CAST(@___PhoneNumber AS bigint) AS varchar(50)), ''),
             @___Address,
             @PatientName,
             TRY_CONVERT(DATE, @Birthday),
             @Sex,
             @ID_Gender,
             @Breed,
             'Imported '
             + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
             + CHAR(10) + CHAR(13) + @COMMENT )

      FETCH NEXT FROM db_cursor INTO @ClientName,
                                     @PhoneNumber,
                                     @Address,
                                     @PatientName,
                                     @Birthday,
                                     @Sex,
                                     @Breed
  END

CLOSE db_cursor

DEALLOCATE db_cursor

INSERT dbo.tClient
       (ID_Company,
        tempID,
        Name,
        Address,
        ContactNumber,
        ContactNumber2,
        Email,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT @ID_Company_NorthernValleyVeterinaryClinic,
       tbl.tempClientID,
       tbl.ClientName,
       tbl.Address,
       tbl.PhoneNumber,
       '',
       '',
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   (SELECT DISTINCT tempClientID,
                        ClientName,
                        PhoneNumber,
                        Address
        FROM   @Table) tbl
INSERT tPatient
       (ID_Company,
        ID_Client,
        Name,
        Comment,
        Species,
        ID_Gender,
        DateBirth,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company_NorthernValleyVeterinaryClinic,
       client.ID,
       importFile.PatientName,
       importFile.Comment,
       importFile.Breed,
       importFile.ID_Gender,
       CONVERT(DATETIME, importFile.Birthday),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @Table importFile
       inner JOIN tClient client
               on importFile.tempClientID = client.tempID
WHERE  client.ID_Company = @ID_Company_NorthernValleyVeterinaryClinic 
