DEclare @GUID_Company VARCHAR(MAX) = '0B66D210-1D0B-4D01-A402-1DEEE9BF8EAC'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex         INT NOT NULL IDENTITY PRIMARY KEY,
     ID_Client        INT,
     Name_Client      varchar(500),
     LastName_Client  varchar(500),
     FirstName_Client varchar(500),
     Name_Patient     varchar(500),
     Species_Patient  varchar(500),
     ContactNumber    varchar(500),
     ContactNumber2   varchar(500)
  )
DECLARE @ForDuplicateRecord TABLE
  (
     LastName_Client  varchar(500),
     FirstName_Client varchar(500),
     Name_Patient     varchar(500),
     Species_Patient  varchar(500),
     ContactNumber    varchar(500),
     ContactNumber2   varchar(500)
  )
DECLARE @ForGoodsImport TABLE
  (
     ID_Client        INT,
     Name_Client      varchar(500),
     LastName_Client  varchar(500),
     FirstName_Client varchar(500),
     Name_Patient     varchar(500),
     Species_Patient  varchar(500),
     ContactNumber    varchar(500),
     ContactNumber2   varchar(500)
  )

INSERT @ForImport
       (LastName_Client,
        FirstName_Client,
        Name_Patient,
        Species_Patient,
        ContactNumber,
        ContactNumber2)
SELECT dbo.fGetCleanedString(REPLACE(REPLACE([LAST NAME], '{', ''), '}', '')),
       dbo.fGetCleanedString(REPLACE(REPLACE([FIRST NAME], '{', ''), '}', '')),
       dbo.fGetCleanedString(REPLACE(REPLACE([PET NAME], '{', ''), '}', '')),
       dbo.fGetCleanedString(REPLACE(REPLACE([type], '{', ''), '}', '')),
       dbo.fGetCleanedString(REPLACE(REPLACE([NUMBER ], '{', ''), '}', '')),
       [Col1]
FROm   [bark And Meao Client and Patient]

Update @ForImport
SET    ContactNumber = '0' + ContactNumber
WHERE  LEN(ContactNumber) > 0

Update @ForImport
SET    ContactNumber = '0' + ContactNumber2
WHERE  LEN(ContactNumber2) > 0

DELETE FROM @ForImport
WHERE  LEN(LastName_Client) = 0
        or LEN(FirstName_Client) = 0

update @ForImport
SET    Name_Client = LastName_Client + ', ' + FirstName_Client

Update @ForImport
SET    ID_Client = client.ID
FROM   @ForImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  IsActive = 1
       and ID_Company = @ID_Company

SELECT *
FROm   tClient
WHERE  ID_Company = @ID_Company

DELETE FROM @ForImport
WHERE  ID_Client IS NOT NULL

Insert @ForDuplicateRecord
       (LastName_Client,
        FirstName_Client,
        Name_Patient)
SELECT LastName_Client,
       FirstName_Client,
       Name_Patient
FROm   @ForImport
GROUP  BY LastName_Client,
          FirstName_Client,
          Name_Patient
HAVING COUNT(*) > 1
ORDER  BY LastName_Client,
          FirstName_Client,
          Name_Patient

INSERT @ForGoodsImport
       (Name_Client,
        LastName_Client,
        FirstName_Client,
        Name_Patient,
        Species_Patient,
        ContactNumber,
        ContactNumber2)
SELECT Name_Client,
       LastName_Client,
       FirstName_Client,
       Name_Patient,
       Species_Patient,
       ContactNumber,
       ContactNumber2
FROm   @ForImport
WHERE  RowIndex NOT IN (SELECT RowIndex
                        FROm   @ForImport import
                               INNER JOIN @ForDuplicateRecord duplicate
                                       on import.LastName_Client = duplicate.LastName_Client
                                          AND import.FirstName_Client = duplicate.FirstName_Client
                                          AND import.Name_Patient = duplicate.Name_Patient)

SELECT *
FROM   @ForGoodsImport

SELECT LastName_Client,
       FirstName_Client,
       Name_Patient,
       Species_Patient,
       ContactNumber,
       ContactNumber2
FROm   @ForImport
WHERE  RowIndex IN (SELECT RowIndex
                    FROm   @ForImport import
                           INNER JOIN @ForDuplicateRecord duplicate
                                   on import.LastName_Client = duplicate.LastName_Client
                                      AND import.FirstName_Client = duplicate.FirstName_Client
                                      AND import.Name_Patient = duplicate.Name_Patient)
ORDER  BY LastName_Client,
          FirstName_Client,
          Name_Patient

INSERT dbo.tClient
       (ID_Company,
        Name,
        ContactNumber,
        ContactNumber2,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                Name_Client,
                ContactNumber,
                ContactNumber2,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @ForGoodsImport

Update @ForGoodsImport
SET    ID_Client = client.ID
FROM   @ForGoodsImport import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  IsActive = 1
       and ID_Company = @ID_Company

INSERT dbo.tPatient
       (ID_Company,
        ID_Client,
        Name,
        Species,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                Species_Patient,
                1,
                Getdate(),
                GETDATE(),
                1,
                1,
                'Imported '
                + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROm   @ForGoodsImport 

