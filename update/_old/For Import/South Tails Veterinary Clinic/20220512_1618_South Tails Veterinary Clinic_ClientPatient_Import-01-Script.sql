IF COL_LENGTH('dbo.tPatient', 'Color') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient',
        'Color',
        1
  END

GO

exec _pRefreshAllViews

GO

DECLARE @GUID_Company VARCHAR(MAX) = '5CAD91A5-9802-482C-92CF-C809FA2C2E5F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @import TABLE
  (
     RowIndex             INT,
     ID_Client            INT,
     Name_Client          VARCHAR(MAX),
     Address_Client       VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     ID_Patient           VARCHAR(MAX),
     Name_Patient         VARCHAR(MAX),
     Species_Patient      VARCHAR(MAX),
     Breed_Patient        VARCHAR(MAX),
     Color_Patient        VARCHAR(MAX)
  )

Update tClient
set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company
       and IsActive = 1

Update tPatient
set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company
       and IsActive = 1

INSERT @import
       (RowIndex,
        Name_Client,
        Address_Client,
        ContactNumber_Client,
        Name_Patient,
        Species_Patient,
        Breed_Patient,
        Color_Patient)
SELECT RowIndex,
       dbo.fGetCleanedString([Owner's Name]),
       dbo.fGetCleanedString([Address]),
       dbo.fGetCleanedString([Contact Number]),
       dbo.fGetCleanedString([Pet's Name (Pet 1/Pet 2)]),
       dbo.fGetCleanedString([SPECIES]),
       dbo.fGetCleanedString([BREED]),
       dbo.fGetCleanedString([COLOR])
FROM   [ClientPatientTemplate.Southtails2022]

Update @import
SET    Species_Patient = CASE
                           WHEN LEN(Breed_Patient) = 0 THEN Species_Patient
                           ELSE Species_Patient + ' - ' + Breed_Patient
                         END

Update @import
SET    ID_Client = client.ID
FRom   @import import
       inner join tClient client
               on client.Name = import.Name_Client
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient = patient.ID
FRom   @import import
       inner join tPatient patient
               on patient.Name = import.Name_Patient
                  and patient.ID_Client = import.ID_Client
       INNER JOIN tClient client
               on client.ID = patient.ID_Client
where  client.ID_Company = @ID_Company
       and patient.ID_Company = @ID_Company
       and patient.IsActive = 1

DELETE FROM @import
WHERE  RowIndex IN (SELECT MIN(RowIndex)
                    FROM   @import
                    GROUP  BY Name_Client,
                              Name_Patient
                    HAVING COUNT(*) > 1)

INSERT dbo.tClient
       (ID_Company,
        Name,
        Address,
        ContactNumber,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        tempID)
SELECT DISTINCT @ID_Company,
                Name_Client,
                Address_Client,
                ContactNumber_Client,
                'Imported on '
                + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm:ss tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1,
                RowIndex
FROm   @import
WHERE  ID_Client IS NULL

Update @import
SET    ID_Client = client.ID
FRom   @import import
       inner join tClient client
               on client.Name = import.Name_Client
where  ID_Company = @ID_Company
       and IsActive = 1

INSERT INTO [dbo].[tPatient]
            ([ID_Company],
             [ID_Client],
             [Name],
             [Species],
             Color,
             Comment,
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT @ID_Company,
                ID_Client,
                Name_Patient,
                Species_Patient,
                Color_Patient,
                'Imported on '
                + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm:ss tt'),
                1,
                GETDATE(),
                GETDATE(),
                1,
                1
FROm   @Import
WHERE  ID_Client IS NOT NULL
       AND ID_Patient IS NULL

Update @import
SET    ID_Patient = patient.ID
FRom   @import import
       inner join tPatient patient
               on patient.Name = import.Name_Patient
                  and patient.ID_Client = import.ID_Client
       INNER JOIN tClient client
               on client.ID = patient.ID_Client
where  client.ID_Company = @ID_Company
       and patient.ID_Company = @ID_Company
       and patient.IsActive = 1 


SELECT * FROM vUser WHERE ID_Company = @ID_Company