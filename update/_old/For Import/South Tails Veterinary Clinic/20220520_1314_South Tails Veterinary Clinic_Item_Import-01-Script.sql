IF COL_LENGTH('dbo.tPatient', 'Color') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient',
        'Color',
        1
  END

GO

exec _pRefreshAllViews

GO

DECLARE @GUID_Company VARCHAR(MAX) = '5CAD91A5-9802-482C-92CF-C809FA2C2E5F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tItem
Set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

DECLARE @forImportDuplicate TABLE
  (
     Name_Item VARCHAR(MAX)
  )
DECLARE @forImport TABLE
  (
     ID_Item               INT,
     CustomCode_Item       Varchar(MAX),
     Name_Item             Varchar(MAX),
     Description_Item      Varchar(MAX),
     ID_Supplier           INT,
     Name_Supplier         Varchar(MAX),
     ID_ItemCategory       INT,
     Name_ItemCategory     Varchar(MAX),
     UnitCost              Decimal(18, 2),
     UnitPrice             Decimal(18, 2),
     CurrentInventoryCount INT
  )
DECLARE @IsActive INT=1
DECLARE @Items_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @ID_CreatedBy INT=1
DECLARE @ID_LastModifiedBy INT=1

INSERT @forImport
       (CustomCode_Item,
        Name_Item,
        Description_Item,
        Name_Supplier,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,
        CurrentInventoryCount)
SELECT dbo.fGetCleanedString([CODE]),
       dbo.fGetCleanedString([PRODUCT]),
       dbo.fGetCleanedString([DESCRIPTION]),
       dbo.fGetCleanedString([SUPPLIER]),
       dbo.fGetCleanedString([CATEGORY]),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString([PRICE AT COST])),
       TRY_CONVERT(DECIMAL(18, 2), dbo.fGetCleanedString([PRICE])),
       TRY_CONVERT(INT, dbo.fGetCleanedString([COUNT]))
FROM   dbo.[South Tails Veterinary Clinic - ItemImportTemplate] import

DELETE FROM @forImport
WHERE  CustomCode_Item = 'ITM-0112'

Update @forImport
SET    Name_Item = REPLACE(Name_Item, ' ', '')

Update @forImport
SET    Name_Item = Name_Item
                   + CASE
                       WHEN LEN(ISNULL(Description_Item, '')) > 0 THEN ' - ' + Description_Item
                       ELSE ''
                     END

INSERT @forImportDuplicate
SELECT Name_Item
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

Update @forImport
SET    Name_Item = import.Name_Item
                   + CASE
                       WHEN LEN(ISNULL(Name_Supplier, '')) > 0 THEN ' - ' + Name_Supplier
                       ELSE ''
                     END
FROM   @forImport import
       inner join @forImportDuplicate duplicateItem
               on import.Name_Item = duplicateItem.Name_Item

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType
       and IsActive = 1

DELETE FROM @forImportDuplicate

INSERT @forImportDuplicate
SELECT Name_Item
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

SELECT *
FROm   @forImport

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             UnitCost,
             [UnitPrice],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT import.Name_Item,
                @Items_ID_ItemType,
                import.UnitCost,
                import.UnitPrice,
                @IsActive,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy
FROM   @forImport import
WHERE  ID_Item IS NULL

SELECT *
FROm   @forImport
WHERE  ID_Item IS NULL

SELECT import.*
FROM   @forImport import
       inner JOIN @forImportDuplicate tbl
               on import.Name_Item = tbl.Name_Item

SELECT Name_Item
FROm   @forImport
GROUP  BY Name_Item
HAVING COUNT(*) > 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Items_ID_ItemType
       and IsActive = 1

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession Int

select TOP 1 @ID_UserSession = ID
FROm   tUserSession
where  ID_User = 10
ORDER  BY ID DESC

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Import From Excel File',
       import.ID_Item,
       CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FRom   @forImport import
WHERE  ISNULL(import.CurrentInventoryCount, 0) > 0

SELECT *
FROM   @adjustInventory

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession 
