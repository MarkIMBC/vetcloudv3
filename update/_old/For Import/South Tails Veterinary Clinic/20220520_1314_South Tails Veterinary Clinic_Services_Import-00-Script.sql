if OBJECT_ID('dbo.[South Tails Veterinary Clinic -ServiceImportTemplate]') is not null
  BEGIN
      DROP TABLE [South Tails Veterinary Clinic -ServiceImportTemplate]
  END

GO

CREATE TABLE [dbo].[South Tails Veterinary Clinic -ServiceImportTemplate]
  (
     [Code]        varchar(500),
     [Services]    varchar(500),
     [Description] varchar(500),
     [Category]    varchar(500),
     [Price]       varchar(500)
  )

GO

INSERT INTO [dbo].[South Tails Veterinary Clinic -ServiceImportTemplate]
            ([Code],
             [Services],
             [Description],
             [Category],
             [Price])
SELECT 'SVC-0001',
       'Local 1',
       '1-10 kg',
       'ANESTHESIA',
       '500'
UNION ALL
SELECT 'SVC-0002',
       'Local 2',
       '11-20 kg',
       'ANESTHESIA',
       '1000'
UNION ALL
SELECT 'SVC-0003',
       'Local 3',
       '21-30 kg',
       'ANESTHESIA',
       '1500'
UNION ALL
SELECT 'SVC-0004',
       'General with Preanesth 1',
       '1-5kg',
       'ANESTHESIA',
       '1500'
UNION ALL
SELECT 'SVC-0005',
       'General with Preanesth 2',
       '6-10 kg',
       'ANESTHESIA',
       '2000'
UNION ALL
SELECT 'SVC-0006',
       'General with Preanesth 3',
       '11-15 kg',
       'ANESTHESIA',
       '2500'
UNION ALL
SELECT 'SVC-0007',
       'General with Preanesth 4',
       '16-20 kg',
       'ANESTHESIA',
       '3000'
UNION ALL
SELECT 'SVC-0008',
       'General with Preanesth 5',
       '21-25 kg',
       'ANESTHESIA',
       '3500'
UNION ALL
SELECT 'SVC-0009',
       'General with Preanesth 6',
       '26-30 kg',
       'ANESTHESIA',
       '4000'
UNION ALL
SELECT 'SVC-0010',
       'General with Preanesth 7',
       '31-35 kg',
       'ANESTHESIA',
       '4500'
UNION ALL
SELECT 'SVC-0011',
       'Zoletil',
       'per 1ml',
       'ANESTHESIA',
       '1000'
UNION ALL
SELECT 'SVC-0012',
       'Boarding 1',
       'Small Breed (1-10 kg)',
       'Boarding (Excluding Food)',
       '400'
UNION ALL
SELECT 'SVC-0013',
       'Boarding 2',
       'Medium (11-20kg)',
       'Boarding (Excluding Food)',
       '500'
UNION ALL
SELECT 'SVC-0014',
       'Boarding 3',
       'Large (21- 30kg)',
       'Boarding (Excluding Food)',
       '600'
UNION ALL
SELECT 'SVC-0015',
       'Boarding 4',
       'Extra Large ( 31kg to 40 kg)',
       'Boarding (Excluding Food)',
       '700'
UNION ALL
SELECT 'SVC-0016',
       'Delivery/Whelping  Assistance',
       'Includes Extraction of dead fetus',
       'BREEDING AND DELIVERY',
       '5000'
UNION ALL
SELECT 'SVC-0017',
       'Nematocide Susp (1 kg and below)',
       'Suspension',
       'DEWORMING',
       '75'
UNION ALL
SELECT 'SVC-0018',
       'Caniverm Puppy/Kitten (1 to  2kg)',
       'Tablet',
       'DEWORMING',
       '150'
UNION ALL
SELECT 'SVC-0019',
       'Caniverm Adult',
       'Tablet',
       'DEWORMING',
       '200'
UNION ALL
SELECT 'SVC-0020',
       'Drontal Tablet',
       'Tablet',
       'DEWORMING',
       '200'
UNION ALL
SELECT 'SVC-0021',
       'Worm Be Gone Large 20 to 30 kg',
       'Tablet',
       'DEWORMING',
       '200'
UNION ALL
SELECT 'SVC-0022',
       'Milbemax Puppy/Small Breeds',
       'Tablet',
       'DEWORMING',
       '200'
UNION ALL
SELECT 'SVC-0023',
       'Milbemax Adult/Large Breeds',
       'Tablet',
       'DEWORMING',
       '250'
UNION ALL
SELECT 'SVC-0024',
       'Direct Fecal Smear (Fecalysis)',
       'Fecalysis',
       'DIRECT MICROSCOPY',
       '200'
UNION ALL
SELECT 'SVC-0025',
       'Ear Swab (Ear Mite Check)',
       'Earmite Check',
       'DIRECT MICROSCOPY',
       '200'
UNION ALL
SELECT 'SVC-0026',
       'Ear Cytology',
       'Yeast Infection/ Bacterial Infection',
       'DIRECT MICROSCOPY',
       '500'
UNION ALL
SELECT 'SVC-0027',
       'Microfilaria (MCF/DBS)',
       'Heartworm Screening/ Direct Blood Smear',
       'DIRECT MICROSCOPY',
       '250'
UNION ALL
SELECT 'SVC-0028',
       'Skin Scraping',
       'Mange Test',
       'DIRECT MICROSCOPY',
       '200'
UNION ALL
SELECT 'SVC-0029',
       'Urinalysis (Dip Stick)',
       'Dip Stick',
       'DIRECT MICROSCOPY',
       '300'
UNION ALL
SELECT 'SVC-0030',
       'Urine Microscopy',
       'Urolithiasis Screening',
       'DIRECT MICROSCOPY',
       '200'
UNION ALL
SELECT 'SVC-0031',
       'Vaginal Smear',
       'Vaginal Cytology',
       'DIRECT MICROSCOPY',
       '500'
UNION ALL
SELECT 'SVC-0032',
       'Euthanasia (Cat)',
       'minimum of 1-5 kg (* plus 500 per 5 kg additional body weight)',
       'EUTHANASIA',
       '2500'
UNION ALL
SELECT 'SVC-0033',
       'Euthanasia (Dog)',
       'minimum of 1-5 kg (* plus 500 per 5 kg additional body weight)',
       'EUTHANASIA',
       '2500'
UNION ALL
SELECT 'SVC-0034',
       'SUMMERCUT 1',
       'Small Breed (10kg or less)',
       'GROOMING',
       '500'
UNION ALL
SELECT 'SVC-0035',
       'SUMMERCUT 2',
       'Medium Breed ( 11kg- 20 kg)',
       'GROOMING',
       '650'
UNION ALL
SELECT 'SVC-0036',
       'SUMMERCUT 3',
       'Large Breed ( 21 kg to 30 kg)',
       'GROOMING',
       '750'
UNION ALL
SELECT 'SVC-0037',
       'SUMMERCUT 4',
       'XL Breed (31 kg and up)',
       'GROOMING',
       '950'
UNION ALL
SELECT 'SVC-0038',
       'PUPPYCUT 1',
       'Small Breed (10kg or less)',
       'GROOMING',
       '550'
UNION ALL
SELECT 'SVC-0039',
       'PUPPYCUT 2',
       'Medium Breed ( 11kg- 20 kg)',
       'GROOMING',
       '700'
UNION ALL
SELECT 'SVC-0040',
       'PUPPYCUT 3',
       'Large Breed ( 21 kg to 30 kg)',
       'GROOMING',
       '850'
UNION ALL
SELECT 'SVC-0041',
       'PUPPYCUT 4',
       'XL Breed (31 kg and up)',
       'GROOMING',
       '1000'
UNION ALL
SELECT 'SVC-0042',
       'Deluxe Bath Package 1',
       'Small Breed (10kg or less)',
       'GROOMING',
       '350'
UNION ALL
SELECT 'SVC-0043',
       'Deluxe Bath Package 2',
       'Medium Breed ( 11kg- 20 kg)',
       'GROOMING',
       '500'
UNION ALL
SELECT 'SVC-0044',
       'Deluxe Bath Package 3',
       'Large Breed ( 21 kg to 30 kg)',
       'GROOMING',
       '650'
UNION ALL
SELECT 'SVC-0045',
       'Deluxe Bath Package 4',
       'XL Breed (31 kg and up)',
       'GROOMING',
       '750'
UNION ALL
SELECT 'SVC-0046',
       'ALA CARTE 1',
       'Toy & Small Breeds',
       'GROOMING',
       '250'
UNION ALL
SELECT 'SVC-0047',
       'ALA CARTE 2',
       'Medium Breed',
       'GROOMING',
       '300'
UNION ALL
SELECT 'SVC-0048',
       'ALA CARTE 3',
       'Large Breed',
       'GROOMING',
       '350'
UNION ALL
SELECT 'SVC-0049',
       'ALA CARTE 4',
       'Facial Trim Only',
       'GROOMING',
       '150'
UNION ALL
SELECT 'SVC-0050',
       'ALA CARTE 5',
       'Nail Clipping',
       'GROOMING',
       '150'
UNION ALL
SELECT 'SVC-0051',
       'ALA CARTE 6',
       'Ear Cleaning',
       'GROOMING',
       '150'
UNION ALL
SELECT 'SVC-0052',
       'ALA CARTE 7',
       'Anal Gland Drain',
       'GROOMING',
       '250'
UNION ALL
SELECT 'SVC-0053',
       'CAT GROOMING RATE 1',
       'Full Grooming',
       'GROOMING',
       '750'
UNION ALL
SELECT 'SVC-0054',
       'CAT GROOMING RATE 2',
       'Deluxe Bath',
       'GROOMING',
       '600'
UNION ALL
SELECT 'SVC-0055',
       'Milbemax Puppy/Small Breeds',
       'per tab',
       'HEARTWORM PREVENTIVES',
       '200'
UNION ALL
SELECT 'SVC-0056',
       'Milbemax Adult/Large Breeds',
       'per tab',
       'HEARTWORM PREVENTIVES',
       '250'
UNION ALL
SELECT 'SVC-0057',
       'Heartgard (Small) Blue',
       'per piece',
       'HEARTWORM PREVENTIVES',
       '190'
UNION ALL
SELECT 'SVC-0058',
       'Heartgard (Medium) Green',
       'per pc',
       'HEARTWORM PREVENTIVES',
       '240'
UNION ALL
SELECT 'SVC-0059',
       'Heartgard (Large) Orange',
       'per pc',
       'HEARTWORM PREVENTIVES',
       '250'
UNION ALL
SELECT 'SVC-0060',
       'Proheart XS',
       '1-5kg',
       'HEARTWORM PREVENTIVES',
       '1500'
UNION ALL
SELECT 'SVC-0061',
       'Proheart S',
       '6-10kg',
       'HEARTWORM PREVENTIVES',
       '1800'
UNION ALL
SELECT 'SVC-0062',
       'Proheart M',
       '11-15kg',
       'HEARTWORM PREVENTIVES',
       '2500'
UNION ALL
SELECT 'SVC-0063',
       'Proheart L',
       '16-20kg',
       'HEARTWORM PREVENTIVES',
       '3500'
UNION ALL
SELECT 'SVC-0064',
       'Proheart XL',
       '21-25kg',
       'HEARTWORM PREVENTIVES',
       '4500'
UNION ALL
SELECT 'SVC-0065',
       'Proheart XXL',
       '26-30kg',
       'HEARTWORM PREVENTIVES',
       '5500'
UNION ALL
SELECT 'SVC-0066',
       'Immiticide (Melarsomine)',
       'Vial',
       'HEARTWORM TREATMENT',
       '3700'
UNION ALL
SELECT 'SVC-0067',
       'Small Breed & Puppy/Kitten (1-10 kg)',
       'Non-infectious',
       'HOSPITALIZATION',
       '400'
UNION ALL
SELECT 'SVC-0068',
       'Medium (11-20kg)',
       'Non-infectious',
       'HOSPITALIZATION',
       '600'
UNION ALL
SELECT 'SVC-0069',
       'Large (21- 30kg)',
       'Non-infectious',
       'HOSPITALIZATION',
       '700'
UNION ALL
SELECT 'SVC-0070',
       'Extra Large ( 31kg to 40 kg)',
       'Non-infectious',
       'HOSPITALIZATION',
       '800'
UNION ALL
SELECT 'SVC-0071',
       'Small Breed/Puppy',
       'Infectious (Isolation)',
       'HOSPITALIZATION',
       '450'
UNION ALL
SELECT 'SVC-0072',
       'Medium',
       'Infectious (Isolation)',
       'HOSPITALIZATION',
       '650'
UNION ALL
SELECT 'SVC-0073',
       'Large Breed',
       'Infectious (Isolation)',
       'HOSPITALIZATION',
       '750'
UNION ALL
SELECT 'SVC-0074',
       'Pregnancy Diagnosis',
       'Ultrasound',
       'IMAGING',
       '850'
UNION ALL
SELECT 'SVC-0075',
       'Abdomen',
       'Ultrasound',
       'IMAGING',
       '1500'
UNION ALL
SELECT 'SVC-0076',
       'Thoracic',
       'Ultrasound',
       'IMAGING',
       '1500'
UNION ALL
SELECT 'SVC-0077',
       'CBC',
       'Complete Blood Chemistry',
       'LABORATORY /DIAGNOSTIC TESTS',
       '600'
UNION ALL
SELECT 'SVC-0078',
       'Chem 10',
       'A focused chemistry foundation; combine with a CBC, electrolytes, and other tests such as SDMA. Use albumin level to guide safe administration of anesthesia.',
       'LABORATORY /DIAGNOSTIC TESTS',
       '1800'
UNION ALL
SELECT 'SVC-0079',
       'Chem 15',
       'A comprehensive chemistry profile with GGT, valuable for assessing liver function, particularly in feline patients. Combine with a CBC, electrolytes, and tests such as SDMA and Total T4 for full evaluation.',
       'LABORATORY /DIAGNOSTIC TESTS',
       '2400'
UNION ALL
SELECT 'SVC-0080',
       'Chem 17',
       'A comprehensive chemistry profile with amylase and lipase, making it ideal for canine patients. Combine with a CBC, electrolytes, and tests such as SDMA and Total T4 for full evaluation.',
       'LABORATORY /DIAGNOSTIC TESTS',
       '2600'
UNION ALL
SELECT 'SVC-0081',
       'Nsaid 6',
       'Establish a baseline and monitor NSAID therapy. Ideal for identifying liver damage.',
       'LABORATORY /DIAGNOSTIC TESTS',
       '1200'
UNION ALL
SELECT 'SVC-0082',
       'Lyte 4',
       'Electrolytes provide information on acid-base and hydration status and can help to identify underlying metabolic disease.',
       'LABORATORY /DIAGNOSTIC TESTS',
       '1000'
UNION ALL
SELECT 'SVC-0083',
       'SDMA',
       'Monitor kidney function with a new standard of care that detects and reflects disease',
       'LABORATORY /DIAGNOSTIC TESTS',
       '1500'
UNION ALL
SELECT 'SVC-0084',
       'T4',
       'Easily screen, diagnose, and manage thyroid disease',
       'LABORATORY /DIAGNOSTIC TESTS',
       '1800'
UNION ALL
SELECT 'SVC-0085',
       'Alt',
       'Liver Exam',
       'LABORATORY /DIAGNOSTIC TESTS',
       '400'
UNION ALL
SELECT 'SVC-0086',
       'Alkp',
       'Liver Screening',
       'LABORATORY /DIAGNOSTIC TESTS',
       '400'
UNION ALL
SELECT 'SVC-0087',
       'Bun',
       'Kidney Exam Screening',
       'LABORATORY /DIAGNOSTIC TESTS',
       '350'
UNION ALL
SELECT 'SVC-0088',
       'Crea',
       'Kidney Exam Screening',
       'LABORATORY /DIAGNOSTIC TESTS',
       '350'
UNION ALL
SELECT 'SVC-0089',
       'Fibrinogen Test',
       'Clotting Factor',
       'LABORATORY /DIAGNOSTIC TESTS',
       '300'
UNION ALL
SELECT 'SVC-0090',
       'Abdominocentesis',
       '',
       'OTHER PROCEDURES',
       '1500'
UNION ALL
SELECT 'SVC-0091',
       'Aural Hematoma Bandaging',
       'Unilateral',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0092',
       'Aural Hematoma Bandaging',
       'Bilateral',
       'OTHER PROCEDURES',
       '1000'
UNION ALL
SELECT 'SVC-0093',
       'Bandaging (Leg)',
       'Unilateral',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0094',
       'Enema (Soapy)',
       '',
       'OTHER PROCEDURES',
       '1000'
UNION ALL
SELECT 'SVC-0095',
       'Thoracocentesis',
       '',
       'OTHER PROCEDURES',
       '1500'
UNION ALL
SELECT 'SVC-0096',
       'Nebulization',
       'per session',
       'OTHER PROCEDURES',
       '200'
UNION ALL
SELECT 'SVC-0097',
       'TVT Chemotherapy',
       'per vial',
       'OTHER PROCEDURES',
       '3000'
UNION ALL
SELECT 'SVC-0098',
       'FDT ( Flourescein Dye Test)',
       '',
       'OTHER PROCEDURES',
       '300'
UNION ALL
SELECT 'SVC-0099',
       'Aural Hematoma Draining',
       'Unilateral',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0100',
       'Aural Hematoma Draining',
       'Bilateral',
       'OTHER PROCEDURES',
       '1000'
UNION ALL
SELECT 'SVC-0101',
       'Urinary Catheterization',
       '',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0102',
       'Urinary Catheter (Male)',
       '',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0103',
       'Urinary Catheter (Female)',
       '',
       'OTHER PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0104',
       'IV Fluid Set (1 Bot. IV fluid, 1 IV cannula, 1 IV line)',
       '',
       'OTHER PROCEDURES',
       '850'
UNION ALL
SELECT 'SVC-0105',
       'Lactated Ringer''s Solution',
       '',
       'OTHER PROCEDURES',
       '150'
UNION ALL
SELECT 'SVC-0106',
       '0.9% Sodium Chloride',
       '1 bottle',
       'OTHER PROCEDURES',
       '150'
UNION ALL
SELECT 'SVC-0107',
       'IV Cannula (Gauge 24, 21)',
       '1 bottle',
       'OTHER PROCEDURES',
       '250'
UNION ALL
SELECT 'SVC-0108',
       'IV line',
       'per piece',
       'OTHER PROCEDURES',
       '250'
UNION ALL
SELECT 'SVC-0109',
       'IV Reinsertion (1 IV Cannula)',
       'per piece',
       'OTHER PROCEDURES',
       '350'
UNION ALL
SELECT 'SVC-0110',
       'Heplock',
       '',
       'OTHER PROCEDURES',
       '250'
UNION ALL
SELECT 'SVC-0111',
       'Pregnant 1',
       '1-5 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '850'
UNION ALL
SELECT 'SVC-0112',
       'Pregnant 2',
       '6-10 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '950'
UNION ALL
SELECT 'SVC-0113',
       'Pregnant 3',
       '11-15 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '1050'
UNION ALL
SELECT 'SVC-0114',
       'Pregnant 4',
       '16-20 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '1150'
UNION ALL
SELECT 'SVC-0115',
       'Pregnant 5',
       '21-25 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '1250'
UNION ALL
SELECT 'SVC-0116',
       'Pregnant 6',
       '26-30 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '1350'
UNION ALL
SELECT 'SVC-0117',
       'Pregnant 7',
       '31-35 kg',
       'Pregnant (For Whelping Assistance/CS)',
       '1450'
UNION ALL
SELECT 'SVC-0118',
       'CPV Ag  TEST',
       '',
       'RAPID TEST',
       '850'
UNION ALL
SELECT 'SVC-0119',
       'CPV - CCV Ag TEST',
       '',
       'RAPID TEST',
       '950'
UNION ALL
SELECT 'SVC-0120',
       'CPV-CCV-Giardia',
       '',
       'RAPID TEST',
       '1000'
UNION ALL
SELECT 'SVC-0121',
       'CDV Ag Test',
       '',
       'RAPID TEST',
       '850'
UNION ALL
SELECT 'SVC-0122',
       'CHW Ag Test',
       '',
       'RAPID TEST',
       '850'
UNION ALL
SELECT 'SVC-0123',
       'Relaxin Test (Pregnancy Test)',
       '',
       'RAPID TEST',
       '850'
UNION ALL
SELECT 'SVC-0124',
       'CVP- CCV ehrlichia Anaplasma',
       '',
       'RAPID TEST',
       '1000'
UNION ALL
SELECT 'SVC-0125',
       '4-Way Test (Ana, Her, Leish, HW)',
       '',
       'RAPID TEST',
       '1000'
UNION ALL
SELECT 'SVC-0126',
       'SNAP 4Dx Test',
       '',
       'RAPID TEST',
       '1500'
UNION ALL
SELECT 'SVC-0127',
       'Anal Sac Draining',
       '',
       'ROUTINE PROCEDURES',
       '300'
UNION ALL
SELECT 'SVC-0128',
       'Blood Transfusion ( Procedure Only)',
       '1 to 5 kg (Cats)',
       'ROUTINE PROCEDURES',
       '3000'
UNION ALL
SELECT 'SVC-0129',
       'Blood Transfusion ( Procedure Only)',
       '6 to 10 kg  (Cats)',
       'ROUTINE PROCEDURES',
       '4000'
UNION ALL
SELECT 'SVC-0130',
       'Blood Transfusion ( Procedure Only)',
       '20 to 25 kg Dogs',
       'ROUTINE PROCEDURES',
       '6000'
UNION ALL
SELECT 'SVC-0131',
       'Blood Transfusion ( Procedure Only)',
       '26 to 30 kg Dogs',
       'ROUTINE PROCEDURES',
       '7000'
UNION ALL
SELECT 'SVC-0132',
       'Ear Cleaning',
       '',
       'ROUTINE PROCEDURES',
       '150'
UNION ALL
SELECT 'SVC-0133',
       'Ear Cleaning with Earmites',
       '',
       'ROUTINE PROCEDURES',
       '200'
UNION ALL
SELECT 'SVC-0134',
       'Ear Cleaning with Otitis (Ear Flushing)',
       '',
       'ROUTINE PROCEDURES',
       '500'
UNION ALL
SELECT 'SVC-0135',
       'Nail Clipping',
       '',
       'ROUTINE PROCEDURES',
       '150'
UNION ALL
SELECT 'SVC-0136',
       'Simple Wound Dressing (PF Only)',
       'Minimum',
       'ROUTINE PROCEDURES',
       '350'
UNION ALL
SELECT 'SVC-0137',
       'Amputation of the Leg * (Unilateral) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0138',
       'Amputation of the Leg * (Unilateral) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0139',
       'Amputation of the Leg * (Unilateral) 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0140',
       'Amputation of the Leg * (Unilateral) 4',
       '16- 20 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0141',
       'Amputation of the Leg * (Unilateral) 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0142',
       'Amputation of the Leg * (Unilateral) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0143',
       'Amputation of the Leg * (Unilateral) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0144',
       'Anal Prolapse Repair 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0145',
       'Anal Prolapse Repair 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0146',
       'Anal Prolapse Repair 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0147',
       'Anal Prolapse Repair 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0148',
       'Anal Prolapse Repair 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0149',
       'Anal Prolapse Repair 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0150',
       'Anal Prolapse Repair 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0151',
       'Anal Sac Resection 1',
       '1-5kg BW',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0152',
       'Anal Sac Resection 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0153',
       'Anal Sac Resection 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0154',
       'Anal Sac Resection 4',
       '16-30 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0155',
       'Anal Sac Resection 5',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0156',
       'Auricular Hematoma Repair (Unilateral)',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '4000'
UNION ALL
SELECT 'SVC-0157',
       'Auricular Hematoma Repair (Bilateral) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0158',
       'Auricular Hematoma Repair (Bilateral) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0159',
       'Auricular Hematoma Repair (Bilateral) 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0160',
       'Auricular Hematoma Repair (Bilateral) 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0161',
       'Auricular Hematoma Repair (Bilateral) 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0162',
       'Auricular Hematoma Repair (Bilateral) 6',
       '26-30kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0163',
       'Auricular Hematoma Repair (Bilateral) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0164',
       'Caesarean Section (Cat) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0165',
       'Caesarean Section (Cat) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0166',
       'Caesarean Section (Dog) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0167',
       'Caesarean Section (Dog) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0168',
       'Caesarean Section (Dog) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0169',
       'Caesarean Section (Dog) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0170',
       'Caesarean Section (Dog) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '16000'
UNION ALL
SELECT 'SVC-0171',
       'Caesarean Section (Dog) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '18000'
UNION ALL
SELECT 'SVC-0172',
       'Caesarean Section (Dog) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '20000'
UNION ALL
SELECT 'SVC-0173',
       'Caesarean Section with Ovariohystectomy Cat 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0174',
       'Caesarean Section with Ovariohystectomy Cat 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0175',
       'Caesarean Section with Ovariohystectomy Dog 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0176',
       'Caesarean Section with Ovariohystectomy Dog 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0177',
       'Caesarean Section with Ovariohystectomy Dog 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0178',
       'Caesarean Section with Ovariohystectomy Dog 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '15000'
UNION ALL
SELECT 'SVC-0179',
       'Caesarean Section with Ovariohystectomy Dog 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '17000'
UNION ALL
SELECT 'SVC-0180',
       'Caesarean Section with Ovariohystectomy Dog 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '19000'
UNION ALL
SELECT 'SVC-0181',
       'Caesarean Section with Ovariohystectomy Dog 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '21000'
UNION ALL
SELECT 'SVC-0182',
       'Castration Cat 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '1000'
UNION ALL
SELECT 'SVC-0183',
       'Castration Cat 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '1500'
UNION ALL
SELECT 'SVC-0184',
       'Castration Dog (Normal) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '3500'
UNION ALL
SELECT 'SVC-0185',
       'Castration Dog (Normal) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '4000'
UNION ALL
SELECT 'SVC-0186',
       'Castration Dog (Normal) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '4500'
UNION ALL
SELECT 'SVC-0187',
       'Castration Dog (Normal) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0188',
       'Castration Dog (Normal) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '5500'
UNION ALL
SELECT 'SVC-0189',
       'Castration Dog (Normal) 6',
       '26-30kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0190',
       'Castration Dog (Normal) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '6500'
UNION ALL
SELECT 'SVC-0191',
       'Castration Dog (Inguinal) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0192',
       'Castration Dog (Inguinal) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0193',
       'Castration Dog (Inguinal) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0194',
       'Castration Dog (Inguinal) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0195',
       'Castration Dog (Inguinal) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0196',
       'Castration Dog (Inguinal) 6',
       '25- 30 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0197',
       'Castration Dog (Inguinal) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0198',
       'Castration Dog (Inguinal) 8',
       '36-40 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0199',
       'Elbow Hygroma (Bilateral) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0200',
       'Elbow Hygroma (Bilateral) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0201',
       'Elbow Hygroma (Bilateral) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0202',
       'Elbow Hygroma (Bilateral) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0203',
       'Elbow Hygroma (Bilateral) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0204',
       'Elbow Hygroma (Bilateral) 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0205',
       'Elbow Hygroma (Bilateral) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0206',
       'Elbow Hygroma (Unilateral) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '4000'
UNION ALL
SELECT 'SVC-0207',
       'Elbow Hygroma (Unilateral) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0208',
       'Elbow Hygroma (Unilateral) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0209',
       'Elbow Hygroma (Unilateral) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0210',
       'Elbow Hygroma (Unilateral) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0211',
       'Elbow Hygroma (Unilateral) 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0212',
       'Elbow Hygroma (Unilateral) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0213',
       'Enterotomy 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0214',
       'Enterotomy 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0215',
       'Enterotomy 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0216',
       'Enterotomy 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0217',
       'Enterotomy 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0218',
       'Enterotomy 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0219',
       'Enterotomy 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0220',
       'Entropion (Unilateral) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0221',
       'Entropion (Unilateral) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0222',
       'Entropion (Unilateral) 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0223',
       'Entropion (Unilateral) 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0224',
       'Entropion (Unilateral) 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0225',
       'Entropion (Unilateral) 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0226',
       'Entropion (Unilateral) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0227',
       'Entropion (Bilateral ) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0228',
       'Entropion (Bilateral ) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0229',
       'Entropion (Bilateral ) 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0230',
       'Entropion (Bilateral ) 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0231',
       'Entropion (Bilateral ) 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0232',
       'Entropion (Bilateral ) 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0233',
       'Entropion (Bilateral ) 7',
       '31- 35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0234',
       'Enucleation 1',
       '1-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0235',
       'Enucleation 2',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0236',
       'Enucleation 3',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0237',
       'Enucleation 4',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0238',
       'Enucleation 5',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0239',
       'Enucleation 6',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0240',
       'Exploratomy Laparotomy 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0241',
       'Exploratomy Laparotomy 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0242',
       'Exploratomy Laparotomy 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0243',
       'Exploratomy Laparotomy 4',
       '16- 20 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0244',
       'Exploratomy Laparotomy 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0245',
       'Exploratomy Laparotomy 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0246',
       'Exploratomy Laparotomy 7',
       '31- 35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0247',
       'Gastrotomy 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0248',
       'Gastrotomy 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0249',
       'Gastrotomy 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0250',
       'Gastrotomy 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0251',
       'Gastrotomy 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0252',
       'Gastrotomy 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0253',
       'Gastrotomy 7',
       '31- 35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0254',
       'Hernia Repair (Inguinal) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0255',
       'Hernia Repair (Inguinal) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0256',
       'Hernia Repair (Inguinal) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0257',
       'Hernia Repair (Inguinal) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0258',
       'Hernia Repair (Inguinal) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0259',
       'Hernia Repair (Inguinal) 6',
       '26- 30 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0260',
       'Hernia Repair (Inguinal) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0261',
       'Hernia Repair (Perineal) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0262',
       'Hernia Repair (Perineal) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0263',
       'Hernia Repair (Perineal) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0264',
       'Hernia Repair (Perineal) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0265',
       'Hernia Repair (Perineal) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0266',
       'Hernia Repair (Perineal) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0267',
       'Hernia Repair (Perineal) 7',
       '31- 35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0268',
       'Hernia Repair (Umbilical) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0269',
       'Hernia Repair (Umbilical) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0270',
       'Hernia Repair (Umbilical) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0271',
       'Hernia Repair (Umbilical) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0272',
       'Hernia Repair (Umbilical) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0273',
       'Hernia Repair (Umbilical) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0274',
       'Hernia Repair (Umbilical) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0275',
       'Intestinal Anastomosis 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0276',
       'Intestinal Anastomosis 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0277',
       'Intestinal Anastomosis 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0278',
       'Intestinal Anastomosis 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0279',
       'Intestinal Anastomosis 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0280',
       'Intestinal Anastomosis 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0281',
       'Intestinal Anastomosis 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0282',
       'Mastectomy (Regional) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0283',
       'Mastectomy (Regional) 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0284',
       'Mastectomy (Regional) 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0285',
       'Mastectomy (Regional) 4',
       '16- 20 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0286',
       'Mastectomy (Regional) 5',
       '21- 25 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0287',
       'Mastectomy (Regional) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0288',
       'Mastectomy (Regional) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '14000'
UNION ALL
SELECT 'SVC-0289',
       'Ovariohysterectomy/Spay (Dog) 1',
       '1-5kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0290',
       'Ovariohysterectomy/Spay (Dog) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0291',
       'Ovariohysterectomy/Spay (Dog) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0292',
       'Ovariohysterectomy/Spay (Dog) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0293',
       'Ovariohysterectomy/Spay (Dog) 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0294',
       'Ovariohysterectomy/Spay (Dog) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0295',
       'Ovariohysterectomy/Spay (Dog) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0296',
       'Ovariohysterectomy with Pyometra 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0297',
       'Ovariohysterectomy with Pyometra 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0298',
       'Ovariohysterectomy with Pyometra 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0299',
       'Ovariohysterectomy with Pyometra 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0300',
       'Ovariohysterectomy with Pyometra 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0301',
       'Ovariohysterectomy with Pyometra 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0302',
       'Ovariohysterectomy with Pyometra 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0303',
       'Ovariohysterectom/Spay (Cat)',
       'Minimum per pup',
       'SURGERIES: (Procedure Only)',
       '3500'
UNION ALL
SELECT 'SVC-0304',
       'Stitch Up (Local Anesthesia)',
       'Minimum per pup',
       'SURGERIES: (Procedure Only)',
       '500'
UNION ALL
SELECT 'SVC-0305',
       'Tail Docking (3-day old pup)',
       'Minimum per pup',
       'SURGERIES: (Procedure Only)',
       '300'
UNION ALL
SELECT 'SVC-0306',
       'Tarsorrhapy Unilateral 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '4000'
UNION ALL
SELECT 'SVC-0307',
       'Tarsorrhapy Unilateral 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0308',
       'Tarsorrhapy Unilateral 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0309',
       'Tarsorrhapy Unilateral 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0310',
       'Tarsorrhapy Unilateral 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0311',
       'Tarsorrhapy Unilateral 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0312',
       'Tarsorrhapy Unilateral 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0313',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '5000'
UNION ALL
SELECT 'SVC-0314',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 2',
       '6-10 kg',
       'SURGERIES: (Procedure Only)',
       '6000'
UNION ALL
SELECT 'SVC-0315',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 3',
       '11-15 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0316',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 4',
       '16-20 kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0317',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 5',
       '21-25 kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0318',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0319',
       'Third Eyelid Prolapse Repair (Cherry Eye) Unilateral 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0320',
       'Urethrostomy (Dog/Cat) 1',
       '1-5 kg',
       'SURGERIES: (Procedure Only)',
       '7000'
UNION ALL
SELECT 'SVC-0321',
       'Urethrostomy (Dog/Cat) 2',
       '6-10kg',
       'SURGERIES: (Procedure Only)',
       '8000'
UNION ALL
SELECT 'SVC-0322',
       'Urethrostomy (Dog/Cat) 3',
       '11-15kg',
       'SURGERIES: (Procedure Only)',
       '9000'
UNION ALL
SELECT 'SVC-0323',
       'Urethrostomy (Dog/Cat) 4',
       '16-20kg',
       'SURGERIES: (Procedure Only)',
       '10000'
UNION ALL
SELECT 'SVC-0324',
       'Urethrostomy (Dog/Cat) 5',
       '21-25kg',
       'SURGERIES: (Procedure Only)',
       '11000'
UNION ALL
SELECT 'SVC-0325',
       'Urethrostomy (Dog/Cat) 6',
       '26-30 kg',
       'SURGERIES: (Procedure Only)',
       '12000'
UNION ALL
SELECT 'SVC-0326',
       'Urethrostomy (Dog/Cat) 7',
       '31-35 kg',
       'SURGERIES: (Procedure Only)',
       '13000'
UNION ALL
SELECT 'SVC-0327',
       'Advocate  Cat Small- Up to 4kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '450'
UNION ALL
SELECT 'SVC-0328',
       'Advocate  Dog Small- Up to 4kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '400'
UNION ALL
SELECT 'SVC-0329',
       'Advocate Dog Extra > 25kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '700'
UNION ALL
SELECT 'SVC-0330',
       'Advocate Dog Large 10 to 25',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '600'
UNION ALL
SELECT 'SVC-0331',
       'Advocate Dog Medium- 4-10kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '450'
UNION ALL
SELECT 'SVC-0332',
       'Bravecto 10 to 20kg (Medium)',
       'chewable',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '1500'
UNION ALL
SELECT 'SVC-0333',
       'Bravecto 16 to 20 (Large)',
       'chewable',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '1500'
UNION ALL
SELECT 'SVC-0334',
       'Bravecto 2 to 4.5kg (Extra Small)',
       'chewable',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '1500'
UNION ALL
SELECT 'SVC-0335',
       'Bravecto 4.5-10kg (Small)',
       'chewable',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '1500'
UNION ALL
SELECT 'SVC-0336',
       'Broadline for Cats <2.5 kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '550'
UNION ALL
SELECT 'SVC-0337',
       'Broadline for Cats 2.5 - 7.5 kg',
       'pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '570'
UNION ALL
SELECT 'SVC-0338',
       'Nexgard Large 10-25kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '565'
UNION ALL
SELECT 'SVC-0339',
       'Nexgard Medium 4-10kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '540'
UNION ALL
SELECT 'SVC-0340',
       'Nexgard Small 2-4kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '510'
UNION ALL
SELECT 'SVC-0341',
       'Nexgard Spectra Large 15-30kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '800'
UNION ALL
SELECT 'SVC-0342',
       'Nexgard Spectra Medium 7.5 to 15kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '740'
UNION ALL
SELECT 'SVC-0343',
       'Nexgard Spectra Small 3.5-7.5 kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '670'
UNION ALL
SELECT 'SVC-0344',
       'Nexgard Spectra XL 30-60kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '890'
UNION ALL
SELECT 'SVC-0345',
       'Nexgard Spectra XS 2-3.5kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '610'
UNION ALL
SELECT 'SVC-0346',
       'Nexgard XL 25-50kg',
       'per chewable tablet',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '600'
UNION ALL
SELECT 'SVC-0347',
       'Tick and Flea Defense  Medium',
       '23-44lbs pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '500'
UNION ALL
SELECT 'SVC-0348',
       'Tick and Flea Defense  Small',
       '< 22 lbs pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '450'
UNION ALL
SELECT 'SVC-0349',
       'Tick and Flea Defense Large',
       '45-88lbs pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '550'
UNION ALL
SELECT 'SVC-0350',
       'Tick and Flea Defense XL',
       '89-132 lbs pipette',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '600'
UNION ALL
SELECT 'SVC-0351',
       'Jojoba Tick and Flea Spray',
       '250 ml bottle',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '450'
UNION ALL
SELECT 'SVC-0352',
       'Venoma Spray',
       '120 ml bottle',
       'TICK AND FLEA PREVENTIVES/ MITE TREATMENT AND HEARTWORM PREVENTIVES',
       '395'
UNION ALL
SELECT 'SVC-0353',
       '5in1 (DHLPPi)',
       'Dose',
       'VACCINATION',
       '400'
UNION ALL
SELECT 'SVC-0354',
       '2in1 (Parvo/Distemper)',
       'Dose',
       'VACCINATION',
       '350'
UNION ALL
SELECT 'SVC-0355',
       'Rabies',
       'Dose',
       'VACCINATION',
       '250'
UNION ALL
SELECT 'SVC-0356',
       'Kennel Cough (Bronchicine)',
       'Dose',
       'VACCINATION',
       '450'
UNION ALL
SELECT 'SVC-0357',
       '3in1 (Cat/Kitten Vaccine)',
       'Dose',
       'VACCINATION',
       '950'
UNION ALL
SELECT 'SVC-0358',
       'Consultation',
       'Professional Fee/ First visit Fee',
       'WELLNESS',
       '300'
UNION ALL
SELECT 'SVC-0359',
       'Emergency Fee',
       'Additional Charges for check up without appointment',
       'WELLNESS',
       '1000'
UNION ALL
SELECT 'SVC-0360',
       'House Call',
       'Home Services Charge',
       'WELLNESS',
       '500'
UNION ALL
SELECT 'SVC-0361',
       'Health Certificate',
       'Travel Documentation',
       'WELLNESS',
       '350'

GO 
