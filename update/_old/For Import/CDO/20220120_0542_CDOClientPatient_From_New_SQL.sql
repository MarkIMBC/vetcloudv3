DECLARE @CDO_ID_Company INT = 241
DECLARE @ClientName VARCHAR(MAX) = ''
DECLARE @PetName VARCHAR(MAX) = ''
DECLARE @EmailAddress VARCHAR(MAX) = ''
DECLARE @MobilePhone VARCHAR(MAX) = ''
DECLARE @ID_Client INT = 0
DECLARE @ID_Patient INT = 0
DECLARE @_____ClientName VARCHAR(MAX) = ''
DECLARE @_____Patient VARCHAR(MAX) = ''
DECLARE db_cursor CURSOR FOR
  SELECT DISTINCT [Client Name],
                  [Pet Name],
                  MAX(  [E-mail Address]),
                 MAX( [Mobile Phone])
  FROM   _importCDOClientPatient_New
  WHERE  LEN(ISNULL([Client Name], '')) > 0
         and [Client Name] NOT IN ( '`' ) AND LEN(ISNULL([Pet Name], '')) > 0
	GROUP BY
	 [Client Name],
                  [Pet Name]
		 ORDER BY [Client Name]

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ClientName, @PetName, @EmailAddress, @MobilePhone

WHILE @@FETCH_STATUS = 0
  BEGIN
      if( @_____ClientName <> @ClientName )
        BEGIN
            INSERT dbo.tClient
                   (Name,
                    IsActive,
                    ID_Company,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ContactNumber)
            SELECT @ClientName,
                   1,
                   @CDO_ID_Company,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @MobilePhone

            SET @ID_Client = @@IDENTITY
            SET @_____ClientName = @ClientName
        END

      if( @_____Patient <> @PetName
          AND LEN(@PetName) > 0 )
        BEGIN
            DECLARE @Pets TABLE
              (
                 Name VARCHAR(MAX)
              )

            INSERT @Pets
            SELECT Part
            FROM   [dbo].[fGetSplitString] (@PetName, ',')

			INSERT tPatient
                   (ID_Company,
				    ID_Client,
                    Name,
                    IsActive,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy)
            SELECT @CDO_ID_Company,
                   @ID_Client,
                   Name,
				   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1
				   FROM @Pets p 
				   WHERE Name NOT IN (
					   SELECT Name FROM tPatient WHERE ID_Client = @ID_Client
				   )

            SET @_____Patient = @PetName
			
			DELETE FROM @Pets
        END

      FETCH NEXT FROM db_cursor INTO @ClientName, @PetName, @EmailAddress, @MobilePhone
  END

CLOSE db_cursor

DEALLOCATE db_cursor 
