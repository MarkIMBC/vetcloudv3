DECLARE @CDO_ID_Company INT = 241
DECLARE @AssetID INT
DECLARE @ClientName VARCHAR(MAX) = ''
DECLARE @Address VARCHAR(MAX) = ''
DECLARE @Office VARCHAR(MAX) = ''
DECLARE @PetID VARCHAR(MAX) = ''
DECLARE @ContactNo VARCHAR(MAX) = ''
DECLARE @TelNo VARCHAR(MAX) = ''
DECLARE @Born VARCHAR(MAX) = ''
DECLARE @Weight VARCHAR(MAX) = ''
DECLARE @Marking VARCHAR(MAX) = ''
DECLARE @Breed VARCHAR(MAX) = ''
DECLARE @_____ClientName VARCHAR(MAX) = ''
DECLARE @_____Patient VARCHAR(MAX) = ''
DECLARE @ID_Client INT = 0
DECLARE @ID_Patient INT = 0
DECLARE db_cursor CURSOR FOR
  SELECT [Asset ID],
         [Client Name],
         [Address],
         [Office],
         [PetID],
         [Contact No],
         [Tel No],
         [Born],
         [Weight],
         [Marking],
         [Breed]
  FROM   _importCDOClientPatient_Old
  WHERE  LEN(ISNULL([Client Name], '')) > 0
         and [Client Name] NOT IN ( '-', '`' )
  ORDER  BY [Client Name],
            [Asset ID]

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @AssetID,
                               @ClientName,
                               @Address,
                               @Office,
                               @PetID,
                               @ContactNo,
                               @TelNo,
                               @Born,
                               @Weight,
                               @Marking,
                               @Breed

WHILE @@FETCH_STATUS = 0
  BEGIN
      if( @_____ClientName <> @ClientName )
        BEGIN
            INSERT dbo.tClient
                   (Name,
                    IsActive,
                    ID_Company,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ContactNumber,
                    Address)
            SELECT @ClientName,
                   1,
                   @CDO_ID_Company,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ContactNo,
                   @Address

            SET @ID_Client = @@IDENTITY
            SET @_____ClientName = @ClientName
        END

      if( @_____Patient <> @PetID AND LEN(@PetID) > 0 )
        BEGIN
            IF( @Born = '1900-01-01 00:00:00.000' )
              SET @Born = NULL

            INSERT tPatient
                   (ID_Company,
                    ID_Client,
                    CustomCode,
                    Name,
                    DateBirth,
                    Species,
                    Comment,
                    IsActive,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy)
            SELECT @CDO_ID_Company,
                   @ID_Client,
                   @AssetID,
                   @PetID,
                   @Born,
                   @Breed,
                   CASE
                     WHEN @Weight IS NOT NULL THEN 'Weight: ' + @Weight
                     ELSE ''
                   END
                   + CHAR(13)
                   + CASE
                       WHEN @Marking IS NOT NULL THEN 'Marking: ' + @Marking
                       ELSE ''
                     END,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1

            SET @ID_Patient = @@IDENTITY
            SET @_____Patient = @PetID
        END

      FETCH NEXT FROM db_cursor INTO @AssetID,
                                     @ClientName,
                                     @Address,
                                     @Office,
                                     @PetID,
                                     @ContactNo,
                                     @TelNo,
                                     @Born,
                                     @Weight,
                                     @Marking,
                                     @Breed
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT ID_Client, Name_Client, CustomCode, ID, 
       Name
FROM   vPatient
WHERE  ID_Company = 241
ORDER  BY Name_Client 

