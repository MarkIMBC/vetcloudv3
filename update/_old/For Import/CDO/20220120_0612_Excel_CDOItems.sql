	
GO
/****** Object:  Table [dbo].[CDO_Items]    Script Date: 19/01/2022 3:37:45 pm ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CDO_Items]') AND type in (N'U'))
DROP TABLE [dbo].[CDO_Items]
GO
/****** Object:  Table [dbo].[CDO_Items]    Script Date: 19/01/2022 3:37:45 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CDO_Items](
	[Item] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[BuyingPrice] [float] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC1 -- ADJUSTABLE COLLAR', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC10 -- COLLAR BELL (SMALL)', N'ACCESSORIES', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC101 -- SOCKS', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC102 -- SPORTS BALL W/ ROPE TOY', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC103 -- SQUEAK-BALL TOY', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC104 -- SQUEK-FLAT BONE TOY', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC106 -- TEETHER 8 ROPE TOY', N'ACCESSORIES', 65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC107 -- TH-DOG BED SMALL', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC108 -- TRAVEL CRATES (EXTRA LARGE) WITH WHEELS', N'ACCESSORIES', 3290)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC109 -- TRAVEL CRATES (EXTRA SMALL)', N'ACCESSORIES', 390)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC11 -- COLLAR RETRO', N'ACCESSORIES', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC111 -- TRAVEL CRATES (SMALL)', N'ACCESSORIES', 720)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC112 -- TRAVEL CRATES( MEDIUM)', N'ACCESSORIES', 1260)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC113 -- Twisted Cord Short WH-A (TCS)', N'ACCESSORIES', 128)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC114 -- TWO COLOR LEASH 18mm WH-A (NPR-1848X)', N'ACCESSORIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC115 -- FOLDABLE CAGE (EXTRA-LARGE)', N'ACCESSORIES', 2300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC116 -- WATER FEEDER ADAPTOR (SINGLE NOZZLE)', N'ACCESSORIES', 118)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC117 -- WATER FEEDER FOR TRAVELLING', N'ACCESSORIES', 165)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC118 -- WATER FEEDER W/ BOTTLE', N'ACCESSORIES', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC119 -- DOG DESIGN FOOD & WATER FEEDER W/ POLE', N'ACCESSORIES', 245)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC12 -- COLLAR WH-A (FC 2.0)', N'ACCESSORIES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC120 -- MICHIKO PET BED LARGE', N'ACCESSORIES', 440)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC121 -- MICHIKO PET BED SMALL', N'ACCESSORIES', 330)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC122 -- PRINTED COLLAR SMALL', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC123 -- PRINTED COLLAR MEDIUM', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC124 -- PRINTED COLLAR W/ LEASH SMALL', N'ACCESSORIES', 135)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC125 -- PRINTED COLLAR W/ LEASH MEDIUM', N'ACCESSORIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC126 -- PET DIGITAL SCALE', N'ACCESSORIES', 1300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC128 -- THIN LEASH', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC129 -- COLLAR SFCLA', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC13 -- COOLING MAT', N'ACCESSORIES', 200)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC14 -- Dog Leash (Long) WH-A (DA1096)', N'ACCESSORIES', 180)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC15 -- Dog Leash (Short) WH-A (DA1095)', N'ACCESSORIES', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC16 -- DOG MAT', N'ACCESSORIES', 180)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC17 -- DOUBLE SIDED COMB', N'ACCESSORIES', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC18 -- E-COLLAR #1', N'ACCESSORIES', 130)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC2 -- Aqua Dog (Water Bottle)', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC25 -- FEMALE PET DRESS', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC26 -- FOLDABLE CAGE (LARGE)', N'ACCESSORIES', 1400)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC27 -- FOLDABLE CAGE (MEDIUM)', N'ACCESSORIES', 1050)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC28 -- FOLDABLE CAGE (SMALL)', N'ACCESSORIES', 750)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC29 -- FOLDABLE CAGE (EXTRA-SMALL)', N'ACCESSORIES', 500)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC3 -- AQUA NOZZLE', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC30 -- HAIR CLIPS', N'ACCESSORIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC31 -- HAIRCLIP CAP', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC32 -- HARNESS (X-LARGE)', N'ACCESSORIES', 95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC3221 -- MICHIKO COLLAR W/ LEASH', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC33 -- Harness Large ( WH-A)', N'ACCESSORIES', 268)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC34 -- Harness XXL (WH-A)', N'ACCESSORIES', 450)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC36 -- LEASH (GENERIC)', N'ACCESSORIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC37 -- LEASH TCL', N'ACCESSORIES', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC4 -- BED', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC41 -- MICHIKO COMB TANGLE AWAY COMB LARGE', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC42 -- MICHIKO NAIL CLIPPER', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC43 -- MICHIKO NAIL CLIPPER MEDIUM', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC44 -- MICHIKO NAIL CLIPPER SMALL', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC45 -- MICHIKO PACI CHEW TOY (MEDIUM)', N'ACCESSORIES', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC46 -- MICHIKO PACI CHEW TOY (SMALL)', N'ACCESSORIES', 65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC47 -- MICHIKO TOOTHBRUSH 2PCS', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC48 -- MICHIKO TOOTHBRUSH 3PCS', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC49 -- NLWH BODY HARNESS SMALL', N'ACCESSORIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC50 -- NYLON COLLAR (L)', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC51 -- NYLON COLLAR (M)', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC52 -- NYLON COLLAR (S)', N'ACCESSORIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC53 -- NYLON COLLAR (XL)', N'ACCESSORIES', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC54 -- NYLON Collar + Lead set (Small)', N'ACCESSORIES', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC55 -- NYLON Collar + Leadset (Large)', N'ACCESSORIES', 145)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC56 -- NYLON COLLAR + LEADSET (M) 15MM', N'ACCESSORIES', 105)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC58 -- NYLON FABRIC MUZZLE SIZE 0', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC59 -- NYLON FABRIC MUZZLE SIZE 2', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC6 -- Cat Collar', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC61 -- NYLON FABRIC MUZZLE SIZE 4', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC62 -- NYLON Lead + Harness SET (Large)', N'ACCESSORIES', 210)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC63 -- NYLON Lead + Harness SET (Med)', N'ACCESSORIES', 165)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC64 -- NYLON Lead + Harness SET (Small) 10MM', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC65 -- NYLON Lead + Harness SET (X-Large)', N'ACCESSORIES', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC67 -- PET BED PAW VELVET (Small)', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC68 -- PET CARRIER RED', N'ACCESSORIES', 145)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC69 -- PET COSTUME', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC7 -- CO 1.5 Collar ( WH-A)', N'ACCESSORIES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC70 -- PET HARD CARRIER (LARGE)', N'ACCESSORIES', 650)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC71 -- PET HARD CARRIER (MEDIUM)', N'ACCESSORIES', 550)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC72 -- PET HARD CARRIER (SMALL)', N'ACCESSORIES', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC73 -- PET NANNY', N'ACCESSORIES', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC75 -- PL BELLY BOWL CF- 103 (L)', N'ACCESSORIES', 95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC78 -- PLANTI-SPILL BOWL CF-105', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC79 -- PLASTIC DOG BOWL 15CM', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC8 -- CO2 Collar (WH-A)', N'ACCESSORIES', 258)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC82 -- PVC MUZZLE (SIZE 1)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC83 -- PVC MUZZLE (SIZE 2)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC84 -- PVC MUZZLE (SIZE 3)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC85 -- PVC MUZZLE (SIZE 4)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC86 -- PVC MUZZLE (SIZE 5)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC87 -- PVC MUZZLE (SIZE 6)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC88 -- Regular Non Skid Bowl (16oz)', N'ACCESSORIES', 48.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC89 -- Regular Non Skid Bowl (32 Oz)', N'ACCESSORIES', 82)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC9 -- COLLAR BELL (MEDIUM)', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC90 -- Regular Non Skid BOwl (64oz)', N'ACCESSORIES', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC91 -- Regular Non Skid Bowl (8 Oz)', N'ACCESSORIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC92 -- Regular Non Skid Bowl (96 Oz)', N'ACCESSORIES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC93 -- Regular Non Skid Bowl( 24Oz)', N'ACCESSORIES', 82)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC95 -- RUBBER BALL W/ ROPE TOY', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC96 -- SANTA HAT', N'ACCESSORIES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC97 -- SC LONG ORDINARY SMALL', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC98 -- SILICONE MUZZLE (Large)', N'ACCESSORIES', 285)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC99 -- SILICONE MUZZLE (Medium)', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS7 -- SALBUTAMOL SULFATE + IPRATROPIUM BROMIDE', N'AMPULES', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR1 -- ADVOCATE cat 4kg or less', N'ANTI PARASITICS', 296.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR10 -- BROADLINE for cats 2.5 kg spot on', N'ANTI PARASITICS', 321.42)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR11 -- CANIMAX BOX (4TABS)', N'ANTI PARASITICS', 614)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR12 -- CANIMAX PER TAB', N'ANTI PARASITICS', 204.66)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR13 -- EARMITICIDE BOTTLE 20ML', N'ANTI PARASITICS', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR15 -- ENDOGARD PER TAB', N'ANTI PARASITICS', 135.33)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR16 -- FLEA & TICK SPOT ON FOR DOGS  GREEN(10KG-20KG)', N'ANTI PARASITICS', 249)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR17 -- FLEA & TICK SPOT ON FOR DOGS VIOLET(25KG N UP)', N'ANTI PARASITICS', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR18 -- FLEA + TICK DEFENSE SPRAY FOR DOGS AND CATS 100ML', N'ANTI PARASITICS', 342)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR2 -- ADVOCATE DOG   4KG  or less (0.4ML  per pippette)', N'ANTI PARASITICS', 753.96)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR20 -- MILBEMAX 12.5mg', N'ANTI PARASITICS', 117)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR23 -- NEXGARD 2.4KG per tab (ORANGE)', N'ANTI PARASITICS', 301.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR24 -- NEXGARD 25-50KG tab (RED)', N'ANTI PARASITICS', 357.91)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR26 -- NEXGARD SPECTRA 15-30KG 3TABS (VIOLET)', N'ANTI PARASITICS', 593.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR27 -- NEXGARD SPECTRA 2-3.5KG tab (ORANGE)', N'ANTI PARASITICS', 453.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR29 -- NEXGARD SPECTRA 30-60KG  (RED)', N'ANTI PARASITICS', 718.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR3 -- ADVOCATE DOG 4-10kg  (1.0ml per pipette)3x1.0ML', N'ANTI PARASITICS', 305.91)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR37 -- VENOMA 120ML', N'ANTI PARASITICS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR38 -- VERMIFUGE 60ml (dewormer syrup)', N'ANTI PARASITICS', 186)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR4 -- ADVOCATE DOG over 25kg', N'ANTI PARASITICS', 1323)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR40 -- SIMPARICA 11.1-22.0 lbs (brown)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR41 -- SIMPARICA 22.1-44.0 lbs (sky blue)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR42 -- SIMPARICA 44.1-88.0 lbs (Green)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR43 -- EFFIPRO SPRAY', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR44 -- SIMPARICA TRIO VIOLET', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR45 -- SIMAPRICA TRIO BROWN', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR46 -- SIMPARICA TRIO BLUE', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR47 -- SIMPARICA TRIO GREEN', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR48 -- SIMPARICA TRIO BROWN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR5 -- ADVOCATE DOG 10kg-25kg', N'ANTI PARASITICS', 1071)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR6 -- BRAVECTO (10-20kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR7 -- BRAVECTO (20-40kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR8 -- BRAVECTO (4.5-10kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR9 -- BROADLINE for cats  2.5-7.5KG spot on', N'ANTI PARASITICS', 361.6)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0034 -- PLAISIR small', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0038 -- SPECIAL CAT CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF1 -- BEEFPRO ADULT BAG (22KG)', N'DOG & CAT FOOD', 1960)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF11 -- NUTRI CHUNKS', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF12 -- PEDIGREE ADULT BAG', N'DOG & CAT FOOD', 1951.9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF15 -- PEDIGREE PUPPY BAG', N'DOG & CAT FOOD', 1725.65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF16 -- PEDIGREE PUPPY PER KILO', N'DOG & CAT FOOD', 116)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF18 -- PRINCESS CAT (1KG)', N'DOG & CAT FOOD', 99)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF187 -- BEEF PRO ADULT', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF2 -- BEEFPRO ADULT PER KILO', N'DOG & CAT FOOD', 83)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF21 -- TEMPTATION CHICKEN', N'DOG & CAT FOOD', 85.15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF22 -- TEMPTATION TUNA', N'DOG & CAT FOOD', 85.15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF23 -- VALUEMEAL  ADULT BAG', N'DOG & CAT FOOD', 1700)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF25 -- VITALITY  PUPPY BAG', N'DOG & CAT FOOD', 2030)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF28 -- WHISKAS 7KG', N'DOG & CAT FOOD', 874.65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF29 -- NUTRI CHUNKS MAINTENANCE 1 KILO', N'FLUIDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF30 -- TOP BREED PUPPY 1KG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF33 -- ALA CARTE SALMON/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF35 -- ALA CARTE LAMB/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF36 -- PEDIGREE CAN small', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF37 -- WHISKAS CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF38 -- SPECIAL DOG/ SACK', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF41 -- MAXI PUPPY/SACK', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF42 -- MAXI ADULT/SACK', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF43 -- VALUEMEAL CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF44 -- PLAISIR BIG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF45 -- HOLISTIC PER KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF46 -- VITALITY CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF47 -- SPECIAL CAT PER KG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF48 -- SPECIAL DOG PUPPY/KG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF50 -- BINGO PUPPY/KL', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF51 -- VITALITY CLASSIC/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF53 -- VALUEMEAL CLASSIC/KL', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF54 -- VALUEMEAL PUPPY', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF55 -- LAMB & RICE', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF57 -- JERKY', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF58 -- BINGO ADULT/KL', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF60 -- PEDIGREE CAN MEDIUM', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF63 -- DIANA CAT FOOD', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF756 -- RECOVERY FOOD CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF9 -- HEROI PUPPY PER KILO', N'DOG & CAT FOOD', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED15 -- OXYTOCIN', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED19 -- TRANQUILIZER -CALMIVET-ACEPROMAZINE MALEATE PER INJECTION 10KG AND BELOW', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED2 -- ATROSITE PER INJECTION UP TO10KG', N'INJECTIBLE MEDS', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED20 -- TRANQUILIZER-CALMIVET-ACEPROMAZINE MALEATE PER INJECTION 10KG-15KG', N'INJECTIBLE MEDS', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED21 -- ACETYLCYSTEINE', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED22 -- CONVENIA (antibiotic) ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED24 -- TERBUTALINE INJ.', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED3 -- CA + (Calcium) per ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED6 -- DCM(Dextrose,magnesiumgluconate and calcium borogluconate) injection per ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED9 -- Euthanasia meds', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB21 -- PROGESTERONE TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB47 -- CHEM 15', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB49 -- ALKP TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB50 -- ALT TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS1 -- ACETYLCYTEINE SACHET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS10 -- ANTI ITCH SPRAY 118ML', N'MEDICINES', 365)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS103 -- URETEX', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS106 -- LC-DOX SYRUP 60ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS109 -- LC-VIT OB BOTTLE', N'MEDICINES', 255.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS11 -- APOQUEIL 16mg', N'MEDICINES', 127)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS115 -- CYSTAID PLUS', N'MEDICINES', 17.95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS116 -- CELLMANAX', N'MEDICINES', 430)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS118 -- BENZYL PENICILLIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS119 -- VIBRAVET', N'MEDICINES', 430.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS120 -- CAZITEL PLUS', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS122 -- SCOURVET 60ML', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS124 -- BIOCURE TAB (doxycycline hci)', N'MEDICINES', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS125 -- VET TREAT (enrofloxacin)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS126 -- ENROFLOX SYRUP', N'MEDICINES', 356)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS127 -- K9 CALCIUM CHEWS BOTTLE', N'MEDICINES', 610.95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS128 -- WORMRID TAB', N'MEDICINES', 49.56)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS129 -- IMMUNE HEALTH PER TAB', N'MEDICINES', 6.35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS13 -- ATOPEX 25 tab', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS130 -- VETPRAMIDE TAB', N'MEDICINES', 5.1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS131 -- ANTI COPROPHAGIC BOTTLE', N'MEDICINES', 712)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS133 -- NUTRICH BOTTLE 60tabs', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS135 -- EYE SHIELD', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS138 -- MICROFILARICIDE MEDS', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS139 -- BLADDER CONTROL PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS14 -- ATOPEX 50 TAB', N'MEDICINES', 152.76724)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS140 -- HOMEOPATHIC DROPS/ BOTTLE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS141 -- HYCLENS SKIN CLEANSER', N'MEDICINES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS142 -- HONELLE WOUND GEL', N'MEDICINES', 389)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS145 -- ADULTICIDE FOR (25KG-UP)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS147 -- PIMOBENDAN TAB 1.25mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS15 -- AURIZON 10ML(EAR DROPS)', N'MEDICINES', 534.9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS150 -- EMERFLOX', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS152 -- K9 CALCIUM TABLET/TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS153 -- RENAL N POWDER', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS154 -- CEPHALEXIN SYRUP (CFLEX) 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS155 -- PIMOBENDAN 5mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS156 -- K9 DOXY TAB/PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS16 -- AZITHROMYCIN SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS160 -- RENAL N /PER 1 SCOOP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS161 -- RENAL N/ PER SMALL SCOOP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS17 -- BARFY', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS1763 -- ANTI COPROPHAGIC', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS178 -- CYSTOCURE FORTE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS18 -- Black Armour', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS2 -- AIR-LAX', N'MEDICINES', 460)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS20 -- CANGLOB D Per ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS21 -- CANGLOB D VIAL', N'MEDICINES', 734.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS24 -- CARPROFEN 50MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS25 -- CEFALEXIN', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS26 -- CHARCOAL VET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS29 -- Co-amox 125MG SYRUP', N'MEDICINES', 131)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS3 -- ALLERMAX 4MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS32 -- DEXAMETHASONE TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS36 -- EAR CLEANSE 118ML', N'MEDICINES', 311)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS37 -- EASOTIC EAR DROPS', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS38 -- Enalapril', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS39 -- ENROFLOX BOX (50TABS)', N'MEDICINES', 563.33333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS4 -- ALLERMYL 200ML', N'MEDICINES', 507.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS41 -- EPIOTIC 125ML', N'MEDICINES', 380.31)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS42 -- ERGON 125MCG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS43 -- ERYTHROPOIETIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS44 -- EXCELVIT', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS45 -- EYE RINSE 118ML', N'MEDICINES', 333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS47 -- FLEET ENEMA', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS48 -- FUROSEMIDE 20MG TAB', N'MEDICINES', 4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS5 -- ALUMINUM  HYDROXIDE SYRUP 60ML', N'MEDICINES', 16)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS50 -- HYPACE 20MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS52 -- ITRACONAZOLE 100mg', N'MEDICINES', 25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS53 -- ITRACONAZOLE 25mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS54 -- CLAVAMOX 250mg tab(Co amoxiclav)', N'MEDICINES', 42)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS55 -- K9 EAR DROPS 60ML', N'MEDICINES', 283)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS57 -- KETOCONAZOLE PER TAB(200MG)', N'MEDICINES', 23)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS58 -- LAKTRAZINE BOTTLE', N'MEDICINES', 165)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS59 -- LAKTRAZINE TAB', N'MEDICINES', 8.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS61 -- LC DOX BOTTLE (Doxycycline 100mg tab-100pcs)', N'MEDICINES', 1222)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS62 -- LIVERATOR', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS63 -- LPS DR.', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS64 -- Lysine', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS66 -- MELOXICAM SYRUP PER ML', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS67 -- MENADIONE 10MG tab', N'MEDICINES', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS68 -- METHYLERGOMETRINE 125MCG TAB', N'MEDICINES', 3)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS69 -- METOCLOPRAMIDE SYRUP', N'MEDICINES', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS7 -- AMINOLEBAN', N'MEDICINES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS70 -- Metoclopramide Tab', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS72 -- MICONATE SHAMPOO', N'MEDICINES', 285)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS73 -- MICROSCOT', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS75 -- OMEPRAZOLE 20MG', N'MEDICINES', 13)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS76 -- ORIDERMYL', N'MEDICINES', 530.90909)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS78 -- PEN-G', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS79 -- PET REBOOST', N'MEDICINES', 214.28571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS8 -- Amitraz Vial', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS82 -- PIRANTEL DEWORMER BOTTLE 30TABS', N'MEDICINES', 885)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS83 -- PIRANTEL DEWORMER PER TAB', N'MEDICINES', 29.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS88 -- PROXANTEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS89 -- PULMOQUIN', N'MEDICINES', 93.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS9 -- AMOXICILLIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS90 -- PUPPY BOOST', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS92 -- RECOVERY PASTE LYSINE', N'MEDICINES', 505)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS94 -- SEBOLYTIC shampoo 200ML', N'MEDICINES', 499.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS95 -- SMP 250', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS97 -- TERBUTALINE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS99 -- Tobrason', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'OTH1291 -- MORINGACURE', N'OTHERS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC27 -- Nebulization with meds.', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET10 -- ROYAL CANIN HEPATIC 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET13 -- ROYAL CANIN RENAL can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET14 -- ROYAL CANIN S/O can', N'PRESCRIPTION DIETS', 162.36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET15 -- ROYAL CANIN SATIETY DIET  2KG', N'PRESCRIPTION DIETS', 951)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET17 -- ROYAL CANINE S/O 2kg', N'PRESCRIPTION DIETS', 1159)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET18 -- Singen Kidney Diet VKD1 (canine)', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET19 -- VET SOLUTION ANTI- OBESITY 2KG(MONGE)-DRY PELLETS', N'PRESCRIPTION DIETS', 920)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET20 -- VET SOLUTION HEPATIC 12KG-DRY PELLETS', N'PRESCRIPTION DIETS', 4800)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET22 -- Vet Solution Renal per 1kg', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET23 -- ROYAL CANIN KITTEN 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET24 -- ROYAL CANIN INSTINCTIVE 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET25 -- ROYAL CANIN PERSIAN 2KG', N'PRESCRIPTION DIETS', 1090.77)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET26 -- ROYAL CANIN PERSIAN KITTEN 2kg', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET28 -- ROYAL CANIN FELINE S/O 2KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET33 -- DIAMOND CARE URINARY SUPPORT/ KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET4 -- MONGEE HEPATIC 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET5 -- Mongee Urinary Struvite', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET6 -- RENAL 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET9 -- ROYAL CANIN HAIR & SKIN FOR CATS 2KG', N'PRESCRIPTION DIETS', 850)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SED2 -- ZOLETIL BOTTLE 5ML', N'SEDATIVE/TRANQUILIZER', 1100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER1 -- AVIAN HEALTH CERTIFICATE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER11 -- DAILY CONFINEMENT FEE MOTHER AND PUPS/KITTENS', N'SERVICES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER1324 -- 3-WAY TEST', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER1654 -- BLOOD CHEM', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER28 -- MICROCHIPPING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER34 -- PF,SURGERY AND WOUND SUTURING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER35 -- SCABICIDE TX  0-3kg', N'SERVICES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV39 -- EMERGENCY FEE/DOCTORS FEE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV40 -- PF + WHELPING ASSISTANCE & EXTRACTION OF PUPS', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL10 -- CARD', N'SUPPLIES', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL12 -- CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL13 -- CHROMIC CATGUT 0', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL14 -- CHROMIC CATGUT 1', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL16 -- CHROMIC CATGUT 3/0', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL18 -- COVER SLIP', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL20 -- DISPOSABLE DOG CATHETER', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL21 -- DISPOSABLE INFUSION SET', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL24 -- ECO-BAG', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL26 -- ELECTRIC TOOTHBRUSH', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL27 -- FEEDING TUBE', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL28 -- FEEDING TUBE AND URETHRAL CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL3 -- BETADINE SPRAY', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL31 -- HEMO-NATE SYRINGE FILTER', N'SUPPLIES', 1000)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL32 -- Indwelling foley Catheter', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL33 -- INFUSION SET', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL35 -- MICROTAINER EDTA', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL36 -- NASAL OXYGEN TUBE INSERTION,SUPPLIES AND LOCAL ANESTHETICS', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL37 -- NEBULIZER KIT', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL38 -- NOVOSYN (90CM)', N'SUPPLIES', 270.45455)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL4 -- BIO-FLON CANNULA', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL40 -- OXYGEN CANINE MASK (S)', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL41 -- OXYGEN FELINE MASK (L)', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL42 -- OXYGEN FELINE MASK (M)', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL43 -- OXYGEN FELINE MASK (S)', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL44 -- PDO SOTURE 1-0', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL45 -- PDO SOTURE 2-0', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL46 -- PEN LIGHT', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL47 -- PGA', N'SUPPLIES', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL48 -- POLYDIOXANONE (24MM)', N'SUPPLIES', 200)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL50 -- RED TOP 5ml', N'SUPPLIES', 400)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL51 -- SILK (non absorbable suture)', N'SUPPLIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL53 -- STERILE GLOVES', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL55 -- SUCTION CATHETER FOR SINGLE USE', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL56 -- SUCTION CATHETER W/ FINGERTIP CONTROL', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL57 -- SUCTION CATHETER WITH CONTROL', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL58 -- SURGICAL BLADES', N'SUPPLIES', 9.25926)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL6 -- BLOOD BAG', N'SUPPLIES', 200)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL60 -- SURGICAL HAIR NET', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL62 -- SURGICAL STAPLER', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL65 -- URINARY CANINE  CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL66 -- VET CASTING TAPE & SUPPLIES', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL68 -- VIOLET TUBES', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL7 -- BLOOD TRANSFUSION SET', N'SUPPLIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL8 -- BONE WIRE', N'SUPPLIES', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL9 -- BUDZ VET SUTURES CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SURG2 -- Cesarean Section W/ Panhysterectomy (Dog)', N'SURGERY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST11 -- IDEXX LEPTOSPIROSIS TEST', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST15 -- PREGNANCY TEST', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST17 -- CDV/CPV CONFIRMATORY TEST', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST2 -- Anaplasma-Ehrlichiosis-Babesia- Heart Worm Test kit', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST9 -- Ehrlichia-Anaplasma-Babesia-Heartworm Test', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL1 -- CAT LITTER PAN W/ SCOOPER', N'TOILETRIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL11 -- DONO DIAPER EXTRA-SMALL PER PC', N'TOILETRIES', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL14 -- HUSHPET DIAPER LARGE PER PC', N'TOILETRIES', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL15 -- HUSHPET DIAPER MEDIUM PER PC', N'TOILETRIES', 12)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL16 -- HUSHPET DIAPER MEDIUM (12PCS/PACK)', N'TOILETRIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL18 -- HUSHPET DIAPER SMALL PER PC', N'TOILETRIES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL19 -- PAPI AMITRAZ 250ML shampoo', N'TOILETRIES', 128)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL20 -- PAPI AMITRAZ 500ML', N'TOILETRIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL21 -- PAPI AMITRAZ SOAP', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL213 -- CATSAN SCENTED', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL22 -- PAPI ANTI TICK & FLEA SOAP', N'TOILETRIES', 66)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL24 -- PAPI GROOM & BLOOM (BLUE) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL25 -- PAPI GROOM & BlOOM (GREEN) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL26 -- PAPI GROOM & BLOOM (RED) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL27 -- PAPI HAIR GROWER SOAP', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL28 -- PAPI MADRE DE CACAO SOAP', N'TOILETRIES', 71.81818)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL29 -- PAPI PET POWDER 100G', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL30 -- PAPI PURE ABACA EXTRACT 115G', N'TOILETRIES', 79)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL33 -- PET SOCIETY HERBAL SOAP 60G', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL34 -- PET SOCIETY SHAMPOO 450ML', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL35 -- PET SPLASH ANTI TIQUES 250ML', N'TOILETRIES', 246)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL38 -- PET SPLASH COTON FRAIS 250ML', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL39 -- PETSENTIALS PET COLOGNE 100ml', N'TOILETRIES', 125.48)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL4 -- CATSAN ULTRA ODOR CONTROL 5li (Pink)', N'TOILETRIES', 180.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL41 -- PETSENTIALS STAIN & ODOR REMOVER 250ml', N'TOILETRIES', 135.61)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL42 -- PLAY PETS LONG COAT SHAMPOO 250ML', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL44 -- ST. ROCHE BASIC SHAMPOO 1000ml', N'TOILETRIES', 270)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL45 -- ST. ROCHE DETANGLING SPRAY 318ml', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL46 -- ST. ROCHE SHAMPOO 1050ml', N'TOILETRIES', 405)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL47 -- ST. ROCHE SHAMPOO 628ml', N'TOILETRIES', 261)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL48 -- DIAPER EXTRA-LARGE PER PC', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL49 -- PETSENTIALS SHAMPOO ANTI TICK & FLEA 250ml', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL5 -- DENTAL HYGIENE KIT', N'TOILETRIES', 401)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL50 -- PETSENTIALS SHAMPOO 250ml', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL52 -- TOOTH PASTE', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL53 -- PETSENTIALLS SHAMPOO 500ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL54 -- FUR MAGIC SOAP', N'TEST KITS', 180)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL55 -- ST. ROCHE SHAMPOO 250ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL56 -- SAINT ROCHE COLOGNE 125ml', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL57 -- PAPI GROOM & BLOOM 1L (red)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL58 -- ORAL CARE PLUS', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL59 -- Furfect Biosulfur + Madre de Cacao soap', N'TOILETRIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL6 -- DOGGIE''S CHOICE ANTI MANGE', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL60 -- Furfect fipronil Soap', N'TOILETRIES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL62 -- Top Tail Shampoo 500ml', N'TOILETRIES', 318)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL63 -- Top Tail Soap', N'TOILETRIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL64 -- Top Tail Talc', N'TOILETRIES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL65 -- ENZYMATIC TOOTHPASTE', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL66 -- MADRE DE CACAO LIQUID SOAP 500ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL67 -- MADRE DE CACAO LIQUID SOAP 250ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL68 -- MADRE DE CACAO ALOE VERA SHAMPOO 250ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL7 -- DOGGIE''S CHOICE POWDER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL70 -- PAWSOME SCENTS COLOGNE', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL71 -- HAPPYPETS FRENZY SHAMPOO', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL8 -- DOGGIE''S CHOICE SHAMPOO ANTI MANGE', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL9 -- DOGGIE''S CHOICE TICK & FLEA SOAP', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS1 -- charcoas 80 G', N'TREATS', 69)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS2 -- DENTASTIX SINGLE', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS3 -- HAPPI DOGGY DENTAL CHEW', N'TREATS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS4 -- PEDIGREE DENTASTIX (10-25KG) 98G', N'TREATS', 61.65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS5 -- PEDIGREE DENTASTIX (3-12MONTHS) 56G', N'TREATS', 62.02)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS6 -- PEDIGREE DENTASTIX (5-10KG) 75G', N'TREATS', 61.02)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS7 -- PEDIGREE DENTASTIX (5KG) 60G', N'TREATS', 61.02)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS8 -- SLEEKY STICK 50mg', N'TREATS', 62.1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX3 -- FELINE (CAT VAX)', N'VACCINATION', 450)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX8 -- 8N1', N'VACCINATION', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP12 -- FURBABY''S 300GM', N'VITAMINS AND SUPPLEMENTS', 365)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP13 -- Healthy Coat bottle', N'VITAMINS AND SUPPLEMENTS', 512.64)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP14 -- Healthy Coat per tab', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP15 -- GLUCOSAMINE BOTTLE', N'VITAMINS AND SUPPLEMENTS', 1055.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP16 -- HIP & JOINT CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP17 -- IMMUNOL BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 448)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP18 -- IMMUNOL PER TAB', N'VITAMINS AND SUPPLEMENTS', 7.46)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP2 -- ALLERGY SUPPORT 90CHEWABLES', N'VITAMINS AND SUPPLEMENTS', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP20 -- LC-VIT 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP21 -- LC-VIT 60ml', N'VITAMINS AND SUPPLEMENTS', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP22 -- LIV-52 BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP25 -- LIVOTINE 60ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP27 -- METHIOVET BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 605)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP28 -- METHIOVET PER TAB', N'VITAMINS AND SUPPLEMENTS', 11)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP3 -- APT FORMULA LAC', N'VITAMINS AND SUPPLEMENTS', 650)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP30 -- NASTY HABIT BOTTLE 6OCAPS(Stool eating prevention)', N'VITAMINS AND SUPPLEMENTS', 672)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP31 -- NASTY HABIT PER CAP(Stool eating prevention)', N'VITAMINS AND SUPPLEMENTS', 11.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP32 -- NEFROTEC BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP34 -- NEUROVET BOTTLE 20TABS(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP36 -- NUTRI TIER GEL', N'VITAMINS AND SUPPLEMENTS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP37 -- NUTRIBLEND GEL', N'VITAMINS AND SUPPLEMENTS', 387.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP38 -- NUTRICAL 120ml', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP4 -- BREWERS YEAST CHEWABLE BOTTLE (300TABS)', N'VITAMINS AND SUPPLEMENTS', 473.42917)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP41 -- PAPI OB SYRUP 120ML', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP42 -- PROBIOTICS BOTTLE (60CAPS)', N'VITAMINS AND SUPPLEMENTS', 980)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP43 -- PROBIOTICS PER CAP', N'VITAMINS AND SUPPLEMENTS', 17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP44 -- PUPPY VITE GEL', N'VITAMINS AND SUPPLEMENTS', 445)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP45 -- REFAMOL BOTTLE (30CAPS)', N'VITAMINS AND SUPPLEMENTS', 360)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP46 -- REFAMOL PER CAP', N'VITAMINS AND SUPPLEMENTS', 12)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP48 -- SHED DEFENSE MAX CHEWABLE BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 456)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP49 -- SHED DEFENSE MAX PER TAB', N'VITAMINS AND SUPPLEMENTS', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP5 -- BREWERS YEAST CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP51 -- HEMACARE-FE', N'VITAMINS AND SUPPLEMENTS', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP52 -- PUPS GAIN PLUS PER BOX', N'MEDICINES', 900)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP53 -- PUPS GAIN PLUS PER SACHET', N'VITAMINS AND SUPPLEMENTS', 225)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP54 -- MILKY BITCH PER BOX', N'VITAMINS AND SUPPLEMENTS', 1540)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP55 -- MILKY BITCH PER SACHET', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP56 -- PET-C', N'VITAMINS AND SUPPLEMENTS', 97)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP57 -- SORVIT SYRUP', N'VITAMINS AND SUPPLEMENTS', 127.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP58 -- NUTRIDEX', N'VITAMINS AND SUPPLEMENTS', 50.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP59 -- HEMACARE-FE 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP6 -- COAT SHINE 120ML', N'VITAMINS AND SUPPLEMENTS', 232)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP60 -- GLUCOSAMINE PLUS (joint formula) BOTTLE', N'VITAMINS AND SUPPLEMENTS', 1212)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP61 -- GLUCOSAMINE PLUS TAB', N'VITAMINS AND SUPPLEMENTS', 20.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP62 -- HEALTHY LIVER BOTTLE', N'VITAMINS AND SUPPLEMENTS', 947)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP64 -- PET-TABS BOTTLE (180 tabs)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP65 -- PET-TABS BOTTLE (60tabs)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP66 -- PET-TABS PER TAB', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP67 -- LIVEROLIN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP69 -- PUPPY NUTRIGEL', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP7 -- DELTACAL BOTTLE (50TABS)', N'VITAMINS AND SUPPLEMENTS', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP70 -- TOP B+', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP71 -- SINGEN CARE MILK', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP72 -- PUPPY SURE MILK', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP73 -- VITA PET  120ML', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP74 -- VET PET 60ML', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP75 -- ASCORBIC ACID 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP77 -- PREFOLIC-CEE 120ml', N'VITAMINS AND SUPPLEMENTS', 195)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP78 -- EMERPLEX 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP79 -- PETS OWN CAT & KITTEN MILK 1L', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP80 -- PETS OWN DOG & PUPPY MILK 1L', N'VITAMINS AND SUPPLEMENTS', 230)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP81 -- LC VIT PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION AND BLOOD TRANSFUSION', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC18 -- E-COLLAR #1', N'ACCESSORIES', 130)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC26 -- FOLDABLE CAGE (LARGE)', N'ACCESSORIES', 1400)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC56 -- NYLON COLLAR + LEADSET (M) 15MM', N'ACCESSORIES', 105)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR18 -- FLEA + TICK DEFENSE SPRAY FOR DOGS AND CATS 100ML', N'ANTI PARASITICS', 342)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR26 -- NEXGARD SPECTRA 15-30KG 3TABS (VIOLET)', N'ANTI PARASITICS', 593.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR41 -- SIMPARICA 22.1-44.0 lbs (sky blue)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR7 -- BRAVECTO (20-40kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF44 -- PLAISIR BIG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF51 -- VITALITY CLASSIC/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED15 -- OXYTOCIN', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB47 -- CHEM 15', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS106 -- LC-DOX SYRUP 60ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS141 -- HYCLENS SKIN CLEANSER', N'MEDICINES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS148 -- PRONTROVET WOUND GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS156 -- K9 DOXY TAB/PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS16 -- AZITHROMYCIN SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS161 -- RENAL N/ PER SMALL SCOOP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS43 -- ERYTHROPOIETIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS67 -- MENADIONE 10MG tab', N'MEDICINES', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS92 -- RECOVERY PASTE LYSINE', N'MEDICINES', 505)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET13 -- ROYAL CANIN RENAL can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER35 -- SCABICIDE TX  0-3kg', N'SERVICES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV39 -- EMERGENCY FEE/DOCTORS FEE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL32 -- Indwelling foley Catheter', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL65 -- URINARY CANINE  CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST17 -- CDV/CPV CONFIRMATORY TEST', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST9 -- Ehrlichia-Anaplasma-Babesia-Heartworm Test', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL14 -- HUSHPET DIAPER LARGE PER PC', N'TOILETRIES', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS5 -- PEDIGREE DENTASTIX (3-12MONTHS) 56G', N'TREATS', 62.02)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP18 -- IMMUNOL PER TAB', N'VITAMINS AND SUPPLEMENTS', 7.46)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP22 -- LIV-52 BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP25 -- LIVOTINE 60ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP44 -- PUPPY VITE GEL', N'VITAMINS AND SUPPLEMENTS', 445)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP6 -- COAT SHINE 120ML', N'VITAMINS AND SUPPLEMENTS', 232)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP67 -- LIVEROLIN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION MACASANDIG', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC18 -- E-COLLAR #1', N'ACCESSORIES', 130)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC61 -- NYLON FABRIC MUZZLE SIZE 4', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR24 -- NEXGARD 25-50KG tab (RED)', N'ANTI PARASITICS', 357.91)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR26 -- NEXGARD SPECTRA 15-30KG 3TABS (VIOLET)', N'ANTI PARASITICS', 593.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR29 -- NEXGARD SPECTRA 30-60KG  (RED)', N'ANTI PARASITICS', 718.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR42 -- SIMPARICA 44.1-88.0 lbs (Green)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0038 -- SPECIAL CAT CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF16 -- PEDIGREE PUPPY PER KILO', N'DOG & CAT FOOD', 116)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF18 -- PRINCESS CAT (1KG)', N'DOG & CAT FOOD', 99)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF43 -- VALUEMEAL CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF59 -- ECO OSCAR BIG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF63 -- DIANA CAT FOOD', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED21 -- ACETYLCYSTEINE', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS1 -- ACETYLCYTEINE SACHET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS106 -- LC-DOX SYRUP 60ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS115 -- CYSTAID PLUS', N'MEDICINES', 17.95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS116 -- CELLMANAX', N'MEDICINES', 430)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS118 -- BENZYL PENICILLIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS119 -- VIBRAVET', N'MEDICINES', 430.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS122 -- SCOURVET 60ML', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS125 -- VET TREAT (enrofloxacin)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS129 -- IMMUNE HEALTH PER TAB', N'MEDICINES', 6.35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS130 -- VETPRAMIDE TAB', N'MEDICINES', 5.1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS141 -- HYCLENS SKIN CLEANSER', N'MEDICINES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS148 -- PRONTROVET WOUND GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS150 -- EMERFLOX', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS178 -- CYSTOCURE FORTE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS20 -- CANGLOB D Per ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS26 -- CHARCOAL VET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS63 -- LPS DR.', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS64 -- Lysine', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS65 -- MARBOCYLTABINE 5MG TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS69 -- METOCLOPRAMIDE SYRUP', N'MEDICINES', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS83 -- PIRANTEL DEWORMER PER TAB', N'MEDICINES', 29.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS92 -- RECOVERY PASTE LYSINE', N'MEDICINES', 505)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC27 -- Nebulization with meds.', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET23 -- ROYAL CANIN KITTEN 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET28 -- ROYAL CANIN FELINE S/O 2KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET30 -- FELINE S/O POUCH', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET33 -- DIAMOND CARE URINARY SUPPORT/ KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER34 -- PF,SURGERY AND WOUND SUTURING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL11 -- CAT CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL4 -- BIO-FLON CANNULA', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL4 -- CATSAN ULTRA ODOR CONTROL 5li (Pink)', N'TOILETRIES', 180.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL58 -- ORAL CARE PLUS', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP16 -- HIP & JOINT CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP18 -- IMMUNOL PER TAB', N'VITAMINS AND SUPPLEMENTS', 7.46)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP22 -- LIV-52 BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP32 -- NEFROTEC BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP37 -- NUTRIBLEND GEL', N'VITAMINS AND SUPPLEMENTS', 387.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP5 -- BREWERS YEAST CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP58 -- NUTRIDEX', N'VITAMINS AND SUPPLEMENTS', 50.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP59 -- HEMACARE-FE 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP61 -- GLUCOSAMINE PLUS TAB', N'VITAMINS AND SUPPLEMENTS', 20.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP67 -- LIVEROLIN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP70 -- TOP B+', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP77 -- PREFOLIC-CEE 120ml', N'VITAMINS AND SUPPLEMENTS', 195)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP78 -- EMERPLEX 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION MAIN', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC101 -- SOCKS', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC125 -- PRINTED COLLAR W/ LEASH MEDIUM', N'ACCESSORIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC14 -- Dog Leash (Long) WH-A (DA1096)', N'ACCESSORIES', 180)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC18 -- E-COLLAR #1', N'ACCESSORIES', 130)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC2 -- Aqua Dog (Water Bottle)', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC27 -- FOLDABLE CAGE (MEDIUM)', N'ACCESSORIES', 1050)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC28 -- FOLDABLE CAGE (SMALL)', N'ACCESSORIES', 750)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC29 -- FOLDABLE CAGE (EXTRA-SMALL)', N'ACCESSORIES', 500)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC35 -- JERSEY SHIRTS', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC44 -- MICHIKO NAIL CLIPPER SMALL', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC47 -- MICHIKO TOOTHBRUSH 2PCS', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC48 -- MICHIKO TOOTHBRUSH 3PCS', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC49 -- NLWH BODY HARNESS SMALL', N'ACCESSORIES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC50 -- NYLON COLLAR (L)', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC53 -- NYLON COLLAR (XL)', N'ACCESSORIES', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC54 -- NYLON Collar + Lead set (Small)', N'ACCESSORIES', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC55 -- NYLON Collar + Leadset (Large)', N'ACCESSORIES', 145)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC63 -- NYLON Lead + Harness SET (Med)', N'ACCESSORIES', 165)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC73 -- PET NANNY', N'ACCESSORIES', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC86 -- PVC MUZZLE (SIZE 5)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC91 -- Regular Non Skid Bowl (8 Oz)', N'ACCESSORIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC99 -- SILICONE MUZZLE (Medium)', N'ACCESSORIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR1 -- ADVOCATE cat 4kg or less', N'ANTI PARASITICS', 296.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR10 -- BROADLINE for cats 2.5 kg spot on', N'ANTI PARASITICS', 321.42)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR12 -- CANIMAX PER TAB', N'ANTI PARASITICS', 204.66)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR16 -- FLEA & TICK SPOT ON FOR DOGS  GREEN(10KG-20KG)', N'ANTI PARASITICS', 249)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR17 -- FLEA & TICK SPOT ON FOR DOGS VIOLET(25KG N UP)', N'ANTI PARASITICS', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR20 -- MILBEMAX 12.5mg', N'ANTI PARASITICS', 117)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR23 -- NEXGARD 2.4KG per tab (ORANGE)', N'ANTI PARASITICS', 301.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR24 -- NEXGARD 25-50KG tab (RED)', N'ANTI PARASITICS', 357.91)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR26 -- NEXGARD SPECTRA 15-30KG 3TABS (VIOLET)', N'ANTI PARASITICS', 593.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR29 -- NEXGARD SPECTRA 30-60KG  (RED)', N'ANTI PARASITICS', 718.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR33 -- Nutrivet Defense (Dark Violet)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR37 -- VENOMA 120ML', N'ANTI PARASITICS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR38 -- VERMIFUGE 60ml (dewormer syrup)', N'ANTI PARASITICS', 186)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR40 -- SIMPARICA 11.1-22.0 lbs (brown)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR41 -- SIMPARICA 22.1-44.0 lbs (sky blue)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR42 -- SIMPARICA 44.1-88.0 lbs (Green)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR6 -- BRAVECTO (10-20kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR9 -- BROADLINE for cats  2.5-7.5KG spot on', N'ANTI PARASITICS', 361.6)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0038 -- SPECIAL CAT CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF10 -- MEOW MIX (with real chicken & turkey in gravy)', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF11 -- NUTRI CHUNKS', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF16 -- PEDIGREE PUPPY PER KILO', N'DOG & CAT FOOD', 116)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF17 -- Premium Tuna With Plus', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF18 -- PRINCESS CAT (1KG)', N'DOG & CAT FOOD', 99)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF2 -- BEEFPRO ADULT PER KILO', N'DOG & CAT FOOD', 83)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF36 -- PEDIGREE CAN small', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF37 -- WHISKAS CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF43 -- VALUEMEAL CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF45 -- HOLISTIC PER KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF46 -- VITALITY CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF48 -- SPECIAL DOG PUPPY/KG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF49 -- SPECIAL DOG ADULT/KG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF51 -- VITALITY CLASSIC/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF55 -- LAMB & RICE', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF56 -- PRO DIET CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF60 -- PEDIGREE CAN MEDIUM', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF61 -- PEDIGREE CAN LARGE', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF63 -- DIANA CAT FOOD', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF7 -- HEROI ADULT PER KILO', N'DOG & CAT FOOD', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF756 -- RECOVERY FOOD CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED11 -- IVERMECTIN', N'INJECTIBLE MEDS', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED15 -- OXYTOCIN', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED18 -- TRANQUILIZER CALMIVET 20KG & UP', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED2 -- ATROSITE PER INJECTION UP TO10KG', N'INJECTIBLE MEDS', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED20 -- TRANQUILIZER-CALMIVET-ACEPROMAZINE MALEATE PER INJECTION 10KG-15KG', N'INJECTIBLE MEDS', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED21 -- ACETYLCYSTEINE', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED22 -- CONVENIA (antibiotic) ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED3 -- CA + (Calcium) per ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED5 -- Co-amox (IV infusion) per 1ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED6 -- DCM(Dextrose,magnesiumgluconate and calcium borogluconate) injection per ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED9 -- Euthanasia meds', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB47 -- CHEM 15', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS1 -- ACETYLCYTEINE SACHET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS10 -- ANTI ITCH SPRAY 118ML', N'MEDICINES', 365)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS106 -- LC-DOX SYRUP 60ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS109 -- LC-VIT OB BOTTLE', N'MEDICINES', 255.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS115 -- CYSTAID PLUS', N'MEDICINES', 17.95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS116 -- CELLMANAX', N'MEDICINES', 430)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS117 -- AZITHROMYCIN TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS119 -- VIBRAVET', N'MEDICINES', 430.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS122 -- SCOURVET 60ML', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS123 -- SODIUM BICARBONATE TAB', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS124 -- BIOCURE TAB (doxycycline hci)', N'MEDICINES', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS125 -- VET TREAT (enrofloxacin)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS126 -- ENROFLOX SYRUP', N'MEDICINES', 356)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS128 -- WORMRID TAB', N'MEDICINES', 49.56)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS129 -- IMMUNE HEALTH PER TAB', N'MEDICINES', 6.35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS133 -- NUTRICH BOTTLE 60tabs', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS135 -- EYE SHIELD', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS139 -- BLADDER CONTROL PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS141 -- HYCLENS SKIN CLEANSER', N'MEDICINES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS142 -- HONELLE WOUND GEL', N'MEDICINES', 389)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS146 -- SUCRALFATE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS147 -- PIMOBENDAN TAB 1.25mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS148 -- PRONTROVET WOUND GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS15 -- AURIZON 10ML(EAR DROPS)', N'MEDICINES', 534.9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS150 -- EMERFLOX', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS152 -- K9 CALCIUM TABLET/TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS153 -- RENAL N POWDER', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS154 -- CEPHALEXIN SYRUP (CFLEX) 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS155 -- PIMOBENDAN 5mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS156 -- K9 DOXY TAB/PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS157 -- SOLCOSERYL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS158 -- FLUIMUCIL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS159 -- IMMUNE HEALTH BOTTLE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS16 -- AZITHROMYCIN SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS162 -- TRITOZINE 60ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS178 -- CYSTOCURE FORTE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS18 -- Black Armour', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS180 -- LACTULOSE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS2 -- AIR-LAX', N'MEDICINES', 460)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS20 -- CANGLOB D Per ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS21 -- CANGLOB D VIAL', N'MEDICINES', 734.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS25 -- CEFALEXIN', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS26 -- CHARCOAL VET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS29 -- Co-amox 125MG SYRUP', N'MEDICINES', 131)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS32 -- DEXAMETHASONE TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS36 -- EAR CLEANSE 118ML', N'MEDICINES', 311)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS38 -- Enalapril', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS39 -- ENROFLOX BOX (50TABS)', N'MEDICINES', 563.33333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS41 -- EPIOTIC 125ML', N'MEDICINES', 380.31)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS43 -- ERYTHROPOIETIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS45 -- EYE RINSE 118ML', N'MEDICINES', 333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS47 -- FLEET ENEMA', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS48 -- FUROSEMIDE 20MG TAB', N'MEDICINES', 4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS5 -- ALUMINUM  HYDROXIDE SYRUP 60ML', N'MEDICINES', 16)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS53 -- ITRACONAZOLE 25mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS54 -- CLAVAMOX 250mg tab(Co amoxiclav)', N'MEDICINES', 42)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS55 -- K9 EAR DROPS 60ML', N'MEDICINES', 283)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS57 -- KETOCONAZOLE PER TAB(200MG)', N'MEDICINES', 23)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS59 -- LAKTRAZINE TAB', N'MEDICINES', 8.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS6 -- ALUMINUM HYDROXIDE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS61 -- LC DOX BOTTLE (Doxycycline 100mg tab-100pcs)', N'MEDICINES', 1222)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS62 -- LIVERATOR', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS63 -- LPS DR.', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS64 -- Lysine', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS65 -- MARBOCYLTABINE 5MG TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS66 -- MELOXICAM SYRUP PER ML', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS67 -- MENADIONE 10MG tab', N'MEDICINES', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS68 -- METHYLERGOMETRINE 125MCG TAB', N'MEDICINES', 3)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS69 -- METOCLOPRAMIDE SYRUP', N'MEDICINES', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS7 -- AMINOLEBAN', N'MEDICINES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS70 -- Metoclopramide Tab', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS73 -- MICROSCOT', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS76 -- ORIDERMYL', N'MEDICINES', 530.90909)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS78 -- PEN-G', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS81 -- PHENOBARBITAL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS83 -- PIRANTEL DEWORMER PER TAB', N'MEDICINES', 29.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS84 -- POTASSIUM CHLORIDE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS92 -- RECOVERY PASTE LYSINE', N'MEDICINES', 505)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS93 -- RECTAL SUPPOSITORY', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS97 -- TERBUTALINE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS99 -- Tobrason', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC27 -- Nebulization with meds.', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC28 -- PET ICU', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC43 -- CYSTOCENTESIS/RETROPULSION OF STONES', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET10 -- ROYAL CANIN HEPATIC 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET13 -- ROYAL CANIN RENAL can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET14 -- ROYAL CANIN S/O can', N'PRESCRIPTION DIETS', 162.36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET15 -- ROYAL CANIN SATIETY DIET  2KG', N'PRESCRIPTION DIETS', 951)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET17 -- ROYAL CANINE S/O 2kg', N'PRESCRIPTION DIETS', 1159)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET18 -- Singen Kidney Diet VKD1 (canine)', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET21 -- Vet Solution Hepatic feline', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET22 -- Vet Solution Renal per 1kg', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET23 -- ROYAL CANIN KITTEN 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET24 -- ROYAL CANIN INSTINCTIVE 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET28 -- ROYAL CANIN FELINE S/O 2KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET3 -- KIDNEY DIET VKC1 (feline)', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET30 -- FELINE S/O POUCH', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET31 -- ROYAL CANIN CARDIAC 2kg', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET32 -- ROYAL CANIN STARTER CAN', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET33 -- DIAMOND CARE URINARY SUPPORT/ KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET4 -- MONGEE HEPATIC 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET6 -- RENAL 2KG', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIETt34 -- ROYAL CANIN RENAL SACHET FELIN1', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER11 -- DAILY CONFINEMENT FEE MOTHER AND PUPS/KITTENS', N'SERVICES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER34 -- PF,SURGERY AND WOUND SUTURING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV39 -- EMERGENCY FEE/DOCTORS FEE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV40 -- PF + WHELPING ASSISTANCE & EXTRACTION OF PUPS', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL1 -- 50 ml Feeding syringe', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL11 -- CAT CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL12 -- CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL15 -- CHROMIC CATGUT 2/0', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL16 -- CHROMIC CATGUT 3/0', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL32 -- Indwelling foley Catheter', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL36 -- NASAL OXYGEN TUBE INSERTION,SUPPLIES AND LOCAL ANESTHETICS', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL48 -- POLYDIOXANONE (24MM)', N'SUPPLIES', 200)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL51 -- SILK (non absorbable suture)', N'SUPPLIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL62 -- SURGICAL STAPLER', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL65 -- URINARY CANINE  CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL68 -- VIOLET TUBES', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL9 -- BUDZ VET SUTURES CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SURG44 -- PF RECTAL PROLAPSE CORRECTION', N'SURGERY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST11 -- IDEXX LEPTOSPIROSIS TEST', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST17 -- CDV/CPV CONFIRMATORY TEST', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST2 -- Anaplasma-Ehrlichiosis-Babesia- Heart Worm Test kit', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST9 -- Ehrlichia-Anaplasma-Babesia-Heartworm Test', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL1 -- CAT LITTER PAN W/ SCOOPER', N'TOILETRIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL11 -- DONO DIAPER EXTRA-SMALL PER PC', N'TOILETRIES', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL15 -- HUSHPET DIAPER MEDIUM PER PC', N'TOILETRIES', 12)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL16 -- HUSHPET DIAPER MEDIUM (12PCS/PACK)', N'TOILETRIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL18 -- HUSHPET DIAPER SMALL PER PC', N'TOILETRIES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL23 -- PAPI COAT MAINTENANCE SOAP', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL24 -- PAPI GROOM & BLOOM (BLUE) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL3 -- CATSAN 5li cat litter (Green)', N'TOILETRIES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL33 -- PET SOCIETY HERBAL SOAP 60G', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL35 -- PET SPLASH ANTI TIQUES 250ML', N'TOILETRIES', 246)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL4 -- CATSAN ULTRA ODOR CONTROL 5li (Pink)', N'TOILETRIES', 180.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL45 -- ST. ROCHE DETANGLING SPRAY 318ml', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL47 -- ST. ROCHE SHAMPOO 628ml', N'TOILETRIES', 261)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL5 -- DENTAL HYGIENE KIT', N'TOILETRIES', 401)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL53 -- PETSENTIALLS SHAMPOO 500ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL55 -- ST. ROCHE SHAMPOO 250ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL62 -- Top Tail Shampoo 500ml', N'TOILETRIES', 318)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL63 -- Top Tail Soap', N'TOILETRIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS3 -- HAPPI DOGGY DENTAL CHEW', N'TREATS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS6 -- PEDIGREE DENTASTIX (5-10KG) 75G', N'TREATS', 61.02)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS8 -- SLEEKY STICK 50mg', N'TREATS', 62.1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX3 -- FELINE (CAT VAX)', N'VACCINATION', 450)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX8 -- 8N1', N'VACCINATION', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP13 -- Healthy Coat bottle', N'VITAMINS AND SUPPLEMENTS', 512.64)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP15 -- GLUCOSAMINE BOTTLE', N'VITAMINS AND SUPPLEMENTS', 1055.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP16 -- HIP & JOINT CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP18 -- IMMUNOL PER TAB', N'VITAMINS AND SUPPLEMENTS', 7.46)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP20 -- LC-VIT 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP21 -- LC-VIT 60ml', N'VITAMINS AND SUPPLEMENTS', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP22 -- LIV-52 BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP25 -- LIVOTINE 60ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP28 -- METHIOVET PER TAB', N'VITAMINS AND SUPPLEMENTS', 11)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP3 -- APT FORMULA LAC', N'VITAMINS AND SUPPLEMENTS', 650)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP32 -- NEFROTEC BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP34 -- NEUROVET BOTTLE 20TABS(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP36 -- NUTRI TIER GEL', N'VITAMINS AND SUPPLEMENTS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP37 -- NUTRIBLEND GEL', N'VITAMINS AND SUPPLEMENTS', 387.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP38 -- NUTRICAL 120ml', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP41 -- PAPI OB SYRUP 120ML', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP43 -- PROBIOTICS PER CAP', N'VITAMINS AND SUPPLEMENTS', 17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP44 -- PUPPY VITE GEL', N'VITAMINS AND SUPPLEMENTS', 445)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP45 -- REFAMOL BOTTLE (30CAPS)', N'VITAMINS AND SUPPLEMENTS', 360)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP46 -- REFAMOL PER CAP', N'VITAMINS AND SUPPLEMENTS', 12)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP5 -- BREWERS YEAST CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP51 -- HEMACARE-FE', N'VITAMINS AND SUPPLEMENTS', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP56 -- PET-C', N'VITAMINS AND SUPPLEMENTS', 97)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP57 -- SORVIT SYRUP', N'VITAMINS AND SUPPLEMENTS', 127.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP58 -- NUTRIDEX', N'VITAMINS AND SUPPLEMENTS', 50.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP59 -- HEMACARE-FE 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP6 -- COAT SHINE 120ML', N'VITAMINS AND SUPPLEMENTS', 232)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP61 -- GLUCOSAMINE PLUS TAB', N'VITAMINS AND SUPPLEMENTS', 20.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP62 -- HEALTHY LIVER BOTTLE', N'VITAMINS AND SUPPLEMENTS', 947)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP65 -- PET-TABS BOTTLE (60tabs)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP66 -- PET-TABS PER TAB', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP67 -- LIVEROLIN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP69 -- PUPPY NUTRIGEL', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP7 -- DELTACAL BOTTLE (50TABS)', N'VITAMINS AND SUPPLEMENTS', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP70 -- TOP B+', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP71 -- SINGEN CARE MILK', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP77 -- PREFOLIC-CEE 120ml', N'VITAMINS AND SUPPLEMENTS', 195)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP78 -- EMERPLEX 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP8 -- DELTACAL PER TAB', N'VITAMINS AND SUPPLEMENTS', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP81 -- LC VIT PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION N SURGERY', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC18 -- E-COLLAR #1', N'ACCESSORIES', 130)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC50 -- NYLON COLLAR (L)', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC64 -- NYLON Lead + Harness SET (Small) 10MM', N'ACCESSORIES', 100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR1 -- ADVOCATE cat 4kg or less', N'ANTI PARASITICS', 296.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0038 -- SPECIAL CAT CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF16 -- PEDIGREE PUPPY PER KILO', N'DOG & CAT FOOD', 116)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF37 -- WHISKAS CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF43 -- VALUEMEAL CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED11 -- IVERMECTIN', N'INJECTIBLE MEDS', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED15 -- OXYTOCIN', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED2 -- ATROSITE PER INJECTION UP TO10KG', N'INJECTIBLE MEDS', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED22 -- CONVENIA (antibiotic) ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED5 -- Co-amox (IV infusion) per 1ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED6 -- DCM(Dextrose,magnesiumgluconate and calcium borogluconate) injection per ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED9 -- Euthanasia meds', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB47 -- CHEM 15', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS109 -- LC-VIT OB BOTTLE', N'MEDICINES', 255.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS119 -- VIBRAVET', N'MEDICINES', 430.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS135 -- EYE SHIELD', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS141 -- HYCLENS SKIN CLEANSER', N'MEDICINES', 218)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS142 -- HONELLE WOUND GEL', N'MEDICINES', 389)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS148 -- PRONTROVET WOUND GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS18 -- Black Armour', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS25 -- CEFALEXIN', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS29 -- Co-amox 125MG SYRUP', N'MEDICINES', 131)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS45 -- EYE RINSE 118ML', N'MEDICINES', 333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS63 -- LPS DR.', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS64 -- Lysine', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS66 -- MELOXICAM SYRUP PER ML', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS78 -- PEN-G', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS79 -- PET REBOOST', N'MEDICINES', 214.28571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS97 -- TERBUTALINE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC37 -- Tumor Dressing & Myasis Manual Worm Removal', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET14 -- ROYAL CANIN S/O can', N'PRESCRIPTION DIETS', 162.36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET17 -- ROYAL CANINE S/O 2kg', N'PRESCRIPTION DIETS', 1159)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET24 -- ROYAL CANIN INSTINCTIVE 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER11 -- DAILY CONFINEMENT FEE MOTHER AND PUPS/KITTENS', N'SERVICES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER34 -- PF,SURGERY AND WOUND SUTURING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV40 -- PF + WHELPING ASSISTANCE & EXTRACTION OF PUPS', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL12 -- CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL14 -- CHROMIC CATGUT 1', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL15 -- CHROMIC CATGUT 2/0', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL16 -- CHROMIC CATGUT 3/0', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL32 -- Indwelling foley Catheter', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL38 -- NOVOSYN (90CM)', N'SUPPLIES', 270.45455)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL45 -- PDO SOTURE 2-0', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL51 -- SILK (non absorbable suture)', N'SUPPLIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL61 -- SURGICAL NEEDLES', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL62 -- SURGICAL STAPLER', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL65 -- URINARY CANINE  CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL66 -- VET CASTING TAPE & SUPPLIES', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL99 -- orthopedic casting materials and supplies', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST2 -- Anaplasma-Ehrlichiosis-Babesia- Heart Worm Test kit', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL18 -- HUSHPET DIAPER SMALL PER PC', N'TOILETRIES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP16 -- HIP & JOINT CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP3 -- APT FORMULA LAC', N'VITAMINS AND SUPPLEMENTS', 650)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP37 -- NUTRIBLEND GEL', N'VITAMINS AND SUPPLEMENTS', 387.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP38 -- NUTRICAL 120ml', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP41 -- PAPI OB SYRUP 120ML', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP53 -- PUPS GAIN PLUS PER SACHET', N'VITAMINS AND SUPPLEMENTS', 225)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP7 -- DELTACAL BOTTLE (50TABS)', N'VITAMINS AND SUPPLEMENTS', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP77 -- PREFOLIC-CEE 120ml', N'VITAMINS AND SUPPLEMENTS', 195)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP79 -- PETS OWN CAT & KITTEN MILK 1L', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION NHA', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR24 -- NEXGARD 25-50KG tab (RED)', N'ANTI PARASITICS', 357.91)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR42 -- SIMPARICA 44.1-88.0 lbs (Green)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR9 -- BROADLINE for cats  2.5-7.5KG spot on', N'ANTI PARASITICS', 361.6)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0038 -- SPECIAL CAT CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF37 -- WHISKAS CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF51 -- VITALITY CLASSIC/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF56 -- PRO DIET CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF63 -- DIANA CAT FOOD', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS116 -- CELLMANAX', N'MEDICINES', 430)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS122 -- SCOURVET 60ML', N'MEDICINES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS124 -- BIOCURE TAB (doxycycline hci)', N'MEDICINES', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS125 -- VET TREAT (enrofloxacin)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS160 -- RENAL N /PER 1 SCOOP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS178 -- CYSTOCURE FORTE TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS32 -- DEXAMETHASONE TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS36 -- EAR CLEANSE 118ML', N'MEDICINES', 311)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS63 -- LPS DR.', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS69 -- METOCLOPRAMIDE SYRUP', N'MEDICINES', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS76 -- ORIDERMYL', N'MEDICINES', 530.90909)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS83 -- PIRANTEL DEWORMER PER TAB', N'MEDICINES', 29.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC27 -- Nebulization with meds.', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET14 -- ROYAL CANIN S/O can', N'PRESCRIPTION DIETS', 162.36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET24 -- ROYAL CANIN INSTINCTIVE 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET30 -- FELINE S/O POUCH', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET32 -- ROYAL CANIN STARTER CAN', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET33 -- DIAMOND CARE URINARY SUPPORT/ KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL11 -- CAT CATHETER', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP28 -- METHIOVET PER TAB', N'VITAMINS AND SUPPLEMENTS', 11)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP43 -- PROBIOTICS PER CAP', N'VITAMINS AND SUPPLEMENTS', 17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP5 -- BREWERS YEAST CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP56 -- PET-C', N'VITAMINS AND SUPPLEMENTS', 97)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP67 -- LIVEROLIN', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP70 -- TOP B+', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP74 -- VET PET 60ML', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP77 -- PREFOLIC-CEE 120ml', N'VITAMINS AND SUPPLEMENTS', 195)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP78 -- EMERPLEX 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMISSION UPTOWN', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR9 -- BROADLINE for cats  2.5-7.5KG spot on', N'ANTI PARASITICS', 361.6)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF61 -- PEDIGREE CAN LARGE', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB1876 -- MANUAL EXTRACTION OF FOREIGN BODY', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS137 -- BESAME 200', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS143 -- HYCLENS WOUND SPRAY', N'MEDICINES', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS150 -- EMERFLOX', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS153 -- RENAL N POWDER', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS160 -- RENAL N /PER 1 SCOOP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS180 -- LACTULOSE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS36 -- EAR CLEANSE 118ML', N'MEDICINES', 311)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS56 -- K9-EYE DROPS', N'MEDICINES', 306)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS69 -- METOCLOPRAMIDE SYRUP', N'MEDICINES', 14)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS80 -- PETTY GEL', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'OTH1412 -- MOUTH GUARD', N'OTHERS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET13 -- ROYAL CANIN RENAL can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SURG44 -- PF RECTAL PROLAPSE CORRECTION', N'SURGERY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL3 -- CATSAN 5li cat litter (Green)', N'TOILETRIES', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ADMIT/EMER', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR7 -- BRAVECTO (20-40kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF0034 -- PLAISIR small', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF44 -- PLAISIR BIG', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED15 -- OXYTOCIN', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED22 -- CONVENIA (antibiotic) ml', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED23 -- ORNIPURAL ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED6 -- DCM(Dextrose,magnesiumgluconate and calcium borogluconate) injection per ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS108 -- ANTI-DIARRHEA LIQUID', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS26 -- CHARCOAL VET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS28 -- CLINDAMYCIN 300mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS33 -- DIAZEPAM', N'MEDICINES', 114)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS49 -- FUROSEMIDE 40MG', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET32 -- ROYAL CANIN STARTER CAN', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV39 -- EMERGENCY FEE/DOCTORS FEE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL12 -- CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL52 -- SOLUSET', N'SUPPLIES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS1 -- charcoas 80 G', N'TREATS', 69)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP34 -- NEUROVET BOTTLE 20TABS(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP38 -- NUTRICAL 120ml', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP41 -- PAPI OB SYRUP 120ML', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP47 -- RENACURE 60ML(renal tonic)', N'VITAMINS AND SUPPLEMENTS', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP59 -- HEMACARE-FE 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP61 -- GLUCOSAMINE PLUS TAB', N'VITAMINS AND SUPPLEMENTS', 20.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'BLOOD TRANSFUSION', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB46 --  NSAID TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST8 -- EHRLICHIA-ANAPLASMA-BABESIA Antigen test', N'TEST KITS', 570)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'BOARDING', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC50 -- NYLON COLLAR (L)', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR23 -- NEXGARD 2.4KG per tab (ORANGE)', N'ANTI PARASITICS', 301.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF2 -- BEEFPRO ADULT PER KILO', N'DOG & CAT FOOD', 83)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF43 -- VALUEMEAL CAN', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF51 -- VITALITY CLASSIC/KILO', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS10 -- ANTI ITCH SPRAY 118ML', N'MEDICINES', 365)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL25 -- PAPI GROOM & BlOOM (GREEN) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS2 -- DENTASTIX SINGLE', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX8 -- 8N1', N'VACCINATION', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP48 -- SHED DEFENSE MAX CHEWABLE BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 456)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'EMERGENCY', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SERV39 -- EMERGENCY FEE/DOCTORS FEE', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FIRST VISIT', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'GROOMING', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC102 -- SPORTS BALL W/ ROPE TOY', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC106 -- TEETHER 8 ROPE TOY', N'ACCESSORIES', 65)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC109 -- TRAVEL CRATES (EXTRA SMALL)', N'ACCESSORIES', 390)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC110 -- TRAVEL CRATES (LARGE)', N'ACCESSORIES', 1850)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC118 -- WATER FEEDER W/ BOTTLE', N'ACCESSORIES', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC26 -- FOLDABLE CAGE (LARGE)', N'ACCESSORIES', 1400)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC27 -- FOLDABLE CAGE (MEDIUM)', N'ACCESSORIES', 1050)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC29 -- FOLDABLE CAGE (EXTRA-SMALL)', N'ACCESSORIES', 500)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC44 -- MICHIKO NAIL CLIPPER SMALL', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC54 -- NYLON Collar + Lead set (Small)', N'ACCESSORIES', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC55 -- NYLON Collar + Leadset (Large)', N'ACCESSORIES', 145)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC84 -- PVC MUZZLE (SIZE 3)', N'ACCESSORIES', 83.4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC9 -- COLLAR BELL (MEDIUM)', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC94 -- ROPE W/ TEETHER BONE TOY', N'ACCESSORIES', 95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR26 -- NEXGARD SPECTRA 15-30KG 3TABS (VIOLET)', N'ANTI PARASITICS', 593.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR30 -- NEXGARD SPECTRA 7.5-15KG  (GREEN)', N'ANTI PARASITICS', 549)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR37 -- VENOMA 120ML', N'ANTI PARASITICS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR41 -- SIMPARICA 22.1-44.0 lbs (sky blue)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR42 -- SIMPARICA 44.1-88.0 lbs (Green)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR7 -- BRAVECTO (20-40kg)', N'ANTI PARASITICS', 331.66667)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR9 -- BROADLINE for cats  2.5-7.5KG spot on', N'ANTI PARASITICS', 361.6)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF16 -- PEDIGREE PUPPY PER KILO', N'DOG & CAT FOOD', 116)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF18 -- PRINCESS CAT (1KG)', N'DOG & CAT FOOD', 99)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF2 -- BEEFPRO ADULT PER KILO', N'DOG & CAT FOOD', 83)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF23 -- VALUEMEAL  ADULT BAG', N'DOG & CAT FOOD', 1700)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF4 -- BEEFPRO PUPPY PER KILO', N'DOG & CAT FOOD', 92)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED19 -- TRANQUILIZER -CALMIVET-ACEPROMAZINE MALEATE PER INJECTION 10KG AND BELOW', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED20 -- TRANQUILIZER-CALMIVET-ACEPROMAZINE MALEATE PER INJECTION 10KG-15KG', N'INJECTIBLE MEDS', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS11 -- APOQUEIL 16mg', N'MEDICINES', 127)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS115 -- CYSTAID PLUS', N'MEDICINES', 17.95)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS139 -- BLADDER CONTROL PER TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS4 -- ALLERMYL 200ML', N'MEDICINES', 507.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS76 -- ORIDERMYL', N'MEDICINES', 530.90909)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS83 -- PIRANTEL DEWORMER PER TAB', N'MEDICINES', 29.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS94 -- SEBOLYTIC shampoo 200ML', N'MEDICINES', 499.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET14 -- ROYAL CANIN S/O can', N'PRESCRIPTION DIETS', 162.36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET28 -- ROYAL CANIN FELINE S/O 2KG', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET32 -- ROYAL CANIN STARTER CAN', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL11 -- DONO DIAPER EXTRA-SMALL PER PC', N'TOILETRIES', 15)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL14 -- HUSHPET DIAPER LARGE PER PC', N'TOILETRIES', 250)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL18 -- HUSHPET DIAPER SMALL PER PC', N'TOILETRIES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL22 -- PAPI ANTI TICK & FLEA SOAP', N'TOILETRIES', 66)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL23 -- PAPI COAT MAINTENANCE SOAP', N'TOILETRIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL24 -- PAPI GROOM & BLOOM (BLUE) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL26 -- PAPI GROOM & BLOOM (RED) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL39 -- PETSENTIALS PET COLOGNE 100ml', N'TOILETRIES', 125.48)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL5 -- DENTAL HYGIENE KIT', N'TOILETRIES', 401)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL53 -- PETSENTIALLS SHAMPOO 500ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL66 -- MADRE DE CACAO LIQUID SOAP 500ML', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS2 -- DENTASTIX SINGLE', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TREATS9 -- WHISKAS POUCH', N'TREATS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VAX8 -- 8N1', N'VACCINATION', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP13 -- Healthy Coat bottle', N'VITAMINS AND SUPPLEMENTS', 512.64)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP17 -- IMMUNOL BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 448)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP18 -- IMMUNOL PER TAB', N'VITAMINS AND SUPPLEMENTS', 7.46)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP20 -- LC-VIT 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP28 -- METHIOVET PER TAB', N'VITAMINS AND SUPPLEMENTS', 11)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP35 -- NEUROVET PER TAB(vit b complex)', N'VITAMINS AND SUPPLEMENTS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP4 -- BREWERS YEAST CHEWABLE BOTTLE (300TABS)', N'VITAMINS AND SUPPLEMENTS', 473.42917)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP40 -- PAPI Multivitamins syrup 120ML', N'VITAMINS AND SUPPLEMENTS', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP48 -- SHED DEFENSE MAX CHEWABLE BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 456)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP49 -- SHED DEFENSE MAX PER TAB', N'VITAMINS AND SUPPLEMENTS', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP68 -- HI-LITE (dextrose powder)', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP69 -- PUPPY NUTRIGEL', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP7 -- DELTACAL BOTTLE (50TABS)', N'VITAMINS AND SUPPLEMENTS', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'macasandig admission', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'paid', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC19 -- E-COLLAR # 2', N'ACCESSORIES', 115)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC21 -- E-COLLAR #4', N'ACCESSORIES', 70)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC24 -- E-COLLAR #7', N'ACCESSORIES', 40)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC5 -- BITTER BANDAGE ROLL', N'ACCESSORIES', 194)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC66 -- PET BED PAW VELVET (LARGE)', N'ACCESSORIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS1 -- DIPHENHYDRAMINE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS3 -- FUROSEMIDE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS4 -- METOCLOPRAMIDE HCL', N'AMPULES', 9)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS6 -- RANITIDINE AMPULE', N'AMPULES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR16 -- FLEA & TICK SPOT ON FOR DOGS  GREEN(10KG-20KG)', N'ANTI PARASITICS', 249)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR20 -- MILBEMAX 12.5mg', N'ANTI PARASITICS', 117)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR22 -- NEXGARD 10-25KG (VIOLET)', N'ANTI PARASITICS', 381.25)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR25 -- NEXGARD 4-10KG  (BLUE)', N'ANTI PARASITICS', 360.58)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR27 -- NEXGARD SPECTRA 2-3.5KG tab (ORANGE)', N'ANTI PARASITICS', 453.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR28 -- NEXGARD SPECTRA 3.5-7.5KG  (YELLOW)', N'ANTI PARASITICS', 498.75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR31 -- NUTRI VET SPOT-ON (RED)', N'ANTI PARASITICS', 843.49)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR32 -- NutriVet Defense (Blue)', N'ANTI PARASITICS', 792.28792)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR33 -- Nutrivet Defense (Dark Violet)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR34 -- NutriVet Defense (Red)', N'ANTI PARASITICS', 816.95192)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR35 -- Nutrivet Defense (Violet)', N'ANTI PARASITICS', 843.18984)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR36 -- SIMPARICA', N'ANTI PARASITICS', 1380)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ATPR41 -- SIMPARICA 22.1-44.0 lbs (sky blue)', N'ANTI PARASITICS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF13 -- PEDIGREE ADULT PER KILO', N'DOG & CAT FOOD', 86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF14 -- PEDIGREE POUCH', N'DOG & CAT FOOD', 27)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF2 -- BEEFPRO ADULT PER KILO', N'DOG & CAT FOOD', 83)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF24 -- VALUEMEAL  ADULT PER KILO', N'DOG & CAT FOOD', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF26 -- VITALITY  PUPPY PER KILO', N'DOG & CAT FOOD', 136)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF27 -- WHISKAS (1KG)', N'DOG & CAT FOOD', 125)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF36 -- PEDIGREE CAN small', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF5 -- CESAR', N'DOG & CAT FOOD', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF52 -- PRO DIET POUCH', N'DOG & CAT FOOD', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI2 -- D5LRS(DEXTROSE 5%IN LACTATED RINGERS SOLN)', N'FLUIDS', 75)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI4 -- DEXTROSE IN WATER', N'FLUIDS', 71.57143)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED13 -- METRONIDAZOLE VIAL', N'INJECTIBLE MEDS', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED2 -- ATROSITE PER INJECTION UP TO10KG', N'INJECTIBLE MEDS', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED21 -- ACETYLCYSTEINE', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED4 -- CERENIA PER ML', N'INJECTIBLE MEDS', 350)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED6 -- DCM(Dextrose,magnesiumgluconate and calcium borogluconate) injection per ML', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED7 -- DEXAMETHASONE PER ML', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED8 -- DOXYVET', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED9 -- Euthanasia meds', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'LAB27 -- SGPT LIVER TEST', N'LABORATORY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS1 -- ACETYLCYTEINE SACHET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS105 -- LC-SCOUR', N'MEDICINES', 139.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS107 -- LC-DOX SYRUP 120ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS113 -- PREVICOX 57', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS114 -- BESAME 100', N'MEDICINES', 76.8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS116 -- CELLMANAX', N'MEDICINES', 430)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS118 -- BENZYL PENICILLIN', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS119 -- VIBRAVET', N'MEDICINES', 430.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS121 -- METRONIDAZOLE SYRUP 60ML', N'MEDICINES', 18)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS125 -- VET TREAT (enrofloxacin)', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS147 -- PIMOBENDAN TAB 1.25mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS19 -- BRONCURE 60ML', N'MEDICINES', 185)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS2 -- AIR-LAX', N'MEDICINES', 460)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS22 -- CANGLOB P Per ml', N'MEDICINES', 639.45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS26 -- CHARCOAL VET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS27 -- CLINDAMYCIN 150mg', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS35 -- DOXY TABLET', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS36 -- EAR CLEANSE 118ML', N'MEDICINES', 311)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS39 -- ENROFLOX BOX (50TABS)', N'MEDICINES', 563.33333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS40 -- ENROFLOX PER TAB', N'MEDICINES', 10.83333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS45 -- EYE RINSE 118ML', N'MEDICINES', 333)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS46 -- FLAVET 60ML(METRONIDAZOLE)', N'MEDICINES', 85)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS48 -- FUROSEMIDE 20MG TAB', N'MEDICINES', 4)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS5 -- ALUMINUM  HYDROXIDE SYRUP 60ML', N'MEDICINES', 16)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS60 -- LC DOX  tab(Doxycycline 100mg tab)', N'MEDICINES', 12.22)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS61 -- LC DOX BOTTLE (Doxycycline 100mg tab-100pcs)', N'MEDICINES', 1222)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS64 -- Lysine', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS67 -- MENADIONE 10MG tab', N'MEDICINES', 5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS68 -- METHYLERGOMETRINE 125MCG TAB', N'MEDICINES', 3)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS71 -- METRONIDAZOLE 500MG tab', N'MEDICINES', 8)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS76 -- ORIDERMYL', N'MEDICINES', 530.90909)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS77 -- PAPI SCOUR 60ML', N'MEDICINES', 110)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS79 -- PET REBOOST', N'MEDICINES', 214.28571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS85 -- PREDNISONE 10mg TAB', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS86 -- PREDNISONE 5MG', N'MEDICINES', 1)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS87 -- PREDNISONE 60ML SYRUP', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS90 -- PUPPY BOOST', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS94 -- SEBOLYTIC shampoo 200ML', N'MEDICINES', 499.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS97 -- TERBUTALINE', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'PROC27 -- Nebulization with meds.', N'PROCEDURE', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET11 -- ROYAL CANIN HEPATIC can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET12 -- ROYAL CANIN RECOVERY can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET13 -- ROYAL CANIN RENAL can', N'PRESCRIPTION DIETS', 148.59)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET15 -- ROYAL CANIN SATIETY DIET  2KG', N'PRESCRIPTION DIETS', 951)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET22 -- Vet Solution Renal per 1kg', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET23 -- ROYAL CANIN KITTEN 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET24 -- ROYAL CANIN INSTINCTIVE 85g', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET29 -- DR. CLAUDERS', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET8 -- ROYAL CANIN GASTROINTESTINAL', N'PRESCRIPTION DIETS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SED2 -- ZOLETIL BOTTLE 5ML', N'SEDATIVE/TRANQUILIZER', 1100)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER21 -- HEAT REGULATION & HEATING PAD', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER35 -- SCABICIDE TX  0-3kg', N'SERVICES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL15 -- CHROMIC CATGUT 2/0', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL36 -- NASAL OXYGEN TUBE INSERTION,SUPPLIES AND LOCAL ANESTHETICS', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL37 -- NEBULIZER KIT', N'SUPPLIES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL44 -- PDO SOTURE 1-0', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL5 -- Bitter Bandage', N'SUPPLIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL51 -- SILK (non absorbable suture)', N'SUPPLIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL62 -- SURGICAL STAPLER', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SURG44 -- PF RECTAL PROLAPSE CORRECTION', N'SURGERY', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TEST9 -- Ehrlichia-Anaplasma-Babesia-Heartworm Test', N'TEST KITS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL24 -- PAPI GROOM & BLOOM (BLUE) 500ML', N'TOILETRIES', 163)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL40 -- PETSENTIALS STAIN & ODOR 500ml', N'TOILETRIES', 237.52)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL51 -- MEOWTECH CAT LITTER', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN1 -- MACROSET(adult)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP1 -- 1-TDC 120SOFT GELS', N'VITAMINS AND SUPPLEMENTS', 35)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP10 -- ENER-G 60ML(probiotics syrup)', N'VITAMINS AND SUPPLEMENTS', 150)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP11 -- ENMALAC 120ML(lactation enhancer)', N'VITAMINS AND SUPPLEMENTS', 175)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP19 -- IRON PLUS 120ML', N'VITAMINS AND SUPPLEMENTS', 177)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP20 -- LC-VIT 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP22 -- LIV-52 BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP23 -- LIV-52 PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP24 -- LIVOTINE 120ML', N'VITAMINS AND SUPPLEMENTS', 168)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP26 -- LIV-WELL 60ML', N'VITAMINS AND SUPPLEMENTS', 199)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP32 -- NEFROTEC BOTTLE (60TABS)', N'VITAMINS AND SUPPLEMENTS', 412)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP33 -- NEFROTEC PER TAB', N'VITAMINS AND SUPPLEMENTS', 6.86)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP36 -- NUTRI TIER GEL', N'VITAMINS AND SUPPLEMENTS', 280)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP43 -- PROBIOTICS PER CAP', N'VITAMINS AND SUPPLEMENTS', 17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP44 -- PUPPY VITE GEL', N'VITAMINS AND SUPPLEMENTS', 445)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP5 -- BREWERS YEAST CHEWABLE PER TAB', N'VITAMINS AND SUPPLEMENTS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP50 -- ULTRALITE SACHET(ORAL REHYDRATION)', N'VITAMINS AND SUPPLEMENTS', 12.61481)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP59 -- HEMACARE-FE 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP61 -- GLUCOSAMINE PLUS TAB', N'VITAMINS AND SUPPLEMENTS', 20.2)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP62 -- HEALTHY LIVER BOTTLE', N'VITAMINS AND SUPPLEMENTS', 947)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP63 -- HEALTHY LIVER TAB', N'VITAMINS AND SUPPLEMENTS', 11.87)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP7 -- DELTACAL BOTTLE (50TABS)', N'VITAMINS AND SUPPLEMENTS', 220)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP75 -- ASCORBIC ACID 120ml', N'VITAMINS AND SUPPLEMENTS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP9 -- DUPHALYTE Per 1ml', N'VITAMINS AND SUPPLEMENTS', 571)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SURGERY', N'NULL', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC20 -- E-COLLAR # 3', N'ACCESSORIES', 90)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC22 -- E-COLLAR # 5', N'ACCESSORIES', 55)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC23 -- E-COLLAR #6', N'ACCESSORIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'ACC27 -- FOLDABLE CAGE (MEDIUM)', N'ACCESSORIES', 1050)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS2 -- EPINEPHRINE', N'AMPULES', 60)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS5 -- PHYTOMENADIONE', N'AMPULES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'AMPS9 -- TRANEXAMIC ACID AMPULE', N'AMPULES', 36)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF19 -- PYRAMID HILL BEEF', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'DCF20 -- PYRAMID HILL LAMB', N'DOG & CAT FOOD', 69.17)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI1 -- .9% SODIUM CHLORIDE DEXTROSE(PLAIN NSS)', N'FLUIDS', 80)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'FLUI3 -- DEXTROSE IN LACTATED RINGERS SOLN.', N'FLUIDS', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED1 -- ATROPINE', N'INJECTIBLE MEDS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED12 -- MARBOFLOXACIN PER ML', N'INJECTIBLE MEDS', 26)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED14 -- FERCOBSANG', N'INJECTIBLE MEDS', 10)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED16 --  TOLFINE PER ML', N'INJECTIBLE MEDS', 21)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'INJMED2 -- ATROSITE PER INJECTION UP TO10KG', N'INJECTIBLE MEDS', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS100 -- TOLFEDINE TAB 60MG TAB', N'MEDICINES', 34.13636)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS101 -- TOLFENOL 30ml', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS102 -- TOPI DERM 20G', N'MEDICINES', 160)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS112 -- PREVICOX 227', N'MEDICINES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS25 -- CEFALEXIN', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS30 -- CO-AMOX TAB', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS31 -- CO-AMOXICLAV 250MG 60ML', N'MEDICINES', 170)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS34 -- DOXY 60ML', N'MEDICINES', 179)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS51 -- IOZIN 120ML', N'MEDICINES', 0)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'MEDS74 -- Neovax 20G', N'MEDICINES', 480)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'RXDIET32 -- ROYAL CANIN STARTER CAN', N'PRESCRIPTION DIETS', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SER34 -- PF,SURGERY AND WOUND SUTURING', N'SERVICES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL12 -- CATGUT', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL15 -- CHROMIC CATGUT 2/0', N'SUPPLIES', 50)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL25 -- ELASTIC BANDAGE', N'SUPPLIES', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL34 -- IV CANNULA', N'SUPPLIES', 20)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL51 -- SILK (non absorbable suture)', N'SUPPLIES', 45)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL62 -- SURGICAL STAPLER', N'SUPPLIES', 600)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL64 -- SYRINGES', N'SUPPLIES', 2.5)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'SUPPL67 -- VICRYL (90CM)', N'SUPPLIES', 300)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL31 -- PET SHEET (Large)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL32 -- PET SHEET (Small)', N'TOILETRIES', NULL)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'TOIL39 -- PETSENTIALS PET COLOGNE 100ml', N'TOILETRIES', 125.48)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VEN2 -- MICROSET(pedia)', N'VENOSET', 30)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP29 -- NACALVIT-C 120ML', N'VITAMINS AND SUPPLEMENTS', 140)
GO
INSERT [dbo].[CDO_Items] ([Item], [Category], [BuyingPrice]) VALUES (N'VITSUP39 -- NUTRI-PLUS GEL 120G', N'VITAMINS AND SUPPLEMENTS', 435)
GO
