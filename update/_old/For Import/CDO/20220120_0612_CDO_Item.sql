DECLARE @CategoryTempTable TABLE
  (
     ID       INT IDENTITY(1, 1) primary key,
     Category VARCHAR(200)
  )
DECLARE @ItemsTempTable TABLE
  (
     ID       INT IDENTITY(1, 1) primary key,
     Item     VARCHAR(200),
     Price    INT,
     Category INT
  )
DECLARE @CDO_ID_Company INT = 241
DECLARE @Inventoriable_ID_ItemType INT = 1
DECLARE @ID_ItemType INT =2
DECLARE @IsActive INT =1
DECLARE @Comment VARCHAR(1000)='Imported 01-19-2022 6:00am'
DECLARE @ID_CreatedBy INT =1
DECLARE @ID_LastModifiedBy INT =1

INSERT INTO @CategoryTempTable
            (Category)
SELECT DISTINCT [Category]
FROM   CDO_Items WHERE Category IS NOT NULL AND Category <> 'NULL'

DELETE E
FROM   @CategoryTempTable E
       INNER JOIN (SELECT *,
                          RANK()
                            OVER(
                              PARTITION BY [Category]
                              ORDER BY ID) rank
                   FROM   @CategoryTempTable) T
               ON E.ID = t.ID
WHERE  rank > 1;

DELETE FROM @CategoryTempTable
WHERE  Category IN (SELECT Name
                    FROM   tItemCategory
                    WHERE  ID_ItemType = 2)

DELETE FROM @CategoryTempTable
WHERE  Category IS NULL

INSERT INTO [dbo].[tItemCategory]
            ([Name],
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT category.Category,
       @IsActive,
       @CDO_ID_Company,
       GETDATE(),
       GETDATE(),
       @ID_CreatedBy,
       @ID_LastModifiedBy,
       @ID_ItemType
FROM   @CategoryTempTable category WHERE category.Category NOT IN (
SELECT Name FROM tItemCategory WHERE ID_ItemType = @ID_ItemType
)

--Insert To Temporary Table
INSERT INTO @ItemsTempTable
            (Item,
             Price,
             Category)
SELECT DISTINCT import.[Item],
                MAX(ISNULL(import.[BuyingPrice], 0)),
                MAX(a.ID)
FROM   CDO_Items import
       LEFT JOIN tItemCategory a
               ON import.Category = a.Name
GROUP  BY import.[Item]

INSERT INTO [dbo].[tItem]
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [UnitPrice])
SELECT DISTINCT import.Item,
                @IsActive,
                @CDO_ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                @ID_CreatedBy,
                @ID_LastModifiedBy,
                @ID_ItemType,
                import.Category,
                import.Price
FROM   @ItemsTempTable import

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession Int

select TOP 1 @ID_UserSession = ID
FROm   tUserSession
where  ID_User = 10
ORDER  BY ID DESC

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT @Comment,
       item.id,
       0,
       0.00,
       NULL,
       NULL,
       3,
       @CDO_ID_Company,
       @Comment,
       1
FRom   titem item
       inner join @ItemsTempTable import
               on item.Name = import.[ITEM]
WHERE  ID_COMpany = @CDO_ID_Company
       and Comment = @Comment
       and ISNULL(0, 0) > 0

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT Name,
       Count(*)
FROM   tItem
WHERE  ID_Company = 241
GROUP  BY Name
HAVING COUNT(*) > 1 

SELECT *
FROM   tItem
WHERE  ID_Company = 241