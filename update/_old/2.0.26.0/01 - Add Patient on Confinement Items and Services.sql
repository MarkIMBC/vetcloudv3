EXEC _pAddModelProperty
  'tPatient_Confinement_ItemsServices',
  'ID_Patient',
  2

EXEC _pRefreshAllViews

GO

ALTER VIEW vPatient_Confinement_ItemsServices  
AS  
  SELECT H.*,  
         UC.Name                               AS CreatedBy,  
         UM.Name                               AS LastModifiedBy,  
         item.Name                             AS Name_Item,  
         REPLACE(H.Comment, CHAR(13), '<br/>') AS CommentHTML,  
         item.ID_ItemType                      AS ID_ItemType  ,
		 patient.Name Name_Patient
  FROM   tPatient_Confinement_ItemsServices H  
         LEFT JOIN tUser UC  
                ON H.ID_CreatedBy = UC.ID  
         LEFT JOIN tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
         LEFT JOIN tItem item  
                ON H.ID_Item = item.ID     LEFT JOIN tPatient patient
                ON H.ID_Patient= patient.ID   
GO
ALTER PROC [dbo].[pPatient_Confinement_Validation] (@ID_Patient_Confinement INT,
                                                    @ID_Patient             INT,
                                                    @ID_Patients            TYPINTLIST READONLY,
                                                    @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConfinedPatientCount INT = 0

      BEGIN TRY
          IF( ISNULL(@ID_Patient, 0) > 0 )
            BEGIN
                SELECT @ConfinedPatientCount = Count(*)
                FROM   tPatient_Confinement
                WHERE  ID NOT IN ( @ID_Patient_Confinement )
                       AND ID_Patient = @ID_Patient
                       AND ID_FilingStatus = @Confined_ID_FilingStatus

                IF ( @ConfinedPatientCount > 0 )
                  BEGIN
                      SET @message = 'Selected Patient is already confined. Please discharge the patient first.';

                      THROW 50001, @message, 1;
                  END;
            END
          ELSE IF (SELECT Count(*)
              FROM   @ID_Patients) > 0
            BEGIN
                SELECT @ConfinedPatientCount = Count(*)
                FROM   tPatient_Confinement_Patient confPatient
                       INNER JOIN tPatient_Confinement confi
                               ON confPatient.ID_Patient_Confinement = confi.ID
                       INNER JOIN @ID_Patients patientIDs
                               ON confPatient.ID_Patient = patientIDs.ID
                WHERE  confi.ID NOT IN ( @ID_Patient_Confinement )
                       AND ID_FilingStatus = @Confined_ID_FilingStatus

                IF ( @ConfinedPatientCount > 0 )
                  BEGIN
                      SET @message = 'The patients is already confined. Please discharge them first:';

                      SELECT @message = @message + Char(13) + Char(10)
                                        + confPatient.Name_Patient + ' (' + confi.Code
                                        + '} '
                      FROM   vPatient_Confinement_Patient confPatient
                             INNER JOIN vPatient_Confinement confi
                                     ON confPatient.ID_Patient_Confinement = confi.ID
                             INNER JOIN @ID_Patients patientIDs
                                     ON confPatient.ID_Patient = patientIDs.ID
                      WHERE  confi.ID NOT IN ( @ID_Patient_Confinement )
                             AND ID_FilingStatus = @Confined_ID_FilingStatus;

                      THROW 50001, @message, 1;
                  END;
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END 
