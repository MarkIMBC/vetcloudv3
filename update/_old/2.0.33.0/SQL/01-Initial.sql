ALTER FUNCTION fGetConfinmentDeposit (@ID_Patient_Confinement INT,
                                      @IDs_Patient            typIntList Readonly)
RETURNS DECIMAL(18, 4)
AS
  BEGIN
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3

      IF(SELECT COUNT(*)
         FROM   @IDs_Patient) = 0
        BEGIN
            SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
            FROM   tClientDeposit
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END
      ELSE
        BEGIN
            SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
            FROM   tClientDeposit clientDeposit
                   inner join @IDs_Patient idsPatient
                           on clientDeposit.ID_Patient = idsPatient.ID
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END

      RETURN @ConfinementDepositAmount
  END

GO

CREATE OR
ALTER VIEW dbo.vActiveItemService
AS
  SELECT *
  FROM   dbo.vItemService
  Where  IsActive = 1;

GO

CREATE OR
ALTER VIEW dbo.vActiveItemInventoriable
AS
  SELECT *
  FROM   dbo.vItemInventoriable
  Where  IsActive = 1;

GO

CREATE OR
ALTER PROC [dbo].[pGetConfinmentDepositByPatients](@ID_Patient_Confinement int,
                                                   @IDs_Patient            typIntList Readonly)
AS
  BEGIN
      DECLARE @CurrentCreditAmount Decimal(18, 4)= 0
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @RemainingDepositAmount Decimal(18, 4)= 0
      DECLARE @ID_Client INT = 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @MaxDate_ClientDeposit DateTime
      DECLARE @ClientDeposit TABLE
        (
           ID                INT,
           Date              DateTIme,
           Code              VARCHAR(50),
           Name_Patient      VARCHAR(MAX),
           DepositAmount     Decimal(18, 4),
           Name_FilingStatus VARCHAR(MAX)
        )

      SELECT @ID_Client = ID_Client
      FROM   tPatient_Confinement
      where  ID = @ID_Patient_Confinement

      IF(SELECT COUNT(*)
         FROM   @IDs_Patient) = 0
        BEGIN
            SELECT @CurrentCreditAmount = client.CurrentCreditAmount
            FROM   tclient client
            WHERE  ID = @ID_Client

            SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement, @IDs_Patient);
            SET @RemainingDepositAmount = ISNULL(@CurrentCreditAmount, 0) - ISNULL(@ConfinementDepositAmount, 0)

            if( @RemainingDepositAmount < 0 )
              SET @RemainingDepositAmount = 0

            SELECT @MaxDate_ClientDeposit = MAX(Date)
            FROm   tClient_CreditLogs
            where  ID_Client = @ID_Client
                   AND Code NOT IN (SELECT Code
                                    FROM   @ClientDeposit)

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount)
            SELECT NULL,
                   @MaxDate_ClientDeposit,
                   'Remaining',
                   '',
                   @RemainingDepositAmount

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount,
                    Name_FilingStatus)
            SELECT ID,
                   Date,
                   Code,
                   Name_Patient,
                   DepositAmount,
                   Name_FilingStatus
            FROM   dbo.vClientDeposit
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END
      else
        begin
            SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement, @IDs_Patient);
            SET @RemainingDepositAmount = ISNULL(@ConfinementDepositAmount, 0)
            SET @CurrentCreditAmount = ISNULL(@RemainingDepositAmount, 0)

            if( @RemainingDepositAmount < 0 )
              SET @RemainingDepositAmount = 0

            INSERT @ClientDeposit
                   (ID,
                    Date,
                    Code,
                    Name_Patient,
                    DepositAmount,
                    Name_FilingStatus)
            SELECT clientDeposit.ID,
                   Date,
                   clientDeposit.Code,
                   patient.Name Name_Patient,
                   DepositAmount,
                   fs.Name
            FROM   dbo.tClientDeposit clientDeposit
                   INNER JOIN tPatient patient
                           on patient.ID = clientDeposit.ID_Patient
                   inner join @IDs_Patient idsPatient
                           on idsPatient.ID = clientDeposit.ID_Patient
                   INNER JOIN tFilingStatus fs
                           on fs.ID = clientDeposit.ID_FilingStatus
            WHERE  clientDeposit.ID_Patient_Confinement = @ID_Patient_Confinement
                   AND clientDeposit.ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END

      SELECT '_',
             '' AS ClientDeposits;

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             @RemainingDepositAmount              RemainingDepositAmount,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount,
             ISNULL(@CurrentCreditAmount, 0)      TotalDepositAmount;

      SELECT *
      FROM   @ClientDeposit
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetConfinmentDeposit](@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @IDs_Patient typIntlist

      exec pGetConfinmentDepositByPatients
        @ID_Patient_Confinement,
        @IDs_Patient
  END

GO

CREATE OR
ALTER PROC pValidateOnRemoving_Patient_Confinement_Patient (@ID_Patient_Confinement INT,
                                                            @ID_Patient             INT)
AS
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(400) = '';

      BEGIN TRY
          DECLARE @msg VARCHAR(400) = '';
          Declare @hasApprovedClientDeposit TABLE
            (
               Code_ClientDeposit VARCHAR(Max),
               DepositAmount      Decimal(18, 2),
               Name_Patient       VARCHAR(MAX),
               Name_FilingStatus  VARCHAR(MAX)
            )
          Declare @Count_hasApprovedClientDeposit INT

          INSERT @hasApprovedClientDeposit
          SELECT Distinct cDeposit.Code,
                          cDeposit.DepositAmount,
                          patient.Name Name_Patient,
                          fs.Name      Name_FilingStatus
          FROm   tClientDeposit cDeposit
                 left join tpatient patient
                        on cDeposit.ID_Patient = patient.ID
                 LEFT JOIN tFilingStatus fs
                        on fs.ID = cDeposit.ID_FilingStatus
          where  cDeposit.ID_FilingStatus IN ( 3 )
                 and cDeposit.ID_Patient_Confinement = @ID_Patient_Confinement
                 and cDeposit.ID_Patient = @ID_Patient

          SELECT @Count_hasApprovedClientDeposit = count(*)
          FROm   @hasApprovedClientDeposit

          IF @Count_hasApprovedClientDeposit > 0
            BEGIN
                SET @msg = 'The following Patient'
                           + CASE
                               WHEN @Count_hasApprovedClientDeposit > 1 THEN 's are '
                               ELSE ' is '
                             END
                           + 'not allowed to removed:';

                SELECT @msg = @msg + CHAR(10) + Code_ClientDeposit + ' - '
                              + Name_FilingStatus
                FROM   @hasApprovedClientDeposit;

                THROW 50001, @msg, 1;
            END
      ------------------------------------------------------------------------------
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message
  END

GO 
