go

exec _pAddModelProperty
  'tBillingInvoice_Detail',
  'tempID',
  1

go

exec _pAddModelProperty
  'tBillingInvoice_Patient',
  'tempID',
  1

go

exec _pAddModelProperty
  'tBillingInvoice',
  'tempID',
  1

go

exec _pAddModelProperty
  'tEmployee',
  'tempID',
  1

go

exec _pAddModelProperty
  'tInventoryTrail',
  'tempID',
  1

go

exec _pAddModelProperty
  'tItem',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_Confinement_Patient',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_Confinement',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_SOAP_Plan',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_SOAP_Prescription',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_SOAP_Treatment',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient_SOAP',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPatient',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPaymentTransaction',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPurchaseOrder_Detail',
  'tempID',
  1

go

exec _pAddModelProperty
  'tPurchaseOrder',
  'tempID',
  1

go

exec _pAddModelProperty
  'tReceivingReport_Detail',
  'tempID',
  1

go

exec _pAddModelProperty
  'tReceivingReport',
  'tempID',
  1

go

ALTER TABLE tClient
  ALTER COLUMN Address [varchar](8000) NULL

GO

ALTER PROC [dbo].[pCreateClientUser] (@ID_Client INT,
                                      @Username  VARCHAR(100),
                                      @Password  VARCHAR(100))
AS
  BEGIN
      DECLARE @ClientName VARCHAR(100)
      DECLARE @IsActive INT=1;
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @ID_UserGroup INT=9;
      DECLARE @ID_LastModifiedBy INT=1;
      DECLARE @ID_CreatedBy INT=1;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SET @ClientName=(SELECT Name
                       FROM   tClient
                       WHERE  ID = @ID_Client)

      BEGIN TRY
          INSERT INTO [dbo].[tUser]
                      ([Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [Username],
                       [ID_UserGroup],
                       [Password])
          VALUES      (@ClientName,
                       @IsActive,
                       @DateCreated,
                       @DateModified,
                       @ID_CreatedBy,
                       @ID_LastModifiedBy,
                       @Username,
                       @ID_UserGroup,
                       @Password)

          Update tClient
          SET    ID_User = @@IDENTITY
          WHERE  ID = @ID_Client
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;
  END;

GO

ALTER VIEW [dbo].[vItemInventoriable]
AS
  SELECT H.*,
         UC.Name                                               AS CreatedBy,
         UM.Name                                               AS LastModifiedBy,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         CASE
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
           ELSE ''
         END                                                   RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays
  FROM   dbo.tItem H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
  WHERE  H.ID_ItemType = 2;

GO

ALTER VIEW [dbo].[vPurchaseOrder_Detail]
as
  SELECT DET.*,
         I.Name                   AS Item,
         UOM.Name                 AS UOM,
         I.Name                   AS Name_Item,
         UOM.Name                 AS Name_UOM,
         purchaseOrder.ID_Company Main_ID_Company
  FROM   tPurchaseOrder_Detail DET
         LEFT JOIN tItem I
                ON DET.ID_Item = I.ID
         LEFT JOIN tUnitOfMeasure UOM
                ON DET.ID_UOM = UOM.ID
         LEFT JOIN vPurchaseOrder purchaseOrder
                on purchaseOrder.ID = DET.ID_PurchaseOrder

GO

EXEC _pRefreshAllViews

GO 
