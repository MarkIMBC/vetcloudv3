IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tSMSPatientSOAP_Company'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tSMSPatientSOAP_Company', 0, 0, NULL
END


GO

IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tPatient_SOAP_Prescription'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tPatient_SOAP_Prescription', 0, 0, NULL
END


GO



IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tPatient_SOAP_SMSStatus'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tPatient_SOAP_SMSStatus', 0, 0, NULL
END

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP_Prescription'
    AND COLUMN_NAME = 'ID_Item'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP_Prescription'
                         ,'ID_Item'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP_Prescription'
    AND COLUMN_NAME = 'Quantity'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP_Prescription'
                         ,'Quantity'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP_Prescription'
    AND COLUMN_NAME = 'ID_Patient_SOAP'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP_Prescription'
                         ,'ID_Patient_SOAP'
                         , 2

	exec _pAddModelDetail 'tPatient_SOAP_Prescription' , 'ID_Patient_SOAP', 'tPatient_SOAP'
END;

GO


IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP_SMSStatus'
    AND COLUMN_NAME = 'iTextMo_Status'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP_SMSStatus'
                         ,'iTextMo_Status'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP_SMSStatus'
    AND COLUMN_NAME = 'ID_Patient_SOAP'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP_SMSStatus'
                         ,'ID_Patient_SOAP'
                         , 2
END;

GO

CREATE OR ALTER FUNCTION [dbo].[fGetPatient_SOAP_Prescription_String]
(
    @ID_Patient_SOAP INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

    DECLARE @Rows TABLE
    (
        Name_Item VARCHAR(MAX),
		Quantity INT,
        Comment VARCHAR(MAX)
    );

    DECLARE @prescription VARCHAR(MAX) = '';
    DECLARE @msg VARCHAR(MAX) = '';

	SELECT  
		@prescription = ISNULL(Prescription, '')
	FROM    tPatient_SOAP 
	WHERE
		ID = @ID_Patient_SOAP

    INSERT @Rows
    (
        Name_Item,
		Quantity,
        Comment
    )
    SELECT
           Name_Item,
		   Quantity,
           Comment
    FROM dbo.vPatient_SOAP_Prescription
    WHERE ID_Patient_SOAP = @ID_Patient_SOAP

    SELECT @msg
        = @msg + '' +ISNULL(Name_Item, '') + CASE WHEN ISNULL(Quantity, 0) > 0 THEN ' Qty: ' + FORMAT(ISNULL(Quantity, 0), '#,#0') ELSE ' ' END + '<br/>'
          + CASE
                WHEN LEN(ISNULL(Comment, '')) > 0 THEN
                    '&nbsp;&nbsp;&nbsp;&nbsp;- '
                ELSE
                    ''
            END + ISNULL(Comment, '') + '<br/><br/>'
    FROM @Rows;

	SET @msg = @msg + '<br/>'
	SET @msg = @msg + @prescription
	SET @msg = REPLACE(REPLACE(REPLACE(@msg, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') 

    RETURN @msg;
END;

Go


CREATE OR ALTER FUNCTION dbo.fGetSOAPLANMessage (
	@CompanyName Varchar(MAX), 
	@Client Varchar(MAX),
	@ContactNumber Varchar(MAX),
	@Pet Varchar(MAX),
	@Service Varchar(MAX),
	@DateReturn DateTime
)
RETURNS VARCHAR(MAX)
BEGIN

	Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')

	Declare @message  Varchar(MAX) = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.
Pls. contact /*CompanyName*/ /*ContactNumber*/
'

	SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
	SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
	SET @message = REPLACE(@message, '/*ContactNumber*/',LTRIM(RTRIM( @ContactNumber)))
	SET @message = REPLACE(@message, '/*Pet*/',LTRIM(RTRIM( @Pet)))
	SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
	SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))

	RETURN @message
END

GO

CREATE OR ALTER VIEW [dbo].[vPatient_SOAP_Prescription] AS 
	SELECT 
		H.* ,
		UC.Name AS CreatedBy,
		UM.Name AS LastModifiedBy,
		item.Name Name_Item
	FROM tPatient_SOAP_Prescription H
		LEFT JOIN 
			tUser UC ON H.ID_CreatedBy = UC.ID	
		LEFT JOIN 
			tUser UM ON H.ID_LastModifiedBy = UM.ID
		LEFT JOIN 
			tItem item ON H.ID_Item = item.ID
GO

ALTER   VIEW [dbo].[vzPatientSOAP]
AS
	SELECT
	  patientSOAP.ID
	 ,patientSOAP.Code
	 ,patientSOAP.Name_Client
	 ,patientSOAP.Name_Patient
	 ,patientSOAP.Date
	 ,patientSOAP.Comment
	 ,patientSOAP.Name_CreatedBy
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.History, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') History
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClinicalExamination, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClinicalExamination
	 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Diagnosis, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Diagnosis
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Treatment, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Treatment
	 ,dbo.fGetPatient_SOAP_Prescription_String(patientSOAP.ID) Prescription
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClientCommunication, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClientCommunication
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Subjective
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Objective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
	 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
	 ,patientSOAP.ID_Company
	 ,patientSOAP.Name_FilingStatus
	 ,company.ImageLogoLocationFilenamePath
	 ,company.Name Name_Company
	 ,company.Address Address_Company
	 ,company.ContactNumber ContactNumber_Company
	 ,CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	  CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	  CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM dbo.vPatient_SOAP patientSOAP
	LEFT JOIN dbo.vCompany company
	  ON company.ID = patientSOAP.ID_Company;

GO

Create OR ALTER Proc pDeleteCompanyAccount(@ID_Company INT)
as
BEGIN

	Declare @IDs_User typIntList 
	Declare @IDs_Employee typIntList 

	INSERT @IDs_User
	SELECT  
		DISTINCT
		ID
	FROM    vUser 
	WHERE ID_Company =  @ID_Company

	INSERT @IDs_Employee
	SELECT  
		DISTINCT
		ID_Employee
	FROM    vUser 
	WHERE ID_Company =  @ID_Company

	Update tUserSession SET ID_User = 1 WHERE ID_User IN (SELECT ID FROM @IDs_User)  /*TODO*/
	Update tCompany set	ID_LastModifiedBy = 1, ID_CreatedBy = 1 WHERE ID = @ID_Company
	DELETE FROM tUser_Roles WHERE  ID_User IN (SELECT  ID FROM @IDs_User)
	DELETE FROM tUser WHERE  ID IN (SELECT  ID FROM @IDs_User)
	DELETE FROM tEmployee WHERE  ID IN (SELECT  ID FROM @IDs_Employee)
	Update tCompany SET IsActive = 0, Comment = ISNULL(Comment,'') + 'Deleted User Access - ' + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt') WHERE ID = @ID_Company

END

GO

CREATE OR ALTER PROCEDURE [dbo].[pGetPatient_SOAP] @ID INT = -1,
	@ID_Patient INT = NULL,
	@ID_SOAPType INT = NULL,
	@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'
   ,'' AS LabImages
   ,'' AS Patient_SOAP_Plan
   ,'' AS Patient_SOAP_Prescription;

  DECLARE @ID_User INT;
  DECLARE @ID_Warehouse INT;
  DECLARE @FILED_ID_FilingStatus INT = 1;
  DECLARE @IsDeceased BIT = 1;

  DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): '+ CHAR(9) + CHAR(13) + 
											'Respiratory Rate (brpm): '+ CHAR(9) + CHAR(13) + 	
											'Weight (kg): '+ CHAR(9) + CHAR(13) + 	
											'Length (cm): '+ CHAR(9) + CHAR(13) + 
											'CRT: '+ CHAR(9) + CHAR(13) + 
											'BCS: '+ CHAR(9) + CHAR(13) + 	
											'Lymph Nodes: '+ CHAR(9) + CHAR(13) + 
											'Palpebral Reflex: '+ CHAR(9) + CHAR(13) + 
											'Temperature: '+ CHAR(9) + CHAR(13)

  DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): '+ CHAR(9) + CHAR(13) + 
											'Respiratory Rate (brpm): '+ CHAR(9) + CHAR(13) + 	
											'Weight (kg): '+ CHAR(9) + CHAR(13) + 	
											'Length (cm): '+ CHAR(9) + CHAR(13) + 
											'CRT: '+ CHAR(9) + CHAR(13) + 
											'BCS: '+ CHAR(9) + CHAR(13) + 	
											'Lymph Nodes: '+ CHAR(9) + CHAR(13) + 
											'Palpebral Reflex: '+ CHAR(9) + CHAR(13) + 
											'Temperature: '+ CHAR(9) + CHAR(13)

  DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Notes: '+ CHAR(9) + CHAR(13) + 		
											'Test Results: '+ CHAR(9) + CHAR(13) + 		
											'Final Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Prognosis: '+ CHAR(9) + CHAR(13) + 	
											'Category: '+ CHAR(9) + CHAR(13) 

  DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Notes: '+ CHAR(9) + CHAR(13) + 		
											'Test Results: '+ CHAR(9) + CHAR(13) + 		
											'Final Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Prognosis: '+ CHAR(9) + CHAR(13) + 	
											'Category: '+ CHAR(9) + CHAR(13) 

  DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: '+ CHAR(9) + CHAR(13) + 	
											'  Wbc= '+ CHAR(9) + CHAR(13) + 	
											'  Lym= '+ CHAR(9) + CHAR(13) + 	
											'  Mon= '+ CHAR(9) + CHAR(13) + 	
											'  Neu= '+ CHAR(9) + CHAR(13) + 	
											'  Eos= '+ CHAR(9) + CHAR(13) + 	
											'  Bas= '+ CHAR(9) + CHAR(13) + 	
											'  Rbc= '+ CHAR(9) + CHAR(13) + 	
											'  Hgb= '+ CHAR(9) + CHAR(13) + 	
											'  Hct= '+ CHAR(9) + CHAR(13) + 	
											'  Mcv= '+ CHAR(9) + CHAR(13) + 	
											'  Mch= '+ CHAR(9) + CHAR(13) + 	
											'  Mchc '+ CHAR(9) + CHAR(13) + 
											'  Plt= '+ CHAR(9) + CHAR(13) + 	
											'  Mpv= '+ CHAR(9) + CHAR(13) + CHAR(9) + CHAR(13) +	
											'Blood Chem: '+ CHAR(9) + CHAR(13) + 	
											'  Alt= '+ CHAR(9) + CHAR(13) + 	
											'  Alp= '+ CHAR(9) + CHAR(13) + 	
											'  Alb= '+ CHAR(9) + CHAR(13) + 	
											'  Amy= '+ CHAR(9) + CHAR(13) + 	
											'  Tbil= '+ CHAR(9) + CHAR(13) + 	
											'  Bun= '+ CHAR(9) + CHAR(13) + 	
											'  Crea= '+ CHAR(9) + CHAR(13) + 	
											'  Ca= '+ CHAR(9) + CHAR(13) + 	
											'  Phos= '+ CHAR(9) + CHAR(13) + 	
											'  Glu= '+ CHAR(9) + CHAR(13) + 	
											'  Na= '+ CHAR(9) + CHAR(13) + 	
											'  K= '+ CHAR(9) + CHAR(13) + 	
											'  TP= '+ CHAR(9) + CHAR(13) + 	
											'  Glob= '+ CHAR(9) + CHAR(13) +  CHAR(9) + CHAR(13) + 	 	
											'Microscopic Exam: '+ CHAR(9) + CHAR(13) 

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM dbo.tUserSession
  WHERE ID = @ID_Session;

  SELECT  @IsDeceased = Isnull(IsDeceased,0)
  FROM tPatient WHERE ID = @ID_Patient;
  
  DECLARE @LabImage TABLE (
    ImageRowIndex INT
   ,RowIndex INT
   ,ImageNo VARCHAR(MAX)
   ,FilePath VARCHAR(MAX)
   ,Remark VARCHAR(MAX)
  );

  INSERT @LabImage (ImageRowIndex, RowIndex, ImageNo, FilePath, Remark)
  SELECT
    c.ImageRowIndex
   ,ROW_NUMBER() OVER (ORDER BY ImageNo) AS RowIndex
   ,c.ImageNo
   ,FilePath
   ,c.Remark
  FROM tPatient_SOAP ps
  CROSS APPLY (SELECT
      '01'
     ,LabImageRowIndex01
     ,LabImageFilePath01
     ,LabImageRemark01
    UNION ALL
    SELECT
      '02'
     ,LabImageRowIndex02
     ,LabImageFilePath02
     ,LabImageRemark02
    UNION ALL
    SELECT
      '03'
     ,LabImageRowIndex03
     ,LabImageFilePath03
     ,LabImageRemark03
    UNION ALL
    SELECT
      '04'
     ,LabImageRowIndex04
     ,LabImageFilePath04
     ,LabImageRemark04
    UNION ALL
    SELECT
      '05'
     ,LabImageRowIndex05
     ,LabImageFilePath05
     ,LabImageRemark05
    UNION ALL
    SELECT
      '06'
     ,LabImageRowIndex06
     ,LabImageFilePath06
     ,LabImageRemark06
    UNION ALL
    SELECT
      '07'
     ,LabImageRowIndex07
     ,LabImageFilePath07
     ,LabImageRemark07
    UNION ALL
    SELECT
      '08'
     ,LabImageRowIndex08
     ,LabImageFilePath08
     ,LabImageRemark08
    UNION ALL
    SELECT
      '09'
     ,LabImageRowIndex09
     ,LabImageFilePath09
     ,LabImageRemark09
    UNION ALL
    SELECT
      '10'
     ,LabImageRowIndex10
     ,LabImageFilePath10
     ,LabImageRemark10
    UNION ALL
    SELECT
      '11'
     ,LabImageRowIndex11
     ,LabImageFilePath11
     ,LabImageRemark11
    UNION ALL
    SELECT
      '12'
     ,LabImageRowIndex12
     ,LabImageFilePath12
     ,LabImageRemark12
    UNION ALL
    SELECT
      '13'
     ,LabImageRowIndex13
     ,LabImageFilePath13
     ,LabImageRemark13
    UNION ALL
    SELECT
      '14'
     ,LabImageRowIndex14
     ,LabImageFilePath14
     ,LabImageRemark14
    UNION ALL
    SELECT
      '15'
     ,LabImageRowIndex15
     ,LabImageFilePath15
     ,LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
  WHERE 
	ps.ID = @ID AND
	(
		ISNULL(FilePath, '') <> '' OR 
		LEN(ISNULL(Remark, '')) > 0
	)
  ORDER BY c.ImageRowIndex ASC;


  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,patient.Name Name_Patient
     ,patient.Name_Client
     ,fs.Name Name_FilingStatus
     ,soapType.Name Name_SOAPType
    FROM (
		SELECT
			NULL AS [_]
		   ,-1 AS [ID]
		   ,'-New-' AS [Code]
		   ,NULL AS [Name]
		   ,1 AS [IsActive]
		   ,NULL AS [ID_Company]
		   ,NULL AS [Comment]
		   ,NULL AS [DateCreated]
		   ,NULL AS [DateModified]
		   ,@ID_User AS [ID_CreatedBy]
		   ,NULL AS [ID_LastModifiedBy]
		   ,@ID_Patient AS ID_Patient
		   ,GETDATE() Date
		   ,@FILED_ID_FilingStatus ID_FilingStatus
		   ,@IsDeceased IsDeceased
		   ,@ID_SOAPType ID_SOAPType
		   ,@ObjectiveTemplate ObjectiveTemplate 
		   ,@AssessmentTemplate AssessmentTemplate
		   ,@LaboratoryTemplate LaboratoryTemplate
		   ,@ClinicalExaminationTemplate ClinicalExaminationTemplate
		   ,@DiagnosisTemplate DiagnosisTemplate
	   
	   ) H
    LEFT JOIN dbo.tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vPatient patient
      ON patient.ID = H.ID_Patient
    LEFT JOIN dbo.tFilingStatus fs
      ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tSOAPType soapType
      ON soapType.ID = H.ID_SOAPType;
  END;
  ELSE
  BEGIN
    SELECT
		H.*
		,patient.IsDeceased
		,@ObjectiveTemplate ObjectiveTemplate 
		,@AssessmentTemplate AssessmentTemplate
		,@LaboratoryTemplate LaboratoryTemplate
		,@ClinicalExaminationTemplate ClinicalExaminationTemplate
		,@DiagnosisTemplate DiagnosisTemplate
    FROM dbo.vPatient_SOAP H
	LEFT JOIN tPatient patient on h.ID_Patient = patient.ID
    WHERE H.ID = @ID;
  END;


  SELECT * FROM @LabImage ORDER BY ImageRowIndex DESC;

  SELECT
    *
  FROM dbo.vPatient_SOAP_Plan
  WHERE ID_Patient_SOAP = @ID
  ORDER BY DateReturn ASC;


  SELECT
    *
  FROM dbo.vPatient_SOAP_Prescription
  WHERE ID_Patient_SOAP = @ID
 

END;

GO

CREATE OR ALTER PROC [dbo].[pGetBillingInvoice]
    @ID INT = -1,
    @ID_Client INT = NULL,
    @ID_Patient INT = NULL,
    @ID_Session INT = NULL,
	@ID_Patient_SOAP INT = NULL,
	@AttendingPhysician_ID_Employee INT = NULL
AS
BEGIN
    SELECT '_',
           '' BillingInvoice_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               fs.Name Name_FilingStatus,
               client.Name Name_Client,
			   patient.Name Name_Patient,
			   client.Address BillingAddress,
			   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- NEW -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   GETDATE() AS Date,
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   0 AS [ID_CreatedBy],
                   0 AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
                   0 AS [ID_Taxscheme],
                   @ID_Client ID_Client,
                   0 IsComputeDiscountRate,
                   @ID_Patient ID_Patient,
				   @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
            LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
            LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
			LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
				ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vBillingInvoice H
        WHERE H.ID = @ID;
    END;

	if ISNULL(@ID_Patient_SOAP, 0) > 0 AND (@ID = -1)
	BEGIN

		SELECT 
			soapPrescription.ID_Item, 
			soapPrescription.Name_Item, 
			ISNULL(soapPrescription.Quantity, 0) Quantity,
			ISNULL(item.UnitPrice, 0) UnitPrice,
			item.OtherInfo_DateExpiration DateExpiration
		FROM dbo.vPatient_SOAP_Prescription soapPrescription
		INNER JOIN tItem item 
			ON item.ID = soapPrescription.ID_Item
		WHERE ID_Patient_SOAP = @ID_Patient_SOAP;
	END
	ELSE
	BEGIN

		SELECT *
		FROM dbo.vBillingInvoice_Detail
		WHERE ID_BillingInvoice = @ID;
	END

END;
GO

ALTER PROC [dbo].[pApproveBillingInvoice_validation]  
(  
    @IDs_BillingInvoice typIntList READONLY,  
    @ID_UserSession INT  
)  
AS  
BEGIN  
  
    DECLARE @Filed_ID_FilingStatus INT = 1;  
    DECLARE @message VARCHAR(400) = '';  

  	/*---------------------------------------------------------------------------------------------------------------------*/
    DECLARE @ValidateNotFiled TABLE  
    (  
        Code VARCHAR(30),  
        Name_FilingStatus VARCHAR(30)  
    );  
    DECLARE @Count_ValidateNotFiled INT = 0;  
  
  
    /* Validate Billing Invoices Status is not Filed*/  
    INSERT @ValidateNotFiled  
    (  
        Code,  
        Name_FilingStatus  
    )  
    SELECT Code,  
           Name_FilingStatus  
    FROM dbo.vBillingInvoice bi  
    WHERE EXISTS  
    (  
        SELECT ID FROM @IDs_BillingInvoice ids WHERE ids.ID = bi.ID  
    )  
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );  
  
    SELECT @Count_ValidateNotFiled = COUNT(*)  
    FROM @ValidateNotFiled;  
  
    IF (@Count_ValidateNotFiled > 0)  
    BEGIN  
  
        SET @message = 'The following record' + CASE  
                                                    WHEN @Count_ValidateNotFiled > 1 THEN  
                                                        's are'  
                                                    ELSE  
                                                        ' is '  
                                                END + 'not allowed to approved:';  
  
        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus  
        FROM @ValidateNotFiled;  
        THROW 50001, @message, 1;  
  
    END;
	/*---------------------------------------------------------------------------------------------------------------------*/
	/* Validate Remaining Inventory */
	DECLARE @Inventoriable_ID_ItemType INT = 2;   
	DECLARE @ValidateItemInventory TABLE (
									ID_Item INT, 
									ItemName VARCHAR(MAX),
									BIQty INT,
									RemQty INT
								 )
	Declare @Count_ValidateItemInventory INT = 0

	INSERT @ValidateItemInventory
	(
		ID_Item,
		ItemName,
		BIQty,
		RemQty
	)
	SELECT 
		biDetail.ID_Item, 
		item.Name,
		SUM(biDetail.Quantity), 
		0
	FROM dbo.tBillingInvoice_Detail biDetail
	INNER JOIN dbo.tBillingInvoice biHed
		ON biDetail.ID_BillingInvoice = biHed.ID
	INNER JOIN dbo.tItem item
		ON item.ID = biDetail.ID_Item
	INNER JOIN @IDs_BillingInvoice ids
		ON ids.ID = biHed.ID
	WHERE 
		item.ID_ItemType = @Inventoriable_ID_ItemType
	GROUP BY
		biDetail.ID_Item,
		item.Name

	UPDATE @ValidateItemInventory 
		SET RemQty = tbl.TotalRemQty
	FROM @ValidateItemInventory invtItem
		INNER JOIN 
	(
		SELECT 
			ID_Item, 
			SUM(Quantity) TotalRemQty
		FROM tInventoryTrail
		GROUP BY ID_Item
	) tbl ON invtItem.ID_Item = tbl.ID_Item

	DELETE FROM @ValidateItemInventory WHERE (ISNULL(RemQty, 0) - ISNULL(BIQty, 0) ) >= 0

	SELECT @Count_ValidateItemInventory = COUNT(*) FROM @ValidateItemInventory 

    IF (@Count_ValidateItemInventory > 0)  
    BEGIN  
  
        SET @message = 'The following item' + CASE  
                                                    WHEN @Count_ValidateItemInventory > 1 THEN  
                                                        's are '  
                                                    ELSE  
                                                        ' is '  
                                                END + 'insufficient inventory count:';  
  
        SELECT @message = @message + CHAR(10) + ItemName + ': Rem. Qty - ' + CONVERT(varchar(MAX), RemQty) + ' Bi Qty - ' + CONVERT(varchar(MAX), BIQty) 
        FROM @ValidateItemInventory;  
        THROW 50001, @message, 1;  
  
    END;  
  	/*---------------------------------------------------------------------------------------------------------------------*/
	/* Validate Has No TotalAmount */  
	DECLARE @ValidateHasNoTotalAmount TABLE  
    (  
        Code VARCHAR(30)
    );  
    DECLARE @Count_ValidateHasNoTotalAmount INT = 0;  
  

    INSERT @ValidateHasNoTotalAmount  
    (  
        Code 
    )  
    SELECT 
		Code
    FROM dbo.vBillingInvoice bi  
	INNER JOIN @IDs_BillingInvoice ids
		ON bi.ID = ids.ID
    WHERE 
       bi.TotalAmount IS NULL OR bi.NetAmount IS  NULL
	  
    SELECT @Count_ValidateHasNoTotalAmount = COUNT(*)  
    FROM @ValidateHasNoTotalAmount;  
  
    IF (@Count_ValidateHasNoTotalAmount > 0)  
    BEGIN  
  
        SET @message = 'The following record' + CASE  
                                                    WHEN @Count_ValidateHasNoTotalAmount > 1 THEN  
                                                        's have'  
                                                    ELSE  
                                                        ' has '  
                                                END + 'incorrect computation. Please check and try again';  
  
        SELECT @message = @message + CHAR(10) + Code 
        FROM @ValidateHasNoTotalAmount;  
        THROW 50001, @message, 1;  
  
    END;


END;  

GO

CREATE OR ALTER PROC pAdd_SMSPatientSOAP_Company(@ID_Company INT)
AS
BEGIN
	
	Declare @Already_Exist_Count INT = 0
	DECLARE @CompanyName VARCHAR(400) = '';
	DECLARE @message VARCHAR(400) = '';


	/* Validate if Company Exis*/  
    SELECT 
		@Already_Exist_Count = COUNT(*)
    FROM dbo.tSMSPatientSOAP_Company
    WHERE 
		ID_Company = @ID_Company;

    IF @Already_Exist_Count > 0
    BEGIN

		SELECT  
			@CompanyName = Name
		FROM    tCompany
		WHERE
			ID = @ID_Company

        SET @message = @CompanyName + ' is already exist.';
        THROW 50001, @message, 1;
    END;

	INSERT INTO [dbo].[tSMSPatientSOAP_Company]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy])
		 VALUES
			   (NULL
			   ,NULL
			   ,1
			   ,@ID_Company
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1)	
END

GO

ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Patient_SOAP INT, @iTextMo_Status INT)
AS
BEGIN
	/*
		iTextMo Status

		"1" = Invalid Number.
		"2" = Number prefix not supported. Please contact us so we can add.
		"3" = Invalid ApiCode.
		"4" = Maximum Message per day reached. This will be reset every 12MN.
		"5" = Maximum allowed characters for message reached.
		"6" = System OFFLINE.
		"7" = Expired ApiCode.
		"8" = iTexMo Error. Please try again later.
		"9" = Invalid Function Parameters.
		"10" = Recipient's number is blocked due to FLOODING, message was ignored.
		"11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.
		"12" = Invalid request. You can't set message priorities on non corporate apicodes.
		"13" = Invalid or Not Registered Custom Sender ID.
		"14" = Invalid preferred server number.
		"15" = IP Filtering enabled - Invalid IP.
		"16" = Authentication error. Contact support at support@itexmo.com
		"17" = Telco Error. Contact Support support@itexmo.com
		"18" = Message Filtering Enabled. Contact Support support@itexmo.com
		"19" = Account suspended. Contact Support support@itexmo.com
		"0" = Success! Message is now on queue and will be sent soon
	*/

	DECLARE @Success BIT = 1;

	IF @iTextMo_Status = 0
	BEGIN

	  UPDATE dbo.tPatient_SOAP_Plan
	  SET IsSentSMS = 1
		 ,DateSent = GETDATE()
	  FROM tPatient_SOAP_Plan psp
	  WHERE
		psp.ID = @ID_Patient_SOAP

	END

	INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[iTextMo_Status]
			   ,[ID_Patient_SOAP])
		 VALUES
			   (NULL
			   ,NULL
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,@iTextMo_Status
			   ,@ID_Patient_SOAP)

	SELECT '_'

	SELECT @Success Success;

END

GO

Create OR ALTER   PROC [dbo].[pGetSendSOAPPlan]( @Date DateTime, @IsSMSSent Bit = NULL)
AS
BEGIN

  DECLARE @Success BIT = 1;
  DECLARE @SMSSent Table (IsSMSSent bit)

  if @IsSMSSent IS NULL
	INSERT @SMSSent
	VALUES (0), (1)
  ELSE
	INSERT @SMSSent
	VALUES (@IsSMSSent)

	SET @Date = ISNULL(@Date, GETDATE())

  SELECT
  
    '_'
   ,'' AS summary
   ,'' AS records;

 Declare @record TABLE (
							Name_Company VARCHAR(MAX), 
							Name_Client  VARCHAR(MAX), 
							ContactNumber_Client VARCHAR(MAX),
							DateReturn DATETime,
							Name_Item  VARCHAR(MAX),
							Comment  VARCHAR(MAX),
							Message VARCHAR(MAX),
							DateSending  DATETime,
							ID_Patient_SOAP_Plan INT
					   )

INSERT @record 
SELECT
	c.Name Name_Company
	,client.Name Name_Client
	,ISNULL(client.ContactNumber,'0') ContactNumber_Client
	,soapPlan.DateReturn
	,soapPlan.Name_Item
	,ISNULL(patientSOAP.Comment, '') Comment
	,dbo.fGetSOAPLANMessage(c.Name, client.Name , ISNULL(c.ContactNumber, ''), patient.Name,  soapPlan.Name_Item , soapPlan.DateReturn) Message
	,CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
	,soapPlan.ID ID_Patient_SOAP_Plan
FROM dbo.tPatient_SOAP patientSOAP
	LEFT JOIN dbo.tPatient patient
	ON patient.ID = patientSOAP.ID_Patient
	LEFT JOIN dbo.tClient client
	ON client.ID = patient.ID_Client
	LEFT JOIN tCompany c
	ON c.iD = patientSOAP.ID_Company
	INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
	ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
WHERE patientSOAP.ID_FilingStatus IN (1, 3)
AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent FROM @SMSSent)
AND ISNULL(ID_CLient,0) > 0
AND ISNULL(patient.IsDeceased,0) = 0
AND ( 
		(CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date)) 
		OR
		(CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date)) 
	)
AND patientSOAP.ID_Company IN (
	SELECT  ID_Company
	FROM    tSMSPatientSOAP_Company
)


SELECT
    @Success Success;

SELECT  
	 FORMAT(DateSending,'yyyy-MM-dd') DateSending
	 ,tbl.Name_Company	
	 ,Count(*) Count
	 ,SUM(CASE WHEN LEN(tbl.Message) <= 160 THEN 1 ELSE 
		CASE WHEN LEN(tbl.Message) <= 306 THEN 2 ELSE 
			CASE WHEN LEN(tbl.Message) <= 459 THEN 3 ELSE 
				4
			END
		END
	END ) ConsumedSMSCredit
FROM    
	(
		SELECT  *
		FROM    @record
	) tbl 
GROUP BY  	 
		FORMAT(DateSending,'yyyy-MM-dd'),
		Name_Company
Order BY 
	FORMAT(DateSending,'yyyy-MM-dd'),
	Name_Company
END

SELECT  *
FROM    @record
Order BY 
	Name_Company
	
GO

ALTER   PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

DECLARE @Date DateTime  = GETDATE();

    exec pGetSendSOAPPlan @Date, 0
END

GO
