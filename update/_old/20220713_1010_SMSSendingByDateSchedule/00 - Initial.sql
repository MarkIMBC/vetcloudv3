GO

CREATE OR
ALTER PROC [dbo].pValidateCompanySMSByDateSchedule(@ID_Company   INT,
                                                   @DateSchedule DATE)
AS
    DECLARE @IsAllowedToSendSMS BIT = 0
    DECLARE @MaxSMSCountPerDay INT = 0
    DECLARE @TotalSMSCount INT = 0

    SELECT @MaxSMSCountPerDay = MaxSMSCountPerDay
    FROM   tCompany_SMSSetting
    WHERE  ID_Company = @ID_Company

    SELECT @TotalSMSCount = COUNT(*)
    FROM   vSentSMSRecord
    WHERE  ID_Company = @ID_Company
           AND CONVERT(Date, DateSchedule) = CONVERT(Date, @DateSchedule)

    SET @IsAllowedToSendSMS = CASE
                                WHEN ( @TotalSMSCount < @MaxSMSCountPerDay ) THEN 1
                                ELSE 0
                              END

    SELECT '_'

    SELECT @ID_Company         ID_Company,
           @DateSchedule       DateSchedule,
           @IsAllowedToSendSMS IsAllowedToSendSMS,
           @TotalSMSCount      TotalSMSCount,
           @MaxSMSCountPerDay  MaxSMSCountPerDay

GO

