GO

IF COL_LENGTH('tPatientWaitingList', 'Oid_Model_Reference') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatientWaitingList',
        'Oid_Model_Reference',
        1
  END

GO

IF COL_LENGTH('tPatientWaitingList', 'IsQueued') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatientWaitingList',
        'IsQueued',
        4
  END

GO

IF COL_LENGTH('tPatientWaitingList', 'ID_Reference') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatientWaitingList',
        'ID_Reference',
        2
  END

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER FUNCTION fGetPatientAppoinmentEventStuff(@DateStart  DateTime,
                                               @DateEnd    DateTime,
                                               @ID_Patient INT)
RETURNS @PateintAppoinmentEventStuff TABLE (
  ID_Patient        INT,
  ReferenceCodeList VARCHAR(MAX),
  UniqueIDList      VARCHAR(MAX),
  UniqueIDListTest  VARCHAR(MAX))
AS
  BEGIN
      DECLARE @table TABLE
        (
           Name_Model    VARCHAR(MAX),
           ReferenceCode VARCHAR(MAX),
           ID_Client     INT,
           ID_Patient    INT,
           Particular    VARCHAR(MAX),
           UniqueID      VARCHAR(MAX),
           Description   VARCHAR(MAX)
        )

      INSERT @table
      SELECT DISTINCT _model.Name,
                      ReferenceCode,
                      ID_Client,
                      ID_Patient,
                      Paticular,
                      UniqueID,
                      Description
      FROM   vAppointmentEvent appntEvent
             inner join _tModel _model
                     on appntEvent.Oid_Model = _model.Oid
      WHERE  CONVERT(Date, DateStart) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)
             AND ID_Patient IN ( @ID_Patient )

      INSERT @PateintAppoinmentEventStuff
      select t.ID_Patient,
             STUFF((SELECT DISTINCT ', ' + ReferenceCode
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS ReferenceCodeList,
             STUFF((SELECT DISTINCT ', ' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniquiIDList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @table
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniquiIDListTest
      FROM   @table t
      GROUP  BY t.ID_Patient

      RETURN;
  END

GO

CREATE OR
ALTER VIEW vPatientWaitingList_ListView
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         appointment.UniqueIDList,
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.ID_Reference
  FROM   vPatientWaitingList waitingList
         OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            appointment.UniqueIDList,
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.ID_Reference

GO

GO


Create   OR
ALTER PROC pAutoGeneratePatientWaitingListByCompanies(@IDs_Company typIntList ReadOnly)
as
    DECLARE @record TABLE
      (
         RowID      INT,
         ID_Company INT
      )
    DECLARE @currentCounter INT = 1
    DECLARE @maxCounter INT = 1

    INSERT @record
    SELECT ROW_NUMBER()
             OVER(
               ORDER BY ID ASC) AS RowID,
           ID
    FROM   @IDs_Company

    SELECT @maxCounter = COUNT(*)
    FROM   @IDs_Company

    WHILE @currentCounter <= @maxCounter
      BEGIN
          DECLARE @ID_Company INT = 0

          SELECT @ID_Company = ID_Company
          FROM   @record
          WHERE  RowID = @currentCounter

          exec pAutoGeneratePatientWaitingListPerCompany
            @ID_Company

          SET @currentCounter = @currentCounter + 1
      END

GO 

CREATE OR
ALTER PROC pAddPatientToQueueWaitingList(@IDs_Patient    typIntList READONLY,
                                         @ID_UserSession INT)
AS
    DECLARE @Patient_Oid_Model Varchar(MAX) = ''
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @Patient_Oid_Model = Oid
    FROM   _tModel
    WHERE  TableName = 'tPatient'

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    DECLARE @DateCreatedMax Date

    SELECT @DateCreatedMax = CONVERT(DATE, MAX(DateCreated))
    FROm   tPatientWaitingList

    if( @DateCreatedMax < CONVERT(DATE, GETDATE()) )
      begin
          Update tDocumentSeries
          set    Counter = 1
          FROM   tDocumentSeries docSeries
                 INNER JOIN _tModel model
                         on docSeries.ID_Model = model.Oid
          where  TableName = 'tPatientWaitingList'
      END

    INSERT INTO [dbo].[tPatientWaitingList]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_Client],
                 [ID_Patient],
                 [WaitingStatus_ID_FilingStatus],
                 Oid_Model_Reference,
                 ID_Reference,
                 IsQueued)
    SELECT H.Name,
           H.IsActive,
           patient.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           patient.ID_Client,
           patient.ID ID_Patient,
           @Waiting_ID_FilingStatus,
           @Patient_Oid_Model,
           patient.ID,
           1
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDate() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @Waiting_ID_FilingStatus
    FROM   tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    DECLARE @IDs_Company typIntList

    INSERT @IDs_Company
    SELECT Distinct ID_Company
    FROM   tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

    exec pAutoGeneratePatientWaitingListByCompanies @IDs_Company

GO

ALTER PROC pDoAddPatientToWaitingList(@IDs_Patient    typIntList READONLY,
                                      @ID_UserSession INT)
AS
    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0;

    BEGIN TRY
        SELECT @ID_User = ID_User
        FROM   dbo.tUserSession
        WHERE  ID = @ID_UserSession;

        exec [pDoAddPatientToWaitingList_validation]
          @IDs_Patient,
          @ID_UserSession

        exec pAddPatientToQueueWaitingList
          @IDs_Patient,
          @ID_UserSession
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

Update tPatientWaitingList
SET    IsQueued = 1
WHERE  ID IN (SELECT ID
              FROM   vPatientWaitingList_ListView
              WHERE  UniqueIDList IS NULL) 
