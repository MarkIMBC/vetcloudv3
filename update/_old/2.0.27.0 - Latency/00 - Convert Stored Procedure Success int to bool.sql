ALTER PROC pGetModelDateColumns(@Oid_Model VARCHAR(500))
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @PropertyList TABLE
        (
           Oid  VARCHAR(MAX),
           Name VARCHAR(MAX)
        )
      DECLARE @PropertyListDateColumns TABLE
        (
           ParentName     VARCHAR(MAX),
           DateColumnName VARCHAR(MAX)
        )

      INSERT @PropertyListDateColumns
      SELECT '',
             Name
      FROM   _tModel_Property
      WHERE  ID_Model = @Oid_Model
             AND ID_PropertyType IN ( 5, 6 )

      INSERT @PropertyList
      SELECT ID_PropertyModel,
             Name
      FROM   _tModel_Property
      WHERE  Lower(ID_Model) = Lower(@Oid_Model)
             AND ID_PropertyType IN ( 10 )

      INSERT @PropertyListDateColumns
      SELECT propList.Name,
             modelProp.Name
      FROM   _tModel_Property modelProp
             INNER JOIN @PropertyList propList
                     ON modelProp.ID_Model = propList.OId
      WHERE  ID_PropertyType IN ( 5, 6 )
      ORDER  BY propList.Name

      SELECT '_' [_],
             ''  PropertyList,
             ''  DateColumns

      SELECT @Success Success

      SELECT DISTINCT ParentName
      FROM   @PropertyListDateColumns

      SELECT DISTINCT DateColumnName
      FROM   @PropertyListDateColumns
  END

GO

ALTER PROC [dbo].[pGetModelReports](@CompanyID INT,
                                    @Oid_Model VARCHAR(200))
AS
  BEGIN
      DECLARE @Success BIT = 1;

      SELECT '_',
             '' AS Reports

      SELECT @Success Success

      SELECT model.Name       Name_Model,
             modelReport.Oid_Report,
             modelReport.Name Name_ModelReport
      FROM   _tModelReport modelReport
             INNER JOIN _tMOdel model
                     ON model.Oid = modelReport. Oid_Model
             INNER JOIN _tReport report
                     ON report.Oid = modelReport. Oid_Report
      WHERE  Lower(modelReport.Oid_Model) = Lower(@Oid_Model)
             AND modelReport.ID_Company = @CompanyID
             AND modelReport.IsActive = 1
      ORDER  BY report.Name
  END

GO 
