ALTER VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                                 AS CreatedBy,
         UM.Name                                 AS LastModifiedBy,
         UC.Name                                 AS Name_CreatedBy,
         UM.Name                                 AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101)      DateString,
         soapType.Name                           Name_SOAPType,
         patient.Name                            Name_Patient,
         patient.Name_Client                     Name_Client,
         approvedUser.Name                       Name_ApprovedBy,
         canceledUser.Name                       Name_CanceledBy,
         fs.Name                                 Name_FilingStatus,
         patient.ID_Client                       ID_Client,
         attendingPhysicianEmloyee.Name          AttendingPhysician_Name_Employee,
         confinement.Code                        Code_Patient_Confinement,
         confinement.Date                        Date_Patient_Confinement,
         confinement.DateDischarge               DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus             ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus           Name_FilingStatus_Patient_Confinement,
         REPLACE(H.Diagnosis, CHAR(13), '<br/>') DiagnosisHTML,
         billfs.Name                             BillingInvoice_Name_FilingStatus
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement
         LEFT JOIN dbo.tFilingStatus billfs
                ON billfs.ID = H.BillingInvoice_ID_FilingStatus

GO
 
CREATE OR
ALTER VIEW vpivotBilledPatientConfinement
AS
  SELECT *
  FROM   (SELECT ID_Patient_Confinement,
                 BillingInvoice_RowID,
                 Info_BillingInvoice
          FROM   (SELECT bi.ID_Patient_Confinement,
                         ROW_NUMBER()
                           OVER(
                             PARTITION BY bi.ID_Patient_Confinement
                             ORDER BY ID_Patient_Confinement ASC, bi.ID)      BillingInvoice_RowID,
                         format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                  FROM   vBillingINvoice bi
                         inner JOIN tPatient_Confinement soap
                                 ON bi.ID_Patient_Confinement = soap.ID
                  where  ISNULL(bi.ID_Patient_Confinement, 0) <> 0
                         AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
         PIVOT(MAX([Info_BillingInvoice])
              FOR [BillingInvoice_RowID] IN([1],
                                            [2],
                                            [3],
                                            [4],
                                            [5],
                                            [6],
                                            [7],
                                            [8],
                                            [9],
                                            [10] )) AS PivotTable;

Go

CREATE OR
ALTER VIEW vpivotBilledPatientSOAP
AS
  SELECT *
  FROM   (SELECT ID_Patient_SOAP,
                 BillingInvoice_RowID,
                 Info_BillingInvoice
          FROM   (SELECT bi.ID_Patient_SOAP,
                         ROW_NUMBER()
                           OVER(
                             PARTITION BY bi.ID_Patient_SOAP
                             ORDER BY ID_Patient_SOAP ASC, bi.ID)             BillingInvoice_RowID,
                         format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice
                  FROM   vBillingINvoice bi
                         inner JOIN tPatient_SOAP soap
                                 ON bi.ID_Patient_SOAP = soap.ID
                  where  ISNULL(bi.ID_Patient_SOAP, 0) <> 0
                         AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable
         PIVOT(MAX([Info_BillingInvoice])
              FOR [BillingInvoice_RowID] IN([1],
                                            [2],
                                            [3],
                                            [4],
                                            [5],
                                            [6],
                                            [7],
                                            [8],
                                            [9],
                                            [10] )) AS PivotTable;

Go

Create OR
ALTER View vForBilling_ListView
as
  Select confinement.ID,
         confinement.Code RefNo,
         confinement.ID   ID_CurrentObject,
         m.Oid            Oid_Model,
         confinement.Date,
         confinement.ID_Company,
         confinement.ID_Client,
         confinement.ID_Patient,
         confinement.BillingInvoice_ID_FilingStatus,
         confinement.Name_Client,
         confinement.Name_Patient,
         confinement.BillingInvoice_Name_FilingStatus,
         billConfinement.[1],
         billConfinement.[2],
         billConfinement.[3],
         billConfinement.[4],
         billConfinement.[5],
         billConfinement.[6],
         billConfinement.[7],
         billConfinement.[8],
         billConfinement.[9],
         billConfinement.[10]
  FROM   vPatient_Confinement confinement
         LEFT JOIN vpivotBilledPatientConfinement billConfinement
                 ON confinement.ID = billConfinement.ID_Patient_Confinement,
         _tModel m
  where  m.TableName = 'tPatient_Confinement'
         AND ID_FilingStatus NOT IN ( 4 )
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  Select soap.ID,
         soap.Code RefNo,
         soap.ID   ID_CurrentObject,
         m.Oid     Oid_Model,
         soap. Date,
         soap.ID_Company,
         soap.ID_Client,
         soap.ID_Patient,
         soap.BillingInvoice_ID_FilingStatus,
         soap.Name_Client,
         soap.Name_Patient,
         soap.BillingInvoice_Name_FilingStatus,
         billSOAP.[1],
         billSOAP.[2],
         billSOAP.[3],
         billSOAP.[4],
         billSOAP.[5],
         billSOAP.[6],
         billSOAP.[7],
         billSOAP.[8],
         billSOAP.[9],
         billSOAP.[10]
  FROM   vPatient_SOAP soap
         LEFT JOIN vpivotBilledPatientSOAP billSOAP
                 ON soap.ID = billSOAP.ID_Patient_SOAP,
         _tModel m
  where  m.TableName = 'tPatient_SOAP'
         AND ID_FilingStatus NOT IN ( 4 )
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )

GO

CREATE OR
ALTER VIEW vPatientWaitingList_ListView
as
  SELECT ID,
         ID_Company,
         DateCreated,
         Name_Client,
         Name_Patient,
         ID_Client,
         ID_Patient,
         WaitingStatus_ID_FilingStatus,
         BillingInvoice_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         ISNULL(BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus
  FROM   vPatientWaitingList
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )

GO

GO

CREATE OR
ALTER PROC pAddPatientToWaitingList(@IDs_Patient    typIntList READONLY,
                                    @ID_UserSession INT)
AS
    DECLARE @Waiting_ID_FilingStatus INT = 8
    DECLARE @Ongoing_ID_FilingStatus INT = 9
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    INSERT INTO [dbo].[tPatientWaitingList]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_Client],
                 [ID_Patient],
                 [WaitingStatus_ID_FilingStatus])
    SELECT H.Name,
           H.IsActive,
           patient.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           patient.ID_Client,
           patient.ID ID_Patient,
           @Waiting_ID_FilingStatus
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDate() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatient patient
           INNER JOIN @IDs_Patient ids
                   ON patient.ID = ids.ID

GO

CREATE OR
ALTER PROC [dbo].[pDoAddPatientToWaitingList_validation] (@IDs_Patient    typIntList READONLY,
                                                          @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @PatientAlreadyWaitingList TABLE
        (
           Name_Client                     VARCHAR(30),
           Name_Patient                    VARCHAR(30),
           WaitingStatus_Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_PatientAlreadyWaitingList INT = 0;

      INSERT @PatientAlreadyWaitingList
             (Name_Client,
              Name_Patient,
              WaitingStatus_Name_FilingStatus)
      SELECT Distinct Name_Client,
                      Name_Patient,
                      WaitingStatus_Name_FilingStatus
      FROm   vPatientWaitingList pWait
             INNER JOIN @IDs_Patient ids
                     ON pWait.ID_Patient = ids.ID
      WHERE  WaitingStatus_ID_FilingStatus NOT IN ( @Done_ID_FilingStatus, @Canceled_ID_FilingStatus )

      SELECT @Count_PatientAlreadyWaitingList = COUNT(*)
      FROM   @PatientAlreadyWaitingList;

      IF ( @Count_PatientAlreadyWaitingList > 0 )
        BEGIN
            SET @message = 'The following patient'
                           + CASE
                               WHEN @Count_PatientAlreadyWaitingList > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'already on waiting list:';

            SELECT @message = @message + CHAR(10) + Name_Client + ' Pet:'
                              + Name_Patient + ' - '
                              + WaitingStatus_Name_FilingStatus
            FROM   @PatientAlreadyWaitingList;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC pDoAddPatientToWaitingList(@IDs_Patient    typIntList READONLY,
                                      @ID_UserSession INT)
AS
    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0;

    BEGIN TRY
        SELECT @ID_User = ID_User
        FROM   dbo.tUserSession
        WHERE  ID = @ID_UserSession;

        exec [pDoAddPatientToWaitingList_validation]
          @IDs_Patient,
          @ID_UserSession

        exec pAddPatientToWaitingList
          @IDs_Patient,
          @ID_UserSession
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

Alter PROC [dbo].[pGetPatient_Confinement] @ID              INT = -1,
                                           @ID_Client       INT = NULL,
                                           @ID_Patient      INT = NULL,
                                           @ID_Patient_SOAP INT = NULL,
                                           @ID_Session      INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Confinement_ItemsServices

      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Employee INT
      DECLARE @ID_Company INT
      DECLARE @Confinement_ID_FilingStatus INT = 14

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF( ISNULL(@ID_Patient_SOAP, 0) <> 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   fs.Name      Name_FilingStatus
            FROM   (SELECT NULL                         AS [_],
                           -1                           AS [ID],
                           GETDATE()                    AS [Date],
                           '-- NEW --'                  AS [Code],
                           NULL                         AS [Name],
                           1                            AS [IsActive],
                           @ID_Company                  AS [ID_Company],
                           NULL                         AS [Comment],
                           GETDATE()                    AS [DateCreated],
                           GETDATE()                    AS [DateModified],
                           NULL                         AS [ID_CreatedBy],
                           NULL                         AS [ID_LastModifiedBy],
                           @Confinement_ID_FilingStatus AS ID_FilingStatus,
                           @ID_Client                   AS ID_Client,
                           @ID_Patient                  AS ID_Patient,
                           @ID_Patient_SOAP             AS ID_Patient_SOAP) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            DECLARE @BillingInvoice Table
              (
                 ID_BillingInvoice          INT,
                 ID_Patient_Confinement     INT,
                 Code_BillingInvoice        VARCHAR(MAX),
                 Status_BillingInvoice      VARCHAR(MAX),
                 TotalAmount_BillingInvoice DECIMAL(18, 2)
              )

            INSERT @BillingInvoice
            SELECT TOP 1 biHed.ID          ID_BillingInvoice,
                         biHed.ID_Patient_Confinement,
                         biHed.Code        Code_BillingInvoice,
                         biHed.Status      Status_BillingInvoice,
                         biHed.TotalAmount TotalAmount_BillingInvoice
            FROM   vBillingInvoice biHed
            WHERE  biHed.ID_Patient_Confinement = @ID
                   AND biHed.ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )

            SELECT H.*,
                   biHed.*
            FROM   vPatient_Confinement H
                   LEFT JOIN @BillingInvoice biHed
                          ON H.ID = biHed.ID_Patient_Confinement
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Confinement_ItemsServices
      WHERE  ID_Patient_Confinement = @ID
  END

GO

CREATE OR
ALTER PROC pUpdatePatientToWaitingListStatus(@IDs_PatientWaitingList        typIntList READONLY,
                                             @WaitingStatus_ID_FilingStatus INT,
                                             @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    /* Update Waiting List Status */
    Update tPatientWaitingList
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus
    FROM   tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /* Add Logs */
    INSERT INTO [dbo].[tPatientWaitingList_Logs]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_PatientWaitingList],
                 [WaitingStatus_ID_FilingStatus])
    SELECT H.Name,
           H.IsActive,
           waitingList.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           waitingList.ID,
           @WaitingStatus_ID_FilingStatus
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDATE() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

CREATE   OR
ALTER PROC pUpdatePatientSOAPBillStatus(@IDs_Patient_SOAP               typIntList READONLY,
                                        @BillingInvoice_ID_FilingStatus INT,
                                        @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    /* Update Waiting List Status */
    Update tPatient_SOAP
    SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus
    FROM   tPatient_SOAP _SOAP
           INNER JOIN @IDs_Patient_SOAP ids
                   ON _SOAP.ID = ids.ID

    SELECT '_';

    SELECT @Success Success,
           @message message; 

GO

GO

CREATE OR
ALTER FUNCTION fGetSOAPBillingStatus(@Filed_Count         INT = 0,
                                     @Approved_Count      INT = 0,
                                     @Done_Count          INT = 0,
                                     @Pending_Count       INT = 0,
                                     @PartiallyPaid_Count INT = 0,
                                     @FullyPaid_Count     INT = 0)
ReTURNS INT
AS
  BEGIN
      DECLARE @ID_FilingStatus INT = NULL
      Declare @TotalCount INT = ISNULL(@Filed_Count, 0)
        + ISNULL(@Approved_Count, 0)
        + ISNULL(@Done_Count, 0)
        + ISNULL(@Pending_Count, 0)
        + ISNULL(@PartiallyPaid_Count, 0)
        + ISNULL(@FullyPaid_Count, 0)

      SET @Filed_Count = ISNULL(@Filed_Count, 0)
      SET @Approved_Count = ISNULL(@Approved_Count, 0)
      SET @Done_Count = ISNULL(@Done_Count, 0)
      SET @Pending_Count = ISNULL(@Pending_Count, 0)
      SET @PartiallyPaid_Count = ISNULL(@PartiallyPaid_Count, 0)
      SET @FullyPaid_Count = ISNULL(@FullyPaid_Count, 0)

	  IF( @TotalCount = @FullyPaid_Count AND @FullyPaid_Count > 0 )
        SET @ID_FilingStatus = 12
      ELSE IF( @TotalCount = 0 )
        SET @ID_FilingStatus = 16
      ELSE IF( @TotalCount = @Done_Count  AND @Done_Count > 0 )
        SET @ID_FilingStatus = 12
      ELSE IF ( @PartiallyPaid_Count > 0 )
        SET @ID_FilingStatus = 11
      ELSE IF ( @Pending_Count > 0 )
        SET @ID_FilingStatus = 2
      ELSE IF ( @Filed_Count > 0 )
        SET @ID_FilingStatus = 1


      return @ID_FilingStatus
  END

GO

CREATE OR
ALTER PROC pUpdatePatient_SOAP_BillingStatus(@IDs_Patient_SOAP typIntList READONLY)
AS
    DECLARE @Cancelled_ID_FilingStatus INT = 4
    DECLARE @ForBilling_ID_FilingStatus INT = 16
    DECLARE @Patient_SOAP TABLE
      (
         ID_Patient_SOAP                INT,
         BillingInvoice_ID_FilingStatus INT
      )
    DECLARE @Patient_SOAP_FromBI TABLE
      (
         ID_Patient_SOAP                INT,
         BillingInvoice_ID_FilingStatus INT
      )

    INSERT @Patient_SOAP
    SELECT ID,
           @ForBilling_ID_FilingStatus
    FROM   @IDs_Patient_SOAP

    INSERT @Patient_SOAP_FromBI
    SELECT ID_Patient_SOAP,
           dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])
    FROM   (SELECT idsSOAP.ID ID_Patient_SOAP,
                   fs.ID      BillingInvoice_ID_FilingStatus,
                   COUNT(*)   BillingInvoiceCount
            FROM   vBillingInvoice bi
                   inner JOIN @IDs_Patient_SOAP idsSOAP
                           on idsSOAP.ID = bi.ID_Patient_SOAP
                   LEFT JOIN tFilingStatus fs
                          ON fs.Name = bi.Status
            GROUP  BY idsSOAP.ID,
                      fs.ID) AS SourceTable
           PIVOT ( AVG(BillingInvoiceCount)
                 FOR BillingInvoice_ID_FilingStatus IN ([1],
                                                        [3],
                                                        [2],
                                                        [11],
                                                        [12],
                                                        [13]) ) AS PivotTable;

    UPDATE @Patient_SOAP
    SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus
    FROM   @Patient_SOAP soapRec
           INNER JOIN @Patient_SOAP_FromBI soapFromPivot
                   ON soapRec.ID_Patient_SOAP = soapFromPivot.ID_Patient_SOAP

    UPDATE tPatient_SOAP
    SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus
    FROM   tPatient_SOAP soap
           INNER JOIN @Patient_SOAP soapRec
                   ON soap.ID = soapRec.ID_Patient_SOAP

GO

ALTER PROC dbo.pModel_AfterSaved_BillingInvoice (@ID_CurrentObject VARCHAR(10),
                                                 @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tBillingInvoice
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'BillingInvoice';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tBillingInvoice
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      /*Update Confinement Bill Status*/
      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement

      /*Update SOAP Bill Status*/
      declare @IDs_Patient_SOAP typIntList

      INSERT @IDs_Patient_SOAP
      SELECT ID_Patient_SOAP
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      exec pUpdatePatient_SOAP_BillingStatus
        @IDs_Patient_SOAP
  END;

GO

ALTER PROC [dbo].[pUpdateBillingInvoicePayment] (@IDs_BillingInvoice typIntList READONLY)
AS
  BEGIN
      DECLARE @FILINGSTATUS_PENDING INT = 2;
      DECLARE @FILINGSTATUS_APPROVED INT = 3;
      DECLARE @FILINGSTATUS_PARTIALLYPAID INT = 11;
      DECLARE @FILINGSTATUS_FULLYPAID INT = 12;

      exec dbo.pUpdatePaymentTransaction_RemainingAmount_By_BIs
        @IDs_BillingInvoice

      /*Update SOAP Bill Status*/
      declare @IDs_Patient_SOAP typIntList

      INSERT @IDs_Patient_SOAP
      SELECT ID_Patient_SOAP
      FROM   tBillingInvoice bi
             INNER JOIN @IDs_BillingInvoice ids
                     ON bi.ID = ids.ID

      exec pUpdatePatient_SOAP_BillingStatus
        @IDs_Patient_SOAP
  END; 
