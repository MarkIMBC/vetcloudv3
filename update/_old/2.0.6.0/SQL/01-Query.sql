IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'AttendingPhysician_ID_Employee'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'AttendingPhysician_ID_Employee'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tBillingInvoice'
    AND COLUMN_NAME = 'AttendingPhysician_ID_Employee'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tBillingInvoice'
                         ,'AttendingPhysician_ID_Employee'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tEmployee'
    AND COLUMN_NAME = 'IsSystemUsed'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tEmployee'
                         ,'IsSystemUsed'
                         , 4
END;

GO

exec _pRefreshAllViews

GO

ALTER VIEW [dbo].[vPatient_SOAP]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       UC.Name AS Name_CreatedBy,
       UM.Name AS Name_LastModifiedBy,
       CONVERT(VARCHAR(100), H.Date, 101) DateString,
       soapType.Name Name_SOAPType,
       patient.Name Name_Patient,
       patient.Name_Client,
	   approvedUser.Name Name_ApprovedBy,
	   canceledUser.Name Name_CanceledBy,
	   fs.Name Name_FilingStatus,
	   patient.ID_Client,
	   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
FROM dbo.tPatient_SOAP H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vPatient patient
        ON H.ID_Patient = patient.ID
    LEFT JOIN dbo.tSOAPType soapType
        ON soapType.ID = H.ID_SOAPType
    LEFT JOIN dbo.tUser approvedUser
        ON approvedUser.ID = H.ID_ApprovedBy
    LEFT JOIN dbo.tUser canceledUser
        ON canceledUser.ID = H.ID_CanceledBy
    LEFT JOIN dbo.tFilingStatus fs
        ON fs.ID = H.ID_FilingStatus
	LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
        ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
GO

ALTER VIEW [dbo].[vBillingInvoice]
AS
SELECT H.*,
       CONVERT(VARCHAR(100), H.Date, 101) DateString,
       UC.Name AS CreatedBy_Name_User,
       UM.Name AS LastModifiedBy_Name_User,
       approvedUser.Name AS ApprovedBy_Name_User,
       cancelUser.Name AS CanceledBy_Name_User,
       company.Name Company,
       patient.Name Name_Patient,
       taxScheme.Name Name_TaxScheme,
       fs.Name Name_FilingStatus,
       fsPayment.Name Payment_Name_FilingStatus,
       client.Name Name_Client,
       CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,
       CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
       CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
       CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString,
	   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
FROM dbo.tBillingInvoice H
    LEFT JOIN dbo.tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tUser approvedUser
        ON H.ID_ApprovedBy = approvedUser.ID
    LEFT JOIN dbo.tUser cancelUser
        ON H.ID_CanceledBy = cancelUser.ID
    LEFT JOIN dbo.tCompany company
        ON H.ID_Company = company.ID
    LEFT JOIN dbo.tPatient patient
        ON patient.ID = H.ID_Patient
    LEFT JOIN dbo.tTaxScheme taxScheme
        ON taxScheme.ID = H.ID_TaxScheme
    LEFT JOIN dbo.tFilingStatus fs
        ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tFilingStatus fsPayment
        ON fsPayment.ID = H.Payment_ID_FilingStatus
    LEFT JOIN dbo.tClient client
        ON client.ID = H.ID_Client
	LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
        ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
GO

CREATE OR ALTER VIEW [dbo].[vNonSystemUseEmployee]
AS

	SELECT H.* 
	FROM vEmployee H 
	WHERE ISNULL(IsSystemUsed,0) = 0
GO

CREATE OR ALTER VIEW [dbo].[vzBillingInvoicePaidListReport]
as
	SELECT biHed.ID ID_BillingInvoice
			,biHed.Code Code_BillingInvoice
			,biHed.Date Date_BillingInvoice
			,piHed.Date Date_PaymentTransaction
			,biHed.Name_Client
			,bihed.Name_Patient
			,biHed.Payment_ID_FilingStatus
			,biHed.Payment_Name_FilingStatus
			,biHed.ID_Company
			,ISNULL(biHed.TotalAmount,0) TotalAmount
			,CASE WHEN ISNULL(piHed.ChangeAmount, 0) > 0 THEN 
					piHed.PayableAmount 
				ELSE 
					piHed.PaymentAmount 
				END PaymentAmount
			,company.ImageLogoLocationFilenamePath
			,company.Name Name_Company
			,company.Address Address_Company
	FROM  vBillingInvoice biHed 
	LEFT JOIN tPaymentTransaction piHed
		on piHed.ID_BillingInvoice = biHed.ID
	LEFT JOIN dbo.vCompany company 
	 ON company.ID = biHed.ID_Company
	WHERE
		biHed.Payment_ID_FilingStatus IN (11, 12) AND 
		biHed.ID_FilingStatus IN (3) AND 
		piHed.ID_FilingStatus IN (3)
	GROUP BY
		 biHed.ID
		,biHed.Code
		,biHed.Date
		,piHed.Date
		,piHed.ID_FilingStatus
		,biHed.Name_Client
		,bihed.Name_Patient
		,biHed.Payment_ID_FilingStatus
		,biHed.Payment_Name_FilingStatus
		,biHed.ID_Company
		,biHed.TotalAmount
		,piHed.ChangeAmount
		,piHed.PayableAmount
		,piHed.PaymentAmount
		,company.ImageLogoLocationFilenamePath
		,company.Name 
		,company.Address
UNION ALL
	SELECT   biHed.ID ID_BillingInvoice
			,biHed.Code Code_BillingInvoice
			,biHed.Date Date_BillingInvoice
			,NULL Date_PaymentTransaction
			,biHed.Name_Client
			,bihed.Name_Patient
			,biHed.Payment_ID_FilingStatus
			,biHed.Payment_Name_FilingStatus
			,biHed.ID_Company
			,ISNULL(biHed.TotalAmount,0) TotalAmount
			,0.00 PaymentAmount
			,company.ImageLogoLocationFilenamePath
			,company.Name Name_Company
			,company.Address Address_Company
	FROM  vBillingInvoice biHed 
	LEFT JOIN dbo.vCompany company 
	 ON company.ID = biHed.ID_Company
	WHERE
		biHed.Payment_ID_FilingStatus IN (2) AND 
		biHed.ID_FilingStatus IN (3) 
	GROUP BY
		 biHed.ID
		,biHed.Code
		,biHed.Date
		,biHed.Name_Client
		,bihed.Name_Patient
		,biHed.Payment_ID_FilingStatus
		,biHed.Payment_Name_FilingStatus
		,biHed.ID_Company
		,biHed.TotalAmount
		,company.ImageLogoLocationFilenamePath
		,company.Name 
		,company.Address
	
GO

ALTER VIEW [dbo].[vzPOSSummary]
AS
SELECT biDetail.ID_Item,
       biDetail.Name_Item,
       biDetail.UnitPrice,
       biDetail.Amount,
	   biDetail.Quantity,
	   biDetail.ID_BillingInvoice,
       bi.Date,
	   bi.DiscountAmount,
       bi.Date Date1,
       item.ID_ItemType,
       item.Name_ItemType,
       bi.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.vBillingInvoice_Detail biDetail
    LEFT JOIN dbo.vBillingInvoice bi
        ON bi.ID = biDetail.ID_BillingInvoice
    LEFT JOIN dbo.vItem item
        ON item.ID = biDetail.ID_Item
    LEFT JOIN dbo.vCompany company
        ON company.ID = bi.ID_Company
WHERE bi.Payment_ID_FilingStatus IN ( 2, 11, 12 )
      AND bi.ID_FilingStatus = 3
      AND Name_Item IS NOT NULL;
GO

ALTER PROC [dbo].[pCreateNewCompanyAccess]
(
    @CompanyName VARCHAR(MAX),
    @code VARCHAR(MAX)
)
AS
BEGIN

    DECLARE @ID_Company INT = 0;
    DECLARE @Admin_ID_UserRole INT = 5;
    DECLARE @Sole_ID_UserRole INT = 11;
    DECLARE @Standard_ID_UserRole INT = 12;

    DECLARE @Default_IDs_Employee typIntList;

    EXEC dbo.pCreateNewCompanyAccess_validation @CompanyName, @code;

    INSERT @Default_IDs_Employee
    (
        ID
    )
    VALUES
    (1  ),
    (13),
    (14);

    INSERT dbo.tCompany
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Country,
        Address
    )
    VALUES
    (   @code,        -- Code - varchar(50)
        @CompanyName, -- Name - varchar(200)
        1,            -- IsActive - bit
        '',           -- Comment - varchar(max)
        GETDATE(),    -- DateCreated - datetime
        GETDATE(),    -- DateModified - datetime
        1,            -- ID_CreatedBy - int
        1,            -- ID_LastModifiedBy - int
        1,            -- ID_Country - int
        ''            -- Address - varchar(300)
        );

    SET @ID_Company = @@IDENTITY;

    INSERT dbo.tEmployee
    (
        Code,
        IsActive,
        ID_CreatedBy,
        ID_LastModifiedBy,
        DateCreated,
        DateModified,
        ImageFile,
        ID_Department,
        ID_EmployeePosition,
        ID_Position,
        LastName,
        FirstName,
        MiddleName,
        ID_Gender,
        ID_EmployeeStatus,
        FullAddress,
        Email,
        ContactNumber,
        Name,
        ID_Company,
		IsSystemUsed
    )
    SELECT Code,
           IsActive,
           ID_CreatedBy,
           ID_LastModifiedBy,
           DateCreated,
           DateModified,
           ImageFile,
           ID_Department,
           ID_EmployeePosition,
           ID_Position,
           LastName,
           FirstName,
           MiddleName,
           ID_Gender,
           ID_EmployeeStatus,
           FullAddress,
           Email,
           ContactNumber,
           Name,
           @ID_Company,
		   CASE WHEN ids.ID = 1  THEN 1 ELSE 0 END  
    FROM dbo.tEmployee
        INNER JOIN @Default_IDs_Employee ids
            ON ids.ID = tEmployee.ID;

    INSERT dbo.tUser
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Employee,
        Username,
        ID_UserGroup,
        Password,
        IsRequiredPasswordChangedOnLogin,
        ID_Patient
    )
    SELECT '',
           emp.Name,
           1,
           '',
           GETDATE(),
           GETDATE(),
           1,
           1,
           emp.ID,
           LOWER(@code) + '-' + LOWER(emp.LastName),
           1,
           LEFT( NEWID(),4),
           1,
           NULL
    FROM dbo.tEmployee emp
    WHERE emp.ID_Company = @ID_Company;

    INSERT dbo.tUser_Roles
    (
        Code,
        Name,
        IsActive,
        Comment,
        ID_User,
        ID_UserRole,
        SeqNo
    )
    SELECT '',
           userRole.Name,
           1,
           '',
           _user.ID,
           userRole.ID,
           1
    FROM dbo.tUser _user
        INNER JOIN dbo.tEmployee emp
            ON emp.ID = _user.ID_Employee
        INNER JOIN dbo.tUserRole userRole
            ON LOWER(userRole.Name) = LOWER(emp.LastName)
    WHERE emp.ID_Company = @ID_Company;

    INSERT dbo.tDocumentSeries
    (
        Code,
        Name,
        IsActive,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Model,
        Counter,
        Prefix,
        IsAppendCurrentDate,
        DigitCount,
        ID_Company
    )
    SELECT Code,
           Name,
           IsActive,
           Comment,
           DateCreated,
           DateModified,
           ID_CreatedBy,
           ID_LastModifiedBy,
           ID_Model,
           1,
           Prefix,
           IsAppendCurrentDate,
           5,
           @ID_Company
    FROM dbo.tDocumentSeries
    WHERE ID_Company = 1;
END;

GO

ALTER PROC [dbo].[pGetBillingInvoice]
    @ID INT = -1,
    @ID_Client INT = NULL,
    @ID_Patient INT = NULL,
    @ID_Session INT = NULL,
	@AttendingPhysician_ID_Employee INT = NULL
AS
BEGIN
    SELECT '_',
           '' BillingInvoice_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               fs.Name Name_FilingStatus,
               client.Name Name_Client,
			   patient.Name Name_Patient,
			   client.Address BillingAddress,
			   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- NEW -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   GETDATE() AS Date,
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   0 AS [ID_CreatedBy],
                   0 AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
                   0 AS [ID_Taxscheme],
                   @ID_Client ID_Client,
                   0 IsComputeDiscountRate,
                   @ID_Patient ID_Patient,
				   @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
            LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
            LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
			LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
				ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vBillingInvoice H
        WHERE H.ID = @ID;
    END;

    SELECT *
    FROM dbo.vBillingInvoice_Detail
    WHERE ID_BillingInvoice = @ID;
END;
GO

ALTER PROC [dbo].[pGetEmployee] @ID INT = -1, @ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'

  DECLARE @ID_User INT
         ,@ID_Warehouse INT;

  DECLARE @ID_Company INT;
  DECLARE @ID_Employee INT;

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM tUserSession
  WHERE ID = @ID_Session;

  SELECT
    @ID_Employee = ID_Employee
   ,@ID_Company = emp.ID_Company
  FROM dbo.tUser _user
  INNER JOIN dbo.tEmployee emp
    ON emp.ID = _user.ID_Employee
  WHERE _user.ID = @ID_User;

  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,112312123 AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,NULL AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_Company [ID_Company]
       ,'' [Company]
	   ,0 IsSystemUsed
	   ) H
    LEFT JOIN tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
      ON H.ID_LastModifiedBy = UM.ID
  END
  ELSE
  BEGIN
    SELECT
      H.*
    FROM vEmployee H
    WHERE H.ID = @ID
  END
END

GO

DECLARE @ID_REPORT_BillingInvoicePaidListReport UNIQUEIDENTIFIER;

SELECT @ID_REPORT_BillingInvoicePaidListReport = Oid
FROM dbo._tReport
WHERE Name = 'BillingInvoicePaidListReport';


UPDATE _tNavigation SET Caption = 'Billing Invoice Paid List'
WHERE  Name = 'BillingInvoicePaidListReport_Navigation'

IF (NOT EXISTS
(
    SELECT *
    FROM dbo.tCustomNavigationLink
    WHERE [Oid_Report] = @ID_REPORT_BillingInvoicePaidListReport
))
BEGIN

	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   , UPPER('BillingInvoicePaidListReport')
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,NULL
			   ,'BillingInvoicePaidListReport'
			   ,3 
			   ,@ID_REPORT_BillingInvoicePaidListReport)

END

IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_BillingInvoicePaidListReport
          AND Name = 'Date_BillingInvoice'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Date_BillingInvoice',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_BillingInvoicePaidListReport, -- ID_Report - uniqueidentifier
        6,                     -- ID_ControlType - int
        6,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Date_BillingInvoice'                 -- Caption - varchar(300)
        );
END;

IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_BillingInvoicePaidListReport
          AND Name = 'Date_PaymentTransaction'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Date_PaymentTransaction',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_BillingInvoicePaidListReport, -- ID_Report - uniqueidentifier
        6,                     -- ID_ControlType - int
        6,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Date_PaymentTransaction'                 -- Caption - varchar(300)
        );
END;

IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_BillingInvoicePaidListReport
          AND Name = 'Name_Client'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Name_Client',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_BillingInvoicePaidListReport, -- ID_Report - uniqueidentifier
        1,                     -- ID_ControlType - int
        1,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Name_Client'                 -- Caption - varchar(300)
        );
END;

IF (NOT EXISTS
(
    SELECT *
    FROM dbo._tReport_Filters
    WHERE ID_Report = @ID_REPORT_BillingInvoicePaidListReport
          AND Name = 'Name_Patient'
)
   )
BEGIN

    INSERT dbo._tReport_Filters
    (
        Oid,
        Name,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_Report,
        ID_ControlType,
        ID_PropertyType,
        DataSource,
        Caption
    )
    VALUES
    (   NEWID(),               -- Oid - uniqueidentifier
        'Name_Patient',         -- Name - varchar(200)
        1,                     -- IsActive - bit
        GETDATE(),             -- DateCreated - datetime
        GETDATE(),             -- DateModified - datetime
        1,                     -- ID_CreatedBy - int
        1,                     -- ID_LastModifiedBy - int
        @ID_REPORT_BillingInvoicePaidListReport, -- ID_Report - uniqueidentifier
        1,                     -- ID_ControlType - int
        1,                     -- ID_PropertyType - int
        '',                    -- DataSource - varchar(300)
        'Name_Patient'                 -- Caption - varchar(300)
        );
END;

GO

update tEmployee Set IsSystemUsed = 1
WHERE LastName = 'System' AND
	FirstName = 'System'


update tEmployee Set IsSystemUsed = 0
WHERE LastName = 'Administrator' AND
	FirstName = 'Administrator'
