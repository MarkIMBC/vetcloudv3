﻿
IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tItem'
    AND COLUMN_NAME = 'OtherInfo_DateExpiration')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tItem'
                         ,'OtherInfo_DateExpiration'
                         ,5
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tBillingInvoice_Detail'
    AND COLUMN_NAME = 'DateExpiration')
  = 0
BEGIN

EXEC _pAddModelProperty @TableName = 'tBillingInvoice_Detail'
                       ,@PropertyName = 'DateExpiration'
                       ,@Type = 5
END;

GO


IF (NOT EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'History')
  )
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'History'
                         ,1
END;


GO
IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.fGetAge')
    AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fGetAge

GO


CREATE FUNCTION fGetAge (@dateBirth DATETIME, @InvalidDateCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = @InvalidDateCaption

  DECLARE @date DATETIME
         ,@tmpdate DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @dateBirth IS NULL
    RETURN ''

  SELECT
    @date = @dateBirth

  SELECT
    @tmpdate = @date

  SELECT
    @years = DATEDIFF(yy, @tmpdate, GETDATE()) -
    CASE
      WHEN (MONTH(@date) > MONTH(GETDATE())) OR
        (MONTH(@date) = MONTH(GETDATE()) AND
        DAY(@date) > DAY(GETDATE())) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(yy, @years, @tmpdate)
  SELECT
    @months = DATEDIFF(m, @tmpdate, GETDATE()) -
    CASE
      WHEN DAY(@date) > DAY(GETDATE()) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(m, @months, @tmpdate)
  SELECT
    @days = DATEDIFF(d, @tmpdate, GETDATE())



  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 0 THEN 's'
      ELSE ''
    END + ' old'
  IF (@years = 0
    AND @months > 0)
    SET @result = CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 0 THEN 's'
      ELSE ''
    END + ' old'
  IF (@years = 0
    AND @months = 0)
    SET @result = CONVERT(VARCHAR(MAX), @days) + ' day' +
    CASE
      WHEN @days > 0 THEN 's'
      ELSE ''
    END + ' old'


  --  SELECT
  --    @years
  --   ,@months
  --   ,@days

  RETURN @result
END

GO


IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.GetRemainingYearMonthDays')
    AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))

  DROP FUNCTION dbo.GetRemainingYearMonthDays

GO


CREATE FUNCTION dbo.GetRemainingYearMonthDays (@Date DATETIME, @RemainingCaption VARCHAR(MAX) = NULL, @InvalidRemainingCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = ''
  DECLARE @date1 DATETIME
         ,@date2 DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @Date IS NULL
    RETURN ''

  SET @RemainingCaption = ISNULL(@RemainingCaption, 'remaining')

  SELECT
    @date1 = GETDATE()
   ,@date2 = @Date

  SET @years = DATEDIFF(mm, @date1, @date2) / 12
  SET @months = DATEDIFF(mm, @date1, @date2) % 12
  SET @days = DATEDIFF(dd, DATEADD(mm, DATEDIFF(mm, @date1, @date2), @date1), @date2)

  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years = 0
    AND @months > 0)
    SET @result = CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years <= 0
    AND @months <= 0)
  BEGIN

    IF (ISNULL(@days, 0) > 0)
    BEGIN

      SET @result = CONVERT(VARCHAR(MAX), @days) + ' day' +
      CASE
        WHEN @days > 0 THEN 's'
        ELSE ''
      END + ' ' + @RemainingCaption

    END
    ELSE
    BEGIN
      SET @result = @InvalidRemainingCaption
    END

  END


  RETURN @result
END

GO


IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.vItemInventoriable_ListvIew'))
BEGIN

  DROP VIEW dbo.vItemInventoriable_ListvIew
END

GO


CREATE VIEW vItemInventoriable_ListvIew
AS

SELECT
  item.ID
 ,item.Name
 ,CASE
    WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
    ELSE ''
  END RemainingBeforeExpired
 ,DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays
 ,item.OtherInfo_DateExpiration DateExpired
 ,item.ID_Company
FROM dbo.titem item
WHERE item.ID_ItemType = 2

GO




IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.vItemInventoriableForBillingLookUp'))
BEGIN

  DROP VIEW dbo.vItemInventoriableForBillingLookUp
END

GO


CREATE VIEW vItemInventoriableForBillingLookUp
AS

SELECT
  ID
 ,Name
 ,FORMAT(ISNULL(UnitPrice, 0), '#,###,##0.00') UnitPrice
 ,FORMAT(ISNULL(UnitCost, 0), '#,###,##0.00') UnitCost
 ,FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount
 ,ISNULL(CurrentInventoryCount, 0) CurrentInventoryCount
 ,ID_Company
 ,CASE
    WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
    ELSE ''
  END RemainingBeforeExpired
 ,DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration) RemainingDays
FROM dbo.vActiveItem
WHERE ID_ItemType = 2;

GO


ALTER VIEW dbo.vPatient
AS
SELECT
  H.*
 ,UC.Name AS CreatedBy
 ,UM.Name AS LastModifiedBy
 ,H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName
 ,gender.Name Name_Gender
 ,country.Name PhoneCode_Country
 ,client.Name Name_Client
 ,dbo.fGetAge(h.DateBirth, 'N/A') Age
FROM tPatient H
LEFT JOIN tUser UC
  ON H.ID_CreatedBy = UC.ID
LEFT JOIN tUser UM
  ON H.ID_LastModifiedBy = UM.ID
LEFT JOIN dbo.tGender gender
  ON H.ID_Gender = gender.ID
LEFT JOIN dbo.tCountry country
  ON H.ID_Country = country.ID
LEFT JOIN dbo.tClient client
  ON client.ID = H.ID_Client;

GO

ALTER VIEW dbo.vzPatientSOAP
AS
SELECT
  patientSOAP.ID
 ,patientSOAP.Code
 ,patientSOAP.Name_Client
 ,patientSOAP.Name_Patient
 ,patientSOAP.Date
 ,patientSOAP.Comment
 ,patientSOAP.Name_CreatedBy
 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9), '<br/>'), CHAR(10), '<br/>'), CHAR(13), '<br/>') Subjective
 ,REPLACE(REPLACE(patientSOAP.Objective, CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
 ,REPLACE(REPLACE(patientSOAP.Prescription, CHAR(9), '<br/>'), CHAR(13), '<br/>') Prescription
 ,patientSOAP.ID_Company
 ,patientSOAP.Name_FilingStatus
 ,company.ImageLogoLocationFilenamePath
 ,company.Name Name_Company
 ,company.Address Address_Company
FROM dbo.vPatient_SOAP patientSOAP
LEFT JOIN dbo.vCompany company
  ON company.ID = patientSOAP.ID_Company;

GO



ALTER PROC dbo.pGetItem @ID INT = -1,
@ID_ItemType INT = NULL,
@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_';

  DECLARE @ID_User INT
         ,@ID_Warehouse INT;
  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM tUserSession
  WHERE ID = @ID_Session;

  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,itemType.Name Name_ItemType
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,NULL AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [ID_Company]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,NULL AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_ItemType ID_ItemType) H
    LEFT JOIN tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tItemType itemType
      ON itemType.ID = H.ID_ItemType;
  END;
  ELSE
  BEGIN
    SELECT
      H.*
     ,CASE
        WHEN H.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(H.OtherInfo_DateExpiration, '', 'Expired')
        ELSE ''
      END RemainingBeforeExpired
     ,DATEDIFF(DAY, GETDATE(), H.OtherInfo_DateExpiration) RemainingDays
    FROM vItem H
    WHERE H.ID = @ID;
  END;
END;

GO


ALTER PROC dbo.pApproveBillingInvoice
(
    @IDs_BillingInvoice typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Approved_ID_FilingStatus INT = 3;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pApproveBillingInvoice_validation @IDs_BillingInvoice,
                                                   @ID_UserSession;

        UPDATE dbo.tBillingInvoice
        SET ID_FilingStatus = @Approved_ID_FilingStatus,
            DateApproved = GETDATE(),
            ID_ApprovedBy = @ID_User
        FROM dbo.tBillingInvoice bi
            INNER JOIN @IDs_BillingInvoice ids
                ON bi.ID = ids.ID;

        /*Inventory Trail */
        INSERT INTO dbo.tInventoryTrail
        (
            Code,
            ID_Company,
            DateCreated,
            ID_Item,
            Quantity,
            UnitPrice,
            ID_FilingStatus,
            Date,
            DateExpired
        )
        SELECT hed.Code,
               hed.ID_Company,
               hed.DateCreated,
               detail.ID_Item,
               0 - detail.Quantity,
               detail.UnitPrice,
               hed.ID_FilingStatus,
               hed.Date,
               detail.DateExpiration
        FROM dbo.tBillingInvoice hed
            LEFT JOIN dbo.tBillingInvoice_Detail detail
                ON hed.ID = detail.ID_BillingInvoice
            LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item
            INNER JOIN @IDs_BillingInvoice ids
                ON hed.ID = ids.ID
        WHERE item.ID_ItemType = @Inventoriable_ID_ItemType;

        EXEC pUpdateItemCurrentInventory;

        EXEC dbo.pUpdateBillingInvoicePayment @IDs_BillingInvoice;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

ALTER PROC dbo.pCancelBillingInvoice
(
    @IDs_BillingInvoice typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Canceled_ID_FilingStatus INT = 4;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelBillingInvoice_validation @IDs_BillingInvoice,
                                                  @ID_UserSession;

        UPDATE dbo.tBillingInvoice
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tBillingInvoice bi
            INNER JOIN @IDs_BillingInvoice ids
                ON bi.ID = ids.ID;

        /*Inventory Trail */
        INSERT INTO tInventoryTrail
        (
            Code,
            ID_Company,
            DateCreated,
            ID_Item,
            Quantity,
            UnitPrice,
            ID_FilingStatus,
            Date,
            DateExpired
        )
        SELECT hed.Code,
               hed.ID_Company,
               hed.DateCreated,
               detail.ID_Item,
               detail.Quantity,
               detail.UnitPrice,
               hed.ID_FilingStatus,
               hed.Date,
               detail.DateExpiration 
        FROM dbo.tBillingInvoice hed
            LEFT JOIN dbo.tBillingInvoice_Detail detail
                ON hed.ID = detail.ID_BillingInvoice
            LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item
            INNER JOIN @IDs_BillingInvoice ids
                ON hed.ID = ids.ID
        WHERE ISNULL(hed.ID_ApprovedBy, 0) <> 0
              AND item.ID_ItemType = @Inventoriable_ID_ItemType;

        EXEC dbo.pUpdateItemCurrentInventory;

        EXEC dbo.pUpdateBillingInvoicePayment @IDs_BillingInvoice;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

Drop Proc pReceiveInventory
Go

/****** Object:  UserDefinedTableType [dbo].[typReceiveInventory]    Script Date: 2/13/2021 4:27:49 AM ******/
DROP TYPE [dbo].[typReceiveInventory]
GO

/****** Object:  UserDefinedTableType [dbo].[typReceiveInventory]    Script Date: 2/13/2021 4:27:49 AM ******/
CREATE TYPE [dbo].[typReceiveInventory] AS TABLE(
	[Code] [varchar](50) NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [decimal](18, 4) NULL DEFAULT ((0)),
	[DateExpired] [datetime] NULL,
	[BatchNo] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_Company] [int] NULL,
	Comment [varchar](MAX) NULL,
	IsAddInventory BIT
)
GO


Create PROC dbo.pReceiveInventory (@record typReceiveInventory READONLY, @ID_UserSession Int)  
AS  
BEGIN  
  
  DECLARE @Success BIT = 1;  
  DECLARE @message VARCHAR(MAX) = '';  
   
  DECLARE @ID_User INT = 0;

  DECLARE @count_noitemid INT = 0;  

  SELECT @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;
  
  BEGIN TRY  
  
    SELECT  
      @count_noitemid = COUNT(*)  
    FROM @record  
    WHERE ISNULL(ID_Item, 0) = 0;  
  
    IF (@count_noitemid > 0)  
    BEGIN;  
  
      THROW 50005, N'Reference Item is required.', 1;  
    END;  
  
    INSERT INTO dbo.tInventoryTrail (Code,  
    ID_Item,  
    Quantity,  
    UnitPrice,  
    DateExpired,  
    BatchNo,  
    ID_FilingStatus,  
    ID_Company,  
    DateCreated,  
    Date, 
	Comment,
	ID_CreatedBy)  
      SELECT  
        CASE  
          WHEN ISNULL(record.Code,'') = '' THEN 'Received Item'  
          ELSE record.Code  
        END  
       ,record.ID_Item  
       ,case when record.IsAddInventory = 0 then  0 - Abs(record.Quantity) else Abs(record.Quantity) end
       ,record.UnitPrice  
       ,record.DateExpired  
       ,record.BatchNo  
       ,record.ID_FilingStatus  
       ,item.ID_Company  
       ,GETDATE()  
       ,GETDATE()
	   ,record.Comment,
	   @ID_User
      FROM @record record  
      INNER JOIN dbo.tItem item  
        ON item.ID = record.ID_Item;  
  
    EXEC dbo.pUpdateItemCurrentInventory;  
  END TRY  
  BEGIN CATCH  
  
    SET @Success = 0;  
    SET @message = ERROR_MESSAGE();  
  END CATCH;  
  
  SELECT  
    '_';  
  
  SELECT  
    @Success Success  
   ,@message message;  
END;  
  

EXEC _pRefreshAllViews
GO

 