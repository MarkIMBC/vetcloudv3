IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ID_SOAPType'
          AND Object_ID = Object_ID(N'dbo.tBillingInvoice'))
BEGIN

	exec _pAddModelProperty 'tBillingInvoice', 'ID_SOAPType', 2
END

GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ID_Patient_SOAP'
          AND Object_ID = Object_ID(N'dbo.tBillingInvoice'))
BEGIN

	exec _pAddModelProperty 'tBillingInvoice', 'ID_Patient_SOAP', 2
END

GO

CREATE OR ALTER VIEW vClient_ListView
AS
    SELECT  ID,
			ID_Company,
            Name,
			 ContactNumber + 
			 CASE WHEN LEN(ISNULL(ContactNumber, '')) > 0 AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / ' ELSE '' END +  
			 ContactNumber2 
			 ContactNumbers,
			DateCreated,
			ISNULL(IsActive, 0) IsActive
    FROM dbo.vClient
	WHERE
		ISNULL(IsActive, 0) = 1
GO

CREATE OR ALTER VIEW vPatient_ListView
AS
    SELECT  ID,
			ID_Company,
            Code,
            Name,
            Name_Client,
            Email,
            Species,
            Name_Gender,
            ContactNumber,
            IsDeceased,
            DateLastVisited
    FROM dbo.vPatient
	WHERE
		ISNULL(IsActive, 0) = 1
GO

CREATE OR ALTER VIEW [dbo].[vItemInventoriable_ListvIew]  
AS  
	SELECT  item.ID  
		   ,item.Name  
		   ,CASE WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN 
				dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')  
		  	ELSE 
				''  
		    END RemainingBeforeExpired  
		   ,DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays  
		   ,item.OtherInfo_DateExpiration DateExpired  
		   ,item.ID_Company  
		   ,item.CurrentInventoryCount  
		   ,item.ID_InventoryStatus  
		   ,invtStatus.Name Name_InventoryStatus  
		   ,item.IsActive  
	FROM dbo.titem item  
	LEFT JOIN tInventoryStatus invtStatus   
	 on item.ID_InventoryStatus = invtStatus.ID  
	WHERE   
		item.ID_ItemType = 2  AND
		ISNULL(item.IsActive, 0) = 1 
GO

CREATE OR ALTER VIEW dbo.vItemService_Listview
AS  
	SELECT  item.ID  
		   ,item.Name  
		   ,item.ID_Company  
		   ,ISNULL(item.UnitPrice, 0) UnitPrice
		   ,item.IsActive  
	FROM dbo.titem item  
	WHERE   
		item.ID_ItemType = 1  AND
		ISNULL(item.IsActive, 0) = 1
GO

CREATE OR ALTER VIEW [dbo].[vBillingInvoice]  
AS  
	SELECT H.*,  
		   CONVERT(VARCHAR(100), H.Date, 101) DateString,  
		   UC.Name AS CreatedBy_Name_User,  
		   UM.Name AS LastModifiedBy_Name_User,  
		   approvedUser.Name AS ApprovedBy_Name_User,  
		   cancelUser.Name AS CanceledBy_Name_User,  
		   company.Name Company,  
		   patient.Name Name_Patient,  
		   taxScheme.Name Name_TaxScheme,  
		   fs.Name Name_FilingStatus,  
		   fsPayment.Name Payment_Name_FilingStatus,  
		   client.Name Name_Client,  
		   CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,  
		   CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,  
		   CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,  
		   CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString,  
		attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee,  
		CASE WHEN H.ID_FilingStatus = 3 THEN fsPayment.Name ELSE fs.Name END Status,
		soapType.Name Name_SOAPType
	FROM dbo.tBillingInvoice H  
		LEFT JOIN dbo.tUser UC  
			ON H.ID_CreatedBy = UC.ID  
		LEFT JOIN dbo.tUser UM  
			ON H.ID_LastModifiedBy = UM.ID  
		LEFT JOIN dbo.tUser approvedUser  
			ON H.ID_ApprovedBy = approvedUser.ID  
		LEFT JOIN dbo.tUser cancelUser  
			ON H.ID_CanceledBy = cancelUser.ID  
		LEFT JOIN dbo.tCompany company  
			ON H.ID_Company = company.ID  
		LEFT JOIN dbo.tPatient patient  
			ON patient.ID = H.ID_Patient  
		LEFT JOIN dbo.tTaxScheme taxScheme  
			ON taxScheme.ID = H.ID_TaxScheme  
		LEFT JOIN dbo.tFilingStatus fs  
			ON fs.ID = H.ID_FilingStatus  
		LEFT JOIN dbo.tFilingStatus fsPayment  
			ON fsPayment.ID = H.Payment_ID_FilingStatus  
		LEFT JOIN dbo.tClient client  
			ON client.ID = H.ID_Client  
		LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee  
			ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee  
		LEFT JOIN tSOAPType soapType 
			ON H.ID_SOAPType = soapType.ID
GO

ALTER VIEW [dbo].[vzInventorySummaryReport]  
	AS  
	SELECT hed.ID_Item ID,  
		   LTRIM(RTRIM(hed.Name_Item)) Name_Item,  
		   SUM(hed.Quantity) TotalQuantity,  
		   NULL DateExpired,  
		   NULL BatchNo,  
		   MAX(   CASE  
					  WHEN hed.Quantity > 0 THEN  
						  Date  
					  ELSE  
						  NULL  
				  END  
			  ) DateLastIn,  
		   MAX(   CASE  
					  WHEN hed.Quantity < 0 THEN  
						  Date  
					  ELSE  
						  NULL  
				  END  
			  ) DateLastOut, 
		   ISNULL(item.UnitCost, 0) UnitCost,
		   ISNULL(item.UnitPrice, 0) UnitPrice,
		   item.ID_Company,  
		   company.ImageLogoLocationFilenamePath,  
		   company.Name Name_Company,  
		   company.Address Address_Company,  
		CASE  
	   WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN '(' + dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired' + ')', 'Expired')  
	  ELSE ''  
	   END RemainingBeforeExpired,  
	  DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,  
	  invtStatus.Name Name_InventoryStatus,  
	  CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +  
	  CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END +   
	  CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company
	FROM dbo.vInventoryTrail hed  
	LEFT JOIN dbo.tItem item   
	 ON item.ID = hed.ID_Item  
	LEFT JOIN dbo.vCompany company  
		ON company.ID = item.ID_Company  
	LEFT JOIN dbo.tInventoryStatus invtStatus  
		ON invtStatus.ID = item.ID_InventoryStatus  
	GROUP BY ID_Item,  
			 Name_Item,  
			 ISNULL(item.UnitCost, 0),
			 ISNULL(item.UnitPrice, 0),
			 company.ImageLogoLocationFilenamePath,  
			 company.Name,  
	   OtherInfo_DateExpiration,  
			 company.Address,  
			 item.ID_Company,  
	   invtStatus.Name,  
	   company.ContactNumber,  
	   company.Email  
GO

ALTER PROC [dbo].[pGetSendSOAPPlan]( @Date DateTime, @IsSMSSent Bit = NULL)  
AS  
BEGIN  
  
  DECLARE @Success BIT = 1;  
  DECLARE @SMSSent Table (IsSMSSent bit)  
  
  if @IsSMSSent IS NULL  
 INSERT @SMSSent  
 VALUES (0), (1)  
  ELSE  
 INSERT @SMSSent  
 VALUES (@IsSMSSent)  
  
 SET @Date = ISNULL(@Date, GETDATE())  
  
  SELECT  
    
    '_'  
   ,'' AS summary  
   ,'' AS records;  
  
 Declare @record TABLE (  
       Name_Company VARCHAR(MAX),   
       Name_Client  VARCHAR(MAX),   
       ContactNumber_Client VARCHAR(MAX),  
       DateReturn DATETime,  
       Name_Item  VARCHAR(MAX),  
       Comment  VARCHAR(MAX),  
       Message VARCHAR(MAX),  
       DateSending  DATETime,  
       ID_Patient_SOAP_Plan INT  
        )  
  
INSERT @record   
SELECT  
 c.Name Name_Company  
 ,client.Name Name_Client  
 ,ISNULL(client.ContactNumber,'0') ContactNumber_Client  
 ,soapPlan.DateReturn  
 ,soapPlan.Name_Item  
 ,ISNULL(patientSOAP.Comment, '') Comment  
 ,dbo.fGetSOAPLANMessage(c.Name, client.Name , ISNULL(c.ContactNumber, ''), patient.Name,  soapPlan.Name_Item , soapPlan.DateReturn) Message  
 ,CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending  
 ,soapPlan.ID ID_Patient_SOAP_Plan  
FROM dbo.tPatient_SOAP patientSOAP  
 LEFT JOIN dbo.tPatient patient  
 ON patient.ID = patientSOAP.ID_Patient  
 LEFT JOIN dbo.tClient client  
 ON client.ID = patient.ID_Client  
 LEFT JOIN tCompany c  
 ON c.iD = patientSOAP.ID_Company  
 INNER JOIN dbo.vPatient_SOAP_Plan soapPlan  
 ON soapPlan.ID_Patient_SOAP = patientSOAP.ID  
WHERE patientSOAP.ID_FilingStatus IN (1, 3)  
AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent FROM @SMSSent)  
AND ISNULL(ID_CLient,0) > 0  
AND ISNULL(patient.IsDeceased,0) = 0  
AND (   
  (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date))   
 OR  
  (CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date))   
 )  
AND patientSOAP.ID_Company IN (  
 SELECT  ID_Company  
 FROM    tSMSPatientSOAP_Company  
)  
ORDER BY c.Name  
  
SELECT  
    @Success Success;  
  
SELECT    
  FORMAT(DateSending,'yyyy-MM-dd') DateSending  
  ,tbl.Name_Company   
  ,Count(*) Count  
  ,SUM(CASE WHEN LEN(tbl.Message) <= 160 THEN 1 ELSE   
  CASE WHEN LEN(tbl.Message) <= 306 THEN 2 ELSE   
   CASE WHEN LEN(tbl.Message) <= 459 THEN 3 ELSE   
    4  
   END  
  END  
 END ) ConsumedSMSCredit  
FROM      
 (  
  SELECT  *  
  FROM    @record  
 ) tbl   
GROUP BY      
  FORMAT(DateSending,'yyyy-MM-dd'),  
  Name_Company  
Order BY   
  DateSending DESC,
  Name_Company

   
END  
  
SELECT  *  
FROM    @record  
Order BY 
	FORMAT(DateSending,'yyyy-MM-dd') DESC,  
	DateReturn ASC, 
	Name_Company
GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_SOAP]
(
    @ID_CurrentObject VARCHAR(10),
    @IsNew BIT = 0
)
AS
BEGIN

    IF @IsNew = 1
    BEGIN

        /* Generate Document Series */
        DECLARE @Oid_Model UNIQUEIDENTIFIER;
        DECLARE @Code VARCHAR(MAX) = '';
        DECLARE @ID_Company INT;

        SELECT @ID_Company = ID_Company
        FROM dbo.tPatient_SOAP
        WHERE ID = @ID_CurrentObject;

        SELECT @Oid_Model = m.Oid
        FROM dbo._tModel m
        WHERE m.TableName = 'tPatient_SOAP';

        SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

        UPDATE dbo.tDocumentSeries
        SET Counter = Counter + 1
        WHERE ID_Model = @Oid_Model
              AND ID_Company = @ID_Company;

        UPDATE dbo.tPatient_SOAP
        SET Code = @Code
        WHERE ID = @ID_CurrentObject;
    END;


	DECLARE @IDs_Patient typIntList

	INSERT @IDs_Patient 
	SELECT patient.ID 
	FROM 
		tPatient_SOAP soap INNER JOIN 
		tPatient patient ON
			patient.ID = soap.ID_Patient
	WHERE
		soap.ID = @ID_CurrentObject

	EXEC dbo.pUpdatePatientsLastVisitedDate  @IDs_Patient 

END;

GO

CREATE OR ALTER PROC [dbo].[pValidateItem]  
(  
	@ID_Item INT,  
	@Name VARCHAR(MAX),  
	@ID_ItemType INT,  
	@ID_UserSession INT  
)  
AS  
BEGIN  
  
    DECLARE @isValid BIT = 1;  
    DECLARE @IsWarning BIT = 0;  
    DECLARE @message VARCHAR(MAX) = '';  
    DECLARE @msgTHROW VARCHAR(MAX) = '';  
  
    DECLARE @ID_User INT = 0;  
    DECLARE @ID_Company INT = 0;  
  
    SELECT   
  @ID_User = userSession.ID_User,  
  @ID_Company = _user.ID_Company  
    FROM dbo.tUserSession userSession INNER JOIN vUser _user ON userSession.ID_User = _user.ID  
    WHERE userSession.ID = @ID_UserSession;  
  
 SET @Name = LTRIM(RTRIM(ISNULL(@Name,'')))  
  
 if ISNULL(@ID_Company, 0) = 0  
 BEGIN  
  
  SELECT    
   @ID_Company = ID_Company  
  FROM tItem  
  WHERE   
   ID = @ID_Item  
 END  
  
    BEGIN TRY  
  
  DECLARE @ExistName Varchar(MAX) = ''  
  DECLARE @Count INT = 0  
  
  IF LEN(@Name) = 0  
  BEGIN  
     
   SET @msgTHROW = CASE WHEN @ID_ItemType = 2 THEN 'Item' ELSE 'Service' END  + ' is required.';  
   THROW 50005, @msgTHROW, 1;  
  END  
    
    
  SELECT    
   @ExistName = Name  
  FROM tItem   
  WHERE   
   ID NOT IN (@ID_Item) AND (  
   LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND  
   IsActive = 1 AND  
   ID_ItemType = @ID_ItemType AND
   ID_Company = @ID_Company)  
  
  
  IF LEN(LTRIM(RTRIM(ISNULL(LOWER(@ExistName), '')))) > 0  
  BEGIN  
  
   SET @msgTHROW = @Name +''' is already exist';  
  
   SELECT    
    @Count = COUNT(*)  
   FROM tItem   
   WHERE   
    (  
    LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND  
    IsActive = 1 AND  
	ID_ItemType = @ID_ItemType AND
    ID_Company = @ID_Company)  
   GROUP BY  
    Name  
       
   if(@Count > 1)  
   BEGIN  
  
		SET @IsWarning = 1  
		SET @msgTHROW = @msgTHROW + ' ' + FORMAT(@Count,'0') + ' times'  
   END;  
  
   set @msgTHROW = @msgTHROW + '.';  
  
   THROW 50005, @msgTHROW, 1;  
  END;  
     
    END TRY  
    BEGIN CATCH  
  
        SET @message = ERROR_MESSAGE();  
        SET @isValid = 0;  
    END CATCH;  
  
    SELECT '_';   
  
    SELECT @isValid isValid,  
     @IsWarning isWarning,  
           @message message;  
  
END;  

GO  

ALTER PROC [dbo].[pApprovePatient_SOAP]
(
    @IDs_Patient_SOAP typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Approved_ID_FilingStatus INT = 3;
    DECLARE @Pending_ID_FilingStatus INT = 2;
	DECLARE @IDs_Patient typIntList

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pApprovePatient_SOAP_validation @IDs_Patient_SOAP,
                                                  @ID_UserSession;
		
		INSERT @IDs_Patient 
		SELECT
			patient.ID 
		FROM 
		tPatient_SOAP soap 
		INNER JOIN tPatient patient ON
			patient.ID = soap.ID_Patient
		INNER JOIN @IDs_Patient_SOAP idsSOAP ON
			idsSOAP.ID = soap.ID

		EXEC dbo.pUpdatePatientsLastVisitedDate  @IDs_Patient 

        UPDATE dbo.tPatient_SOAP
        SET ID_FilingStatus = @Approved_ID_FilingStatus,
            DateApproved = GETDATE(),
            ID_ApprovedBy = @ID_User
        FROM dbo.tPatient_SOAP bi
            INNER JOIN @IDs_Patient_SOAP ids
                ON bi.ID = ids.ID;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;
GO

ALTER PROC [dbo].[pCancelPatient_SOAP]
(
    @IDs_Patient_SOAP typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Canceled_ID_FilingStatus INT = 4;
    DECLARE @Pending_ID_FilingStatus INT = 2;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;
		DECLARE @IDs_Patient typIntList

		INSERT @IDs_Patient
		SELECT 
			DISTINCT ID_Patient
		FROM    tPatient_SOAP soap
		INNER JOIN @IDs_Patient_SOAP tbl
			ON soap.ID = tbl.ID

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelPatient_SOAP_validation @IDs_Patient_SOAP,
                                                  @ID_UserSession;

        UPDATE dbo.tPatient_SOAP
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tPatient_SOAP bi
            INNER JOIN @IDs_Patient_SOAP ids
                ON bi.ID = ids.ID;

		exec dbo.pUpdatePatientsLastVisitedDate @IDs_Patient

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

ALTER PROC [dbo].[pGetBillingInvoice]  
    @ID INT = -1,  
    @ID_Client INT = NULL,  
    @ID_Patient INT = NULL,  
    @ID_Session INT = NULL,  
    @ID_Patient_SOAP INT = NULL,  
    @AttendingPhysician_ID_Employee INT = NULL  
AS  
BEGIN  
    SELECT '_',  
           '' BillingInvoice_Detail;  
    
    DECLARE @ID_SOAPType INT = 0
	DECLARE @Name_SOAPType VARCHAR(MAX) = ''

    DECLARE @ID_User INT,  
            @ID_Warehouse INT;  

    SELECT @ID_User = ID_User,  
           @ID_Warehouse = ID_Warehouse  
    FROM dbo.tUserSession  
    WHERE ID = @ID_Session;  

	IF(ISNULL(@ID_Patient_SOAP, 0) > 0)
	BEGIN
		
		SELECT 
			@ID_SOAPType = ID_SOAPType,
			@Name_SOAPType = Name_SOAPType
		FROM vPatient_SOAP
		WHERE 
			ID = @ID_Patient_SOAP 

	END
  
    IF (@ID = -1)  
    BEGIN  
        SELECT H.*,  
               fs.Name Name_FilingStatus,  
               client.Name Name_Client,  
			   patient.Name Name_Patient,  
			   client.Address BillingAddress,  
			   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee  
        FROM  
        (  
            SELECT NULL AS [_],  
                   -1 AS [ID],  
                   '- NEW -' AS [Code],  
                   NULL AS [Name],  
                   1 AS [IsActive],  
                   GETDATE() AS Date,  
                   NULL AS [ID_Company],  
                   NULL AS [Comment],  
                   NULL AS [DateCreated],  
                   NULL AS [DateModified],  
                   0 AS [ID_CreatedBy],  
                   0 AS [ID_LastModifiedBy],  
                   1 AS [ID_FilingStatus],  
                   0 AS [ID_Taxscheme],  
                   @ID_Client ID_Client,  
                   0 IsComputeDiscountRate,  
                   @ID_Patient ID_Patient,  
				   @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
		    	   @ID_SOAPType ID_SOAPType,
		    	   @Name_SOAPType Name_SOAPType,
				   @ID_Patient_SOAP ID_Patient_SOAP
        ) H  
            LEFT JOIN dbo.tUser UC  
                ON H.ID_CreatedBy = UC.ID  
            LEFT JOIN dbo.tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
            LEFT JOIN dbo.tFilingStatus fs  
                ON H.ID_FilingStatus = fs.ID  
            LEFT JOIN dbo.tClient client  
                ON client.ID = H.ID_Client  
            LEFT JOIN dbo.tPatient patient  
                ON patient.ID = H.ID_Patient  
		   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee  
				ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee  
    END;  
    ELSE  
    BEGIN  
        SELECT H.*  
        FROM dbo.vBillingInvoice H  
        WHERE H.ID = @ID;  
    END;  
  
 if ISNULL(@ID_Patient_SOAP, 0) > 0 AND (@ID = -1)  
 BEGIN  
  
	SELECT   
		(ROW_NUMBER() OVER(ORDER BY soapPrescription.ID DESC)) - 999999  ID,  
		soapPrescription.ID_Item,   
		soapPrescription.Name_Item,   
		ISNULL(soapPrescription.Quantity, 0) Quantity,  
		ISNULL(item.UnitPrice, 0) UnitPrice,  
		item.OtherInfo_DateExpiration DateExpiration  
	FROM dbo.vPatient_SOAP_Prescription soapPrescription  
	INNER JOIN tItem item   
		ON item.ID = soapPrescription.ID_Item  
	WHERE 
		ID_Patient_SOAP = @ID_Patient_SOAP;  
 END  
 ELSE  
 BEGIN  
  
	SELECT *  
	FROM dbo.vBillingInvoice_Detail  
	WHERE ID_BillingInvoice = @ID;  
 END  
  
END;

GO

DECLARE @IDs_Patient typIntList

INSERT @IDs_Patient
SELECT 
	DISTINCT ID_Patient
FROM    tPatient_SOAP soap

exec dbo.pUpdatePatientsLastVisitedDate @IDs_Patient