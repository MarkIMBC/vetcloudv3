GO

ALTER VIEW villingInvoiceWalkInList
AS
  SELECT H.*
  FROM   vBillingInvoiceWalkInList H

GO

IF NOT EXISTS (SELECT 1
               FROM   sys.objects
               WHERE  object_id = Object_id(N'[dbo].[tClientAppointmentRequest]')
                      AND type IN ( N'U' ))
  BEGIN
      EXEC _pcreateappmodulewithtable
        'tClientAppointmentRequest',
        1,
        1,
        NULL

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_Client',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_Patient',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'AttendingPhysician_ID_Employee',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'DateStart',
        5

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_SOAPType',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'DateEnd',
        5

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_FilingStatus',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_ApprovedBy',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'ID_CanceledBy',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'DateApproved',
        5

      EXEC _paddmodelproperty
        'tClientAppointmentRequest',
        'DateCanceled',
        5

      DECLARE @ClientAppointmentRequest_Oid_Model VARCHAR(MAX) = ''

      SELECT @ClientAppointmentRequest_Oid_Model = Oid
      FROm   _tModel
      WHERE  TableName = 'tClientAppointmentRequest'

      if(SELECT COUNT(*)
         FROm   tDocumentSeries
         WHERE  ID_Model = @ClientAppointmentRequest_Oid_Model) = 0
        BEGIN
            INSERT INTO tDocumentSeries
                        (Code,
                         DigitCount,
                         Name,
                         Comment,
                         ID_CreatedBy,
                         ID_Model,
                         Counter,
                         IsActive,
                         ID_Company,
                         ID_LastModifiedBy,
                         Prefix)
            VALUES      ('ClientAppointmentRequest',
                         6,
                         'ClientAppointmentRequest',
                         NULL,
                         94,
                         @ClientAppointmentRequest_Oid_Model,
                         1,
                         1,
                         1,
                         94,
                         'CAR');

            DECLARE @ID_DocumentSeries INT = 0

            SELECT @ID_DocumentSeries = ID
            FROM   tDocumentSeries
            WHERE  ID_Model = @ClientAppointmentRequest_Oid_Model
                   and ID_Company = 1

            INSERT dbo.tDocumentSeries
                   (Code,
                    Name,
                    IsActive,
                    Comment,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy,
                    ID_Model,
                    Counter,
                    Prefix,
                    IsAppendCurrentDate,
                    DigitCount,
                    ID_Company)
            SELECT docSeries.Code,
                   docSeries.Name,
                   docSeries.IsActive,
                   docSeries.Comment,
                   docSeries.DateCreated,
                   docSeries.DateModified,
                   docSeries.ID_CreatedBy,
                   docSeries.ID_LastModifiedBy,
                   docSeries.ID_Model,
                   1,
                   docSeries.Prefix,
                   docSeries.IsAppendCurrentDate,
                   5,
                   company.ID
            FROM   dbo.tDocumentSeries docSeries,
                   tCompany company
            WHERE  docSeries.ID_Company = 1
                   AND docSeries.ID = @ID_DocumentSeries
                   And company.ID <> 1;
        END

      Declare @Oid_ClientAppointmentRequest_ListView UNIQUEIDENTIFIER

      SELECT @Oid_ClientAppointmentRequest_ListView = Oid
      FROM   _tListView
      WHERE  Name = 'ClientAppointmentRequest_ListView'

      IF(SELECT COUNT(*)
         FROM   tCustomNavigationLink
         WHERE  Name = 'ClientAppointmentRequest_ListView') = 0
        BEGIN
            INSERT INTO [dbo].[tCustomNavigationLink]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [Oid_ListView],
                         [RouterLink],
                         [ID_ViewType],
                         [Oid_Report])
            VALUES      (NULL,
                         'ClientAppointmentRequest_ListView',
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @Oid_ClientAppointmentRequest_ListView,
                         'ClientAppointmentRequestList',
                         1,
                         NULL)
        END
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   sys.objects
               WHERE  object_id = Object_id(N'[dbo].[tClientAppointmentRequest_Patient]')
                      AND type IN ( N'U' ))
  BEGIN
      EXEC _pcreateappmodulewithtable
        'tClientAppointmentRequest_Patient',
        1,
        1,
        NULL

      EXEC _paddmodelproperty
        'tClientAppointmentRequest_Patient',
        'ID_ClientAppointmentRequest',
        2

      EXEC _paddmodelproperty
        'tClientAppointmentRequest_Patient',
        'ID_Patient',
        2

      exec _pAddModelDetail
        'tClientAppointmentRequest_Patient',
        'ID_ClientAppointmentRequest',
        'tClientAppointmentRequest',
        'tClientAppointmentRequest'
  END

EXEC _prefreshallviews

GO

IF NOT EXISTS (SELECT 1
               FROM   sys.objects
               WHERE  object_id = Object_id(N'[dbo].[tClientFeedback]')
                      AND type IN ( N'U' ))
  BEGIN
      EXEC _pcreateappmodulewithtable
        'tClientFeedback',
        1,
        1,
        NULL

      EXEC _paddmodelproperty
        'tClientFeedback',
        'ID_Client',
        2

      Declare @Oid_ClientFeedback_ListView UNIQUEIDENTIFIER

      SELECT @Oid_ClientFeedback_ListView = Oid
      FROM   _tListView
      WHERE  Name = 'ClientFeedback_ListView'

      IF(SELECT COUNT(*)
         FROM   tCustomNavigationLink
         WHERE  Name = 'ClientFeedback_ListView') = 0
        BEGIN
            INSERT INTO [dbo].[tCustomNavigationLink]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [Oid_ListView],
                         [RouterLink],
                         [ID_ViewType],
                         [Oid_Report])
            VALUES      (NULL,
                         'ClientFeedback_ListView',
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @Oid_ClientFeedback_ListView,
                         'ClientFeedbackList',
                         1,
                         NULL)
        END
  END

GO

GO

IF NOT EXISTS (SELECT 1
               FROM   sys.objects
               WHERE  object_id = Object_id(N'[dbo].[tAnnouncement]')
                      AND type IN ( N'U' ))
  BEGIN
      EXEC _pcreateappmodulewithtable
        'tAnnouncement',
        1,
        1,
        NULL

      EXEC _paddmodelproperty
        'tAnnouncement',
        'DateStart',
        5

      EXEC _paddmodelproperty
        'tAnnouncement',
        'DateEnd',
        5

      Declare @Oid_Appointment_ListView UNIQUEIDENTIFIER

      SELECT @Oid_Appointment_ListView = Oid
      FROM   _tListView
      WHERE  Name = 'Announcement_ListView'

      IF(SELECT COUNT(*)
         FROM   tCustomNavigationLink
         WHERE  Name = 'Announcement_ListView') = 0
        BEGIN
            INSERT INTO [dbo].[tCustomNavigationLink]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [Oid_ListView],
                         [RouterLink],
                         [ID_ViewType],
                         [Oid_Report])
            VALUES      (NULL,
                         'Announcement_ListView',
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @Oid_Appointment_ListView,
                         'AnnouncementList',
                         1,
                         NULL)
        END
  END

exec _pRefreshAllViews

GO

IF COL_LENGTH('dbo.tClient', 'ID_User') IS NULL
  BEGIN
      EXEC _paddmodelproperty
        'tClient',
        'ID_User',
        2
  END

GO

CREATE OR
ALTER VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         H.DateLastVisited,
         H.CurrentCreditAmount,
         acct.Username,
         acct.Password
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tUser acct
                ON H.ID_User = acct.ID

GO

CREATE OR
ALTER VIEW dbo.vClientPatient
AS
  SELECT patient.ID,
         patient.Name Name_Patient,
         client.ID    ID_Client,
         client.Name  Name_Client,
         client.ID_Company,
         _user.ID     ID_User
  FROm   tPatient patient
         INNER JOIN tClient client
                 on patient.ID_Client = client.ID
         Inner JOIN tUser _user
                 on _user.ID = client.ID_User

GO

CREATE  OR
ALTER VIEW vClientAppointmentRequest
AS
  SELECT H.*,
         UC.Name                  CreatedBy,
         UM.Name                  LastModifiedBy,
         client.Name              Name_Client,
         patient.Name             Name_Patient,
         fs.Name                  Name_FilingStatus,
         soapType.Name            Name_SOAPType,
         _AttendingPhysician.Name AttendingPhysician_Name_Employee
  FROM   tClientAppointmentRequest H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType

GO

CREATE OR
ALTER VIEW vClientAppointmentRequest_ListView
AS
  SELECT ID,
         ID_Company,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         DateStart,
         DateEnd,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         AttendingPhysician_Name_Employee
  FROM   vClientAppointmentRequest
  WHERE  ID_FilingStatus NOT IN ( 4 )

GO

CREATE  OR
ALTER VIEW dbo.vClientUser
AS
  SELECT h.*,
         C.Name       AS Client,
         C.ID         AS ID_Client,
         C.Name       AS Name_Client,
         UG.Name      AS UserGroup,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         C.ID_Company,
         company.Name Name_Company,
         company.Code Code_Company,
         company.Guid,
         company.ReceptionPortalGuid
  FROM   dbo.tUser h
         LEFT JOIN dbo.tClient C
                ON h.ID = C.ID_User
         LEFT JOIN dbo.tUserGroup UG
                ON UG.ID = h.ID_UserGroup
         LEFT JOIN tUser UC
                ON h.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON h.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tCompany company
                ON company.ID = C.ID_Company;

GO

CREATE OR
ALTER PROCEDURE pGetClientAppointmentRequest @ID         INT = -1,
                                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS ClientAppointmentRequest_Patient

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Client    INT
      DECLARE @FILED_ID_FilingStatus INT = 1;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Client = ID
      FROM   tClient
      WHERE  ID_User = @ID_User

      IF ( @ID = -1 )
        BEGIN
            DECLARE @_dateStart DateTime = Convert(datetime, FORMAT(GETDATE(), 'yyyy-MM-dd HH:00:00.000'))
            DECLARE @_dateEnd DateTime = DATEADD(HOUR, 1, @_dateStart)

            SELECT H.*,
                   client.Name Name_Client,
                   fs.Name     Name_FilingStatus
            FROM   (SELECT NULL                   AS [_],
                           -1                     AS [ID],
                           '- New -'              AS [Code],
                           NULL                   AS [Name],
                           1                      AS [IsActive],
                           NULL                   AS [ID_Company],
                           NULL                   AS [Comment],
                           NULL                   AS [DateCreated],
                           NULL                   AS [DateModified],
                           @_dateStart            AS [DateStart],
                           @_dateEnd              AS [DateEnd],
                           NULL                   AS [ID_CreatedBy],
                           NULL                   AS [ID_LastModifiedBy],
                           @FILED_ID_FilingStatus AS ID_FilingStatus,
                           @ID_Client             AS ID_Client,
                           NULL                   AS ID_SOAPType) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON H.ID_Client = client.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vClientAppointmentRequest H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROm   vClientAppointmentRequest_Patient
      WHERE  ID_ClientAppointmentRequest = @ID
  END

GO

CREATE OR
ALTER VIEW vClientAppointmentRequest_Patient
AS
  SELECT H.*,
         patient.Name Name_Patient,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy
  FROM   tClientAppointmentRequest_Patient H
         INNER JOIN tPatient patient
                 ON H.ID_Patient = patient.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

CREATE OR
ALTER PROC dbo.pCreateClientUser (@ID_Client INT,
                                  @Username  VARCHAR(100),
                                  @Password  VARCHAR(100))
AS
  BEGIN
      DECLARE @ClientName VARCHAR(100)
      DECLARE @IsActive INT=1;
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @ID_UserGroup INT=9;
      DECLARE @ID_LastModifiedBy INT=1;
      DECLARE @ID_CreatedBy INT=1;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SET @ClientName=(SELECT Name
                       FROM   tClient
                       WHERE  ID = @ID_Client)

      BEGIN TRY
          INSERT INTO [dbo].[tUser]
                      ([Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [Username],
                       [ID_UserGroup],
                       [Password])
          VALUES      (@ClientName,
                       @IsActive,
                       @DateCreated,
                       @DateModified,
                       @ID_CreatedBy,
                       @ID_LastModifiedBy,
                       @Username,
                       @ID_UserGroup,
                       @Password)

          Update tClient
          SET    ID_User = @@IDENTITY
          WHERE  ID = @ID_Client
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '';

      SELECT @Success Success,
             @message message;
  END

GO

CREATE OR
ALTER PROC pDoClientLogin(@Username VARCHAR(300),
                          @Password VARCHAR(300))
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0
    DECLARE @ID_UserSession INT = 0

    BEGIN TRY
        EXEC pDoClientLogin_validation
          @Username,
          @Password

        SELECT @ID_User = ID
        FROM   tUser
        WHERE  username = @Username
               AND Password = @Password
               AND IsActive = 1

        INSERT INTO [dbo].[tUserSession]
                    ([Code],
                     [Name],
                     [IsActive],
                     [Comment],
                     [ID_User],
                     [ID_Warehouse],
                     [DateCreated])
        VALUES      (NULL,
                     NULL,
                     1,
                     '',
                     @ID_User,
                     1,
                     GetDate())

        SET @ID_UserSession = @@IDENTITY
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_',
           '' CurrentUser;

    SELECT @Success Success,
           @message message;

    SELECT _user.*,
           client.Name,
           @ID_UserSession ID_UserSession,
           client.ID       ID_User_Reference,
           model.Oid       ID_Model_Reference,
           company.dbusername,
           company.dbpassword,
           company.Code    Code_Company
    FROM   vClientUser _user
           INNER JOIN tClient client
                   ON _user.ID = client.ID_User
           INNER JOIN tCompany company
                   ON client.ID_Company = company.ID
           INNER JOIN _tModel model
                   ON model.Name = 'Client'
    WHERE  _user.ID = @ID_User

GO

CREATE OR
ALTER PROC [dbo].[pDoClientLogin_validation](@Username VARCHAR(300),
                                             @Password VARCHAR(300))
AS
    DECLARE @message VARCHAR(400) = '';
    /*Validate if Username is not exist.*/
    DECLARE @Count_ValidateUserNameExist INT = 0;
    DECLARE @ID_Company INT

    SELECT @Count_ValidateUserNameExist = COUNT(*)
    FROM   tUser _user
           inner join tClient client
                   on _user.ID = client.ID_User
    Where  _user.username = @Username
           and _user.IsActive = 1

    IF( @Count_ValidateUserNameExist = 0 )
      BEGIN
          SET @message = 'Username is not exist.';

          THROW 50001, @message, 1;
      END

    /*Validate if Password does not match.*/
    DECLARE @Count_ValidateUserPasswordDoesNotMatch INT = 0;

    SELECT @Count_ValidateUserPasswordDoesNotMatch = COUNT(*)
    FROM   tUser _user
           inner join tClient client
                   on _user.ID = client.ID_User
    Where  _user.username = @Username
           and _user.Password = @Password
           and _user.IsActive = 1

    IF( @Count_ValidateUserPasswordDoesNotMatch = 0 )
      BEGIN
          SET @message = 'Username and Password does not match.';

          THROW 50001, @message, 1;
      END

    /*Validate if User is multiple accounts.*/
    DECLARE @Count_ValidateUserMultipleAccounts INT = 0;

    SELECT @Count_ValidateUserMultipleAccounts = COUNT(*)
    FROM   tUser
    Where  username = @Username
           and Password = @Password
           and IsActive = 1

    IF( @Count_ValidateUserMultipleAccounts > 1 )
      BEGIN
          SET @message = 'User cannot identify login account. Please contact Vet Cloud Support.';

          THROW 50001, @message, 1;
      END

    /*Validate if User is Inactive.*/
    DECLARE @Count_ValidateUserInactive INT = 0;

    SELECT @Count_ValidateUserInactive = COUNT(*)
    FROM   tUser
    Where  username = @Username
           and Password = @Password
           and IsActive = 0

    IF( @Count_ValidateUserInactive = 1 )
      BEGIN
          SET @message = 'Login account is inactive.';

          THROW 50001, @message, 1;
      END

    /*Validate if Company is Inactive.*/
    DECLARE @IsActive_Company BIT = 0;

    SELECT @ID_Company = ID_Company
    FROM   vClientUser
    Where  username = @Username
           and Password = @Password

    SELECT @IsActive_Company = ISNULL(IsActive, 0)
    FROM   tCompany
    WHERE  ID = @ID_Company

    IF( @IsActive_Company = 0 )
      BEGIN
          SET @message = 'Company Access is inactive. Please contact Vet Cloud Support.';

          THROW 50001, @message, 1;
      END

/*Validate if User is is not yet subscribe.*/
--   DECLARE @isSubscribed BIT = 0;        
--DECLARE @CurrentDate DateTIME = '2021-08-19 00:00:00:000'      
--SELECT @ID_Company = ID_Company      
--   FROM   vUser        
--   Where  username = @Username        
--          and Password = @Password        
--          and IsActive = 1        
--SELECT @isSubscribed = isSubscribed      
--FROM   dbo.fGetCompany_CurrentSubscription(@ID_Company)       
----   if(@isSubscribed = 0)      
----BEGIN      
----      SET @message = 'Login account is not yet subscribe.';        
----         THROW 50001, @message, 1;        
----END 
GO

CREATE OR
ALTER PROC dbo.pAddClientFeedback (@Feedback       VARCHAR(600),
                                   @ID_Client      INT,
                                   @ID_UserSession INT)
AS
  BEGIN
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @IsActive INT =1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Success BIT = 1;
      DECLARE @ID_Company INT;
      DECLARE @ID_User INT;

      SET @ID_Company= (SELECT ID_Company
                        FROM   tClient
                        WHERE  ID = @ID_Client)

      SELECT @ID_User = ID_User
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      BEGIN TRY
          INSERT INTO [dbo].[tClientFeedback]
                      ([IsActive],
                       [ID_Company],
                       [Comment],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy])
          VALUES      (@IsActive,
                       @ID_Company,
                       @Feedback,
                       @DateCreated,
                       @DateModified,
                       @ID_User,
                       @ID_User )
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE OR
ALTER PROC dbo.pCreateClientUser (@ID_Client INT,
                                  @Username  VARCHAR(100),
                                  @Password  VARCHAR(100))
AS
  BEGIN
      DECLARE @ClientName VARCHAR(100)
      DECLARE @IsActive INT=1;
      DECLARE @DateCreated DATE = GETDATE();
      DECLARE @DateModified DATE = GETDATE();
      DECLARE @ID_UserGroup INT=9;
      DECLARE @ID_LastModifiedBy INT=1;
      DECLARE @ID_CreatedBy INT=1;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SET @ClientName=(SELECT Name
                       FROM   tClient
                       WHERE  ID = @ID_Client)

      BEGIN TRY
          INSERT INTO [dbo].[tUser]
                      ([Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [Username],
                       [ID_UserGroup],
                       [Password])
          VALUES      (@ClientName,
                       @IsActive,
                       @DateCreated,
                       @DateModified,
                       @ID_CreatedBy,
                       @ID_LastModifiedBy,
                       @Username,
                       @ID_UserGroup,
                       @Password)

          Update tClient
          SET    ID_User = (SELECT ID
                            FROM   tUser
                            WHERE  Name = @ClientName)
          WHERE  ID = @ID_Client
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pApprove_ClientAppointmentRequest] (@IDs_ClientAppointment typIntList READONLY,
                                                      @ID_UserSession        INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IsActive INT=1;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tClientAppointmentRequest
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 ID_ApprovedBy = @ID_User
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientAppointment)

          INSERT INTO [dbo].[tPatientAppointment]
                      ([Comment],
                       [IsActive],
                       [ID_Company],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [ID_Patient],
                       [DateStart],
                       [DateEnd],
                       [ID_Doctor],
                       [ID_SOAPType],
                       [ID_Client],
                       [ID_FilingStatus])
          SELECT req.Comment,
                 @IsActive,
                 req.ID_Company,
                 GETDATE(),
                 GETDATE(),
                 1,
                 1,
                 pat.ID_Patient,
                 req.DateStart,
                 req.DateEnd,
                 req.AttendingPhysician_ID_Employee,
                 req.ID_SOAPType,
                 req.ID_Client,
                 @Filed_ID_FilingStatus
          FROM   vClientAppointmentRequest req
                 INNER JOIN vClientAppointmentRequest_Patient pat
                         ON req.ID = pat.ID_ClientAppointmentRequest
          WHERE  req.ID IN (SELECT ID
                            FROM   @IDs_ClientAppointment)
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE  OR
ALTER PROC [dbo].[pModel_AfterSaved_ClientAppointmentRequest] (@ID_CurrentObject VARCHAR(10),
                                                               @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Lodging
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tClientAppointmentRequest';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tClientAppointmentRequest
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

GO

Alter PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_Client] (@ID_CurrentObject VARCHAR(10),
                                             @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @username VARCHAR(MAX);
            DECLARE @password VARCHAR(MAX);

            SELECT @ID_Company = ID_Company
            FROM   dbo.tClient
            WHERE  ID = @ID_CurrentObject;

            SELECT @username = Name
            FROM   tClient
            WHERE  ID = @ID_CurrentObject;

            SET @username = LOWER(REPLACE(LTRIM(RTRIM(@username)), '  ', ' '))
            SET @password = LEFT(NewId(), 4);

            EXEC pCreateClientUser
              @ID_CurrentObject,
              @username,
              @password

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tClient';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tClient
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Client TYPINTLIST

      INSERT @IDs_Client
      VALUES (@ID_CurrentObject)
  END;

GO

CREATE OR
ALTER PROC [dbo].[pGetClient] @ID         INT = -1,
                              @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Client_Patient;

      DECLARE @ID_User INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @UserName VARCHAR(MAX);
      DECLARE @Password VARCHAR(MAX);
      DECLARE @CommentTemplate VARCHAR(MAX)= '' + 'Owner''s birthday: ' + CHAR(9) + CHAR(13)
        + 'Occupation: ' + CHAR(9) + CHAR(13)
        + 'How did you find out about our clinic? : '
        + CHAR(9) + CHAR(13) + 'Reffered by: ' + CHAR(9)
        + CHAR(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserName = Username,
             @Password = Password
      FROM   vClient client
             inner join tUser _user
                     on client.ID_User = _user.ID
      WHERE  client.ID = @ID
             and _user.IsActive = 1

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL             AS [_],
                           -1               AS [ID],
                           NULL             AS [Code],
                           NULL             AS [Name],
                           1                AS [IsActive],
                           ''               [ContactNumber],
                           ''               [ContactNumber2],
                           NULL             AS [ID_Company],
                           NULL             AS [Comment],
                           NULL             AS [DateCreated],
                           NULL             AS [DateModified],
                           NULL             AS [ID_CreatedBy],
                           NULL             AS [ID_LastModifiedBy],
                           @UserName        AS [Username],
                           @Password        AS [Password],
                           @CommentTemplate AS CommentTemplate) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @CommentTemplate CommentTemplate
            FROM   dbo.vClient H
            WHERE  H.ID = @ID
        END;

      SELECT patient.*
      FROM   dbo.vPatient patient
      WHERE  patient.ID_Client = @ID
             AND ISNULL(patient.IsActive, 0) = 1
  END;

GO

CREATE OR
ALTER PROCEDURE pGetAnnouncement @ID         INT = -1,
                                 @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vAnnouncement H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROCEDURE pGetClientFeedBack @ID         INT = -1,
                                   @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vClientFeedback H
            WHERE  H.ID = @ID
        END
  END

GO

GO

CREATE  OR
ALTER PROC [dbo].[pCancel_ClientAppointmentRequest] (@IDs_ClientAppointment typIntList READONLY,
                                                     @ID_UserSession        INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @IDs_Patient typIntList

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelPatient_SOAP_validation
            @IDs_ClientAppointment,
            @ID_UserSession;

          UPDATE dbo.tClientAppointmentRequest
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientAppointmentRequest bi
                 INNER JOIN @IDs_ClientAppointment ids
                         ON bi.ID = ids.ID;
      --exec dbo.pUpdatePatientsLastVisitedDate
      --  @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancel_ClientAppointmentRequest_validation] (@IDs_ClientAppointment typIntList READONLY,
                                                                @ID_UserSession        INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @PartiallyServed_ID_FilingStatus INT = 5;
      DECLARE @FullyServed_ID_FilingStatus INT = 6;
      DECLARE @OverServed_ID_FilingStatus INT = 7;
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Billing Invoices Status is not Approved*/
      INSERT @ValidateNotApproved
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientAppointmentRequest bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientAppointment ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus, @Done_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO

Update _tNavigation
SET    ID_Parent = '24C522DA-EA51-439F-BE1B-F7D096BEEF51',
       Caption = 'Client Appt. Requests'
WHERE  Name = 'ClientAppointmentRequest_ListView'

GO

Update _tNavigation
SET    ID_Parent = '6D6F6247-DE8B-44CC-94E4-10C16EF776F2',
       Caption = 'Feedback'
WHERE  Name = 'ClientFeedback_ListView'

Update _tNavigation
SET    ID_Parent = '6D6F6247-DE8B-44CC-94E4-10C16EF776F2'
WHERE  Name = 'Announcement_ListView'

GO

-- Add Client Appointment Request Only User Role
if(SELECT Count(*)
   FROM   tUserRole
   where  Name = 'Client Appointment Request Only') = 0
  BEGIN
      DECLARE @ClientAppointmentRequestOnly_ID_UserRole int = 0
      DEClare @ClientAppointmentRequest_ID_Model VARCHAR(MAX) = ''

      SELECT @ClientAppointmentRequest_ID_Model = Oid
      FROm   _tModel
      WHERE  TableName = 'tClientAppointmentRequest'

      INSERT INTO tUserRole
                  (Code,
                   Description,
                   ID_CreatedBy,
                   Comment,
                   IsActive,
                   ID_LastModifiedBy,
                   Name)
      VALUES      (NULL,
                   NULL,
                   10,
                   NULL,
                   1,
                   10,
                   'Client Appointment Request Only');

      SET @ClientAppointmentRequestOnly_ID_UserRole = @@IDENTITY

      INSERT INTO tUserRole_Detail
                  (IsEdit,
                   IsCreate,
                   IsDelete,
                   ID_Model,
                   IsView,
                   ID_UserRole)
      VALUES      (1,
                   1,
                   1,
                   @ClientAppointmentRequest_ID_Model,
                   1,
                   @ClientAppointmentRequestOnly_ID_UserRole);
  END

GO

-- Add Announcement Only User Role
if(SELECT Count(*)
   FROM   tUserRole
   where  Name = 'Announcement Only') = 0
  BEGIN
      DECLARE @AnnouncementOnly_ID_UserRole int = 0
      DEClare @Announcement_ID_Model VARCHAR(MAX) = ''

      SELECT @Announcement_ID_Model = Oid
      FROm   _tModel
      WHERE  TableName = 'tAnnouncement'

      INSERT INTO tUserRole
                  (Code,
                   Description,
                   ID_CreatedBy,
                   Comment,
                   IsActive,
                   ID_LastModifiedBy,
                   Name)
      VALUES      (NULL,
                   NULL,
                   10,
                   NULL,
                   1,
                   10,
                   'Announcement Only');

      SET @AnnouncementOnly_ID_UserRole = @@IDENTITY

      INSERT INTO tUserRole_Detail
                  (IsEdit,
                   IsCreate,
                   IsDelete,
                   ID_Model,
                   IsView,
                   ID_UserRole)
      VALUES      (1,
                   1,
                   1,
                   @Announcement_ID_Model,
                   1,
                   @AnnouncementOnly_ID_UserRole);
  END

GO

-- Add Client Feedback Only User Role
if(SELECT Count(*)
   FROM   tUserRole
   where  Name = 'Client Feedback Only') = 0
  BEGIN
      DECLARE @ClientFeedbackOnly_ID_UserRole int = 0
      DEClare @ClientFeedback_ID_Model VARCHAR(MAX) = ''

      SELECT @ClientFeedback_ID_Model = Oid
      FROm   _tModel
      WHERE  TableName = 'tClientFeedback'

      INSERT INTO tUserRole
                  (Code,
                   Description,
                   ID_CreatedBy,
                   Comment,
                   IsActive,
                   ID_LastModifiedBy,
                   Name)
      VALUES      (NULL,
                   NULL,
                   10,
                   NULL,
                   1,
                   10,
                   'Client Feedback Only');

      SET @ClientFeedbackOnly_ID_UserRole = @@IDENTITY

      INSERT INTO tUserRole_Detail
                  (IsEdit,
                   IsCreate,
                   IsDelete,
                   ID_Model,
                   IsView,
                   ID_UserRole)
      VALUES      (1,
                   1,
                   1,
                   @ClientFeedback_ID_Model,
                   1,
                   @ClientFeedbackOnly_ID_UserRole);
  END

GO

-- User Role
Declare @AllAdmission_ID_UserRole INT = 29
Declare @Administration_ID_UserRole INT = 12
DEClare @ClientAppointmentRequest_ID_Model VARCHAR(MAX) = ''
DEClare @Announcement_ID_Model VARCHAR(MAX) = ''
DEClare @ClientFeedback_ID_Model VARCHAR(MAX) = ''

SELECT @ClientAppointmentRequest_ID_Model = Oid
FROm   _tModel
WHERE  TableName = 'tClientAppointmentRequest'

SELECT @Announcement_ID_Model = Oid
FROm   _tModel
WHERE  TableName = 'tAnnouncement'

SELECT @ClientFeedback_ID_Model = Oid
FROm   _tModel
WHERE  TableName = 'tClientFeedback'

-- Add Client Appointment Request, Feedback and Announcement on All Adminission Role
IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @ClientAppointmentRequest_ID_Model
          AND ID_UserRole = @AllAdmission_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@ClientAppointmentRequest_ID_Model,
                   @AllAdmission_ID_UserRole)
  END

IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @Announcement_ID_Model
          AND ID_UserRole = @AllAdmission_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@Announcement_ID_Model,
                   @AllAdmission_ID_UserRole)
  END

IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @ClientFeedback_ID_Model
          AND ID_UserRole = @AllAdmission_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@ClientFeedback_ID_Model,
                   @AllAdmission_ID_UserRole)
  END

-- Add Client Appointment Request, Feedback and Announcement on Administration Role
IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @ClientAppointmentRequest_ID_Model
          AND ID_UserRole = @Administration_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@ClientAppointmentRequest_ID_Model,
                   @Administration_ID_UserRole)
  END

IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @Announcement_ID_Model
          AND ID_UserRole = @Administration_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@Announcement_ID_Model,
                   @Administration_ID_UserRole)
  END

IF(SELECT COUNT(*)
   FROM   tUserRole_Detail
   WHERE  ID_Model = @ClientFeedback_ID_Model
          AND ID_UserRole = @Administration_ID_UserRole) = 0
  BEGIN
      INSERT INTO tUserRole_Detail
                  (ID_Model,
                   ID_UserRole)
      VALUES      (@ClientFeedback_ID_Model,
                   @Administration_ID_UserRole)
  END

GO 
