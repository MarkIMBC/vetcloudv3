GO

ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         IsNull(patientSOAP.Name_SOAPType, '')
         + ' - '
         + IsNull(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + '0' + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellSched.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull('', '') + ' - '
                  + IsNull(wellSched.Comment, '')                                      Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

Alter VIEW [dbo].[vItemInventoriableForBillingLookUp]
AS
  SELECT ID,
         Name,
         ISNULL(UnitPrice, 0)                                  UnitPrice,
         ISNULL(UnitCost, 0)                                   UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company,
         CASE
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
           ELSE ''
         END                                                   RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays,
         OtherInfo_DateExpiration,
         ID_ItemType
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO

ALTER VIEW [dbo].[vPatient_Wellness_Detail_Listview]
AS
  select welldetail.ID,
         welldetail.ID_Patient_Wellness,
         welldetail.Name_Item,
         wellHed.Date,
         welldetail.Comment,
         ID_Patient_SOAP,
         wellHed.ID_FilingStatus,
         wellHed.ID_Company,
         wellHed.ID_Client,
         wellHed.ID_Patient
  FROM   vPatient_Wellness wellHed
         INNER JOIN vPatient_Wellness_Detail welldetail
                 on wellHed.ID = welldetail.ID_Patient_Wellness
  WHERE  wellHed.ID_FilingStatus NOT IN ( 4 )

GO

ALTER VIEW [dbo].[vPaymentTransaction]
AS
  SELECT H.*,
         CONVERT(VARCHAR(100), H.Date, 101)            DateString,
         UC.Name                                       AS CreatedBy,
         UM.Name                                       AS LastModifiedBy,
         approvedUser.Name                             AS ApprovedBy_Name_User,
         cancelUser.Name                               AS CanceledBy_Name_User,
         fs.Name                                       Name_FilingStatus,
         ISNULL(biHed.Name_Client, WalkInCustomerName) Name_Client,
         biHed.Name_Patient,
         taxScheme.Name                                Name_TaxScheme,
         biHed.Code                                    Code_BillingInvoice,
         biHed.GrossAmount                             GrossAmount_BillingInvoice,
         biHed.VatAmount                               VatAmount_BillingInvoice,
         biHed.NetAmount                               NetAmount_BillingInvoice,
         biHed.RemainingAmount                         RemainingAmount_BillingInvoice,
         pMth.Name                                     Name_PaymentMethod,
         cardType.Name                                 Name_CardType,
         CONVERT(VARCHAR, H.DateCreated, 0)            DateCreatedString,
         CONVERT(VARCHAR, H.DateModified, 0)           DateModifiedString,
         CONVERT(VARCHAR, H.DateApproved, 0)           DateApprovedString,
         CONVERT(VARCHAR, H.DateCanceled, 0)           DateCanceledString,
         biHed.PatientNames
  FROM   tPaymentTransaction H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.vBillingInvoice biHed
                ON biHed.ID = H.ID_BillingInvoice
         LEFT JOIN dbo.tPaymentMethod pMth
                ON pMth.ID = H.ID_PaymentMethod
         LEFT JOIN dbo.tTaxScheme taxScheme
                ON taxScheme.ID = biHed.ID_TaxScheme
         LEFT JOIN dbo.tCardType cardType
                ON cardType.ID = H.ID_CardType

GO 
