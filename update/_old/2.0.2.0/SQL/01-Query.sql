ALTER VIEW [dbo].[vItemInventoriable_ListvIew]
AS

SELECT
  item.ID
 ,item.Name
 ,CASE
    WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
    ELSE ''
  END RemainingBeforeExpired
 ,DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays
 ,item.OtherInfo_DateExpiration DateExpired
 ,item.ID_Company
 ,item.CurrentInventoryCount
 ,item.ID_InventoryStatus
 ,invtStatus.Name Name_InventoryStatus
 ,item.IsActive
FROM dbo.titem item
LEFT JOIN tInventoryStatus invtStatus 
	on item.ID_InventoryStatus = invtStatus.ID
WHERE 
	item.ID_ItemType = 2

GO

ALTER VIEW [dbo].[vPatient]
AS
SELECT
  H.*
 ,UC.Name AS CreatedBy
 ,UM.Name AS LastModifiedBy
 ,H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName
 ,gender.Name Name_Gender
 ,country.Name PhoneCode_Country
 ,client.Name Name_Client
 ,client.IsActive IsActive_Client
FROM tPatient H
LEFT JOIN tUser UC
  ON H.ID_CreatedBy = UC.ID
LEFT JOIN tUser UM
  ON H.ID_LastModifiedBy = UM.ID
LEFT JOIN dbo.tGender gender
  ON H.ID_Gender = gender.ID
LEFT JOIN dbo.tCountry country
  ON H.ID_Country = country.ID
LEFT JOIN dbo.tClient client
  ON client.ID = H.ID_Client;

GO

IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pSetActiveInactiveItem'))

  DROP PROC dbo.pSetActiveInactiveItem

GO

IF EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'typActiveInactiveItem'))
  BEGIN

	DROP TYPE [dbo].[typActiveInactiveItem]
  END 
GO

CREATE TYPE [dbo].[typActiveInactiveItem] AS TABLE(
	ID_Item INT,
	IsActive BIT
)
GO

CREATE PROC pSetActiveInactiveItem
(
	@record typActiveInactiveItem READONLY, 
	@ID_UserSession INT
) AS
BEGIN

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
	
	DECLARE @ID_User INT = 0;
	DECLARE @ID_Company INT = 0;

	SELECT @ID_User = userSession.ID_User,
		   @ID_Company = _user.ID_Company
	FROM dbo.tUserSession userSession 
	INNER JOIN vUser _user 
		on userSession.ID_User = _user.ID
	WHERE userSession.ID = @ID_UserSession;

	UPDATE tItem SET IsActive = record.IsActive
	FROM tItem item 
	INNER JOIN @record record 
		ON item.ID = record.ID_Item
	WHERE item.ID_Company = @ID_Company

    SELECT '_';

    SELECT @Success Success,
           @message message;
END

GO

IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pSetActiveInactivePatient'))

  DROP PROC dbo.pSetActiveInactivePatient

GO

IF EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'typActiveInactivePatient'))
  BEGIN

	DROP TYPE [dbo].[typActiveInactivePatient]
  END 
GO

CREATE TYPE [dbo].[typActiveInactivePatient] AS TABLE(
	ID_Patient INT,
	IsActive BIT
)
GO

CREATE PROC pSetActiveInactivePatient
(
	@record typActiveInactivePatient READONLY, 
	@ID_UserSession INT
) AS
BEGIN

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
	
	DECLARE @ID_User INT = 0;
	DECLARE @ID_Company INT = 0;

	SELECT @ID_User = userSession.ID_User,
		   @ID_Company = _user.ID_Company
	FROM dbo.tUserSession userSession 
	INNER JOIN vUser _user 
		on userSession.ID_User = _user.ID
	WHERE userSession.ID = @ID_UserSession;

	UPDATE tPatient SET IsActive = record.IsActive
	FROM tPatient patient 
	INNER JOIN @record record 
		ON patient.ID = record.ID_Patient
	WHERE patient.ID_Company = @ID_Company

    SELECT '_';

    SELECT @Success Success,
           @message message;
END
GO

IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pSetActiveInactiveClient'))

  DROP PROC dbo.pSetActiveInactiveClient

GO

IF EXISTS (SELECT *
           FROM   [sys].[table_types]
           WHERE  user_type_id = Type_id(N'typActiveInactiveClient'))
  BEGIN

	DROP TYPE [dbo].[typActiveInactiveClient]
  END 
GO

CREATE TYPE [dbo].[typActiveInactiveClient] AS TABLE(
	ID_Client INT,
	IsActive BIT
)
GO

CREATE PROC pSetActiveInactiveClient
(
	@record typActiveInactiveClient READONLY, 
	@ID_UserSession INT
) AS
BEGIN

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
	
	DECLARE @ID_User INT = 0;
	DECLARE @ID_Company INT = 0;

	SELECT @ID_User = userSession.ID_User,
		   @ID_Company = _user.ID_Company
	FROM dbo.tUserSession userSession 
	INNER JOIN vUser _user 
		on userSession.ID_User = _user.ID
	WHERE userSession.ID = @ID_UserSession;

	UPDATE tClient SET IsActive = record.IsActive
	FROM tClient client 
	INNER JOIN @record record 
		ON client.ID = record.ID_Client
	WHERE client.ID_Company = @ID_Company

    SELECT '_';

    SELECT @Success Success,
           @message message;
END
GO