exec _pAddModelProperty
  'tBillingInvoice',
  'ID_Patient_Confinement',
  2

GO

exec _pAddModelProperty
  'tBillingInvoice_Detail',
  'ID_Patient_Confinement_ItemsServices',
  2

GO

exec _pRefreshAllViews

GO

ALTER VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         H.DateLastVisited,
         H.CurrentCreditAmount
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
GO

ALTER VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                                 AS CreatedBy,
         UM.Name                                 AS LastModifiedBy,
         UC.Name                                 AS Name_CreatedBy,
         UM.Name                                 AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101)      DateString,
         soapType.Name                           Name_SOAPType,
         patient.Name                            Name_Patient,
         patient.Name_Client                     Name_Client,
         approvedUser.Name                       Name_ApprovedBy,
         canceledUser.Name                       Name_CanceledBy,
         fs.Name                                 Name_FilingStatus,
         patient.ID_Client                       ID_Client,
         attendingPhysicianEmloyee.Name          AttendingPhysician_Name_Employee,
         confinement.Code                        Code_Patient_Confinement,
         confinement.Date                        Date_Patient_Confinement,
         confinement.DateDischarge               DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus             ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus           Name_FilingStatus_Patient_Confinement,
         REPLACE(H.Diagnosis, CHAR(13), '<br/>') DiagnosisHTML
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement

GO

ALTER VIEW vPatient_Confinement
AS
  SELECT H.*,
         UC.Name                        AS CreatedBy,
         UM.Name                        AS LastModifiedBy,
         client.Name                    Name_Client,
         patient.Name                   Name_Patient,
         fs.Name                        Name_FilingStatus,
         ISNULL(fsBilling.Name, '----') BillingInvoice_Name_FilingStatus
  FROM   tPatient_Confinement H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tFilingStatus fsBilling
                ON fsBilling.ID = H.BillingInvoice_ID_FilingStatus

go

Go

ALTER VIEW vPatient_Confinement_Listview
AS
  SELECT ID,
         Name_Client,
         Name_Patient,
         Code,
         Date,
         REPLACE(dbo.fGetAge(h.Date, case
                                       WHEN ISNULL(h.ID_FilingStatus, 0) = 15 then h.DateDischarge
                                       ELSE
                                         case
                                           WHEN ISNULL(h.ID_FilingStatus, 0) = 4 then h.DateCanceled
                                           ELSE GETDATE()
                                         END
                                     END, '1 day'), ' old', '') ConfinementDays,
         ID_Company,
         ID_FilingStatus,
         DateDischarge,
         Name_FilingStatus,
         BillingInvoice_Name_FilingStatus
  FROM   vPatient_Confinement h 
GO

ALTER VIEW [dbo].[vBillingInvoice_Detail]
AS
  SELECT detail.*,
         item.Name Name_Item
  FROM   dbo.tBillingInvoice_Detail detail
         LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item;

GO

Alter PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail;

      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                        Name_FilingStatus,
                   client.Name                    Name_Client,
                   patient.Name                   Name_Patient,
                   client.Address                 BillingAddress,
                   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GETDATE()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      if ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999 ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                           UnitPrice,
                   item.OtherInfo_DateExpiration                       DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP;
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                        UnitPrice,
                   ISNULL(item.UnitCost, 0)                         UnitCost,
                   item.OtherInfo_DateExpiration                    DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement;
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Detail
            WHERE  ID_BillingInvoice = @ID;
        END
  END;

GO

CREATE OR
ALTER PROC [dbo].[pBillingInvoice_Validation] (@ID_BillingInvoice      INT,
                                               @ID_Patient_Confinement INT,
                                               @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConfinementCount INT = 0

      BEGIN TRY
          DECLARE @Code_Patient_Confinement VARCHAR(MAX) = ''
          DECLARE @Code_BillingInvoice VARCHAR(MAX) = ''
          DECLARE @BillingInvoices TABLE
            (
               Code              VARCHAR(MAX),
               Name_FilingStatus VARCHAR(MAX)
            )

          SELECT @ID_Patient_Confinement = confinement.ID,
                 @Code_Patient_Confinement = confinement.Code
          FROM   tPatient_Confinement confinement
          WHERE  confinement.ID = @ID_Patient_Confinement

          INSERT @BillingInvoices
          SELECT Code,
                 Name_FilingStatus
          FROM   vBillingInvoice
          WHERE  ID NOT IN ( @ID_BillingInvoice )
                 AND ID_Patient_Confinement = @ID_Patient_Confinement
                 AND ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )
          ORder  BY Date ASC

          IF (SELECT COUNT(*)
              FROM   @BillingInvoices) > 0
            BEGIN
                SET @message = 'Billing Invoice for Confinement '
                               + @Code_Patient_Confinement
                               + ' is already created.' + CHAR(13) + CHAR(10);

                SELECT @message = @message + Code + ' - ' + Name_FilingStatus
                                  + CHAR(13) + CHAR(10)
                FROM   @BillingInvoices;

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO

ALTER PROC pGetPatient_Confinement @ID         INT = -1,
                                   @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Confinement_ItemsServices

      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Employee INT
      DECLARE @ID_Company INT
      DECLARE @Confinement_ID_FilingStatus INT = 14

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name Name_FilingStatus
            FROM   (SELECT NULL                         AS [_],
                           -1                           AS [ID],
                           GETDATE()                    AS [Date],
                           '-- NEW --'                  AS [Code],
                           NULL                         AS [Name],
                           1                            AS [IsActive],
                           @ID_Company                  AS [ID_Company],
                           NULL                         AS [ID_Client],
                           NULL                         AS [ID_Patient],
                           NULL                         AS [Comment],
                           GETDATE()                    AS [DateCreated],
                           GETDATE()                    AS [DateModified],
                           NULL                         AS [ID_CreatedBy],
                           NULL                         AS [ID_LastModifiedBy],
                           @Confinement_ID_FilingStatus AS ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            DECLARE @BillingInvoice Table
              (
                 ID_BillingInvoice          INT,
                 ID_Patient_Confinement     INT,
                 Code_BillingInvoice        VARCHAR(MAX),
                 Status_BillingInvoice      VARCHAR(MAX),
                 TotalAmount_BillingInvoice DECIMAL(18, 2)
              )

            INSERT @BillingInvoice
            SELECT TOP 1 biHed.ID                     ID_BillingInvoice,
                         biHed.Code                   Code_BillingInvoice,
                         biHed.ID_Patient_Confinement ID_Patient_Confinement,
                         biHed.Status                 Status_BillingInvoice,
                         biHed.TotalAmount            TotalAmount_BillingInvoice
            FROM   vBillingInvoice biHed
            WHERE  biHed.ID_Patient_Confinement = @ID
                   AND biHed.ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )

            SELECT H.*,
                   biHed.*
            FROM   vPatient_Confinement H
                   LEFT JOIN @BillingInvoice biHed
                          ON H.ID = biHed.ID_Patient_Confinement
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Confinement_ItemsServices
      WHERE  ID_Patient_Confinement = @ID
  END 


  GO

  GO

CREATE OR
ALTER PROC pUpdatePatient_Confinemen_BillingStatus (@IDs_Patient_Confinement typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @PatientConfinement TABLE
        (
           ID_Patient_Confinement         INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @PatientConfinement
      SELECT ID,
             NULL
      FROM   @IDs_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = tbl.ID_FilingStatus
      FROM   @PatientConfinement pConfine
             INNER JOIN (SELECT itemservices.ID_Patient_Confinement,
                                CASE
                                  WHEN Count(*) > 0 THEN @ForBilling_ID_FilingStatus
                                  ELSE NULL
                                END ID_FilingStatus
                         FROM   tPatient_Confinement_ItemsServices itemservices
                                INNER JOIN tPatient_Confinement confineRec
                                        ON confineRec.ID = itemservices.ID_Patient_Confinement
                                INNER JOIN @IDs_Patient_Confinement ids
                                        ON ids.ID = itemservices.ID_Patient_Confinement
                         WHERE  confineRec.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )
                         GROUP  BY itemservices.ID_Patient_Confinement) tbl
                     on tbl.ID_Patient_Confinement = pConfine.ID_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = fs.ID
      FROM   @PatientConfinement ids
             INNER JOIN vBillingInvoice biHed
                     on ids.ID_Patient_Confinement = biHed.ID_Patient_Confinement
             LEFT JOIN tFilingStatus fs
                    on fs.Name = biHed.Status
      WHERE  biHed.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )

      Update tPatient_Confinement
      SET    BillingInvoice_ID_FilingStatus = ids.BillingInvoice_ID_FilingStatus
      FROM   tPatient_Confinement hed
             inner join @PatientConfinement ids
                     ON ids.ID_Patient_Confinement = hed.ID
  END

GO

ALTER PROC dbo.pModel_AfterSaved_BillingInvoice (@ID_CurrentObject VARCHAR(10),
                                                 @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tBillingInvoice
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'BillingInvoice';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tBillingInvoice
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement
  END;

GO

ALTER PROC dbo.pModel_AfterSaved_Patient_Confinement (@ID_CurrentObject VARCHAR(10),
                                                      @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      SELECT @ID_Patient = ID_Patient
      FROM   tPatient_Confinement
      WHERE  ID = @ID_CurrentObject

      UPDATE tPatient_SOAP
      SET    ID_Patient = @ID_Patient
      WHERE  ID_Patient_Confinement = @ID_CurrentObject

      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      VALUES (@ID_CurrentObject)

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement
  END;

GO

ALTER PROC [dbo].[pDischargePatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                             @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotConfined TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotConfined INT = 0;

      /* Validate Patient_Confinement Status is not Confined*/
      INSERT @ValidateNotConfined
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Confined_ID_FilingStatus );

      SELECT @Count_ValidateNotConfined = COUNT(*)
      FROM   @ValidateNotConfined;

      IF ( @Count_ValidateNotConfined > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotConfined > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to discharged:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotConfined;

            THROW 50001, @message, 1;
        END;

      /* Validate SOAP Status is not yet done.*/
      DECLARE @ConfinementSOAP TABLE
        (
           Code                           VARCHAR(MAX),
           Code_Patient_SOAP              VARCHAR(MAX),
           Name_FilingStatus_Patient_SOAP VARCHAR(MAX)
        )

      INSERT @ConfinementSOAP
      SELECT confinement.Code,
             soap.Code,
             soap.Name_FilingStatus
      FROM   vPatient_Confinement confinement
             INNER JOIN @IDs_Patient_Confinement ids
                     ON confinement.ID = ids.ID
             INNER JOIN vPatient_SOAP soap
                     ON confinement.ID = soap.ID_Patient_Confinement
      WHERE  soap.ID_FilingStatus = @Filed_ID_FilingStatus

      if(SELECT COUNT(*)
         FROM   @ConfinementSOAP) > 0
        BEGIN
            SET @message = 'The following record not allowed to discharged:';

            SELECT @message = @message + CHAR(13) + CHAR(10)
                              + Code_Patient_SOAP + ' (' +
                              + Name_FilingStatus_Patient_SOAP + ') '
            FROM   @ConfinementSOAP;

            THROW 50001, @message, 1;
        END
  END; 
GO

Create OR
ALTER PROC dbo.pDischargePatient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                              @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          exec [pDischargePatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Discharged_ID_FilingStatus,
                 DateDischarge = GETDATE(),
                 ID_DischargeBy = @ID_User
          FROM   dbo.tPatient_Confinement bi
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          exec pUpdatePatient_Confinemen_BillingStatus
            @IDs_Patient_Confinement
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pUpdatePaymentTransaction_RemainingAmount_By_BI] (@ID_BillingInvoice INT)
AS
  BEGIN
      Declare @Approved_ID_FilingStatus INT = 3
      Declare @PayableAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @PaymentHistory TABLE
        (
           RowID                 int identity(1, 1) primary key,
           ID_PaymentTransaction Int,
           Date                  DateTime,
           Code                  Varchar(20),
           Name_PaymentMethod    Varchar(200),
           PayableAmount         DECIMAL(18, 4),
           PaymentAmount         DECIMAL(18, 4),
           RemainingBalance      DECIMAL(18, 4)
        )

      SELECT @PayableAmount_BillingInvoice = bi.NetAmount
      FROM   tBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      INSERT @PaymentHistory
             (ID_PaymentTransaction,
              Date,
              Code,
              Name_PaymentMethod,
              PaymentAmount,
              RemainingBalance)
      SELECT pt.ID,
             pt.Date,
             pt.Code,
             pt.Name_PaymentMethod,
             pt.PaymentAmount,
             0
      FROM   dbo.vPaymentTransaction pt
      WHERE  ID_BillingInvoice = @ID_BillingInvoice
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus )

      DECLARE @maxID   INT,
              @counter INT

      SET @counter = 1

      SELECT @maxID = COUNT(*)
      FROM   @PaymentHistory

      WHILE ( @counter <= @maxID )
        BEGIN
            Declare @PaymentAmount DECIMAL(18, 4) = 0
            Declare @PayableAmount DECIMAL(18, 4) = 0
            Declare @ChangeAmount DECIMAL(18, 4) = 0

            SELECT @PaymentAmount = PaymentAmount
            FROM   @PaymentHistory
            WHERE  RowID = @counter

            SET @PayableAmount = @PayableAmount_BillingInvoice
            SET @PayableAmount_BillingInvoice = @PayableAmount_BillingInvoice - @PaymentAmount

            IF @PayableAmount < @PaymentAmount
              BEGIN
                  SET @PayableAmount_BillingInvoice = 0
              END

            Update @PaymentHistory
            SET    PayableAmount = @PayableAmount,
                   RemainingBalance = @PayableAmount_BillingInvoice
            WHERE  RowID = @counter

            SET @counter = @counter + 1
        END

      UPDATE tPaymentTransaction
      SET    RemainingAmount = payHistory.RemainingBalance
      FROM   tPaymentTransaction pt
             INNER JOIN @PaymentHistory payHistory
                     on pt.ID = payHistory.ID_PaymentTransaction

      UPDATE dbo.tBillingInvoice
      SET    RemainingAmount = @PayableAmount_BillingInvoice
      WHERE  ID = @ID_BillingInvoice

      UPDATE dbo.tBillingInvoice
      SET    Payment_ID_FilingStatus = dbo.fGetPaymentStatus(NetAmount, RemainingAmount)
      WHERE  ID = @ID_BillingInvoice

      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_BillingInvoice

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement
  END

GO


CREATE OR
ALTER PROC pAdjustClientCredits(@ClientCredits  typClientCredit READONLY,
                                @ID_UserSession INT)
AS
  BEGIN
      INSERT INTO [dbo].[tClient_CreditLogs]
                  ([ID_Company],
                   ID_Client,
                   [Date],
                   [CreditAmount],
                   [Code],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [IsActive])
      SELECT client.ID_Company,
             clientCredit.ID_Client,
             clientCredit.Date,
             clientCredit.CreditAmount,
             clientCredit.Code,
             clientCredit.Comment,
             GETDATE(),
             GETDATE(),
             1,
             1,
             1
      FROm   @ClientCredits clientCredit
             LEFT JOIN tClient client
                    ON clientCredit.ID_Client = client.ID

      exec pUpdateClientCredits
  END

GO

CREATE OR
ALTER PROC pUpdateClientCredits
AS
  BEGIN
      UPDATE tClient
      SET    CurrentCreditAmount = ISNULL(tbl.TotalCreditAmount, 0)
      FROm   tClient client
             LEFT JOIN (SELECT ID_Client,
                               SUM(CreditAmount) TotalCreditAmount
                        FROm   tClient_CreditLogs
                        GROUP  BY ID_Client) tbl
                    ON client.ID = tbl.ID_Client
  END

GO

CREATE OR
ALTER PROC [dbo].pAdjustClientCredits_validation (@ClientCredits  typClientCredit READONLY,
                                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConfinementCount INT = 0
      DECLARE @_ClientCredits typClientCredit

      INSERT @_ClientCredits
      SELECT *
      FROm   @ClientCredits

      DECLARE @ClientCurrentCredit TABLE
        (
           ClientName          VARCHAR(MAX),
           CreditAmount        Decimal(18, 4),
           CurrentCreditAmount Decimal(18, 4)
        )

      INSERT @ClientCurrentCredit
      SELECT ClientName,
             TotalWitdrawalAmount,
             CurrentCreditAmount
      FROM   (SELECT clientcredit.ID_Client     ID_Client,
                     client.Name                ClientName,
                     SUM(CreditAmount)          TotalWitdrawalAmount,
                     ISNULL(client.CurrentCreditAmount, 0) CurrentCreditAmount
              FROM   @_ClientCredits clientcredit
                     inner join tClient client
                             on client.ID = clientcredit.ID_Client
              WHERE  clientcredit.CreditAmount < 0
              GROUP  BY clientcredit.ID_Client,
                        client.Name,
                        client.CurrentCreditAmount) tbl
      WHERE  CurrentCreditAmount + TotalWitdrawalAmount < 0

      IF (SELECT COUNT(*)
          FROM   @ClientCurrentCredit) > 0
        BEGIN
            SET @message = 'The ff. clients are not able to consume credit:' +
                           + CHAR(13) + CHAR(10)

            SELECT @message = @message + ClientName + ' Rem: '
                              + FORMAT(CurrentCreditAmount, '#,#0.00')
                              + CHAR(13) + CHAR(10)
            FROM   @ClientCurrentCredit;

            THROW 50001, @message, 1;
        END;

  END

GO 

CREATE OR
ALTER PROC pDoAdjustClientCredits(@ClientCredits  typClientCredit READONLY,
                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      BEGIN TRY
          EXEC pAdjustClientCredits_validation
            @ClientCredits,
            @ID_UserSession

          exec pAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;

      END CATCH
	  
      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO

CREATE OR
ALTER PROC pValidate_AdjustClientCredits(@ClientCredits  typClientCredit READONLY,
                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      BEGIN TRY
          EXEC pAdjustClientCredits_validation
            @ClientCredits,
            @ID_UserSession 
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;

      END CATCH
	  
      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO

ALTER PROC dbo.pApprovePaymentTransaction (@IDs_PaymentTransaction typIntList READONLY,
                                           @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Credits_ID_PaymentMethod INT = 5;
      DECLARE @IDs_BillingInvoice typIntList;
      DECLARE @ClientCredits typClientCredit
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          INSERT @IDs_BillingInvoice
                 (ID)
          SELECT ptHed.ID_BillingInvoice
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID;

          INSERT @ClientCredits
          SELECT biHed.ID_Client,
                 ptHed.Date,
                 0 - ABS(ptHed.CreditAmount),
                 ptHed.Code,
                 'Consume Credit as payment'
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID
                 LEFT JOIN tBillingInvoice biHed
                        on ptHed.ID_BillingInvoice = biHed.ID
          WHERE  ptHed.ID_PaymentMethod = @Credits_ID_PaymentMethod

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApprovePaymentTransaction_validation
            @IDs_PaymentTransaction,
            @ID_UserSession;

          IF(SELECT COUNT(*)
             FROM   @ClientCredits) > 0
            BEGIN
                EXEC pAdjustClientCredits_validation
                  @ClientCredits,
                  @ID_UserSession
            END

          UPDATE dbo.tPaymentTransaction
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tPaymentTransaction bi
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON bi.ID = ids.ID;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          EXEC pAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC dbo.pCancelPaymentTransaction (@IDs_PaymentTransaction typIntList READONLY,
                                          @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Credits_ID_PaymentMethod INT = 5;
      DECLARE @IDs_BillingInvoice typIntList;
      DECLARE @ClientCredits typClientCredit
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          INSERT @IDs_BillingInvoice
                 (ID)
          SELECT ptHed.ID_BillingInvoice
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID;

          INSERT @ClientCredits
          SELECT biHed.ID_Client,
                 ptHed.Date,
                 ABS(ptHed.CreditAmount),
                 ptHed.Code,
                 'Re-deposit Credit as payment cancel.'
          FROM   dbo.tPaymentTransaction ptHed
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON ids.ID = ptHed.ID
                 LEFT JOIN tBillingInvoice biHed
                        on ptHed.ID_BillingInvoice = biHed.ID
          WHERE  ptHed.ID_PaymentMethod = @Credits_ID_PaymentMethod

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelPaymentTransaction_validation
            @IDs_PaymentTransaction,
            @ID_UserSession;

          UPDATE dbo.tPaymentTransaction
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPaymentTransaction bi
                 INNER JOIN @IDs_PaymentTransaction ids
                         ON bi.ID = ids.ID;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          IF(SELECT COUNT(*)
             FROM   @ClientCredits) > 0
            BEGIN
                EXEC pAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
GO

GO

CREATE OR
ALTER PROC pTransferConfinementItemsServicesFromSOAP(@IDs_Patient_SOAP typINtList READONLY)
AS
  BEGIN
      DECLARE @IDs_Patient_Confinement typINtList

      INSERT @IDs_Patient_Confinement
      SELECT soap.ID_Patient_Confinement
      FRom   tPatient_SOAP_Prescription soapPrescription
             INNER JOIN tPatient_SOAP soap
                     on soap.ID = soapPrescription.ID_Patient_SOAP
             INNER JOIN @IDs_Patient_SOAP ids_SOAP
                     on soapPrescription.ID_Patient_SOAP = ids_SOAP.ID
      WHERE  ISNULL(soap.ID_Patient_Confinement, 0) <> 0

      INSERT INTO [dbo].[tPatient_Confinement_ItemsServices]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Confinement],
                   [ID_Patient_SOAP],
                   [ID_Item],
                   [Quantity],
                   [Date],
                   [DateExpiration],
                   [UnitPrice],
                   [UnitCost],
                   [Amount])
      select '',
             '',
             1,
             item.ID_Company,
             soapPrescription.Comment,
             GETDATE(),
             GETDATE(),
             soap.ID_CreatedBy,
             soap.ID_CreatedBy,
             soap.ID_Patient_Confinement,
             soap.ID,
             soapPrescription.ID_Item,
             soapPrescription.Quantity,
             soap.Date,
             item.OtherInfo_DateExpiration,
             item.UnitPrice,
             item.UnitCost,
             0
      FRom   tPatient_SOAP_Prescription soapPrescription
             INNER JOIN tPatient_SOAP soap
                     on soap.ID = soapPrescription.ID_Patient_SOAP
             INNER JOIN @IDs_Patient_SOAP ids_SOAP
                     on soapPrescription.ID_Patient_SOAP = ids_SOAP.ID
             LEFT JOIN tItem item
                    on soapPrescription.ID_Item = item.ID
      WHERE  ISNULL(soap.ID_Patient_Confinement, 0) <> 0

      UPDATE [tPatient_Confinement_ItemsServices]
      SET    Amount = ( ( Convert(Decimal(18, 4), ISNULL(Quantity, 0)) ) * ( ISNULL(UnitPrice, 0) ) )
      FROM   [tPatient_Confinement_ItemsServices] itemServices
             INNER JOIN @IDs_Patient_Confinement IDsConfinement
                     ON IDsConfinement.ID = itemServices.ID_Patient_Confinement

      UPDATE tPatient_Confinement
      SET    SubTotal = tbl.TotalAmount,
             TotalAmount = tbl.TotalAmount
      FROM   tPatient_Confinement confinement
             INNER JOIn @IDs_Patient_Confinement ids_Confinement
                     ON confinement.ID = ids_Confinement.ID
             INNER JOIN (SELECT itemServices.ID_Patient_Confinement,
                                SUM(Amount) TotalAmount
                         FROM   [tPatient_Confinement_ItemsServices] itemServices
                         GROUP  BY itemServices.ID_Patient_Confinement) tbl
                     ON confinement.ID = tbl.ID_Patient_Confinement
  END

GO

CREATE OR
ALTER PROC [dbo].[pDonePatient_SOAP] (@IDs_Patient_SOAP typIntList READONLY,
                                      @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Doned_ID_FilingStatus INT = 13;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pDonePatient_SOAP_validation
            @IDs_Patient_SOAP,
            @ID_UserSession;

          INSERT @IDs_Patient
          SELECT patient.ID
          FROM   tPatient_SOAP soap
                 INNER JOIN tPatient patient
                         ON patient.ID = soap.ID_Patient
                 INNER JOIN @IDs_Patient_SOAP idsSOAP
                         ON idsSOAP.ID = soap.ID

          EXEC dbo.pUpdatePatientsLastVisitedDate
            @IDs_Patient

          UPDATE dbo.tPatient_SOAP
          SET    ID_FilingStatus = @Doned_ID_FilingStatus,
                 DateDone = GETDATE(),
                 ID_DoneBy = @ID_User
          FROM   dbo.tPatient_SOAP bi
                 INNER JOIN @IDs_Patient_SOAP ids
                         ON bi.ID = ids.ID;

          exec pTransferConfinementItemsServicesFromSOAP
            @IDs_Patient_SOAP
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 
