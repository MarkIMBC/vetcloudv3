/*
exec _pCreateAppModuleWithTable 'tPatient_Confinement_ItemsServices', 0, 0, NULL

exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'ID_Patient_Confinement', 2
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'ID_Item', 2
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'Quantity', 2
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'Date', 5
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'DateExpiration', 5
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'UnitCost', 3
exec _pAddModelProperty 'tPatient_Confinement_ItemsServices', 'UnitPrice', 3
exec _pAddModelDetail 'tPatient_Confinement_ItemsServices', 'ID_Patient_Confinement', 'tPatient_Confinement',  'tPatient_Confinement'

exec _pCreateAppModuleWithTable 'tCaseType', 0, 0, NULL
exec _pAddModelProperty 'tPatient_SOAP', 'CaseType', 1

exec _pRefreshAllViews

*/

IF (SELECT COUNT(*)
    FROM   INFORMATION_SCHEMA.COLUMNS
    WHERE  TABLE_SCHEMA = 'dbo'
           AND TABLE_NAME = 'tClient'
           AND COLUMN_NAME = 'DateLastVisited') = 0
  BEGIN
      exec _pAddModelProperty
        'tClient',
        'DateLastVisited',
        5
  END;

GO

IF (SELECT COUNT(*)
    FROM   INFORMATION_SCHEMA.COLUMNS
    WHERE  TABLE_SCHEMA = 'dbo'
           AND TABLE_NAME = 'tBillingInvoice_Detail'
           AND COLUMN_NAME = 'DiscountAmount') = 0
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_Detail',
        'DiscountAmount',
        3

      exec _pAddModelProperty
        'tBillingInvoice_Detail',
        'DiscountRate',
        3

      exec _pAddModelProperty
        'tBillingInvoice_Detail',
        'IsComputeDiscountRate',
        4

      exec _pRefreshAllViews
  END;

GO

ALTER VIEW [dbo].[vClient]
AS
  SELECT H.ID,
         H.Code,
         H.Name,
         H.IsActive,
         H.ID_Company,
         H.Comment,
         H.DateCreated,
         H.DateModified,
         H.ID_CreatedBy,
         H.ID_LastModifiedBy,
         H.ContactNumber,
         H.Email,
         H.Address,
         H.ContactNumber2,
         H.Old_client_id,
         H.tempID,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         H.DateLastVisited
  FROM   tClient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

ALTER VIEW vClient_ListView
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '') ContactNumbers,
         DateCreated,
         DateLastVisited,
         ISNULL(IsActive, 0)          IsActive
  FROM   dbo.vClient
  WHERE  ISNULL(IsActive, 0) = 1

GO

ALTER VIEW [dbo].[vBillingInvoice_Detail]
AS
  SELECT detail.ID,
         detail.Code,
         detail.Name,
         detail.IsActive,
         detail.Comment,
         detail.ID_BillingInvoice,
         detail.ID_Item,
         detail.Quantity,
         ISNULL(detail.UnitCost, 0) UnitCost,
         detail.UnitPrice,
         detail.Amount,
         item.Name                  Name_Item,
         detail.DiscountRate,
         detail.DiscountAmount,
         detail.IsComputeDiscountRate
  FROM   dbo.tBillingInvoice_Detail detail
         LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item;

GO

ALTER VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                            AS CreatedBy,
         UM.Name                            AS LastModifiedBy,
         UC.Name                            AS Name_CreatedBy,
         UM.Name                            AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101) DateString,
         soapType.Name                      Name_SOAPType,
         patient.Name                       Name_Patient,
         patient.Name_Client                Name_Client,
         approvedUser.Name                  Name_ApprovedBy,
         canceledUser.Name                  Name_CanceledBy,
         fs.Name                            Name_FilingStatus,
         patient.ID_Client                  ID_Client,
         attendingPhysicianEmloyee.Name     AttendingPhysician_Name_Employee,
         confinement.Code                   Code_Patient_Confinement,
         confinement.Date                   Date_Patient_Confinement,
         confinement.DateDischarge          DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus        ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus      Name_FilingStatus_Patient_Confinement
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement

GO

ALTER VIEW vPatient_Confinement_ItemsServices
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
		 item.Name AS Name_Item
  FROM   tPatient_Confinement_ItemsServices H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItem item
                ON H.ID_Item = item.ID 

GO


Alter VIEW vPayable
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         fs.Name Payment_Name_FilingStatus
  FROM   tPayable H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.Payment_ID_FilingStatus

GO

CREATE OR
ALTER VIEW vPayable_PayableDetail_Listview
as
  SELECT payable.ID,
         payable.Date,
         payable.ID_Company,
         expensesCat.Name   Name_ExpenseCategory,
         payableDetail.Name Name_Payable_Detail,
         payable.TotalAmount,
         payable.Payment_ID_FilingStatus,
         fs.Name            Payment_Name_FilingStatus,
         payable.RemaningAmount,
         payable.PaidAmount
  FROM   tPayable payable
         INNER JOIN tPayable_Detail payableDetail
                 ON payableDetail.ID_Payable = payable.ID
         LEFT JOIN tExpenseCategory expensesCat
                ON expensesCat.ID = payableDetail.ID_ExpenseCategory
         LEFT JOIN tFilingStatus fs
                ON fs.ID = payable.Payment_ID_FilingStatus

GO

CREATE OR
ALTER VIEW vPatient_Confinement_Listview
AS
  SELECT ID,
         Name_Client,
         Name_Patient,
         Code,
         Date,
         REPLACE(dbo.fGetAge(h.Date, case
                                       WHEN ISNULL(h.ID_FilingStatus, 0) = 15 then h.DateDischarge
                                       ELSE
                                         case
                                           WHEN ISNULL(h.ID_FilingStatus, 0) = 4 then h.DateCanceled
                                           ELSE GETDATE()
                                         END
                                     END, '1 day'), ' old', '') ConfinementDays,
         ID_Company,
         ID_FilingStatus,
         DateDischarge,
         Name_FilingStatus
  FROM   vPatient_Confinement h

GO

CREATE OR
ALTER VIEW vPatient_Confinement_Listview
AS
  SELECT ID,
         Name_Client,
         Name_Patient,
         Code,
         Date,
         REPLACE(dbo.fGetAge(h.Date, case
                                       WHEN ISNULL(h.ID_FilingStatus, 0) = 15 then h.DateDischarge
                                       ELSE GETDATE()
                                     END, '1 day'), ' old', '') ConfinementDays,
         ID_Company,
         ID_FilingStatus,
         DateDischarge                                          Name_FilingStatus
  FROM   vPatient_Confinement h

GO

ALTER PROCEDURE [dbo].[pGetPayable] @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' as Payable_Detail

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           GETDATE() AS Date,
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           2         AS Payment_ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPayable H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   tPayable_Detail
      WHERE  ID_Payable = @ID
  END

GO

ALTER PROC pGetPatient_Confinement @ID         INT = -1,  
                                   @ID_Session INT = NULL  
AS  
  BEGIN  
      SELECT '_', '' AS Patient_Confinement_ItemsServices 
  
      DECLARE @ID_User INT  
      DECLARE @ID_Warehouse INT  
      DECLARE @ID_Employee INT  
      DECLARE @ID_Company INT  
      DECLARE @Confinement_ID_FilingStatus INT = 14  
  
      SELECT @ID_User = ID_User,  
             @ID_Warehouse = ID_Warehouse  
      FROM   tUserSession  
      WHERE  ID = @ID_Session  
  
      SELECT @ID_Company = ID_Company  
      FROM   vUser  
      WHERE  ID = @ID_User  
  
      IF ( @ID = -1 )  
        BEGIN  
            SELECT H.*, fs.Name Name_FilingStatus  
            FROM   (SELECT NULL                         AS [_],  
                           -1                           AS [ID],  
                           GETDATE()                    AS [Date],  
                           '-- NEW --'                  AS [Code],  
                           NULL                         AS [Name],  
                           1                            AS [IsActive],  
                           @ID_Company                  AS [ID_Company],  
                           NULL                         AS [ID_Client],  
                           NULL                         AS [ID_Patient],  
                           NULL                         AS [Comment],  
                           GETDATE()                    AS [DateCreated],  
                           GETDATE()                    AS [DateModified],  
                           NULL                         AS [ID_CreatedBy],  
                           NULL                         AS [ID_LastModifiedBy],  
                           @Confinement_ID_FilingStatus AS ID_FilingStatus) H  
                   LEFT JOIN tUser UC  
                          ON H.ID_CreatedBy = UC.ID  
                   LEFT JOIN tUser UM  
                          ON H.ID_LastModifiedBy = UM.ID  
               LEFT JOIN tFilingStatus fs  
                ON fs.ID = H.ID_FilingStatus  
        END  
      ELSE  
        BEGIN  
            SELECT H.*  
            FROM   vPatient_Confinement H  
            WHERE  H.ID = @ID  
        END  

	SELECT * FROM vPatient_Confinement_ItemsServices
	WHERE ID_Patient_Confinement =  @ID

  END  
GO

ALTER PROC [dbo].[pPatient_Confinement_Validation] (@ID_Patient_Confinement INT,
                                                    @ID_Patient             INT,
                                                    @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      Declare @ConfinedPatientCount INT = 0

      BEGIN TRY
          SELECT @ConfinedPatientCount = Count(*)
          FROM   tPatient_Confinement
          WHERE  ID NOT IN ( @ID_Patient_Confinement )
                 AND ID_Patient = @ID_Patient
                 AND ID_FilingStatus = @Confined_ID_FilingStatus

          IF ( @ConfinedPatientCount > 0 )
            BEGIN
                SET @message = 'Selected Patient is already confined. Please discharge the patient first.';

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO

Create OR
ALTER PROC [dbo].[pDischargePatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                             @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotConfined TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotConfined INT = 0;

      /* Validate Patient_Confinement Status is not Confined*/
      INSERT @ValidateNotConfined
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Confined_ID_FilingStatus );

      SELECT @Count_ValidateNotConfined = COUNT(*)
      FROM   @ValidateNotConfined;

      IF ( @Count_ValidateNotConfined > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotConfined > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to discharged:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotConfined;

            THROW 50001, @message, 1;
        END;
  END;

GO

Create OR
ALTER PROC dbo.pDischargePatient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                              @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          exec [pDischargePatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Discharged_ID_FilingStatus,
                 DateDischarge = GETDATE(),
                 ID_DischargeBy = @ID_User
          FROM   dbo.tPatient_Confinement bi
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

Create OR
ALTER PROC [dbo].[pCancelPatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                          @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotDischarged TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotDischarged INT = 0;

      /* Validate Patient_Confinement Status is not Discharged*/
      INSERT @ValidateNotDischarged
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Discharged_ID_FilingStatus, @Confined_ID_FilingStatus );

      SELECT @Count_ValidateNotDischarged = COUNT(*)
      FROM   @ValidateNotDischarged;

      IF ( @Count_ValidateNotDischarged > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotDischarged > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to Canceled:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotDischarged;

            THROW 50001, @message, 1;
        END;
  END;

GO

Create OR
ALTER PROC dbo.pCancelPatient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          exec [pCancelPatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Confinement bi
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          Update tPatient_SOAP
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   tPatient_SOAP soap
                 INNER JOIN tPatient_Confinement confinement
                         ON soap.ID_Patient_Confinement = confinement.ID
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON confinement.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pGetEmployee] @ID         INT = -1,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;
      DECLARE @ID_Company INT;
      DECLARE @ID_Employee INT;
      DECLARE @UserAccount_ID_User INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT @UserAccount_ID_User = ID
      FROM   tUser
      WHERE  ID_Employee = @ID

      SELECT @ID_Employee = ID_Employee,
             @ID_Company = emp.ID_Company
      FROM   dbo.tUser _user
             INNER JOIN dbo.tEmployee emp
                     ON emp.ID = _user.ID_Employee
      WHERE  _user.ID = @ID_User;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL                 AS [_],
                           -1                   AS [ID],
                           ''                   AS [Code],
                           NULL                 AS [Name],
                           1                    AS [IsActive],
                           NULL                 AS [Comment],
                           NULL                 AS [DateCreated],
                           NULL                 AS [DateModified],
                           NULL                 AS [ID_CreatedBy],
                           NULL                 AS [ID_LastModifiedBy],
                           @ID_Company          [ID_Company],
                           ''                   [Company],
                           @UserAccount_ID_User UserAccount_ID_User,
                           0                    IsSystemUsed) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   @UserAccount_ID_User UserAccount_ID_User
            FROM   vEmployee H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROC [dbo].[SaveEmployeeUserAccount] @CompanyID  INT,
                                           @EmployeeID INT = -1,
                                           @Username   VARCHAR(300),
                                           @Password   VARCHAR(300)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Name_Company VARCHAR(300) = '';
      DECLARE @Username_User VARCHAR(300) = '';

      SELECT @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @CompanyID

      SELECT @Username_User = USername
      FROM   tUser
      WHERE  ID_Employee = @EmployeeID

      SET @Username = ISNULL(@Username, '')
      SET @Password = ISNULL(@Password, '')

      BEGIN TRY
          DECLARE @Count_EmployeeCompany INT =0
          DECLARE @Count_UserNameExist INT =0

          SELECT @Count_EmployeeCompany = COUNT(*)
          FROM   tEmployee
          WHERE  ID_Company = @CompanyID
                 AND ID = @EmployeeID

          if( @Count_EmployeeCompany = 0 )
            BEGIN
                SET @message = 'User is not included in ' + @Name_Company;

                THROW 50001, @message, 1;
            END

          SELECT @Count_UserNameExist = COUNT(*)
          FROM   vUser
          WHERE  LOWER(Username) = LOWER(@Username)
                 AND Username NOT IN ( @Username_User )

          if( @Count_UserNameExist > 0 )
            BEGIN
                SET @message = 'Username is already exist.';

                THROW 50001, @message, 1;
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      if( @Success = 1 )
        BEGIN
            UPDATE tUser
            SET    Username = @Username,
                   Password = @Password
            WHERE  ID_Employee = @EmployeeID
        END

      SELECT '_'

      SELECT @Success Success,
             @message message;
  END

GO

CREATE OR
ALTER PROC [dbo].[LoadEmployeeUserAccount] @EmployeeID INT = -1
AS
  BEGIN
      DECLARE @Username VARCHAR(MAX) = '';

      SELECT @Username = Username
      FROM   tUser
      WHERE  ID_Employee = @EmployeeID

      SELECT '_'

      SELECT @Username Username
  END

GO

CREATE OR
ALTER PROC pApprovePayablePayment (@IDs_PayablePayment typIntList READONLY,
                                   @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT '_';

      Update tPayablePayment
      SET    ID_FIlingStatus = 3
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      DECLARE @IDs_Payable typIntList

      INSERT @IDs_Payable
      SELECT ID_Payable
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      exec dbo.pUpdatePayableStatus
        @IDs_Payable

      SELECT @Success Success,
             @message message;
  END

GO

CREATE OR
ALTER PROC pCancelPayablePayment(@IDs_PayablePayment typIntList READONLY,
                                 @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT '_';

      Update tPayablePayment
      SET    ID_FIlingStatus = 4
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      DECLARE @IDs_Payable typIntList

      INSERT @IDs_Payable
      SELECT ID_Payable
      FROM   tPayablePayment pPay
             inner join @IDs_PayablePayment ids
                     ON ids.ID = pPay.ID

      exec dbo.pUpdatePayableStatus
        @IDs_Payable

      SELECT @Success Success,
             @message message;
  END

GO

CREATE   OR
Alter PROC pUpdatePayableStatus(@IDs_Payable typIntList READONLY)
AS
  BEGIN
      Declare @Payable table
        (
           ID_Payable    Int,
           PaymentAmount Decimal(18, 2)
        )

      INSERT @Payable
      SELECT ID,
             0
      FROM   @IDs_Payable

      Update @Payable
      SET    PaymentAmount = ISNULL(tbl.PaymentAmount, 0)
      FROM   @Payable payable
             LEFT JOIN (SELECT pPay.ID_Payable,
                               SUM(ISNULL(pPay.TotalAmount, 0)) PaymentAmount
                        FROm   tPayablePayment pPay
                               INNER JOIN @IDs_Payable idsPayable
                                       ON pPay.ID_Payable = idsPayable.ID
                        WHERE  pPay.ID_FilingStatus IN ( 3 )
                        GROUP  BY pPay.ID_Payable) tbl
                    ON tbl.ID_Payable = payable.ID_Payable

      Update tPayable
      SET    RemaningAmount = ISNULL(pAble.TotalAmount, 0) - ISNULL(payTotal.PaymentAmount, 0),
             PaidAmount = ISNULL(payTotal.PaymentAmount, 0)
      FROM   tPayable pAble
             inner JOIN @Payable payTotal
                     ON pAble.ID = payTotal.ID_Payable

      Update tPayable
      SET    Payment_ID_FilingStatus = dbo.fGetPaymentStatus(TotalAmount, RemaningAmount)
      FROM   tPayable pAble
             inner JOIN @Payable payTotal
                     ON pAble.ID = payTotal.ID_Payable
  END

GO

Alter PROC pGetPayable_PayableDetail_Listview_Summary(@CompanyID    INT,
                                                      @filterString VARCHAR(MAX))
AS
  BEGIN
      Declare @Payable       Decimal(18, 0),
              @TotalRemaning Decimal(18, 0),
              @TotalPaid     Decimal(18, 0)
      DECLARE @Record TABLE
        (
           TotalPayable  Decimal(18, 0),
           TotalRemaning Decimal(18, 0),
           TotalPaid     Decimal(18, 0)
        )

      IF( LEN(RTRIM(LTRIM(ISNULL(@filterString, '')))) > 0 )
        SET @filterString = ' AND ' + @filterString

      INSERT @Record
      EXEC ('SELECT 
					SUM(TotalAmount) TotalPayable,
					SUM(RemaningAmount) TotalRemaning,
					SUM(ISNULL(PaidAmount, 0 )) TotalPaidAmount
             FROM   vPayable_PayableDetail_Listview 
			 WHERE 
				ID_Company = '+ @CompanyID +' 
				'+ @filterString +'
				AND 1 = 1
			 ')

      SELECT '_'

      SELECT *
      FROM   @Record
  END

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC pLogItemUnitPrice
              @ID_CurrentObject

            EXEC pLogItemUnitCost
              @ID_CurrentObject

            EXEC dbo.pUpdateItemCurrentInventory
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC dbo.pModel_AfterSaved_Patient_Confinement (@ID_CurrentObject VARCHAR(10),
                                                      @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      SELECT @ID_Patient = ID_Patient
      FROM   tPatient_Confinement
      WHERE  ID = @ID_CurrentObject

      UPDATE tPatient_SOAP
      SET    ID_Patient = @ID_Patient
      WHERE  ID = @ID_CurrentObject
  END;

GO

ALTER PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,
                                    @ID_Client              INT = NULL,
                                    @ID_Patient             INT = NULL,
                                    @ID_SOAPType            INT = NULL,
                                    @ID_Patient_Confinement INT = NULL,
                                    @ID_Session             INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription;

      DECLARE @ID_User INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @Confinement_ID_SOAPType INT = 2;
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
        + 'Vet Visit History: ' + CHAR(9) + CHAR(13)
        + 'Vaccinations: ' + CHAR(9) + CHAR(13)
        + 'Deworming: ' + CHAR(9) + CHAR(13) + 'Vitamins: '
        + CHAR(9) + CHAR(13) + 'Spayed/neutered: '
        + CHAR(9) + CHAR(13) + 'Medications given: '
        + CHAR(9) + CHAR(13)
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + CHAR(9) + CHAR(13) + '  Wbc= ' + CHAR(9)
        + CHAR(13) + '  Lym= ' + CHAR(9) + CHAR(13)
        + '  Mon= ' + CHAR(9) + CHAR(13) + '  Neu= ' + CHAR(9)
        + CHAR(13) + '  Eos= ' + CHAR(9) + CHAR(13)
        + '  Bas= ' + CHAR(9) + CHAR(13) + '  Rbc= ' + CHAR(9)
        + CHAR(13) + '  Hgb= ' + CHAR(9) + CHAR(13)
        + '  Hct= ' + CHAR(9) + CHAR(13) + '  Mcv= ' + CHAR(9)
        + CHAR(13) + '  Mch= ' + CHAR(9) + CHAR(13)
        + '  Mchc ' + CHAR(9) + CHAR(13) + '  Plt= ' + CHAR(9)
        + CHAR(13) + '  Mpv= ' + CHAR(9) + CHAR(13) + CHAR(9)
        + CHAR(13) + 'Blood Chem: ' + CHAR(9) + CHAR(13)
        + '  Alt= ' + CHAR(9) + CHAR(13) + '  Alp= ' + CHAR(9)
        + CHAR(13) + '  Alb= ' + CHAR(9) + CHAR(13)
        + '  Amy= ' + CHAR(9) + CHAR(13) + '  Tbil= '
        + CHAR(9) + CHAR(13) + '  Bun= ' + CHAR(9) + CHAR(13)
        + '  Crea= ' + CHAR(9) + CHAR(13) + '  Ca= ' + CHAR(9)
        + CHAR(13) + '  Phos= ' + CHAR(9) + CHAR(13)
        + '  Glu= ' + CHAR(9) + CHAR(13) + '  Na= ' + CHAR(9)
        + CHAR(13) + '  K= ' + CHAR(9) + CHAR(13) + '  TP= '
        + CHAR(9) + CHAR(13) + '  Glob= ' + CHAR(9) + CHAR(13)
        + CHAR(9) + CHAR(13) + 'Microscopic Exam: '
        + CHAR(9) + CHAR(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SET @ID_SOAPType = @Confinement_ID_SOAPType

            SELECT @ID_Client = patient.ID_Client,
                   @ID_Patient = ID_Patient,
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus
            FROM   tPatient_Confinement hed
                   LEFT JOIN tPatient patient
                          ON hed.ID_Patient = patient.ID
            WHERE  hed.ID = @ID_Patient_Confinement
        END

      IF ISNULL(@ID_Patient, 0) <> 0
        BEGIN
            SELECT @IsDeceased = Isnull(IsDeceased, 0),
                   @ID_Client = ID_Client
            FROM   tPatient
            WHERE  ID = @ID_Patient;
        END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT c.ImageRowIndex,
             ROW_NUMBER()
               OVER (
                 ORDER BY ImageNo) AS RowIndex,
             c.ImageNo,
             FilePath,
             c.Remark
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
      WHERE  ps.ID = @ID
             AND ( ISNULL(FilePath, '') <> ''
                    OR LEN(ISNULL(Remark, '')) > 0 )
      ORDER  BY c.ImageRowIndex ASC;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   patient.Name                  Name_Patient,
                   patient.Name_Client,
                   fs.Name                       Name_FilingStatus,
                   soapType.Name                 Name_SOAPType,
                   confinement.Code              Code_Patient_Confinement,
                   confinement.Date              Date_Patient_Confinement,
                   confinement.DateDischarge     DateDischarge_Patient_Confinement,
                   confinement.ID_FilingStatus   ID_FilingStatus_Patient_Confinement,
                   confinement.Name_FilingStatus Name_FilingStatus_Patient_Confinement
            FROM   (SELECT NULL                                 AS [_],
                           -1                                   AS [ID],
                           '-New-'                              AS [Code],
                           NULL                                 AS [Name],
                           1                                    AS [IsActive],
                           NULL                                 AS [ID_Company],
                           NULL                                 AS [Comment],
                           NULL                                 AS [DateCreated],
                           NULL                                 AS [DateModified],
                           @ID_User                             AS [ID_CreatedBy],
                           NULL                                 AS [ID_LastModifiedBy],
                           @ID_Client                           AS ID_Client,
                           @ID_Patient                          AS ID_Patient,
                           @ID_Patient_Confinement              AS ID_Patient_Confinement,
                           GETDATE()                            Date,
                           @FILED_ID_FilingStatus               ID_FilingStatus,
                           @IsDeceased                          IsDeceased,
                           @ID_SOAPType                         ID_SOAPType,
                           @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,
                           @ObjectiveTemplate                   ObjectiveTemplate,
                           @AssessmentTemplate                  AssessmentTemplate,
                           @LaboratoryTemplate                  LaboratoryTemplate,
                           @ClinicalExaminationTemplate         ClinicalExaminationTemplate,
                           @DiagnosisTemplate                   DiagnosisTemplate) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType
                   LEFT JOIN vPatient_Confinement confinement
                          ON confinement.ID = H.ID_Patient_Confinement
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,
                   patient.IsDeceased,
                   @ObjectiveTemplate                   ObjectiveTemplate,
                   @AssessmentTemplate                  AssessmentTemplate,
                   @LaboratoryTemplate                  LaboratoryTemplate,
                   @ClinicalExaminationTemplate         ClinicalExaminationTemplate,
                   @DiagnosisTemplate                   DiagnosisTemplate
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          on h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID
  END;

GO

ALTER PROC dbo.pDonePatient_SOAP_validation (@IDs_Patient_SOAP typIntList READONLY,
                                             @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      /* Validate Patient SOAP Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_SOAP bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_SOAP ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to Doned:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pGetSendSOAPPlan](@Date      DateTime,
                                    @IsSMSSent Bit = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETime,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETime,
           ID_Patient_SOAP_Plan INT
        )

      INSERT @record
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                                            Name_Client,
             ISNULL(client.ContactNumber, '0')                                                                                                                                                      ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, ISNULL(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND ( ( CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date) )
                    OR ( CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ) )
             AND patientSOAP.ID_Company IN (SELECT ID_Company
                                            FROM   tCompany_SMSSetting
                                            WHERE  ISNULL(IsActive, 0) = 1)
      ORDER  BY c.Name

      SELECT @Success Success;

      SELECT FORMAT(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             SUM(CASE
                   WHEN LEN(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN LEN(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN LEN(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          ConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateSending, 'yyyy-MM-dd'),
                Name_Company
      Order  BY DateSending DESC,
                Name_Company
  END

SELECT *
FROM   @record
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,
          Name_Company,
          Name_Client

GO

ALTER PROC pUpdatePatientsLastVisitedDate(@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @tbl TABLE
        (
           ID_Patient      INT,
           DateLastVisited DATETIME
        )

      INSERT @tbl
             (ID_Patient)
      SELECT ID
      FROM   @IDs_Patient

      UPDATE @tbl
      SET    DateLastVisited = rec.DateLastVisited
      FROM   @tbl tbl
             inner join (SELECT MAX(Date) DateLastVisited,
                                soap.ID_Patient
                         FROM   tPatient_SOAP soap
                                INNER JOIN @tbl tbl
                                        ON tbl.ID_Patient = soap.ID_Patient
                         WHERE  ID_FilingStatus IN ( 1, 3, 13 )
                         GROUP  BY soap.ID_Patient) rec
                     ON rec.ID_Patient = tbl.ID_Patient

      UPDATE tPatient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tPatient patient
             INNER JOIN @tbl tbl
                     ON tbl.ID_Patient = patient.ID

      Update tClient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tClient client
             INNER JOIN (SELECT ID_Client,
                                MAX(patient.DateLastVisited) DateLastVisited
                         FROM   tPatient patient
                                INNER JOIN @tbl tbl
                                        ON tbl.ID_Patient = patient.ID
                         GROUP  BY ID_Client) tbl
                     ON client.ID = tbl.ID_Client
  END

GO

DECLARE @Patient_Confinement_ListView VARCHAR(MAX) = ''

SELECT @Patient_Confinement_ListView = Oid
FROM   _tListview
where  Name = 'Patient_Confinement_ListView'

IF ( NOT EXISTS (SELECT *
                 FROM   dbo.tCustomNavigationLink
                 WHERE  Oid_ListView = @Patient_Confinement_ListView) )
  BEGIN
      INSERT INTO [dbo].[tCustomNavigationLink]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_ListView],
                   [RouterLink],
                   [ID_ViewType],
                   [Oid_Report])
      VALUES      (NULL,
                   UPPER('Patient_Confinement_ListView'),
                   1,
                   1,
                   NULL,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @Patient_Confinement_ListView,
                   'ConfinementList',
                   1,
                   NULL)
  END

GO 

Update _tnavigation
SET Caption = 'Confinement'
Where Name = 'Patient_Confinement_ListView'

Update _tMOdel
SET DisplayName = 'Confinement'
Where Name = 'Patient_Confinement'