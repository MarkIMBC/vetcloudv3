GO

exec _pAddModelProperty
  'tPatient_SOAP_Plan',
  'DateRescheduletUpdated',
  5

exec _pAddModelProperty
  'tPatient_Wellness_Schedule',
  'DateRescheduletUpdated',
  5

exec _pAddModelProperty
  'tPatientAppointment',
  'DateRescheduletUpdated',
  5

exec _pAddModelProperty
  'tPatient_SOAP_Plan',
  'DateReschedule',
  5

exec _pAddModelProperty
  'tPatient_Wellness_Schedule',
  'DateReschedule',
  5

exec _pAddModelProperty
  'tPatientAppointment',
  'DateReschedule',
  5

exec _pAddModelProperty
  'tAppointmentStatusLog',
  'DateReschedule',
  5

exec _pAddModelProperty
  'tPatientWaitingList',
  'DateReschedule',
  5

exec _pAddModelProperty
  'tPatientWaitingList_Logs',
  'DateReschedule',
  5

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER FUNCTION [dbo].fGetLabelActionQueue (@WaitingStatus_ID_FilingStatus   INT,
                                           @WaitingStatus_Name_FilingStatus VARCHAR(MAX))
RETURNS VARCHAR(200)
AS
  BEGIN
      DECLARE @label VARCHAR(200) = ''
      DECLARE @ID_FilingStatus_Pending INT = 4
      DECLARE @ID_FilingStatus_Canceled INT = 4
      DECLARE @ID_FilingStatus_Waiting INT = 8
      DECLARE @ID_FilingStatus_Done INT = 13
      DECLARE @ID_FilingStatus_Rescheduled INT = 21

      SET @WaitingStatus_ID_FilingStatus = ISNULL(@WaitingStatus_ID_FilingStatus, @ID_FilingStatus_Canceled)

      IF( @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Canceled
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Done
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Rescheduled
           OR @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Pending )
        BEGIN
            SET @label = 'Add to Queue'
        END
      else IF ( @WaitingStatus_ID_FilingStatus = @ID_FilingStatus_Waiting )
        BEGIN
            SET @label = 'Remove From Queue'
        END
      ELSE
        SET @label = @WaitingStatus_Name_FilingStatus

      RETURN @label;
  END;

GO

----------------------------------------------------VIEWS --------------------------------------------------------------  
GO

CREATE       OR
ALTER VIEW vPatientWaitingList_ListView_temp
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         --IsnULL(appointment.UniqueIDList, '') UniqueIDList,        
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.Name_Model_Reference,
         waitingList.ID_Reference,
         Convert(Date, waitingList.DateReschedule)                   DateReschedule
  FROM   vPatientWaitingList waitingList
         LEFT Join tPatient patient
                on waitingList.ID_Patient = patient.ID
  -- OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment        
  WHERE  waitingList.WaitingStatus_ID_FilingStatus IN ( 8, 9 )
          OR ( ISNULL(waitingList.IsQueued, 0) = 1
               and ISNULL(waitingList.WaitingStatus_ID_FilingStatus, 0) NOT IN ( 0, 4, 13, 21 ) )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            --appointment.UniqueIDList,        
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.Name_Model_Reference,
            waitingList.ID_Reference,
            Convert(Date, waitingList.DateReschedule)

GO

GO

CREATE      OR
ALTER VIEW [dbo].[vPatientWaitingCanceledList_ListView]
AS
  SELECT DISTINCT ISNULL(appointment.UniqueID, CONVERT(VARCHAR(MAX), MAX(hed.ID))) UniqueID,
                  hed.ID_Company,
                  ISNULL(appointment.DateStart, hed.DateCreated)                   DateStart,
                  hed.DateCreated                                                  DateCreated,
                  ISNULL(appointment.DateEnd, hed.DateCreated)                     DateEnd,
                  hed.ID_Client,
                  hed.Name_Client,
                  hed.ID_Patient,
                  hed.Name_Patient,
                  ISNULL(appointment.ReferenceCode, CASE
                                                      WHEN hed.IsQueued = 1 THEN 'Queued'
                                                      ELSE ''
                                                    END)                           ReferenceCode,
                  hed.WaitingStatus_ID_FilingStatus,
                  hed.WaitingStatus_Name_FilingStatus,
                  appointment.Description,
                  hed.IsQueued,
                  hed.Oid_Model_Reference,
                  hed.ID_Reference,
                  CONVERT(Date, hed.DateReschedule)                                DateReschedule
  FROM   vPatientWaitingList hed
         LEFT JOIN vAppointmentEvent appointment
                on hed.ID_Client = appointment.ID_Client
                   and hed.ID_Patient = appointment.ID_Patient
                   AND hed.Oid_Model_Reference = appointment.Oid_Model
                   and hed.ID_Reference = appointment.Appointment_ID_CurrentObject
         LEFT JOIN tPatient patient
                on patient.ID = appointment.ID_Patient
  where  hed.WaitingStatus_ID_FilingStatus IN ( 4, 21 )
  GROUP  BY appointment.UniqueID,
            hed.IsQueued,
            hed.ID_Company,
            hed.DateCreated,
            appointment.DateStart,
            appointment.DateEnd,
            hed.ID_Client,
            hed.Name_Client,
            hed.ID_Patient,
            hed.Name_Patient,
            appointment.ReferenceCode,
            hed.WaitingStatus_ID_FilingStatus,
            hed.WaitingStatus_Name_FilingStatus,
            appointment.Description,
            hed.Oid_Model_Reference,
            hed.ID_Reference,
            CONVERT(Date, hed.DateReschedule)

GO

CREATE OR
ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         IsNull(patientSOAP.Name_SOAPType, '')
         + CASE
             WHEN LEN(IsNull(patientSOAPPlan.Name_Item, '')) > 0 THEN ' - '
                                                                      + IsNull(patientSOAPPlan.Name_Item, '')
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(patientSOAPPlan.Comment, '')) > 0 THEN ' - ' + patientSOAPPlan.Comment
             ELSE ''
           END                                                                                 Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks,
         patientSOAPPlan.DateReschedule,
         patientSOAP.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + '0' + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks,
         patientAppnt.DateReschedule,
         patientAppnt.AttendingPhysician_Name_Employee
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellSched.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull(wellSched.Comment, '')                                        Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks,
                  wellSched.DateReschedule,
                  wellness.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

GO

CREATE OR
ALTER VIEW vPayable_PayableDetail_Listview
as
  SELECT payable.ID,
         payable.Date,
         payable.ID_Company,
         payableDetail.ID_ExpenseCategory ID_ExpenseCategory,
         expensesCat.Name                 Name_ExpenseCategory,
         payableDetail.Name               Name_Payable_Detail,
         payable.TotalAmount,
         payable.Payment_ID_FilingStatus,
         fs.Name                          Payment_Name_FilingStatus,
         payable.RemaningAmount,
         payable.PaidAmount
  FROM   tPayable payable
         INNER JOIN tPayable_Detail payableDetail
                 ON payableDetail.ID_Payable = payable.ID
         LEFT JOIN tExpenseCategory expensesCat
                ON expensesCat.ID = payableDetail.ID_ExpenseCategory
         LEFT JOIN tFilingStatus fs
                ON fs.ID = payable.Payment_ID_FilingStatus
  WHERE  Payment_ID_FilingStatus NOT IN ( 4 )

GO

CREATE OR
ALTER VIEW [dbo].[vzInventorySummaryReport]
AS
  SELECT item.ID                                                 ID,
         LTRIM(RTRIM(item.Name))                                 Name_Item,
         SUM(IsNULL(hed.Quantity, 0))                            TotalQuantity,
         OtherInfo_DateExpiration                                DateExpired,
         FORMAT(OtherInfo_DateExpiration, 'MM/dd/yyyy')          FormattedDateExpired,
         NULL                                                    BatchNo,
         MAX(CASE
               WHEN hed.Quantity > 0 THEN Date
               ELSE NULL
             END)                                                DateLastIn,
         MAX(CASE
               WHEN hed.Quantity < 0 THEN Date
               ELSE NULL
             END)                                                DateLastOut,
         ISNULL(item.UnitCost, 0)                                UnitCost,
         ISNULL(item.UnitPrice, 0)                               UnitPrice,
         item.ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         CASE
           WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN '('
                                                               + dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired' + ')', 'Expired')
           ELSE ''
         END                                                     RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
         invtStatus.Name                                         Name_InventoryStatus,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         item.ID_InventoryStatus
  FROM   dbo.tItem item
         LEFT JOIN dbo.tInventoryTrail hed
                ON item.ID = hed.ID_Item
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
         LEFT JOIN dbo.tInventoryStatus invtStatus
                ON invtStatus.ID = item.ID_InventoryStatus
  WHERE  ISNULL(item.IsActive, 0) = 1
         and ID_ITEMTYpe = 2
  GROUP  BY item.ID,
            item.Name,
            ISNULL(item.UnitCost, 0),
            ISNULL(item.UnitPrice, 0),
            company.ImageLogoLocationFilenamePath,
            company.Name,
            OtherInfo_DateExpiration,
            company.Address,
            item.ID_Company,
            invtStatus.Name,
            company.ContactNumber,
            company.Email,
            item.ID_InventoryStatus

GO

GO

CREATE   OR
ALTER PROC _pUpdatePatientToWaitingListStatus(@IDs_PatientWaitingList        typIntList READONLY,
                                              @WaitingStatus_ID_FilingStatus INT,
                                              @DateReschedule                DateTime,
                                              @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    /* Update Waiting List Status */
    Update tPatientWaitingList
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus,
           DateSent = NULL,
           IsSentSMS = NULL,
           DateReschedule = @DateReschedule
    FROM   tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /*Update WaitingStatus_ID_FilingStatus on Patient Record*/
    Update tPatient
    SET    WaitingStatus_ID_FilingStatus = @WaitingStatus_ID_FilingStatus
    FROM   tPatient patient
           inner join tPatientWaitingList waitingList
                   ON patient.ID = waitingList.ID_Patient
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

    /* Add Logs */
    INSERT INTO [dbo].[tPatientWaitingList_Logs]
                ([Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [ID_CreatedBy],
                 [ID_PatientWaitingList],
                 [WaitingStatus_ID_FilingStatus],
                 DateReschedule)
    SELECT H.Name,
           H.IsActive,
           waitingList.ID_Company,
           H.Comment,
           H.DateCreated,
           H.ID_CreatedBy,
           waitingList.ID,
           @WaitingStatus_ID_FilingStatus,
           @DateReschedule
    FROM   (SELECT NULL      [Name],
                   NULL      [IsActive],
                   NULL      [Comment],
                   GETDATE() [DateCreated],
                   @ID_User  [ID_CreatedBy]) H,
           tPatientWaitingList waitingList
           INNER JOIN @IDs_PatientWaitingList ids
                   ON waitingList.ID = ids.ID

GO

CREATE   OR
ALTER PROC pUpdateLatestPatientToWaitingListStatus(@IDs_Patient                   typIntList READONLY,
                                                   @WaitingStatus_ID_FilingStatus INT,
                                                   @ID_UserSession                INT)
AS
  BEGIN
      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      SELECT MAX(patientWaitingList.ID)
      FROM   tPatientWaitingList patientWaitingList
             inner join @IDs_Patient patient
                     on patientWaitingList.ID_Patient = patient.ID
      GROUP  BY patientWaitingList.ID_Patient

      exec pUpdatePatientToWaitingListStatus
        @IDs_PatientWaitingList,
        null,
        @WaitingStatus_ID_FilingStatus,
        @ID_UserSession
  END

Go

GO

CREATE    OR
ALTER PROC [dbo].[pDoAddPatientToWaitingList_validation] (@IDs_Patient    typIntList READONLY,
                                                          @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Reschedule_ID_FilingStatus INT = 21;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @PatientAlreadyWaitingList TABLE
        (
           Name_Client                     VARCHAR(30),
           Name_Patient                    VARCHAR(30),
           WaitingStatus_Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_PatientAlreadyWaitingList INT = 0;

      INSERT @PatientAlreadyWaitingList
             (Name_Client,
              Name_Patient,
              WaitingStatus_Name_FilingStatus)
      SELECT Distinct Name_Client,
                      Name_Patient,
                      WaitingStatus_Name_FilingStatus
      FROm   vPatientWaitingList pWait
             INNER JOIN @IDs_Patient ids
                     ON pWait.ID_Patient = ids.ID
      WHERE  WaitingStatus_ID_FilingStatus NOT IN ( @Done_ID_FilingStatus, @Canceled_ID_FilingStatus, @Reschedule_ID_FilingStatus )

      SELECT @Count_PatientAlreadyWaitingList = COUNT(*)
      FROM   @PatientAlreadyWaitingList;

      IF ( @Count_PatientAlreadyWaitingList > 0 )
        BEGIN
            SET @message = 'The following patient'
                           + CASE
                               WHEN @Count_PatientAlreadyWaitingList > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'already on waiting list:';

            SELECT @message = @message + CHAR(10) + Name_Client + ' Pet:'
                              + Name_Patient + ' - '
                              + WaitingStatus_Name_FilingStatus
            FROM   @PatientAlreadyWaitingList;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE   OR
ALTER PROC pUpdatePatientToWaitingListStatus(@IDs_PatientWaitingList        typIntList READONLY,
                                             @WaitingStatus_ID_FilingStatus INT,
                                             @DateReschedule                DateTime,
                                             @ID_UserSession                INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    exec _pUpdatePatientToWaitingListStatus
      @IDs_PatientWaitingList,
      @WaitingStatus_ID_FilingStatus,
      @DateReschedule,
      @ID_UserSession

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

CREATE   OR
ALTER PROC pUpdateAppointmentStatus(@Oid_Model                    VARCHAR(MAX),
                                    @Appointment_ID_CurrentObject INT,
                                    @Appointment_ID_FilingStatus  INT,
                                    @DateReschedule               DateTime,
                                    @ID_UserSession               INT,
                                    @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @TableName VARCHAR(MAX) = ''
      DECLARE @ID_Company INT = 0
      DECLARE @ID_User INT = 0
      DECLARE @Reschedule_ID_FilingStatus INT = 21
      DECLARE @ID_Patient INT

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @TableName = TableName
      FROM   _tModel
      WHERE  Oid = @Oid_Model

      IF( @TableName = 'tPatientAppointment' )
        BEGIN
            UPDATE tPatientAppointment
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatientAppointment
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_SOAP' )
        BEGIN
            UPDATE tPatient_SOAP_Plan
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_SOAP_Plan _plan
                   INNER JOIN tPatient_SOAP _soap
                           on _plan.ID_Patient_SOAP = _soap.ID
            WHERE  _plan.ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_Wellness' )
        BEGIN
            UPDATE tPatient_Wellness_Schedule
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks,
                   DateReschedule = @DateReschedule
            WHERE  ID = @Appointment_ID_CurrentObject

            SELECT @ID_Patient = ID_Patient
            FROM   tPatient_Wellness_Schedule _schedule
                   INNER JOIN tPatient_Wellness _wellness
                           on _schedule.ID_Patient_Wellness = _wellness.ID
            WHERE  _schedule.ID = @Appointment_ID_CurrentObject
        END

      ---------------------------------------------------------------------------------  
      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      SELECT MAX(_waiting.ID)
      FROM   tPatientWaitingList _waiting
      WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
             AND ID_Reference = @Appointment_ID_CurrentObject
             AND ID_Patient = @ID_Patient

      DELETE FROM @IDs_PatientWaitingList
      WHERE  ID IS NULL

      IF(SELECT COUNT(*)
         FROM   @IDs_PatientWaitingList) = 0
        BEGIN
            exec pAddAppointmentToWaitingList
              @Oid_Model,
              @Appointment_ID_CurrentObject,
              @ID_Patient,
              @ID_UserSession

            INSERT @IDs_PatientWaitingList
            SELECT MAX(_waiting.ID)
            FROM   tPatientWaitingList _waiting
            WHERE  LOWER(Oid_Model_Reference) = LOWER(@Oid_Model)
                   AND ID_Reference = @Appointment_ID_CurrentObject
                   AND ID_Patient = @ID_Patient
        END

      exec _pUpdatePatientToWaitingListStatus
        @IDs_PatientWaitingList,
        @Appointment_ID_FilingStatus,
        @DateReschedule,
        @ID_UserSession

      ---------------------------------------------------------------------------------  
      /*Logs*/
      INSERT INTO [dbo].[tAppointmentStatusLog]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Appointment_ID_CurrentObject],
                   [Appointment_ID_FilingStatus],
                   DateReschedule)
      VALUES      ( 1,
                    @ID_Company,
                    @Remarks,
                    GetDate(),
                    GetDate(),
                    @ID_User,
                    @ID_User,
                    @Oid_Model,
                    @Appointment_ID_CurrentObject,
                    @Appointment_ID_FilingStatus,
                    @DateReschedule)
  END

GO

CREATE OR
Alter PROC pDoUpdateAppointmentStatus(@Oid_Model                    VARCHAR(MAX),
                                      @Appointment_ID_CurrentObject INT,
                                      @Appointment_ID_FilingStatus  INT,
                                      @DateReschedule               DateTime,
                                      @ID_UserSession               INT,
                                      @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_User INT = 0;

      BEGIN TRY
          EXEC pUpdateAppointmentStatus
            @Oid_Model,
            @Appointment_ID_CurrentObject,
            @Appointment_ID_FilingStatus,
            @DateReschedule,
            @ID_UserSession,
            @Remarks
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO

GO

CREATE   OR
ALTER PROC pDoUpdateAppointmentScheduleTime(@Oid_Model                    VARCHAR(MAX),
                                            @Appointment_ID_CurrentObject INT,
                                            @DateTime                     DateTime,
                                            @ID_UserSession               INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @TableName VARCHAR(MAX) = ''
          DECLARE @ID_Company INT = 0
          DECLARE @ID_User INT = 0
          DECLARE @Reschedule_ID_FilingStatus INT = 21
          DECLARE @ID_Patient INT

          SELECT @ID_User = ID_User
          FROM   tUserSession
          WHERE  ID = @ID_UserSession

          SELECT @ID_Company = ID_Company
          FROM   vUser
          WHERE  ID = @ID_User

          SELECT @TableName = TableName
          FROM   _tModel
          WHERE  Oid = @Oid_Model

          IF( @TableName = 'tPatientAppointment' )
            BEGIN
                UPDATE tPatientAppointment
                SET    DateStart = Convert(DateTime, FORMAT(DateStart, 'yyyy-MM-dd') + ' '
                                                     + FORMAT(@DateTime, 'HH:mm')),
                       DateEnd = Convert(DateTime, FORMAT(DateStart, 'yyyy-MM-dd') + ' '
                                                   + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
          ELSE IF( @TableName = 'tPatient_SOAP' )
            BEGIN
                UPDATE tPatient_SOAP_Plan
                SET    DateReturn = Convert(DateTime, FORMAT(DateReturn, 'yyyy-MM-dd') + ' '
                                                      + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
          ELSE IF( @TableName = 'tPatient_Wellness' )
            BEGIN
                UPDATE tPatient_Wellness_Schedule
                SET    Date = Convert(DateTime, FORMAT(Date, 'yyyy-MM-dd') + ' '
                                                + FORMAT(@DateTime, 'HH:mm'))
                WHERE  ID = @Appointment_ID_CurrentObject
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO

Update _tNavigation
set    Caption = 'Expenses'
where  Name = 'Payable_ListView' 
