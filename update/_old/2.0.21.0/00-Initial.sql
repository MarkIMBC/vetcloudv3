GO

CREATE OR
ALTER VIEW vClientWithdraw
AS
  SELECT H.*,
         confinement.Name_Client,
         confinement.ID_Patient,
         confinement.Name_Patient,
         UC.Name           AS CreatedBy,
         UM.Name           AS LastModifiedBy,
         approvedUser.Name AS ApprovedBy_Name_User,
         cancelUser.Name   AS CanceledBy_Name_User,
         fs.Name           Name_FilingStatus,
         confinement.Code  Code_Patient_Confinement
  FROM   tClientWithdraw H
         LEFT JOIN vPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.ID_FilingStatus
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID

GO

CREATE OR
ALTER PROC [dbo].[pApproveClientWithdraw_validation] (@IDs_ClientWithdraw typIntList READONLY,
                                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientWithdraw bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientWithdraw ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pApproveClientWithdraw] (@IDs_ClientWithdraw typIntList READONLY,
                                           @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @NoAssignedClient_IDs_ClientWithdraw TABLE
            (
               ID_ClientWithdraw INT,
               ClientCount       INT
            )

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveClientWithdraw_validation
            @IDs_ClientWithdraw,
            @ID_UserSession;

          UPDATE dbo.tClientWithdraw
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tClientWithdraw bi
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON bi.ID = ids.ID;

          -- Add Withdraw on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.WithdrawAmount * -1,
                 Code,
                 Comment
          FROM   tClientWithdraw cd
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

GO

CREATE OR
ALTER PROC [dbo].[pCancelClientWithdraw_validation] (@IDs_ClientWithdraw typIntList READONLY,
                                                     @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @PartiallyServed_ID_FilingStatus INT = 5;
      DECLARE @FullyServed_ID_FilingStatus INT = 6;
      DECLARE @OverServed_ID_FilingStatus INT = 7;
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Text Blast Status is not Approved*/
      INSERT @ValidateNotApproved
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientWithdraw bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientWithdraw ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus, @Done_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancelClientWithdraw] (@IDs_ClientWithdraw typIntList READONLY,
                                          @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelClientWithdraw_validation
            @IDs_ClientWithdraw,
            @ID_UserSession;

          UPDATE dbo.tClientWithdraw
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientWithdraw bi
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON bi.ID = ids.ID;

          -- Subtract Withdraw on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.WithdrawAmount,
                 Code,
                 'Cancel Withdraw'
          FROM   tClientWithdraw cd
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_PatientWaitingList] (@ID_CurrentObject VARCHAR(10),
                                                         @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatientWaitingList
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatientWaitingList';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatientWaitingList
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_PatientWaitingList typIntList

      INSERT @IDs_PatientWaitingList
      VALUES (@ID_CurrentObject)
  END;

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_ClientWithdraw] (@ID_CurrentObject VARCHAR(10),
                                                     @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tClientWithdraw
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tClientWithdraw';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tClientWithdraw
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_ClientWithdraw typIntList

      INSERT @IDs_ClientWithdraw
      VALUES (@ID_CurrentObject)
  END;

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO 
