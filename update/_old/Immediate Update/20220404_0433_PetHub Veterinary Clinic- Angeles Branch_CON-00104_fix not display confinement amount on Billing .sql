DECLARE @GUID_Company VARCHAR(MAX) = '4A0E491E-5B07-4436-BB68-6D79D42AD35B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tBillingInvoice
SET    ConfinementDepositAmount = 5750
WHERE  ID = 232127
       and ID_Company = @ID_Company

SELECT *
FROm   vPatient_Confinement
WHERE  ID = 5109
       and ID_Company = @ID_Company

SELECT ID,
       Code,
       Date,
       InitialConfinementDepositAmount,
       ConfinementDepositAmount,
       SubTotal,
       GrossAmount,
       TotalAmount
FROm   vBillingInvoice
WHERE  ID = 232127
       and ID_Company = @ID_Company

SELECT *
FROM   vUser
WHERE  ID_Company = @ID_Company 



