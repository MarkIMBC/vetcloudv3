DEclare @GUID_Company VARCHAR(MAX) = '30B74061-6489-4093-BAFE-CF1AE8200052'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
SELECT *
FROM   vActiveItem
WHERE   ID_Company = @ID_Company

Update tItem
SET    IsActive = 0
WHERE  ID_Company = @ID_Company 


Update tDocumentSeries SET Counter = 1 where ID_Company = @ID_Company and Name = 'Item'