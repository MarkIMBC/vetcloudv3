IF COL_LENGTH('tPatient_SOAP_Plan', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP_Plan',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-09-14-Patient_SOAP_Plan', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-14-Patient_SOAP_Plan]

GO

Declare @NotYetInserted_Temp_ID_Patient_SOAP_Plan TABLE
  (
     Temp_ID_Patient_SOAP_Plan VARCHAR(MAX)
  )

SELECT company.Guid                                  Main_Guid_Company,
       company.ID                                    Main_ID_Company,
       company.Name                                  Name_Company,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP-'
       + Convert(Varchar(MAX), Patient_SOAP_.ID)     Temp_ID_Patient_SOAP,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP_Plan-'
       + Convert(Varchar(MAX), Patient_SOAP_Plan.ID) Temp_ID_Patient_SOAP_Plan,
       Patient_SOAP_Plan.*
INTO   [dbo].[Temp-2022-09-14-Patient_SOAP_Plan]
FROM   _______db_vetcloudv3_oldserver_20220914164457.[dbo].vPatient_SOAP Patient_SOAP_
       INNER JOIN _______db_vetcloudv3_oldserver_20220914164457.[dbo].tPatient_SOAP_Plan Patient_SOAP_Plan
               on Patient_SOAP_.ID = Patient_SOAP_Plan.ID_Patient_SOAP
       inner join _______db_vetcloudv3_oldserver_20220914164457.[dbo].tCompany company
               on Patient_SOAP_.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-09-14-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient_SOAP_.ID_Client)
                 and mainClient.ID_Company = Patient_SOAP_.ID_Company
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-09-14-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), Patient_SOAP_.ID_Patient)
                 and mainPatient.ID_Company = Patient_SOAP_.ID
where  Patient_SOAP_.DateCreated > '2022-08-30 16:42:32.770' and company.GUID = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

INSERT @NotYetInserted_Temp_ID_Patient_SOAP_Plan
SELECT tempPatient_SOAP.Temp_ID_Patient_SOAP_Plan
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Plan] tempPatient_SOAP

DELETE FROM @NotYetInserted_Temp_ID_Patient_SOAP_Plan
WHERE  Temp_ID_Patient_SOAP_Plan IN (SELECT tempID
                                     FROM   tPatient_SOAP_Plan)

SELECT * FROM  [dbo].[Temp-2022-09-14-Patient_SOAP_Plan] Order by DateReturn

INSERT tPatient_SOAP_Plan
       (ID_Patient_SOAP,
        DateReturn,
        ID_Item,
        Comment,
        DateCreated,
        DateModified,
        DateSent,
        IsSentSMS,
        CustomItem,
        old_return_patient_id,
        Appointment_ID_FilingStatus,
        Appointment_CancellationRemarks,
        tempID)
SELECT ISNULL(mainPatient_SOAP.ID, mainPatient_SOAP2.ID) Main_ID_Patient_SOAP,
       tempPatient_SOAP_Plan.DateReturn,
       ISNULL(mainItem.ID, mainItem2.ID)                 Main_ID_Item,
       tempPatient_SOAP_Plan.Comment,
       tempPatient_SOAP_Plan.DateCreated,
       tempPatient_SOAP_Plan.DateModified,
       tempPatient_SOAP_Plan.DateSent,
       tempPatient_SOAP_Plan.IsSentSMS,
       tempPatient_SOAP_Plan.CustomItem,
       tempPatient_SOAP_Plan.old_return_patient_id,
       tempPatient_SOAP_Plan.Appointment_ID_FilingStatus,
       tempPatient_SOAP_Plan.Appointment_CancellationRemarks,
       tempPatient_SOAP_Plan.Temp_ID_Patient_SOAP_Plan
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Plan] tempPatient_SOAP_Plan
       inner join @NotYetInserted_Temp_ID_Patient_SOAP_Plan unInsertedTempPatient_SOAP_Plan
               on tempPatient_SOAP_Plan.Temp_ID_Patient_SOAP_Plan = unInsertedTempPatient_SOAP_Plan.Temp_ID_Patient_SOAP_Plan
       ------------ SOAP ----------------
       LEFT JOIN tPatient_SOAP mainPatient_SOAP
              on mainPatient_SOAP.tempID = 'Temp-2022-09-14-'
                                           + tempPatient_SOAP_Plan.Main_Guid_Company
                                           + '-Patient_SOAP-'
                                           + Convert(Varchar(MAX), tempPatient_SOAP_Plan.ID_Patient_SOAP)
                 and mainPatient_SOAP.ID_Company = tempPatient_SOAP_Plan.Main_ID_Company
       LEFT JOIN tPatient_SOAP mainPatient_SOAP2
              on mainPatient_SOAP2.ID = tempPatient_SOAP_Plan.ID_Patient_SOAP
                 and mainPatient_SOAP2.ID_Company = tempPatient_SOAP_Plan.Main_ID_Company
       ------------ SOAP END --------------------------
       /*                                  */
       ------------ Item ----------------
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-09-14-'
                                   + tempPatient_SOAP_Plan.Main_Guid_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempPatient_SOAP_Plan.ID_Item)
                 and mainItem.ID_Company = tempPatient_SOAP_Plan.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempPatient_SOAP_Plan.ID_Item
                 and mainItem2.ID_Company = tempPatient_SOAP_Plan.Main_ID_Company
------------ Item END ----------------
/*                                  */
WHERE  1 = 1 
 