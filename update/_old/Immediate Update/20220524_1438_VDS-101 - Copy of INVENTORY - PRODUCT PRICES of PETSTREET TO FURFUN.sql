DECLARE @PetStreetVeterinaryClinicAndGroomingSalon_Server2_GUID_Company VARCHAR(MAX) = 'B57AFCDA-3B2B-427B-A576-850E3503D418'
DECLARE @FurfunVeterinaryClinic_Server4_GUID_Company VARCHAR(MAX) = '508D2EA7-8553-41C8-BE44-320C4F503B86'

IF(SELECT Count(*)
   FROM   db_vetcloudv3_server2.dbo.tCompany
   WHERE  Guid = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

IF(SELECT Count(*)
   FROM   db_vetcloudv3_server4.dbo.tCompany
   WHERE  Guid = @FurfunVeterinaryClinic_Server4_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT 'Server 2',
       *
FROM   db_vetcloudv3_server2.dbo.tCompany
WHERE  Guid = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_GUID_Company

SELECT 'Server 4',
       *
FROM   db_vetcloudv3_server4.dbo.tCompany
WHERE  Guid = @FurfunVeterinaryClinic_Server4_GUID_Company

----------------------------------------------------------------
DECLARE @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company INT
DECLARE @FurfunVeterinaryClinic_Server4_ID_Company INT

SELECT @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company = ID
FROM   db_vetcloudv3_server2.dbo.tCompany
WHERE  Guid = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_GUID_Company

SELECT @FurfunVeterinaryClinic_Server4_ID_Company = ID
FROM   db_vetcloudv3_server4.dbo.tCompany
WHERE  Guid = @FurfunVeterinaryClinic_Server4_GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex  INT NOT NULL IDENTITY PRIMARY KEY,
     ID_Item   INT,
     Name_Item varchar(500),
     UnitCost  Decimal(18, 2),
     UnitPrice Decimal(18, 2),
     Comment   varchar(500)
  )

Update db_vetcloudv3_server2.dbo.tItem
SET    Name = dbo.fGetCleanedString(Name)
where  ID_Company = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company

Update db_vetcloudv3_server4.dbo.tItem
SET    Name = dbo.fGetCleanedString(Name)
where  ID_Company = @FurfunVeterinaryClinic_Server4_ID_Company

DECLARE @Name_Items TABLE
  (
     Name_Item VARCHAR(MAX)
  )

INSERT @Name_Items
SELECT Name
FROM   db_vetcloudv3_server2.dbo.tItem
where  ID_ItemType = 2
       and ID_Company = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company
       AND IsActive = 1
Except
SELECT Name
FROM   db_vetcloudv3_server4.dbo.tItem
where  ID_ItemType = 2
       and ID_Company = @FurfunVeterinaryClinic_Server4_ID_Company
       AND IsActive = 1

INSERT INTO db_vetcloudv3_server4.dbo.[tItem]
            ([Code],
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [MinInventoryCount],
             [MaxInventoryCount],
             [UnitCost],
             [UnitPrice],
             [Old_item_id],
             [Old_procedure_id],
             [OtherInfo_DateExpiration],
             [ID_InventoryStatus],
             [BarCode],
             [CustomCode],
             [_tempSupplier],
             [tempID])
SELECT NULL,
       [Name],
       [IsActive],
       @FurfunVeterinaryClinic_Server4_ID_Company,
       [Comment],
       GETDATE(),
		GETDATE(),
       1,
       1,
       [ID_ItemType],
       [ID_ItemCategory],
       [MinInventoryCount],
       [MaxInventoryCount],
       [UnitCost],
       [UnitPrice],
       [Old_item_id],
       [Old_procedure_id],
       [OtherInfo_DateExpiration],
       [ID_InventoryStatus],
       [BarCode],
       [CustomCode],
       [_tempSupplier],
       [tempID]
FROM   db_vetcloudv3_server2.dbo.tItem item  inner join @Name_Items itemName on item.Name = itemName.Name_Item
where  ID_ItemType = 2
       and ID_Company = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company
       AND IsActive = 1

GO

exec pUpdateItemCurrentInventory

GO
--SELECT * FROm   db_vetcloudv3_server2.dbo.vUser WHERE ID_Company = @PetStreetVeterinaryClinicAndGroomingSalon_Server2_ID_Company
--SELECT * FROm   db_vetcloudv3_server4.dbo.vUser WHERE ID_Company = @FurfunVeterinaryClinic_Server4_GUID_Company
