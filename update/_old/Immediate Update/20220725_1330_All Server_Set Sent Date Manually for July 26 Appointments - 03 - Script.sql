
GO
IF OBJECT_ID('tempdb..#SMSRefereceIDs') IS NOT NULL
  DROP TABLE #SMSRefereceIDs
GO
IF OBJECT_ID('tempdb..#DateSentInfo') IS NOT NULL
  DROP TABLE #DateSentInfo
GO
----
----
----
----
----
----
----
SELECT CONVERT(DateTime, '2022-07-12 22:15:00') DateSent
INTO #DateSentInfo

select distinct source.*,
                REPLACE(result.[created_at], 'T', ' ')                                                                    DateCreateddOffset,
                REPLACE(result.updated_at, 'T', ' ')                                                                      DateSentOffset,
                Convert(DATETIME, SWITCHOFFSET(cast(REPLACE(result.[created_at], 'T', ' ') as datetimeoffset), '+08:00')) DateCreatedSMSGateway,
                Convert(DATETIME, SWITCHOFFSET(cast(REPLACE(result.updated_at, 'T', ' ') as datetimeoffset), '+08:00'))   DateSentSMSGateway
INTo   #SMSRefereceIDs
FROm   ForImport.dbo.[Retrieve_Sent_SMS-1658721330477.json] result
       INNER JOIN ForImport.[dbo].[For Appointment 2022-07-25_Unsent-Send by Manual Need now] source
               on result.phone_number = source.ContactNumber_Client
                  and result.message = source.Message
       inner join vCompanyActive company
               on source.Name_Company = company.Name
WHERE  ( [ContactNumber_Client] like '9%'
         and Len(ContactNumber_Client) = 10 )
        OR ( [ContactNumber_Client] like '09%'
             and Len(ContactNumber_Client) = 11 )
        OR ( [ContactNumber_Client] like '63%'
             and Len(ContactNumber_Client) = 12 )
           AND status = 'sent' 




SELECT *
FROM   #DateSentInfo

SELECT *
FROM   #SMSRefereceIDs

GO

ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT, @DateSent DATETIME)
AS
  BEGIN
	
	DECLARE @CustomDateSent DATETIME 

	--SELECT @CustomDateSent = DateSent FROM  #DateSentInfo
	set @CustomDateSent = @DateSent

      /*            
       iTextMo Status            
                 
       "1" = Invalid Number.            
       "2" = Number prefix not supported. Please contact us so we can add.            
       "3" = Invalid ApiCode.            
       "4" = Maximum Message per day reached. This will be reset every 12MN.            
       "5" = Maximum allowed characters for message reached.            
       "6" = System OFFLINE.            
       "7" = Expired ApiCode.            
       "8" = iTexMo Error. Please try again later.            
       "9" = Invalid Function Parameters.            
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.            
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.            
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.            
       "13" = Invalid or Not Registered Custom Sender ID.            
       "14" = Invalid preferred server number.            
       "15" = IP Filtering enabled - Invalid IP.            
       "16" = Authentication error. Contact support at support@itexmo.com            
       "17" = Telco Error. Contact Support support@itexmo.com            
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com            
       "19" = Account suspended. Contact Support support@itexmo.com            
       "0" = Success! Message is now on queue and will be sent soon           
      "-1" = Reach VetCloud SMS Count Limit           
      "20" = Manual Sent SMS        
            
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1;

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = @CustomDateSent
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
                         AND ISNULL(IsSentSMS, 0) = 0
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         @CustomDateSent,
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @CustomDateSent
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
                         AND ISNULL(IsSentSMS, 0) = 0
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         @CustomDateSent,
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @CustomDateSent
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
                         AND ISNULL(IsSentSMS, 0) = 0
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         @CustomDateSent,
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
  --SELECT '_'
  --SELECT @Success Success;
  END

GO

DECLARE @ID_Reference INT = 0
DECLARE @ID_Model VARCHAR(MAX) = ''
DECLARE  @dateSent Datetime
DECLARE db_cursor CURSOR FOR
  SELECT ID_Reference,
         Oid_Model, DateSentSMSGateway
  FROM   #SMSRefereceIDs

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_Reference, @ID_Model, @dateSent

WHILE @@FETCH_STATUS = 0
  BEGIN
      exec pNoteSOAPPlanAsSend
        @ID_Reference,
        @ID_Model,
        20, @dateSent

      FETCH NEXT FROM db_cursor INTO @ID_Reference, @ID_Model, @dateSent
  END

CLOSE db_cursor

DEALLOCATE db_cursor

GO

GO

ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT)
AS
  BEGIN
      /*            
       iTextMo Status            
                 
       "1" = Invalid Number.            
       "2" = Number prefix not supported. Please contact us so we can add.            
       "3" = Invalid ApiCode.            
       "4" = Maximum Message per day reached. This will be reset every 12MN.            
       "5" = Maximum allowed characters for message reached.            
       "6" = System OFFLINE.            
       "7" = Expired ApiCode.            
       "8" = iTexMo Error. Please try again later.            
       "9" = Invalid Function Parameters.            
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.            
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.            
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.            
       "13" = Invalid or Not Registered Custom Sender ID.            
       "14" = Invalid preferred server number.            
       "15" = IP Filtering enabled - Invalid IP.            
       "16" = Authentication error. Contact support at support@itexmo.com            
       "17" = Telco Error. Contact Support support@itexmo.com            
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com            
       "19" = Account suspended. Contact Support support@itexmo.com            
       "0" = Success! Message is now on queue and will be sent soon           
      "-1" = Reach VetCloud SMS Count Limit           
      "20" = Manual Sent SMS        
            
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1;

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = GETDATE()
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule])
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO 

IF OBJECT_ID('tempdb..#DateSentInfo') IS NOT NULL
  DROP TABLE #DateSentInfo
GO

IF OBJECT_ID('tempdb..#SMSRefereceIDs') IS NOT NULL
  DROP TABLE #SMSRefereceIDs

GO

