DECLARE @GUID_Company VARCHAR(MAX) = '77F3B4CF-6521-4CCF-AA71-2A1FC83EFF81'
DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany--Active
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_UserSession INT = 0
Declare @adjustInventory typReceiveInventory

SELECT @ID_UserSession = MAX(_session.ID)
FROM   tUserSession _session
       INNER JOIN vUser _user
               on _session.ID_User = _user.ID
where  _user.ID_Company = 1

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory',
       ID,
       20,
       UnitPrice,
       OtherInfo_DateExpiration,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FROM   titem
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @ID_ItemType_Inventoriable
       AND IsActive = 1

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT TOP 2000 *
FRom   tInventoryTrail
Order  by ID DESC 
