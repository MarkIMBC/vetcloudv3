DECLARE @GUID_Company VARCHAR(MAX) = '5BC70825-90E0-4FB6-8AB3-10BC424E48FC'
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

-------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
  -- AND IsActive = 1
  ) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @import Table
  (
     ID_Client            INT,
     Name_Client          VARCHAR(MAX),
     Address_Client       VARCHAR(MAX),
     City_Client          VARCHAR(MAX),
     Email_Client         VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     GUID_Client          VARCHAR(MAX),
     ID_Patient           INT,
     Name_Patient         VARCHAR(MAX),
     Specie_Patient       VARCHAR(MAX),
     ID_Gender            VARCHAR(MAX),
     DateBirth_Patient    DateTime,
     GUID                 VARCHAR(MAX),
     Breed_Import         VARCHAR(MAX),
     Gender_Import        VARCHAR(MAX),
     DateBirth_Import     VARCHAR(MAX)
  )

INSERT @import
       (Name_Client,
        Address_Client,
        City_Client,
        Email_Client,
        ContactNumber_Client,
        Name_Patient,
        Specie_Patient,
        Breed_Import,
        Gender_Import,
        DateBirth_Import,
        GUID)
SELECT dbo.fGetCleanedString([CLIENT NAME]),
       dbo.fGetCleanedString([ADDRESS]),
       dbo.fGetCleanedString([Column1]),
       dbo.fGetCleanedString([EMAIL]),
       dbo.fGetCleanedString([CONTACT NUMBER]),
       dbo.fGetCleanedString([PATIENT NAME]),
       dbo.fGetCleanedString([SPECIES]),
       dbo.fGetCleanedString([BREED]),
       dbo.fGetCleanedString([GENDER]),
       dbo.fGetCleanedString([DATE OF BIRTH]),
       GUID
FROM   ForImport.[dbo].[Client-Patient-Template (2)]

Update @import
SET    Name_Patient = '(No Name)'
WHERE  LEN(ISNULL(Name_Patient, '')) = 0
        OR Name_Patient = '-'

Update @import
SET    Name_Client = RTRIM(LTRIM(REPLACE(Name_Client, '  ', ' ')))
WHERE  Name_Client = '-'

Update @import
SET    Address_Client = Address_Client + ', ' + City_Client
WHERE  LEN(Address_Client) > 0

Update @import
SET    Specie_Patient = ''
WHERE  Specie_Patient = '-'

Update @import
SET    Breed_Import = ''
WHERE  Breed_Import = '-'

Update @import
SET    Specie_Patient = Specie_Patient + ' - ' + Breed_Import
WHERE  LEN(ISNULL(Breed_Import, '')) > 0

Update @import
SET    ID_Gender = 1
WHERE  Gender_Import = 'Male'

Update @import
SET    ID_Gender = 2
WHERE  Gender_Import = 'Female'

Update @import
SET    DateBirth_Patient = TRY_CONVERT(datetime, DateBirth_Import)

Update @import
SET    DateBirth_Patient = NULL
WHERE  DateBirth_Patient = '1900-01-01 00:00:00.000'

Update @import
Set    GUID_Client = tbl.GUID
FROM   @import import
       inner join (SELECT Name_Client,
                          MAX(GUID) + '-Client' GUID
                   FROM   @import
                   GROUP  BY Name_Client) tbl
               on import.Name_Client = tbl.Name_Client

Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.GUID_Client = client.tempID
WHERE  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.GUID = patient.tempID
WHERE  ID_Company = @ID_Company
       and IsActive = 1

INSERT INTO [dbo].tClient
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Client,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID_Client
FROM   @import import
WHERE  ID_Client IS NULL
       and LEN(Name_Client) > 0

INSERT INTO [dbo].tPatient
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Patient,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @import import
WHERE  ID_Patient IS NULL
       and LEN(Name_Patient) > 0

Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.GUID_Client = client.tempID
WHERE  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.GUID = patient.tempID
WHERE  ID_Company = @ID_Company
       and IsActive = 1

Update tClient
SET    Name = import.Name_Client,
       Address = import.Address_Client,
       ContactNumber = import.ContactNumber_Client
FROM   tClient client
       inner join @import import
               on client.ID = import.ID_Client

Update tPatient
SET    Name = import.Name_Patient,
       Species = import.Specie_Patient,
       DateBirth = import.DateBirth_Patient,
       ID_Gender = import.ID_Gender, ID_Client = import.ID_Client
FROM   tPatient patient
       inner join @import import
               on patient.ID = import.ID_Patient

SELECT *
FROM   @import
order  by Name_Client,
          Name_Patient 
