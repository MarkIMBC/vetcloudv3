DEclare @GUID_Company VARCHAR(MAX) = '5CDC807F-B62D-4871-BCF7-7BC063D9BD35'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     Option_Name_Item  VARCHAR(MAX),
     Name_ItemCategory VARCHAR(MAX),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2),
     tempID            VARCHAR(MAX)
  )

INSERT @import
       (Name_Item,
        Option_Name_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,
        tempID)
SELECT dbo.fGetCleanedString([ITEM]),
       dbo.fGetCleanedString([Option]),
       dbo.fGetCleanedString([Category]),
       TRY_CONVERT(Decimal(18, 2), [Cost]),
       TRY_CONVERT(Decimal(18, 2), Price),
       [GUID]
FROM   ForImport.[dbo].[wags and purs updt]
where  type = 'GOODS'

Update @import
set    Name_Item = Name_Item + ' - ' + Option_Name_Item
where  LEN(ISNULL(Option_Name_Item, 0)) > 0


Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join tItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType


SELECT Distinct Name_ItemCategory
FROM   @import
Order  by Name_ItemCategory

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       @Inventoriable_ID_ItemType,
       cat.ID
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join tItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType


Update tItem
set    UnitCost = import.UnitCost,
       UnitPrice = import.UnitPrice, IsActive = 1
FROM   tItem item
       inner join @import import
               on item.ID = import.ID_Item
WHERE  item.ID_Company = @ID_Company

SELECT *
FROM   @import 
