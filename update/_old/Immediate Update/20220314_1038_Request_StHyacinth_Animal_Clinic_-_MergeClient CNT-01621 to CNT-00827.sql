DECLARE @GUID_Company VARCHAR(MAX) = 'CE106252-85B7-4348-8441-517560A07A7C'
DECLARE @SourceID_Client INT =286075
DECLARE @DestinationID_Client INT =255190
DECLARE @ID_Patient_1 INT =371038
DECLARE @ID_Patient_2 INT =405787

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT *
FROM   tClient
WHERE  ID = @SourceID_Client

SELECT *
FROM   tPatient
WHERE  ID_Client = @SourceID_Client

SELECT *
FROM   tClient
WHERE  ID = @DestinationID_Client

SELECT *
FROM   tPatient
WHERE  ID_Client = @DestinationID_Client


SELECT ID,
       Code,
	   ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vPatient_SOAP
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT ID,
       Code,
	   ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vVeterinaryHealthCertificate
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   hed.ID_Client,
       hed.Name_Client,
       biPatient.Name_Patient,
       biPatient.ID_Patient
FROM   vBillingInvoice hed
       inner join vBillingInvoice_Patient biPatient
               on hed.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   hed.ID_Client,
       hed.Name_Client,
       conPatient.Name_Patient,
       conPatient.ID_Patient
FROM   vPatient_Confinement hed
       inner join vPatient_Confinement_Patient conPatient
               on hed.ID = conPatient.ID_Patient_Confinement
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   ID_Client,
       hed.Name_Client,
       hed.Name_Patient,
       hed.ID_Patient
FROM   vPatient_Wellness hed
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company

SELECT ID,
       Code,
       ID_Client,
       Name_Client,
       Name_Patient
FROM   vPatient_Wellness
WHERE  ID_Client = @SourceID_Client

SELECT ID,
       Code,
       ID_Client,
       Name_Client
FROM   vBillingInvoice
WHERE  ID_Client = @SourceID_Client

Update tPatient
SET    ID_Client = @DestinationID_Client
WHERE  ID = @ID_Patient_1
       AND ID_Company = @ID_Company
       AND ID_Client = @SourceID_Client

Update tPatient
SET    ID_Client = @DestinationID_Client
WHERE  ID = @ID_Patient_2
       AND ID_Company = @ID_Company
       AND ID_Client = @SourceID_Client

Update tPatient_SOAP
SET    ID_Client = @DestinationID_Client
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company

Update tPatient_Wellness
SET    ID_Client = @DestinationID_Client
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company

Update tBillingInvoice
SET    ID_Client = @DestinationID_Client
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company

SELECT ID,
       Code,
	   ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vPatient_SOAP
WHERE  ID_Client = @DestinationID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT ID,
       Code,
	   ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vVeterinaryHealthCertificate
WHERE  ID_Client = @DestinationID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   hed.ID_Client,
       hed.Name_Client,
       biPatient.Name_Patient,
       biPatient.ID_Patient
FROM   vBillingInvoice hed
       inner join vBillingInvoice_Patient biPatient
               on hed.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   hed.ID_Client,
       hed.Name_Client,
       conPatient.Name_Patient,
       conPatient.ID_Patient
FROM   vPatient_Confinement hed
       inner join vPatient_Confinement_Patient conPatient
               on hed.ID = conPatient.ID_Patient_Confinement
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
	   ID_Client,
       hed.Name_Client,
       hed.Name_Patient,
       hed.ID_Patient
FROM   vPatient_Wellness hed
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company


