DECLARE @GUID_Company VARCHAR(MAX) = '17C7284C-0CE9-4206-A461-F2E98FF82F2E'
DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')

-----------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item                    INT,
     Code_Item                  varchar(MAX),
     Name_Item                  varchar(MAX),
     ID_ItemCategory            INT,
     Name_ItemCategory          varchar(MAX),
     UnitCost_Item              Decimal(18, 2),
     UnitPrice_Item             Decimal(18, 2),
     CurrentInventoryCount_Item INT,
     BuyingPrice_Import         varchar(MAX),
     SellingPrice_Import        varchar(MAX),
     Inventory_Import           varchar(MAX),
     GUID                       VARCHAR(MAX),
     RowIndex                   INT
  )

INSERT @import
       (Code_Item,
        Name_Item,
        Name_ItemCategory,
        BuyingPrice_Import,
        SellingPrice_Import,
        Inventory_Import,
        GUID,
        RowIndex)
SELECT dbo.fGetCleanedString([Code]),
       dbo.fGetCleanedString([Name]),
       dbo.fGetCleanedString([Name_ItemCategory]),
       dbo.fGetCleanedString([BuyingPrice]),
       dbo.fGetCleanedString([SellingPrice]),
       dbo.fGetCleanedString([Inventory]),
       [GUID],
       RowIndex
FROM   ForImport.[dbo].[20220120_1401_Ric-Ric_Animal_Clinic_Items]

------------------- UpdaTE ID_Item by Name and Code ------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType_Inventoriable

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Code_Item = item.Code
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType_Inventoriable

exec pInsertItemCategory
  'Eye & Ear Drops',
  2

------------------------------------------------------------
Update @import
set    ID_ItemCategory = cat.ID
FROM   @import import
       inner join (SELECT Max(ID)                     ID,
                          dbo.fGetCleanedString(Name) Name
                   FROM   vActiveItemCategory
                   where  ID_ItemType = @ID_ItemType_Inventoriable
                   GROUP  BY Name) cat
               on import.Name_ItemCategory = cat.Name

Update @import
SET    BuyingPrice_Import = 0
WHERE  LEN(BuyingPrice_Import) = 0
SELECT *  FROM @import
WHERE  ISNUMERIC(BuyingPrice_Import) = 0
        OR ISNUMERIC(SellingPrice_Import) = 0
        OR ISNUMERIC(Inventory_Import) = 0


DELETE FROM @import
WHERE  ISNUMERIC(BuyingPrice_Import) = 0
        OR ISNUMERIC(SellingPrice_Import) = 0
        OR ISNUMERIC(Inventory_Import) = 0

Update @import
SET    CurrentInventoryCount_Item = CONVERT(INT, Inventory_Import)

Update @import
SET    UnitCost_Item = CONVERT(decimal(18, 2), BuyingPrice_Import)

Update @import
SET    UnitPrice_Item = CONVERT(decimal(18, 2), SellingPrice_Import)

Update @import
SET    CurrentInventoryCount_Item = CONVERT(INT, Inventory_Import)

SELECT *
FROM   @import
