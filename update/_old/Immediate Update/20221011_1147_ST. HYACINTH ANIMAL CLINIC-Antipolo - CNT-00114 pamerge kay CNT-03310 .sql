DEclare @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_Code_Client VARCHAR(MAX) = 'CNT-00114'
DECLARE @Destination_Code_Client VARCHAR(MAX) = 'CNT-03310'
DECLARE @Source1_ID_Client INT
DECLARE @Destination_ID_Client INT
DECLARE @Source1_Name_Client VARCHAR(MAX) = ''
DECLARE @Destination_Name_Client VARCHAR(MAX) = ''
DECLARE @Comment VARCHAR(MAX) = ''

SELECT @Source1_ID_Client = ID,
       @Source1_Name_Client = Name
FROm   vClient
WHERE  Code = @Source1_Code_Client
       AND ID_Company = @ID_Company

SELECT @Destination_ID_Client = ID,
       @Destination_Name_Client = Name
FROm   vClient
WHERE  Code = @Destination_Code_Client
       AND ID_Company = @ID_Company

SET @Comment = '' + @Name_Company + ' - Merge Client - '
               + @Destination_Name_Client + ' from '
               + @Source1_Code_Client + ' to '
               + @Destination_Code_Client

SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  @Comment

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company 
