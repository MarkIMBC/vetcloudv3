DECLARE @GUID_Company VARCHAR(MAX) = 'DF524B40-F185-43CF-B2E8-49FD1AB21FA0'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.    Pls. contact /*CompanyName*/ /*ContactNumber*/'
DECLARE @SMSTemplate VARCHAR(MAX) = ''
  ---
  + 'Client Name: /*Client*/ /*br*/'
  + 'Pet  Name: /*Pet*/ /*br*/'
  + 'Service/Remarks: /*Service*/ /*br*/'
  + 'Date Return: /*DateReturn*/ /*br*//*br*/'
  + 'MAIN STA MARIA:/*br*/'
  + 'MONDAYS CLINIC ONLY 9AM-5PM/*br*/'
  + 'TUES to SAT 8AM-5PM GROOM / 9AM-5PM CLINIC /*br*/'
  + 'SUNDAYS GROOM ONLY/*br*/'
  + '09178540246/*br*/' + '/*br*/'
  + 'BOCAUE BRANCH:/*br*/'
  + 'MONDAYS CLOSED/*br*/'
  + 'TUES to SUN/*br*/'
  + '8AM-5PM GROOMING/CLINIC/*br*/'
  + '09175235883/*br*//*br*/'
  + '/*CompanyName*/ EST. 1999'

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
