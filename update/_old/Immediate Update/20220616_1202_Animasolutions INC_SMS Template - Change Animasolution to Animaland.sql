DEclare @GUID_Company VARCHAR(MAX) = 'E1682B9E-22CB-4522-BD4E-2B995FFFD552'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT ID,
       Name,
       SOAPPlanSMSMessage,
       REPLACE(SOAPPlanSMSMessage, CHAR(13), '+ ''CHAR(13)'' +')
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.    Pls. contact Animaland /*ContactNumber*/'
WHERE  ID = @ID_Company

select Name_Company,
       Name_Client,
       Message
FROM   dbo.fGetSendSoapPlan(GETDATE(), NULL, @ID_Company) 
