DECLARE @GUID_Company VARCHAR(MAX) = 'C2BB5272-3DF0-4869-A1A3-A34CBF5C8808'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tBillingInvoice
SET    Date = '2022-04-03 00:00:00'
where  Code = 'BI-00656'
       and ID_Company = @ID_Company

Update tBillingInvoice
SET    Date = '2022-04-03 00:00:00'
where  Code = 'BI-00659'
       and ID_Company = @ID_Company

Update vPaymentTransaction
SET    Date = '2022-04-03 00:00:00'
where  Code = 'PT-00637'
       and ID_Company = @ID_Company
       AND Code_BillingInvoice = 'BI-00656'

Update vPaymentTransaction
SET    Date = '2022-04-03 00:00:00'
where  Code = 'PT-00640'
       and ID_Company = @ID_Company
       AND Code_BillingInvoice = 'BI-00659'
