DECLARE @GUID_Company VARCHAR(MAX) = '81B8232D-73F4-4DDE-8053-C0C1E3B87C16'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.    
Pls. contact /*CompanyName*/ /*ContactNumber*/'
------
DECLARE @SMSTemplate VARCHAR(MAX) = ''
  + 'Good day po /*Client*//*br*/, please be reminded of '
  + 'veterinary schedule of /*Pet*/ for /*Service*/ '
  + 'on /*DateReturn*/ at /*CompanyName*/./*br*//*br*/ This '
  + 'is an automated SMS reminder PLEASE DO NOT REPLY '
  + 'or CALL HERE. Kindly contact us at /*ContactNumber*/ '
  + 'should you have concern. Thank You.'

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

--
SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
