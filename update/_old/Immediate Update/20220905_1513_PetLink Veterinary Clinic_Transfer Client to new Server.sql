IF OBJECT_ID(N'Temp-2022-09-05-Client', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-05-Client];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-09-05-' + company.Guid + '-Client-'
       + Convert(Varchar(MAX), client.ID)            tempID,
       client.Name,
       client.ContactNumber,
       client.ContactNumber2,
       client.Email,
       client.Address,
       CASE
         WHEN LEN(ISNULL(client.Comment, '')) > 0 THEN CHAR(13)
         ELSE ''
       END
       + 'Imported from Old Server' Comment,
       ID_Company,
       client.DateCreated                            DateCreated_Client
INTO   [dbo].[Temp-2022-09-05-Client]
FROM   _____db_vetcloudv3_oldserver_20220905105555.[dbo].tClient client
       inner join _____db_vetcloudv3_oldserver_20220905105555.[dbo].tCompany company
               on client.ID_Company = company.ID
where  client.ID > 460693 AND company.GUID = 'D4D74C3B-BF59-43B7-8F32-7E93D16B196A'

INSERT @NotYetInserted_TempID
SELECT tempClient.tempID
FROm   [dbo].[Temp-2022-09-05-Client] tempClient

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tclient)

INSERT dbo.tClient
       (ID_Company,
        tempID,
        Name,
        ContactNumber,
        ContactNumber2,
        Address,
        Email,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempClient.ID_Company,
       tempClient.tempID,
       tempClient.Name,
       tempClient.ContactNumber,
       tempClient.ContactNumber2,
       tempClient.Address,
       tempClient.Email,
       Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [dbo].[Temp-2022-09-05-Client] tempClient
       inner join @NotYetInserted_TempID unInsertedTempCLient
               on tempClient.tempID = unInsertedTempCLient.tempID

SELECT *
FROm   [Temp-2022-09-05-Client] 
