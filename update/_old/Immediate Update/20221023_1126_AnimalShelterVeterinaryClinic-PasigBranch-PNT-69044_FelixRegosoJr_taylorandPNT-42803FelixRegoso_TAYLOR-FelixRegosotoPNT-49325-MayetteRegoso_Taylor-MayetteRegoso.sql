/* Transfer PNT-69044 - Felix Regoso Jr. / taylor */
exec pTransferPetToNewClientByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-69044',
  'CNT-24543',
  'CNT-29140' 

/* PNT-42803  - Felix Regoso / TAYLOR - Felix Regoso */
exec pTransferPetToNewClientByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-42803',
  'CNT-06297',
  'CNT-29140' 

/*Transfer PNT-69044 - Felix Regoso Jr. / taylor to Mayette Regoso (Taylor - Mayette Regoso)*/
exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-42803',
  'PNT-49325'

/* PNT-42803  - Felix Regoso (TAYLOR - Felix Regoso) to / taylor to Mayette Regoso (Taylor - Mayette Regoso)*/
exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-69044',
  'PNT-49325'
 
/*Update SOAP CLient by Patient*/
UPDATE tPatient_SOAP
SET    ID_Client = _soap.ID_Client
FRom   tPatient_SOAP _soap
       inner join vCompanyActive c
               on _soap.ID_Company = c.ID
       inner join vPatient patient
               on _soap.ID_Patient = patient.id
WHERE  Guid = '36B26761-B361-4912-87C4-1387C432B407'
       and patient.Code = 'PNT-49325'
