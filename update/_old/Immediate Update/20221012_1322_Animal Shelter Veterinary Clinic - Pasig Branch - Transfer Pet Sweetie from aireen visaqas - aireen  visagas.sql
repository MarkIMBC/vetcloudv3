GO

DECLARE @GUID_Company VARCHAR(MAX) = '36B26761-B361-4912-87C4-1387C432B407'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @ID_Patient INT = 578551
DECLARE @Source_ID_Client INT = 424079
DECLARE @Destination_ID_Client INT = 455133

SELECT Name,
       Name_Client
FROm   vPatient
where  ID = @ID_Patient
       and ID_Company = @ID_Company

SELECT ID,
       Code,
       Name,
       'Source 1',
       IsActive
FROm   vClient
WHERE  ID = @Source_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination',
       IsActive
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

Update tPatient
SET    ID_Client = @Destination_ID_Client
WHERe  ID = @ID_Patient
       AND ID_Company = @ID_Company
       and ID_Client = @Source_ID_Client

SELECT Name,
       Name_Client
FROm   vPatient
where  ID = @ID_Patient
       and ID_Company = @ID_Company

GO

CREATE OR
ALTER PROC pMergePatientRecordByCompany(@GUID_Company             VARCHAR(MAX),
                                        @Source_Code_Patient      VARCHAR(MAX),
                                        @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   vCompanyActive
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------
      DECLARE @Source1_ID_Patient INT
      DECLARE @Destination_ID_Patient INT
      DECLARE @Source1_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Patient = ID,
             @Source1_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Source_Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Patient = ID,
             @Destination_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Destination_Code_Patient
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Patient - '
                     + @Destination_Name_Patient + ' from '
                     + @Source_Code_Patient + ' to '
                     + @Destination_Code_Patient

      SELECT ID,
             Code,
             Name,
             'Source 1',
             IsActive
      FROm   vPatient
      WHERE  ID = @Source1_ID_Patient
             AND ID_Company = @ID_Company
      Union ALL
      SELECT ID,
             Code,
             Name,
             'Destination',
             IsActive
      FROm   vPatient
      WHERE  ID = @Destination_ID_Patient
             AND ID_Company = @ID_Company

      exec pMergePatientRecord
        @Source1_ID_Patient,
        @Destination_ID_Patient,
        @Comment

      Update tPatient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Patient
             and ID_Company = @ID_Company
  END

GO

exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-68031',
  'PNT-69121' 
