DECLARE @GUID_Company VARCHAR(MAX) = 'D4D74C3B-BF59-43B7-8F32-7E93D16B196A'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item              INT,
     Name_Item            VARCHAR(MAX),
     Name_Brand           VARCHAR(MAX),
     ID_ItemCategory      VARCHAR(MAX),
     Name_ItemCategory    VARCHAR(MAX),
     Import_CategoryBrand VARCHAR(MAX),
     GUID                 VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        Import_CategoryBrand,
        GUID)
select dbo.fGetCleanedString([Name]),
       [Category-Brand],
       [GUID]
FROM   ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch_Inventory_20221112_0951]
WHERE  LEN(dbo.fGetCleanedString([Name])) > 0

DELETE FROM @forImport
WHERE  Name_Item IN (SELECT Distinct Import_CategoryBrand
                     FROM   @forImport)

Update @forImport
SET    Name_Item = Import_CategoryBrand + ' - ' + Name_Item
where  Import_CategoryBrand NOT IN ( 'VACCINES&TESTKITS', 'PARASITICIDES', 'MEDS', 'SUPPLEMENTS',
                                     'ANYBRAND OF DIAPER FEMALE', 'ANYBRAND OF DIAPER MALE', 'OTHERS' )

Update @forImport
SET    ID_ItemCategory = cat.ID
FROM   @forImport import
       inner join tItemCategory cat
               on import.Import_CategoryBrand = cat.Name
where  cat.ID_ItemType = 2

SELECT Distinct Import_CategoryBrand
FROM   @forImport

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    Name = import.Name_Item,
       ID_ItemCategory = import.ID_ItemCategory
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 
