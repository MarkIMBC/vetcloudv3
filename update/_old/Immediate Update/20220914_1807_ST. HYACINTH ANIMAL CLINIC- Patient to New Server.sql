IF OBJECT_ID(N'AuditTrail-2022-09-14-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[AuditTrail-2022-09-14-Patient];

GO

IF OBJECT_ID(N'Temp-2022-09-14-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-14-Patient];

Declare @NotYetInserted_PatientTempID TABLE
  (
     tempPatientID VARCHAR(MAX)
  )

SELECT ISNULL(mainClient.ID, mainClient2.ID) Main_ID_Client,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient-'
       + Convert(Varchar(MAX), Patient.ID)   tempPatientID,
       Patient.Name,
       Patient.DateBirth,
       Patient.Species,
       Patient.IsNeutered,
       Patient.IsDeceased,
       Patient.AnimalWellness,
       Patient.DateDeceased,
       Patient.DateLastVisited,
       Patient.ProfileImageFile,
       Patient.CustomCode,
       Patient.WaitingStatus_ID_FilingStatus,
       Patient.Comment,
       Patient.ID_Company
INTO   [dbo].[Temp-2022-09-14-Patient]
FROM   _______db_vetcloudv3_oldserver_20220914164457.[dbo].vPatient Patient
       inner join _______db_vetcloudv3_oldserver_20220914164457.[dbo].tCompany company
               on Patient.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-09-14-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient.ID_Client)
                 and mainClient.ID_Company = Patient.ID_Company
       LEFT JOIN tClient mainClient2
              on mainClient2.ID = Patient.ID_Client
                 and mainClient2.ID_Company = Patient.ID_Company
where  Patient.ID > 622693
       AND company.GUID =  '56CD6D30-4877-47AA-9BFC-C407A7A532D3'
ORDER  BY Patient.ID ASC

SELECT *
FROM   [dbo].[Temp-2022-09-14-Patient]

INSERT @NotYetInserted_PatientTempID
SELECT tempPatientID
FROm   [dbo].[Temp-2022-09-14-Patient]

DELETE FROM @NotYetInserted_PatientTempID
WHERE  tempPatientID IN (SELECT tempID
                         FROM   tPatient)

INSERT tPatient
       (ID_Company,
        ID_Client,
        tempID,
        Name,
        DateBirth,
        Species,
        IsNeutered,
        IsDeceased,
        AnimalWellness,
        DateDeceased,
        DateLastVisited,
        ProfileImageFile,
        CustomCode,
        WaitingStatus_ID_FilingStatus,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempPatient.ID_Company,
       tempPatient.Main_ID_Client,
       tempPatient.tempPatientID,
       tempPatient.Name,
       tempPatient.DateBirth,
       tempPatient.Species,
       tempPatient.IsNeutered,
       tempPatient.IsDeceased,
       tempPatient.AnimalWellness,
       tempPatient.DateDeceased,
       tempPatient.DateLastVisited,
       tempPatient.ProfileImageFile,
       tempPatient.CustomCode,
       tempPatient.WaitingStatus_ID_FilingStatus,
       tempPatient.Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [Temp-2022-09-14-Patient] tempPatient
       inner join tClient mainClient
               on mainClient.ID = tempPatient.Main_ID_Client
                  and mainClient.ID_Company = tempPatient.ID_Company
       INNER JOIn @NotYetInserted_PatientTempID unInsertedPatienttempIDs
               on tempPatient.tempPatientID = unInsertedPatienttempIDs.tempPatientID
order  by Name 
