DEclare @GUID_Company VARCHAR(MAX) = '1D4E035E-E736-494E-A1BA-A0AA7695D6F1'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     Name_ItemCategory VARCHAR(MAX),
     UnitPrice         DECIMAL(18, 2),
     tempID            VARCHAR(MAX)
  )

Update tItem
set    Name = dbo.fGetCleanedString(Name)

INSERT @import
       (Name_Item,
        Name_ItemCategory,
        UnitPrice,
        tempID)
SELECT dbo.fGetCleanedString([ITEM]),
       NULL,
       0.00,
       [GUID]
FROM   ForImport.[dbo].[BUENAVIDA-ITEMS]

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType =@Inventoriable_ID_ItemType

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [UnitCost],
             [UnitPrice])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
      @Inventoriable_ID_ItemType,
       cat.ID,
       0,
       hed.[UnitPrice]
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL

SELECT *
FROM   @import

DECLARE @IDs_Item_ForDeleted typIntList

INSERT @IDs_Item_ForDeleted
SELECT MAX(ID)
FROM   vActiveItem
WHERE  ID_Company = @ID_Company
       AND ID_ItemType =@Inventoriable_ID_ItemType
GROUP  BY Name
HAVING COUNT(*) > 1

Update tItem
SET    IsActive = 0
where  ID IN (SELECT ID
              FROM   @IDs_Item_ForDeleted) 
