IF( (SELECT COUNT (Name)
     FROM   tSOAPType
     WHERE  Name = 'Release') < 1 )
  BEGIN
      INSERT INTO [dbo].[tSOAPType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Release',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

IF( (SELECT COUNT (Name)
     FROM   tSOAPType
     WHERE  Name = 'Laboratory') < 1 )
  BEGIN
      INSERT INTO [dbo].[tSOAPType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Laboratory',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END 
