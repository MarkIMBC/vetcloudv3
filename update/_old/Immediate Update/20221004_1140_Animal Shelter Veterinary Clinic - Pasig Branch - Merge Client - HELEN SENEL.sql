DEclare @GUID_Company VARCHAR(MAX) = '36B26761-B361-4912-87C4-1387C432B407'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 401096 -- Helen Jenel
DECLARE @Destination_ID_Client INT = 335275 -- HELEN SENEL
SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'Animal Shelter Veterinary Clinic - Pasig Branch - Merge Client - HELEN SENEL from CNT-36726 to CNT-34723'

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company