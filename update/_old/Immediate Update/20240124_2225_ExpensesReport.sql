GO

CREATE OR
ALTER VIEW vzExpensesReport
as
  SELECT payable.ID,
         payable.Date,
         payableDetail.ID_ExpenseCategory,
         category.Name         Name_ExpenseCategory,
         payableDetail.Name,
         payable.TotalAmount,
         payable.PaidAmount,
         payable.RemaningAmount,
         fs.Name               Payment_Name_FilingStatus,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                 HeaderInfo_Company
  FROM   tPayable payable WITH (NOLOCK)
         INNER JOIN tPayable_Detail payableDetail WITH (NOLOCK)
                 on payable.ID = payableDetail.ID_Payable
         INNER JOIN tFilingStatus fs WITH (NOLOCK)
                 on payable.Payment_ID_FilingStatus = fs.ID
         INNER JOIN tExpenseCategory category WITH (NOLOCK)
                 on payableDetail.ID_ExpenseCategory = category.ID
         INNER JOIN vCompany company WITH (NOLOCK)
                 on payable.ID_Company = company.ID

GO

CREATE OR
ALTER PROC pAddUserRoleReport(@Name_UserRole VARCHAR(MAX),
                              @Name_Report   VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Report VARCHAR(MAX) = '';
      DECLARE @ID_UserRole INT = 0;

      IF(SELECT COUNT(*)
         FROm   tUserRole
         WHERE  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess],
                         [IsAdministrator])
            VALUES      (@Name_UserRole,
                         1,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         0,
                         0)

            exec _pCreateReportView
              @Name_Report,
              1
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      SELECT @ID_Report = Oid
      FROM   _tReport
      where  Name = @Name_Report

      if(select COUNT(*)
         FROM   tUserRole_Reports
         WHERE  ID_UserRole = @ID_UserRole
                AND ID_Report = @ID_Report) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Reports]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Report])
            VALUES      (NULL,
                         1,
                         @ID_UserRole,
                         @ID_Report)
        END

      declare @IDs_UserAdministrator typIntlist

      INSERT @IDs_UserAdministrator
      select ID_User
      FROm   vCompanyActiveUserAdministrator WITH (NOLOCK)
      EXCEPT
      SELECT ID_User
      FROM   tUser_Roles WITH (NOLOCK)
      WHERE  ID_UserRole = @ID_UserRole

      INSERT INTO [dbo].[tUser_Roles]
                  ([IsActive],
                   [ID_User],
                   [ID_UserRole])
      SELECT 1,
             ID,
             @ID_UserRole
      FROM   @IDs_UserAdministrator
  END

GO

DECLARE @Name_UserRole VARCHAR(MAX) = 'Expenses Report Only';
DECLARE @Name_Report VARCHAR(MAX) = 'ExpensesReport';

exec pAddUserRoleReport
  @Name_UserRole,
  @Name_Report

Update _tNavigation
SET    Route = 'ReportExpenses',
       Caption = 'Expenses Report'
WHERE  Name = 'ExpensesReport_Navigation' 
