if OBJECT_ID('ForImport.dbo.[20230314_1401_Pet Wellness HQ_Import Items]') is not null
  BEGIN
      DROP TABLE ForImport.dbo.[20230314_1401_Pet Wellness HQ_Import Items]
  END

GO

CREATE TABLE ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Items]
  (
     [Code]            varchar(5000),
     [Item]            varchar(5000),
     [Inventory Count] varchar(5000),
     [Buying Price]    varchar(5000),
     [RowIndex]        varchar(5000),
     [GUID]            varchar(5000)
  )

GO

INSERT INTO ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Items]
            ([Code],
             [Item],
             [Inventory Count],
             [Buying Price],
             [RowIndex],
             [GUID])
SELECT 'FP201001',
       'DRUG ENVELOPES #3',
       '200',
       '',
       '1',
       '37A9E32F-0D48-4D55-BBC4-5EBC3CBA5613-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201002',
       'DRUG ENVELOPES #4',
       '200',
       '',
       '2',
       '4757E7C0-7D2F-4DBF-98B4-48079B92846D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201003',
       'DRUG ENVELOPES #6',
       '200',
       '',
       '3',
       '96DC1C46-9FC3-495A-B2DD-0C02C4E1EE8A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201004',
       'PAPER BAGS #06',
       '200',
       '',
       '4',
       '3B98E08D-C2EE-4EAC-A231-D19D33CA9F65-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201005',
       'PAPER BAGS #25',
       '200',
       '',
       '5',
       '2256D642-895E-47B3-B437-52E695F7E9C0-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201006',
       'E-COLLAR #1',
       '2',
       '',
       '6',
       '1BC89E6F-3C1C-43C2-94E7-FE3D553A39BF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201007',
       'E-COLLAR #2',
       '2',
       '',
       '7',
       'B4AD5663-A088-4834-A0B8-BC2B908E9E73-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201008',
       'E-COLLAR #3',
       '2',
       '',
       '8',
       'F051E006-ECFC-448B-9239-8D97D5F9403E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201009',
       'E-COLLAR #4',
       '2',
       '',
       '9',
       'F6C06ED4-3D10-447D-8F5A-B1BEA78181E4-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201010',
       'E-COLLAR #5',
       '2',
       '',
       '10',
       'A4FF357B-2016-4B75-BF4A-20A7E9D10687-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201011',
       'E-COLLAR #6',
       '2',
       '',
       '11',
       'CAE3FF31-9C12-43D2-81A9-37B4FA76B2B7-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201012',
       'E-COLLAR #7',
       '2',
       '',
       '12',
       '2B4866F9-0978-4F2B-9058-57D992BC0E43-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201013',
       'PET NURSING KIT, 60ML',
       '3',
       '',
       '13',
       'D134BEBC-B576-4358-8BA4-FCCFBFFCD519-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201014',
       'UNDERPAD',
       '40',
       '',
       '14',
       '280C381C-1BB9-4802-A920-87F170BA13EE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201015',
       'NEXGARD SPECTRA (2-3.5 KG)',
       '15',
       '',
       '15',
       '7FCD5CEF-1F79-481C-85E2-CA699AC40A81-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201016',
       'NEXGARD SPECTRA (3.5-7.5 KG)',
       '15',
       '',
       '16',
       '19D3D4B7-05E8-47C6-AADF-33C1E51C3CC5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201017',
       'NEXGARD SPECTRA (7.5-15 KG)',
       '15',
       '',
       '17',
       '7E30555F-F423-40F4-AF7F-BC3A53F300DF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201018',
       'NEXGARD SPECTRA (15-30 KG)',
       '15',
       '',
       '18',
       '57F17876-4026-4301-8338-A877E71FCA9D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201019',
       'NEXGARD SPECTRA (30-60 KG)',
       '-',
       '',
       '19',
       'EF5E249D-72BB-4ED9-9DCD-E0D90022EF20-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201020',
       'BROADLINE (<2.5KG)',
       '6',
       '',
       '20',
       '9C088509-375D-49E8-9D17-720BA7C089F5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201021',
       'BROADLINE (2.5-7.5KG)',
       '6',
       '',
       '21',
       'D642B770-3BA7-4ADF-A18B-65163DF6CF58-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201022',
       'FRONTLINE SPRAY, 100ML',
       '3',
       '',
       '22',
       'A0723084-ACE4-4609-A3F2-D7B067717682-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201023',
       'FRONTLINE SPRAY, 250ML',
       '3',
       '',
       '23',
       '4B09F1CB-9361-4E08-9E77-EB10BEEA062F-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201024',
       'IMMUNOL SYRUP GREEN',
       '5',
       '',
       '24',
       '9EBE4526-58D0-4DFD-95E6-DB73E37CC6D6-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201025',
       'MEGADERM, 4ML',
       '56',
       '',
       '25',
       '23EEE245-9044-4834-8AD7-CC51842B2271-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201026',
       'MEGADERM, 8ML',
       '56',
       '',
       '26',
       'B98B6368-12E0-46F1-A0F2-EEED54220A68-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201027',
       'COAMOXICLAV ORAL SUSP, 60ML',
       '10',
       '',
       '27',
       '7A6D7CAC-BFA1-4CE4-85FB-93B73CEF467E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201028',
       'CETIRIZINE TAB, 10MG',
       '100',
       '',
       '28',
       '0A96AB80-2BB9-4683-8E1C-0E64B4F995D3-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201029',
       'METOCLOPROMIDE SYRUP, 60 ML',
       '5',
       '',
       '29',
       '4CD1C981-98A7-41BD-ADE3-E98515551F2F-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201030',
       'METRONIDAZOLE SYRUP, 125MG/5ML, 60 ML',
       '10',
       '',
       '30',
       '0C790289-AEBC-4D43-A6B2-62C0E463AC44-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201031',
       'TOBRAMYCIN',
       '5',
       '',
       '31',
       '9246394D-7BF2-4646-AC1F-06B514760713-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201032',
       'ALUMINUM HYDROXIDE, 200/100MG/5ML, 60 ML, SYRUP',
       '5',
       '',
       '32',
       '325457E3-D644-4502-BB0E-AE4B305EC7D4-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201033',
       'LACTULOSE, 3.35MG/ML, 120ML, SYRUP',
       '1',
       '',
       '33',
       '32463847-D816-401B-A331-A6770FC898BA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201034',
       'PREDNISOLONE, 3MG/ML, 60ML, SYRUP',
       '3',
       '',
       '34',
       '4E6AC064-B56B-4850-90AD-38D9657A3F0B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201035',
       'GENTAMICIN EYE DROPS',
       '5',
       '',
       '35',
       '7FDC263F-7BF8-46F5-B7DC-3E43A3E1BC3A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201036',
       'HYPROMELLOSE 0.3% EYE DROPS',
       '5',
       '',
       '36',
       'CCDAC735-BC2E-4A6D-9A73-B3C8E9430300-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201037',
       'MUPIROCIN OINTMENT, 5G',
       '5',
       '',
       '37',
       '219EEBAF-3679-43E1-BB4F-89CA564EF25D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201038',
       'LC DOX TABLET',
       '200',
       '',
       '38',
       'B408BACD-47F1-4CB1-B961-9303EF79F5FE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201039',
       'LC DOX SYRUP, 60ML',
       '5',
       '',
       '39',
       'A73D978A-5DB4-493D-A5F6-DB4C592C147B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201040',
       'OTIDERM',
       '5',
       '',
       '40',
       'FE35D780-4D04-49C7-98EF-77EEF76009D9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201041',
       'KETADINE SHAMPOO',
       '5',
       '',
       '41',
       '3060BD82-FDA6-4AFD-98CC-151DC8074031-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201042',
       'YUM YUM HI-PROTEIN DOG FOOD, 20KG',
       '1',
       '',
       '42',
       '45A7C982-05FF-45C2-91A8-F5AC797DDABF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201043',
       'YUM YUM ADULT DOG FOOD, 20KG',
       '',
       '',
       '43',
       'B2C3FC10-A57C-4398-AADF-49F20DED5678-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'ARATON DOG AD LAMB 15KG KBL 1 BAG',
       '',
       '',
       '44',
       'AD4C585B-135E-4E22-A208-F594A6463575-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201044',
       'ARATON DOG JR LAMB 15KG KBL 1 BAG',
       '',
       '',
       '45',
       'B28F5171-7CF0-4A44-B6DE-1656C11D7974-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201045',
       'ARATON DOG AD SALMON 15KG KBL 1 BAG',
       '1',
       '',
       '46',
       '743B24E9-DC89-4054-9A20-2EADB5193D24-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201046',
       'ARATON DOG AD POULT 15KG KBL 1 BAG',
       '1',
       '',
       '47',
       '0EEE0C0D-EA7A-44B8-9FB9-ED051DD86F38-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201047',
       'ARATON DOG JR ALLBRD 15KG KBL 1 BAG',
       '1',
       '',
       '48',
       '687F3F0D-0A4D-41CF-9325-104C2A2552AF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201048',
       'ARATON CAT AD SALMON 15KG KBL 1 BAG SS',
       '1',
       '',
       '49',
       '252884E4-6B4B-418F-9D87-D3D13E0E3561-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201049',
       'ARATON KITTEN 15KG KBL 1 BAG SS PH',
       '1',
       '',
       '50',
       'B07FF94A-52AB-4496-801D-B7C5AB1504B1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201050',
       'ARATON DOG LAMB 3 KG RESEALABLE PACK',
       '2',
       '',
       '51',
       '8CED5424-8558-423B-AA29-DB125E26540D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201051',
       'ARATON DOG JR LAMB 3 KG RESEALABLE PACK',
       '2',
       '',
       '52',
       '17F8AFB6-7206-4D9C-B339-65985CAF1A37-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201052',
       'ARATON DOG SALMON 3 KG RESEALABLE PACK',
       '2',
       '',
       '53',
       '3733F225-0060-4DBE-85B3-9AF07956900A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201053',
       'ARATON DOG POULTRY 3 KG RESEALABLE PACK',
       '2',
       '',
       '54',
       'D4979B4F-3A32-4925-9935-2B3CCB312C07-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201054',
       'ARATON DOG JUNIOR 3 KG RESEALABLE PACK',
       '2',
       '',
       '55',
       'EC27C981-143A-4D39-A6AA-9614BCF3EBFA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201055',
       'ARATON CAT SALMON 1.5 KG RESEALABLE PACK',
       '2',
       '',
       '56',
       '77DC26E7-F6FF-44C7-BF29-C8E7F84E4D89-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201056',
       'ARATON KITTEN 1.5 KG RESEALABLE PACK',
       '2',
       '',
       '57',
       '2165B160-1BA1-4F72-BC07-19EA6750905B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201057',
       'VET NATURALS SOAP ANTI-PARASITIC 135G BAR',
       '6',
       '',
       '58',
       '49F07651-BCD4-4923-9183-12A6DA968CAB-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201058',
       'VET NATURALS SOAP ANTI-FUNGAL 135G BAR',
       '6',
       '',
       '59',
       '7C61BC4E-051A-4B90-9BE1-0FDD4A6710AD-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201059',
       'VET NATURALS SOAP WOUND MANAGEMENT 135G BAR',
       '6',
       '',
       '60',
       'B66BEB69-4349-4C32-B054-9E0601D9CE2D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201060',
       'BUBBLE BATH SHAMPOO NOURISH AND PROTECT 200ML BOTTLE',
       '6',
       '',
       '61',
       'BBE1BD4E-B291-4CE7-B522-B629B7530AE1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201061',
       'BUBBLE BATH SHAMPOO NOURISH AND PROTECT 500ML BOTTLE',
       '6',
       '',
       '62',
       '8ECD5685-9DD8-4908-9548-7228BEB8CF59-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201062',
       'BUBBLE BATH SHAMPOO NOURISH AND PROTECT 1,000ML BOTTLE',
       '6',
       '',
       '63',
       '8046B6ED-1F57-445C-81BC-05F0FD6AA074-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201063',
       'BUBBLE BATH SHAMPOO NOURISH AND PROTECT 20ML X 24 SACHET',
       '6',
       '',
       '64',
       'F8B28715-02C7-469A-B0C7-015E19E1D71C-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201064',
       'BUBBLE BATH SHAMPOO SOFT AND SHINY 200ML BOTTLE',
       '6',
       '',
       '65',
       'EA9E09DC-88A2-42A2-8677-CB430B005511-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201065',
       'BUBBLE BATH SHAMPOO SOFT AND SHINY 500ML BOTTLE',
       '6',
       '',
       '66',
       '4ADC29F6-02E4-4989-A8E5-2FE18E08E238-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201066',
       'BUBBLE BATH SHAMPOO SOFT AND SHINY 1,000ML BOTTLE',
       '6',
       '',
       '67',
       'C0D895D7-5351-4FD6-846E-101A210286A6-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201067',
       'BUBBLE BATH SHAMPOO SOFT AND SHINY 20ML X 24 SACHETS',
       '6',
       '',
       '68',
       '9A1EBC4A-6784-4EB9-A157-74C59B044205-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201068',
       'PETSURE 60ML BOTTLE',
       '12',
       '',
       '69',
       '1CFF8592-90D5-41EA-86D5-24D2F6E99E5D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201069',
       'PETSURE 120ML BOTTLE',
       '12',
       '',
       '70',
       'CE10293D-2EEE-4F0F-ABC3-3A1564ACCADE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201070',
       'APPEBOOST 60ML BOTTLE',
       '12',
       '',
       '71',
       '0D5A5E7C-55E6-4264-9D3C-8BEA5A84094A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201071',
       'APPEBOOST 120ML BOTTLE',
       '12',
       '',
       '72',
       'F5FCC8B1-F29C-4303-ABB2-0B9FBF192C83-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201072',
       'GROWTH ADVANCE 120ML BOTTLE',
       '12',
       '',
       '73',
       'BA6ACA3B-6B8D-4593-B743-9C1E81DD3A3B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201073',
       'GROWTH ADVANCE 250ML BOTTLE',
       '12',
       '',
       '74',
       'AFEFA4CB-7608-4EB3-8D7C-EC0A2ACF12C3-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201074',
       'CFLEX (CEFALEXIN 250 MG/5 ML) 60ML BOTTLE',
       '-',
       '',
       '75',
       'D3FBF8C3-36EB-46C4-B934-54EC285344F7-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201075',
       'ENTEROVET 500 MG TAB',
       '900',
       '',
       '76',
       '58B7A7E8-2311-4CE0-B9BB-6DF4AD2AA511-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201076',
       'DEXTROLYTE RECOVERY PACK 5GX7 SACHETS',
       '12',
       '',
       '77',
       '69E01D48-6FD0-45B2-A484-6D4E3D34DAEE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201077',
       'DEXTROLYTE LACTA RECOVERY PACK 5GX7SACHETS',
       '6',
       '',
       '78',
       'C86899E4-2530-448B-BBA3-FBF807F66444-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT 'FP201078',
       'DEXTROLYTE BOX 5GX48 SACHETS',
       '6',
       '',
       '79',
       'DE82F248-4731-4060-9599-D7CB96BC78B1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Aeolus complete cordless',
       '1',
       '4500',
       '80',
       'A44FCF91-D4A4-4205-93EB-EC2E84FA3728-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Andis Cool Care Plus Spray',
       '1',
       '550',
       '81',
       '1B79F451-A332-45ED-960A-56F5141F1DD8-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Cyclone table top dryer',
       '1',
       '8000',
       '82',
       '7262FC85-9A9B-4E8B-A481-251F2F80A246-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Dryer stand',
       '1',
       '3500',
       '83',
       '3DB0B4F1-8401-4F93-9B59-8D5A00B4BB51-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'GTS stainless comb-Greyhound style (35-17 small)',
       '1',
       '350',
       '84',
       '05F32744-B3E5-4872-9CA8-DB32D2FD1ED3-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'GTS stainless comb-Greyhound style (42)',
       '1',
       '310',
       '85',
       '006471AF-E74F-417E-8859-DB996A95BEFE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'GTS stainless comb-Greyhound style (Size: 20-37)',
       '1',
       '310',
       '86',
       '5B10E832-F9CF-44D1-B84F-AC6373D17688-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hikato Pro 3 Star 8.0 Curved, scissor',
       '1',
       '1450',
       '87',
       '49D4EDB2-5504-4CE1-9A9C-38B1C3B4CDEA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hikato Pro 3 Star 8.5 straight, scissor',
       '1',
       '1250',
       '88',
       'C8616D18-2831-4A45-A238-CF8EBBD97CFC-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube A5 cordless clipper',
       '2',
       '13500',
       '89',
       '849479F5-518C-4B5A-88A2-0053E65CA825-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #10',
       '4',
       '850',
       '90',
       'E0B60530-886B-40D4-9A63-708B9BF4A9A1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #30',
       '2',
       '850',
       '91',
       '0E8FBCB5-CFA8-43E7-81B6-5EF48ABF6119-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #3F',
       '2',
       '1600',
       '92',
       'CD828C9E-12E6-41FF-B1FF-8B2440528FB8-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #4F',
       '4',
       '1250',
       '93',
       'F867AA58-65B6-4928-AF7F-347F9D73A65C-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #5F',
       '2',
       '1050',
       '94',
       'B5DA2E40-4D73-486F-9F20-4F9956D49F0E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laube Blade #7F',
       '4',
       '1000',
       '95',
       '6DE61403-19A5-413D-B77C-60402E572548-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Monster dryer, 4HP',
       '1',
       '9500',
       '96',
       '651033E5-2806-4423-A2BF-49EB9745CEE9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Muzzle, 004',
       '1',
       '80',
       '97',
       '59781F86-6D68-46D3-ABFA-15684422C911-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Muzzle, S4',
       '1',
       '150',
       '98',
       '43FBE6AB-8F90-4F6A-9F17-7E63541D9A1A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Muzzle, Size 005',
       '1',
       '170',
       '99',
       'CFAFD6E1-2815-46E4-8476-060E89C85510-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Muzzle, Size 006',
       '1',
       '180',
       '100',
       '57F5C13B-84F9-45D9-946A-076055B33F5D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Nail clipper',
       '1',
       '250',
       '101',
       'DF79E732-0AD3-41ED-B936-BDB103E1170B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Premium slicker brush',
       '1',
       '250',
       '102',
       '876D474C-715C-4CEA-9B57-9391152DFF0D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Regular non skid bowl, #24',
       '2',
       '130',
       '103',
       'B2402905-E5DE-4626-9A0A-FAFB6C48E5EB-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Regular non skid bowl, #32',
       '2',
       '155',
       '104',
       '972C4A20-B3FB-45E4-AD0F-1FEB3AE55AED-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Regular non skid bowl, #80',
       '2',
       '70',
       '105',
       'ABEFC36A-ECE1-4617-8CAD-D311B3168DFA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Slicker brush',
       '1',
       '250',
       '106',
       '8A0ABC3E-D63F-44E2-9EE8-D0F6FA0A408F-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tangle away comb',
       '1',
       '250',
       '107',
       '671208AB-792D-46D8-8F2F-CEA2ADC941F5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Thinning scissors, 8" Scissors',
       '2',
       '4300',
       '108',
       '2510FE7B-61AF-4620-B318-238898F4FDDE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Toothpaste',
       '1',
       '150',
       '109',
       '1EA7C35E-A576-4311-8F23-724783ABE0B6-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Towel (Cannon, Turquiose)',
       '4',
       '299.95',
       '110',
       'A256B797-496D-49F8-AD0C-2A862555D3E1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Training leash, large',
       '1',
       '250',
       '111',
       '481A4667-960A-45A9-B0BE-270895765505-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Training leash, medium',
       '1',
       '200',
       '112',
       '8CF341D4-40BE-45CF-9EEC-6D982B057BD9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Bubble Bath - Nourish and Protect, gallon',
       '1',
       '',
       '113',
       '072486FB-C7F1-4553-B8B0-A23CE95D4D2E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Pet Cologne',
       '2',
       '150',
       '114',
       '4916F551-D1D8-4F22-AE83-5332AADB7938-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Ear Powder, 30g',
       '1',
       '380',
       '115',
       '1121DC7A-917F-4371-B8BF-F28AED856CAD-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Funky shampoo',
       '2',
       '230',
       '116',
       '72DC8546-C1DE-40AF-944E-3A60438DAAE0-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Nutrivet ear wash',
       '2',
       '630',
       '117',
       '1DB8FB28-B0D9-49E5-AE21-4698C13A89CE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Perla Bar, white, 380g x4',
       '3',
       '51.65',
       '118',
       '59570F70-0835-4B64-A1EF-5670B399BBDC-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Toothpaste',
       '1',
       '150',
       '119',
       '297D6C62-0A8A-42B4-8CD8-24330E41366B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Pet ear cleaner',
       '3',
       '',
       '120',
       'BB01FA58-AC6E-4A3B-A218-A26FB85D0A8E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Styptic powder, 1.5 oz',
       '1',
       '580',
       '121',
       '13636468-EEBB-40C5-B8AF-92855CB54631-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Emery board',
       '24',
       '6.25',
       '122',
       '4EC0B0E1-36C3-49AE-866E-A33F7D22B639-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Acepromazine',
       '1',
       '780',
       '123',
       '201F6354-3910-4D39-B769-BB559E2493B3-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Atropine, 50 mL',
       '1',
       '780',
       '124',
       '62E7A13E-BA38-4EB1-A82E-5E0A2F741C56-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Xylazine',
       '1',
       '4500',
       '125',
       '2837DD13-729F-46CD-B72D-8E5E0675F794-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       '0.9% Nacl, IV, 1000ml',
       '5',
       '60',
       '126',
       '23C60835-F54A-43B0-BFD4-E14BE4BF5C1B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'D5LRS,IV, 1000ml',
       '6',
       '60',
       '127',
       'B8C939DD-A766-4A40-BF20-D0C632FF0B80-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'D5W,IV, 1000ml',
       '6',
       '60',
       '128',
       'F715F20B-19ED-4841-8F8C-3712E17A6FF5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tramadol 50mg/ml vial',
       '10',
       '10',
       '129',
       'A3C7F4C9-014C-4DBA-9660-307C4A811881-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Diphenhydramine 50mg/ml',
       '10',
       '26',
       '130',
       '90129418-E799-4E88-9B32-4E99EE12C147-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Epinephrine',
       '10',
       '30',
       '131',
       'F9E11E50-3575-491D-B020-7E6493DB9C89-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Ampicillin',
       '10',
       '16',
       '132',
       'B4EC7F02-8581-4905-90CD-B3243746EF7E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Metoclopramide',
       '10',
       '12.5',
       '133',
       'ACCCE6D5-2619-48B3-A895-2EDFC6DB10D1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Ranitidine',
       '10',
       '26.5',
       '134',
       'B6FDEC04-6526-4498-A764-6E1530112CF7-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tranexamic acid vial',
       '10',
       '27.5',
       '135',
       '2EF8F963-33CF-4F0B-B99D-F582009A28FF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Furosemide 10mg/ml, 2ml',
       '10',
       '6',
       '136',
       '65CE63DE-074F-476D-A29E-65E511019B58-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Phytomenadione (Vitamin K)',
       '10',
       '17.5',
       '137',
       '9C19C626-F210-47F0-A81A-EE22C8450DA0-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hyoscine N-Butylbromide, 20mg/mL',
       '10',
       '28',
       '138',
       '5A6720F1-8E96-4C03-8A5A-1C1D9D4290B5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Lidocaine HCl 2%, 50mL',
       '1',
       '83',
       '139',
       '10DE4C46-6829-4528-BB4A-6D143E24DA83-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Potassium chloride, 20mL',
       '5',
       '69',
       '140',
       '06148FA8-D9E5-41C5-AC88-81B1D3B52547-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Sterile water for IV, 50mL',
       '1',
       '45',
       '141',
       'D5DD6325-23CD-480C-930E-E57F19F9B34A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Calcium gluconate 10%',
       '5',
       '40',
       '142',
       'F0F0EBF6-C908-4469-AEE3-E5DF91D9A831-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Abdominal retractor',
       '1',
       '1800',
       '143',
       '047950FF-BF1D-414E-9164-23D132860DFA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Allis Tissue Forcep (5x6T) 6"',
       '1',
       '650',
       '144',
       '46FD8A8E-659E-4BC6-8203-DC429854DBD2-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Backhaus Towel Clamp 5 1/4"',
       '3',
       '150',
       '145',
       '386EB67F-1AD6-4EFD-8AE3-7413CB649168-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Blade, # 11',
       '1',
       '3.2',
       '146',
       '6AB33A01-8533-4027-A01A-A8F402A05499-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Blade, # 20',
       '1',
       '3.2',
       '147',
       '0BB55E27-65BD-4105-8CFD-340A45C186AE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Broom (tambo)',
       '1',
       '259',
       '148',
       '5D75E9C2-F9F6-4CA1-AB87-44C3234033C1-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Capillary sealing clay',
       '1',
       '320',
       '149',
       '60A53E72-38E2-47A6-B970-29B3070F4E31-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Carmalt forceps, 6.5" straight',
       '1',
       '1050',
       '150',
       'B28FCD12-0DC6-4ED3-9194-C1C7A9F3A355-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Digital Thermometer',
       '1',
       '70',
       '151',
       'D6F51EAD-DC97-4DD2-97F8-C277B6F97BE9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Water dipper',
       '1',
       '39',
       '152',
       'B58294BB-22EC-4618-81F2-EA8009EF6763-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Glucometer set',
       '1',
       '1500',
       '153',
       '2CE07F03-9FD4-49D5-B283-2FB11BD8383A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hemocytometer set',
       '1',
       '1600',
       '154',
       'D26BD345-823E-43B0-ACE9-2742CB420E52-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hooks (Colorful)',
       '3',
       '87.5',
       '155',
       'A616ED32-0D1C-43CE-9D4D-AF3F348208C8-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hooks (Dog design)',
       '2',
       '100',
       '156',
       '9279CE6A-B257-4769-BACC-0FB9D9DF602F-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hot water bag',
       '3',
       '75',
       '157',
       'DE59BE5F-520D-4CEA-8567-BE7FEE31DA1E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Instrument tray, 8x10',
       '1',
       '1280',
       '158',
       '1C5C521B-A988-4926-91FA-7EAEEC5CAA95-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'IV Stand with wheels',
       '1',
       '1500',
       '159',
       'B9CEF695-26D4-42C9-93B1-092C742D5A92-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Kelly curved forceps',
       '4',
       '250',
       '160',
       '07CCE4B8-CC86-46C4-AEE0-6AFFD4B4257E-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Kelly straight forceps',
       '1',
       '250',
       '161',
       'B27DE918-192A-4115-936E-0A7DA6A97CFF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Kidney basin',
       '2',
       '330',
       '162',
       'B7751F4F-546B-4471-95DE-7903F97DA0CD-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Magnifying lens',
       '2',
       '70',
       '163',
       '3AEA4EE5-618B-4B2F-8AB0-DCC68304ED14-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Mayo Dissecting Scissor 6 3/4" curved',
       '1',
       '250',
       '164',
       'F4C1071C-91ED-4240-98F0-29673514B6BE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Mayo Dissecting Scissor 6 3/4" straight',
       '3',
       '250',
       '165',
       '4E74B051-51FE-4E52-90B8-6AB70DA996E7-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Mayo-Hegar Needle Holder 6"',
       '2',
       '330',
       '166',
       'E74E0274-9AE3-4214-B515-20F06EFABF62-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Metzenbaum Scissor 7" curved',
       '1',
       '250',
       '167',
       '5D93C843-6D61-4E9C-BF71-974EDCBBC1F5-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Mops',
       '1',
       '849',
       '168',
       'DF23FD63-3C6A-4EBA-BF3A-93601112F0B2-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Operating Scissor S/B 5 1/2" straight',
       '3',
       '250',
       '169',
       'A6761CD2-B357-4039-B7C0-B6A6C0FC0A06-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Oxygen mask, adult',
       '5',
       '45',
       '170',
       '22E006C8-94B8-4B6F-AD45-37E283B054E9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Oxygen mask, pedia',
       '4',
       '45',
       '171',
       '98AC2CA4-18A6-4274-8FA3-7E33C5988BBD-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Oxygen regulator',
       '1',
       '980',
       '172',
       'E5FE42B6-B317-4B18-8999-F96C22AD9EAE-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Oxygen tank, 20lbs',
       '1',
       '3200',
       '173',
       'BBAD82DF-1F36-4933-8C66-01A4FBAC8A60-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Pail, 5 gallon',
       '',
       '199',
       '174',
       '872E1C70-07EE-4B53-8FAC-2806B9EB4613-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Penlight',
       '',
       '70',
       '175',
       'F8C1A27E-8F2B-42D1-8DF8-3C920017CC97-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Punch paper big heavy duty',
       '',
       '218.75',
       '176',
       'FC431F9B-3687-4C4D-8658-A668C053F881-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'RBC pipette',
       '',
       '140',
       '177',
       '7B1CF441-E8F7-4668-810B-6678CB91E8C4-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stamp (date)',
       '',
       '38.5',
       '178',
       'D87C9350-1005-46FA-876C-20A82C0AFD76-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stamp (dewormed)',
       '',
       '300',
       '179',
       'A2A4A3F4-A89F-437F-9FD2-A03FF4D55EBC-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stamp (paid with date)',
       '',
       '50',
       '180',
       'F815EAD9-A489-4DD6-BF9F-4C443004A3CF-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stamp (PWHQ logo)',
       '',
       '480',
       '181',
       '9952EE55-A79F-45B3-A5F8-2EED66DA2F0F-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stapler, heavy duty',
       '',
       '214.29',
       '182',
       '54701C77-E37F-4ACE-9F6A-306EE7418099-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Stethoscope',
       '',
       '650',
       '183',
       '7E215265-92FD-4718-9515-893564A10DFA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Surgical drapes set (6-piece)',
       '',
       '1100',
       '184',
       '546B1644-9864-4614-A03A-4CB2F2B4E8C4-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tape dispenser',
       '',
       '80',
       '185',
       '6996342B-313E-4614-8EBF-229300D5AC29-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tourniquet',
       '',
       '15',
       '186',
       '0199701F-C6E5-43DB-88A2-EDAD3F34EE0C-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Tray desk, metal double',
       '',
       '225',
       '187',
       '355E56D1-0D7D-4BF4-9E0D-FEDAA96905EA-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Vacuum Wet and Dry',
       '',
       '4004',
       '188',
       '10520A60-8AEF-4C21-B7BF-AB0E937CC5D4-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'WBC pipette',
       '',
       '140',
       '189',
       '0FB70736-DDD1-4134-80F9-4F65930D5965-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Whiteboard, magnetic, 2x3',
       '',
       '550',
       '190',
       '6C4CB714-92FF-4EB0-A957-D7E0C8BC714A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Universal socket',
       '',
       '584.1',
       '191',
       '857C0745-EA8C-418F-80F6-19ACA22EF9C3-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Mesh tray',
       '',
       '129',
       '192',
       'C49AD6C2-7D80-4246-9402-DEB69B350BD2-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Drawer organizer, small',
       '',
       '59.75',
       '193',
       'A0AA293B-B7A6-4700-8B59-660122E12489-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Extension cord, 3 gang',
       '',
       '449',
       '194',
       '8A50ECF3-BB8E-4C6E-8251-CA37ACF6136D-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Drawer organizer, medium',
       '',
       '59.75',
       '195',
       '32E39942-2159-46FF-9F1E-ABE8A21FFAE9-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Laundry Rack',
       '',
       '649',
       '196',
       '30C13819-3303-4F03-869E-8054D18BC20A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Trash bin, 80L',
       '',
       '1049',
       '197',
       '10C1A3C6-6003-4745-BC68-0E815DC5A43C-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Trash bin, 13L',
       '',
       '279.75',
       '198',
       'F7C95F5B-AE96-4AA6-B962-3786583EED04-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Utility trash bin, 38L',
       '',
       '699',
       '199',
       '4D16A7BE-3C0F-47E7-832C-906ED00C8C6A-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Trash bin, 9L',
       '',
       '209',
       '200',
       'C0BF00BE-3C91-41B2-A47D-27592761AD03-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Hand towel',
       '',
       '129',
       '201',
       '1035DC30-6F9B-4993-904D-E9306CC2EC7B-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Caddy and Stool in One',
       '',
       '179',
       '202',
       'D362986D-D052-49D0-9490-98ECC6BE22F2-[20230314_1401_Pet Wellness HQ_Import Items]'
UNION ALL
SELECT '',
       'Battery AA, 12 pack',
       '',
       '269.75',
       '203',
       '329157CE-75F5-4F40-89C5-BE6D6B2D2432-[20230314_1401_Pet Wellness HQ_Import Items]'

GO

GO

SELECT 'ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Items]' TableName

GO

SELECT *
FROM   ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Items]

GO 


                if OBJECT_ID('ForImport.dbo.[Untitled 1]') is not null
                BEGIN
                    DROP TABLE ForImport.dbo.[Untitled 1]
                END 

                GO

                CREATE TABLE ForImport.[dbo].[Untitled 1]
                (
                      [Old] varchar(5000) ,[New] varchar(5000) ,[RowIndex] varchar(5000) ,[GUID] varchar(5000) 
                ) 

                GO

                

                INSERT INTO ForImport.[dbo].[Untitled 1]
                           ([Old],[New],[RowIndex],[GUID])
                SELECT '01/2025','2025-01-01','1','6B21F336-1330-4559-B475-9220BF8FAE3E-[Untitled 1]' UNION ALL SELECT '02/2027','2027-02-01','2','5ED280DF-84B9-4A16-90AE-09FC4878838C-[Untitled 1]' UNION ALL SELECT '03/2024','2024-03-01','3','8E23608E-A8B5-4338-AEE9-74620BEF380B-[Untitled 1]' UNION ALL SELECT '03/2025','2025-03-01','4','49192023-CE63-4ACB-AE77-82D140AF2368-[Untitled 1]' UNION ALL SELECT '03/2027','2027-03-01','5','20373347-08FD-4770-8F5F-84BE0E0C5DB1-[Untitled 1]' UNION ALL SELECT '04/2024','2024-04-01','6','E54EB121-6104-4254-819B-3439D1E89375-[Untitled 1]' UNION ALL SELECT '04/2025','2025-04-01','7','94AE1400-CE07-4BAD-8D6F-EA7025A255D9-[Untitled 1]' UNION ALL SELECT '04/2027','2027-04-01','8','FE5DA653-0F5A-4E42-AD61-ED3B03EB7FD0-[Untitled 1]' UNION ALL SELECT '05/2024','2024-05-01','9','E34250DA-C8D1-4896-A476-059B7B3152C2-[Untitled 1]' UNION ALL SELECT '05/2025','2025-05-01','10','EFEAFE4D-6072-47E2-85C7-7B7A4ACD87E0-[Untitled 1]' UNION ALL SELECT '05/2027','2027-05-01','11','936EF499-E8D9-4AB1-8CCE-889D41EE4BC4-[Untitled 1]' UNION ALL SELECT '07/2026','2026-07-01','12','1852CE31-BD41-4963-911C-BBD1A0EC0730-[Untitled 1]' UNION ALL SELECT '07/2027','2027-07-01','13','46C7AE97-9DDE-4674-BD43-7386A6E5D9FF-[Untitled 1]' UNION ALL SELECT '08/2024','2024-08-01','14','822269F3-4C96-46E9-A0FE-F747D62CE6E4-[Untitled 1]' UNION ALL SELECT '08/2026','2026-08-01','15','C1BC7A21-E945-4517-903B-F0EAFCB7F459-[Untitled 1]' UNION ALL SELECT '08/2027','2027-08-01','16','805FB0EF-1DEF-4B16-8B3F-2784B177CFEA-[Untitled 1]' UNION ALL SELECT '09/2025','2025-09-01','17','BFB8B68E-2F4D-416C-96E6-47346FB3A5E7-[Untitled 1]' UNION ALL SELECT '1/2025','2025-01-01','18','2F529456-7AD1-4891-BDA8-AB2E0B403347-[Untitled 1]' UNION ALL SELECT '10/2023','2023-10-01','19','95E3E5AB-7D1B-44B1-ABA8-AE752ECB8D36-[Untitled 1]' UNION ALL SELECT '10/2024','2024-10-01','20','792822C3-755C-4711-B73A-0FB286CBA4A9-[Untitled 1]' UNION ALL SELECT '10/2025','2025-10-01','21','373A93CC-7BC2-44B4-801F-BDDDB08430E6-[Untitled 1]' UNION ALL SELECT '10/2026','2026-10-01','22','A6888572-1CB1-4263-8F0E-435908A35354-[Untitled 1]' UNION ALL SELECT '11/2022','2022-11-01','23','AFA9A833-2E55-4D4F-9CEB-D1E92FD13573-[Untitled 1]' UNION ALL SELECT '11/2023','2023-11-01','24','E38AACE8-60FA-4E62-887A-9C2FF25114C8-[Untitled 1]' UNION ALL SELECT '11/2024','2024-11-01','25','2AC65A51-8945-4920-88DE-638EFA3FCE19-[Untitled 1]' UNION ALL SELECT '12/2023','2023-12-01','26','7D8F1EA3-86B5-46EC-8302-8CEF1F1EFC73-[Untitled 1]' UNION ALL SELECT '12/2024','2024-12-01','27','3D3818FC-659E-46E2-94EC-F739D02B233A-[Untitled 1]' UNION ALL SELECT '12/2025','2025-12-01','28','8B6B9B20-AFF0-44D8-86FA-3A05A9B26A9F-[Untitled 1]' UNION ALL SELECT '12/2026','2026-12-01','29','6E6AF143-1093-4172-83BD-B5036D58F933-[Untitled 1]' UNION ALL SELECT '2/2024','2024-02-01','30','C9F51DBF-AD94-40DB-AE45-CCDEE397501E-[Untitled 1]' UNION ALL SELECT '2/2025','2025-02-01','31','8CBCDD35-6312-4EEB-855A-F6951E4E56F6-[Untitled 1]' UNION ALL SELECT '2/2026','2026-02-01','32','FD8E1E86-2787-4821-928F-271979BDC283-[Untitled 1]' UNION ALL SELECT '3/2023','2023-03-01','33','05C08742-E706-4469-98E5-5BC1C6907B23-[Untitled 1]' UNION ALL SELECT '3/2024','2024-03-01','34','42011048-94F2-4D0B-884F-CD72FA5DF492-[Untitled 1]' UNION ALL SELECT '3/2025','2025-03-01','35','01D59B11-1071-42A4-9E05-2D261D1A23C5-[Untitled 1]' UNION ALL SELECT '4/2023','2023-04-01','36','FAC84A7D-0120-4AFA-88B9-51F66FCFE19B-[Untitled 1]' UNION ALL SELECT '4/2024','2024-04-01','37','1C8D66B4-3AE6-442B-9BE5-823B4CD189A3-[Untitled 1]' UNION ALL SELECT '4/2025','2025-04-01','38','A9D17503-67E1-4206-9B4E-5E9720275E18-[Untitled 1]' UNION ALL SELECT '4/2027','2027-04-01','39','3929C772-DB58-4375-96E1-8441512FAC63-[Untitled 1]' UNION ALL SELECT '5/2025','2025-05-01','40','92F6734B-168C-4D61-BA88-AEC9DB15F321-[Untitled 1]' UNION ALL SELECT '5/2027','2027-05-01','41','567A03D8-39FC-4208-A835-20AC96B231D5-[Untitled 1]' UNION ALL SELECT '6/2023','2023-06-01','42','46A8A4C7-6A5B-4D13-9EF6-B098AAD08A08-[Untitled 1]' UNION ALL SELECT '6/2024','2024-06-01','43','E34D2C70-7E62-477A-A5E2-A76590A4A2C5-[Untitled 1]' UNION ALL SELECT '7/2024','2024-07-01','44','787891DF-477C-4DEA-B221-F797CFD5F82B-[Untitled 1]' UNION ALL SELECT '7/2025','2025-07-01','45','8919D7B4-2A84-4FEB-A065-D24603819D91-[Untitled 1]' UNION ALL SELECT '8/2024','2024-08-01','46','7A5B8B45-2996-44DF-B94F-8055577C0707-[Untitled 1]' UNION ALL SELECT '9/2023','2023-09-01','47','91A320BB-71F5-4A6E-BE5A-56C6F8CEB809-[Untitled 1]' UNION ALL SELECT '9/2024','2024-09-01','48','4AA86B8A-B75A-479A-8910-0013DB731E16-[Untitled 1]'
                GO
            

                GO

                SELECT 'ForImport.[dbo].[Untitled 1]' TableName
                GO


CREATE OR ALTER FUNCTION [dbo].[fGetDaysInMonth] ( @pDate    DATETIME )
RETURNS INT
AS
BEGIN

    SET @pDate = CONVERT(VARCHAR(10), @pDate, 101)
    SET @pDate = @pDate - DAY(@pDate) + 1

    RETURN DATEDIFF(DD, @pDate, DATEADD(MM, 1, @pDate))
END
GO


                SELECT *, dbo.[ufn_GetDaysInMonth](New), CONVERT(Date,  FORMAT(CONVERT(Date, New),'yyyy-MM-') + CONVERT(VARCHAR, (dbo.[ufn_GetDaysInMonth](New)))) Date FROM ForImport.[dbo].[Untitled 1]

                GO
            