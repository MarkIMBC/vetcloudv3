IF OBJECT_ID(N'Temp-2022-07-29-Client', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-07-29-Client];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-07-29-' + company.Guid + '-Client-'
       + Convert(Varchar(MAX), client.ID)            tempID,
       client.Name,
       client.ContactNumber,
       client.ContactNumber2,
       client.Email,
       client.Address,
       CASE
         WHEN LEN(ISNULL(client.Comment, '')) > 0 THEN CHAR(13)
         ELSE ''
       END
       + 'Imported from VetCloudTemp July 29, 2022' Comment,
       ID_Company,
       client.DateCreated                            DateCreated_Client
INTO   [dbo].[Temp-2022-07-29-Client]
FROM   db_vetcloudv3_thecatclinic.[dbo].tClient client
       inner join db_vetcloudv3_thecatclinic.[dbo].tCompany company
               on client.ID_Company = company.ID
where  client.ID >= 428915
       AND company.GUID = 'ACE352E0-1293-4416-AB07-B8A611F37F6C'

INSERT @NotYetInserted_TempID
SELECT tempClient.tempID
FROm   [dbo].[Temp-2022-07-29-Client] tempClient

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tclient)

INSERT dbo.tClient
       (ID_Company,
        tempID,
        Name,
        ContactNumber,
        ContactNumber2,
        Address,
        Email,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempClient.ID_Company,
       tempClient.tempID,
       tempClient.Name,
       tempClient.ContactNumber,
       tempClient.ContactNumber2,
       tempClient.Address,
       tempClient.Email,
       Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [dbo].[Temp-2022-07-29-Client] tempClient
       inner join @NotYetInserted_TempID unInsertedTempCLient
               on tempClient.tempID = unInsertedTempCLient.tempID

SELECT *
FROm   [Temp-2022-07-29-Client] 
