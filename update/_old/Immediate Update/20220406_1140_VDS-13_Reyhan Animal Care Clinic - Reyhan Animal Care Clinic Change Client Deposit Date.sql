DECLARE @GUID_Company VARCHAR(MAX) = '53235405-B413-4E2F-858E-45023E359E1A'
DECLARE @Source_ID_Patient INT=452922
DECLARE @Destination_ID_Client INT=331045

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tClientDeposit
Set    Date = '2022-04-04 00:00:00'
WHERE  ID_Company = @ID_Company
       and Code = 'DPS-00001'

Update tClientDeposit
Set    Date = '2022-04-05 00:00:00'
WHERE  ID_Company = @ID_Company
       and Code = 'DPS-00002'

update tBillingInvoice
SET    ConfinementDepositAmount = InitialConfinementDepositAmount
where  iD = 233257
       and ID_Company = @ID_Company

SELECT *
FROm   tClientDeposit
WHERE  ID_Company = @ID_Company

SELECT *
FROM   tBillingInvoice
where  iD = 233257
       and ID_Company = @ID_Company 
