DECLARE @GUID_Company VARCHAR(MAX) = 'D4D74C3B-BF59-43B7-8F32-7E93D16B196A'

IF(SELECT Count(*)
   FROM   vCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Service_ID_ItemType INT = 1
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([Service Name])
       + CASE
           WHEN LEN(dbo.fGetCleanedString([Service Name1])) > 0 then ' '
                                                                     + dbo.fGetCleanedString([Service Name1])
           ELSE ''
         END,
       dbo.fGetCleanedString([Service Fee]),
       dbo.fGetCleanedString([CATEGORY]),
       [GIUD]
FROM   ForImport.dbo.[Petlink-Acacia-Services_20220812_0427]

Update @ForImport
SET    Import_Category = 'Grooming Services'
WHERE  Import_Category = 'GROOMING'

--Anal Sac Draining All Breeds
DELETE FROM @forImport
WHERE  GUID = 'DFBA5D78-8066-431D-A2A4-592581674175-[Petlink-Acacia-Services_20220812_0427]'

--Furosemide Medium (7-15kgs)
DELETE FROM @forImport
WHERE  GUID = 'B25FA2EF-9B65-4598-9459-6E202F6B45E0-[Petlink-Acacia-Services_20220812_0427]'

--Organic Full Grooming XLarge Breed
DELETE FROM @forImport
WHERE  GUID = '0EDB1DD8-5DF3-4B41-B64E-A4BA32D2905F-[Petlink-Acacia-Services_20220812_0427]'

--Organic Full Grooming XxLarge Breed
DELETE FROM @forImport
WHERE  GUID = '4D530CE2-C4FA-4179-8909-AF954CA2B08E-[Petlink-Acacia-Services_20220812_0427]'

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Service_ID_ItemType
       and category.IsActive = 1

SELECT Import_Category,
       COUNT(*)
FROM   @forImport
WHERE  ID_ItemCategory IS NULL
GROUP  BY Import_Category

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Service_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    Import_Category = 'Grooming Services'
WHERE  Import_Category = 'GROOMING'

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Service_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice,
       Name = import.Name_Item
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 
