DECLARE @GUID_Company VARCHAR(MAX) = '30B74061-6489-4093-BAFE-CF1AE8200052'
DECLARE @ID_ItemType INT = 1
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')

-----------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitPrice         Decimal(18, 2),
     GUID              VARCHAR(MAX)
  )

INSERT @import
       (Name_Item,
        Name_ItemCategory,
        UnitPrice,
        GUID)
SELECT dbo.fGetCleanedString([SERVICE]),
       dbo.fGetCleanedString([CATEGORY]),
       TRY_CONVERT(Decimal(18, 2), dbo.fGetCleanedString([SERVICE FEE])),
       [GUID]
FROM   ForImport.[dbo].[PetWellnessSERVICES]

exec pInsertItemCategory
  'Laboratories',
  1,
  1

Update @import
SET    Name_ItemCategory = 'Boarding Services'
WHERE  Name_ItemCategory = 'Boarding'

Update @import
SET    Name_ItemCategory = 'Castrations'
WHERE  Name_ItemCategory = 'Castration'

Update @import
SET    Name_ItemCategory = 'Grooming Services'
WHERE  Name_ItemCategory = 'Grooming'

Update @import
SET    Name_ItemCategory = 'Laboratories'
WHERE  Name_ItemCategory = 'Laboratories'

Update @import
set    ID_ItemCategory = cat.ID
FROM   @import import
       inner join (SELECT Max(ID)                     ID,
                          dbo.fGetCleanedString(Name) Name
                   FROM   tItemCategory
                   where  ID_ItemType = @ID_ItemType
                          and IsActive = 1
                   GROUP  BY Name) cat
               on import.Name_ItemCategory = cat.Name

--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.GUID = item.tempID
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

-------------------------------------------
INSERT INTO [dbo].[tItem]
            ([Name],
             [IsActive],
             [ID_Company],
             tempID,
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT DISTINCT Name_Item,
                1,
                @ID_Company,
                Guid,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                @ID_ItemType
FROM   @import
where  LEN(Name_Item) > 0
       and ID_Item IS NULL

--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.GUID = item.tempID
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

-------------------------------------------
Update tItem
SET    Name = import.Name_Item,
       ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @import import
               on item.ID = import.ID_Item
WHERE  item.ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType

SELECT *
FROM   @import

SELECT DISTINCT Name_ItemCategory
FROM   @import
where  ID_ItemCategory IS NULL 
