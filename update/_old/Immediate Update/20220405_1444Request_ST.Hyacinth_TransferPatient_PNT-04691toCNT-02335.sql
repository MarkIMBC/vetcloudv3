DECLARE @GUID_Company VARCHAR(MAX) = 'CE106252-85B7-4348-8441-517560A07A7C'
DECLARE @Source_ID_Patient INT=452922
DECLARE @Destination_ID_Client INT=331045
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company


--Checking
SELECT * FROM tPatient WHERE ID_Client=@Destination_ID_Client
--Transfer Patient to New Owner
Update tPatient
SET ID_Client=@Destination_ID_Client 
WHERE ID=@Source_ID_Patient AND ID_Company=@ID_Company

--Transfer Records to New Owner

--SOAP
Update tPatient_SOAP 
SET ID_Client=@Destination_ID_Client 
WHERE ID_Patient=@Source_ID_Patient AND ID_Company=@ID_Company

--Wellness
Update tPatient_Wellness
SET ID_Client=@Destination_ID_Client 
WHERE ID_Patient=@Source_ID_Patient AND ID_Company=@ID_Company


--Checking
SELECT * FROM tPatient WHERE ID_Client=@Destination_ID_Client
