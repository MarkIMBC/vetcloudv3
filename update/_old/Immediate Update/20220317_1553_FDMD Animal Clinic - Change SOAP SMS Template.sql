DECLARE @GUID_Company VARCHAR(MAX) = '287BA525-1C3C-4C50-8139-1DAA4669874D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'This is a system generated message. PLEASE DO NOT REPLY. Contact /*CompanyName*/ /*ContactNumber*/ for more details. '
                            + CHAR(13) + CHAR(13)
                            + ' Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.'
WHERE  ID = @ID_Company

select *
FROM   dbo.fGetSendSoapPlan('2022-03-16 00:00:00', NULL, '70') 
