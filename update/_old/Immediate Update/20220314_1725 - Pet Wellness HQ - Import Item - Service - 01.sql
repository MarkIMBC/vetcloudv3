DEclare @GUID_Company VARCHAR(MAX) = '30B74061-6489-4093-BAFE-CF1AE8200052'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END


SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item                    INT,
     Name_Item                  VARCHAR(MAX),
     CustomCode_Item            VARCHAR(MAX),
     CurrentInventoryCount_Item INT,
     ID_ItemType                INT,
     UnitCost                   DECIMAL(18, 2),
     UnitPrice                  DECIMAL(18, 2),
     ID_ItemCategory            INT,
     Name_ItemCategory          VARCHAR(MAX),
     tempID                     VARCHAR(MAX),
     DateExpiration_String      VARCHAR(MAX),
     DateExpiration     DateTime
  )


exec pInsertItemCategory
  'C1 ',
  2

exec pInsertItemCategory
  'C2/ TREATMENT ROOM',
  2

exec pInsertItemCategory
  'CAT BOARDING',
  2

exec pInsertItemCategory
  'CAT GROOMING',
  2

exec pInsertItemCategory
  'CLINIC CONSUMABLES',
  2

exec pInsertItemCategory
  'DOG BOARDING ',
  2

exec pInsertItemCategory
  'DOG GROOMING',
  2

exec pInsertItemCategory
  'GROOMING CONSUMABLES',
  2

exec pInsertItemCategory
  'GROOMING EQUIPMENT',
  2

exec pInsertItemCategory
  'NOOK AREA ',
  2

exec pInsertItemCategory
  'PHARMACY AND GOODS FOR SALE',
  2

exec pInsertItemCategory
  'SURGERY ROOM ',
  2

exec pInsertItemCategory
  'WHELPING ROOM ',
  2

exec pInsertItemCategory
  'Laboratory Services',
  1

exec pInsertItemCategory
  'Other Services',
  1

exec pInsertItemCategory
  'Pet Rehabilitation Services',
  1

exec pInsertItemCategory
  'Surgery',
  1

exec pInsertItemCategory
  'Treatment and Confinement',
  1

exec pInsertItemCategory
  'Whelping and Queening Services',
  1

Update tItem
set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company;




INSERT @import
       (Name_ItemCategory,
        Name_Item,
        CustomCode_Item,
        CurrentInventoryCount_Item,
        UnitCost,
        UnitPrice,
        DateExpiration_String,
        ID_ItemType,
        tempID)
SELECT dbo.fGetCleanedString([Category]),
       dbo.fGetCleanedString([PRODUCT NAME]),
       dbo.fGetCleanedString([PRODUCT CODE]),
       TRY_CONVERT(INT, REPLACE(REPLACE(TRIM(ISNULL([INVENTORY], '0')), ',', ''), '.00', '')),
       ISNULL(TRY_CONVERT(DECIMAL(18, 2), REPLACE([ACQUISITION PRICE], ',', '')), 0),
       ISNULL(TRY_CONVERT(DECIMAL(18, 2), REPLACE([SELLING PRICE], ',', '')), 0),
       dbo.fGetCleanedString([EXPIRATION DATE]),
       CASE
         WHEN [Type] = 'Inventoriable' THEN 2
         ELSE
           CASE
             WHEN [Type] = 'Service' THEN 1
             ELSE null
           END
       END,
       [GUID]
FROM   ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Item - Services]

 


 
Update @import SET DateExpiration = CONVERT(Date,  FORMAT(CONVERT(Date, New),'yyyy-MM-') + CONVERT(VARCHAR, (dbo.[ufn_GetDaysInMonth](New))))
FROM @import import inner join ForImport.[dbo].[Untitled 1] _date
	on import.DateExpiration_String = _date.[Old]


Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = import.ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = import.ID_ItemType

Update @import
SET    ID_ItemCategory = category.ID
FROM   @import import
       INNER JOIN (SELECT MAX(ID) ID,
                          Name,
                          ID_ItemType
                   FROM   tItemCategory
                   GROUP  BY Name,
                             ID_ItemType) category
               on Name_ItemCategory = category.Name
                  And import.ID_ItemType = category.ID_ItemType

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       hed.ID_ItemType
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL
       AND LEN(hed.Name_Item) > 0

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = import.ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = import.ID_ItemType

Update tItem
SET    ID_ItemType = import.ID_ItemType,
       ID_ItemCategory = import.ID_ItemCategory,
       CustomCode = import.CustomCode_Item,
       UnitPrice = import.UnitPrice
FROM   tItem item
       INNER JOIN @import import
               on item.ID = import.ID_Item
where  item.ID_COmpany = @ID_Company

SELECT *
FROM   @import
-------------------------------------------------------------------------------------------------------------------
--DECLARE @importInventory typReceiveInventory
--INSERT @importInventory
--       ([Code],
--        [ID_Item],
--        [Quantity],
--        [UnitPrice],
--        [DateExpired],
--        [BatchNo],
--        [ID_FilingStatus],
--        [ID_Company],
--        [Comment],
--        [IsAddInventory])
--SELECT 'Initial Inventory',
--       ID_Item,
--       CurrentInventoryCount_Item,
--       ISNULL(UnitPrice, 0),
--       NULL,
--       NULL,
--       3,
--       @ID_Company,
--       'Imported as of '
--       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
--       1
--FROM   @import
--WHERE  ISNULL(CurrentInventoryCount_Item, 0) > 0
--DECLARE @ID_Usersession INT
--SELECT @ID_Usersession = MAX(_session.ID)
--FROM   tUserSession _session
--       inner join vUser _user
--               on _session.ID_User = _user.ID
--where  ID_Company = @ID_Company
--exec pReceiveInventory
--  @importInventory,
--  @ID_Usersession
--SELECT *
--FROM   @importInventory 
