DECLARE @GUID_Company VARCHAR(MAX) = 'C01942F4-1B11-4EFA-BF08-EB7292BA56F7'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. '
  + 'Pls. contact /*CompanyName*/ /*ContactNumber*/'
DECLARE @SMSTemplate VARCHAR(MAX) = 'Hi /*Client*/,'
  + ' /*Pet*/ has an appointment for /*Service*/ /*DateReturn*/.'
  + ' Please contact (0908) 399 0446 Animal Heart Veterinary Clinic And Grooming Center - Daet Branch for further inquiries.'
  + ' Thank You.'
  + 'Reminder: Holiday na po ang clinic on December 24-25, 2022'

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
