DEclare @GUID_Company VARCHAR(MAX) = '91C3CEF9-1B32-4C1D-BB92-843F4B1116AA'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 345936 -- IAN PAUL
DECLARE @Source2_ID_Client INT = 344170 -- MARIE GRACE APOLLANA
DECLARE @Destination_ID_Client INT = 349792 -- ANJO APOLONIO
SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Source 2'
FROm   vClient
WHERE  ID = @Source2_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - ANJO APOLONIO from CNT-41431 to CNT-41776'

exec pMergeClientRecord
  @Source2_ID_Client,
  @Destination_ID_Client,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - ANJO APOLONIO from CNT-40063 to CNT-41776'

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company

Update tClient
SET    IsActive = 0
WHERE  ID = @Source2_ID_Client
       and ID_Company = @ID_Company 
