DEclare @GUID_Company VARCHAR(MAX) = '30B74061-6489-4093-BAFE-CF1AE8200052'
DEclare @Service_ID_ItemType INT = 1

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
SELECT *
FROM   tItem
WHERE   ID_ItemType = @Service_ID_ItemType
       and ID_Company = @ID_Company

Update tItem
SET    IsActive = 0
WHERE  ID_ItemType = @Service_ID_ItemType
       and ID_Company = @ID_Company 
