DECLARE @GUID_Company VARCHAR(MAX) = '96BE41FF-0C98-44D3-9D9B-9D3DB1C4D7AA'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tItem
SET    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company

Update [JMIC-HEALTH-NEEDS]
SET    [PARTICULARS] = dbo.fGetCleanedString([PARTICULARS])

DECLARE @ImportRecord TABLE
  (
     ID_Item      INT,
     Name_Item    VARCHAR(MAX),
     SKUCode_Item VARCHAR(MAX)
  )

INSERT @ImportRecord
       (SKUCode_Item,
        Name_Item)
SELECT [SKU Codes],
       [PARTICULARS]
FROM   [JMIC-HEALTH-NEEDS] import

Update @ImportRecord
SET    ID_Item = item.ID
FROM   @ImportRecord import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND IsActive = 1
       and ID_ItemType = 2

SELECT *
FROm   @ImportRecord
where  ID_Item IS NULl

Update tItem
SET    CustomCode = import.SKUCode_Item
FROM   tItem item
       inner join @ImportRecord import
               on item.iD = import.ID_Item

SELECT *
FROm   @ImportRecord
where  ID_Item IS NOT NULl 
