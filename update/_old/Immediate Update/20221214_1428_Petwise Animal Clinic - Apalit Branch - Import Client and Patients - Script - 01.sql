DECLARE @GUID_Company VARCHAR(MAX) = 'E9262033-6657-4803-AAB2-17C725C5C382'

-------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
  -- AND IsActive = 1
  ) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @import tABLE
  (
     ID_Client            INT,
     Name_Client          Varchar(MAX),
     ContactNumber_Client Varchar(MAX),
     Address              Varchar(MAX),
     ID_Patient_1         Int,
     Name_Patient_1       VARCHAR(MAX),
     ID_Patient_2         Int,
     Name_Patient_2       VARCHAR(MAX),
     ID_Patient_3         Int,
     Name_Patient_3       VARCHAR(MAX),
     ID_Patient_4         Int,
     Name_Patient_4       VARCHAR(MAX),
     ID_Patient_5         Int,
     Name_Patient_5       VARCHAR(MAX),
     ID_Patient_6         Int,
     Name_Patient_6       VARCHAR(MAX),
     ID_Patient_7         Int,
     Name_Patient_7       VARCHAR(MAX),
     ID_Patient_8         Int,
     Name_Patient_8       VARCHAR(MAX),
     ID_Patient_9         Int,
     Name_Patient_9       VARCHAR(MAX),
     ID_Patient_10        Int,
     Name_Patient_10      VARCHAR(MAX),
     ID_Patient_11        Int,
     Name_Patient_11      VARCHAR(MAX),
     GUID                 VARCHAR(MAX)
  )

INSERT @import
       (Name_Client,
        ContactNumber_Client,
        Address,
        Name_Patient_1,
        Name_Patient_2,
        Name_Patient_3,
        Name_Patient_4,
        Name_Patient_5,
        Name_Patient_6,
        Name_Patient_7,
        Name_Patient_8,
        Name_Patient_9,
        Name_Patient_10,
        Name_Patient_11,
        GUID)
SELECT [NAMe],
       [CONTACT NO.],
       [`],
       [PET 1],
       [PET 2],
       [PET 3],
       [PET 4],
       [PET 5],
       [PET 6],
       [PET 7],
       [PET 8],
       [PET 9],
       [PET 10],
       [PET 11],
       GUID
FROM   ForImport.[dbo].[PWAC-Clients-v2022]

---------------------------------------------------------
Update @import
SET    ID_Client = client.ID
FROM   @import excel
       INNER JOIN tClient client
               on excel.GUID = client.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_1 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_1' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_2 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_2' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_3 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_3' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_4 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_4' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_5 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_5' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_6 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_6' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_7 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_7' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_8 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_8' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_9 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_9' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_10 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_10' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_11 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_11' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

---------------------------------------------------------
INSERT INTO [dbo].tClient
            ([Name],
             tempID,
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name_Client,
       Guid,
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NULL
       and LEN(Name_Client) > 0

---------------------------------------------------------
Update @import
SET    ID_Client = client.ID
FROM   @import excel
       INNER JOIN tClient client
               on excel.GUID = client.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

---------------------------------------------------------
INSERT INTO [dbo].tPatient
            ([Name],
             ID_Client,
             tempID,
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name_Patient_1,
       ID_Client,
       Guid + '_1',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_1 IS NULL
       and LEN(Name_Patient_1) > 0
UNION ALL
SELECT Name_Patient_2,
       ID_Client,
       Guid + '_2',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_2 IS NULL
       and LEN(Name_Patient_2) > 0
UNION ALL
SELECT Name_Patient_3,
       ID_Client,
       Guid + '_3',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_3 IS NULL
       and LEN(Name_Patient_3) > 0
UNION ALL
SELECT Name_Patient_4,
       ID_Client,
       Guid + '_4',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_4 IS NULL
       and LEN(Name_Patient_4) > 0
UNION ALL
SELECT Name_Patient_5,
       ID_Client,
       Guid + '_5',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_5 IS NULL
       and LEN(Name_Patient_5) > 0
UNION ALL
SELECT Name_Patient_6,
       ID_Client,
       Guid + '_6',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_6 IS NULL
       and LEN(Name_Patient_6) > 0
UNION ALL
SELECT Name_Patient_7,
       ID_Client,
       Guid + '_7',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_7 IS NULL
       and LEN(Name_Patient_7) > 0
UNION ALL
SELECT Name_Patient_8,
       ID_Client,
       Guid + '_8',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_8 IS NULL
       and LEN(Name_Patient_8) > 0
UNION ALL
SELECT Name_Patient_9,
       ID_Client,
       Guid + '_9',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_9 IS NULL
       and LEN(Name_Patient_9) > 0
UNION ALL
SELECT Name_Patient_10,
       ID_Client,
       Guid + '_10',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_10 IS NULL
       and LEN(Name_Patient_10) > 0
UNION ALL
SELECT Name_Patient_11,
       ID_Client,
       Guid + '_11',
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import
where  ID_Client IS NOT NULL
       and ID_Patient_11 IS NULL
       and LEN(Name_Patient_11) > 0

---------------------------------------------------------
Update @import
SET    ID_Patient_1 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_1' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_2 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_2' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_3 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_3' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_4 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_4' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_5 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_5' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_6 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_6' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_7 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_7' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_8 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_8' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_9 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_9' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_10 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_10' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

Update @import
SET    ID_Patient_11 = patient.ID
FROM   @import excel
       INNER JOIN tPatient patient
               on excel.GUID + '_11' = patient.tempID
where  ID_Company = @ID_Company
       and IsActive = 1

---------------------------------------------------------


Update tClient SET  ContactNumber = import.ContactNumber_Client, Address = import.Address
FROM tClient client inner join @import import on client.tempID = import.GUID



SELECT client.ContactNumber, client.Address
FROM tClient client inner join @import import on client.tempID = import.GUID




