GO

CREATE OR
ALTER PROC pMergePatientRecordByCompany(@GUID_Company             VARCHAR(MAX),
                                        @Source_Code_Patient      VARCHAR(MAX),
                                        @Destination_Code_Patient VARCHAR(MAX))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   vCompanyActive
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------
      DECLARE @Source1_ID_Patient INT
      DECLARE @Destination_ID_Patient INT
      DECLARE @Source1_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Destination_Name_Patient VARCHAR(MAX) = ''
      DECLARE @Comment VARCHAR(MAX) = ''

      SELECT @Source1_ID_Patient = ID,
             @Source1_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Source_Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Patient = ID,
             @Destination_Name_Patient = Name
      FROm   vPatient
      WHERE  Code = @Destination_Code_Patient
             AND ID_Company = @ID_Company

      SET @Comment = '' + @Name_Company + ' - Merge Patient - '
                     + @Destination_Name_Patient + ' from '
                     + @Source_Code_Patient + ' to '
                     + @Destination_Code_Patient

      SELECT ID,
             Code,
             Name,
             'Source 1',
             IsActive
      FROm   vPatient
      WHERE  ID = @Source1_ID_Patient
             AND ID_Company = @ID_Company
      Union ALL
      SELECT ID,
             Code,
             Name,
             'Destination',
             IsActive
      FROm   vPatient
      WHERE  ID = @Destination_ID_Patient
             AND ID_Company = @ID_Company

      exec pMergePatientRecord
        @Source1_ID_Patient,
        @Destination_ID_Patient,
        @Comment

      Update tPatient
      SET    IsActive = 0
      WHERE  ID = @Source1_ID_Patient
             and ID_Company = @ID_Company
  END

GO

CREATE OR
ALTER PROC pTransferPetToNewClientByCompany(@GUID_Company            VARCHAR(MAX),
                                            @Code_Patient            VARCHAR(MAX),
                                            @Source_Code_Client      VARCHAR(MAX),
                                            @Destination_Code_Client VARCHAR(MAX))
as
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   vCompanyActive
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT
      DECLARE @Name_Company VARCHAR(MAX)

      SELECT @ID_Company = ID,
             @Name_Company = Name
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------
      DECLARE @ID_Patient INT = 0
      DECLARE @Source_ID_Client INT = 0
      DECLARE @Destination_ID_Client INT = 0

      SELECT @ID_Patient = ID
      FROm   vPatient
      WHERE  COde = @Code_Patient
             AND ID_Company = @ID_Company

      SELECT @Source_ID_Client = ID
      FROm   vClient
      WHERE  COde = @Source_Code_Client
             AND ID_Company = @ID_Company

      SELECT @Destination_ID_Client = ID
      FROm   vClient
      WHERE  COde = @Destination_Code_Client
             AND ID_Company = @ID_Company

      SELECT Code,
             Name,
             Name_Client
      FROm   vPatient
      where  ID = @ID_Patient
             and ID_Company = @ID_Company

      SELECT ID,
             Code,
             Name,
             'Source 1',
             IsActive
      FROm   vClient
      WHERE  ID = @Source_ID_Client
             AND ID_Company = @ID_Company
      Union ALL
      SELECT ID,
             Code,
             Name,
             'Destination',
             IsActive
      FROm   vClient
      WHERE  ID = @Destination_ID_Client
             AND ID_Company = @ID_Company

      Update tPatient
      SET    ID_Client = @Destination_ID_Client
      WHERe  ID = @ID_Patient
             AND ID_Company = @ID_Company
             and ID_Client = @Source_ID_Client

      SELECT Code,
             Name,
             Name_Client
      FROm   vPatient
      where  ID = @ID_Patient
             and ID_Company = @ID_Company
  -----------------------------------------------------------------
  END

GO