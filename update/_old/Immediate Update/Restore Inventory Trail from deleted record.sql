DECLARE @GUID_Company VARCHAR(MAX) = '1F2F41DD-AD24-4C46-AC15-09C7591C7ADA'
DECLARE @ID_Model_BillingInvoice_Detail VARCHAR(MAX) = ''

----------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @excludeIDs_InventoryTrail_DeletedRecord typIntList

INSERT @excludeIDs_InventoryTrail_DeletedRecord
SELECT TRY_CONVERT(INT, REPLACE(tempID, '', ''))
FROm   tInventoryTrail
where  LEN(tempID) > 0

DELETE FROM @excludeIDs_InventoryTrail_DeletedRecord
where  ID IS NULL

INSERT tInventoryTrail
       ([Code],
        [Name],
        [IsActive],
        [ID_Company],
        [Comment],
        [DateCreated],
        [DateModified],
        [ID_CreatedBy],
        [ID_LastModifiedBy],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [ID_FilingStatus],
        [Date],
        [DateExpired],
        [BatchNo],
        [tempID])
SELECT [Code],
       [Name],
       [IsActive],
       [ID_Company],
       CASE
         WHEN LEN(ISNULL([Comment], '')) > 0 THEN '|'
         ELSE ''
       END
       + 'From ID_InventoryTrail_DeletedRecord - '
       + CONVERT(varchar(max), id),
       [DateCreated],
       [DateModified],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [ID_Item],
       [Quantity],
       [UnitPrice],
       [ID_FilingStatus],
       [Date],
       [DateExpired],
       [BatchNo],
       'From ID_InventoryTrail_DeletedRecord - '
       + CONVERT(varchar(max), id)
FROM   tInventoryTrail_DeletedRecord
where  id not IN (SELECT *
                  FROM   @excludeIDs_InventoryTrail_DeletedRecord)
       and ID_Company = @ID_Company
       AND COnvert(date, Date) BETWEEN '2022-12-01' AND '2022-12-30' 


SELECT * FROM tInventoryTrail where TempID LIKE 'From ID_InventoryTrail_DeletedRecord - %' and ID_Reference IS NULL