DEclare @GUID_Company VARCHAR(MAX) = 'CE106252-85B7-4348-8441-517560A07A7C'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @From_ID_Client INT
DECLARE @To_ID_Client INT
DECLARE @From_ID_Patient INT 
DECLARE @To_ID_Patient INT

SELECT @From_ID_Client = ID
FROM   tClient
WHERE  Code = 'CNT-00741'
       AND ID_Company = @ID_Company

SELECT @To_ID_Client = ID
FROM   tClient
WHERE  Code = 'CNT-00894'
       AND ID_Company = @ID_Company

Select @From_ID_Patient = ID
FROM   vPatient
WHERE  ID_Client = @From_ID_Client
       AND Name = 'Macey'
       AND ID_Company = @ID_Company

Select @To_ID_Patient = ID
FROM   vPatient
WHERE  ID_Client = @To_ID_Client
       AND Name = 'MACEY'
       AND ID_Company = @ID_Company

Select Name_Client,
       Name,
       ID ID_Patient, IsActive
FROM   vPatient
WHERE  ID = @From_ID_Patient

Select Name_Client,
       Name,
       ID ID_Patient, IsActive
FROM   vPatient
WHERE  ID = @To_ID_Patient


SELECT ID, Date, Code, Name_Client, Name_Patient, Name_SOAPType FROm vPatient_SOAP WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

SELECT ID, Date, Code, Name_Client, Name_Patient FROm vPatient_Wellness WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

SELECT ID, Date, Code, Name_Client, Name_Patient FROm vPatient_Vaccination WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient


-- transfer Medical Record 
Update tPatient_SOAP
SET    ID_Client = @To_ID_Client,
       ID_Patient = @To_ID_Patient
WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

-- transfer Medical Record 
Update tPatient_Wellness
SET    ID_Client = @To_ID_Client,
       ID_Patient = @To_ID_Patient
WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

-- transfer Vaccination
Update tPatient_Vaccination
SET    ID_Client = @To_ID_Client,
       ID_Patient = @To_ID_Patient
WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

-- transfer VeterinaryHealthCertificate
Update tVeterinaryCertificate
SET    ID_Client = @To_ID_Client,
       ID_Patient = @To_ID_Patient
WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

Update tVeterinaryHealthCertificate
SET    ID_Client = @To_ID_Client,
       ID_Patient = @To_ID_Patient
WHERE  ID_Company = @ID_Company
       AND ID_Client = @From_ID_Client
       AND ID_Patient = @From_ID_Patient

-- Inactive Julienne Serafin
Update tClient
SET    IsActive = 0
WHERE  ID_Company = @ID_Company
       AND ID = @From_ID_Client

-- Inactive Julienne Serafin
Update tPatient
SET    IsActive = 0
WHERE  ID_Company = @ID_Company
       AND ID = @From_ID_Patient 

SELECT ID, Date, Code, Name_Client, Name_Patient, Name_SOAPType FROm vPatient_SOAP WHERE  ID_Company = @ID_Company
       AND ID_Client = @To_ID_Client
       AND ID_Patient = @To_ID_Patient

SELECT ID, Date, Code, Name_Client, Name_Patient FROm vPatient_Wellness WHERE  ID_Company = @ID_Company
        AND ID_Client = @To_ID_Client
       AND ID_Patient = @To_ID_Patient

SELECT ID, Date, Code, Name_Client, Name_Patient FROm vVeterinaryHealthCertificate WHERE  ID_Company = @ID_Company
       AND ID_Client = @To_ID_Client
       AND ID_Patient = @To_ID_Patient

	   
SELECT ID, Date, Code, Name_Client, Name_Patient FROm vPatient_Vaccination WHERE  ID_Company = @ID_Company
       AND ID_Client = @To_ID_Client
       AND ID_Patient = @To_ID_Patient

