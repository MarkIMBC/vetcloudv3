DECLARE @Guids_Company table
  (
     Guid VARCHAR(MAX)
  )

INSERT @Guids_Company
SELECt Guid
FROM   vCompanyActive company
WHERE  Guid IN ( 'E788C1B2-6229-4F9F-8F7F-1AB92E9B8215', 'E20EEA77-74ED-427C-B8DC-45937F9AA40D', '609FB716-74F0-40D7-AEE4-A51A641212EF', '2315C81F-CD53-4954-A618-5196A4AFF28C',
                 '0870C451-F61A-49D4-8AA5-0B5DD1F43A5C', '81523EFA-8477-42B7-9D20-74BF7B390B15', 'E83D48C4-1E46-4241-A42F-CCBDBC501DE1' )

SELECt *
FROM   vCompanyActive company
       inner join @Guids_Company _guids
               on company.Guid = _guids.Guid

Declare @IDs_Company TypIntList

INSERT @IDs_Company
SELECt ID
FROM   vCompanyActive company
       inner join @Guids_Company _guids
               on company.Guid = _guids.Guid

Update tCompany
set    IsShowPaymentWarningLabel = 1
WHERE  ID IN (SELECT ID
              FROM   @IDs_Company) 
