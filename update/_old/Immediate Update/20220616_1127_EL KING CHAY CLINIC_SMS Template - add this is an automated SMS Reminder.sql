DEclare @GUID_Company VARCHAR(MAX) = '2EEB5DFE-075E-4EB2-B2D8-9160836F014F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT ID,
       Name,
       SOAPPlanSMSMessage,
       REPLACE(SOAPPlanSMSMessage, CHAR(13), '+ ''CHAR(13)'' +')
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.  '
                            + CHAR(13)
                            + ' Pls. contact /*CompanyName*/ /*ContactNumber*/ or message us at our FB Page @elkingchayclinic for further inquiries.'
                            + CHAR(13)
                            + 'This is an automated SMS reminder. PLEASE DO NOT REPLY HERE.'
where  ID = @ID_Company

select Name_Company,
       DateSending,
       Message, LEN( Message) CharacterLength
FROM   dbo.fGetSendSoapPlan(getdate(), NULL, @ID_Company) 
