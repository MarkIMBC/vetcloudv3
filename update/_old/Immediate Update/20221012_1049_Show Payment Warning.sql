DECLARE @Guids_Company Table
  (
     GUID VARCHAR(MAX)
  )

INSERT @Guids_Company
VALUES ('90C12186-3E8C-44FC-A01C-8B1162957F44'),
       ('FB93AC2B-DC42-4FC0-8A94-C7A4E69F9B20'),
       ('95C671AE-5E98-43E9-975E-E3D23EAC6E7B'),
       ('DA9E6202-5553-4C74-BDBE-67DBD2E54A1A')

SELECT Name,
       DateCreated,
       IsShowPaymentWarningLabel,
       DB_NAME()
FROM   vCompanyActive c
       INNER JOIN @Guids_Company guid_
               on c.Guid = guid_.GUID

Update vCompanyActive
SET    IsShowPaymentWarningLabel = 1
FROM   vCompanyActive c
       INNER JOIN @Guids_Company guid_
               on c.Guid = guid_.GUID 
