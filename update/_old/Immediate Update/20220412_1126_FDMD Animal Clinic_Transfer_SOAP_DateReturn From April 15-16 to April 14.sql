IF OBJECT_ID(N'Temp-FDMD-Transfer_April_15_16_To_April_14', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-FDMD-Transfer_April_15_16_To_April_14];
GO
DECLARE @GUID_Company VARCHAR(MAX) = '287BA525-1C3C-4C50-8139-1DAA4669874D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT soapPlan.ID                    ID_Patient_SOAP_Plan,
       soapPlan.DateReturn            DateReturn,
       soap.Code,
       Name_Client,
       Name_Patient,
       ISNULL(soapPlan.Name_Item, '') + ' - '
       + ISNULL(soapPlan.Comment, '') Note,
       ContactNumber_Client
INTO   [Temp-FDMD-Transfer_April_15_16_To_April_14]
FROM   vPatient_SOAP_Plan soapPlan
       Inner join vPatient_SOAP soap
               on soap.ID = soapPlan.ID_Patient_SOAP
WHERE  soap.ID_Company = @ID_Company
       and Convert(Date, soapPlan.DateReturn) BETWEEN '2022-04-15' AND '2022-04-16'
       and ID_FilingStatus NOT IN ( 4 )
Order  by DateReturn,
          Name_Client,
          Name_Patient
SELECT *
FROM   [Temp-FDMD-Transfer_April_15_16_To_April_14]
Order  by DateReturn,
          Name_Client,
          Name_Patient


Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, -1, forTransfer.DateReturn)
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-FDMD-Transfer_April_15_16_To_April_14] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, soapPlan.DateReturn) = '2022-04-15'

Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, -2, forTransfer.DateReturn)
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-FDMD-Transfer_April_15_16_To_April_14] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, soapPlan.DateReturn) = '2022-04-16'

SELECT soapPlan.ID ID_Patient_SOAP_Plan,
       forTransfer.DateReturn,
       soapPlan.DateReturn,
       soap.Code,
       soap.Name_Client,
       soap.Name_Patient,
       forTransfer.Note,
       soap.ContactNumber_Client
FROM   vPatient_SOAP_Plan soapPlan
       inner join [Temp-FDMD-Transfer_April_15_16_To_April_14] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
       Inner join vPatient_SOAP soap
              on soap.ID = soapPlan.ID_Patient_SOAP
WHERE  Convert(Date, forTransfer.DateReturn)  BETWEEN '2022-04-15' AND '2022-04-16'
Order  by forTransfer.DateReturn,
          soap.Name_Client,
          soap.Name_Patient

GO
IF OBJECT_ID(N'Temp-FDMD-Transfer_April_15_16_To_April_14', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-FDMD-Transfer_April_15_16_To_April_14];
GO 
