DEclare @GUID_Company VARCHAR(MAX) = '2F40C744-01EE-4977-9F19-9ED34D5F8A9F'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Supplier           INT,
     Name_Supplier         VARCHAR(MAX),
     ID_Item               INT,
     Name_Item             VARCHAR(MAX),
     UOM_Item              VARCHAR(MAX),
     ID_ItemCategory     INT,
     Name_ItemCategory     VARCHAR(MAX),
     UnitCost              DECIMAL(18, 2),
     UnitPrice             DECIMAL(18, 2),
     DateExpiration        DateTime,
     CurrentInventoryCount INT,
     tempID                VARCHAR(MAX)
  )

INSERT @import
       (Name_Supplier,
        Name_Item,
        UOM_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,
        DateExpiration,
        CurrentInventoryCount,
        tempID)
SELECT dbo.fGetCleanedString([Supplier Name]),
       dbo.fGetCleanedString([Description]),
       dbo.fGetCleanedString([UoM]),
       dbo.fGetCleanedString([Category]),
       TRY_CONVERT(Decimal(18, 2), [Buying Price]),
       TRY_CONVERT(Decimal(18, 2), [Selling Price]),
       TRY_CONVERT(datetime, [Expiration Date]),
       TRY_CONVERT(INT, TRY_CONVERT(decimal, [Critical Level])),
       [GUID]
FROM   ForImport.[dbo].[INVENTORY-2023-TO-VETCLOUD]

Update @import
set    Name_Item = Name_Item + ' - ' + UOM_Item
where  LEN(ISNULL(UOM_Item, 0)) > 0

UPdate @import
set    DateExpiration = NULL
WHERE  DateExpiration = '1900-01-01 00:00:00.000'


UPdate @import
set    Name_ItemCategory = 'Dewormers'
WHERE  Name_ItemCategory = 'Dewormer'

UPdate @import
set    Name_ItemCategory = 'VITAMINS & MINERALS SUPPLEMENTS'
WHERE  Name_ItemCategory = 'Vitamins'


UPdate @import
set    Name_ItemCategory = 'Anti-Tick & Flea'
WHERE  Name_ItemCategory = 'Antitick and Flea'


Update @import
SET    ID_ItemCategory = categoery.ID
FROM   @import import
       inner join tItemCategory categoery
               on import.Name_ItemCategory = categoery.Name
where  categoery.ID_ItemType = @Inventoriable_ID_ItemType
	   




Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join tItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType
	   
Update @import
SET    ID_Supplier = supplier.ID
FROM   @import import
       inner join tSupplier supplier
               on import.Name_Supplier = supplier.Name
where  supplier.ID_Company = @ID_Company

SELECT Distinct Name_ItemCategory
FROM   @import
WHERE ID_ItemCategory IS NULL
Order  by Name_ItemCategory

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory])
SELECT DISTINCT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       @Inventoriable_ID_ItemType,
       cat.ID
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL and cat.ID_ItemType = 2


INSERT INTO [dbo].tSupplier
            ([Name],
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT DISTINCT hed.Name_Supplier,
       @ID_Company,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @import hed
where  hed.ID_Supplier IS NULL and LEN(Name_Supplier) > 0

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join tItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType
	   
Update @import
SET    ID_Supplier = supplier.ID
FROM   @import import
       inner join tSupplier supplier
               on import.Name_Supplier = supplier.Name
where  supplier.ID_Company = @ID_Company


Update tItem
set    UnitCost = import.UnitCost,
       UnitPrice = import.UnitPrice, OtherInfo_DateExpiration = DateExpiration,
	   ID_ItemCategory = import.ID_ItemCategory,
       IsActive = 1
FROM   tItem item
       inner join @import import
               on item.ID = import.ID_Item
WHERE  item.ID_Company = @ID_Company

SELECT *
FROM   @import

SELECT Name_Item,
       COUNT(*)
FROM   @import
GROUP  BY Name_Item
HAVING COUNT(*) > 1 
