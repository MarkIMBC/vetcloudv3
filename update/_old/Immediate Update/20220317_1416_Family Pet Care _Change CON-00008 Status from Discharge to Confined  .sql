DECLARE @GUID_Company VARCHAR(MAX) = '85F52AE1-690F-4D93-852E-C7795341424B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tPatient_Confinement
SET    ID_FilingStatus = 14
WHERE  ID = 5374
       AND ID_Company = @ID_Company

SELECT *
FROM   vPatient_Confinement
WHERE  ID = 5374
       AND ID_Company = @ID_Company 
