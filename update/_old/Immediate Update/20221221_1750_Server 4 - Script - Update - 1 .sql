SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE OR
ALTER view [dbo].[vReRunProcessQueueRecordOngoing]
as
  WITH CTE
       as (SELECT DB_NAME()         DatabaseName,
                  'tBillingInvoice' TableName,
                  c.Name            Name_Company,
                  bi.ID             ID_CurrentObject,
                  bi.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tBillingInvoice bi
                  INNER JOIN vCompanyActive c
                          on bi.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) IN( 9 )
                             ANd ID_FilingStatus NOT IN ( 4 )
                             AND CONVERT(Date, bi.DateModified) BETWEEN DATEADD(DAY, -60, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE())
           UNion ALL
           SELECT DB_NAME()       DatabaseName,
                  'tPatient_SOAP' TableName,
                  c.Name          Name_Company,
                  _soap.ID        ID_CurrentObject,
                  _soap.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tPatient_SOAP _soap
                  INNER JOIN vCompanyActive c
                          on _soap.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) IN( 9 )
                             ANd ID_FilingStatus NOT IN ( 4 )
                             AND CONVERT(Date, _soap.DateModified) BETWEEN DATEADD(DAY, -60, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE()))
  SELECT DatabaseName + TableName
         + CONVERT(varchar(MAX), ID_CurrentObject) Oid,
         *
  FROm   CTE

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER VIEW [dbo].[vBillingInvoice]
AS
  SELECT H.*,
         CONVERT(VARCHAR(100), H.Date, 101)  DateString,
         UC.Name                             AS CreatedBy_Name_User,
         UM.Name                             AS LastModifiedBy_Name_User,
         approvedUser.Name                   AS ApprovedBy_Name_User,
         cancelUser.Name                     AS CanceledBy_Name_User,
         company.Name                        Company,
         h.PatientNames                      Name_Patient,
         taxScheme.Name                      Name_TaxScheme,
         fs.Name                             Name_FilingStatus,
         fsPayment.Name                      Payment_Name_FilingStatus,
         client.Name                         Name_Client,
         CONVERT(VARCHAR, H.DateCreated, 0)  DateCreatedString,
         CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
         CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
         CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString,
         attendingPhysicianEmloyee.Name      AttendingPhysician_Name_Employee,
         CASE
           WHEN H.ID_FilingStatus = 3 THEN fsPayment.Name
           ELSE fs.Name
         END                                 Status,
         soapType.Name                       Name_SOAPType,
         h.Code
         + CASE
             WHEN ISNULL(h.OtherReferenceNumber, '') <> '' THEN ' (' + h.OtherReferenceNumber + ')'
             ELSE ''
           END                               CustomCode,
         confinement.Code                    Code_Patient_Confinement
  FROM   dbo.tBillingInvoice H
         LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tCompany company
                ON H.ID_Company = company.ID
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN dbo.tTaxScheme taxScheme
                ON taxScheme.ID = H.ID_TaxScheme
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tFilingStatus fsPayment
                ON fsPayment.ID = H.Payment_ID_FilingStatus
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN tSOAPType soapType
                ON H.ID_SOAPType = soapType.ID
         LEFT JOIN tPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE OR
ALTER PROC [dbo].[pValidateCompanySMSByDateSchedule](@ID_Company   INT,
                                                     @DateSchedule DATE)
AS
    DECLARE @IsAllowedToSendSMS BIT = 0
    DECLARE @MaxSMSCountPerDay INT = 0
    DECLARE @TotalSMSCount INT = 0

    SELECT @MaxSMSCountPerDay = MaxSMSCountPerDay
    FROM   tCompany_SMSSetting
    WHERE  ID_Company = @ID_Company

    SELECT @TotalSMSCount = COUNT(*)
    FROM   vSentSMSRecord
    WHERE  ID_Company = @ID_Company
           AND CONVERT(Date, DateSchedule) = CONVERT(Date, @DateSchedule)

    SET @IsAllowedToSendSMS = CASE
                                WHEN ( @TotalSMSCount < @MaxSMSCountPerDay ) THEN 1
                                ELSE 0
                              END

    SELECT '_'

    SELECT @ID_Company         ID_Company,
           @DateSchedule       DateSchedule,
           @IsAllowedToSendSMS IsAllowedToSendSMS,
           @TotalSMSCount      TotalSMSCount,
           @MaxSMSCountPerDay  MaxSMSCountPerDay

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROC [dbo].[pUpdatePatientSOAPBillStatus](@IDs_Patient_SOAP               TYPINTLIST READONLY,
                                                @BillingInvoice_ID_FilingStatus INT,
                                                @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @isForBilling BIT = 0;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT
    DECLARE @ForBilling_BillingInvoice_ID_FilingStatus INT = 16

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    BEGIN TRY
        if( @BillingInvoice_ID_FilingStatus = @ForBilling_BillingInvoice_ID_FilingStatus )
          BEGIN
              SET @isForBilling = 1
          END

        UPDATE tPatient_SOAP
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus,
               IsForBilling = @isForBilling
        FROM   tPatient_SOAP _SOAP
               INNER JOIN @IDs_Patient_SOAP ids
                       ON _SOAP.ID = ids.ID;
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE    OR
ALTER PROC [dbo].[pInsertPosition](@Position   VARCHAR(MAX),
                                   @ID_Company INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tPosition
         WHERE  Name = @Position) = 0
        BEGIN
            INSERT INTO [dbo].tPosition
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Position,
                         1,
                         @ID_Company)
        END
  END

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE OR
ALTER PROC [dbo].[pInsertItemCategory](@itemCateogry VARCHAR(MAX),
                                       @ID_ItemType  INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tItemCategory
         WHERE  Name = @itemCateogry
                AND ID_ItemType = @ID_ItemType) = 0
        BEGIN
            INSERT INTO [dbo].tItemCategory
                        ([Name],
                         ID_ItemType,
                         [IsActive],
                         ID_Company)
            VALUES      (@itemCateogry,
                         @ID_ItemType,
                         1,
                         1)
        END
  END

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER proc [dbo].[pGetLoggedUserSession](@ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @IsActive     bit = 0

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @IsActive = IsActive
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT '_'

      SELECT @ID_UserSession         ID,
             CONVERT(BIT, @IsActive) IsActive,
             CONVERT(BIT, @IsActive) IsUserSessionActive
  END

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROC [dbo].[pGetCompanyImageFileNames](@pathSource       VARCHAR(MAX),
                                             @pathDesitination VARCHAR(MAX),
                                             @IDsCompanyString varchar(MAX))
AS
  BEGIN
      DECLARE @IDS_Company typIntList
      DECLARE @IsSuccess BIT = 1
      DECLARE @IsFolderized BIT = 1

      if( LEN(@IDsCompanyString) > 0 )
        BEGIN
            INSERT @IDS_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDS_Company
            SELECT ID
            FROM   tCompany
            WHERE  IsActive = 1
        END

      DECLARE @ImagePaths TABLE
        (
           ID_Company    VARCHAR(MAX),
           ImageFilePath VARCHAR(MAX)
        )

      /* Company */
      INSERT @ImagePaths
      SELECT DISTINCT company.ID,
                      ImageFile
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )
      UNIOn ALL
      SELECT company.ID,
             ImageHeaderFilename
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageHeaderFilename, '') <> '' )
      UNIOn ALL
      SELECT company.ID,
             ImageLogoFilename
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageLogoFilename, '') <> '' )

      /*Employee*/
      INSERT @ImagePaths
      SELECT DISTINCT ID_Company,
                      ImageFile
      FROm   tEmployee emp
             inner join @IDS_Company ids
                     on emp.ID_Company = ids.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )

      /*Patient*/
      INSERT @ImagePaths
      SELECT DISTINCT ID_Company,
                      ProfileImageFile
      FROm   tPatient patient
             inner join @IDS_Company ids
                     on patient.ID_Company = ids.ID
      WHERE  ( ISNULL(ProfileImageFile, '') <> '' )

      /* SOAP */
      INSERT @ImagePaths
      SELECT DISTINCT ID_Company,
                      FilePath
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
             INNER JOIN @IDS_Company idsCompany
                     on idsCompany.ID = ps.ID_Company
      WHERE  ( ISNULL(FilePath, '') <> '' )

      SELECT '_',
             ''                   AS Company,
             'Company.ID_Company' AS ImagePaths

      select @IsSuccess    IsSuccess,
             @IsFolderized IsFolderized

      SELECT DISTINCT CONVERT(INT, imgPath.ID_Company) ID,
                      c.Name                           Name,
                      COUNT(*)                         TotalImagePath
      FROm   @ImagePaths imgPath
             iNNER JOIN tCompany c
                     on imgPath.ID_Company = c.ID
      GROUP  BY CONVERT(INT, imgPath.ID_Company),
                c.Name
      ORDER  BY c.Name

      SELECT CONVERT(INT, imgPath.ID_Company) ID_Company,
             c.Name                           Name_Company,
             imgPath.ImageFilePath            Filename,
             @pathSource + '\api\wwwroot\Content\Image\'
             + imgPath.ImageFilePath          Source,
             @pathDesitination
             + '\api\wwwroot\Content\Image\'
             + imgPath.ImageFilePath          Destination
      FROm   @ImagePaths imgPath
             iNNER JOIN tCompany c
                     on imgPath.ID_Company = c.ID
      where  ImageFilePath NOT LIKE '%&%'
      UNION ALL
      SELECT CONVERT(INT, imgPath.ID_Company) ID_Company,
             c.Name                           Name_Company,
             imgPath.ImageFilePath            Filename,
             @pathSource
             + '\api\wwwroot\Content\Thumbnali\'
             + imgPath.ImageFilePath          Source,
             @pathDesitination
             + '\api\wwwroot\Content\Thumbnali\'
             + imgPath.ImageFilePath          Destination
      FROm   @ImagePaths imgPath
             iNNER JOIN tCompany c
                     on imgPath.ID_Company = c.ID
      where  ImageFilePath NOT LIKE '%&%'
  END

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

CREATE OR ALTER PROC [dbo].[pGetClientMobileNumberByCompany](@Guid_Company varchar(MAX))
AS
  BEGIN
      DECLARE @name VARCHAR(MAX)
      DECLARE @dateStart DateTime
      DECLARE @dateEnd DateTime
      DECLARE @table TABLE
        (
           Name_Company  VARCHAR(MAX),
           ContactNumber VARCHAR(MAX),
           DatabaseName  VARCHAR(MAX)
        )
      DECLARE db_cursorppGetCompanyActive_VetCloudv3 CURSOR FOR
        SELECT name
        FROM   MASTER.dbo.sysdatabases
        WHERE  ( name LIKE 'db_vetcloudv3_%' )

      OPEN db_cursorppGetCompanyActive_VetCloudv3

      FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name

      WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @dateStart = GETDATE();

            BEGIN TRY
                INSERT @table
                exec( 'select Name_Company, ContactNumber, DB_NAME() DatabaseName FROM dbo.fGetClientMobileNumberByCompany('''+ @Guid_Company +''')')

                SET @dateEnd = GETDATE();
            END TRY
            BEGIN CATCH
                DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
                SET @dateEnd = GETDATE();
                SELECT @msg
            END CATCH

            FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name
        END

      CLOSE db_cursorppGetCompanyActive_VetCloudv3

      DEALLOCATE db_cursorppGetCompanyActive_VetCloudv3
  END

SELECT DISTINCT *
FROM   @table
Order  by Name_Company,
          ContactNumber

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @ID_Patient_Vaccination         INT = NULL,
                                      @ID_Patient_Wellness            INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL,
                                      @IsWalkIn                       BIT = 0
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @InitialConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        SET @ID_Patient = NULL

      IF ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Wellness, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_Wellness
            WHERE  ID = @ID_Patient_Wellness
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = ISNULL(CurrentCreditAmount, 0)
            FROM   tPatient_Confinement confinement
                   INNER JOIN tClient client
                           ON confinement.ID_Client = client.ID
            WHERE  confinement.ID = @ID_Patient_Confinement

            SET @InitialConfinementDepositAmount = @ConfinementDepositAmount
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                          Name_FilingStatus,
                   client.Name                      Name_Client,
                   patient.Name                     Name_Patient,
                   client.Address                   BillingAddress,
                   attendingPhysicianEmloyee.Name   AttendingPhysician_Name_Employee,
                   @InitialConfinementDepositAmount InitialConfinementDepositAmount,
                   @ConfinementDepositAmount        ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GetDate()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement,
                           @ID_Patient_Vaccination         ID_Patient_Vaccination,
                           @ID_Patient_Wellness            ID_Patient_Wellness,
                           CASE
                             WHEN @IsWalkIn = 1 THEN 'Walk In Customer'
                             ELSE ''
                           END                             WalkInCustomerName,
                           @IsWalkIn                       IsWalkIn) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      IF ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapTreatment.ID DESC) ) - 799999                    ID,
                   soapTreatment.ID_Item,
                   soapTreatment.Name_Item,
                   ISNULL(soapTreatment.Quantity, 0)                                   Quantity,
                   ISNULL(soapTreatment.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapTreatment.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapTreatment.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Treatment soapTreatment
                   INNER JOIN tItem item
                           ON item.ID = soapTreatment.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
            UNION ALL
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999                    ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                                   Quantity,
                   ISNULL(soapPrescription.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapPrescription.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapPrescription.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
                   AND soapPrescription.IsActive = 1
                   AND soapPrescription.IsCharged = 1
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(itemsservices.UnitPrice, 0)               UnitPrice,
                   ISNULL(itemsservices.UnitCost, 0)                UnitCost,
                   itemsservices.DateExpiration                     DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
        END
      ELSE IF ISNULL(@ID_Patient_Wellness, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY wellnessdetail.ID DESC) ) - 999999 ID,
                   wellnessdetail.ID_Item,
                   wellnessdetail.Name_Item,
                   ISNULL(wellnessdetail.Quantity, 1)                Quantity,
                   ISNULL(wellnessdetail.UnitPrice, 0)               UnitPrice,
                   ISNULL(wellnessdetail.UnitCost, 0)                UnitCost,
                   wellnessdetail.DateExpiration                     DateExpiration,
                   wellnessdetail.ID                                 ID_Patient_Wellness_Detail
            FROM   dbo.vPatient_Wellness_Detail wellnessdetail
                   INNER JOIN tItem item
                           ON item.ID = wellnessdetail.ID_Item
            WHERE  ID_Patient_Wellness = @ID_Patient_Wellness
        END
      ELSE IF ISNULL(@ID_Patient_Vaccination, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY vaccination.ID DESC) ) - 999999 ID,
                   vaccination.ID_Item,
                   vaccination.Name_Item,
                   1                                              Quantity,
                   ISNULL(vaccination.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(vaccination.UnitCost, item.UnitCost)    UnitCost,
                   vaccination.DateExpiration                     DateExpiration,
                   vaccination.ID                                 ID_Patient_Vaccination
            FROM   dbo.vPatient_Vaccination vaccination
                   INNER JOIN tItem item
                           ON item.ID = vaccination.ID_Item
            WHERE  vaccination.ID = @ID_Patient_Vaccination
        END
      ELSE
        BEGIN
            SELECT *,
                   conf.Code               Code_Patient_Confinement,
                   conf.Date               Date_Patient_Confinement,
                   soap_.Code              Code_Patient_SOAP,
                   soap_.Name_FilingStatus Name_FilingStatus_Patient_SOAP,
                   confItemServices.ID_Patient_SOAP_Prescription,
                   confItemServices.ID_Patient_SOAP_Treatment,
                   ''                      Note
            FROM   dbo.vBillingInvoice_Detail biDetail
                   INNER join tBillingInvoice bi
                           on biDetail.ID_BillingInvoice = bi.ID
                   LEFT join vPatient_Confinement_ItemsServices confItemServices
                          on biDetail.ID_Patient_Confinement_ItemsServices = confItemServices.ID
                   LEFT JOIN vPatient_SOAP soap_
                          on soap_.ID = confItemServices.ID_Patient_SOAP
                   LEFT join vPatient_Confinement conf
                          on bi.ID_Patient_Confinement = conf.ID
            WHERE  ID_BillingInvoice = @ID
            Order  by ISNULL(ID_Patient_Confinement_ItemsServices, 100000000) ASC,
                      biDetail.ID ASC
        END

      IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID_Patient,
                   Name_Patient,
                   ID                                 ID_Patient_Confinement_Patient
            FROM   vPatient_Confinement_Patient
            WHERE  ID_Patient_Confinement IN ( @ID_Patient_Confinement )
            ORDER  BY ID DESC
        END
      ELSE IF ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID
        END
  END;

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROC [dbo].[pApproveBillingInvoice_validation] (@IDs_BillingInvoice typIntList READONLY,
                                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @message VARCHAR(MAX) = '';
      /*---------------------------------------------------------------------------------------------------------------------*/
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      /* Validate Billing Invoices Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vBillingInvoice bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_BillingInvoice ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      /* Validate Remaining Inventory */
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @ValidateItemInventory TABLE
        (
           ID_Item  INT,
           ItemName VARCHAR(MAX),
           BIQty    INT,
           RemQty   INT
        )
      Declare @Count_ValidateItemInventory INT = 0

      INSERT @ValidateItemInventory
             (ID_Item,
              ItemName,
              BIQty,
              RemQty)
      SELECT biDetail.ID_Item,
             item.Name,
             SUM(biDetail.Quantity),
             0
      FROM   dbo.tBillingInvoice_Detail biDetail
             INNER JOIN dbo.tBillingInvoice biHed
                     ON biDetail.ID_BillingInvoice = biHed.ID
             INNER JOIN dbo.tItem item
                     ON item.ID = biDetail.ID_Item
             INNER JOIN @IDs_BillingInvoice ids
                     ON ids.ID = biHed.ID
      WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType
      GROUP  BY biDetail.ID_Item,
                item.Name

      UPDATE @ValidateItemInventory
      SET    RemQty = ISNULL(tbl.TotalRemQty, 0)
      FROM   @ValidateItemInventory invtItem
             INNER JOIN (SELECT ID_Item,
                                SUM(Quantity) TotalRemQty
                         FROM   tInventoryTrail
                         GROUP  BY ID_Item) tbl
                     ON invtItem.ID_Item = tbl.ID_Item

      DELETE FROM @ValidateItemInventory
      WHERE  ( ISNULL(RemQty, 0) - ISNULL(BIQty, 0) ) >= 0

      SELECT @Count_ValidateItemInventory = COUNT(*)
      FROM   @ValidateItemInventory

      IF ( @Count_ValidateItemInventory > 0 )
        BEGIN
            SET @message = 'The following item'
                           + CASE
                               WHEN ISNULL(@Count_ValidateItemInventory, 0) > 1 THEN 's are '
                               ELSE ' is '
                             END
                           + 'insufficient inventory count:';

            SELECT TOP 2 @message = @message + CHAR(10) + ISNULL(ItemName, '')
                                    + ': Rem. Qty - '
                                    + CONVERT(varchar(MAX), RemQty) + ' Bi Qty - '
                                    + CONVERT(varchar(MAX), BIQty)
            FROM   @ValidateItemInventory;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      /* Validate Has No TotalAmount */
      DECLARE @ValidateHasNoTotalAmount TABLE
        (
           Code VARCHAR(30)
        );
      DECLARE @Count_ValidateHasNoTotalAmount INT = 0;

      INSERT @ValidateHasNoTotalAmount
             (Code)
      SELECT Code
      FROM   dbo.vBillingInvoice bi
             INNER JOIN @IDs_BillingInvoice ids
                     ON bi.ID = ids.ID
      WHERE  bi.TotalAmount IS NULL
              OR bi.NetAmount IS NULL

      SELECT @Count_ValidateHasNoTotalAmount = COUNT(*)
      FROM   @ValidateHasNoTotalAmount;

      IF ( @Count_ValidateHasNoTotalAmount > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateHasNoTotalAmount > 1 THEN 's have'
                               ELSE ' has '
                             END
                           + 'incorrect computation. Please check and try again';

            SELECT @message = @message + CHAR(10) + Code
            FROM   @ValidateHasNoTotalAmount;

            THROW 50001, @message, 1;
        END;

  /*---------------------------------------------------------------------------------------------------------------------*/
      --/* Validate if the confinement deposit amount is OUTDATED*/  
      --DECLARE @Count_validateOutDatedConfinemntDepositAmount INT = 0;  
      --DECLARE @validateOutDatedConfinemntDepositAmount TABLE  
      --  (  
      --     Code Varchar(100)  
      --  )  
      --INSERT @validateOutDatedConfinemntDepositAmount  
      --SELECT bi.Code  
      --FROM   dbo.vBillingInvoice bi  
      --       INNER JOIN @IDs_BillingInvoice ids  
      --               ON bi.ID = ids.ID  
      --       INNER JOIN tClient client  
      --               on bi.ID_Client = client.ID  
      --WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0  
      --       AND ISNULL(ConfinementDepositAmount, 0) <> ISNULL(client.CurrentCreditAmount, 0)  
      --SELECT @Count_validateOutDatedConfinemntDepositAmount = COUNT(*)  
      --FROM   @validateOutDatedConfinemntDepositAmount;  
      --IF ( @Count_validateOutDatedConfinemntDepositAmount > 0 )  
      --  BEGIN  
      --      SET @message = 'The following record'  
      --                     + CASE  
      --                         WHEN @Count_validateOutDatedConfinemntDepositAmount > 1 THEN 's have'  
      --                         ELSE ' has '  
      --                       END  
      --                     + 'updated Deposit Credit. Please click refresh and save and try to click Approve Button.';  
      --      SELECT @message = @message + CHAR(10) + Code  
      --      FROM   @validateOutDatedConfinemntDepositAmount;  
      --      THROW 50001, @message, 1;  
      --  END;  
      /*---------------------------------------------------------------------------------------------------------------------*/
      DECLARE @ValidateNotDischarge TABLE
        (
           Code_Patient_Confinement              VARCHAR(30),
           Name_FilingStatus_Patient_Confinement VARCHAR(30)
        );
      DECLARE @Count_ValidateNotDischarge INT = 0;

      /* Validate Confinement is not Discharge*/
      INSERT @ValidateNotDischarge
             (Code_Patient_Confinement,
              Name_FilingStatus_Patient_Confinement)
      SELECT confinement.Code,
             confinement.Name_FilingStatus
      FROM   dbo.vBillingInvoice bi
             INNER JOIN @IDs_BillingInvoice ids
                     ON bi.ID = ids.ID
             INNER JOIN vPatient_Confinement confinement
                     on confinement.ID = bi.ID_Patient_Confinement
      WHERE  confinement.ID_FilingStatus NOT IN ( @Discharged_ID_FilingStatus );

      SELECT @Count_ValidateNotDischarge = COUNT(*)
      FROM   @ValidateNotDischarge;

      IF ( @Count_ValidateNotDischarge > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotDischarge > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + ' required to discharge first:';

            SELECT @message = @message + CHAR(10) + Code_Patient_Confinement
                              + ' - '
                              + Name_FilingStatus_Patient_Confinement
            FROM   @ValidateNotDischarge;

            THROW 50001, @message, 1;
        END;
  END;

GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER FUNCTION [dbo].[fGetCompanyUsageList]()
RETURNS @ComapanyUsageList TABLE(
  ID_Company       INT,
  Name_company     VARCHAR(MAX),
  IsActive         bit,
  LastDateAccess   DateTime,
  TotalAccessCount INT,
  LastBillDate     DateTime,
  TotalBillCount   INT,
  LastSOAPDate     DateTime,
  TotalSOAPCount   INT )
AS
  BEGIN
      INSERT @ComapanyUsageList
             (ID_Company,
              Name_company,
              IsActive)
      SELECT ID,
             Name,
             IsActive
      FROM   tCompany c

      Update @ComapanyUsageList
      SET    LastDateAccess = tbl.LastDateAccess,
             TotalAccessCount = tbl.TotalAccessCount
      FROM   @ComapanyUsageList record
             inner join (SELECT c.ID                                ID_Company,
                                c.Name                              Name_Company,
                                CONVERT(DATE, Max(auditTrail.Date)) LastDateAccess,
                                Count(auditTrail.ID)                TotalAccessCount
                         FROM   tCompany c
                                LEFT JOIN vUser _user
                                       on c.ID = _user.ID_Company
                                LEFT JOIN tAuditTrail auditTrail
                                       on auditTrail.ID_User = _user.ID
                         GROUP  BY c.ID,
                                   c.Name) tbl
                     on record.ID_Company = tbl.ID_Company

      Update @ComapanyUsageList
      SET    LastSOAPDate = tbl.LastSOAPDate,
             TotalSOAPCount = tbl.TotalSOAPCount
      FROM   @ComapanyUsageList record
             inner join (SELECT c.ID                                ID_Company,
                                c.Name                              Name_Company,
                                CONVERT(DATE, Max(auditTrail.Date)) LastSOAPDate,
                                Count(auditTrail.ID)                TotalSOAPCount
                         FROM   tCompany c
                                LEFT JOIN tPatient_SOAP auditTrail
                                       on auditTrail.ID_Company = c.ID
                         GROUP  BY c.ID,
                                   c.Name) tbl
                     on record.ID_Company = tbl.ID_Company

      Update @ComapanyUsageList
      SET    LastBillDate = tbl.LastBillDate,
             TotalBillCount = tbl.TotalBillCount
      FROM   @ComapanyUsageList record
             inner join (SELECT c.ID                                ID_Company,
                                c.Name                              Name_Company,
                                CONVERT(DATE, Max(auditTrail.Date)) LastBillDate,
                                Count(auditTrail.ID)                TotalBillCount
                         FROM   tCompany c
                                LEFT JOIN tBillingInvoice auditTrail
                                       on auditTrail.ID_Company = c.ID
                         GROUP  BY c.ID,
                                   c.Name) tbl
                     on record.ID_Company = tbl.ID_Company

      RETURN
  END

GO 
