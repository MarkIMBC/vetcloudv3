DECLARE @AnimalHeartGUID_Company VARCHAR(MAX) = 'C01942F4-1B11-4EFA-BF08-EB7292BA56F7'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @AnimalHeartGUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID IN ( @AnimalHeartGUID_Company )

----------------------------------------------------------------
DECLARE @ID_Company_AnimalHeart INT

SELECT @ID_Company_AnimalHeart = ID
FROM   tCompany
WHERE  Guid = @AnimalHeartGUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ /*DateReturn*/. Please contact /*ContactNumber*/ /*CompanyName*/ for further inquiries. Thank You.'



select top 1 company.Name,
             SOAPPlanSMSMessage,
             ContactNumber_Client
           FROM   vPatient_SOAP_Plan soap_Plan
       inner join vPatient_SOAP _soap
               on soap_Plan.ID_Patient_SOAP = _soap.ID
       inner join tCompany company
               on _soap.ID_Company = company.ID
                  And _soap.ID_Company = @ID_Company_AnimalHeart 
Order by soap_Plan.ID DESC


select top 1 
             dbo.fGetSOAPLANMessage(company.Name, company.SOAPPlanSMSMessage, Name_Client, company.ContactNumber, Name_Patient, Name_Item, soap_Plan.Comment, soap_Plan.DateReturn) Message
FROM   vPatient_SOAP_Plan soap_Plan
       inner join vPatient_SOAP _soap
               on soap_Plan.ID_Patient_SOAP = _soap.ID
       inner join tCompany company
               on _soap.ID_Company = company.ID
                  And _soap.ID_Company = @ID_Company_AnimalHeart 
Order by soap_Plan.ID DESC