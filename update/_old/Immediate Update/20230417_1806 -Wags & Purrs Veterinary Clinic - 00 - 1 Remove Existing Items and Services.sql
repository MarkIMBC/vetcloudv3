DEclare @GUID_Company VARCHAR(MAX) = '5CDC807F-B62D-4871-BCF7-7BC063D9BD35'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company


Update tItem SET iSactive = 0 FROM tItem where ID_Company = @ID_Company