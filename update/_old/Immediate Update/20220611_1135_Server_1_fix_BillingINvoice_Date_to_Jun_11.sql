DECLARE @ID_AuditTrail INT = 150856

DECLARE @TableName VARCHAR(MAX) = 'tBillingInvoice'
DECLARE @IDs_BillingInvoice_Created typIntList
DECLARE @IDs_BillingInvoice_Mdified typIntList

select TOP 100000 _auditTrail.ID,
                  _user.Name_Company,
                  _user.Name_Employee,
                  Date,
                  Description,
                  Model,
                  ID_CurrentObject,
                  TableName
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail
Order  by _auditTrail.ID_CurrentObject DESC

select TOP 100000 _auditTrail.ID,
                  _user.Name_Company,
                  _user.Name_Employee,
                  Date,
                  Description,
                  Model,
                  ID_CurrentObject,
                  TableName
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  _auditTrail.ID = @ID_AuditTrail
Order  by _auditTrail.ID_CurrentObject DESC

INSERT @IDs_BillingInvoice_Created
select Distinct ID_CurrentObject
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail
       AND Description LIKE '%added%'

INSERT @IDs_BillingInvoice_Mdified
select Distinct ID_CurrentObject
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail
       AND Description LIKE '%modified%'

Update tBillingInvoice
set    Date = hbk.Date
FROM   tBillingInvoice h
       inner join db_vetcloudv3_server1_backup.dbo.tBillingInvoice hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_BillingInvoice_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10'

Update tBillingInvoice
set    Date = '2022-06-11 00:00:00'
FROM   tBillingInvoice h
       inner join db_vetcloudv3_server1_backup.dbo.tBillingInvoice hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_BillingInvoice_Created)
       and COnvert(Date, hbk.Date) = '2022-06-10'

Update tPaymentTransaction
SET    Date = hbk.Date
FROM   tPaymentTransaction h
       inner join db_vetcloudv3_server1_backup.dbo.tPaymentTransaction hbk
               on h.ID = hbk.ID
WHERE  h.ID_BillingInvoice IN (SELECT ID
                               FROM   @IDs_BillingInvoice_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10'

Update tPaymentTransaction
SET    Date = '2022-06-11 00:00:00'
FROM   tPaymentTransaction h
       inner join db_vetcloudv3_server1_backup.dbo.tPaymentTransaction hbk
               on h.ID = hbk.ID
WHERE  h.ID_BillingInvoice IN (SELECT ID
                               FROM   @IDs_BillingInvoice_Created
                               WHERE  ID NOT IN (SELECT ID
                                                 FROM   @IDs_BillingInvoice_Mdified))
       and COnvert(Date, hbk.Date) = '2022-06-10'



DELETE FROM @IDs_BillingInvoice_Mdified
WHERE  ID IN (SELECT ID
              FROM   @IDs_BillingInvoice_Created)

SELECT h.ID,
       h.Date,
       hbk.Date,
       h.DateCreated,
       hbk.DateModified
FROM   tBillingInvoice h
       inner join db_vetcloudv3_server1_backup.dbo.tBillingInvoice hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_BillingInvoice_Created)
       and COnvert(Date, hbk.Date) = '2022-06-10'
Order  by h.ID DESC

SELECT 'Addedd PT',
       h.ID,
       h.Date,
       hbk.Date,
       h.DateCreated,
       hbk.DateModified
FROM   tPaymentTransaction h
       inner join db_vetcloudv3_server1_backup.dbo.tPaymentTransaction hbk
               on h.ID = hbk.ID
WHERE  h.ID_BillingInvoice IN (SELECT ID
                               FROM   @IDs_BillingInvoice_Created)
       and COnvert(Date, hbk.Date) = '2022-06-10'

SELECT h.ID,
       h.Date,
       hbk.Date,
       hbk.DateCreated,
       hbk.DateModified
FROM   tBillingInvoice h
       inner join db_vetcloudv3_server1_backup.dbo.tBillingInvoice hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_BillingInvoice_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10'
Order  by h.ID DESC

SELECT 'Modified PT',
       h.ID,
       h.Date,
       hbk.Date,
       hbk.DateCreated,
       hbk.DateModified
FROM   tPaymentTransaction h
       inner join db_vetcloudv3_server1_backup.dbo.tPaymentTransaction hbk
               on h.ID = hbk.ID
WHERE  h.ID_BillingInvoice IN (SELECT ID
                               FROM   @IDs_BillingInvoice_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10' 
