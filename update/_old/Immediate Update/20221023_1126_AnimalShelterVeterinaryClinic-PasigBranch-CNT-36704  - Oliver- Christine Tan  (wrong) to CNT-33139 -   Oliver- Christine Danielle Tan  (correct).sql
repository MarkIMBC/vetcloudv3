/*CNT-36704 Christine Tan  (wrong) to CNT-33139 Christine Danielle Tan  (correct)*/
exec pMergeClientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'CNT-36704',
  'CNT-33139'

/*Merge Oliver*/
exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-61644',
  'PNT-66385'
