GO

GO

ALTER VIEW [dbo].[vBillingInvoice_ListView]
as
  SELECT ID,
         Date,
         Code,
         ID_SOAPType,
         Name_SOAPType,
         Name_Client,
         Name_Patient,
         PatientNames,
         GrossAmount,
         VatAmount,
         NetAmount,
         SubTotal,
         DiscountAmount,
         TotalAmount,
         RemainingAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         AttendingPhysician_Name_Employee,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Payment_ID_FilingStatus,
         Name_FilingStatus,
         Payment_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         ID_Company,
         Comment,
         OtherReferenceNumber,
         Status,
         CASE
           WHEN isnull(OtherReferenceNumber, '') <> '' then 'Other Ref. #: ' + OtherReferenceNumber
                                                            + CHAR(13)
           ELSE ''
         END
         + CASE
             WHEN isnull(Comment, '') <> '' then 'Comment: ' + CHAR(13) + Comment
             ELSE ''
           END TooltipText
  FROM   dbo.vBillingInvoice with (NOLOCK)
  WHERE  ISNULL(IsWalkIn, 0) = 0

GO

ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         IsNull(patientSOAP.Name_SOAPType, '')
         + CASE
             WHEN LEN(IsNull(patientSOAPPlan.Name_Item, '')) > 0 THEN ' - '
                                                                      + IsNull(patientSOAPPlan.Name_Item, '')
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(patientSOAPPlan.Comment, '')) > 0 THEN ' - ' + patientSOAPPlan.Comment
             ELSE ''
           END                                                                                 Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks,
         patientSOAPPlan.DateReschedule,
         patientSOAP.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_SOAP patientSOAP WITH (NOLOCK)
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan WITH (NOLOCK)
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client WITH (NOLOCK)
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model WITH (NOLOCK)
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                                  ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + '0' + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                                           UniqueID,
         _model.Oid                                                                                Oid_Model,
         _model.Name                                                                               Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                                           ID_CurrentObject,
         patientAppnt.ID                                                                           Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                                    DateStart,
         patientAppnt.DateEnd                                                                      DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                              FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                                FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                                FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                                  FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                                ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                               Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                                       Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1)               TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                      ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks,
         patientAppnt.DateReschedule,
         ISNULL(patientAppnt.Groomer_Name_Employee, patientAppnt.AttendingPhysician_Name_Employee) AttendingPhysician_Name_Employee
  FROM   dbo.vPatientAppointment patientAppnt WITH (NOLOCK)
         INNER JOIN tClient client WITH (NOLOCK)
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model WITH (NOLOCK)
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellSched.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull(wellSched.Comment, '')                                        Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks,
                  wellSched.DateReschedule,
                  wellness.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_Wellness wellness WITH (NOLOCK)
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched WITH (NOLOCK)
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client WITH (NOLOCK)
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model WITH (NOLOCK)
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

ALTER VIEW vPatient_SOAP_ListView
AS
  SELECT ID,
         ID_Company,
         ID_SOAPType,
         ID_FilingStatus,
         Date,
         Code,
         Name_SOAPType,
         AttendingPhysician_Name_Employee,
         ID_Client,
         Name_Client,
         Name_Patient,
         Name_FilingStatus,
         ID_Patient,
         ID_Patient_Confinement,
         CaseType,
         IsNull(History, '')                                   History,
         IsNull(ClinicalExamination, '')                       ClinicalExamination,
         IsNull(Interpretation, '')                            Interpretation,
         IsNull(Diagnosis, '')                                 Diagnosis,
         IsNull(Treatment, '')                                 Treatment,
         IsNull(ClientCommunication, '')                       ClientCommunication,
         IsNull(Prescription, '')                              Prescription,
         len(Trim(IsNull(History, '')))                        Count_History,
         len(Trim(IsNull(ClinicalExamination, '')))            Count_ClinicalExamination,
         IsNull(soapLabImgCount.TotalCount, 0)
         + len(Trim(IsNull(Interpretation, '')))               Count_LaboratoryImages,
         len(Trim(IsNull(Diagnosis, '')))                      Count_Diagnosis,
         len(Trim(IsNull(Treatment, '')))                      Count_Treatment,
         len(Trim(IsNull(ClientCommunication, '')))            Count_ClientCommunication,
         IsNull(soapPlanCount.TotalCount, 0)                   Count_Plan,
         IsNull(soapPrescription.TotalCount, 0)
         + len(Trim(IsNull(soap.Prescription, '')))            Count_Prescription,
         soap.DateCreated,
         soap.DateModified,
         'Date Created: '
         + Format(soap.DateCreated, 'MMM. dd, yyyy hh:mm tt')
         + Char(13) + 'Date Modified: '
         + Format(soap.DateModified, 'MMM. dd, yyyy hh:mm tt') TooltipText,
         Comment
  FROM   dbo.vPatient_SOAP soap WITH (NOLOCK)
         LEFT JOIN vPatient_SOAP_Plan_Count soapPlanCount WITH (NOLOCK)
                ON soap.ID = soapPlanCount.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_Prescription_Count soapPrescription WITH (NOLOCK)
                ON soap.ID = soapPrescription.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_LaboratoryImages_Count soapLabImgCount WITH (NOLOCK)
                ON soap.ID = soapLabImgCount.ID_Patient_SOAP
  WHERE  ISNULL(ID_FilingStatus, 0) NOT IN ( 0 )

GO

ALTER view [dbo].[vReRunProcessQueueRecord]
AS
  WITH CTE
       as (SELECT DB_NAME()         DatabaseName,
                  'tBillingInvoice' TableName,
                  c.Name            Name_Company,
                  bi.ID             ID_CurrentObject,
                  bi.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tBillingInvoice bi WITH (NOLOCK)
                  INNER JOIN vCompanyActive c WITH (NOLOCK)
                          on bi.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN( 13 )
                             ANd ID_FilingStatus NOT IN ( 4 )
                             AND CONVERT(Date, bi.DateModified) BETWEEN DATEADD(DAY, -10, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE())
           UNion ALL
           SELECT DB_NAME()       DatabaseName,
                  'tPatient_SOAP' TableName,
                  c.Name          Name_Company,
                  _soap.ID        ID_CurrentObject,
                  _soap.DateModified,
                  RunAfterSavedProcess_ID_FilingStatus
           FROm   tPatient_SOAP _soap WITH (NOLOCK)
                  INNER JOIN vCompanyActive c WITH (NOLOCK)
                          on _soap.ID_Company = c.ID
                             and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN( 13 )
                             --ANd ID_FilingStatus NOT IN (4)                  
                             AND CONVERT(Date, _soap.DateModified) BETWEEN DATEADD(DAY, -10, CONVERT(Date, GETDATE())) AND CONVERT(Date, GETDATE()))
  SELECT DatabaseName + TableName
         + CONVERT(varchar(MAX), ID_CurrentObject) Oid,
         *
  FROm   CTE

GO

create     OR
ALTER proc [dbo].[pGetLoggedUserSession](@ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @IsActive     bit = 0

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @IsActive = IsActive
      FROM   tUserSession WITH (NOLOCK)
      WHERE  ID = @ID_UserSession

      SELECT '_'

      SELECT @ID_UserSession         ID,
             CONVERT(BIT, @IsActive) IsActive,
             CONVERT(BIT, @IsActive) IsUserSessionActive
  END

GO

CREATE OR
ALTER proc pReRun_AfterSaved_Process_BillingInvoice_By_IDs(@IDs_CurrentObject typIntList ReadOnly)
AS
  BEGIN
      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                DECLARE @TranName VARCHAR(MAX);
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

                SET @TranName = 'pReRun_AfterSaved_Process_BillingInvoice_By_IDs-'
                                + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID

                IF(SELECT COUNT(*)
                   FROM   @IDs_CurrentObject) > 0
                  BEGIN
                      DECLARE @IDs_BillingINvoice TYPINTLIST
                      DECLARE @IDs_Patient_Confinement TYPINTLIST
                      /*Update And remove Duplicate Inventory Trails*/
                      DECLARE @IDs_Item typIntList

                      INSERT @IDs_Item
                      SELECT DISTINCT biDetail.ID_Item
                      FROM   tBillingInvoice_Detail biDetail WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on biDetail.ID_BillingInvoice = ids.ID

                      exec pUpdateItemCurrentInventoryByIDsItems
                        @IDs_Item

                      /*Update Confinement Bill Status*/
                      INSERT @IDs_Patient_Confinement
                      SELECT DISTINCT ID_Patient_Confinement
                      FROM   tBillingInvoice bi WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      EXEC PupDatePatient_ConfineMen_BillIngStatus
                        @IDs_Patient_Confinement

                      /*Update SOAP Bill Status*/
                      DECLARE @IDs_Patient_SOAP TYPINTLIST

                      INSERT @IDs_Patient_SOAP
                      SELECT DISTINCT ID_Patient_SOAP
                      FROM   tBillingInvoice bi
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      exec dbo.pModel_AfterSaved_BillingInvoice_Computation
                        @IDs_BillingInvoice

                      EXEC pUpdatePatient_SOAP_BillingStatus
                        @IDs_Patient_SOAP

                      Update tBillingInvoice
                      SET    DateRunAfterSavedProcess = GETDATE(),
                             RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                      FROm   tBillingInvoice bi
                             INNER JOIN @IDs_CurrentObject ids
                                     ON bi.ID = ids.ID
                  END
            END TRY
            BEGIN CATCH
                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID
            END CATCH
        END
  END

GO

CREATE     OR
ALTER PROC pUpdatePatient_SOAP_BillingStatus(@IDs_Patient_SOAP typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @Patient_SOAP TABLE
        (
           ID_Patient_SOAP                INT,
           BillingInvoice_ID_FilingStatus INT
        )
      DECLARE @Patient_SOAP_FromBI TABLE
        (
           ID_Patient_SOAP                INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @Patient_SOAP
      SELECT _soap.ID,
             CASE
               WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus
               ELSE NULL
             END
      FROM   tPatient_SOAP _soap WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _soap.ID = ids.ID

      INSERT @Patient_SOAP_FromBI
      SELECT ID_Patient_SOAP,
             dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])
      FROM   (SELECT idsSOAP.ID ID_Patient_SOAP,
                     fs.ID      BillingInvoice_ID_FilingStatus,
                     COUNT(*)   BillingInvoiceCount
              FROM   vBillingInvoice bi WITH (NOLOCK)
                     inner JOIN @IDs_Patient_SOAP idsSOAP
                             on idsSOAP.ID = bi.ID_Patient_SOAP
                     LEFT JOIN tFilingStatus fs
                            ON fs.Name = bi.Status
              WHERE  ID_FilingStatus NOT IN ( 4 )
              GROUP  BY idsSOAP.ID,
                        fs.ID) AS SourceTable
             PIVOT ( AVG(BillingInvoiceCount)
                   FOR BillingInvoice_ID_FilingStatus IN ([1],
                                                          [3],
                                                          [2],
                                                          [11],
                                                          [12],
                                                          [13]) ) AS PivotTable;

      UPDATE @Patient_SOAP
      SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus
      FROM   @Patient_SOAP soapRec
             INNER JOIN @Patient_SOAP_FromBI soapFromPivot
                     ON soapRec.ID_Patient_SOAP = soapFromPivot.ID_Patient_SOAP

      UPDATE tPatient_SOAP
      SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus
      FROM   tPatient_SOAP soap WITH (NOLOCK)
             INNER JOIN @Patient_SOAP soapRec
                     ON soap.ID = soapRec.ID_Patient_SOAP
  END

GO

CREATE  OR
ALTER PROC pUpdatePatient_Confinemen_BillingStatus (@IDs_Patient_Confinement typIntList READONLY)
AS
  BEGIN
      DECLARE @Cancelled_ID_FilingStatus INT = 4
      DECLARE @ForBilling_ID_FilingStatus INT = 16
      DECLARE @PatientConfinement TABLE
        (
           ID_Patient_Confinement         INT,
           BillingInvoice_ID_FilingStatus INT
        )

      INSERT @PatientConfinement
      SELECT ID,
             NULL
      FROM   @IDs_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = tbl.ID_FilingStatus
      FROM   @PatientConfinement pConfine
             INNER JOIN (SELECT itemservices.ID_Patient_Confinement,
                                CASE
                                  WHEN Count(*) > 0 THEN @ForBilling_ID_FilingStatus
                                  ELSE NULL
                                END ID_FilingStatus
                         FROM   tPatient_Confinement_ItemsServices itemservices WITH (NOLOCK)
                                INNER JOIN tPatient_Confinement confineRec WITH (NOLOCK)
                                        ON confineRec.ID = itemservices.ID_Patient_Confinement
                                INNER JOIN @IDs_Patient_Confinement ids
                                        ON ids.ID = itemservices.ID_Patient_Confinement
                         WHERE  confineRec.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )
                         GROUP  BY itemservices.ID_Patient_Confinement) tbl
                     on tbl.ID_Patient_Confinement = pConfine.ID_Patient_Confinement

      Update @PatientConfinement
      SET    BillingInvoice_ID_FilingStatus = fs.ID
      FROM   @PatientConfinement ids
             INNER JOIN vBillingInvoice biHed WITH (NOLOCK)
                     on ids.ID_Patient_Confinement = biHed.ID_Patient_Confinement
             LEFT JOIN tFilingStatus fs
                    on fs.Name = biHed.Status
      WHERE  biHed.ID_FilingStatus NOT IN ( @Cancelled_ID_FilingStatus )

      Update tPatient_Confinement
      SET    BillingInvoice_ID_FilingStatus = ids.BillingInvoice_ID_FilingStatus
      FROM   tPatient_Confinement hed WITH (NOLOCK)
             inner join @PatientConfinement ids
                     ON ids.ID_Patient_Confinement = hed.ID
  END

GO

CREATE OR
ALTER proc pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs(@IDs_CurrentObject typIntList ReadOnly)
as
  BEGIN
      DECLARE @TranName VARCHAR(MAX);
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

      SET @TranName = 'pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs-'
                      + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID

                DECLARE @IDs_Patient_SOAP TYPINTLIST

                INSERT @IDs_Patient_SOAP
                SELECT ID
                FROM   @IDs_CurrentObject

                EXEC dbo.pUpdatPatientConfinementItemsServicesByPatientSOAP
                  @IDs_Patient_SOAP

                --------------------------------------------        
                exec pRemoveMulticatePatientConfinementFromSOAP
                  @IDs_Patient_SOAP

                --------------------------------------------        
                DECLARE @IDs_Patient TYPINTLIST

                INSERT @IDs_Patient
                SELECT patient.ID
                FROM   tPatient_SOAP soap
                       INNER JOIN tPatient patient
                               ON patient.ID = soap.ID_Patient
                       INNER JOIN @IDs_CurrentObject ids
                               ON soap.ID = ids.ID

                EXEC dbo.PupDatePatientsLastVisitedDate
                  @IDs_Patient

                --------------------------------------------        
                EXEC _pCancelPatient_Confinement_temp

                -----------------------------------------------    
                exec pCancelMulticatePatient_SOAP_Record
                  @IDs_Patient_SOAP

                exec pUpdatePatient_SOAP_BillingStatus
                  @IDs_Patient_SOAP

                Update tPatient_SOAP
                SET    DateRunAfterSavedProcess = GETDATE(),
                       RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID
            END TRY
            BEGIN CATCH
                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID
            END CATCH
        END
  END

GO

alter PROC dbo.pCancelPatient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_ClientDeposit typIntList

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tClientDeposit cd with (NOLOCK)
                 inner join @IDs_Patient_Confinement idsCOnfinement
                         ON cd.ID_Patient_Confinement = idsCOnfinement.ID
                            and ID_FilingStatus = @Approved_ID_FilingStatus

          exec [pCancelPatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          EXEC dbo.pCancelClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Confinement bi with (NOLOCK)
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          Update tPatient_SOAP
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   tPatient_SOAP soap with (NOLOCK)
                 INNER JOIN tPatient_Confinement confinement with (NOLOCK)
                         ON soap.ID_Patient_Confinement = confinement.ID
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON confinement.ID = ids.ID;

          exec [pCancelClientDeposit]
            @IDs_ClientDeposit,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @records TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @records2 TABLE
        (
           ID_Client                      INT,
           ID_Patient                     INT,
           ID_CurrentObject               INT,
           AttendingPhysician_ID_Employee INT,
           DateLastVisited                DATETIME,
           TableName                      VARCHAR(MAX),
           DateCreated                    DATETIME,
           Count                          Int
        )
      DECLARE @MaxRecords TABLE
        (
           ID_Patient  INT,
           DateCreated DATETIME
        )

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_SOAP hed WITH (NOLOCK)
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_SOAP'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @records
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT hed.ID_Patient,
             model.TableName,
             MAX(hed.DateCreated),
             Count(*)
      FROM   tPatient_Wellness hed WITH (NOLOCK)
             inner JOIN @IDs_Patient tbl
                     ON tbl.ID = hed.ID_Patient,
             _tModel model
      WHERE  ID_FilingStatus IN ( 1, 3, 13 )
             and model.TableName = 'tPatient_Wellness'
      GROUP  BY hed.ID_Patient,
                model.TableName

      INSERT @MaxRecords
             (ID_Patient,
              DateCreated)
      SELECT ID_Patient,
             MAX(DateCreated)
      FROM   @records
      GROUP  BY ID_Patient

      INSERT @records2
             (ID_Patient,
              TableName,
              DateCreated,
              Count)
      SELECT record.ID_Patient,
             record.TableName,
             record.DateCreated,
             record.Count
      FROM   @records record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_SOAP hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_SOAP'

      Update @records2
      SET    ID_CurrentObject = hed.ID,
             ID_Client = hed.ID_Client,
             AttendingPhysician_ID_Employee = hed.AttendingPhysician_ID_Employee,
             DateLastVisited = hed.Date
      FROM   @records2 record
             INNER JOIN @MaxRecords maxRecord
                     on record.ID_Patient = maxRecord.ID_Patient
                        AND record.DateCreated = maxRecord.DateCreated
             inner join tPatient_Wellness hed
                     on hed.ID_Patient = maxRecord.ID_Patient
                        AND hed.DateCreated = maxRecord.DateCreated
             INNER JOIN @IDs_Patient patient
                     on hed.ID_Patient = patient.ID
      where  record.TableName = 'tPatient_Wellness'

      --SELECT *  
      --FROM   @records2 record  
      UPDATE tPatient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tPatient patient WITH (NOLOCK)
             INNER JOIN @records2 record
                     ON record.ID_Patient = patient.ID

      UPDATE tClient
      SET    DateLastVisited = record.DateLastVisited,
             LastAttendingPhysician_ID_Employee = record.AttendingPhysician_ID_Employee
      FROM   tClient client WITH (NOLOCK)
             INNER JOIN @records2 record
                     ON record.ID_Client = client.ID
  END

GO

SELECT top 10000 *
FROM   vPatient_SOAP_ListView

GO

SELECT *
FROM   vReRunProcessQueueRecord

GO

SELECT *
FROM   vCompanyActiveUser
Order  by Name_Company 
