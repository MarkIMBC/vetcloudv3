DEclare @GUID_Company VARCHAR(MAX) = '2F40C744-01EE-4977-9F19-9ED34D5F8A9F'
DEclare @Service_ID_ClientType INT = 1

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Client             INT,
     Name_Client           VARCHAR(MAX),
     ContactNumber_Client  VARCHAR(MAX),
     ContactNumber2_Client VARCHAR(MAX),
     tempID_Client         VARCHAR(MAX),
     ID_Patient            INT,
     CustomCode_Patient    VARCHAR(MAX),
     Name_Patient          VARCHAR(MAX),
     Specie_Patient        VARCHAR(MAX),
     tempID_Patient        VARCHAR(MAX),
     RowIndex              INT
  )

INSERT INTO @import
            (Name_Client,
             CustomCode_Patient,
             ContactNumber_Client,
             tempID_Client,
             Name_Patient,
             Specie_Patient,
             tempID_Patient,
             RowIndex)
SELECT dbo.fGetCleanedString([client name]),
       dbo.fGetCleanedString([ref_id]),
       dbo.fGetCleanedString([contact no]),
       dbo.fGetCleanedString([GUID] + '-Client'),
       dbo.fGetCleanedString([patient name]),
       dbo.fGetCleanedString([breed]),
       dbo.fGetCleanedString([GUID]),
       RowIndex
FROM   ForImport.[dbo].[client-and-patients-list-Dr S&J Veterinary Clinic] import
ORDER  BY RowIndex

-------------------------------------------------
Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.tempID_Client = client.tempID
where  client.ID_Company = @ID_Company

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.tempID_Patient = patient.tempID
where  patient.ID_Company = @ID_Company

------------------------------------------------------------------
Update @import
set    ContactNumber_Client = REPLACE(ContactNumber_Client, '-', '')

--------------------------------------------------------------------
DECLARE @SelectedClient TABLE
  (
     RowIndex Int
  )

INSERT @SelectedClient
       (RowIndex)
SELECT MIN(RowIndex)
FROM   @import hed
where  hed.ID_Client IS NULL
GROUP  By Name_Client

INSERT INTO [dbo].tClient
            ([ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified])
SELECT @ID_Company,
       hed.tempID_Client,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE()
FROM   @import hed
       INNER JOIN @SelectedClient client
               on hed.RowIndex = client.RowIndex


Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.tempID_Client = client.tempID
where  client.ID_Company = @ID_Company

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.tempID_Patient = patient.tempID
where  patient.ID_Company = @ID_Company

-------------------------------------------------------------------
DECLARE @ClientContactNumber TABLE
  (
     ID_Client     INT,
     ContactNumber VARCHAR(MAX),
     [Index]       VARCHAR(MAX)
  )
DECLARE @ID_Client INT = 0
DECLARE @LastID_Client INT = 0
DECLARE @LastTempID_Client VARCHAR(MAX) = ''
DECLARE @ContactNumber_Client VARCHAR(MAX) = ''
DECLARE @tempID_Client VARCHAR(MAX) = ''
DECLARE @RowIndex INT
DECLARE @ClientMapping TABLE
  (
     RowIndex      INT,
     ID_Client     INT,
     tempID_Client VARCHAR(MAX)
  )
DECLARE db_cursor CURSOR FOR
  SELECT ID_Client,
         ContactNumber_Client,
         tempID_Client,
         RowIndex
  FROM   @import
  ORDER  BY Name_Client, RowIndex

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_Client, @ContactNumber_Client, @tempID_Client, @RowIndex

WHILE @@FETCH_STATUS = 0
  BEGIN
  
      INSERT @ClientContactNumber (ID_Client, ContactNumber, [Index])
      SELECT @ID_Client , Part, ID FROM dbo.fGetSplitString(@ContactNumber_Client, '/')

      IF( ISNULL(@ID_Client, 0) = 0 )
        BEGIN
            INSERT @ClientMapping
                   (RowIndex,
                    ID_Client,
                    TempID_Client)
            SELECT @RowIndex,
                   @LastID_Client,
                   @LastTempID_Client
        END

      IF( ISNULL(@ID_Client, 0) > 0 )
        BEGIN
            SET @LastID_Client = ISNULL(@ID_Client, 0)
            SET @LastTempID_Client = @tempID_Client
        END


      FETCH NEXT FROM db_cursor INTO @ID_Client, @ContactNumber_Client, @tempID_Client, @RowIndex
  END

CLOSE db_cursor

DEALLOCATE db_cursor

Update @import
Set    ID_Client = clientmap.ID_Client,
       tempID_Client = clientmap.tempID_Client
FROM   @import import
       INNER JOIN @ClientMapping clientmap
               on import.RowIndex = clientmap.RowIndex

Update @import
SET    import.ContactNumber_Client = COntactNumber.ContactNumber
FROM   @import import
       INNER JOIn @ClientContactNumber contactNumber
               on import.ID_Client = COntactNumber.ID_CLient
WHERE  COntactNumber.[Index] = 1

Update @import
SET    import.ContactNumber2_Client = COntactNumber.ContactNumber
FROM   @import import
       INNER JOIn @ClientContactNumber contactNumber
               on import.ID_Client = COntactNumber.ID_CLient
WHERE  COntactNumber.[Index] = 2


SELECT * FROM @ClientContactNumber



INSERT INTO [dbo].tPatient
            ([ID_Company],
             ID_Client,
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified])
SELECT @ID_Company,
       ID_Client,
       hed.tempID_Patient,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE()
FROM   @import hed
where  hed.ID_Patient IS NULL and ID_Client IS NOT NULL


Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.tempID_Patient = patient.tempID
where  patient.ID_Company = @ID_Company



Update tClient
set    Name = import.Name_Client,
       COntactNumber = import.ContactNumber_Client, ContactNumber2 =  import.ContactNumber2_Client
FROM   tClient client
       inner join @import import
               on client.ID = import.ID_Client

Update tPatient
set    Name = import.Name_Patient, ID_Client = patient.ID_Client,
       Species = import.Specie_Patient, CustomCode = import.CustomCode_Patient
FROM   tPatient patient
       inner join @import import
               on patient.ID = import.ID_Patient

SELECT *
FROM   @import
order  by RowIndex 
