Declare @From_IDs_RecordValue typIntList
Declare @To_ID_RecordValue INT = 496337

INSERT @From_IDs_RecordValue
VALUES ( 496627)

exec pAddRecordValueTransferLog
  @From_IDs_RecordValue,
  @To_ID_RecordValue,
  'ID',
  'ID_Patient',
  'tPatient_SOAP',
  'Luna - Jess Ampoloquio to Luna Jess/Luna Ampoloquio Medical Record'

exec pAddRecordValueTransferLog
  @From_IDs_RecordValue,
  @To_ID_RecordValue,
  'ID',
  'ID_Patient',
  'tPatient_Wellness',
  'Luna - Jess Ampoloquio to Luna Jess/Luna Ampoloquio Wellness'

exec pAddRecordValueTransferLog
  @From_IDs_RecordValue,
  @To_ID_RecordValue,
  'ID',
  'ID_Patient',
  'tBillingInvoice_Patient',
  'Luna - Jess Ampoloquio to Luna Jess/Luna Ampoloquio Billing'

/*Client*/
DELETE FROM @From_IDs_RecordValue

INSERT @From_IDs_RecordValue
VALUES (372633)

SET @To_ID_RecordValue = 372436

exec pAddRecordValueTransferLog
  @From_IDs_RecordValue,
  @To_ID_RecordValue,
  'ID',
  'ID_Client',
  'tPatient',
  'St. Hyacinth - Main CNT-03143 pamerge kay CNT-03127 - Patient Record'

exec pAddRecordValueTransferLog
  @From_IDs_RecordValue,
  @To_ID_RecordValue,
  'ID',
  'ID_Client',
  'tBillingInvoice',
  'St. Hyacinth - Main CNT-03143 pamerge kay CNT-03127 - Billing Invoice Record'

Update tPatient
SET    IsActive = 0
WHERE  ID = 496627

Update tClient
SET    IsActive = 0
WHERE  ID = 372633

SELECT *
FROM   tRecordValueTransferLogs 
