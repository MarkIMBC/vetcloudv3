DEclare @GUID_Company VARCHAR(MAX) = 'BB6A855D-638B-4A00-A5B4-887ACEFA7A1E'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item   INT,
     Name_Item VARCHAR(MAX),
	 UnitCost Decimal(18, 2),
	 UnitPrice Decimal(18, 2),
     Quantity  INT
  )

INSERT @forImport
       (
        Name_Item,
		UnitCost,
		UnitPrice,
        Quantity)
SELECT 
       import.Item,
	    [Buying Price]  ,
	    [Selling Price], 
       CONVERT(INT, import.[Quantity])
FROM   [VET_CLOUD_PRODUCT_SUMMARY] import


uPDATE @forImport set ID_Item = item.id
froM @forImport import 
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company


SELECT *
FROm   @forImport WHERE ID_Item is nulL


Update tItem SET UnitCost = import.UnitCost, UnitPrice = import.UnitPrice
FROM   tItem item
       inner join   @forImport import
               on import.ID_Item = item.ID
where ID_Company = @ID_Company

--Declare @adjustInventory typReceiveInventory
--Declare @ID_UserSession int

--SELECT @ID_UserSession = MAX(ID)
--FROM   tUserSession
--WHERE  ID_User = 10

--INSERT @adjustInventory
--       ([Code],
--        [ID_Item],
--        [Quantity],
--        [UnitPrice],
--        [DateExpired],
--        [BatchNo],
--        [ID_FilingStatus],
--        [ID_Company],
--        [Comment],
--        [IsAddInventory])
--SELECT 'Reset inventory on '
--       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
--       ID_Item,
--       item.CurrentInventoryCount,
--       0.00,
--       NULL,
--       NULL,
--       3,
--       @ID_Company,
--       'Reset inventory on '
--       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
--       0
--FROM   @forImport import
--       inner join tItem item
--               on import.ID_Item = item.ID
--WHERE  item.CurrentInventoryCount > 0
--ORDER  BY ID_Item

--SELECT *
--FROM   @adjustInventory 

--exec pReceiveInventory
--  @adjustInventory,
-- @ID_UserSession

--DELETE FROM @adjustInventory

--INSERT @adjustInventory
--       ([Code],
--        [ID_Item],
--        [Quantity],
--        [UnitPrice],
--        [DateExpired],
--        [BatchNo],
--        [ID_FilingStatus],
--        [ID_Company],
--        [Comment],
--        [IsAddInventory])
--SELECT 'Import Inventory Count '
--       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
--       ID_Item,
--       import.Quantity,
--       0.00,
--       NULL,
--       NULL,
--       3,
--       @ID_Company,
--       'Import Inventory Count '
--       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
--       1
--FROM   @forImport import
--       inner join tItem item
--               on import.ID_Item = item.ID
--WHERE  import.Quantity > 0
--ORDER  BY ID_Item

--SELECT *
--FROM   @adjustInventory 

--INSERT @forImport
--       (ID_Item,
--        Name_Item,
--        Quantity)
--SELECT item.ID,
--       item.Name,
--       CONVERT(INT, import.[Quantity])
--FROM   [VET_CLOUD_PRODUCT_SUMMARY] import
--       inner join tItem item
--               on import.Item = item.Name
--WHERE  ID_Company = @ID_Company

--exec pReceiveInventory
--  @adjustInventory,
-- @ID_UserSession
  
