GO

GO

CREATE  OR
ALTER PROC pInsertItemCategory(@ItemCategory VARCHAR(MAX),
                               @ID_ItemType  INT = 0,
                               @ID_Company   INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tItemCategory
         WHERE  Name = @ItemCategory
                and ID_ItemType = @ID_ItemType) = 0
        BEGIN
            INSERT INTO [dbo].tItemCategory
                        ([Name],
                         [IsActive],
                         ID_ItemType,
                         ID_Company)
            VALUES      (@ItemCategory,
                         1,
                         @ID_ItemType,
                         @ID_Company)
        END
  END

GO

exec pInsertItemCategory
  'PHARMACY',
  2,
  1

GO

DECLARE @GUID_Company VARCHAR(MAX) = '99F63150-DA14-4A67-90F4-73C4F719CD4F'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item               INT,
     Name_Item             VARCHAR(MAX),
     ID_ItemCategory       INT,
     UnitCost              DECIMAL(18, 2),
     UnitPrice             DECIMAL(18, 2),
     CurrentInventoryCount INT,
     Import_Category       VARCHAR(MAX),
     GUID                  VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitCost,
        UnitPrice,
		CurrentInventoryCount,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([PRODUCTS]),
       TRY_CONVERT(decimal, REPLACE(dbo.fGetCleanedString([Buying Price ]), ',', '')),
       TRY_CONVERT(decimal, REPLACE(dbo.fGetCleanedString([Selling Price]), ',', '')),
	   TRY_CONVERT(INT, REPLACE(dbo.fGetCleanedString([INITIAL]), ',', '')),
       dbo.fGetCleanedString([Category]),
       [GIUD]
FROM   ForImport.[dbo].[24_Seven Veterinary Clinic-Import Products]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @forImport
set    Name_Item = 'Bladder Control 1'
where  GUID = '5A07D024-8EE6-4978-AFAD-D2E4A636BF49-[24_Seven Veterinary Clinic-Import Products]'

Update @forImport
set    Name_Item = 'Canine Hartworm Test Kit 1'
where  GUID = '3957BC2C-7202-4521-B4FF-F3CCA5B44912-[24_Seven Veterinary Clinic-Import Products]'

Update @forImport
set    Import_Category = 'Laboratory Test & Kits'
where  Import_Category = 'TEST KIT'

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType
       and category.IsActive = 1

SELECT Import_Category,
       COUNT(*)
FROM   @forImport
WHERE  ID_ItemCategory IS NULL
GROUP  BY Import_Category

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice,
       Name = import.Name_Item
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 

------------------------------------------------------------------------------------------
Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset Inventory',
       ID_Item,
       CurrentInventoryCount,
       UnitPrice,
       NUll,
       NULL,
       3,
       @ID_Company,
       'Initial Inventory on ' + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       1
FROM   @forImport
       WHERE   ISNULL(CurrentInventoryCount, 0) > 0


Declare @ID_UserSession INT

select TOP 1 @ID_UserSession = ID
FRom   tUsersession
where  ID_User = 1
ORDER  BY ID DESC

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

  
	   SELECT * FROM @adjustInventory