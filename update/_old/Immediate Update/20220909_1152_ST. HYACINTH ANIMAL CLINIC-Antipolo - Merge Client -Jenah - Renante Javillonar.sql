DEclare @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 324952 -- Renante Javillonor
DECLARE @Source2_ID_Client INT = 421475 -- JENAH JAVILLONAR
DECLARE @Destination_ID_Client INT = 425253 -- Jenah / Renante Javillonar

SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Source 2'
FROm   vClient
WHERE  ID = @Source2_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'ST. HYACINTH ANIMAL CLINIC-Antipolo - Merge Client - Renante Javillonor to Jenah / Renante Javillonar'

exec pMergeClientRecord
  @Source2_ID_Client,
  @Destination_ID_Client,
  'ST. HYACINTH ANIMAL CLINIC-Antipolo - Merge Client - JENAH JAVILLONAR to Jenah / Renante Javillonar'

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company

Update tClient
SET    IsActive = 0
WHERE  ID = @Source2_ID_Client
       and ID_Company = @ID_Company

----------------------------------Pet: CELINE   ------------------------------------------------------
DECLARE @Source1_ID_Patient INT = 573111 --PNT-04128  
DECLARE @Destination_ID_Patient INT = 426409 -- PNT-01120  

exec pMergePatientRecord
  @Source1_ID_Patient,
  @Destination_ID_Patient,
  'ST. HYACINTH ANIMAL CLINIC-Antipolo - Merge Patient - CELINE From PNT-04128 to PNT-01120' 
  
Update tPatient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company
