DECLARE @GUID_Company VARCHAR(MAX) = '96BE41FF-0C98-44D3-9D9B-9D3DB1C4D7AA'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @reschedReference Table
  (
     Code           VARCHAR(MAX),
     DateReturn     DateTime,
     DateReSchedule DateTime
  )

INSERT @reschedReference
SELECT *
FROM   (SELECT soap.Code,
               DateReturn,
               DATEADD(DAY, 2, soapplan.DateReturn) DateSchedule
        FROm   tPatient_SOAP_Plan soapplan
               INNER JOIN tPatient_SOAP soap
                       ON soapplan.ID_Patient_SOAP = soap.ID
        WHERE  CONVERT(Date, soapplan.DateReturn) IN ( '2022-03-23' )
               AND soap.ID_Company = @ID_Company
        UNION ALL
        SELECT soap.Code,
               DateReturn,
               DATEADD(DAY, 1, soapplan.DateReturn) DateSchedule
        FROm   tPatient_SOAP_Plan soapplan
               INNER JOIN tPatient_SOAP soap
                       ON soapplan.ID_Patient_SOAP = soap.ID
        WHERE  CONVERT(Date, soapplan.DateReturn) IN ( '2022-03-24' )
               AND soap.ID_Company = @ID_Company
        UNION ALL
        SELECT wellness.Code,
               schedule.Date,
               DATEADD(DAY, 2, schedule.Date) DateSchedule
        FROm   tPatient_Wellness_Schedule schedule
               INNER JOIN tPatient_Wellness wellness
                       ON schedule.ID_Patient_Wellness = wellness.ID
        WHERE  CONVERT(Date, schedule.Date) = '2022-03-23'
               AND wellness.ID_Company = @ID_Company
        UNION ALL
        SELECT wellness.Code,
               schedule.Date,
               DATEADD(DAY, 1, schedule.Date) DateSchedule
        FROm   tPatient_Wellness_Schedule schedule
               INNER JOIN tPatient_Wellness wellness
                       ON schedule.ID_Patient_Wellness = wellness.ID
        WHERE  CONVERT(Date, schedule.Date) = '2022-03-24'
               AND wellness.ID_Company = @ID_Company) tbl
order  by tbl.DateReturn

SELECT *
FROm   @reschedReference

--Update tPatient_SOAP_Plan
--SET    DateReturn = DATEADD(DAY, 2, soapplan.DateReturn)
--FROm   tPatient_SOAP_Plan soapplan
--       INNER JOIN tPatient_SOAP soap
--               ON soapplan.ID_Patient_SOAP = soap.ID
--WHERE  CONVERT(Date, soapplan.DateReturn) = '2022-03-23'
--       AND soap.ID_Company = @ID_Company

--Update tPatient_SOAP_Plan
--SET    DateReturn = DATEADD(DAY, 1, soapplan.DateReturn)
--FROm   tPatient_SOAP_Plan soapplan
--       INNER JOIN tPatient_SOAP soap
--               ON soapplan.ID_Patient_SOAP = soap.ID
--WHERE  CONVERT(Date, soapplan.DateReturn) = '2022-03-24'
--       AND soap.ID_Company = @ID_Company

--Update tPatient_Wellness_Schedule
--SET    Date = DATEADD(DAY, 2, schedule.Date)
--FROm   tPatient_Wellness_Schedule schedule
--       INNER JOIN tPatient_Wellness wellness
--               ON schedule.ID_Patient_Wellness = wellness.ID
--WHERE  CONVERT(Date, schedule.Date) = '2022-03-23'
--       AND wellness.ID_Company = @ID_Company

--Update tPatient_Wellness_Schedule
--SET    Date = DATEADD(DAY, 1, schedule.Date)
--FROm   tPatient_Wellness_Schedule schedule
--       INNER JOIN tPatient_Wellness wellness
--               ON schedule.ID_Patient_Wellness = wellness.ID
--WHERE  CONVERT(Date, schedule.Date) = '2022-03-24'
--       AND wellness.ID_Company = @ID_Company

SELECT Distinct *
FROM   (SELECT soap.Code,
               DateReturn,
			   Name_Client, Name_Patient,
               soapplan.Comment
        FROm   tPatient_SOAP_Plan soapplan
               INNER JOIN vPatient_SOAP soap
                       ON soapplan.ID_Patient_SOAP = soap.ID
        WHERE  CONVERT(Date, soapplan.DateReturn) IN ( '2022-03-25' )
               AND soap.ID_Company = @ID_Company
        UNION ALL
        SELECT soap.Code,
               DateReturn,
			   Name_Client, Name_Patient,
               soapplan.Comment
        FROm   tPatient_SOAP_Plan soapplan
               INNER JOIN vPatient_SOAP soap
                       ON soapplan.ID_Patient_SOAP = soap.ID
        WHERE  CONVERT(Date, soapplan.DateReturn) IN ( '2022-03-25' )
               AND soap.ID_Company = @ID_Company
        UNION ALL
        SELECT wellness.Code,
               schedule.Date,
			   Name_Client, Name_Patient,
               schedule.Comment
        FROm   tPatient_Wellness_Schedule schedule
               INNER JOIN vPatient_Wellness wellness
                       ON schedule.ID_Patient_Wellness = wellness.ID
        WHERE  CONVERT(Date, schedule.Date) = '2022-03-25'
               AND wellness.ID_Company = @ID_Company
        UNION ALL
        SELECT wellness.Code,
               schedule.Date,
			   Name_Client, Name_Patient,
               schedule.Comment
        FROm   tPatient_Wellness_Schedule schedule
               INNER JOIN vPatient_Wellness wellness
                       ON schedule.ID_Patient_Wellness = wellness.ID
        WHERE  CONVERT(Date, schedule.Date) = '2022-03-25'
               AND wellness.ID_Company = @ID_Company) tbl
       inner join @reschedReference resched
               on resched.Code = tbl.Code
                  and resched.DateReSchedule = tbl.DateReturn
order  by tbl.DateReturn 
