DECLARE @Guids_Company table
  (
     Guid VARCHAR(MAX)
  )

INSERT @Guids_Company
values ('2C824598-B78D-4EE7-AB61-C85AA337D8CC'),
       ('1340AF32-C28B-402D-B54B-63C15488D6DE'),
       ('21A72433-49E9-4D92-8CBD-E413534667EC'),
       ('ACE352E0-1293-4416-AB07-B8A611F37F6C'),
       ('E405F420-CE8E-490E-9A51-5DF9B096F5C0')

SELECt *
FROM   vCompanyActive company
       inner join @Guids_Company _guids
               on company.Guid = _guids.Guid

Declare @IDs_Company TypIntList

INSERT @IDs_Company
SELECt ID
FROM   vCompanyActive company
       inner join @Guids_Company _guids
               on company.Guid = _guids.Guid

Update tCompany
set    IsShowPaymentWarningLabel = 0
WHERE  ID IN (SELECT ID
              FROM   @IDs_Company)

SELECT *
FROm   vUser
WHERE  ID_Company IN (SELECT ID
                      FROM   @IDs_Company) 
