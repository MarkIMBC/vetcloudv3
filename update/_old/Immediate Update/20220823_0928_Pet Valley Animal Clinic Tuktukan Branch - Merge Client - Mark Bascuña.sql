DEclare @GUID_Company VARCHAR(MAX) = '91C3CEF9-1B32-4C1D-BB92-843F4B1116AA'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 179027 -- Mark Bascuna
DECLARE @Destination_ID_Client INT = 179028 -- Mark Bascu�a
SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - Mark Bascu�a from CNT-31578 to CNT-31579'

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company 

Update tClient
SET    ContactNumber = '09995245560'
WHERE  ID = @Destination_ID_Client
       and ID_Company = @ID_Company 