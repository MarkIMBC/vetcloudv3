DEclare @GUID_Company VARCHAR(MAX) = '1324F5AB-A86D-413B-ADFD-05D678C09DD9'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2),
     tempID            VARCHAR(MAX),
     [Option]           VARCHAR(MAX)
  )

INSERT @import
       (Name_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,
        tempID, [Option])
SELECT dbo.fGetCleanedString([ITEM]),
       dbo.fGetCleanedString([Category]),
       TRY_CONVERT(Decimal(18, 2), [Cost]),
       TRY_CONVERT(Decimal(18, 2), Price),
       [GUID], [Option]
FROM   ForImport.[dbo].[ItemProduct]

Update @import
SET    ID_ItemCategory = itemCategory.ID
FROM   @import import
       inner join vItemCategory itemCategory
               on import.Name_ItemCategory = itemCategory.Name
where  itemCategory.ID_ItemType = @Inventoriable_ID_ItemType

Update @import SET Name_Item  = Name_Item + ' - ' + [Option]
where LEN(ISNULL([Option], 0)) > 0

------------------------------------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

SELECT Distinct Name_ItemCategory
FROM   @import
where  ID_ItemCategory IS NULL
Order  by Name_ItemCategory

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       @Inventoriable_ID_ItemType 
FROM   @import hed
where  hed.ID_Item IS NULL

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update tItem
set    Name = import.Name_Item,
       UnitCost = import.UnitCost,
       UnitPrice = import.UnitPrice,
       ID_ItemCategory = import.ID_ItemCategory
FROM   tItem item
       inner join @import import
               on item.ID = import.ID_Item
WHERE  item.ID_Company = @ID_Company

SELECT *
FROM   @import Order by Name_Item
