DECLARE @GUID_Company VARCHAR(MAX) = '21D87AC1-A5D8-4E8B-B57E-A591C499A062'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.    
Pls. contact /*CompanyName*/ /*ContactNumber*/'
------
DECLARE @SMSTemplate VARCHAR(MAX) = 'Good day! /*Pet*/ is due for /*Service*/ on /*DateReturn*/. ' 
+ 'Please be advised that this message is an automated SMS reminder, DO NOT REPLY or CALL HERE.' 
+ 'You may reach us at our clinic number /*ContactNumber*/. See you at the clinic.' 
+ 'Thank you. - /*CompanyName*/'

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

--
SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
