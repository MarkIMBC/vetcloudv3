exec _pAddModelProperty
  'tItem',
  'tempID',
  1

exec _pRefreshAllViews

IF OBJECT_ID(N'Temp-2022-07-29-Item', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-07-29-Item];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-07-29-' + company.Guid + '-Item-'
       + Convert(Varchar(MAX), Item.ID)              tempID,
       item.Code,
       item.Name,
       item.IsActive,
       item.ID_ItemType,
       item.ID_ItemCategory,
       item.MinInventoryCount,
       item.MaxInventoryCount,
       item.UnitCost,
       item.UnitPrice,
       item.OtherInfo_DateExpiration,
       item.BarCode,
       item.CustomCode,
       item._tempSupplier,
       CASE
         WHEN LEN(ISNULL(Item.Comment, '')) > 0 THEN CHAR(13)
         ELSE ''
       END
       + 'Imported from VetCloudTemp July 29, 2022' Comment,
       Item. ID_Company
INTO   [dbo].[Temp-2022-07-29-Item]
FROM   db_vetcloudv3_thecatclinic.[dbo].tItem Item
       inner join db_vetcloudv3_thecatclinic.[dbo].tCompany company
               on Item.ID_Company = company.ID
where  Item.ID >= 125472 AND company.Guid = 'ACE352E0-1293-4416-AB07-B8A611F37F6C'

INSERT @NotYetInserted_TempID
SELECT tempItem.tempID
FROm   [dbo].[Temp-2022-07-29-Item] tempItem

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tItem)

INSERT tItem
       (ID_Company,
        tempID,
        Code,
        Name,
        ID_ItemType,
        ID_ItemCategory,
        MinInventoryCount,
        MaxInventoryCount,
        UnitCost,
        UnitPrice,
        OtherInfo_DateExpiration,
        BarCode,
        CustomCode,
        _tempSupplier,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempporyItem.ID_Company,
       tempporyItem.tempID,
       tempporyItem.Code,
       tempporyItem.Name,
       tempporyItem.ID_ItemType,
       tempporyItem.ID_ItemCategory,
       tempporyItem.MinInventoryCount,
       tempporyItem.MaxInventoryCount,
       tempporyItem.UnitCost,
       tempporyItem.UnitPrice,
       tempporyItem.OtherInfo_DateExpiration,
       tempporyItem.BarCode,
       tempporyItem.CustomCode,
       tempporyItem._tempSupplier,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [Temp-2022-07-29-Item] tempporyItem
       inner join @NotYetInserted_TempID temp
               on tempporyItem.tempID = temp.tempID 