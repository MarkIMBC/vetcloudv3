DEclare @GUID_Company VARCHAR(MAX) = 'BB184EAB-6E8A-4EEA-BE7D-DD41F70782B9'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ /*DateReturn*/. '
                            + CHAR(13)
                            + ' Please contact /*ContactNumber*/ /*CompanyName*/ for further inquiries. '
                            + CHAR(13)
                            + 'THIS IS AN AUTOMATED SMS reminder. PLEASE DO NOT REPLY HERE.'
                            + CHAR(13) + 'Thank You.'
WHERE  ID = @ID_Company

select Name_Company,
       Message
FROM   dbo.fGetSendSoapPlan('2022-04-10 00:00:00', NULL, @ID_Company) 



SELECT * FROm vUser WHERE ID_Company = 248