DECLARE @GUID_Company VARCHAR(MAX) = '804E6163-18D4-484D-8981-9EB2E1910925'
DECLARE @ID_ItemType INT = 1
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')

----------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @excel TABLE
  (
     ID_Item   INt,
     Name_Item varchar(5000),
     UnitCost  DECIMAL(18, 2),
     UnitPrice DECIMAL(18, 2),
     GUID      varchar(5000)
  )

INSERT @excel
       (Name_Item,
        UnitCost,
        UnitPrice,
        GUid)
SELECT dbo.fGetCleanedString([Name]),
       ISNULL(TRY_COnvert(decimal, dbo.fGetCleanedString([BuyingPrice])), 0),
       ISNULL(TRY_COnvert(decimal, dbo.fGetCleanedString([FINAL PRICE])), 0),
       GUID
FROM   ForImport.[dbo].[VET-CLOUD-SERVICES-PRICE-ADJUSTMENT(1)]

Update @excel
SET    ID_Item = item.ID
FROM   @excel excel
       INNER JOIN titem item
               on excel.Name_Item = item.Name
WHERE  item.ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = @ID_ItemType

SELECT *
FROM   @excel
WHERE  ID_Item IS NULL
-------------------------------------------
INSERT INTO [dbo].[tItem]
            ([Name],
             [IsActive],
             [ID_Company],
             tempID,
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT DISTINCT Name_Item,
                1,
                @ID_Company,
                Guid,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                @ID_ItemType
FROM   @excel
where  LEN(Name_Item) > 0
       and ID_Item IS NULL

--------------------------------------------
Update @excel
SET    ID_Item = item.ID
FROM   @excel excel
       INNER JOIN titem item
               on excel.Name_Item = item.Name
WHERE  item.ID_Company = @ID_Company
       and IsActive = 1
       and ID_ItemType = @ID_ItemType

---------------------------------------------------------
Update tItem
SET    UnitCost = excel.UnitCost,
       UnitPRice = excel.UnitPrice
FROM   tItem item
       inner join @excel excel
               on item.ID = excel.ID_Item
WHERE  ID_Company = @ID_Company


SELECT *
FROM   @excel 
