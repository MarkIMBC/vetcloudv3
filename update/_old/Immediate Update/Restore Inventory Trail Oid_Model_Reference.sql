DECLARE @GUID_Company VARCHAR(MAX) = '1F2F41DD-AD24-4C46-AC15-09C7591C7ADA'
DECLARE @ID_Model_BillingInvoice_Detail VARCHAR(MAX) = ''

----------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @Code_BillingInvoice VARCHAR(MAX) = ''
DECLARE @ID_BillingInvoice_Detail INT = 0
DECLARE @ID_Item [int]= NULL
DECLARE @Quantity [int]= NULL
DECLARE @UnitCost [decimal](18, 4)= NULL
DECLARE @UnitPrice [decimal](18, 4)= NULL
DECLARE @Amount [decimal](18, 4) =NULL
DECLARE @Oid_Model_Reference [varchar](1) =NULL
DECLARE @ID_Reference [int] =NULL
DECLARE @biDetails TABLE
  (
     Code_BillingInvoice      [varchar](MAX) NULL,
	 Date      DateTime,
     Name_FilingStatus      [varchar](MAX) NULL,
     ID_BillingInvoice_Detail [int] NULL,
     ID_Item                  [int] NULL,
     Name_Item                [varchar](MAX) NULL,
     Quantity                 [int] NULL,
     UnitCost                 [decimal](18, 4) NULL,
     UnitPrice                [decimal](18, 4) NULL,
     Amount                   [decimal](18, 4) NULL,
     Oid_Model_Reference      [varchar](MAX) NULL,
     ID_Reference             [int] NULL,
     ID_InventoryTrail        [int] NULL
  )

SELECT @ID_Model_BillingInvoice_Detail = Oid
FROM   _tModel
where  TableName = 'tBillingInvoice_Detail'

SELECT *
FROM   tBillingInvoice
where  ID_Company = @ID_Company
       AND Code = @Code_BillingInvoice

INSERT @biDetails
SELECT bi.COde, bi.Date, bi.Name_FilingStatus, det.ID ID_BillingInvoice_Detail,
       det.ID_Item,
       Name_Item,
       det.Quantity,
       det.UnitCost,
       det.UnitPrice,
       det.Amount,
       NULL   Oid_Model_Reference,
       NULL   ID_Reference,
       NULL   ID_InventoryTrail
FROM   vBillingInvoice_Detail det
       inner join vBillingInvoice bi
               on det.ID_BillingInvoice = bi.ID
			   INNER JOIN tItem item on item.ID = det.ID_Item
WHERE  bi.ID_Company = @ID_Company
       and COnvert(date, bi.Date) BETWEEN '2022-12-01' AND '2022-12-30' and item.ID_ItemType = 2 and Item.ID = 26680

DECLARE @Count INT =0
DECLARE db_cursor CURSOR FOR
  SELECT  Code_BillingInvoice, [ID_BillingInvoice_Detail],
         [ID_Item],
         [Quantity],
         [UnitCost],
         [UnitPrice],
         [Amount]
  FROM   @biDetails
  Order  by ID_BillingInvoice_Detail

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Code_BillingInvoice,
                               @ID_BillingInvoice_Detail,
                               @ID_Item,
                               @Quantity,
                               @UnitCost,
                               @UnitPrice,
                               @Amount

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_InventoryTrail INT


       SELECT @ID_InventoryTrail = MIN(ID)
            FROm   vInventoryTrail
            where  ID_Item = @ID_Item
                   and ID_Company = @ID_Company
                   and Code = @Code_BillingInvoice
                   and ABS(Quantity) = @Quantity
                   and ABS(UnitPrice) = ABS(@UnitPrice)
                         and ID NOT IN (SELECT ID_InventoryTrail
                                  FROm   @biDetails
                                  WHERE  ID_InventoryTrail IS NOT NULL)

            Update @biDetails
            SET    ID_InventoryTrail = @ID_InventoryTrail
            where  ID_BillingInvoice_Detail = @ID_BillingInvoice_Detail

		 PRINT @Code_BillingInvoice
		 PRINT @ID_InventoryTrail
            PRINT @Count

            SET @Count = @Count + 1

      FETCH NEXT FROM db_cursor INTO @Code_BillingInvoice,
                                     @ID_BillingInvoice_Detail,
                                     @ID_Item,
                                     @Quantity,
                                     @UnitCost,
                                     @UnitPrice,
                                     @Amount
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT *
FROM   @biDetails
Order  by ID_BillingInvoice_Detail

Update tInventoryTrail
SET    Oid_Model_Reference = @ID_Model_BillingInvoice_Detail,
       ID_Reference = bidetail.ID_BillingInvoice_Detail
FROM   tInventoryTrail invt
       INNER JOIN @biDetails bidetail
               on invt.ID = bidetail.ID_InventoryTrail
Where  invt.Oid_Model_Reference is NULL 
