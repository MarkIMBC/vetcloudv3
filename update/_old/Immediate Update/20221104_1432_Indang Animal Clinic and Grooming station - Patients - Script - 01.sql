GO

CREATE   OR
ALTER FUNCTION fGetCleanedString(@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@Value, '  ', ' '), CHAR(9), ''), CHAR(10), ''), CHAR(13), '')))

      if( LEFT(@result, 1) = '''' )
        set @result = RIGHT(@result, LEN(@result) - 1)

      if( RIGHT(@result, 1) = '''' )
        set @result = LEFT(@result, LEN(@result) - 1)

      return @result
  END

GO

GO

DECLARE @GUID_Company VARCHAR(MAX) = '8440EE8A-3F74-4AD8-B18D-2E3C63D7F1FD'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @ClientPatient TABLE
  (
     Name_Client          VARCHAR(MAX),
     Name_Patient         VARCHAR(MAX),
     ClientPatient_Import VARCHAR(MAX),
     GUID                 VARCHAR(MAX)
  )
DECLARE @import TABLE
  (
     CustomCode_Patient    VARCHAR(MAX),
     ID_Client             INT,
     ID_Patient            INT,
     Name_Client           VARCHAR(MAX),
     Name_Patient          VARCHAR(MAX),
     Address_Client        VARCHAR(MAX),
     ContactNumber_Client  VARCHAR(MAX),
     ContactNumber2_Client VARCHAR(MAX),
     ID_Gender_Patient     INT,
     DateBirth_Patient     DateTime,
     DateBirth_Import      VARCHAR(MAX),
     Specie_Patient        VARCHAR(MAX),
     GUID                  VARCHAR(MAX)
  )
DECLARE @ClientPatient_Import VARCHAR(MAX) = ''
DECLARE @GUID_Import VARCHAR(MAX) = ''
DECLARE db_cursor CURSOR FOR
  SELECT [Patient Name],
         GUID
  FROM   ForImport.dbo.[Indang Animal Clinic and Grooming station - Patients]
  where  [Patient Name] LIKE '%-%'

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ClientPatient_Import, @GUID_Import

WHILE @@FETCH_STATUS = 0
  BEGIN
      Declare @splitString TABLE
        (
           Part VARCHAR(MAX),
           ID   Int
        )
      DECLARE @Name_Client VARCHAR(MAX) = '';
      DECLARE @Name_Patient VARCHAR(MAX) = '';

      INSERT @splitString
      SELECT Part,
             ID
      FROM   dbo.fGetSplitString(@ClientPatient_Import, '-');

      SELECT @Name_Client = LTRIM(RTRIM(Part))
      FROM   @splitString
      where  ID = 1

      SELECT @Name_Patient = LTRIM(RTRIM(Part))
      FROM   @splitString
      where  ID = 2

      if( LEFT(@Name_Client, 1) = '''' )
        BEGIN
            SET @Name_Client = RIGHT(@Name_Client, LEN(@Name_Client) - 1)
        END

      if( RIGHT(@Name_Patient, 1) = '''' )
        BEGIN
            SET @Name_Patient = LEFT(@Name_Patient, LEN(@Name_Patient) - 1)
        END

      INSERT @ClientPatient
             (Name_Client,
              Name_Patient,
              ClientPatient_Import,
              GUID)
      SELECT @Name_Client,
             @Name_Patient,
             @ClientPatient_Import,
             @GUID_Import

      delete FROM @splitString

      FETCH NEXT FROM db_cursor INTO @ClientPatient_Import, @GUID_Import
  END

CLOSE db_cursor

DEALLOCATE db_cursor

DECLARE @GUIDs TABLE
  (
     GUID VARCHAR(MAX)
  )

INSERT @GUIDs
SELECT GUID
FROM   @ClientPatient
GROUP  BY GUID
HAVING COUNT(*) = 2
Order  by GUID

INSERT @import
       (CustomCode_Patient,
        Name_Client,
        Name_Patient,
        ContactNumber_Client,
        ContactNumber2_Client,
        Address_Client,
        ID_Gender_Patient,
        DateBirth_Import,
        Specie_Patient,
        GUID)
SELECT dbo.fGetCleanedString([Patient Number]),
       clientpatient.Name_Client,
       clientpatient.Name_Patient,
       dbo.fGetCleanedString([Mobile Number]),
       dbo.fGetCleanedString([Secondary Mobile]),
       Address + ' ' + Locality + ' ' + City,
       CASE
         WHEN Gender LIKE '%M%' THEN 1
         ELSE
           CASE
             WHEN Gender LIKE '%F%' THEN 2
             ELSE NULL
           END
       END,
       [Date of Birth],
       dbo.fGetCleanedString(Groups),
       clientpatient.GUID
FROM   @ClientPatient clientpatient
       inner join ForImport.dbo.[Indang Animal Clinic and Grooming station - Patients] import
               on clientpatient.GUID = import.GUID
Order  by clientpatient.Name_Client

Update @import
SET    Address_Client = dbo.fGetCleanedString(Address_Client)

Update @import
SET    ContactNumber_Client = LEFT(ContactNumber_Client, LEN(ContactNumber_Client) - 1)
where  RIGHT(ContactNumber_Client, 1) = ''''

Update @import
SET    ContactNumber_Client = RIGHT(ContactNumber_Client, LEN(ContactNumber_Client) - 1)
where  LEFT(ContactNumber_Client, 1) = ''''

Update @import
SET    DateBirth_Import = LEFT(DateBirth_Import, LEN(DateBirth_Import) - 1)
where  RIGHT(DateBirth_Import, 1) = ''''

Update @import
SET    DateBirth_Import = RIGHT(DateBirth_Import, LEN(DateBirth_Import) - 1)
where  LEFT(DateBirth_Import, 1) = ''''

Update @import
SET    DateBirth_Patient = RIGHT(DateBirth_Import, LEN(DateBirth_Import) - 1)
where  LEFT(DateBirth_Import, 1) = ''''

Update @import
SET    ContactNumber_Client = REPLACE(ContactNumber_Client, '+63', '0')

Update @import
SET    ContactNumber2_Client = REPLACE(ContactNumber2_Client, '+63', '0')


Update @import
SET    ID_Client = client.ID
FROM   @import import
       INNER JOIN tClient client
               on import.GUID + '-Client' = client.tempID
where  client.ID_Company = @ID_Company

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       INNER JOIN tPatient patient
               on import.GUID = patient.tempID
where  patient.ID_Company = @ID_Company

INSERT tClient
       (Name,
        ID_Company,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        tempID)
SELECT DISTINCT Name_Client,
                @ID_Company,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1,
                MAX(GUID) + '-Client'
FROM   @import
WHEre  LEN(Name_Client) > 0
       and ISNULL(ID_Client, 0) = 0
Group by Name_Client

Update @import
SET    ID_Client = client.ID
FROM   @import import
       INNER JOIN tClient client
               on import.GUID + '-Client' = client.tempID
where  client.ID_Company = @ID_Company

Update @import
SET    ID_Patient = patient.ID
FROM   @import import
       INNER JOIN tPatient patient
               on import.GUID = patient.tempID
where  patient.ID_Company = @ID_Company

INSERT tPatient
       (Name,
        ID_Client,
        ID_Company,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        tempID)
SELECT DISTINCT Name_Patient,
                ID_Client,
                @ID_Company,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1,
                MAX(GUID) 
FROM   @import
WHEre  LEN(Name_Patient) > 0
       and ISNULL(ID_Patient, 0) = 0
Group by Name_Patient, ID_Client

SELECT *
FROM   @import ORder by Name_Client

Update tClient
set    ContactNumber = import.ContactNumber_Client,
       ContactNumber2 = import.ContactNumber2_Client,
       Address = import.Address_Client
FROM   tClient client
       inner join @import import
               on client.tempID = import.GUID + '-Client'
                  and ID_Company = @ID_Company 
Update tPatient
set    CustomCode = import.CustomCode_Patient,
       ID_Gender = import.ID_Gender_Patient,
       DateBirth = import.DateBirth_Patient,
	   Species = import.Specie_Patient
FROM   tPatient patient
       inner join @import import
               on patient.tempID = import.GUID  
                  and ID_Company = @ID_Company 



