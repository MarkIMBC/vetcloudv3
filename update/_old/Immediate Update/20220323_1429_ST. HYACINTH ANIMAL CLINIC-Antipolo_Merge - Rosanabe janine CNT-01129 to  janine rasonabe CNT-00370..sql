DECLARE @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ID_Client_To INT
DECLARE @ID_Company_From INT

SELECT Name_Company,
       Name_Employee,
       Username,
       Password
FROm   vUser
WHERE  ID_Company = @ID_Company

print '/*Transfer all patient to old client CNT-00370*/'

Update tPatient
SET    ID_Client = 282119
WHERE  ID_Client = 346415
       and ID_Company = @ID_Company

print '/*Delete CNT-01129*/'

Update tClient
SET    IsActive = 0
WHERE  ID = 346415
       and ID_Company = @ID_Company

/*Merge Panther records FROm 457617 to PNT-00950*/
print '/*Merge Panther records FROm PNT-01565 to PNT-00950*/'

Update tPatient_SOAP
SET    ID_Patient = 405990,
       ID_Client = 282119
WHERE  ID_Patient = 457617
       and ID_Company = @ID_Company

print '/*Delete PNT-01565*/'

Update tPatient
set    IsActive = 0
WHERE  ID = 457617
       and ID_Company = @ID_Company 
