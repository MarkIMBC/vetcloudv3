DECLARE @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'
DECLARE @SourceID_Client INT =303537
DECLARE @DestinationID_Client INT =212873

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
SET    SOAPPlanSMSMessage = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. To avoid overcrowding, please follow one pet one companion policy.  This is an automated SMS reminder Please DO NOT REPLY or CALL Here! Pls. contact /*CompanyName*/ /*ContactNumber*/. Thank You. '
where  ID = @ID_Company

SELECT TOP 10 *
FROm   dbo.fGetSendSoapPlanDateCovered('2022-08-01', '2022-08-31', NULL, @ID_Company) 
