DEclare @GUID_Company VARCHAR(MAX) = '91C3CEF9-1B32-4C1D-BB92-843F4B1116AA'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 174666 -- Jhanel Cerafica
DECLARE @Destination_ID_Client INT = 173605 -- Janelle Cerafica
SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - Janelle Cerafica from CNT-27217 to CNT-26156'

Update tClient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Client
       and ID_Company = @ID_Company

---------------------------------------------------------------------------------------
DECLARE @Source1_ID_Patient INT = 217977 -- Jhanel Cerafica
DECLARE @Destination_ID_Patient INT = 228454 -- Janelle Cerafica

SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vPatient
WHERE  ID = @Source1_ID_Patient
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vPatient
WHERE  ID = @Destination_ID_Patient
       AND ID_Company = @ID_Company

exec pMergePatientRecord
  @Source1_ID_Patient,
  @Destination_ID_Patient,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - Janelle Cerafica --> Merge Patient - Skittles - Janelle Cerafica from PNT-26845 to PNT-37322'

Update tPatient
SET    IsActive = 0
WHERE  ID = @Source1_ID_Patient 
