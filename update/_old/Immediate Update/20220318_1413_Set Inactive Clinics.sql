DECLARE @FitchieeGUID_Company VARCHAR(MAX) = '35573DAB-37E6-4F5D-9773-4F599DB51167'
DECLARE @LEMERYGUID_Company VARCHAR(MAX) = '38A5E6B5-7520-445F-8BC7-C638AF25D67C'
DECLARE @SurigaoGUID_Company VARCHAR(MAX) = '81523EFA-8477-42B7-9D20-74BF7B390B15'
DECLARE @UnivetGUID_Company VARCHAR(MAX) = '1F511172-0FC0-4532-9B91-9A36C69D3D7D'
DECLARE @PetIdeasGUID_Company VARCHAR(MAX) = 'BDCE5B44-FF10-41FB-8A9A-5260BF705D49'
DECLARE @BarkMeowGUID_Company VARCHAR(MAX) = '0B66D210-1D0B-4D01-A402-1DEEE9BF8EAC'
DECLARE @PAWSDOCTORGUID_Company VARCHAR(MAX) = '8C8D0823-44EE-4CDB-8BEA-15016F05FA9B'
DECLARE @GoldenGUID_Company VARCHAR(MAX) = '7ADCEF17-D19C-426D-BD11-36F6E98DED43'
DECLARE @VetLifeGUID_Company VARCHAR(MAX) = '60B280D9-F575-4DC1-B563-D8741B87E73F'
DECLARE @BarkAvenueGUID_Company VARCHAR(MAX) = '5F6BD70F-379A-40A3-9E06-9A1F669D5870'
DECLARE @FaithfulGUID_Company VARCHAR(MAX) = '21B9CBF2-38A5-40D9-B4AC-8B3A3C067F56'
DECLARE @CompanyTempTable TABLE
  (
     ID         INT IDENTITY(1, 1) primary key,
     ID_Company INT
  )

INSERT INTO @CompanyTempTable
            (ID_Company)
SELECT ID
FROm   vCompanyActive
WHERE  GUID IN ( @FitchieeGUID_Company, @LEMERYGUID_Company, @SurigaoGUID_Company, @UnivetGUID_Company,
                 @FaithfulGUID_Company, @PetIdeasGUID_Company, @BarkMeowGUID_Company, @PAWSDOCTORGUID_Company,
                 @GoldenGUID_Company, @VetLifeGUID_Company, @BarkAvenueGUID_Company )

----------------------------------------------------------------
/* Update Company*/
Update tCompany
SET    IsActive = 0
WHERE  ID IN (SELECT ID_Company
              FROM   @CompanyTempTable)

/* Update Users*/
Update tUser
SET    IsActive = 0
FROM   tUser users
       INNER JOIN tEmployee emp
               ON users.ID_Employee = emp.ID
WHERE  emp.ID_Company IN (SELECT ID_Company
                          FROM   @CompanyTempTable)

/* Update Session*/
Update tUserSession
SET    IsActive = 0
FROM   tUserSession session
       INNER JOIN tUser users
               ON session.ID_User = users.ID
       INNER JOIN tEmployee emp
               ON users.ID_Employee = emp.ID
WHERE  emp.ID_Company IN (SELECT ID_Company
                          FROM   @CompanyTempTable)

SELECT ID,
       Name,
       IsActive
FROM   vCompany
WHERE  ID IN (SELECT ID_Company
              FROM   @CompanyTempTable)
order  by Name

SELECT Name_Company,
       Name,
       Username,
       Password,
       IsActive
FROM   vUser
WHERE  ID_Company IN (SELECT ID_Company
                      FROM   @CompanyTempTable)

SELECT session.*
FROM   tUserSession session
       INNER JOIN tUser users
               ON session.ID_User = users.ID
       INNER JOIN tEmployee emp
               ON users.ID_Employee = emp.ID
WHERE  emp.ID_Company IN (SELECT ID_Company
                          FROM   @CompanyTempTable) 
