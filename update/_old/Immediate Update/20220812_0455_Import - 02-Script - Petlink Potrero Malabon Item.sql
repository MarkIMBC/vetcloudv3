DECLARE @GUID_Company VARCHAR(MAX) = '804E6163-18D4-484D-8981-9EB2E1910925'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([ITEM NAME])
       + CASE
           WHEN LEN(dbo.fGetCleanedString([ITEM NAME1])) > 0 then ' ' + dbo.fGetCleanedString([ITEM NAME1])
           ELSE ''
         END,
       dbo.fGetCleanedString([SELLING PRICE]),
       dbo.fGetCleanedString([CATEGORY]),
       [GIUD]
FROM   ForImport.dbo.[Petlink-Potrero-ITEMS_20220812_0430]

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    Import_Category = 'ANTIBIOTICS & OTHERS'
WHERE  Import_Category = 'ANTIBIOTIC & OTHERS'

Update @forImport
SET    Import_Category = 'VITAMINS & MINERALS SUPPLEMENTS'
WHERE  Import_Category = 'VITAMINS & MINERALS SUPPLEMENT'

DELETE FROM @forImport
WHERE  GUID IN ( '528D1460-8F65-49A4-AEE6-F49AFF10E513-[Petlink-Potrero-ITEMS_20220812_0430]', 'DA8CB480-784F-46F9-B4CF-473A2DD3D260-[Petlink-Potrero-ITEMS_20220812_0430]', '4B6C1EBF-0E40-44A4-9FFE-7131CC68DB4A-[Petlink-Potrero-ITEMS_20220812_0430]', '52E73A13-57CD-4421-B5B0-4A20D8692139-[Petlink-Potrero-ITEMS_20220812_0430]', '1CE2EC09-EDA8-4B54-8929-A0EBBB2DC1EB-[Petlink-Potrero-ITEMS_20220812_0430]' )

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType
       and category.IsActive = 1

SELECT Import_Category,
       COUNT(*)
FROM   @forImport
WHERE  ID_ItemCategory IS NULL
GROUP  BY Import_Category

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice,
       Name = import.Name_Item
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

DELETE FROM tItem
WHERE  ID_Company = @ID_Company
       AND tempID IN ( '528D1460-8F65-49A4-AEE6-F49AFF10E513-[Petlink-Potrero-ITEMS_20220812_0430]', 'DA8CB480-784F-46F9-B4CF-473A2DD3D260-[Petlink-Potrero-ITEMS_20220812_0430]', '4B6C1EBF-0E40-44A4-9FFE-7131CC68DB4A-[Petlink-Potrero-ITEMS_20220812_0430]', '52E73A13-57CD-4421-B5B0-4A20D8692139-[Petlink-Potrero-ITEMS_20220812_0430]', '1CE2EC09-EDA8-4B54-8929-A0EBBB2DC1EB-[Petlink-Potrero-ITEMS_20220812_0430]' )

SELECT *
FROM   @forImport 
