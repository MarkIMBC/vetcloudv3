--SERVER 1
DECLARE @GUID_Company_Petfriends VARCHAR(MAX) = '213ED508-D862-46FA-A1FB-DA0056573F13'
DECLARE @GUID_Company_SFA VARCHAR(MAX) = 'E75F3224-F5CD-4D1D-AFF3-EE9797140D61'
DECLARE @GUID_Company_Artemis VARCHAR(MAX) = '023A0921-533C-468D-9C69-11653D6D6271'
DECLARE @GUID_Company_PetPoint VARCHAR(MAX) = '0162A8BC-FE86-4F21-8280-D94C3B17A76E'
DECLARE @IDs_Company_ToActivate [dbo].[typIntList]

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company_Petfriends
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

INSERT INTO @IDs_Company_ToActivate
SELECT ID
FROM   tCompany
WHERE  Guid IN ( @GUID_Company_Petfriends, @GUID_Company_SFA, @GUID_Company_Artemis, @GUID_Company_PetPoint )

SELECT *
FROm   @IDs_Company_ToActivate

SELECT Name,
       GUID
FROM   vCompanyActive
WHERE  ID IN(SELECT *
             FROM   @IDs_Company_ToActivate)

----------------------------------------------------------------
Update vCompanyActive
SET    isShowPaymentWarningLabel = 1
WHERE  ID IN(SELECT *
             FROM   @IDs_Company_ToActivate) 
