DECLARE @GUID_Company VARCHAR(MAX) = '69D3F592-B779-4444-AE6B-C7AB112B0A48'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        GUID)
select dbo.fGetCleanedString(Items),
       [GUID]
FROM  ForImport.[dbo].[IDOC MAVIE_S VETERINARY CLINIC-NVENTORY_MONTHLY_20221112_0951]
WHERE LEN(dbo.fGetCleanedString(Items)) > 0


Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType
       and category.IsActive = 1

SELECT Import_Category,
       COUNT(*)
FROM   @forImport
WHERE  ID_ItemCategory IS NULL
GROUP  BY Import_Category


DELETE FROM @forImport WHERE GUID IN (
SELECT MAX(GUID)
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1
)


SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item



INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET   
       Name = import.Name_Item
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 
