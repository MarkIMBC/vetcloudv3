DECLARE @GUID_Company VARCHAR(MAX) = 'E9262033-6657-4803-AAB2-17C725C5C382'

-------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
  -- AND IsActive = 1
  ) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Client INt = 0
DECLAre @ContactNumber VARCHAR(MAX) = ''
DECLARE @table TABLE
  (
     ID_CLient     INt,
     COntactNUmber_Client VARCHAR(MAX),
     COntactNUmber VARCHAR(MAX),
     RowIndex      Int
  )
DECLARE db_cursor CURSOR FOR
  SELECT ID,
         ContactNumber
  FROM   tCLient
  where  IsActive = 1
         and ID_Company = @ID_Company
         and ContactNumber LIKE '%/%'

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_Client, @ContactNumber

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT @table
      SELECT @ID_Client,
	  @ContactNumber,
             REPLACE(Part, ' ', ''),
             ID
      FROM   dbo.fGetSplitString(@ContactNumber, '/')

      FETCH NEXT FROM db_cursor INTO @ID_Client, @ContactNumber
  END

CLOSE db_cursor

DEALLOCATE db_cursor

Update tClient
SET    ContactNumber = t.COntactNUmber
FROM   tClient client
       inner join @table t
               on client.ID = t.ID_CLient
WHERE  ID_Company = @ID_Company
       and RowIndex = 1

Update tClient
SET    ContactNumber2 = t.COntactNUmber
FROM   tClient client
       inner join @table t
               on client.ID = t.ID_CLient
WHERE  ID_Company = @ID_Company
       and RowIndex = 2 
	   
SELECT * FROM @table