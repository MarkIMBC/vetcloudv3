DECLARE @Felix_From_ID_Patient INT = 455368
DECLARE @Felix_To_ID_Patient INT = 471107
--
DECLARE @MAC_From_ID_Patient INT = 455345
DECLARE @MAC_To_ID_Patient INT =  471108
--
DECLARE @Cheese_From_ID_Patient INT = 455332
DECLARE @Cheese_To_ID_Patient INT = 471109

DECLARE @CELADINAREBEKBAH_From_ID_Client INT = 344914
DECLARE @RebekkaJoyCeladina_To_ID_Client INT = 355308

exec pMergePatientRecord
  @Felix_From_ID_Patient,
  @Felix_To_ID_Patient,
  'St. Hyacinth - Antipolo Branch - Merge CNT 01051 to CNT 01331 rebekka joy celadena - Tranfer Pet FELIX'

exec pMergePatientRecord
  @MAC_From_ID_Patient,
  @MAC_To_ID_Patient,
  'St. Hyacinth - Antipolo Branch - Merge CNT 01051 to CNT 01331 rebekka joy celadena - Tranfer Pet Mac'

exec pMergePatientRecord
  @Cheese_From_ID_Patient,
  @Cheese_To_ID_Patient,
  'St. Hyacinth - Antipolo Branch - Merge CNT 01051 to CNT 01331 rebekka joy celadena - Tranfer Pet Cheese'


exec pMergeClientRecord
  @CELADINAREBEKBAH_From_ID_Client,
  @RebekkaJoyCeladina_To_ID_Client,
  'St. Hyacinth - Antipolo Branch - Merge CNT 01051 to CNT 01331 rebekka joy celadena'

Update tPatient
SET    IsActive = 0
where  ID IN ( @Felix_From_ID_Patient, @MAC_From_ID_Patient, @Cheese_From_ID_Patient )

Update tPatient
SET    IsActive = 1
where  ID IN ( @MAC_To_ID_Patient)

Update tClient
SET    IsActive = 0
where  ID IN (@CELADINAREBEKBAH_From_ID_Client )

SELECT *
FROm   tRecordValueTransferLogs 
