if OBJECT_ID('dbo.[JMIC-HEALTH-NEEDS]') is not null
  BEGIN
      DROP TABLE [JMIC-HEALTH-NEEDS]
  END

GO

CREATE TABLE [dbo].[JMIC-HEALTH-NEEDS]
  (
     [SeqNo]                      varchar(500),
     [SKU Codes]                  varchar(500),
     [Dept/Sub Dept/Class/ Code ] varchar(500),
     [DESCRIPTION]                varchar(500),
     [SUB-CLASS DESCRIPTION]      varchar(500),
     [PARTICULARS]                varchar(500),
     [SRP]                        varchar(500),
     [COST]                       varchar(500),
     [GM]                         varchar(500)
  )

GO

INSERT INTO [dbo].[JMIC-HEALTH-NEEDS]
            ([SeqNo],
             [SKU Codes],
             [Dept/Sub Dept/Class/ Code ],
             [DESCRIPTION],
             [SUB-CLASS DESCRIPTION],
             [PARTICULARS],
             [SRP],
             [COST],
             [GM])
SELECT '1',
       '10279111',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pet One Natural Response 120 Tab',
       '3,960.00',
       '3,168.00',
       '20%'
UNION ALL
SELECT '2',
       '10279113',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pet One Natural Response 1 tab',
       '36.00',
       '28.80',
       '20%'
UNION ALL
SELECT '3',
       '10279115',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Iron Aid',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '4',
       '10279116',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Inflacam 32ml',
       '840.00',
       '672.00',
       '20%'
UNION ALL
SELECT '5',
       '10279150',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Tefrosol Forte',
       '240.00',
       '192.00',
       '20%'
UNION ALL
SELECT '6',
       '10279118',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Previcox 57mg Tab',
       '84.00',
       '67.20',
       '20%'
UNION ALL
SELECT '7',
       '10279119',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Previcox 227mg Tab',
       '180.00',
       '144.00',
       '20%'
UNION ALL
SELECT '8',
       '10279120',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pet Sure 120ml',
       '276.00',
       '220.80',
       '20%'
UNION ALL
SELECT '9',
       '10279121',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pet Sure 60ml',
       '240.00',
       '192.00',
       '20%'
UNION ALL
SELECT '11',
       '10279123',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Hemacare 60ml',
       '264.00',
       '211.20',
       '20%'
UNION ALL
SELECT '12',
       '10279124',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Hemacare 120ml',
       '336.00',
       '268.80',
       '20%'
UNION ALL
SELECT '13',
       '10279125',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Methiovet 60 tabs',
       '1,020.00',
       '816.00',
       '20%'
UNION ALL
SELECT '14',
       '10279151',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Methiovet 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '15',
       '10279126',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'LC Dox 60ml',
       '300.00',
       '240.00',
       '20%'
UNION ALL
SELECT '16',
       '10279127',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'LC Dox 120ml',
       '420.00',
       '336.00',
       '20%'
UNION ALL
SELECT '17',
       '10279128',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'LC Dox 100 Tabs',
       '1,800.00',
       '1,440.00',
       '20%'
UNION ALL
SELECT '18',
       '10279152',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'LC Dox 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '19',
       '10279154',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Scourvet 60ml',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '20',
       '10279129',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'LC Scour 60ml',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '21',
       '10279130',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Micoderm 15ml',
       '300.00',
       '240.00',
       '20%'
UNION ALL
SELECT '22',
       '10279131',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Apex Ear Drops Otiderm 15ml',
       '660.00',
       '528.00',
       '20%'
UNION ALL
SELECT '23',
       '10279132',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Livtone 60ml',
       '384.00',
       '307.20',
       '20%'
UNION ALL
SELECT '24',
       '10279133',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Livotone 120ml',
       '480.00',
       '384.00',
       '20%'
UNION ALL
SELECT '25',
       '10279134',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'NV Brewers Yeast Chewables Bottle',
       '1,020.00',
       '816.00',
       '20%'
UNION ALL
SELECT '26',
       '10279155',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'NV Brewers Yeast Chewables 1 tab',
       '12.00',
       '9.60',
       '20%'
UNION ALL
SELECT '27',
       '10279135',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'NV Probiotics Capsules 1 bottle',
       '1,920.00',
       '1,536.00',
       '20%'
UNION ALL
SELECT '28',
       '10279137',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'NV Probiotics 1 Capsule',
       '36.00',
       '28.80',
       '20%'
UNION ALL
SELECT '29',
       '10279136',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'NV Eye Rinse Liquid',
       '780.00',
       '624.00',
       '20%'
UNION ALL
SELECT '30',
       '10279138',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Multi Chewables 1 bottle',
       '900.00',
       '720.00',
       '20%'
UNION ALL
SELECT '31',
       '10279156',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Multi Chewable 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '32',
       '10279139',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Eye Drops',
       '636.00',
       '508.80',
       '20%'
UNION ALL
SELECT '33',
       '10279140',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Ear Drops',
       '636.00',
       '508.80',
       '20%'
UNION ALL
SELECT '34',
       '10279141',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Healthy Liver Chewable 1 bottle',
       '1,020.00',
       '816.00',
       '20%'
UNION ALL
SELECT '35',
       '10279158',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Healthy Liver Chewable 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '36',
       '10279142',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Bladder Control 1 bottle',
       '1,020.00',
       '816.00',
       '20%'
UNION ALL
SELECT '37',
       '10279160',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Bladder Control 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '38',
       '10279143',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Calcium Chewables 1 bottle',
       '900.00',
       '720.00',
       '20%'
UNION ALL
SELECT '39',
       '10279161',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS K9 Calcium Chewables 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '40',
       '10279144',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Anti Mange Spray',
       '720.00',
       '576.00',
       '20%'
UNION ALL
SELECT '41',
       '10279145',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Tick and Flea Away 1 bottle',
       '960.00',
       '768.00',
       '20%'
UNION ALL
SELECT '42',
       '10279163',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Tick and Flea Away 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '43',
       '10279146',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Immune Health 1 bottle',
       '900.00',
       '720.00',
       '20%'
UNION ALL
SELECT '44',
       '10279165',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Immune Health 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '45',
       '10279167',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'AS Anti Coprophagic 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '46',
       '10279147',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Nefrotec Tabs 60s',
       '660.00',
       '528.00',
       '20%'
UNION ALL
SELECT '47',
       '10279169',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Nefrotec 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '48',
       '10279148',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Immunol Tab 60s',
       '720.00',
       '576.00',
       '20%'
UNION ALL
SELECT '49',
       '10279171',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Immunol Tab 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '50',
       '10279149',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Liv52 Tab 60s',
       '660.00',
       '528.00',
       '20%'
UNION ALL
SELECT '51',
       '10279174',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Liv52 1 tab',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '52',
       '10279112',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Nefrotec DS Tab 60s',
       '900.00',
       '720.00',
       '20%'
UNION ALL
SELECT '53',
       '10279153',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Nefrotec DS 1 Tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '54',
       '10279157',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Baytril 50mg Tab',
       '114.00',
       '91.20',
       '20%'
UNION ALL
SELECT '55',
       '10279159',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Aluminum OH + Magnesium OH Chewable Zilgam',
       '12.00',
       '9.60',
       '20%'
UNION ALL
SELECT '56',
       '10279162',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cefalexin Monohydrate Cap 250mg Diacef',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '57',
       '10279164',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cefalexin Monohydrate Cap 250mg Falteria',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '58',
       '10279166',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cefalexin Monohydrate Cap 500mg Exel',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '59',
       '10279168',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ceterizine HCl Syr 5mg/5ml Reax 60ml',
       '180.00',
       '144.00',
       '20%'
UNION ALL
SELECT '60',
       '10279170',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cetirizine HCl Syrup 2.5mg/ml 60ml',
       '180.00',
       '144.00',
       '20%'
UNION ALL
SELECT '61',
       '10279172',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cetirizine HCL tab 10mg Cetzy-10',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '62',
       '10279173',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ciprofloxacin tab 500mg Cyfrox',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '63',
       '10279175',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Clarithromycin Susp 125mg/5ml Clariwell 60ml',
       '300.00',
       '240.00',
       '20%'
UNION ALL
SELECT '64',
       '10279176',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Co-Amoxiclav FC Tab 625mg Asiclav 625',
       '48.00',
       '38.40',
       '20%'
UNION ALL
SELECT '65',
       '10279177',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Co-Amoxiclav Susp 250mg/62.5mg/5ml Clovimed 60ml',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '66',
       '10279178',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Co-Amoxiclav Tab 250mg/125mg Myclav 375',
       '36.00',
       '28.80',
       '20%'
UNION ALL
SELECT '67',
       '10279179',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cotrimoxazole Tab 400mg/80mg Kathrex',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '68',
       '10279180',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Cotrimoxazole Tab 800mg/160mg Zolbach',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '69',
       '10279181',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Diphenhydramine HCL Syrup 12.5mg/5ml Hiztasyn 60ml',
       '120.00',
       '96.00',
       '20%'
UNION ALL
SELECT '70',
       '10279182',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ferrous Sulfate+Folic+Vit B Complex Cap Foralivit',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '71',
       '10279183',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Furosemide Tab 20mg',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '72',
       '10279184',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Lactulose Syrup 3.35g Accelac 60ml',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '73',
       '10279185',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Metoclopramide HCl Susp 5mg/5ml Motillex 60ml',
       '120.00',
       '96.00',
       '20%'
UNION ALL
SELECT '74',
       '10279186',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Metoclopramide HCl Tab 10mg',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '75',
       '10279187',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Metronidazole Susp 125mg/5ml Ambidazol 60ml',
       '144.00',
       '115.20',
       '20%'
UNION ALL
SELECT '76',
       '10279188',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Metronidazole Tab 500mg Flagex',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '77',
       '10279189',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Omeprazole Delated Released cap 20mg Ometift',
       '30.00',
       '24.00',
       '20%'
UNION ALL
SELECT '78',
       '10279190',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Prednisolone Syrup 15mg/5ml Impresol 60ml',
       '240.00',
       '192.00',
       '20%'
UNION ALL
SELECT '79',
       '10279191',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Prednisone Susp 10mg/5ml Lefesone 60ml',
       '180.00',
       '144.00',
       '20%'
UNION ALL
SELECT '80',
       '10279192',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Prednisone Tab 10mg',
       '30.00',
       '24.00',
       '20%'
UNION ALL
SELECT '81',
       '10279193',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Prednisone Tab 5mg',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '82',
       '10279194',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ranitidine FC Tab 150mg Sriranit',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '83',
       '10279195',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Vitamin B1 + Vitamin B6 + Vitamin B12 Tab Myrevit B',
       '18.00',
       '14.40',
       '20%'
UNION ALL
SELECT '84',
       '10279196',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ursodeoxycholic Acid 300mg Tab Urcid',
       '72.00',
       '57.60',
       '20%'
UNION ALL
SELECT '85',
       '10279197',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Emerflox (Enrofloxacin 20%) 60ml',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '86',
       '10279198',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Tolfenol (Tolfenamic Acid Syrup)',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '87',
       '10279199',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pomisol Ear Drops',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '88',
       '10279200',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Love Drops 200 tabs',
       '2,400.00',
       '1,920.00',
       '20%'
UNION ALL
SELECT '89',
       '10279201',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Love Drops 1 tab',
       '30.00',
       '24.00',
       '20%'
UNION ALL
SELECT '90',
       '10279202',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Prednil 1 tab',
       '24.00',
       '19.20',
       '20%'
UNION ALL
SELECT '91',
       '10279203',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Ketaconazole 1 tab',
       '36.00',
       '28.80',
       '20%'
UNION ALL
SELECT '92',
       '10279204',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Pulmoquin Syrup 60ml',
       '240.00',
       '192.00',
       '20%'
UNION ALL
SELECT '93',
       '10279205',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'Xiclospor Eye Ointment',
       '660.00',
       '528.00',
       '20%'
UNION ALL
SELECT '94',
       '10279206',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-Collar 7.5cm',
       '240.00',
       '192.00',
       '20%'
UNION ALL
SELECT '95',
       '10279207',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-Collar 10cm',
       '300.00',
       '240.00',
       '20%'
UNION ALL
SELECT '96',
       '10279208',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-Collar 12.5cm',
       '360.00',
       '288.00',
       '20%'
UNION ALL
SELECT '97',
       '10279209',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-Collar 15cm',
       '480.00',
       '384.00',
       '20%'
UNION ALL
SELECT '98',
       '10279210',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-collar 20cm',
       '600.00',
       '480.00',
       '20%'
UNION ALL
SELECT '99',
       '10279210',
       '72-64-146-02',
       'Dog Care',
       'Health Needs',
       'E-Collar 30cm',
       '720.00',
       '576.00',
       '20%'

GO 
