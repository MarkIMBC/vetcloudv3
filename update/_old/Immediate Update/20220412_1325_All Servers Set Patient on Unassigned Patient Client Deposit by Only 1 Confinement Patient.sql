--exec _pBackUpDatabase
--GO
DECLARE @PatientConfinementOnOnePatientSpecial TABLE
  (
     ID_Patient_Confinement INT
  )

INSERT @PatientConfinementOnOnePatientSpecial
select confimentPatient.ID_Patient_Confinement
FROm   tPatient_Confinement_Patient confimentPatient
       inner join tPatient_Confinement confinement
               on confimentPatient.ID_Patient_Confinement = confinement.ID
       INNER JOIN vCompanyActive company
               on company.ID = confinement.ID_Company
WHERE  confimentPatient.ID_Patient IS NOT NULL
GROUP  BY confimentPatient.ID_Patient_Confinement
HAVING Count(*) = 1

SELECT clientDeposit.ID,
       clientDeposit.Code,
       clientDeposit.ID_Company,
       clientDeposit.ID_Patient,
       confinementPatient.ID_Patient,
       clientDeposit.ID_Patient_Confinement,
       confinementOnePatientOnly.ID_Patient_Confinement
FROm   tClientDeposit clientDeposit
       INNER JOIN tPatient_Confinement_Patient confinementPatient
               on confinementPatient.ID_Patient_Confinement = clientDeposit.ID_Patient_Confinement
       INNER JOIN @PatientConfinementOnOnePatientSpecial confinementOnePatientOnly
               on confinementOnePatientOnly.ID_Patient_Confinement = confinementPatient.ID_Patient_Confinement
WHERE  clientDeposit.ID_FilingStatus = 3
Order  by ID_Company,
          clientDeposit.ID ASC

SELECT clientDeposit.ID,
       clientDeposit.ID_Patient_Confinement,
       COUNT(*) Count
FROm   tClientDeposit clientDeposit
       INNER JOIN tPatient_Confinement_Patient confinementPatient
               on confinementPatient.ID_Patient_Confinement = clientDeposit.ID_Patient_Confinement
       INNER JOIN @PatientConfinementOnOnePatientSpecial confinementOnePatientOnly
               on confinementOnePatientOnly.ID_Patient_Confinement = confinementPatient.ID_Patient_Confinement
WHERE  clientDeposit.ID_FilingStatus = 3
Group  BY clientDeposit.ID,
          clientDeposit.ID_Patient_Confinement
HAVING COUNT(*) > 1
Order  by clientDeposit.ID,
          clientDeposit.ID_Patient_Confinement ASC

Update tClientDeposit
SET    ID_Patient = confinementPatient.ID_Patient
FROm   tClientDeposit clientDeposit
       INNER JOIN tPatient_Confinement_Patient confinementPatient
               on confinementPatient.ID_Patient_Confinement = clientDeposit.ID_Patient_Confinement
       INNER JOIN @PatientConfinementOnOnePatientSpecial confinementOnePatientOnly
               on confinementOnePatientOnly.ID_Patient_Confinement = confinementPatient.ID_Patient_Confinement
WHERE  clientDeposit.ID_FilingStatus = 3
       AND clientDeposit.ID_Patient IS NULL 
