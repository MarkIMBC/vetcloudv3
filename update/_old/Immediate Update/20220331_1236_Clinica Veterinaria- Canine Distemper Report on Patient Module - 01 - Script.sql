DECLARE @GUID_Company VARCHAR(MAX) = '460051C7-CE9C-44D4-8A24-570B4F82A4A5'
DECLARE @OID_ClinicaFigura VARCHAR(MAX)
DECLARE @ClinicaFigura VARCHAR(MAX) ='ClinicaVeterinaria-CanineDistemperConfinementAgreement'
DECLARE @OID_MODEL VARCHAR(MAX) ='38C42E70-228A-4441-98FE-96C805EF153B'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

exec _pCreateReportView
  'ClinicaVeterinaria-CanineDistemperConfinementAgreement'

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT @OID_ClinicaFigura = Oid
FROM   _tReport
WHERE  Name = @ClinicaFigura


INSERT INTO [dbo].[_tModelReport]
            ([Name],
             [IsActive],
             [ID_Company],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [DateCreated],
             [DateModified],
             [Oid_Model],
             [Oid_Report])
VALUES     ( 'Canine Distemper Confinement Agreement',
             1,
             @ID_Company,
             1,
             1,
             GETDATE(),
             GETDATE(),
             @OID_MODEL,
             @OID_ClinicaFigura)

GO 