DECLARE @GUID_Company VARCHAR(MAX) = 'E388E095-6DE7-4E39-AE66-EB643DA832C6'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company


update tCompany
SET    SOAPPlanSMSMessage = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. '
                            + CHAR(13) + CHAR(13)
                            + 'Clinic hours :9am-5pm only.' + CHAR(13)
                            + 'Close every Thursday.' + CHAR(13) + CHAR(13)
                            + 'This is a system generated message. Please do not reply.'
                            + CHAR(13) + CHAR(13)
                            + 'Contact /*CompanyName*/ /*ContactNumber*/ for more details. Thank you! '
WHERE  ID = @ID_Company 

