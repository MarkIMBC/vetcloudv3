

                if OBJECT_ID('ForImport.dbo.[PetRxITEMQuantity]') is not null
                BEGIN
                    DROP TABLE ForImport.dbo.[PetRxITEMQuantity]
                END 

                GO

            CREATE TABLE ForImport.[dbo].[PetRxITEMQuantity]
              (
                 [ITEM]     varchar(5000),
                 [QTY]      varchar(5000),
                 [RowIndex] varchar(5000),
                 [GUID]     varchar(5000)
              ) 
            

                GO

                

            INSERT INTO ForImport.[dbo].[PetRxITEMQuantity]
                        ([ITEM],
                         [QTY],
                         [RowIndex],
                         [GUID])
            SELECT '2022 ADVOCATE 10KG TO 25KG - LARGE PCS EXP: 11/23',
                   '16',
                   '1',
                   'C9E1AF18-D17B-4FA4-9E84-DDECBB04719F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ADVOCATE 25KG TO 40KG XL PCS EXP: 05/24',
                   '33',
                   '2',
                   '7D9F2B9F-6974-4490-88F5-F0FEB0D51B2D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ADVOCATE 4KG OR LESS - SMALL PCS EXP: 10/23',
                   '34',
                   '3',
                   'C9FEF29B-0B4F-476A-9A59-82CBEFE76D06-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ADVOCATE 4KG TO 10KG - MEDIUM PCS EXP: 06/24',
                   '28',
                   '4',
                   '0785CA0E-E034-4031-9A11-A9879872A5CF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ADVOCATE CAT - 4KG OR LESS PCS',
                   '33',
                   '5',
                   'AEB7F224-1FE6-4932-9741-2EE199A824B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRAVECTO LARGE 20-40KGS(INFINITY PETCARE) EXP: 11/23',
                   '10',
                   '6',
                   '3C6AB8C7-2BF6-493F-89F7-CE74ABECBD62-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRAVECTO MEDIUM 10-20KGS(INFINITY PETCARE) EXP: 10/23',
                   '10',
                   '7',
                   '4AC97B03-3597-476C-A3AC-04AE89DC5AD4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRAVECTO SMALL 4-10KGS (INFINITY PETCARE) EXP: 11/23',
                   '17',
                   '8',
                   'EB761910-3A01-4637-AB72-8921CF74F1A2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRAVECTO X-SMALL 2-4KGS (INFINITY PETCARE) EXP: 08/23',
                   '13',
                   '9',
                   'F9E5B475-AF78-4AD5-A41B-095762588FFC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BROADLINE CAT 2.5-7.5KGS-LARGE EXP: 05/23',
                   '7',
                   '10',
                   'C6BFA5A2-8B93-44DB-83E9-8622965DB502-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BROADLINE CAT LESS 2.5KG SMALL EXP: 07/23',
                   '15',
                   '11',
                   '6C1B6F05-CFAD-4226-886A-12C1AEAC78E5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CANINE WORMER 50 TABS EXP: 04/22',
                   '72',
                   '12',
                   '200DA792-B86E-44DE-B6F9-1FE52BECC817-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DRONTAL PLUS TASTY TABLET EXP: 11/25',
                   '58',
                   '13',
                   'B6187FCF-8CFB-442C-B701-47A555779741-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DRONTAL SUSP EVR',
                   '0',
                   '14',
                   '2B8E4D7C-701E-4ADC-8392-174F76FB5F5D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPOT ON PLUS - CATS EXP: 12/24',
                   '16',
                   '15',
                   'C1B6F259-A7C5-4192-B4D5-EB93F236FC93-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPOT ON PLUS (LARGE BREED) EXP: 05/23',
                   '34',
                   '16',
                   '9B0BC312-3C16-48F4-B8C9-3D396CFCF52F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPOT ON PLUS (MEDIUM BREED) ..',
                   '33',
                   '17',
                   '57C00FA2-68D1-451C-82A4-67A1DCFADD9C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPOT ON PLUS (SMALL BREED) ..',
                   '27',
                   '18',
                   '8DFC9647-8021-402E-BABF-183B7063DF56-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPRAY LARGE (250ML.) ..',
                   '2',
                   '19',
                   'CABDA64D-5217-4137-B583-BA16189F5A32-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRONTLINE SPRAY SMALL (100ML.)',
                   '2',
                   '20',
                   'D1233B30-341E-4DAA-8276-B354ACA7914A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HEARTGARD LARGE 23KGS. AND UP',
                   '40',
                   '21',
                   '5B2A28B7-9F15-4B4F-AD89-189F13B92EA7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICROZOLE 30ML',
                   '16',
                   '22',
                   '6611C28B-F7B6-49AB-9950-718AE2408990-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MILBEMAX 125MG TABLET',
                   '9',
                   '23',
                   '2C299164-9974-4A5E-84E1-B0E7FE9ADFBE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MILBEMAX 25MG',
                   '6',
                   '24',
                   '7FB29906-4C28-4F73-AA58-2F428ACCDCA1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEMATOCIDE',
                   '6',
                   '25',
                   '5821A319-BDEB-4599-82CA-398842891D57-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD SPECTRA 15-30KGS',
                   '11',
                   '26',
                   '5E797016-A227-4D9B-8967-D6A41086DC57-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD SPECTRA 2-3.5KGS',
                   '16',
                   '27',
                   '02B9FD63-4E20-4C4F-B5E1-05C82685ADD8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD SPECTRA 3.5-7.5KG',
                   '7',
                   '28',
                   '30A39DBC-2D02-4BB3-AAF1-73D37E845C32-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD SPECTRA 30-60KGS',
                   '6',
                   '29',
                   'EDD91910-3376-40D1-9178-AAE3B40D0F52-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD SPECTRA 7.5-15KGS',
                   '11',
                   '30',
                   '002F56F3-D5A3-475C-9EB8-CDD1F1E9AFA7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEXGARD XL 25-50KG',
                   '3',
                   '31',
                   'D75F2458-E893-4722-9848-64A5EC8EAC49-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PETWORMER 30ML',
                   '49',
                   '32',
                   '4C415B41-7440-4C6D-9809-C8EC427AA724-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PRAZINATE SUSPENSION 60ML',
                   '36',
                   '33',
                   '33ECF90E-1C8D-4EB2-9227-9B28909AC656-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PRAZIVET 50 TABLET',
                   '0',
                   '34',
                   'B17BCC18-83E5-42B1-BFCB-3E0203870E0F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PROXANTEL 50TABS.',
                   '0',
                   '35',
                   '578D3D6E-1BC8-41AD-B4B7-5E9985241CDD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PYRANEX',
                   '6',
                   '36',
                   '3CBC2DAD-EB8F-4EAA-B51B-96BE12DCE435-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PYRAQUANTAL 100S',
                   '4',
                   '37',
                   '3133D4C9-4947-4329-AF07-F81A77E90C60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SERESTO 2.5MG/38CM SMALL',
                   '0',
                   '38',
                   'E0DFCB5D-B0B7-432A-9906-00746F2FC6DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SERESTO 45G/70CM LARGE',
                   '0',
                   '39',
                   '1291F9D1-C84F-4FD7-8ADC-A62156FDA907-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIMPARICA 10-20KGS',
                   '15',
                   '40',
                   '27EE86F8-C2D5-4ED5-B30E-6F2655767400-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIMPARICA TRIO 5-10KGS',
                   '12',
                   '41',
                   'AAB9AFE5-C3FB-449B-8730-FED16E241B03-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 WORM SHIELD 100TABS',
                   '9',
                   '42',
                   'E01D4321-90D0-48C0-8403-0F9ADB219018-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ADVOCATE 10 - 25KGS PET DISC',
                   '0',
                   '43',
                   '01CF1308-FD3D-4AE3-AF9D-5A6975159B4B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ADVOCATE 4-10KGS PET DISCOUNT',
                   '0',
                   '44',
                   '1EC5F5C6-4D02-49CB-9359-91A0EFA267F0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ADVOCATE 4kg or less - Small',
                   '0',
                   '45',
                   '5DC6F6A4-2975-40BC-8455-FFC191B6017B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ADVOCATE 4kg to 10kg - Medium',
                   '0',
                   '46',
                   'CF1B0AAE-79A3-4646-98FA-3E81FA7D0D6A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'AMITRAZ SOLUTION MIX',
                   '0',
                   '47',
                   '87352578-425E-45FF-A24B-8BE00EB240F9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DRONTAL SUSPENSION 358P',
                   '0',
                   '48',
                   '0B25B0C2-C49B-482C-A0F1-8C759BD31561-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Frontline Spot On Plus (Small Breed) ..',
                   '0',
                   '49',
                   '2AC9E03F-E8FC-4F15-A4D6-875F8371D7F3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICODERM 15G. (ANTI-FUNGAL)',
                   '0',
                   '50',
                   '565C5BA6-656E-4D9B-AAE8-3C95D688BAC7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEXGARD LARGE BREED 10-25KGS',
                   '0',
                   '51',
                   '6D5D8871-8A81-4CA1-8B0C-4228F398D352-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEXGARD MEDIUM BREED 4 TO 10KGS',
                   '0',
                   '52',
                   'DD5829AB-ABA6-4BAB-BEF1-CEFAE9E6FACD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEXGARD SMALL BREED 2-4KGS',
                   '0',
                   '53',
                   '749A694D-65EF-4FBE-B817-3A910F3FBE8A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PRACTIC 12.5 % 1.1ML SMALL',
                   '0',
                   '54',
                   '7A3F92AA-84BF-4B9A-BFEC-704848563C68-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SALOGE SPRAY',
                   '0',
                   '55',
                   '9C363FC9-8CE7-4D17-A374-46C55825876E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WORM GUARD TABLET',
                   '0',
                   '56',
                   'DBE50F1F-AC3E-4FFC-AD3E-4664ACFE91C7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WORM RID TABLET',
                   '0',
                   '57',
                   '29F0C9AC-6919-4EB7-B05D-92F1DA581F9D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WORMECTIN 100 TABLET',
                   '0',
                   '58',
                   '277E9EDB-004E-472C-B309-29749EECB2B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 3 LAYER FOLDABLE CAGE  67*46*149',
                   '2',
                   '59',
                   '319AFB36-F734-42D6-A7C5-8E1558DEAF6F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT CAGE (2 LAYERS) BLUE / CI - 91F',
                   '1',
                   '60',
                   '52920A89-7A84-4990-ACE3-8C6F7FF33B5C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PL 1.5 EXERCISE PEN (BLACK)',
                   '0',
                   '61',
                   '0EE01604-C8D8-464A-9477-A48FAF05E211-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PL 2 EXERCISE PEN (BLACK) 61*61*8 PARTS',
                   '3',
                   '62',
                   '7D8DF275-DB71-4D50-BF76-3454F61F689D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PL 2.5 EXERCISE PEN (BLACK) 76*61*8 PARTS',
                   '3',
                   '63',
                   'B924FD7F-BB11-4968-BE3F-1ED2073389B1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PL 3 EXERCISE PEN (BLACK) 91*61*8 PARTS',
                   '2',
                   '64',
                   'E7C809C0-3243-440E-8D4B-08352BF2C4DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 1 CAGE (BLUE)/EL-1.5 FOLDABLE CAGE 46*30*37CM',
                   '5',
                   '65',
                   'BBECF377-9CFB-423C-B5F8-D3E27C1B422C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 1 CAGE (PINK) / EL-1.5 FOLDABLE DOG CAGE 46*30*37CM',
                   '2',
                   '66',
                   '49B91518-AD48-4140-BE5A-A9ABE8AF683A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 1 CAGE (PURPLE) / EL-1.5 FOLDABLE DOG CAGE 46*30*37CM',
                   '3',
                   '67',
                   '4DCBC437-05B9-4AFD-AC8E-C17F1A56253D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 CAGE (BLACK) / EL-2 FOLDABLE DOG CAGE 2FT.*1.5FT*1.7FT',
                   '1',
                   '68',
                   '71E6A210-D347-47E8-BF34-91BBA9D35AA8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 CAGE (BLUE) / EL-2 FOLDABLE DOG CAGE 2FT.*1.5FT*1.7FT',
                   '2',
                   '69',
                   '4C2E69E8-410D-4DD9-8AF8-3C5FE1DF9C65-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 CAGE (PINK) / EL-2 FOLDABLE DOG CAGE 2FT.*1.5FT*1.7FT',
                   '2',
                   '70',
                   'F4B435C7-392F-415B-A9A6-82B96782A1BD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 CAGE (PURPLE) / EL-2 FOLDABLE DOG CAGE 2FT.*1.5FT*1.7FT',
                   '0',
                   '71',
                   '38E4750F-6CCD-45F9-BE65-C034900785E6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 PLAYPEN (PINK) / 4D-A2 PLAYPEN WITH FLOORING 60*46*56.5CM',
                   '2',
                   '72',
                   '5D0DE300-37BC-4B7D-9B7F-0EDBCD1F80C9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 2 PLAYPEN (PURPLE)/4D-A2P PLAYPEN WITH FLOORING 60*46*56.5CM',
                   '2',
                   '73',
                   '452609BB-37CC-401C-BADC-0A9B84ABC215-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 3 CAGE (BLACK) / EL-2.5 FOLDABLE DOG CAGE 76*48*57CM',
                   '4',
                   '74',
                   'B983BC95-35C2-49A8-A589-68EFB808C672-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 3 CAGE (BLUE) / EL-2.5 FOLDABLE DOG CAGE 76*48*57CM',
                   '5',
                   '75',
                   '84986A69-D611-41A6-AF2A-D7196EA637BA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 3 CAGE (PINK) / EL-2.5 FOLDABLE DOG CAGE 76*48*57CM',
                   '9',
                   '76',
                   '42B26226-51D4-4242-9161-AE8D1BF493B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 3 CAGE (PURPLE) / EL-2.5 FOLDABLE DOG CAGE 76*48*57CM',
                   '8',
                   '77',
                   '42A3B773-6D68-4A9B-9022-B357349383C9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 CAGE (BLACK) / EL-3 FOLDABLE DOG CAGE 91*56*67CM',
                   '2',
                   '78',
                   '6F2638BD-5C4B-4CCC-AEF9-16078890ACD8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 CAGE (BLUE) / EL-3 FOLDABLE DOG CAGE 91*56*67CM',
                   '4',
                   '79',
                   '72BC3649-A2CB-45C3-965A-26B14153A2F2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 CAGE (PINK) / EL-3 FOLDABLE DOG CAGE 91*56*67CM',
                   '4',
                   '80',
                   'DED83EE1-183A-441A-B1D4-426F9106F84C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 CAGE (PURPLE) / EL-3 FOLDABLE DOG CAGE 91*56*67CM',
                   '5',
                   '81',
                   'B7596E24-CAAD-49C7-A314-488C072D2929-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 PLAYPEN (PINK) / 4D-A1 PLAYPEN WITH FLOORING 90*60*62CM',
                   '3',
                   '82',
                   'F9CEDC9F-DDBC-4015-A30D-D25505062D10-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 PLAYPEN (PURPLE) / 4D-A1 PLAYPEN WITH FLOORING 90*60*62CM',
                   '4',
                   '83',
                   '70920A17-4D05-41D8-AA17-E9B5E3B21F1A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 4 PLAYPEN(BLUE)/ 4D-A1 PLAYPEN WITH FLOORING 90*60*62CM',
                   '3',
                   '84',
                   '4251BC95-7349-4F02-ACC4-7A69D55B51F1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SIZE 5 GIANT 106MA FOLD CAGE (BLACK) 107*69*79 CM',
                   '1',
                   '85',
                   '8916DD40-6E1B-43D5-B333-55FE02694B52-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 0.5 CAT AND PUPPY CARRIER (42X28X26.5CM)',
                   '2',
                   '86',
                   '28D110F2-A4BA-4B7D-969A-297DE6E7918E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 1001 (48.35X31.76X30.3CM)',
                   '1',
                   '87',
                   '74AF9EEA-E45B-4F14-ADAD-240BF8A7ABBC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 1002 (58.4X36.8X35.2CM)',
                   '1',
                   '88',
                   '3F8C8D3F-6622-411B-8FCB-839003364982-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 1003 (66X47X45.8)',
                   '1',
                   '89',
                   'AEA22583-76CE-466F-A3B5-78E61E672B3A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 1004 (81.2X57.5X60.6CM.)',
                   '1',
                   '90',
                   '96788DDE-B05C-483C-AB80-77DBD2224919-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRAVEL CRATES 1005 (90.7X63.55X68.6CM)',
                   '1',
                   '91',
                   'C51FF7D7-6FA7-4F2E-A959-2A4C3A8B0289-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT CAGE (3 LAYERS) W/ WIRE FLOOR / CP-3W 91X56.5X130CM',
                   '0',
                   '92',
                   '12D03557-1F89-41B4-88A6-2F418C7CEE23-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT CAGE (3 LAYERS) WITH WIRE FLOORING CI-3F 92X62X130CM',
                   '0',
                   '93',
                   'DBBC3A37-33EE-491D-8653-AB9CD912B9A9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'D-213 - B/Heavy Duty Wire Cage W/ Mats 122*75*99 cm',
                   '0',
                   '94',
                   '4783B84B-00A9-4CAE-AA12-8349C08F546A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FENCE-LARGE (PURPLE)/6D-A1 L70XH69X6 PANEL',
                   '0',
                   '95',
                   'A8C59C24-5811-4B7D-958E-FFBBD9F7EE34-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SIZE 1 CAGE (BLACK) / EL-1.5 FOLDABLE DOG CAGE 46*30*37CM',
                   '0',
                   '96',
                   'A8663181-491E-4690-8541-65D2413C264C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SIZE 2 PLAYPEN (BLUE) / 4D-A2 PLAYPEN WITH FLOORING 60*46*56.5CM',
                   '0',
                   '97',
                   '9F24E894-A29E-4AB2-8690-C4D394FDE704-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Size 6 Giant (Black Silver) D2133LA L125.5xW92.5xH103cm',
                   '0',
                   '98',
                   '5BB8B716-C77F-40C8-B179-D271FBF3F12D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WEE WEE 3-SIDES TRAINING TRAY',
                   '0',
                   '99',
                   'BAE3CF14-BEB5-4615-995A-9A1EB8DE2369-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WEE WEE TRAINING TRAY LARGE',
                   '0',
                   '100',
                   'FC5CDB5E-6B34-4F8B-ADEA-5538D327E82F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE MOUSSE WITH CHICKEN LIVER (K & A)',
                   '65',
                   '101',
                   'EA135705-CE27-436A-863F-58EF0D2FB830-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE MOUSSE WITH SMOKED TUNA (K & A)',
                   '74',
                   '102',
                   'B7863C4B-7564-4CAB-B516-49820FBC674B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE PATE WITH CHOPPED SARDINE AND CHICKEN',
                   '54',
                   '103',
                   'F8353E40-87A4-44D9-ADDC-619C8AC2DAF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE PATE WITH CHOPPED SARDINE AND SALMON',
                   '54',
                   '104',
                   '764EC376-7D73-4958-BE59-7AE9AD881AE2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE PATE WITH CHOPPED SARDINE AND SHRIMP',
                   '78',
                   '105',
                   '0200CBFD-7E80-4E91-9B26-D562C7A9386D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT - SARDINE PATE WITH CHOPPED SARDINE AND WHITEBAIT',
                   '77',
                   '106',
                   '7AF5FFA6-2797-4D8E-A5D3-EF3C9D534F7D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT FOOD - OCEAN FISH FOR ALL STAGE 20KGS.',
                   '1',
                   '107',
                   '64E14349-018B-4674-83C4-A25A05BEE25D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT FOOD 1KG.',
                   '13',
                   '108',
                   '2A110E07-74A9-4793-8B6A-D4860DCD7E4B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT WETFOOD TUNA IN JELLY CHICKEN',
                   '45',
                   '109',
                   'C1A9B777-C626-4F30-828B-B89646C64925-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT WETFOOD TUNA IN JELLY HAIRBALL',
                   '46',
                   '110',
                   '7DF03D6A-0C60-40D7-8F5F-B589580A6FC3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT WETFOOD TUNA IN JELLY SHRIMP',
                   '47',
                   '111',
                   '9EFEE0C9-B213-4498-8147-2A081A6C5000-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT WETFOOD TUNA IN JELLY VEGETABLE',
                   '48',
                   '112',
                   '14B702B6-7181-4585-89FF-921DE1FE1B13-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY CAT WETFOOD TUNA IN JELLY WHITEBAIT',
                   '48',
                   '113',
                   '0F2EAE7D-46C5-4391-96A2-6C67719335E7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY CAT TREAT SALMON',
                   '22',
                   '114',
                   '5F3F1825-86AC-4466-B415-5DAF48549D63-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY CAT TREAT SEAFOOD',
                   '30',
                   '115',
                   'D5623FD7-0FE6-406C-887A-B0CF0F49A72B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY CAT TREAT TUNA',
                   '21',
                   '116',
                   '32D8AED4-DB8B-4D4D-9E9C-57266DEA8FB2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY LIQUID TUNA  SCALLOP 56G',
                   '30',
                   '117',
                   '4C78FEAC-7752-45E2-8387-D8455B8B8321-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY LIQUID TUNA CRAB SURIMI 56G',
                   '35',
                   '118',
                   'EDA9CBB6-1C2F-4C79-9538-D30FD45247A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JINNY LIQUID TUNA SHRIMP 56G',
                   '31',
                   '119',
                   'BE795B9E-F499-4C9E-B424-6432D94F38C8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH (AGNELLO) LAMB ADULT 85G',
                   '24',
                   '120',
                   '686E5945-7FE8-4BB9-B932-86A1672DFE72-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH (CONIGLIO) LAMB RABBIT 85G',
                   '13',
                   '121',
                   '13AB9FBE-7A38-44A0-A92B-DC253C667C9C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH (SGOMBRO) MACKAREL STERILIZED 85G',
                   '24',
                   '122',
                   '64B6E9F0-ED6C-40A0-838C-7CC43DA1C4FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH (TROTA) TROUT 85G',
                   '31',
                   '123',
                   'F9FC9439-0CB6-45CA-A32E-53A4D57361C1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH (VITELLO) VEAL STERILIZED 85G',
                   '23',
                   '124',
                   '29302D9B-4166-403B-8BCE-C2086B9B8634-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY CAT POUCH STERILIZED 85G GALLETO COCKAREL',
                   '12',
                   '125',
                   '2143F4E9-66A3-4CE7-82B9-79B44E3BEC08-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE JELLY POUCH GRILL 85G KITTEN SALMON (SALMON)',
                   '43',
                   '126',
                   'A808E9DA-8C05-4570-959F-96CDCCBCCF87-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE NATURAL SUPER PREMIUM ADULT  CAT 10KG',
                   '6',
                   '127',
                   'AB9AE106-904B-4944-B43D-1A93825DA675-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE NATURAL SUPER PREMIUM KITTEN 10KGS',
                   '5',
                   '128',
                   '917A549F-9332-4CBF-943B-DE73C5E1B48D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT 7KGS',
                   '55',
                   '129',
                   '9E66EAE4-A049-4321-80B4-31B72CCE1045-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT MOUSSE WITH BEEF & LIVER 400G',
                   '33',
                   '130',
                   '4EC21708-CE27-415A-A112-CE08D144B871-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT MOUSSE WITH CHICKEN HEART & LIVER 400G',
                   '15',
                   '131',
                   'C09A7702-58C3-4824-9BC0-4534E76F2E2A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT MOUSSE WITH RABBIT & LIVER 400G',
                   '48',
                   '132',
                   '5D289FAF-CEF4-4818-8230-6242FD8F7763-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT MOUSSE WITH TUNA & OCEAN FISH 400G',
                   '3',
                   '133',
                   'A5964974-0C8C-43F9-B494-938407E90CD5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT REPACKED 1KG',
                   '10',
                   '134',
                   '63E2B799-F426-4CDF-901B-1DA7477709EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT URINARY 1KG',
                   '2',
                   '135',
                   'E520274C-A045-40CF-9201-C1F8AD1F1920-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL CAT URINARY 7KGS',
                   '0',
                   '136',
                   '496D582E-BCB4-4106-85EF-1E07B007A35A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION FELINE URINARY OXALATE 1.5KG',
                   '0',
                   '137',
                   '08E9C2A5-7D97-486A-A7CC-19BF22783BC1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION FELINE URINARY STRUVITE 1.5KGS',
                   '3',
                   '138',
                   '729A3F67-7C0B-4FFF-A514-7CCC9179154C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION FELINE WET RECOVERY ALUTRAY 100GMS',
                   '29',
                   '139',
                   '989C4307-9E2C-405D-8B5A-83F67B9A865A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY CATCARE CAT FOOD 1KG.',
                   '4',
                   '140',
                   'D8F6BBA6-5E43-4223-B7B6-B0C3EEAE3A3F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY CATCARE CAT FOOD 7KGS',
                   '8',
                   '141',
                   '7EF51D5E-C916-4953-8E9B-63942738AE51-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LE CHAT POUCH BEEF ADULT',
                   '0',
                   '142',
                   '4911B197-4D67-4F59-AC88-94C2CEB0612A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LE CHAT POUCH CHUNKIES SALMON ADULT',
                   '0',
                   '143',
                   '98DEB5BE-203E-4730-95B0-2085FD2B15CC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LE CHAT POUCH TUNA KITTEN',
                   '0',
                   '144',
                   'EA789C19-022F-444E-8E43-A0DEE540F890-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONELLO ADULT 7KG.',
                   '0',
                   '145',
                   '1BE06578-1053-473F-9D95-DD9F7F562CBE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONELLO CAT 7KG',
                   '0',
                   '146',
                   'C80193E1-81D1-468E-B464-DABF44C15E22-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE DELICATE CHICKEN WITH HAM 80G.',
                   '0',
                   '147',
                   '92A76C9A-4B68-45F9-8006-09F71A76901A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE JELLY TUNA WITH ANCHOVIES 80G.',
                   '0',
                   '148',
                   'F4A15282-130A-422F-B7E2-915C8AEEF95B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE JELLY TUNA WITH SHRIMP 80G.',
                   '0',
                   '149',
                   'C3C21803-055A-467B-9AF5-8ACD749226DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE JELLY TUNA WITH SURIMI 80G.',
                   '0',
                   '150',
                   'C63B7A06-016E-4241-8136-E0FC675636F2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POWER CAT OCEAN FISH 7KGS PLUS 1',
                   '0',
                   '151',
                   '6345BA06-EE55-466F-AD40-10EAECBEED4A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POWER CAT OCEAN TUNA 7KG PLUS 1',
                   '0',
                   '152',
                   '1A31679D-81B6-4390-B058-979875C3B369-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPECIAL CAT 1KG',
                   '0',
                   '153',
                   '6ECE3D9B-C68A-4DE2-BEE9-B36477B96A9D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION FELINE RENAL 1.5KGS',
                   '0',
                   '154',
                   'A969F080-35A5-4C2E-A2EB-5E957784444C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION FELINE WET ALUTRAY RENAL&OXALATE 100GMS',
                   '0',
                   '155',
                   '7209CD81-DC6F-4E64-AB57-71B83A915C29-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION FELINE WET ALUTRAY URINARY STRUVITE 100GMS',
                   '0',
                   '156',
                   '2199F428-9B77-4382-ABC1-95CFB78C4342-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT TOY 3PCS BALL WITH TAIL',
                   '0',
                   '157',
                   '509B913B-E84B-4892-87CF-DF09623BDC9A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CLEAN CAT LITTER SAND 5KGS / 8 LITERS',
                   '0',
                   '158',
                   '63FD4840-A10D-4C23-B4BE-20852C632AB1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Michiko Cat - Spider Toy',
                   '0',
                   '159',
                   '3E52EDE3-67C7-4CED-A906-720294A2959A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '1/2 CIRCLE ROUND',
                   '0',
                   '160',
                   'D36E1CC4-C512-438A-89DC-C578C4983A90-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AGUA OXIGENADA 1L',
                   '3',
                   '161',
                   '74E8DEF4-4041-47DE-AECF-9DC111BB568F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ALCOHOL 1GALLON (2018) 70%',
                   '3',
                   '162',
                   '1C28D1A4-C7A9-4C25-A46A-268203FAAF31-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARGYLE ASSORTED DOG CATHETER MULTI PET',
                   '16',
                   '163',
                   'B2A1F369-973C-4CAC-8EBA-DAEACA6DCD01-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BANDAGE NUTRIVET',
                   '0',
                   '164',
                   '07DD0CA0-B3D0-4A8E-9369-9CA0CF50D732-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT CATHETER WITH STYLET 1.0MM',
                   '43',
                   '165',
                   'F6DC6648-8DEC-4D4C-B4A8-07520BB2DECF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT CATHETER WITHOUT STYLET',
                   '4',
                   '166',
                   '933AAF0F-1413-44FC-92F8-AB5B4CB0C864-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CATHETER IV CANNULA G.18 (CATHY) GREEN',
                   '55',
                   '167',
                   '1F35B668-C8D8-4CEF-A4DB-FFA4499BED63-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHROMIC CATGUT SURGICAL SUTURES 2-0 CCUTTING TUDOR',
                   '9',
                   '168',
                   '4137B014-5056-4219-908F-41ADB5206E1B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON BALLS BIG PACK',
                   '10',
                   '169',
                   'F8761D34-910C-4A3B-ABDA-C816B3F5261B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON ROLLS',
                   '3',
                   '170',
                   'A97E25D3-B343-4CEE-9F2B-C20E368A3199-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CRYLEX 1 ROUND SUTURE',
                   '0',
                   '171',
                   'DCAE015E-BFC9-4C67-9784-9A0181CF841E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CRYLEX 2.0 ROUND SUTURE',
                   '0',
                   '172',
                   '2DA3046F-08E5-4D56-91B4-DC7E610B8C63-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 D5LR 1LITER EUROMED (2018)',
                   '48',
                   '173',
                   'D7D38C30-9E0D-413C-9111-E17F793D5854-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 D5NM 1L ORANGE 720P',
                   '44',
                   '174',
                   'ECD62A09-3952-495F-B732-13C7F95266DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE GLOVES LATEX LARGE (SURE-GUARD)',
                   '23',
                   '175',
                   '9591124C-3141-4247-889A-C26B13CF2CC3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE GLOVES LATEX MEDIUM (SURE-GUARD)',
                   '3',
                   '176',
                   'D7344AD3-252A-4CC9-8E0B-2EF87457B146-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE GLOVES LATEX SMALL (SURE-GUARD)',
                   '11',
                   '177',
                   '0C14899C-C0B9-417D-B4AA-870AF41AC07D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE SYRINGE 1CC (DELCARE) 100S',
                   '20',
                   '178',
                   '29557A06-3D45-4519-ADE7-C674FEA60348-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE SYRINGE 20CC (DELCARE) 100S',
                   '1',
                   '179',
                   'C44871C3-A660-4E70-9D20-8D414A2A2BB4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DISPOSABLE SYRINGE 3CC (DELCARE) 100S',
                   '30',
                   '180',
                   '0D64FBE7-E0DA-4BF8-B9D3-E0344FFAB45E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DRAPES',
                   '70',
                   '181',
                   '83F0EA9F-FA74-4EE1-8F4C-6ED80F2988FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 EDTA K2 0.5ML TRULAB',
                   '156',
                   '182',
                   '47C86FCE-1BF1-41AC-8726-DA7A028A0E35-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ELASTIC BANDAGE 3INCHES PLAIN (SURGERY ROOM)',
                   '108',
                   '183',
                   'E80DED08-011D-4B8A-951B-A3D649D66F4C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ENZYMATIC CLEANER 30ML',
                   '13',
                   '184',
                   'E30B08D8-184A-46C8-8F82-DEFE56DCF3DD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ES QUATICIDE',
                   '11',
                   '185',
                   '8DABC7C3-3A18-4B02-9FF4-3A41204AF8BF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FECALYZER SET TWIN OX',
                   '2',
                   '186',
                   '04E931F0-0AF2-4034-BE94-F3530E2E385E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FOAM PADDED SPLINT LARGE',
                   '12',
                   '187',
                   'C7801F79-9DCB-45FD-9FAB-B972A70F7A54-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FOAM PADDED SPLINT MEDIUM',
                   '12',
                   '188',
                   '4BB886C8-5D7B-467D-9C21-AA0440116670-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FOAM PADDED SPLINT SMALL',
                   '11',
                   '189',
                   'C8EA2ADB-393D-4B0A-9480-C67B37CC46DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GAUZE PAD 4X4 NON STERILE 90P',
                   '16',
                   '190',
                   '1EC5A6BC-6562-4943-A0BC-FD8D0892E01B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GAUZE PAD 4X4 STERILE (TRUCARE)',
                   '5',
                   '191',
                   '51402A16-FB3E-4840-A7B3-6A2122A61EB9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GLOVES STERILE #7 DR. CARE',
                   '2',
                   '192',
                   '9F72342D-3261-4681-A18E-888C1EFEFBA5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV CANNULA G22 MS (CANCARE)',
                   '100',
                   '193',
                   '5EB09B2A-170E-48C2-820B-90301C11B8EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV CANNULA G26',
                   '46',
                   '194',
                   '8EB2B216-8FAE-4F4B-9BED-D52D8892C93F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV CATHETER SURGITECH G24 JAG MED',
                   '160',
                   '195',
                   '18108CBA-DC48-4354-A22A-623D71EE33CE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV POLE',
                   '1',
                   '196',
                   'DF58AA29-A88D-4683-95C1-C59CFE0792D2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV SET ADULT',
                   '84',
                   '197',
                   '57D2710A-4D54-4431-989D-CAC1F26E2974-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IV SET PEDIA',
                   '90',
                   '198',
                   '1A1E75F6-2EAD-484A-85EF-70706788BCC0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICRO SURGERY SET',
                   '0',
                   '199',
                   '5C966767-AB35-422A-A90A-90F68EA0C91D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICROSCORPIC GLASS SLIDE 7105 FROSTED',
                   '10',
                   '200',
                   '36FC714F-CF26-402D-9248-3E8065E4765C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MINDRAY DILUENT 20L',
                   '0',
                   '201',
                   '3CFFF122-3B54-4645-A0D5-79D24FF3E2B5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MINDRAY RINSE 20L',
                   '1',
                   '202',
                   'EACD43B1-1C17-4FC8-9FD3-51DD27518EE0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MINDRAY V-28CFL LYSE',
                   '2',
                   '203',
                   '99DBD050-4F68-4527-8083-6829634A95CF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MINDRAY V-28E E-Z CLEANSER',
                   '2',
                   '204',
                   '9A46297C-D513-46E3-A4E9-EA0CDB9EA5FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MODABLE SPLINT (ORTHISPLINT) 52CM X 92CM',
                   '3',
                   '205',
                   '98E24482-E717-482F-B491-504E5C0076B8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NGT DUODENAL TUBE FR8 (KENXIN)',
                   '8',
                   '206',
                   '7B611502-CA07-40D7-9F39-929A95651CFD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NGT FR10 MED TRADE MED SUPPLY',
                   '8',
                   '207',
                   '0D96A886-E59F-4B58-9C6A-1885D4903B9D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NGT FR5 PARTNERS',
                   '5',
                   '208',
                   '637CB816-374B-4244-851A-D028AE85317E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON 3-0 ROUND',
                   '7',
                   '209',
                   'FCACD2C4-C3CB-4B6B-B2E4-6596D9C7CC40-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 OAKTREE POLYGLACTIN 1.0 ROUND',
                   '12',
                   '210',
                   '4AAAA0F0-A08C-4E73-85B8-BF326840D7C7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ORTHOWRAP',
                   '3',
                   '211',
                   'E1122E1D-4EC0-40C1-B923-ACFD5E7A442F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAIN CATGUT SURGICAL SUTURES 2-0 ROUND TUDOR',
                   '12',
                   '212',
                   '887BFDD2-B529-4AD0-A8AD-BE654428BF6C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PNSS 1L 12S EURO',
                   '35',
                   '213',
                   '0631D2CB-9204-4CF7-A160-E8F9420E3489-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PNSS 50ML EUROMED',
                   '30',
                   '214',
                   '6F27DCE9-5F8C-48B8-8A8B-CC350D9E6133-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PNSS IRRIGATION 1L (EUROMED)',
                   '48',
                   '215',
                   'DD5ABDB4-901A-4400-9B0B-EF211FFBCD74-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYDIAXONE 4-0 CUTTING',
                   '36',
                   '216',
                   '9808955C-0726-466E-B7B6-ACE0995FDA72-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYDIAXONE 4-0 TAPER',
                   '0',
                   '217',
                   '4C26A9F3-58D4-489F-99B8-967959DD9A6D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYGACTIN 1-0 ROUND (TUDOR)(2018) 12PCS',
                   '27',
                   '218',
                   'F891325C-5D02-4DBD-BD2E-3DF74AC23437-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYGLACTIN 1.0 CUTTING',
                   '15',
                   '219',
                   '7E1FECEC-BB12-43B5-A536-28542B7D7A12-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYGLACTIN 2-0 CUTTING',
                   '21',
                   '220',
                   '32F35CC2-5FCD-47D0-847F-F68E14AB1C68-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYGLACTIN 3-0 CUTTING TUDOR',
                   '58',
                   '221',
                   'AD95A3B9-6B20-408C-8F0A-0DA3F5A3F1BA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POLYGLACTIN 3-0 ROUND 12S PER BOX',
                   '24',
                   '222',
                   'EC886AB0-A21D-49D4-98C1-80A5B4E461DC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POVIDONE IODINE 10% GAL',
                   '3',
                   '223',
                   'B34691D0-E0D5-4E96-AA1D-4B2E6AF5930A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SILICONE POLEY 10PR CATHETER',
                   '3',
                   '224',
                   '38160F78-3919-46EB-9916-4D5D441E0798-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SILICONE POLEY 12PR CATHETER',
                   '2',
                   '225',
                   'F11123F4-DD4D-4F2D-BAC2-D4FE5CFC6CE0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SILICONE POLEY 6PR CATHETER',
                   '3',
                   '226',
                   'F4895945-88B6-47EC-BF71-9E2382620B56-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SILICONE POLEY 8PR CATHETER',
                   '3',
                   '227',
                   'EB6378C9-37CB-495B-A04F-CEB402ABE10F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 STERILE WATER 50ML',
                   '17',
                   '228',
                   '37672F66-98CE-4397-AE33-E070E3597132-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SURGICAL BLADE #10 STAINLESS (PARTNERS)',
                   '101',
                   '229',
                   'E404DC9F-95E5-469E-ADBE-463A8FA4E299-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SUTURE 2.0 NYLON TUDOR CURVED',
                   '36',
                   '230',
                   '00F2A2B6-7442-431B-B5EB-6339C53DFF49-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SYRINGE 10CC COSMED (2018)',
                   '293',
                   '231',
                   '14D6CB94-8AFF-4440-BF02-5E9BB6F448EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SYRINGE 1CC COSMED (2018)',
                   '0',
                   '232',
                   '2F35E037-4EF4-4B80-9307-02F5A5954FAB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SYRINGE 30ML 2019 50S',
                   '79',
                   '233',
                   '56CE2688-6A55-4A87-9545-6B4FE1662240-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRANSGEL ULTRASOUND GEL',
                   '3',
                   '234',
                   '78D99DA1-B656-437A-BFAA-098D19C5C80E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TWIN OX DOG CATHETHER ASST ARGYLE',
                   '0',
                   '235',
                   '30FA2F1E-60A7-4B42-B0CB-2FC1BA4FFA54-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIMEX ADHESIVE PLASTER',
                   '0',
                   '236',
                   '5F069791-79D5-4AA6-9E0B-13C5D1EA1A9F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET ANESTHESIA MASK MED',
                   '1',
                   '237',
                   '94EE1885-1C2C-48D0-BB82-605B1E0F6886-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET ANESTHESIA MASK SMALL',
                   '1',
                   '238',
                   '76B625F7-4A2A-4FC8-8E49-0ADD84A52237-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VICRYL SUTURES 1.0 ROUND  2350P',
                   '48',
                   '239',
                   '7FFF9D80-F209-4723-A289-027E1B61D67A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VICRYL SUTURES 2.0 ROUND (2018)',
                   '64',
                   '240',
                   '3E288601-C6C0-4B87-890F-2A14781E76BB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VICRYL SUTURES 3.0 ROUND 1650P',
                   '44',
                   '241',
                   '6272A9CD-A830-49A4-843E-FE9B1C0CDE56-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VRYL 910 ABSORBABLE SURGICAL SUTURES 3-0 CURVED CUTTING',
                   '4',
                   '242',
                   'BE462A97-2B2F-4046-9446-060E5158DD1C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VRYL 910 PGA ABSORBABLE SURGICAL 3-0 ROUND',
                   '8',
                   '243',
                   'C05334D3-88A3-4D53-BDA1-A51EE63C4102-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZOLETIL 100',
                   '0',
                   '244',
                   '4FF2385C-0782-40A1-A33A-A98FFAC000BF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZOLETIL CARCHI 50',
                   '29',
                   '245',
                   '33537E25-2277-4B7D-A9A0-912D600DD2A0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ADSON THUMB FORCEP',
                   '0',
                   '246',
                   '51967680-176F-43BC-9F2A-8FA16CF9F985-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ALCOHOL REGULAR',
                   '0',
                   '247',
                   '74CFA93C-73BE-4D8C-BA69-5D461C7E5D4C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ALCOHOL SCENTED',
                   '0',
                   '248',
                   '009828D0-DA85-46A4-82E0-05C5F0C1943C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BALFOUR 14CM',
                   '0',
                   '249',
                   '741F2AB0-F8B2-48C2-9331-2E79587F6BD4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BALFOUR 9CM',
                   '0',
                   '250',
                   '5B38EB69-27DD-4CD9-BF15-BE45BA4D2476-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT CATHETER VETAID 1.3X100MM',
                   '0',
                   '251',
                   '2B68460F-B33E-42B5-B564-F52033963D0A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT CATHETER WITH STYLET 1.3MM',
                   '0',
                   '252',
                   'E76766E1-7732-43ED-B9CC-B56748BE57E0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHUNKER SCISSOR',
                   '0',
                   '253',
                   '76545992-1D70-4E13-BB99-29044A8E8ED7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COTTON THREAD SPOOL',
                   '0',
                   '254',
                   '5D597265-A52E-430D-9783-8B03B25BC17A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COVID-19 AG TEST',
                   '0',
                   '255',
                   'E98A36C3-35DD-40A6-80C0-A26EB6E134F1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'D5LR 500ML EUROMED (2018)',
                   '0',
                   '256',
                   '1F99CA26-76C0-47C7-8359-09A6B4B5CDB8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DEBAKEY VASCULAR CLAMP 10INCH',
                   '0',
                   '257',
                   '4889BC10-87E8-4DEA-9F0B-E765AD81C64B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DEV OPEN END CAT CATHETER VAXILIFE',
                   '0',
                   '258',
                   '67BAED08-2B62-4247-990F-91E5F62DB137-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DISPOSABLE GLOVES LATEX MEDIUM (VANTAGE) POWDERED',
                   '0',
                   '259',
                   'A2BB5761-786F-4F99-A33C-792E362B71B8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DR. CHOICE GLOVES SMALL (2018)',
                   '0',
                   '260',
                   '44C9B70F-00C8-49CE-AB90-D78798BA38A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FACEMASK PARTNERS',
                   '0',
                   '261',
                   '3CC70FBD-B3E4-40D8-9FED-AB5166EF00CC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GELPI 3.5"',
                   '0',
                   '262',
                   'A33DCEED-57B2-41DC-8339-97A0AD64D7FD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GELPI 5.5"',
                   '0',
                   '263',
                   '29B7C20F-3B85-4A24-8080-99BE8B18469D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GENTAMICIN BOTTLE',
                   '0',
                   '264',
                   'F56C1B78-8D36-4CD3-8CD4-E5A2FF72864A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GLASS SLIDE',
                   '0',
                   '265',
                   '2916990E-7FAB-41D0-86F4-FD1183E7FCC6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GLOVES DISPOSABLE M 110P',
                   '0',
                   '266',
                   '7FA9C06B-1863-4B4D-8985-DB4835A21274-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GLOVES DISPOSABLE NITRILE MEDIUM VANTAGE',
                   '0',
                   '267',
                   '49257C10-45D2-4E2A-9F55-47EF4B103E67-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GLOVES DISPOSABLE SMALL 110P',
                   '0',
                   '268',
                   '60A4F797-6DD9-4595-B1CE-CD8E7204B0AE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GLOVES LARGE',
                   '0',
                   '269',
                   'A8030606-58E4-452F-9701-A52A8DD26F52-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HEAT PAD',
                   '0',
                   '270',
                   'EEEB3A37-95C3-4EFB-89B8-5C9DDD3F8A2C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HYDROGEN PEROXIDE',
                   '0',
                   '271',
                   'EC8D50A6-F0C8-4429-97A1-D3231A88FD29-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'INSYTE-W G24 (BD) (2018)',
                   '0',
                   '272',
                   '06CC7097-0FBA-4B6E-9F57-B99AD647C378-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IV catheter Indoflon 1200p',
                   '0',
                   '273',
                   '60259B7B-A846-461F-AEAF-11469742CFBD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IV SET MACROSET ADULT COSMED (2018)',
                   '0',
                   '274',
                   '6883428E-AEBA-4E1A-BEF7-80D3B78C4E16-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IV SET MICROSET PEDIA COSMED (2018)',
                   '0',
                   '275',
                   'A1F88814-5D1C-4D37-8FC9-D1DA59215058-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MALACHITE GREEN',
                   '0',
                   '276',
                   '5169D389-3F48-44FC-9A97-23CE3D93954B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MAYO SCISSOR CURVE 5IN',
                   '0',
                   '277',
                   'F2802176-227E-4A98-B711-4EF8D2281FC5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'METZEMBAUM STRAIGHT 7IN',
                   '0',
                   '278',
                   '8A24F3A5-F8AA-4D86-8631-A53DFA85C4B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MS RODETOX GRAINS 10KG',
                   '0',
                   '279',
                   'CBBFF530-6589-4DAE-90F7-29076F539372-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'OAKTREE POLYGLACTIN 2.0 ROUND',
                   '0',
                   '280',
                   'FDC124DB-84FD-4559-8607-4879B7986A71-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'OLSER HEGAR TWIN OX',
                   '0',
                   '281',
                   'C1186277-287E-493A-B777-4851F139F344-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POLYGACTIN 3-0 ROUND (TUDOR)(2018) 12PCS',
                   '0',
                   '282',
                   'EC11B130-8014-401E-B9F8-22786CD47F33-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POVIDONE IODINE 1LITER',
                   '0',
                   '283',
                   '82432EFA-9B42-4C37-8E5D-271182F4878C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PROBE DIRECTOR OR GROOVE DIRECTOR',
                   '0',
                   '284',
                   'DDE5E425-01A9-4CEE-A329-2053EC47C518-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PS DOG CATHETER 1.3MMX500MMM',
                   '0',
                   '285',
                   'AB7F2491-E119-48CE-B3B7-C345E7C9D6F9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PS DOG CATHETER 1.6MMX500MM',
                   '0',
                   '286',
                   '28E72B0F-DD61-4D66-9646-29DF14F514A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PS DOG CATHETER 2.0MMX 500MM',
                   '0',
                   '287',
                   '7B2801CA-F1C9-44D3-8573-E408F68EF9D2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SILK SUTURE ROUND STERILE 1-0',
                   '0',
                   '288',
                   '058FBE71-07A2-4857-A67A-C4A58866E911-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SUTURE SILK 2.0 TUDOR CURVED',
                   '0',
                   '289',
                   'A181D89E-DF5E-44A8-A458-F018ECEA58E0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SYRINGE 20CC COSMED 50S',
                   '0',
                   '290',
                   '67DD6500-4926-46E2-B76A-911334A00A08-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SYRINGE 30ML',
                   '0',
                   '291',
                   '88781CD2-4F57-4637-9E1B-477A30C9DF54-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Syringe 3cc Cosmed 190p',
                   '0',
                   '292',
                   'C2B2F0BA-D392-49DE-AA82-0C3E1973C387-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SYRINGE 3CC INDOPLAS (2018)',
                   '0',
                   '293',
                   '458D9182-F874-4C50-9CF8-F58853500C24-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SYRINGE 5CC INDOPLAS',
                   '0',
                   '294',
                   '1F6BA8F7-F6E0-4D84-A495-EF8FC6B8C8DA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SYRINGE 5OML 1BOX 25S',
                   '0',
                   '295',
                   'ED399B5A-AD4E-42DF-B542-D4FE9F6DBEF4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TIPS YELLOW',
                   '0',
                   '296',
                   '38CB5BF3-19C9-4946-92DD-FA9B858EA5D0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOM CATHETER CLOSE END',
                   '0',
                   '297',
                   '61BF25BA-0FB1-4002-9A4B-4DAE013500FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TONGUE DEPRESSOR STERILE',
                   '0',
                   '298',
                   '1665AD8E-BC1B-4E70-98E6-37C1CDBFEF93-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TWIN OX TOM CATHETHER OPEN END',
                   '0',
                   '299',
                   '717C3DE3-FD1F-4475-956A-025EF505CDFB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VICRYL 4.0 ROUND',
                   '0',
                   '300',
                   '0F9FE117-C074-40B4-842E-0FD3F6D1BD5C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AOZI CAN FOR DOG - BEEF',
                   '47',
                   '301',
                   '49497DD8-BAE4-4DBA-B3D5-8F90916CED92-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AOZI PUPPY 1KG.',
                   '14',
                   '302',
                   'D3B5252C-C70A-4DF0-AB29-8034988F402D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AOZI PUPPY 20KGS',
                   '3',
                   '303',
                   'CFB3C4EC-9F3B-47E7-88CC-A0A73574BC53-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BREEDERS PACK BEEF PRO PERFORMANCE 1KG',
                   '20',
                   '304',
                   '98986752-9E31-4B68-9331-EE08DF01E935-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BREEDERS PACK BEEF PRO PUPPY 1KG',
                   '7',
                   '305',
                   'FB126453-B4C2-439E-BB57-505675DDF09D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BREEDERS PACK BEEFPRO PERFORMANCE 50LBS .',
                   '1',
                   '306',
                   '1D7A6ED1-71BD-41CE-A8A9-6A22ADD1A8A5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BREEDERS PACK BEEFPRO PUPPY 50LBS .',
                   '8',
                   '307',
                   'BA21C065-B376-49B1-8501-93F7F2A38E6C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRIT GRAINFREE SALMON & POTATO 12KGS.',
                   '10',
                   '308',
                   '262C090A-7445-42E5-A052-B0426E534021-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRIT PUPPY LAMB & RICE 12KGS',
                   '0',
                   '309',
                   '52CD6391-D27A-4ED4-BB4C-D74F486BDF40-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOLISTIC 1KG',
                   '14',
                   '310',
                   '812A8A16-C2D7-496D-9A22-1A206752DF7E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOLISTIC PUPPY 15KGS',
                   '3',
                   '311',
                   '913A2E39-AB9E-4118-9662-83391AE5FE46-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY HI PROTEIN 1KG',
                   '10',
                   '312',
                   '5CCFB5BD-4FBF-4A69-B3F4-25185E6D5B9C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 INFINITY HI PROTEIN 20KG',
                   '235',
                   '313',
                   '55288B70-DD2E-4A66-A6A9-55AE08430A6C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE POUCH GRILL CHUNKIES BEEF',
                   '97',
                   '314',
                   '5D840B8F-852A-4BBC-9CA3-F5FD57D79872-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE POUCH GRILL CHUNKIES LAMB',
                   '42',
                   '315',
                   '46461D3A-4BC2-483E-8C76-25B711458E17-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE POUCH GRILL CHUNKIES PORK',
                   '46',
                   '316',
                   '741F7801-6445-42B6-B878-8028B84565E2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONGE POUCH GRILL CHUNKIES SALMON',
                   '120',
                   '317',
                   'E2E21660-206F-463A-89CA-C8436474B272-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MR NICE GUY ADULT 15KGS',
                   '3',
                   '318',
                   'C78CB1DB-710D-4C72-BAD1-96709F71019D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MR NICE GUY ADULT 1KG',
                   '10',
                   '319',
                   '657FFE32-5C55-4E85-BAAC-CF4F6994007F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MR NICE GUY PUPPY 15KGS',
                   '3',
                   '320',
                   '91BF7BB3-6A94-4B1E-9339-9F208650CAB9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MR NICE GUY PUPPY 1KG',
                   '15',
                   '321',
                   '2C30F5FD-02B4-441A-8A32-87ABF753484E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHN MINI JUNIOR PUPPY 2KGS RC',
                   '2',
                   '322',
                   '3B28AB3F-AD26-461D-849F-946A18B207A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHN MINI JUNIOR PUPPY 8KGS RC',
                   '0',
                   '323',
                   '53960E54-7ADF-4F3B-87CF-B949C6CDDF91-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHN MINI PUPPY 85G RC',
                   '43',
                   '324',
                   '3C96788F-14D4-44DD-8972-12D0C8CCC1D0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHN MINI STARTER M&B 1KG RC',
                   '3',
                   '325',
                   '3AC13BCA-D355-4B9A-8595-C7E34CA7552D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHN MINI STARTER M&B 4KGS',
                   '0',
                   '326',
                   '69823A81-24AB-4F83-94A5-90ABAF6CB689-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG ADULT 1KL',
                   '5',
                   '327',
                   '474F9E0A-E683-48C2-BA66-F832793E68E9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG ADULT 9KGS',
                   '306',
                   '328',
                   '524C614A-D1F0-43D2-A6BB-935E349EF5F6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PATE WITH BEEF TRIPE 400G.',
                   '104',
                   '329',
                   '8F206F9B-73C6-4AF8-A6A6-4898C6F65AB5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PATE WITH JUNIOR 400G (FOR PUPPY) (24CANS/CASE)',
                   '117',
                   '330',
                   'A814A814-B07F-432E-8497-5888F7592BB6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PATE WITH LAMB & TURKEY 400G.',
                   '81',
                   '331',
                   'D8C64AD5-69D7-4D14-9B17-F61F25CB62DD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PATE WITH LAMB TRIPE 400G',
                   '98',
                   '332',
                   '200983CF-5469-45F4-98A5-5C4880B8CB24-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PATE WITH MEAT & VEGETABLE 400G.',
                   '139',
                   '333',
                   '3CB33842-27B1-4026-B1D0-F3D2274E9540-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PUPPY 9KGS',
                   '335',
                   '334',
                   'DBD06BDD-F355-4DA0-8B7B-761693F14FFE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPECIAL DOG PUPPY REPACKED 1KG',
                   '4',
                   '335',
                   '59CF32BD-482F-49B2-96E4-3963399B41E3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VALUE MEAL 1KG ADULT',
                   '9',
                   '336',
                   'F0415A63-F601-477E-8EEE-868F5A460911-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VALUE MEAL ADULT 20KG.',
                   '12',
                   '337',
                   'CB2B6012-0798-483D-876D-1D66F03C4A81-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VALUE MEAL PUPPY 20KGS',
                   '10',
                   '338',
                   '11404F2E-6263-4C44-886D-59C594964C56-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD HEPATIC CANINE 1.5KGS RC',
                   '0',
                   '339',
                   '4EA9C563-8AA1-461B-B8E5-8D895275A4AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD RECOVERY FEL/CAN 195G RC',
                   '36',
                   '340',
                   'FD03E6B1-A9B3-4C9D-9EB0-DCAEFF44A09E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD RENAL CANINE 2KG RC',
                   '1',
                   '341',
                   '9FF9A257-7F8A-4D35-BF9C-85DCE02B745D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD URINARY CANINE 2KG RC',
                   '3',
                   '342',
                   '1761D4E9-4E26-4CA2-9BEA-9952DCDFD78F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD URINARY FELINE S/O 1.5KGS RC',
                   '3',
                   '343',
                   '352999FA-859A-406A-AAA2-6CA87E46CF08-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VD URINARY S/O CANINE SD 1.5KGS RC',
                   '0',
                   '344',
                   'E5A8EB41-FE67-43B5-B044-1F91EF5E441B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION CANINE DERMOTOSIS 2KGS',
                   '4',
                   '345',
                   '8144AD59-78DB-4D22-9712-0CE4CFA3A5F0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION CANINE HEPATIC 2KGS',
                   '2',
                   '346',
                   '815020BD-580A-4AF5-B3E5-94A11BB82F1A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION CANINE RENAL 12KGS',
                   '2',
                   '347',
                   '3284300D-6A99-43F1-A411-49B8EB17AE41-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION CANINE WET RECOVERY 150GMS',
                   '2',
                   '348',
                   '8BE49B63-357E-40D0-90A4-D0A7BDBF191F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET SOLUTION GASTROINTESTINAL CANINE DRY 2KGS',
                   '1',
                   '349',
                   '6654C40D-9289-4E16-9F5B-4B04A9FD7071-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY ADULT 15KG.',
                   '8',
                   '350',
                   'B85A8BCC-8AE5-45A3-AA4C-4AB37BEECC8E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY ADULT 1KG',
                   '7',
                   '351',
                   '5FEFBBAC-C31C-4968-82AE-0A7301B55F13-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY HI-ENERGY 15KG.',
                   '12',
                   '352',
                   '9E25B773-63D6-44B0-960F-1E7CAE48AD86-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITALITY HI-ENERGY 1KG',
                   '12',
                   '353',
                   '4A7B7A40-3FB6-44C6-9B13-7DF0D4C2CA2F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BEEF PRO ADULT JEYCERY',
                   '0',
                   '354',
                   '331D43CA-C761-41FC-9C47-DB6103367AD2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BEEF PRO PUPPY JEYCERY',
                   '0',
                   '355',
                   '3C6DD1CA-042C-48F0-A55B-5DBAAEB90476-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BRIT ADULT SALMON & POTATO',
                   '0',
                   '356',
                   'A05909B4-DCC0-459F-8DED-B420BFC5E051-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BRITCARE PUPPY 1KL',
                   '0',
                   '357',
                   'A797A30F-AA1E-40AB-ADD2-675267ABF9F4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'INFINITY HIPRO 20KGS BROKEN BAG FOR KENNEL ONLY',
                   '0',
                   '358',
                   '5A421AD0-AEFE-442C-9E91-42EA3DB3C169-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'INFINITY MAINTENANCE',
                   '0',
                   '359',
                   'B8544E4A-1BB7-44B3-8342-8BA771A81B56-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'INFINTIY MAINTENANCE 1KG',
                   '0',
                   '360',
                   '332D7954-E8A3-4B53-B8FB-D0857048E878-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE HYPOALLERGENIC SUPER PREMIUM TUNA AND SALMON 12KGS',
                   '0',
                   '361',
                   '9674E19D-6709-4FDA-BD86-E5FD418F6D25-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE MONOPROTEIN PATE DUCK',
                   '0',
                   '362',
                   '708ABBFA-A3AA-4ABD-84B6-2984D8D7C556-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE MONOPROTEIN PATE LAMB',
                   '0',
                   '363',
                   '69B761A0-8F99-4375-A89E-A6288A216FB5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE MONOPROTEIN PATE PORK',
                   '0',
                   '364',
                   '4CB67781-D9A6-49AC-A678-11923334EB40-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE MONOPROTEIN PATE TUNA',
                   '0',
                   '365',
                   '81E6B4A2-CA82-4E8E-9BF7-F3382E9D7038-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE MONOPROTEIN PATE TURKEY',
                   '0',
                   '366',
                   '0F3E101B-EE46-460F-A4F0-8749CC4B0A94-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE NATURAL BROKEN PUPPY MEDIUM',
                   '0',
                   '367',
                   'C410CB29-DCF1-476E-9CF5-19C3567F36D6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE NATURAL SUPER PREMIUM MAXI PUPPY JUNIOR 12KGS',
                   '0',
                   '368',
                   'DFEB7283-16CF-4C99-A333-9B768DA350EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE NATURAL SUPER PREMIUM MEDIUM (PUPPY/JUNIOR) 12KGS',
                   '0',
                   '369',
                   '4FBD41C5-9948-4D16-8121-7229E4CC2347-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE SALMON AND RICE 2.5KGS',
                   '0',
                   '370',
                   'BBC93A7A-D406-4788-8A67-69FE58222F2D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE SALMON AND RICE SUPER PREMIUM 12KGS',
                   '0',
                   '371',
                   '5C971CB1-EF0D-4FA1-B1BF-023B9FF127DF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE SUPER PREMIUM ACTIVE 12KGS (ALL BREED DOG)',
                   '0',
                   '372',
                   '3E324458-489E-42A1-A8E8-B7E48E125005-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MONGE SUPER PREMIUM MAXI JUNIOR 3KGS',
                   '0',
                   '373',
                   '6EB1EC25-888B-4B48-931F-60FBDFE4202B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRICHUNKS ADULT BEEF + CHIX 1.36KGS',
                   '0',
                   '374',
                   '1165E7EA-18A6-4EA1-A7C7-70B9BBB53669-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRICHUNKS ADULT LAMB 1.36KGS',
                   '0',
                   '375',
                   '3EDF764E-C8D9-4A07-9564-5D640323707B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRICHUNKS ADULT MAINTENANCE 1.36KGS',
                   '0',
                   '376',
                   'CBECEF6A-98C5-4CDB-88FE-3C99E84CE0E5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRICHUNKS ADULT SALMON 1.36KGS',
                   '0',
                   '377',
                   'FFA7265C-47FD-4C77-AF4D-7D1E6191CFA5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRICHUNKS PUPPY PROTEIN + LAMB 1.36KGS',
                   '0',
                   '378',
                   '814DF53F-7E6A-4DDE-A566-BAF7E698466C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RX LIQUID RECOVERY FOR DOG SINGEN 250G',
                   '0',
                   '379',
                   '070631BF-4629-4A68-BE28-8B66270DB81F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPECIAL DOG ADULT 1.5KGS',
                   '0',
                   '380',
                   '54F75BC2-638B-43E8-B23B-F664B84D0E25-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPECIAL DOG GRAIN FREE DUCK AND POTATO',
                   '0',
                   '381',
                   '0C4D8AF5-C0E0-4C59-B908-38EE92F77F31-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPECIAL DOG PUPPY 1.5KGS',
                   '0',
                   '382',
                   '5375D607-6DCA-46E0-90F3-FE890F6D0771-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VALUE MEAL 1KG PUPPY',
                   '0',
                   '383',
                   'A616B890-5DB4-46BE-8F10-D49902E83790-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION CANINE DERMATOSIS 12KGS',
                   '0',
                   '384',
                   '6DC6A717-BC24-4BF4-9865-6B3BBEEC12C5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION CANINE RENAL 2KGS',
                   '0',
                   '385',
                   'F73D26C3-7202-4940-BB00-E143B642FCFE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION CANINE RENAL AND OXALATE 2KGS',
                   '0',
                   '386',
                   'F15BCAF7-8727-4F7E-96B0-72E1C6C324F2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION CANINE WET RENAL AND OXALATE',
                   '0',
                   '387',
                   '2BE11FC7-19B2-4C6D-86A2-B39F2CBAE765-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET SOLUTION RENAL 1KG',
                   '0',
                   '388',
                   '2198AD6E-2073-43BF-9C4E-527451ACDE8C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AEOLIS KISS MC-230 BLADE',
                   '17',
                   '389',
                   '4318D5A0-9C77-4E9F-8CAC-249F2685BD10-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AEOLUS DIGITAL CLIPPER KIT MC-750',
                   '0',
                   '390',
                   '58FD5C83-D1F9-4C20-AF62-9B5022E4C4EE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AEOLUS ELECTRIC NAIL GRINDER KIT',
                   '1',
                   '391',
                   'DDD715F9-1008-411D-9CB6-65024F34D1CE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AEOLUS MC-750 BLADE BIG',
                   '0',
                   '392',
                   '73E9B78F-7A42-4177-AD5A-2924CA0D0109-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ALL SYSTEM SHAMPOO BRIGHTENING',
                   '0',
                   '393',
                   '499C1B0D-E25D-4A0B-BB5E-5A7F16648EF9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ALL SYSTEM SHAMPOO PURE WHITE',
                   '0',
                   '394',
                   'ADE6AA49-B3F0-4C70-B89F-C061981C7BB7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ANDIS CLIPPER - AGCB SUPER2 (BURGUNDY/MAROON) #25000',
                   '1',
                   '395',
                   '87C7D455-15B0-49DC-9596-D6D14E502BFB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ASSEMBLED STAINLESS TUB BTS133 46LX25WX55H',
                   '0',
                   '396',
                   '81D8C693-E7B0-4CA3-B209-5002B5198094-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JOJOBA ORIGINAL SCENT 4L/1G',
                   '0',
                   '397',
                   'C8C41FDF-7D1F-4B85-AC33-5EED20E234F8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 KISS AEOLUS DIGITAL CLIPPER KIT SMALL',
                   '2',
                   '398',
                   'D59F9679-6EA5-4155-AD03-F224A9F5C449-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #10 [KALBO]',
                   '8',
                   '399',
                   '3E4F54A8-9A11-4E91-B5A3-E01254288DCE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #30 [SKIN HEAD]',
                   '4',
                   '400',
                   'A624ECE7-0E6B-4E9B-821D-8410FF5AFEF4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #3F [PUPPY CUT]',
                   '5',
                   '401',
                   '0D4D1092-FBF6-48ED-B02C-7AF48CA1A617-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #40 [SKIN HEAD (SURGICAL)]',
                   '4',
                   '402',
                   'B8075169-00D8-4C6C-AA05-19677DD41797-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #4F [SUMMER CUT]',
                   '4',
                   '403',
                   'BEB1F5AC-6791-4FE9-BB60-4C270276AC1D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #5F',
                   '4',
                   '404',
                   '41E6183B-A23E-4CF0-8110-7134B0A9B6D0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE BLADE #7F [SEMI KALBO]',
                   '3',
                   '405',
                   '59D963AE-690D-43EB-9622-058D8E7938DC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE EAR POWDER 24G',
                   '0',
                   '406',
                   '4D9120EA-C4AA-4643-AB84-C5B8FDFB70A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE KELCO STYPTIC POWDER',
                   '11',
                   '407',
                   '76D99DA6-B7FE-4DC1-80AC-D45909367514-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE PROFESSIONAL PIN BRUSH OVAL 8.5',
                   '0',
                   '408',
                   '0C5AC0B3-45DC-4E67-B6BE-3603FCE13764-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LAUBE-N-COOL SPRAY',
                   '15',
                   '409',
                   '41AE136F-1962-4DBF-BCE7-AD2248687BF3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONSTER CARBON BRUSH REPAIR',
                   '1',
                   '410',
                   '4E1ECA3C-1458-4373-BF06-AD9FE00B740E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONSTER DRYER',
                   '4',
                   '411',
                   '279E7817-802E-401C-817B-666F932477B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONSTER DRYER EXTRA MOTOR',
                   '3',
                   '412',
                   'B6C979B5-EBD1-41CF-B7C7-FF8E1D39B97A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEASH WITH LOOP FOR GROOMERS ARM',
                   '5',
                   '413',
                   '3E38C562-9632-4977-93EC-AA2E3E6CA225-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET TOWEL CHAMOIS',
                   '8',
                   '414',
                   'C8BC6B9C-43F9-445A-89EF-3A5A01BF416F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET TOWEL MICROFIBER',
                   '2',
                   '415',
                   '0C3BAAB6-216C-444F-B5F4-36067E0FAD4E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL PET EAR CLEANER 4000ML',
                   '1',
                   '416',
                   'F37BC3B7-1D8C-4CFF-8462-3AFB9DB98F0C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZAP EAR CLEANER GALLON',
                   '1',
                   '417',
                   '206E87E2-46FA-42B6-84FB-9BB3640DFF12-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ALL NATURAL MADRE DE CACAO (GREEN)',
                   '0',
                   '418',
                   'D85F9DCF-B38F-454C-BB37-47112230CC5E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DR. CLAUDERS EAR CARE 50ML PET IDEAS',
                   '0',
                   '419',
                   'FF070AA3-D14E-44B3-BA1E-EDC19D55D805-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GROOMER ARM WITH LEASH',
                   '0',
                   '420',
                   'CF2673FC-0B8B-48BE-AFA7-8D88ACE3D9AA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GROOMER TOP SELECTION STRAIGHT SHEAR 5.5IN',
                   '0',
                   '421',
                   '6CC420B1-CA34-47C1-AA14-1F7449F300F6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GROOMERS CHAIR WITH BACKREST GC-001',
                   '0',
                   '422',
                   'E5E517ED-8D4B-49D3-9AE7-FCE0189CCC37-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS GROOMING SHAMPOO HAPPINESS',
                   '0',
                   '423',
                   'B1503B23-42B1-4EF5-98B8-D7902CE089FD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS GROOMING SHAMPOO LULLABY',
                   '0',
                   '424',
                   '2F08FFF2-2622-46DF-B5E7-27E668CF991D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS STAINLESS COMB 20-47',
                   '1',
                   '425',
                   '7604FD46-E64C-4F68-A915-DA3CB3263D64-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS STAINLESS COMB 22-43',
                   '1',
                   '426',
                   'C586BE98-42E4-4D2E-821F-CD35E6CB6E3C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS STAINLESS COMB 35-17 LARGE',
                   '1',
                   '427',
                   'C2962044-C9DB-4574-A9BE-EBD25538E8CD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS STAINLESS COMB 42 GREYHOUND STYLE',
                   '2',
                   '428',
                   'C1D031EE-AC92-49B4-8598-2CABC713934C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GTS STAINLESS COMB 60',
                   '1',
                   '429',
                   'A23F5EDA-E494-402E-B837-0A64F06BE884-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIKATO 4 STAR 7.0 STRAIGHT SHEAR',
                   '0',
                   '430',
                   'D0C8DB65-32D9-4A70-9A66-D45BBFF3D90F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIKATO PRO 3 STAR 7.5 THINNER SHEAR',
                   '0',
                   '431',
                   '2A98662E-9F58-49E9-AC11-093ECF5BED3A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Laube Professional Pin Brush Oval 9',
                   '0',
                   '432',
                   'F75EA1B2-96EC-48D1-8800-EE7D2EE9C621-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LAUBE STANDARD GROOMING TABLE MEDIUM',
                   '0',
                   '433',
                   '83CA6EC9-F575-46B1-9451-97080ECAEDEE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LAUBE STYLISH TRIMMER',
                   '0',
                   '434',
                   '38876EBD-C77E-4502-9B1C-E0B3E72CCE09-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Pampered Pooch Powder Fabulous',
                   '0',
                   '435',
                   'A0089FCF-2A9B-481F-ADE6-84AE1BAB5820-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAMPERED POOCH POWDER FRESH',
                   '0',
                   '436',
                   '6B48831B-FECB-42ED-BBC4-1EA4317565A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAPI AMITRAZ SOAP',
                   '0',
                   '437',
                   '2F33AF25-4C6C-4E34-BBB3-94C187CB533D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAPI MADRE DE CACAO SOAP',
                   '0',
                   '438',
                   '47A02EA3-D520-4255-9769-6F2BF93F288E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE SOAP HEAVEN SCENT CONDITIONER',
                   '0',
                   '439',
                   '95AD1F2B-5150-45A7-B72E-FA86864FF096-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE SOAP SWEET EMBRACE CONDITIONER',
                   '0',
                   '440',
                   'BD127C8A-E2BE-4CF0-AD8D-870FDDC7B5AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VETNODERM SOAP MANZ',
                   '0',
                   '441',
                   '549156DD-7468-47B2-8337-4964BB0E5040-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WALL BRACKET BLOWER',
                   '0',
                   '442',
                   '3E29F70E-8970-49E8-99BB-695B05FE113D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WALL BRACKET FOR AEOLUS DRYER TD-9012',
                   '0',
                   '443',
                   'CF7566EC-8590-4BAF-8BA1-D20BEC70CDBF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Feline Fresh (Carbon Activated) 5L',
                   '0',
                   '444',
                   '69CCA020-546D-4C42-99E7-A6A69560E7C2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FELINE FRESH 10KG (APPLE) 10L',
                   '0',
                   '445',
                   'A54AC4F3-36FA-4E2B-A9DD-B9B82586E66E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FELINE FRESH 10KG (BABY POWDER) 10L',
                   '0',
                   '446',
                   '07FE2192-4C97-4298-92F0-A3D0EACB0246-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MEOWTECH CAT LITTER (GRAPE) 12L',
                   '0',
                   '447',
                   '31D27374-1DAA-41E7-8339-4C4935FA5795-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MEOWTECH LAVANDER',
                   '0',
                   '448',
                   'A5EA8071-1427-451E-A2AA-A3A27C97C370-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MEOWTECH LEMON',
                   '0',
                   '449',
                   '7179FA93-F52B-45A4-B8E6-C0292819E83C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NATURAL TOFU CAT LITTER ASSRTD',
                   '0',
                   '450',
                   'A1A9EADC-2A21-436E-914A-BE46E9B6B6B8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ALLOPURINOL 100MG TAB (1/2018)',
                   '0',
                   '451',
                   '9862EAA0-33AB-47AA-AA4C-7138BAAED1CA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ALLUSPRAY VAXILIFE',
                   '13',
                   '452',
                   'F88C823D-CA53-40E1-AA19-1C5EB99464E2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AMBROXOL 30MG TABLET',
                   '256',
                   '453',
                   'A618A335-3868-49F6-9290-F817615D13E0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ANTI-COPROPHAGIC PET TABLET',
                   '1020',
                   '454',
                   '7E5C4A1B-055E-4254-BFC3-980212A1C896-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ANTI-COPROPHAGIC STOP STOOL EATING FORMULA',
                   '18',
                   '455',
                   '0C904818-7B44-41CC-B63B-E3A39192D924-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 APOQUEL 16MG/TAB',
                   '50',
                   '456',
                   '48A2F420-98F7-454C-AB53-2A9737A7E489-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 APOQUEL 5.4MG/TAB',
                   '79',
                   '457',
                   '66B1F3E4-A618-4C45-804C-13AB3E80B69A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 AURIZON 10ML',
                   '9',
                   '458',
                   '64551B0B-1E12-494B-941E-891692D329A1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BANJO DERMEX',
                   '53',
                   '459',
                   'FE4355DA-DCD4-4FD9-9C05-EBBD479498D9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BECLOGEN CREAM 20G',
                   '2',
                   '460',
                   '5516A04E-2253-483B-AC7E-D945CA2DF771-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BEESION WOUND SPRAY',
                   '5',
                   '461',
                   '290F8C12-21E2-4F61-BAA2-E13FA09831CB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOBETANOX LA 15% (VETRIMOX)',
                   '0',
                   '462',
                   '427D30BE-DF22-4FFB-B77C-427EA8E1C9C2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLINE EAR CARE 50ML',
                   '6',
                   '463',
                   'A28D5D8A-06D9-48F0-A20C-8BB4C9D0F38B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLINE EYE CARE 50ML',
                   '4',
                   '464',
                   '5EBF082F-00CA-4C75-90D2-B516C86C04FC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CANIBROM (BROMHEXINE) 60ML',
                   '27',
                   '465',
                   '578C9414-286D-4C1F-AA48-A345A7DC485D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CANIDOX SYRUP 120ML DOXYCYCLINE HYCLATE',
                   '32',
                   '466',
                   '401FDB92-043B-4907-B8FB-5FEC81A026B4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CEFALEXIN 250MG CAP 115P',
                   '1314',
                   '467',
                   'E7E007A6-0DC3-4E5A-83AA-94DBA1C7B570-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CEFALEXIN 250MG SUSPENSION',
                   '84',
                   '468',
                   '9B931D44-4DE0-462C-8825-E1B83C72872C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CEFALEXIN 500MG CAPSULE',
                   '5577',
                   '469',
                   '3C428901-A8BB-43DB-8098-CE82B16852E0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CEFUROXIME (ZEFSUR-500) 500MG TABLET',
                   '100',
                   '470',
                   'A60C9375-DE48-4EE2-97DC-4BD868E6A547-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CERENIA INJ TWIN OX',
                   '3',
                   '471',
                   'FE3F1868-775E-469E-A688-209EB6674166-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CO AMOXICLAV (CO-AMOXISAPH) 250MG SUSPENSION',
                   '121',
                   '472',
                   'DAD0895C-8727-42CD-AB3F-E5E5A3D69235-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CO AMOXICLAV 625MG TABLET (RANICLAV) 14S',
                   '101',
                   '473',
                   'A2AD0DDE-3D95-469B-A183-42C8362605B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CORTAMETHASONE INJ DEXA INJ VETOQUINOL',
                   '2',
                   '474',
                   '4C464C84-B268-47CA-B8E4-BE642344B292-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTRIMOXAZOLE 480MG TAB (ZOLBACH)',
                   '353',
                   '475',
                   'B036BF27-14FE-4EA7-A8B3-E284CF0309AE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTRIMOXAZOLE 960MG TABLET (KATHREX)',
                   '190',
                   '476',
                   '6E101A19-A8B8-4638-BAF1-836370B014FB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COXELAX CARPROFEN 50MG',
                   '5',
                   '477',
                   'D8946CEB-9C07-45FC-A89A-A95E16535C52-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DEXAMETHASONE (DEXAT) 500MCG TABLET (1/2018)',
                   '1390',
                   '478',
                   '99449199-BEAA-4338-A18A-ADB76C715FBB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DILANTIN 125MG SUSP',
                   '0',
                   '479',
                   'CA014CDB-94F3-4420-8861-2FA6DE471B55-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DOMOPROL (RANITIDINE + DOMPERIDONE) 60ML',
                   '10',
                   '480',
                   'E849097F-F1BB-4006-ABFA-9BF18DE3C84F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DOXYCYCLINE HCL DOXY 10% INJ',
                   '2',
                   '481',
                   '9D36F8C3-D907-43F4-8735-84F786007A06-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DOXYVET CAPLET EDGEL',
                   '0',
                   '482',
                   'AB3E5D47-50CC-4F78-8BD2-A6320195464F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 EMERFLOX (EN ROFLOXACIN 20%) ITO NA',
                   '13',
                   '483',
                   '54278B98-48AD-4AA2-A96E-0E8097CEE7A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FTD BOTTLE',
                   '1',
                   '484',
                   'CEAA495E-FBD5-4C93-95CC-5D8870B6F5D1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT AMITRAZ+KETO SHAMPOO',
                   '19',
                   '485',
                   '2DAD6729-CB2A-40ED-B9CC-9ED037F2FD5E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT AMITRAZ+KETO SOAP 150G',
                   '13',
                   '486',
                   '48B29207-E38F-40C2-80FD-E3251C387C36-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT BIOSULFUR SOAP+MDC 150GMS',
                   '33',
                   '487',
                   '827F014C-8618-451E-AC34-6E4722A3D2B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT BIOSULFUR+MDC SHAMPOO 500ML',
                   '8',
                   '488',
                   '4EB7A68B-B5A9-4CCF-BFD2-835172DA4D30-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT FIPRONIL SHAMPOO 500ML',
                   '6',
                   '489',
                   '6200B6D6-6B53-4172-A24C-61C09D8FD812-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FURFECT FIPRONIL SOAP 150GMS',
                   '15',
                   '490',
                   'A7E5B141-4ACD-471F-B435-FBAA4863CD65-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUROSEMIDE 40MG (FUSEDEX) 100S (1/2018)',
                   '1206',
                   '491',
                   'A480247C-6B4C-49FF-81CE-7C04C3B5EA87-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GENTIN DROPS BIO GENTA 10ML MEDIVET',
                   '114',
                   '492',
                   'D6A8808A-A7C5-4731-8831-F586FF24C806-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HEMOVET PLUS SYRUP 100ML',
                   '8',
                   '493',
                   'B03AC836-7FD2-4A63-B7B0-8854FF221976-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HIMALAYA LIV52 TABLET 60PCS',
                   '7',
                   '494',
                   '5EC5E4D6-E382-4CBD-9A43-F657035F859F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IVOMEC INJECTIBLES 100ML...',
                   '1',
                   '495',
                   '13BB944E-813F-4D9F-84DB-F08260A51638-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 K9BIOTIX OTIC GEL 3ML',
                   '11',
                   '496',
                   'BE46D7B0-40EB-43F7-9011-91B95F6BC84E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 KBOOST VITAMIN K3 10MG',
                   '11',
                   '497',
                   'FBAA588A-B3BD-4807-9C83-54053CAA592D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 KBOOST VITAMIN K3 10MG PER PCS.',
                   '330',
                   '498',
                   '1B7573C2-6CD3-4FE3-80A7-9174C654F2F4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC DOX SUSPENSION 120ML',
                   '73',
                   '499',
                   '74391920-E48C-45CF-ACB9-C044842E82D2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC DOX SUSPENSION 60ML',
                   '24',
                   '500',
                   '42FB8F2D-3B9D-4B7A-8E2E-A05D0FBA9C87-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC DOX TABLET (ANTIBACTERIAL) PER TAB',
                   '1227',
                   '501',
                   '031B62BC-91D2-40FB-A5A1-91D3B8AF305B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LIGHTSENSE EYE DROPS',
                   '1',
                   '502',
                   '9CB4FC6A-3E55-4194-BA69-5C32FE52EE21-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MARBOCYL 2% BOTTLE',
                   '1',
                   '503',
                   '1DF2A30C-E4AF-4108-893F-1593B2AD8CB4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MAXWELL EARMITE AND TICK EARDROP',
                   '10',
                   '504',
                   '9874DA7C-BA96-4952-9564-EA298FE38D40-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MELOXICAM (INFLACAM) 1.5MG/ML 32ML',
                   '6',
                   '505',
                   'A3DF8A91-3D6E-4BF2-9E70-DD6AEEEA12C3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METAPRIME BOT 100ML',
                   '20',
                   '506',
                   'F670BF11-857B-41C6-BC6C-7621F6BFFB8F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METAPYRONE BOTLLE 100ML',
                   '2',
                   '507',
                   '348C9967-D4B2-419B-B4EA-628A255B2793-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METHYLERGOMETRINE TAB ERGON 115P',
                   '728',
                   '508',
                   '8FAFAD49-9F20-4A6C-AB8D-1FF5DC92B766-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METOCLOPRAMIDE (MOTILLEX) 5MG/ML (1/2018)',
                   '8',
                   '509',
                   '991D937B-A702-4E4D-BF06-D626E70C25B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METRONIDAZOLE SUSP (METROZOLE) 125MG/5ML (1/2018)',
                   '24',
                   '510',
                   '46838667-1EF7-4D57-AD09-BD0EE2D3B633-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METRONIDAZOLE TABLET (METROZOLE) 500MG (1/2018)',
                   '470',
                   '511',
                   'EBC656B7-AB42-4BB8-9D6A-E83AA93EC80D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MOXIBEN PLUS EYE DROPS',
                   '25',
                   '512',
                   '1F24001B-F09B-4AF2-8EED-F29FD2ADD7CA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MUCOTAN (LINCOMYCIN+SPECTINOMYCIN+BROMHEXINE+PREDNISONE) 60ML',
                   '8',
                   '513',
                   '2E768B27-20B8-4727-AB94-476AD8330DE0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MYCOCIDE SHAMPOO',
                   '32',
                   '514',
                   'A761B1A4-D8E7-4AAD-99E2-428BBEBBA74D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 OPTICARE(GENTAMICIN) 7ML',
                   '14',
                   '515',
                   '298AD7B5-2527-431E-805D-511C45857145-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ORIDERMYL 10MGS',
                   '11',
                   '516',
                   '0B385D29-EE23-4930-B681-05B1C8A703EB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 OTIDERM EAR DROPS 15ML',
                   '10',
                   '517',
                   '7A93EFCD-D90D-4146-BFD7-B6C051B01DBC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 OTIFORTE EAR DROPS',
                   '42',
                   '518',
                   'FE1990FD-74F8-4D9A-96C8-F2CFD332FAC7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI AMITRAZ SHAMPOO 500ML',
                   '14',
                   '519',
                   '188A113A-B8A9-45DD-B3CD-9B3117F48F98-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI ENROFLOXACIN TAB 50MG',
                   '0',
                   '520',
                   '49AC1458-AFE6-42B0-A5CB-811728BD9708-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PETCLAV 50MG',
                   '106',
                   '521',
                   '9B06EF35-ED0E-43C2-ABFF-ABAAB6DAC3B4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 POMISOL EAR DROPS 15ML',
                   '0',
                   '522',
                   'B70E779A-AC6C-4D5D-992D-D5F93C206188-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREDNISOLONE ACETATE EYE DROPS',
                   '19',
                   '523',
                   '33AD8715-2927-402D-9D7F-5F1AE54D69D2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREDNISONE 10MG PER ML / BOTTLE',
                   '85',
                   '524',
                   'E3004A32-D2EE-4B91-9B54-7D10A4FF3DC7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREDNISONE CORT 10MG/ML SUSP',
                   '25',
                   '525',
                   '8B5FD409-DAE0-4DE0-97D0-08B31511E86B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREDNISONE VONWELT 10MG TABLET (1/2018)',
                   '802',
                   '526',
                   '79F05A28-088C-4279-BBD1-4EF86D0E5737-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREVICOX 57MG/60S',
                   '75',
                   '527',
                   '7A5539BC-B4D3-4CF8-B3DD-E695F9EEF71B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PULMOQUIN AMBROXOL SYRUP',
                   '36',
                   '528',
                   '5C4E4432-C36B-45A5-AE77-E56E46614357-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SCOURVET SUSPENSION 60ML',
                   '46',
                   '529',
                   'A7900D69-E1DD-4599-A05E-CE17AFFFD467-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SEPTOTRYL PER BOT',
                   '3',
                   '530',
                   'AF00A1F9-50D6-4719-B07F-01F37847F86D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SODAGO CORDVET SERUM 1ML',
                   '85',
                   '531',
                   'A94F42F2-6A30-47AB-B858-B16B01273AFB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TAKTIC 1L',
                   '0',
                   '532',
                   'C45F6941-75FD-42FD-B08A-AEB6E35CB090-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TAKTIC PER BOTTLE',
                   '70',
                   '533',
                   '96A58F7B-0887-4DAE-9A0D-FC112513C1A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 THROM BEAT 100ML',
                   '0',
                   '534',
                   '5C91D682-A3E2-4810-8682-D9FDA614472E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TIMOLOL MALEATE OCUPER EYE DROPS',
                   '0',
                   '535',
                   'E6B69330-4E83-4ACD-AC4C-C43483CDCA37-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOPICURE SPRAY',
                   '13',
                   '536',
                   'F3D00F57-2715-4C73-9888-88229BE97385-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOPI-DERM',
                   '0',
                   '537',
                   'BBDB634C-BB98-4418-9A86-5B85F8A0093F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOPOSAL INJ',
                   '0',
                   '538',
                   '9365611E-FF58-47BA-BD8D-251677F30DB4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRANEXAMIC ACID HAEMOREX 500MG CAP',
                   '144',
                   '539',
                   '2AA1234A-5FED-4CC7-9BFC-8D4A6ABC7677-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRANEXAMIC AMP',
                   '71',
                   '540',
                   '97B435A0-6A6E-42FD-A9FE-782367A22C60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO CANDONTAL 10PCS/BOX',
                   '126',
                   '541',
                   'A3BED889-AAFD-4C73-A918-F8520E8EBC59-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO CARNIKO ORAL PASTE',
                   '10',
                   '542',
                   'B590E1B4-68E7-4DEC-A774-D5683666DF17-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO CLAVPET 500 10PCS/BOX',
                   '11',
                   '543',
                   '5B5F5C72-10F8-40F7-AB35-AC774E795033-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO DOXYKO',
                   '19',
                   '544',
                   'CA721A70-3117-4DBF-95EB-098AA74152BA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO ENROKO',
                   '96',
                   '545',
                   '4932A9D9-D175-48B2-94B0-EF16DE373EC5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO JUMP BUDDY 50 6PCS/BOX',
                   '100',
                   '546',
                   '7DF63964-02D5-443B-BC8A-20FA92D65AF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO KICK RASH 100ML',
                   '10',
                   '547',
                   'CD51A56E-1FE4-42AD-B137-0BF001F0693E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO KICK TAPE CAT',
                   '100',
                   '548',
                   '7ED95F79-F267-438D-9972-C4EFE65ABDE3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO KICK TAPE DOG',
                   '18',
                   '549',
                   '12A1E51D-CE93-433A-A2BE-4F75B6E299FF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO MARBLOW 25',
                   '190',
                   '550',
                   '8FDEC2BF-7BAC-4857-A636-210B74150098-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO MARBLOW 50',
                   '180',
                   '551',
                   '9FFD6D26-BDA5-4351-8BF9-77F90AAC70AD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO OTIKOO EARDROPS',
                   '10',
                   '552',
                   '3E01EF50-49FA-4486-ADAB-3B2160A40384-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO PETMEDIN 1.25',
                   '150',
                   '553',
                   '39D74102-9E5D-4463-B7E5-C4F7DFBA4EA7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO PETMEDIN 10',
                   '115',
                   '554',
                   '12CB60A7-857D-4DD6-BEDD-513C0E0D5C23-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEKO PETMEDIN 2.5',
                   '150',
                   '555',
                   'BA844FCB-E1BE-43F2-8C4E-E40106942152-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VETNODERM CREAM PET WELLNESS',
                   '48',
                   '556',
                   '24355A84-E990-4E79-BE21-91A95CFB615A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VETNODERM SOAP',
                   '26',
                   '557',
                   '2FF031C5-72D3-47AC-9757-BA288312DC85-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VITAMIN ADE BOTTLE',
                   '2',
                   '558',
                   'B8372B12-CD3F-43A7-AE07-6E60F5D298D4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ALUMINUM AND MAGNESIUM HYDROXIDE 200MG/100MG/5ML MEDALEM',
                   '0',
                   '559',
                   '2B6DDF3F-B996-4C5B-A3FA-70E5EC1DAEE8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'B COMPLEX NEURONERV B1 B6 B12',
                   '0',
                   '560',
                   'D68A80FE-D378-4A5E-9804-401E2B4995AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BIOCURE 20TABS',
                   '0',
                   '561',
                   '333199CE-7318-4648-B0D5-E538A5DABFFE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BROMIVET BROMHEXINE SYRUP 60ML',
                   '0',
                   '562',
                   '67C2DBAE-ECB8-4194-A8C1-3ACD61FFCD2A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CALMIVET 50ML',
                   '0',
                   '563',
                   'B20DC483-73A9-407D-BB38-5CF9B281C51D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHLORAMPHENICOL 125MG SUSPENSION ZINETT',
                   '0',
                   '564',
                   'C4F5CB89-2F93-45E0-97B9-D2CEBB82F506-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CO AMOXICLAV (MIXCLAV) 250MG/5ML SUSPENSION  (1/2018)',
                   '0',
                   '565',
                   'E02EE814-AC9B-46AE-891B-605683C78682-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CO AMOXICLAV (RAPICLAV) 625MG TABLET 21PCS (1/2018)',
                   '0',
                   '566',
                   'FDF1FB32-4492-44A7-BEA8-9CD6161C1F74-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CO-AMOXICLAV (COMXICLA) SUHITAS 625MG TABLET 20S',
                   '0',
                   '567',
                   '31F332B0-4672-4F23-8731-1C5D8B7A8CC5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COTRIMOXAZOLE SUSPENSION 400MG (2018)',
                   '0',
                   '568',
                   '0726C94D-DED6-448C-9A6E-D089871FA70C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COUGH AND COLD REMEDY 60ML',
                   '0',
                   '569',
                   '02FB2AD1-EF2D-4819-B89A-AF03A07B568E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DERMGARD 10ML',
                   '0',
                   '570',
                   'EC37F603-D038-4649-B90A-E921B29EBC4D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOXXI 100 TABS',
                   '0',
                   '571',
                   '44E0107A-3C4A-4E25-A18E-588798AB00A6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOXYVET SYRUP EDGEL',
                   '0',
                   '572',
                   'AEAA82CA-857B-47F8-AA5F-4C3F7881063F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'EMERFLOX (ENROFLOXACIN 20% 60ML)',
                   '0',
                   '573',
                   'D684797E-0551-4527-94DD-10F74A1AFF20-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Farameter cap 210p',
                   '0',
                   '574',
                   'E2461B50-9D8A-4DC4-B9DF-02A8097181CC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Foralivit 130p',
                   '0',
                   '575',
                   'B954A814-F87D-419B-811F-F0069BBD9BF9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIMALAYA HIMPYRIN',
                   '0',
                   '576',
                   '2D44B485-064E-4835-8760-AEABC643F53C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIMALAYA NEFROTEC VET TAB 60PCS',
                   '0',
                   '577',
                   '7EE59FDA-DA01-495F-98B9-552605C76858-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIMALAYA SCAVON WOUND SPRAY BEESION',
                   '0',
                   '578',
                   'B84BA15C-01F6-475F-9918-36F55005EFF5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IMMUNO BEST SYRUP 100ML',
                   '0',
                   '579',
                   'D8196C57-B0DF-41BB-8DBE-C78A0C11FF4D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IMMUNOL 60 TABLET',
                   '0',
                   '580',
                   'D0D4F148-371B-4D2A-BC7B-14785254F82E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LC DOX TABLET (ANTIBACTERIAL) 100TABLET',
                   '0',
                   '581',
                   'DBE2590E-8FE3-4FDB-A828-D47190178418-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LC-SELEN 250ML AVLI',
                   '0',
                   '582',
                   '5A1C1D96-D8E0-4D69-B088-57D55DEAC6FC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LIV52 SYRUP 30ML',
                   '0',
                   '583',
                   'B7AD2261-1B9C-4BF2-8990-6B30D88644A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MELONEX ORAL (MELOXICAM) 10ML',
                   '0',
                   '584',
                   '8515B03B-B43A-478B-82FC-DDF29215B16C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MELOXICAM 15ML 1.5MG PER ML',
                   '0',
                   '585',
                   '10CCE36D-990F-4D9B-992C-09675B506717-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'METHIOVET',
                   '0',
                   '586',
                   '64A9F73A-A855-4C4B-AE75-710E81132D1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Methozine',
                   '0',
                   '587',
                   '22E3CBB9-8F23-4431-9298-374F95D8E9D5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'METRONIDAZOLE FLAVET 60ML',
                   '0',
                   '588',
                   '23589D59-B13C-48C3-B7A8-6D80B8247173-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'METRONIDAZOLE SUSPENSION 125MG',
                   '0',
                   '589',
                   'D3484B41-F116-442A-BD27-6813A9160EE3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MIDOX DOXYCYCLINE 100MG',
                   '0',
                   '590',
                   'BC453455-3A6A-42A1-A7B5-58F92B5502FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAXPET 10MG',
                   '0',
                   '591',
                   'EA5EB75E-81C6-4BC2-90DF-26BF6EE9E6BB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAXPET 30MG',
                   '0',
                   '592',
                   '9CA85C59-3ED5-4FA6-95C8-84AFFC36A8B5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAXPET SYRUP',
                   '0',
                   '593',
                   '497C9B92-AAD4-4456-AF46-CB4AC3D8DA72-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEPHROTEC',
                   '0',
                   '594',
                   '189C35B6-E586-441D-94B4-1BF61A755D0B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEUROMAX HI POTENCY B-COMPLEX VITAMINS',
                   '0',
                   '595',
                   'E46F16CF-E3A7-4A64-AFE3-DDAE32A719F7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NORFLOXACIN TAB 200MG',
                   '0',
                   '596',
                   'E5863691-3C60-4BEC-B141-D3A3CF70D92E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRIPET 200G',
                   '0',
                   '597',
                   '056ADC7C-41C0-419F-A439-FF9F437F9BA4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAPI DOXY',
                   '0',
                   '598',
                   '4BC89140-BA75-4690-8E7F-AE2B3AA235B0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAPI IOZIN WOUND SPRAY',
                   '0',
                   '599',
                   'C367E58D-9681-4CE8-A067-B88714E8669C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET FEVER GO 30ML',
                   '0',
                   '600',
                   '69562E34-F759-4D48-86F7-CCAADD29488B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PETCLAV 250MG',
                   '0',
                   '601',
                   '8B175D7B-5505-4596-A718-88ED4AE50561-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PETCLAV 500MG',
                   '0',
                   '602',
                   '2D826C7C-101A-4BFB-8E87-F734C672F367-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PETZYME ANTI TICK AND FLEA POWDER',
                   '0',
                   '603',
                   '377EAD58-00E6-48C9-8F95-25ED2A22FCBD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'REMEND CORNEAL REPAIR GEL 3ML EVR',
                   '0',
                   '604',
                   '7FD2549F-6A61-490F-BD26-2066665F6FB2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SCAVON VET CREAM',
                   '0',
                   '605',
                   'A34C55EF-6DC3-4C85-BF24-D94D46F5AAD4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SODAGO CORDVET GEL 10GTUBE',
                   '0',
                   '606',
                   '1A502798-FADD-4F17-A56B-9E9191055A96-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'THUNDERBIRD RED CELL',
                   '0',
                   '607',
                   'C25835DF-403E-4B23-82E8-E82B54D54F18-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TRANEXAMIC ACID (BETRANEX) 500MG/5ML AMP (1/2018)',
                   '0',
                   '608',
                   '07FDA007-5986-420A-89D5-2A51F0DD7697-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TRANEXAMIC ACID VASOTRAN-CARCHI',
                   '0',
                   '609',
                   'CE311EBA-1685-4275-9885-F037D040C7A2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TRANEXAMIS ACID 250MG CAPSULE (CLOTINEX)',
                   '0',
                   '610',
                   '3A441228-4607-43C6-8473-D561AE7EADC9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET REMEDY SOAP 200G',
                   '0',
                   '611',
                   '6A5C68BF-B57B-4FCD-A512-E4B7F5AD9253-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VETNODERM CREAM MANZ',
                   '0',
                   '612',
                   '79D43450-33EA-4218-B63E-7F7B5850EB15-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VETNODERM SOAP PET WELLNESS',
                   '0',
                   '613',
                   '815F4488-26C2-4168-8CA3-B67D3939D03D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'XICLOSPOR CYCLOSPORIN OPHTHALMIC OINTMENT 2MG/GM',
                   '0',
                   '614',
                   '5DD6BE16-2539-4F17-A1C8-04B1AA137B34-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'XYLAVET FORTE XYLAZINE 50ML',
                   '0',
                   '615',
                   'C6C26CE6-0D0F-470D-9E4F-9DD3DFA9B9DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BASKET',
                   '11',
                   '616',
                   '8AE301A4-0D8E-411E-9D87-B5D852DD307D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIO ION ALKALINE BATTERY AA',
                   '8',
                   '617',
                   '7E854141-F515-4D9B-B05F-D9B6E6535FD0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIO ION DEO SANITIZER AEROSOL REFILL SAKURA SCENT 250ML',
                   '4',
                   '618',
                   '18FEF4E5-F1EF-47D3-A174-7234B69AF470-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAUTERY PAD',
                   '1',
                   '619',
                   '6BD4A6E1-3866-4B01-930E-B53695944449-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHLORHEX',
                   '10',
                   '620',
                   '5427E84C-C7FF-4C84-9F43-089AC78A0DAE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CREMATION',
                   '99',
                   '621',
                   '54434849-B092-4F9F-AAE8-2FB4C93F3CF0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ELIMINATOR PLUS CARBOY',
                   '1',
                   '622',
                   '7ADFD621-750C-4151-B825-21C1CB8A859D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET DIGITAL SCALE - BATTERY',
                   '1',
                   '623',
                   'DD341E71-1A6B-4BD2-BBE9-E250B011BF06-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET RX VACCINATION CARD',
                   '1600',
                   '624',
                   '751B071E-76DF-4C93-A451-D62FA45B7CE9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PROTECT PLUS GOLD DISINFECTANT',
                   '9',
                   '625',
                   'F82B0AA2-A751-4A38-A0C9-3E8D3AEA10A7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RESTRAINT GLOVES',
                   '1',
                   '626',
                   '958E258B-2C1B-47B5-AEE5-8FEE600644FF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RX900 LCD DISPENSER DECS LED WHITE',
                   '4',
                   '627',
                   '4252AB77-7F8F-4C32-BE3B-6647E3970319-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 S1 CHLOR-T BROAD SPECTRUM DISENFECT',
                   '0',
                   '628',
                   '8E6BD8AA-AD08-48D1-AF1C-E054836894D5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SHIPPING FEE',
                   '19',
                   '629',
                   'DA88A75B-518F-4339-B5CB-F525BECB859E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SKIN STAPLER (SURGITECH)',
                   '10',
                   '630',
                   'CA30D4DF-B6FA-451A-8FCD-8D55D2198575-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VETSCOPE 214 MICROSCOPE',
                   '1',
                   '631',
                   'EBC590AA-E2A2-46B8-9463-CD8918109245-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BP MONITOR AUTOMATIC/DIGITAL (YUWELL)',
                   '0',
                   '632',
                   'A1F8358D-0D1D-4F24-B106-3925F5302F1A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CENTRIFUDE REPLACER',
                   '1',
                   '633',
                   'B429B15D-8BF9-477A-A1DF-676DF7F5083D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DENTAL TARTAR FORCEP',
                   '0',
                   '634',
                   'E8BE8310-0089-43FD-82CF-5E265F02A017-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DIGITAL THERMOMETER',
                   '0',
                   '635',
                   '4E4D1FA1-77C1-467B-9141-5DB2F3B8C6F9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HI - COP 1KG',
                   '0',
                   '636',
                   '1E10ACD2-17F0-43B0-8390-FE0DA5CDB920-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LEUKOPLAST TAPE',
                   '0',
                   '637',
                   'CA7F1124-4412-49AE-897D-6E7EC6F59A74-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICROPIPETTOR',
                   '1',
                   '638',
                   '4993A9B9-109B-448F-85AA-E0443C1B48EF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEEDLE HOLDER',
                   '0',
                   '639',
                   '4A6677CA-FFD4-41A7-BFD8-3E9CE72705E3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NEEDLE HOLDER 7INCH',
                   '0',
                   '640',
                   '99BBACF7-9A00-4EF1-8BED-49762AC85B85-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET DIGITAL SCALE WITH RUBBER MAT 2X3FT 50-200KGS',
                   '0',
                   '641',
                   'CEBC03D3-19EA-460E-B1AC-3C305F1027E6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PIPETTE',
                   '0',
                   '642',
                   '9CD507FA-88E6-4F1D-93CD-B3F76B492C8D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POWER SCISSORS',
                   '0',
                   '643',
                   'EACEEFDA-7368-48CB-A83E-4AAB0FFE0328-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'STAINLESS STEEL TRAY-8X12/OLTEN',
                   '0',
                   '644',
                   '75721A62-7955-42C7-A02E-87884DF3C0A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'STRAIGHT SHARP SCISSORS',
                   '0',
                   '645',
                   '9B056E8C-D3CD-404A-B55E-9BB52B65F9B4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SURGICAL SCISSORS, VETPROVE-FERGUSON STONE SCOOP/12.5',
                   '0',
                   '646',
                   '071FC416-5C72-4E6B-A72C-D250E9995793-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'THERMOMETER COVER 100PCS/BOX',
                   '10',
                   '647',
                   '4702D409-2BE0-4397-8F8F-2A32404AB2F3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VACCINATION RECORD',
                   '0',
                   '648',
                   '4ED2003F-7D69-4C13-9D99-46B77A0BFCE5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET GOWN',
                   '0',
                   '649',
                   'C0316BA2-159B-4415-95C4-A10D9DE71873-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2 PCS POOP BAG WITH CAPSULE DISPENSER',
                   '0',
                   '650',
                   'AB238102-7E45-46CB-ABB5-A7E5B68541EA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ACTIVIM',
                   '0',
                   '651',
                   'CDB8DF60-3C72-430A-B3D4-6ED4F3214796-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI LOOP TYPE',
                   '0',
                   '652',
                   'F8DEEB55-9B5C-4B4F-9773-9810B48BA75F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI SHOW LEASH KNOTTED 4MM',
                   '17',
                   '653',
                   'AEC5984F-C104-4EF8-91DB-C2B2385C6F5D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI SLIP LEASH 10MM',
                   '16',
                   '654',
                   'AE705D44-29A0-4B82-80E1-49BD673908A6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI SLIP LEASH 6MM',
                   '18',
                   '655',
                   '35393B4D-D093-4283-B2EC-C1240F25F5E5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI SLIP LEASH 8MM',
                   '0',
                   '656',
                   'E3A70DAC-CA4C-4306-A2BF-AFE3CEC85CA4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARMANI STACKLEASH',
                   '0',
                   '657',
                   '020945B5-86F5-489E-B5C8-071500F6301B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARQUI FRESH TOOTHBRUSH AND TOOTHPASTE SET',
                   '0',
                   '658',
                   'C4CB1408-795A-40C8-88E5-45E3B1578B40-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARQUIFRESH SINGLE TOOTHPASTE MINT',
                   '5',
                   '659',
                   '65F32CC9-3464-4C09-8269-87FD92DF5CAA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARQUIFRESH SINGLE TOOTHPASTE ORANGE',
                   '4',
                   '660',
                   '7CD490D8-C7D8-4942-8DAD-83ABD3576A1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ARQUIFRESH SINGLE TOOTHPASTE STRAWBERRY',
                   '4',
                   '661',
                   '3A68FEAA-8369-47BC-9B31-9182EE79DAFF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BEARING POWDER DRY SHAMPOO 150GMS.',
                   '8',
                   '662',
                   'BF629DE2-1944-4C87-8BA0-2E44738336AD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BEARING SOAP 100G',
                   '6',
                   '663',
                   '9AD8C254-A7AB-4636-8BBA-FE11C7489533-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLIE PUPPY TRAINER',
                   '6',
                   '664',
                   'FF55604B-02BF-40CA-BA35-FF9E91B343E9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLINE DENTAL CARE SET BEEF (PASTE AND BRUSH)',
                   '3',
                   '665',
                   '09D87651-5588-4BD1-A763-CC1DF091D7C7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLINE DENTAL CARE SET CHICKEN (PASTE AND BRUSH)',
                   '0',
                   '666',
                   '3C1FD057-EE05-423C-86BC-B5EF1B490CDE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOLINE TEAR STAIN REMOVER 50ML CHARLES PET',
                   '8',
                   '667',
                   'BC2D1245-7509-406C-AB36-BDE9F6B73B3C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT H2O FOUNTAIN',
                   '3',
                   '668',
                   '043F3729-D553-4BD4-8226-DA65082DCB6F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT H2O FOUNTAIN FILTER REPLACEMENT',
                   '4',
                   '669',
                   '19DC411E-80AA-4C52-A799-09B47424B917-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT H2O FOUNTAIN PUMP REPLACEMENT',
                   '3',
                   '670',
                   'C6CBBF11-CDE6-45D4-AE5F-E7FA44081722-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT LEASH JTC HARNESS',
                   '14',
                   '671',
                   'E476F3B9-F02B-4053-B644-05C9A10F2646-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT LITTER SCOOP',
                   '2',
                   '672',
                   'E32DCB8B-28E7-4603-948E-3AFD8E7CB51E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CAT TEASER',
                   '2',
                   '673',
                   'CDDF7BFE-58FB-4CC5-BED7-B5FB4112A632-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHARLES PET CAT TOY 5PCS ASSORTED WD-1308',
                   '6',
                   '674',
                   '5B26132D-681E-47EF-946A-A5048F424D7F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHARLES PET ROPE TOY GREEN S/A Y304-T02',
                   '7',
                   '675',
                   '32792FEF-642A-4561-A517-D71725A8A775-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHARLES PET ROPE TOY PINK PC 3003',
                   '8',
                   '676',
                   '7179062F-40BE-4C57-A910-9F02FF1F6C5D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHOKER CHAIN 4.0 CHARLES PET DC-9513',
                   '5',
                   '677',
                   '812CC574-E97C-4644-9484-A74F92845B3A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CLASSICO INDOOR 3WALL PET TOILET LARGE',
                   '1',
                   '678',
                   '11185833-9C19-4777-BCDB-C11AA7BECF6C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON E.COLLAR LARGE CHARLES PET',
                   '0',
                   '679',
                   'D2125AF1-4B82-4B4D-8422-0DBB649F3F03-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON E.COLLAR MEDIUM CHARLES PET',
                   '0',
                   '680',
                   'B98CFAC7-2B39-420E-BFB5-3495A6F6D35B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON E.COLLAR SMALL CHARLES PET',
                   '20',
                   '681',
                   'AFA63880-0DEA-4748-BC60-2CDAFB89E7ED-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COTTON E.COLLAR XLARGE CHARLES PET',
                   '11',
                   '682',
                   '036791FD-761F-4BE7-BA22-2D40E85BFCCC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DOG FOOD SCOOPER BIG TEARDROP',
                   '19',
                   '683',
                   '9315CF70-B5C9-400B-93F4-C83497A482CD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DOG SHOES SMALL',
                   '0',
                   '684',
                   '2E8D73C1-BBC7-4DBE-9A7F-BA002B7CD001-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ELEVATED MATTING HEAVY DUTY',
                   '0',
                   '685',
                   '4CE459D8-656E-40BB-8824-63F63AC875D2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELINE FRESH 10 LITERS (COFFEE)',
                   '18',
                   '686',
                   '5BD7A28F-F2FC-4EFF-ACCD-54C2531287D9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELINE FRESH 10KG (LAVANDER) 10L',
                   '12',
                   '687',
                   '907A9017-1708-4AB5-9D5F-E36F009D8C2B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELINE FRESH 10L LEMON',
                   '18',
                   '688',
                   '4BE58161-6457-486B-98BE-15D9D2ACEB1D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELINE FRESH PINK SAND PREMIUM FINE',
                   '26',
                   '689',
                   '32DC6A89-9BC2-4158-8758-881389BDFAA1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FIREFLY 50W BULB',
                   '79',
                   '690',
                   '0D169E6F-93F7-4383-ABC0-CE7AEA2C4069-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FRENZY SHAMPOO 250ML (TOMMY)',
                   '0',
                   '691',
                   '27EE3C28-0F08-46F5-8055-0F6D79EC737B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 1L BLUE',
                   '11',
                   '692',
                   '8334890A-E9D8-42F3-AA79-4DFD983B1662-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 1L PINK',
                   '11',
                   '693',
                   '16BFCA42-9ECE-49DC-A5F2-646214867DF9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 1L VIOLET',
                   '6',
                   '694',
                   '1023A5F4-7C6A-437C-AB76-B6B4F92CD4EE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 300ML BLUE',
                   '5',
                   '695',
                   '4B65D02C-4626-4718-8375-B8F9E80E746B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 300ML PINK',
                   '9',
                   '696',
                   '278B5BB9-85F5-4AE1-B045-F69E605327EF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FUR MAGIC SHAMPOO 300ML VIOLET',
                   '12',
                   '697',
                   'EEC877AF-E40D-49D0-8053-B734CEF509A9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 H520 WATER FEEDER ADAPTOR (SINGLE NOZZLE)',
                   '53',
                   '698',
                   '17AAC279-D157-4318-9664-303AC8269C29-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET DELUXE DIAPER (L) 12S BREATHABLE, REUSABLE 140P',
                   '247',
                   '699',
                   '71377CDB-B888-49C8-9BC4-E158F639DCEF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET DELUXE DIAPER (M) 12S BREATHABLE, REUSABLE 125P',
                   '179',
                   '700',
                   'A50BDB56-8234-49E7-932D-2E7B9A802356-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET DELUXE DIAPER (S) 12S BREATHABLE, REUSABLE 90P',
                   '557',
                   '701',
                   '9F120493-3A4E-4812-9915-14334E6D1AC2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET DELUXE DIAPER (XL) 12S BREATHABLE, REUSABLE 150P',
                   '119',
                   '702',
                   '314203AD-53B3-483B-80D1-A737DA46FDFC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET DELUXE DIAPER (XS) 12S BREATHABLE, REUSABLE 80P',
                   '182',
                   '703',
                   '4FA64FA9-3259-4ACA-8C59-D3349B80DE87-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET MALE WRAP DIAPER LARGE 12S',
                   '26',
                   '704',
                   '4D3B15EB-3463-4CAA-A461-C01F2455FE8E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET MALE WRAP DIAPER MEDIUM 12PCS',
                   '0',
                   '705',
                   '9018B017-54C7-4246-95BC-2DF57153DD7E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET MALE WRAP DIAPER SMALL 12PCS',
                   '87',
                   '706',
                   '2BF4600E-40D2-4E35-8389-379A0361ACA9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET SHEETS 300X450 (S) 100S 380P',
                   '1336',
                   '707',
                   '801E8953-02BD-4A38-8DE0-40751F10264D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET SHEETS 600X450 (M) 50S 390P',
                   '442',
                   '708',
                   '93CA935B-DDD9-4A1B-901F-575CB015D7D8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET SHEETS 600X900 (L) 10S 155P',
                   '223',
                   '709',
                   '01568B0C-4D49-4A29-A2E1-8C9F07AF9B7B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HUSH PET WIPES MEDIUM 80S',
                   '44',
                   '710',
                   '3A68C938-C501-4589-A602-DA7FB371F9C5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC 5MM DOG CHAIN WITH LEASH',
                   '1',
                   '711',
                   'A8BB7BF8-B7BA-4544-A7F2-5FACBC49B4C9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC 6MM DOG CHAIN PADDED WITH LEASH',
                   '0',
                   '712',
                   '8DA26F52-4B3F-42E2-9812-E80A78690CAF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC DOG TOY DUMBELL SMALL',
                   '0',
                   '713',
                   '77B6CF89-15C4-4429-965B-2F4A744561A7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC DOG TOY TELEPHONE',
                   '4',
                   '714',
                   'E8480CDF-E4E4-42BB-A322-A4E97EC78ED0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC PADDED COLLAR LARGE',
                   '7',
                   '715',
                   '42A5996F-DD3F-40EF-8FC7-F3CC2B22ABFD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC PADDED COLLAR MED',
                   '15',
                   '716',
                   '4765F151-F459-46EE-826B-CA4E96133EAA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC PADDED COLLAR SMALL',
                   '16',
                   '717',
                   'F3FEFB2A-F31F-42BE-899C-AB0C0114006B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y006',
                   '1',
                   '718',
                   'B7691878-D686-44EF-8CA1-0006CAB53055-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y007',
                   '2',
                   '719',
                   '5F1431FB-4F25-458C-B003-E1BF4B5B6F27-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y009',
                   '0',
                   '720',
                   '925D301E-B3A2-4584-B8BE-16FEC0D58B4C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y012',
                   '0',
                   '721',
                   '8BFEC744-DA0C-4328-BF98-8BD84E83C41B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y020',
                   '1',
                   '722',
                   '77687477-51FA-4EEF-86A8-3E39FB506E97-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JTC ROUND LEASH Y023',
                   '5',
                   '723',
                   '6A2DD165-7122-4D3F-9D66-65FE27215B76-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 KAWATI NATURALE OATMEAL TAWAS SHAMPOO 250ML',
                   '15',
                   '724',
                   '5E073E39-8B0F-451E-A610-C0852E5F9135-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MADRE DE CACAO GALLON',
                   '0',
                   '725',
                   '1B67A756-41D2-4A28-BD14-7D9DA2FA0CB7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MAXWELL USA EAR POWDER 30G.',
                   '10',
                   '726',
                   '843C33DC-4265-4775-8C7D-FF6F83303D4D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MEOWTECH CAT LITTER (APPLE) 12L',
                   '7',
                   '727',
                   'C147361E-8FB3-4235-9D11-9B0594C52BF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO DOUBLE SIDED COMB',
                   '12',
                   '728',
                   '8A0197D1-CF9C-41A4-BBF3-FDC95CD9F5AA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO FINISHING COMB',
                   '5',
                   '729',
                   '0D13CB05-20D0-4C6C-99FB-B9460F19A92B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO MAT AWAY COMB',
                   '9',
                   '730',
                   '7041AA3D-1A64-40F7-B284-296E965022F0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO NAIL SCISSOR FOR PUPPIES / CATS / SMALL PETS ..',
                   '18',
                   '731',
                   '6F6105EB-EEF2-44CE-B236-985B0CC56ECA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO PREMIUM NAIL CLIPPER LARGE',
                   '12',
                   '732',
                   '0B784A55-814A-4571-B6CE-C9FC98B3BEF6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO PREMIUM NAIL CLIPPER SMALL',
                   '11',
                   '733',
                   'A731E468-6148-4DFB-9F13-0B368CF2FB60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO PROFESSIONAL 2-SIDED BRUSH',
                   '2',
                   '734',
                   '6E384FAB-205B-4740-8C8B-C20AA8934715-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO RUBBER MASSAGE BRUSH',
                   '6',
                   '735',
                   '68924744-8903-4C04-832F-67020635DB9B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SLICKER BRUSH (L) PINK/BLUE',
                   '12',
                   '736',
                   '131F5806-4EF0-45FE-8244-25019678CB39-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SLICKER BRUSH (S) PINK/BLUE',
                   '9',
                   '737',
                   '1AD30288-5247-4715-BD2F-07CF5F20E367-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SOFT SLICKER LARGE',
                   '14',
                   '738',
                   'AACE0D7C-5F8F-4B6E-9AC1-E463EB8EC081-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SOFT SLICKER SMALL',
                   '12',
                   '739',
                   '816BF3BD-7E33-4664-8FB3-CC820C9C09D3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SQUEAK BALL (SMALL)',
                   '19',
                   '740',
                   '3A21CE9C-0CE3-4973-8448-7591D34BE488-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SQUEAK BALL LARGE',
                   '12',
                   '741',
                   '04AF9D7B-724E-4CAD-8D98-1A3859CCAC6A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SQUEAK BALL SMALL STARS NEW',
                   '0',
                   '742',
                   'D2D99160-86B2-4182-8B83-22567E40E46E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SQUEAK BONE (SMALL)',
                   '12',
                   '743',
                   '109E17FE-818E-461C-80FB-E273BE9B5822-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO SQUEAK BONE LARGE',
                   '10',
                   '744',
                   '0EDA52AF-C2C5-4B4C-B70B-A7EA0284301E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MICHIKO TANGLE AWAY COMB',
                   '5',
                   '745',
                   'FE4C56B2-1699-40D7-9199-8468857C915A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MODERNO WATER TANK BOWL',
                   '3',
                   '746',
                   '05F43D87-55A7-411E-B77C-DF7034753FC7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MUZZLE - ADJUSTABLE (L) - 25 - 30CM',
                   '3',
                   '747',
                   '215FFFF5-0BFE-4B23-B793-8FF4F903FF09-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MUZZLE - ADJUSTABLE (M) -20-25CM',
                   '6',
                   '748',
                   '62DD0302-FA43-45B8-ABE4-F6EAB3C0AD52-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MUZZLE - ADJUSTABLE (S) 13-18CM',
                   '7',
                   '749',
                   '10CA3225-6ECF-40DF-A58C-A64CF11D1D6F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NECK PILLOW LARGE',
                   '3',
                   '750',
                   'C7ECCBA0-27D4-4590-8C1E-DA1D0DDE5891-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NECK PILLOW MED',
                   '4',
                   '751',
                   'C5389C99-8BA6-4E3C-9866-C85BBC067159-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NECK PILLOW SMALL',
                   '5',
                   '752',
                   '8EFE27BC-1DB1-485C-8EF5-3FE7C49CF998-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NECK PILLOW XS',
                   '5',
                   '753',
                   '7AB8DE98-4A30-4C2A-BAB6-628B2EC82526-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEW ROYAL TAIL ESSENTIALS MDC ALPINE BREEZE SOAP 150G',
                   '2',
                   '754',
                   'EB8B8D99-772B-4947-B088-D897A9494815-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEW ROYAL TAIL ESSENTIALS MDC AQUA RENDEZVOUS SOAP 150G',
                   '4',
                   '755',
                   'BFFC26BB-1028-4FE5-9CA3-6FE90D9C2C4D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEW ROYAL TAIL ESSENTIALS MDC FRESH PUPPY SOAP 150G',
                   '7',
                   '756',
                   'C7881104-5C22-488D-BCA5-9EF9579E214B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEW ROYAL TAIL ESSENTIALS MDC TUTTI FRUITIE SOAP 150G',
                   '8',
                   '757',
                   '4904C702-0B4E-43EE-AF16-01E07292CF83-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEW ROYAL TAIL ESSENTIALS MDC ZEN GARDEN SOAP 150G',
                   '7',
                   '758',
                   'EA78A438-8997-4D73-ACBE-884B6FE949C4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NURSING BOTTLE BIG CHARLES PET',
                   '125',
                   '759',
                   '3400D089-5584-46AD-9AE4-7BD9870B315A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NURSING BOTTLE SMALL CHARLES PET',
                   '181',
                   '760',
                   '5179DD00-9C72-49D0-BB3A-B86EC64440DD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR - LARGE',
                   '6',
                   '761',
                   '7C40C8AC-434F-40CB-9CD5-DC961CC56976-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR - MEDIUM',
                   '6',
                   '762',
                   '75D10F47-8EAB-4718-ABE7-AE35069617E4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR - SMALL',
                   '11',
                   '763',
                   'EB8B3689-346E-4E34-A4A1-B5750EDDBA92-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR - XLARGE',
                   '17',
                   '764',
                   '8AA7E399-52A9-4F40-84B3-780AB541A415-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR+LEAD SET- LARGE',
                   '15',
                   '765',
                   '2ABC1252-6267-47D9-B785-8FAADBA26612-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR+LEAD SET- MEDIUM',
                   '4',
                   '766',
                   '9C269717-5D02-4B97-B673-CF7A0BDFA110-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR+LEAD SET- SMALL',
                   '6',
                   '767',
                   'AA1E66D9-F509-4C4A-A2DC-745769804959-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON COLLAR+LEAD SET- XLARGE',
                   '14',
                   '768',
                   '4ECA2ED3-2DD7-440F-B23A-9D5D0798072A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEAD SMALL',
                   '2',
                   '769',
                   'BEBF718F-0092-49BC-ADE7-B73AC2868E7B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEAD+HARNESS SET- LARGE',
                   '11',
                   '770',
                   '479E30EE-09F3-4E44-AA16-16E141E0AED8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEAD+HARNESS SET- MEDIUM',
                   '16',
                   '771',
                   '82CA0257-E247-46A1-80DD-42252C317EF4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEAD+HARNESS SET- SMALL',
                   '7',
                   '772',
                   'F647A023-8125-45B2-A7DE-48B5944FB925-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NYLON LEAD+HARNESS SET- XLARGE',
                   '12',
                   '773',
                   '00C4B1E5-39A7-40DB-B7CC-752BD0D14C8C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PADDED HARNESS ONLY',
                   '1',
                   '774',
                   '068A85EA-04C6-4435-8454-1B46392575EF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH ANTIMANGE SHAMPOO 260ML',
                   '5',
                   '775',
                   'EBED7C68-6024-436E-B028-C68C13E4A266-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH SHAMPOO 260ML ANTI BACTERIAL',
                   '9',
                   '776',
                   '86961095-2654-48A7-9B20-90386D28D93A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH SHAMPOO 260ML ANTI TICK AND FLEA',
                   '2',
                   '777',
                   '116DB35A-E514-4F5F-9BED-BA6C7A062CB9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH SHAMPOO ANTI BACTERIAL 1L',
                   '5',
                   '778',
                   '96125DAD-F2BF-47B2-ACEC-54436A2571F9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH SHAMPOO ANTI MANGE 1L',
                   '6',
                   '779',
                   '0F401765-540F-4A40-B52E-D4F311DAED65-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAMPERED POOCH SHAMPOO ANTI TICK AND FLEA 1L',
                   '0',
                   '780',
                   '47BB75D6-8A3F-4B6C-9BA2-EF8EE364E0FC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET E. COLLAR #1',
                   '2',
                   '781',
                   '951D42B3-957B-46A3-8137-3F8D3DF553B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET E. COLLAR #2',
                   '2',
                   '782',
                   'DE05D4FA-53CD-4C31-B6A2-2B43472C03E1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET E. COLLAR #5',
                   '6',
                   '783',
                   'ADCE1E5B-A47C-42D6-A6C7-6C5EB655ABAA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET HOUSE POWDER',
                   '9',
                   '784',
                   '6D1B939F-C23E-4DBB-84AF-00A6DA44C90E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET SOFT TOOTHBRUSH 2PCS. SET',
                   '9',
                   '785',
                   'B5EAB088-305A-4EDA-95C3-DEABB21BE3AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET TOOTHBRUSH 2 PIECE SET..',
                   '10',
                   '786',
                   '62567348-2088-4392-8D81-B58208E30776-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PILL GUN',
                   '1',
                   '787',
                   '4DF9C5FF-9D85-4DB1-885C-3EACD3F6E0D9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLASTIC MATTING 1X3 (WHITE) - CLASS A..',
                   '5',
                   '788',
                   '392BBE78-430F-4D57-8167-53838862419A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS PET SPLASH AZURE 250ML',
                   '7',
                   '789',
                   '8830B443-534A-443A-82F9-34EBC35782B2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS PET SPLASH LAVANDER 250ML',
                   '2',
                   '790',
                   '8F97CC10-72DE-43E8-83CC-2AC0EFE791D7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS PET SPLASH PINK CANDY 250ML',
                   '2',
                   '791',
                   'D17D5ECD-D09D-499E-A013-07AF075BB02D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS PET SPLASH RED DREAM SCENT 250ML',
                   '4',
                   '792',
                   'D18D3D41-3780-4BF2-9115-27C3716E8DB9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS PINK CANDY 1L SHAMPOO',
                   '9',
                   '793',
                   '8EE131FF-BCA6-4FF7-A28B-155651944CEF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS SHAMPOO 1L ANTI MANGE',
                   '2',
                   '794',
                   'F250204E-CC4D-4DCC-88BF-59E89A43C020-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS SHAMPOO 1L OATMEAL',
                   '6',
                   '795',
                   '52132775-DA8B-4B81-8EED-4702A2BCA1F5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS SHAMPOO AZURE 1L',
                   '8',
                   '796',
                   '452DA463-93B3-460B-A1B1-49C4F78D92D3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS SHAMPOO COTTON FRESH 1L',
                   '0',
                   '797',
                   '50585022-1D2A-4923-A52C-D3E14D72D34D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PLAYPETS SHAMPOO ODOR ELIMINATOR 1L',
                   '0',
                   '798',
                   '88FEBE5D-CFFC-4B90-9A4A-2E9C38E90D1D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PUPPY SAUCER 12 INCHES',
                   '7',
                   '799',
                   '25A57CDA-D88E-466E-9B09-E9F9D6C2B35D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PUPPY SAUCER 13.5',
                   '4',
                   '800',
                   '175C6396-0F8D-4FCF-AA4A-88780BEA1F99-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL 16OZ.',
                   '10',
                   '801',
                   'D5B3335A-E24E-404B-8197-AEEFC86F28B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL 24OZ',
                   '24',
                   '802',
                   '9662E42B-3093-448F-BA2D-C8F31DF657A4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL 32OZ',
                   '33',
                   '803',
                   '26F99059-9668-4A0D-ACC4-56005B44AC70-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL 64OZ',
                   '11',
                   '804',
                   '45C59E5E-16FF-4965-B819-AB27C27DF629-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL 96OZ',
                   '13',
                   '805',
                   '1DBF3AB0-AB0D-41F6-8BBB-FF87C27D9DE9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REGULAR NON SKID BOWL(8OZ)',
                   '6',
                   '806',
                   '5E18B57F-6DF1-4285-BD66-C762D5B48C4C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL PET EAR CLEANER 110ML',
                   '14',
                   '807',
                   'F5793283-D432-4390-80CD-C675C5A7228A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1 GAL. FRESH PUPPY SHAMPOO',
                   '5',
                   '808',
                   '22E6C084-187F-4F11-A9C7-ADF552B281AB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1GAL. ALPHINE BREEZE SHAMPOO',
                   '5',
                   '809',
                   'D83BFDFE-D847-4471-9A86-DA00CF89274D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1GAL. AQUA RENDEZVOUS SHAMPOO',
                   '3',
                   '810',
                   'C4F38CAB-A1B0-461A-A1FF-6A5CFDFBA36D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1GAL. SUMMER FIESTA SHAMPOO',
                   '2',
                   '811',
                   'BD5EB840-9CCA-4F75-A5E2-07D37D6DF223-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1GAL. TUTTI FRUTIE SHAMPOO',
                   '2',
                   '812',
                   '63C63617-7C80-4DFB-864F-5C9535A36591-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1GAL. ZEN GARDEN SHAMPOO',
                   '2',
                   '813',
                   'E9A3FEB2-D124-4692-B35A-31E834957CB9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1L ALPINE BREEZE SHAMPOO',
                   '3',
                   '814',
                   '58510275-E7A8-4217-BD31-207367525C23-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1L AQUA RENDEZVOUS SHAMPOO',
                   '3',
                   '815',
                   '15BE068E-9B5E-4F0D-81A7-CBACAE0A4B8E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1L FRESH PUPPY SHAMPOO',
                   '3',
                   '816',
                   '6BB0DC25-6282-4468-B34D-D2A2BE839762-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1L SUMMER FIESTA SHAMPOO',
                   '3',
                   '817',
                   'FD39AC82-C751-4929-9266-9C5CCDF155B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 1L ZEN GARDEN SHAMPOO',
                   '4',
                   '818',
                   '93BCD437-7942-46B2-9A15-5B69CBF50330-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML  FREES PUPPY SHAMPOO',
                   '1',
                   '819',
                   '9C9D2562-2B08-4753-8FD6-068831A535F8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML  TUTTI FRUITIE SHAMPOO',
                   '3',
                   '820',
                   '9C918210-9EEA-4FD0-B536-0A94D8752281-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML ALPHINE BREEZE SHAMPOO',
                   '2',
                   '821',
                   '41D47050-0672-4C56-9CB9-EC78D4EAD4AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML AQUA RENDEZVOUS SHAMPOO',
                   '2',
                   '822',
                   'E8FF872D-2207-4425-AAA1-12010B495903-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML SUMMER FIESTA SHAMPOO',
                   '3',
                   '823',
                   '80694167-1623-4485-BF90-4F7470929C1B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROYAL TAIL ESSENTIALS MDC 500ML ZEN GARDEN SHAMPOO',
                   '3',
                   '824',
                   '9DF1A9B8-63B3-4354-A6B6-C5D7C0136C83-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SOFT BED LARGE',
                   '1',
                   '825',
                   '8AE27415-C3A3-4670-9D99-ED895BED1E9A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SOFT BED SMALL',
                   '0',
                   '826',
                   '7DA14F74-9282-421B-866D-7AF589AC5E23-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE - PREMIUM DOG SOAP HEAVEN SCENT 135G.',
                   '5',
                   '827',
                   'C0F390DB-AB4E-4B10-A367-5D778A0C4339-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE BASIC DOG SHAMPOO CHERUB SCENT 1000ML',
                   '12',
                   '828',
                   '2AA88D5E-4CF8-4126-9CC8-30CDFCAD717B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE BASIC DOG SHAMPOO LOVELY LOVENDER SCENT 1000ML',
                   '2',
                   '829',
                   '235366E0-BFD4-480F-8178-D16E18970E75-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE DETANGLING SPRAY HEAVEN SCENT',
                   '2',
                   '830',
                   'B300843F-148D-47DC-A483-8306C32D322F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE DETANGLING SPRAY MOTHER NATURE',
                   '6',
                   '831',
                   '69B43683-67F9-413F-A5AD-67D92E8C1E22-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE DOG EDT COLOGNE HAPPINESS 125ML',
                   '7',
                   '832',
                   '22FC7799-0552-44CF-A7C4-0EA8E54931B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE DOG EDT COLOGNE HEAVEN SCENT 125ML',
                   '7',
                   '833',
                   '058C6EA2-9961-461D-A72D-31F22EA97AB5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE DOG EDT COLOGNE SWEET EMBRACE 125ML',
                   '10',
                   '834',
                   '8646B7DE-A78F-4BFF-8016-CF05EE2DDB1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN - CONDITIONER HAPPINESS 338ML',
                   '3',
                   '835',
                   'EA885B48-815E-4B29-AC5E-193CAA328395-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN - CONDITIONER HEAVEN SCENT 338ML',
                   '3',
                   '836',
                   '806C77AC-3A67-44C6-96E8-EC24540E41C6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN - CONDITIONER PEACE 338ML',
                   '3',
                   '837',
                   '7C020465-7ADF-40E7-8AE6-5710277ABE5B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN - CONDITIONER SWEET EMBRACE 338ML',
                   '2',
                   '838',
                   'EED2C24F-F89B-4863-BE67-00230DF20ADA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN BODY WASH HAPPINESS (518ML)',
                   '1',
                   '839',
                   'A5BB3B5C-BFE3-4A1F-A11D-F4183BA6D445-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN BODY WASH HEAVEN SCENT (518ML)',
                   '2',
                   '840',
                   '11470013-FB09-4428-8DF9-5CD09E1A29AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN BODY WASH PEACE (518ML)',
                   '2',
                   '841',
                   '13A80F46-3E21-48F1-B588-67BBF4FAA704-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN BODY WASH SWEET EMBRACE (518ML)',
                   '3',
                   '842',
                   '999DDAB3-109C-4F2F-B621-6EADBCD51FE2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN LOTION HAPPINESS 128ML',
                   '2',
                   '843',
                   '083DBBB1-411C-4059-98B9-899402803792-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN LOTION HEAVEN SCENT 128ML',
                   '1',
                   '844',
                   'B3714441-01E6-4A3F-A929-9CD22152720A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN LOTION PEACE 128ML',
                   '0',
                   '845',
                   '5EE8EEA7-485B-40C4-AC59-022BB80AEC37-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN LOTION SWEET EMBRACE 128ML',
                   '2',
                   '846',
                   'A381655D-09D2-4352-A950-D28EB2C19485-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN SHAMPOO HAPPINESS  (538ML)',
                   '1',
                   '847',
                   '12C78F4E-9AE6-4396-BB0E-EFBC3FE6ECD0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN SHAMPOO HEAVEN SCENT (538ML)',
                   '0',
                   '848',
                   'D4178E42-541E-4F7B-B109-9D959E78A5B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE HOOMAN SHAMPOO PEACE (538ML)',
                   '3',
                   '849',
                   '91C08DFA-9723-4763-882D-FF54094B602C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE PREMIUM DOG CONDITIONER',
                   '4',
                   '850',
                   '330959F6-B5C5-4482-BB63-322E92645079-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO HAPPINESS 1L',
                   '0',
                   '851',
                   '710CF17F-AFD0-419D-85F1-73028919D83F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO HAPPINESS 250ML',
                   '5',
                   '852',
                   'FD667C37-CB7D-4CD7-B4AF-2DAF0D7242E0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO HEAVEN SCENT 1L',
                   '3',
                   '853',
                   'B0A20232-DC9B-484C-AB2B-2AB5334E0C96-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO HEAVEN SCENT 250ML',
                   '2',
                   '854',
                   '2AF332B9-B77D-4C55-BB1D-86802F7F3F93-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO SWEET EMBRACE 1L',
                   '4',
                   '855',
                   '53F790CC-DB97-468A-A51B-04DAF36B5167-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ST. ROCHE SHAMPOO SWEET EMBRACE 250ML',
                   '9',
                   '856',
                   '9729C107-F270-441D-9EA0-97C12628A538-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TIMELESS LITTER PAN LARGE RECTANGULAR',
                   '2',
                   '857',
                   '1677D613-D4F3-4401-AEDB-80CCBE4643F8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TIMELESS LITTER PAN MEDIUM RECTANGULAR',
                   '6',
                   '858',
                   '8B189F4E-B203-409A-BC07-4311CF25789E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TIMELESS LITTER PAN SMALL RECTANGULAR',
                   '4',
                   '859',
                   'A304196D-52DE-4ABF-A91C-59B48D2612F5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOOTHBRUSH F-425 CHARLES PET 2SIDED',
                   '8',
                   '860',
                   '441E6BC2-9FA4-4DC6-BD2C-264848A59A14-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOP TAIL CITRUS TWIST PET COLOGNE 100ML',
                   '18',
                   '861',
                   'F3397DC3-94F0-437D-A7D8-6F0416688826-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOP TAIL ESSENTIALS CITRUS TWIST TALC POWDER 100G',
                   '0',
                   '862',
                   '8B503860-EDAB-4285-B51D-E62F97AE0577-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOP TAILS SOAP FRESH MINT',
                   '0',
                   '863',
                   '39DB3D4D-9839-4675-B72F-C15CA87F5FAB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOP TAILS SOAP ODOR ABSORBER',
                   '0',
                   '864',
                   '29A56299-B95D-4BC8-9E58-34470C2A0C36-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOP TAILS SOAP PUPPY WITH OAT',
                   '0',
                   '865',
                   'E84FC0E6-95A0-44B4-A217-A41DFF4B077E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 10CM',
                   '0',
                   '866',
                   'EAB8F8D2-A1B2-43E3-AA8F-D3E558963ED4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 15CM',
                   '7',
                   '867',
                   'B913219E-55D5-4694-9433-AE9163ADC688-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 20CM',
                   '7',
                   '868',
                   'A152AA80-9B23-4ADF-82DE-FF1D5C23C871-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 25CM',
                   '6',
                   '869',
                   'A55E169E-192B-4E99-9C32-6130476544B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 30CM',
                   '8',
                   '870',
                   '907C9408-EFA7-4A1A-B1E6-3B2FE222B773-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 UNIVET DOG COLLAR 7.5CM',
                   '9',
                   '871',
                   '89036704-1761-4DC7-B124-9430C76153EA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZAP EAR CLEANER 118ML',
                   '11',
                   '872',
                   '2B8CFC58-1BCD-4361-BA90-4EBB3BE6C060-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022PLAYPETS PET SPLASH 2IN1 ANTI TICK AND FLEA 250ML',
                   '3',
                   '873',
                   '0F5A697F-F915-4771-9009-6C6C51698DCE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ARMANI WALKING LEASH 10MM',
                   '0',
                   '874',
                   '06E77908-4D0D-4953-B1B8-C65BE5BAC4FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'AS STYPTIC POWDER',
                   '0',
                   '875',
                   '155F4864-5322-49FD-8A40-50CD3B19F0B1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ATOM BALL WITH BELL',
                   '0',
                   '876',
                   '942CE417-496F-486A-8987-D3DFAC025EA4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BARK SOAP ANTI TICK AND FLEA',
                   '0',
                   '877',
                   'E58102A4-30DB-48E5-A759-2A4E0A8BF101-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BARK SOAP MADRE DE CACAO',
                   '0',
                   '878',
                   '3D8829F7-94D0-4DB5-B10F-07EF69FE614E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BEARING ANTI TICK AND FLEA POWDER 300G',
                   '0',
                   '879',
                   '166530E3-F655-45A8-B09E-AF08E12B3486-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BEARING POWDER ANTI-FLEA AND TICK 150GMS.',
                   '0',
                   '880',
                   '3CAFBB34-A697-4B69-AF59-26A0F98371EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BEARING POWDER DRY SHAMPOO 300GMS.',
                   '0',
                   '881',
                   '4C2159FB-0F4F-4096-AD54-6C1D3C37F012-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BIOLINE PERFUME CHARLES PET',
                   '0',
                   '882',
                   '0B9D433F-E242-4CBA-92C7-64A4965F0D42-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BLUE SMALL CUP FEEDER',
                   '0',
                   '883',
                   '62C7C327-966A-4518-8151-B4A96AB69076-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BOOTS LARGE CHARLES PET',
                   '0',
                   '884',
                   '6804C8E4-FFDC-4968-9722-1FC2BB731CEC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BOOTS SMALL CHARLES PET',
                   '0',
                   '885',
                   'EB0F11F6-81A1-46E6-BABF-ABBF1D50B40E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BURGER XL',
                   '0',
                   '886',
                   '903B73AB-781A-434D-920E-824655BDFE7E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CABLE WIRE LEASH',
                   '0',
                   '887',
                   '3F7E6918-3596-431A-9F33-299D45792AEB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CAT TEASER GLITTER HANDLE',
                   '0',
                   '888',
                   '82F3C29C-49B1-4FA9-A41A-B51DAA7EC526-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CATNIP JTC',
                   '0',
                   '889',
                   '46F7376B-ED2B-447D-8FA7-9C72A8C3698E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHICKEN YELLOW MINI SQUEAKY',
                   '0',
                   '890',
                   '5697F1BA-6B9D-41AB-AF48-ED08D6DC46D5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CLASSICO INDOOR 3WALL PET TOILET SMALL',
                   '0',
                   '891',
                   'DDE767A8-499C-41A2-B246-743FD08EB3BC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CLASSICO INDOOR PET TOILET LARGE',
                   '0',
                   '892',
                   'EAEC183A-B659-4EB3-9E9F-2A38BD75A36E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CLASSICO INDOOR PET TOILET SMALL',
                   '0',
                   '893',
                   '531C4B6E-5FE6-49F6-8AD7-F781A37C3802-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CLICKER WITH WHISTLE C-06 CHARLES PET',
                   '0',
                   '894',
                   '7BB5C50B-D1C2-44CF-88DC-E8757ADBE4D3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR SINGLE RIBBON WITH DIAMOND',
                   '0',
                   '895',
                   'E4208E3D-37E8-457A-BD5A-0638F2F91580-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR SINGLE STRIPES FLOWER',
                   '0',
                   '896',
                   '39271AE3-850A-4E4E-87AD-F522DFFE863B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR SINGLE WHITE PAWS',
                   '0',
                   '897',
                   'D2101F0E-9289-4DBB-A2D1-790AA40B9251-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR SMALL W/BELL NEON',
                   '0',
                   '898',
                   '50D4D4E2-EFCF-4E94-8BA2-6BB8D2040E30-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR WITH BELL JTC 2018 SMALL',
                   '0',
                   '899',
                   'A3397453-CDDC-4CE3-8D74-8E5002326EE1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLLAR WITH SINGLE BELL JTC 2018 BIG',
                   '0',
                   '900',
                   'DE88AC89-4AE0-4811-9C2F-A7AB7E5BD63E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLORED BELL MEDIUM CHARLES PET',
                   '0',
                   '901',
                   '571C2E47-52F4-4CDC-83EE-899C1D4FBA28-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLORED CUP LARGE',
                   '0',
                   '902',
                   '66D0F12C-9C1A-4CAC-8396-C125C81C8CA6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COLORED MOUSE SQUEAKY TOY 4S',
                   '0',
                   '903',
                   '5890CC67-0573-4766-BD27-50D142F40F0D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'COTTON E. COLLAR POLKA GREEN',
                   '0',
                   '904',
                   '1A3EDD99-5CA1-43A2-B603-0FCAF3BB193E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CRYSTAL COVERED LITTER PAN LARGE',
                   '0',
                   '905',
                   '1836B781-7426-47FB-B576-54965DBB1938-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CRYSTAL HIGH BACK LITTER PAN LARGE',
                   '0',
                   '906',
                   'E389E435-9E34-45C9-9F5F-94E2B8066600-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CRYSTAL HIGH BACK LITTER PAN MEDIUM',
                   '0',
                   '907',
                   '7EADAF48-FB01-4789-AF3F-47D68F895D28-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CRYSTAL HIGH BACK LITTER PAN SMALL',
                   '0',
                   '908',
                   '6BAD77F2-F9A5-4D61-A50B-E2A7EAF78EED-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG BOWL PLASTIC BIG',
                   '0',
                   '909',
                   'EFDB7814-FC2D-44EA-AB11-F7243E3D9295-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG BOWL XL',
                   '0',
                   '910',
                   '6A39F025-3096-4CC1-93A0-707D832AC020-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG BOWL XXL',
                   '0',
                   '911',
                   'D1B29D60-FB59-4D64-B361-4EB87854C48C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG BOWL XXXL',
                   '0',
                   '912',
                   'CA35BA49-87DF-4AB1-83F2-6AD31196AFD9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG DESIGN FOOD AND WATER FEEDER WITH POLE',
                   '0',
                   '913',
                   '871A0B25-C8FB-4963-8CE7-B8772F32DADA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG DISH 30CM COLORED',
                   '0',
                   '914',
                   'AFB93E12-379B-47E2-B836-6E74F5FD4D53-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG FOOD SCOOPER SMALL FISH DESIGN',
                   '0',
                   '915',
                   'C1F52FD8-70FE-4D62-B752-0E8FE6E15E25-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG SHOES LARGE',
                   '0',
                   '916',
                   '2C4EA901-9014-44E4-917E-809D1BB79EEB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG SHOES SOCKS',
                   '0',
                   '917',
                   'DA8D84CF-CD4C-454F-8B4D-37BFA6D4643C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG SHOW LEASH 12X48 LARGE CHARLES PET',
                   '0',
                   '918',
                   '8DFA9A4E-B914-495B-99E1-33DC502AA17C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOG SOCKS',
                   '0',
                   '919',
                   '7152A134-7111-4189-8992-B97F91346974-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN BALANCE SOAP 115G',
                   '0',
                   '920',
                   '7EB4DB42-EB93-4A0F-94B9-3F6F7C0140DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN BUGS OFF SPRAY 120ML SMALL ESSENCE',
                   '0',
                   '921',
                   '7BCDAAB9-D0E8-479C-B6E4-5CE62856106C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN BUGS OFF SPRAY 280ML ESSENCE',
                   '0',
                   '922',
                   '547A02D9-B835-4029-8DEE-09B148D278DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN COLOGNE 120ML MORNINGS',
                   '0',
                   '923',
                   'A1C4B134-CFF8-49E8-9517-B2BAACA6D736-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN COLOGNE 120ML SWEET PINK',
                   '0',
                   '924',
                   'F072F4AC-EF1E-41F6-A09F-2ED35BED0529-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN COLOGNE 120ML SWEET POWDER',
                   '0',
                   '925',
                   '327F0994-745D-43A1-87C6-21C95E035BBE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN EAR WASH 15ML DROPS',
                   '0',
                   '926',
                   '01D41F0C-7CBD-40FA-B190-A9EC33891E1B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN SHAMPOO 296ML ESSENCE',
                   '0',
                   '927',
                   '03676D05-8905-49C0-B8F4-F4BBEC9BC129-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN SHAMPOO 296ML LAVANDER',
                   '0',
                   '928',
                   '8656307D-5F40-423B-AC2A-F8AF474559DB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DOGGO CLEAN SHAMPOO 600ML LAVANDER BIG',
                   '0',
                   '929',
                   'CE2AADCF-1554-4CB0-8396-B66583D365A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DONUT BIG SQEUAKY TOY',
                   '0',
                   '930',
                   'A14688F4-999B-4D0C-8181-D940740A4343-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DONUT SQUEAKY TOY SMALL',
                   '0',
                   '931',
                   '3C67B019-018D-415F-A7D1-3F5245BE03A9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DRESS SMALL',
                   '0',
                   '932',
                   '6B649F32-078E-4B8B-BA24-882ADEC29CE0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'E. COLLAR ASSTD  XL',
                   '0',
                   '933',
                   'B66335CC-5F33-47C2-B656-CA056F096530-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'E1 DOG DISH',
                   '0',
                   '934',
                   '708CB44D-76FD-404D-9EFC-F0EB4274BB03-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'E2 DOG DISH',
                   '0',
                   '935',
                   '470ECE36-A26D-4899-A6F6-A0F5CD0FF7C0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'E4 DOG DISH',
                   '0',
                   '936',
                   '10FB3A2F-B440-4A94-9CB6-EA936ECCF295-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'E5 DOG DISH',
                   '0',
                   '937',
                   '80232FE5-ED67-4FE6-9D20-308205861118-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'EYE CARE DR CLAUDERS 50ML',
                   '0',
                   '938',
                   'A4E84FE3-798A-47B7-9B25-BE20DBB77B73-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FRENZY SHAMPOO 250ML ATHLETIC',
                   '0',
                   '939',
                   '428AF195-8E45-4F07-B746-B00032D86E7F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FRENZY SHAMPOO 250ML STRAWBERRY',
                   '0',
                   '940',
                   'CBE766D6-49BB-4B7A-94A8-EC7A382230DF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FUR MAGIC FREEZE 1L GREEN',
                   '0',
                   '941',
                   'B1451E88-0B4F-4FAF-B317-E860273BAB78-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GIGGLE BONE',
                   '0',
                   '942',
                   'BF930F84-4B3A-4223-B5FE-EBBFB0A34FD9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GIGGLE JACK',
                   '0',
                   '943',
                   '0A7C7AA7-ED26-4377-889F-76DAC9635FC0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GRANDE COVERED LITTER PAN',
                   '0',
                   '944',
                   '28013935-1BB6-4AC6-9A3E-9329CF0905C6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'H570 WATER FEEDER ADAPTOR BIG',
                   '0',
                   '945',
                   '783C667F-CC1C-49A3-9610-BFE111F58802-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HEATING PAD SHOPPEE',
                   '0',
                   '946',
                   'B8532F9A-4AE2-4737-B508-92EC091A9074-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HUSH PET DELUXE DIAPER XS 1 PACK',
                   '0',
                   '947',
                   '847106E4-3C33-47C7-B188-825A867308E7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HUSH PET WIPES MEDIUM 40S',
                   '0',
                   '948',
                   '1CF28949-4453-431F-AF89-BFD46AF84A46-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ICE CREAM SQUEAKY',
                   '0',
                   '949',
                   'F8025C30-AD78-4AD9-AB3A-266E00363EA8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC COLLAR WITH BELLS',
                   '0',
                   '950',
                   '73E47B01-8E03-488C-AF63-53F570138E72-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOG BES',
                   '0',
                   '951',
                   '498C86D3-231C-4C9C-BD56-3A5DA257F345-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOG LEATHER LEASH MED',
                   '0',
                   '952',
                   '5EEE35E4-A30F-4DFB-8F21-E0CB91E3988D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOG LEATHER LEASH SMALL',
                   '0',
                   '953',
                   'DC29073F-C8E5-4931-9C2D-79E6EFC9AB54-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOG TOY DUMBELL LARGE',
                   '0',
                   '954',
                   '0A732BB0-E40B-4D4F-9B25-285799193A8C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOG TOY SMALL BASKETBALL',
                   '0',
                   '955',
                   '1B9C257E-E175-4D11-9E57-17EBE014DD4A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC DOUBLE SIDED BRUSH SMALL',
                   '0',
                   '956',
                   'B52A8C77-6C9F-42D6-8BDE-A9A61902E082-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC HEDGE HOG LARGE',
                   '0',
                   '957',
                   '67934848-3C97-447E-BC09-CF94C1E78740-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC HEDGEHOGS TOY',
                   '0',
                   '958',
                   '216B1307-361D-4A25-9A4E-CE86A2ED3AEB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC LEASH ROUND RAINBOW XL',
                   '0',
                   '959',
                   '9D7D3CF0-1F7D-463B-9257-E323471117FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC REFLECTIVE DOG LEASH LARGE',
                   '0',
                   '960',
                   '43DE8912-B856-4834-AC02-E5E57872FC60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JTC SMALL NAIL CUTTER WITH NAIL FILE',
                   '0',
                   '961',
                   '33DDFF1F-A5F2-4BD4-8341-44804098AB30-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LEASH ROUND BLACK VIOLET XL',
                   '0',
                   '962',
                   '75C9996A-10F2-402E-92EB-575BA79FE7DF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LEASH ROUND RAINBOW LARGE',
                   '0',
                   '963',
                   'F75B3BC3-9786-4E17-85EF-66DA9A521927-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LITTER PAN SCOOPER',
                   '0',
                   '964',
                   'C4B80E2C-98D2-4E3D-8344-4C3BE2164770-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LUXE HIGHBACK RECTANGULAR LITTER PAN LARGE',
                   '0',
                   '965',
                   'EDA17E50-8D69-4DCC-99B6-29E700B4941B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MADRE DE CACAO JTC 8/19',
                   '0',
                   '966',
                   '7B0462CC-F562-4494-A59C-3852805048F6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MADRE DE CACO SHAMPOO 300ML',
                   '0',
                   '967',
                   'DB1D35C2-0DF3-4585-AC39-B6C3470F4009-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MAXWELL SHAMPOO SMALL 150ML',
                   '0',
                   '968',
                   'D1BFB340-D70B-48BC-A96A-24A283833239-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MAXWELLSHAMPOO BIG 400ML',
                   '0',
                   '969',
                   '6B9089E7-CB65-4322-A394-E3D63FADC28D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MAZURI TORTOISE',
                   '0',
                   '970',
                   '24340B18-38D2-4F7D-8350-67610733C0AB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO DONUT BED (S) - BLUE',
                   '0',
                   '971',
                   '2474CC44-9573-4F00-9885-EF8D588F4407-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO DONUT BED (S) - PINK',
                   '0',
                   '972',
                   'ACA76CFF-EA55-4AD7-AA7F-B150E973CFEE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO DONUT BED LARGE',
                   '0',
                   '973',
                   'C4D7D45D-9991-4A2B-8829-E15935D2914A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO FINISHING COMB (WIDE)',
                   '0',
                   '974',
                   '5D4ACD56-083E-46DE-933D-B0EBAD1DD2FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO FLEA COMB',
                   '0',
                   '975',
                   'D3129A6C-4E36-4A18-B374-770B1781BA47-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO OXFORD (PAWPRINTS) RECTANGULAR BED LARGE',
                   '0',
                   '976',
                   'E946A5DA-6616-4865-B4E8-A755D92A5AB0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO OXFORD PAWPRINTS DONUT BED SMALL',
                   '0',
                   '977',
                   '879AF566-C66D-4DB0-8F77-C8A8A400977E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO OXFORD PAWPRINTS MATRESS LARGE BED BLUE (32X23X8CM)',
                   '0',
                   '978',
                   '18552856-C9C0-45DC-B4EE-5618C7F350EA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO POLKA DOT BED (L) - BLUE',
                   '0',
                   '979',
                   'E2EEA6F5-9D75-4236-A674-02F4683DC12C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO POLKA DOT BED (L) - PINK',
                   '0',
                   '980',
                   'A182DA77-90F6-4A0F-B551-7783E504AFC5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PREMIUM SLICKER (L)',
                   '0',
                   '981',
                   '08891166-7402-4EE7-AA86-CBD66956A5A6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PREMIUM SLICKER (M)',
                   '0',
                   '982',
                   '7810A8AE-EEE9-4881-8628-A9E2CC37DA23-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PREMIUM SLICKER (S)',
                   '0',
                   '983',
                   'C1D90FEC-49ED-4FE4-BCDB-D8345323DBAF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PREMIUM SLICKER (XL)',
                   '0',
                   '984',
                   '5EE6B97E-DAE5-4804-AB11-E5FA61CF2A19-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PREMIUM SLICKER(M) - WOOD HANDLE',
                   '0',
                   '985',
                   '120380B3-9933-402C-88E0-10E9B67AA898-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO PUPPY TEETHING ROPE',
                   '0',
                   '986',
                   '2B032C4F-695B-4785-9D5A-D11D080C38AD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO SLICKER BRUSH (M) PINK/BLUE',
                   '0',
                   '987',
                   '272D158B-27C2-4B99-9532-5ED31B5291FC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO SQUEAK BONE FLAT STAR',
                   '0',
                   '988',
                   'CF79A6A9-A423-4770-A269-6C235BAC1FF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MICHIKO TOOTHBRUSH FINGER 2PCS SET',
                   '0',
                   '989',
                   '03BE6F86-7C3A-4302-AA44-CE2F5DAA81B6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MODERNO FOOD TANK BOWL',
                   '0',
                   '990',
                   '63724F37-76D3-4D02-9F99-6C2249CF9504-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER A07 CHARLES PET',
                   '0',
                   '991',
                   '930C7886-2343-4E11-8BDF-EDE3EA167E58-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER A10 CHARLES PET',
                   '0',
                   '992',
                   'BD61BD38-0D68-41B2-853A-95C46BD8A409-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER A-27 CHARLES PET',
                   '0',
                   '993',
                   '6033EF63-4C07-4EF6-8243-9351D99371D6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER BLACK RED PAYAT',
                   '0',
                   '994',
                   '2C670ACC-A39D-44E6-8D54-7FBCD3B07DF0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Nail Cutter Plier Type blue',
                   '0',
                   '995',
                   'D1E61613-DDD1-4EC7-845E-F4A9344C115B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER SCISSOR TYPE BLACK RED A12',
                   '0',
                   '996',
                   '12370CEA-8590-4E78-98AF-DC9ACE717C5E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NAIL CUTTER SMALL CHARLES PET',
                   '0',
                   '997',
                   '47EB253D-5034-4F06-B7AD-60736F03E0E3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NON SKID BOWL 4OZ XS',
                   '0',
                   '998',
                   'D59BE347-0858-4529-9382-C607BB783F65-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON LEAD LARGE',
                   '0',
                   '999',
                   '3B4EA10A-5F13-4F7C-9BFB-A61DF296E254-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON LEAD MEDIUM',
                   '0',
                   '1000',
                   '73B071C5-5CE2-4A56-9042-CC02F54451B5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON LEAD XL',
                   '0',
                   '1001',
                   '6B167BC7-B18A-4923-BF42-E0681D4C2056-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON TRAINING LEAD LARGE',
                   '0',
                   '1002',
                   'B438D552-4C76-414D-8A37-23A198002DA0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON TRAINING LEAD MEDIUM',
                   '0',
                   '1003',
                   'E08AEA4E-54E4-401C-AA82-C879C6F14ED0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NYLON TRAINING LEAD SMALL',
                   '0',
                   '1004',
                   '5CC489D5-62C7-4778-824D-D5AF44254385-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ORAL CARE TOOTHPASTE GRAPES FLAVOR CHARLES PET',
                   '0',
                   '1005',
                   '79D87EA6-02FC-4FA2-92C2-63C8009271A4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ORDINARY LEASH MEDIUM',
                   '0',
                   '1006',
                   '39F49080-A6EA-469D-80A6-8466A7FC9AC7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PACI CHEW MEDIUM',
                   '0',
                   '1007',
                   '7D6301F2-966A-44F8-A05F-6D03976A48D7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PACI CHEW SMALL',
                   '0',
                   '1008',
                   '5B0A3086-1B09-40EC-BEB8-863C6412B39C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAMPERED POOCH SOAP ANTI BACTERIAL',
                   '0',
                   '1009',
                   'AC8B94E3-44A4-4DA5-A9B0-C7D3EA322914-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAMPERED POOCH SOAP ANTI MANGE',
                   '0',
                   '1010',
                   '6C3402CF-FD8F-4119-B102-939AFCD48ECC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAMPERED POOCH SOAP ANTI TICK AND FLEA',
                   '0',
                   '1011',
                   '17E71710-C1D7-4D11-AD05-A5588437E91D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAWELL ALL IN ONE SHAMPOO',
                   '0',
                   '1012',
                   '55A1B8F4-0870-4164-B45D-95DDA51928A5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAWELL ORGANIC SPRAY',
                   '0',
                   '1013',
                   'F2586E68-A185-4455-B929-6381CEC4B96B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PAWELL PET COLOGNE',
                   '0',
                   '1014',
                   'BD1D2CC0-049D-437A-AE55-1EF024595368-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PERFECT CHUM SHAMPOO GALLON',
                   '0',
                   '1015',
                   '18420D96-25D8-4777-AEEB-98951EA8563D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET DOG COTTON CLOTHES 2XL',
                   '0',
                   '1016',
                   'EF1327C1-2ACC-4768-A7D1-E22837D2C3FC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET DOG COTTON CLOTHES XL',
                   '0',
                   '1017',
                   '9D5F9D25-445E-4901-92B5-E115B87C99AB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET E. COLLAR #3',
                   '0',
                   '1018',
                   'A0DE5F86-0B62-45A6-AFDE-52855656A54F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET E. COLLAR #4',
                   '0',
                   '1019',
                   '0B663A89-0529-4372-B345-AAB51B564D22-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET E. COLLAR #5 COLORED',
                   '0',
                   '1020',
                   '23B2B80B-820A-4713-90FB-251A2E42AC30-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET E. COLLAR #6',
                   '0',
                   '1021',
                   '66771BDD-A829-4FEF-9621-0A5ADE348D1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET RAINBOW COLOR JUMPER',
                   '0',
                   '1022',
                   '191A40A8-B4B2-44A0-B72E-D40A698163FD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET SUN GLASSES',
                   '0',
                   '1023',
                   '39BFCA1C-93B6-4EF1-B441-DE576C611F0B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#1 CHARLES PET',
                   '0',
                   '1024',
                   'BBEAED25-C45F-41FF-AE4A-70BD69A223FD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#2 CHARLES PET',
                   '0',
                   '1025',
                   'F8FF7D7C-6231-4766-82FF-CB104F2567DA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#3 CHARLES PET',
                   '0',
                   '1026',
                   '35872A96-D730-482E-ADDC-021517DB47D1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#4 CHARLES PET',
                   '0',
                   '1027',
                   'E8AB075C-8F78-4536-AF76-7B4F8CD5205F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#5 CHARLES PET',
                   '0',
                   '1028',
                   'F41BCB6B-05D6-45CB-BED8-E92D21CA8EFB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#6 CHARLES PET',
                   '0',
                   '1029',
                   '52E609FC-300C-4470-8F00-42B7752655DA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLASTIC BUSAL#7 CHARLES PET',
                   '0',
                   '1030',
                   '07A38CCA-1E37-4323-9B62-311387ED4572-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS COLOGNE ROSE BONBON',
                   '0',
                   '1031',
                   'D7FFC304-2EE2-48E6-89FC-FE2CC4F71225-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS PET SPLASH COTTON FRESH 250ML',
                   '0',
                   '1032',
                   'A80770AA-544F-41DF-9174-017EF3BD0DD6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS SHAMPOO 1L ANTI TICK AND FLEA',
                   '0',
                   '1033',
                   '9FC55D9F-1335-446C-94B9-15EB22CB5A32-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS SHAMPOO 1L DETANGLING CONDITIONER',
                   '0',
                   '1034',
                   '28C30DB1-BE80-4F71-98E6-09EC2E92AD29-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS SHAMPOO 1L LAVANDER BREEZE SCENT',
                   '0',
                   '1035',
                   'AF01FD30-A996-43F5-B1DA-2EDAFBC56913-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PLAYPETS VCO FOR PETS',
                   '0',
                   '1036',
                   '034F745C-382B-4606-8EC8-A5D9F4C27B32-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'POLNET TRANSPARENT COLLAR',
                   '0',
                   '1037',
                   '4171891F-ED12-4C31-81B5-06BA01BDA081-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PRONG COLLAR 4.0',
                   '0',
                   '1038',
                   'A3CB7821-B8B3-4811-9D7A-57DAD9026811-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RING BELL ASSORTED COLOR CHARLES PET',
                   '0',
                   '1039',
                   '380F8A2A-B06B-4289-9EE2-F03D99D2A18F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROWATINEX CAPSULE',
                   '0',
                   '1040',
                   'E224932E-FEB8-463E-AFFC-55E897BCDFC1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RUBBER BATH COMB CHARLES PET',
                   '0',
                   '1041',
                   '4A120CFD-BF27-4A44-8210-44CDB8CBE967-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SANDO LARGE',
                   '0',
                   '1042',
                   'CF964290-DD84-420A-957A-AA25F4F7C379-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SANDO MEDIUM',
                   '0',
                   '1043',
                   '9699DDBB-8FCC-4F9D-9593-9ED0D5FC21D1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SANDO SMALL',
                   '0',
                   '1044',
                   '0F0922F7-E0FC-444B-ABA7-E826D50D839B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SANDO XL',
                   '0',
                   '1045',
                   '9BB0C183-7F2F-4144-95BB-0762C53E9270-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SHOPEE DINOSOUR PINK',
                   '0',
                   '1046',
                   'B5F076A1-3B3C-4AE0-995B-283B1AF73675-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SHOPEE SANDO',
                   '0',
                   '1047',
                   '7E54D74F-C51B-405E-BEEE-58D9EF6B19D5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SHOPEE TIGER',
                   '0',
                   '1048',
                   '5D909022-5813-45E8-B7D6-236F72C40BCE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SHOPPE FLUPPY DRESS',
                   '0',
                   '1049',
                   'E224639C-3E26-433B-9207-20EFE100F0B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SHOPPEE TOYS FOR DOGS',
                   '0',
                   '1050',
                   'F898E3AC-C2C0-458A-B352-DD4E9C43E1EA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SLIPPER TOYS PLUSH',
                   '0',
                   '1051',
                   'F0AD5F68-4606-4538-827F-B739627E9D91-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SOFT BED MEDIUM',
                   '0',
                   '1052',
                   '95C3C3D0-EB1E-4D39-9F81-F0AE18306731-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPIKEY BALL (Y.GREEN PINK BLUE)',
                   '0',
                   '1053',
                   'C552F79E-18EB-4EE6-B350-1C4061C7C377-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPIKEY BOMB (Y.GREEN, BLUE, PINK)',
                   '0',
                   '1054',
                   '6B6D88E1-5B4C-435F-A452-5B09D236B69F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SQUEAKY BONE FLAT COLORFUL',
                   '0',
                   '1055',
                   'E001B34F-EBBB-4FED-90E0-E0D687CA4490-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SQUEAKY EMOJI TOY COOL',
                   '0',
                   '1056',
                   '64AEB950-9965-4C9E-91AA-2660CE090B9D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SQUEAKY EMOJI TOY POOP',
                   '0',
                   '1057',
                   '2FC63942-AB4F-44D4-8DE4-0C34AA752617-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST ROCHE HOOMAN SOAP HAPPINESS (100G)',
                   '0',
                   '1058',
                   '6DA92A56-10D8-41CD-A08E-02AD5E4B74CC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE BASIC DOG SHAMPOO PINK BLOOSOM 1000ML',
                   '0',
                   '1059',
                   '80399E4F-80A3-484C-BFBD-DF33433CD696-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE BASIC DOG SHAMPOO SWEET LOVE 1000ML',
                   '0',
                   '1060',
                   '8DC2F249-C702-4162-A395-BCC8A497B8BA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HAPPINESS HOOMAN CONDITIONER',
                   '0',
                   '1061',
                   'A44D8BAF-49A4-4B41-881E-67869389914D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HEAVEN SCENT HOOMAN CONDITIONER 198ML',
                   '0',
                   '1062',
                   '340CF70B-A334-4A70-B4CD-7D51E6254542-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HOOMAN SHAMPOO SWEET EMBRACE 538ML',
                   '0',
                   '1063',
                   'E0588D97-6C2E-452C-81BC-676277F0F5DF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HOOMAN SOAP HEAVEN SCENT (100G)',
                   '0',
                   '1064',
                   '2A67447E-AC2F-4C59-B736-3ED516484E9E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HOOMAN SOAP PEACE (100G)',
                   '0',
                   '1065',
                   '1DB1A86A-DFB5-4CE7-A719-AC6EAFF60A72-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE HOOMAN SOAP SWEET EMBRACE (100G)',
                   '0',
                   '1066',
                   'E89D4D82-554D-45D4-8977-8CBE1DF907FF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE SHAMPOO MOTHER NATURE 1L',
                   '0',
                   '1067',
                   '03D26527-BEA0-43BF-9F5E-A07B2DDF0BB6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE SHAMPOO MOTHER NATURE 250ML',
                   '0',
                   '1068',
                   '13346620-978E-4902-9C43-77DE6F47EFDF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ST. ROCHE SWEET EMBRACE HOOMAN CONDITIONER 198ML',
                   '0',
                   '1069',
                   '06738EE5-0050-4EF6-9AEF-03E872E0F8CB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SUNFLOWER E. COLLAR LARGE',
                   '0',
                   '1070',
                   '388EC636-7DC8-4D61-B0FA-657C7653D0BD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SUNFLOWER E. COLLAR MEDIUM',
                   '0',
                   '1071',
                   'FC7FDAD3-EED7-4582-B69E-BE2E6E667024-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SUNFLOWER E. COLLAR SMALL',
                   '0',
                   '1072',
                   'BB66C27A-7842-417A-8CA9-443336D47EAE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TIMELESS CORNER LITTER PAN LARGE HEART',
                   '0',
                   '1073',
                   'C1A024B6-BFB9-471C-839E-F31B111F830D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TIMELESS LITTER PAN ROUND SMALL',
                   '0',
                   '1074',
                   'DCA033A6-4E6B-4EB5-9A40-DE7C8BF9DC24-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOOTH BRUSH AND TOOTHPASTE 75G',
                   '0',
                   '1075',
                   '468DEBB8-65C8-458E-8F97-0D99620AEC4D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOOTHBRUSH WITH FINGER',
                   '0',
                   '1076',
                   '6E5B24D5-F966-44E7-A844-5474E6F80ADD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOOTHPASTE BIOLINE',
                   '0',
                   '1077',
                   '42FEBDD9-BE46-4038-B6A2-F2A406350E73-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOP TAILS  SOAP SWEET APPLE',
                   '0',
                   '1078',
                   'B4190F2C-C25C-4234-B621-E2F4AA51581D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOP TAILS SOAP SOFT AND SHINE 150GRMS',
                   '0',
                   '1079',
                   '311FDAE8-0805-49BB-A888-EE93EC1E5A13-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOY CHAIN RING WITH SPIKE',
                   '0',
                   '1080',
                   '1B5B045D-06C6-4608-9D23-1446172DCCBE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOY CHAIN STAR MOON SUN RUBBER',
                   '0',
                   '1081',
                   '62D3DA44-E2C0-4306-925D-149486E0A8DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOY CHAIN TRANSPARENT 3PCS RING SPIKES',
                   '0',
                   '1082',
                   '908C7541-158E-4D99-9495-4C75E97B2989-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOY KONG BALL LARGE JEYCERY',
                   '0',
                   '1083',
                   '6DEE08C1-EDAD-4DDC-8DDE-F16D0B5EF4B0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TOY KONG BALL SMALL',
                   '0',
                   '1084',
                   'AA10D14C-D281-461A-9F8B-16331C19AE0B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TUBE LEASH SLIP TYPE',
                   '0',
                   '1085',
                   'D39E39FA-3A9A-4B94-A6C8-99AAB228B69D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WONDERZYME CITRONELLA SPRAY 50ML',
                   '0',
                   '1086',
                   'C55893C0-B7D5-4B40-AD60-65068442753D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WONDERZYME PET BREATHER SPRAY 50ML',
                   '0',
                   '1087',
                   'B55D8968-9541-4CF0-B08D-6B2CC54C2F8D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WONDERZYME PET SPRAY VANILLA 330ML',
                   '0',
                   '1088',
                   '8FE89B3F-82B2-4479-88B6-07D4C5B6F2C9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WONDERZYME PETZYMESOAP AND SHAMPOO BAR 100G',
                   '0',
                   '1089',
                   '9D5C4E49-654B-4065-B5FA-FA226742D267-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WONDERZYME SOAP MADRE DE CACAO',
                   '0',
                   '1090',
                   '14AA9C5F-7287-4927-8A76-5CACD3483FF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'XO TURTLE STICK 400G',
                   '0',
                   '1091',
                   '5E33AB96-519C-409D-A2FE-4C3F0F6C8435-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BIOPSY',
                   '97',
                   '1092',
                   '66E30DCB-8C23-4155-B1AC-D38D01476631-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BLOOD CHEM LIVER KIDNEY DISC',
                   '0',
                   '1093',
                   'B4B52412-A23B-4AC4-AD79-7435A69907BF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BLUT CDV/CAV TEST KIT',
                   '8',
                   '1094',
                   '73B7E81D-9594-4A6C-9424-070957D7FAD6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BLUT CPV TEST KIT',
                   '0',
                   '1095',
                   'DD197D7F-79CA-4F8E-BFF4-1E683DA44656-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BLUT RAPID CDV TEST KIT',
                   '11',
                   '1096',
                   '111316B3-4ED6-4B5F-BBFD-C696056FCC1D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CBC ONLY',
                   '72',
                   '1097',
                   '5E985763-C609-400D-9ABA-CE38CF8E5D60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 EAR MITE TEST OR EAR SWAB',
                   '39',
                   '1098',
                   'D111D561-19C2-4FF6-BF27-C5761795BE1A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FECAL EXAM FECALYSIS',
                   '93',
                   '1099',
                   '9DBF9BD1-34DA-474A-A48D-5CFD77E266B1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELINE CALICI VIRUS TEST KIT',
                   '15',
                   '1100',
                   'FD3D5B1E-74D4-46C4-8B4E-EBAF8B26E1A1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PRE ANESTHETIC PLUS DISC BLOOD CHEM BC',
                   '68',
                   '1101',
                   '9046CAFF-A05D-4CF3-B128-D1861FFF6035-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PROGESTERONE TEST KIT',
                   '197',
                   '1102',
                   'F6F5ECB3-2BFC-4499-84FA-1B3414235F28-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PROGETERONE ICHROMA 2',
                   '1',
                   '1103',
                   '702BFFA5-C1C2-4E46-8E66-8BB8D5059119-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 QUICKVET 3IN1 CPV CCV GIARDIA',
                   '22',
                   '1104',
                   '449F6165-BAC1-4181-8D74-865783748CC1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 QUICKVET DISTEMPER TEST KIT QUICK VET (CDV) ED ONG',
                   '15',
                   '1105',
                   '41FA1B35-2369-458F-8698-33C4FF111306-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 QUICKVET PARVO TEST KIT',
                   '29',
                   '1106',
                   '7454C2B2-99FB-440F-BFB9-8889F4273382-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ROHI CDV CPV TEST KIT ROHI BIOCHEM',
                   '2',
                   '1107',
                   '3FAD396E-3960-490D-9C58-30BE1010EC3A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SKIN SCRAPE',
                   '81',
                   '1108',
                   '9EAED6E9-B1D8-450A-A583-C459F587B099-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SMEAR',
                   '70',
                   '1109',
                   'E7208042-21BB-4D69-9515-DD80B941280B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SPERM COUNT',
                   '59',
                   '1110',
                   '3C1D57C3-869B-49C9-813F-788EF25F5FA1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TRIVET 30ML',
                   '2',
                   '1111',
                   'D1AF2F9A-570C-4058-BDE4-3E4C37B0FC67-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ULTRASOUND',
                   '61',
                   '1112',
                   '0F0909DF-1FD3-4432-A706-94BC87FD730A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 URINARY TEST STRIP',
                   '115',
                   '1113',
                   'E6D9F4DD-5136-4ED1-8AE7-D0BE3A5BC106-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VEGEBRAND CDV TEST KIT',
                   '1',
                   '1114',
                   '7A427706-7E42-4C95-9D23-2B92E78614E1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX 3WAY CDV+CAV+CIV',
                   '10',
                   '1115',
                   '1A6FB38F-1DBE-4FDC-A936-0A1FEB391F36-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX 3WAY CPV+CCV+GIA',
                   '7',
                   '1116',
                   'EC236E88-3DC3-4270-9DA9-75534FB82849-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX 3WAY EHR + ANA + BAB',
                   '16',
                   '1117',
                   '88FAA640-941C-4986-82F1-84AF6D84E45C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX 3WAY FPV+FCOV+GIA',
                   '5',
                   '1118',
                   '179C6B60-09C8-4A8D-A490-FAAED4BD9D0F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX ASIAN4 ANA+EHR+BAB+HW',
                   '9',
                   '1119',
                   '51831A73-3CD4-46EE-BA6C-D42238F3816C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX CAT4WAY FCV+FHV+FCOV+FPV',
                   '10',
                   '1120',
                   '3421EF2C-3C15-4237-87E8-081B7D993FDF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX CDV AG',
                   '28',
                   '1121',
                   '6AACF0E6-CE9D-4787-AE02-58F32D7CDC29-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX CPL',
                   '10',
                   '1122',
                   '1FE08639-0027-4A77-B834-86E0260EA8FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX CPV TEST KIT',
                   '49',
                   '1123',
                   '15AE0F9A-3428-4D5B-8DF4-4B37A63773B7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX CRYPTO',
                   '9',
                   '1124',
                   '1CE78F1B-D81C-430F-AE5D-F91E403BC1E9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX HW AG',
                   '33',
                   '1125',
                   'C4DC0E30-3B33-4B04-878E-F2993776ECE5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX LAZYKIT CDV AG+CDV AB',
                   '7',
                   '1126',
                   'A1DAB547-8A66-44CB-97D7-EB9404E96208-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX LAZYKIT CPV AG+CPV AB',
                   '8',
                   '1127',
                   '4DBC04D8-B293-4C53-815F-CB28D0FBFCD3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VET DX LEPTO TEXT KIT',
                   '10',
                   '1128',
                   'AADA108A-668A-4F5E-813C-127E05CCBEAF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZRBIO EHRLICHIA AB TEST KIT EVR',
                   '4',
                   '1129',
                   '2190B2B5-72E9-428F-82E0-698007310315-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BIOGUARD 4IN1',
                   '0',
                   '1130',
                   '5C79B2D8-3777-454F-BC19-1ABAA4FB3ED2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BIOTECH PROGESTERONE TEST KIT',
                   '0',
                   '1131',
                   '5990A244-EB1B-4FBD-8CC8-277B1F879C60-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BLOOD CHEMISTRY POS BUT INPUT .0',
                   '0',
                   '1132',
                   'CB61D09C-014C-49D2-8572-ECA1A7C2A486-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CANINE HEARTWORM (CHW AG) TEST KIT',
                   '0',
                   '1133',
                   'B89B27C0-0AC5-4024-BF16-E9C6D1C598D5-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CPV/CCV',
                   '0',
                   '1134',
                   '7115C765-9000-454E-B2FF-35705FAFDC01-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DISTEMPER TEST KIT',
                   '0',
                   '1135',
                   '3037DF5C-0064-471B-9D43-E8490CA3B1B0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FELINE PANLEUKOPENIA (PARVO) TEST KIT',
                   '0',
                   '1136',
                   'DDEE4088-6097-4650-ACB4-519B27F635B2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FPV QUICKVET TEST KIT',
                   '0',
                   '1137',
                   '6963E95A-BFE2-40E4-8BE2-05B65484FA12-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PARVO + CORONA TEST KIT 5200P',
                   '0',
                   '1138',
                   '8EEC55B0-F29E-4E81-BA1D-E933D8F0A57F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PROGESTERONE TEST KIT FINE TECH',
                   '0',
                   '1139',
                   '8DCFBA43-06C0-4918-ACDF-89FFF2093455-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'QUICKVET 2IN1 CPV/CDV TEST KIT',
                   '0',
                   '1140',
                   '72717C54-B16E-4D2F-BE5E-78B96EA84F61-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'QUICKVET 3IN1 CPV/CDV/GIARDIA TEST KIT',
                   '0',
                   '1141',
                   '813F23A9-1C5C-4CC0-9A87-D1C58B188ADD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'QUICKVET EHRLICHIA TEST KIT (ED ONG)',
                   '0',
                   '1142',
                   'DA7C022A-A560-4D0E-B878-BE4D7E296C64-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'QUICKVET LEPTOSPIRA TEST KIT',
                   '0',
                   '1143',
                   'CEE7C662-C000-4D39-AF0E-491B5D5DA98B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RELAXIN PREGNANCY TEST',
                   '0',
                   '1144',
                   'D9B36226-19D7-4D12-9563-DF2FD35C67F9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROHI CPV CCV GARDIA TEST KIT 3IN1',
                   '0',
                   '1145',
                   '71A28F8C-1D0A-471A-B563-D253208FB989-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROHI CPV CDV TEST KIT 2IN1',
                   '0',
                   '1146',
                   'A064E26A-7417-4B79-B8AE-3BD970AEC721-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROHI EHR ANNA CHW TEST KIT 3IN1',
                   '0',
                   '1147',
                   '1AC873EE-60D4-4A0C-B6DF-56E618A23170-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROTOR (NEW PLEASE EDIT) BLOOD CHEM',
                   '0',
                   '1148',
                   '0D2CA166-8B28-4A79-B20F-5BB678B841A1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SEINOFY CHW-EHR-ANA 3IN1 TEST',
                   '0',
                   '1149',
                   'F1BEC9CB-A420-479C-8F83-DFFEB4D2C9B0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SEINOFY CPV TEST',
                   '0',
                   '1150',
                   'BDA46870-29E1-48CE-A8B2-EF4853D24C86-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SEINOFY CPV-CCV-GIA 3IN1 TEST',
                   '0',
                   '1151',
                   '5BDA2D1E-0A1A-4BAE-9698-F8E428DB8F1C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VEGEBRAND CPV TEST KIT',
                   '0',
                   '1152',
                   '679C5951-0FB5-4517-B8E2-EFB813415CE7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET DX CPV+CCV+GIAR+CRV AG 4IN1 TEST',
                   '0',
                   '1153',
                   '0B2D2905-39C1-46D5-92B9-97AD8918C78C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VET DX LIPGRADE 4WAY CPIV',
                   '0',
                   '1154',
                   '46313547-3517-43DC-8559-4CC141446839-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VIRBAC OVULATION TEST',
                   '0',
                   '1155',
                   '8B8288B9-EE19-4F8C-83B0-5BBD38B3C4DD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VIRBAC SPEED DISTEMPER TEST KIT NORDEP',
                   '0',
                   '1156',
                   'D7CB10DF-5A7F-4E45-AC27-012EFB35A567-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BALL W/ DOG FACE',
                   '0',
                   '1157',
                   'A2CE2765-9498-432B-BAC1-0442C244483F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BALL W/ DOG FACE',
                   '0',
                   '1158',
                   'CBA27A78-1A26-440A-93BA-8D6CF9D958FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHICKEN SQUAEKY BIG',
                   '0',
                   '1159',
                   '37ADB4A2-E523-4AFB-8221-8EB02BCE31A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHICKEN SQUAEKY SMALL',
                   '0',
                   '1160',
                   '09B2F372-697A-450E-BFD7-B1165FDDC1BB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DUCKLING FACE TOY',
                   '0',
                   '1161',
                   '215DCCA3-D32F-44D4-AEEC-5B5D71BEDF15-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SPIKEY FLAT BONE',
                   '0',
                   '1162',
                   '935A86E8-9B61-48B5-BA1A-56466EC7FBC2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Stuffless Chix -S 15071PD',
                   '0',
                   '1163',
                   'C16FC24F-CCD6-4BBF-94D7-7F8ECCFE1790-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Stuffless Squirrel -L 15078PD',
                   '0',
                   '1164',
                   '382F236E-D653-430B-AD6A-7B5FF9E521BE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WEE WEE 3-SIDES TRAINING TRAY',
                   '0',
                   '1165',
                   '36E5747E-B78A-4CC1-99D8-30CF19B6E4E9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'WEE WEE TRAINING TRAY',
                   '0',
                   '1166',
                   'AE3BE86D-735A-429D-B2F5-20C6BDD7411B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '1KL TREATS',
                   '0',
                   '1167',
                   '942836B9-E273-4C3B-89F7-01B9456B058B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BEARING JERKY TREATS',
                   '15',
                   '1168',
                   '75595A30-44DB-4FE0-982A-8525982EC446-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHEWBONE BLEACHED 6IN  PET PLUS',
                   '8',
                   '1169',
                   'CBAA90E8-77B2-45F8-BEEF-DDD0FD122F93-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHEWBONE BLEACHED 8IN  PET PLUS',
                   '15',
                   '1170',
                   '33CD35C2-FE45-4E2F-8855-7003AB95366C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHEWY TREATS CANINE 2PCS',
                   '0',
                   '1171',
                   '9EAD64AF-66F7-4BBA-B263-5D43CE364B66-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHEWY TREATS CANINE 6PCS',
                   '0',
                   '1172',
                   '42DEB463-C482-4163-9FAD-4674ECDC3F10-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CHEWY TREATS MUNCH ROLL',
                   '0',
                   '1173',
                   '76E3767E-BB41-4800-A1EC-1B73110DCB78-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GOAT MILK SOFT DICE TREAT 180G',
                   '37',
                   '1174',
                   '21AEB47B-C232-419B-B31B-854F9B19831D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GRAIN-FREE ROUND PUFFED STICK',
                   '25',
                   '1175',
                   '658C3B23-549C-406B-80F8-DA23246C8AC6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOW BONE BEEF 15S 270G',
                   '11',
                   '1176',
                   'D0F880AE-CAA0-4D4C-AA92-AD16CE250DF7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOW BONE BEEF 30S 270G',
                   '16',
                   '1177',
                   'CD42BEFE-370C-4D1B-BF54-BB10DFB4852F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOW BONE DENTAL STICK JUST PURE CHICKEN FLAVOURED  75G',
                   '49',
                   '1178',
                   '37353593-1469-491D-9A85-E3371A569635-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOW BONE MILK 15S 2270G',
                   '9',
                   '1179',
                   '08B7EAE9-FFBA-4213-98E2-7AB9D902CC7A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HOW BONE MILK 30S 270G',
                   '14',
                   '1180',
                   '4F81523B-A7FB-441A-AE29-BE0B82335707-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH DEN T STICKS BACON 70G',
                   '36',
                   '1181',
                   'A3F7F6B6-42C4-45FE-93FA-3B4AFAA0DBFF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH DEN T STICKS MILK 70GMS',
                   '44',
                   '1182',
                   'B4EED350-A8F9-41C6-9937-9A7FD11637E6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH POUCH  BEEF GRILLED AND CARROT IN GRAVY',
                   '50',
                   '1183',
                   'B0329EF5-88DA-487A-8C07-8C37A1A5C990-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH POUCH  CHICKEN GRILLED  IN GRAVY',
                   '63',
                   '1184',
                   'CBFDAF1B-6C96-4A88-ABE7-7AF2F879F38B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH POUCH  ROAST DUCK IN GRAVY',
                   '53',
                   '1185',
                   '199C4588-49F5-432A-8474-38C8FC157AA1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH POUCH CHICKEN AND LIVER IN GRAVY',
                   '52',
                   '1186',
                   '119174E2-E24A-4296-93D4-1A5DF252177E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH POUCH CHICKEN AND VEGE IN GRAVY',
                   '51',
                   '1187',
                   '167E20DE-4E02-4AE9-A948-89709C522B36-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (BEEF) ..  (12PKS/BOX)',
                   '0',
                   '1188',
                   'E7A4AA6C-4869-4D98-B208-6B6E27C955EC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (BLUEBERRY) ..  (12PKS/BOX)',
                   '14',
                   '1189',
                   '4C8A33CE-A7EE-442C-8313-9925601C4CAF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (CARROT) ..  (12PKS/BOX)',
                   '18',
                   '1190',
                   '49EE60BB-A9F0-40EC-AA79-344D452F66E4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (CHICKEN JERKY) ..  (12PKS/BOX)',
                   '6',
                   '1191',
                   'FD46788A-416B-48DC-83F2-5DB1F4AD9E31-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (LIVER) STICK',
                   '24',
                   '1192',
                   'F46B5BFE-B873-4EC1-BFA2-0DF5AAC8E002-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (MILKY) .. (12PKS/BOX)',
                   '20',
                   '1193',
                   '8121D5CC-B877-4C3C-98AE-77B0F8D61899-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (SNACK BACON) .. (12PKS/BOX)',
                   '0',
                   '1194',
                   '9B868C02-C1DD-4205-A70A-EE0BE00450DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (SPINACH) ..  (12PKS/BOX)',
                   '33',
                   '1195',
                   'CC1FE2E3-9FD0-49FC-B544-952C0F2273AA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 JERHIGH TREATS (STRAWBERRY) ..  (12PKS/BOX)',
                   '1',
                   '1196',
                   '8DCFB93C-633A-42C3-94EA-5AD7D991BC1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET SHINE DOUBLE COLORSTAR ROLL STRAWBERY 1KG',
                   '0',
                   '1197',
                   'F9F45FC8-17E6-43C8-BD92-748D869438A1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET SHINE FILLING MILK STRAWBERRY 1KG',
                   '0',
                   '1198',
                   'EFF00D07-F9D6-48FA-8BAD-BFC771AD0D66-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET SNACKS TUNA CHICKEN LIQUID 15G',
                   '107',
                   '1199',
                   'B76D6E06-37AB-40BA-B6F5-A71D53C6847A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 YAHO TREATS IN JAR STEAMED BUN 200G',
                   '0',
                   '1200',
                   '40AFE0B8-8490-461D-B380-F1C1F0E744BC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 YUFENG BISCUIT CHICKEN',
                   '18',
                   '1201',
                   '3E1CCE23-A0C4-4073-A7D5-108E9A89F21B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 YUFENG BISCUIT FILLED WITH BEEF',
                   '15',
                   '1202',
                   '6D3C9F3E-F21B-4D7B-BF7C-0FBA7CABCDDA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 YUFENG BISCUIT MILK FLAVOR',
                   '16',
                   '1203',
                   '611C556C-1505-44BB-8EEF-5A4D54DBC29C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 YUFENG BISCUIT STRAWBERRY',
                   '21',
                   '1204',
                   '15D96FE6-1680-4DA3-BAB6-D42304D58167-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE BLUEBERRY CHEESCAKE',
                   '3',
                   '1205',
                   'DAFAB542-0388-43C1-8C46-33F8D31B9EA4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE CHEESE GELATO',
                   '11',
                   '1206',
                   '0EAEE2E2-65AE-4F3C-9668-8F5F5D5BF825-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE MANGO CHEESECAKE',
                   '14',
                   '1207',
                   '4BC51E18-D3EB-464C-A434-0F04BAB7A30F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE PISTACIO GELATO',
                   '22',
                   '1208',
                   '6D8728F1-654C-4BB4-9930-4994B2E0A28A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE STRAWBERRY GELATO',
                   '8',
                   '1209',
                   '68DFEDBD-D238-4A03-B63D-7818070A1224-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ZERT - DOG DENTACARE STRAWBERRY SHORT CAKE',
                   '14',
                   '1210',
                   'CB38163B-E01A-4A63-AAF2-3506D73C0F06-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BONE KNOTT CHEESE',
                   '0',
                   '1211',
                   'C0C03E06-A843-45EE-A5F7-5FA64E05F7A6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHEW TOY K9 BALL',
                   '0',
                   '1212',
                   '2AF13A29-E7D7-48D5-85D1-B839AB78A6D3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CHEWBONE REGULAR 8IN  PET PLUS',
                   '0',
                   '1213',
                   '281F0D54-ABD9-4DEB-B9FC-A89F3F9F4FAC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DELICIO TREATS SALMON 70G',
                   '0',
                   '1214',
                   'E5D34BEB-2777-4AD1-A2AF-066E7C1466AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DENTAL EFFECT STICK 170G',
                   '0',
                   '1215',
                   '85CC3432-4C78-4338-B7D1-A8263641350C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DENTAL EFFECTS 60G',
                   '0',
                   '1216',
                   '0EACDE99-CBA0-4688-B0FE-9E573007E33F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DENTAL EFFECTS MILK 160G',
                   '0',
                   '1217',
                   '0E46890E-64DD-4A7D-9CB3-A9DCB0E49E7E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'DENTAL PURE STAR 75G/PACK 12 PC/BOX',
                   '0',
                   '1218',
                   '34B6A7BB-385D-412B-B0A5-57EF68DEF985-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ENERGY TREATS GOODIES',
                   '0',
                   '1219',
                   '10B25056-2BD7-43F1-AD17-04E4D787CC7F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FRENCH FRIES RAWHIDE 500G',
                   '0',
                   '1220',
                   '27BE3F04-DEC6-4D4F-8415-8BE59E37F9CC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GNAWLER DENTAL CARE 3INCH - BACON 6PC/PK..',
                   '0',
                   '1221',
                   '2F757312-FD2C-4351-886C-CD985DD73487-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GNAWLER DENTAL CARE 3INCH - BEEF 6PC/PK..',
                   '0',
                   '1222',
                   'FC9594B7-1941-40D4-9040-BAE368CB822F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Gnawler Dental Care 3inch - Chicken 6pc/pk..',
                   '0',
                   '1223',
                   'D46F8E79-9755-4478-8D8A-AE37A6F28BA3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GNAWLER DENTAL CARE 4.5INCH - BACON 2PC/PK..',
                   '0',
                   '1224',
                   '1DACC890-683A-4DBE-9BC1-78DBCE65358F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GNAWLER DENTAL CARE 4.5INCH - BEEF 2PC/PK..',
                   '0',
                   '1225',
                   '9DEF1CF4-E3FF-4CFE-A4B8-603FFACB3A98-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Gnawler Dental Care 4.5inch - Chicken 2pc/pk..',
                   '0',
                   '1226',
                   '0B2E21AD-9984-472E-B892-D5FDE5A6E4AF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Goat Milk & pumpkin twist sticks 80g.',
                   '0',
                   '1227',
                   '5A16557A-B977-46E5-8092-76FFF02D6E91-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Goat Milk & puple sweet potato twist sticks 80g.',
                   '0',
                   '1228',
                   '3A1D03A9-6F37-48D2-B1E9-AE82149A479A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Goat Milk Twist & blueberry twist sticks 80g.',
                   '0',
                   '1229',
                   '4F49FD46-1F52-49E6-81DB-132587DA0A4B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HEALTHY BISCUIT',
                   '0',
                   '1230',
                   '89EE005D-7F67-4FFC-BEBF-6C636523F4DE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOLISTIC TREATS',
                   '0',
                   '1231',
                   'B01930BC-0C97-4E69-AF60-CC49385730C8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW BONE 2INCH MIX FLAVOR 30PC/PACK 270G.',
                   '0',
                   '1232',
                   'EBA52F61-4DBC-4DDF-8EEB-C94E298DF236-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW BONE JUST PURE MEAT AND MILK (BONE) 75G',
                   '0',
                   '1233',
                   'CFE88EA4-CB42-4D5E-A0F6-E70D2A485EB6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW BONE JUST PURE MIX FRUITS (STICK) 75G',
                   '0',
                   '1234',
                   'CD1A1F9D-A995-4D10-91E7-EFA95B6CC999-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW BONE JUST PURE MIX MEATY (STAR TREATS) 75G',
                   '0',
                   '1235',
                   '80A4BA8E-23C5-416B-9125-4965AF0EEEE8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW BONE MIX FLAVOR 15PC/PACK 270G.',
                   '0',
                   '1236',
                   '5038D737-D066-4DC8-BF22-0E02FF4F1E70-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW CHEWY GREEN ORAL CARE',
                   '0',
                   '1237',
                   'A0C33A47-2C14-457F-8A54-386F54BCCC63-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW CHEWY ORANGE LOW ALLERGY 70G',
                   '0',
                   '1238',
                   '51944FFE-FF91-4E72-897D-F783C22B0EAD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HOW CHEWY PINK HEALTHY DIGESTION 70G',
                   '0',
                   '1239',
                   '0DA6DB8B-A083-475F-807A-A676723A95CB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH BIG PACK 420G LIVER',
                   '0',
                   '1240',
                   'EC3903F4-4E25-49D4-A0A7-E29A97E1624B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH BIG PACK MILKY 420G',
                   '0',
                   '1241',
                   '717F467A-9638-4B44-939D-4D05FE719964-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH BIG PACK STRAWBERRY 420G',
                   '0',
                   '1242',
                   'EEC9A89D-E86C-471D-90F8-E791A2C78B7A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH DEN T STICKS TUNA 70G',
                   '0',
                   '1243',
                   'C41D909B-3126-4B3B-9BBA-F936FE219621-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH DUO MILKY STRAWBERRY STICK 50G',
                   '0',
                   '1244',
                   'E42D389D-19F1-493D-BC65-8018A1A941A3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH STRIP',
                   '0',
                   '1245',
                   '0231CACC-39AE-45D0-9C04-DB9252A73714-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Jerhigh Treats (Banana) ..  (12pks/box)',
                   '0',
                   '1246',
                   '42A44FD7-AC09-4D3E-A14B-3B9DF2E535DF-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH TREATS (COOKIE) .. (12PKS/BOX)',
                   '0',
                   '1247',
                   'D5F2E0EF-BF13-400C-9C70-44CA8F09B4F4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH TREATS (STICK)..  (12PKS/BOX)',
                   '0',
                   '1248',
                   'BE0A4F46-6CA4-4D4A-9E7B-DB7302E97864-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERHIGH TREATS (STRIP) ..  (12PKS/BOX)',
                   '0',
                   '1249',
                   '2D4F43DA-2756-4D29-892F-81D4A022334F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'JERKY (BEESION)',
                   '0',
                   '1250',
                   '207DCDF7-26B9-49D6-9FEE-AB2876E43DFD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PACK N PRIDE DOG TREAT JTC',
                   '0',
                   '1251',
                   '2766391A-11B7-496E-8BC0-BF9F22E3571F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PET SNACKS TUNA SHRIMP LIQUIDC15G',
                   '97',
                   '1252',
                   'E239A1CA-9675-4D1F-9E9F-DF9ABDDA1C3D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RAWHIDE FREE CHEESE CHEWBONE 4INCH PETPLUS',
                   '0',
                   '1253',
                   'BF44ADAD-4789-42BC-85C8-312502BB98FA-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SMART BONE 3 LARGE',
                   '0',
                   '1254',
                   'AF25EE63-BD4C-47B5-8245-D7BF3CC56942-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SMART BONE 4MEDIUM',
                   '0',
                   '1255',
                   '4F8731B0-A867-4DBB-81D4-ADCEBA99B4F4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SMART BONE 8MINI',
                   '0',
                   '1256',
                   '4F5B689E-AE0F-454E-8AED-ED5E586AF1C7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'TREATS REPACKED PER KG 380',
                   '0',
                   '1257',
                   '86375711-5C1B-49F8-A757-11671A384581-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRONCHICINE KENNEL COUGH KC',
                   '141',
                   '1258',
                   '6CB4088A-D6D1-4FE2-9245-DC046B9E5F0F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DEFENSOR ARV',
                   '0',
                   '1259',
                   'B3F41871-A77C-4382-AB77-02D1F49CF593-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 FELOCELL CAT VACCINE RHINO-CALICI-PANLEUKO-CHLAMYDIA 4IN1',
                   '67',
                   '1260',
                   'DBC1C453-60D7-4E17-B2B6-2CC7D0554182-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NOBIVAC 2IN1 DP (DISTEMPER PARVO)',
                   '44',
                   '1261',
                   'CF390B89-8B88-4A7A-93F9-62B3325BDD6D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NOBIVAC KENNEL COUGH',
                   '21',
                   '1262',
                   'BB49B75E-B03D-47CA-8867-D5A6B431BFDC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NOBIVAC L4 8IN1',
                   '82',
                   '1263',
                   '29DCF3A3-CFAE-43A4-9D73-4D10318292A8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RABISIN ARV',
                   '303',
                   '1264',
                   '4BBBE691-161A-494A-B184-C1236BA34FCC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RECOMBITEK C6/CV 6IN1',
                   '122',
                   '1265',
                   'A727EFA3-A861-4FAE-BB71-C1274EFD4830-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VANGUARD 5IN1 EDGEL VET SUPPLIES',
                   '173',
                   '1266',
                   '2F28D047-098C-4E21-B419-F4AB70425A94-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VANGUARD 6IN1',
                   '59',
                   '1267',
                   '4EC75015-75C8-4335-BFE8-6216D72345B3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'BIOCAN R INJECTION ARV',
                   '0',
                   '1268',
                   '2368BD4A-4C9F-4EC5-89D7-AD82C6FCC24C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIPRADOG 5IN1',
                   '0',
                   '1269',
                   '33D0F23E-66EC-486D-8FAC-35DCB9B3AF45-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PNEUMODOG KENNEL COUGH KC',
                   '0',
                   '1270',
                   'E4604273-782B-49C8-BC03-29A305E93B3C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PUREVAX FELINE',
                   '0',
                   '1271',
                   '08308383-6399-45F9-B22E-336608D367FE-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'QUANTUM 6IN1',
                   '0',
                   '1272',
                   '398B4A7E-601D-40CF-A93A-A08045E34E67-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'RECOMBITEK 5IN1',
                   '0',
                   '1273',
                   '142F1845-60EE-4FD1-AE62-D97733AED0C8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VANGUARD 5IN1 PET WELLNESS',
                   '0',
                   '1274',
                   'D0610544-4583-476E-A5D4-DE54DDBD89FB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'VANGUARD 5L4 8IN1',
                   '0',
                   '1275',
                   '54E7CD0F-45A0-4612-8A8F-6B7AC2FB8DEC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BREWERS YEAST NUTRIVET',
                   '11',
                   '1276',
                   '942A563E-1D52-4ADB-979F-1BCB024BD747-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 BRONCURE',
                   '29',
                   '1277',
                   'F7B6BF1A-A32E-4E6E-8DBF-EC1C572FB63B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 CALCIUBEST 100 TABS',
                   '23',
                   '1278',
                   '6F318836-EB8F-4F95-8FE2-E4BEE1DE09B1-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COAT SHINE SYRUP 120ML',
                   '16',
                   '1279',
                   'D9C06C26-620C-4003-9F1A-F2579B90FCF9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COAT SHINE SYRUP 60ML',
                   '43',
                   '1280',
                   'A880DABD-B705-4092-B5F3-4D6811BD41BB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 COSI MILK PETS MILK 1L',
                   '48',
                   '1281',
                   '48778FFA-D636-409D-87E1-1D15CE189896-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 DELTACAL 50 TABS.',
                   '18',
                   '1282',
                   '12AA88F4-D966-4DCE-B2E7-E7454D316B6B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 EMERPLEX (VIT B COMPLEX) 120ML',
                   '45',
                   '1283',
                   '1AC91059-F772-4A8B-90BC-A40A161C3B8C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ENFABOOST 120ML',
                   '28',
                   '1284',
                   'E2245542-7FBF-493E-A738-418D82522B38-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ENMALAC 120ML',
                   '31',
                   '1285',
                   '9D7C82C1-0AAC-459B-ADCD-372020D57F43-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ESBILAC BIG',
                   '30',
                   '1286',
                   'F2D8FE39-FB93-4AB9-84F7-ACAF3A1B4E7A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 ESBILAC POWDER 12OZ',
                   '0',
                   '1287',
                   '6289F3B4-F974-49FB-842B-198240D98418-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GLUCOSAMINE PLUS AS NUTRIVET',
                   '11',
                   '1288',
                   'DDB49684-7CC5-43DE-A809-08C8ED18C8AD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 GOATS MILK POWDER 300G.',
                   '31',
                   '1289',
                   '9343D096-22E5-4D89-9036-9FC69983F2A6-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 HEMACARE-FE 120ML',
                   '30',
                   '1290',
                   '73B7DD93-D0C2-49BC-A910-C0980EACC4BB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IMMUNO BOOST 60 TAB',
                   '0',
                   '1291',
                   '248AF10D-1489-41EF-AEEB-92B57676C468-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 IMMUNO BOOST SYRUP 100ML',
                   '3',
                   '1292',
                   '8ED5C05F-FFEC-4629-BAD3-4524BBDF50CD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC CAL 60ML',
                   '0',
                   '1293',
                   '82792C27-1764-4CBB-82BB-2F900CF40103-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC SCOUR 60ML',
                   '0',
                   '1294',
                   'F7A08EC3-50D5-4901-A270-09FAE61BE11C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC-VIT OB',
                   '29',
                   '1295',
                   '757915CE-D697-4E32-B7EE-62D852DB9C9B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC-VIT SYRUP 120ML',
                   '26',
                   '1296',
                   '1EA1C341-DF40-46AE-AA0C-3DB50DAEF35C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LC-VIT SYRUP 60ML',
                   '78',
                   '1297',
                   '93C0B3E8-11BA-48FF-9854-6D07AEEF2518-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LIVOTINE 120ML',
                   '8',
                   '1298',
                   '752A252E-3704-4B14-9676-37751A039E1E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 LIVOTINE 60ML',
                   '20',
                   '1299',
                   '0BABD475-7C80-47B7-8859-DB6985773E54-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 METHIOVET AVLI 60TABS 0395',
                   '6',
                   '1300',
                   'C1DE0974-C9AA-4D72-B8FF-CF0CE2810E2F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONDEX BIG 340GMS',
                   '55',
                   '1301',
                   '3FDF5925-4724-4150-9FF2-3A86AA2B8454-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 MONDEX SMALL 100GMS',
                   '133',
                   '1302',
                   'D3A29B7B-1FBF-409B-B029-0E2E6293ADD2-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEFROFLUSH 60TAB',
                   '3',
                   '1303',
                   '7A766198-003D-4DCF-B6B5-0A051A9BBE48-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NEUROVET TABLET',
                   '27',
                   '1304',
                   'D4608B6C-96E3-4DE6-819C-5499B9F4FDAB-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI BLEND GEL- PASTE VITAMIN 220GMS TUBE',
                   '32',
                   '1305',
                   '1F08D7D5-9D3B-4194-AE4D-2B487EAAA05B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI BREED PLUS 60 TABS',
                   '4',
                   '1306',
                   '69E720A6-AC36-4F9E-ACF9-D328BEBB1145-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI MAX FORTE 30ML',
                   '1',
                   '1307',
                   '4DA41F49-57EC-4FF4-A368-A82DFA06E63A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRICAL SYRUP 120 ML',
                   '38',
                   '1308',
                   'DFB83934-A711-4A3F-8241-51A12FBE6457-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRICAL SYRUP 60 ML',
                   '36',
                   '1309',
                   'F6579920-B2FF-4204-8D7F-87EA8F91D783-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI-KAT L-LYSINE PASTE',
                   '15',
                   '1310',
                   '7C197550-FACD-4E10-A85C-A0C47C1490A0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI-PLUS GEL 120.5G',
                   '169',
                   '1311',
                   '5054AA59-5AC8-4304-A034-3BE414380FA7-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRI-TIER GEL PET ENERGY BOOST 30G',
                   '18',
                   '1312',
                   '30BEDCB0-1498-45AD-BE9D-408BF402AF0F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 NUTRITION CREAM HAIR BEAUTY SPECIAL',
                   '0',
                   '1313',
                   'DAC6E2E5-84E2-4E38-A2D0-88DC29DB5CA9-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI ENER G 60ML',
                   '63',
                   '1314',
                   '2A04BB2E-AC89-49A0-BB0E-81722838AE32-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI IRON PLUS',
                   '0',
                   '1315',
                   'AA021DB3-E4A5-4823-9865-78857F4E8A6C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI MVP MULTIVITAMINS 120ML',
                   '15',
                   '1316',
                   '59E4D5BF-85A9-4ACF-90C8-35EF1F940253-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI NACALVIT C',
                   '34',
                   '1317',
                   '34B3A740-B454-41A3-AA51-671FF3282B7C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PAPI OB',
                   '54',
                   '1318',
                   '90D6FA15-E593-4334-8EFA-1066EFE09BFD-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET NUTRIDROPS 15ML',
                   '27',
                   '1319',
                   'A29D03A7-E0B2-413C-ACB8-92945740B46E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET TABS 180TABS. ..',
                   '5',
                   '1320',
                   'EEAFC6ED-11A2-4C40-A827-D8A00D553B79-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PET TABS 60TABS..',
                   '5',
                   '1321',
                   'C2D91A12-6339-4662-9B89-2D3BA9828725-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PETLIV 60 TAB',
                   '0',
                   '1322',
                   '25429D3D-D3FE-4CE2-B350-0E8B5936906F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PETLIV SYRUP 100ML',
                   '4',
                   '1323',
                   '619D9E2C-B2D2-41C6-B304-FDCE5E501678-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PETS OWN CAT AND KITTEN MILK 1L',
                   '0',
                   '1324',
                   '3872D855-0578-4CC0-B0B1-EA544FC7325B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PREFOLIC-CEE 120ML(SODIUM ASCORBATE+FOLIC ACID+FERROUS SULFATE)',
                   '71',
                   '1325',
                   '80B51E55-5C0A-49E6-BBFC-E853188A4116-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 PUPPY BOOST15ML TUBE (PUPPY AND KITTEN)',
                   '6',
                   '1326',
                   'C4457873-577F-42D0-9607-03C96722CC1F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 REFAMOL 30CAPS..',
                   '14',
                   '1327',
                   '7EB6060F-D8DB-4BA6-9FB5-42D9D2DF7326-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RENACURE',
                   '0',
                   '1328',
                   '831319C7-429D-4A92-BA26-E720C0741616-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 RENAL CARE 120ML',
                   '8',
                   '1329',
                   '5370BF29-C1AD-4952-8F6C-BD4EDD5BE537-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SORVIT BIG 120ML',
                   '38',
                   '1330',
                   '978A7D64-B068-4492-86A1-FE84C07A655F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 SORVIT SYRUP 60ML',
                   '38',
                   '1331',
                   '52C2BC56-CF83-4017-8211-73F62149C948-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 THE GOURMET SPECIALIST PREM VCO BACOM (300ML)',
                   '8',
                   '1332',
                   '50A65E03-866C-4A4E-89CA-3D3CDC20FE18-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 THE GOURMET SPECIALIST VCO CHICKEN (300ML)',
                   '7',
                   '1333',
                   'D979D928-DF8A-4B6F-A6CE-AACB3B40A0F4-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 THE GOURMET SPECIALIST VCO SALMON (30ML)',
                   '2',
                   '1334',
                   'DB586E94-D58D-44C7-BD3E-647366267555-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TOLFENOL(TOLFENAMIC ACID SYRUP) 30ML',
                   '47',
                   '1335',
                   '451F1541-DC4C-4116-8C29-5DC36A51EA7F-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 TROY NUTRIPET 200G',
                   '5',
                   '1336',
                   '23906F68-669E-4DFE-B50E-B17D13FF968D-[PetRxITEMQuantity]'
            UNION ALL
            SELECT '2022 VIRBAC NUTRICH 6O TABLETS',
                   '92',
                   '1337',
                   '492258D0-DE8A-4412-835E-EC4DF137FF46-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ASCORBIC ACID (BONALET-CEE) 100MG/5ML 120ML',
                   '0',
                   '1338',
                   'C0452F59-B685-4E0F-96A6-E386911DF6B8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ASCORBIC ACID (CORBIMAX) 100MG/5ML 120ML SYRUP',
                   '0',
                   '1339',
                   '8F57A708-D0F4-4B38-80A7-F9FB42A9C5E8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'CARCHI KAWU CALCIUM',
                   '0',
                   '1340',
                   '9E36C6AE-DFA0-439F-9B42-A71F5FD2A6A0-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'Esbilac Small',
                   '0',
                   '1341',
                   '265D96FC-3409-4A1C-956D-72457D50B975-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'FREXIMOL CAP',
                   '0',
                   '1342',
                   '65257323-2687-4412-8FD7-3661330DF413-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'GOATS MILK POWDER 25G SACHET',
                   '0',
                   '1343',
                   'C8A92D17-993F-4A22-8170-F92C745A1BAC-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HEMACARE-FE 60ML',
                   '0',
                   '1344',
                   '17C06249-8395-4A8C-A9A7-4DE0942B5B69-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'HIMALAYA IMMUNOL SYRUP 100ML.',
                   '0',
                   '1345',
                   'D504C532-411A-4290-8F9B-BB9B9DAEF579-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'IMMUNOL SUSPENSION MANZ',
                   '0',
                   '1346',
                   '6DF41B1B-8FE3-4AFE-AFCF-3BE5DC0DF652-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'KAWU CALCIUM',
                   '0',
                   '1347',
                   'AF4C37BB-03F3-44B7-BB96-D4104206E1E8-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LC VIT OB 30TABS AVLI 0209',
                   '0',
                   '1348',
                   '63CD7958-2E58-45A0-8CB7-E496EB50F883-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LIV52 200ML MANZ PET SUPPLY',
                   '0',
                   '1349',
                   '4EC6F628-DD2E-4020-9514-B1BD2106C69C-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'LIVEROLIN 150ML',
                   '0',
                   '1350',
                   '867160EE-497E-4948-BB8B-81F0CEA20665-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'MALUNGAY POWDER 1KG',
                   '0',
                   '1351',
                   'B40DF3AF-2D8E-486E-BB3A-9121B0276EE3-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NATRAKELP',
                   '0',
                   '1352',
                   '97A5975D-9AE4-4DD7-814B-B7EEC808536E-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'NUTRITION CREAM VEGEBRAND',
                   '0',
                   '1353',
                   '91CDE3B1-172A-404B-A5C5-0C175B641E1A-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'PETS OWN DOG AND PUPPY MILK 1L',
                   '0',
                   '1354',
                   '81475E2A-6334-4B2C-AC34-02DA05260251-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'ROYAL CANIN PUPPY MILK',
                   '0',
                   '1355',
                   '8E24A11B-7D61-4C13-BAFD-3DD66CD44E2B-[PetRxITEMQuantity]'
            UNION ALL
            SELECT 'SEPTAPLEX',
                   '0',
                   '1356',
                   'B238949A-45B5-4D32-83FF-C9A03E9B1C9F-[PetRxITEMQuantity]'
            
            GO 
            

                GO

                SELECT 'ForImport.[dbo].[PetRxITEMQuantity]' TableName
                GO

                SELECT * FROM ForImport.[dbo].[PetRxITEMQuantity]

                GO
            