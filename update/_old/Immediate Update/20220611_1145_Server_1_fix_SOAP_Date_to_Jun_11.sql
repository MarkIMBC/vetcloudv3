DECLARE @ID_AuditTrail INT = 150856
DECLARE @TableName VARCHAR(MAX) = 'tPatient_SOAP'
DECLARE @IDs_Patient_SOAP_Created typIntList
DECLARE @IDs_Patient_SOAP_Mdified typIntList

select TOP 100000 _auditTrail.ID,
                  _user.Name_Company,
                  _user.Name_Employee,
                  Date,
                  Description,
                  Model,
                  ID_CurrentObject,
                  TableName
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail
Order  by _auditTrail.ID DESC

select TOP 100000 _auditTrail.ID,
                  _user.Name_Company,
                  _user.Name_Employee,
                  Date,
                  Description,
                  Model,
                  ID_CurrentObject,
                  TableName
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  _auditTrail.ID = @ID_AuditTrail
Order  by _auditTrail.ID_CurrentObject DESC


INSERT @IDs_Patient_SOAP_Created
select Distinct ID_CurrentObject
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail  AND Description LIKE '%added%'


INSERT @IDs_Patient_SOAP_Mdified
select Distinct ID_CurrentObject
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel m
              on m.Oid = _auditTrail.ID_Model
where  TableName = @TableName
       AND _auditTrail.ID > @ID_AuditTrail AND Description LIKE '%modified%'


Update tPatient_SOAP set Date = hbk.Date
FROM   tPatient_SOAP h
       inner join db_vetcloudv3_server1_backup.dbo.tPatient_SOAP hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_Patient_SOAP_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10'

Update tPatient_SOAP set Date = '2022-06-11 00:00:00'
FROM   tPatient_SOAP h
       inner join db_vetcloudv3_server1_backup.dbo.tPatient_SOAP hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_Patient_SOAP_Created)
       and COnvert(Date, hbk.Date) = '2022-06-10'


DELETE FROM @IDs_Patient_SOAP_Mdified
WHERE  ID IN (SELECT ID
              FROM   @IDs_Patient_SOAP_Created)


SELECT h.ID,
       h.Date,
       hbk.Date,
       h.DateCreated,
       hbk.DateModified
FROM   tPatient_SOAP h
       inner join db_vetcloudv3_server1_backup.dbo.tPatient_SOAP hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_Patient_SOAP_Created)
       and COnvert(Date, hbk.Date) = '2022-06-10'
Order  by h.ID DESC 

SELECT h.ID,
       h.Date,
       hbk.Date,
      hbk.DateCreated,
       hbk.DateModified
FROM   tPatient_SOAP h
       inner join db_vetcloudv3_server1_backup.dbo.tPatient_SOAP hbk
               on h.ID = hbk.ID
WHERE  h.ID IN (SELECT ID
                FROM   @IDs_Patient_SOAP_Mdified)
       and COnvert(Date, hbk.Date) = '2022-06-10'
Order  by h.ID DESC 

