DECLARE @GUID_Company VARCHAR(MAX) = '04267418-6D75-45C4-A94D-EDDF082B9740'
DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')
------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany--Active
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @Table TABLE
  (
     ID_Item                  INT,
     Name_Item                VARCHAR(MAX),
     ID_ItemCategory          INT,
     Name_ItemCategory        VARCHAR(MAX),
     UOM                      VARCHAR(MAX),
     UnitCost                 DECIMAL(18, 2),
     UnitPrice                DECIMAL(18, 2),
     MinInventoryCount        INT,
     MaxInventoryCount        INT,
     CurrentInventoryCount    INT,
     OtherInfo_DateExpiration DateTime,
     GUID                     VARCHAR(MAX),
     DateExpirationString     VARCHAR(MAX)
  )

Insert @Table
       (Name_Item,
        Name_ItemCategory,
        UOM,
        UnitCost,
        UnitPrice,
        MinInventoryCount,
        MaxInventoryCount,
        CurrentInventoryCount,
        DateExpirationString,
        GUID)
SELECT dbo.fGetCleanedString([Item]),
       dbo.fGetCleanedString([Category]),
       dbo.fGetCleanedString([Unit]),
       TRY_CONVERT(Decimal, REPLACE([Buy. Price], ',', '')),
       TRY_CONVERT(Decimal, REPLACE([Sell. Price], ',', '')),
       TRY_CONVERT(int, REPLACE([Min. Count], ',', '')),
       TRY_CONVERT(int, REPLACE([Max Count], ',', '')),
       TRY_CONVERT(int, REPLACE([Current Int. Count], ',', '')),
       dbo.fGetCleanedString([Expiry]),
       GUid
FROM   ForImport.[dbo].[Inventory_Vet_Cloud_Dec._2022 (1)]

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

exec pInsertItemCategory
  'PET CARE AND LODGING',
  2

Update @Table
SET    Name_ItemCategory = 'ANTIBIOTICS & OTHERS'
where  Name_ItemCategory = 'ANTIBIOTIC & OTHERS'

Update @Table
SET    Name_ItemCategory = 'Laboratory Test & Kits'
where  Name_ItemCategory = 'TESTKITS'

Update @Table
SET    Name_ItemCategory = 'VITAMINS & MINERALS SUPPLEMENTS'
where  Name_ItemCategory = 'VITAMIN & MINERAL SUPPLEMENTS'

Update @Table
SET    Name_ItemCategory = 'Vaccination'
where  Name_ItemCategory = 'Vacccines'

Update @Table
SET    Name_ItemCategory = 'Vaccination'
where  Name_ItemCategory = 'Vaccines'

Update @Table
SET    Name_Item = REPLACE(Name_Item, '  ', ' ')

Update @Table
SET    Name_Item = Name_Item
                   + CASE
                       WHEN LEN(UOM) > 0 then ' (' + UOM + ')'
                       ELSE ''
                     end

Update @Table
SET    OtherInfo_DateExpiration = TRY_CONVERT(datetime, DateExpirationString)

delete from @Table
where  len(Name_Item) = 0

Update @Table
set    ID_ItemCategory = MaxID
FROm   @Table excel
       inner join (SELECT MAX(ID) MaxID,
                          Name
                   FROM   tItemCategory
                   where  ID_ItemType = @ID_ItemType_Inventoriable
                          and IsActive = 1
                   Group  by Name) category
               on excel.Name_ItemCategory = category.Name

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             tempID,
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name_Item,
       @ID_ItemType_Inventoriable,
       Guid,
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @Table
where  ID_Item IS NULL

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

SELECT Distinct Name_ItemCategory
FROM   @Table
where  ID_ItemCategory IS NULl

SELECT *
FROM   @Table

Update tItem
SET    UnitCost = excel.UnitCost,
       MinInventoryCount = excel.MinInventoryCount,
       MaxInventoryCount = excel.MaxInventoryCount,
       UnitPrice = excel.UnitPrice,
	   OtherInfo_DateExpiration = excel.OtherInfo_DateExpiration,
       ID_ItemCategory = excel.ID_ItemCategory
FROM   tItem item
       inner join @Table excel
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       AND ID_ItemType = @ID_ItemType_Inventoriable 

----------------------------------------------------------------
DECLARE @ID_UserSession INT = 0
Declare @adjustInventory typReceiveInventory

SELECT @ID_UserSession = MAX(_session.ID)
FROM   tUserSession _session
       INNER JOIN vUser _user
               on _session.ID_User = _user.ID
where  _user.ID_Company = 1

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory',
       ID_Item,
       CurrentInventoryCount,
       UnitPrice,
       OtherInfo_DateExpiration,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FROM   @Table
WHERE  ISNULL(CurrentInventoryCount, 0) > 0


exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT TOP 2000 *
FRom   tInventoryTrail
Order  by ID DESC 
