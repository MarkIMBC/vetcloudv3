DEclare @GUID_Company VARCHAR(MAX) = '30B74061-6489-4093-BAFE-CF1AE8200052'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item                    INT,
     Name_Item                  VARCHAR(MAX),
     CustomCode_Item            VARCHAR(MAX),
     CurrentInventoryCount_Item INT,
     ID_ItemType                INT,
     UnitCost                   DECIMAL(18, 2),
     ID_ItemCategory            INT,
     Name_ItemCategory          VARCHAR(MAX),
     tempID                     VARCHAR(MAX)
  )

Update tItem
set    Name = dbo.fGetCleanedString(Name)
WHERE  ID_Company = @ID_Company;

INSERT @import
       (Name_Item,
        CustomCode_Item,
        CurrentInventoryCount_Item,
        UnitCost,
        tempID)
SELECT dbo.fGetCleanedString([Item]),
       dbo.fGetCleanedString([Code]),
       ISNULL(TRY_CONVERT(INT, [Inventory Count]), 0),
       ISNULL(TRY_CONVERT(DECIMAL(18, 2), [Buying Price]), 0),
       [GUID]
FROM   ForImport.[dbo].[20230314_1401_Pet Wellness HQ_Import Items]

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_ItemCategory = category.ID
FROM   @import import
       INNER JOIN (SELECT MAX(ID) ID,
                          Name,
                          ID_ItemType
                   FROM   tItemCategory
                   GROUP  BY Name,
                             ID_ItemType) category
               on Name_ItemCategory = category.Name
                  And import.ID_ItemType = category.ID_ItemType

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       @Inventoriable_ID_ItemType
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update tItem
SET    ID_ItemType = @Inventoriable_ID_ItemType,
       ID_ItemCategory = import.ID_ItemCategory,
       CustomCode = import.CustomCode_Item,
       UnitCost = import.UnitCost
FROM   tItem item
       INNER JOIN @import import
               on item.ID = import.ID_Item
where  item.ID_COmpany = @ID_Company

-------------------------------------------------------------------------------------------------------------------
DECLARE @importInventory typReceiveInventory

INSERT @importInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory',
       ID_Item,
       CurrentInventoryCount_Item,
       ISNULL(UnitCost, 0),
       NULL,
       NULL,
       3,
       @ID_Company,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt'),
       1
FROM   @import
WHERE  ISNULL(CurrentInventoryCount_Item, 0) > 0

SELECT *
FROM   @import

DECLARE @ID_Usersession INT

SELECT @ID_Usersession = MAX(_session.ID)
FROM   tUserSession _session
       inner join vUser _user
               on _session.ID_User = _user.ID
where  ID_Company = @ID_Company

exec pReceiveInventory
  @importInventory,
  @ID_Usersession

SELECT *
FROM   @importInventory 
