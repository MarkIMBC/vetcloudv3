DEclare @GUID_Company VARCHAR(MAX) = '841008C8-F3F7-4A1B-8A99-D11B1CF5562C'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------
DECLARE @Source_ID_Employee INT = 2554 
DECLARE @Destination_ID_Employee INT = 2550 

SELECT * FROM vEmployee where ID_Company = @ID_Company AND  ID = @Source_ID_Employee
SELECT * FROM vEmployee where  ID_Company = @ID_Company AND  ID = @Destination_ID_Employee

Update tBillingInvoice
set    AttendingPhysician_ID_Employee = @Destination_ID_Employee
WHERE  ID_Company = @ID_Company
       and AttendingPhysician_ID_Employee = @Source_ID_Employee 
	   
Update tPatient_SOAP
set    AttendingPhysician_ID_Employee = @Destination_ID_Employee
WHERE  ID_Company = @ID_Company
       and AttendingPhysician_ID_Employee = @Source_ID_Employee 

Update tPatient_Wellness
set    AttendingPhysician_ID_Employee = @Destination_ID_Employee
WHERE  ID_Company = @ID_Company
       and AttendingPhysician_ID_Employee = @Source_ID_Employee 
	   