DEclare @GUID_Company VARCHAR(MAX) = '9F3CEF51-A356-4477-BAF9-8F2857875664'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
Update ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services]
SET    UnitCost = 0
WHERE  UnitCost = 'NULL'

Update ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services]
SET    UnitPrice = 0
WHERE  UnitPrice = 'NULL'

Update ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services]
SET    ID_ItemCategory = NULL
WHERE  ID_ItemCategory = 'NULL'

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [UnitCost],
             [UnitPrice])
SELECT hed.[Name],
       @ID_Company,
       GUID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       1,
       cat.ID,
       hed.[UnitCost],
       hed.[UnitPrice]
FROM   ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services] hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.ID
where  hed.ID_ItemType = 1
Order  by hed.Name

Update ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services]
SET    ID_ItemCategory = NULL

Update ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services]
SET    ID_ItemCategory = cat.ID
FROM   ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services] hed
       INNER JOIN tItemCategory cat
               on hed.Name_ItemCategory = cat.Name
where  hed.ID_ItemType = 1

Update tItem
SET    ID_ItemCategory = excel.ID_ItemCategory
FROM   tItem hed
       INNER JOIN ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services] excel
               on hed.tempID = excel.GUID
where  hed.ID_ItemType = 1
       AND hed.ID_Company = @ID_Company

SELECT hed.ID,
       hed.Name,
       hed.ID_ItemCategory
FROM   vItem hed
       INNER JOIN ForImport.[dbo].[PetLink Veterinary Clinic - Acacia Branch - Services] excel
               on hed.tempID = excel.GUID
where  hed.ID_ItemType = 1
       AND hed.ID_Company = @ID_Company

GO 
