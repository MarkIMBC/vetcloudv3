DECLARE @GUID_Company VARCHAR(MAX) = '3681AA04-EE98-4115-90CA-AB7AA9E6377D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. '
  + 'Pls. contact /*CompanyName*/ /*ContactNumber*/'
DECLARE @SMSTemplate VARCHAR(MAX) = 'Hi, /*Client*/  This is to remind you that /*Pet*/  has an appointment for /*Service*/ on /*DateReturn*/. This is an automated SMS reminder. PLEASE DO NOT REPLY or CALL HERE./*br*//*br*/
Kindly CONFIRM your appointment by reaching us at /*CompanyName*/ numbers 09424240763 or 8423-0530. Thank You". '

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
