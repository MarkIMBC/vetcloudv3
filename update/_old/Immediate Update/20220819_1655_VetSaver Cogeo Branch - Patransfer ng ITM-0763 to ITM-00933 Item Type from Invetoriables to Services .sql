DEclare @GUID_Company VARCHAR(MAX) = 'DA13BBB1-B9F9-424A-824E-91B8EBA2D1B9'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
Update vItem
SET    ID_ItemType = 1
where  ID_Company = @ID_Company
       AND ID_ItemType = 2
       and IsActive = 1
       AND Code BETWEEN 'ITM-00763' AND 'ITM-00933' 
