DECLARE @GUID_Company VARCHAR(MAX) = '87FA31AA-6D96-4A11-BEC2-488B4678E6F3'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

IF OBJECT_ID(N'Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18', N'U') IS NULL
  BEGIN
      SELECT soapPlan.ID                    ID_Patient_SOAP_Plan,
             soapPlan.DateReturn            DateReturn,
             soap.Code,
             Name_Client,
             Name_Patient,
             ISNULL(soapPlan.Name_Item, '') + ' - '
             + ISNULL(soapPlan.Comment, '') Note,
             ContactNumber_Client
      INTO   [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18]
      FROM   vPatient_SOAP_Plan soapPlan
             Inner join vPatient_SOAP soap
                     on soap.ID = soapPlan.ID_Patient_SOAP
      WHERE  soap.ID_Company = @ID_Company
             and Convert(Date, soapPlan.DateReturn) BETWEEN '2022-04-14' AND '2022-04-17'
             and ID_FilingStatus NOT IN ( 4 )
      Order  by DateReturn,
                Name_Client,
                Name_Patient
  END

SELECT  Name_Model,
                Convert(Date, DateStart),
                COunt(*) Count
FROm   vAppointmentEvent
WHERE  ID_Company = @ID_Company
       and Convert(Date, DateStart) BETWEEN '2022-04-14' AND '2022-04-17'
GROUP  BY Name_Model,
          Convert(Date, DateStart)

Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, 4, forTransfer.DateReturn),
       DateSent = NULL,
       IsSentSMS = 0
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, forTransfer.DateReturn) = '2022-04-14'

Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, 3, forTransfer.DateReturn),
       DateSent = NULL,
       IsSentSMS = 0
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, forTransfer.DateReturn) = '2022-04-15'

Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, 2, forTransfer.DateReturn),
       DateSent = NULL,
       IsSentSMS = 0
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, forTransfer.DateReturn) = '2022-04-16'

Update tPatient_SOAP_Plan
SET    DateReturn = DATEADD(DAY, 1, forTransfer.DateReturn),
       DateSent = NULL,
       IsSentSMS = 0
FROM   tPatient_SOAP_Plan soapPlan
       inner join [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
WHERE  Convert(Date, forTransfer.DateReturn) = '2022-04-17'

SELECT soapPlan.ID ID_Patient_SOAP_Plan,
       forTransfer.DateReturn,
       soapPlan.DateReturn,
       soap.Code,
       soap.Name_Client,
       soap.Name_Patient,
       forTransfer.Note,
       soap.ContactNumber_Client,
       soapPlan.DateSent,
       soapPlan.IsSentSMS
FROM   vPatient_SOAP_Plan soapPlan
       inner join [Temp-The Paws Palace Veterinary Clinic-Transfer_April_14_15_16_17_To_April_18] forTransfer
               on soapPlan.ID = forTransfer.ID_Patient_SOAP_Plan
       Inner join vPatient_SOAP soap
               on soap.ID = soapPlan.ID_Patient_SOAP
WHERE  Convert(Date, forTransfer.DateReturn) BETWEEN '2022-04-14' AND '2022-04-17'
Order  by forTransfer.DateReturn,
          soap.Name_Client,
          soap.Name_Patient

GO
