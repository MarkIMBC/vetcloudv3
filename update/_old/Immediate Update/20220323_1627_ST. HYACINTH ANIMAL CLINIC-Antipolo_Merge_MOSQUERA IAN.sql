DECLARE @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT *
FROM   vClient
WHERE  Code = 'CNT-00635'
       and ID_Company = @ID_Company

SELECT *
FROM   vClient
WHERE  Code = 'CNT-01128'
       and ID_Company = @ID_Company



print '/*Transfer Patient FROM CNT-00635 to CNT-01128*/'
Update tPatient
SET    ID_Client = 346410
WHERE  ID_Client = 305219
       and ID_Company = @ID_Company

print '/*Transfer tPatient_SOAP FROM CNT-00635 to CNT-01128*/'

Update tPatient_SOAP
SET    ID_Client = 346410
WHERE  ID_Client = 305219
       and ID_Company = @ID_Company

print '/*Transfer tPatient_Wellness FROM CNT-00635 to CNT-01128*/'

Update tPatient_Wellness
SET    ID_Client = 346410
WHERE  ID_Client = 305219
       and ID_Company = @ID_Company

print '/*Transfer tVeterinaryHealthCertificate FROM CNT-00635 to CNT-01128*/'

Update tVeterinaryHealthCertificate
SET    ID_Client = 346410
WHERE  ID_Client = 305219
       and ID_Company = @ID_Company

print '/*Delete FROM CNT-01128*/'
UPdate tClient
SET    IsActive = 0
WHERE  ID = 305219
       and ID_Company = @ID_Company 
