--OLD SERVER
DECLARE @GUID_Company_Petaholic VARCHAR(MAX) = 'E20EEA77-74ED-427C-B8DC-45937F9AA40D'
DECLARE @GUID_Company_Oltier VARCHAR(MAX) = '5C063901-B6D0-4CD2-BC97-B8CE22B4415C'
DECLARE @GUID_Company_PepaPets VARCHAR(MAX) = '937C0685-F584-4345-8DFC-8B3EA4B1CF87'
DECLARE @GUID_Company_JpAdriano VARCHAR(MAX) = '9E32BDE2-1452-49A2-A224-A3A4F741DB31'
DECLARE @GUID_Company_VetterHealth VARCHAR(MAX) = '2370F382-FD7E-4AA8-AAB4-43AC860DD5D1'
DECLARE @GUID_Company_Vetline VARCHAR(MAX) = '21D87AC1-A5D8-4E8B-B57E-A591C499A062'
DECLARE @GUID_Company_JustPets VARCHAR(MAX) = 'A5BCA3EB-4405-4543-8920-699A22A67E30'
DECLARE @IDs_Company_ToActivate [dbo].[typIntList]

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company_Petaholic
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

INSERT INTO @IDs_Company_ToActivate
SELECT ID
FROM   tCompany
WHERE  Guid IN ( @GUID_Company_Petaholic, @GUID_Company_Oltier, @GUID_Company_PepaPets, @GUID_Company_JpAdriano,
                 @GUID_Company_VetterHealth, @GUID_Company_Vetline, @GUID_Company_JustPets )

SELECT Name,
       GUID
FROM   vCompanyActive
WHERE  ID IN(SELECT *
             FROM   @IDs_Company_ToActivate)
order  by Name

----------------------------------------------------------------
Update vCompanyActive
SET    isShowPaymentWarningLabel = 1
WHERE  ID IN(SELECT *
             FROM   @IDs_Company_ToActivate) 
