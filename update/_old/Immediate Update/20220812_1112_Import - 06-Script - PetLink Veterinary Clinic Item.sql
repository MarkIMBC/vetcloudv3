DECLARE @GUID_Company VARCHAR(MAX) = 'D4D74C3B-BF59-43B7-8F32-7E93D16B196A'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX)= 'Imported '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @forImport TABLE
  (
     ID_Item         INT,
     Name_Item       VARCHAR(MAX),
     ID_ItemCategory INT,
     UnitPrice       DECIMAL(18, 2),
     Import_Category VARCHAR(MAX),
     GUID            VARCHAR(MAX)
  )

INSERT @forImport
       (Name_Item,
        UnitPrice,
        Import_Category,
        GUID)
select dbo.fGetCleanedString([ITEM NAME])
       + CASE
           WHEN LEN(dbo.fGetCleanedString([ITEM NAME1])) > 0 then ' ' + dbo.fGetCleanedString([ITEM NAME1])
           ELSE ''
         END,
       dbo.fGetCleanedString([SELLING PRICE]),
       dbo.fGetCleanedString([CATEGORY]),
       [GIUD]
FROM   ForImport.dbo.[Petlink-Acacia-ITEMS_20220812_0427]


Update @ForImport SET Import_Category = 'ANTIBIOTICS & OTHERS'
WHERE Import_Category = 'ANTIBIOTIC & OTHERS'

Update @ForImport SET Import_Category = 'VITAMINS & MINERALS SUPPLEMENTS'
WHERE Import_Category = 'VITAMINS & MINERALS SUPPLEMENT'

--Ciao SC72
DELETE FROM @forImport WHERE GUID = '95FCFAAF-CBFD-481F-BACB-993301EE2642-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao IC204
DELETE FROM @forImport WHERE GUID = '1E527911-9566-4405-BFBE-A3CC4AA12A19-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao IMC145
DELETE FROM @forImport WHERE GUID = '3B59AF96-923D-461E-8C54-60F344599A28-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao IMC162
DELETE FROM @forImport WHERE GUID = '86F2B3EE-C09A-4777-90A2-32DF349453E7-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC101
DELETE FROM @forImport WHERE GUID = '612C3785-AA07-48E9-B174-F5A7B373304F-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC126
DELETE FROM @forImport WHERE GUID = '7A132F55-2870-4C5F-AF84-E967CE5CF530-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC129
DELETE FROM @forImport WHERE GUID = '734AE869-7D75-4377-BFE2-10AE99D1E54B-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC130
DELETE FROM @forImport WHERE GUID = 'F72288DA-3C4F-4EED-91D3-60CE5AB7CBB3-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC72
DELETE FROM @forImport WHERE GUID = '1B69B51A-C899-497D-9E18-D105514AC2ED-[Petlink-Acacia-ITEMS_20220812_0427]'

--Ciao SC79
DELETE FROM @forImport WHERE GUID = 'AF8B7B0C-AB60-4D93-9810-30F58E98A426-[Petlink-Acacia-ITEMS_20220812_0427]'

--COridermyl
DELETE FROM @forImport WHERE GUID = 'BDC6E21A-60E8-448B-8008-E3DAE09CBCB0-[Petlink-Acacia-ITEMS_20220812_0427]'

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update @forImport
SET    ID_ItemCategory = category.ID
FROM   @forImport import
       inner join tItemCategory category
               on import.Import_Category = category.Name
WHERE  category.ID_ItemType = @Inventoriable_ID_ItemType
       and category.IsActive = 1

SELECT Import_Category,
       COUNT(*)
FROM   @forImport
WHERE  ID_ItemCategory IS NULL
GROUP  BY Import_Category

SELECT *
FROM   @forImport import
       INNER JOIN (SELECT Name_Item,
                          COUNT(*) Count
                   FROM   @forImport
                   GROUP  BY Name_Item
                   HAVING COUNT(*) > 1) tbl
               on import.Name_Item = tbl.Name_Item

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             tempID)
SELECT DISTINCT import.Name_Item,
                @Inventoriable_ID_ItemType,
                1,
                @ID_Company,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                GUID
FROM   @forImport import
WHERE  ID_Item IS NULL
       and LEN(Name_Item) > 0

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.GUID = item.tempID
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1

Update @ForImport
SET    ID_Item = item.ID
FROM   @ForImport import
       inner join tItem item
               on import.Name_Item = item.Name
WHERE  ID_Company = @ID_Company
       AND ID_ItemType = @Inventoriable_ID_ItemType
       and IsActive = 1
       and ID_Item IS NULL

Update tItem
SET    ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice,
       Name = import.Name_Item
FROM   tItem item
       inner join @forImport import
               on item.ID = import.ID_Item

SELECT *
FROM   @forImport 
