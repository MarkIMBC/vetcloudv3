DECLARE @GUID_Company VARCHAR(MAX) = 'C23B6601-5A2D-4AA5-BF30-B31323AF35FF'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = ''
DECLARE @SMSTemplate VARCHAR(MAX) = 'Good Afternoon /*Client*/! A friendly reminder from /*CompanyName*/, schedule of /*Pet*/ for /*Service*/ tomorrow /*DateReturn*/. Disregard this reminder if already complied.'
 
 SET  @SMSTemplate = 'Dear /*Client*/,/*br*/' + 
 'Your Pet, /*Pet*/ is due to return on /*DateReturn*/ for the following services: /*Service*/./*br*/' + 
 'If you have any concerns, you may contact us at /*ContactNumber*//*br*/' +
 '/*CompanyName*/' 

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    SOAPPlanSMSMessage = @SMSTemplate
where  ID = @ID_Company

SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-09-01', '2022-10-10', NULL, @ID_Company) 
