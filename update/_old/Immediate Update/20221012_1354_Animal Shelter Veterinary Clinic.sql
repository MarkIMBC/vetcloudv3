DECLARE @ClientCodes TABLE
  (
     Code VARCHAR(MAX)
  )
DECLARE @Code VARCHAR(MAX)

INSERT @ClientCodes
VALUES ('CNT-02854'),
       ('CNT-02852'),
       ('CNT-02841'),
       ('CNT-01590'),
       ('CNT-00782'),
       ('CNT-00756'),
       ('CNT-00037'),
       ('CNT-07664')

DECLARE db_cursor CURSOR FOR
  SELECT Code
  FROm   @ClientCodes

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Code

WHILE @@FETCH_STATUS = 0
  BEGIN
      exec pMergeClientRecordByCompany
        '9E8AC765-358D-4A0C-8CF7-389FCD1DD917',
        @Code,
        'CNT-02428'

      FETCH NEXT FROM db_cursor INTO @Code
  END

CLOSE db_cursor

DEALLOCATE db_cursor 
