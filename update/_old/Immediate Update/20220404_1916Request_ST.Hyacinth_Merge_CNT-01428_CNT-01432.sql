DECLARE @GUID_Company VARCHAR(MAX) = 'CE106252-85B7-4348-8441-517560A07A7C'
DECLARE @Source_ID_Client INT=278668
DECLARE @Destination_ID_Client INT=278844

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT 'Source' Note,
       *
FROM   vClient
WHERE  ID = @Source_ID_Client

SELECT 'Destination' Note,
       *
FROM   vClient
WHERE  ID = @Destination_ID_Client

SELECT *
FROM   tBillingInvoice
WHERE  ID_Client = @Source_ID_Client

SELECT *
FROM   tBillingInvoice
WHERE  ID_Client = @Destination_ID_Client

Update tBillingInvoice
SET    ID_Client = @Destination_ID_Client
WHERE  ID_Client = @Source_ID_Client
       AND ID_Company = @ID_Company

Update tClient
SET    IsActive = 0
FROM   tClient
WHERE  ID = @Source_ID_Client
       AND ID_Company = @ID_Company

Update tPatient
SET    Isactive = 0
WHERE  ID_Client = @Source_ID_Client
       AND ID_Company = @ID_Company

SELECT *
FROM   tBillingInvoice
WHERE  ID_Client = @Source_ID_Client

SELECT *
FROM   tBillingInvoice
WHERE  ID_Client = @Destination_ID_Client 
