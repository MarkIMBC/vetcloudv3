DEclare @GUID_Company VARCHAR(MAX) = '6965C9C8-3C12-4D28-858E-707DB4BF144C'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
Update tPatient_SOAP_Plan
SET    DateReturn = '2022-08-22'
WHERE  ID IN ( 290774, 290785, 290792, 290795,
               290804, 292263, 292323, 292338, 292350 ) 
