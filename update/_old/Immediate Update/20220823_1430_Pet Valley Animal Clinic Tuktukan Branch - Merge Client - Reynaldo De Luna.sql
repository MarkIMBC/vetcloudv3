DEclare @GUID_Company VARCHAR(MAX) = '91C3CEF9-1B32-4C1D-BB92-843F4B1116AA'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @Source1_ID_Client INT = 184203 -- Sir Reynaldo De Luna
DECLARE @Destination_ID_Client INT = 182377 -- Reynaldo De Luna 
SELECT ID,
       Code,
       Name,
       'Source 1'
FROm   vClient
WHERE  ID = @Source1_ID_Client
       AND ID_Company = @ID_Company
Union ALL
SELECT ID,
       Code,
       Name,
       'Destination'
FROm   vClient
WHERE  ID = @Destination_ID_Client
       AND ID_Company = @ID_Company

exec pMergeClientRecord
  @Source1_ID_Client,
  @Destination_ID_Client,
  'Pet Valley Animal Clinic Tuktukan Branch - Merge Client - Reynaldo De Luna from CNT-36754 to CNT-34928'

Update tClient
SET    IsActive = 0
WHERE  ID IN ( @Source1_ID_Client )
       and ID_Company = @ID_Company

Update tClient
SET    ContactNumber = '09077041279'
WHERE  ID = @Destination_ID_Client 
