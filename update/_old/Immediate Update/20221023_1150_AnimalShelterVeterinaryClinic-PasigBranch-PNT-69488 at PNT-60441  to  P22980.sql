/* Transfer ARIANA MATABANG to Aryanna Matabang */
exec pTransferPetToNewClientByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-60441',
  'CNT-32320',
  'CNT-02062'

/* Transfer ARYANNA MATABANG to Aryanna Matabang */
exec pTransferPetToNewClientByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-69488',
  'CNT-39030',
  'CNT-02062'


/*Merge Uno*/
exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-60441',
  'PNT-53053'

exec pMergePatientRecordByCompany
  '36B26761-B361-4912-87C4-1387C432B407',
  'PNT-69488',
  'PNT-53053'


/*Update SOAP CLient by Patient*/
UPDATE tPatient_SOAP
SET    ID_Client = _soap.ID_Client
FRom   tPatient_SOAP _soap
       inner join vCompanyActive c
               on _soap.ID_Company = c.ID
       inner join vPatient patient
               on _soap.ID_Patient = patient.id
WHERE  Guid = '36B26761-B361-4912-87C4-1387C432B407'
       and patient.Code = 'PNT-53053'
