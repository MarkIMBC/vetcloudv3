DEclare @GUID_Company VARCHAR(MAX) = '2F40C744-01EE-4977-9F19-9ED34D5F8A9F'
DEclare @Inventoriable_ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')
IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Supplier         INT,
     Name_Supplier         VARCHAR(MAX),
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     UOM_Item  VARCHAR(MAX),
     Name_ItemCategory VARCHAR(MAX),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2),
     DateExpiration DateTime,
     CurrentInventoryCount INT,
     tempID            VARCHAR(MAX)
  )

INSERT @import
       (Name_Supplier, Name_Item,
        UOM_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,DateExpiration, CurrentInventoryCount,
        tempID)
SELECT dbo.fGetCleanedString([Supplier Name]),dbo.fGetCleanedString([Description]),
       dbo.fGetCleanedString([UoM]),
       dbo.fGetCleanedString([Category]),
       TRY_CONVERT(Decimal(18, 2), [Buying Price]),
       TRY_CONVERT(Decimal(18, 2), [Selling Price]),
       TRY_CONVERT(datetime, [Expiration Date]),
       TRY_CONVERT(INT, TRY_CONVERT(decimal, [Critical Level])),
       [GUID]
FROM   ForImport.[dbo].[INVENTORY-2023-TO-VETCLOUD]

UPdate @import set DateExpiration = NULL WHERE DateExpiration = '1900-01-01 00:00:00.000'

Update @import
set    Name_Item = Name_Item + ' - ' + UOM_Item
where  LEN(ISNULL(UOM_Item, 0)) > 0


Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join tItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

	   
----------------------------------------------------------------
DECLARE @ID_UserSession INT = 0
Declare @adjustInventory typReceiveInventory

SELECT @ID_UserSession = MAX(_session.ID)
FROM   tUserSession _session
       INNER JOIN vUser _user
               on _session.ID_User = _user.ID
where  _user.ID_Company = 1

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory',
       ID_Item,
       CurrentInventoryCount,
       UnitPrice,
       DateExpiration,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FROM   @import WHERE ISNULL(CurrentInventoryCount, 0) > 0

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT TOP 2000 *
FRom   tInventoryTrail
Order  by ID DESC 
