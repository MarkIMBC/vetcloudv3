DECLARE @GUID_Company VARCHAR(MAX) = 'AAB4369F-2413-4770-8293-02CC60E9F1B0'
DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany--Active
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

---------------------------------------------------------------------------
DECLARE @Table TABLE
  (
     ID_Item           INT,
     Code_Item         VARCHAR(MAX),
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitCost          DECIMAL(18, 2),
     UnitPrice         DECIMAL(18, 2),
     ID_Supplier       INT,
     Name_Supplier     VARCHAR(MAX),
     GUID              VARCHAR(MAX)
  )

Insert @Table
       (Code_Item,
        Name_Item,
        Name_ItemCategory,
        UnitCost,
        UnitPrice,
        GUID)
SELECT dbo.fGetCleanedString([CODE]),
       dbo.fGetCleanedString([Item]),
       dbo.fGetCleanedString([Category]),
       TRY_CONVERT(Decimal, REPLACE([BUYING PRICE], ',', '')),
       TRY_CONVERT(Decimal, REPLACE([SELLING PRICE], ',', '')),
       GUid
FROM   ForImport.[dbo].[PetRXITEMS]

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

exec pInsertItemCategory
  'PET CARE AND LODGING',
  2,
  1

exec pInsertItemCategory
  'CAGES AND MATTINGS',
  2,
  1

exec pInsertItemCategory
  'CAT Accessories',
  2,
  1

exec pInsertItemCategory
  'CAT FOOD',
  2,
  1

exec pInsertItemCategory
  'DOG FOOD',
  2,
  1

exec pInsertItemCategory
  'GROOMING SUPPLIES',
  2,
  1

exec pInsertItemCategory
  'Toys',
  2,
  1

exec pInsertItemCategory
  'Tray',
  2,
  1

exec pInsertItemCategory
  'NURSERY SERVICES',
  2,
  1

exec pInsertItemCategory
  'CHECK UP AND INJECTIONS',
  2,
  1

Update @Table
SET    Name_ItemCategory = 'Antiparasitic Products (Oral, Spot-on, Spray) '
WHERE  Name_ItemCategory = 'ANTI-PARASITIC PRODUCTS - 0019'

Update @Table
SET    Name_ItemCategory = 'CAGES AND MATTINGS'
WHERE  Name_ItemCategory = 'CAGES AND MATTINGS - 0020'

Update @Table
SET    Name_ItemCategory = 'CAT FOOD'
WHERE  Name_ItemCategory = 'CAT FOOD - 0021'

Update @Table
SET    Name_ItemCategory = 'CAT Accessories'
WHERE  Name_ItemCategory = 'Cats - 0022'

Update @Table
SET    Name_ItemCategory = 'Others'
WHERE  Name_ItemCategory = 'CLINIC SUPPLIES - 0032'

Update @Table
SET    Name_ItemCategory = 'DOG FOOD'
WHERE  Name_ItemCategory = 'DOG FOOD - 0023'

Update @Table
SET    Name_ItemCategory = 'Grooming Services'
WHERE  Name_ItemCategory = 'GROOMING ONLY - 0040'

Update @Table
SET    Name_ItemCategory = 'GROOMING SUPPLIES'
WHERE  Name_ItemCategory = 'GROOMING SUPPLIES - 0026'

Update @Table
SET    Name_ItemCategory = 'Merchandise & Accessories'
WHERE  Name_ItemCategory = 'litter Sand - 0027'

Update @Table
SET    Name_ItemCategory = 'Medical Supply'
WHERE  Name_ItemCategory = 'MEDICATIONS(ANTIBIOTICS, MEDICATED SHAMPOO,OINTMEN - 0029'

Update @Table
SET    Name_ItemCategory = 'Misc'
WHERE  Name_ItemCategory = 'MISCELLANEOUS - 0039'

Update @Table
SET    Name_ItemCategory = 'NURSERY SERVICES'
WHERE  Name_ItemCategory = 'NURSERY SERVICES - 0024'

Update @Table
SET    Name_ItemCategory = 'CHECK UP AND INJECTIONS'
WHERE  Name_ItemCategory = 'PET RX SERVICE (CHECK UP AND INJECTIONS) - 0038'

Update @Table
SET    Name_ItemCategory = 'Merchandise & Accessories'
WHERE  Name_ItemCategory = 'PET SUPPLIES AND ACCESSORIES(PRODUCTS NOT MEDICAL - 0018'

Update @Table
SET    Name_ItemCategory = 'SURGERY'
WHERE  Name_ItemCategory = 'SURGERY - 0030'

Update @Table
SET    Name_ItemCategory = 'Laboratory Test & Kits'
WHERE  Name_ItemCategory = 'TEST KITS, LABORATORY AND TEST - 0033'

Update @Table
SET    Name_ItemCategory = 'Toys'
WHERE  Name_ItemCategory = 'toys - 0034'

Update @Table
SET    Name_ItemCategory = 'Tray'
WHERE  Name_ItemCategory = 'tray - 0035'

Update @Table
SET    Name_ItemCategory = 'Treats'
WHERE  Name_ItemCategory = 'Treats - 0036'

Update @Table
SET    Name_ItemCategory = 'Vaccination'
WHERE  Name_ItemCategory = 'VACCINATIONS - 0037'

Update @Table
SET    Name_ItemCategory = 'VITAMINS & MINERALS SUPPLEMENTS'
WHERE  Name_ItemCategory = 'VITAMIN AND SUPPLEMENTS - 0031'

DELETE FROM @Table
where  Name_ItemCategory IN ( 'Grooming Services', 'NURSERY SERVICES', 'CHECK UP AND INJECTIONS', 'SURGERY' )

Update @Table
SET    Name_Item = REPLACE(Name_Item, '  ', ' ')

delete from @Table
where  len(Name_Item) = 0

Update @Table
set    ID_ItemCategory = MaxID
FROm   @Table excel
       inner join (SELECT MAX(ID) MaxID,
                          Name
                   FROM   tItemCategory
                   where  ID_ItemType = @ID_ItemType_Inventoriable
                          and IsActive = 1
                   Group  by Name) category
               on excel.Name_ItemCategory = category.Name

Update @Table
set    ID_ItemCategory = MaxID
FROm   @Table excel
       inner join (SELECT MAX(ID) MaxID,
                          Name
                   FROM   tItemCategory
                   where  ID_ItemType = 1
                          and IsActive = 1
                   Group  by Name) category
               on excel.Name_ItemCategory = category.Name
WHERE  Name_ItemCategory IN ( 'Grooming Services', 'NURSERY SERVICES', 'CHECK UP AND INJECTIONS', 'SURGERY' )

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             tempID,
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name_Item,
       @ID_ItemType_Inventoriable,
       Guid,
       @ID_Company,
       @Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @Table
where  ID_Item IS NULL

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

SELECT Distinct Name_ItemCategory
FROM   @Table
where  ID_ItemCategory IS NULl

SELECT *
FROM   @Table

Update tItem
SET    UnitCost = excel.UnitCost,
       UnitPrice = excel.UnitPrice,
       CustomCode = excel.Code_Item,
       ID_ItemCategory = excel.ID_ItemCategory
FROM   tItem item
       inner join @Table excel
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       AND ID_ItemType = @ID_ItemType_Inventoriable 
