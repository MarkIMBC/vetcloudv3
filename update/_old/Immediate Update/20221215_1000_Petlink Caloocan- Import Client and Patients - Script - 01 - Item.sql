DECLARE @GUID_Company VARCHAR(MAX) = '77F3B4CF-6521-4CCF-AA71-2A1FC83EFF81'
DECLARE @ID_ItemType_Inventoriable INT = 2
------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany--Active
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
Update tItem
set    IsActive = 0,
       Comment = CASE
                   WHEN LEN(ISNULL(Comment, '')) > 0 then CHAR(13) + '|'
                   ELSE ''
                 END
                 + 'Deleted as of '
                 + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')
where  ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType_Inventoriable
       and IsActive = 1
-----------------------------------------------------------------

DECLARE @Table TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitPrice         DECIMAL(18, 2),
     GUID              VARCHAR(MAX)
  )

Insert @Table
       (Name_Item,
        Name_ItemCategory,
        UnitPrice,
        GUID)
SELECT dbo.fGetCleanedString([ITEM]),
       [CATEGORY],
       TRY_CONVERT(Decimal, REPLACE([SELLING PRICE], ',', '')),
       GUid
FROM   ForImport.[dbo].[CALOOCAN-VET-CLOUD-PRICE-INCREASE-2022-1(1)-Items]

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

Update @Table
SET    Name_ItemCategory = 'ANTIBIOTICS & OTHERS'
where  Name_ItemCategory = 'ANTIBIOTIC & OTHERS'

Update @Table
SET    Name_ItemCategory = 'Laboratory Test & Kits'
where  Name_ItemCategory = 'TESTKITS'

Update @Table
SET    Name_ItemCategory = 'VITAMINS & MINERALS SUPPLEMENTS'
where  Name_ItemCategory = 'VITAMIN & MINERAL SUPPLEMENTS'

Update @Table
set    ID_ItemCategory = MaxID
FROm   @Table excel
       inner join (SELECT MAX(ID) MaxID,
                          Name
                   FROM   tItemCategory
                   where  ID_ItemType = @ID_ItemType_Inventoriable
                          and IsActive = 1
                   Group  by Name) category
               on excel.Name_ItemCategory = category.Name

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_ItemType],
             [UnitPrice],
             tempID,
             [ID_Company],
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name_Item,
       @ID_ItemType_Inventoriable,
       UnitPrice,
       Guid,
       @ID_Company,
       + 'Imported as of '
       + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss'),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @Table
where  ID_Item IS NULL

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

SELECT Distinct Name_ItemCategory
FROM   @Table
where  ID_ItemCategory IS NULl

SELECT *
FROM   @Table

Update tItem
SET    UnitPrice = excel.UnitPrice,
       ID_ItemCategory = excel.ID_ItemCategory
FROM   tItem item
       inner join @Table excel
               on item.tempID = excel.GUID
where  ID_Company = @ID_Company
       AND ID_ItemType = @ID_ItemType_Inventoriable 
