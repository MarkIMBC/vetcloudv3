DECLARE @GUID_Company VARCHAR(MAX) = 'AAB4369F-2413-4770-8293-02CC60E9F1B0'
DECLARE @ID_ItemType_Inventoriable INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompany--Active
   WHERE  Guid = @GUID_Company) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

---------------------------------------------------------------------------
DECLARE @Table TABLE
  (
     ID_Item               INT,
     Name_Item             VARCHAR(MAX),
     CurrentInventoryCount VARCHAR(MAX),
     GUID                  VARCHAR(MAX)
  )

Insert @Table
       (Name_Item,
        CurrentInventoryCount,
        GUID)
SELECT dbo.fGetCleanedString([ITEM]),
       TRY_CONVERT(INT, REPLACE([QTY], ',', '')),
       GUid
FROM   ForImport.dbo.[PetRxITEMQuantity]

Update @Table
SET    ID_Item = item.ID
FROM   @Table excel
       INNER JOIN tItem item
               on item.Name = excel.Name_Item
where  ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType_Inventoriable

DECLARE @ID_UserSession INT = 0
Declare @adjustInventory typReceiveInventory

SELECT @ID_UserSession = MAX(_session.ID)
FROM   tUserSession _session
       INNER JOIN vUser _user
               on _session.ID_User = _user.ID
where  _user.ID_Company = 1

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory',
       ID_Item,
       CurrentInventoryCount,
       0,
       NULL,
       NULL,
       3,
       @ID_Company,
       @Comment,
       1
FROM   @Table
WHERE  ISNULL(CurrentInventoryCount, 0) > 0

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

SELECT TOP 2000 *
FRom   tInventoryTrail
Order  by ID DESC 
