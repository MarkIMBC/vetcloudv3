IF COL_LENGTH('tInventoryTrail', 'Oid_Model_Reference') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tInventoryTrail',
        'Oid_Model_Reference',
        1
  END

IF COL_LENGTH('tInventoryTrail', 'ID_Reference') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tInventoryTrail',
        'ID_Reference',
        2
  END

Go

IF COL_LENGTH('dbo.tInventoryTrail_DeletedRecord', 'Oid_Model_Reference') IS NULL
  BEGIN
      ALTER TABLE dbo.tInventoryTrail_DeletedRecord
        ADD Oid_Model_Reference VARCHAR(MAX) NULL;
  END

GO

IF COL_LENGTH('dbo.tInventoryTrail_DeletedRecord', 'ID_Reference') IS NULL
  BEGIN
      ALTER TABLE dbo.tInventoryTrail_DeletedRecord
        ADD ID_Reference INT NULL;
  END

Go

exec _pRefreshAllViews

GO

ALTER PROC [dbo].[pApproveBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,
                                           @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @Oid_Model_BillingInvoice_Detail = Oid
          FROM   _tModel
          where  TableName = 'tBillingInvoice_Detail'

          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession

          Update tBillingInvoice_Detail
          SET    UnitCost = ISNULL(item.UnitCost, 0)
          FROM   tBillingInvoice_Detail biDetail
                 INNER JOIn @IDs_BillingInvoice ids
                         on biDetail.ID_BillingInvoice = ids.iD
                 INNER JOIN tItem item
                         on biDetail.ID_Item = item.ID

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO dbo.tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired,
                       Oid_Model_Reference,
                       ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 0 - detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration,
                 @Oid_Model_BillingInvoice_Detail,
                 detail.ID
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------  
          ---  
          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Used Deposit on Credit Logs      
          Declare @IDs_ClientDeposit typIntList
          DECLARE @ClientCredits typClientCredit

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ) * -1,
                          bi.Code,
                          'Use Deposit from '
                          + FORMAT(ISNULL(bi.ConfinementDepositAmount, 0), '#,#0.00')
                          + ' to '
                          + FORMAT(ISNULL( bi.RemainingDepositAmount, 0), '#,#0.00')
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Used_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          IF (SELECT COUNT(*)
              FROM   @ClientCredits) > 0
            begin
                EXEC pAdjustClientCredits_validation
                  @ClientCredits,
                  @ID_UserSession

                exec pAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pCancelBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,
                                          @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ClientCredits typClientCredit
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @Oid_Model_BillingInvoice_Detail = Oid
          FROM   _tModel
          where  TableName = 'tBillingInvoice_Detail'

          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ),
                          bi.Code,
                          'Rollback Deposit '
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0
                 AND ID_FilingStatus = @Approved_ID_FilingStatus

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired,
                       Oid_Model_Reference,
                       ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration,
                 @Oid_Model_BillingInvoice_Detail,
                 detail.ID
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0
                 AND item.ID_ItemType = @Inventoriable_ID_ItemType;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------  
          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Rollback Deposit on Credit Logs      
          Declare @IDs_ClientDeposit typIntList

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC [dbo].[pApproveReceivingReport] (@IDs_ReceivingReport typIntList READONLY,
                                            @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model_ReceivingReport_Detail VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @Oid_Model_ReceivingReport_Detail = Oid
          FROM   _tModel
          where  TableName = 'tReceivingReport_Detail'

          DECLARE @ID_User INT = 0;
          DECLARE @IDs_PurchaseOrder typIntList;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveReceivingReport_validation
            @IDs_ReceivingReport,
            @ID_UserSession;

          UPDATE dbo.tReceivingReport
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tReceivingReport bi
                 INNER JOIN @IDs_ReceivingReport ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       Oid_Model_Reference,
                       ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 @Oid_Model_ReceivingReport_Detail,
                 detail.ID
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------  
          /* Update PO Serving Status */
          INSERT @IDs_PurchaseOrder
                 (ID)
          SELECT DISTINCT rrHed.ID_PurchaseOrder
          FROM   dbo.tReceivingReport rrHed
                 INNER JOIN @IDs_ReceivingReport ids
                         ON rrHed.ID = ids.ID;

          EXEC dbo.pUpdatePOServingStatus
            @IDs_PurchaseOrder;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
 

ALTER PROC [dbo].[pCancelReceivingReport] (@IDs_ReceivingReport typIntList READONLY,
                                       @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model_ReceivingReport_Detail VARCHAR(MAX) = '';

      BEGIN TRY

          SELECT @Oid_Model_ReceivingReport_Detail = Oid
          FROM   _tModel
          where  TableName = 'tReceivingReport_Detail'

          DECLARE @ID_User INT = 0;
          DECLARE @IDs_PurchaseOrder typIntList;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelReceivingReport_validation
            @IDs_ReceivingReport,
            @ID_UserSession;

          UPDATE dbo.tReceivingReport
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tReceivingReport bi
                 INNER JOIN @IDs_ReceivingReport ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date, Oid_Model_Reference, ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 0 - detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date, @Oid_Model_ReceivingReport_Detail, detail.ID
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0;

          -- pUpdate Item Current Inventory   
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tReceivingReport hed
                 LEFT JOIN dbo.tReceivingReport_Detail detail
                        ON hed.ID = detail.ID_ReceivingReport
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_ReceivingReport ids
                         ON hed.ID = ids.ID

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------  
          /* Update PO Serving Status */
          INSERT @IDs_PurchaseOrder
                 (ID)
          SELECT DISTINCT rrHed.ID_PurchaseOrder
          FROM   dbo.tReceivingReport rrHed
                 INNER JOIN @IDs_ReceivingReport ids
                         ON rrHed.ID = ids.ID;

          EXEC dbo.pUpdatePOServingStatus
            @IDs_PurchaseOrder;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO



CREATE OR
ALTER PROC pRemoveMulticateInventoryTrail(@IsShowQueryTestResult BIT = 0)
as
  BEGIN
      if OBJECT_ID('dbo.tInventoryTrail_DeletedRecord') is null
        BEGIN
            CREATE TABLE [dbo].[tInventoryTrail_DeletedRecord]
              (
                 [ID]                [int],
                 [Code]              [varchar](50) NULL,
                 [Name]              [varchar](200) NULL,
                 [IsActive]          [bit] NULL,
                 [ID_Company]        [int] NULL,
                 [Comment]           [varchar](max) NULL,
                 [DateCreated]       [datetime] NULL,
                 [DateModified]      [datetime] NULL,
                 [ID_CreatedBy]      [int] NULL,
                 [ID_LastModifiedBy] [int] NULL,
                 [ID_Item]           [int] NULL,
                 [Quantity]          [int] NULL,
                 [UnitPrice]         [decimal](18, 4) NULL,
                 [ID_FilingStatus]   [int] NULL,
                 [Date]              [datetime] NULL,
                 [DateExpired]       [datetime] NULL,
                 [BatchNo]           [int] NULL,
                 [tempID]            [varchar](300) NULL,
                 DateDeleted         [datetime] NULL,
                 LastDuplicateCount  INT
              )
        END

      DECLARE @record TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           [Count]             [int] NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL
        )
      DECLARE @forDeleterecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL,
           [Count]             [int] NULL
        )
      DECLARE @NotDeletedrecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL,
           [Count]             [int] NULL
        )

      INSERT @record
      select c.ID,
             c.Name,
             MAX(invt.ID) MaxID,
             invt.Code,
             invt.ID_Item,
             invt.Name_Item,
             invt.Quantity,
             invt.UnitPrice,
             invt.Date,
             NULL,
             invt.Name_FilingStatus,
             COUNT(*)     Count,
             Oid_Model_Reference,
             ID_Reference
      FROm   vInventoryTrail invt
             Inner join tItem item
                     on invt.ID_Item = item.ID
             inner join vCompanyActive c
                     on item.ID_Company = c.ID
      GROUP  BY c.ID,
                c.Name,
                invt.Code,
                invt.ID_Item,
                invt.Name_Item,
                invt.Quantity,
                invt.UnitPrice,
                invt.Date,
                invt.Name_FilingStatus,
                Oid_Model_Reference,
                ID_Reference
      HAVING COUNT(*) > 1

      DECLARE @ItemWithBiDetail TABLE
        (
           ID_BIllingInvoice_Detail INT,
           Code_BillingInvoice      VARCHAR(MAX),
           ID_Company               INT,
           ID_Item                  Int,
           Quantity                 Int
        )
      DECLARE @BiWithMultipleItem TABLE
        (
           Code_BillingInvoice VARCHAR(MAX),
           ID_Company          INT,
           ID_Item             INT,
           Quantity            Int
        )

      INSERT @ItemWithBiDetail
      SELECT DISTINCT biDet.ID,
                      bi.Code,
                      bi.ID_Company,
                      biDet.ID_Item,
                      biDet.Quantity
      FROM   tBillingInvoice bi
             inner join tBillingInvoice_Detail biDet
                     on bi.ID = bidet.ID_BillingInvoice
             INNER JOIN @record record
                     on bi.Code = record.Code
                        and bi.ID_Company = record.ID_Company
                        and biDet.ID_Item = record.ID_Item
                        and biDet.Quantity = record.Quantity

      INSERT @BiWithMultipleItem
      SELECT Code_BillingInvoice,
             ID_Company,
             ID_Item,
             Quantity
      FROM   @ItemWithBiDetail
      GROUP  BY Code_BillingInvoice,
                ID_Company,
                ID_Item,
                Quantity
      HAVING COUNT(*) > 1

      INSERT @forDeleterecord
      SELECT *
      FROM   @record
      EXCEPT
      SELECT rec.*
      FROm   @record rec
             inner join @BiWithMultipleItem bi
                     on rec.Code = bi.Code_BillingInvoice
                        and rec.ID_Company = bi.ID_Company
                        and bi.ID_Item = rec.ID_Item
                        and bi.ID_Item = rec.ID_Item

      IF( @IsShowQueryTestResult = 1 )
        BEGIN
            INSERT @NotDeletedrecord
            SELECT *
            FROM   @record
            except
            SELECT rec.*
            FROm   @forDeleterecord rec

            SELECT *
            FROm   tInventoryTrail_DeletedRecord

            SELECT *
            FROm   @record
            Order  by Code

            SELEct 'Not For Deleted',
                   *
            FROm   @NotDeletedrecord

            SELEct 'For Deleted',
                   *
            FROm   @forDeleterecord
        END

      INSERT tInventoryTrail_DeletedRecord
      SELECT invt.*,
             GETDATE(),
             rec.Count
      FROM   tInventoryTrail invt
             inner join @forDeleterecord rec
                     on invt.ID = rec.MaxID

      DELETE FROM tInventoryTrail
      WHERE  ID IN (SELECT MaxID
                    FROM   @forDeleterecord)
  END

GO

GO

ALTER PROC pRemoveMulticateInventoryTrailByIDsItems(@IDs_Item              typIntList READONLY,
                                                    @IsShowQueryTestResult BIT = 0)
as
  BEGIN
      if OBJECT_ID('dbo.tInventoryTrail_DeletedRecord') is null
        BEGIN
            CREATE TABLE [dbo].[tInventoryTrail_DeletedRecord]
              (
                 [ID]                [int],
                 [Code]              [varchar](50) NULL,
                 [Name]              [varchar](200) NULL,
                 [IsActive]          [bit] NULL,
                 [ID_Company]        [int] NULL,
                 [Comment]           [varchar](max) NULL,
                 [DateCreated]       [datetime] NULL,
                 [DateModified]      [datetime] NULL,
                 [ID_CreatedBy]      [int] NULL,
                 [ID_LastModifiedBy] [int] NULL,
                 [ID_Item]           [int] NULL,
                 [Quantity]          [int] NULL,
                 [UnitPrice]         [decimal](18, 4) NULL,
                 [ID_FilingStatus]   [int] NULL,
                 [Date]              [datetime] NULL,
                 [DateExpired]       [datetime] NULL,
                 [BatchNo]           [int] NULL,
                 [tempID]            [varchar](300) NULL,
                 DateDeleted         [datetime] NULL,
                 LastDuplicateCount  INT
              )
        END

      DECLARE @record TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           [Count]             [int] NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL
        )
      DECLARE @forDeleterecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL,
           [Count]             [int] NULL
        )
      DECLARE @NotDeletedrecord TABLE
        (
           ID_Company          [int] NULL,
           Name_Company        [varchar](MAX),
           [MaxID]             [int] NULL,
           [Code]              [varchar](50) NULL,
           [ID_Item]           [int] NULL,
           [Name_Item]         [varchar](200) NULL,
           [Quantity]          [int] NULL,
           [UnitPrice]         [decimal](18, 4) NULL,
           [Date]              [datetime] NULL,
           [DateCreated]       [datetime] NULL,
           [Name_FilingStatus] [varchar](200) NULL,
           Oid_Model_Reference [varchar](200) NULL,
           ID_Reference        [int] NULL,
           [Count]             [int] NULL
        )

      INSERT @record
      select c.ID,
             c.Name,
             MAX(invt.ID) MaxID,
             invt.Code,
             invt.ID_Item,
             invt.Name_Item,
             invt.Quantity,
             invt.UnitPrice,
             invt.Date,
             NULL,
             invt.Name_FilingStatus,
             COUNT(*)     Count,
             Oid_Model_Reference,
             ID_Reference
      FROm   vInventoryTrail invt
             Inner join tItem item
                     on invt.ID_Item = item.ID
             inner join vCompanyActive c
                     on item.ID_Company = c.ID
             INNER JOIN @IDs_Item idsItem
                     on idsItem.ID = invt.ID_Item
      GROUP  BY c.ID,
                c.Name,
                invt.Code,
                invt.ID_Item,
                invt.Name_Item,
                invt.Quantity,
                invt.UnitPrice,
                invt.Date,
                invt.Name_FilingStatus,
                Oid_Model_Reference,
                ID_Reference
      HAVING COUNT(*) > 1

      DECLARE @ItemWithBiDetail TABLE
        (
           ID_BIllingInvoice_Detail INT,
           Code_BillingInvoice      VARCHAR(MAX),
           ID_Company               INT,
           ID_Item                  Int,
           Quantity                 Int
        )
      DECLARE @BiWithMultipleItem TABLE
        (
           Code_BillingInvoice VARCHAR(MAX),
           ID_Company          INT,
           ID_Item             INT,
           Quantity            Int
        )

      INSERT @ItemWithBiDetail
      SELECT DISTINCT biDet.ID,
                      bi.Code,
                      bi.ID_Company,
                      biDet.ID_Item,
                      biDet.Quantity
      FROM   tBillingInvoice bi
             inner join tBillingInvoice_Detail biDet
                     on bi.ID = bidet.ID_BillingInvoice
             INNER JOIN @record record
                     on bi.Code = record.Code
                        and bi.ID_Company = record.ID_Company
                        and biDet.ID_Item = record.ID_Item
                        and biDet.Quantity = record.Quantity

      INSERT @BiWithMultipleItem
      SELECT Code_BillingInvoice,
             ID_Company,
             ID_Item,
             Quantity
      FROM   @ItemWithBiDetail
      GROUP  BY Code_BillingInvoice,
                ID_Company,
                ID_Item,
                Quantity
      HAVING COUNT(*) > 1

      INSERT @forDeleterecord
      SELECT *
      FROM   @record
      EXCEPT
      SELECT rec.*
      FROm   @record rec
             inner join @BiWithMultipleItem bi
                     on rec.Code = bi.Code_BillingInvoice
                        and rec.ID_Company = bi.ID_Company
                        and bi.ID_Item = rec.ID_Item
                        and bi.ID_Item = rec.ID_Item

      IF( @IsShowQueryTestResult = 1 )
        BEGIN
            INSERT @NotDeletedrecord
            SELECT *
            FROM   @record
            except
            SELECT rec.*
            FROm   @forDeleterecord rec

            SELECT *
            FROm   tInventoryTrail_DeletedRecord

            SELECT *
            FROm   @record
            Order  by Code

            SELEct 'Not For Deleted',
                   *
            FROm   @NotDeletedrecord

            SELEct 'For Deleted',
                   *
            FROm   @forDeleterecord
        END

      INSERT tInventoryTrail_DeletedRecord
      SELECT invt.*,
             GETDATE(),
             rec.Count
      FROM   tInventoryTrail invt
             inner join @forDeleterecord rec
                     on invt.ID = rec.MaxID

      DELETE FROM tInventoryTrail
      WHERE  ID IN (SELECT MaxID
                    FROM   @forDeleterecord)
  END

go

CREATE OR
ALTER PROC [dbo].[pUpdateItemCurrentInventory]
AS
  BEGIN
      exec pRemoveMulticateInventoryTrail
        0

      UPDATE dbo.tItem
      SET    CurrentInventoryCount = ISNULL(inventory.Qty, 0),
             ID_InventoryStatus = dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount, 0), ISNULL(MaxInventoryCount, 0))
      FROM   dbo.tItem item
             LEFT JOIN vInventoryTrailTotal inventory
                    ON inventory.ID_Item = item.ID;
  END;

GO

exec pRemoveMulticateInventoryTrail
  1

exec pUpdateItemCurrentInventory

select top 100 *
fROm   tInventoryTrail
Order  by ID DESC

select top 100 *
fROm   tInventoryTrail_DeletedRecord
Order  by ID DESC 
