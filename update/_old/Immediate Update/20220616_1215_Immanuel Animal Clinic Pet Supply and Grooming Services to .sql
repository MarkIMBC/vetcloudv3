Update tCompany
SET    IsActive = 1
where  iD = 145

GO

DEclare @SOurce_GUID_Company VARCHAR(MAX) = 'F5BA7DF7-38AD-4F3B-A9F1-27A5D0263A69'
DEclare @Destination_GUID_Company VARCHAR(MAX) = 'E2F78167-18DA-408B-A29A-18C3C9912544'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @SOurce_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Source Company does not exist.', 1;
  END

SELECT Name Source_Company,
       *
FROm   tCompany
WHERE  GUID = @SOurce_GUID_Company

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @Destination_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Destination Company does not exist.', 1;
  END

SELECT Name Destination_Company,
       *
FROm   tCompany
WHERE  GUID = @Destination_GUID_Company

DECLARE @Comment VARCHAR(MAX) = 'Transfer from ' + @Destination_GUID_Company + ' '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
DECLARE @Source_ID_Company INT
DECLARE @Destination_ID_Company INT
DECLARE @Source_Name_Company VARCHAR(MAX)
DECLARE @Destination_Name_Company VARCHAR(MAX)

SELECt @Source_ID_Company = ID,
       @Source_Name_Company = Name
FROm   tCompany
WHERE  Guid = @SOurce_GUID_Company

SELECt @Destination_ID_Company = ID,
       @Destination_Name_Company = Name
FROm   tCompany
WHERE  Guid = @Destination_GUID_Company

-----------------------------------------------------------
Update tClient
SET    ID_Company = @Destination_ID_Company,
       Comment = ISNULL(Comment, '')
                 + CASE
                     WHEN LEN(ISNULL(Comment, '')) > 0 then CHAR(13) + CHAR(13)
                     ELSE ''
                   END
                 + @Comment
WHERE  ID_Company = @Source_ID_Company
       AND ID NOT IN (SELECT ID
                      FROM   tBillingInvoice
                      WHERE  ID_Company = @Source_ID_Company)

Update tPatient
SET    ID_Company = @Destination_ID_Company,
       Comment = ISNULL(Comment, '')
                 + CASE
                     WHEN LEN(ISNULL(Comment, '')) > 0 then CHAR(13) + CHAR(13)
                     ELSE ''
                   END
                 + @Comment
WHERE  ID_Company = @Source_ID_Company
       AND ID_Client NOT IN (SELECT ID
                             FROM   tBillingInvoice
                             WHERE  ID_Company = @Source_ID_Company)

Update tPatient_SOAP
SET    ID_Company = @Destination_ID_Company,
       Comment = ISNULL(Comment, '')
                 + CASE
                     WHEN LEN(ISNULL(Comment, '')) > 0 then CHAR(13) + CHAR(13)
                     ELSE ''
                   END
                 + @Comment
WHERE  ID_Company = @Source_ID_Company

SELECT *
FROm   tClient
WHERE  ID_Company = @Destination_ID_Company 
