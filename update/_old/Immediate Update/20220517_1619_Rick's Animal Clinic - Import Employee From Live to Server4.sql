IF OBJECT_ID(N'Temp-2022-05-17-Employee', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-05-17-Employee];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-05-17-' + company.Guid
       + '-Employee-'
       + Convert(Varchar(MAX), Employee.ID) tempEmployeeID,
       Employee.ID                          Main_ID_Employee,
       ID_Company                           Main_ID_Company,
       Employee.*
INTO   [dbo].[Temp-2022-05-17-Employee]
FROM   db_vetcloudv3_live_dev.[dbo].tEmployee Employee
       inner join db_vetcloudv3_live_dev.[dbo].tCompany company
               on Employee.ID_Company = company.ID
where  Employee.ID >= 2345
       and Guid = '17C7284C-0CE9-4206-A461-F2E98FF82F2E'

INSERT @NotYetInserted_TempID
SELECT tempEmployee.tempID
FROm   [dbo].[Temp-2022-05-17-Employee] tempEmployee

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tEmployee)

SELECT tempEmployee.ID_Company,
       tempEmployee.tempID,
       tempEmployee.Name,
       tempEmployee.ContactNumber,
       tempEmployee.Email,
       Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [dbo].[Temp-2022-05-17-Employee] tempEmployee
       inner join @NotYetInserted_TempID unInsertedTempEmployee
               on tempEmployee.tempID = unInsertedTempEmployee.tempID

INSERT INTO [dbo].[tEmployee]
            ([Code],
             [IsActive],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [DateCreated],
             [DateModified],
             [ImageFile],
             [ID_Department],
             [ID_EmployeePosition],
             [ID_Position],
             [LastName],
             [FirstName],
             [MiddleName],
             [ID_Gender],
             [ID_EmployeeStatus],
             [FullAddress],
             [Email],
             [ContactNumber],
             [Name],
             [ID_Company],
             [Comment],
             [IsSystemUsed],
             [PRCLicenseNumber],
             [PTR],
             [S2],
             [TINNumber],
             [DatePRCExpiration],
             [tempID])
SELECT [Code],
       [IsActive],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [DateCreated],
       [DateModified],
       [ImageFile],
       [ID_Department],
       [ID_EmployeePosition],
       [ID_Position],
       [LastName],
       [FirstName],
       [MiddleName],
       [ID_Gender],
       [ID_EmployeeStatus],
       [FullAddress],
       [Email],
       [ContactNumber],
       [Name],
       [ID_Company],
       [Comment],
       [IsSystemUsed],
       [PRCLicenseNumber],
       [PTR],
       [S2],
       [TINNumber],
       [DatePRCExpiration],
       tempEmployeeID
FROm   [Temp-2022-05-17-Employee] 
