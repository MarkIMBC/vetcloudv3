DEclare @GUID_Company VARCHAR(MAX) = '1324F5AB-A86D-413B-ADFD-05D678C09DD9'
DEclare @Inventoriable_ID_ItemType INT = 2

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     [Option]          VARCHAR(MAX),
     ID_ItemCategory   INT,
     Name_ItemCategory VARCHAR(MAX),
     UnitPrice         DECIMAL(18, 2),
     ID_ItemType       INT,
     UnitCost          DECIMAL(18, 2),
     tempID            VARCHAR(MAX)
  )

Update tItem
set    Name = dbo.fGetCleanedString(Name)

exec pInsertItemCategory
  'ANTIBIOTICS & OTHERS',
  '2'

exec pInsertItemCategory
  'BLOOD TEST CLIPS',
  '2'

exec pInsertItemCategory
  'Blood trans',
  '2'

exec pInsertItemCategory
  'Blood',
  '2'

exec pInsertItemCategory
  'Boarding',
  '2'

exec pInsertItemCategory
  'Caesarian Section',
  '2'

exec pInsertItemCategory
  'Confinement',
  '1'

exec pInsertItemCategory
  'Dental Extraction',
  '2'

exec pInsertItemCategory
  'Dental Scaling',
  '2'

exec pInsertItemCategory
  'DOWN PAYMENT',
  '1'

exec pInsertItemCategory
  'First aid',
  '1'

exec pInsertItemCategory
  'Glucose Test',
  '2'

exec pInsertItemCategory
  'Grooming',
  '2'

exec pInsertItemCategory
  'INJECTABLES',
  '2'

exec pInsertItemCategory
  'NURSING',
  '2'

exec pInsertItemCategory
  'OTHERS',
  '2'

exec pInsertItemCategory
  'PARASITICIDES',
  '2'

exec pInsertItemCategory
  'PET FOOD',
  '2'

exec pInsertItemCategory
  'PET SUPPLIES',
  '2'

exec pInsertItemCategory
  'Procedure',
  '1'

exec pInsertItemCategory
  'PROFESSIONAL FEE',
  '1'

exec pInsertItemCategory
  'Sedation',
  '2'

exec pInsertItemCategory
  'SERVICES',
  '1'

exec pInsertItemCategory
  'Sevices',
  '1'

exec pInsertItemCategory
  'SURGERY',
  '1'

exec pInsertItemCategory
  'Suture Removal',
  '1'

exec pInsertItemCategory
  'Suture',
  '2'

exec pInsertItemCategory
  'TEST KIT',
  '2'

exec pInsertItemCategory
  'Vaccines & Deworming',
  '2'

exec pInsertItemCategory
  'VACCINES',
  '2'

exec pInsertItemCategory
  'VET DIET',
  '2'

exec pInsertItemCategory
  'VET SERVICES',
  '1'

exec pInsertItemCategory
  'VITAMINS & MINERALS SUPPLEMENTS',
  '2'

exec pInsertItemCategory
  'VITAMINS & MINERALS SUPPLIMENTS',
  '2'

exec pInsertItemCategory
  'wound repair',
  '1'

INSERT @import
       (Name_Item,
        [Option],
        Name_ItemCategory,
        UnitPrice,
        UnitCost,
        tempID)
SELECT dbo.fGetCleanedString([ITEM]),
       [Option],
       [Category],
       ISNULL(TRY_CONVERT(DECIMAL(18, 2), [Price]), 0),
       ISNULL(TRY_CONVERT(DECIMAL(18, 2), [Cost]), 0),
       [GUID]
FROM   ForImport.[dbo].[PET_INNOVATION_LIST_OF_INVENTORY_AND_SERVICES]

Update @import
SET    Name_Item = Name_Item + ' ' + [Option]
WHERE  [Option] is NOT NULL

Update @import
SET    ID_ItemType = 2

Update @import
SET    ID_ItemType = 1
WHERE  Name_ItemCategory IN ( 'wound repair', 'VET SERVICES', 'SERVICES', 'Sevices',
                              'Suture Removal', 'DOWN PAYMENT', 'Procedure', 'PROFESSIONAL FEE',
                              'SURGERY', 'First aid', 'Confinement' )

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_ItemCategory = category.ID
FROM   @import import
       INNER JOIN (SELECT MAX(ID) ID,
                          Name,
                          ID_ItemType
                   FROM   tItemCategory
                   GROUP  BY Name,
                             ID_ItemType) category
               on Name_ItemCategory = category.Name
                  And import.ID_ItemType = category.ID_ItemType

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT hed.Name_Item,
       @ID_Company,
       hed.tempID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       hed.ID_ItemType
FROM   @import hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_Item IS NULL

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.tempID = item.tempID
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and item.ID_ItemType = @Inventoriable_ID_ItemType

Update tItem
SET    ID_ItemType = import.ID_itemType,
       ID_ItemCategory = import.ID_ItemCategory,
       UnitPrice = import.UnitPrice,
       UnitCost = import.UnitCost
FROM   tItem item
       INNER JOIN @import import
               on item.ID = import.ID_Item
where  item.ID_COmpany = @ID_Company

SELECT *
FROM   @import 
