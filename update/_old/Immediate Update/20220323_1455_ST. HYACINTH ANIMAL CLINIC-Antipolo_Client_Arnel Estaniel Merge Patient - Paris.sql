DECLARE @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT *
FROM   vPatient
WHERE  Code = 'PNT-00145'
       and ID_Company = @ID_Company

SELECT *
FROM   vPatient
WHERE  Code = 'PNT-00146'
       and ID_Company = @ID_Company


SELECT Date,
       Code
FROm   tPatient_SOAP
WHERE  ID_Patient IN ( 341379 )
       and ID_Company = @ID_Company
UNION ALL
SELECT Date,
       Code
FROm   tPatient_Wellness
WHERE  ID_Patient IN ( 341379 )
       and ID_Company = @ID_Company
UNION ALL
SELECT Date,
       Code
FROm   tVeterinaryHealthCertificate
WHERE  ID_Patient IN ( 341379 )
       and ID_Company = @ID_Company


Update tPatient_SOAP
SET    ID_Patient = 341376
WHERE  ID_Patient IN ( 341379)
       and ID_Company = @ID_Company

Update tPatient_Wellness
SET    ID_Patient = 341376
WHERE  ID_Patient IN ( 341379 )
       and ID_Company = @ID_Company

Update tVeterinaryHealthCertificate
SET    ID_Patient = 341376
WHERE  ID_Patient IN ( 341379 )
       and ID_Company = @ID_Company

Update tPatient
SET    IsActive = 0
WHERE  ID IN ( 341379 )
       and ID_Company = @ID_Company

SELECT Date,
       Code
FROm   tPatient_SOAP
WHERE  ID_Patient IN ( 341376 )
       and ID_Company = @ID_Company
UNION ALL
SELECT Date,
       Code
FROm   tPatient_Wellness
WHERE  ID_Patient IN ( 341376 )
       and ID_Company = @ID_Company
UNION ALL
SELECT Date,
       Code
FROm   tVeterinaryHealthCertificate
WHERE  ID_Patient IN ( 341376 )
       and ID_Company = @ID_Company