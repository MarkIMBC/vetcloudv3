DEclare @GUID_Company VARCHAR(MAX) = '42866A68-8483-4C01-8D53-AD62B8B6470A'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @BillingInvoice_Error TABLE
  (
     ID_BillingInvoice INT,
     Message           VARCHAR(MAX)
  )
DECLARE @ID_BillingInvoice VARCHAR(50) = ''
DECLARE db_cursor CURSOR FOR
  SELECT ID
  FROm   vBillingInvoice
  WHERE  ID_Company = @ID_Company
         AND IsWalkIn = 1
         and ID_FilingStatus = 1

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_BillingInvoice

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @IDs_BillingInvoice typIntLIst

      INSERT @IDs_BillingInvoice
      VALUES (@ID_BillingInvoice)

      BEGIN TRY
          exec pApproveBillingInvoice
            @IDs_BillingInvoice,
            4610

          DELETE FROM @IDs_BillingInvoice
      END TRY
      BEGIN CATCH
          INSERT @BillingInvoice_Error
          VALUES ( @ID_BillingInvoice,
                   ERROR_MESSAGE() )
      END CATCH;

      FETCH NEXT FROM db_cursor INTO @ID_BillingInvoice
  END

CLOSE db_cursor

DEALLOCATE db_cursor 

SELECT * FROm @BillingInvoice_Error