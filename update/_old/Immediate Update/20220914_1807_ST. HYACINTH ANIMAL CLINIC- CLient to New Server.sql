IF OBJECT_ID(N'Temp-2022-09-05-Client', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-05-Client];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-09-05-' + company.Guid + '-Client-'
       + Convert(Varchar(MAX), client.ID)            tempID,
       client.Name,
       client.ContactNumber,
       client.ContactNumber2,
       client.Email,
       client.Address,
       CASE
         WHEN LEN(ISNULL(client.Comment, '')) > 0 THEN CHAR(13)
         ELSE ''
       END
       + 'Imported from Old Server' Comment,
       ID_Company,
       client.DateCreated                            DateCreated_Client
INTO   [dbo].[Temp-2022-09-05-Client]
FROM   _______db_vetcloudv3_oldserver_20220914164457.[dbo].tClient client
       inner join _______db_vetcloudv3_oldserver_20220914164457.[dbo].tCompany company
               on client.ID_Company = company.ID
where  client.ID > 460966 AND company.GUID = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

INSERT @NotYetInserted_TempID
SELECT tempClient.tempID
FROm   [dbo].[Temp-2022-09-05-Client] tempClient

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tclient)

INSERT dbo.tClient
       (ID_Company,
        tempID,
        Name,
        ContactNumber,
        ContactNumber2,
        Address,
        Email,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempClient.ID_Company,
       tempClient.tempID,
       tempClient.Name,
       tempClient.ContactNumber,
       tempClient.ContactNumber2,
       tempClient.Address,
       tempClient.Email,
       Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [dbo].[Temp-2022-09-05-Client] tempClient
       inner join @NotYetInserted_TempID unInsertedTempCLient
               on tempClient.tempID = unInsertedTempCLient.tempID

SELECT *
FROm   [Temp-2022-09-05-Client] 
