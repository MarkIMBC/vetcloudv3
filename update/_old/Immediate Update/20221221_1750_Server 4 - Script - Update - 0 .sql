GO

exec _pBackUpDatabase

go

DECLARE @ErrorMessage TABLE
  (
     [Procedure] VARCHAR(MAX),
     [Message]   VARCHAR(MAX)
  )
DECLARE @tableName VARCHAR(50) -- database name 
DECLARE db_cursor CURSOR FOR
  SELECT TableName
  FROM   _tModel

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @tableName

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @sqlCreated NVARCHAR(MAX) = N'
		CREATE OR ALTER TRIGGER [dbo].[rDateCreated_/*TableName*/]
		ON [dbo].[/*TableName*/]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo./*TableName*/
			SET    DateCreated = GETDATE()
			FROM   dbo./*TableName*/ hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;'
      DECLARE @sqlModified NVARCHAR(MAX) = N'
		CREATE OR ALTER TRIGGER [dbo].[rDateModified_/*TableName*/]
		ON [dbo].[/*TableName*/]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo./*TableName*/
			SET    DateModified = GETDATE()
			FROM   dbo./*TableName*/ hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;'

      SET @sqlCreated= REPLACE(@sqlCreated, '/*TableName*/', @tableName)
      SET @sqlModified= REPLACE(@sqlModified, '/*TableName*/', @tableName)

      BEGIN TRY
          exec (@sqlCreated)
      END TRY
      BEGIN CATCH
          INSERT @ErrorMessage
          VALUES(ERROR_PROCEDURE(),
                 ERROR_MESSAGE())
      END CATCH

      BEGIN TRY
          exec (@sqlModified)
      END TRY
      BEGIN CATCH
          INSERT @ErrorMessage
          VALUES(ERROR_PROCEDURE(),
                 ERROR_MESSAGE())
      END CATCH

      FETCH NEXT FROM db_cursor INTO @tableName
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT *
FROM   @ErrorMessage 
GO


GO

if OBJECT_ID('dbo.vSMSList') is not null
  BEGIN
      DROP VIEW vSMSList;
  END

GO

if OBJECT_ID('dbo.tSMSList') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tSMSList',
        1,
        NULL,
        NULL
  END

GO

IF COL_LENGTH('dbo.tVeterinaryHealthCertificate', 'ValidityDayCount') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tVeterinaryHealthCertificate',
        'ValidityDayCount',
        2
  END

GO

exec _pRefreshAllViews

GO

ALTER VIEW [dbo].[vzVeterinaryHealthClinicReport]
AS
  SELECT vetcert.ID,
         vetcert.Code,
         vetcert.Date,
         vetcert.DestinationAddress,
         vetcert. Color,
         vetcert. Weight,
         vetcert.ID_Item,
         vetcert.Name_Item,
         CASE
           WHEN len(IsNull(vetcert.LotNumber, '')) > 0 THEN ' with Serial / Lot Number'
                                                            + vetcert.LotNumber
           ELSE ''
         END                                                                                                                                                   LotNumber,
         patient.ID                                                                                                                                            ID_Patient,
         patient.NAME                                                                                                                                          Name_Patient,
         IsNull(Species, '')                                                                                                                                   Name_Species,
         IsNull(Name_Gender, '')                                                                                                                               Name_Gender,
         patient.Microchip,
         IsNull(FORMAT(patient.DateBirth, 'MM/dd/yyyy'), '')                                                                                                   Birthdate,
         dbo.fGetAge(patient.DateBirth, CASE
                                          WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                                            Age_Patient,
         IsNull(client.NAME, '')                                                                                                                               Name_Client,
         IsNull(client.Address, '')                                                                                                                            Address_Client,
         IsNull(client.ContactNumber, '')                                                                                                                      ContactNumber_Client,
         IsNull(client.ContactNumber2, '')                                                                                                                     ContactNumber2_Client,
         IsNull(client.Email, '')                                                                                                                              Email_Client,
         company.ID                                                                                                                                            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.NAME                                                                                                                                          Name_Company,
         company.Address                                                                                                                                       Address_Company,
         company.ContactNumber                                                                                                                                 ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                                                 HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         _AttendingPhysician.TINNumber,
         _AttendingPhysician.PTR,
         _AttendingPhysician.PRCLicenseNumber,
         CASE
           WHEN _AttendingPhysician.DatePRCExpiration IS NOT NULL THEN FORMAT(_AttendingPhysician.DatePRCExpiration, 'MM/dd/yyyy')
           ELSE ''
         END                                                                                                                                                   DatePRCExpiration,
         CASE
           WHEN vetCert.DateVaccination IS NOT NULL THEN FORMAT(vetCert.DateVaccination, 'MM/dd/yyyy')
           ELSE '                '
         END                                                                                                                                                   DateVaccination,
         dbo.fGetVeterinaryHealthCertificateVaccinationOptionComment(Comment_VaccinationOption, vetCert.DateVaccination, vetCert.Name_Item, vetCert.LotNumber) Comment_VaccinationOption,
         ISNULL(vetCert.ValidityDayCount, 0)                                                                                                                   ValidityDayCount
  FROM   vVeterinaryHealthCertificate vetCert
         LEFT JOIN vPatient patient
                ON vetCert.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = vetCert.ID_Company
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = vetCert.AttendingPhysician_ID_Employee

GO

CREATE OR
ALTER VIEW vSMSList
as
  SELECT ID,
         ID              ID_Reference,
         ID_Patient_SOAP Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_SOAP_Plan
  Union All
  SELECT ID,
         ID                     ID_Reference,
         ID_Patient_Vaccination Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Vaccination_Schedule
  UNION ALL
  SELECT ID,
         ID                  ID_Reference,
         ID_Patient_Wellness Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Wellness_DetailSchedule

GO

CREATE OR
ALTER VIEW vPatientWaitingList_ListView_temp
as
  SELECT MAX(waitingList.ID)                                         ID,
         waitingList.ID_Company,
         MAX(waitingList.DateCreated)                                DateCreated,
         waitingList.Name_Client,
         waitingList.Name_Patient,
         waitingList.ID_Client,
         waitingList.ID_Patient,
         waitingList.WaitingStatus_ID_FilingStatus,
         waitingList.BillingInvoice_ID_FilingStatus,
         waitingList.WaitingStatus_Name_FilingStatus,
         ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus,
         --IsnULL(appointment.UniqueIDList, '') UniqueIDList,  
         waitingList.IsQueued,
         waitingList.Oid_Model_Reference,
         waitingList.ID_Reference
  FROM   vPatientWaitingList waitingList
  -- OUTER APPLY dbo.fGetPatientAppoinmentEventStuff(waitingList.DateCreated, waitingList.DateCreated, waitingList. ID_Patient) appointment  
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )
  Group  BY waitingList.ID_Company,
            waitingList.Name_Client,
            waitingList.Name_Patient,
            waitingList.ID_Client,
            waitingList.ID_Patient,
            waitingList.WaitingStatus_ID_FilingStatus,
            waitingList.BillingInvoice_ID_FilingStatus,
            waitingList.WaitingStatus_Name_FilingStatus,
            ISNULL(waitingList.BillingInvoice_Name_FilingStatus, '---'),
            --appointment.UniqueIDList,  
            waitingList.IsQueued,
            waitingList.Oid_Model_Reference,
            waitingList.ID_Reference

GO

CREATE OR
ALTER PROC pGeEmployeetUserInfo(@ID_Employee    INT,
                                @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      INSERT @table
      SELECT c.Name,
             c.Code,
             _user.Username
      FROM   vUser _user
             inner join tCompany c
                     on _user.ID_Company = c.ID
      WHERE  ID_Company = @ID_Company
             AND _user.ID_Employee = @ID_Employee

      SELECT '_'

      SELECT TOP 1 *
      FROm   @table
  END

GO

GO

CREATE OR
ALTER PROC pUpdateEmployeetUserInfo_validation(@ID_Employee    INT,
                                               @UserName       VARCHAR(MAX),
                                               @Password       VARCHAR(MAX),
                                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @IsActive_UserSession BIT
      DECLARE @ID_Company INT
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      SELECT '_'

      SELECT *
      FROm   @table
  END

GO

CREATE OR
ALTER PROC pUpdateEmployeetUserInfo(@ID_Employee    INT,
                                    @Username       VARCHAR(MAX),
                                    @Password       VARCHAR(MAX),
                                    @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @table TABLE
        (
           Name_Company VARCHAR(MAX),
           Code_Company VARCHAR(MAX),
           Username     VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUSer
      where  iD = @ID_User

      Update vUser
      SET    Username = @UserName,
             Password = @Password
      where  ID = @ID_User
             AND ID_Company = @ID_Company

      SELECT '_'

      SELECT @Success Success,
             @message Message
  END

GO

CREATE OR
ALTER PROC pGetPatientWaitingAppoitmentReference(@IDs_Patient typIntList READONLY,
                                                 @DateStart   DATETIme)
as
  BEGIN
      DECLARE @patientAppointment TABLE
        (
           Name_Model    VARCHAR(MAX),
           ReferenceCode VARCHAR(MAX),
           ID_Client     INT,
           ID_Patient    INT,
           Particular    VARCHAR(MAX),
           UniqueID      VARCHAR(MAX),
           Description   VARCHAR(MAX)
        )

      INSERT @patientAppointment
      SELECT DISTINCT _model.Name,
                      ReferenceCode,
                      ID_Client,
                      ID_Patient,
                      Paticular,
                      UniqueID,
                      Description
      FROM   vAppointmentEvent appntEvent
             inner join _tModel _model
                     on appntEvent.Oid_Model = _model.Oid
             INNER JOIN @IDs_Patient idsPatient
                     on idsPatient.ID = appntEvent.ID_Patient
      WHERE  CONVERT(Date, DateStart) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateStart)

      if(SELECT COUNT(*)
         FROm   @patientAppointment) = 0
        BEGIN
            INSERT @patientAppointment
                   (ID_Patient)
            VALUES(0)
        END

      SELECT '_',
             '' PatientWaitingAppoitmentReference

      SELECT GETDATE() DateStart

      select ROW_NUMBER()
               OVER(
                 PARTITION BY ID_Patient
                 ORDER BY ID_Patient ASC)                                           AS ID,
             t.ID_Patient,
             STUFF((SELECT DISTINCT ', ' + ReferenceCode
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS ReferenceCodeList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniqueIDList,
             STUFF((SELECT DISTINCT '~' + UniqueID + '|' + ISNULL(ReferenceCode, '')
                                    + '|' + ISNULL(Description, '')
                    from   @patientAppointment
                    WHERE  ID_Patient = T.ID_Patient
                    FOR XML PATH(''), TYPE). value('.', 'NVARCHAR(MAX)'), 1, 2, '') AS UniqueIDListTest
      FROM   @patientAppointment t
      GROUP  BY t.ID_Patient
  END

GO

Update _tNavigation
SET    Route = 'CurrentEmployeeInfo'
where  Name = 'EmployeeInfo_ListView'

Update _tNavigation
SET    Route = 'SMSList',
       ID_Parent = '6D6F6247-DE8B-44CC-94E4-10C16EF776F2',
       Caption = 'SMS'
where  Name = 'SMSList_ListView' 
