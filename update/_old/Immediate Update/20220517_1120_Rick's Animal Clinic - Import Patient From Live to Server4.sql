IF OBJECT_ID(N'AuditTrail-2022-05-17-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[AuditTrail-2022-05-17-Patient];

GO

IF OBJECT_ID(N'Temp-2022-05-17-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-05-17-Patient];

GO

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [AuditTrail-2022-05-17-Patient]
from   db_vetcloudv3_live_dev.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_vetcloudv3_live_dev.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_vetcloudv3_live_dev.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Model = 'Patient'
       and Description LIKE '%added%'
       AND Date >= '2022-05-01 11:00:00'
Order  by Date DESC

Declare @NotYetInserted_PatientTempID TABLE
  (
     tempPatientID VARCHAR(MAX)
  )

SELECT ISNULL(mainClient.ID, mainClient2.ID) Main_ID_Client,
       'Temp-2022-05-17-' + company.Guid
       + '-Patient-'
       + Convert(Varchar(MAX), Patient.ID)   tempPatientID,
       Patient.Name,
       Patient.DateBirth,
       Patient.Species,
       Patient.IsNeutered,
       Patient.IsDeceased,
       Patient.AnimalWellness,
       Patient.DateDeceased,
       Patient.DateLastVisited,
       Patient.ProfileImageFile,
       Patient.CustomCode,
       Patient.WaitingStatus_ID_FilingStatus,
       Patient.Comment,
       Patient.ID_Company
INTO   [dbo].[Temp-2022-05-17-Patient]
FROM   db_vetcloudv3_live_dev.[dbo].vPatient Patient
       inner join db_vetcloudv3_live_dev.[dbo].tCompany company
               on Patient.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-05-17-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient.ID_Client)
                 and mainClient.ID_Company = Patient.ID_Company
       LEFT JOIN tClient mainClient2
              on mainClient2.ID = Patient.ID_Client
                 and mainClient2.ID_Company = Patient.ID_Company
where  Patient.ID IN (SELECT ID_CurrentObject
                      FROM   [AuditTrail-2022-05-17-Patient])
       and Guid = '17C7284C-0CE9-4206-A461-F2E98FF82F2E'
ORDER  BY Patient.ID ASC

INSERT @NotYetInserted_PatientTempID
SELECT tempPatientID
FROm   [dbo].[Temp-2022-05-17-Patient]

DELETE FROM @NotYetInserted_PatientTempID
WHERE  tempPatientID IN (SELECT tempID
                         FROM   tPatient)

INSERT tPatient
       (ID_Company,
        ID_Client,
        tempID,
        Name,
        DateBirth,
        Species,
        IsNeutered,
        IsDeceased,
        AnimalWellness,
        DateDeceased,
        DateLastVisited,
        ProfileImageFile,
        CustomCode,
        WaitingStatus_ID_FilingStatus,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy
		)
SELECT tempPatient.ID_Company,
       tempPatient.Main_ID_Client,
       tempPatient.tempPatientID,
       tempPatient.Name,
       tempPatient.DateBirth,
       tempPatient.Species,
       tempPatient.IsNeutered,
       tempPatient.IsDeceased,
       tempPatient.AnimalWellness,
       tempPatient.DateDeceased,
       tempPatient.DateLastVisited,
       tempPatient.ProfileImageFile,
       tempPatient.CustomCode,
       tempPatient.WaitingStatus_ID_FilingStatus,
       tempPatient.Comment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [Temp-2022-05-17-Patient] tempPatient
       inner join tClient mainClient
               on mainClient.ID = tempPatient.Main_ID_Client
                  and mainClient.ID_Company = tempPatient.ID_Company
       INNER JOIn @NotYetInserted_PatientTempID unInsertedPatienttempIDs
               on tempPatient.tempPatientID = unInsertedPatienttempIDs.tempPatientID
order  by Name 
