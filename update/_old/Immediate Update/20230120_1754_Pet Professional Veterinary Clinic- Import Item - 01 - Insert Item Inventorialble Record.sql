DECLARE @GUID_Company VARCHAR(MAX) = '3950FD88-A135-42E9-B52D-2F423BA9E870'
DECLARE @ID_ItemType INT = 2
DECLARE @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')

-----------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Item           INT,
     Name_Item         VARCHAR(MAX),
     CurrentInventoryCount           INT,
     UnitCost         Decimal(18, 2),
     UnitPrice         Decimal(18, 2),
     GUID              VARCHAR(MAX)
  )

INSERT @import
       (Name_Item,
	   CurrentInventoryCount,
        UnitCost,
        UnitPrice,
        GUID)
SELECT dbo.fGetCleanedString([ITEM NAME ]),
       dbo.fGetCleanedString([QUANTITY on HAND]),
       TRY_CONVERT(Decimal(18, 2), dbo.fGetCleanedString([BUYING PRICE ])),
       TRY_CONVERT(Decimal(18, 2), dbo.fGetCleanedString([SELLING PRICE])),
       [GUID]
FROM   ForImport.[dbo].[PetProfessionalITEMS]


--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.Name_Item = item.Name
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.GUID = item.tempID
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

-------------------------------------------
INSERT INTO [dbo].[tItem]
            ([Name],
             [IsActive],
             [ID_Company],
             tempID,
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType])
SELECT DISTINCT Name_Item,
                1,
                @ID_Company,
                Guid,
                @Comment,
                GETDATE(),
                GETDATE(),
                1,
                1,
                @ID_ItemType
FROM   @import
where  LEN(Name_Item) > 0
       and ID_Item IS NULL

--------------------------------------------
Update @import
SET    ID_Item = item.ID
FROM   @import import
       inner join vActiveItem item
               on import.GUID = item.tempID
where  item.ID_Company = @ID_Company
       and ID_ItemType = @ID_ItemType

-------------------------------------------
Update tItem
SET    Name = import.Name_Item,
       UnitCost = import.UnitCost,
       UnitPrice = import.UnitPrice
FROM   tItem item
       inner join @import import
               on item.ID = import.ID_Item
WHERE  item.ID_Company = @ID_Company
       and IsActive = 1
       AND ID_ItemType = @ID_ItemType

SELECT *
FROM   @import

