GO

exec _pBackUpDatabase

GO

DEclare @GUID_Company VARCHAR(MAX) = '1C7B30E3-DEC1-49A3-8073-B8425190EED8'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
Update tBillingInvoice_Detail
SET    UnitCost = item.UnitCost
FROm   tBillingInvoice_Detail biDetail
       inner join tItem item
               on biDetail.ID_Item = item.ID
       inner join tBillingInvoice biHed
               on biHed.ID = biDetail.ID_BillingInvoice
where  ISNULL(biDetail.UnitCost, 0) = 0
       and ISNULL(item.UnitCost, 0) > 0
       and biHed.ID_Company = @ID_Company 
