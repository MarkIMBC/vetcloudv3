DECLARE @GUID_Company VARCHAR(MAX) = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'
DECLARE @SourceID_Client INT =338343
DECLARE @DestinationID_Client INT =271076
DECLARE @SourceID_Patient INT =446264
DECLARE @DestinationID_Patient INT =351755

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

declare @Transaction TABLE
  (
     ID_Reference INT,
     Code         VARCHAR(MAX),
     Name_Client  VARCHAR(MAX),
     Name_Patient VARCHAR(MAX)
  )

SELECT *
FROM   tClient
WHERE  ID = @SourceID_Client

SELECT *
FROM   tPatient
WHERE  ID_Client = @SourceID_Client

SELECT *
FROM   tClient
WHERE  ID = @DestinationID_Client

SELECT *
FROM   tPatient
WHERE  ID_Client = @DestinationID_Client

SELECT ID,
       Name 'SourceClient'
FROM   tClient
WHERE  ID = @SourceID_Client

SELECT ID,
       Code,
       ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vPatient_SOAP
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT ID,
       Code,
       ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vVeterinaryHealthCertificate
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       hed.ID_Client,
       hed.Name_Client,
       biPatient.Name_Patient,
       biPatient.ID_Patient
FROM   vBillingInvoice hed
       inner join vBillingInvoice_Patient biPatient
               on hed.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       hed.ID_Client,
       hed.Name_Client,
       conPatient.Name_Patient,
       conPatient.ID_Patient
FROM   vPatient_Confinement hed
       inner join vPatient_Confinement_Patient conPatient
               on hed.ID = conPatient.ID_Patient_Confinement
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       ID_Client,
       hed.Name_Client,
       hed.Name_Patient,
       hed.ID_Patient
FROM   vPatient_Wellness hed
WHERE  ID_Client = @SourceID_Client
       AND hed.ID_Company = @ID_Company

Update tPatient_Wellness
SET    ID_Patient = @DestinationID_Patient
WHERE  ID_Patient = @SourceID_Patient
       AND ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company

Update tPatient_Wellness
SET    ID_Client = @DestinationID_Client
WHERE  ID_Patient = @DestinationID_Patient
       AND ID_Company = @ID_Company

Update tBillingInvoice
SET    ID_Client = @DestinationID_Client
WHERE  ID_Client = @SourceID_Client
       AND ID_Company = @ID_Company

Update tBillingInvoice_Patient
SET    ID_Patient = @DestinationID_Patient
FROM   tBillingInvoice billingInvoice
       inner join tBillingInvoice_Patient biPatient
               on billingInvoice.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @SourceID_Client
       AND billingInvoice.ID_Company = @ID_Company

Update tBillingInvoice_Patient
SET    ID_Patient = @DestinationID_Patient
FROM   tBillingInvoice billingInvoice
       inner join tBillingInvoice_Patient biPatient
               on billingInvoice.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @DestinationID_Client
       AND billingInvoice.ID_Company = @ID_Company

Update tClient SET IsActive = 0 WHERE ID = @SourceID_Client  AND ID_Company = @ID_Company
Update tPatient SET IsActive = 0 WHERE ID_Client = @SourceID_Client AND IsActive = 1  AND ID_Company = @ID_Company


SELECT ID,
       Name 'Destination Client', IsActive
FROM   tClient
WHERE  ID = @DestinationID_Client

SELECT ID,
       Code,
       ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vPatient_SOAP
WHERE  ID_Client = @DestinationID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT ID,
       Code,
       ID_Client,
       Name_Client,
       Name_Patient,
       ID_Patient
FROM   vVeterinaryHealthCertificate
WHERE  ID_Client = @DestinationID_Client
       AND ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       hed.ID_Client,
       hed.Name_Client,
       biPatient.Name_Patient,
       biPatient.ID_Patient
FROM   vBillingInvoice hed
       inner join vBillingInvoice_Patient biPatient
               on hed.ID = biPatient.ID_BillingInvoice
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       hed.ID_Client,
       hed.Name_Client,
       conPatient.Name_Patient,
       conPatient.ID_Patient
FROM   vPatient_Confinement hed
       inner join vPatient_Confinement_Patient conPatient
               on hed.ID = conPatient.ID_Patient_Confinement
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company
UNION ALL
SELECT hed.ID,
       hed.Code,
       ID_Client,
       hed.Name_Client,
       hed.Name_Patient,
       hed.ID_Patient
FROM   vPatient_Wellness hed
WHERE  ID_Client = @DestinationID_Client
       AND hed.ID_Company = @ID_Company 


