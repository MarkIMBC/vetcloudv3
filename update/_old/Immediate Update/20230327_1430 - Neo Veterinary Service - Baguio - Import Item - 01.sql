DEclare @GUID_Company VARCHAR(MAX) = '791EAF7B-FCAC-4EA9-B29B-3440B55F25B1'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
DECLARE @import TABLE
  (
     ID_Client             INT,
     Name_Client           VARCHAR(MAX),
     ContactNumber_Client  VARCHAR(MAX),
     ContactNumber2_Client VARCHAR(MAX),
     ID_Patient            INT,
     Name_Patient          VARCHAR(MAX),
     Bread_Patient         VARCHAR(MAX),
     Specie_Patient        VARCHAR(MAX),
     ID_Gender             INT,
     Gender_Patient        VARCHAR(MAX),
     tempID                VARCHAR(MAX),
     RowIndex              INT,
     tempIDClient          VARCHAR(MAX)
  )
DEclare @Comment VARCHAR(MAX) = 'Imported as of '
  + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')

INSERT @import
       (Name_Client,
        ContactNumber_Client,
        Name_Patient,
        Bread_Patient,
        Specie_Patient,
        Gender_Patient,
        tempID,
        RowIndex)
SELECT Column1
       + CASE
           WHEN LEN(ISNULL([NAME OF CLIENT], '')) > 0 THEN ' '
           ELSE ''
         END
       + [NAME OF CLIENT],
       [CONTACT NO.],
       [NAME OF PET],
       BREED,
       SPECIES,
       SEX,
       GUID,
       RowIndex
FROM   ForImport.[dbo].[PATIENTS AND OWNERS 3]
Order  by CONVERT(INT, RowIndex)

DELETE FROM @import
WHERE  RowIndex = 1

Update @import
SET    tempIDClient = tempID + TRIM(Name_Client)
                      + CONVERT(varchar(MAX), RowIndex)

Update @import
SET    ContactNumber_Client = REPLACE(ContactNumber_Client, '-', '')

Update @import
SET    Specie_Patient = Specie_Patient
                        + CASE
                            WHEN LEN(ISNULL(Bread_Patient, '')) > 0 THEN ' - '
                            ELSE ''
                          END
                        + Bread_Patient

Update @import
SET    ID_Gender = CASE
                     WHEN Gender_Patient = 'M' THEN 1
                     ELSE
                       CASE
                         WHEN Gender_Patient = 'F' THEN 2
                         ELSE NULL
                       END
                   END

Update @import
set    ID_Patient = patient.ID
FROM   tPatient patient
       inner join @import import
               on patient.tempID = import.tempID
where  patient.ID_Company = @ID_Company

Update @import
set    ID_Client = client.ID
FROM   tClient client
       inner join @import import
               on client.tempID = import.tempIDClient
where  client.ID_Company = @ID_Company

INSERT INTO [dbo].[tClient]
            ([Name],
             [ContactNumber],
             [ID_Company],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [tempID],
             Comment)
SELECT Name_Client,
       ContactNumber_Client,
       @ID_Company,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       tempIDClient,
       @Comment
FROM   @import
where  LEN(Name_Client) > 0
       and ID_Client IS NULL

Update @import
set    ID_Client = client.ID
FROM   tClient client
       inner join @import import
               on client.tempID = import.tempIDClient
where  client.ID_Company = @ID_Company

INSERT INTO [dbo].tPatient
            (ID_Client,
             [Name],
             Species,
             ID_Gender,
             [ID_Company],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [tempID],
             Comment)
SELECT ID_Client,
       Name_Patient,
       Specie_Patient,
       ID_Gender,
       @ID_Company,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       tempID,
       @Comment
FROM   @import
where  LEN(Name_Client) > 0
       and ID_Client IS NOT NULL
       AND ID_Patient IS NULL

Update @import
set    ID_Patient = patient.ID
FROM   tPatient patient
       inner join @import import
               on patient.tempID = import.tempID
where  patient.ID_Company = @ID_Company

SELECT *
FROM   @import

Update tPatient
set    Species = import.Specie_Patient
FROM   tPatient patient
       inner join @import import
               on patient.ID = import.ID_Patient
where  patient.ID_Company = @ID_Company

Update tClient
set    ContactNumber = import.ContactNumber_Client,
       ContactNumber2 = import.ContactNumber2_Client
FROM   tClient patient
       inner join @import import
               on patient.ID = import.ID_Client
where  patient.ID_Company = @ID_Company 
