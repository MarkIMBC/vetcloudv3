DECLARE @GUID_Company VARCHAR(MAX) = '1F2F41DD-AD24-4C46-AC15-09C7591C7ADA'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @SMSTemplate_OLd VARCHAR(MAX) = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. '
  + 'This is an automated SMS reminder please DO NOT REPLY OR CALL HERE. '
  + 'You may reach /*CompanyName*/ at /*ContactNumber*/. '
  + 'Thank you.'
------
DECLARE @SMSTemplate VARCHAR(MAX) = 'Hi, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/. '
  + '/*br*/'
  + 'This is an automated SMS reminder please DO NOT REPLY OR CALL HERE. '
  + '/*br*/'
  + 'You may reach /*CompanyName*/ at /*ContactNumber*/. '
  + '/*br*/' + 'Thank you.' + '/*br*/'
  --+ 'Reminders: Out of stock pa ang  Antirabies Vaccine. Mga next week pa kami magkaka-restock at magte-text na lang kami sa kanila if ok na ulit. Salamat po.'

SEt @SMSTemplate = REPLACE(@SMSTemplate, '/*br*/', CHAR(13))

SELECT @SMSTemplate_OLd = SOAPPlanSMSMessage
FROM   tCompany
WHERE  Guid = @GUID_Company

--Update tCompany
--set    SOAPPlanSMSMessage = @SMSTemplate
--where  ID = @ID_Company
SELECT ID,
       @SMSTemplate_OLd SMSTemplate_OLd,
       SOAPPlanSMSMessage
FROM   vCompanyActive
where  ID = @ID_Company

SELECT Name_Company,
       Message
FROM   dbo.fGetSendSoapPlanDateCovered('2022-10-10', '2022-10-10', NULL, @ID_Company)
Order  By DateCreated DESC 
