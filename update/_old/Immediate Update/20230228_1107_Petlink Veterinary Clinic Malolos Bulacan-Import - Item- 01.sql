DEclare @GUID_Company VARCHAR(MAX) = '9F3CEF51-A356-4477-BAF9-8F2857875664'
 
IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company


DELETE FROM ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
WHERE GUID IN (



SELECT MAX(GUID) FROM ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
GROUP BY Name
HAVING COUNT(*) > 1

)


SELECT * FROM  ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]


----------------------------------------------------------------------------------------
Update ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
SET    UnitCost = 0
WHERE  UnitCost = 'NULL'

Update ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
SET    UnitPrice = 0
WHERE  UnitPrice = 'NULL'

Update ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
SET    ID_ItemCategory = NULL
WHERE  ID_ItemCategory = 'NULL'

INSERT INTO [dbo].[tItem]
            ([Name],
             [ID_Company],
             tempID,
             [Comment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [UnitCost],
             [UnitPrice])
SELECT DISTINCT hed.[Name],
       @ID_Company,
       GUID,
       'Imported as of '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt') [Comment],
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       2,
       cat.ID,
       hed.[UnitCost],
       hed.[UnitPrice]
FROM   ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items] hed
       LEFT join vItemCategory cat
              on hed.Name_ItemCategory = cat.Name
where  hed.ID_ItemType = 2
Order  by hed.Name

Update ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
SET    ID_ItemCategory = NULL

Update ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items]
SET    ID_ItemCategory = cat.ID
FROM   ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items] hed
       INNER JOIN tItemCategory cat
               on hed.Name_ItemCategory = cat.Name
where  hed.ID_ItemType = 2

Update tItem
SET    ID_ItemCategory = excel.ID_ItemCategory
FROM   tItem hed
       INNER JOIN ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items] excel
               on hed.tempID = excel.GUID
where  hed.ID_ItemType = 2
       AND hed.ID_Company = @ID_Company

SELECT hed.ID,
       hed.Name,
       hed.ID_ItemCategory
FROM   vItem hed
       INNER JOIN ForImport.[dbo].[20230228_1035_PetLink Veterinary Clinic - Acacia Branch - Items] excel
               on hed.tempID = excel.GUID
where  hed.ID_ItemType = 2
       AND hed.ID_Company = @ID_Company

--------------------------------------------------------------------------------------------------------------------------------------------------

Update tItem set Name =dbo.fGetCleanedString(Name)


DECLARE @IDs_Item typIntList 

INSERT @IDs_Item
SELECT MAX(ID) FROM vActiveItem where ID_Company = @ID_Company AND ID_ItemType = 2
GROUP BY Name 
Having COUNT(*) > 1


SELECT * FROM @IDs_Item
