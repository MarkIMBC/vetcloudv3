IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient'
    AND COLUMN_NAME = 'DateLastVisited'
) = 0
BEGIN

	exec _pAddModelProperty 'tPatient', 'DateLastVisited', 5
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tBillingInvoice_Detail'
    AND COLUMN_NAME = 'UnitCost'
) = 0
BEGIN

	exec _pAddModelProperty 'tBillingInvoice_Detail', 'UnitCost', 3
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tEmployee'
    AND COLUMN_NAME = 'PRCLicenseNumber'
) = 0
BEGIN

	exec _pAddModelProperty 'tEmployee', 'PRCLicenseNumber', 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tEmployee'
    AND COLUMN_NAME = 'PTR'
) = 0
BEGIN

	exec _pAddModelProperty 'tEmployee', 'PTR', 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tEmployee'
    AND COLUMN_NAME = 'S2'
) = 0
BEGIN

	exec _pAddModelProperty 'tEmployee', 'S2', 1
END;

GO

exec _pRefreshAllViews

GO

CREATE OR ALTER VIEW vzSalesIncomentReport
AS
	SELECT 
		ID_Company,
		company.ImageLogoLocationFilenamePath,
		company.Name Name_Company,
		company.Address Address_Company,
		CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company,
		ID_Client,
		LTRIM(RTRIM(Name_Client)) Name_Client,
		ID_Item,
		LTRIM(RTRIM(Name_Item)) Name_Item,
		Convert(Date, biHed.Date) Date_BillingInvoice, 
		SUM(Quantity) TotalQuantity,
		ISNULL(UnitCost, 0) UnitCost,
		ISNULL(UnitPrice, 0) UnitPrice, 
		(ISNULL(UnitPrice, 0) - ISNULL(UnitCost, 0)) * SUM(Quantity) NetCost
	FROM vBillingInvoice biHed 
	INNER JOIN vCompany company 
		ON company.ID = biHed.ID_Company
	Inner JOIN vBillingInvoice_Detail biDetail 
		ON biHed.ID = biDetail.ID_BillingInvoice
	WHERE
		biHed.ID_FilingStatus IN (3)
	GROUP BY 	
		ID_Company,
		company.ImageLogoLocationFilenamePath,
		company.Name,
		company.Address,
		CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END ,
		Convert(Date, biHed.Date),
		ID_Client,
		Name_Client,
		ID_Item,
		Name_Item,
		UnitCost,
		UnitPrice
GO

CREATE OR ALTER VIEW vzVeterinaryHealthClinicReport
AS
	SELECT
		 patient.ID
		,patient.Name Name_Patient
		,ISNULL(Species,'') Name_Species
		,ISNULL(Name_Gender,'') Name_Gender
		,dbo.fGetAge(patient.DateBirth, case WHEN ISNULL(patient.IsDeceased,0) = 1 then  patient.DateDeceased ELSE NULL END ,'N/A') Age_Patient
		,ISNULL(client.Name,'')  Name_Client
		,ISNULL(client.Address,'') Address_Client
		,ISNULL(client.ContactNumber,'') ContactNumber_Client
		,ISNULL(client.ContactNumber2,'') ContactNumber2_Client
		,ISNULL(client.Email,'') Email_Client
		,company.ImageLogoLocationFilenamePath
		,company.Name Name_Company
		,company.Address Address_Company
		,company.ContactNumber ContactNumber_Company
		,CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
		  CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
		  CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company
	FROM vPatient patient 
	LEFT JOIN dbo.tClient client
		ON client.ID = patient.ID_Client
	LEFT JOIN dbo.vCompany company
	  ON company.ID = patient.ID_Company
GO

ALTER VIEW [dbo].[vzPatientBillingInvoiceReport]
AS
SELECT  biHed.ID,
       biHed.Code,
       biHed.Date,
       biHed.Name_Patient,
       biHed.BillingAddress,
       biHed.Name_FilingStatus,
       biHed.CreatedBy_Name_User,
       biHed.ApprovedBy_Name_User,
       biHed.Name_TaxScheme,
       biHed.DateApproved,
	   biHed.SubTotal,
	   ISNULL(biHed.DiscountRate, 0) DiscountRate,
	   ISNULL(biHed.DiscountAmount, 0) DiscountAmount,
	   biHed.TotalAmount,
       biHed.GrossAmount,
       biHed.VatAmount,
       biHed.NetAmount,
       biDetail.ID ID_BillingInvoice_Detail,
       biDetail.Name_Item,
       biDetail.Quantity,
       biDetail.UnitPrice,
       biDetail.Amount,
       biHed.ID_Company,
       biHed.Name_Client,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   company.ContactNumber ContactNumber_Company,
	   CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	   CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	   CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company
FROM dbo.vBillingInvoice biHed
    LEFT JOIN dbo.vBillingInvoice_Detail biDetail
        ON biHed.ID = biDetail.ID_BillingInvoice
    LEFT JOIN dbo.vCompany company
        ON company.ID = biHed.ID_Company
GO

CREATE OR ALTER   VIEW [dbo].[vzPatientSOAP]
AS
	SELECT
	  patientSOAP.ID
	 ,patientSOAP.Code
	 ,patientSOAP.Name_Client
	 ,patientSOAP.Name_Patient
	 ,patientSOAP.Date
	 ,patientSOAP.Comment
	 ,patientSOAP.Name_CreatedBy
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.History, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') History
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClinicalExamination, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClinicalExamination
	 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Diagnosis, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Diagnosis
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Treatment, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Treatment
	 ,dbo.fGetPatient_SOAP_Prescription_String(patientSOAP.ID) Prescription
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.ClientCommunication, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') ClientCommunication
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Subjective
	 ,REPLACE(REPLACE(REPLACE(patientSOAP.Objective, CHAR(9) + CHAR(13), '<br/>'), CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
	 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
	 ,patientSOAP.ID_Company
	 ,patientSOAP.Name_FilingStatus
	 ,company.ImageLogoLocationFilenamePath
	 ,company.Name Name_Company
	 ,company.Address Address_Company
	 ,company.ContactNumber ContactNumber_Company
	 ,CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
	  CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
	  CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company
	 ,CASE WHEN emp.ID_Position IN (3, 10) THEN emp.PRCLicenseNumber ELSE  '' END AttendingPhysician_PRCLicenseNumber_Employee
	 ,CASE WHEN emp.ID_Position IN (3, 10) THEN emp.PTR ELSE  '' END AttendingPhysician_PTR_Employee
	 ,CASE WHEN emp.ID_Position IN (3, 10) THEN emp.S2 ELSE  '' END AttendingPhysician_S2_Employee
	 ,CASE WHEN emp.ID_Position IN (3 ,10) THEN patientSOAP.AttendingPhysician_Name_Employee ELSE  '' END AttendingPhysician_Name_Employee
	FROM dbo.vPatient_SOAP patientSOAP
	LEFT JOIN dbo.vCompany company
	  ON company.ID = patientSOAP.ID_Company
	LEFT JOIN tEmployee emp 
		ON patientSOAP.AttendingPhysician_ID_Employee = emp.ID
GO

ALTER VIEW [dbo].[vzPaymentTransactionSummaryReport]
as

	SELECT	biHed.ID ID_BillingInvoice,
			pHed.ID ID_PaymentTransaction,
			biHed.Code Code_BillingInvoice,
			biHed.Date Date_BillingInvoice,
			biHed.TotalAmount BalanceAmount ,
			pHed.Code Code_PaymentTransaction,
			pHed.Date Date_PaymentTransaction,
			client.Name Name_Client,
			patient.Name Name_Patient,
			biHed.ID_Company,
			pHed.ID_PaymentMethod,
			payMethod.Name Name_PaymentMethod,
			phed.PayableAmount,
			CASE WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount ELSE phed.PaymentAmount END PaymentAmount,
			pHed.RemainingAmount,
			company.ImageLogoLocationFilenamePath,
			company.Name Name_Company,
			company.Address Address_Company,
			CASE WHEN LEN(company.Address) > 0 THEN '' + company.Address ELSE '' END +
			CASE WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber ELSE '' END + 
			CASE WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email ELSE '' END HeaderInfo_Company 
	FROM    tPaymentTransaction pHed 
	INNER JOIN tBillingInvoice biHed 
		on biHed.ID = pHed.ID_BillingInvoice
	LEFT JOIN tClient client 
		ON client.ID = biHed.ID_Client
	LEFT JOIN tPatient patient 
		ON patient.ID = biHed.ID_Patient
	LEFT JOIN tPaymentMethod payMethod
		ON payMethod.ID = pHed.ID_PaymentMethod
	LEFT JOIN dbo.vCompany company
		ON company.ID = biHed.ID_Company
	WHERE biHed.Payment_ID_FilingStatus IN (11, 12) AND pHed.ID_FilingStatus IN (3)
GO

CREATE OR ALTER PROC pUpdatePatientsLastVisitedDate(@IDs_Patient typIntList READONLY)
AS
BEGIN

	DECLARE @tbl TABLE (ID_Patient INT, DateLastVisited DATETIME)

	INSERT @tbl (ID_Patient)
	SELECT  ID
	FROM    @IDs_Patient

	UPDATE  @tbl
		SET DateLastVisited = rec.DateLastVisited
	FROM @tbl tbl 
	inner join 
	(
		SELECT MAX(Date) DateLastVisited, soap.ID_Patient 
		FROM    tPatient_SOAP soap
		INNER JOIN @tbl tbl
			ON tbl.ID_Patient = soap.ID_Patient
		WHERE 
			ID_FilingStatus IN (1, 3)
		GROUP BY 
			soap.ID_Patient 
	) rec ON rec.ID_Patient = tbl.ID_Patient

	UPDATE tPatient 
		SET DateLastVisited = tbl.DateLastVisited
	FROM tPatient patient
	INNER JOIN @tbl tbl 
		ON tbl.ID_Patient = patient.ID
		
END

GO

ALTER PROC [dbo].[pCancelPatient_SOAP]
(
    @IDs_Patient_SOAP typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Canceled_ID_FilingStatus INT = 4;
    DECLARE @Pending_ID_FilingStatus INT = 2;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;
		DECLARE @IDs_Patient typIntList

		INSERT @IDs_Patient
		SELECT 
			DISTINCT ID_Patient
		FROM    tPatient_SOAP soap
		INNER JOIN @IDs_Patient_SOAP tbl
			ON soap.ID = tbl.ID

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelPatient_SOAP_validation @IDs_Patient_SOAP,
                                                  @ID_UserSession;

        UPDATE dbo.tPatient_SOAP
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tPatient_SOAP bi
            INNER JOIN @IDs_Patient_SOAP ids
                ON bi.ID = ids.ID;

		exec dbo.pUpdatePatientsLastVisitedDate @IDs_Patient

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient]
(
    @ID_CurrentObject VARCHAR(10),
    @IsNew BIT = 0
)
AS
BEGIN

    IF @IsNew = 1
    BEGIN

        /* Generate Document Series */
        DECLARE @Oid_Model UNIQUEIDENTIFIER;
        DECLARE @Code VARCHAR(MAX) = '';
        DECLARE @ID_Company INT;

        SELECT @ID_Company = ID_Company
        FROM dbo.tPatient
        WHERE ID = @ID_CurrentObject;

        SELECT @Oid_Model = m.Oid
        FROM dbo._tModel m
        WHERE m.TableName = 'tPatient';

        SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

        UPDATE dbo.tDocumentSeries
        SET Counter = Counter + 1
        WHERE ID_Model = @Oid_Model
              AND ID_Company = @ID_Company;

        UPDATE dbo.tPatient
        SET Code = @Code
        WHERE ID = @ID_CurrentObject;

    END;

	DECLARE @IDs_Patient typIntList

	INSERT @IDs_Patient 
	VALUES (@ID_CurrentObject)
	
	EXEC dbo.pUpdatePatientsLastVisitedDate @IDs_Patient

END;

GO

ALTER PROC [dbo].[pGetBillingInvoice]
    @ID INT = -1,
    @ID_Client INT = NULL,
    @ID_Patient INT = NULL,
    @ID_Session INT = NULL,
	@ID_Patient_SOAP INT = NULL,
	@AttendingPhysician_ID_Employee INT = NULL
AS
BEGIN
    SELECT '_',
           '' BillingInvoice_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               fs.Name Name_FilingStatus,
               client.Name Name_Client,
			   patient.Name Name_Patient,
			   client.Address BillingAddress,
			   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- NEW -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   GETDATE() AS Date,
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   0 AS [ID_CreatedBy],
                   0 AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
                   0 AS [ID_Taxscheme],
                   @ID_Client ID_Client,
                   0 IsComputeDiscountRate,
                   @ID_Patient ID_Patient,
				   @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
            LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
            LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
			LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
				ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vBillingInvoice H
        WHERE H.ID = @ID;
    END;

	if ISNULL(@ID_Patient_SOAP, 0) > 0 AND (@ID = -1)
	BEGIN

		SELECT 
		 
			(ROW_NUMBER() OVER(ORDER BY soapPrescription.ID DESC)) - 999999  ID,
			soapPrescription.ID_Item, 
			soapPrescription.Name_Item, 
			ISNULL(soapPrescription.Quantity, 0) Quantity,
			ISNULL(item.UnitPrice, 0) UnitPrice,
			item.OtherInfo_DateExpiration DateExpiration
		FROM dbo.vPatient_SOAP_Prescription soapPrescription
		INNER JOIN tItem item 
			ON item.ID = soapPrescription.ID_Item
		WHERE ID_Patient_SOAP = @ID_Patient_SOAP;
	END
	ELSE
	BEGIN

		SELECT *
		FROM dbo.vBillingInvoice_Detail
		WHERE ID_BillingInvoice = @ID;
	END

END;
GO

ALTER   PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout]
(
    @ID_PaymentTransaction INT
)
AS
BEGIN

	DECLARE @isAutoPrint BIT = 1

	DECLARE @Cash_ID_PaymentMethod INT = 1
	DECLARE @Check_ID_PaymentMethod INT = 2
	DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
	DECLARE @GCash_ID_PaymentMethod INT = 3
	DECLARE @millisecondDelay INT = 1000


	DECLARE @billingItemsLayout VARCHAR(MAX)= ''
	DECLARE @paymentLayout VARCHAR(MAX)= ''

	Declare @ID_Company int	= 0
	Declare @ID_BillingInvoice int	= 0
	Declare @Name_Company VARCHAR(MAX)= ''
	Declare @Address_Company VARCHAR(MAX)= ''
	Declare @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
	Declare @ContactNumber_Company VARCHAR(MAX)= ''
	Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
	Declare @ID_PaymentMode int	= 0

	Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
	Declare @CheckNumber VARCHAR(MAX)= ''
	Declare @CardNumber VARCHAR(MAX)= ''
	Declare @Name_CardType VARCHAR(MAX)= ''
	Declare @CardHolderName VARCHAR(MAX)= ''
	
	Declare @Name_PaymentStatus VARCHAR(MAX)= ''

	Declare @CashAmount DECIMAL(18, 4) = 0.00
	Declare @GCashAmount DECIMAL(18, 4) =  0.00
	Declare @CardAmount DECIMAL(18, 4) =  0.00
	Declare @CheckAmount DECIMAL(18, 4) =  0.00
	
	Declare @PayableAmount DECIMAL(18, 4) =  0.00
	Declare @PaymentAmount DECIMAL(18, 4) =  0.00
	Declare @ChangeAmount DECIMAL(18, 4) =  0.00
	Declare @RemainingAmount DECIMAL(18, 4) =  0.00

	Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
	Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
	
	Declare @Name_Client VARCHAR(MAX)= ''
	Declare @Name_Patient VARCHAR(MAX)= ''
	Declare @Date_BillingInvoice datetime
	Declare @Code_BillingInvoice VARCHAR(MAX)= ''

	SELECT  @ID_Company = ID_Company,
			@ID_BillingInvoice = ID_BillingInvoice,
			@ID_PaymentMode = ID_PaymentMethod,
			@Code_PaymentTranaction = Code,
			@PayableAmount = PayableAmount,
			@RemainingAmount = RemainingAmount,
			@ChangeAmount = ChangeAmount,
			@CashAmount = CashAmount,
			@ReferenceTransactionNumber = ReferenceTransactionNumber,
			@GCashAmount = GCashAmount,
			@Name_CardType = Name_CardType,
			@CardNumber = CardNumber,
			@CardHolderName = CardHolderName,
			@CardAmount = CardAmount,
			@CheckNumber = CheckNumber,
			@CheckAmount = CheckAmount,
			@PaymentAmount = PaymentAmount
	FROM    vPaymentTransaction 
	WHERE 
		ID = @ID_PaymentTransaction

	SELECT  @Name_Company = ISNULL(Name,'N/A'),
			@Address_Company = ISNULL(Address,''),
			@ContactNumber_Company = ISNULL(ContactNumber,''),
			@ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath,'')
	FROM    vCompany 
	WHERE ID = @ID_Company

	SELECT  @Name_Client = ISNULL(bi.Name_Client,''),
			@Name_Patient = ISNULL(bi.Name_Patient,'N/A'),
			@Date_BillingInvoice = Date,
			@Code_BillingInvoice = Code,
			@SubTotal_BillingInvoice = SubTotal,
			@DiscountRate_BillingInvoice = DiscountRate,
			@DiscountAmount_BillingInvoice = DiscountAmount,
			@TotalAmount_BillingInvoice = TotalAmount, 
			@Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus,'')
	FROM    vBillingInvoice bi
	WHERE
		bi.ID = @ID_BillingInvoice

	SELECT  @billingItemsLayout = @billingItemsLayout + '
			<div class="display-block clearfix">
				<span class="bold">'+  biDetail.Name_Item +'</span><br/>
				<span class="float-left">
					&nbsp;&nbsp;&nbsp;'+  FORMAT(biDetail.Quantity,'#,#0.##') +'
				</span>
				<span class="float-right">'
					+  FORMAT(biDetail.Amount,'#,#0.00') +
				'</span>
			</div>'
	FROM    vBillingInvoice_Detail biDetail
	WHERE 
		biDetail.ID_BillingInvoice = @ID_BillingInvoice

	if @ID_PaymentMode = @Cash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Cash Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@CashAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	else if @ID_PaymentMode = @GCash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Ref. No.
				</span>
				<span class="float-right">'
					+ @ReferenceTransactionNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					G-Cash
				</span>
				<span class="float-right">'
					+ FORMAT(@GCashAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Type
				</span>
				<span class="float-right">'
					+ @Name_CardType +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card #
				</span>
				<span class="float-right">'
					+ @CardNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Holder
				</span>
				<span class="float-right">'
					+ @CardHolderName +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CardAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check No.
				</span>
				<span class="float-right">'
					+ @CheckNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CheckAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	DECLARE @title VARCHAR(MAX)= ''
	DECLARE @style VARCHAR(MAX)= '
		body{
			margin: 0px;
			padding: 0px;
			font-family:  arial, sans-serif;
			font-weight: normal;
			font-style: normal;
			font-size: 13px;
		}

		.logo{

			width: 120px;
			margin-bottom: 10px;
		}

		.receipt-container{
			width: 160px;
		}

		.company-logo{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
			margin-bottom: 12px;
		}

		.company-name{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
		}

		.company-address{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-size: 11px
		}

		.company-contactnum-container{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-weight: bold;
			font-size: 11px
		}

		.company-contactnum{
			font-weight: 200;
			text-align: center;
			word-wrap: break-word;

		}

		.float-left{
			float: left;
		}

		.float-right{
			float: right;
		}

		.clearfix {
			overflow: auto;
		}

		.bold{
			font-weight: bold;
		}

		.display-block{

			display: block;
		}

		.title{
			text-align: center;
			word-wrap: break-word;
			display: block;
			padding-top: 15px;
			padding-bottom: 15px;
		}
	'

	DECLARE @content VARCHAR(MAX)= '
		<div class="receipt-container">
			<div class="company-logo"><img class="logo" src="'+ @ImageLogoLocationFilenamePath_Company +'" alt="" srcset=""></div>
			<div class="company-name">'+ @Name_Company+ '' +'</div>
			<div class="company-address">'+ @Address_Company+ '' +'</div>
			<div class="company-contactnum-container" style="'+ CASE WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none' ELSE '' END +'">
				Contact No: <span class="company-contactnum">'+ @ContactNumber_Company +'</span>
			</div>
			<div class="title bold">INVOICE</div>
			<div class="display-block"><span class="bold">Bill To: </span>'+ @Name_Client +'</div>
			<div class="display-block"><span class="bold">Pet Name: </span>'+ @Name_Patient +'</div>
			<div class="display-block">
				<span class="bold">Invoice Date: </span>'+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy') +'
			</div> 
			<div class="display-block"><span class="bold">BI #: </span>'+ @Code_BillingInvoice +'</div> 
			<br/>
			'+ @billingItemsLayout +'

			<div class="title bold">TOTALS</div>
			
			<div class="display-block clearfix">
				<span class="float-left bold">
					Subtotal
				</span>
				<span class="float-right">'
					+  FORMAT(@SubTotal_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Rate
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountRate_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Total Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@TotalAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>

			<div class="title bold">PAYMENT</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					PT #
				</span>
				<span class="float-right">'
					+  @Code_PaymentTranaction +
				'</span>
			</div>
			'+ @paymentLayout +'
			<br/>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Balance
				</span>
				<span class="float-right">'
					+ FORMAT(@PayableAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Payment
				</span>
				<span class="float-right">'
					+ FORMAT(@PaymentAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Remaining
				</span>
				<span class="float-right">'
					+ FORMAT(@RemainingAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Change
				</span>
				<span class="float-right">'
					+ FORMAT(@ChangeAmount,'#,#0.00') +
				'</span>
			</div>

			<div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>

			<div class="title"> Print Date '+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy hh:mm:ss tt') +'</div>
		</div>
	'

    SELECT '_'

	SELECT 
		@Code_PaymentTranaction title, 
		@style style, 
		@content content, 
		@isAutoPrint isAutoPrint, 
		@ID_PaymentTransaction ID_PaymentTransaction, 
		@ID_BillingInvoice ID_BillingInvoice,
		@millisecondDelay millisecondDelay

END

GO

IF (
SELECT  COUNT(*)
FROM _tREport
WHERE
	Name = 'VeterinaryHealthClinic') = 0
BEGIN 

	exec [_pCreateReportView] 'VeterinaryHealthClinic', 0
END


DECLARE @ID_REPORT_SalesIncomeReportViewer UNIQUEIDENTIFIER;

SELECT @ID_REPORT_SalesIncomeReportViewer = Oid
FROM dbo._tReport
WHERE Name = 'SalesIncomeReport';

UPDATE _tNavigation SET Caption = 'Sales Income Report'
WHERE  Name = 'SalesIncomeReport_Navigation'

IF (NOT EXISTS
(
    SELECT *
    FROM dbo.tCustomNavigationLink
    WHERE [Oid_Report] = @ID_REPORT_SalesIncomeReportViewer
))
BEGIN

	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   , UPPER('SalesIncomeReport')
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,NULL
			   ,'SalesIncomeReport'
			   ,3 
			   ,@ID_REPORT_SalesIncomeReportViewer)

END
