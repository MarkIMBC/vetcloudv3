----------------------------Modified Table -----------------------------------
IF NOT EXISTS(SELECT 1
              FROM   sys.columns
              WHERE  Name = N'AttendingPhysician_ID_Employee'
                     AND Object_ID = Object_ID(N'tPatientAppointment'))
  BEGIN
      exec _pAddModelProperty
        tPatientAppointment,
        'AttendingPhysician_ID_Employee',
        2
  END

GO

exec _pRefreshAllViews

GO

ALTER VIEW [dbo].[vPatientAppointment]
AS
  SELECT H.*,
         CONVERT(VARCHAR, H.DateStart, 0) DateStartString,
         CONVERT(VARCHAR, H.DateEnd, 0)   DateEndString,
         SOAPType.Name                    Name_SOAPType,
         client.Name                      Name_Client,
         patient.Name                     Name_Patient,
         fs.Name                          Name_FilingStatus,
         fsAppointment.Name               Appointment_Name_FilingStatus,
         vet.Name                         AttendingPhysician_Name_Employee
  FROM   dbo.tPatientAppointment H
         LEFT JOIN dbo.tSOAPType SOAPType
                ON SOAPType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tFilingStatus fsAppointment
                ON fsAppointment.ID = H.Appointment_ID_FilingStatus
         LEFT JOIN dbo.vAttendingVeterinarian vet
                ON vet.ID = H.AttendingPhysician_ID_Employee

GO

ALTER VIEW vPatient_SOAP_ListView
AS
  SELECT ID,
         ID_Company,
         ID_SOAPType,
         ID_FilingStatus,
         Date,
         Code,
         Name_SOAPType,
         AttendingPhysician_Name_Employee,
         ID_Client,
         Name_Client,
         Name_Patient,
         Name_FilingStatus,
         ID_Patient,
         ID_Patient_Confinement,
         CaseType,
         IsNull(History, '')                                   History,
         IsNull(ClinicalExamination, '')                       ClinicalExamination,
         IsNull(Interpretation, '')                            Interpretation,
         IsNull(Diagnosis, '')                                 Diagnosis,
         IsNull(Treatment, '')                                 Treatment,
         IsNull(ClientCommunication, '')                       ClientCommunication,
         IsNull(Prescription, '')                              Prescription,
         len(Trim(IsNull(History, '')))                        Count_History,
         len(Trim(IsNull(ClinicalExamination, '')))            Count_ClinicalExamination,
         IsNull(soapLabImgCount.TotalCount, 0)
         + len(Trim(IsNull(Interpretation, '')))               Count_LaboratoryImages,
         len(Trim(IsNull(Diagnosis, '')))                      Count_Diagnosis,
         len(Trim(IsNull(Treatment, '')))                      Count_Treatment,
         len(Trim(IsNull(ClientCommunication, '')))            Count_ClientCommunication,
         IsNull(soapPlanCount.TotalCount, 0)                   Count_Plan,
         IsNull(soapPrescription.TotalCount, 0)
         + len(Trim(IsNull(soap.Prescription, '')))            Count_Prescription,
         soap.DateCreated,
         soap.DateModified,
         'Date Created: '
         + Format(soap.DateCreated, 'MMM. dd, yyyy hh:mm tt')
         + Char(13) + 'Date Modified: ',
         + Format(soap.DateModified, 'MMM. dd, yyyy hh:mm tt') TooltipText
  FROM   dbo.vPatient_SOAP soap
         LEFT JOIN vPatient_SOAP_Plan_Count soapPlanCount
                ON soap.ID = soapPlanCount.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_Prescription_Count soapPrescription
                ON soap.ID = soapPrescription.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_LaboratoryImages_Count soapLabImgCount
                ON soap.ID = soapLabImgCount.ID_Patient_SOAP
  WHERE  ID_FilingStatus NOT IN ( 4 )

GO

----------------------------------------------------VIEWS --------------------------------------------------------------
ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         Format(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         Format(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         IsNull(patientSOAP.Name_SOAPType, '')
         + ' - '
         + IsNull(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks,
         patientSOAP.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 ON client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + '0' + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         Format(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         Format(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         Format(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         Format(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         IsNull(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         IsNull(patientAppnt.Name_Patient, '')
         + ' - '
         + IsNull(patientAppnt.Name_SOAPType, '') + ' '
         + IsNull(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks,
         patientAppnt.AttendingPhysician_Name_Employee
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 ON client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT DISTINCT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID)                                 ID,
                  CONVERT(VARCHAR(MAX), _model.Oid) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellSched.ID) + '|'
                  + CONVERT(VARCHAR(MAX), wellness.ID_Client)                          UniqueID,
                  _model.Oid                                                           Oid_Model,
                  _model.Name                                                          Name_Model,
                  wellness.ID_Company,
                  wellness.ID_Client,
                  wellness.ID                                                          ID_CurrentObject,
                  wellSched.ID                                                         Appointment_ID_CurrentObject,
                  wellSched.Date                                                       DateStart,
                  wellSched.Date                                                       DateEnd,
                  Format(wellSched.Date, 'yyyy-MM-dd')                                 FormattedDateStart,
                  Format(wellSched.Date, 'yyyy-MM-dd ')                                FormattedDateEnd,
                  Format(wellSched.Date, 'hh:mm tt')                                   FormattedDateStartTime,
                  ''                                                                   FormattedDateEndTime,
                  wellness.Code                                                        ReferenceCode,
                  wellness.Name_Client + ' - '
                  + wellness.Name_Patient                                              Paticular,
                  IsNull('', '') + ' - '
                  + IsNull(wellSched.Comment, '')                                      Description,
                  dbo.fGetDateCoverageString(wellSched.Date, wellSched.Date, 1)        TimeCoverage,
                  wellness.ID_FilingStatus,
                  Name_FilingStatus,
                  wellness.Name_Client,
                  wellness.ID_Patient,
                  wellness.Name_Patient,
                  dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2) ContactNumber,
                  wellSched.Appointment_ID_FilingStatus,
                  wellSched.Appointment_Name_FilingStatus,
                  wellSched.Appointment_CancellationRemarks,
                  wellness.AttendingPhysician_Name_Employee
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_Schedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 ON client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

exec _pRefreshAllViews

GO 
