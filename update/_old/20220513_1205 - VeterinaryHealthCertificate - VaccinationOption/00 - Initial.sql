if OBJECT_ID('dbo.tVaccinationOption') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tVaccinationOption',
        1,
        1,
        NULL

      INSERT INTO [dbo].tVaccinationOption
                  (Comment,
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('This further certifies that I have vaccinated the above-described dog/cat against Rabies on /*****Date*****/ using /*****Vaccination*****/ with /*****Serial/Lot Number*****/.',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)

      INSERT INTO [dbo].tVaccinationOption
                  (Comment,
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('This further certifies that the above-described dog/cat was vaccinated against Rabies on using /*****Vaccination*****/ with /*****Serial/Lot Number*****/.',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

IF COL_LENGTH('dbo.tVeterinaryHealthCertificate', 'ID_VaccinationOption') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tVeterinaryHealthCertificate',
        'ID_VaccinationOption',
        2
  END

GO

exec _pRefreshAllViews

GO

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetVeterinaryHealthCertificateVaccinationOptionComment](@VaccinationOptionComment VARCHAR(MAX),
                                                                               @DateVaccinated           DateTime,
                                                                               @Name_Item                VARCHAR(MAX),
                                                                               @LotNumber                VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Date*****/', '<u>'
                                                                                             + IsNULL(FORMAT(@DateVaccinated, 'MM/dd/yyyy'), '')
                                                                                             + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Vaccination*****/', '<u>' + IsNULL(@Name_Item, '') + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Serial/Lot Number*****/', '<u>' + IsNULL(@LotNumber, '') + '</u>')

      RETURN @VaccinationOptionComment
  END

GO

ALTER VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         CASE
           WHEN len(Trim(IsNull(CustomCode, ''))) = 0 THEN Code
           ELSE CustomCode
         END                                                                                      Code,
         Name,
         ID_Client,
         Name_Client,
         Email,
         ISNULL(Species, '')                                                                      Species,
         ISNULL(Name_Gender, '')                                                                  Name_Gender,
         ContactNumber,
         IsDeceased,
         DateDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile,
         IsNull(IsActive, 0)                                                                      IsActive,
         WaitingStatus_ID_FilingStatus,
         WaitingStatus_Name_FilingStatus,
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue,
         DateBirth,
         IsNull(Microchip, '')                                                                    Microchip,
         ISNULL(Color, '')                                                                        Color,
         dbo.fGetAge(DateBirth, GETDATE(), '')                                                    Age
  FROM   dbo.vPatient

GO

ALTER VIEW vVeterinaryHealthCertificate
AS
  SELECT H.*,
         UC.Name                   CreatedBy,
         UM.Name                   LastModifiedBy,
         client.Name               Name_Client,
         patient.Name              Name_Patient,
         fs.Name                   Name_FilingStatus,
         _AttendingPhysician.Name  AttendingPhysician_Name_Employee,
         item.Name                 Name_Item,
         vaccinationOption.Comment Comment_VaccinationOption
  FROM   tVeterinaryHealthCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN tItem item
                ON item.ID = H.ID_Item
         LEFT JOIN tVaccinationOption vaccinationOption
                ON vaccinationOption.ID = H.ID_VaccinationOption

GO

ALTER VIEW [dbo].[vzVeterinaryHealthClinicReport]
AS
  SELECT vetcert.ID,
         vetcert.Code,
         vetcert.Date,
         vetcert.DestinationAddress,
         vetcert. Color,
         vetcert. Weight,
         vetcert.ID_Item,
         vetcert.Name_Item,
         CASE
           WHEN len(IsNull(vetcert.LotNumber, '')) > 0 THEN ' with Serial / Lot Number'
                                                            + vetcert.LotNumber
           ELSE ''
         END                                                                                                                                                   LotNumber,
         patient.ID                                                                                                                                            ID_Patient,
         patient.NAME                                                                                                                                          Name_Patient,
         IsNull(Species, '')                                                                                                                                   Name_Species,
         IsNull(Name_Gender, '')                                                                                                                               Name_Gender,
         patient.Microchip,
         dbo.fGetAge(patient.DateBirth, CASE
                                          WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                                            Age_Patient,
         IsNull(client.NAME, '')                                                                                                                               Name_Client,
         IsNull(client.Address, '')                                                                                                                            Address_Client,
         IsNull(client.ContactNumber, '')                                                                                                                      ContactNumber_Client,
         IsNull(client.ContactNumber2, '')                                                                                                                     ContactNumber2_Client,
         IsNull(client.Email, '')                                                                                                                              Email_Client,
         company.ID                                                                                                                                            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.NAME                                                                                                                                          Name_Company,
         company.Address                                                                                                                                       Address_Company,
         company.ContactNumber                                                                                                                                 ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                                                 HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         _AttendingPhysician.TINNumber,
         _AttendingPhysician.PTR,
         _AttendingPhysician.PRCLicenseNumber,
         CASE
           WHEN _AttendingPhysician.DatePRCExpiration IS NOT NULL THEN FORMAT(_AttendingPhysician.DatePRCExpiration, 'MM/dd/yyyy')
           ELSE ''
         END                                                                                                                                                   DatePRCExpiration,
         CASE
           WHEN vetCert.DateVaccination IS NOT NULL THEN FORMAT(vetCert.DateVaccination, 'MM/dd/yyyy')
           ELSE '                '
         END                                                                                                                                                   DateVaccination,
         dbo.fGetVeterinaryHealthCertificateVaccinationOptionComment(Comment_VaccinationOption, vetCert.DateVaccination, vetCert.Name_Item, vetCert.LotNumber) Comment_VaccinationOption
  FROM   vVeterinaryHealthCertificate vetCert
         LEFT JOIN vPatient patient
                ON vetCert.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = vetCert.ID_Company
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = vetCert.AttendingPhysician_ID_Employee

GO
ALTER PROCEDURE pGetVeterinaryHealthCertificate (@ID         INT = -1,
                                                 @ID_Client  INT = NULL,
                                                 @ID_Patient INT = NULL,
                                                 @ID_Session INT = NULL)
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '-- NEW --' AS [Code],
                           NULL        AS [Name],
                           GETDATE()   AS [Date],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           AS ID_FilingStatus,
                           'N/A'       AS [DestinationAddress],
                           @ID_Client  ID_Client,
                           @ID_Patient ID_Patient,
                           1           ID_VaccinationOption) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vVeterinaryHealthCertificate H
            WHERE  H.ID = @ID
        END
  END 
GO

Update tVeterinaryHealthCertificate
SET    ID_VaccinationOption = 1
where  ID_VaccinationOption IS NULL

select ID,
       Comment_VaccinationOption
FROM   [vzVeterinaryHealthClinicReport] 
