IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tPatientSOAPList'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tPatientSOAPList', 1, 0, NULL
END

GO


IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tIssueTracker'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tIssueTracker', 1, 0, NULL
END

GO

IF(SELECT  COUNT(*)
	FROM _tModel 
	WHERE TableName = 'tBreedSpecie'
) = 0
BEGIN

	exec _pCreateAppModuleWithTable 'tBreedSpecie', 0, 0, NULL
END

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'Issue'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'Issue'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'DeveloperSide'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'DeveloperSide'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'Solution'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'Solution'
                         , 1
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'ID_FilingStatus'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'ID_FilingStatus'
                         , 2
END;

GO

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'ID_ApprovedBy'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'ID_ApprovedBy'
                         , 2

END;

GO 

IF (
	SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tIssueTracker'
    AND COLUMN_NAME = 'DateApproved'
) = 0
BEGIN

  EXEC _pAddModelProperty 'tIssueTracker'
                         ,'DateApproved'
                         , 5

END;

GO

CREATE OR ALTER VIEW [dbo].[vIssueTracker] 
AS 
	SELECT 
		H.* ,
		UC.Name AS CreatedBy,
		UM.Name AS LastModifiedBy,
		fs.Name Name_FilingStatus,
		approvedBy.Name Name_ApprovedBy
	FROM tIssueTracker H
	LEFT JOIN tUser UC 
		ON H.ID_CreatedBy = UC.ID	
	LEFT JOIN tUser 
		UM ON H.ID_LastModifiedBy = UM.ID
	LEFT JOIN tUser approvedBy
		 ON H.ID_ApprovedBy = approvedBy.ID
	LEFT JOIN tFilingStatus fs
		on fs.ID = H.ID_FilingStatus

Go

CREATE OR ALTER PROCEDURE [dbo].[pGetIssueTracker] @ID INT = -1 , @ID_Session INT = NULL
AS
BEGIN

	SELECT '_'

	DECLARE @ID_User INT, @ID_Warehouse INT
	SELECT @ID_User = ID_User, @ID_Warehouse = ID_Warehouse FROM tUserSession WHERE ID = @ID_Session

	IF (@ID = -1)
		BEGIN
			SELECT 
				H.* 
			FROM ( 
				SELECT 
					NULL AS [_]
					, 	-1 AS [ID]
					, '(New)' AS [Code]
					, NULL AS [Name]
					, 1 AS [IsActive]
					, NULL AS [ID_Company]
					, NULL AS [Comment]
					,  NULL AS [DateCreated]
					,  NULL AS [DateModified]
					, NULL AS [ID_CreatedBy]
					, NULL AS [ID_LastModifiedBy]
					, 'Input Concern He2334234re' [Concern]
					, 1 ID_FilingStatus

			) H
			LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
			LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
		END
	ELSE
		BEGIN
			SELECT 
				H.* 
			FROM vIssueTracker H 
			WHERE H.ID = @ID
		END
END

GO

ALTER   PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout]
(
    @ID_PaymentTransaction INT
)
AS
BEGIN

	DECLARE @isAutoPrint BIT = 1

	DECLARE @Cash_ID_PaymentMethod INT = 1
	DECLARE @Check_ID_PaymentMethod INT = 2
	DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
	DECLARE @GCash_ID_PaymentMethod INT = 3


	DECLARE @billingItemsLayout VARCHAR(MAX)= ''
	DECLARE @paymentLayout VARCHAR(MAX)= ''

	Declare @ID_Company int	= 0
	Declare @ID_BillingInvoice int	= 0
	Declare @Name_Company VARCHAR(MAX)= ''
	Declare @Address_Company VARCHAR(MAX)= ''
	Declare @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
	Declare @ContactNumber_Company VARCHAR(MAX)= ''
	Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
	Declare @ID_PaymentMode int	= 0

	Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
	Declare @CheckNumber VARCHAR(MAX)= ''
	Declare @CardNumber VARCHAR(MAX)= ''
	Declare @Name_CardType VARCHAR(MAX)= ''
	Declare @CardHolderName VARCHAR(MAX)= ''
	
	Declare @Name_PaymentStatus VARCHAR(MAX)= ''

	Declare @CashAmount DECIMAL(18, 4) = 0.00
	Declare @GCashAmount DECIMAL(18, 4) =  0.00
	Declare @CardAmount DECIMAL(18, 4) =  0.00
	Declare @CheckAmount DECIMAL(18, 4) =  0.00
	
	Declare @PayableAmount DECIMAL(18, 4) =  0.00
	Declare @PaymentAmount DECIMAL(18, 4) =  0.00
	Declare @ChangeAmount DECIMAL(18, 4) =  0.00
	Declare @RemainingAmount DECIMAL(18, 4) =  0.00

	Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
	Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
	
	Declare @Name_Client VARCHAR(MAX)= ''
	Declare @Name_Patient VARCHAR(MAX)= ''
	Declare @Date_BillingInvoice datetime
	Declare @Code_BillingInvoice VARCHAR(MAX)= ''

	SELECT  @ID_Company = ID_Company,
			@ID_BillingInvoice = ID_BillingInvoice,
			@ID_PaymentMode = ID_PaymentMethod,
			@Code_PaymentTranaction = Code,
			@PayableAmount = PayableAmount,
			@RemainingAmount = RemainingAmount,
			@ChangeAmount = ChangeAmount,
			@CashAmount = CashAmount,
			@ReferenceTransactionNumber = ReferenceTransactionNumber,
			@GCashAmount = GCashAmount,
			@Name_CardType = Name_CardType,
			@CardNumber = CardNumber,
			@CardHolderName = CardHolderName,
			@CardAmount = CardAmount,
			@CheckNumber = CheckNumber,
			@CheckAmount = CheckAmount,
			@PaymentAmount = PaymentAmount
	FROM    vPaymentTransaction 
	WHERE 
		ID = @ID_PaymentTransaction

	SELECT  @Name_Company = ISNULL(Name,'N/A'),
			@Address_Company = ISNULL(Address,''),
			@ContactNumber_Company = ISNULL(ContactNumber,''),
			@ImageLogoLocationFilenamePath_Company = ImageLogoLocationFilenamePath
	FROM    vCompany 
	WHERE ID = @ID_Company

	SELECT  @Name_Client = ISNULL(bi.Name_Client,''),
			@Name_Patient = ISNULL(bi.Name_Patient,'N/A'),
			@Date_BillingInvoice = Date,
			@Code_BillingInvoice = Code,
			@SubTotal_BillingInvoice = SubTotal,
			@DiscountRate_BillingInvoice = DiscountRate,
			@DiscountAmount_BillingInvoice = DiscountAmount,
			@TotalAmount_BillingInvoice = TotalAmount, 
			@Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus,'')
	FROM    vBillingInvoice bi
	WHERE
		bi.ID = @ID_BillingInvoice

	SELECT  @billingItemsLayout = @billingItemsLayout + '
			<div class="display-block clearfix">
				<span class="bold">'+  biDetail.Name_Item +'</span><br/>
				<span class="float-left">
					&nbsp;&nbsp;&nbsp;'+  FORMAT(biDetail.Quantity,'#,#0.##') +'
				</span>
				<span class="float-right">'
					+  FORMAT(biDetail.Amount,'#,#0.00') +
				'</span>
			</div>'
	FROM    vBillingInvoice_Detail biDetail
	WHERE 
		biDetail.ID_BillingInvoice = @ID_BillingInvoice

	if @ID_PaymentMode = @Cash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Cash Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@CashAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	else if @ID_PaymentMode = @GCash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Ref. No.
				</span>
				<span class="float-right">'
					+ @ReferenceTransactionNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					G-Cash
				</span>
				<span class="float-right">'
					+ FORMAT(@GCashAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Type
				</span>
				<span class="float-right">'
					+ @Name_CardType +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card #
				</span>
				<span class="float-right">'
					+ @CardNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Holder
				</span>
				<span class="float-right">'
					+ @CardHolderName +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CardAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check No.
				</span>
				<span class="float-right">'
					+ @CheckNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CheckAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	DECLARE @title VARCHAR(MAX)= ''
	DECLARE @style VARCHAR(MAX)= '
		body{
			margin: 0px;
			padding: 0px;
			font-family:  arial, sans-serif;
			font-weight: normal;
			font-style: normal;
			font-size: 13px;
		}

		.logo{

			width: 120px;
			margin-bottom: 10px;
		}

		.receipt-container{
			width: 160px;
		}

		.company-logo{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
			margin-bottom: 12px;
		}

		.company-name{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
		}

		.company-address{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-size: 11px
		}

		.company-contactnum-container{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-weight: bold;
			font-size: 11px
		}

		.company-contactnum{
			font-weight: 200;
			text-align: center;
			word-wrap: break-word;

		}

		.float-left{
			float: left;
		}

		.float-right{
			float: right;
		}

		.clearfix {
			overflow: auto;
		}

		.bold{
			font-weight: bold;
		}

		.display-block{

			display: block;
		}

		.title{
			text-align: center;
			word-wrap: break-word;
			display: block;
			padding-top: 15px;
			padding-bottom: 15px;
		}
	'

	DECLARE @content VARCHAR(MAX)= '
		<div class="receipt-container">
			<div class="company-logo"><img class="logo" src="'+ @ImageLogoLocationFilenamePath_Company +'" alt="" srcset=""></div>
			<div class="company-name">'+ @Name_Company+ '' +'</div>
			<div class="company-address">'+ @Address_Company+ '' +'</div>
			<div class="company-contactnum-container" style="'+ CASE WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none' ELSE '' END +'">
				Contact No: <span class="company-contactnum">'+ @ContactNumber_Company +'</span>
			</div>
			<div class="title bold">INVOICE</div>
			<div class="display-block"><span class="bold">Bill To: </span>'+ @Name_Client +'</div>
			<div class="display-block"><span class="bold">Pet Name: </span>'+ @Name_Patient +'</div>
			<div class="display-block">
				<span class="bold">Invoice Date: </span>'+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy') +'
			</div> 
			<div class="display-block"><span class="bold">BI #: </span>'+ @Code_BillingInvoice +'</div> 
			<br/>
			'+ @billingItemsLayout +'

			<div class="title bold">TOTALS</div>
			
			<div class="display-block clearfix">
				<span class="float-left bold">
					Subtotal
				</span>
				<span class="float-right">'
					+  FORMAT(@SubTotal_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Rate
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountRate_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Total Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@TotalAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>

			<div class="title bold">PAYMENT</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					PT #
				</span>
				<span class="float-right">'
					+  @Code_PaymentTranaction +
				'</span>
			</div>
			'+ @paymentLayout +'
			<br/>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Balance
				</span>
				<span class="float-right">'
					+ FORMAT(@PayableAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Payment
				</span>
				<span class="float-right">'
					+ FORMAT(@PaymentAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Remaining
				</span>
				<span class="float-right">'
					+ FORMAT(@RemainingAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Change
				</span>
				<span class="float-right">'
					+ FORMAT(@ChangeAmount,'#,#0.00') +
				'</span>
			</div>

			<div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>

			<div class="title"> Print Date '+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy hh:mm:ss tt') +'</div>
		</div>
	'

    SELECT '_'

	SELECT @Code_PaymentTranaction title, @style style, @content content, @isAutoPrint isAutoPrint, @ID_PaymentTransaction, @ID_BillingInvoice

END

GO

Declare @IssueTracker_ID_ListView UNIQUEIDENTIFIER 

SELECT  @IssueTracker_ID_ListView = Oid
FROM _tListView 
WHERE 
	Name = 'IssueTracker_ListView'

IF(SELECT  COUNT(*)
	FROM tCustomNavigationLink 
	WHERE Name = 'IssueTracker_ListView'
) = 0
BEGIN

	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   ,'IssueTracker_ListView'
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,@IssueTracker_ID_ListView
			   ,'IssueTrackerList'
			   ,1
			   ,NULL)
END

GO

Declare @PatientSOAPList_ID_ListView UNIQUEIDENTIFIER 

SELECT  @PatientSOAPList_ID_ListView = Oid
FROM _tListView 
WHERE 
	Name = 'PatientSOAPList_ListView'

IF(SELECT  COUNT(*)
	FROM tCustomNavigationLink 
	WHERE Name = 'PatientSOAPList_ListView'
) = 0
BEGIN

	INSERT INTO [dbo].[tCustomNavigationLink]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy]
			   ,[Oid_ListView]
			   ,[RouterLink]
			   ,[ID_ViewType]
			   ,[Oid_Report])
		 VALUES
			   (NULL
			   ,'PatientSOAPList_ListView'
			   ,1
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,GETDATE()
			   ,1
			   ,1
			   ,@PatientSOAPList_ID_ListView
			   ,'PatientSOAPList'
			   ,1
			   ,NULL)
END

GO

IF(SELECT  COUNT(*)
	FROM tBreedSpecie 
) = 0
BEGIN

	DECLARE @BreedSpecie TABLE (Item VARCHAR(MAX))

	INSERT @BreedSpecie
	VALUES
	('Dog - Afador')
	,('Dog - Affenpinscher')
	,('Dog - Afghan Hound')
	,('Dog - Airedale Terrier')
	,('Dog - Akita')
	,('Dog - Alaskan Klee Kai')
	,('Dog - Alaskan Malamute')
	,('Dog - American Bulldo')
	,('Dog - American English Coonhound')
	,('Dog - American Eskimo Do')
	,('Dog - American Foxhound')
	,('Dog - American Leopard Hound')
	,('Dog - American Pit Bull Terrier')
	,('Dog - American Pugabull')
	,('Dog - American Staffordshire Terrier')
	,('Dog - American Water Spaniel')
	,('Dog - Anatolian Shepherd Dog')
	,('Dog - Appenzeller Sennenhunde')
	,('Dog - Auggie')
	,('Dog - Aussiedoodle')
	,('Dog - Aussiepom')
	,('Dog - Australian Cattle Dog')
	,('Dog - Australian Kelpie')
	,('Dog - Australian Shepherd')
	,('Dog - Australian Terrier')
	,('Dog - Azawakh')
	,('Dog - Barbet')
	,('Dog - Basenji')
	,('Dog - Bassador')
	,('Dog - Basset Fauve de Bretagne')
	,('Dog - Basset Hound')
	,('Dog - Bavarian Mountain Scent Hound')
	,('Dog - Beagle')
	,('Dog - Bearded Collie')
	,('Dog - Bedlington Terrier')
	,('Dog - Belgian Malinois')
	,('Dog - Belgian Sheepdog')
	,('Dog - Belgian Tervuren')
	,('Dog - Berger Picard')
	,('Dog - Bernedoodle')
	,('Dog - Bernese Mountain Dog')
	,('Dog - Bichon Frise')
	,('Dog - Biewer Terrier')
	,('Dog - Black and Tan Coonhound')
	,('Dog - Black Mouth Cur')
	,('Dog - Black Russian Terrier')
	,('Dog - Bloodhound')
	,('Dog - Blue Lacy')
	,('Dog - Bluetick Coonhound')
	,('Dog - Bocker')
	,('Dog - Boerboel')
	,('Dog - Boglen Terrier')
	,('Dog - Bolognese')
	,('Dog - Borador')
	,('Dog - Border Collie')
	,('Dog - Border Terrier')
	,('Dog - Bordoodle')
	,('Dog - Borzoi')
	,('Dog - Boston Terrier')
	,('Dog - Bouvier des Flandres')
	,('Dog - Boxer')
	,('Dog - Boxerdoodle')
	,('Dog - Boxmatian')
	,('Dog - Boykin Spaniel')
	,('Dog - Bracco Italiano')
	,('Dog - Braque du Bourbonnais')
	,('Dog - Briard')
	,('Dog - Brittany')
	,('Dog - Broholmer')
	,('Dog - Brussels Griffon')
	,('Dog - Bull Terrier')
	,('Dog - Bullboxer Pit')
	,('Dog - Bulldog')
	,('Dog - Bullmastiff')
	,('Dog - Bullmatian')
	,('Dog - Cairn Terrier')
	,('Dog - Canaan Dog')
	,('Dog - Cane Corso')
	,('Dog - Cardigan Welsh Corgi')
	,('Dog - Catahoula Bulldog')
	,('Dog - Catahoula Leopard Dog')
	,('Dog - Caucasian Shepherd Dog')
	,('Dog - Cav-a-Jack')
	,('Dog - Cavachon')
	,('Dog - Cavador')
	,('Dog - Cavalier King Charles Spaniel')
	,('Dog - Cavapoo')
	,('Dog - Cesky Terrier')
	,('Dog - Chabrador')
	,('Dog - Cheagle')
	,('Dog - Chesapeake Bay Retriever')
	,('Dog - Chi-Poo')
	,('Dog - Chihuahua')
	,('Dog - Chilier')
	,('Dog - Chinese Crested')
	,('Dog - Chinese Shar-Pei')
	,('Dog - Chinook')
	,('Dog - Chipin')
	,('Dog - Chiweenie')
	,('Dog - Chow Chow')
	,('Dog - Chug')
	,('Dog - Chusky')
	,('Dog - Clumber Spaniel')
	,('Dog - Cockapoo')
	,('Dog - Cocker Spaniel')
	,('Dog - Collie')
	,('Dog - Corgi Inu')
	,('Dog - Corgidor')
	,('Dog - Coton de Tulear')
	,('Dog - Curly-Coated Retriever')
	,('Dog - Dachsador')
	,('Dog - Dachshund')
	,('Dog - Dalmatian')
	,('Dog - Dandie Dinmont Terrier')
	,('Dog - Deutscher Wachtelhund')
	,('Dog - Doberdor')
	,('Dog - Doberman Pinscher')
	,('Dog - Docker')
	,('Dog - Dogo Argentino')
	,('Dog - Dogue de Bordeaux')
	,('Dog - Dorgi')
	,('Dog - Doxiepoo')
	,('Dog - Doxle')
	,('Dog - Dutch Shepherd')
	,('Dog - English Cocker Spaniel')
	,('Dog - English Foxhound')
	,('Dog - English Setter')
	,('Dog - English Springer Spaniel')
	,('Dog - English Toy Spaniel')
	,('Dog - Entlebucher Mountain Dog')
	,('Dog - Eurasier')
	,('Dog - Field Spaniel')
	,('Dog - Finnish Lapphund')
	,('Dog - Finnish Spitz')
	,('Dog - Flat-Coated Retriever')
	,('Dog - French Bulldog')
	,('Dog - French Spaniel')
	,('Dog - German Pinscher')
	,('Dog - German Shepherd Dog')
	,('Dog - German Sheprador')
	,('Dog - German Shorthaired Pointer')
	,('Dog - German Spitz')
	,('Dog - German Wirehaired Pointer')
	,('Dog - Giant Schnauzer')
	,('Dog - Glen of Imaal Terrier')
	,('Dog - Goberian')
	,('Dog - Goldador')
	,('Dog - Golden Cocker Retriever')
	,('Dog - Golden Retriever')
	,('Dog - Goldendoodle')
	,('Dog - Gollie')
	,('Dog - Gordon Setter')
	,('Dog - Great Dane')
	,('Dog - Great Pyrenees')
	,('Dog - Greater Swiss Mountain Dog')
	,('Dog - Greyhound')
	,('Dog - Harrier')
	,('Dog - Havanese')
	,('Dog - Hokkaido')
	,('Dog - Horgi')
	,('Dog - Ibizan Hound')
	,('Dog - Icelandic Sheepdog')
	,('Dog - Irish Red and White Setter')
	,('Dog - Irish Setter')
	,('Dog - Irish Terrier')
	,('Dog - Irish Water Spaniel')
	,('Dog - Irish Wolfhound')
	,('Dog - Italian Greyhound')
	,('Dog - Jack Chi')
	,('Dog - Jack Russell Terrier')
	,('Dog - Japanese Chin')
	,('Dog - Japanese Spitz')
	,('Dog - Korean Jindo Dog')
	,('Dog - Karelian Bear Dog')
	,('Dog - Keeshond')
	,('Dog - Kerry Blue Terrier')
	,('Dog - Komondor')
	,('Dog - Kooikerhondje')
	,('Dog - Kuvasz')
	,('Dog - Kyi-Leo')
	,('Dog - Lab Pointer')
	,('Dog - Labernese')
	,('Dog - Labmaraner')
	,('Dog - Labrabull')
	,('Dog - Labradane')
	,('Dog - Labradoodle')
	,('Dog - Labrador Retriever')
	,('Dog - Labrastaff')
	,('Dog - Labsky')
	,('Dog - Lagotto Romagnolo')
	,('Dog - Lakeland Terrier')
	,('Dog - Lancashire Heeler')
	,('Dog - Leonberger')
	,('Dog - Lhasa Apso')
	,('Dog - Lowchen')
	,('Dog - Maltese')
	,('Dog - Maltese Shih Tzu')
	,('Dog - Maltipoo')
	,('Dog - Manchester Terrier')
	,('Dog - Mastador')
	,('Dog - Mastiff')
	,('Dog - Miniature Pinscher')
	,('Dog - Miniature Schnauzer')
	,('Dog - Mongrel')
	,('Dog - Morkie')
	,('Dog - Mudi')
	,('Dog - Mutt')
	,('Dog - Neapolitan Mastiff')
	,('Dog - Newfoundland')
	,('Dog - Norfolk Terrier')
	,('Dog - Norwegian Buhund')
	,('Dog - Norwegian Elkhound')
	,('Dog - Norwegian Lundehund')
	,('Dog - Norwich Terrier')
	,('Dog - Nova Scotia Duck Tolling Retriever')
	,('Dog - Old English Sheepdog')
	,('Dog - Otterhound')
	,('Dog - Papillon')
	,('Dog - Peekapoo')
	,('Dog - Pekingese')
	,('Dog - Pembroke Welsh Corgi')
	,('Dog - Petit Basset Griffon Vendeen')
	,('Dog - Pharaoh Hound')
	,('Dog - Pitsky')
	,('Dog - Plott')
	,('Dog - Pocket Beagle')
	,('Dog - Pointer')
	,('Dog - Polish Lowland Sheepdog')
	,('Dog - Pomapoo')
	,('Dog - Pomchi')
	,('Dog - Pomeagle')
	,('Dog - Pomeranian')
	,('Dog - Pomsky')
	,('Dog - Poochon')
	,('Dog - Poodle')
	,('Dog - Portuguese Podengo Pequeno')
	,('Dog - Portuguese Water Dog')
	,('Dog - Pug')
	,('Dog - Puggle')
	,('Dog - Puli')
	,('Dog - Pyrenean Shepherd')
	,('Dog - Rat Terrier')
	,('Dog - Redbone Coonhound')
	,('Dog - Rhodesian Ridgeback')
	,('Dog - Rottweiler')
	,('Dog - Saint Berdoodle')
	,('Dog - Saint Bernard')
	,('Dog - Saluki')
	,('Dog - Samoyed')
	,('Dog - Schipperke')
	,('Dog - Schnoodle')
	,('Dog - Scottish Deerhound')
	,('Dog - Scottish Terrier')
	,('Dog - Sealyham Terrier')
	,('Dog - Shepsky')
	,('Dog - Shetland Sheepdog')
	,('Dog - Shiba Inu')
	,('Dog - Shih-Poo')
	,('Dog - Shih Tzu')
	,('Dog - Shiranian')
	,('Dog - Shorkie')
	,('Dog - Siberian Husky')
	,('Dog - Silken Windhound')
	,('Dog - Silky Terrier')
	,('Dog - Skye Terrier')
	,('Dog - Sloughi')
	,('Dog - Small Munsterlander Pointer')
	,('Dog - Soft Coated Wheaten Terrier')
	,('Dog - Stabyhoun')
	,('Dog - Staffordshire Bull Terrier')
	,('Dog - Standard Schnauzer')
	,('Dog - Sussex Spaniel')
	,('Dog - Swedish Vallhund')
	,('Dog - Tibetan Mastiff')
	,('Dog - Tibetan Spanie')
	,('Dog - Tibetan Terrier')
	,('Dog - Toy Fox Terrier')
	,('Dog - Treeing Tennessee Brindle')
	,('Dog - Treeing Walker Coonhound')
	,('Dog - Valley Bulldog')
	,('Dog - Vizsla')
	,('Dog - Weimaraner')
	,('Dog - Welsh Springer Spaniel')
	,('Dog - Welsh Terrier')
	,('Dog - West Highland White Terrier')
	,('Dog - Westiepoo')
	,('Dog - Whippet')
	,('Dog - Whoodle')
	,('Dog - Wirehaired Pointing Griffon')
	,('Dog - Xoloitzcuintli')
	,('Dog - Yorkipoo')
	,('Dog - Yorkshire Terrier')
	,('Dog - Others')
	,('Cat - Abyssinian')
	,('Cat - American Bobtail')
	,('Cat - American Curl')
	,('Cat - American Shorthair')
	,('Cat - American Wirehair')
	,('Cat - Balinese-Javanese')
	,('Cat - Bengal')
	,('Cat - Birman')
	,('Cat - Bombay')
	,('Cat - British Shorthair')
	,('Cat - Burmese')
	,('Cat - Chartreux')
	,('Cat - Cornish Rex')
	,('Cat - Devon Rex')
	,('Cat - Egyptian Mau')
	,('Cat - European Burmese')
	,('Cat - Exotic Shorthair')
	,('Cat - Havana Brown')
	,('Cat - Himalayan')
	,('Cat - Japanese Bobtail')
	,('Cat - Korat')
	,('Cat - LaPerm')
	,('Cat - Maine Coon')
	,('Cat - Manx')
	,('Cat - Norwegian Forest Cat')
	,('Cat - Ocicat')
	,('Cat - Oriental')
	,('Cat - Persian')
	,('Cat - Peterbald')
	,('Cat - Pixiebob')
	,('Cat - Puspin')
	,('Cat - Ragamuffin')
	,('Cat - Ragdoll')
	,('Cat - Russian Blue')
	,('Cat - Savannah')
	,('Cat - Scottish Fold')
	,('Cat - Selkirk Rex')
	,('Cat - Others')

	TRUNCATE TABLE [tBreedSpecie]

	INSERT INTO [dbo].[tBreedSpecie]
			   ([Code]
			   ,[Name]
			   ,[IsActive]
			   ,[ID_Company]
			   ,[Comment]
			   ,[DateCreated]
			   ,[DateModified]
			   ,[ID_CreatedBy]
			   ,[ID_LastModifiedBy])
	SELECT  Distinct  NULL,  Item, 1, 1, NULL, GetDate(), GetDate(),  1, 1
	FROM    @BreedSpecie
	
END

GO

update _tNavigation set 
	SeqNo = 2000, Caption = 'Issue Tracker'
   ,ID_Parent = '6D6F6247-DE8B-44CC-94E4-10C16EF776F2' 
WHERE 
	Name = 'IssueTracker_ListView'

UPDATE _tNavigation 
SET 
	Caption = 'Inv. Items And Services' 
WHERE 
	Name LIKE 'POSSummary_Navigation'

update _tNavigation 
SET 
	Caption = 'SOAP', 
	ID_Parent = '24C522DA-EA51-439F-BE1B-F7D096BEEF51' 
WHERE 
	Name = 'PatientSOAPList_ListView'
