    
CREATE OR ALTER    
 VIEW vPatient_Lodging    
AS    
  SELECT hed.* 
		,company.Name	  Name_Company	
		,createdBy.Name	              Name_CreatedBy	
		,ModifiedUserBy.Name	              Name_LastModifiedBy	
		,client.Name	              Name_Client	
		,patient.Name	              Name_Patient	
		,fs.Name	              Name_FilingStatus
  FROm   TPatient_Lodging   hed 
  LEFT JOIN tCompany company on company.ID = hed.ID_Company
  LEFT JOIN tUser createdBy on createdBy.ID = hed.ID_CreatedBy
  LEFT JOIN tUser ModifiedUserBy on ModifiedUserBy.ID = hed.ID_LastModifiedBy
  LEFT JOIN tClient client on client.ID = hed.ID_Client
  LEFT JOIN tPatient patient on patient.ID = hed.ID_Patient
  LEFT JOIN tFilingStatus fs on fs.ID = hed.ID_FilingStatus


GO
    
CREATE OR ALTER    
 VIEW vPatient_Lodging_ListvIew   
AS 
SELECT 
	ID, 
	ID_Company, 
	Name_Client, 
	Name_Patient, 
	DateStart, 
	DateEnd 
FROm vPatient_Lodging
GO