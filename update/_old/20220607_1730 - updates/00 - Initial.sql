IF COL_LENGTH('dbo.tPatient_Wellness', 'Temperature') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatient_Wellness',
        'Temperature',
        1
  END

GO

exec _pRefreshAllViews

GO

