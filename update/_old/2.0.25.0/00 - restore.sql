GO

ALTER PROC [dbo].[pModel_AfterSaved_BillInGinVoice] (@ID_CurrentObject VARCHAR(10),
                                                     @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @HayopTownVeterinaryClinic_ID_Company INT = 73;
      DECLARE @Code VARCHAR(MAX) = '';
      DECLARE @ID_CreatedBy INT;
      DECLARE @ID_USERSESSION INT;
      DECLARE @ID_FilingStatus INT;
      DECLARE @ID_Company INT;
      DECLARE @IDs_BillingINvoice TYPINTLIST

      INSERT @IDs_BillingINvoice
      VALUES (@ID_CurrentObject)

      SELECT @ID_Company = ID_Company,
             @ID_FilingStatus = ID_FilingStatus,
             @ID_CreatedBy = ID_CreatedBy
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tBillingInvoice
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'BillingInvoice';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tBillingInvoice
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      /*Update Confinement Bill Status*/
      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      EXEC PupDatePatient_ConfineMen_BillIngStatus
        @IDs_Patient_Confinement

      /*Update SOAP Bill Status*/
      DECLARE @IDs_Patient_SOAP TYPINTLIST

      INSERT @IDs_Patient_SOAP
      SELECT ID_Patient_SOAP
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      EXEC PupDatePatient_Soap_BillIngStatus
        @IDs_Patient_SOAP

      UPDATE [tBillingInvoice]
      SET    TotalItemDiscountAmount = biDetailDiscount.TotalItemDiscountAmount
      FROM   [tBillingInvoice] biHed
             INNER JOIN (SELECT [ID_BillingInvoice],
                                Sum(IsNull([DiscountAmount], 0)) TotalItemDiscountAmount
                         FROM   [tBillingInvoice_Detail]
                         GROUP  BY [ID_BillingInvoice]) biDetailDiscount
                     ON biHed.[ID] = biDetailDiscount.[ID_BillingInvoice]
      WHERE  biHed.ID = @ID_CurrentObject
  --if( @IsNew = 1      
  --    AND @ID_FilingStatus = @Filed_ID_FilingStatus )      
  --  BEGIN      
  --      SELECT @ID_USERSESSION = MAX(ID)      
  --      FROM   tUserSession      
  --      where  ID_User = @ID_CreatedBy      
  --      exec pApproveBillingInvoice      
  --        @IDs_BillingINvoice,      
  --        @ID_USERSESSION      
  --  END      
  END;

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_Confinement] (@ID_CurrentObject VARCHAR(10),
                                                          @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_Patient_SOAP INT;

            SELECT @ID_Company = ID_Company,
                   @ID_Patient_SOAP = ID_Patient_SOAP
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;

            UPDATE tPatient_SOAP
            SET    ID_Patient_Confinement = @ID_CurrentObject
            WHERE  ID = @ID_Patient_SOAP
        END;

      SELECT @ID_Patient = ID_Patient
      FROM   tPatient_Confinement
      WHERE  ID = @ID_CurrentObject

      UPDATE tPatient_SOAP
      SET    ID_Patient = @ID_Patient
      WHERE  ID_Patient_Confinement = @ID_CurrentObject

      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      VALUES (@ID_CurrentObject)

      EXEC PupDatePatient_ConfineMen_BillIngStatus
        @IDs_Patient_Confinement

      --EXEC _pCancelPatient_Confinement_temp
  END;

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_Soap] (@ID_CurrentObject VARCHAR(10),
                                                   @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_SOAP
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_SOAP';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_SOAP
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      DECLARE @IDs_Patient TYPINTLIST

      INSERT @IDs_Patient
      SELECT patient.ID
      FROM   tPatient_SOAP soap
             INNER JOIN tPatient patient
                     ON patient.ID = soap.ID_Patient
      WHERE  soap.ID = @ID_CurrentObject

      EXEC dbo.PupDatePatientsLastVisitedDate
        @IDs_Patient
  -- EXEC _pCancelPatient_Confinement_temp  
  END; 
