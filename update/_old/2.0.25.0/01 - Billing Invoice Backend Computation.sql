GO

CREATE OR
ALTER FUNCTION dbo.fGetBillingInvoiceComputatedColumns (@DetailTotalAmount        DECIMAL(18, 4),
                                                        @DiscountRate             DECIMAL(18, 4),
                                                        @DiscountAmount           DECIMAL(18, 4),
                                                        @IsComputeDiscountRate    BIT,
                                                        @ID_TaxScheme             INT,
                                                        @ConfinementDepositAmount DECIMAL(18, 4))
RETURNS @table TABLE (
  InitialSubtotalAmount  DECIMAL(18, 4),
  ConsumedDepositAmount  DECIMAL(18, 4),
  RemainingDepositAmount DECIMAL(18, 4),
  SubTotal               DECIMAL(18, 4),
  TotalAmount            DECIMAL(18, 4),
  DiscountRate           DECIMAL(18, 4),
  DiscountAmount         DECIMAL(18, 4),
  GrossAmount            DECIMAL(18, 4),
  VatAmount              DECIMAL(18, 4),
  NetAmount              DECIMAL(18, 4))
AS
  BEGIN
      DECLARE @InitialSubtotalAmount DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount DECIMAL(18, 4) = 0
      DECLARE @SubTotal DECIMAL(18, 4) = 0
      DECLARE @TotalAmount DECIMAL(18, 4) = 0
      DECLARE @GrossAmount DECIMAL(18, 4) = 0
      DECLARE @VatAmount DECIMAL(18, 4) = 0
      DECLARE @NetAmount DECIMAL(18, 4) = 0
      DECLARE @ID_TaxScheme_TaxExclusive INT = 1
      DECLARE @ID_TaxScheme_TaxInclusive INT = 2
      DECLARE @ID_TaxScheme_ZeroRated INT = 3

      SET @ID_TaxScheme = IsNull(@ID_TaxScheme, 0);
      SET @IsComputeDiscountRate = IsNull(@IsComputeDiscountRate, 0);
      SET @InitialSubtotalAmount = @DetailTotalAmount
      SET @SubTotal = @DetailTotalAmount
      /*Confinement Deposit Computation*/
      SET @RemainingDepositAmount = @ConfinementDepositAmount - @SubTotal

      IF @RemainingDepositAmount < 0
        BEGIN
            SET @RemainingDepositAmount = 0
        END

      SET @RemainingDepositAmount = Round(@RemainingDepositAmount, 2)
      SET @ConsumedDepositAmount = @ConfinementDepositAmount - @RemainingDepositAmount
      SET @SubTotal = @SubTotal - @ConfinementDepositAmount

      ------------------------------------------------------------
      /*Subtotal Total Amount*/
      IF @SubTotal <= 0
        BEGIN
            SET @SubTotal = 0;
        END

      SET @SubTotal = Round(@SubTotal, 2)
      SET @TotalAmount = @SubTotal

      ------------------------------------------------------------
      /*Discount Amount and Disount Rate*/
      IF @SubTotal > 0
        BEGIN
            IF( @IsComputeDiscountRate = 1 )
              BEGIN
                  SET @DiscountAmount = @TotalAmount * ( @DiscountRate / 100 )
                  SET @DiscountAmount = Round(@DiscountAmount, 2)
              END
            ELSE
              BEGIN
                  SET @DiscountRate = ( @DiscountAmount / @TotalAmount ) * 100
                  SET @DiscountRate = Round(@DiscountRate, 2)
              END
        END
      ELSE
        BEGIN
            SET @DiscountRate = 0
            SET @DiscountAmount = 0
        END

      ------------------------------------------------------------
      /*GrossAmount, VatAmount, NetAmount*/
      IF @SubTotal > 0
        BEGIN
            SET @TotalAmount = @TotalAmount - @DiscountAmount;
            SET @TotalAmount = Round(@TotalAmount, 2)
            SET @GrossAmount = @TotalAmount
            SET @VatAmount = ( @GrossAmount / 1.12 ) * 0.12
            SET @VatAmount = Round(@VatAmount, 2)

            IF( @ID_TaxScheme = 0
                 OR @ID_TaxScheme = @ID_TaxScheme_ZeroRated )
              BEGIN
                  SET @VatAmount = 0
                  SET @NetAmount = @GrossAmount
              END
            ELSE IF( @ID_TaxScheme = @ID_TaxScheme_TaxExclusive )
              BEGIN
                  SET @NetAmount = @GrossAmount + @VatAmount
              END
            ELSE IF( @ID_TaxScheme = @ID_TaxScheme_TaxInclusive )
              BEGIN
                  SET @NetAmount = @GrossAmount - @VatAmount
              END
        END

      INSERT @table
             (InitialSubtotalAmount,
              ConsumedDepositAmount,
              RemainingDepositAmount,
              SubTotal,
              TotalAmount,
              DiscountRate,
              DiscountAmount,
              GrossAmount,
              VatAmount,
              NetAmount)
      SELECT @InitialSubtotalAmount,
             @ConsumedDepositAmount,
             @RemainingDepositAmount,
             @SubTotal,
             @TotalAmount,
             @DiscountRate,
             @DiscountAmount,
             @GrossAmount,
             @VatAmount,
             @NetAmount

      ------------------------------------------------------------
      RETURN
  END

GO

CREATE OR
ALTER PROC pModel_AfterSaved_BillingInvoice_Computation(@IDs_BillingInvoice TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @ComputedBillingInvoice TABLE
        (
           RowIndex                 INT IDENTITY(1, 1),
           ID_BillingInvoice        INT,
           ID_TaxScheme             INT,
           IsComputeDiscountRate    BIT,
           ConfinementDepositAmount DECIMAL(18, 4),
           InitialSubtotalAmount    DECIMAL(18, 4),
           ConsumedDepositAmount    DECIMAL(18, 4),
           RemainingDepositAmount   DECIMAL(18, 4),
           SubTotal                 DECIMAL(18, 4),
           TotalAmount              DECIMAL(18, 4),
           DiscountRate             DECIMAL(18, 4),
           DiscountAmount           DECIMAL(18, 4),
           GrossAmount              DECIMAL(18, 4),
           VatAmount                DECIMAL(18, 4),
           NetAmount                DECIMAL(18, 4)
        )
      DECLARE @ComputedBillingInvoiceItems TABLE
        (
           ID_BillingInvoice        INT,
           ID_BillingInvoice_Detail INT,
           Quantity                 INT,
           UnitPrice                DECIMAL(18, 4),
           DiscountRate             DECIMAL(18, 4),
           DiscountAmount           DECIMAL(18, 4),
           IsComputeDiscountRate    BIT,
           Amount                   DECIMAL(18, 4)
        )

      INSERT @ComputedBillingInvoice
             (ID_BillingInvoice,
              ID_TaxScheme,
              IsComputeDiscountRate,
              DiscountRate,
              DiscountAmount,
              ConfinementDepositAmount)
      SELECT biHed.ID,
             biHed.ID_TaxScheme,
             bihed.IsComputeDiscountRate,
             biHed.DiscountRate,
             biHed.DiscountAmount,
             biHed.ConfinementDepositAmount
      FROM   dbo.tBillingInvoice biHed
             INNER JOIN @IDs_BillingInvoice ids
                     ON biHed.ID = ids.ID

      INSERT @ComputedBillingInvoiceItems
             (ID_BillingInvoice,
              ID_BillingInvoice_Detail,
              Quantity,
              UnitPrice,
              DiscountRate,
              DiscountAmount,
              IsComputeDiscountRate,
              Amount)
      SELECT ID_BillingInvoice,
             biDetail.ID,
             Quantity,
             UnitPrice,
             DiscountRate,
             DiscountAmount,
             IsComputeDiscountRate,
             Amount
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN @IDs_BillingInvoice ids
                     ON biDetail.ID_BillingInvoice = ids.ID

      -- Computed Billing Invoice Items 
      --     Amount 
      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = CONVERT(DECIMAL(18, 4), Quantity) * IsNull(UnitPrice, 0)

      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Round(Amount, 2)

      --     Discount Amount ( IsComputeDiscountRate IS TRUE)  
      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountAmount = Amount * ( DiscountRate / 100 )
      WHERE  IsComputeDiscountRate = 1

      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountAmount = Round(DiscountAmount, 2)
      WHERE  IsComputeDiscountRate = 1

      --     Discount Amount ( IsComputeDiscountRate IS FALSE)  
      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountRate = CASE
                              WHEN IsNull(Amount, 0) > 0 THEN ( DiscountAmount / Amount ) * 100
                              ELSE 0
                            END
      WHERE  IsNull(IsComputeDiscountRate, 0) = 0
             AND IsNull(Amount, 0) > 0

      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountRate = Round(DiscountRate, 2)
      WHERE  IsNull(IsComputeDiscountRate, 0) = 0

      --    Recompute Amount (Less Discount Amount)
      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Amount - DiscountAmount

      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Round(Amount, 2)

      -- Computed Billing Invoice
      -- SubTotal
      UPDATE @ComputedBillingInvoice
      SET    SubTotal = b.TotalAmount 
      FROM   @ComputedBillingInvoice a
             INNER JOIN (SELECT computedBillingInvoice.ID_BillingInvoice,
                                Sum(Amount) TotalAmount
                         FROM   @ComputedBillingInvoice computedBillingInvoice
                                INNER JOIN @ComputedBillingInvoiceItems computedBillingInvoiceItems
                                        ON computedBillingInvoice.ID_BillingInvoice = computedBillingInvoiceItems.ID_BillingInvoice
                         GROUP  BY computedBillingInvoice.ID_BillingInvoice) b
                     ON a.ID_BillingInvoice = b.ID_BillingInvoice

      UPDATE @ComputedBillingInvoice
      SET    InitialSubtotalAmount = computedBI.InitialSubtotalAmount,
             ConsumedDepositAmount = computedBI.ConsumedDepositAmount,
             RemainingDepositAmount = computedBI.RemainingDepositAmount,
             SubTotal = computedBI.SubTotal,
             TotalAmount = computedBI.TotalAmount,
             DiscountRate = computedBI.DiscountRate,
             DiscountAmount = computedBI.DiscountAmount,
             GrossAmount = computedBI.GrossAmount,
             VatAmount = computedBI.VatAmount,
             NetAmount = computedBI.NetAmount
      FROM   @ComputedBillingInvoice a
             CROSS APPLY dbo.fGetBillingInvoiceComputatedColumns(a.SubTotal, a.DiscountRate, a.DiscountAmount, a.IsComputeDiscountRate, a.ID_TaxScheme, a.ConfinementDepositAmount) computedBI

      UPDATE tBillingInvoice_Detail
      SET    Quantity = computedBIItems.Quantity,
             UnitPrice = computedBIItems.UnitPrice,
             DiscountRate = computedBIItems.DiscountRate,
             DiscountAmount = computedBIItems.DiscountAmount,
             Amount = computedBIItems.Amount
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN @ComputedBillingInvoiceItems computedBIItems
                     ON biDetail.ID = computedBIItems.ID_BillingInvoice_Detail

      UPDATE tBillingInvoice
      SET    InitialSubtotalAmount = computedBI.InitialSubtotalAmount,
             ConsumedDepositAmount = computedBI.ConsumedDepositAmount,
             RemainingDepositAmount = computedBI.RemainingDepositAmount,
             SubTotal = computedBI.SubTotal,
             TotalAmount = computedBI.TotalAmount,
             DiscountRate = computedBI.DiscountRate,
             DiscountAmount = computedBI.DiscountAmount,
             GrossAmount = computedBI.GrossAmount,
             VatAmount = computedBI.VatAmount,
             NetAmount = computedBI.NetAmount
      FROM   tBillingInvoice bi
             INNER JOIN @ComputedBillingInvoice computedBI
                     ON bi.ID = computedBI.ID_BillingInvoice
  END

GO

GO

ALTER PROC [dbo].[pModel_AfterSaved_BillingInvoice] (@ID_CurrentObject VARCHAR(10),
                                                     @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @HayopTownVeterinaryClinic_ID_Company INT = 73;
      DECLARE @Code VARCHAR(MAX) = '';
      DECLARE @ID_CreatedBy INT;
      DECLARE @ID_USERSESSION INT;
      DECLARE @ID_FilingStatus INT;
      DECLARE @ID_Company INT;
      DECLARE @IDs_BillingINvoice TYPINTLIST

      INSERT @IDs_BillingINvoice
      VALUES (@ID_CurrentObject)

      SELECT @ID_Company = ID_Company,
             @ID_FilingStatus = ID_FilingStatus,
             @ID_CreatedBy = ID_CreatedBy
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tBillingInvoice
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'BillingInvoice';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tBillingInvoice
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      /*Update Confinement Bill Status*/
      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      SELECT ID_Patient_Confinement
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      EXEC PupDatePatient_ConfineMen_BillIngStatus
        @IDs_Patient_Confinement

      /*Update SOAP Bill Status*/
      DECLARE @IDs_Patient_SOAP TYPINTLIST

      INSERT @IDs_Patient_SOAP
      SELECT ID_Patient_SOAP
      FROM   tBillingInvoice
      WHERE  ID = @ID_CurrentObject

      EXEC pUpdatePatient_SOAP_BillingStatus
        @IDs_Patient_SOAP

      UPDATE [tBillingInvoice]
      SET    TotalItemDiscountAmount = biDetailDiscount.TotalItemDiscountAmount
      FROM   [tBillingInvoice] biHed
             INNER JOIN (SELECT [ID_BillingInvoice],
                                Sum(IsNull([DiscountAmount], 0)) TotalItemDiscountAmount
                         FROM   [tBillingInvoice_Detail]
                         GROUP  BY [ID_BillingInvoice]) biDetailDiscount
                     ON biHed.[ID] = biDetailDiscount.[ID_BillingInvoice]
      WHERE  biHed.ID = @ID_CurrentObject

	  exec dbo.pModel_AfterSaved_BillingInvoice_Computation @IDs_BillingInvoice

  --if( @IsNew = 1    
  --    AND @ID_FilingStatus = @Filed_ID_FilingStatus )    
  --  BEGIN    
  --      SELECT @ID_USERSESSION = MAX(ID)    
  --      FROM   tUserSession    
  --      where  ID_User = @ID_CreatedBy    
  --      exec pApproveBillingInvoice    
  --        @IDs_BillingINvoice,    
  --        @ID_USERSESSION    
  --  END    
  END; 


  GO