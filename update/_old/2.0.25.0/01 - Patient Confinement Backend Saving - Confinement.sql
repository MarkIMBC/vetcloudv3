GO

CREATE OR
ALTER PROC pModel_AfterSaved_Patient_Confinement_Computation(@IDs_Patient_Confinement TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @ComputeditemsServices TABLE
        (
           ID_Patient_Confinement               INT,
           ID_Patient_Confinement_ItemsServices INT,
           Quantity                             INT,
           UnitPrice                            DECIMAL(18, 4),
           Amount                               DECIMAL(18, 4)
        )
      DECLARE @ComputedPatient_Confinement TABLE
        (
           ID_Patient_Confinement INT,
           TotalAmount            DECIMAL(18, 4)
        )

      INSERT @ComputeditemsServices
             (ID_Patient_Confinement,
              ID_Patient_Confinement_ItemsServices,
              Quantity,
              UnitPrice)
      SELECT itemServices.ID_Patient_Confinement,
             itemServices.ID,
             Quantity,
             UnitPrice
      FROM   tPatient_Confinement_ItemsServices itemServices
             INNER JOIN @IDs_Patient_Confinement ids
                     ON itemServices.ID_Patient_Confinement = ids.ID

      /*Compute Amount on Items Services*/
      UPDATE @ComputeditemsServices
      SET    Amount = CONVERT(DECIMAL(18, 4), Quantity) * IsNull(UnitPrice, 0)

      /*Compute Total Amount Patient Computation*/
      INSERT @ComputedPatient_Confinement
             (ID_Patient_Confinement,
              TotalAmount)
      SELECT ID_Patient_Confinement,
             Sum(Amount) TotalAmount
      FROM   @ComputeditemsServices
      GROUP  BY ID_Patient_Confinement

      /*Update Patient_Confinement_ItemsServices*/
      UPDATE tPatient_Confinement_ItemsServices
      SET    Amount = itemServComputed.Amount,
             UnitPrice = IsNull(itemServices.UnitPrice, 0)
      FROM   tPatient_Confinement_ItemsServices itemServices
             INNER JOIN @ComputeditemsServices itemServComputed
                     ON itemServices.ID = itemServComputed.ID_Patient_Confinement_ItemsServices

      /*Update Patient_Confinement*/
      UPDATE tPatient_Confinement
      SET    SubTotal = ComputedConfinement.TotalAmount,
             TotalAmount = ComputedConfinement.TotalAmount
      FROM   tPatient_Confinement confinement
             INNER JOIN @ComputedPatient_Confinement ComputedConfinement
                     ON confinement.ID = ComputedConfinement.ID_Patient_Confinement
  END

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_Confinement] (@ID_CurrentObject VARCHAR(10),
                                                          @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0
      DECLARE @IDs_Patient_Confinement TYPINTLIST

      INSERT @IDs_Patient_Confinement
      VALUES (@ID_CurrentObject)

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_Patient_SOAP INT;

            SELECT @ID_Company = ID_Company,
                   @ID_Patient_SOAP = ID_Patient_SOAP
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;

            UPDATE tPatient_SOAP
            SET    ID_Patient_Confinement = @ID_CurrentObject
            WHERE  ID = @ID_Patient_SOAP
        END;

      SELECT @ID_Patient = ID_Patient
      FROM   tPatient_Confinement
      WHERE  ID = @ID_CurrentObject

      UPDATE tPatient_SOAP
      SET    ID_Patient = @ID_Patient
      WHERE  ID_Patient_Confinement = @ID_CurrentObject

      EXEC pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement

      ------EXEC _pCancelPatient_Confinement_temp
      EXEC dbo.pModel_AfterSaved_Patient_Confinement_Computation
        @IDs_Patient_Confinement
		
	  exec dbo.pUpdateBillingInvoiceItemsByPatientConfinement @IDs_Patient_Confinement
  END;

GO

--DECLARE @IDs_Patient_Confinement TYPINTLIST
--INSERT @IDs_Patient_Confinement
--VALUES (3663),
--       (3672)
--EXEC pModel_AfterSaved_Patient_Confinement_Computation
--  @IDs_Patient_Confinement 
GO

CREATE OR
ALTER PROC pUpdateBillingInvoiceItemsByPatientConfinement(@IDs_Patient_Confinement TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @IDs_BillingInvoice TYPINTLIST
      DECLARE @Filed_ID_FilingStatus INT = 1

      INSERT @IDs_BillingInvoice
      SELECT biHed.ID
      FROM   tBillingInvoice biHed
             INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                     ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
      WHERE  ID_FilingStatus = @Filed_ID_FilingStatus

      -- Remove Deleted Confinement Items and Services on BIlling Invoice Items
      DECLARE @ForRemove_IDs_Patient_Confinement_ItemsServices TYPINTLIST

      INSERT @ForRemove_IDs_Patient_Confinement_ItemsServices
      SELECT ID_Patient_Confinement_ItemsServices
      FROM   (SELECT biDetail.ID_Patient_Confinement_ItemsServices
              FROM   tBillingInvoice_Detail biDetail
                     INNER JOIN tBillingInvoice biHed
                             ON biDetail.ID_BillingInvoice = biHed.ID
                     INNER JOIN @IDs_BillingInvoice IDsBI
                             ON biHed.ID = IDsBI.ID
                     INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                             ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
              WHERE  ID_FilingStatus = @Filed_ID_FilingStatus
              EXCEPT
              SELECT itemsServices.ID ID_Patient_Confinement_ItemsServices
              FROM   tPatient_Confinement_ItemsServices itemsServices
                     INNER JOIN @IDs_Patient_Confinement ids
                             ON itemsServices.ID_Patient_Confinement = ids.ID) tbl

      DELETE FROM tBillingInvoice_Detail
      WHERE  ID_Patient_Confinement_ItemsServices IN (SELECT ID
                                                      FROM   @ForRemove_IDs_Patient_Confinement_ItemsServices)
             AND ID_BillingInvoice IN (SELECT ID
                                       FROM   @IDs_BillingInvoice)

      -- Update Confinement Items and Services to BIlling Invoice Items
      UPDATE tBillingInvoice_Detail
      SET    ID_Item = confItemsServices.ID_Item,
             Quantity = confItemsServices.Quantity,
             UnitCost = confItemsServices.UnitCost,
             UnitPrice = confItemsServices.UnitPrice,
             DateExpiration = confItemsServices.DateExpiration
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN tPatient_Confinement_ItemsServices confItemsServices
                     ON biDetail.ID_Patient_Confinement_ItemsServices = confItemsServices.ID
             INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                     ON confItemsServices.ID_Patient_Confinement = ids_Patient_Confinement.ID
             INNER JOIN @IDs_BillingInvoice IDsBI
                     ON biDetail.ID_BillingInvoice = IDsBI.ID

      -- Insert Confinement Items and Services to BIlling Invoice Items
      -- TODO: Dapat able to insert sa seperate patient Confinement
      DECLARE @ForInsert_IDs_Patient_Confinement_ItemsServices TYPINTLIST

      INSERT @ForInsert_IDs_Patient_Confinement_ItemsServices
      SELECT ID_Patient_Confinement_ItemsServices
      FROM   (SELECT itemsServices.ID ID_Patient_Confinement_ItemsServices
              FROM   tPatient_Confinement_ItemsServices itemsServices
                     INNER JOIN @IDs_Patient_Confinement ids
                             ON itemsServices.ID_Patient_Confinement = ids.ID
              EXCEPT
              SELECT biDetail.ID_Patient_Confinement_ItemsServices
              FROM   tBillingInvoice_Detail biDetail
                     INNER JOIN tBillingInvoice biHed
                             ON biDetail.ID_BillingInvoice = biHed.ID
                     INNER JOIN @IDs_BillingInvoice IDsBI
                             ON biHed.ID = IDsBI.ID
                     INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                             ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
              WHERE  ID_FilingStatus = @Filed_ID_FilingStatus) tbl

      INSERT INTO [dbo].[tBillingInvoice_Detail]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_BillingInvoice],
                   [ID_Item],
                   [Quantity],
                   [UnitPrice],
                   [UnitCost],
                   [DateExpiration],
                   [Amount],
                   [IsComputeDiscountRate],
                   [DiscountAmount],
                   [DiscountRate],
                   [ID_Patient_Confinement_ItemsServices])
      SELECT 1,
             bihed.ID_Company,
             GetDate(),
             GetDate(),
             1,
             1,
             bihed.ID,
             itemsServices.ID_Item,
             itemsServices.Quantity,
             itemsServices.UnitPrice,
             itemsServices.UnitCost,
             itemsServices.DateExpiration,
             0,
             0,
             0,
             0,
             itemsServices.ID
      FROM   tPatient_Confinement_ItemsServices itemsServices
             INNER JOIN tPatient_Confinement confinement
                     ON itemsServices.ID_Patient_Confinement = confinement.ID
             INNER JOIN @ForInsert_IDs_Patient_Confinement_ItemsServices forINsertItemsServices
                     ON itemsServices.ID = forINsertItemsServices.ID
             INNER JOIN tBillingInvoice bihed
                     ON bihed.ID_Patient_Confinement = confinement.ID
             INNER JOIN @IDs_BillingInvoice idsBI
                     ON bihed.ID = idsBI.ID

      EXEC dbo.pModel_AfterSaved_BillingInvoice_Computation
        @IDs_BillingInvoice
  END

GO 
