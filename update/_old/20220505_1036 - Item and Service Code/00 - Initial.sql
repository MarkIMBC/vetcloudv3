IF COL_LENGTH('dbo.tItem', 'SKUCode') IS  NULL
  BEGIN
      EXEC _pAddModelProperty
        'tItem',
        'SKUCode',
        1
  END

GO

exec _pRefreshAllViews

GO

ALTER VIEW dbo.vItemServiceLookUp
AS
  SELECT ID,
         Code,
         Name,
         ISNULL(UnitPrice, 0) UnitPrice,
         ISNULL(UnitCost, 0)  UnitCost,
         ID_Company
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 1

GO

ALTER VIEW vRemainingQuantityPurchaseOrderDetail
AS
  SELECT detail.ID,
         detail.ID_Item,
         detail.Name_Item,
         item.Code Code_Item,
         detail.Quantity,
         detail.RemainingQuantity,
         detail.UnitPrice,
         detail.ID_PurchaseOrder,
         hed.ID_FilingStatus,
         hed.Name_FilingStatus,
         hed.ServingStatus_Name_FilingStatus,
         hed.ID_Company,
         detail.UnitCost
  FROM   vPurchaseOrder_Detail detail
         INNER JOIN vPurchaseOrder hed
                 On hed.ID = detail.ID_PurchaseOrder
         LEFT JOIn tItem item
                on item.ID = detail.ID_Item
  WHERE  ISNULL(detail.RemainingQuantity, 0) > 0

GO

ALTER VIEW [dbo].vItemInventoriableForBillingLookUp
AS
  SELECT ID,
         Code,
         Name,
         ISNULL(UnitPrice, 0)                                  UnitPrice,
         ISNULL(UnitCost, 0)                                   UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company,
         CASE
           WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
           ELSE ''
         END                                                   RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration)    RemainingDays,
         OtherInfo_DateExpiration,
         ID_ItemType
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO

ALTER VIEW dbo.vItemInventoriableLookUp
AS
  SELECT ID,
         Code,
         Name,
         FORMAT(ISNULL(UnitPrice, 0), '#,###,##0.00')          UnitPrice,
         FORMAT(ISNULL(UnitCost, 0), '#,###,##0.00')           UnitCost,
         FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount,
         ISNULL(CurrentInventoryCount, 0)                      CurrentInventoryCount,
         ID_Company
  FROM   dbo.vActiveItem
  WHERE  ID_ItemType = 2;

GO

ALTER VIEW dbo.vItemService_Listview
AS
  SELECT item.ID,
         item.Code,
         item.Name,
         item.ID_Company,
         ISNULL(item.UnitCost, 0)           UnitCost,
         ISNULL(item.UnitPrice, 0)          UnitPrice,
         item.IsActive,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '') Name_ItemCategory
  FROM   dbo.vitem item
  WHERE  item.ID_ItemType = 1
         AND ISNULL(item.IsActive, 0) = 1

GO

ALTER VIEW [dbo].[vItemInventoriable_ListvIew]
AS
  SELECT item.ID,
         item.Code,
         item.Name,
         CASE
           WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
           ELSE ''
         END                                                     RemainingBeforeExpired,
         DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
         item.OtherInfo_DateExpiration                           DateExpired,
         item.ID_Company,
         item.CurrentInventoryCount,
         item.ID_InventoryStatus,
         invtStatus.Name                                         Name_InventoryStatus,
         item.IsActive,
         item.UnitCost,
         Item.UnitPrice,
         item.OtherInfo_DateExpiration,
         item.ID_ItemCategory,
         ISNULL(item.Name_ItemCategory, '')                      Name_ItemCategory
  FROM   dbo.vitem item
         LEFT JOIN tInventoryStatus invtStatus
                on item.ID_InventoryStatus = invtStatus.ID
  WHERE  item.ID_ItemType = 2
         AND ISNULL(item.IsActive, 0) = 1

GO

ALTER PROC [dbo].[pModel_AfterSaved_Item] (@ID_CurrentObject VARCHAR(10),
                                           @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_ItemType INT;
            DECLARE @Services_ID_ItemType INT = 1
            DECLARE @Inventorialble_ID_ItemType INT = 2

            SELECT @ID_Company = ID_Company,
                   @ID_ItemType = ID_ItemType
            FROM   dbo.tItem
            WHERE  ID = @ID_CurrentObject;

            if( @ID_ItemType = @Inventorialble_ID_ItemType )
              BEGIN
                  SELECT @Oid_Model = m.Oid
                  FROM   dbo._tModel m
                  WHERE  m.TableName = 'tItem';
              END
            ELSE if( @ID_ItemType = @Services_ID_ItemType )
              BEGIN
                  SELECT @Oid_Model = m.Oid
                  FROM   dbo._tModel m
                  WHERE  m.TableName = 'tItemService';
              END

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tItem
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      EXEC PloGitEmuNitPrice
        @ID_CurrentObject

      EXEC PloGitEmuNitCost
        @ID_CurrentObject
  --EXEC dbo.pUpdateItemCurrentInventory    
  END;

GO 
