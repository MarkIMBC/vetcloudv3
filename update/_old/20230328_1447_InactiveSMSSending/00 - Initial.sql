GO

CREATE OR
ALTER VIEW vInactiveSMSSending_Listview
AS
  SELECT hed.*
  FROM   vActiveInactiveSMSSending hed

GO

CREATE OR
ALTER VIEW vPatient_Grooming_Detail_Listview
AS
  SELECT det.ID,
         hed.ID_Company,
         det.ID_Patient_Grooming,
         det.ID_Item,
         det.Name_Item,
         ISNULL(det.Comment, '') Comment,
         hed.ID_FilingStatus,
         hed.Date,
         hed.ID_Patient ,
         hed.Code ,
         hed.AttendingPhysician_Name_Employee 
  FROM   vPatient_Grooming_Detail det
         inner join vPatient_Grooming hed
                 on hed.ID = det.ID_Patient_Grooming

GO

CREATE OR
ALTER PROCEDURE pGetInactiveSMSSending @ID         INT = -1,
                                       @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           '--new--' AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vInactiveSMSSending H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE   OR
alter PROC [dbo].[pModel_AfterSaved_InactiveSMSSending] (@ID_CurrentObject VARCHAR(10),
                                                         @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @Code_Company VARCHAR(MAX)
            DECLARE @username VARCHAR(MAX);
            DECLARE @password VARCHAR(MAX);

            SELECT @ID_Company = ID_Company
            FROM   dbo.tInactiveSMSSending
            WHERE  ID = @ID_CurrentObject;

            SELECT @Code_Company = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            SELECT @username = Name
            FROM   tInactiveSMSSending
            WHERE  ID = @ID_CurrentObject;

            SET @username = replace(replace(replace(dbo.fGetCleanedString(@username), '.', ''), ',', ''), ' ', '')
            SET @username = LOWER(@username)
            SET @username = @Code_Company + '-' + @username
            SET @password = LEFT(NewId(), 4);

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tInactiveSMSSending';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tInactiveSMSSending
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

go

GO

CREATE  OR
ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PayablePayment'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_PayablePayment]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Grooming'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Patient_Grooming]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'InactiveSMSSending'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_InactiveSMSSending]
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

GO

CREATE   OR
ALTER PROC pGetPatient_Grooming @ID         INT = -1,
                                @ID_Client  INT = NULL,
                                @ID_Patient INT = NULL,
                                @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Patient_Grooming_Detail

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   fs.Name      Name_FilingStatus
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '-New-'     AS [Code],
                           GetDate()   AS Date,
                           NULL        AS [Name],
                           1           AS [IsActive],
                           NULL        AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           ID_FilingStatus,
                           @ID_Client  ID_Client,
                           @ID_Patient ID_Patient) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN tClient client
                          ON H.ID_Client = client.ID
                   LEFT JOIN tPatient patient
                          ON H.ID_Patient = patient.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Grooming H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Grooming_Detail
      where  ID_Patient_Grooming = @ID
  END

GO 
