
/*
exec dbo._pCreateAppModuleWithTable  'tVeterinaryHealthCertificate', 1, NULL, NULL
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'Date', 5
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'ID_Client', 2
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'ID_Patient', 2
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'PatientDateBirth', 5
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'DestinationAddress', 1
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'DateVaccinated', 5
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'ID_Item', 2
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'SerialNumber', 1
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'LotNumber', 1
exec _pAddModelProperty 'tVeterinaryHealthCertificate', 'AttendingPhysician_ID_Employee', 2

exec _pRefreshAllViews
*/
GO
select  Species FROM tPatient
CREATE OR
ALTER VIEW vVeterinaryHealthCertificate
AS
  SELECT H.*,
         UC.Name                        AS CreatedBy,
         UM.Name                        AS LastModifiedBy,
         client.Name                    Name_Client,
         patient.Name                   Name_Patient,
         attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee,
         item.Name                      Name_Item
  FROM   tVeterinaryHealthCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tItem item
                ON item.ID = H.ID_Item

GO

CREATE OR
ALTER VIEW vVeterinaryHealthCertificate_ListView
AS
  SELECT ID,
         Code,
		 ID_Company,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         DestinationAddress,
         ID_Item,
         Name_Item
  FROm   vVeterinaryHealthCertificate

GO 
