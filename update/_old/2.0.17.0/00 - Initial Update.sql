GO

CREATE OR
ALTER PROC pGetConfinmentDeposit(@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @ConfinementDepositAmount Decimal(18, 2)= 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3

      SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
      FROM   tClientDeposit
      WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )

      SELECT '_';

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount;
  END 

GO

ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount Decimal(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      if ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT COUNT(*)
         FROM   vUSER
         where  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = SUM(ISNULL(DepositAmount, 0))
            FROM   tClientDeposit
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
                   AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                        Name_FilingStatus,
                   client.Name                    Name_Client,
                   patient.Name                   Name_Patient,
                   client.Address                 BillingAddress,
                   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee,
                   @ConfinementDepositAmount      ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GETDATE()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @ConfinementDepositAmount ConfinementDepositAmount
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      if ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999 ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                           UnitPrice,
                   item.OtherInfo_DateExpiration                       DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP;
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                        UnitPrice,
                   ISNULL(item.UnitCost, 0)                         UnitCost,
                   item.OtherInfo_DateExpiration                    DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement;
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Detail
            WHERE  ID_BillingInvoice = @ID;
        END

      if ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID;
        END
  END; 
GO
ALTER PROC dbo.pApproveBillingInvoice (@IDs_BillingInvoice typIntList READONLY,
                                       @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          Update tBillingInvoice_Detail
          SET    UnitCost = ISNULL(item.UnitCost, 0)
          FROM   tBillingInvoice_Detail biDetail
                 INNER JOIn @IDs_BillingInvoice ids
                         on biDetail.ID_BillingInvoice = ids.iD
                 INNER JOIN tItem item
                         on biDetail.ID_Item = item.ID

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO dbo.tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 0 - detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          EXEC pUpdateItemCurrentInventory;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Used Deposit on Credit Logs
          Declare @IDs_ClientDeposit typIntList
          DECLARE @ClientCredits typClientCredit

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          cd.DepositAmount * -1,
                          bi.Code,
                          'Use Deposit from ' + cd.Code
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Used_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

ALTER PROC dbo.pCancelBillingInvoice (@IDs_BillingInvoice typIntList READONLY,
                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0
                 AND item.ID_ItemType = @Inventoriable_ID_ItemType;

          EXEC dbo.pUpdateItemCurrentInventory;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Rollback Deposit on Credit Logs
          Declare @IDs_ClientDeposit typIntList
          DECLARE @ClientCredits typClientCredit

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          cd.DepositAmount * -1,
                          bi.Code,
                          'Rollback Deposit from ' + cd.Code
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
GO

ALTER PROC dbo.pCancelPatient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                           @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_ClientDeposit typIntList

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tClientDeposit cd
                 inner join @IDs_Patient_Confinement idsCOnfinement
                         ON cd.ID_Patient_Confinement = idsCOnfinement.ID
                            and ID_FilingStatus = @Approved_ID_FilingStatus

          exec [pCancelPatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          EXEC dbo.pCancelClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Confinement bi
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          Update tPatient_SOAP
          SET    ID_FilingStatus = @Cancelled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   tPatient_SOAP soap
                 INNER JOIN tPatient_Confinement confinement
                         ON soap.ID_Patient_Confinement = confinement.ID
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON confinement.ID = ids.ID;

          exec [pCancelClientDeposit]
            @IDs_ClientDeposit,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
  GO

GO

ALTER PROCEDURE pGetClientDeposit @ID         INT = -1,
                                  @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @FilingStatus_ID_FilingStatus INT = 1;
      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL                          AS [_],
                           -1                            AS [ID],
                           NULL                          AS [Code],
                           NULL                          AS [Name],
                           1                             AS [IsActive],
                           NULL                          AS [ID_Company],
                           NULL                          AS [Comment],
                           NULL                          AS [DateCreated],
                           NULL                          AS [DateModified],
                           NULL                          AS [ID_CreatedBy],
                           NULL                          AS [ID_LastModifiedBy],
                           @FilingStatus_ID_FilingStatus ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vClientDeposit H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROC [dbo].[pApproveClientDeposit_validation] (@IDs_ClientDeposit typIntList READONLY,
                                                     @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      /* Validate Text Blast Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientDeposit bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientDeposit ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to approved:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pApproveClientDeposit] (@IDs_ClientDeposit typIntList READONLY,
                                          @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @NoAssignedClient_IDs_ClientDeposit TABLE
            (
               ID_ClientDeposit INT,
               ClientCount      INT
            )

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tClientDeposit bi
                 INNER JOIN @IDs_ClientDeposit ids
                         ON bi.ID = ids.ID;

          -- Add Deposit on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.DepositAmount,
                 Code,
                 Comment
          FROM   tClientDeposit cd
                 INNER JOIN @IDs_ClientDeposit ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE   OR
ALTER PROC [dbo].[pCancelClientDeposit_validation] (@IDs_ClientDeposit typIntList READONLY,
                                                    @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @PartiallyServed_ID_FilingStatus INT = 5;
      DECLARE @FullyServed_ID_FilingStatus INT = 6;
      DECLARE @OverServed_ID_FilingStatus INT = 7;
      DECLARE @Done_ID_FilingStatus INT = 13;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Text Blast Status is not Approved*/
      INSERT @ValidateNotApproved
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vClientDeposit bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_ClientDeposit ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus, @Done_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancelClientDeposit] (@IDs_ClientDeposit typIntList READONLY,
                                         @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tClientDeposit
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tClientDeposit bi
                 INNER JOIN @IDs_ClientDeposit ids
                         ON bi.ID = ids.ID;

          -- Subtract Deposit on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.DepositAmount * -1,
                 Code,
                 'Cancel Deposit'
          FROM   tClientDeposit cd
                 INNER JOIN @IDs_ClientDeposit ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 
