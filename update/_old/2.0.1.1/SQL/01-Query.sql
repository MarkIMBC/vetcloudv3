﻿IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatientAppointment'
    AND COLUMN_NAME = 'ID_FilingStatus')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatientAppointment'
                         ,'ID_FilingStatus'
                         , 2
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatientAppointment'
    AND COLUMN_NAME = 'DateDone')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatientAppointment'
                         ,'DateDone'
                         , 5
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatientAppointment'
    AND COLUMN_NAME = 'ID_DoneBy')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatientAppointment'
                         ,'ID_DoneBy'
                         , 2
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatientAppointment'
    AND COLUMN_NAME = 'DateCanceled')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatientAppointment'
                         ,'DateCanceled'
                         , 5
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatientAppointment'
    AND COLUMN_NAME = 'ID_CanceledBy')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatientAppointment'
                         ,'ID_CanceledBy'
                         , 2
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient'
    AND COLUMN_NAME = 'AnimalWellness')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient'
                         ,'AnimalWellness'
                         ,1
END;

GO
IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient'
    AND COLUMN_NAME = 'DateDeceased')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tPatient'
                         ,'DateDeceased'
                         ,5
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tItem'
    AND COLUMN_NAME = 'OtherInfo_DateExpiration')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tItem'
                         ,'OtherInfo_DateExpiration'
                         ,5
END;

GO

IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tItem'
    AND COLUMN_NAME = 'ID_InventoryStatus')
  = 0
BEGIN

  EXEC _pAddModelProperty 'tItem'
                         ,'ID_InventoryStatus'
                         ,2
END;

GO


IF (SELECT
      COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tBillingInvoice_Detail'
    AND COLUMN_NAME = 'DateExpiration')
  = 0
BEGIN

EXEC _pAddModelProperty @TableName = 'tBillingInvoice_Detail'
                       ,@PropertyName = 'DateExpiration'
                       ,@Type = 5
END;

GO


IF (NOT EXISTS (SELECT
      *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'dbo'
    AND TABLE_NAME = 'tPatient_SOAP'
    AND COLUMN_NAME = 'History')
  )
BEGIN

  EXEC _pAddModelProperty 'tPatient_SOAP'
                         ,'History'
                         ,1
END;


GO
IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.fGetAge')
    AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fGetAge

GO

EXEC _pRefreshAllViews

GO

CREATE FUNCTION fGetAge (@dateBirth DATETIME, @dateDateStart DateTime = NULL, @InvalidDateCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = @InvalidDateCaption

  DECLARE @date DATETIME
         ,@tmpdate DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @dateBirth IS NULL
    RETURN ''

  IF @dateDateStart IS NULL
	SET @dateDateStart = getdate()	

  SELECT
    @date = @dateBirth

  SELECT
    @tmpdate = @date

  SELECT
    @years = DATEDIFF(yy, @tmpdate, @dateDateStart) -
    CASE
      WHEN (MONTH(@date) > MONTH(@dateDateStart)) OR
        (MONTH(@date) = MONTH(@dateDateStart) AND
        DAY(@date) > DAY(@dateDateStart)) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(yy, @years, @tmpdate)
  SELECT
    @months = DATEDIFF(m, @tmpdate, @dateDateStart) -
    CASE
      WHEN DAY(@date) > DAY(@dateDateStart) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(m, @months, @tmpdate)
  SELECT
    @days = DATEDIFF(d, @tmpdate, @dateDateStart)
	 
  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 0 THEN 's'
      ELSE ''
    END + ' old'
  IF (@years = 0
    AND @months > 0)
    SET @result = CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 0 THEN 's'
      ELSE ''
    END + ' old'
  IF (@years = 0
    AND @months = 0)
    SET @result = CONVERT(VARCHAR(MAX), @days) + ' day' +
    CASE
      WHEN @days > 0 THEN 's'
      ELSE ''
    END + ' old'
	  
   RETURN @result
END

GO


IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.GetRemainingYearMonthDays')
    AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))

  DROP FUNCTION dbo.GetRemainingYearMonthDays

GO


CREATE FUNCTION dbo.GetRemainingYearMonthDays (@Date DATETIME, @RemainingCaption VARCHAR(MAX) = NULL, @InvalidRemainingCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = ''
  DECLARE @date1 DATETIME
         ,@date2 DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @Date IS NULL
    RETURN ''

  SET @RemainingCaption = ISNULL(@RemainingCaption, 'remaining')

  SELECT
    @date1 = GETDATE()
   ,@date2 = @Date

  SET @years = DATEDIFF(mm, @date1, @date2) / 12
  SET @months = DATEDIFF(mm, @date1, @date2) % 12
  SET @days = DATEDIFF(dd, DATEADD(mm, DATEDIFF(mm, @date1, @date2), @date1), @date2)

  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years = 0
    AND @months > 0)
    SET @result = CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 0 THEN 's'
      ELSE ''
    END + ' ' + @RemainingCaption

  IF (@years <= 0
    AND @months <= 0)
  BEGIN

    IF (ISNULL(@days, 0) > 0)
    BEGIN

      SET @result = CONVERT(VARCHAR(MAX), @days) + ' day' +
      CASE
        WHEN @days > 0 THEN 's'
        ELSE ''
      END + ' ' + @RemainingCaption

    END
    ELSE
    BEGIN
      SET @result = @InvalidRemainingCaption
    END

  END


  RETURN @result
END

GO


ALTER FUNCTION [dbo].[fGetInventoryStatus] (@Count int, @MinCount int, @MaxCount int)
RETURNS int	 AS
BEGIN
	Declare @ID_InventoryStatus_NoStock int = 1
	Declare @ID_InventoryStatus_LOW int = 2
	Declare @ID_InventoryStatus_Medium int = 3
	Declare @ID_InventoryStatus_High int = 4


	Declare @ID_InventoryStatus int = 0

	set @Count  = ISNULL(@Count, 0)
	set @MinCount  = ISNULL(@MinCount, 0)
	set @MaxCount  = ISNULL(@MaxCount, 0)


	if(@Count = 0)
		set @ID_InventoryStatus = @ID_InventoryStatus_NoStock
	else if(@Count BETWEEN @MinCount AND @MaxCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_Medium
	else if(@Count > @MaxCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_High
	else if(@Count < @MinCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_LOW


    RETURN @ID_InventoryStatus
END

GO


IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.vItemInventoriable_ListvIew'))
BEGIN

  DROP VIEW dbo.vItemInventoriable_ListvIew
END

GO


ALTER VIEW [dbo].[vAppointmentEvent]
AS
/* From tPatient_SOAP */
SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|' + CONVERT(VARCHAR(MAX), patientSOAP.ID) UniqueID,
       _model.Oid Oid_Model,
       _model.Name Name_Model,
       patientSOAP.ID_Company,
       patientSOAP.ID ID_CurrentObject,
       patientSOAPPlan.DateReturn DateStart,
       patientSOAPPlan.DateReturn DateEnd,
       FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd') FormattedDateStart,
       FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ') FormattedDateEnd,
       '' FormattedDateStartTime,
        '' FormattedDateEndTime,
       patientSOAP.Code ReferenceCode,
       patientSOAP.Name_Client Paticular,
       patientSOAP.Name_SOAPType + ' - ' + patientSOAPPlan.Name_Item Description
FROM dbo.vPatient_SOAP patientSOAP
    INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
        ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
    INNER JOIN dbo._tModel _model
        ON _model.TableName = 'tPatient_SOAP'
WHERE patientSOAP.ID_FilingStatus IN ( 1, 3 )
UNION ALL
/* From tPatientAppointment */
SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|' + CONVERT(VARCHAR(MAX), patientAppnt.ID) UniqueID,
       _model.Oid Oid_Model,
       _model.Name Name_Model,
       patientAppnt.ID_Company,
       patientAppnt.ID ID_CurrentObject,
       patientAppnt.DateStart DateStart,
       patientAppnt.DateEnd DateEnd,
       FORMAT(patientAppnt.DateStart, 'yyyy-MM-dd') FormattedDateStart,
       FORMAT(patientAppnt.DateEnd, 'yyyy-MM-dd') FormattedDateEnd,
       FORMAT(patientAppnt.DateStart, 'hh:mm tt') FormattedDateStartTime,
       FORMAT(patientAppnt.DateEnd, 'hh:mm tt') FormattedDateEndTime,
       patientAppnt.Code ReferenceCode,
       patientAppnt.Name_Client Paticular,
       patientAppnt.Name_Patient + ' - ' + patientAppnt.Name_SOAPType Description
FROM dbo.vPatientAppointment patientAppnt
    INNER JOIN dbo._tModel _model
        ON _model.TableName = 'tPatientAppointment'
WHERE
	patientAppnt.ID_FilingStatus IN (1)

GO

CREATE VIEW vItemInventoriable_ListvIew
AS

SELECT
  item.ID
 ,item.Name
 ,CASE
    WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired', 'Expired')
    ELSE ''
  END RemainingBeforeExpired
 ,DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays
 ,item.OtherInfo_DateExpiration DateExpired
 ,item.ID_Company
 ,item.CurrentInventoryCount
 ,item.ID_InventoryStatus
 ,invtStatus.Name Name_InventoryStatus
FROM dbo.titem item
LEFT JOIN tInventoryStatus invtStatus 
	on item.ID_InventoryStatus = invtStatus.ID
WHERE 
	item.ID_ItemType = 2

GO

IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.vItemInventoriableForBillingLookUp'))
BEGIN

  DROP VIEW dbo.vItemInventoriableForBillingLookUp
END

GO


CREATE VIEW vItemInventoriableForBillingLookUp
AS

SELECT
  ID
 ,Name
 ,FORMAT(ISNULL(UnitPrice, 0), '#,###,##0.00') UnitPrice
 ,FORMAT(ISNULL(UnitCost, 0), '#,###,##0.00') UnitCost
 ,FORMAT(ISNULL(CurrentInventoryCount, 0), '#,###,##0') FormattedCurrentInventoryCount
 ,ISNULL(CurrentInventoryCount, 0) CurrentInventoryCount
 ,ID_Company
 ,CASE
    WHEN OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(OtherInfo_DateExpiration, '', 'Expired')
    ELSE ''
  END RemainingBeforeExpired
 ,DATEDIFF(DAY, GETDATE(), OtherInfo_DateExpiration) RemainingDays
FROM dbo.vActiveItem
WHERE ID_ItemType = 2;

GO


ALTER VIEW dbo.vPatient
AS
SELECT
  H.*
 ,UC.Name AS CreatedBy
 ,UM.Name AS LastModifiedBy
 ,H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName
 ,gender.Name Name_Gender
 ,country.Name PhoneCode_Country
 ,client.Name Name_Client
FROM tPatient H
LEFT JOIN tUser UC
  ON H.ID_CreatedBy = UC.ID
LEFT JOIN tUser UM
  ON H.ID_LastModifiedBy = UM.ID
LEFT JOIN dbo.tGender gender
  ON H.ID_Gender = gender.ID
LEFT JOIN dbo.tCountry country
  ON H.ID_Country = country.ID
LEFT JOIN dbo.tClient client
  ON client.ID = H.ID_Client;

GO


ALTER VIEW dbo.vzPatientSOAP
AS
SELECT
  patientSOAP.ID
 ,patientSOAP.Code
 ,patientSOAP.Name_Client
 ,patientSOAP.Name_Patient
 ,patientSOAP.Date
 ,patientSOAP.Comment
 ,patientSOAP.Name_CreatedBy
 ,REPLACE(REPLACE(REPLACE(patientSOAP.Subjective, CHAR(9), '<br/>'), CHAR(10), '<br/>'), CHAR(13), '<br/>') Subjective
 ,REPLACE(REPLACE(patientSOAP.Objective, CHAR(9), '<br/>'), CHAR(13), '<br/>') Objective
 ,REPLACE(REPLACE(patientSOAP.Assessment, CHAR(9), '<br/>'), CHAR(13), '<br/>') Assessment
 ,dbo.fGetPatient_SOAP_String(patientSOAP.ID) Planning
 ,REPLACE(REPLACE(patientSOAP.Prescription, CHAR(9), '<br/>'), CHAR(13), '<br/>') Prescription
 ,patientSOAP.ID_Company
 ,patientSOAP.Name_FilingStatus
 ,company.ImageLogoLocationFilenamePath
 ,company.Name Name_Company
 ,company.Address Address_Company
FROM dbo.vPatient_SOAP patientSOAP
LEFT JOIN dbo.vCompany company
  ON company.ID = patientSOAP.ID_Company;

GO

ALTER VIEW dbo.vzInventoryDetailReport
AS
SELECT hed.*,
       item.Name Name_Item,
       status.Name Name_FilingStatus,
       CONVERT(VARCHAR, hed.Date, 9) DateString,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   invtStatus.Name Name_InventoryStatus
FROM dbo.tInventoryTrail hed
    LEFT JOIN dbo.tItem item
        ON item.ID = hed.ID_Item
    LEFT JOIN dbo.tFilingStatus status
        ON status.ID = hed.ID_FilingStatus
    LEFT JOIN dbo.vCompany company
        ON company.ID = item.ID_Company
	LEFT JOIN dbo.tInventoryStatus invtStatus
		ON invtStatus.ID = item.ID_InventoryStatus
GO


ALTER VIEW dbo.vzInventorySummaryReport
AS
SELECT hed.ID_Item ID,
       hed.Name_Item,
       SUM(hed.Quantity) TotalQuantity,
       hed.DateExpired,
       hed.BatchNo,
       MAX(   CASE
                  WHEN hed.Quantity > 0 THEN
                      Date
                  ELSE
                      NULL
              END
          ) DateLastIn,
       MAX(   CASE
                  WHEN hed.Quantity < 0 THEN
                      Date
                  ELSE
                      NULL
              END
          ) DateLastOut,
       item.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company,
	   CASE
			WHEN item.OtherInfo_DateExpiration IS NOT NULL THEN '(' + dbo.GetRemainingYearMonthDays(item.OtherInfo_DateExpiration, 'before expired' + ')', 'Expired')
		ELSE ''
			END RemainingBeforeExpired,
		DATEDIFF(DAY, GETDATE(), item.OtherInfo_DateExpiration) RemainingDays,
		invtStatus.Name Name_InventoryStatus
FROM dbo.vInventoryTrail hed
LEFT JOIN dbo.tItem item 
	ON item.ID = hed.ID_Item
LEFT JOIN dbo.vCompany company
    ON company.ID = item.ID_Company
LEFT JOIN dbo.tInventoryStatus invtStatus
    ON invtStatus.ID = item.ID_InventoryStatus
GROUP BY ID_Item,
         Name_Item,
         DateExpired,
         BatchNo,
         company.ImageLogoLocationFilenamePath,
         company.Name,
		 OtherInfo_DateExpiration,
         company.Address,
         item.ID_Company,
		 invtStatus.Name
GO

ALTER VIEW [dbo].[vPatientAppointment]
AS
SELECT H.*,
       CONVERT(VARCHAR, H.DateStart, 0) DateStartString,
       CONVERT(VARCHAR, H.DateEnd, 0) DateEndString,
       SOAPType.Name Name_SOAPType,
       client.Name Name_Client,
       patient.Name Name_Patient,
	   fs.Name Name_FilingStatus
FROM dbo.tPatientAppointment H
    LEFT JOIN dbo.tSOAPType SOAPType
        ON SOAPType.ID = H.ID_SOAPType
    LEFT JOIN dbo.tClient client
        ON client.ID = H.ID_Client
    LEFT JOIN dbo.tPatient patient
        ON patient.ID = H.ID_Patient
	LEFT JOIN tFilingStatus fs
		ON fs.ID = H.ID_FilingStatus

GO

ALTER PROC dbo.pGetItem @ID INT = -1,
@ID_ItemType INT = NULL,
@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_';

  DECLARE @ID_User INT
         ,@ID_Warehouse INT;
  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM tUserSession
  WHERE ID = @ID_Session;

  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,itemType.Name Name_ItemType
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,NULL AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [ID_Company]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,NULL AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_ItemType ID_ItemType) H
    LEFT JOIN tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tItemType itemType
      ON itemType.ID = H.ID_ItemType;
  END;
  ELSE
  BEGIN
    SELECT
      H.*
     ,CASE
        WHEN H.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(H.OtherInfo_DateExpiration, '', 'Expired')
        ELSE ''
      END RemainingBeforeExpired
     ,DATEDIFF(DAY, GETDATE(), H.OtherInfo_DateExpiration) RemainingDays
    FROM vItem H
    WHERE H.ID = @ID;
  END;
END;

GO

  
Alter PROC dbo.pGetPatient    
    @ID INT = -1,    
    @ID_Client INT = NULL,    
    @ID_Session INT = NULL    
AS    
BEGIN    
    SELECT '_',    
           '' AS Patient_DentalExamination,    
           '' AS Patient_History,    
           '' AS Patient_SOAP_RegularConsoltation,    
           '' AS Patient_SOAP;    
    
    DECLARE @Approved_ID_FIlingStatus INT = 3;    
    DECLARE @Cancelled_ID_FIlingStatus INT = 4;    
    
    DECLARE @ID_User INT,    
            @ID_Employee INT,    
            @ID_Company INT,    
            @ID_Warehouse INT;    
    
    SELECT @ID_User = ID_User,    
           @ID_Warehouse = ID_Warehouse    
    FROM tUserSession    
    WHERE ID = @ID_Session;    
    
    SELECT @ID_Employee = ID_Employee    
    FROM dbo.tUser    
    WHERE ID = @ID_User;    
    
    SELECT @ID_Company = ID_Company    
    FROM dbo.tEmployee    
    WHERE ID = @ID_Employee;    
    
    IF (@ID = -1)    
    BEGIN    
        SELECT H.*,    
               client.Name Name_Client    
        FROM    
        (    
            SELECT NULL AS [_],    
                   -1 AS [ID],    
                   NULL AS [Code],    
                   NULL AS [Name],    
                   1 AS [IsActive],    
                   NULL AS [Comment],    
                   NULL AS [DateCreated],    
                   NULL AS [DateModified],    
                   NULL AS [ID_CreatedBy],    
                   NULL AS [ID_LastModifiedBy],    
                   @ID_Client AS ID_Client    
        ) H    
            LEFT JOIN dbo.tUser UC    
                ON H.ID_CreatedBy = UC.ID    
            LEFT JOIN dbo.tUser UM    
                ON H.ID_LastModifiedBy = UM.ID    
            LEFT JOIN dbo.tClient client    
                ON client.ID = H.ID_Client;    
    END;    
    ELSE    
    BEGIN    
        SELECT H.*, dbo.fGetAge(h.DateBirth, case WHEN ISNULL(h.IsDeceased,0) = 1 then  h.DateDeceased ELSE NULL END ,'N/A') Age    
        FROM dbo.vPatient H    
        WHERE H.ID = @ID;    
    END;    
    
    SELECT *    
    FROM dbo.vPatient_DentalExamination    
    WHERE ID_Patient = @ID;    
    
    SELECT *    
    FROM dbo.vPatient_History    
    WHERE ID_Patient = @ID    
    ORDER BY Date DESC;    
    
    SELECT *    
    FROM dbo.tPatient_SOAP_RegularConsoltation    
    WHERE ID_Patient = @ID;    
    
    SELECT *    
    FROM dbo.vPatient_SOAP    
    WHERE ID_Patient = @ID    
      AND ID_FilingStatus NOT IN (@Cancelled_ID_FIlingStatus)    
    ORDER BY Date DESC;    
    
END;   

Go



ALTER PROCEDURE [dbo].[pGetPatient_SOAP] @ID INT = -1,
	@ID_Patient INT = NULL,
	@ID_SOAPType INT = NULL,
	@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'
   ,'' AS LabImages
   ,'' AS Patient_SOAP_Plan;

  DECLARE @ID_User INT;
  DECLARE @ID_Warehouse INT;
  DECLARE @FILED_ID_FilingStatus INT = 1;
  DECLARE @IsDeceased BIT = 1;
  DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: '+ CHAR(9) + CHAR(13) +
											'Blood Chem: '+ CHAR(9) + CHAR(13) +
											'Microscopic Exam: ';

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM dbo.tUserSession
  WHERE ID = @ID_Session;

  SELECT  @IsDeceased = Isnull(IsDeceased,0)
  FROM tPatient WHERE ID = @ID_Patient;
  
  DECLARE @LabImage TABLE (
    ImageRowIndex INT
   ,RowIndex INT
   ,ImageNo VARCHAR(MAX)
   ,FilePath VARCHAR(MAX)
   ,Remark VARCHAR(MAX)
  );

  INSERT @LabImage (ImageRowIndex, RowIndex, ImageNo, FilePath, Remark)
  SELECT
    c.ImageRowIndex
   ,ROW_NUMBER() OVER (ORDER BY ImageNo) AS RowIndex
   ,c.ImageNo
   ,FilePath
   ,c.Remark
  FROM tPatient_SOAP ps
  CROSS APPLY (SELECT
      '01'
     ,LabImageRowIndex01
     ,LabImageFilePath01
     ,LabImageRemark01
    UNION ALL
    SELECT
      '02'
     ,LabImageRowIndex02
     ,LabImageFilePath02
     ,LabImageRemark02
    UNION ALL
    SELECT
      '03'
     ,LabImageRowIndex03
     ,LabImageFilePath03
     ,LabImageRemark03
    UNION ALL
    SELECT
      '04'
     ,LabImageRowIndex04
     ,LabImageFilePath04
     ,LabImageRemark04
    UNION ALL
    SELECT
      '05'
     ,LabImageRowIndex05
     ,LabImageFilePath05
     ,LabImageRemark05
    UNION ALL
    SELECT
      '06'
     ,LabImageRowIndex06
     ,LabImageFilePath06
     ,LabImageRemark06
    UNION ALL
    SELECT
      '07'
     ,LabImageRowIndex07
     ,LabImageFilePath07
     ,LabImageRemark07
    UNION ALL
    SELECT
      '08'
     ,LabImageRowIndex08
     ,LabImageFilePath08
     ,LabImageRemark08
    UNION ALL
    SELECT
      '09'
     ,LabImageRowIndex09
     ,LabImageFilePath09
     ,LabImageRemark09
    UNION ALL
    SELECT
      '10'
     ,LabImageRowIndex10
     ,LabImageFilePath10
     ,LabImageRemark10
    UNION ALL
    SELECT
      '11'
     ,LabImageRowIndex11
     ,LabImageFilePath11
     ,LabImageRemark11
    UNION ALL
    SELECT
      '12'
     ,LabImageRowIndex12
     ,LabImageFilePath12
     ,LabImageRemark12
    UNION ALL
    SELECT
      '13'
     ,LabImageRowIndex13
     ,LabImageFilePath13
     ,LabImageRemark13
    UNION ALL
    SELECT
      '14'
     ,LabImageRowIndex14
     ,LabImageFilePath14
     ,LabImageRemark14
    UNION ALL
    SELECT
      '15'
     ,LabImageRowIndex15
     ,LabImageFilePath15
     ,LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
  WHERE ps.ID = @ID
        AND ISNULL(FilePath, '') <> ''
  ORDER BY c.ImageRowIndex ASC;


  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,patient.Name Name_Patient
     ,patient.Name_Client
     ,fs.Name Name_FilingStatus
     ,soapType.Name Name_SOAPType
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,'-New-' AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [ID_Company]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,@ID_User AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_Patient AS ID_Patient
       ,GETDATE() DATE
       ,@FILED_ID_FilingStatus ID_FilingStatus
	   ,@IsDeceased IsDeceased
       ,@ID_SOAPType ID_SOAPType
	   ,@LaboratoryTemplate LaboratoryTemplate) H
    LEFT JOIN dbo.tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vPatient patient
      ON patient.ID = H.ID_Patient
    LEFT JOIN dbo.tFilingStatus fs
      ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tSOAPType soapType
      ON soapType.ID = H.ID_SOAPType;
  END;
  ELSE
  BEGIN
    SELECT
      H.*
	  , patient.IsDeceased, @LaboratoryTemplate LaboratoryTemplate
    FROM dbo.vPatient_SOAP H
	LEFT JOIN tPatient patient on h.ID_Patient = patient.ID
    WHERE H.ID = @ID;
  END;


  SELECT * FROM @LabImage ORDER BY ImageRowIndex DESC;

  SELECT
    *
  FROM dbo.vPatient_SOAP_Plan
  WHERE ID_Patient_SOAP = @ID
  ORDER BY DateReturn ASC;
END;


GO


ALTER PROC dbo.pApproveBillingInvoice
(
    @IDs_BillingInvoice typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Approved_ID_FilingStatus INT = 3;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pApproveBillingInvoice_validation @IDs_BillingInvoice,
                                                   @ID_UserSession;

        UPDATE dbo.tBillingInvoice
        SET ID_FilingStatus = @Approved_ID_FilingStatus,
            DateApproved = GETDATE(),
            ID_ApprovedBy = @ID_User
        FROM dbo.tBillingInvoice bi
            INNER JOIN @IDs_BillingInvoice ids
                ON bi.ID = ids.ID;

        /*Inventory Trail */
        INSERT INTO dbo.tInventoryTrail
        (
            Code,
            ID_Company,
            DateCreated,
            ID_Item,
            Quantity,
            UnitPrice,
            ID_FilingStatus,
            Date,
            DateExpired
        )
        SELECT hed.Code,
               hed.ID_Company,
               hed.DateCreated,
               detail.ID_Item,
               0 - detail.Quantity,
               detail.UnitPrice,
               hed.ID_FilingStatus,
               hed.Date,
               detail.DateExpiration
        FROM dbo.tBillingInvoice hed
            LEFT JOIN dbo.tBillingInvoice_Detail detail
                ON hed.ID = detail.ID_BillingInvoice
            LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item
            INNER JOIN @IDs_BillingInvoice ids
                ON hed.ID = ids.ID
        WHERE item.ID_ItemType = @Inventoriable_ID_ItemType;

        EXEC pUpdateItemCurrentInventory;

        EXEC dbo.pUpdateBillingInvoicePayment @IDs_BillingInvoice;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

ALTER PROC dbo.pCancelBillingInvoice
(
    @IDs_BillingInvoice typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Inventoriable_ID_ItemType INT = 2;
    DECLARE @Canceled_ID_FilingStatus INT = 4;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelBillingInvoice_validation @IDs_BillingInvoice,
                                                  @ID_UserSession;

        UPDATE dbo.tBillingInvoice
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tBillingInvoice bi
            INNER JOIN @IDs_BillingInvoice ids
                ON bi.ID = ids.ID;

        /*Inventory Trail */
        INSERT INTO tInventoryTrail
        (
            Code,
            ID_Company,
            DateCreated,
            ID_Item,
            Quantity,
            UnitPrice,
            ID_FilingStatus,
            Date,
            DateExpired
        )
        SELECT hed.Code,
               hed.ID_Company,
               hed.DateCreated,
               detail.ID_Item,
               detail.Quantity,
               detail.UnitPrice,
               hed.ID_FilingStatus,
               hed.Date,
               detail.DateExpiration 
        FROM dbo.tBillingInvoice hed
            LEFT JOIN dbo.tBillingInvoice_Detail detail
                ON hed.ID = detail.ID_BillingInvoice
            LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item
            INNER JOIN @IDs_BillingInvoice ids
                ON hed.ID = ids.ID
        WHERE ISNULL(hed.ID_ApprovedBy, 0) <> 0
              AND item.ID_ItemType = @Inventoriable_ID_ItemType;

        EXEC dbo.pUpdateItemCurrentInventory;

        EXEC dbo.pUpdateBillingInvoicePayment @IDs_BillingInvoice;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

Drop Proc pReceiveInventory
Go

/****** Object:  UserDefinedTableType [dbo].[typReceiveInventory]    Script Date: 2/13/2021 4:27:49 AM ******/
DROP TYPE [dbo].[typReceiveInventory]
GO

/****** Object:  UserDefinedTableType [dbo].[typReceiveInventory]    Script Date: 2/13/2021 4:27:49 AM ******/
CREATE TYPE [dbo].[typReceiveInventory] AS TABLE(
	[Code] [varchar](50) NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [decimal](18, 4) NULL DEFAULT ((0)),
	[DateExpired] [datetime] NULL,
	[BatchNo] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_Company] [int] NULL,
	Comment [varchar](MAX) NULL,
	IsAddInventory BIT
)
GO


Create PROC dbo.pReceiveInventory (@record typReceiveInventory READONLY, @ID_UserSession Int)  
AS  
BEGIN  
  
  DECLARE @Success BIT = 1;  
  DECLARE @message VARCHAR(MAX) = '';  
   
  DECLARE @ID_User INT = 0;

  DECLARE @count_noitemid INT = 0;  

  SELECT @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;
  
  BEGIN TRY  
  
    SELECT  
      @count_noitemid = COUNT(*)  
    FROM @record  
    WHERE ISNULL(ID_Item, 0) = 0;  
  
    IF (@count_noitemid > 0)  
    BEGIN;  
  
      THROW 50005, N'Reference Item is required.', 1;  
    END;  
  
    INSERT INTO dbo.tInventoryTrail (Code,  
    ID_Item,  
    Quantity,  
    UnitPrice,  
    DateExpired,  
    BatchNo,  
    ID_FilingStatus,  
    ID_Company,  
    DateCreated,  
    Date, 
	Comment,
	ID_CreatedBy)  
      SELECT  
        CASE  
          WHEN ISNULL(record.Code,'') = '' THEN 'Adjust Inventory'  
          ELSE record.Code  
        END  
       ,record.ID_Item  
       ,case when record.IsAddInventory = 0 then  0 - Abs(record.Quantity) else Abs(record.Quantity) end
       ,record.UnitPrice  
       ,record.DateExpired  
       ,record.BatchNo  
       ,record.ID_FilingStatus  
       ,item.ID_Company  
       ,GETDATE()  
       ,GETDATE()
	   ,record.Comment,
	   @ID_User
      FROM @record record  
      INNER JOIN dbo.tItem item  
        ON item.ID = record.ID_Item;  
  
    EXEC dbo.pUpdateItemCurrentInventory;  
  END TRY  
  BEGIN CATCH  
  
    SET @Success = 0;  
    SET @message = ERROR_MESSAGE();  
  END CATCH;  
  
  SELECT  
    '_';  
  
  SELECT  
    @Success Success  
   ,@message message;  
END;  
  

GO

 
ALTER PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

  DECLARE @Success BIT = 1;
  Declare @MDIgnacioAnimalClinic_ID_Company INT = 8
  Declare @AnimaCareVeterinaryClinic_ID_Company INT = 33

  SELECT
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

  SELECT
    client.Name Name_Client
   ,client.ContactNumber ContactNumber_Client
   ,soapPlan.ID ID_Patient_SOAP_Plan
   ,soapPlan.DateReturn
   ,soapPlan.Name_Item
   ,ISNULL(patientSOAP.Comment, '') Comment
   ,'Good day, ' + client.Name + CHAR(9) + CHAR(13) + ' You have an upcoming appointment for your pet ''' +
    patient.Name  + '''' + ' at ' +  c.Name   + ' at ' +
    FORMAT(soapPlan.DateReturn, 'MM/dd/yyyy dddd') + '.' + CHAR(9) + CHAR(13) +
    'Plan: ' + +CHAR(9) + CHAR(13) +
    ' ' + soapPlan.Name_Item +
    CASE
      WHEN LEN(ISNULL(soapPlan.Comment, '')) > 0 THEN ' - '
      ELSE ''
    END + ISNULL(soapPlan.Comment, '')   + CHAR(9) + CHAR(13) + Case WHEN LEN(ISNULL(c.ContactNumber, '')) > 0 THEN 'If you have questions, kindly message ' + c.ContactNumber + '.' ELSE '' END  Message,
    CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
  FROM dbo.tPatient_SOAP patientSOAP
  LEFT JOIN dbo.tPatient patient
    ON patient.ID = patientSOAP.ID_Patient
  LEFT JOIN dbo.tClient client
    ON client.ID = patient.ID_Client
  LEFT JOIN tCompany c
    ON c.iD = patientSOAP.ID_Company
  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
    ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
  WHERE patientSOAP.ID_FilingStatus IN (3)
	AND ISNULL(soapPlan.IsSentSMS, 0) = 0
	AND (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE()))
	AND patientSOAP.ID_Company IN (@MDIgnacioAnimalClinic_ID_Company, @AnimaCareVeterinaryClinic_ID_Company, 1)
END

Go

ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@IDsString VARCHAR(MAX))
AS
BEGIN

  DECLARE @IDs typIntList

  INSERT @IDs (ID)
    SELECT
      Part
    FROM dbo.fGetSplitString(@IDsString, ',')

  UPDATE dbo.tPatient_SOAP_Plan
  SET IsSentSMS = 1
     ,DateSent = GETDATE()
  FROM tPatient_SOAP_Plan psp
  INNER JOIN @ids ids
    ON psp.ID = ids.ID

  DECLARE @Success BIT = 1;

  SELECT '_'

  SELECT @Success Success;

END

GO
USE [db_vetcloudv3_WaitingListReference_dev]
GO

ALTER PROC [dbo].[pGetPatientAppointment]
    @ID INT = -1,
    @DateStart DATETIME = NULL,
    @DateEnd DATETIME = NULL,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_';

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   NULL AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   @DateStart DateStart,
                   @DateEnd DateEnd,
				   1 [ID_FilingStatus]
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM vPatientAppointment H
        WHERE H.ID = @ID;
    END;
END;

GO

IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pDonePatientAppointment_validation'))
BEGIN

  DROP PROC dbo.pDonePatientAppointment_validation
END
GO
 
IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pDonePatientAppointment'))
BEGIN

  DROP PROC dbo.pDonePatientAppointment
END
GO
 
IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pCanceledPatientAppointment_validation'))
BEGIN

  DROP PROC dbo.pCanceledPatientAppointment_validation
END
GO
 
IF EXISTS (SELECT
      *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'dbo.pCanceledPatientAppointment'))
BEGIN

  DROP PROC dbo.pCanceledPatientAppointment
END
GO

Create PROC [dbo].[pDonePatientAppointment_validation]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Filed_ID_FilingStatus INT = 1;
    DECLARE @message VARCHAR(400) = '';

    DECLARE @ValidateNotFiled TABLE
    (
        Code VARCHAR(30),
        Name_FilingStatus VARCHAR(30)
    );
    DECLARE @Count_ValidateNotFiled INT = 0;


    /* Validate Billing Invoices Status is not Filed*/
    INSERT @ValidateNotFiled
    (
        Code,
        Name_FilingStatus
    )
    SELECT Code,
           Name_FilingStatus
    FROM dbo.vPatientAppointment bi
    WHERE EXISTS
    (
        SELECT ID FROM @IDs_PatientAppointment ids WHERE ids.ID = bi.ID
    )
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

    SELECT @Count_ValidateNotFiled = COUNT(*)
    FROM @ValidateNotFiled;

    IF (@Count_ValidateNotFiled > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotFiled > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to Doned:';

        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus
        FROM @ValidateNotFiled;
        THROW 50001, @message, 1;

    END;

END;


GO

Create PROC [dbo].[pDonePatientAppointment]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Done_ID_FilingStatus INT = 13;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pDonePatientAppointment_validation @IDs_PatientAppointment,
                                                  @ID_UserSession;

        UPDATE dbo.tPatientAppointment
        SET ID_FilingStatus = @Done_ID_FilingStatus,
            DateDone = GETDATE(),
            ID_DoneBy = @ID_User
        FROM dbo.tPatientAppointment bi
            INNER JOIN @IDs_PatientAppointment ids
                ON bi.ID = ids.ID;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;
	 
    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO
 
Create PROC [dbo].[pCanceledPatientAppointment_validation]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Filed_ID_FilingStatus INT = 1;
    DECLARE @message VARCHAR(400) = '';

    DECLARE @ValidateNotFiled TABLE
    (
        Code VARCHAR(30),
        Name_FilingStatus VARCHAR(30)
    );
    DECLARE @Count_ValidateNotFiled INT = 0;

    INSERT @ValidateNotFiled
    (
        Code,
        Name_FilingStatus
    )
    SELECT Code,
           Name_FilingStatus
    FROM dbo.vPatientAppointment bi
    WHERE EXISTS
    (
        SELECT ID FROM @IDs_PatientAppointment ids WHERE ids.ID = bi.ID
    )
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

    SELECT @Count_ValidateNotFiled = COUNT(*)
    FROM @ValidateNotFiled;

    IF (@Count_ValidateNotFiled > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotFiled > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to Canceled:';

        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus
        FROM @ValidateNotFiled;
        THROW 50001, @message, 1;

    END;

END;


GO
Create PROC [dbo].[pCanceledPatientAppointment]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Canceled_ID_FilingStatus INT = 4;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCanceledPatientAppointment_validation @IDs_PatientAppointment,
                                                  @ID_UserSession;

        UPDATE dbo.tPatientAppointment
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tPatientAppointment bi
            INNER JOIN @IDs_PatientAppointment ids
                ON bi.ID = ids.ID;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;
	 
    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO

ALTER PROC [dbo].[pUpdateItemCurrentInventory]
AS
BEGIN

    UPDATE dbo.tItem
    SET CurrentInventoryCount = ISNULL(inventory.Qty, 0),
		ID_InventoryStatus = dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount,0),  ISNULL(MaxInventoryCount,0))
    FROM dbo.tItem item
        LEFT JOIN
        (
			SELECT inventory.ID_Item,
                   SUM(inventory.Quantity) Qty
            FROM dbo.tInventoryTrail inventory
			INNER JOIN tItem item on item.ID = inventory.ID_Item
            GROUP BY 
				inventory.ID_Item,
				item.MinInventoryCount,
				item.MaxInventoryCount
        ) inventory
            ON inventory.ID_Item = item.ID;
END;

GO

/* Update tPatientAppointment with  NULL value as 1 (Filed) in ID_FilingStatus */
UPDATE tPatientAppointment Set ID_FilingStatus = 1 WHERE ISNULL(ID_FilingStatus, 0) = 0

