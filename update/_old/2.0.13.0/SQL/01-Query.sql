if( COL_LENGTH('dbo.tCompany', 'IsShowPOSReceiptLogo') ) = 0
  begin
      exec _pAddModelProperty
        'tCompany',
        'IsShowPOSReceiptLogo',
        4;

      ALTER TABLE tCompany
        ADD DEFAULT(1) FOR IsShowPOSReceiptLogo;
  END

GO

if( COL_LENGTH('dbo.tCompany', 'IsRemoveBoldText') ) = 0
  begin
      exec _pAddModelProperty
        'tCompany',
        'IsRemoveBoldText',
        4;

      ALTER TABLE tCompany
        ADD DEFAULT(0) FOR IsRemoveBoldText;
  END

GO

if( COL_LENGTH('dbo.tCompany', 'SOAPPlanSMSMessage') ) = 0
  begin
      exec _pAddModelProperty
        'tCompany',
        'SOAPPlanSMSMessage',
        1;

      ALTER TABLE tCompany
        ALTER COLUMN SOAPPlanSMSMessage VARCHAR(700);
  END

GO

if( COL_LENGTH('dbo.tPatient_SOAP', 'DateDone') ) = 0
  begin
      exec _pAddModelProperty
        'tPatient_SOAP',
        'DateDone',
        5;
  END

GO

if( COL_LENGTH('dbo.tPatient_SOAP', 'ID_DoneBy') ) = 0
  begin
      exec _pAddModelProperty
        'tPatient_SOAP',
        'ID_DoneBy',
       2;
  END

GO

exec _pRefreshAllViews

GO

CREATE or
ALTER FUNCTION dbo.fGetSOAPLANMessage (@CompanyName        Varchar(MAX),
                                       @SOAPPlanSMSMessage VARCHAR(MAX),
                                       @Client             Varchar(MAX),
                                       @ContactNumber      Varchar(MAX),
                                       @Pet                Varchar(MAX),
                                       @Service            Varchar(MAX),
                                       @DateReturn         DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')
      Declare @message Varchar(MAX) = @SOAPPlanSMSMessage

      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Pet)))
      SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
      SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))

      RETURN @message
  END

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         ''                                                                                    FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client                                                               Paticular,
         ISNULL(patientSOAP.Name_SOAPType, '')
         + ' - '
         + ISNULL(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         FORMAT(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         FORMAT(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         FORMAT(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         FORMAT(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         ISNULL(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client                                                    Paticular,
         ISNULL(patientAppnt.Name_Patient, '')
         + ' - '
         + ISNULL(patientAppnt.Name_SOAPType, '')                                    Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )

GO

CREATE OR
ALTER VIEW vSMSList_Patient_SOAP_Plan
AS
  SELECT soapPlan.ID,
         soapPlan.ID_Patient_SOAP,
         soap.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), Name_Patient, ISNULL(soapPlan.Name_Item, ''), soapPlan.DateReturn) Message,
         DATEADD(DAY, -1, DateReturn)                                                                                                                                          DateSending,
         soap.ID_Company,
         ISNULL(soapPlan.IsSentSMS, 0)                                                                                                                                         IsSentSMS
  FROM   vPatient_SOAP_Plan soapPlan
         INNER JOIN vPatient_SOAP soap
                 on soapPlan.ID_Patient_SOAP = soap.ID
         INNER JOIN tClient c
                 on c.ID = soap.ID_Client
         INNER JOIN tCOmpany comp
                 on comp.ID = soap.ID_Company

GO

CREATE OR
ALTER VIEW [dbo].vzAcknowledgementReport
AS
  SELECT patient.ID,
         patient.Name                               Name_Patient,
         ISNULL(Species, '')                        Name_Species,
         ISNULL(Name_Gender, '')                    Name_Gender,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A') Age_Patient,
         ISNULL(client.Name, '')                    Name_Client,
         ISNULL(client.Address, '')                 Address_Client,
         ISNULL(client.ContactNumber, '')           ContactNumber_Client,
         ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
         ISNULL(client.Email, '')                   Email_Client,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company

GO

CREATE OR
ALTER VIEW [dbo].vzAdmissionReport
AS
  SELECT patient.ID,
         patient.Name                               Name_Patient,
         ISNULL(Species, '')                        Name_Species,
         ISNULL(Name_Gender, '')                    Name_Gender,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A') Age_Patient,
         ISNULL(client.Name, '')                    Name_Client,
         ISNULL(client.Address, '')                 Address_Client,
         ISNULL(client.ContactNumber, '')           ContactNumber_Client,
         ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
         ISNULL(client.Email, '')                   Email_Client,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company

GO

CREATE OR
ALTER VIEW [dbo].vzAgreementForConfinement
AS
  SELECT patient.ID,
         patient.Name                               Name_Patient,
         ISNULL(Species, '')                        Name_Species,
         ISNULL(Name_Gender, '')                    Name_Gender,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A') Age_Patient,
         ISNULL(client.Name, '')                    Name_Client,
         ISNULL(client.Address, '')                 Address_Client,
         ISNULL(client.ContactNumber, '')           ContactNumber_Client,
         ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
         ISNULL(client.Email, '')                   Email_Client,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company

GO

CREATE OR
ALTER VIEW [dbo].vzConcentToOperation
AS
  SELECT patient.ID,
         patient.Name                               Name_Patient,
         ISNULL(Species, '')                        Name_Species,
         ISNULL(Name_Gender, '')                    Name_Gender,
         dbo.fGetAge(patient.DateBirth, case
                                          WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A') Age_Patient,
         ISNULL(client.Name, '')                    Name_Client,
         ISNULL(client.Address, '')                 Address_Client,
         ISNULL(client.ContactNumber, '')           ContactNumber_Client,
         ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
         ISNULL(client.Email, '')                   Email_Client,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company

GO

CREATE OR
ALTER VIEW [dbo].vzEuthanasiaAuthorization
AS
  SELECT TOP 60 patient.ID,
                patient.Name                               Name_Patient,
                ISNULL(Species, '')                        Name_Species,
                ISNULL(Name_Gender, '')                    Name_Gender,
                dbo.fGetAge(patient.DateBirth, case
                                                 WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                                 ELSE NULL
                                               END, 'N/A') Age_Patient,
                ISNULL(client.Name, '')                    Name_Client,
                ISNULL(client.Address, '')                 Address_Client,
                ISNULL(client.ContactNumber, '')           ContactNumber_Client,
                ISNULL(client.ContactNumber2, '')          ContactNumber2_Client,
                ISNULL(client.Email, '')                   Email_Client,
                company.ID                                 ID_Company,
                company.ImageLogoLocationFilenamePath,
                company.Name                               Name_Company,
                company.Address                            Address_Company,
                company.ContactNumber                      ContactNumber_Company,
                CASE
                  WHEN LEN(company.Address) > 0 THEN '' + company.Address
                  ELSE ''
                END
                + CASE
                    WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
                    ELSE ''
                  END
                + CASE
                    WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
                    ELSE ''
                  END                                      HeaderInfo_Company
  FROM   vPatient patient
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = patient.ID_Company

GO

CREATE  OR
ALTER VIEW vzSalesIncomentReport
AS
  SELECT ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                   Name_Company,
         company.Address                                                Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                          HeaderInfo_Company,
         ID_Client,
         LTRIM(RTRIM(Name_Client))                                      Name_Client,
         ID_Item,
         LTRIM(RTRIM(Name_Item))                                        Name_Item,
         Convert(Date, biHed.Date)                                      Date_BillingInvoice,
         SUM(Quantity)                                                  TotalQuantity,
         ISNULL(UnitCost, 0)                                            UnitCost,
         ISNULL(UnitPrice, 0)                                           UnitPrice,
         ( ISNULL(UnitPrice, 0) - ISNULL(UnitCost, 0) ) * SUM(Quantity) NetCost,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')               AttendingPhysician_ID_Employee,
         ISNULL(biHed.Name, '(No Assigned)')                            AttendingPhysician_Name_Employee
  FROM   vBillingInvoice biHed
         INNER JOIN vCompany company
                 ON company.ID = biHed.ID_Company
         Inner JOIN vBillingInvoice_Detail biDetail
                 ON biHed.ID = biDetail.ID_BillingInvoice
  WHERE  biHed.ID_FilingStatus IN ( 3 )
  GROUP  BY ID_Company,
            company.ImageLogoLocationFilenamePath,
            company.Name,
            company.Address,
            CASE
              WHEN LEN(company.Address) > 0 THEN '' + company.Address
              ELSE ''
            END
            + CASE
                WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
                ELSE ''
              END
            + CASE
                WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
                ELSE ''
              END,
            Convert(Date, biHed.Date),
            ID_Client,
            Name_Client,
            ID_Item,
            Name_Item,
            UnitCost,
            UnitPrice,
            ISNULL(biHed.AttendingPhysician_ID_Employee, ''),
            ISNULL(biHed.Name, '(No Assigned)')

GO

CREATE OR
ALTER VIEW [dbo].[vzBillingInvoicePaidListReport]
as
  SELECT biHed.ID                                         ID_BillingInvoice,
         biHed.Code                                       Code_BillingInvoice,
         biHed.Date                                       Date_BillingInvoice,
         piHed.Date                                       Date_PaymentTransaction,
         biHed.Name_Client,
         bihed.Name_Patient,
         biHed.Payment_ID_FilingStatus,
         biHed.Payment_Name_FilingStatus,
         biHed.ID_Company,
         ISNULL(biHed.TotalAmount, 0)                     TotalAmount,
         CASE
           WHEN ISNULL(piHed.ChangeAmount, 0) > 0 THEN piHed.PayableAmount
           ELSE piHed.PaymentAmount
         END                                              PaymentAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                     Name_Company,
         company.Address                                  Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                            HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '') AttendingPhysician_ID_Employee,
         ISNULL(biHed.Name, '(No Assigned)')              AttendingPhysician_Name_Employee
  FROM   vBillingInvoice biHed
         LEFT JOIN tPaymentTransaction piHed
                on piHed.ID_BillingInvoice = biHed.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.Payment_ID_FilingStatus IN ( 11, 12 )
         AND biHed.ID_FilingStatus IN ( 3 )
         AND piHed.ID_FilingStatus IN ( 3 )
  GROUP  BY biHed.ID,
            biHed.Code,
            biHed.Date,
            piHed.Date,
            piHed.ID_FilingStatus,
            biHed.Name_Client,
            bihed.Name_Patient,
            biHed.Payment_ID_FilingStatus,
            biHed.Payment_Name_FilingStatus,
            biHed.ID_Company,
            biHed.TotalAmount,
            piHed.ChangeAmount,
            piHed.PayableAmount,
            piHed.PaymentAmount,
            company.ImageLogoLocationFilenamePath,
            company.Name,
            company.Address,
            company.ContactNumber,
            company.Email,
            ISNULL(biHed.AttendingPhysician_ID_Employee, ''),
            ISNULL(biHed.Name, '(No Assigned)')
  UNION ALL
  SELECT biHed.ID                                         ID_BillingInvoice,
         biHed.Code                                       Code_BillingInvoice,
         biHed.Date                                       Date_BillingInvoice,
         NULL                                             Date_PaymentTransaction,
         biHed.Name_Client,
         bihed.Name_Patient,
         biHed.Payment_ID_FilingStatus,
         biHed.Payment_Name_FilingStatus,
         biHed.ID_Company,
         ISNULL(biHed.TotalAmount, 0)                     TotalAmount,
         0.00                                             PaymentAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                     Name_Company,
         company.Address                                  Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                            HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '') AttendingPhysician_ID_Employee,
         ISNULL(biHed.Name, '(No Assigned)')              AttendingPhysician_Name_Employee
  FROM   vBillingInvoice biHed
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.Payment_ID_FilingStatus IN ( 2 )
         AND biHed.ID_FilingStatus IN ( 3 )
  GROUP  BY biHed.ID,
            biHed.Code,
            biHed.Date,
            biHed.Name_Client,
            bihed.Name_Patient,
            biHed.Payment_ID_FilingStatus,
            biHed.Payment_Name_FilingStatus,
            biHed.ID_Company,
            biHed.TotalAmount,
            company.ImageLogoLocationFilenamePath,
            company.Name,
            company.Address,
            company.ContactNumber,
            company.Email,
            ISNULL(biHed.AttendingPhysician_ID_Employee, ''),
            ISNULL(biHed.Name, '(No Assigned)')

GO

CREATE OR
ALTER VIEW vzBillingInvoiceAgingReport
AS
  SELECT biHed.ID,
         biHed.Date,
         biHed.Date                                                  Date_BillingInvoice,
         biHed.Code,
         biHed.ID_Client,
         LTRIM(RTRIM(biHed.Name_Client))                             Name_Client,
         biHed.TotalAmount,
         biHed.RemainingAmount,
         biHed.Status,
         REPLACE(dbo.fGetAge(biHed.Date, GetDate(), ''), ' old', '') Age,
         DATEDIFF(Day, biHed.Date, GetDate())                        AgeDays,
         company.ID                                                  ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                Name_Company,
         company.Address                                             Address_Company,
         company.ContactNumber                                       ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                       HeaderInfo_Company
  FROM   vBillingInvoice biHed
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.ID_FilingStatus IN ( 3 )
         and biHed.Payment_ID_FilingStatus IN ( 2, 11 )

GO

CREATE  OR
ALTER VIEW [dbo].[vzPaymentTransactionSummaryReport]
as
  SELECT biHed.ID                                            ID_BillingInvoice,
         pHed.ID                                             ID_PaymentTransaction,
         biHed.Code                                          Code_BillingInvoice,
         biHed.Date                                          Date_BillingInvoice,
         biHed.TotalAmount                                   BalanceAmount,
         pHed.Code                                           Code_PaymentTransaction,
         pHed.Date                                           Date_PaymentTransaction,
         client.Name                                         Name_Client,
         patient.Name                                        Name_Patient,
         biHed.ID_Company,
         pHed.ID_PaymentMethod,
         payMethod.Name                                      Name_PaymentMethod,
         phed.PayableAmount,
         CASE
           WHEN ISNULL(ChangeAmount, 0) > 0 THEN phed.PayableAmount
           ELSE phed.PaymentAmount
         END                                                 PaymentAmount,
         pHed.RemainingAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                        Name_Company,
         company.Address                                     Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                               HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')    AttendingPhysician_ID_Employee,
         ISNULL(attendingVeterinarian.Name, '(No Assigned)') AttendingPhysician_Name_Employee,
         attendingVeterinarian.Name_Position
  FROM   tPaymentTransaction pHed
         INNER JOIN tBillingInvoice biHed
                 on biHed.ID = pHed.ID_BillingInvoice
         LEFT JOIN tClient client
                ON client.ID = biHed.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = biHed.ID_Patient
         LEFT JOIN tPaymentMethod payMethod
                ON payMethod.ID = pHed.ID_PaymentMethod
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
         LEFT JOIN dbo.vEmployee attendingVeterinarian
                ON attendingVeterinarian.ID = biHed.[AttendingPhysician_ID_Employee]
  WHERE  biHed.Payment_ID_FilingStatus IN ( 11, 12 )
         AND pHed.ID_FilingStatus IN ( 3 )

GO

ALTER PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout] (@ID_PaymentTransaction INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      Declare @ID_Company int = 0
      Declare @ID_BillingInvoice int = 0
      Declare @Name_Company VARCHAR(MAX)= ''
      Declare @Address_Company VARCHAR(MAX)= ''
      Declare @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      Declare @ImageLogo_Company VARCHAR(MAX)= ''
      Declare @ContactNumber_Company VARCHAR(MAX)= ''
      Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
      Declare @ID_PaymentMode int = 0
      Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
      Declare @CheckNumber VARCHAR(MAX)= ''
      Declare @CardNumber VARCHAR(MAX)= ''
      Declare @Name_CardType VARCHAR(MAX)= ''
      Declare @CardHolderName VARCHAR(MAX)= ''
      Declare @Name_PaymentStatus VARCHAR(MAX)= ''
      Declare @CashAmount DECIMAL(18, 4) = 0.00
      Declare @GCashAmount DECIMAL(18, 4) = 0.00
      Declare @CardAmount DECIMAL(18, 4) = 0.00
      Declare @CheckAmount DECIMAL(18, 4) = 0.00
      Declare @PayableAmount DECIMAL(18, 4) = 0.00
      Declare @PaymentAmount DECIMAL(18, 4) = 0.00
      Declare @ChangeAmount DECIMAL(18, 4) = 0.00
      Declare @RemainingAmount DECIMAL(18, 4) = 0.00
      Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      Declare @Name_Client VARCHAR(MAX)= ''
      Declare @Name_Patient VARCHAR(MAX)= ''
      Declare @Date_BillingInvoice datetime
      Declare @Code_BillingInvoice VARCHAR(MAX)= ''
      Declare @IsShowPOSReceiptLogo BIT = 1
      Declare @IsRemoveBoldText BIT = 0

      SELECT @ID_Company = ID_Company,
             @ID_BillingInvoice = ID_BillingInvoice,
             @ID_PaymentMode = ID_PaymentMethod,
             @Code_PaymentTranaction = Code,
             @PayableAmount = PayableAmount,
             @RemainingAmount = RemainingAmount,
             @ChangeAmount = ChangeAmount,
             @CashAmount = CashAmount,
             @ReferenceTransactionNumber = ReferenceTransactionNumber,
             @GCashAmount = GCashAmount,
             @Name_CardType = Name_CardType,
             @CardNumber = CardNumber,
             @CardHolderName = CardHolderName,
             @CardAmount = CardAmount,
             @CheckNumber = CheckNumber,
             @CheckAmount = CheckAmount,
             @PaymentAmount = PaymentAmount
      FROM   vPaymentTransaction
      WHERE  ID = @ID_PaymentTransaction

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0)
      FROM   vCompany
      WHERE  ID = @ID_Company

      SELECT @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.Name_Patient, 'N/A'),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      SELECT @billingItemsLayout = @billingItemsLayout
                                   + '    
   <div class="display-block clearfix">    
    <span class="bold">'
                                   + biDetail.Name_Item
                                   + '</span><br/>    
    <span class="float-left">    
     &nbsp;&nbsp;&nbsp;'
                                   + FORMAT(biDetail.Quantity, '#,#0.##')
                                   + '    
    </span>    
    <span class="float-right">'
                                   + FORMAT(biDetail.Amount, '#,#0.00')
                                   + '</span>    
   </div>'
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      if @ID_PaymentMode = @Cash_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Cash Amount    
    </span>    
    <span class="float-right">'
                                 + FORMAT(@CashAmount, '#,#0.00') + '</span>    
   </div>'
        END
      else if @ID_PaymentMode = @GCash_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Ref. No.    
    </span>    
    <span class="float-right">'
                                 + @ReferenceTransactionNumber + '</span>    
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     G-Cash    
    </span>    
    <span class="float-right">'
                                 + FORMAT(@GCashAmount, '#,#0.00') + '</span>    
   </div>'
        END
      ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Card Type    
    </span>    
    <span class="float-right">'
                                 + @Name_CardType + '</span>    
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Card #    
    </span>    
    <span class="float-right">'
                                 + @CardNumber + '</span>    
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Holder    
    </span>    
    <span class="float-right">'
                                 + @CardHolderName + '</span>    
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Card Amt.    
    </span>    
    <span class="float-right">'
                                 + FORMAT(@CardAmount, '#,#0.00') + '</span>    
   </div>'
        END
      ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Check No.    
    </span>    
    <span class="float-right">'
                                 + @CheckNumber + '</span>    
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Check Amt.    
    </span>    
    <span class="float-right">'
                                 + FORMAT(@CheckAmount, '#,#0.00') + '</span>    
   </div>'
        END

      DECLARE @title VARCHAR(MAX)= ''
      DECLARE @style VARCHAR(MAX)= '    
  body{    
   margin: 0px;    
   padding: 0px;    
   font-family:  arial, sans-serif;    
   font-weight: normal;    
   font-style: normal;    
   font-size: 13px;    
  }    
    
  .logo{    
    
   width: 120px;    
   margin-bottom: 10px;    
  }    
    
  .receipt-container{    
     width: 100vw;
	 position: relative;
	 left: 50%;
	 right: 50%;
	 margin-left: -50vw;
	 margin-right: -50vw;
  }    
    
  .company-logo{    
   display: '
        + CASE
            WHEN LEN(LTRIM(RTRIM(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';    
   text-align: center;    
   word-wrap: break-word;    
   margin-bottom: 12px;    
  }    
    
  .company-name{    
   display: block;    
   text-align: center;    
   word-wrap: break-word;    
  }    
    
  .company-address{    
   display: block;    
   text-align: center;    
   word-wrap: break-word;    
   font-size: 11px    
  }    
    
  .company-contactnum-container{    
   display: block;    
   text-align: center;    
   word-wrap: break-word;    
   font-size: 11px    
  }    
    
  .company-contactnum{    
   text-align: center;    
   word-wrap: break-word;    
    
  }    
    
  .float-left{    
   float: left;    
  }    
    
  .float-right{    
   float: right;    
  }    
    
  .clearfix {    
   overflow: auto;    
  }    
    
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + ' 
    
  .display-block{    
	 word-wrap: break-word;
	display: block;    
  }    
    
  .title{    
   text-align: center;    
   word-wrap: break-word;    
   display: block;    
   padding-top: 15px;    
   padding-bottom: 15px;    
  }    
 '
      DECLARE @content VARCHAR(MAX)= '    
  <div class="receipt-container">    
  '
        + CASE
            WHEN @IsShowPOSReceiptLogo = 1 THEN '<div class="company-logo bold"><img class="logo" src="'
                                                + @ImageLogoLocationFilenamePath_Company
                                                + '" alt="" srcset=""></div>'
            ELSE ''
          END
        + '    
   <div class="company-name bold">'
        + @Name_Company + ''
        + '</div>    
   <div class="company-address">'
        + @Address_Company + ''
        + '</div>    
   <div class="company-contactnum-container bold" style="'
        + CASE
            WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none'
            ELSE ''
          END
        + '">    
    Contact No: <span class="company-contactnum">'
        + @ContactNumber_Company
        + '</span>    
   </div>    
   <div class="title bold">INVOICE</div>    
   <div class="display-block"><span class="bold">Bill To: </span>'
        + @Name_Client
        + '</div>    
   <div class="display-block"><span class="bold">Pet Name: </span>'
        + @Name_Patient
        + '</div>    
   <div class="display-block">    
    <span class="bold">Invoice Date: </span>'
        + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy')
        + '    
   </div>     
   <div class="display-block"><span class="bold">BI #: </span>'
        + @Code_BillingInvoice + '</div>     
   <br/>    
   '
        + @billingItemsLayout
        + '    
    
   <div class="title bold">TOTAL</div>    
       
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Subtotal    
    </span>    
    <span class="float-right">'
        + FORMAT(@SubTotal_BillingInvoice, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Disc. Rate    
    </span>    
    <span class="float-right">'
        + FORMAT(@DiscountRate_BillingInvoice, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Disc. Amount    
    </span>    
    <span class="float-right">'
        + FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Total Amount    
    </span>    
    <span class="float-right">'
        + FORMAT(@TotalAmount_BillingInvoice, '#,#0.00')
        + '</span>    
   </div>    
    
   <div class="title bold">PAYMENT</div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     PT #    
    </span>    
    <span class="float-right">'
        + @Code_PaymentTranaction + '</span>    
   </div>    
   '
        + @paymentLayout
        + '    
   <br/>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Balance    
    </span>    
    <span class="float-right">'
        + FORMAT(@PayableAmount, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Payment    
    </span>    
    <span class="float-right">'
        + FORMAT(@PaymentAmount, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Remaining    
    </span>    
    <span class="float-right">'
        + FORMAT(@RemainingAmount, '#,#0.00')
        + '</span>    
   </div>    
   <div class="display-block clearfix">    
    <span class="float-left bold">    
     Change    
    </span>    
    <span class="float-right">'
        + FORMAT(@ChangeAmount, '#,#0.00')
        + '</span>    
   </div>    
    
   <div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>    
    
   <div class="title"> Print Date '
        + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy hh:mm:ss tt')
        + '</div>    
  </div>    
 '

      SELECT '_'

      SELECT @Code_PaymentTranaction title,
             @style                  style,
             @content                content,
             @isAutoPrint            isAutoPrint,
             @ID_PaymentTransaction  ID_PaymentTransaction,
             @ID_BillingInvoice      ID_BillingInvoice,
             @millisecondDelay       millisecondDelay
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetSendSOAPPlan](@Date      DateTime,
                                    @IsSMSSent Bit = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETime,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETime,
           ID_Patient_SOAP_Plan INT
        )

      INSERT @record
      SELECT c.ID                                                                                                                                                  ID_Company,
             c.Name                                                                                                                                                Name_Company,
             client.Name                                                                                                                                           Name_Client,
             ISNULL(client.ContactNumber, '0')                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                       Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                  DateSending,
             soapPlan.ID                                                                                                                                           ID_Patient_SOAP_Plan
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND ( ( CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date) )
                    OR ( CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ) )
             AND patientSOAP.ID_Company IN (SELECT ID_Company
                                            FROM   tCompany_SMSSetting
                                            WHERE  ISNULL(IsActive, 0) = 1)
      ORDER  BY c.Name

      SELECT @Success Success;

      SELECT FORMAT(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             SUM(CASE
                   WHEN LEN(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN LEN(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN LEN(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          ConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateSending, 'yyyy-MM-dd'),
                Name_Company
      Order  BY DateSending DESC,
                Name_Company
  END

SELECT *
FROM   @record
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,
          DateReturn ASC,
          Name_Company

GO

ALTER PROCEDURE [dbo].[pGetPatient_SOAP] @ID          INT = -1,
                                         @ID_Client   INT = NULL,
                                         @ID_Patient  INT = NULL,
                                         @ID_SOAPType INT = NULL,
                                         @ID_Session  INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription;

      DECLARE @ID_User INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + CHAR(9) + CHAR(13) + '  Wbc= ' + CHAR(9)
        + CHAR(13) + '  Lym= ' + CHAR(9) + CHAR(13)
        + '  Mon= ' + CHAR(9) + CHAR(13) + '  Neu= ' + CHAR(9)
        + CHAR(13) + '  Eos= ' + CHAR(9) + CHAR(13)
        + '  Bas= ' + CHAR(9) + CHAR(13) + '  Rbc= ' + CHAR(9)
        + CHAR(13) + '  Hgb= ' + CHAR(9) + CHAR(13)
        + '  Hct= ' + CHAR(9) + CHAR(13) + '  Mcv= ' + CHAR(9)
        + CHAR(13) + '  Mch= ' + CHAR(9) + CHAR(13)
        + '  Mchc ' + CHAR(9) + CHAR(13) + '  Plt= ' + CHAR(9)
        + CHAR(13) + '  Mpv= ' + CHAR(9) + CHAR(13) + CHAR(9)
        + CHAR(13) + 'Blood Chem: ' + CHAR(9) + CHAR(13)
        + '  Alt= ' + CHAR(9) + CHAR(13) + '  Alp= ' + CHAR(9)
        + CHAR(13) + '  Alb= ' + CHAR(9) + CHAR(13)
        + '  Amy= ' + CHAR(9) + CHAR(13) + '  Tbil= '
        + CHAR(9) + CHAR(13) + '  Bun= ' + CHAR(9) + CHAR(13)
        + '  Crea= ' + CHAR(9) + CHAR(13) + '  Ca= ' + CHAR(9)
        + CHAR(13) + '  Phos= ' + CHAR(9) + CHAR(13)
        + '  Glu= ' + CHAR(9) + CHAR(13) + '  Na= ' + CHAR(9)
        + CHAR(13) + '  K= ' + CHAR(9) + CHAR(13) + '  TP= '
        + CHAR(9) + CHAR(13) + '  Glob= ' + CHAR(9) + CHAR(13)
        + CHAR(9) + CHAR(13) + 'Microscopic Exam: '
        + CHAR(9) + CHAR(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

	  IF ISNULL(@ID_Patient, 0) <> 0
	  BEGIN
	  
		  SELECT @IsDeceased = Isnull(IsDeceased, 0), @ID_Client = ID_Client
		  FROM   tPatient
		  WHERE  ID = @ID_Patient;
	  END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT c.ImageRowIndex,
             ROW_NUMBER()
               OVER (
                 ORDER BY ImageNo) AS RowIndex,
             c.ImageNo,
             FilePath,
             c.Remark
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
      WHERE  ps.ID = @ID
             AND ( ISNULL(FilePath, '') <> ''
                    OR LEN(ISNULL(Remark, '')) > 0 )
      ORDER  BY c.ImageRowIndex ASC;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   patient.Name  Name_Patient,
                   patient.Name_Client,
                   fs.Name       Name_FilingStatus,
                   soapType.Name Name_SOAPType
            FROM   (SELECT NULL                         AS [_],
                           -1                           AS [ID],
                           '-New-'                      AS [Code],
                           NULL                         AS [Name],
                           1                            AS [IsActive],
                           NULL                         AS [ID_Company],
                           NULL                         AS [Comment],
                           NULL                         AS [DateCreated],
                           NULL                         AS [DateModified],
                           @ID_User                     AS [ID_CreatedBy],
                           NULL                         AS [ID_LastModifiedBy],
                           @ID_Client                   AS ID_Client,
                           @ID_Patient                  AS ID_Patient,
                           GETDATE()                    Date,
                           @FILED_ID_FilingStatus       ID_FilingStatus,
                           @IsDeceased                  IsDeceased,
                           @ID_SOAPType                 ID_SOAPType,
                           @ObjectiveTemplate           ObjectiveTemplate,
                           @AssessmentTemplate          AssessmentTemplate,
                           @LaboratoryTemplate          LaboratoryTemplate,
                           @ClinicalExaminationTemplate ClinicalExaminationTemplate,
                           @DiagnosisTemplate           DiagnosisTemplate) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   patient.IsDeceased,
                   @ObjectiveTemplate           ObjectiveTemplate,
                   @AssessmentTemplate          AssessmentTemplate,
                   @LaboratoryTemplate          LaboratoryTemplate,
                   @ClinicalExaminationTemplate ClinicalExaminationTemplate,
                   @DiagnosisTemplate           DiagnosisTemplate
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          on h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID
  END;

GO

CREATE OR
ALTER PROC dbo.pDonePatient_SOAP_validation (@IDs_Patient_SOAP typIntList READONLY,
                                             @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotFiled TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotFiled INT = 0;

      /* Validate Patient SOAP Status is not Filed*/
      INSERT @ValidateNotFiled
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_SOAP bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_SOAP ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus );

      SELECT @Count_ValidateNotFiled = COUNT(*)
      FROM   @ValidateNotFiled;

      IF ( @Count_ValidateNotFiled > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotFiled > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to Doned:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotFiled;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pDonePatient_SOAP] (@IDs_Patient_SOAP typIntList READONLY,
                                      @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Doned_ID_FilingStatus INT = 13;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pDonePatient_SOAP_validation
            @IDs_Patient_SOAP,
            @ID_UserSession;

          INSERT @IDs_Patient
          SELECT patient.ID
          FROM   tPatient_SOAP soap
                 INNER JOIN tPatient patient
                         ON patient.ID = soap.ID_Patient
                 INNER JOIN @IDs_Patient_SOAP idsSOAP
                         ON idsSOAP.ID = soap.ID

          EXEC dbo.pUpdatePatientsLastVisitedDate
            @IDs_Patient

          UPDATE dbo.tPatient_SOAP
          SET    ID_FilingStatus = @Doned_ID_FilingStatus,
                 DateDone = GETDATE(),
                 ID_DoneBy = @ID_User
          FROM   dbo.tPatient_SOAP bi
                 INNER JOIN @IDs_Patient_SOAP ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO


Declare @template VARCHAR(MAX) = 'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.  
Pls. contact /*CompanyName*/ /*ContactNumber*/'

Update tCompany
SET    SOAPPlanSMSMessage = @template
WHERE  ISNULL(SOAPPlanSMSMessage, '') = ''

Update tCompany
SET    IsRemoveBoldText = 1
WHERE  ISNULL(IsRemoveBoldText, 0) = 0 
