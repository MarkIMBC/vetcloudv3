
IF COL_LENGTH('dbo.tPurchaseOrder', 'DiscountRate') IS  NULL
BEGIN
   exec _pAddModelProperty  'tPurchaseOrder', 'DiscountRate', 3
END

GO

IF COL_LENGTH('dbo.tPurchaseOrder', 'DiscountAmount') IS  NULL
BEGIN
   exec _pAddModelProperty  'tPurchaseOrder', 'DiscountAmount', 3
END

GO

IF COL_LENGTH('dbo.tPurchaseOrder', 'IsComputeDiscountRate') IS  NULL
BEGIN
   exec _pAddModelProperty  'tPurchaseOrder', 'IsComputeDiscountRate', 4
END

GO

IF COL_LENGTH('dbo.tPurchaseOrder', 'SubTotal') IS  NULL
BEGIN
   exec _pAddModelProperty  'tPurchaseOrder', 'SubTotal', 3
END

GO

IF COL_LENGTH('dbo.tPurchaseOrder', 'TotalAmount') IS  NULL
BEGIN
   exec _pAddModelProperty  'tPurchaseOrder', 'TotalAmount', 3
END

GO

IF COL_LENGTH('dbo.tReceivingReport', 'DiscountRate') IS  NULL
BEGIN
   exec _pAddModelProperty  'tReceivingReport', 'DiscountRate', 3
END

GO

IF COL_LENGTH('dbo.tReceivingReport', 'DiscountAmount') IS  NULL
BEGIN
   exec _pAddModelProperty  'tReceivingReport', 'DiscountAmount', 3
END

GO

IF COL_LENGTH('dbo.tReceivingReport', 'IsComputeDiscountRate') IS  NULL
BEGIN
   exec _pAddModelProperty  'tReceivingReport', 'IsComputeDiscountRate', 4
END

GO

IF COL_LENGTH('dbo.tReceivingReport', 'SubTotal') IS  NULL
BEGIN
   exec _pAddModelProperty  'tReceivingReport', 'SubTotal', 3
END

GO

IF COL_LENGTH('dbo.tReceivingReport', 'TotalAmount') IS  NULL
BEGIN
   exec _pAddModelProperty  'tReceivingReport', 'TotalAmount', 3
END

GO

IF COL_LENGTH('dbo.tCompany', 'ImageHeaderFilename') IS  NULL
BEGIN
   exec _pAddModelProperty  'tCompany', 'ImageHeaderFilename', 1
END

GO


IF COL_LENGTH('dbo.tClient', 'tempID') IS  NULL
BEGIN
   exec _pAddModelProperty  'tClient', 'tempID', 1
END

GO

exec _pRefreshAllViews

GO

ALTER VIEW [dbo].[vCompany]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       'https://api.veterinarycloudsystem.com/Content/Image/' + H.ImageHeaderFilename ImageHeaderLocationFilenamePath,
	   'https://api.veterinarycloudsystem.com/Content/Thumbnail/' + H.ImageHeaderFilename ImageHeaderThumbNameLocationFilenamePath,
	   'https://api.veterinarycloudsystem.com/Content/Image/' + H.ImageLogoFilename ImageLogoLocationFilenamePath,
	   'https://api.veterinarycloudsystem.com/Content/Thumbnail/' + H.ImageLogoFilename ImageLogoThumbNameLocationFilenamePath
FROM tCompany H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID;

GO

ALTER PROCEDURE [dbo].[_pCreateReportView] @ReportName   VARCHAR(300), 
                                           @IsNavigation BIT          = 1
AS
     DECLARE @Oid_Report UNIQUEIDENTIFIER= NEWID();

     INSERT INTO dbo._tReport
     (Oid, 
      Code, 
      Name, 
      IsActive, 
      Comment, 
      DateCreated, 
      DateModified, 
      ID_CreatedBy, 
      ID_LastModifiedBy, 
      ReportPath
     )
     VALUES
     (@Oid_Report, -- Oid - uniqueidentifier
      NULL, -- Code - varchar
      @ReportName, -- Name - varchar
      1, -- IsActive - bit
      NULL, -- Comment - varchar
      GETDATE(), -- DateCreated - datetime
      GETDATE(), -- DateModified - datetime
      1, -- ID_CreatedBy - int
      NULL, -- ID_LastModifiedBy - int
      @ReportName	 -- ReportPath - varchar
     );

     DECLARE @Oid_View UNIQUEIDENTIFIER= NEWID();

     INSERT INTO dbo._tView
     (Oid, 
      Code, 
      Name, 
      IsActive, 
      ID_ListView, 
      ID_Report, 
      ID_Dashboard, 
      ID_ViewType, 
      CustomViewPath, 
      ControllerPath, 
      Comment, 
      DateCreated, 
      ID_CreatedBy, 
      ID_LastModifiedBy, 
      DateModified, 
      ID_Model, 
      DataSource
     )
     VALUES
     (@Oid_View, -- Oid - uniqueidentifier
      NULL, -- Code - varchar
      @ReportName + '_ReportView', -- Name - varchar
      1, -- IsActive - bit
      NULL, -- ID_ListView - uniqueidentifier
      @Oid_Report, -- ID_Report - uniqueidentifier
      NULL, -- ID_Dashboard - uniqueidentifier
      3, -- ID_ViewType - int
      NULL, -- CustomViewPath - varchar
      NULL, -- ControllerPath - varchar
      NULL, -- Comment - varchar
      GETDATE(), -- DateCreated - datetime
      1, -- ID_CreatedBy - int
      NULL, -- ID_LastModifiedBy - int
      GETDATE(), -- DateModified - datetime
      NULL, -- ID_Model - uniqueidentifier
      NULL -- DataSource - varchar
     );

     IF(@IsNavigation = 1)
         BEGIN

             INSERT INTO dbo._tNavigation
             (Oid, 
              Code, 
              Name, 
              IsActive, 
              Comment, 
              ID_CreatedBy, 
              ID_LastModifiedBy, 
              DateCreated, 
              DateModified, 
              ID_View, 
              Caption, 
              Icon, 
              SeqNo, 
              ID_Parent
             )
             VALUES
             (NEWID(), -- Oid - uniqueidentifier
              NULL, -- Code - varchar
              @ReportName + '_Navigation', -- Name - varchar
              1, -- IsActive - bit
              NULL, -- Comment - varchar
              1, -- ID_CreatedBy - int
              NULL, -- ID_LastModifiedBy - int
              GETDATE(), -- DateCreated - datetime
              GETDATE(), -- DateModified - datetime
              @Oid_View, -- ID_View - uniqueidentifier
              @ReportName, -- Caption - varchar
              NULL, -- Icon - varchar
              NULL, -- SeqNo - int
              '4E94C28D-7955-4726-8676-65B06A142876' -- ID_Parent - uniqueidentifier
             );
     END;
GO

CREATE OR ALTER PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout]
(
    @ID_PaymentTransaction INT
)
AS
BEGIN

	DECLARE @isAutoPrint BIT = 1

	DECLARE @Cash_ID_PaymentMethod INT = 1
	DECLARE @Check_ID_PaymentMethod INT = 2
	DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
	DECLARE @GCash_ID_PaymentMethod INT = 3


	DECLARE @billingItemsLayout VARCHAR(MAX)= ''
	DECLARE @paymentLayout VARCHAR(MAX)= ''


	Declare @ID_Company int	= 0
	Declare @ID_BillingInvoice int	= 0
	Declare @Name_Company VARCHAR(MAX)= ''
	Declare @Address_Company VARCHAR(MAX)= ''
	Declare @ContactNumber_Company VARCHAR(MAX)= ''
	Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
	Declare @ID_PaymentMode int	= 0

	Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
	Declare @CheckNumber VARCHAR(MAX)= ''
	Declare @CardNumber VARCHAR(MAX)= ''
	Declare @Name_CardType VARCHAR(MAX)= ''
	Declare @CardHolderName VARCHAR(MAX)= ''
	
	Declare @Name_PaymentStatus VARCHAR(MAX)= ''

	Declare @CashAmount DECIMAL(18, 4) = 0.00
	Declare @GCashAmount DECIMAL(18, 4) =  0.00
	Declare @CardAmount DECIMAL(18, 4) =  0.00
	Declare @CheckAmount DECIMAL(18, 4) =  0.00
	
	Declare @PayableAmount DECIMAL(18, 4) =  0.00
	Declare @PaymentAmount DECIMAL(18, 4) =  0.00
	Declare @ChangeAmount DECIMAL(18, 4) =  0.00
	Declare @RemainingAmount DECIMAL(18, 4) =  0.00

	Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
	Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
	Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
	
	Declare @Name_Client VARCHAR(MAX)= ''
	Declare @Name_Patient VARCHAR(MAX)= ''
	Declare @Date_BillingInvoice datetime
	Declare @Code_BillingInvoice VARCHAR(MAX)= ''

	SELECT  @ID_Company = ID_Company,
			@ID_BillingInvoice = ID_BillingInvoice,
			@ID_PaymentMode = ID_PaymentMethod,
			@Code_PaymentTranaction = Code,
			@PayableAmount = PayableAmount,
			@RemainingAmount = RemainingAmount,
			@ChangeAmount = ChangeAmount,
			@CashAmount = CashAmount,
			@ReferenceTransactionNumber = ReferenceTransactionNumber,
			@GCashAmount = GCashAmount,
			@Name_CardType = Name_CardType,
			@CardNumber = CardNumber,
			@CardHolderName = CardHolderName,
			@CardAmount = CardAmount,
			@CheckNumber = CheckNumber,
			@CheckAmount = CheckAmount,
			@PaymentAmount = PaymentAmount
	FROM    vPaymentTransaction 
	WHERE 
		ID = @ID_PaymentTransaction

	SELECT  @Name_Company = ISNULL(Name,'N/A'),
			@Address_Company = ISNULL(Address,''),
			@ContactNumber_Company = ISNULL(ContactNumber,'')
	FROM    tCompany 
	WHERE ID = @ID_Company

	SELECT  @Name_Client = ISNULL(bi.Name_Client,''),
			@Name_Patient = ISNULL(bi.Name_Patient,'N/A'),
			@Date_BillingInvoice = Date,
			@Code_BillingInvoice = Code,
			@SubTotal_BillingInvoice = SubTotal,
			@DiscountRate_BillingInvoice = DiscountRate,
			@DiscountAmount_BillingInvoice = DiscountAmount,
			@TotalAmount_BillingInvoice = TotalAmount, 
			@Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus,'')
	FROM    vBillingInvoice bi
	WHERE
		bi.ID = @ID_BillingInvoice

	SELECT  @billingItemsLayout = @billingItemsLayout + '
			<div class="display-block clearfix">
				<span class="bold">'+  biDetail.Name_Item +'</span><br/>
				<span class="float-left">
					&nbsp;&nbsp;&nbsp;'+  FORMAT(biDetail.Quantity,'#,#0.##') +'
				</span>
				<span class="float-right">'
					+  FORMAT(biDetail.Amount,'#,#0.00') +
				'</span>
			</div>'
	FROM    vBillingInvoice_Detail biDetail
	WHERE 
		biDetail.ID_BillingInvoice = @ID_BillingInvoice

	if @ID_PaymentMode = @Cash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Cash Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@CashAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	else if @ID_PaymentMode = @GCash_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Ref. No.
				</span>
				<span class="float-right">'
					+ @ReferenceTransactionNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					G-Cash
				</span>
				<span class="float-right">'
					+ FORMAT(@GCashAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Type
				</span>
				<span class="float-right">'
					+ @Name_CardType +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card #
				</span>
				<span class="float-right">'
					+ @CardNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Holder
				</span>
				<span class="float-right">'
					+ @CardHolderName +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Card Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CardAmount,'#,#0.00') +
				'</span>
			</div>'
	END

	ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod 
	BEGIN
		
		SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check No.
				</span>
				<span class="float-right">'
					+ @CheckNumber +
				'</span>
			</div>'

			SET @paymentLayout = @paymentLayout + '
			<div class="display-block clearfix">
				<span class="float-left bold">
					Check Amt.
				</span>
				<span class="float-right">'
					+ FORMAT(@CheckAmount,'#,#0.00') +
				'</span>
			</div>'
	END
	DECLARE @title VARCHAR(MAX)= ''
	DECLARE @style VARCHAR(MAX)= '
		body{
			margin: 0px;
			padding: 0px;
			font-family:  arial, sans-serif;
			font-weight: normal;
			font-style: normal;
			font-size: 13px;
		}

		.receipt-container{
			width: 160px;
		}

		.company-name{
			display: block;
			font-weight: bold;
			text-align: center;
			word-wrap: break-word;
		}

		.company-address{
			display: block;
			text-align: center;
			word-wrap: break-word;
		}

		.company-contactnum-container{
			display: block;
			text-align: center;
			word-wrap: break-word;
			font-weight: bold;
		}

		.company-contactnum{
			font-weight: 200;
			text-align: center;
			word-wrap: break-word;
		}

		.float-left{
			float: left;
		}

		.float-right{
			float: right;
		}

		.clearfix {
			overflow: auto;
		}

		.bold{
			font-weight: bold;
		}

		.display-block{

			display: block;
		}

		.title{
			text-align: center;
			word-wrap: break-word;
			display: block;
			padding-top: 15px;
			padding-bottom: 15px;
		}
	'

	DECLARE @content VARCHAR(MAX)= '
		<div class="receipt-container">
			<div class="company-name">'+ @Name_Company+ '' +'</div>
			<div class="company-address">'+ @Address_Company+ '' +'</div>
			<div class="company-contactnum-container" style="'+ CASE WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none' ELSE '' END +'">
				Contact No: <span class="company-contactnum">'+ @ContactNumber_Company +'</span>
			</div>
			<div class="title bold">INVOICE</div>
			<div class="display-block"><span class="bold">Bill To: </span>'+ @Name_Client +'</div>
			<div class="display-block"><span class="bold">Pet Name: </span>'+ @Name_Patient +'</div>
			<div class="display-block">
				<span class="bold">Invoice Date: </span>'+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy') +'
			</div> 
			<div class="display-block"><span class="bold">BI #: </span>'+ @Code_BillingInvoice +'</div> 
			<br/>
			'+ @billingItemsLayout +'

			<div class="title bold">TOTALS</div>
			
			<div class="display-block clearfix">
				<span class="float-left bold">
					Subtotal
				</span>
				<span class="float-right">'
					+  FORMAT(@SubTotal_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Rate
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountRate_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Disc. Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@DiscountAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Total Amount
				</span>
				<span class="float-right">'
					+  FORMAT(@TotalAmount_BillingInvoice,'#,#0.00') +
				'</span>
			</div>

			<div class="title bold">PAYMENT</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					PT #
				</span>
				<span class="float-right">'
					+  @Code_PaymentTranaction +
				'</span>
			</div>
			'+ @paymentLayout +'
			<br/>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Balance
				</span>
				<span class="float-right">'
					+ FORMAT(@PayableAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Payment
				</span>
				<span class="float-right">'
					+ FORMAT(@PaymentAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Remaining
				</span>
				<span class="float-right">'
					+ FORMAT(@RemainingAmount,'#,#0.00') +
				'</span>
			</div>
			<div class="display-block clearfix">
				<span class="float-left bold">
					Change
				</span>
				<span class="float-right">'
					+ FORMAT(@ChangeAmount,'#,#0.00') +
				'</span>
			</div>

			<div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>

			<div class="title"> Print Date '+ FORMAT(@Date_BillingInvoice,'MM/dd/yyyy hh:mm:ss tt') +'</div>
		</div>
	'

    SELECT '_'

	SELECT @Code_PaymentTranaction title, @style style, @content content, @isAutoPrint isAutoPrint, @ID_PaymentTransaction, @ID_BillingInvoice

END

GO

ALTER PROCEDURE [dbo].[pGetPatient_SOAP] @ID INT = -1,
	@ID_Patient INT = NULL,
	@ID_SOAPType INT = NULL,
	@ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'
   ,'' AS LabImages
   ,'' AS Patient_SOAP_Plan;

  DECLARE @ID_User INT;
  DECLARE @ID_Warehouse INT;
  DECLARE @FILED_ID_FilingStatus INT = 1;
  DECLARE @IsDeceased BIT = 1;

  DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): '+ CHAR(9) + CHAR(13) + 
											'Respiratory Rate (brpm): '+ CHAR(9) + CHAR(13) + 	
											'Weight (kg): '+ CHAR(9) + CHAR(13) + 	
											'Length (cm): '+ CHAR(9) + CHAR(13) + 	
											'BCS: '+ CHAR(9) + CHAR(13) + 	
											'Temperature: '+ CHAR(9) + CHAR(13)

  DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Notes: '+ CHAR(9) + CHAR(13) + 		
											'Test Results: '+ CHAR(9) + CHAR(13) + 		
											'Final Diagnosis: '+ CHAR(9) + CHAR(13) + 		
											'Prognosis: '+ CHAR(9) + CHAR(13) + 	
											'Category: '+ CHAR(9) + CHAR(13) 

  DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: '+ CHAR(9) + CHAR(13) + 	
											'  Wbc= '+ CHAR(9) + CHAR(13) + 	
											'  Lym= '+ CHAR(9) + CHAR(13) + 	
											'  Mon= '+ CHAR(9) + CHAR(13) + 	
											'  Neu= '+ CHAR(9) + CHAR(13) + 	
											'  Eos= '+ CHAR(9) + CHAR(13) + 	
											'  Bas= '+ CHAR(9) + CHAR(13) + 	
											'  Rbc= '+ CHAR(9) + CHAR(13) + 	
											'  Hgb= '+ CHAR(9) + CHAR(13) + 	
											'  Hct= '+ CHAR(9) + CHAR(13) + 	
											'  Mcv= '+ CHAR(9) + CHAR(13) + 	
											'  Mch= '+ CHAR(9) + CHAR(13) + 	
											'  Mchc '+ CHAR(9) + CHAR(13) + 
											'  Plt= '+ CHAR(9) + CHAR(13) + 	
											'  Mpv= '+ CHAR(9) + CHAR(13) + CHAR(9) + CHAR(13) +	
											'Blood Chem: '+ CHAR(9) + CHAR(13) + 	
											'  Alt= '+ CHAR(9) + CHAR(13) + 	
											'  Alp= '+ CHAR(9) + CHAR(13) + 	
											'  Alb= '+ CHAR(9) + CHAR(13) + 	
											'  Amy= '+ CHAR(9) + CHAR(13) + 	
											'  Tbil= '+ CHAR(9) + CHAR(13) + 	
											'  Bun= '+ CHAR(9) + CHAR(13) + 	
											'  Crea= '+ CHAR(9) + CHAR(13) + 	
											'  Ca= '+ CHAR(9) + CHAR(13) + 	
											'  Phos= '+ CHAR(9) + CHAR(13) + 	
											'  Glu= '+ CHAR(9) + CHAR(13) + 	
											'  Na= '+ CHAR(9) + CHAR(13) + 	
											'  K= '+ CHAR(9) + CHAR(13) + 	
											'  TP= '+ CHAR(9) + CHAR(13) + 	
											'  Glob= '+ CHAR(9) + CHAR(13) +  CHAR(9) + CHAR(13) + 	 	
											'Microscopic Exam: '+ CHAR(9) + CHAR(13) 

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM dbo.tUserSession
  WHERE ID = @ID_Session;

  SELECT  @IsDeceased = Isnull(IsDeceased,0)
  FROM tPatient WHERE ID = @ID_Patient;
  
  DECLARE @LabImage TABLE (
    ImageRowIndex INT
   ,RowIndex INT
   ,ImageNo VARCHAR(MAX)
   ,FilePath VARCHAR(MAX)
   ,Remark VARCHAR(MAX)
  );

  INSERT @LabImage (ImageRowIndex, RowIndex, ImageNo, FilePath, Remark)
  SELECT
    c.ImageRowIndex
   ,ROW_NUMBER() OVER (ORDER BY ImageNo) AS RowIndex
   ,c.ImageNo
   ,FilePath
   ,c.Remark
  FROM tPatient_SOAP ps
  CROSS APPLY (SELECT
      '01'
     ,LabImageRowIndex01
     ,LabImageFilePath01
     ,LabImageRemark01
    UNION ALL
    SELECT
      '02'
     ,LabImageRowIndex02
     ,LabImageFilePath02
     ,LabImageRemark02
    UNION ALL
    SELECT
      '03'
     ,LabImageRowIndex03
     ,LabImageFilePath03
     ,LabImageRemark03
    UNION ALL
    SELECT
      '04'
     ,LabImageRowIndex04
     ,LabImageFilePath04
     ,LabImageRemark04
    UNION ALL
    SELECT
      '05'
     ,LabImageRowIndex05
     ,LabImageFilePath05
     ,LabImageRemark05
    UNION ALL
    SELECT
      '06'
     ,LabImageRowIndex06
     ,LabImageFilePath06
     ,LabImageRemark06
    UNION ALL
    SELECT
      '07'
     ,LabImageRowIndex07
     ,LabImageFilePath07
     ,LabImageRemark07
    UNION ALL
    SELECT
      '08'
     ,LabImageRowIndex08
     ,LabImageFilePath08
     ,LabImageRemark08
    UNION ALL
    SELECT
      '09'
     ,LabImageRowIndex09
     ,LabImageFilePath09
     ,LabImageRemark09
    UNION ALL
    SELECT
      '10'
     ,LabImageRowIndex10
     ,LabImageFilePath10
     ,LabImageRemark10
    UNION ALL
    SELECT
      '11'
     ,LabImageRowIndex11
     ,LabImageFilePath11
     ,LabImageRemark11
    UNION ALL
    SELECT
      '12'
     ,LabImageRowIndex12
     ,LabImageFilePath12
     ,LabImageRemark12
    UNION ALL
    SELECT
      '13'
     ,LabImageRowIndex13
     ,LabImageFilePath13
     ,LabImageRemark13
    UNION ALL
    SELECT
      '14'
     ,LabImageRowIndex14
     ,LabImageFilePath14
     ,LabImageRemark14
    UNION ALL
    SELECT
      '15'
     ,LabImageRowIndex15
     ,LabImageFilePath15
     ,LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
  WHERE ps.ID = @ID
        AND ISNULL(FilePath, '') <> ''
  ORDER BY c.ImageRowIndex ASC;


  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
     ,patient.Name Name_Patient
     ,patient.Name_Client
     ,fs.Name Name_FilingStatus
     ,soapType.Name Name_SOAPType
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,'-New-' AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [ID_Company]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,@ID_User AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_Patient AS ID_Patient
       ,GETDATE() Date
       ,@FILED_ID_FilingStatus ID_FilingStatus
	   ,@IsDeceased IsDeceased
       ,@ID_SOAPType ID_SOAPType
	   ,@ObjectiveTemplate ObjectiveTemplate 
	   ,@AssessmentTemplate AssessmentTemplate
	   ,@LaboratoryTemplate LaboratoryTemplate) H
    LEFT JOIN dbo.tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
      ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vPatient patient
      ON patient.ID = H.ID_Patient
    LEFT JOIN dbo.tFilingStatus fs
      ON fs.ID = H.ID_FilingStatus
    LEFT JOIN dbo.tSOAPType soapType
      ON soapType.ID = H.ID_SOAPType;
  END;
  ELSE
  BEGIN
    SELECT
      H.*
	  , patient.IsDeceased
	  , @LaboratoryTemplate LaboratoryTemplate
	  ,@ObjectiveTemplate ObjectiveTemplate 
	  ,@AssessmentTemplate AssessmentTemplate
    FROM dbo.vPatient_SOAP H
	LEFT JOIN tPatient patient on h.ID_Patient = patient.ID
    WHERE H.ID = @ID;
  END;


  SELECT * FROM @LabImage ORDER BY ImageRowIndex DESC;

  SELECT
    *
  FROM dbo.vPatient_SOAP_Plan
  WHERE ID_Patient_SOAP = @ID
  ORDER BY DateReturn ASC;
END;

GO

ALTER PROC [dbo].[pGetForSendSOAPPlan]
AS
BEGIN

  DECLARE @Success BIT = 1;
  Declare @MDIgnacioAnimalClinic_ID_Company INT = 8
  Declare @AnimaCareVeterinaryClinic_ID_Company INT = 33
  Declare @AssumptaDogAndCatClinic_ID_Company INT = 38
  Declare @DVMAnimalClinic_ID_Company INT = 24
  Declare @BalagtasAnimalClinicandGroomingCenter_ID_Company INT = 35
  Declare @RDTVeterinaryClinic_ID_Company INT = 41
  Declare @PetFamilyAnimalClinicandGroomingCenter_ID_Company INT = 7
  Declare @CardinalPetClinic_ID_Company INT = 14

  SELECT
    '_'
   ,'' AS records;

  SELECT
    @Success Success;

  SELECT
	c.Name Name_Company
   ,client.Name Name_Client
   ,client.ContactNumber ContactNumber_Client
   ,soapPlan.ID ID_Patient_SOAP_Plan
   ,soapPlan.DateReturn
   ,soapPlan.Name_Item
   ,ISNULL(patientSOAP.Comment, '') Comment
   ,'Good day, ' + client.Name + CHAR(9) + CHAR(13) + ' You have an upcoming appointment for your pet ''' +
    patient.Name  + '''' + ' at ' +  c.Name   + ' at ' +
    FORMAT(soapPlan.DateReturn, 'MM/dd/yyyy dddd') + '.' + CHAR(9) + CHAR(13) +
    'Plan: ' + +CHAR(9) + CHAR(13) +
    ' ' + soapPlan.Name_Item +
    CASE
      WHEN LEN(ISNULL(soapPlan.Comment, '')) > 0 THEN ' - '
      ELSE ''
    END + ISNULL(soapPlan.Comment, '')   + CHAR(9) + CHAR(13) + 
	Case WHEN LEN(ISNULL(c.ContactNumber, '')) > 0 THEN 'If you have questions, kindly message ' + c.ContactNumber + '.' ELSE '' END   + ' ' +
	Case WHEN LEN(ISNULL(c.Address, '')) > 0 THEN c.Address ELSE '' END   + ''
	Message,
    CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) DateSending
  FROM dbo.tPatient_SOAP patientSOAP
  LEFT JOIN dbo.tPatient patient
    ON patient.ID = patientSOAP.ID_Patient
  LEFT JOIN dbo.tClient client
    ON client.ID = patient.ID_Client
  LEFT JOIN tCompany c
    ON c.iD = patientSOAP.ID_Company
  INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
    ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
  WHERE patientSOAP.ID_FilingStatus IN (3)
	AND ISNULL(soapPlan.IsSentSMS, 0) = 0
	AND ( (CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE()))
	OR (CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, GETDATE())) )
	AND patientSOAP.ID_Company IN (
		1, 
		@MDIgnacioAnimalClinic_ID_Company, 
		@AnimaCareVeterinaryClinic_ID_Company, 
		@DVMAnimalClinic_ID_Company,
		@BalagtasAnimalClinicandGroomingCenter_ID_Company,
		@PetFamilyAnimalClinicandGroomingCenter_ID_Company,
		@AssumptaDogAndCatClinic_ID_Company,
		@RDTVeterinaryClinic_ID_Company,
		@CardinalPetClinic_ID_Company
	)

	ORDEr BY c.Name
END

GO

ALTER PROC [dbo].[pGetPurchaseOrder]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' AS PurchaseOrder_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;

    DECLARE @ID_Company INT;
    DECLARE @ID_Employee INT;

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    SELECT @ID_Employee = ID_Employee,
           @ID_Company = emp.ID_Company
    FROM dbo.tUser _user
        INNER JOIN dbo.tEmployee emp
            ON emp.ID = _user.ID_Employee
    WHERE _user.ID = @ID_User;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               status.Name FilingStatus
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '-NEW-' AS [Code],
                   NULL AS [Name],
                   GETDATE() AS [DocumentDate],
                   GETDATE() AS Date,
                   1 AS [IsActive],
                   @ID_Company AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
				   0 IsComputeDiscountRate
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN tFilingStatus status
                ON status.ID = H.ID_FilingStatus;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM vPurchaseOrder H
        WHERE H.ID = @ID;
    END;


    SELECT *
    FROM vPurchaseOrder_Detail detail
    WHERE detail.ID_PurchaseOrder = @ID;
END;

GO

ALTER PROC [dbo].[pCancelPaymentTransaction_validation]
(
    @IDs_PaymentTransaction typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Filed_ID_FilingStatus INT = 1;
    DECLARE @Approved_ID_FilingStatus INT = 3;
    DECLARE @message VARCHAR(400) = '';

    DECLARE @ValidateNotApproved TABLE
    (
        Code VARCHAR(30),
        Name_FilingStatus VARCHAR(30)
    );
    DECLARE @Count_ValidateNotApproved INT = 0;

    /* Validate Billing Invoices Status is not Approved*/
    INSERT @ValidateNotApproved
    (
        Code,
        Name_FilingStatus
    )
    SELECT Code,
           Name_FilingStatus
    FROM dbo.vPaymentTransaction bi
    WHERE EXISTS
    (
        SELECT ID FROM @IDs_PaymentTransaction ids WHERE ids.ID = bi.ID
    )
          AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus );

    SELECT @Count_ValidateNotApproved = COUNT(*)
    FROM @ValidateNotApproved;

    IF (@Count_ValidateNotApproved > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotApproved > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to cancel:';

        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus
        FROM @ValidateNotApproved;
        THROW 50001, @message, 1;
    END;
	
	/*Validate Need to cancel Last Payment Transaction*/
	DECLARE @BillingInvoiceLastPTtrans TABLE (
		RowID int identity(1,1) primary key,
		ID_BillingInvoice INT, 
		Code_BillingInvoice VARCHAR(MAX),
		ID_PaymentTransaction INT,
		Last_ID_PaymentTransaction INT,
		Last_Code_PaymentTransaction VARCHAR(MAX)
	)

	INSERT @BillingInvoiceLastPTtrans (
		ID_BillingInvoice, 
		Code_BillingInvoice,
		ID_PaymentTransaction
	)
	SELECT 
		DISTINCT p.ID_BillingInvoice, 
		biHed.Code,
		p.ID
	FROM tPaymentTransaction p 
	INNER JOIN @IDs_PaymentTransaction idspt 
		ON p.ID = idspt.ID
	INNER JOIN tBillingInvoice biHed 
		ON biHed.ID = p.ID_BillingInvoice

	UPDATE @BillingInvoiceLastPTtrans
	SET
		Last_ID_PaymentTransaction = tbl.Last_ID_PaymentTransaction,
		Last_Code_PaymentTransaction = pt.Code
	FROM    (
	SELECT 
		MAX(p.ID) Last_ID_PaymentTransaction, 
		p.ID_BillingInvoice
	FROM  @BillingInvoiceLastPTtrans biPTTrans 
	INNER JOIN tPaymentTransaction p 
		ON biPTTrans.ID_BillingInvoice = p.ID_BillingInvoice
	WHERE
		p.ID_FilingStatus IN (3)
	GROUP BY p.ID_BillingInvoice ) tbl 
	INNER JOIN tPaymentTransaction pt
		ON pt.ID = tbl.Last_ID_PaymentTransaction


	DECLARE @ValidateNotLastPaymentTransaction TABLE (
		Code VARCHAR(MAX),
		Code_BillinigInvoice VARCHAR(MAX)
	)
	DECLARE @Count_NotLastPaymentTransaction INT = 0


	INSERT @ValidateNotLastPaymentTransaction (
		Code,
		Code_BillinigInvoice
	)
	SELECT 
		Last_Code_PaymentTransaction,
		Code_BillingInvoice
	FROM @BillingInvoiceLastPTtrans 
	WHERE 
		Last_ID_PaymentTransaction <> ID_PaymentTransaction

	SELECT 
		@Count_NotLastPaymentTransaction = COUNT(*)
	FROM @ValidateNotLastPaymentTransaction

	IF (@Count_NotLastPaymentTransaction > 0)
	BEGIN

		SET @message = 'The following record' + CASE
													WHEN @Count_NotLastPaymentTransaction > 1 THEN
														's are'
													ELSE
														' is '
												END + 'need to cancel first:';

		SELECT @message = @message + CHAR(10) + Code + ' of ' + Code_BillinigInvoice
		FROM @ValidateNotLastPaymentTransaction;
		THROW 50001, @message, 1;
	END;

END;

GO

CREATE OR ALTER PROC [dbo].[pValidateClient]
(
    @ID_Client INT,
	@Name VARCHAR(MAX),
	@ID_UserSession INT
)
AS
BEGIN

    DECLARE @isValid BIT = 1;
    DECLARE @IsWarning BIT = 0;
    DECLARE @message VARCHAR(MAX) = '';
    DECLARE @msgTHROW VARCHAR(MAX) = '';

    DECLARE @ID_User INT = 0;
    DECLARE @ID_Company INT = 0;

    SELECT 
		@ID_User = userSession.ID_User,
		@ID_Company = _user.ID_Company
    FROM dbo.tUserSession userSession INNER JOIN vUser _user ON userSession.ID_User = _user.ID
    WHERE userSession.ID = @ID_UserSession;

	SET @Name = LTRIM(RTRIM(ISNULL(@Name,'')))

	if ISNULL(@ID_Company, 0) = 0
	BEGIN

		SELECT  
			@ID_Company = ID_Company
		FROM tClient
		WHERE 
			ID = @ID_Client
	END

    BEGIN TRY

		DECLARE @ExistName Varchar(MAX) = ''
		DECLARE @Count INT = 0

		IF LEN(@Name) = 0
		BEGIN
			
			SET @msgTHROW = 'Client Name is required.';
			THROW 50005, @msgTHROW, 1;
		END
		
		
		SELECT  
			@ExistName = Name
		FROM tClient 
		WHERE 
			ID NOT IN (@ID_Client) AND (
			LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND
			IsActive = 1 AND
			ID_Company = @ID_Company)


		IF LEN(LTRIM(RTRIM(ISNULL(LOWER(@ExistName), '')))) > 0
		BEGIN

			SET @msgTHROW = 'Client '''+ @Name +''' is already exist';

			SELECT  
				@Count = COUNT(*)
			FROM tClient 
			WHERE 
				(
				LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND
				IsActive = 1 AND
				ID_Company = @ID_Company)
			GROUP BY
				Name
				 
			if(@Count > 1)
			BEGIN

				SET @IsWarning = 1
				SET @msgTHROW = @msgTHROW + ' ' + FORMAT(@Count,'0') + ' times'
			END;

			set @msgTHROW = @msgTHROW + '.';

			THROW 50005, @msgTHROW, 1;
		END;
		 
    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @isValid = 0;
    END CATCH;

    SELECT '_'; 

    SELECT @isValid isValid,
		   @IsWarning isWarning,
           @message message;

END;

GO

Update tcompany set 
	Address = 'Block 4 Lot 6-B Greenvillas 2, Brgy. Buhay na Tubig, Highway Imus, Cavite,4103 Philippines '+ char(9) + char(13) + '
				Clinic Hours: 8am - 5pm
				Lunch break- 12pm- 1pm', 
	ContactNumber = '09062921197' 
WHERE ID = 7

Update tcompany set 
	Address = '',
	ContactNumber = '09225553867 or visit our FB page RDT'
WHERE ID = 41

Update tcompany set
	Address = 'Address: Ortigas Ave., Extension Corner Hunter ROTC. Guerilla St., Brgy. San Juan, Cainta, Rizal', 
	ContactNumber = '09190090841 / 09269910470 / 897-3264'	
WHERE ID = 38

Update tCompany SET ImageHeaderFilename = ImageLogoFilename 
WHERE
	ImageHeaderFilename IS NULL

UPDATE tPurchaseOrder Set SubTotal = GrossAmount, TotalAmount = GrossAmount

UPDATE tReceivingReport Set SubTotal = GrossAmount, TotalAmount = GrossAmount