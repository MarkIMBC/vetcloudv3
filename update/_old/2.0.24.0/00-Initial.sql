IF NOT EXISTS (SELECT 1
               FROM   INFORMATION_SCHEMA.TABLES
               WHERE  TABLE_TYPE = 'BASE TABLE'
                      AND TABLE_NAME = 'tPatient_Lodging')
  BEGIN
      exec _pCreateAppModuleWithTable
        'tPatient_Lodging',
        1,
        NULL,
        NULL

      exec _pAddModelProperty
        'tPatient_Lodging',
        'ID_Client',
        2

      exec _pAddModelProperty
        'tPatient_Lodging',
        'ID_Patient',
        2

      exec _pAddModelProperty
        'tPatient_Lodging',
        'ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatient_Lodging',
        'DateStart',
        5

      exec _pAddModelProperty
        'tPatient_Lodging',
        'DateEnd',
        5

      exec _pAddModelProperty
        'tPatient_Lodging',
        'DateCheckIn',
        5

      exec _pAddModelProperty
        'tPatient_Lodging',
        'DateCheckOut',
        5

				exec _pAddModelProperty
  'tPatient_Lodging',
  'HourCount',
  3

		exec _pAddModelProperty
  'tPatient_Lodging',
  'RateAmount',
  3

  	exec _pAddModelProperty
  'tPatient_Lodging',
  'AdvancedPaymentAmount',
  3

  	exec _pAddModelProperty
  'tPatient_Lodging',
  'TotalAmount',
  3

    	exec _pAddModelProperty
  'tPatient_Lodging',
  'PaymentAmount',
  3

        	exec _pAddModelProperty
  'tPatient_Lodging',
  'ChangeAmount',
  3

    	exec _pAddModelProperty
  'tPatient_Lodging',
  'RemainingAmount',
  3


      	exec _pAddModelProperty
  'tPatient_Lodging',
  'ChangeAmount',
  3




  END

  

      exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vPatient_Lodging
AS
  SELECT H.*,
         client.Name  Name_Client,
         patient.Name Name_Patient,
         fs.Name      Name_FilingStatus
  FROm   TPatient_Lodging H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                on patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                on fs.ID = H.ID_FilingStatus

GO

CREATE OR
ALTER VIEW vPatient_Lodging_Listview
AS
  SELECT ID,
         ID_Client,
         ID_Patient,
         Name_Client,
         Name_Patient,
         DateCheckIn,
         DateCheckOut,
		 HourCount,
		 PayableAmount,
		 Name_FilingStatus
         ID_Company
  FROm   vPatient_Lodging

GO

ALTER PROCEDURE dbo.pGetPatient_Lodging @ID         INT = -1,
                                        @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
						   0  RateAmount,
                           GETDATE() AS DateCheckIn,
                           GETDATE() AS DateCheckOut) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Lodging H
            WHERE  H.ID = @ID
        END
  END

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_Patient_Lodging] (@ID_CurrentObject VARCHAR(10),
                                                      @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Lodging
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Lodging';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Lodging
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

GO 
