IF NOT EXISTS (SELECT 1
               FROM   INFORMATION_SCHEMA.TABLES
               WHERE  TABLE_TYPE = 'BASE TABLE'
                      AND TABLE_NAME = 'tCompany_SMSSetting')
  BEGIN
      CREATE TABLE [dbo].[tCompany_SMSSetting]
        (
           [ID]                [int] IDENTITY(1, 1) NOT NULL,
           [ID_Company]        [int] NULL,
           [MaxSMSCountPerDay] [int] NULL,
           [IsActive]          [bit] NULL,
           [DateCreated]       [datetime] NULL,
           [DateModified]      [datetime] NULL,
           CONSTRAINT [PK_tCompany_SMSSetting] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
        )
      ON [PRIMARY];

      ALTER TABLE [dbo].[tCompany_SMSSetting]
        ADD DEFAULT ((1)) FOR [IsActive];

      ALTER TABLE [dbo].[tCompany_SMSSetting]
        WITH CHECK ADD CONSTRAINT [FK_tCompany_SMSSetting_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID]);

      ALTER TABLE [dbo].[tCompany_SMSSetting]
        CHECK CONSTRAINT [FK_tCompany_SMSSetting_ID_Company];
  END

GO

Alter VIEW vClient_ListView
AS
  SELECT ID,
         ID_Company,
         Name,
         ISNULL(ContactNumber, '')
         + CASE
             WHEN LEN(ISNULL(ContactNumber, '')) > 0
                  AND LEN(ISNULL(ContactNumber2, '')) > 0 THEN ' / '
             ELSE ''
           END
         + ISNULL(ContactNumber2, '') ContactNumbers,
         DateCreated,
         ISNULL(IsActive, 0)          IsActive
  FROM   dbo.vClient
  WHERE  ISNULL(IsActive, 0) = 1

GO

CREATE OR
ALTER VIEW [dbo].[vSMSCountPerCompany_Patient_SOAP_Plan]
AS
  SELECT CONVERT(Date, DateSent) DateSent,
         ID_Company,
         COUNT(*)                TotalSMSCount
  FROM   tPatient_SOAP_Plan soapPlan
         INNER JOIN tPatient_SOAP soap
                 ON soapPlan.ID_Patient_SOAP = soap.ID
  WHERE  DateSent IS NOT NULL
  GROUP  BY ID_Company,
            CONVERT(Date, DateSent)

GO

CREATE OR
ALTER FUNCTION fGetDateCoverageString(@DateStart DateTime,
                                      @DateEnd   DateTime)
RETURNS VARCHAR(1000)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = ''

      IF FORMAT(@DateStart, 'yyyy-MM-dd') = FORMAT(@DateEnd, 'yyyy-MM-dd')
        BEGIN
            SET @result = FORMAT(@DateStart, 'yyyy-MM-dd')

            IF( FORMAT(@DateStart, 'hh:mm tt') <> FORMAT(@DateEnd, 'hh:mm tt') )
              BEGIN
                  SET @result = @result + ' ' + FORMAT(@DateStart, 'hh:mm tt')
                  SET @result = @result + '  to '
                  SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
              END
            ELSE
              BEGIN
                  SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
              END
        END
      ELSE
        BEGIN
            SET @result = FORMAT(@DateStart, 'yyyy-MM-dd')
            SET @result = @result + ' ' + FORMAT(@DateStart, 'hh:mm tt')
            SET @result = @result + '  to '
            SET @result = @result + FORMAT(@DateEnd, 'yyyy-MM-dd')
            SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
        END

      RETURN @result
  END

GO

ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                            ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                            UniqueID,
         _model.Oid                                                                         Oid_Model,
         _model.Name                                                                        Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID                                                                     ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                         DateStart,
         patientSOAPPlan.DateReturn                                                         DateEnd,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                   FormattedDateStart,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                  FormattedDateEnd,
         ''                                                                                 FormattedDateStartTime,
         ''                                                                                 FormattedDateEndTime,
         patientSOAP.Code                                                                   ReferenceCode,
         patientSOAP.Name_Client                                                            Paticular,
         ISNULL(patientSOAP.Name_SOAPType, '')
         + ' - '
         + ISNULL(patientSOAPPlan.Name_Item, '')                                            Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn) DateCoverage
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                 ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                 UniqueID,
         _model.Oid                                                               Oid_Model,
         _model.Name                                                              Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID                                                          ID_CurrentObject,
         patientAppnt.DateStart                                                   DateStart,
         patientAppnt.DateEnd                                                     DateEnd,
         FORMAT(patientAppnt.DateStart, 'yyyy-MM-dd')                             FormattedDateStart,
         FORMAT(patientAppnt.DateEnd, 'yyyy-MM-dd')                               FormattedDateEnd,
         FORMAT(patientAppnt.DateStart, 'hh:mm tt')                               FormattedDateStartTime,
         FORMAT(patientAppnt.DateEnd, 'hh:mm tt')                                 FormattedDateEndTime,
         ISNULL(patientAppnt.Code, 'Patient Appt.')                               ReferenceCode,
         patientAppnt.Name_Client                                                 Paticular,
         ISNULL(patientAppnt.Name_Patient, '')
         + ' - '
         + ISNULL(patientAppnt.Name_SOAPType, '')                                 Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd) DateCoverage
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1 )

GO

ALTER PROC [dbo].[pGetPatientAppointment] @ID         INT = -1,
                                          @DateStart  DATETIME = NULL,
                                          @DateEnd    DATETIME = NULL,
                                          @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_';

      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      if @DateStart IS NULL
        SET @DateStart = GETDATE()

      if @DateEnd IS NULL
        SET @DateEnd = GETDATE()

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL       AS [_],
                           -1         AS [ID],
                           NULL       AS [Code],
                           NULL       AS [Name],
                           1          AS [IsActive],
                           NULL       AS [ID_Company],
                           NULL       AS [Comment],
                           NULL       AS [DateCreated],
                           NULL       AS [DateModified],
                           NULL       AS [ID_CreatedBy],
                           NULL       AS [ID_LastModifiedBy],
                           @DateStart DateStart,
                           @DateEnd   DateEnd,
                           1          [ID_FilingStatus]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID;
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatientAppointment H
            WHERE  H.ID = @ID;
        END;
  END;

GO

ALTER PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout] (@ID_PaymentTransaction INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      Declare @ID_Company int = 0
      Declare @ID_BillingInvoice int = 0
      Declare @Name_Company VARCHAR(MAX)= ''
      Declare @Address_Company VARCHAR(MAX)= ''
      Declare @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      Declare @ImageLogo_Company VARCHAR(MAX)= ''
      Declare @ContactNumber_Company VARCHAR(MAX)= ''
      Declare @Code_PaymentTranaction VARCHAR(MAX)= ''
      Declare @ID_PaymentMode int = 0
      Declare @ReferenceTransactionNumber VARCHAR(MAX)= ''
      Declare @CheckNumber VARCHAR(MAX)= ''
      Declare @CardNumber VARCHAR(MAX)= ''
      Declare @Name_CardType VARCHAR(MAX)= ''
      Declare @CardHolderName VARCHAR(MAX)= ''
      Declare @Name_PaymentStatus VARCHAR(MAX)= ''
      Declare @CashAmount DECIMAL(18, 4) = 0.00
      Declare @GCashAmount DECIMAL(18, 4) = 0.00
      Declare @CardAmount DECIMAL(18, 4) = 0.00
      Declare @CheckAmount DECIMAL(18, 4) = 0.00
      Declare @PayableAmount DECIMAL(18, 4) = 0.00
      Declare @PaymentAmount DECIMAL(18, 4) = 0.00
      Declare @ChangeAmount DECIMAL(18, 4) = 0.00
      Declare @RemainingAmount DECIMAL(18, 4) = 0.00
      Declare @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      Declare @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      Declare @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      Declare @Name_Client VARCHAR(MAX)= ''
      Declare @Name_Patient VARCHAR(MAX)= ''
      Declare @Date_BillingInvoice datetime
      Declare @Code_BillingInvoice VARCHAR(MAX)= ''

      SELECT @ID_Company = ID_Company,
             @ID_BillingInvoice = ID_BillingInvoice,
             @ID_PaymentMode = ID_PaymentMethod,
             @Code_PaymentTranaction = Code,
             @PayableAmount = PayableAmount,
             @RemainingAmount = RemainingAmount,
             @ChangeAmount = ChangeAmount,
             @CashAmount = CashAmount,
             @ReferenceTransactionNumber = ReferenceTransactionNumber,
             @GCashAmount = GCashAmount,
             @Name_CardType = Name_CardType,
             @CardNumber = CardNumber,
             @CardHolderName = CardHolderName,
             @CardAmount = CardAmount,
             @CheckNumber = CheckNumber,
             @CheckAmount = CheckAmount,
             @PaymentAmount = PaymentAmount
      FROM   vPaymentTransaction
      WHERE  ID = @ID_PaymentTransaction

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, '')
      FROM   vCompany
      WHERE  ID = @ID_Company

      SELECT @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.Name_Patient, 'N/A'),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      SELECT @billingItemsLayout = @billingItemsLayout
                                   + '  
   <div class="display-block clearfix">  
    <span class="bold">'
                                   + biDetail.Name_Item
                                   + '</span><br/>  
    <span class="float-left">  
     &nbsp;&nbsp;&nbsp;'
                                   + FORMAT(biDetail.Quantity, '#,#0.##')
                                   + '  
    </span>  
    <span class="float-right">'
                                   + FORMAT(biDetail.Amount, '#,#0.00')
                                   + '</span>  
   </div>'
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      if @ID_PaymentMode = @Cash_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Cash Amount  
    </span>  
    <span class="float-right">'
                                 + FORMAT(@CashAmount, '#,#0.00') + '</span>  
   </div>'
        END
      else if @ID_PaymentMode = @GCash_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Ref. No.  
    </span>  
    <span class="float-right">'
                                 + @ReferenceTransactionNumber + '</span>  
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     G-Cash  
    </span>  
    <span class="float-right">'
                                 + FORMAT(@GCashAmount, '#,#0.00') + '</span>  
   </div>'
        END
      ELSE if @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Card Type  
    </span>  
    <span class="float-right">'
                                 + @Name_CardType + '</span>  
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Card #  
    </span>  
    <span class="float-right">'
                                 + @CardNumber + '</span>  
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Holder  
    </span>  
    <span class="float-right">'
                                 + @CardHolderName + '</span>  
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Card Amt.  
    </span>  
    <span class="float-right">'
                                 + FORMAT(@CardAmount, '#,#0.00') + '</span>  
   </div>'
        END
      ELSE if @ID_PaymentMode = @Check_ID_PaymentMethod
        BEGIN
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Check No.  
    </span>  
    <span class="float-right">'
                                 + @CheckNumber + '</span>  
   </div>'
            SET @paymentLayout = @paymentLayout
                                 + '  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Check Amt.  
    </span>  
    <span class="float-right">'
                                 + FORMAT(@CheckAmount, '#,#0.00') + '</span>  
   </div>'
        END

      DECLARE @title VARCHAR(MAX)= ''
      DECLARE @style VARCHAR(MAX)= '  
  body{  
   margin: 0px;  
   padding: 0px;  
   font-family:  arial, sans-serif;  
   font-weight: normal;  
   font-style: normal;  
   font-size: 13px;  
  }  
  
  .logo{  
  
   width: 120px;  
   margin-bottom: 10px;  
  }  
  
  .receipt-container{  
   width: 160px;  
  }  
  
  .company-logo{  
   display: block;  
   font-weight: bold;  
   text-align: center;  
   word-wrap: break-word;  
   margin-bottom: 12px;  
  }  
  
  .company-name{  
   display: block;  
   font-weight: bold;  
   text-align: center;  
   word-wrap: break-word;  
  }  
  
  .company-address{  
   display: block;  
   text-align: center;  
   word-wrap: break-word;  
   font-size: 11px  
  }  
  
  .company-contactnum-container{  
   display: block;  
   text-align: center;  
   word-wrap: break-word;  
   font-weight: bold;  
   font-size: 11px  
  }  
  
  .company-contactnum{  
   font-weight: 200;  
   text-align: center;  
   word-wrap: break-word;  
  
  }  
  
  .float-left{  
   float: left;  
  }  
  
  .float-right{  
   float: right;  
  }  
  
  .clearfix {  
   overflow: auto;  
  }  
  
  .bold{  
   font-weight: bold;  
  }  
  
  .display-block{  
  
   display: block;  
  }  
  
  .title{  
   text-align: center;  
   word-wrap: break-word;  
   display: block;  
   padding-top: 15px;  
   padding-bottom: 15px;  
  }  
 '
      DECLARE @content VARCHAR(MAX)= '  
  <div class="receipt-container">  
  '
        + CASE
            WHEN LEN(@ImageLogo_Company) > 0 THEN '<div class="company-logo"><img class="logo" src="'
                                                  + @ImageLogoLocationFilenamePath_Company
                                                  + '" alt="" srcset=""></div>'
            ELSE ''
          END
        + '  
   <div class="company-name">'
        + @Name_Company + ''
        + '</div>  
   <div class="company-address">'
        + @Address_Company + ''
        + '</div>  
   <div class="company-contactnum-container" style="'
        + CASE
            WHEN LEN(@ContactNumber_Company) = 0 THEN 'display: none'
            ELSE ''
          END
        + '">  
    Contact No: <span class="company-contactnum">'
        + @ContactNumber_Company
        + '</span>  
   </div>  
   <div class="title bold">INVOICE</div>  
   <div class="display-block"><span class="bold">Bill To: </span>'
        + @Name_Client
        + '</div>  
   <div class="display-block"><span class="bold">Pet Name: </span>'
        + @Name_Patient
        + '</div>  
   <div class="display-block">  
    <span class="bold">Invoice Date: </span>'
        + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy')
        + '  
   </div>   
   <div class="display-block"><span class="bold">BI #: </span>'
        + @Code_BillingInvoice + '</div>   
   <br/>  
   '
        + @billingItemsLayout
        + '  
  
   <div class="title bold">TOTALS</div>  
     
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Subtotal  
    </span>  
    <span class="float-right">'
        + FORMAT(@SubTotal_BillingInvoice, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Disc. Rate  
    </span>  
    <span class="float-right">'
        + FORMAT(@DiscountRate_BillingInvoice, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Disc. Amount  
    </span>  
    <span class="float-right">'
        + FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Total Amount  
    </span>  
    <span class="float-right">'
        + FORMAT(@TotalAmount_BillingInvoice, '#,#0.00')
        + '</span>  
   </div>  
  
   <div class="title bold">PAYMENT</div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     PT #  
    </span>  
    <span class="float-right">'
        + @Code_PaymentTranaction + '</span>  
   </div>  
   '
        + @paymentLayout
        + '  
   <br/>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Balance  
    </span>  
    <span class="float-right">'
        + FORMAT(@PayableAmount, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Payment  
    </span>  
    <span class="float-right">'
        + FORMAT(@PaymentAmount, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Remaining  
    </span>  
    <span class="float-right">'
        + FORMAT(@RemainingAmount, '#,#0.00')
        + '</span>  
   </div>  
   <div class="display-block clearfix">  
    <span class="float-left bold">  
     Change  
    </span>  
    <span class="float-right">'
        + FORMAT(@ChangeAmount, '#,#0.00')
        + '</span>  
   </div>  
  
   <div class="title">THIS IS NOT AN OFFICIAL RECEIPT</div>  
  
   <div class="title"> Print Date '
        + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy hh:mm:ss tt')
        + '</div>  
  </div>  
 '

      SELECT '_'

      SELECT @Code_PaymentTranaction title,
             @style                  style,
             @content                content,
             @isAutoPrint            isAutoPrint,
             @ID_PaymentTransaction  ID_PaymentTransaction,
             @ID_BillingInvoice      ID_BillingInvoice,
             @millisecondDelay       millisecondDelay
  END

GO

ALTER PROC pImportPatientSOAP (@records        typImportPatientSOAP READONLY,
                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_Company INT = 0;
      DECLARE @ID_User INT = 0;
      DECLARE @Count INT = 0;
      DECLARE @Counter_DocumentNo INT = 0;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model UNIQUEIDENTIFIER;
      DECLARE @Old_soap_ids TABLE
        (
           Old_soap_id INT
        )
      DECLARE @forImport TABLE
        (
           Old_soap_id     INT,
           Old_patient_id  INT,
           Date            DATETIME,
           ID_SOAPType     INT,
           ID_FilingStatus INT,
           Subjective      VARCHAR(MAX),
           Objective       VARCHAR(MAX),
           Assessment      VARCHAR(MAX),
           Prescription    VARCHAR(MAX),
           Planning        VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = u.ID_Company
      FROM   vUser u
      WHERE  u.ID = @ID_User

      INSERT @Old_soap_ids
             (Old_soap_id)
      SELECT CONVERT(INT, soap_id)
      FROM   @records
      EXCEPT
      SELECT ps.Old_soap_id
      FROM   tPatient_SOAP ps
      WHERE  ps.ID_Company = @ID_Company

      SELECT @Oid_Model = m.Oid
      FROM   dbo._tModel m
      WHERE  m.TableName = 'tPatient_SOAP';

      SELECT @Count = COUNT(*)
      FROM   @Old_soap_ids

      INSERT @forImport
             (Old_soap_id,
              Old_patient_id,
              Date,
              ID_SOAPType,
              ID_FilingStatus,
              Subjective,
              Objective,
              Assessment,
              Planning,
              Prescription)
      SELECT record.soap_id,
             record.pet_id,
             record.date,
             CASE
               WHEN LOWER(record.status) = 'confinement' THEN 2
               ELSE 1
             END,
             3,
             record.subj,
             'Heart Rate (bpm): '
             + CASE
                 WHEN LEN(ISNULL(heart_rate, '')) > 0 THEN record.heart_rate
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13)
             + 'Respiratory Rate (brpm): '
             + CASE
                 WHEN LEN(ISNULL(respiratory_rate, '')) > 0 THEN record.respiratory_rate
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Weight: '
             + CASE
                 WHEN LEN(ISNULL(weight, '')) > 0 THEN record.weight + ' ' + record.weight_unit
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Length: '
             + CASE
                 WHEN LEN(ISNULL(Length, '')) > 0 THEN record.Length
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'BCS: '
             + CASE
                 WHEN LEN(ISNULL(bcs, '')) > 0 THEN record.bcs
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Temperature: '
             + CASE
                 WHEN LEN(ISNULL(Temperature, '')) > 0
                      AND Temperature <> 'NULL' THEN record.Temperature + ' '
                                                     + record.temperature_degrees
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             'Differential Diagnosis: : '
             + CASE
                 WHEN LEN(ISNULL(diagnosis, '')) > 0
                      AND diagnosis <> 'NULL' THEN record.diagnosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Notes: '
             + CASE
                 WHEN LEN(ISNULL(notes, '')) > 0
                      AND notes <> 'NULL' THEN record.notes
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Test Results: '
             + CASE
                 WHEN LEN(ISNULL(test_result, '')) > 0
                      AND test_result <> 'NULL' THEN record.test_result
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Final Diagnosis: '
             + CASE
                 WHEN LEN(ISNULL(final_diagnosis, '')) > 0
                      AND final_diagnosis <> 'NULL' THEN record.final_diagnosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Prognosis: '
             + CASE
                 WHEN LEN(ISNULL(prognosis, '')) > 0
                      AND prognosis <> 'NULL' THEN record.prognosis
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Category: '
             + CASE
                 WHEN LEN(ISNULL(category, '')) > 0
                      AND category <> 'NULL' THEN record.category
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             'Treatment: '
             + CASE
                 WHEN LEN(ISNULL(treatment, '')) > 0 THEN record.treatment
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Remarks: '
             + CASE
                 WHEN LEN(ISNULL(remarks, '')) > 0 THEN record.remarks
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Date Return: '
             + CASE
                 WHEN record.date_return IS NOT NULL THEN FORMAT(record.date_return, 'MMMM dd, yyyy')
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + 'Reason for Follow-up: '
             + CASE
                 WHEN LEN(ISNULL(reason, '')) > 0 THEN record.reason
                 ELSE 'N/A'
               END
             + CHAR(9) + CHAR(13) + '',
             CASE
               WHEN LEN(ISNULL(prescribed, '')) > 0 THEN record.prescribed
               ELSE 'N/A'
             END
      FROM   @records record

      INSERT tPatient_SOAP
             (Old_soap_id,
              Code,
              Date,
              ID_SOAPType,
              ID_FilingStatus,
              ID_Patient,
              History,
              Subjective,
              Objective,
              Assessment,
              Diagnosis,
              Prescription,
              Planning,
              Name,
              IsActive,
              ID_Company,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_ApprovedBy,
              ID_CanceledBy,
              DateApproved,
              DateCanceled)
      SELECT import.Old_soap_id,
             dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, ROW_NUMBER()
                                                                    OVER (
                                                                      ORDER BY Old_soap_id) - 1) AS Row_Number,
             import.Date,
             import.ID_SOAPType,
             import.ID_FilingStatus,
             ID                                                                                  ID_Patient,
             import.Subjective                                                                   History,
             import.Subjective,
             import.Objective,
             import.Assessment,
             import.Assessment                                                                   Diagnosis,
             import.Prescription,
             '',
             NULL,
             1,
             @ID_Company,
             NULL,
             GETDATE(),
             GETDATE(),
             1,
             1,
             NULL,
             NULL,
             NULL,
             NULL
      FROM   tPatient p
             INNER JOIN @forImport import
                     ON import.Old_patient_id = p.Old_patient_id
      WHERE  p.ID_Company = @ID_Company
             AND import.Old_soap_id IN (SELECT Old_soap_id
                                        FROM   @Old_soap_ids)

      IF @Count > 0
        BEGIN
            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + @Count
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;
        END

      UPDATE tPatient_SOAP
      SET    Subjective = import.Subjective,
             Objective = import.Objective,
             Assessment = import.Assessment,
             Planning = import.Planning,
             Prescription = import.Prescription,
             ID_FilingStatus = import.ID_FilingStatus
      FROM   tPatient_SOAP ps
             INNER JOIN @forImport import
                     ON ps.Old_soap_id = import.Old_soap_id
      WHERE  ps.ID_Company = @ID_Company

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO

CREATE OR ALTER PROC [dbo].[pGetSendSOAPPlan](@Date      DateTime,
                                    @IsSMSSent Bit = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETime,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETime,
           ID_Patient_SOAP_Plan INT
        )

      INSERT @record
      SELECT c.ID                                                                                                                            ID_Company,
             c.Name                                                                                                                          Name_Company,
             client.Name                                                                                                                     Name_Client,
             ISNULL(client.ContactNumber, '0')                                                                                               ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                 Comment,
             dbo.fGetSOAPLANMessage(c.Name, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                            DateSending,
             soapPlan.ID                                                                                                                     ID_Patient_SOAP_Plan
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND ( ( CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn)) = CONVERT(DATE, @Date) )
                    OR ( CONVERT(DATE, DATEADD(DAY, 0, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ) )
             AND patientSOAP.ID_Company IN (SELECT ID_Company
                                            FROM   tCompany_SMSSetting
                                            WHERE  ISNULL(IsActive, 0) = 1)
      ORDER  BY c.Name

      SELECT @Success Success;

      SELECT FORMAT(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             SUM(CASE
                   WHEN LEN(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN LEN(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN LEN(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          ConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateSending, 'yyyy-MM-dd'),
                Name_Company
      Order  BY DateSending DESC,
                Name_Company
  END

SELECT *
FROM   @record
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,
          DateReturn ASC,
          Name_Company

GO

CREATE OR ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Patient_SOAP INT,
                                        @iTextMo_Status  INT)
AS
  BEGIN
      /*  
       iTextMo Status  
       "-1" = Max Sent Count Reached (VetCloud)
       "1" = Invalid Number.  
       "2" = Number prefix not supported. Please contact us so we can add.  
       "3" = Invalid ApiCode.  
       "4" = Maximum Message per day reached. This will be reset every 12MN.  
       "5" = Maximum allowed characters for message reached.  
       "6" = System OFFLINE.  
       "7" = Expired ApiCode.  
       "8" = iTexMo Error. Please try again later.  
       "9" = Invalid Function Parameters.  
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.  
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.  
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.  
       "13" = Invalid or Not Registered Custom Sender ID.  
       "14" = Invalid preferred server number.  
       "15" = IP Filtering enabled - Invalid IP.  
       "16" = Authentication error. Contact support at support@itexmo.com  
       "17" = Telco Error. Contact Support support@itexmo.com  
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com  
       "19" = Account suspended. Contact Support support@itexmo.com  
       "0" = Success! Message is now on queue and will be sent soon  
      */
      DECLARE @Success BIT = 1;

      IF @iTextMo_Status = 0
        BEGIN
            UPDATE dbo.tPatient_SOAP_Plan
            SET    IsSentSMS = 1,
                   DateSent = GETDATE()
            FROM   tPatient_SOAP_Plan psp
            WHERE  psp.ID = @ID_Patient_SOAP
        END

      INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [iTextMo_Status],
                   [ID_Patient_SOAP])
      VALUES      (NULL,
                   NULL,
                   1,
                   1,
                   NULL,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @iTextMo_Status,
                   @ID_Patient_SOAP)

      SELECT '_'

      SELECT @Success Success;
  END

GO


CREATE OR
ALTER PROC [dbo].[pAdd_Company_SMSSetting](@ID_Company        INT,
                                           @MaxSMSCountPerDay INT)
AS
  BEGIN
      Declare @Already_Exist_Count INT = 0
      DECLARE @CompanyName VARCHAR(400) = '';
      DECLARE @message VARCHAR(400) = '';

      /* Validate if Company Exis*/
      SELECT @Already_Exist_Count = COUNT(*)
      FROM   dbo.tCompany_SMSSetting
      WHERE  ID_Company = @ID_Company;

      IF @Already_Exist_Count > 0
        BEGIN
            SELECT @CompanyName = Name
            FROM   tCompany
            WHERE  ID = @ID_Company

            SET @message = @CompanyName + ' is already exist.';

            THROW 50001, @message, 1;
        END;

      INSERT INTO [dbo].[tCompany_SMSSetting]
                  ([ID_Company],
                   [MaxSMSCountPerDay],
                   [IsActive],
                   [DateCreated],
                   [DateModified])
      VALUES      ( @ID_Company,
                    @MaxSMSCountPerDay,
                    1,
                    GETDATE(),
                    GETDATE() )
  END

GO

CREATE OR
ALTER PROC [dbo].[pCheckCompanySMSSending](@ID_Company  INT,
                                           @DateSending DATE)
AS
    DECLARE @IsAllowedToSendSMS BIT = 0
    DECLARE @MaxSMSCountPerDay INT = 0
    DECLARE @TotalSMSCount INT = 0

    SELECT @MaxSMSCountPerDay = MaxSMSCountPerDay
    FROM   tCompany_SMSSetting
    WHERE  ID_Company = @ID_Company

    SELECT @TotalSMSCount = TotalSMSCount
    FROM   vSMSCountPerCompany_Patient_SOAP_Plan
    WHERE  ID_Company = @ID_Company
           AND CONVERT(Date, DateSent) = CONVERT(Date, @DateSending)

    SET @IsAllowedToSendSMS = CASE
                                WHEN ( @TotalSMSCount < @MaxSMSCountPerDay ) THEN 1
                                ELSE 0
                              END

    SELECT '_'

    SELECT @ID_Company         ID_Company,
           @DateSending        DateSending,
           @IsAllowedToSendSMS IsAllowedToSendSMS,
           @TotalSMSCount      TotalSMSCount,
           @MaxSMSCountPerDay  MaxSMSCountPerDay

GO

/* Update Pawawesome Import SOAP Subjective to History/Primary Proclaim and Assessment to Diagnosis*/
Update tPatient_SOAP
SET    History = Subjective,
       Diagnosis = Assessment
WHERE  ID_COMPANY = 51
       AND Old_soap_id IS NOT NULL

/*
	Pet Family Animal Clinic & Grooming Center
  
	Buhlon, Aura's  Pets : Cooper, Audi and Winter
	Transfer to : Fajardo, Kezia Ann
*/
update tPatient
SET    ID_CLIENT = 39703
Where  ID IN ( 38376, 38381, 38383 )

GO

IF (SELECT COUNT(*)
    FROM   [tCompany_SMSSetting]) = 0
  BEGIN
      INSERT INTO [dbo].[tCompany_SMSSetting]
                  ([ID_Company],
                   [MaxSMSCountPerDay],
                   [IsActive],
                   [DateCreated],
                   [DateModified])
      SELECT ID_Company,
             35 [MaxSMSCount],
             1  [IsActive],
             DateCreated,
             DateModified
      FROM   tSMSPatientSOAP_Company
  END 
