Go

exec dbo._pAddModelProperty
  'tPatientAppointment',
  'DateSent',
  5

exec dbo._pAddModelProperty
  'tPatientAppointment',
  'IsSentSMS',
  4

GO

IF OBJECT_ID('dbo.tPatientAppointment_SMSStatus') IS NULL
  BEGIN
      exec _pCreateTable
        'tPatientAppointment_SMSStatus'
  END

GO

IF (SELECT COUNT(*)
    FROM   _tModel
    WHERE  TableName = 'tPatientAppointment_SMSStatus') = 0
  BEGIN
      exec _pCreateAppModule
        'tPatientAppointment_SMSStatus',
        1,
        1
  END

GO

exec _pAddModelProperty
  'tPatientAppointment_SMSStatus',
  'iTextMo_Status',
  2

exec _pAddModelProperty
  'tPatientAppointment_SMSStatus',
  'ID_PatientAppointment',
  2

exec _pAddModelProperty
  'tPatientAppointment_SMSStatus',
  'DateSent',
  5

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vSMSList_PatientAppointment
AS
  SELECT patientAppnt.ID,
         patientAppnt.ID                                                                                                                                                                                              ID_PatientAppointment,
         patientAppnt.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSOAPLANMessage(comp.Name, comp.SOAPPlanSMSMessage, c.Name, ISNULL(comp.ContactNumber, ''), Name_Patient, ISNULL(patientAppnt.Comment, ''), ISNULL(patientAppnt.Comment, ''), patientAppnt.DateStart) Message,
         DATEADD(DAY, -1, patientAppnt.DateStart)                                                                                                                                                                     DateSending,
         patientAppnt.ID_Company,
         ISNULL(patientAppnt.IsSentSMS, 0)                                                                                                                                                                            IsSentSMS,
         model.Oid                                                                                                                                                                                                    Oid_Model
  FROM   vPatientAppointment patientAppnt
         INNER JOIN tClient c
                 on c.ID = patientAppnt.ID_Client
         INNER JOIN tCOmpany comp
                 on comp.ID = patientAppnt.ID_Company,
         _tModel model
  where  model.tableName = 'tPatientAppointment'
         and patientAppnt.ID_FilingStatus NOT IN ( 4 )

GO

CREATE OR
ALTER VIEW [dbo].[vSMSList]
as
  SELECT ID,
         ID              ID_Reference,
         ID_Patient_SOAP Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_SOAP_Plan
  Union All
  SELECT ID,
         ID                     ID_Reference,
         ID_Patient_Vaccination Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Vaccination_Schedule
  UNION ALL
  SELECT ID,
         ID                  ID_Reference,
         ID_Patient_Wellness Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Wellness_DetailSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientWaitingList Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientWaitingList_ReSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientAppointment Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientAppointment

--UNION ALL    
--SELECT ID,    
--       ID                ID_Reference,    
--       ID_BillingInvoice Parent_ID_Reference,    
--       Code,    
--       DateSent,    
--       Name_Client,    
--       Name_Patient,    
--       ContactNumber,    
--       Message,    
--       DateSending,    
--       ID_Company,    
--       IsSentSMS,    
--       Oid_Model    
--FROM   vSMSList_BillingInvoice_SMSPayableRemider    
GO

CREATE OR
ALTER FUNCTION dbo.fGetSOAPLANMessage (@CompanyName        Varchar(MAX),
                                       @SOAPPlanSMSMessage VARCHAR(MAX),
                                       @Client             Varchar(MAX),
                                       @ContactNumber      Varchar(MAX),
                                       @Pet                Varchar(MAX),
                                       @Service            Varchar(MAX),
                                       @Reason             Varchar(MAX),
                                       @DateReturn         DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateReturn, 'M/dd/yyyy ddd')
      Declare @TimeString Varchar(MAX) = FORMAT(@DateReturn, 'hh:mm tt')
      Declare @message Varchar(MAX) = @SOAPPlanSMSMessage

      if( @TimeString = '12:00 AM' )
        begin
            SET @TimeString = '09:00 AM*'
        END

      if( LEN(ISNULL(@Reason, '')) > 0 )
        SET @Reason = '- ' + @Reason

      SET @Pet = ISNULL(@Pet, 'your Pet')
      SET @Client = ISNULL(@Client, '')
      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Pet)))
      SET @message = REPLACE(@message, '/*Service*/', LTRIM(RTRIM(@Service)))
      SET @message = REPLACE(@message, '/*Reason*/', LTRIM(RTRIM(@Reason)))
      SET @message = REPLACE(@message, '/*DateReturn*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))
      SET @message = REPLACE(@message, '/*TimeReturn*/', LTRIM(RTRIM(ISNULL(@TimeString, ''))))
      SET @message = REPLACE(@message, '"', '``')
      SET @message = REPLACE(@message, ' for for ', ' for ')

      RETURN @message
  END

GO

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetSendSoapPlan](@Date             DATETIME,
                                        @IsSMSSent        BIT = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETIME,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETIME,
  DateCreated          DATETIME,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX),
  Count                INT)
AS
  BEGIN
      DECLARE @IDs_Company TYPINTLIST
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientAppointment_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_ID_Model VARCHAR(MAX) =''
      DECLARE @_table TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )

      SELECT @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      SELECT @Patient_Vaccination_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      SELECT @Patient_SOAP_Plan_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      SELECT @Patient_Wellness_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      SELECT @PatientAppointment_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatientAppointment'

      IF( len(Trim(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')

            DELETE FROM @IDs_Company
            WHERE  ID IN (SELECT ID_Company
                          FROM   tCompany_SMSSetting
                          WHERE  IsActive = 0)
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting cSMSSetting
                   INNER JOIN tCompany c
                           on cSMSSetting.ID_Company = c.ID
            WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
                   and c.IsActive = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent TABLE
        (
           IsSMSSent BIT
        )

      IF @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = IsNull(@Date, GetDate())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*Patient Appointment*/
      INSERT @_table
      SELECT c.ID                                                                                                                                                              ID_Company,
             c.NAME                                                                                                                                                            Name_Company,
             client.NAME                                                                                                                                                       Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                ContactNumber_Client,
             vac.DateStart                                                                                                                                                     DateReturn,
             vac.Comment,
             IsNull(vac.Comment, '')                                                                                                                                           Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Comment, IsNull(vac.Comment, ''), vac.DateStart) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vac.DateStart))                                                                                                                    DateSending,
             vac.DateCreated,
             vac.ID                                                                                                                                                            ID_Patient_Vaccination_Schedule,
             @PatientAppointment_ID_Model,
             vac.Code
      FROM   dbo.tPatientAppointment vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vac.IsSentSMS, 0) IN (SELECT IsSMSSent
                                              FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vac.DateStart)) = CONVERT(DATE, @Date) ))

      /*SOAP PLAN */
      INSERT @_table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             IsNull(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, soapPlan.Name_Item, IsNull(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DateAdd(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = ISNULL(patientSOAP.ID_Client, patient.ID_Client)
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     ON patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND IsNull(patientSOAP.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Patient Wellness Schedule*/
      INSERT @_table
      SELECT DISTINCT c.ID                                                                                                                                                                                                                        ID_Company,
                      c.NAME                                                                                                                                                                                                                      Name_Company,
                      client.NAME                                                                                                                                                                                                                 Name_Client,
                      dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                          ContactNumber_Client,
                      wellDetSched.Date,
                      IsNull(wellDetSched.Comment, ''),
                      IsNull(wellDetSched.Comment, ''),
                      dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, IsNull(client.NAME, ''), IsNull(c.ContactNumber, ''), IsNull(patient.NAME, ''), IsNull(wellDetSched.Comment, ''), IsNull(wellDetSched.Comment, ''), wellDetSched.Date) Message,
                      CONVERT(DATE, DateAdd(DAY, -1, wellDetSched.Date))                                                                                                                                                                          DateSending,
                      wellHed.DateCreated,
                      wellDetSched.ID                                                                                                                                                                                                             ID_Patient_Wellness_Schedule,
                      @Patient_Wellness_Schedule_ID_Model,
                      wellHed.Code
      FROM   dbo.vPatient_Wellness_Schedule wellDetSched
             inner JOIN vPatient_Wellness wellHed
                     ON wellHed.ID = wellDetSched.ID_Patient_Wellness
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellHed.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellHed.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellHed.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON c.ID = idsCompany.ID
      WHERE  wellHed.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND IsNull(wellHed.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, wellDetSched.Date)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Vaccination*/
      DECLARE @IDs_Vaccination_ExistingPatientWellness TYPINTLIST

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @_table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             IsNull(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Name_Item, IsNull(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN tPatient_Vaccination_Schedule vacSchedule
                     ON vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
             AND vac.ID NOT IN (SELECT ID
                                FROM   @IDs_Vaccination_ExistingPatientWellness)

      /*Reschedule WaitingList*/
      INSERT @_table
             (ID_Company,
              Name_Company,
              Name_Client,
              ContactNumber_Client,
              DateReturn,
              Name_Item,
              Comment,
              Message,
              DateSending,
              DateCreated,
              ID_Reference,
              Oid_Model,
              Code)
      SELECT hed.ID_Company,
             company.Name,
             client.Name,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),
             hed.DateCreated,
             '',
             '',
             dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, company.Name, company.ContactNumber, hed.DateReschedule, client.Name, patient.Name, _appointment.ReferenceCode, _appointment.Description),
             hed.DateCreated,
             hed.DateCreated,
             hed.ID,
             @PatientWaitingList_ID_Model,
             hed.Code
      FROM   tPatientWaitingList hed
             INNER JOIN tCompany_SMSSetting smsSetting
                     on smsSetting.ID_Company = hed.ID_Company
             INNER JOIN tCompany company
                     on company.ID = hed.ID_Company
             inner join tClient client
                     on hed.ID_Client = client.ID
             inner join tPatient patient
                     on hed.ID_Patient = patient.ID
             LEFT join vAppointmentEvent _appointment
                    on hed.ID_Reference = _appointment.Appointment_ID_CurrentObject
                       and hed.Oid_Model_Reference = _appointment.Oid_Model
             INNER JOIN @IDs_Company idsCompany
                     ON company.ID = idsCompany.ID
      WHERE  --CONVERT(Date, DateAdd(Day, smsSetting.DayInterval, hed.DateCreated)) = CONVERT(Date, @Date)        
        (( CONVERT(DATE, hed.DateCreated) = CONVERT(DATE, GETDATE()) ))
        AND hed.WaitingStatus_ID_FilingStatus IN ( 21 )
        AND IsNull(hed.IsSentSMS, 0) IN (SELECT IsSMSSent
                                         FROM   @SMSSent)
        AND IsNull(hed.ID_CLient, 0) > 0
        AND IsNull(patient.IsDeceased, 0) = 0

      --/*BillingInvoice SMSPayableRemider*/    
      --INSERT @_table    
      --       (ID_Company,    
      --        Name_Company,    
      --        Name_Client,    
      --        ContactNumber_Client,    
      --        DateReturn,    
      --        Name_Item,    
      --        Comment,    
      --        Message,    
      --        DateSending,    
      --        DateCreated,    
      --        ID_Reference,    
      --        Oid_Model,    
      --        Code)    
      --SELECT biHed.ID_Company,    
      --       company.Name,    
      --       client.Name,    
      --       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),    
      --       bSMSReminder.DateSchedule,    
      --       '',    
      --       '',    
      --       dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, company.ContactNumber, biHed.Code, biHed.Date, client.Name, ISNULL(bSMSReminder.RemainingAmount_BillingInvoice, 0)),    
      --       biHed.Date,    
      --       bSMSReminder.DateCreated,    
      --       bSMSReminder.ID,    
      --       @BillingInvoice_SMSPayableRemider_ID_Model,    
      --       bihed.Code    
      --FROM   vBillingInvoice biHed    
      --       INNER JOIN tBillingInvoice_SMSPayableRemider bSMSReminder    
      --               on biHed.ID = bSMSReminder.ID_BillingInvoice    
      --       INNER JOIN tCompany_SMSSetting smsSetting    
      --               on smsSetting.ID_Company = bSMSReminder.ID_Company    
      --       INNER JOIN tCompany company    
      --               on company.ID = bSMSReminder.ID_Company    
      --       inner join tClient client    
      --               on bSMSReminder.ID_Client = client.ID    
      --       INNER JOIN @IDs_Company idsCompany    
      --               ON company.ID = idsCompany.ID    
      --WHERE  CONVERT(Date, bSMSReminder.dateSchedule) = CONVERT(Date, GETDATE())    
      --       AND ISNULL(bSMSReminder.IsActive, 0) = 1    
      --       AND biHed.ID_FilingStatus = 3    
      --       AND Payment_ID_FilingStatus IN ( 2, 11 )    
      --       AND ISNULL(RemainingAmount, 0) > 0    
      --       AND ISNULL(smsSetting.IsEnableBillingInvoiceNotificationSending, 0) = 1    
      DELETE @_table
      FROM   @_table forSMSSending
             inner join tInactiveSMSSending inactiveSendingSchedule
                     on forSMSSending.ID_Company = inactiveSendingSchedule.ID_Company
                        and CONVERT(Date, forSMSSending.DateSending) = CONVERT(Date, inactiveSendingSchedule.Date)
      WHERE  inactiveSendingSchedule.IsActive = 1

      INSERT @table
      SELECT ID_Company,
             Name_Company,
             Name_Client,
             ContactNumber_Client,
             DateReturn,
             Name_Item,
             Comment,
             Message,
             MAX(DateSending),
             MAX(DateCreated),
             MAX(ID_Reference),
             Oid_Model,
             MAX(Code),
             Count(*)
      FROM   @_table
      GROUP  BY ID_Company,
                Name_Company,
                Name_Client,
                ContactNumber_Client,
                DateReturn,
                Name_Item,
                Comment,
                Message,
                Oid_Model

      Update @table
      set    Comment = REPLACE(Comment, '"', '``')

      Update @table
      set    Name_Item = REPLACE(Name_Item, '"', '``')

      RETURN
  END

GO

GO

CREATE   or
alter PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT,
                                        @DateSent       DateTime)
AS
  BEGIN
      /*                    
       iTextMo Status                    
                         
       "1" = Invalid Number.                    
       "2" = Number prefix not supported. Please contact us so we can add.                    
       "3" = Invalid ApiCode.                    
       "4" = Maximum Message per day reached. This will be reset every 12MN.                    
       "5" = Maximum allowed characters for message reached.                    
       "6" = System OFFLINE.                    
       "7" = Expired ApiCode.                    
       "8" = iTexMo Error. Please try again later.                    
       "9" = Invalid Function Parameters.                    
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.                    
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.                    
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.                    
       "13" = Invalid or Not Registered Custom Sender ID.                    
       "14" = Invalid preferred server number.                    
       "15" = IP Filtering enabled - Invalid IP.                    
       "16" = Authentication error. Contact support at support@itexmo.com                    
       "17" = Telco Error. Contact Support support@itexmo.com                    
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com                    
       "19" = Account suspended. Contact Support support@itexmo.com                    
       "0" = Success! Message is now on queue and will be sent soon                   
      "-1" = Reach VetCloud SMS Count Limit                   
      "20" = Manual Sent SMS                
                    
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientAppointment_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1

      IF @DateSent IS NULL
        SET @DateSent = GETDATE()

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      select @PatientWaitingList_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatientWaitingList'

      SELECT @PatientAppointment_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatientAppointment'

      select @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientWaitingList_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientWaitingList
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientWaitingList_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientWaitingList,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @BillingInvoice_SMSPayableRemider_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tBillingInvoice_SMSPayableRemider
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tBillingInvoice_SMSPayableRemider_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_BillingInvoice_SMSPayableRemider,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientAppointment_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientAppointment
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientAppointment_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientAppointment,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO 
