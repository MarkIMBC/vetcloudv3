USE [SystemManagement]

GO

/****** Object:  StoredProcedure [dbo].[pReUpdateInventoryTrail_VetCloudv3]    Script Date: 05/11/2023 9:19:52 AM ******/
SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

ALTER PROC [dbo].[pReUpdateInventoryTrail_VetCloudv3]
AS
  BEGIN
      DECLARE @databases TABLE
        (
           ID           int NOT NULL identity(1, 1),
           DatabaseName VARCHAR(MAX)
        )

      INSERT @databases
      SELECT name
      FROM   MASTER.dbo.sysdatabases
      WHERE  ( name LIKE 'db_vetcloudv3_server_'
                OR name LIKE 'db_vetcloudv3_%' )

      DECLARE @Counter INT
      DECLARE @MaxCounter INT = 0

      SELECT @MaxCounter = COUNT(*)
      FROM   @databases

      SET @Counter=1

      WHILE ( @Counter <= @MaxCounter )
        BEGIN
            DECLARE @databaseName VARCHAR(MAX) = ''

            SELECT @databaseName = DatabaseName
            FROM   @databases
            WHERE  ID = @Counter

            BEGIN TRY
                SELECT @databaseName

                DECLARE @Sql VARCHAR(MAX) = '
			      UPDATE ' + @databaseName
                  + '.dbo.vActiveItem  
				  SET    CurrentInventoryCount = ISNULL(inventory.Qty, 0),  
						 ID_InventoryStatus = '
                  + @databaseName + '.dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount, 0), ISNULL(MaxInventoryCount, 0))  
				  FROM   ' + @databaseName
                  + '.dbo.vActiveItem item  
						 LEFT JOIN ' + @databaseName
                  + '.dbo.vInventoryTrailTotal inventory 
								ON inventory.ID_Item = item.ID;  '

                exec( @Sql)
            END TRY
            BEGIN CATCH
                SELECT @databaseName,
                       ERROR_MESSAGE()
            END CATCH

            SET @Counter = @Counter + 1
        END
  END

go

