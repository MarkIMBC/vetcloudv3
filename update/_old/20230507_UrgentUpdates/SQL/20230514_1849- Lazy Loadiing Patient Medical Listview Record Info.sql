GO

CREATE OR
ALTER VIEW vPatient_SOAP_ListView
AS
  SELECT H.ID,
         H.ID_Company,
         H.ID_SOAPType,
         H.ID_FilingStatus,
         H.Date,
         H.Code,
         soapType.Name                                      Name_SOAPType,
         attendingPhysicianEmloyee.Name                     AttendingPhysician_Name_Employee,
         H.ID_Client,
         client.Name                                        Name_Client,
         patient.Name                                       Name_Patient,
         fs.Name                                            Name_FilingStatus,
         H.ID_Patient,
         H.ID_Patient_Confinement,
         CaseType,
         IsNull(History, '')                                History,
         IsNull(ClinicalExamination, '')                    ClinicalExamination,
         IsNull(Interpretation, '')                         Interpretation,
         IsNull(Diagnosis, '')                              Diagnosis,
         IsNull(Treatment, '')                              Treatment,
         IsNull(ClientCommunication, '')                    ClientCommunication,
         IsNull(Prescription, '')                           Prescription,
         len(Trim(IsNull(History, '')))                     Count_History,
         len(Trim(IsNull(ClinicalExamination, '')))         Count_ClinicalExamination,
         0 + len(Trim(IsNull(Interpretation, '')))          Count_LaboratoryImages,
         len(Trim(IsNull(Diagnosis, '')))                   Count_Diagnosis,
         len(Trim(IsNull(Treatment, '')))                   Count_Treatment,
         len(Trim(IsNull(ClientCommunication, '')))         Count_ClientCommunication,
         0                                                  Count_Plan,
         0 + len(Trim(IsNull(H.Prescription, '')))          Count_Prescription,
         H.DateCreated,
         H.DateModified,
         'Date Created: '
         + Format(H.DateCreated, 'MMM. dd, yyyy hh:mm tt')
         + Char(13) + 'Date Modified: '
         + Format(H.DateModified, 'MMM. dd, yyyy hh:mm tt') TooltipText,
         H.Comment
  FROM   dbo.tPatient_SOAP H WITH (NOLOCK)
         LEFT JOIN dbo.tPatient patient WITH (NOLOCK)
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client WITH (NOLOCK)
                ON patient.ID_Client = client.ID
         LEFT JOIN dbo.tSOAPType soapType WITH (NOLOCK)
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee WITH (NOLOCK)
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tFilingStatus fs WITH (NOLOCK)
                ON fs.ID = H.ID_FilingStatus
         INNER JOIN dbo.tCompany company WITH (NOLOCK)
                 ON H.ID_Company = company.ID
  WHERE  ISNULL(H.ID_FilingStatus, 0) NOT IN ( 0 )
         and company.IsActive = 1

GO

GO

CREATE OR
ALTER PROC pGet_Patient_SOAP_RecordInfo_Plan(@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @___detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @___detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT _plan.ID_Patient_SOAP,
             TagString = STUFF((SELECT '' + 'Date Return: '
                                       + FORMAT(DateReturn, 'MM/dd/yyyy (ddd) ')
                                       + '/*break*/ '
                                       + CASE
                                           WHEN LEN(ISNULL(__plan.Comment, '')) > 0 THEN __plan.Comment
                                           ELSE item.Name
                                         END
                                       + ' ' + '/*break*/ ' + '/*break*/ '
                                FROM   tPatient_SOAP_Plan __plan WITH (NOLOCK)
                                       INNER JOIN tItem item
                                               on __plan.ID_Item = item.ID
                                WHERE  __plan.ID_Patient_SOAP = _plan.ID_Patient_SOAP
                                Order  by DateReturn DESC
                                FOR XML PATH ('')), 1, 0, ''),
             COUNT(*)  Count
      FROM   tPatient_SOAP_Plan _plan WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _plan.ID_Patient_SOAP = ids.ID
      GROUP  BY _plan.ID_Patient_SOAP

      Update @detail
      set    TagString = det2.TagString,
             Count = det2.Count
      FROM   @detail det1
             INNER JOIN @___detail det2
                     on det1.ID_Patient_SOAP = det2.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO

CREATE OR
ALTER PROC pGet_Patient_SOAP_RecordInfo_LaboratoryImages(@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @LaboratoryImageCounter TABLE
        (
           ID_Patient_SOAP INT,
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @LaboratoryImageCounter
             (ID_Patient_SOAP,
              Count)
      SELECT _soapimage.ID_Patient_SOAP,
             COUNT(*) LaboratoryImages
      FROM   vPatient_SOAP_LaboratoryImages _soapimage WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _soapimage.ID_Patient_SOAP = ids.ID
      GROUP  BY _soapimage.ID_Patient_SOAP

      UPDATE @detail
      SET    TagString = CASE
                           WHEN LEN(ISNULL(_soap.Interpretation, '')) > 0 THEN 'Interpretation: '
                                                                               + ISNULL(_soap.Interpretation, '')
                           ELSE ''
                         END
      FROM   @detail _detail
             inner JOIN tPatient_SOAP _soap
                     on _detail.ID_Patient_SOAP = _soap.ID

      UPDATE @detail
      SET    TagString = TagString
                         + CASE
                             WHEN LEN(TagString) > 0 THEN '/*break*/ '
                             ELSE ''
                           END
                         + CASE
                             WHEN LEN(_labImageCounter.Count) > 0 THEN 'Laboratory Count: '
                                                                       + FORMAT(_labImageCounter.Count, '#,#0')
                                                                       + '/*break*/ '
                             ELSE ''
                           END,
             Count = _labImageCounter.Count
      FROM   @detail _detail
             inner JOIN @LaboratoryImageCounter _labImageCounter
                     on _detail.ID_Patient_SOAP = _labImageCounter.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO

GO

CREATE OR
ALTER PROC pGet_Patient_SOAP_RecordInfo_Prescription(@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @___detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @___detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT _prescription.ID_Patient_SOAP,
             TagString = STUFF((SELECT ''
                                       + Case
                                           WHEN item.ID_ItemType = 1 THEN 'Service: '
                                           ELSE
                                             Case
                                               WHEN item.ID_ItemType = 2 THEN 'Item: '
                                               ELSE 'Other: '
                                             END
                                         END
                                       + ISNULL(item.Name, '') + '/*break*/ '
                                       + 'Quantity: ' + FORMAT(Quantity, '#,#0')
                                       + '/*break*/ '
                                       + CASE
                                           WHEN LEN(ISNULL(__prescription.Comment, '')) > 0 THEN 'Reason: ' + __prescription.Comment
                                           ELSE ''
                                         END
                                       + ' ' + '/*break*/ ' + '/*break*/ '
                                FROM   tPatient_SOAP_Prescription __prescription WITH (NOLOCK)
                                       INNER JOIN tItem item WITH (NOLOCK)
                                               on item.ID = __prescription.ID_Item
                                WHERE  _prescription.ID_Patient_SOAP = __prescription.ID_Patient_SOAP
                                Order  by __prescription.ID ASC
                                FOR XML PATH ('')), 1, 0, ''),
             COUNT(*)  Count
      FROM   tPatient_SOAP_Prescription _prescription WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _prescription.ID_Patient_SOAP = ids.ID
      GROUP  BY _prescription.ID_Patient_SOAP

	        UPDATE @detail
      SET    TagString = CASE
                           WHEN LEN(ISNULL(_soap.Interpretation, '')) > 0 THEN 'Interpretation: '
                                                                               + ISNULL(_soap.Interpretation, '')
                           ELSE ''
                         END
      FROM   @detail _detail
             inner JOIN tPatient_SOAP _soap
                     on _detail.ID_Patient_SOAP = _soap.ID

	  

	  UPDATE @detail
      SET    TagString = CASE
                           WHEN LEN(ISNULL(_soap.Prescription, '')) > 0 THEN 'Prescription: '
                                                                               + ISNULL(_soap.Prescription, '')
                           ELSE ''
                         END
      FROM   @detail _detail
             inner JOIN tPatient_SOAP _soap
                     on _detail.ID_Patient_SOAP = _soap.ID

      Update @detail
      set    TagString = det2.TagString,
             Count = det2.Count
      FROM   @detail det1
             INNER JOIN @___detail det2
                     on det1.ID_Patient_SOAP = det2.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO

CREATE OR
ALTER PROC pGet_Patient_SOAP_RecordInfo_Treatment(@IDs_Patient_SOAP typIntList READONLY)
as
  BEGIN
      DECLARE @detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )
      DECLARE @___detail TABLE
        (
           ID_Patient_SOAP INT,
           TagString       VARCHAR(MAX),
           Count           INT
        )

      INSERT @detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT ID,
             '',
             0
      FROM   @IDs_Patient_SOAP

      INSERT @___detail
             (ID_Patient_SOAP,
              TagString,
              Count)
      SELECT _treatment.ID_Patient_SOAP,
             TagString = STUFF((SELECT ''
                                       + Case
                                           WHEN item.ID_ItemType = 1 THEN 'Service: '
                                           ELSE
                                             Case
                                               WHEN item.ID_ItemType = 2 THEN 'Item: '
                                               ELSE 'Other: '
                                             END
                                         END
                                       + ISNULL(item.Name, '') + '/*break*/ '
                                       + 'Quantity: ' + FORMAT(Quantity, '#,#0')
                                       + '/*break*/ '
                                       + CASE
                                           WHEN LEN(ISNULL(__treatment.Comment, '')) > 0 THEN 'Reason: ' + __treatment.Comment
                                           ELSE ''
                                         END
                                       + ' ' + '/*break*/ ' + '/*break*/ '
                                FROM   tPatient_SOAP_Treatment __treatment WITH (NOLOCK)
                                       INNER JOIN tItem item WITH (NOLOCK)
                                               on item.ID = __treatment.ID_Item
                                WHERE  _treatment.ID_Patient_SOAP = __treatment.ID_Patient_SOAP
                                Order  by __treatment.ID ASC
                                FOR XML PATH ('')), 1, 0, ''),
             COUNT(*)  Count
      FROM   tPatient_SOAP_Treatment _treatment WITH (NOLOCK)
             inner join @IDs_Patient_SOAP ids
                     on _treatment.ID_Patient_SOAP = ids.ID
      GROUP  BY _treatment.ID_Patient_SOAP


	  UPDATE @detail
      SET    TagString = CASE
                           WHEN LEN(ISNULL(_soap.Prescription, '')) > 0 THEN 'Prescription: '
                                                                               + ISNULL(_soap.Prescription, '')
                           ELSE ''
                         END
      FROM   @detail _detail
             inner JOIN tPatient_SOAP _soap
                     on _detail.ID_Patient_SOAP = _soap.ID

      Update @detail
      set    TagString = det2.TagString,
             Count = det2.Count
      FROM   @detail det1
             INNER JOIN @___detail det2
                     on det1.ID_Patient_SOAP = det2.ID_Patient_SOAP

      Update @detail
      SET    Count = ISNULL(Count, 0)

      SELECT '_',
             '' Details;

      SELECT GETDATE() Date

      SELECT *
      FROM   @detail
  END

GO
--DECLARE @IDs_Patient_SOAP typIntList
--INSERT @IDs_Patient_SOAP
--VALUES (1334094),
--       (1334167),
--       (1334181)
--exec pGet_Patient_SOAP_RecordInfo_Plan
--  @IDs_Patient_SOAP
--exec pGet_Patient_SOAP_RecordInfo_LaboratoryImages
--  @IDs_Patient_SOAP
--exec pGet_Patient_SOAP_RecordInfo_Prescription
--  @IDs_Patient_SOAP
--exec pGet_Patient_SOAP_RecordInfo_Treatment
--  @IDs_Patient_SOAP 
