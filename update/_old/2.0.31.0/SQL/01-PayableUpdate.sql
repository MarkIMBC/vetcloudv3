GO

IF COL_LENGTH('dbo.tPayable', 'DateCanceled') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPayable',
        'DateCanceled',
        5
  END

GO

IF COL_LENGTH('dbo.tPayable', 'ID_CanceledBy') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPayable',
        'ID_CanceledBy',
        2
  END

GO

exec _pRefreshAllViews

go

ALTER VIEW vPayable_PayableDetail_Listview
as
  SELECT payable.ID,
         payable.Date,
         payable.ID_Company,
         expensesCat.Name   Name_ExpenseCategory,
         payableDetail.Name Name_Payable_Detail,
         payable.TotalAmount,
         payable.Payment_ID_FilingStatus,
         fs.Name            Payment_Name_FilingStatus,
         payable.RemaningAmount,
         payable.PaidAmount
  FROM   tPayable payable
         INNER JOIN tPayable_Detail payableDetail
                 ON payableDetail.ID_Payable = payable.ID
         LEFT JOIN tExpenseCategory expensesCat
                ON expensesCat.ID = payableDetail.ID_ExpenseCategory
         LEFT JOIN tFilingStatus fs
                ON fs.ID = payable.Payment_ID_FilingStatus
  WHERE  Payment_ID_FilingStatus NOT IN ( 4 )

GO

CREATE OR
ALTER PROC [dbo].[pCancelPayable_validation] (@IDs_Payable    typIntList READONLY,
                                              @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotApproved TABLE
        (
           Name_Payable_Detail VARCHAR(30),
           Name_FilingStatus   VARCHAR(30)
        );
      DECLARE @Count_ValidateNotApproved INT = 0;
      DECLARE @ValidateOnServe TABLE
        (
           Name_Payable_Detail VARCHAR(30),
           Name_FilingStatus   VARCHAR(30)
        );
      DECLARE @Count_ValidateOnServe INT = 0;

      /* Validate Text Blast Status is not Approved*/
      INSERT @ValidateNotApproved
             (Name_Payable_Detail,
              Name_FilingStatus)
      SELECT paymentDetail.Name,
             Payment_Name_FilingStatus
      FROM   dbo.vPayable bi
             inner join tPayable_Detail paymentDetail
                     on bi.ID = paymentDetail.ID_Payable
             inner join @IDs_Payable ids
                     on ids.ID = bi.ID
      WHERE  bi.Payment_ID_FilingStatus NOT IN ( @Pending_ID_FilingStatus );

      SELECT @Count_ValidateNotApproved = COUNT(*)
      FROM   @ValidateNotApproved;

      IF ( @Count_ValidateNotApproved > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotApproved > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to cancel:';

            SELECT @message = @message + CHAR(10) + Name_Payable_Detail + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotApproved;

            THROW 50001, @message, 1;
        END;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancelPayable] (@IDs_Payable    typIntList READONLY,
                                   @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.[pCancelPayable_validation]
            @IDs_Payable,
            @ID_UserSession;

          UPDATE dbo.tPayable
          SET    Payment_ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPayable bi
                 INNER JOIN @IDs_Payable ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 
