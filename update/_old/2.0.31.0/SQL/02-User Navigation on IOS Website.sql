GO

CREATE OR
ALTER PROC pGetUserNavigation (@ID_UserSession INT)
AS
  BEGIN
      Declare @UserModulesNavigation table
        (
           ID_Navigation VARCHAR(MAX)
        )
      Declare @SelectedNavigation table
        (
           tempID      int IDENTITY(1, 1),
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )
      Declare @Navigation table
        (
           tempID      int,
           Oid         VARCHAR(MAX),
           ID_Parent   VARCHAR(MAX),
           Name        VARCHAR(MAX),
           ID_View     VARCHAR(MAX),
           Caption     VARCHAR(MAX),
           Icon        VARCHAR(MAX),
           SeqNo       INT,
           ID_ViewType INT,
           Route       VARCHAR(MAX)
        )
      DECLARE @ID_User INT

      SELECT @ID_User = ID_User
      FROm   tUserSession
      WHERE  ID = @ID_UserSession

      INSERT @UserModulesNavigation
      SELECT ID_Navigation
      FROM   [dbo].fGetUserModules(@ID_User)
      WHERE  ID_Navigation IS NOT NULL

      INSERT @SelectedNavigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route
      FROm   _vNavigation nav
             inner join @UserModulesNavigation userNav
                     on nav.Oid = userNav.ID_Navigation
      WHERE  nav.ID_View IS NULL
              OR nav.IsActive = 1

      INSERT @Navigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route,
              tempID)
      SELECT DISTINCT nav.Oid,
                      nav.ID_Parent,
                      nav.Name,
                      nav.Caption,
                      nav.ID_ViewType,
                      nav.Icon,
                      nav.SeqNo,
                      nav.Route,
                      tempID
      FROm   _vNavigation nav
             INNER JOIN @SelectedNavigation selNav
                     on nav.Oid = selNav.Oid
      WHERE  nav.ID_Parent IS NULL

      INSERT @Navigation
             (Oid,
              ID_Parent,
              Name,
              Caption,
              ID_ViewType,
              Icon,
              SeqNo,
              Route,
              tempID)
      SELECT nav.Oid,
             nav.ID_Parent,
             nav.Name,
             nav.Caption,
             nav.ID_ViewType,
             nav.Icon,
             nav.SeqNo,
             nav.Route,
             MAX(selNav.tempID)
      FROm   _vNavigation nav
             INNER JOIN @SelectedNavigation selNav
                     on nav.Oid = selNav.ID_Parent
      GROUP  BY nav.Oid,
                nav.ID_Parent,
                nav.Name,
                nav.Caption,
                nav.ID_ViewType,
                nav.Icon,
                nav.SeqNo,
                nav.Route

      SELECT ''                         AS _,
             ''                         ParentItem,
             'ParentItem.ID_ParentItem' ChildrenItems

      SELECT 1               ID,
             @ID_UserSession ID_UserSession

      SELECT tempID ID,
             Oid    ID_Parent,
             Name,
             Caption,
             ID_ViewType,
             Icon,
             SeqNo,
             Route
      FROm   @Navigation
      ORder  by SeqNo

      SELECT DISTINCT selNav.tempID                                                 ID,
                      navParent.tempID                                              ID_ParentItem,
                      navParent.Oid                                                 ID_Parent,
                      navParent.Name                                                Name_Parent,
                      selNav.Oid,
                      selNav.Name,
                      ISNULL(selNav.Caption, REPLACE(selNav.Name, '_ListView', '')) Caption,
                      selNav.ID_ViewType,
                      selNav.Icon,
                      navParent.SeqNo,
                      isnull(selNav.SeqNo, 999999999)                               SeqNo,
                      selNav.Route
      FROm   @SelectedNavigation selNav
             INNER JOIN @Navigation navParent
                     on selNav.ID_Parent = navParent.Oid
      ORder  by navParent.SeqNo,
                isnull(selNav.SeqNo, 999999999),
                ISNULL(selNav.Caption, REPLACE(selNav.Name, '_ListView', ''))
  END

GO

UPDATE _tNavigation
SET    SeqNo = 10,
       Route = 'Client'
WHERE  Name = 'Client_ListView'

UPDATE _tNavigation
SET    SeqNo = 200,
       Route = 'Patient'
WHERE  Name = 'Patient_ListView'

UPDATE _tNavigation
SET    SeqNo = 300,
       Route = 'ConfinementList'
WHERE  Name = 'Patient_Confinement_ListView'

UPDATE _tNavigation
SET    SeqNo = 400,
       Route = 'Patient_SOAP'
WHERE  Name = 'PatientSOAPList_ListView'

UPDATE _tNavigation
SET    SeqNo = 500,
       Route = 'PatientWellnessList'
WHERE  Name = 'Patient_Wellness_ListView'

UPDATE _tNavigation
SET    SeqNo = 600,
       Route = 'VeterinaryHealthCertificateList'
WHERE  Name = 'VeterinaryHealthCertificate_ListView'

UPDATE _tNavigation
SET    SeqNo = 2000,
       Route = 'PatientWaitingList'
WHERE  Name = 'PatientWaitingList_ListView'

UPDATE _tNavigation
SET    SeqNo = 50,
       Route = 'ItemInventoriable'
WHERE  Name = 'ItemInventoriable_ListView'

UPDATE _tNavigation
SET    SeqNo = 52,
       Route = 'ItemService'
WHERE  Name = 'ItemService_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = 'PurchaseOrderList'
WHERE  Name = 'PurchaseOrder_ListView'

UPDATE _tNavigation
SET    SeqNo = 200,
       Route = 'ReceivingReportList'
WHERE  Name = 'ReceivingReport_ListView'

UPDATE _tNavigation
SET    SeqNo = 999999999,
       Route = 'SupplierList'
WHERE  Name = 'Supplier_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = 'ForBillingList'
WHERE  Name = 'ForBilling_ListView'

UPDATE _tNavigation
SET    SeqNo = 500,
       Route = 'BillingInvoice'
WHERE  Name = 'BillingInvoice_ListView'

UPDATE _tNavigation
SET    SeqNo = 800,
       Route = 'BillingInvoiceWalkInList'
WHERE  Name = 'BillingInvoiceWalkInList_ListView'

UPDATE _tNavigation
SET    SeqNo = 1200,
       Route = ''
WHERE  Name = 'PaymentTransaction_ListView'

UPDATE _tNavigation
SET    SeqNo = 1500,
       Route = ''
WHERE  Name = 'Payable_ListView'

UPDATE _tNavigation
SET    SeqNo = 50,
       Caption = 'Employee',
       Route = 'Employee'
WHERE  Name = 'Emplyee_ListView'

UPDATE _tNavigation
SET    SeqNo = 10,
       Route = 'ReportInvoiceItemsAndServices'
WHERE  Name = 'POSSummary_Navigation'

UPDATE _tNavigation
SET    SeqNo = 20,
       Route = 'ReportBillingInvoicePaidList'
WHERE  Name = 'BillingInvoicePaidListReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 30,
       Route = 'ReportPaymentTransactionDetail'
WHERE  Name = 'PaymentTransactionDetailReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 40,
       Route = 'ReportInventorySummary'
WHERE  Name = 'InventorySummaryReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 50,
       Route = 'ReportInventoryDetail'
WHERE  Name = 'InventoryDetailReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 60,
       Route = 'ReportSalesIncome'
WHERE  Name = 'SalesIncomeReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 70,
       Route = ''
WHERE  Name = 'ReceivedDepositCredits_Navigation'

UPDATE _tNavigation
SET    SeqNo = 80,
       Route = 'ReportBillingInvoiceAging'
WHERE  Name = 'BillingInvoiceAgingReport_Navigation'

UPDATE _tNavigation
SET    SeqNo = 999999999,
       Route = ''
WHERE  Name = 'TextBlast_ListView'

UPDATE _tNavigation
SET    SeqNo = 100,
       Route = ''
WHERE  Name = 'EmployeeInfo_ListView'

UPDATE _tNavigation
SET    SeqNo = 2000,
       Route = ''
WHERE  Name = 'CompanyInfo_ListView'

UPDATE _tNavigation
SET    SeqNo = 90,
       Route = 'ScheduleList'
WHERE  Name = 'Schedule_ListView'

Update _tNavigation
SET    Route = null
where  Route = ''

GO

exec pGetUserNavigation
  60792 
