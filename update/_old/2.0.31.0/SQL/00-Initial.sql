GO

IF COL_LENGTH('dbo.tClientDeposit', 'ID_Patient') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tClientDeposit',
        'ID_Patient',
        2
  END

GO

IF COL_LENGTH('dbo.tClientWithdraw', 'ID_Patient') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tClientWithdraw',
        'ID_Patient',
        2
  END

GO

CREATE   OR
ALTER VIEW vClientWithdraw
AS
  SELECT H.*,
         confinement.Name_Client,
         patient.Name      Name_Patient,
         UC.Name           AS CreatedBy,
         UM.Name           AS LastModifiedBy,
         approvedUser.Name AS ApprovedBy_Name_User,
         cancelUser.Name   AS CanceledBy_Name_User,
         fs.Name           Name_FilingStatus,
         confinement.Code  Code_Patient_Confinement
  FROM   tClientWithdraw H
         LEFT JOIN vPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.ID_FilingStatus
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN tPatient patient
                ON patient.ID = h.ID_Patient

GO

CREATE OR
ALTER VIEW vClientDeposit
AS
  SELECT H.*,
         ISNULL(confinement.Name_Client, client.Name) Name_Client,
         patient.Name                                 Name_Patient,
         UC.Name                                      AS CreatedBy,
         UM.Name                                      AS LastModifiedBy,
         approvedUser.Name                            AS ApprovedBy_Name_User,
         cancelUser.Name                              AS CanceledBy_Name_User,
         fs.Name                                      Name_FilingStatus,
         confinement.Code                             Code_Patient_Confinement
  FROM   tClientDeposit H
         LEFT JOIN vPatient_Confinement confinement
                ON H.ID_Patient_Confinement = confinement.ID
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFilingStatus fs
                ON fs.ID = h.ID_FilingStatus
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN tClient client
                ON client.ID = h.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = h.ID_Patient

GO

ALTER VIEW vClient_CreditLogs
AS
  SELECT H.*,
         UC.Name                                                               AS CreatedBy,
         UM.Name                                                               AS LastModifiedBy,
         ISNULL(clientDeposit.Name_Patient, clientWithdraw.Name_Patient)       Name_Patient,
         ISNULL(clientDeposit.ID_FilingStatus, clientWithdraw.ID_FilingStatus) ID_FilingStatus,
         ISNULL(clientDeposit.Name_FilingStatus, clientWithdraw.Name_FilingStatus) Name_FilingStatus
  FROM   tClient_CreditLogs H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN vClientDeposit clientDeposit
                on clientDeposit.ID_Company = h.ID_Company
                   and clientDeposit.ID_Client = h.ID_Client
                   and clientDeposit.Code = h.Code
         LEFT JOIN vClientWithdraw clientWithdraw
                on clientWithdraw.ID_Company = h.ID_Company
                   and clientWithdraw.ID_Client = h.ID_Client
                   and clientWithdraw.Code = h.Code

GO

CREATE   OR
ALTER VIEW vClientCredit_ListView
AS
  SELECT creditLogs.ID,
         creditLogs.Code,
         creditLogs.ID_Client,
         client.Name Name_Client,
         creditLogs.ID_Patient,
         creditLogs.Name_Patient,
         creditLogs.Date,
         creditLogs.Comment,
         creditLogs.CreditAmount,
         creditLogs.ID_FilingStatus,
         creditLogs.Name_FilingStatus,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) > 0 THEN creditLogs.CreditAmount
           ELSE 0
         END         DepositAmount,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) < 0 THEN abs(creditLogs.CreditAmount)
           ELSE 0
         END         WithdrawAmount
  FROM   vClient_CreditLogs creditLogs
         INNER JOIN tClient client
                 ON client.ID = creditLogs.ID_Client

GO 
GO

CREATE   OR
ALTER PROC [dbo].[pGetConfinmentDeposit](@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @CurrentCreditAmount Decimal(18, 4)= 0
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @RemainingDepositAmount Decimal(18, 4)= 0
      DECLARE @ID_Client INT = 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @MaxDate_ClientDeposit DateTime
      DECLARE @ClientDeposit TABLE
        (
           ID            INT,
           Date          DateTIme,
           Code          VARCHAR(50),
           Name_Patient  VARCHAR(MAX),
           DepositAmount Decimal(18, 4)
        )

      SELECT @ID_Client = ID_Client
      FROM   tPatient_Confinement
      where  ID = @ID_Patient_Confinement

      SELECT @CurrentCreditAmount = client.CurrentCreditAmount
      FROM   tclient client
      WHERE  ID = @ID_Client

      SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement);
      SET @RemainingDepositAmount = ISNULL(@CurrentCreditAmount, 0) - ISNULL(@ConfinementDepositAmount, 0)

      if( @RemainingDepositAmount < 0 )
        SET @RemainingDepositAmount = 0

      SELECT @MaxDate_ClientDeposit = MAX(Date)
      FROm   tClient_CreditLogs
      where  ID_Client = @ID_Client
             AND Code NOT IN (SELECT Code
                              FROM   @ClientDeposit)

      INSERT @ClientDeposit
             (ID,
              Date,
              Code,
              Name_Patient,
              DepositAmount)
      SELECT NULL,
             @MaxDate_ClientDeposit,
             'Remaining',
             '',
             @RemainingDepositAmount

      INSERT @ClientDeposit
             (ID,
              Date,
              Code,
              Name_Patient,
              DepositAmount)
      SELECT ID,
             Date,
             Code,
             Name_Patient,
             DepositAmount
      FROM   dbo.vClientDeposit
      WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )

      SELECT '_',
             '' AS ClientDeposits;

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             @RemainingDepositAmount              RemainingDepositAmount,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount,
             ISNULL(@CurrentCreditAmount, 0)      TotalDepositAmount;

      SELECT *
      FROM   @ClientDeposit
  END 
GO
exec _pRefreshAllViews