EXEC _pAddModelProperty
  'tBillingInvoice',
  'InitialConfinementDepositAmount',
  3

GO

EXEC _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vBillingInvoice_ListView
as
  SELECT ID,
         Date,
         Code,
         Name_Client,
         Name_Patient,
         PatientNames,
         GrossAmount,
         VatAmount,
         NetAmount,
         SubTotal,
         DiscountAmount,
         TotalAmount,
         RemainingAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         AttendingPhysician_Name_Employee,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Payment_ID_FilingStatus,
         Name_FilingStatus,
         Payment_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         ID_Company
  FROM   dbo.vBillingInvoice
  WHERE  ISNULL(IsWalkIn, 0) = 0
         AND ID_FilingStatus NOT IN ( 4 )

GO

CREATE OR
ALTER VIEW vBillingInvoice_Walkin_ListView
as
  SELECT ID,
         Date,
         Code,
         Name_Client,
         WalkInCustomerName,
         Name_Patient,
         PatientNames,
         GrossAmount,
         VatAmount,
         NetAmount,
         SubTotal,
         DiscountAmount,
         TotalAmount,
         RemainingAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         AttendingPhysician_Name_Employee,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Payment_ID_FilingStatus,
         Name_FilingStatus,
         Payment_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
         ID_Company
  FROM   dbo.vBillingInvoice
  WHERE  ISNULL(IsWalkIn, 0) = 1
         AND ID_FilingStatus NOT IN ( 4 )

GO

CREATE OR
ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @ID_Patient_Vaccination         INT = NULL,
                                      @ID_Patient_Wellness            INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL,
                                      @IsWalkIn                       BIT = 0
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @InitialConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        SET @ID_Patient = NULL

      IF ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Wellness, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_Wellness
            WHERE  ID = @ID_Patient_Wellness
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = ISNULL(CurrentCreditAmount, 0)
            FROM   tPatient_Confinement confinement
                   INNER JOIN tClient client
                           ON confinement.ID_Client = client.ID
            WHERE  confinement.ID = @ID_Patient_Confinement

            SET @InitialConfinementDepositAmount = @ConfinementDepositAmount
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                          Name_FilingStatus,
                   client.Name                      Name_Client,
                   patient.Name                     Name_Patient,
                   client.Address                   BillingAddress,
                   attendingPhysicianEmloyee.Name   AttendingPhysician_Name_Employee,
                   @InitialConfinementDepositAmount InitialConfinementDepositAmount,
                   @ConfinementDepositAmount        ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GetDate()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement,
                           @ID_Patient_Vaccination         ID_Patient_Vaccination,
                           @ID_Patient_Wellness            ID_Patient_Wellness,
                           CASE
                             WHEN @IsWalkIn = 1 THEN 'Walk In Customer'
                             ELSE ''
                           END                             WalkInCustomerName,
                           @IsWalkIn                       IsWalkIn) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      IF ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapTreatment.ID DESC) ) - 799999                    ID,
                   soapTreatment.ID_Item,
                   soapTreatment.Name_Item,
                   ISNULL(soapTreatment.Quantity, 0)                                   Quantity,
                   ISNULL(soapTreatment.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapTreatment.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapTreatment.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Treatment soapTreatment
                   INNER JOIN tItem item
                           ON item.ID = soapTreatment.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
            UNION ALL
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999                    ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                                   Quantity,
                   ISNULL(soapPrescription.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapPrescription.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapPrescription.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
                   AND soapPrescription.IsActive = 1
                   AND soapPrescription.IsCharged = 1
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(itemsservices.UnitPrice, 0)               UnitPrice,
                   ISNULL(itemsservices.UnitCost, 0)                UnitCost,
                   itemsservices.DateExpiration                     DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
        END
      ELSE IF ISNULL(@ID_Patient_Wellness, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY wellnessdetail.ID DESC) ) - 999999 ID,
                   wellnessdetail.ID_Item,
                   wellnessdetail.Name_Item,
                   ISNULL(wellnessdetail.Quantity, 1)                Quantity,
                   ISNULL(wellnessdetail.UnitPrice, 0)               UnitPrice,
                   ISNULL(wellnessdetail.UnitCost, 0)                UnitCost,
                   wellnessdetail.DateExpiration                     DateExpiration,
                   wellnessdetail.ID                                 ID_Patient_Wellness_Detail
            FROM   dbo.vPatient_Wellness_Detail wellnessdetail
                   INNER JOIN tItem item
                           ON item.ID = wellnessdetail.ID_Item
            WHERE  ID_Patient_Wellness = @ID_Patient_Wellness
        END
      ELSE IF ISNULL(@ID_Patient_Vaccination, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY vaccination.ID DESC) ) - 999999 ID,
                   vaccination.ID_Item,
                   vaccination.Name_Item,
                   1                                              Quantity,
                   ISNULL(vaccination.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(vaccination.UnitCost, item.UnitCost)    UnitCost,
                   vaccination.DateExpiration                     DateExpiration,
                   vaccination.ID                                 ID_Patient_Vaccination
            FROM   dbo.vPatient_Vaccination vaccination
                   INNER JOIN tItem item
                           ON item.ID = vaccination.ID_Item
            WHERE  vaccination.ID = @ID_Patient_Vaccination
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Detail
            WHERE  ID_BillingInvoice = @ID;
        END

      IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID_Patient,
                   Name_Patient,
                   ID                                 ID_Patient_Confinement_Patient
            FROM   vPatient_Confinement_Patient
            WHERE  ID_Patient_Confinement IN ( @ID_Patient_Confinement )
            ORDER  BY ID DESC
        END
      ELSE IF ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID;
        END
  END;

GO

Update tBillingInvoice
SET    InitialConfinementDepositAmount = ConfinementDepositAmount
WHERE  ConfinementDepositAmount > 0 and ISNULL(InitialConfinementDepositAmount, 0) = 0
