GO

EXEC _paddmodelproperty
  'tPatient_Wellness',
  'BillingInvoice_ID_FilingStatus',
  2

EXEC _paddmodelproperty
  'tPatient_Wellness',
  'IsForBilling',
  4

EXEC _paddmodelproperty
  'tPatient_Grooming',
  'BillingInvoice_ID_FilingStatus',
  2

EXEC _paddmodelproperty
  'tPatient_Grooming',
  'IsForBilling',
  4

GO

EXEC _prefreshallviews

GO

GO

CREATE   OR
ALTER VIEW vPatient_Wellness
AS
  SELECT H.*,
         UC.Name                                                CreatedBy,
         UM.Name                                                LastModifiedBy,
         client.Name                                            Name_Client,
         patient.Name                                           Name_Patient,
         fs.Name                                                Name_FilingStatus,
         Isnull(_AttendingPhysician.Name, H.AttendingPhysician) AttendingPhysician_Name_Employee,
         bifs.Name                                              BillingInvoice_Name_FilingStatus
  FROM   tPatient_Wellness H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tFilingStatus bifs
                ON bifs.ID = H.BillingInvoice_ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee

GO

CREATE   OR
ALTER VIEW vPatient_Grooming
AS
  SELECT H.*,
         UC.Name            AS CreatedBy,
         UM.Name            AS LastModifiedBy,
         _client.Name       Name_Client,
         _patient.Name      Name_Patient,
         _emp.Name          AttendingPhysician_Name_Employee,
         _filingStatus.Name Name_FilingStatus,
         bifs.Name          BillingInvoice_Name_FilingStatus
  FROM   tPatient_Grooming H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee _emp
                ON H.AttendingPhysician_ID_Employee = _emp.ID
         LEFT JOIN tClient _client
                ON H.ID_Client = _client.ID
         LEFT JOIN tPatient _patient
                ON H.ID_Patient = _patient.ID
         LEFT JOIN tFilingStatus _filingStatus
                ON H.ID_FilingStatus = _filingStatus.ID
         LEFT JOIN tFilingStatus bifs
                ON bifs.ID = H.BillingInvoice_ID_FilingStatus

GO

CREATE     OR
ALTER VIEW vForBilling_ListView_temp
AS
  SELECT confinement.ID,
         confinement.Code RefNo,
         confinement.ID   ID_CurrentObject,
         m.Oid            Oid_Model,
         m.Name           Name_Model,
         confinement.Date,
         confinement.ID_Company,
         confinement.ID_Client,
         confinement.ID_Patient,
         confinement.BillingInvoice_ID_FilingStatus,
         confinement.Name_Client,
         confinement.Name_Patient,
         confinement.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Confinement confinement,
         _tModel m
  WHERE  m.TableName = 'tPatient_Confinement'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT soap.ID,
         soap.Code RefNo,
         soap.ID   ID_CurrentObject,
         m.Oid     Oid_Model,
         m.Name    Name_Model,
         soap. Date,
         soap.ID_Company,
         soap.ID_Client,
         soap.ID_Patient,
         soap.BillingInvoice_ID_FilingStatus,
         soap.Name_Client,
         soap.Name_Patient,
         soap.BillingInvoice_Name_FilingStatus
  FROM   vPatient_SOAP soap,
         _tModel m
  WHERE  m.TableName = 'tPatient_SOAP'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
         AND Isnull(soap.ID_Patient_Confinement, '') = 0
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Wellness hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Wellness'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Grooming hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Grooming'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )

GO

GO

CREATE   OR
ALTER PROC Pupdatepatientwellnessbillstatus(@IDs_Patient_Wellness           TYPINTLIST READONLY,
                                            @BillingInvoice_ID_FilingStatus INT,
                                            @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @isForBilling BIT = 0;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT
    DECLARE @ForBilling_BillingInvoice_ID_FilingStatus INT = 16

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    BEGIN TRY
        IF( @isForBilling = @ForBilling_BillingInvoice_ID_FilingStatus )
          BEGIN
              SET @isForBilling = 1
          END

        UPDATE tPatient_Wellness
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus,
               IsForBilling = @isForBilling
        FROM   tPatient_Wellness _hed
               INNER JOIN @IDs_Patient_Wellness ids
                       ON _hed.ID = ids.ID;
    END TRY
    BEGIN CATCH
        SET @message = Error_message();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

GO

CREATE   OR
ALTER PROC Pupdatepatientgroomingbillstatus(@IDs_Patient_Grooming           TYPINTLIST READONLY,
                                            @BillingInvoice_ID_FilingStatus INT,
                                            @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @isForBilling BIT = 0;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT
    DECLARE @ForBilling_BillingInvoice_ID_FilingStatus INT = 16

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    BEGIN TRY
        IF( @isForBilling = @ForBilling_BillingInvoice_ID_FilingStatus )
          BEGIN
              SET @isForBilling = 1
          END

        UPDATE tPatient_Grooming
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus,
               IsForBilling = @isForBilling
        FROM   tPatient_Grooming _hed
               INNER JOIN @IDs_Patient_Grooming ids
                       ON _hed.ID = ids.ID;
    END TRY
    BEGIN CATCH
        SET @message = Error_message();
        SET @Success = 0;
    END CATCH;

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO 
Go

  
CREATE   OR ALTER
 PROC pGetForBillingBillingInvoiceRecords_Patient_SOAP(@IDs_Patient_SOAP        typIntList READONLY,  
                                               @IDs_Patient_Confinement typIntList READONLY)  
as  
    DECLARE @Patient_SOAP_ID_Model VARCHAR(MAX) = ''  
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''  
  
    SELECT @Patient_SOAP_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_SOAP'  
  
    SELECT @Patient_Confinement_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Confinement'  
  
    DECLARE @record TABLE  
      (  
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,  
         [RefNo]            [varchar](50) NULL,  
         [ID_CurrentObject] [int] NOT NULL,  
         [Oid_Model]        [uniqueidentifier] NOT NULL,  
         [1]                VARCHAR(MAX) NULL,  
         [2]                VARCHAR(MAX) NULL,  
         [3]                VARCHAR(MAX) NULL,  
         [4]                VARCHAR(MAX) NULL,  
         [5]                VARCHAR(MAX) NULL,  
         [6]                VARCHAR(MAX) NULL,  
         [7]                VARCHAR(MAX) NULL,  
         [8]                VARCHAR(MAX) NULL,  
         [9]                VARCHAR(MAX) NULL,  
         [10]               VARCHAR(MAX) NULL  
      )  
  
    /*Patient SOAP*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_SOAP_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_SOAP,  
                   Code_Patient_SOAP,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_SOAP,  
                           soap.Code                                            Code_Patient_SOAP,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_SOAP  
                               ORDER BY ID_Patient_SOAP ASC, bi.ID)             BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_SOAP soap  
                                   ON bi.ID_Patient_SOAP = soap.ID  
                           INNER JOIN @IDs_Patient_SOAP ids  
                                   on soap.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_SOAP, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    /*Patient Confinement*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_Confinement_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_Confinement,  
                   Code_Patient_Confinement,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_Confinement,  
                           soap.Code                                            Code_Patient_Confinement,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_Confinement  
                               ORDER BY ID_Patient_Confinement ASC, bi.ID)      BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_Confinement soap  
                                   ON bi.ID_Patient_Confinement = soap.ID  
                           INNER JOIN @IDs_Patient_Confinement ids  
                                   on soap.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_Confinement, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    SELECT '_',  
           '' BillingInvoices  
  
    SELECT GETDate() Date  
  
    SELECT *  
    FROM   @record  
  

GO

Go

  
CREATE   OR ALTER
 PROC pGetForBillingBillingInvoiceRecords_Patient_Wellness(@IDs_Patient_Wellness        typIntList READONLY,  
                                               @IDs_Patient_Confinement typIntList READONLY)  
as  
    DECLARE @Patient_Wellness_ID_Model VARCHAR(MAX) = ''  
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''  
  
    SELECT @Patient_Wellness_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Wellness'  
  
    SELECT @Patient_Confinement_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Confinement'  
  
    DECLARE @record TABLE  
      (  
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,  
         [RefNo]            [varchar](50) NULL,  
         [ID_CurrentObject] [int] NOT NULL,  
         [Oid_Model]        [uniqueidentifier] NOT NULL,  
         [1]                VARCHAR(MAX) NULL,  
         [2]                VARCHAR(MAX) NULL,  
         [3]                VARCHAR(MAX) NULL,  
         [4]                VARCHAR(MAX) NULL,  
         [5]                VARCHAR(MAX) NULL,  
         [6]                VARCHAR(MAX) NULL,  
         [7]                VARCHAR(MAX) NULL,  
         [8]                VARCHAR(MAX) NULL,  
         [9]                VARCHAR(MAX) NULL,  
         [10]               VARCHAR(MAX) NULL  
      )  
  
    /*Patient hed*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_Wellness_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_Wellness,  
                   Code_Patient_Wellness,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_Wellness,  
                           hed.Code                                            Code_Patient_Wellness,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_Wellness  
                               ORDER BY ID_Patient_Wellness ASC, bi.ID)             BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_Wellness hed  
                                   ON bi.ID_Patient_Wellness = hed.ID  
                           INNER JOIN @IDs_Patient_Wellness ids  
                                   on hed.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_Wellness, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    SELECT '_',  
           '' BillingInvoices  
  
    SELECT GETDate() Date  
  
    SELECT *  
    FROM   @record  
  

GO
  
CREATE   OR ALTER
 PROC pGetForBillingBillingInvoiceRecords_Patient_Grooming(@IDs_Patient_Grooming        typIntList READONLY,  
                                               @IDs_Patient_Confinement typIntList READONLY)  
as  
    DECLARE @Patient_Grooming_ID_Model VARCHAR(MAX) = ''  
    DECLARE @Patient_Confinement_ID_Model VARCHAR(MAX) = ''  
  
    SELECT @Patient_Grooming_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Grooming'  
  
    SELECT @Patient_Confinement_ID_Model = Oid  
    FROm   _tModel  
    where  TableName = 'tPatient_Confinement'  
  
    DECLARE @record TABLE  
      (  
         ID                 INT NOT NULL IDENTITY PRIMARY KEY,  
         [RefNo]            [varchar](50) NULL,  
         [ID_CurrentObject] [int] NOT NULL,  
         [Oid_Model]        [uniqueidentifier] NOT NULL,  
         [1]                VARCHAR(MAX) NULL,  
         [2]                VARCHAR(MAX) NULL,  
         [3]                VARCHAR(MAX) NULL,  
         [4]                VARCHAR(MAX) NULL,  
         [5]                VARCHAR(MAX) NULL,  
         [6]                VARCHAR(MAX) NULL,  
         [7]                VARCHAR(MAX) NULL,  
         [8]                VARCHAR(MAX) NULL,  
         [9]                VARCHAR(MAX) NULL,  
         [10]               VARCHAR(MAX) NULL  
      )  
  
    /*Patient hed*/  
    INSERT @record  
           ([Oid_Model],  
            [ID_CurrentObject],  
            [RefNo],  
            [1],  
            [2],  
            [3],  
            [4],  
            [5],  
            [6],  
            [7],  
            [8],  
            [9],  
            [10])  
    SELECT @Patient_Grooming_ID_Model,  
           *  
    FROM   (SELECT ID_Patient_Grooming,  
                   Code_Patient_Grooming,  
                   BillingInvoice_RowID,  
                   Info_BillingInvoice  
            FROM   (SELECT bi.ID_Patient_Grooming,  
                           hed.Code                                            Code_Patient_Grooming,  
                           ROW_NUMBER()  
                             OVER(  
                               PARTITION BY bi.ID_Patient_Grooming  
                               ORDER BY ID_Patient_Grooming ASC, bi.ID)             BillingInvoice_RowID,  
                           format(bi.ID, '0') + '|' + bi.Code + '|' + bi.Status Info_BillingInvoice  
                    FROM   vBillingINvoice bi  
                           inner JOIN tPatient_Grooming hed  
                                   ON bi.ID_Patient_Grooming = hed.ID  
                           INNER JOIN @IDs_Patient_Grooming ids  
                                   on hed.ID = ids.ID  
                    where  ISNULL(bi.ID_Patient_Grooming, 0) <> 0  
                           AND bi.ID_FilingStatus NOT IN ( 4 )) tbl) AS SourceTable  
           PIVOT(MAX([Info_BillingInvoice])  
                FOR [BillingInvoice_RowID] IN([1],  
                                              [2],  
                                              [3],  
                                              [4],  
                                              [5],  
                                              [6],  
                                              [7],  
                                              [8],  
                                              [9],  
                                              [10] )) AS PivotTable;  
  
    SELECT '_',  
           '' BillingInvoices  
  
    SELECT GETDate() Date  
  
    SELECT *  
    FROM   @record  
  

GO

GO

  
CREATE       OR ALTER
 PROC pUpdatePatient_Wellness_BillingStatus(@IDs_Patient_Wellness typIntList READONLY)  
AS  
  BEGIN  
      DECLARE @Cancelled_ID_FilingStatus INT = 4  
      DECLARE @ForBilling_ID_FilingStatus INT = 16  
      DECLARE @Patient_Wellness TABLE  
        (  
           ID_Patient_Wellness                INT,  
           BillingInvoice_ID_FilingStatus INT  
        )  
      DECLARE @Patient_Wellness_FromBI TABLE  
        (  
           ID_Patient_Wellness                INT,  
           BillingInvoice_ID_FilingStatus INT  
        )  
  
      INSERT @Patient_Wellness  
      SELECT _soap.ID,  
             CASE  
               WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus  
               ELSE NULL  
             END  
      FROM   tPatient_Wellness _soap WITH (NOLOCK)  
             inner join @IDs_Patient_Wellness ids  
                     on _soap.ID = ids.ID  
  
      INSERT @Patient_Wellness_FromBI  
      SELECT ID_Patient_Wellness,  
             dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])  
      FROM   (SELECT idsSOAP.ID ID_Patient_Wellness,  
                     fs.ID      BillingInvoice_ID_FilingStatus,  
                     COUNT(*)   BillingInvoiceCount  
              FROM   vBillingInvoice bi WITH (NOLOCK)  
                     inner JOIN @IDs_Patient_Wellness idsSOAP  
                             on idsSOAP.ID = bi.ID_Patient_Wellness  
                     INNER JOIN tFilingStatus fs  
                            ON fs.Name = bi.Status  
              WHERE  ID_FilingStatus NOT IN ( 4 )  
              GROUP  BY idsSOAP.ID,  
                        fs.ID) AS SourceTable  
             PIVOT ( AVG(BillingInvoiceCount)  
                   FOR BillingInvoice_ID_FilingStatus IN ([1],  
                                                          [3],  
                                                          [2],  
                                                          [11],  
                                                          [12],  
                                                          [13]) ) AS PivotTable;  
  
      UPDATE @Patient_Wellness  
      SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus  
      FROM   @Patient_Wellness soapRec  
             INNER JOIN @Patient_Wellness_FromBI soapFromPivot  
                     ON soapRec.ID_Patient_Wellness = soapFromPivot.ID_Patient_Wellness  
  
      UPDATE tPatient_Wellness  
      SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus  
      FROM   tPatient_Wellness soap WITH (NOLOCK)  
             INNER JOIN @Patient_Wellness soapRec  
                     ON soap.ID = soapRec.ID_Patient_Wellness  
  END  
GO
GO

  
CREATE       OR ALTER
 PROC pUpdatePatient_Grooming_BillingStatus(@IDs_Patient_Grooming typIntList READONLY)  
AS  
  BEGIN  
      DECLARE @Cancelled_ID_FilingStatus INT = 4  
      DECLARE @ForBilling_ID_FilingStatus INT = 16  
      DECLARE @Patient_Grooming TABLE  
        (  
           ID_Patient_Grooming                INT,  
           BillingInvoice_ID_FilingStatus INT  
        )  
      DECLARE @Patient_Grooming_FromBI TABLE  
        (  
           ID_Patient_Grooming                INT,  
           BillingInvoice_ID_FilingStatus INT  
        )  
  
      INSERT @Patient_Grooming  
      SELECT _soap.ID,  
             CASE  
               WHEN _soap.IsForBilling = 1 THEN @ForBilling_ID_FilingStatus  
               ELSE NULL  
             END  
      FROM   tPatient_Grooming _soap WITH (NOLOCK)  
             inner join @IDs_Patient_Grooming ids  
                     on _soap.ID = ids.ID  
  
      INSERT @Patient_Grooming_FromBI  
      SELECT ID_Patient_Grooming,  
             dbo.fGetSOAPBillingStatus([1], [3], [13], [2], [11], [12])  
      FROM   (SELECT idsSOAP.ID ID_Patient_Grooming,  
                     fs.ID      BillingInvoice_ID_FilingStatus,  
                     COUNT(*)   BillingInvoiceCount  
              FROM   vBillingInvoice bi WITH (NOLOCK)  
                     inner JOIN @IDs_Patient_Grooming idsSOAP  
                             on idsSOAP.ID = bi.ID_Patient_Grooming  
                     LEFT JOIN tFilingStatus fs  
                            ON fs.Name = bi.Status  
              WHERE  ID_FilingStatus NOT IN ( 4 )  
              GROUP  BY idsSOAP.ID,  
                        fs.ID) AS SourceTable  
             PIVOT ( AVG(BillingInvoiceCount)  
                   FOR BillingInvoice_ID_FilingStatus IN ([1],  
                                                          [3],  
                                                          [2],  
                                                          [11],  
                                                          [12],  
                                                          [13]) ) AS PivotTable;  
  
      UPDATE @Patient_Grooming  
      SET    BillingInvoice_ID_FilingStatus = soapFromPivot.BillingInvoice_ID_FilingStatus  
      FROM   @Patient_Grooming soapRec  
             INNER JOIN @Patient_Grooming_FromBI soapFromPivot  
                     ON soapRec.ID_Patient_Grooming = soapFromPivot.ID_Patient_Grooming  
  
      UPDATE tPatient_Grooming  
      SET    BillingInvoice_ID_FilingStatus = soapRec.BillingInvoice_ID_FilingStatus  
      FROM   tPatient_Grooming soap WITH (NOLOCK)  
             INNER JOIN @Patient_Grooming soapRec  
                     ON soap.ID = soapRec.ID_Patient_Grooming  
  END  
  
  GO

  CREATE OR ALTER PROC ppUpdatePatient_Wellness_BillingStatus_By_BIs(@IDs_BillingInvoice typIntList READONLY)
  AS
  BEGIN

	DECLARE @IDs_Patient_Wellness typIntList

	INSERT @IDs_Patient_Wellness
	SELECT bi.ID_Patient_Wellness FROM tBillingInvoice bi Inner join @IDs_BillingInvoice ids on bi.ID = ids.ID
	where ISNULL(bi.ID_Patient_Wellness, 0) > 0

	exec pUpdatePatient_Wellness_BillingStatus @IDs_Patient_Wellness
  END

  GO

  
  GO

  CREATE OR ALTER PROC ppUpdatePatient_Grooming_BillingStatus_By_BIs(@IDs_BillingInvoice typIntList READONLY)
  AS
  BEGIN

	DECLARE @IDs_Patient_Grooming typIntList

	INSERT @IDs_Patient_Grooming
	SELECT bi.ID_Patient_Grooming FROM tBillingInvoice bi Inner join @IDs_BillingInvoice ids on bi.ID = ids.ID
	where ISNULL(bi.ID_Patient_Grooming, 0) > 0

	exec pUpdatePatient_Grooming_BillingStatus @IDs_Patient_Grooming
  END

  GO

  GO
  
CREATE     OR ALTER
 PROC [dbo].[pApproveBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,  
                                           @ID_UserSession     INT)  
AS  
  BEGIN  
      DECLARE @Inventoriable_ID_ItemType INT = 2;  
      DECLARE @Approved_ID_FilingStatus INT = 3  
      DECLARE @Used_ID_FilingStatus INT = 17  
      DECLARE @Success BIT = 1;  
      DECLARE @message VARCHAR(300) = '';  
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';  
  
      BEGIN TRY  
          SELECT @Oid_Model_BillingInvoice_Detail = Oid  
          FROM   _tModel  
          where  TableName = 'tBillingInvoice_Detail'  
  
          DECLARE @ID_User INT = 0;  
  
          SELECT @ID_User = ID_User  
          FROM   dbo.tUserSession  
          WHERE  ID = @ID_UserSession;  
  
          EXEC dbo.pApproveBillingInvoice_validation  
            @IDs_BillingInvoice,  
            @ID_UserSession  
  
          Update tBillingInvoice_Detail  
          SET    UnitCost = ISNULL(item.UnitCost, 0)  
          FROM   tBillingInvoice_Detail biDetail  
                 INNER JOIn @IDs_BillingInvoice ids  
                         on biDetail.ID_BillingInvoice = ids.iD  
                 INNER JOIN tItem item  
                         on biDetail.ID_Item = item.ID  
  
          UPDATE dbo.tBillingInvoice  
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,  
                 DateApproved = GETDATE(),  
                 ID_ApprovedBy = @ID_User  
          FROM   dbo.tBillingInvoice bi  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON bi.ID = ids.ID;  
  
          /*Inventory Trail */  
          INSERT INTO dbo.tInventoryTrail  
                      (Code,  
                       ID_Company,  
                       DateCreated,  
                       ID_Item,  
                       Quantity,  
                       UnitPrice,  
                       ID_FilingStatus,  
                       Date,  
                       DateExpired,  
                       Oid_Model_Reference,  
                       ID_Reference)  
          SELECT hed.Code,  
                 hed.ID_Company,  
                 hed.DateCreated,  
                 detail.ID_Item,  
                 0 - detail.Quantity,  
                 detail.UnitPrice,  
                 hed.ID_FilingStatus,  
                 hed.Date,  
                 detail.DateExpiration,  
                 @Oid_Model_BillingInvoice_Detail,  
                 detail.ID  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          -- pUpdate Item Current Inventory         
          DECLARE @IDs_Item typINTList  
  
          INSERT @IDs_Item  
          SELECT DISTINCT ID_Item  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  
  
          -------------------------------------------------------        
          ---        
          EXEC dbo.pUpdateBillingInvoicePayment  
            @IDs_BillingInvoice;  
  
          exec pInsertBillingInvoiceSMSPayableRemider  
            @IDs_BillingInvoice,  
            @ID_UserSession  
			
          exec ppUpdatePatient_Wellness_BillingStatus_By_BIs @IDs_BillingInvoice
          exec ppUpdatePatient_Grooming_BillingStatus_By_BIs @IDs_BillingInvoice
  
          -- Used Deposit on Credit Logs            
          Declare @IDs_ClientDeposit typIntList  
          DECLARE @ClientCredits typClientCredit  
  
          INSERT @IDs_ClientDeposit  
          SELECT cd.ID  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
                 inner join tClientDeposit cd  
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement  
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )  
  
          INSERT @ClientCredits  
          SELECT distinct bi.ID_Client,  
                          bi.Date,  
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ) * -1,  
                          bi.Code,  
                          'Use Deposit from '  
                          + FORMAT(ISNULL(bi.ConfinementDepositAmount, 0), '#,#0.00')  
                          + ' to '  
                          + FORMAT(ISNULL( bi.RemainingDepositAmount, 0), '#,#0.00')  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0  
  
          UPDATE tClientDeposit  
          SET    ID_FilingStatus = @Used_ID_FilingStatus  
          WHERE  ID IN (SELECT ID  
                        FROM   @IDs_ClientDeposit)  
  
          IF (SELECT COUNT(*)  
              FROM   @ClientCredits) > 0  
            begin  
                EXEC pAdjustClientCredits_validation  
                  @ClientCredits,  
                  @ID_UserSession  
  
                exec pAdjustClientCredits  
                  @ClientCredits,  
                  @ID_UserSession  
            END  
      END TRY  
      BEGIN CATCH  
          SET @message = ERROR_MESSAGE();  
          SET @Success = 0;  
      END CATCH;  
  
      SELECT '_';  
  
      SELECT @Success Success,  
             @message message;  
  END;  
  
GO

  
CREATE   OR ALTER
 PROC [dbo].[pCancelBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,  
                                          @ID_UserSession     INT)  
AS  
  BEGIN  
      DECLARE @Approved_ID_FilingStatus INT = 3  
      DECLARE @Used_ID_FilingStatus INT = 17  
      DECLARE @Inventoriable_ID_ItemType INT = 2;  
      DECLARE @Canceled_ID_FilingStatus INT = 4;  
      DECLARE @Success BIT = 1;  
      DECLARE @message VARCHAR(300) = '';  
      DECLARE @ClientCredits typClientCredit  
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';  
  
      BEGIN TRY  
          SELECT @Oid_Model_BillingInvoice_Detail = Oid  
          FROM   _tModel  
          where  TableName = 'tBillingInvoice_Detail'  
  
          DECLARE @ID_User INT = 0;  
  
          SELECT @ID_User = ID_User  
          FROM   dbo.tUserSession  
          WHERE  ID = @ID_UserSession;  
  
          EXEC dbo.pCancelBillingInvoice_validation  
            @IDs_BillingInvoice,  
            @ID_UserSession;  
  
          INSERT @ClientCredits  
          SELECT distinct bi.ID_Client,  
                          bi.Date,  
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ),  
                          bi.Code,  
                          'Rollback Deposit '  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0  
                 AND ID_FilingStatus = @Approved_ID_FilingStatus  
  
          UPDATE dbo.tBillingInvoice  
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,  
                 DateCanceled = GETDATE(),  
                 ID_CanceledBy = @ID_User  
          FROM   dbo.tBillingInvoice bi  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON bi.ID = ids.ID;  
  
          /*Inventory Trail */  
          INSERT INTO tInventoryTrail  
                      (Code,  
                       ID_Company,  
                       DateCreated,  
                       ID_Item,  
                       Quantity,  
                       UnitPrice,  
                       ID_FilingStatus,  
                       Date,  
                       DateExpired,  
                       Oid_Model_Reference,  
                       ID_Reference)  
          SELECT hed.Code,  
                 hed.ID_Company,  
                 hed.DateCreated,  
                 detail.ID_Item,  
                 detail.Quantity,  
                 detail.UnitPrice,  
                 hed.ID_FilingStatus,  
                 hed.Date,  
                 detail.DateExpiration,  
                 @Oid_Model_BillingInvoice_Detail,  
                 detail.ID  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0  
                 AND item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          -- pUpdate Item Current Inventory       
          DECLARE @IDs_Item typINTList  
  
          INSERT @IDs_Item  
          SELECT DISTINCT ID_Item  
          FROM   dbo.tBillingInvoice hed  
                 LEFT JOIN dbo.tBillingInvoice_Detail detail  
                        ON hed.ID = detail.ID_BillingInvoice  
                 LEFT JOIN dbo.tItem item  
                        ON item.ID = detail.ID_Item  
                 INNER JOIN @IDs_BillingInvoice ids  
                         ON hed.ID = ids.ID  
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  
  
          exec pUpdateItemCurrentInventoryByIDsItems  
            @IDs_Item  

		  exec ppUpdatePatient_Wellness_BillingStatus_By_BIs @IDs_BillingInvoice
		  exec ppUpdatePatient_Grooming_BillingStatus_By_BIs @IDs_BillingInvoice
  
          -------------------------------------------------------      
          EXEC dbo.pUpdateBillingInvoicePayment  
            @IDs_BillingInvoice;  
  
          -- Rollback Deposit on Credit Logs          
          Declare @IDs_ClientDeposit typIntList  
  
          INSERT @IDs_ClientDeposit  
          SELECT cd.ID  
          FROM   tBillingInvoice bi  
                 inner join @IDs_BillingInvoice idsBI  
                         on bi.ID = idsBI.ID  
                 inner join tClientDeposit cd  
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement  
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )  
  
          UPDATE tClientDeposit  
          SET    ID_FilingStatus = @Approved_ID_FilingStatus  
          WHERE  ID IN (SELECT ID  
                        FROM   @IDs_ClientDeposit)  
  
          exec pDoAdjustClientCredits  
            @ClientCredits,  
            @ID_UserSession  
      END TRY  
      BEGIN CATCH  
          SET @message = ERROR_MESSAGE();  
          SET @Success = 0;  
      END CATCH;  
  
      SELECT '_';  
  
      SELECT @Success Success,  
             @message message;  
  END;  
  

GO


CREATE   OR
ALTER proc pReRun_AfterSaved_Process_BillingInvoice_By_IDs(@IDs_CurrentObject TYPINTLIST ReadOnly)
AS
  BEGIN
      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                DECLARE @TranName VARCHAR(MAX);
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

                SET @TranName = 'pReRun_AfterSaved_Process_BillingInvoice_By_IDs-'
                                + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID

                IF(SELECT COUNT(*)
                   FROM   @IDs_CurrentObject) > 0
                  BEGIN
                      DECLARE @IDs_BillingINvoice TYPINTLIST
                      DECLARE @IDs_Patient_Confinement TYPINTLIST
                      /*Update And remove Duplicate Inventory Trails*/
                      DECLARE @IDs_Item TYPINTLIST

                      INSERT @IDs_Item
                      SELECT DISTINCT biDetail.ID_Item
                      FROM   tBillingInvoice_Detail biDetail WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on biDetail.ID_BillingInvoice = ids.ID

                      exec pUpdateItemCurrentInventoryByIDsItems
                        @IDs_Item

                      /*Update Confinement Bill Status*/
                      INSERT @IDs_Patient_Confinement
                      SELECT DISTINCT ID_Patient_Confinement
                      FROM   tBillingInvoice bi WITH (NOLOCK)
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      EXEC PupDatePatient_ConfineMen_BillIngStatus
                        @IDs_Patient_Confinement

                      /*Update SOAP Bill Status*/
                      DECLARE @IDs_Patient_SOAP TYPINTLIST

                      INSERT @IDs_Patient_SOAP
                      SELECT DISTINCT ID_Patient_SOAP
                      FROM   tBillingInvoice bi
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      exec dbo.pModel_AfterSaved_BillingInvoice_Computation
                        @IDs_BillingInvoice

                      EXEC pUpdatePatient_SOAP_BillingStatus
                        @IDs_Patient_SOAP

                      exec ppUpdatePatient_Wellness_BillingStatus_By_BIs
                        @IDs_BillingInvoice

                      exec ppUpdatePatient_Grooming_BillingStatus_By_BIs
                        @IDs_BillingInvoice

                      Update tBillingInvoice
                      SET    DateRunAfterSavedProcess = GETDATE(),
                             RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                      FROm   tBillingInvoice bi
                             INNER JOIN @IDs_CurrentObject ids
                                     ON bi.ID = ids.ID
                  END
            END TRY
            BEGIN CATCH
                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID
            END CATCH
        END
  END 

GO
  
CREATE   OR ALTER
 PROC pReRunBillingInvoiceAfterSavedDummy(@IDs_BillingInvoice typIntList ReadOnly)  
as  
  BEGIN  
      SELECT '_';  
  
      SELECT GETDATE() Date  
  END  
GO

  
CREATE   OR ALTER
 PROC pReRunPatient_SOAPAfterSavedDummy(@IDs_Patient_SOAP typIntList ReadOnly)  
as  
  BEGIN  
      SELECT '_';  
  
      SELECT GETDATE() Date  
  END  
GO