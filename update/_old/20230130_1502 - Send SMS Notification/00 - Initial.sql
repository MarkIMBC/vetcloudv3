﻿GO

if OBJECT_ID('dbo.tBillingInvoice_SMSPayableRemider') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tBillingInvoice_SMSPayableRemider',
        1,
        0,
        NULL;

      alter table tCompany_SMSSetting
        Add ID_CreatedBy INT null, ID_LastModifiedBy INT null;

      exec _pCreateAppModule
        'tCompany_SMSSetting',
        1,
        1

      exec _pAddModelDetail
        'tCompany_SMSSetting',
        'ID_COmpany',
        'tCompany',
        'tCompany'
  END

GO

if OBJECT_ID('dbo.tBillingInvoice_SMSPayableRemider_SMSStatus') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tBillingInvoice_SMSPayableRemider_SMSStatus',
        1,
        0,
        NULL
  END

GO

exec _pAddModelProperty
  'tBillingInvoice_SMSPayableRemider_SMSStatus',
  'ID_BillingInvoice_SMSPayableRemider',
  2

exec _pAddModelProperty
  'tBillingInvoice_SMSPayableRemider_SMSStatus',
  'iTextMo_Status',
  2

exec _pAddModelProperty
  'tBillingInvoice_SMSPayableRemider_SMSStatus',
  'DateSent',
  5

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'ID_BillingInvoice') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'ID_BillingInvoice',
        2
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'ID_Client') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'ID_Client',
        2
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'TotalAmount_BillingInvoice') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'TotalAmount_BillingInvoice',
        3;
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'RemainingAmount_BillingInvoice') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'RemainingAmount_BillingInvoice',
        3;
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'DateSent') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'DateSent',
        5
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'SMSMessage') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'SMSMessage',
        1;

      ALTER TABLE tBillingInvoice_SMSPayableRemider
        ALTER COLUMN SMSMessage VARCHAR(MAX) NULL;
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'DateSchedule') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'DateSchedule',
        5
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'DateSent') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'DateSent',
        5
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'IsSentSMS') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'IsSentSMS',
        4
  END

GO

IF COL_LENGTH('tBillingInvoice_SMSPayableRemider', 'ContactNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_SMSPayableRemider',
        'ContactNumber',
        1;
  END

GO

IF COL_LENGTH('tCompany_SMSSetting', 'SMSFormatBillingInvoiceNotification') IS NULL
  BEGIN
      ALTER TABLE tCompany_SMSSetting
        ADD SMSFormatBillingInvoiceNotification VARCHAR(MAX) NULL;
  END

GO

IF COL_LENGTH('tCompany_SMSSetting', 'IsEnableBillingInvoiceNotificationSending') IS NULL
  BEGIN
      ALTER TABLE tCompany_SMSSetting
        ADD IsEnableBillingInvoiceNotificationSending bit NULL CONSTRAINT DF_tCompany_SMSSetting_IsEnableBillingInvoiceNotificationSending DEFAULT 1
  END

GO

--IF COL_LENGTH('tCompany_SMSSetting', 'IsEnableSMSBillingReminder') IS NULL
--  BEGIN
--      ALTER TABLE tCompany_SMSSetting
--        ADD IsEnableSMSBillingReminder bit NULL;
--      ALTER TABLE tCompany_SMSSetting
--        ADD CONSTRAINT DF_tCompany_SMSSetting_IsEnableSMSBillingReminder DEFAULT 1 FOR tCompany_SMSSetting_IsEnableSMSBillingReminder;
--  END
--GO
IF COL_LENGTH('tCompany_SMSSetting', 'DayInterval') IS NULL
  BEGIN
      ALTER TABLE tCompany_SMSSetting
        ADD DayInterval INT NULL CONSTRAINT tCompany_SMSSetting_DayInterval_d DEFAULT 0 WITH VALUES;
  END

GO

IF COL_LENGTH('tSupplier', 'MobileNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tSupplier',
        'MobileNumber',
        1
  END

GO

CREATE   OR
ALTER VIEW vSupplier_Item_List
as
  SELECT hed.ID,
         item.ID_Company,
         item.Name                  Name_Item,
         item.ID                    ID_Item,
         s.ID                       ID_Supplier,
         s.Name                     Name_Supplier,
         item.CurrentInventoryCount CurrentInventoryCount,
         item.ID_InventoryStatus,
         item.Name_InventoryStatus
  FROM   tItem_Supplier hed
         INNER JOIN tSupplier s
                 ON hed.ID_Supplier = s.ID
         INNER JOIN vActiveItem item
                 ON hed.ID_Item = item.ID

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER FUNCTION dbo.fGetSMSMessageBillingInvoiceNotification (@SMSFormat                    VARCHAR(MAX),
                                                             @Name_Company                 Varchar(MAX),
                                                             @ContactNumber_Company        Varchar(MAX),
                                                             @Code_BillingInvoice          Varchar(MAX),
                                                             @Date_BillingInvoice          DateTime,
                                                             @Client_Company               Varchar(MAX),
                                                             @PayableAmount_BillingInvoice Decimal(18, 2))
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @message VARCHAR(MAX) = ''
        + 'Hello, /*Client*/, This is /*Company*/. '
        + CHAR(13)
        + 'We�d like to remind you that a pending payable for /*BICode*/ amounting Php /*PayableAmount*/ billed from /*BIDate*/. '
        + CHAR(13) + CHAR(13)
        + 'Kindly disregard this message if you already settled your Payment.'
        + CHAR(13) + CHAR(13)
        + 'This is an automated SMS reminder PLEASE DO NOT REPLY or CALL directly to this number.'
        + CHAR(13) + CHAR(13)
        + 'For more information, please contact /*Company*/ /*ContactNumber_Company*/. Thank you.'
      Declare @DateReturnString Varchar(MAX) = FORMAT(@Date_BillingInvoice, 'M/dd/yyyy ddd')
      Declare @PayableAmountString Varchar(MAX) = FORMAT(@PayableAmount_BillingInvoice, '#,#0.00')

      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(ISNULL(@Client_Company, 0))))
      SET @message = REPLACE(@message, '/*Company*/', LTRIM(RTRIM(ISNULL(@Name_Company, 0))))
      SET @message = REPLACE(@message, '/*BICode*/', LTRIM(RTRIM(ISNULL(@Code_BillingInvoice, 0))))
      SET @message = REPLACE(@message, '/*BIDate*/', LTRIM(RTRIM(ISNULL(@DateReturnString, 0))))
      SET @message = REPLACE(@message, '/*PayableAmount*/', LTRIM(RTRIM(ISNULL(@PayableAmountString, 0))))
      SET @message = REPLACE(@message, '/*ContactNumber_Company*/', LTRIM(RTRIM(ISNULL(@ContactNumber_Company, 0))))
      SET @message = REPLACE(@message, '"', '``')

      RETURN @message
  END

go

ALTER FUNCTION [dbo].[fGetSendSoapPlan](@Date             DATETIME,
                                        @IsSMSSent        BIT = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETIME,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETIME,
  DateCreated          DATETIME,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX),
  Count                INT)
AS
  BEGIN
      DECLARE @IDs_Company TYPINTLIST
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_ID_Model VARCHAR(MAX) =''
      DECLARE @_table TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )

      SELECT @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      SELECT @Patient_Vaccination_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      SELECT @Patient_SOAP_Plan_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      SELECT @Patient_Wellness_Schedule_ID_Model = Oid
      FROM   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      IF( len(Trim(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')

            DELETE FROM @IDs_Company
            WHERE  ID IN (SELECT ID_Company
                          FROM   tCompany_SMSSetting
                          WHERE  IsActive = 0)
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting cSMSSetting
                   INNER JOIN tCompany c
                           on cSMSSetting.ID_Company = c.ID
            WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
                   and c.IsActive = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent TABLE
        (
           IsSMSSent BIT
        )

      IF @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = IsNull(@Date, GetDate())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*SOAP PLAN */
      INSERT @_table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             IsNull(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, soapPlan.Name_Item, IsNull(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DateAdd(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = ISNULL(patientSOAP.ID_Client, patient.ID_Client)
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     ON patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND IsNull(patientSOAP.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Patient Wellness Schedule*/
      INSERT @_table
      SELECT DISTINCT c.ID                                                                                                                                                                                                                        ID_Company,
                      c.NAME                                                                                                                                                                                                                      Name_Company,
                      client.NAME                                                                                                                                                                                                                 Name_Client,
                      dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                          ContactNumber_Client,
                      wellDetSched.Date,
                      IsNull(wellDetSched.Comment, ''),
                      IsNull(wellDetSched.Comment, ''),
                      dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, IsNull(client.NAME, ''), IsNull(c.ContactNumber, ''), IsNull(patient.NAME, ''), IsNull(wellDetSched.Comment, ''), IsNull(wellDetSched.Comment, ''), wellDetSched.Date) Message,
                      CONVERT(DATE, DateAdd(DAY, -1, wellDetSched.Date))                                                                                                                                                                          DateSending,
                      wellHed.DateCreated,
                      wellDetSched.ID                                                                                                                                                                                                             ID_Patient_Wellness_Schedule,
                      @Patient_Wellness_Schedule_ID_Model,
                      wellHed.Code
      FROM   dbo.vPatient_Wellness_Schedule wellDetSched
             inner JOIN vPatient_Wellness wellHed
                     ON wellHed.ID = wellDetSched.ID_Patient_Wellness
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellHed.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellHed.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellHed.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     ON c.ID = idsCompany.ID
      WHERE  wellHed.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND IsNull(wellHed.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, wellDetSched.Date)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.NAME

      /*Vaccination*/
      DECLARE @IDs_Vaccination_ExistingPatientWellness TYPINTLIST

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @_table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.NAME                                                                                                                                                                 Name_Company,
             client.NAME                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             IsNull(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Name_Item, IsNull(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DateAdd(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             INNER JOIN tPatient_Vaccination_Schedule vacSchedule
                     ON vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     ON vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND IsNull(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND IsNull(vac.ID_CLient, 0) > 0
             AND IsNull(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DateAdd(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
             AND vac.ID NOT IN (SELECT ID
                                FROM   @IDs_Vaccination_ExistingPatientWellness)

      /*Reschedule WaitingList*/
      INSERT @_table
             (ID_Company,
              Name_Company,
              Name_Client,
              ContactNumber_Client,
              DateReturn,
              Name_Item,
              Comment,
              Message,
              DateSending,
              DateCreated,
              ID_Reference,
              Oid_Model,
              Code)
      SELECT hed.ID_Company,
             company.Name,
             client.Name,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),
             hed.DateCreated,
             '',
             '',
             dbo.fGetSMSMessageSMSFormatRescheduleAppointment(smsSetting.SMSFormatRescheduleAppointment, company.Name, company.ContactNumber, hed.DateReschedule, client.Name, patient.Name, _appointment.ReferenceCode, _appointment.Description),
             hed.DateCreated,
             hed.DateCreated,
             hed.ID,
             @PatientWaitingList_ID_Model,
             hed.Code
      FROM   tPatientWaitingList hed
             INNER JOIN tCompany_SMSSetting smsSetting
                     on smsSetting.ID_Company = hed.ID_Company
             INNER JOIN tCompany company
                     on company.ID = hed.ID_Company
             inner join tClient client
                     on hed.ID_Client = client.ID
             inner join tPatient patient
                     on hed.ID_Patient = patient.ID
             LEFT join vAppointmentEvent _appointment
                    on hed.ID_Reference = _appointment.Appointment_ID_CurrentObject
                       and hed.Oid_Model_Reference = _appointment.Oid_Model
             INNER JOIN @IDs_Company idsCompany
                     ON company.ID = idsCompany.ID
      WHERE  --CONVERT(Date, DateAdd(Day, smsSetting.DayInterval, hed.DateCreated)) = CONVERT(Date, @Date)    
        (( CONVERT(DATE, hed.DateCreated) = CONVERT(DATE, GETDATE()) ))
        AND hed.WaitingStatus_ID_FilingStatus IN ( 21 )
        AND IsNull(hed.IsSentSMS, 0) IN (SELECT IsSMSSent
                                         FROM   @SMSSent)
        AND IsNull(hed.ID_CLient, 0) > 0
        AND IsNull(patient.IsDeceased, 0) = 0

      /*BillingInvoice SMSPayableRemider*/
      INSERT @_table
             (ID_Company,
              Name_Company,
              Name_Client,
              ContactNumber_Client,
              DateReturn,
              Name_Item,
              Comment,
              Message,
              DateSending,
              DateCreated,
              ID_Reference,
              Oid_Model,
              Code)
      SELECT biHed.ID_Company,
             company.Name,
             client.Name,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2),
             bSMSReminder.DateSchedule,
             '',
             '',
             dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, company.ContactNumber, biHed.Code, biHed.Date, client.Name, ISNULL(bSMSReminder.RemainingAmount_BillingInvoice, 0)),
             biHed.Date,
             bSMSReminder.DateCreated,
             bSMSReminder.ID,
             @BillingInvoice_SMSPayableRemider_ID_Model,
             bihed.Code
      FROM   vBillingInvoice biHed
             INNER JOIN tBillingInvoice_SMSPayableRemider bSMSReminder
                     on biHed.ID = bSMSReminder.ID_BillingInvoice
             INNER JOIN tCompany_SMSSetting smsSetting
                     on smsSetting.ID_Company = bSMSReminder.ID_Company
             INNER JOIN tCompany company
                     on company.ID = bSMSReminder.ID_Company
             inner join tClient client
                     on bSMSReminder.ID_Client = client.ID
             INNER JOIN @IDs_Company idsCompany
                     ON company.ID = idsCompany.ID
      WHERE  CONVERT(Date, bSMSReminder.dateSchedule) = CONVERT(Date, GETDATE())
             AND ISNULL(bSMSReminder.IsActive, 0) = 1
             AND biHed.ID_FilingStatus = 3
             AND Payment_ID_FilingStatus IN ( 2, 11 )
             AND ISNULL(RemainingAmount, 0) > 0
             AND ISNULL(smsSetting.IsEnableBillingInvoiceNotificationSending, 0) = 1
             AND 1 = 0

      DELETE @_table
      FROM   @_table forSMSSending
             inner join tInactiveSMSSending inactiveSendingSchedule
                     on forSMSSending.ID_Company = inactiveSendingSchedule.ID_Company
                        and CONVERT(Date, forSMSSending.DateSending) = CONVERT(Date, inactiveSendingSchedule.Date)
      WHERE  inactiveSendingSchedule.IsActive = 1

      INSERT @table
      SELECT ID_Company,
             Name_Company,
             Name_Client,
             ContactNumber_Client,
             DateReturn,
             Name_Item,
             Comment,
             Message,
             MAX(DateSending),
             MAX(DateCreated),
             MAX(ID_Reference),
             Oid_Model,
             MAX(Code),
             Count(*)
      FROM   @_table
      GROUP  BY ID_Company,
                Name_Company,
                Name_Client,
                ContactNumber_Client,
                DateReturn,
                Name_Item,
                Comment,
                Message,
                Oid_Model

      Update @table
      set    Comment = REPLACE(Comment, '"', '``')

      Update @table
      set    Name_Item = REPLACE(Name_Item, '"', '``')

      RETURN
  END

GO

CREATE OR
ALTER VIEW vBillingInvoice_SMSPayableRemider_Listview
AS
  SELECT ID,
         DateSent,
         ID_BillingInvoice
  FROM   vBillingInvoice_SMSPayableRemider
  where  DateSent IS NOT NULL
         and ISNULL(IsSentSMS, 0) = 1

GO

CREATE OR
ALTER VIEW vSMSList_BillingInvoice_SMSPayableRemider
AS
  SELECT biSMSReminder.ID,
         biSMSReminder.ID_BillingInvoice,
         bihed.Code,
         DateSent,
         Name_Client,
         Name_Patient,
         c.ContactNumber,
         dbo.fGetSMSMessageBillingInvoiceNotification(smsSetting.SMSFormatBillingInvoiceNotification, company.Name, company.ContactNumber, biHed.Code, biHed.Date, biHed.Name_Client, ISNULL(biSMSReminder.RemainingAmount_BillingInvoice, 0)) Message,
         DATEADD(DAY, -1, biSMSReminder.DateSchedule)                                                                                                                                                                                          DateSending,
         bihed.ID_Company,
         ISNULL(biSMSReminder.IsSentSMS, 0)                                                                                                                                                                                                    IsSentSMS,
         model.Oid                                                                                                                                                                                                                             Oid_Model
  FROM   vBillingInvoice_SMSPayableRemider biSMSReminder
         INNER JOIN vBillingInvoice biHed
                 on biSMSReminder.ID_BillingInvoice = bihed.ID
         INNER JOIN tCompany_SMSSetting smsSetting
                 on smsSetting.ID_Company = biSMSReminder.ID_Company
         INNER JOIN tClient c
                 on c.ID = bihed.ID_Client
         INNER JOIN tCOmpany company
                 on company.ID = bihed.ID_Company,
         _tModel model
  where  model.tableName = 'tBillingInvoice_SMSPayableRemider'
         AND ISNULL(biSMSReminder.IsActive, 0) = 1
         AND biHed.ID_FilingStatus = 3
         AND Payment_ID_FilingStatus IN ( 2, 11 )
         AND ISNULL(RemainingAmount, 0) > 0
         and 1 = 0

GO

ALTER VIEW [dbo].[vSMSList]
as
  SELECT ID,
         ID              ID_Reference,
         ID_Patient_SOAP Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_SOAP_Plan
  Union All
  SELECT ID,
         ID                     ID_Reference,
         ID_Patient_Vaccination Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Vaccination_Schedule
  UNION ALL
  SELECT ID,
         ID                  ID_Reference,
         ID_Patient_Wellness Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_Patient_Wellness_DetailSchedule
  UNION ALL
  SELECT ID,
         ID                    ID_Reference,
         ID_PatientWaitingList Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_PatientWaitingList_ReSchedule
  UNION ALL
  SELECT ID,
         ID                ID_Reference,
         ID_BillingInvoice Parent_ID_Reference,
         Code,
         DateSent,
         Name_Client,
         Name_Patient,
         ContactNumber,
         Message,
         DateSending,
         ID_Company,
         IsSentSMS,
         Oid_Model
  FROM   vSMSList_BillingInvoice_SMSPayableRemider

GO

CREATE OR
ALTER VIEW vBillingInvoiceSMSRemiderForTodayIDs
as
  SELECT bisms.ID_BillingInvoice
  FROM   tBillingInvoice_SMSPayableRemider bisms
         inner join tBillingInvoice bi
                 on bi.ID = bisms.ID_BillingInvoice
  where  ISNULL(bisms.IsSentSMS, 0) = 0
         AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
         AND bi.Payment_ID_FilingStatus IN ( 2, 11 )

GO

CREATE   OR
ALTER PROC pInsertBillingInvoiceSMSPayableRemider(@IDs_BillingInvoice typIntList READONLY,
                                                  @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Success BIT = 1
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @SendStartDate Date = DateAdd(MOnth, 1, GETDATE())
      DECLARE @SendEndDate Date = DateAdd(YEAR, 1, GETDATE())
      DECLARE @DateList Table
        (
           DateSchedule DateTime
        )

      INSERT @DateList
      select Date
      FROM   dbo.fGetDatesByDateCoverage(@SendStartDate, @SendEndDate)
      WHERE  CONVERT(Date, Date) = @SendStartDate
              OR DATEPART(day, Date) = DATEPART(day, @SendStartDate)

      BEGIN TRY
          DECLARE @ID_Company INT
          DECLARE @DateSent DateTime
          DECLARE @ContactNumber VARCHAR(MAX)
          DECLARE @SMSMessage VARCHAR(MAX)
          DECLARE @ID_User INT = 0

          SELECT @ID_User = ID_User
          FROm   tUserSession
          where  ID = @ID_UserSession

          SELECT @SMSMessage = SMSFormatBillingInvoiceNotification
          FROM   tCompany_SMSSetting
          where  ID_Company = @ID_Company

          Update tBillingInvoice_SMSPayableRemider
          SET    IsActive = 0
          FROM   tBillingInvoice_SMSPayableRemider biSMSReminder
                 INNER JOIN @IDs_BillingInvoice bi
                         on biSMSReminder.ID_BillingInvoice = bi.ID

          INSERT INTO [dbo].[tBillingInvoice_SMSPayableRemider]
                      ([ID_Company],
                       ID_BillingInvoice,
                       ID_Client,
                       TotalAmount_BillingInvoice,
                       RemainingAmount_BillingInvoice,
                       DateSchedule,
                       [Name],
                       [IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [SMSMessage],
                       [IsSentSMS],
                       [ContactNumber])
          SELECT biHed.ID_Company,
                 biHed.ID,
                 bihed.ID_Client,
                 ISNULL(biHed.TotalAmount, 0),
                 ISNULL(biHed.RemainingAmount, 0),
                 _dateSchedule.DateSchedule,
                 '',
                 1,
                 GETDATE(),
                 GETDATE(),
                 @ID_User,
                 @ID_User,
                 @SMSMessage,
                 0,
                 client.ContactNumber
          FROM   vBillingInvoice biHed
                 inner join tClient client
                         on biHed.ID_Client = client.ID
                 INNER JOIN @IDs_BillingInvoice biID
                         on biHed.ID = biID.ID,
                 @DateList _dateSchedule
          where  biHed.Payment_ID_FilingStatus IN ( 2, 11 )
      END TRY
      BEGIN CATCH
          SET @Success = 0;
          SET @message = ERROR_MESSAGE();
      END CATCH

      SELECT '_'

      SELECT @Success Success,
             @message message
  END

GO

CREATE     OR
ALTER PROC [dbo].[pNoteSOAPPlanAsSend] (@ID_Reference   INT,
                                        @Oid_Model      VARCHAR(MAX),
                                        @iTextMo_Status INT,
                                        @DateSent       DateTime)
AS
  BEGIN
      /*                  
       iTextMo Status                  
                       
       "1" = Invalid Number.                  
       "2" = Number prefix not supported. Please contact us so we can add.                  
       "3" = Invalid ApiCode.                  
       "4" = Maximum Message per day reached. This will be reset every 12MN.                  
       "5" = Maximum allowed characters for message reached.                  
       "6" = System OFFLINE.                  
       "7" = Expired ApiCode.                  
       "8" = iTexMo Error. Please try again later.                  
       "9" = Invalid Function Parameters.                  
       "10" = Recipient's number is blocked due to FLOODING, message was ignored.                  
       "11" = Recipient's number is blocked temporarily due to HARD sending (after 3 retries of sending and message still failed to send) and the message was ignored. Try again after an hour.                  
       "12" = Invalid request. You can't set message priorities on non corporate apicodes.                  
       "13" = Invalid or Not Registered Custom Sender ID.                  
       "14" = Invalid preferred server number.                  
       "15" = IP Filtering enabled - Invalid IP.                  
       "16" = Authentication error. Contact support at support@itexmo.com                  
       "17" = Telco Error. Contact Support support@itexmo.com                  
       "18" = Message Filtering Enabled. Contact Support support@itexmo.com                  
       "19" = Account suspended. Contact Support support@itexmo.com                  
       "0" = Success! Message is now on queue and will be sent soon                 
      "-1" = Reach VetCloud SMS Count Limit                 
      "20" = Manual Sent SMS              
                  
      */
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @PatientWaitingList_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @BillingInvoice_SMSPayableRemider_ID_Model VARCHAR(MAX) =''
      DECLARE @Success BIT = 1

      IF @DateSent IS NULL
        SET @DateSent = GETDATE()

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      select @PatientWaitingList_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatientWaitingList'

      select @BillingInvoice_SMSPayableRemider_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tBillingInvoice_SMSPayableRemider'

      IF( @Oid_Model = @Patient_SOAP_Plan_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_SOAP_Plan
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_SOAP_Plan psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_SOAP_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_SOAP],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Vaccination_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Vaccination_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Vaccination_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Vaccination_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Vaccination_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @Patient_Wellness_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatient_Wellness_Schedule
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  FROM   tPatient_Wellness_Schedule psp
                  WHERE  psp.ID = @ID_Reference
              END

            INSERT INTO [dbo].[tPatient_Wellness_Schedule_SMSStatus]
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         [ID_Patient_Wellness_Schedule],
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @PatientWaitingList_Schedule_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tPatientWaitingList
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tPatientWaitingList_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_PatientWaitingList,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END
      ELSE IF( @Oid_Model = @BillingInvoice_SMSPayableRemider_ID_Model )
        BEGIN
            IF @iTextMo_Status = 0
                OR @iTextMo_Status = 20
              BEGIN
                  UPDATE dbo.tBillingInvoice_SMSPayableRemider
                  SET    IsSentSMS = 1,
                         DateSent = @DateSent
                  WHERE  ID = @ID_Reference
              END

            INSERT INTO [dbo].tBillingInvoice_SMSPayableRemider_SMSStatus
                        ([Code],
                         [Name],
                         [IsActive],
                         [ID_Company],
                         [Comment],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [iTextMo_Status],
                         ID_BillingInvoice_SMSPayableRemider,
                         DateSent)
            VALUES      (NULL,
                         NULL,
                         1,
                         1,
                         NULL,
                         GETDATE(),
                         GETDATE(),
                         1,
                         1,
                         @iTextMo_Status,
                         @ID_Reference,
                         @DateSent)
        END

      SELECT '_'

      SELECT @Success Success;
  END

GO

go

CREATE OR
ALTER PROCEDURE [dbo].[pGetCompany] @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Company_SMSSetting

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCompany H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vCompany_SMSSetting
      WHERE  ID_Company = @ID
  END

GO

CREATE OR
ALTER PROC [dbo].[pApproveBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,
                                           @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Oid_Model_BillingInvoice_Detail VARCHAR(MAX) = '';

      BEGIN TRY
          SELECT @Oid_Model_BillingInvoice_Detail = Oid
          FROM   _tModel
          where  TableName = 'tBillingInvoice_Detail'

          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession

          Update tBillingInvoice_Detail
          SET    UnitCost = ISNULL(item.UnitCost, 0)
          FROM   tBillingInvoice_Detail biDetail
                 INNER JOIn @IDs_BillingInvoice ids
                         on biDetail.ID_BillingInvoice = ids.iD
                 INNER JOIN tItem item
                         on biDetail.ID_Item = item.ID

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO dbo.tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired,
                       Oid_Model_Reference,
                       ID_Reference)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 0 - detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration,
                 @Oid_Model_BillingInvoice_Detail,
                 detail.ID
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          -- pUpdate Item Current Inventory     
          DECLARE @IDs_Item typINTList

          INSERT @IDs_Item
          SELECT DISTINCT ID_Item
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  item.ID_ItemType = @Inventoriable_ID_ItemType;

          exec pUpdateItemCurrentInventoryByIDsItems
            @IDs_Item

          -------------------------------------------------------    
          ---    
          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          exec pInsertBillingInvoiceSMSPayableRemider
            @IDs_BillingInvoice,
            @ID_UserSession

          -- Used Deposit on Credit Logs        
          Declare @IDs_ClientDeposit typIntList
          DECLARE @ClientCredits typClientCredit

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Approved_ID_FilingStatus )

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ) * -1,
                          bi.Code,
                          'Use Deposit from '
                          + FORMAT(ISNULL(bi.ConfinementDepositAmount, 0), '#,#0.00')
                          + ' to '
                          + FORMAT(ISNULL( bi.RemainingDepositAmount, 0), '#,#0.00')
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Used_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          IF (SELECT COUNT(*)
              FROM   @ClientCredits) > 0
            begin
                EXEC pAdjustClientCredits_validation
                  @ClientCredits,
                  @ID_UserSession

                exec pAdjustClientCredits
                  @ClientCredits,
                  @ID_UserSession
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE   OR
ALTER PROC pUpdateBillingInvoiceSMSReminderRemainingBalance(@IDs_BillingInvoice typINTLIst Readonly)
AS
  BEGIN
      IF(SELECT COUNT(*)
         FROM   @IDs_BillingInvoice) > 0
        BEGIN
            Update tBillingInvoice_SMSPayableRemider
            SET    RemainingAmount_BillingInvoice = bi.RemainingAmount
            FROM   tBillingInvoice_SMSPayableRemider bisms
                   inner join tBillingInvoice bi
                           on bi.ID = bisms.ID_BillingInvoice
                   INNER JOIN @IDs_BillingInvoice ids
                           on bi.ID = ids.ID
            where  ISNULL(bisms.IsSentSMS, 0) = 0
                   AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
                   AND bi.Payment_ID_FilingStatus IN ( 2, 11 )
                   and bi.ID_FilingStatus = 3
        END
      else
        BEGIN
            Update tBillingInvoice_SMSPayableRemider
            SET    RemainingAmount_BillingInvoice = bi.RemainingAmount
            FROM   tBillingInvoice_SMSPayableRemider bisms
                   inner join tBillingInvoice bi
                           on bi.ID = bisms.ID_BillingInvoice
            where  ISNULL(bisms.IsSentSMS, 0) = 0
                   AND Convert(Date, bisms.DateSchedule) = CONVERT(DATE, GETDATE())
                   AND bi.Payment_ID_FilingStatus IN ( 2, 11 )
                   and bi.ID_FilingStatus = 3
        END
  END

GO

--DECLARE @IDs_BillingInvoice typIntList
--DECLARE @ID_Company INT
--DECLARE @ID_UserSession INT
--INSERT @IDs_BillingInvoice
--SELECT biHed.ID
--FROM   tBillingInvoice biHed
--       inner join tUserSession _usersession
--               on biHed.ID_CreatedBy = _usersession.ID_User
--where  biHed.Payment_ID_FilingStatus IN ( 2, 11 )
--       and biHed.ID_Company = 1
--ORDER  BY biHed.ID DESC
--SELECT TOP 1 @ID_UserSession = _usersession.ID
--FROM   tBillingInvoice biHed
--       inner join tUserSession _usersession
--               on biHed.ID_CreatedBy = _usersession.ID_User
--where  biHed.Payment_ID_FilingStatus IN ( 2, 11 )
--       and biHed.ID_Company = 1
--ORDER  BY biHed.ID DESC
--SELECT TOP 1 biHed.ID,
--             _usersession.ID
--FROM   tBillingInvoice biHed
--       inner join tUserSession _usersession
--               on biHed.ID_CreatedBy = _usersession.ID_User
--where  biHed.Payment_ID_FilingStatus IN ( 2, 11 )
--ORDER  BY biHed.ID DESC
--exec pInsertBillingInvoiceSMSPayableRemider
--  @IDs_BillingInvoice,
--  @ID_UserSession
--select @ID_UserSession
--GO 
Update tCompany_SMSSetting
SET    IsEnableBillingInvoiceNotificationSending = 1
where  IsEnableBillingInvoiceNotificationSending IS NULL

Update tBillingInvoice_SMSPayableRemider
SET    IsActive = 0
where  CONVERT(Date, DateSchedule) = CONVERT(Date, GETDATE())
       AND IsActive = 1 
