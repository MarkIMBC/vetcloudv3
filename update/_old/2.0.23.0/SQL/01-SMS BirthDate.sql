IF NOT EXISTS (SELECT 1
               FROM   INFORMATION_SCHEMA.TABLES
               WHERE  TABLE_TYPE = 'BASE TABLE'
                      AND TABLE_NAME = 'tPatient_BirthDateSMSGreetingLog')
  BEGIN
      exec _pCreateAppModuleWithTable
        'tPatient_BirthDateSMSGreetingLog',
        1,
        NULL,
        NULL

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'ID_Client',
        2

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'ContactNumber',
        1

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'ID_Patient',
        2

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'iTextMo_Status',
        2

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'DateSent',
        5

      exec _pAddModelProperty
        'tPatient_BirthDateSMSGreetingLog',
        'Message',
        1
  END

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetITextMessageCreditCount](@Message VARCHAR(255))
RETURNS INT
AS
  BEGIN
      DECLARE @Result INT = 0

      SET @Result = CASE
                      WHEN LEN(@Message) <= 160 THEN 1
                      ELSE
                        CASE
                          WHEN LEN(@Message) <= 306 THEN 2
                          ELSE
                            CASE
                              WHEN LEN(@Message) <= 459 THEN 3
                              ELSE 4
                            END
                        END
                    END

      RETURN @Result
  END

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetSendPatientBirthDate](@BirthDate        VARCHAR(MAX),
                                                @IsSMSSent        Bit = NULL,
                                                @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  ID_Client            INT,
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  ID_Patient           INT,
  Name_Patient         VARCHAR(MAX),
  DateBirth            DateTime,
  Age                  VARCHAR(MAX),
  Message              VARCHAR(MAX))
as
  BEGIN
      DECLARE @IDs_Company typIntList
      DECLARE @IDs_AlreadySentPatient typIntList

      INSERT @IDs_AlreadySentPatient
      SELECT DISTINCT ID_Patient
      FROm   dbo.tPatient_BirthDateSMSGreetingLog _log
             inner join tPatient patient
                     on _log.ID_Patient = patient.ID
      WHERE  FORMAT(patient.DateBirth, 'MM-dd') = @BirthDate
             AND ISNULL(iTextMo_Status, 0) IN ( 0 )

      if( LEN(TRIM(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            Select Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting
            WHERE  ISNULL(IsActive, 0) = 1
        END

      INSERT @table
      select company.ID,
             company.Name,
             client.ID                                                                                                                                                                                                                                                 ID_Client,
             client.Name                                                                                                                                                                                                                                               Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                                                        ContactNumber_Client,
             patient.ID                                                                                                                                                                                                                                                ID_Patient,
             patient.Name                                                                                                                                                                                                                                              Name_Patient,
             DateBirth,
             dbo.fGetAge(DateBirth, GETDATE(), '')                                                                                                                                                                                                                     Age,
             dbo.[fGetPatientBirthDayGreetings](company.Name, 'Good day, Today is /*DateBirth*/. Happy Birthday for your pet /*Pet*/ from /*CompanyName*/.', client.Name, dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2), patient.Name, DateBirth) Message
      From   vPatient patient
             INNER JOIN tClient client
                     on patient.ID_Client = client.ID
             INNER JOIN @IDs_Company idsCompany
                     on patient.ID_Company = idsCompany.ID
             INNER JOIN tCompany company
                     on company.ID = patient.ID_Company
      where  patient.IsActive = 1
             and ISNULL(IsDeceased, 0) = 0
             AND FORMAT(patient.DateBirth, 'MM-dd') = @BirthDate
             AND patient.ID NOT IN (SELECT ID
                                    FROM   @IDs_AlreadySentPatient)

      RETURN
  END

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetPatientBirthDayGreetings] (@CompanyName   Varchar(MAX),
                                                     @template      VARCHAR(MAX),
                                                     @Client        Varchar(MAX),
                                                     @ContactNumber Varchar(MAX),
                                                     @Patient       Varchar(MAX),
                                                     @DateBirth     DateTime)
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @DateReturnString Varchar(MAX) = FORMAT(@DateBirth, 'dddd MMM. dd, yyyy')
      Declare @message Varchar(MAX) = @template

      SET @Patient = ISNULL(@Patient, 'your Pet')
      SET @Client = ISNULL(@Client, '')
      SET @message = REPLACE(@message, '/*CompanyName*/', LTRIM(RTRIM(@CompanyName)))
      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))
      SET @message = REPLACE(@message, '/*ContactNumber*/', LTRIM(RTRIM(@ContactNumber)))
      SET @message = REPLACE(@message, '/*Pet*/', LTRIM(RTRIM(@Patient)))
      SET @message = REPLACE(@message, '/*DateBirth*/', LTRIM(RTRIM(ISNULL(@DateReturnString, ''))))

      RETURN @message
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetSendPatientBirthDate](@BirthDate        VARCHAR(MAX),
                                            @IsSMSSent        Bit = NULL,
                                            @IDsCompanyString VARCHAR(MAX))
AS
  BEGIN
      DECLARE @Success BIT = 1;

      SELECT '_',
             '' AS summary,
             '' AS records;

      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           ID_Client            INT,
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           ID_Patient           INT,
           Name_Patient         VARCHAR(MAX),
           DateBirth            DateTime,
           Age                  VARCHAR(MAX),
           Message              VARCHAR(MAX)
        )

      INSERT @record
      SELECT *
      FROM   dbo.[fGetSendPatientBirthDate](@BirthDate, @IsSMSSent, @IDsCompanyString)

      SELECT @Success Success;

      SELECT FORMAT(DateBirth, 'MM-dd')                        DateBirth,
             tbl.Name_Company,
             Count(*)                                          Count,
             SUM(dbo.fGetITextMessageCreditCount(tbl.Message)) TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateBirth, 'MM-dd'),
                Name_Company
      Order  BY DateBirth DESC,
                Name_Company

      SELECT rec.*,
             dbo.fGetITextMessageCreditCount(rec.Message) ConsumedSMSCredit,
             LEN(Message)                                 CharLength
      FROM   @record rec
             inner join tCompany com
                     on com.ID = rec.ID_Company
      Order  BY FORMAT(DateBirth, 'MM-dd') DESC,
                com.ID_PackagePlan DESC,
                Name_Company,
                Name_Client
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetForSendPatientBirthDate]
AS
  BEGIN
      Declare @BirthDateString VARCHAR(MAX) = FORMAT(GETDATE(), 'MM-dd')

      exec dbo.pGetSendPatientBirthDate
        @BirthDateString,
        0,
        '33'
  END

GO

CREATE OR
ALTER PROC pLog_BirthDateSMSGreeting(@ID_Client      INT,
                                     @ContactNumber  VARCHAR(MAX),
                                     @ID_Patient     INT,
                                     @iTextMo_Status INT,
                                     @Message        VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Company int = 0
      DECLARE @Success BIT = 1
      DECLARE @DateSent DateTime

      if( @iTextMo_Status = 0 )
        SET @DateSent = GETDATE()

      SELECT @ID_Company = ID_Company
      FROM   tPatient
      where  ID = @ID_Patient

      INSERT INTO [dbo].[tPatient_BirthDateSMSGreetingLog]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Client],
                   [ContactNumber],
                   [ID_Patient],
                   [iTextMo_Status],
                   [DateSent],
                   [Message])
      VALUES      (NULL,
                   NULL,
                   1,
                   @ID_Company,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ID_Client,
                   @ContactNumber,
                   @ID_Patient,
                   @iTextMo_Status,
                   @DateSent,
                   @Message)

      SELECT '_'

      SELECT @Success Success;
  END

GO

exec dbo.[pGetForSendPatientBirthDate] 
