IF NOT EXISTS (SELECT 1
               FROM   INFORMATION_SCHEMA.TABLES
               WHERE  TABLE_TYPE = 'BASE TABLE'
                      AND TABLE_NAME = 'tAppointmentStatusLog')
  BEGIN
      exec _pCreateAppModuleWithTable
        'tAppointmentStatusLog',
        1,
        NULL,
        NULL

      exec _pAddModelProperty
        'tAppointmentStatusLog',
        'Oid_Model',
        1

      exec _pAddModelProperty
        'tAppointmentStatusLog',
        'Appointment_ID_CurrentObject',
        2

      exec _pAddModelProperty
        'tAppointmentStatusLog',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatientAppointment',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatient_Vaccination',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatient_SOAP_Plan',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'Appointment_ID_FilingStatus',
        2

      exec _pAddModelProperty
        'tPatientAppointment',
        'Appointment_CancellationRemarks',
        1

      exec _pAddModelProperty
        'tPatient_Vaccination',
        'Appointment_CancellationRemarks',
        1

      exec _pAddModelProperty
        'tPatient_SOAP_Plan',
        'Appointment_CancellationRemarks',
        1

      exec _pAddModelProperty
        'tPatient_Wellness_Detail',
        'Appointment_CancellationRemarks',
        1

      exec _pAddModelProperty
        'tPatient_Wellness_Schedule',
        'Appointment_CancellationRemarks',
        1

      ALTER TABLE tPatientAppointment
        ADD DEFAULT 2 FOR Appointment_ID_FilingStatus

      ALTER TABLE tPatient_Vaccination
        ADD DEFAULT 2 FOR Appointment_ID_FilingStatus

      ALTER TABLE tPatient_SOAP_Plan
        ADD DEFAULT 2 FOR Appointment_ID_FilingStatus

      ALTER TABLE tPatient_Wellness_Detail
        ADD DEFAULT 2 FOR Appointment_ID_FilingStatus

      ALTER TABLE tPatient_Wellness_Schedule
        ADD DEFAULT 2 FOR Appointment_ID_FilingStatus
  END

exec _pRefreshAllViews

GO

ALTER VIEW dbo.vPatient_SOAP_Plan
AS
  SELECT H.*,
         ISNULL(item.Name, '')
         + CASE
             WHEN LEN(ISNULL(H.CustomItem, '')) = 0 then ''
             else ' ' + Char(10) + CHAR(13)
           END
         + ISNULL(H.CustomItem, '') Name_Item,
         fs.Name                    Appointment_Name_FilingStatus
  FROM   dbo.tPatient_SOAP_Plan H
         LEFT JOIN dbo.tItem item
                ON item.ID = H.ID_Item
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.Appointment_ID_FilingStatus

GO

CREATE OR
ALTER VIEW [dbo].[vPatientAppointment]
AS
  SELECT H.*,
         CONVERT(VARCHAR, H.DateStart, 0) DateStartString,
         CONVERT(VARCHAR, H.DateEnd, 0)   DateEndString,
         SOAPType.Name                    Name_SOAPType,
         client.Name                      Name_Client,
         patient.Name                     Name_Patient,
         fs.Name                          Name_FilingStatus,
         fsAppointment.Name               Appointment_Name_FilingStatus
  FROM   dbo.tPatientAppointment H
         LEFT JOIN dbo.tSOAPType SOAPType
                ON SOAPType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tFilingStatus fsAppointment
                ON fsAppointment.ID = H.Appointment_ID_FilingStatus

GO

ALTER VIEW vPatient_Wellness_DetailSchedule
as
  SELECT hed.ID                 ID_Patient_Wellness,
         hed.Code               Code,
         wellDetail.ID          ID_Patient_Wellness_Detail,
         wellSched.ID           ID_Patient_Wellness_Schedule,
         hed.ID_FilingStatus,
         hed.ID_Client,
         hed.ID_Patient,
         hed.ID_Company,
         wellDetail.Name_Item   Name_Item_Patient_Wellness_Detail,
         wellDetail.DateCreated DateCreated_Patient_Wellness_Detail,
         wellDetail.Comment     Comment_Patient_Wellness_Detail,
         wellSched.Date         Date_Patient_Wellness_Schedule,
         wellSched.DateSent,
         wellSched.IsSentSMS,
         wellDetail.Appointment_ID_FilingStatus,
         fsAppointment.Name     Appointment_Name_FilingStatus,
         wellDetail.Appointment_CancellationRemarks
  FROm   tPatient_Wellness hed
         INNER JOIN vPatient_Wellness_Detail wellDetail
                 on hed.ID = wellDetail.ID_Patient_Wellness
         INNER JOIN tPatient_Wellness_Schedule wellSched
                 on hed.ID = wellSched.ID_Patient_Wellness
         LEFT JOIN dbo.tFilingStatus fsAppointment
                ON fsAppointment.ID = wellDetail.Appointment_ID_FilingStatus
  where  wellDetail.Name_Item IS NOT NULL

GO

create or
alter view vAppointment_FilingStatus
AS
  Select ID,
         Name
  FROM   tFilingStatus
  WHERE  ID IN ( 2, 8, 13, 4 )

GO

CREATE OR
ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAPPlan.ID)
         + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.ID                                                                    Appointment_ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         ''                                                                                    FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client + ' - '
         + patientSOAP.Name_Patient                                                            Paticular,
         ISNULL(patientSOAP.Name_SOAPType, '')
         + ' - '
         + ISNULL(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.ID_Patient,
         patientSOAP.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                  ContactNumber,
         patientSOAPPlan.Appointment_ID_FilingStatus,
         patientSOAPPlan.Appointment_Name_FilingStatus,
         patientSOAPPlan.Appointment_CancellationRemarks
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN tClient client
                 on client.ID = patientSOAP.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.ID                                                             Appointment_ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         FORMAT(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         FORMAT(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         FORMAT(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         FORMAT(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         ISNULL(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client + ' - '
         + patientAppnt.Name_Patient                                                 Paticular,
         ISNULL(patientAppnt.Name_Patient, '')
         + ' - '
         + ISNULL(patientAppnt.Name_SOAPType, '') + ' '
         + ISNULL(patientAppnt.Comment, '' )                                         Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.ID_Patient,
         patientAppnt.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)        ContactNumber,
         patientAppnt.Appointment_ID_FilingStatus,
         patientAppnt.Appointment_Name_FilingStatus,
         patientAppnt.Appointment_CancellationRemarks
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN tClient client
                 on client.ID = patientAppnt.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )
  UNION ALL
  /* From tPatient_Wellness */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), wellness.ID)                                                                              ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), wellness.ID) + '|'
         + CONVERT(VARCHAR(MAX), wellness.ID_Client)
         + '|'
         + CONVERT(VARCHAR(MAX), wellSched.ID_Patient_Wellness_Detail)                                                     UniqueID,
         _model.Oid                                                                                                        Oid_Model,
         _model.Name                                                                                                       Name_Model,
         wellness.ID_Company,
         wellness.ID_Client,
         wellness.ID                                                                                                       ID_CurrentObject,
         wellSched.ID_Patient_Wellness_Detail                                                                              Appointment_ID_CurrentObject,
         wellSched.Date_Patient_Wellness_Schedule                                                                          DateStart,
         wellSched.Date_Patient_Wellness_Schedule                                                                          DateEnd,
         FORMAT(wellSched.Date_Patient_Wellness_Schedule, 'yyyy-MM-dd')                                                    FormattedDateStart,
         FORMAT(wellSched.Date_Patient_Wellness_Schedule, 'yyyy-MM-dd ')                                                   FormattedDateEnd,
         ''                                                                                                                FormattedDateStartTime,
         ''                                                                                                                FormattedDateEndTime,
         wellness.Code                                                                                                     ReferenceCode,
         wellness.Name_Client + ' - '
         + wellness.Name_Patient                                                                                           Paticular,
         ISNULL('', '') + ' - '
         + ISNULL(wellSched.Name_Item_Patient_Wellness_Detail, '')                                                         Description,
         dbo.fGetDateCoverageString(wellSched.Date_Patient_Wellness_Schedule, wellSched.Date_Patient_Wellness_Schedule, 1) TimeCoverage,
         wellness.ID_FilingStatus,
         Name_FilingStatus,
         wellness.Name_Client,
         wellness.ID_Patient,
         wellness.Name_Patient,
         dbo.[fGetMobileNumbers](client.ContactNumber, client.ContactNumber2)                                              ContactNumber,
         wellSched.Appointment_ID_FilingStatus,
         wellSched.Appointment_Name_FilingStatus,
         wellSched.Appointment_CancellationRemarks
  FROM   dbo.vPatient_Wellness wellness
         INNER JOIN dbo.vPatient_Wellness_DetailSchedule wellSched
                 ON wellSched.ID_Patient_Wellness = wellness.ID
         INNER JOIN tClient client
                 on client.ID = wellness.ID_Client
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_Wellness'
  WHERE  wellness.ID_FilingStatus NOT IN ( 4 )

GO

GO

CREATE OR
ALTER PROC pUpdateAppointmentStatus(@Oid_Model                    Varchar(MAX),
                                    @Appointment_ID_CurrentObject INT,
                                    @Appointment_ID_FilingStatus  INT,
                                    @ID_UserSession               INT,
                                    @Remarks                      Varchar(MAX))
AS
  BEGIN
      DECLARE @TableName VARCHAR(MAX) = ''
      DECLARE @ID_Company Int = 0
      DECLARE @ID_User Int = 0

      SELECT @ID_User = ID_User
      FROm   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @TableName = TableName
      FROm   _tModel
      WHERE  Oid = @Oid_Model

      IF( @TableName = 'tPatientAppointment' )
        BEGIN
            Update tPatientAppointment
            Set    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE if( @TableName = 'tPatient_SOAP' )
        BEGIN
            Update tPatient_SOAP_Plan
            Set    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_Wellness' )
        BEGIN
            Update tPatient_Wellness_Detail
            Set    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END

      /*Logs*/
      INSERT INTO [dbo].[tAppointmentStatusLog]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Appointment_ID_CurrentObject],
                   [Appointment_ID_FilingStatus])
      VALUES      ( 1,
                    @ID_Company,
                    @Remarks,
                    GETDATE(),
                    GETDATE(),
                    @ID_User,
                    @ID_User,
                    @Oid_Model,
                    @Appointment_ID_CurrentObject,
                    @Appointment_ID_FilingStatus)
  END

GO

CREATE OR
ALTER PROC pDoUpdateAppointmentStatus(@Oid_Model                    Varchar(MAX),
                                      @Appointment_ID_CurrentObject INT,
                                      @Appointment_ID_FilingStatus  INT,
                                      @ID_UserSession               INT,
                                      @Remarks                      Varchar(MAX))
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_User INT = 0;

      BEGIN TRY
          exec pUpdateAppointmentStatus
            @Oid_Model,
            @Appointment_ID_CurrentObject,
            @Appointment_ID_FilingStatus,
            @ID_UserSession,
            @Remarks
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END 
