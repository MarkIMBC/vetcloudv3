ALTER VIEW vPatient_ListView  
AS  
  SELECT ID,  
         ID_Company,  
         CASE  
           WHEN LEN(TRIM(ISNULL(CustomCode, ''))) = 0 THEN Code  
           ELSE CustomCode  
         END                                                                                      Code,  
         Name,  
         ID_Client,  
         Name_Client,  
         Email,  
         Species,  
         Name_Gender,  
         ContactNumber,  
         IsDeceased,  
         DateLastVisited,  
         ProfileImageThumbnailLocationFile,  
         ISNULL(IsActive, 0)                                                                      IsActive,  
         WaitingStatus_ID_FilingStatus,  
         WaitingStatus_Name_FilingStatus,  
         dbo.fGetLabelActionQueue(WaitingStatus_ID_FilingStatus, WaitingStatus_Name_FilingStatus) LabelActionQueue  ,
		 DateBirth
  FROM   dbo.vPatient   