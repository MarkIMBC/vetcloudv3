exec _pAddModelProperty
  'tPatient',
  'ProfileImageFile',
  1

exec _pRefreshAllViews

GO

ALTER FUNCTION dbo.fGetPaymentStatus (@PayableAmount   DECIMAL(18, 4),
                                      @RemainingAmount DECIMAL(18, 4))
RETURNS INT
AS
  BEGIN
      DECLARE @ID_FillingStatus INT = 0;
      DECLARE @FILINGSTATUS_PENDING INT = 2;
      DECLARE @FILINGSTATUS_PARTIALLYPAID INT = 11;
      DECLARE @FILINGSTATUS_FULLYPAID INT = 12;
      DECLARE @FILINGSTATUS_DONE INT = 13;

      IF( @PayableAmount = 0 )
        BEGIN
            SET @ID_FillingStatus = @FILINGSTATUS_DONE;
        END
      ELSE
        BEGIN
            IF @PayableAmount = @RemainingAmount
              SET @ID_FillingStatus = @FILINGSTATUS_PENDING;
            ELSE IF @PayableAmount > @RemainingAmount
               AND @RemainingAmount <> 0
              SET @ID_FillingStatus = @FILINGSTATUS_PARTIALLYPAID;
            ELSE IF @RemainingAmount = 0
              SET @ID_FillingStatus = @FILINGSTATUS_FULLYPAID;
        END

      RETURN @ID_FillingStatus;
  END;

GO

ALTER VIEW [dbo].[vAppointmentEvent]
AS
  /* From tPatient_SOAP */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID)                                               ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientSOAP.ID_Client)                                        UniqueID,
         _model.Oid                                                                            Oid_Model,
         _model.Name                                                                           Name_Model,
         patientSOAP.ID_Company,
         patientSOAP.ID_Client,
         patientSOAP.ID                                                                        ID_CurrentObject,
         patientSOAPPlan.DateReturn                                                            DateStart,
         patientSOAPPlan.DateReturn                                                            DateEnd,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd')                                      FormattedDateStart,
         FORMAT(patientSOAPPlan.DateReturn, 'yyyy-MM-dd ')                                     FormattedDateEnd,
         FORMAT(patientSOAPPlan.DateReturn, 'hh:mm tt')                                        FormattedDateStartTime,
         ''                                                                                    FormattedDateEndTime,
         patientSOAP.Code                                                                      ReferenceCode,
         patientSOAP.Name_Client                                                               Paticular,
         ISNULL(patientSOAP.Name_SOAPType, '')
         + ' - '
         + ISNULL(patientSOAPPlan.Name_Item, '')                                               Description,
         dbo.fGetDateCoverageString(patientSOAPPlan.DateReturn, patientSOAPPlan.DateReturn, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientSOAP.Name_Client,
         patientSOAP.Name_Patient
  FROM   dbo.vPatient_SOAP patientSOAP
         INNER JOIN dbo.vPatient_SOAP_Plan patientSOAPPlan
                 ON patientSOAPPlan.ID_Patient_SOAP = patientSOAP.ID
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatient_SOAP'
  WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
  UNION ALL
  /* From tPatientAppointment */
  SELECT CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID)                                    ID,
         CONVERT(VARCHAR(MAX), _model.Oid) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID) + '|'
         + CONVERT(VARCHAR(MAX), patientAppnt.ID_Client)                             UniqueID,
         _model.Oid                                                                  Oid_Model,
         _model.Name                                                                 Name_Model,
         patientAppnt.ID_Company,
         patientAppnt.ID_Client,
         patientAppnt.ID                                                             ID_CurrentObject,
         patientAppnt.DateStart                                                      DateStart,
         patientAppnt.DateEnd                                                        DateEnd,
         FORMAT(patientAppnt.DateStart, 'yyyy-MM-dd')                                FormattedDateStart,
         FORMAT(patientAppnt.DateEnd, 'yyyy-MM-dd')                                  FormattedDateEnd,
         FORMAT(patientAppnt.DateStart, 'hh:mm tt')                                  FormattedDateStartTime,
         FORMAT(patientAppnt.DateEnd, 'hh:mm tt')                                    FormattedDateEndTime,
         ISNULL(patientAppnt.Code, 'Patient Appt.')                                  ReferenceCode,
         patientAppnt.Name_Client                                                    Paticular,
         ISNULL(patientAppnt.Name_Patient, '')
         + ' - '
         + ISNULL(patientAppnt.Name_SOAPType, '')                                    Description,
         dbo.fGetDateCoverageString(patientAppnt.DateStart, patientAppnt.DateEnd, 1) TimeCoverage,
         ID_FilingStatus,
         Name_FilingStatus,
         patientAppnt.Name_Client,
         patientAppnt.Name_Patient
  FROM   dbo.vPatientAppointment patientAppnt
         INNER JOIN dbo._tModel _model
                 ON _model.TableName = 'tPatientAppointment'
  WHERE  patientAppnt.ID_FilingStatus IN ( 1, 13 )

GO

ALTER VIEW [dbo].[vPatient]
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         H.LastName + ', ' + H.FirstName + ' ' + H.MiddleName FullName,
         gender.Name                                          Name_Gender,
         country.Name                                         PhoneCode_Country,
         client.Name                                          Name_Client,
         client.IsActive                                      IsActive_Client,
         'https://api.veterinarycloudsystem.com/Content/Image/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageLocationFile,
         'https://api.veterinarycloudsystem.com/Content/Thumbnail/'
         + ISNULL(H.ProfileImageFile, 'blank-profile.jpg')    ProfileImageThumbnailLocationFile
  FROM   tPatient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN dbo.tCountry country
                ON H.ID_Country = country.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client;

GO

Alter VIEW vPatient_ListView
AS
  SELECT ID,
         ID_Company,
         Code,
         Name,
         Name_Client,
         Email,
         Species,
         Name_Gender,
         ContactNumber,
         IsDeceased,
         DateLastVisited,
         ProfileImageThumbnailLocationFile
  FROM   dbo.vPatient
  WHERE  ISNULL(IsActive, 0) = 1

GO

Alter VIEW dbo.vAttendingVeterinarian
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Company,
         hed.Name_Position
  FROM   vEmployee hed
  WHERE  hed.ID_Position IN ( 10, 4, 5, 6 )
         AND ISNULL(hed.IsActive, 0) = 1

GO

alter VIEW [dbo].[vzPatientBillingInvoiceReport]
AS
  SELECT biHed.ID,
         biHed.Code,
         biHed.Date,
         biHed.Name_Patient,
         biHed.BillingAddress,
         biHed.Name_FilingStatus,
         CASE
           WHEN LOWER(biHed.CreatedBy_Name_User) NOT like 'admin%' THEN biHed.CreatedBy_Name_User
           ELSE ''
         END                             CreatedBy_Name_User,
         CASE
           WHEN LOWER(biHed.CreatedBy_Name_User) NOT like 'admin%' THEN biHed.ApprovedBy_Name_User
           ELSE ''
         END                             ApprovedBy_Name_User,
         biHed.Name_TaxScheme,
         biHed.DateApproved,
         biHed.SubTotal,
         ISNULL(biHed.DiscountRate, 0)   DiscountRate,
         ISNULL(biHed.DiscountAmount, 0) DiscountAmount,
         biHed.TotalAmount,
         biHed.GrossAmount,
         biHed.VatAmount,
         biHed.NetAmount,
         biDetail.ID                     ID_BillingInvoice_Detail,
         biDetail.Name_Item,
         biDetail.Quantity,
         biDetail.UnitPrice,
         biDetail.Amount,
         biHed.ID_Company,
         biHed.Name_Client,
         company.ImageLogoLocationFilenamePath,
         company.Name                    Name_Company,
         company.Address                 Address_Company,
         company.ContactNumber           ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                           HeaderInfo_Company
  FROM   dbo.vBillingInvoice biHed
         LEFT JOIN dbo.vBillingInvoice_Detail biDetail
                ON biHed.ID = biDetail.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company

GO

ALTER PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,
                                    @ID_Client              INT = NULL,
                                    @ID_Patient             INT = NULL,
                                    @ID_SOAPType            INT = NULL,
                                    @ID_Patient_Confinement INT = NULL,
                                    @ID_Session             INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription;

      DECLARE @ID_User INT;
      DECLARE @AttendingPhysician_ID_Employee INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @Confinement_ID_SOAPType INT = 2;
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)
        + 'Respiratory Rate (brpm): ' + CHAR(9)
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)
        + 'Temperature: ' + CHAR(9) + CHAR(13)
        + 'Vet Visit History: ' + CHAR(9) + CHAR(13)
        + 'Vaccinations: ' + CHAR(9) + CHAR(13)
        + 'Deworming: ' + CHAR(9) + CHAR(13) + 'Vitamins: '
        + CHAR(9) + CHAR(13) + 'Spayed/neutered: '
        + CHAR(9) + CHAR(13) + 'Medications given: '
        + CHAR(9) + CHAR(13)
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)
       + 'Test Results: ' + CHAR(9) + CHAR(13)
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '
       + CHAR(9) + CHAR(13)
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + CHAR(9) + CHAR(13) + '  Wbc= ' + CHAR(9)
        + CHAR(13) + '  Lym= ' + CHAR(9) + CHAR(13)
        + '  Mon= ' + CHAR(9) + CHAR(13) + '  Neu= ' + CHAR(9)
        + CHAR(13) + '  Eos= ' + CHAR(9) + CHAR(13)
        + '  Bas= ' + CHAR(9) + CHAR(13) + '  Rbc= ' + CHAR(9)
        + CHAR(13) + '  Hgb= ' + CHAR(9) + CHAR(13)
        + '  Hct= ' + CHAR(9) + CHAR(13) + '  Mcv= ' + CHAR(9)
        + CHAR(13) + '  Mch= ' + CHAR(9) + CHAR(13)
        + '  Mchc ' + CHAR(9) + CHAR(13) + '  Plt= ' + CHAR(9)
        + CHAR(13) + '  Mpv= ' + CHAR(9) + CHAR(13) + CHAR(9)
        + CHAR(13) + 'Blood Chem: ' + CHAR(9) + CHAR(13)
        + '  Alt= ' + CHAR(9) + CHAR(13) + '  Alp= ' + CHAR(9)
        + CHAR(13) + '  Alb= ' + CHAR(9) + CHAR(13)
        + '  Amy= ' + CHAR(9) + CHAR(13) + '  Tbil= '
        + CHAR(9) + CHAR(13) + '  Bun= ' + CHAR(9) + CHAR(13)
        + '  Crea= ' + CHAR(9) + CHAR(13) + '  Ca= ' + CHAR(9)
        + CHAR(13) + '  Phos= ' + CHAR(9) + CHAR(13)
        + '  Glu= ' + CHAR(9) + CHAR(13) + '  Na= ' + CHAR(9)
        + CHAR(13) + '  K= ' + CHAR(9) + CHAR(13) + '  TP= '
        + CHAR(9) + CHAR(13) + '  Glob= ' + CHAR(9) + CHAR(13)
        + CHAR(9) + CHAR(13) + 'Microscopic Exam: '
        + CHAR(9) + CHAR(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT COUNT(*)
         FROM   vUSER
         where  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SET @ID_SOAPType = @Confinement_ID_SOAPType

            SELECT @ID_Client = patient.ID_Client,
                   @ID_Patient = ID_Patient,
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus
            FROM   tPatient_Confinement hed
                   LEFT JOIN tPatient patient
                          ON hed.ID_Patient = patient.ID
            WHERE  hed.ID = @ID_Patient_Confinement
        END

      IF ISNULL(@ID_Patient, 0) <> 0
        BEGIN
            SELECT @IsDeceased = Isnull(IsDeceased, 0),
                   @ID_Client = ID_Client
            FROM   tPatient
            WHERE  ID = @ID_Patient;
        END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT c.ImageRowIndex,
             ROW_NUMBER()
               OVER (
                 ORDER BY ImageNo) AS RowIndex,
             c.ImageNo,
             FilePath,
             c.Remark
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
      WHERE  ps.ID = @ID
             AND ( ISNULL(FilePath, '') <> ''
                    OR LEN(ISNULL(Remark, '')) > 0 )
      ORDER  BY c.ImageRowIndex ASC;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   patient.Name                               Name_Patient,
                   patient.Name_Client,
                   fs.Name                                    Name_FilingStatus,
                   soapType.Name                              Name_SOAPType,
                   AttendingPhysician.Name                    AttendingPhysician_Name_Employee,
                   confinement.Code                           Code_Patient_Confinement,
                   confinement.Date                           Date_Patient_Confinement,
                   confinement.DateDischarge                  DateDischarge_Patient_Confinement,
                   CASE
                     WHEN @Patient_Confinement_ID_FilingStatus = 0 THEN confinement.ID_FilingStatus
                     ELSE @Patient_Confinement_ID_FilingStatus
                   END,
                   confinement.ID_FilingStatus                ID_FilingStatus_Patient_Confinement,
                   confinement.Name_FilingStatus              Name_FilingStatus_Patient_Confinement,
                   dbo.fGetAge(patient.DateBirth, case
                                                    WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient
            FROM   (SELECT NULL                                 AS [_],
                           -1                                   AS [ID],
                           '-New-'                              AS [Code],
                           NULL                                 AS [Name],
                           1                                    AS [IsActive],
                           NULL                                 AS [ID_Company],
                           NULL                                 AS [Comment],
                           NULL                                 AS [DateCreated],
                           NULL                                 AS [DateModified],
                           @ID_User                             AS [ID_CreatedBy],
                           NULL                                 AS [ID_LastModifiedBy],
                           @ID_Client                           AS ID_Client,
                           @ID_Patient                          AS ID_Patient,
                           @AttendingPhysician_ID_Employee      AS AttendingPhysician_ID_Employee,
                           @ID_Patient_Confinement              AS ID_Patient_Confinement,
                           GETDATE()                            Date,
                           @FILED_ID_FilingStatus               ID_FilingStatus,
                           @IsDeceased                          IsDeceased,
                           @ID_SOAPType                         ID_SOAPType,
                           @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,
                           @ObjectiveTemplate                   ObjectiveTemplate,
                           @AssessmentTemplate                  AssessmentTemplate,
                           @LaboratoryTemplate                  LaboratoryTemplate,
                           @ClinicalExaminationTemplate         ClinicalExaminationTemplate,
                           @DiagnosisTemplate                   DiagnosisTemplate) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType
                   LEFT JOIN vPatient_Confinement confinement
                          ON confinement.ID = H.ID_Patient_Confinement
                   LEFT JOIN tEmployee AttendingPhysician
                          on AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @Patient_Confinement_ID_FilingStatus       Patient_Confinement_ID_FilingStatus,
                   patient.IsDeceased,
                   @ObjectiveTemplate                         ObjectiveTemplate,
                   @AssessmentTemplate                        AssessmentTemplate,
                   @LaboratoryTemplate                        LaboratoryTemplate,
                   @ClinicalExaminationTemplate               ClinicalExaminationTemplate,
                   @DiagnosisTemplate                         DiagnosisTemplate,
                   dbo.fGetAge(patient.DateBirth, case
                                                    WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          on h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID
  END;

GO

ALTER PROC [dbo].[pGetPatient_Confinement] @ID              INT = -1,
                                           @ID_Patient_SOAP INT = NULL,
                                           @ID_Session      INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Patient_Confinement_ItemsServices

      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Employee INT
      DECLARE @ID_Client INT
      DECLARE @ID_Patient INT
      DECLARE @ID_Company INT
      DECLARE @Confinement_ID_FilingStatus INT = 14

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF( ISNULL(@ID_Patient_SOAP, 0) <> 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   client.Name  Name_Client,
                   patient.Name Name_Patient,
                   fs.Name      Name_FilingStatus
            FROM   (SELECT NULL                         AS [_],
                           -1                           AS [ID],
                           GETDATE()                    AS [Date],
                           '-- NEW --'                  AS [Code],
                           NULL                         AS [Name],
                           1                            AS [IsActive],
                           @ID_Company                  AS [ID_Company],
                           NULL                         AS [Comment],
                           GETDATE()                    AS [DateCreated],
                           GETDATE()                    AS [DateModified],
                           NULL                         AS [ID_CreatedBy],
                           NULL                         AS [ID_LastModifiedBy],
                           @Confinement_ID_FilingStatus AS ID_FilingStatus,
                           @ID_Client                   AS ID_Client,
                           @ID_Patient                  AS ID_Patient,
                           @ID_Patient_SOAP             AS ID_Patient_SOAP) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
        END
      ELSE
        BEGIN
            DECLARE @BillingInvoice Table
              (
                 ID_BillingInvoice          INT,
                 ID_Patient_Confinement     INT,
                 Code_BillingInvoice        VARCHAR(MAX),
                 Status_BillingInvoice      VARCHAR(MAX),
                 TotalAmount_BillingInvoice DECIMAL(18, 2)
              )

            INSERT @BillingInvoice
            SELECT TOP 1 biHed.ID          ID_BillingInvoice,
                         biHed.ID_Patient_Confinement,
                         biHed.Code        Code_BillingInvoice,
                         biHed.Status      Status_BillingInvoice,
                         biHed.TotalAmount TotalAmount_BillingInvoice
            FROM   vBillingInvoice biHed
            WHERE  biHed.ID_Patient_Confinement = @ID
                   AND biHed.ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )

            SELECT H.*,
                   biHed.*
            FROM   vPatient_Confinement H
                   LEFT JOIN @BillingInvoice biHed
                          ON H.ID = biHed.ID_Patient_Confinement
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Confinement_ItemsServices
      WHERE  ID_Patient_Confinement = @ID
  END

GO

ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail;

      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT COUNT(*)
         FROM   vUSER
         where  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                        Name_FilingStatus,
                   client.Name                    Name_Client,
                   patient.Name                   Name_Patient,
                   client.Address                 BillingAddress,
                   attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GETDATE()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      if ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999 ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                           UnitPrice,
                   item.OtherInfo_DateExpiration                       DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP;
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(item.UnitPrice, 0)                        UnitPrice,
                   ISNULL(item.UnitCost, 0)                         UnitCost,
                   item.OtherInfo_DateExpiration                    DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement;
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Detail
            WHERE  ID_BillingInvoice = @ID;
        END
  END;

GO

ALTER PROC [dbo].[pModel_AfterSaved_Patient_Confinement] (@ID_CurrentObject VARCHAR(10),
                                                          @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @ID_Patient_SOAP INT;

            SELECT @ID_Company = ID_Company,
                   @ID_Patient_SOAP = ID_Patient_SOAP
            FROM   dbo.tPatient_Confinement
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Confinement';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Confinement
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;

            UPDATE tPatient_SOAP
            SET    ID_Patient_Confinement = @ID_CurrentObject
            WHERE  ID = @ID_Patient_SOAP
        END;

      SELECT @ID_Patient = ID_Patient
      FROM   tPatient_Confinement
      WHERE  ID = @ID_CurrentObject

      UPDATE tPatient_SOAP
      SET    ID_Patient = @ID_Patient
      WHERE  ID_Patient_Confinement = @ID_CurrentObject

      declare @IDs_Patient_Confinement typIntList

      INSERT @IDs_Patient_Confinement
      VALUES (@ID_CurrentObject)

      exec pUpdatePatient_Confinemen_BillingStatus
        @IDs_Patient_Confinement
  END;

GO 
