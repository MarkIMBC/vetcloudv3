ALTER PROCEDURE [dbo].[_pAddModelProperty] @TableName    VARCHAR(100),
                                           @PropertyName VARCHAR(100),
                                           @Type         INT = 1
AS
    BEGIN TRANSACTION;

    BEGIN TRY
        SET NOCOUNT ON

        DECLARE @Message     VARCHAR(MAX),
                @DataType    VARCHAR(200),
                @ID_Model    UNIQUEIDENTIFIER = NULL,
                @InfoSection UNIQUEIDENTIFIER = NULL

        IF NOT EXISTS (SELECT object_id
                       FROM   sys.tables
                       WHERE  name = @TableName)
          BEGIN
              SET @Message = @TableName + ' NOT EXISTS'

              RAISERROR (@Message,16,1 );

              RETURN
          END

        SELECT @ID_Model = Oid
        FROM   dbo.[_tModel]
        WHERE  TableName = @TableName

        IF ( @ID_Model = NULL )
          BEGIN
              SET @Message = @TableName + ' must exits in tModelTable'

              RAISERROR (@Message,16,1 );

              RETURN
          END

        SELECT @DataType = CASE @Type
                             WHEN 1 THEN 'VARCHAR(300)'
                             WHEN 2 THEN 'INT'
                             WHEN 3 THEN 'DECIMAL(18,4)'
                             WHEN 4 THEN 'BIT'
                             WHEN 5 THEN 'DATETIME'
                             WHEN 6 THEN 'DATE'
                             WHEN 7 THEN 'DATETIME'
                             WHEN 8 THEN 'UNIQUEIDENTIFIER'
                           --WHEN 9 THEN '' Doest Not Support List  
                           END

        DECLARE @SQL VARCHAR(300) = 'ALTER TABLE ' + @TableName + ' ADD '
          + @PropertyName + ' ' + @DataType

        EXEC( @SQL )

        DECLARE @Oid_Property UNIQUEIDENTIFIER = NEWID()

        INSERT INTO dbo.[_tModel_Property]
                    (Oid,
                     Name,
                     IsActive,
                     Caption,
                     ID_Model,
                     ID_PropertyType,
                     ID_CreatedBy,
                     ID_LastModifiedBy,
                     DateCreated,
                     DateModified)
        VALUES      ( @Oid_Property,
                      @PropertyName,
                      1,
                      NULL,
                      @ID_Model,
                      @Type,
                      1,
                      NULL,
                      GETDATE(),
                      NULL )

        INSERT INTO dbo.[_tDetailView_Detail]
                    (Oid,
                     Code,
                     Name,
                     IsActive,
                     Comment,
                     ID_DetailView,
                     ID_ModelProperty,
                     ID_CreatedBy,
                     ID_LastModifiedBy,
                     DateCreated,
                     DateModified,
                     ID_Section,
                     SeqNo)
        SELECT NEWID(),
               NULL,
               @PropertyName,
               1,
               NULL,
               Oid,
               @Oid_Property,
               1,
               NULL,
               GETDATE(),
               NULL,
               NULL,
               --                        ( SELECT    TOP 1 Oid  
               --                          FROM      dbo.[_tDetailView_Detail]  
               --                          WHERE     Name LIKE '%InfoSection%'  
               --                                    AND ID_ControlType = 13  
               --                                    AND ID_DetailView = D.Oid  
               --                        ), --TAB  
               -10000
        FROM   dbo.[_tDetailView] D
        WHERE  D.ID_Model = @ID_Model

        INSERT INTO dbo.[_tListView_Detail]
                    (Oid,
                     Code,
                     Name,
                     IsActive,
                     Comment,
                     ID_ListView,
                     ID_ModelProperty,
                     ID_CreatedBy,
                     ID_LastModifiedBy,
                     DateCreated,
                     DateModified,
                     IsVisible,
                     VisibleIndex)
        SELECT NEWID(),
               NULL,
               @PropertyName,
               1,
               NULL,
               Oid,
               @Oid_Property,
               1,
               NULL,
               GETDATE(),
               NULL,
               1,
               1000
        FROM   dbo.[_tListView]
        WHERE  ID_Model = @ID_Model

        PRINT @PropertyName + ' (' + @TableName
              + ') Successfully Added'

        SET NOCOUNT OFF
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER()    AS ErrorNumber,
               ERROR_SEVERITY()  AS ErrorSeverity,
               ERROR_STATE()     AS ErrorState,
               ERROR_PROCEDURE() AS ErrorProcedure,
               ERROR_LINE()      AS ErrorLine,
               ERROR_MESSAGE()   AS ErrorMessage;
        IF @@TRANCOUNT > 0
          ROLLBACK TRANSACTION;
    END CATCH;

    IF @@TRANCOUNT > 0
      COMMIT TRANSACTION;

GO

ALTER VIEW villingInvoiceWalkInList
AS
  SELECT H.*
  FROM   vBillingInvoiceWalkInList H

GO

IF COL_LENGTH('dbo.tCompany', 'IsShowFooter') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tCompany',
        'IsShowFooter',
        4;

      exec _pRefreshAllViews;

      ALTER TABLE dbo.tCompany
        ADD CONSTRAINT DF_Company_IsShowFooter DEFAULT 1 FOR IsShowFooter;

      Update tCompany
      SET    IsShowFooter = 1;
  END

GO

IF COL_LENGTH('dbo.tPatient_SOAP', 'ID_Client') IS NULL
  BEGIN
      EXEC _pAddModelProperty
        'tPatient_SOAP',
        'ID_Client',
        2;

      exec _pRefreshAllViews;
  END

GO

CREATE OR
ALTER VIEW vItem_Supplier
as
  SELECT hed.*,
         s.Name Name_Supplier
  FROM   tItem_Supplier hed
         INNER JOIN tSupplier s
                 ON hed.ID_Supplier = s.ID
         INNER JOIN tItem item
                 ON hed.ID_Item = item.ID

GO

ALTER PROC dbo.pGetItem @ID          INT = -1,
                        @ID_ItemType INT = NULL,
                        @ID_Session  INT = NULL
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT '_',
             '' Item_Supplier;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   itemType.Name Name_ItemType
            FROM   (SELECT -1           AS [ID],
                           NULL         AS [Code],
                           NULL         AS [Name],
                           1            AS [IsActive],
                           NULL         AS [ID_Company],
                           NULL         AS [Comment],
                           NULL         AS [DateCreated],
                           NULL         AS [DateModified],
                           NULL         AS [ID_CreatedBy],
                           NULL         AS [ID_LastModifiedBy],
                           @ID_ItemType ID_ItemType) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tItemType itemType
                          ON itemType.ID = H.ID_ItemType;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   CASE
                     WHEN H.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(H.OtherInfo_DateExpiration, '', 'Expired')
                     ELSE ''
                   END                                                  RemainingBeforeExpired,
                   DATEDIFF(DAY, GETDATE(), H.OtherInfo_DateExpiration) RemainingDays
            FROM   vItem H
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROm   vItem_Supplier
      WHERE  ID_Item = @ID
  END;

GO

ALTER PROC [dbo].[pGetPaymentTransactionPrintReceiptLayout] (@ID_PaymentTransaction INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0
      DECLARE @ID_BillingInvoice INT = 0

      SELECT @ID_Company = ID_Company,
             @ID_BillingInvoice = ID_BillingInvoice,
             @ID_PaymentMode = ID_PaymentMethod,
             @Code_PaymentTranaction = Code,
             @PayableAmount = PayableAmount,
             @RemainingAmount = RemainingAmount,
             @ChangeAmount = ChangeAmount,
             @CashAmount = CashAmount,
             @ReferenceTransactionNumber = ISNULL(ReferenceTransactionNumber, ''),
             @GCashAmount = GCashAmount,
             @Name_CardType = ISNULL(Name_CardType, ''),
             @CardNumber = ISNULL(CardNumber, ''),
             @CardHolderName = ISNULL(CardHolderName, ''),
             @CardAmount = CardAmount,
             @CheckNumber = CheckNumber,
             @CheckAmount = CheckAmount,
             @PaymentAmount = PaymentAmount
      FROM   vPaymentTransaction
      WHERE  ID = @ID_PaymentTransaction

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px')
      FROM   vCompany
      WHERE  ID = @ID_Company

      SELECT @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @TotalItemDiscountAmount_BillingInvoice = TotalItemDiscountAmount,
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, '')
      FROM   vBillingInvoice bi
      WHERE  bi.ID = @ID_BillingInvoice

      DECLARE @style VARCHAR(MAX)= '                      
  body{                      
   margin: 0px;                      
   padding: 0px;                      
   font-family:  arial, sans-serif;                      
   font-weight: normal;                      
   font-style: normal;                      
   font-size: ' + @POSReceiptFontSize
        + ';                      
  }                      
                      
  .logo{                      
                      
   width: 120px;                      
   margin-bottom: 10px;                      
  }                      
                      
  .receipt-container{                      
     width: 100vw;                  
  position: relative;                  
  left: 50%;                  
  right: 50%;                  
  margin-left: -50vw;                  
  margin-right: -50vw;                  
  }                      
                      
  .company-logo{                      
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                      
   text-align: center;                      
   word-wrap: break-word;                      
   margin-bottom: 12px;                      
  }                      
                      
  .company-name{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
  }                      
                      
  .company-address{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
   font-size: 11px                      
  }                      
                      
  .company-contactnum-container{                      
   display: block;                      
   text-align: center;                      
   word-wrap: break-word;                      
   font-size: 11px                      
  }                      
                      
  .company-contactnum{                      
   text-align: center;                      
   word-wrap: break-word;                      
                      
  }                      
          
  .float-left{                      
   float: left;                      
  }                      
                      
  .float-right{                      
   float: right;                      
  }                      
            
  .hide{     
 display: none;    
  }    
                         
  .clearfix {                      
   overflow: auto;            
  }                      
                      
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                   
                      
  .display-block{                      
  word-wrap: break-word;                  
 display: block;                      
  }                      
                      
  .title{                      
   text-align: center;                      
   word-wrap: break-word;                      
   display: block;                      
   padding-top: 15px;                      
   padding-bottom: 15px;                      
  }                      
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'

      IF( @IsShowHeader = 1 )
        BEGIN
            /*Billing Invoice Layout*/
            IF ( @IsShowPOSReceiptLogo = 1 )
              BEGIN
                  SET @content = @content
                                 + '<div class="company-logo"><img src="'
                                 + @ImageLogoLocationFilenamePath_Company
                                 + '" class="logo"></div>'
              END

            SET @content = @content + '<center><div class="bold">'
                           + @Name_Company + '</div>'
            SET @content = @content + '<div>' + @Address_Company + '</div>'
            SET @content = @content + '<div>' + @ContactNumber_Company
                           + '</div></center>'
        END

      SET @content = @content
                     + '<div class="title bold">INVOICE</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('BI #:', @Code_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(Quantity, '#,#0'), FORMAT(Amount, '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Itm Disc Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Disc. Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));
      /*PAYMENT*/
      SET @content = @content
                     + '<div class="title bold">PAYMENT</div>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('PT #', @Code_PaymentTranaction);

      /*----------------PAYMENT METHODS--------------------*/
      IF( @ID_PaymentMode = @Cash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Cash Amount', FORMAT(@CashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @GCash_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Ref. No.', @ReferenceTransactionNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('G-Cash Amount', FORMAT(@GCashAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @DebitCreditCard_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Type', @Name_CardType);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card #', @CardNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Holder', @CardHolderName);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Card Amt.', FORMAT(@CardAmount, '#,#0.00'));
        END
      ELSE IF( @ID_PaymentMode = @Check_ID_PaymentMethod )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check No.', @CheckNumber);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Check Amt.', FORMAT(@CheckAmount, '#,#0.00'));
        END

      /*----------------PAYMENT METHODS END----------------*/
      SET @content = @content + '<br/>'
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Balance', FORMAT(@PayableAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Payment', FORMAT(@PaymentAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining', FORMAT(@RemainingAmount, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Change', FORMAT(@ChangeAmount, '#,#0.00'));

      /*END Main Layout */
      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO

ALTER VIEW [dbo].[vPatient_SOAP]
AS
  SELECT H.*,
         UC.Name                                           AS CreatedBy,
         UM.Name                                           AS LastModifiedBy,
         UC.Name                                           AS Name_CreatedBy,
         UM.Name                                           AS Name_LastModifiedBy,
         CONVERT(VARCHAR(100), H.Date, 101)                DateString,
         soapType.Name                                     Name_SOAPType,
         patient.Name                                      Name_Patient,
         patient.Name_Client                               Name_Client,
         approvedUser.Name                                 Name_ApprovedBy,
         canceledUser.Name                                 Name_CanceledBy,
         fs.Name                                           Name_FilingStatus,
         LTRIM(RTRIM(ISNULL(client.ContactNumber, '') + ' '
                     + ISNULL(client.ContactNumber2, ''))) ContactNumber_Client,
         attendingPhysicianEmloyee.Name                    AttendingPhysician_Name_Employee,
         confinement.Code                                  Code_Patient_Confinement,
         confinement.Date                                  Date_Patient_Confinement,
         confinement.DateDischarge                         DateDischarge_Patient_Confinement,
         confinement.ID_FilingStatus                       ID_FilingStatus_Patient_Confinement,
         confinement.Name_FilingStatus                     Name_FilingStatus_Patient_Confinement,
         REPLACE(H.Diagnosis, CHAR(13), '<br/>')           DiagnosisHTML,
         billfs.Name                                       BillingInvoice_Name_FilingStatus
  FROM   dbo.tPatient_SOAP H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.vPatient patient
                ON H.ID_Patient = patient.ID
         LEFT JOIN dbo.vClient client
                ON patient.ID_Client = client.ID
         LEFT JOIN dbo.tSOAPType soapType
                ON soapType.ID = H.ID_SOAPType
         LEFT JOIN dbo.tUser approvedUser
                ON approvedUser.ID = H.ID_ApprovedBy
         LEFT JOIN dbo.tUser canceledUser
                ON canceledUser.ID = H.ID_CanceledBy
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN vPatient_Confinement confinement
                ON confinement.ID = H.ID_Patient_Confinement
         LEFT JOIN dbo.tFilingStatus billfs
                ON billfs.ID = H.BillingInvoice_ID_FilingStatus

GO

ALTER PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,
                                    @ID_Client              INT = NULL,
                                    @ID_Patient             INT = NULL,
                                    @ID_SOAPType            INT = NULL,
                                    @ID_Patient_Confinement INT = NULL,
                                    @ID_Session             INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS LabImages,
             '' AS Patient_SOAP_Plan,
             '' AS Patient_SOAP_Prescription,
             '' AS Patient_SOAP_Treatment;

      DECLARE @FilingStatus_Confined INT = 14
      DECLARE @FilingStatus_Discharged INT = 15
      DECLARE @ID_User INT;
      DECLARE @ID_Company INT;
      DECLARE @AttendingPhysician_ID_Employee INT;
      DECLARE @ID_Warehouse INT;
      DECLARE @FILED_ID_FilingStatus INT = 1;
      DECLARE @Consultation_ID_SOAPType INT = 1;
      DECLARE @Confinement_ID_SOAPType INT = 2;
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;
      DECLARE @IsDeceased BIT = 1;
      DECLARE @PrimaryComplaintTemplate VARCHAR(MAX) = ''
        + 'Eyes - Normal/Discharge/Infection/Sclelorosis/Inflamed/Eyelid tumor = '
        + Char(9) + Char(13)
        + 'Ears - Normal/Inflamed/Tumor/Dirty/Painful = '
        + Char(9) + Char(13)
        + 'Nose - Normal/Discharge = ' + Char(9)
        + Char(13)
        + 'Mouth,Teeth,Gums - Normal/Tumors/Gingivitis/Periodontitis/Tartar buildup/Loose teeth/Bite over/Under = '
        + Char(9) + Char(13)
        + 'Coat and skin - Normal/Scaly/Infection/Matted/Pruritus/Hair loss/Mass = '
        + Char(9) + Char(13)
        + 'Muskuloskeletal - Normal/Joint problems/Lameness/Nail Problems/Ligaments = '
        + Char(9) + Char(13)
        + 'Lungs - Normal/Breathing Difficulty/Rapid Respiration/ Tracheal Pinch + - / Congestion / Abn Sound = '
        + Char(9) + Char(13)
        + 'Heart - Normal/Murmur/Arrythmia/Muffled/Fast/Slow = '
        + Char(9) + Char(13)
        + 'GI System - Normal/Excessive Gas/Parasites/Abn Feces/Anorexia = '
        + Char(9) + Char(13)
        + 'Abdomen - Normal/Abnormal Mass/Tense/Painful/Bloated/Fluid/Hernia/Enlarged Organ = '
        + Char(9) + Char(13)
        + 'Urogentital - Normal/Abn Urination/Genital Discharge/Blood Seen/Abn Testicle = '
        + Char(9) + Char(13)
        + 'Neurological - Normal/Eye Reflex/Pain Reflex = '
        + Char(9) + Char(13)
        + 'Lymph Nodes - Normal/Submandubular/Prescapular/Axillary/Popiteal/Inguinal = '
        + Char(9) + Char(13)
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + Char(9) + Char(13)
        + 'Respiratory Rate (brpm): ' + Char(9)
        + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
        + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
        + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
        + 'Lymph Nodes: ' + Char(9) + Char(13)
        + 'Palpebral Reflex: ' + Char(9) + Char(13)
        + 'Temperature: ' + Char(9) + Char(13)
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + Char(9) + Char(13)
        + 'Respiratory Rate (brpm): ' + Char(9)
        + Char(13) + 'Weight (kg): ' + Char(9) + Char(13)
        + 'Temperature: ' + Char(9) + Char(13)
        + 'Length (cm): ' + Char(9) + Char(13) + 'CRT: '
        + Char(9) + Char(13) + 'BCS: ' + Char(9) + Char(13)
        + 'Lymph Nodes: ' + Char(9) + Char(13)
        + 'Palpebral Reflex: ' + Char(9) + Char(13)
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + Char(9)
       + Char(13) + 'Notes: ' + Char(9) + Char(13)
       + 'Test Results: ' + Char(9) + Char(13)
       + 'Final Diagnosis: ' + Char(9) + Char(13)
       + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
       + Char(9) + Char(13)
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + Char(9)
       + Char(13) + 'Notes: ' + Char(9) + Char(13)
       + 'Test Results: ' + Char(9) + Char(13)
       + 'Final Diagnosis: ' + Char(9) + Char(13)
       + 'Prognosis: ' + Char(9) + Char(13) + 'Category: '
       + Char(9) + Char(13)
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + Char(9) + Char(13) + '  Wbc= ' + Char(9)
        + Char(13) + '  Lym= ' + Char(9) + Char(13)
        + '  Mon= ' + Char(9) + Char(13) + '  Neu= ' + Char(9)
        + Char(13) + '  Eos= ' + Char(9) + Char(13)
        + '  Bas= ' + Char(9) + Char(13) + '  Rbc= ' + Char(9)
        + Char(13) + '  Hgb= ' + Char(9) + Char(13)
        + '  Hct= ' + Char(9) + Char(13) + '  Mcv= ' + Char(9)
        + Char(13) + '  Mch= ' + Char(9) + Char(13)
        + '  Mchc ' + Char(9) + Char(13) + '  Plt= ' + Char(9)
        + Char(13) + '  Mpv= ' + Char(9) + Char(13) + Char(9)
        + Char(13) + 'Blood Chem: ' + Char(9) + Char(13)
        + '  Alt= ' + Char(9) + Char(13) + '  Alp= ' + Char(9)
        + Char(13) + '  Alb= ' + Char(9) + Char(13)
        + '  Amy= ' + Char(9) + Char(13) + '  Tbil= '
        + Char(9) + Char(13) + '  Bun= ' + Char(9) + Char(13)
        + '  Crea= ' + Char(9) + Char(13) + '  Ca= ' + Char(9)
        + Char(13) + '  Phos= ' + Char(9) + Char(13)
        + '  Glu= ' + Char(9) + Char(13) + '  Na= ' + Char(9)
        + Char(13) + '  K= ' + Char(9) + Char(13) + '  TP= '
        + Char(9) + Char(13) + '  Glob= ' + Char(9) + Char(13)
        + Char(9) + Char(13) + 'Microscopic Exam: '
        + Char(9) + Char(13)

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @Compostela_ID_Company INT = 65
      DECLARE @DandCVeterinaryClinic_ID_Company INT = 63
      DECLARE @GarciaVeterinaryClinicVeterinaryClinic_ID_Company INT = 75
      DECLARE @PetlinkGuiguintoID_Company INT = 29
      DECLARE @VenVet_ID_Company INT = 12
      DECLARE @FDMD_ID_Company INT = 70

      IF( @ID_Company = @FDMD_ID_Company )
        BEGIN
            SET @LaboratoryTemplate = 'PID=  ' + Char(9) + Char(13) + 'SID=  ' + Char(9)
                                      + Char(13) + 'Needle=  ' + Char(9) + Char(13)
                                      + 'Type=  ' + Char(9) + Char(13) + 'WBC=  ' + Char(9)
                                      + Char(13) + 'LYM=  ' + Char(9) + Char(13) + 'MON=  '
                                      + Char(9) + Char(13) + 'NEU=  ' + Char(9) + Char(13)
                                      + 'EOS=  ' + Char(9) + Char(13) + 'BAS=  ' + Char(9)
                                      + Char(13) + 'LYM%=  ' + Char(9) + Char(13)
                                      + 'MON%=  ' + Char(9) + Char(13) + 'NEU%=  ' + Char(9)
                                      + Char(13) + 'EOS%=  ' + Char(9) + Char(13)
                                      + 'BAS%=  ' + Char(9) + Char(13) + 'RBC=  ' + Char(9)
                                      + Char(13) + 'HGB=  ' + Char(9) + Char(13) + 'HCT=  '
                                      + Char(9) + Char(13) + 'MCV=  ' + Char(9) + Char(13)
                                      + 'MCH=  ' + Char(9) + Char(13) + 'MCHC=  ' + Char(9)
                                      + Char(13) + 'RDWC=  ' + Char(9) + Char(13)
                                      + 'RDWS=  ' + Char(9) + Char(13) + 'PLT=  ' + Char(9)
                                      + Char(13) + 'MPV PCT=  ' + Char(9) + Char(13)
                                      + 'PDWc PDWS=  ' + Char(9) + Char(13)
        END

      IF( @ID_Company = @VenVet_ID_Company )
        BEGIN
            SET @PrimaryComplaintTemplate= '' + 'Eyes = ' + Char(9) + Char(13) + 'Ears = '
                                           + Char(9) + Char(13) + 'Nose = ' + Char(9) + Char(13)
                                           + 'Mouth,Teeth,Gums = ' + Char(9) + Char(13)
                                           + 'Coat and skin = ' + Char(9) + Char(13)
                                           + 'Muskuloskeletal = ' + Char(9) + Char(13)
                                           + 'Lungs = ' + Char(9) + Char(13) + 'Heart = '
                                           + Char(9) + Char(13) + 'GI System = ' + Char(9)
                                           + Char(13) + 'Abdomen = ' + Char(9) + Char(13)
                                           + 'Urogentital = ' + Char(9) + Char(13)
                                           + 'Neurological = ' + Char(9) + Char(13)
                                           + 'Lymph Nodes = ' + Char(9) + Char(13)
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @DandCVeterinaryClinic_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 345
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @GarciaVeterinaryClinicVeterinaryClinic_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 426
        END

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @PetlinkGuiguintoID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 216
        END

      /* Patient Confinement */
      IF(SELECT Count(*)
         FROM   tPatient_Confinement
         WHERE  ID_Patient_SOAP = @ID
                AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )) > 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID
            FROM   tPatient_Confinement
            WHERE  ID_Patient_SOAP = @ID
                   AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )
        END

      IF( IsNull(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            DECLARE @confinedPatientCount INT = 0

            SET @ID_SOAPType = @Confinement_ID_SOAPType

            SELECT @ID_Client = hed.ID_Client,
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus
            FROM   tPatient_Confinement hed
            WHERE  hed.ID = @ID_Patient_Confinement

            SELECT @confinedPatientCount = COUNT(*)
            FROM   tPatient_Confinement_Patient confPatient
            WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
            GROUP  BY confPatient.ID_Patient_Confinement

            if( @confinedPatientCount = 1 )
              BEGIN
                  SELECT @ID_Patient = ID_Patient
                  FROM   tPatient_Confinement_Patient confPatient
                  WHERE  confPatient.ID_Patient_Confinement = @ID_Patient_Confinement
              END
        END

      /* Patient Confinement END*/
      IF IsNull(@ID_Patient, 0) <> 0
        BEGIN
            SELECT @IsDeceased = IsNull(IsDeceased, 0),
                   @ID_Client = ID_Client
            FROM   tPatient
            WHERE  ID = @ID_Patient;
        END

      DECLARE @LabImage TABLE
        (
           ImageRowIndex INT,
           RowIndex      INT,
           ImageNo       VARCHAR(MAX),
           FilePath      VARCHAR(MAX),
           Remark        VARCHAR(MAX)
        );

      INSERT @LabImage
             (ImageRowIndex,
              RowIndex,
              ImageNo,
              FilePath,
              Remark)
      SELECT ImageRowIndex,
             RowIndex,
             ImageNo,
             FilePath,
             Remark
      FROM   dbo.fGetPatient_Soap_LaboratoryImages(@ID)

      DECLARE @IsLastConfinmentSOAP BIT = 0
      DECLARE @maxDate DATETIME

      SELECT @maxDate = MaxDate
      FROM   vPatient_Confinement_MaxSOAP
      WHERE  ID_Patient_SOAP = @ID

      IF( @maxDate IS NOT NULL
          AND @Patient_Confinement_ID_FilingStatus = @FilingStatus_Discharged )
        SET @IsLastConfinmentSOAP = 1
      ELSE
        SET @IsLastConfinmentSOAP = 0

      IF ( @ID = -1 )
        BEGIN
            SET @ID_SOAPType = IsNull(@ID_SOAPType, @Consultation_ID_SOAPType)

            SELECT H.*,
                   patient.Name                               Name_Patient,
                   client.Name                                Name_Client,
                   fs.Name                                    Name_FilingStatus,
                   soapType.Name                              Name_SOAPType,
                   AttendingPhysician.Name                    AttendingPhysician_Name_Employee,
                   confinement.Code                           Code_Patient_Confinement,
                   confinement.Date                           Date_Patient_Confinement,
                   confinement.DateDischarge                  DateDischarge_Patient_Confinement,
                   CASE
                     WHEN @Patient_Confinement_ID_FilingStatus = 0 THEN confinement.ID_FilingStatus
                     ELSE @Patient_Confinement_ID_FilingStatus
                   END,
                   confinement.ID_FilingStatus                ID_FilingStatus_Patient_Confinement,
                   confinement.Name_FilingStatus              Name_FilingStatus_Patient_Confinement,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient
            FROM   (SELECT NULL                                 AS [_],
                           -1                                   AS [ID],
                           '-New-'                              AS [Code],
                           NULL                                 AS [Name],
                           1                                    AS [IsActive],
                           NULL                                 AS [ID_Company],
                           NULL                                 AS [Comment],
                           NULL                                 AS [DateCreated],
                           NULL                                 AS [DateModified],
                           @ID_User                             AS [ID_CreatedBy],
                           NULL                                 AS [ID_LastModifiedBy],
                           @ID_Client                           AS ID_Client,
                           @ID_Patient                          AS ID_Patient,
                           @AttendingPhysician_ID_Employee      AS AttendingPhysician_ID_Employee,
                           @ID_Patient_Confinement              AS ID_Patient_Confinement,
                           GetDate()                            Date,
                           @FILED_ID_FilingStatus               ID_FilingStatus,
                           @IsDeceased                          IsDeceased,
                           @ID_SOAPType                         ID_SOAPType,
                           @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,
                           @PrimaryComplaintTemplate            PrimaryComplaintTemplate,
                           @ObjectiveTemplate                   ObjectiveTemplate,
                           @AssessmentTemplate                  AssessmentTemplate,
                           @LaboratoryTemplate                  LaboratoryTemplate,
                           @ClinicalExaminationTemplate         ClinicalExaminationTemplate,
                           @DiagnosisTemplate                   DiagnosisTemplate,
                           @IsLastConfinmentSOAP                IsLastConfinmentSOAP) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.vClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.vPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tFilingStatus fs
                          ON fs.ID = H.ID_FilingStatus
                   LEFT JOIN dbo.tSOAPType soapType
                          ON soapType.ID = H.ID_SOAPType
                   LEFT JOIN vPatient_Confinement confinement
                          ON confinement.ID = H.ID_Patient_Confinement
                   LEFT JOIN tEmployee AttendingPhysician
                          ON AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   @Patient_Confinement_ID_FilingStatus       Patient_Confinement_ID_FilingStatus,
                   patient.IsDeceased,
                   @PrimaryComplaintTemplate                  PrimaryComplaintTemplate,
                   @ObjectiveTemplate                         ObjectiveTemplate,
                   @AssessmentTemplate                        AssessmentTemplate,
                   @LaboratoryTemplate                        LaboratoryTemplate,
                   @ClinicalExaminationTemplate               ClinicalExaminationTemplate,
                   @DiagnosisTemplate                         DiagnosisTemplate,
                   dbo.fGetAge(patient.DateBirth, CASE
                                                    WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                                    ELSE NULL
                                                  END, 'N/A') Age_Patient,
                   @IsLastConfinmentSOAP                      IsLastConfinmentSOAP
            FROM   dbo.vPatient_SOAP H
                   LEFT JOIN tPatient patient
                          ON h.ID_Patient = patient.ID
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROM   @LabImage
      ORDER  BY ImageRowIndex DESC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Plan
      WHERE  ID_Patient_SOAP = @ID
      ORDER  BY DateReturn ASC;

      SELECT *
      FROM   dbo.vPatient_SOAP_Prescription
      WHERE  ID_Patient_SOAP = @ID

      SELECT *
      FROM   dbo.vPatient_SOAP_Treatment
      WHERE  ID_Patient_SOAP = @ID
  END;

GO

-- Update ID_Client from patient Record of SOAP
UPdate tPatient_SOAP
SET    ID_Client = patient.ID_Client
FROm   tPatient_SOAP _soap
       iNNER JOIN tPatient patient
               on _soap.ID_Patient = patient.ID
where  ISNULL(_soap.ID_Client, 0) = 0

SELECT COUNT(*)
FROM   tPatient_SOAP _soap
where  ISNULL(_soap.ID_Client, 0) = 0 
