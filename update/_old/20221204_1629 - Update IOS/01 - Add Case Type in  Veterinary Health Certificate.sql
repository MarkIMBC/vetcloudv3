GO

IF COL_LENGTH('tVeterinaryHealthCertificate', 'CaseType') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tVeterinaryHealthCertificate',
        'CaseType',
        1
  END

GO

exec _pRefreshAllViews

GO

ALTER FUNCTION [dbo].[fGetVeterinaryHealthCertificateVaccinationOptionComment](@VaccinationOptionComment VARCHAR(MAX),
                                                                               @DateVaccinated           DateTime,
                                                                               @Name_Item                VARCHAR(MAX),
                                                                               @LotNumber                VARCHAR(MAX),
                                                                               @CaseType                 VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Date*****/', '<u>'
                                                                                             + IsNULL(FORMAT(@DateVaccinated, 'MM/dd/yyyy'), '')
                                                                                             + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Vaccination*****/', '<u>' + IsNULL(@Name_Item, '') + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Serial/Lot Number*****/', '<u>' + IsNULL(@LotNumber, '') + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****CaseType*****/', '<u>' + IsNULL(@CaseType, '') + '</u>')

      RETURN @VaccinationOptionComment
  END

GO

ALTER VIEW [dbo].[vzVeterinaryHealthClinicReport]
AS
  SELECT vetcert.ID,
         vetcert.Code,
         vetcert.Date,
         vetcert.DestinationAddress,
         vetcert. Color,
         vetcert. Weight,
         vetcert.ID_Item,
         vetcert.Name_Item,
         CASE
           WHEN len(IsNull(vetcert.LotNumber, '')) > 0 THEN ' with Serial / Lot Number'
                                                            + vetcert.LotNumber
           ELSE ''
         END                                                                                                                                                                     LotNumber,
         patient.ID                                                                                                                                                              ID_Patient,
         patient.NAME                                                                                                                                                            Name_Patient,
         IsNull(Species, '')                                                                                                                                                     Name_Species,
         IsNull(Name_Gender, '')                                                                                                                                                 Name_Gender,
         patient.Microchip,
         IsNull(FORMAT(patient.DateBirth, 'MM/dd/yyyy'), '')                                                                                                                     Birthdate,
         dbo.fGetAge(patient.DateBirth, CASE
                                          WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                                                              Age_Patient,
         IsNull(client.NAME, '')                                                                                                                                                 Name_Client,
         IsNull(client.Address, '')                                                                                                                                              Address_Client,
         IsNull(client.ContactNumber, '')                                                                                                                                        ContactNumber_Client,
         IsNull(client.ContactNumber2, '')                                                                                                                                       ContactNumber2_Client,
         IsNull(client.Email, '')                                                                                                                                                Email_Client,
         company.ID                                                                                                                                                              ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.NAME                                                                                                                                                            Name_Company,
         company.Address                                                                                                                                                         Address_Company,
         company.ContactNumber                                                                                                                                                   ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                                                                   HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         _AttendingPhysician.TINNumber,
         _AttendingPhysician.PTR,
         _AttendingPhysician.PRCLicenseNumber,
         CASE
           WHEN _AttendingPhysician.DatePRCExpiration IS NOT NULL THEN FORMAT(_AttendingPhysician.DatePRCExpiration, 'MM/dd/yyyy')
           ELSE ''
         END                                                                                                                                                                     DatePRCExpiration,
         CASE
           WHEN vetCert.DateVaccination IS NOT NULL THEN FORMAT(vetCert.DateVaccination, 'MM/dd/yyyy')
           ELSE '                '
         END                                                                                                                                                                     DateVaccination,
         dbo.fGetVeterinaryHealthCertificateVaccinationOptionComment(Comment_VaccinationOption, vetCert.DateVaccination, vetCert.Name_Item, vetCert.LotNumber, vetCert.CaseType) Comment_VaccinationOption,
         ISNULL(vetCert.ValidityDayCount, 0)                                                                                                                                     ValidityDayCount,
         ISNULL(vetCert.CaseType, 'Rabies')                                                                                                                                      CaseType
  FROM   vVeterinaryHealthCertificate vetCert
         LEFT JOIN vPatient patient
                ON vetCert.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = vetCert.ID_Company
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = vetCert.AttendingPhysician_ID_Employee

GO

Create OR
ALTER VIEW vSMSListActive
AS
  SELECT H.*
  FROM   vSMSList H

--WHERE IsActive = 1
GO

Create OR
ALTER VIEW vSMSListInactive
AS
  SELECT H.*
  FROM   vSMSList H

--WHERE IsActive = 1
GO

Update tVaccinationOption
SET    Comment = 'This further certifies that I have vaccinated the above-described dog/cat against /*****CaseType*****/ on /*****Date*****/ using /*****Vaccination*****/ with Serial/Lot Number /*****Serial/Lot Number*****/.'
where  ID = 1

Update tVaccinationOption
SET    Comment = 'This further certifies that the above-described dog/cat was vaccinated against /*****CaseType*****/ on /*****Date*****/ using /*****Vaccination*****/  with Serial/Lot Number  /*****Serial/Lot Number*****/.'
where  ID = 2

Update tVeterinaryHealthCertificate
set    CaseType = 'Rabies'
WHERE  CaseType IS NULL 
