GO

IF OBJECT_ID('dbo.tReferenceLink', 'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tReferenceLink',
        0,
        0,
        NULL

      exec _pAddModelProperty
        'tReferenceLink',
        'Caption',
        1

      exec _pAddModelProperty
        'tReferenceLink',
        'URLlink',
        1
  END

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER PROC pGetReferenceLinkMenuItems(@ID_UserSession INT)
AS
  BEGIN
      SELECT '_' _,
             ''  MenuItems

      SELECT GETDATE() Date

      SELECT *
      FROM   tReferenceLink
  END

GO

CREATE OR
ALTER PROC pGetReferenceLinkMenuItem(@Name VARCHAR(MAX))
AS
  BEGIN
      SELECT '_' _,
             ''  MenuItems

      SELECT GETDATE() Date

      SELECT *
      FROM   tReferenceLink
      WHERE  Name = @Name
  END

GO

CREATE      OR
ALTER PROC pInsertReferenceLink(@Name    VARCHAR(MAX),
                                @Caption VARCHAR(MAX),
                                @UrlLink VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReferenceLink
         WHERE  Name = @Name) = 0
        BEGIN
            INSERT INTO [dbo].tReferenceLink
                        ([Name],
                         Caption,
                         UrlLink,
                         [IsActive],
                         ID_Company,
                         DateCreated,
                         DateModified)
            VALUES      (@Name,
                         @Caption,
                         @UrlLink,
                         1,
                         1,
                         GETDATE(),
                         GETDATE())
        END
      ELSE
        BEGIN
            Update tReferenceLink
            SET    Caption = @Caption,
                   UrlLink = @UrlLink
            where  Name = @Name
        END
  END

GO

exec pInsertReferenceLink
  'TheMerckVeterinaryManual11thEditionPDF',
  'The Merck Veterinary Manual 11th Edition',
  'https://www.dropbox.com/s/5n1ouo3xm7mgjhi/The%20Merck%20Veterinary%20Manual%2C%2011th%20Edition.pdf?dl=0'

GO

