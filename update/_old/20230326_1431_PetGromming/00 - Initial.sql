GO

if OBJECT_ID('dbo.tPatient_Grooming') is null
  begin
      exec _pCreateAppModuleWithTable
        'tPatient_Grooming',
        1,
        NULL,
        NULL
  end

GO

if OBJECT_ID('dbo.tPatient_Grooming_Detail') is null
  begin
      exec _pCreateAppModuleWithTable
        'tPatient_Grooming_Detail',
        1,
        1,
        NULL;

      exec _pAddModelProperty
        'tPatient_Grooming_Detail',
        'ID_Patient_Grooming',
        2;

      exec _pAddModelDetail
        'tPatient_Grooming_Detail',
        'ID_Patient_Grooming',
        'tPatient_Grooming',
        'tPatient_Grooming';
  end

GO

exec _pAddModelProperty
  'tPatient_Grooming',
  'Date',
  5;

exec _pAddModelProperty
  'tPatient_Grooming',
  'Weight',
  1;

exec _pAddModelProperty
  'tPatient_Grooming',
  'Temperature',
  1;

exec _pAddModelProperty
  'tPatient_Grooming',
  'ID_FilingStatus',
  2;

exec _pAddModelProperty
  'tPatient_Grooming',
  'ID_Client',
  2;

exec _pAddModelProperty
  'tPatient_Grooming',
  'ID_Patient',
  2;

exec _pAddModelProperty
  'tPatient_Grooming',
  'AttendingPhysician_ID_Employee',
  2;

exec _pAddModelProperty
  'tPatient_Grooming',
  'AttendingPhysician_ID_Employee',
  2;

exec _pAddModelProperty
  'tPatient_Grooming',
  'DateCanceled',
  5;

exec _pAddModelProperty
  'tPatient_Grooming',
  'ID_CanceledBy',
  2;

exec _pAddModelProperty
  'tPatient_Grooming_Detail',
  'ID_Item',
  2;

exec _pAddModelProperty
  'tPatient_Grooming_Detail',
  'UnitCost',
  3;

exec _pAddModelProperty
  'tPatient_Grooming_Detail',
  'UnitPrice',
  3;

exec _pAddModelProperty
  'tPatient_Grooming_Detail',
  'Quantity',
  2;

exec _pAddModelProperty
  'tPatient_Grooming_Detail',
  'DateExpiration',
  5;

exec _pAddModelProperty
  'tBillingInvoice_Detail',
  'ID_Patient_Grooming_Detail',
  2;

exec _pAddModelProperty
  'tBillingInvoice',
  'ID_Patient_Grooming',
  2;

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vPatient_Grooming_Detail
AS
  SELECT H.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         item.Name Name_Item
  FROM   tPatient_Grooming_Detail H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tItem item
                ON H.ID_Item = item.ID

Go

CREATE OR
ALTER VIEW vPatient_Grooming
AS
  SELECT H.*,
         UC.Name            AS CreatedBy,
         UM.Name            AS LastModifiedBy,
         _client.Name       Name_Client,
         _patient.Name      Name_Patient,
         _emp.Name          AttendingPhysician_Name_Employee,
         _filingStatus.Name Name_FilingStatus
  FROM   tPatient_Grooming H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee _emp
                ON H.AttendingPhysician_ID_Employee = _emp.ID
         LEFT JOIN tClient _client
                ON H.ID_Client = _client.ID
         LEFT JOIN tPatient _patient
                ON H.ID_Patient = _patient.ID
         LEFT JOIN tFilingStatus _filingStatus
                ON H.ID_FilingStatus = _filingStatus.ID

GO

CREATE    OR
ALTER VIEW [dbo].vPatient_Grooming_Listview
AS
  SELECT grooming.ID,
         grooming.Date,
         grooming.Code,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         Comment,
         ID_Company
  FROM   vPatient_Grooming grooming
  WHERE  ID_FilingStatus NOT IN ( 4 )

GO

GO

GO

CREATE OR
ALTER PROCEDURE pGetPatient_Grooming @ID         INT = -1,
                                     @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Patient_Grooming_Detail

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name Name_FilingStatus
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           '-New-'   AS [Code],
                           GetDate() AS Date,
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           1         ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatient_Grooming H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatient_Grooming_Detail
      where  ID_Patient_Grooming = @ID
  END

GO

CREATE or
alter PROC [dbo].[pModel_AfterSaved_Patient_Grooming] (@ID_CurrentObject VARCHAR(10),
                                                       @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;
            DECLARE @Code_Company VARCHAR(MAX)
            DECLARE @username VARCHAR(MAX);
            DECLARE @password VARCHAR(MAX);

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatient_Grooming
            WHERE  ID = @ID_CurrentObject;

            SELECT @Code_Company = Code
            FROM   tCompany
            WHERE  ID = @ID_Company

            SELECT @username = Name
            FROM   tPatient_Grooming
            WHERE  ID = @ID_CurrentObject;

            SET @username = replace(replace(replace(dbo.fGetCleanedString(@username), '.', ''), ',', ''), ' ', '')
            SET @username = LOWER(@username)
            SET @username = @Code_Company + '-' + @username
            SET @password = LEFT(NewId(), 4);

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatient_Grooming';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatient_Grooming
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

go

CREATE   OR
ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PayablePayment'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_PayablePayment]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Grooming'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Patient_Grooming]
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC pAddDocumentSeries(@TableName_Model VARCHAR(MAX),
                              @Prefix          VARCHAR(MAX),
                              @Counter         INT = 1,
                              @DigitCount      INT = 5)
AS
  BEGIN
      DECLARE @ID_Model UNIQUEIDENTIFIER
      DECLARE @Name_Model VARCHAR(MAX) = ''
      DECLARE @ID_DocumentSeries INT = 0
      DECLARE @IDs_Company typIntList

      SELECT @Name_Model = Name,
             @ID_Model = Oid
      FROM   dbo._tModel
      WHERE  TableName = @TableName_Model

      if(SELECt COUNT(*)
         FROM   tDocumentSeries
         where  ID_Company = 1
                AND ID_Model = @ID_Model) = 0
        begin
            INSERT dbo.tDocumentSeries
                   (Name,
                    ID_Company,
                    ID_Model,
                    Counter,
                    Prefix,
                    DigitCount,
                    IsActive,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy)
            VALUES(@Name_Model,
                   1,
                   @ID_Model,
                   @Counter,
                   @Prefix,
                   @DigitCount,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
        END

      SELECT @ID_DocumentSeries = ID
      FROM   tDocumentSeries
      where  ID_Company = 1
             and Name = @Name_Model

      Insert @IDs_Company
      SELECT ID
      FROm   tCompany
      except
      SELECT docSeries.ID_Company
      FROM   dbo.tDocumentSeries docSeries
      where  Name = @Name_Model

      INSERT dbo.tDocumentSeries
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Model,
              Counter,
              Prefix,
              IsAppendCurrentDate,
              DigitCount,
              ID_Company)
      SELECT docSeries.Code,
             docSeries.Name,
             docSeries.IsActive,
             docSeries.Comment,
             docSeries.DateCreated,
             docSeries.DateModified,
             docSeries.ID_CreatedBy,
             docSeries.ID_LastModifiedBy,
             docSeries.ID_Model,
             docSeries.Counter,
             docSeries.Prefix,
             docSeries.IsAppendCurrentDate,
             docSeries.DigitCount,
             company.ID
      FROM   dbo.tDocumentSeries docSeries,
             @IDs_Company company
      WHERE  docSeries.ID_Company = 1
             AND docSeries.ID = @ID_DocumentSeries
  END

GO

GO

CREATE   OR
ALTER PROC [dbo].[pGetBillingInvoice] @ID                             INT = -1,
                                      @ID_Client                      INT = NULL,
                                      @ID_Patient                     INT = NULL,
                                      @ID_Session                     INT = NULL,
                                      @ID_Patient_SOAP                INT = NULL,
                                      @ID_Patient_Confinement         INT = NULL,
                                      @ID_Patient_Vaccination         INT = NULL,
                                      @ID_Patient_Wellness            INT = NULL,
                                      @ID_Patient_Lodging             INT = NULL,
                                      @ID_Patient_Grooming            INT = NULL,
                                      @AttendingPhysician_ID_Employee INT = NULL,
                                      @IsWalkIn                       BIT = 0
AS
  BEGIN
      SELECT '_',
             '' BillingInvoice_Detail,
             '' BillingInvoice_Patient;

      DECLARE @ConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @InitialConfinementDepositAmount DECIMAL(18, 2)= 0
      DECLARE @Confinement_ID_SOAPType INT = 2
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @ID_SOAPType INT = 0
      DECLARE @Name_SOAPType VARCHAR(MAX) = ''
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_Session;

      IF( @ID_Client = -1 )
        SET @ID_Client = NULL

      IF( @ID_Patient = -1 )
        SET @ID_Patient = NULL

      IF ISNULL(@ID_Patient_Confinement, 0) = 0
        BEGIN
            SELECT @ID_Patient_Confinement = ID_Patient_Confinement
            FROM   tBillingInvoice
            WHERE  ID = @ID
        END

      DECLARE @Compostela_ID_Company INT = 65

      IF(SELECT Count(*)
         FROM   vUSER
         WHERE  ID_COmpany = @Compostela_ID_Company
                AND ID = @ID_User) > 0
        BEGIN
            SELECT @AttendingPhysician_ID_Employee = ID
            FROM   tEmployee
            WHERE  ID = 357
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Grooming, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID,
                   @Name_SOAPType = Name
            FROM   tSOAPType
            where  Name = 'Grooming'

            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_Grooming
            WHERE  ID = @ID_Patient_Grooming
        END

      IF( ISNULL(@ID_Patient_SOAP, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = ID_SOAPType,
                   @Name_SOAPType = Name_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_SOAP
            WHERE  ID = @ID_Patient_SOAP
        END

      IF( ISNULL(@ID_Patient_Wellness, 0) > 0 )
        BEGIN
            SELECT @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient,
                   @AttendingPhysician_ID_Employee = AttendingPhysician_ID_Employee
            FROM   vPatient_Wellness
            WHERE  ID = @ID_Patient_Wellness
        END

      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )
        BEGIN
            SELECT @ID_SOAPType = @Confinement_ID_SOAPType,
                   @ID_Client = ID_Client,
                   @ID_Patient = ID_Patient
            FROM   vPatient_Confinement
            WHERE  ID = @ID_Patient_Confinement

            SELECT @ConfinementDepositAmount = ISNULL(CurrentCreditAmount, 0)
            FROM   tPatient_Confinement confinement
                   INNER JOIN tClient client
                           ON confinement.ID_Client = client.ID
            WHERE  confinement.ID = @ID_Patient_Confinement

            SET @InitialConfinementDepositAmount = @ConfinementDepositAmount
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   fs.Name                          Name_FilingStatus,
                   client.Name                      Name_Client,
                   patient.Name                     Name_Patient,
                   client.Address                   BillingAddress,
                   attendingPhysicianEmloyee.Name   AttendingPhysician_Name_Employee,
                   @InitialConfinementDepositAmount InitialConfinementDepositAmount,
                   @ConfinementDepositAmount        ConfinementDepositAmount
            FROM   (SELECT NULL                            AS [_],
                           -1                              AS [ID],
                           '- NEW -'                       AS [Code],
                           NULL                            AS [Name],
                           1                               AS [IsActive],
                           GetDate()                       AS Date,
                           NULL                            AS [ID_Company],
                           NULL                            AS [Comment],
                           NULL                            AS [DateCreated],
                           NULL                            AS [DateModified],
                           0                               AS [ID_CreatedBy],
                           0                               AS [ID_LastModifiedBy],
                           1                               AS [ID_FilingStatus],
                           0                               AS [ID_Taxscheme],
                           0                               DiscountRate,
                           0                               DiscountAmount,
                           @ID_Client                      ID_Client,
                           0                               IsComputeDiscountRate,
                           @ID_Patient                     ID_Patient,
                           @AttendingPhysician_ID_Employee AttendingPhysician_ID_Employee,
                           @ID_SOAPType                    ID_SOAPType,
                           @Name_SOAPType                  Name_SOAPType,
                           @ID_Patient_SOAP                ID_Patient_SOAP,
                           @ID_Patient_Confinement         ID_Patient_Confinement,
                           @ID_Patient_Vaccination         ID_Patient_Vaccination,
                           @ID_Patient_Wellness            ID_Patient_Wellness,
                           @ID_Patient_Lodging             ID_Patient_Lodging,
                           @ID_Patient_Grooming            ID_Patient_Grooming,
                           CASE
                             WHEN @IsWalkIn = 1 THEN 'Walk In Customer'
                             ELSE ''
                           END                             WalkInCustomerName,
                           @IsWalkIn                       IsWalkIn) H
                   LEFT JOIN dbo.tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN dbo.tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN dbo.tClient client
                          ON client.ID = H.ID_Client
                   LEFT JOIN dbo.tPatient patient
                          ON patient.ID = H.ID_Patient
                   LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                          ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
        END;
      ELSE
        BEGIN
            SELECT H.*
            FROM   dbo.vBillingInvoice H
            WHERE  H.ID = @ID;
        END;

      IF ISNULL(@ID_Patient_SOAP, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapTreatment.ID DESC) ) - 799999                    ID,
                   soapTreatment.ID_Item,
                   soapTreatment.Name_Item,
                   ISNULL(soapTreatment.Quantity, 0)                                   Quantity,
                   ISNULL(soapTreatment.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapTreatment.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapTreatment.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Treatment soapTreatment
                   INNER JOIN tItem item
                           ON item.ID = soapTreatment.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
            UNION ALL
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY soapPrescription.ID DESC) ) - 999999                    ID,
                   soapPrescription.ID_Item,
                   soapPrescription.Name_Item,
                   ISNULL(soapPrescription.Quantity, 0)                                   Quantity,
                   ISNULL(soapPrescription.UnitPrice, item.UnitPrice)                     UnitPrice,
                   ISNULL(soapPrescription.UnitCost, item.UnitCost)                       UnitCost,
                   ISNULL(soapPrescription.DateExpiration, item.OtherInfo_DateExpiration) DateExpiration
            FROM   dbo.vPatient_SOAP_Prescription soapPrescription
                   INNER JOIN tItem item
                           ON item.ID = soapPrescription.ID_Item
            WHERE  ID_Patient_SOAP = @ID_Patient_SOAP
                   AND soapPrescription.IsActive = 1
                   AND soapPrescription.IsCharged = 1
        END
      ELSE IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY itemsservices.ID DESC) ) - 999999 ID,
                   itemsservices.ID_Item,
                   itemsservices.Name_Item,
                   ISNULL(itemsservices.Quantity, 0)                Quantity,
                   ISNULL(itemsservices.UnitPrice, 0)               UnitPrice,
                   ISNULL(itemsservices.UnitCost, 0)                UnitCost,
                   itemsservices.DateExpiration                     DateExpiration,
                   itemsservices.ID                                 ID_Patient_Confinement_ItemsServices
            FROM   dbo.vPatient_Confinement_ItemsServices itemsservices
                   INNER JOIN tItem item
                           ON item.ID = itemsservices.ID_Item
            WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
        END
      ELSE IF ISNULL(@ID_Patient_Wellness, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY wellnessdetail.ID DESC) ) - 999999 ID,
                   wellnessdetail.ID_Item,
                   wellnessdetail.Name_Item,
                   ISNULL(wellnessdetail.Quantity, 1)                Quantity,
                   ISNULL(wellnessdetail.UnitPrice, 0)               UnitPrice,
                   ISNULL(wellnessdetail.UnitCost, 0)                UnitCost,
                   wellnessdetail.DateExpiration                     DateExpiration,
                   wellnessdetail.ID                                 ID_Patient_Wellness_Detail
            FROM   dbo.vPatient_Wellness_Detail wellnessdetail
                   INNER JOIN tItem item
                           ON item.ID = wellnessdetail.ID_Item
            WHERE  ID_Patient_Wellness = @ID_Patient_Wellness
        END
      ELSE IF ISNULL(@ID_Patient_Vaccination, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY vaccination.ID DESC) ) - 999999 ID,
                   vaccination.ID_Item,
                   vaccination.Name_Item,
                   1                                              Quantity,
                   ISNULL(vaccination.UnitPrice, item.UnitPrice)  UnitPrice,
                   ISNULL(vaccination.UnitCost, item.UnitCost)    UnitCost,
                   vaccination.DateExpiration                     DateExpiration,
                   vaccination.ID                                 ID_Patient_Vaccination
            FROM   dbo.vPatient_Vaccination vaccination
                   INNER JOIN tItem item
                           ON item.ID = vaccination.ID_Item
            WHERE  vaccination.ID = @ID_Patient_Vaccination
        END
      ELSE IF ISNULL(@ID_Patient_Grooming, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY groomingdetail.ID DESC) ) - 999999 ID,
                   groomingdetail.ID_Item,
                   groomingdetail.Name_Item,
                   ISNULL(groomingdetail.Quantity, 1)                Quantity,
                   ISNULL(groomingdetail.UnitPrice, 0)               UnitPrice,
                   ISNULL(groomingdetail.UnitCost, 0)                UnitCost,
                   groomingdetail.DateExpiration                     DateExpiration,
                   groomingdetail.ID                                 ID_Patient_Grooming_Detail
            FROM   dbo.vPatient_Grooming_Detail groomingdetail
                   INNER JOIN tItem item
                           ON item.ID = groomingdetail.ID_Item
            WHERE  ID_Patient_Grooming = @ID_Patient_Grooming
        END
      ELSE
        BEGIN
            SELECT *,
                   conf.Code               Code_Patient_Confinement,
                   conf.Date               Date_Patient_Confinement,
                   soap_.Code              Code_Patient_SOAP,
                   soap_.Name_FilingStatus Name_FilingStatus_Patient_SOAP,
                   confItemServices.ID_Patient_SOAP_Prescription,
                   confItemServices.ID_Patient_SOAP_Treatment,
                   ''                      Note
            FROM   dbo.vBillingInvoice_Detail biDetail
                   INNER join tBillingInvoice bi
                           on biDetail.ID_BillingInvoice = bi.ID
                   LEFT join vPatient_Confinement_ItemsServices confItemServices
                          on biDetail.ID_Patient_Confinement_ItemsServices = confItemServices.ID
                   LEFT JOIN vPatient_SOAP soap_
                          on soap_.ID = confItemServices.ID_Patient_SOAP
                   LEFT join vPatient_Confinement conf
                          on bi.ID_Patient_Confinement = conf.ID
            WHERE  ID_BillingInvoice = @ID
            Order  by ISNULL(ID_Patient_Confinement_ItemsServices, 100000000) ASC,
                      biDetail.ID ASC
        END

      IF ISNULL(@ID_Patient_Confinement, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID_Patient,
                   Name_Patient,
                   ID                                 ID_Patient_Confinement_Patient
            FROM   vPatient_Confinement_Patient
            WHERE  ID_Patient_Confinement IN ( @ID_Patient_Confinement )
            ORDER  BY ID DESC
        END
      ELSE IF ISNULL(@ID_Patient, 0) > 0
         AND ( @ID = -1 )
        BEGIN
            SELECT ( ROW_NUMBER()
                       OVER(
                         ORDER BY ID DESC) ) - 999999 ID,
                   ID                                 ID_Patient,
                   Name                               Name_Patient
            FROM   tPatient
            WHERE  ID IN ( @ID_Patient )
        END
      ELSE
        BEGIN
            SELECT *
            FROM   dbo.vBillingInvoice_Patient
            WHERE  ID_BillingInvoice = @ID
        END
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancelPatient_Grooming] (@IDs_Patient_Grooming typIntList READONLY,
                                            @ID_UserSession       INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatient_Grooming
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Grooming bi
                 INNER JOIN @IDs_Patient_Grooming ids
                         ON bi.ID = ids.ID;

          INSERT @IDs_Patient
          SELECT ID_Patient
          FROM   dbo.tPatient_Grooming hed
                 inner join @IDs_Patient_Grooming ids
                         on hed.ID = ids.ID

          exec [pUpdatePatientsLastVisitedDate]
            @IDs_Patient
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

GO

CREATE OR
ALTER PROC pInsertUserRoleModel (@Name_UserRole   VARCHAR(MAX),
                                 @TableName_Model VARCHAR(MAX))
AS
  BEGIN
      DECLARE @ID_UserRole INT = 0
      DECLARE @Oid_Model VARCHAR(MAX) = ''
      DECLARE @Name_Model VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid,
             @Name_Model = Name
      FROM   _tModel
      where  TableName = @TableName_Model

      SELECT @Name_Model

      IF(SELECT COUNT(*)
         FROM   tUserRole
         where  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess])
            SELECT @Name_UserRole,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   0;
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      if(SELECT COUNT(*)
         FROM   tUserRole_Detail
         where  ID_Model = @Oid_Model
                AND ID_UserRole = @ID_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Detail]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Model])
            SELECT @Name_Model,
                   1,
                   @ID_UserRole,
                   @Oid_Model
        END
  END

GO

exec pInsertUserRoleModel
  'Pet Grooming Only',
  'tPatient_Grooming'

exec pInsertUserRoleModel
  'Administrator',
  'tPatient_Grooming'

exec pInsertUserRoleModel
  'All Admission',
  'tPatient_Grooming'

GO

IF(SELECT COUNT(*)
   FROm   vDocumentSeries
   where  Name = 'Patient_Grooming') = 0
  BEGIN
      exec pAddDocumentSeries
        'tPatient_Grooming',
        'PGR',
        1,
        6;
  END

GO

Update _tNavigation
SET    Caption = 'Pet Grooming',
       Route = 'PatientGroomingList',
       ID_Parent = '24C522DA-EA51-439F-BE1B-F7D096BEEF51'
WHERE  Name = 'Patient_Grooming_ListView'

exec pInsertSOAPType
  'Grooming' 
