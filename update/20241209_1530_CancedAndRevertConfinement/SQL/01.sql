GO

CREATE OR
ALTER PROC dbo.pRevertToConfined_Patient_Confinement (@IDs_Patient_Confinement typIntList READONLY,
                                                      @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_ClientDeposit typIntList

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Confined_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatient_Confinement bi with (NOLOCK)
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          exec pUpdatePatient_Confinemen_BillingStatus
            @IDs_Patient_Confinement
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO 

EXEC dbo.pInsertSystemVersion
  'Cancelled And Revert Confinement' 
