GO
GO

CREATE         OR
ALTER PROC dbo.pGetDashboardTopBilledClientDataSourceByDateCoverage (@ID_UserSession INT,
                                                                     @DateStart      DateTime,
                                                                     @DateEnd        DateTime)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT
      DECLARE @Label VARCHAR(MAX) = ''

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @dataSource TABLE
        (
           Name        VARCHAR(MAX),
           TotalAmount DECIMAL(18, 2)
        )

      INSERT @dataSource
      SELECT TOP 10 client.Name,
                    SUM(ISNULL(biHed.TotalAmount, 0)) TotalAmount
      FROM   tBillingInvoice biHed
             INNER JOIN tClient client
                     on biHed.ID_Client = client.ID
      WHERE  CONVERT(Date, biHed.Date) BETWEEN CONVERT(Date, @DateStart) AND CONVERT(Date, @DateEnd)
             AND biHed.ID_Company = @ID_Company
             AND biHed.ID_FilingStatus NOT IN ( 4 )
      GROUP  BY client.Name
      ORDER  BY SUM(ISNULL(biHed.TotalAmount, 0)) DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0

            SET @Label = 'No Record'
        END
      ELSE
        BEGIN
            DECLARE @Count INT = 0

            SELECT @Count = COUNT(*)
            FROM   @dataSource

            SET @Label = 'Top ' + FORMAT(@Count, '#,#') + ' ' + 'from '
                         + FORMAT(@DateStart, 'MM/dd/yyyy') + ' to '
                         + FORMAT(@DateEnd, 'MM/dd/yyy')
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT YEAR(@DateStart) DateYear,
             @Label           Label

      SELECT *
      FROM   @dataSource
      order  by TotalAmount DESC
  END

GO 

CREATE    OR
ALTER PROCEDURE [dbo].[pGetCompany] @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN

      SELECT '_' AS _,
             ''  AS Company_SMSSetting

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCompany H
            WHERE  H.ID = @ID
        END

		SELECT * FROm tCompany_SMSSetting WHERE ID_Company =  @ID
  END

GO 


EXEC dbo.pInsertSystemVersion
  'Top Billed Client'



SELECT ID,
       Name,
       DateCreated
FROM   tSystemVersion
ORDER  BY DateCreated DESC,
          ID DESC 