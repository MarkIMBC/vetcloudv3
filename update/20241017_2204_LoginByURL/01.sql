GO

GO

CREATE       OR
ALTER VIEW vCompanyActiveUserURLLink
as
  SELECT user_.ID_Company,
         user_.Name_Company,
         emp.ID                            ID_Employee,
         emp.Name                          Name_Employee,
         emp.Name_Position,
         user_.ID                          ID_User,
         '/LoginUrl/' + c.Guid + '|'
         + CONVERT(VARCHAR(MAX), user_.ID) UserURLLink
  FROm   vUser user_
         inner join vCompanyActive c
                 on user_.ID_Company = c.ID
         LEFT JOIN vEmployee emp
                on user_.ID_Employee = emp.ID
  WHERE  user_.IsActive = 1
         AND ( user_.Username NOT LIKE '%system'
               AND user_.UserName NOT LIKE '%-sole'
               AND user_.UserName NOT LIKE '%receptionportal'
               AND user_.UserName NOT LIKE '%clientform' )

GO

CREATE    OR
ALTER PROC pDoUserLoginByUserLogin(@ID_User INT)
AS
  BEGIN
      DECLARE @Username VARCHAR(MAX) = ''
      DECLARE @Password VARCHAR(MAX) = ''

      SELECT @Username = Username,
             @Password = Password
      FROM   tUser
      WHERE  ID = @ID_User

      EXEC dbo.pDoUserLogin
        @Username,
        @Password
  END

Go 


EXEC dbo.pInsertSystemVersion
  'Login By URL'

SELECT ID,
       Name,
       DateCreated
FROM   tSystemVersion
ORDER  BY DateCreated DESC,
          ID DESC 