GO

ALTER VIEW vForBilling_ListView_temp
AS
  SELECT confinement.ID,
         confinement.Code RefNo,
         confinement.ID   ID_CurrentObject,
         m.Oid            Oid_Model,
         m.Name           Name_Model,
         confinement.Date,
         confinement.ID_Company,
         confinement.ID_Client,
         confinement.ID_Patient,
         confinement.BillingInvoice_ID_FilingStatus,
         confinement.Name_Client,
         confinement.Name_Patient,
         confinement.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Confinement confinement,
         _tModel m
  WHERE  m.TableName = 'tPatient_Confinement'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT soap.ID,
         soap.Code RefNo,
         soap.ID   ID_CurrentObject,
         m.Oid     Oid_Model,
         m.Name    Name_Model,
         soap. Date,
         soap.ID_Company,
         soap.ID_Client,
         soap.ID_Patient,
         soap.BillingInvoice_ID_FilingStatus,
         soap.Name_Client,
         soap.Name_Patient,
         soap.BillingInvoice_Name_FilingStatus
  FROM   vPatient_SOAP soap,
         _tModel m
  WHERE  m.TableName = 'tPatient_SOAP'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
         AND Isnull(soap.ID_Patient_Confinement, '') = 0
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Wellness hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Wellness'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )
  UNION ALL
  SELECT hed.ID,
         hed.Code RefNo,
         hed.ID   ID_CurrentObject,
         m.Oid    Oid_Model,
         m.Name   Name_Model,
         hed. Date,
         hed.ID_Company,
         hed.ID_Client,
         hed.ID_Patient,
         hed.BillingInvoice_ID_FilingStatus,
         hed.Name_Client,
         hed.Name_Patient,
         hed.BillingInvoice_Name_FilingStatus
  FROM   vPatient_Grooming hed,
         _tModel m
  WHERE  m.TableName = 'tPatient_Grooming'
         AND ID_FilingStatus NOT IN ( 4 )
         AND BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )

GO

EXEC dbo.pInsertSystemVersion
  'Remove Done In For Billing'

GO

ALTER VIEW vUserAdministrator
as
  select _user.ID,
         _user.ID                                                    ID_User,
         _user.ID_Employee,
         CONVERT(BIT, MAX(CONVERT(INT, _userroles.IsAdministrator))) IsAdministrator
  FROM   tUser _user
         LEFT join tUser_Roles _user_roles
                on _user.ID = _user_roles.ID_User
         LEFT join tUserRole _userroles
                on _user_roles.ID_UserRole = _userroles.ID
  GROUP  BY _user.ID,
            _user.ID_Employee
  HAVING CONVERT(BIT, MAX(CONVERT(INT, _userroles.IsAdministrator))) > 0

GO

EXEC dbo.pInsertSystemVersion
  'Fix Cancel Billing Invoice by Administrator Password' 
