GO

CREATE OR
ALTER PROC pInsertSystemVersion(@Name VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   dbo.[tSystemVersion]
         WHERE  Name = @Name) = 0
        BEGIN
            INSERT INTO [dbo].[tSystemVersion]
                        ([Name],
                         [IsActive],
                         [ID_Company],
                         [DateCreated])
            VALUES      ( @Name,
                          1,
                          1,
                          GETDATE())
        END
  END

GO


		  
EXEC dbo.pInsertSystemVersion
  'System Update Log'