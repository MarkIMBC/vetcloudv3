GO

IF OBJECT_ID('dbo.tCompanyCustomReportObjectValue', 'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tCompanyCustomReportObjectValue',
        0,
        0,
        null;

      exec _pAddModelProperty
        'tCompanyCustomReportObjectValue',
        'Oid_Report',
        1;

      exec _pAddModelProperty
        'tCompanyCustomReportObjectValue',
        'ObjectName',
        1;

      exec _pAddModelProperty
        'tCompanyCustomReportObjectValue',
        'ID_ReportObjectType',
        2;

      exec _pAddModelProperty
        'tCompanyCustomReportObjectValue',
        'PropertyName',
        1;

      exec _pAddModelProperty
        'tCompanyCustomReportObjectValue',
        'Value',
        1;
  END

GO

CREATE     OR
ALTER PROC pInsertCompanyCustomReportObjectValue(@ID_Company   INT,
                                                 @Oid_Report   VARCHAR(MAX),
                                                 @ObjectName   VARCHAR(MAX),
                                                 @PropertyName VARCHAR(MAX),
                                                 @Value        VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_CompanyCustomReportObjectValue INT = 0

      IF @ID_Company IS NULL
        return;

      IF(SELECT COUNT(*)
         FROM   dbo.tCompanyCustomReportObjectValue
         WHERE  Oid_Report = @Oid_Report
                AND ID_Company = @ID_Company
                AND ObjectName = @ObjectName
                AND PropertyName = @PropertyName) = 0
        BEGIN
            INSERT INTO [dbo].tCompanyCustomReportObjectValue
                        ([Name],
                         [IsActive],
                         [Oid_Report],
                         ObjectName,
                         PropertyName,
                         ID_Company)
            VALUES      (@ObjectName + '.' + @PropertyName,
                         1,
                         @Oid_Report,
                         @ObjectName,
                         @PropertyName,
                         @ID_Company)
        END

      SELECT @ID_CompanyCustomReportObjectValue = ID
      FROM   dbo.tCompanyCustomReportObjectValue
      WHERE  Oid_Report = @Oid_Report
             AND ID_Company = @ID_Company
             AND ObjectName = @ObjectName
             AND PropertyName = @PropertyName

      UPDATE dbo.tCompanyCustomReportObjectValue
      SET    Value = @Value
      WHERE  ID = @ID_CompanyCustomReportObjectValue
  END

GO

CREATE     OR
ALTER PROC pInsertCompanyCustomReportObjectValueByGuidCompany(@GUID_Company VARCHAR(MAX),
                                                              @Name_Report  VARCHAR(MAX),
                                                              @ObjectName   VARCHAR(MAX),
                                                              @PropertyName VARCHAR(MAX),
                                                              @Value        VARCHAR(MAX))
as
  BEGIN
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company
             AND IsActive = 1

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @Name_Report

      EXEC dbo.pInsertCompanyCustomReportObjectValue
        @ID_Company,
        @Oid_Report,
        @ObjectName,
        @PropertyName,
        @Value
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetBillingInvoicePrintReceiptLayout_for_estacionanimalesclinicaveterinariacompany] (@ID_BillingInvoice INT)
AS
  BEGIN
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Guid_Company VARCHAR(MAX)= ''
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @GrossSubTotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalChangeAmount DECIMAL(18, 4) = 0
      DECLARE @TotalPaymentAmount DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @CustomCode_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0

      ---------------------------- Check for Customed pGetBillingInvoicePrintReceiptLayout Per Company --------------------------  
      SELECT @ID_Company = ID_Company
      FROM   tBillingInvoice
      WHERE  ID = @ID_BillingInvoice

      SELECT @Guid_Company = GUID
      FROM   vCompany
      WHERE  ID = @ID_Company

      ------------------------END Check for Customed pGetBillingInvoicePrintReceiptLayout Per Company END --------------------------  
      SELECT @ID_Company = ID_Company,
             @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @CustomCode_BillingInvoice = CustomCode,
             @TotalItemDiscountAmount_BillingInvoice = ISNULL(TotalItemDiscountAmount, 0),
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, ''),
             @TotalPaymentAmount = ISNULL(paymentTotal.TotalPaymentAmount, 0),
             @TotalChangeAmount = ISNULL(paymentTotal.TotalChangeAmount, 0),
             @GrossSubTotalAmount_BillingInvoice = ISNULL(bi.GrossSubTotalAmount, 0)
      FROM   vBillingInvoice bi
             LEFT JOIN vBillingInvoice_PaymentTransaction_Totals paymentTotal
                    on bi.ID = paymentTotal.ID_BillingInvoice
      WHERE  bi.ID = @ID_BillingInvoice

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px'),
             @Guid_Company = Guid
      FROM   vCompany
      WHERE  ID = @ID_Company

      DECLARE @style VARCHAR(MAX)= '                                  
  body{                                  
   margin: 0px;                                  
   padding: 0px;                                  
   font-family:  arial, sans-serif;                                  
   font-weight: normal;                                  
   font-style: normal;                                  
   font-size: ' + @POSReceiptFontSize
        + ';                                  
  }                                  
                                  
  .logo{                                  
                                  
   width: 120px;                                  
   margin-bottom: 10px;                                  
  }                                  
                                  
  .receipt-container{                                  
     width: 100vw;                              
  position: relative;                              
  left: 50%;                              
  right: 50%;                              
  margin-left: -50vw;                              
  margin-right: -50vw;                              
  }                                  
                                  
  .company-logo{                                  
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   margin-bottom: 12px;                                  
  }                                  
                             
  .company-name{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
  }                                  
                                  
  .company-address{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   font-size: 11px                                  
  }                                  
                                  
  .company-contactnum-container{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                             
   font-size: 11px                                  
  }                                  
                                  
  .company-contactnum{                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
                                  
  }                                  
                                  
  .float-left{                                  
   float: left;            
  }                                  
                                  
  .float-right{                                  
   float: right;                                  
  }                                  
                        
  .hide{                 
 display: none;                
  }                
                                     
  .clearfix {                                  
   overflow: auto;                                  
  }                       
                                  
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                               
                                  
  .display-block{                                  
  word-wrap: break-word;                              
 display: block;                                  
  }                                  
                                  
  .title{                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   display: block;                                  
   padding-top: 15px;                                  
   padding-bottom: 15px;                                  
  }                                  
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'
      /*Billing Invoice Layout*/
      SET @content = @content
                     + '<div class="company-logo"><img src="'
                     + @ImageLogoLocationFilenamePath_Company
                     + '" class="logo"></div>'
      SET @content = @content + '<center><div class="">'
                     + @Name_Company + '</div>'
      SET @content = @content + '<div>' + @Address_Company + '</div>'
      SET @content = @content + '<div>' + @ContactNumber_Company
                     + '</div></center>'
      SET @content = @content
                     + '<div class="title bold">COMPUTATION</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('#:', @CustomCode_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(Quantity, '#,#0'), FORMAT(Amount, '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      Declare @DiscountWord VARCHAR(MAX) = 'Disc.'

      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'
      ------------------------------------------------------------------------------------------------------------    
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Item '+ @DiscountWord +' Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));

      IF( @TotalChangeAmount ) > 0
        BEGIN
            SET @content = @content + '<br/>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Payment Amount', FORMAT(@TotalPaymentAmount, '#,#0.00'));
        END

      IF( @TotalChangeAmount ) > 0
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Change Amount', FORMAT(@TotalChangeAmount, '#,#0.00'));
        END

      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      /*END Main Layout */
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO

CREATE OR
ALTER PROC [dbo].[pGetBillingInvoicePrintReceiptLayout] (@ID_BillingInvoice INT)
AS
  BEGIN
      DECLARE @cardinalpetclinic_Guid_Company VARCHAR(MAX)= 'D91D497F-6108-4D50-88A2-BBC73C81A86D'
      DECLARE @sthyacinth_Guid_Company VARCHAR(MAX)= 'CE106252-85B7-4348-8441-517560A07A7C'
      DECLARE @estacionanimalessantol_Guid_Company VARCHAR(MAX) = 'DD11E0A3-7281-4F31-9B85-2678FB6E0A16'
      DECLARE @estacionanimalesdelafuente_Guid_Company VARCHAR(MAX) = '6C6B0D25-425B-48CD-956F-F9CABDD21A64'
      DECLARE @estacionanimalesotis_Guid_Company VARCHAR(MAX) = '0056C1C4-D0A2-40AC-9010-E384DC15FCE7'
      DECLARE @isAutoPrint BIT = 1
      DECLARE @IsShowHeader BIT = 1
      DECLARE @IsShowFooter BIT = 1
      DECLARE @Cash_ID_PaymentMethod INT = 1
      DECLARE @Check_ID_PaymentMethod INT = 2
      DECLARE @DebitCreditCard_ID_PaymentMethod INT = 4
      DECLARE @GCash_ID_PaymentMethod INT = 3
      DECLARE @millisecondDelay INT = 1000
      DECLARE @billingItemsLayout VARCHAR(MAX)= ''
      DECLARE @paymentLayout VARCHAR(MAX)= ''
      DECLARE @ID_Company INT = 0
      DECLARE @Name_Company VARCHAR(MAX)= ''
      DECLARE @Guid_Company VARCHAR(MAX)= ''
      DECLARE @Address_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogoLocationFilenamePath_Company VARCHAR(MAX)= ''
      DECLARE @ImageLogo_Company VARCHAR(MAX)= ''
      DECLARE @ContactNumber_Company VARCHAR(MAX)= ''
      DECLARE @Code_PaymentTranaction VARCHAR(MAX)= ''
      DECLARE @ID_PaymentMode INT = 0
      DECLARE @ReferenceTransactionNumber VARCHAR(MAX)= ''
      DECLARE @CheckNumber VARCHAR(MAX)= ''
      DECLARE @CardNumber VARCHAR(MAX)= ''
      DECLARE @Name_CardType VARCHAR(MAX)= ''
      DECLARE @CardHolderName VARCHAR(MAX)= ''
      DECLARE @Name_PaymentStatus VARCHAR(MAX)= ''
      DECLARE @CashAmount DECIMAL(18, 4) = 0.00
      DECLARE @GCashAmount DECIMAL(18, 4) = 0.00
      DECLARE @CardAmount DECIMAL(18, 4) = 0.00
      DECLARE @CheckAmount DECIMAL(18, 4) = 0.00
      DECLARE @PayableAmount DECIMAL(18, 4) = 0.00
      DECLARE @PaymentAmount DECIMAL(18, 4) = 0.00
      DECLARE @ChangeAmount DECIMAL(18, 4) = 0.00
      DECLARE @RemainingAmount DECIMAL(18, 4) = 0.00
      DECLARE @SubTotal_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalItemDiscountAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @DiscountRate_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @InitialSubtotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConfinementDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @GrossSubTotalAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount_BillingInvoice DECIMAL(18, 4) = 0
      DECLARE @TotalChangeAmount DECIMAL(18, 4) = 0
      DECLARE @TotalPaymentAmount DECIMAL(18, 4) = 0
      DECLARE @Name_Client VARCHAR(MAX)= ''
      DECLARE @Name_Patient VARCHAR(MAX)= ''
      DECLARE @Date_BillingInvoice DATETIME
      DECLARE @Code_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @CustomCode_BillingInvoice VARCHAR(MAX)= ''
      DECLARE @IsShowPOSReceiptLogo BIT = 1
      DECLARE @IsRemoveBoldText BIT = 0
      DECLARE @POSReceiptFontSize VARCHAR(MAX)= ''
      DECLARE @content VARCHAR(MAX)= ''
      DECLARE @biItemslayout VARCHAR(MAX)= ''
      DECLARE @WalkInCustomerName VARCHAR(MAX)= ''
      DECLARE @IsWalkIn BIT = 0

      ---------------------------- Check for Customed pGetBillingInvoicePrintReceiptLayout Per Company --------------------------  
      SELECT @ID_Company = ID_Company
      FROM   tBillingInvoice
      WHERE  ID = @ID_BillingInvoice

      SELECT @Guid_Company = GUID
      FROM   vCompany
      WHERE  ID = @ID_Company

      IF( @Guid_Company = @sthyacinth_Guid_Company )
        BEGIN
            EXEC dbo.pGetBillingInvoicePrintReceiptLayout_for_sthyacinth
              @ID_BillingInvoice;

            RETURN;
        END

      IF( @Guid_Company IN ( @estacionanimalesdelafuente_Guid_Company, @estacionanimalesotis_Guid_Company, @estacionanimalessantol_Guid_Company ) )
        BEGIN
            EXEC dbo.pGetBillingInvoicePrintReceiptLayout_for_estacionanimalesclinicaveterinariacompany
              @ID_BillingInvoice;

            RETURN;
        END

      ------------------------END Check for Customed pGetBillingInvoicePrintReceiptLayout Per Company END --------------------------  
      SELECT @ID_Company = ID_Company,
             @Name_Client = ISNULL(bi.Name_Client, ''),
             @Name_Patient = ISNULL(bi.PatientNames, ISNULL(bi.Name_Patient, 'N/A')),
             @Date_BillingInvoice = Date,
             @Code_BillingInvoice = Code,
             @CustomCode_BillingInvoice = CustomCode,
             @TotalItemDiscountAmount_BillingInvoice = ISNULL(TotalItemDiscountAmount, 0),
             @SubTotal_BillingInvoice = SubTotal,
             @DiscountRate_BillingInvoice = DiscountRate,
             @DiscountAmount_BillingInvoice = DiscountAmount,
             @TotalAmount_BillingInvoice = TotalAmount,
             @Name_PaymentStatus = ISNULL(Payment_Name_FilingStatus, ''),
             @InitialSubtotalAmount_BillingInvoice = ISNULL(InitialSubtotalAmount, -1),
             @ConfinementDepositAmount_BillingInvoice = ISNULL(ConfinementDepositAmount, -1),
             @ConsumedDepositAmount_BillingInvoice = ISNULL(ConsumedDepositAmount, -1),
             @RemainingDepositAmount_BillingInvoice = ISNULL(RemainingDepositAmount, -1),
             @IsWalkIn = ISNULL(IsWalkIn, 0),
             @WalkInCustomerName = ISNULL(WalkInCustomerName, ''),
             @TotalPaymentAmount = ISNULL(paymentTotal.TotalPaymentAmount, 0),
             @TotalChangeAmount = ISNULL(paymentTotal.TotalChangeAmount, 0),
             @GrossSubTotalAmount_BillingInvoice = ISNULL(bi.GrossSubTotalAmount, 0)
      FROM   vBillingInvoice bi
             LEFT JOIN vBillingInvoice_PaymentTransaction_Totals paymentTotal
                    on bi.ID = paymentTotal.ID_BillingInvoice
      WHERE  bi.ID = @ID_BillingInvoice

      SELECT @Name_Company = ISNULL(Name, 'N/A'),
             @Address_Company = ISNULL(Address, ''),
             @ContactNumber_Company = ISNULL(ContactNumber, ''),
             @ImageLogoLocationFilenamePath_Company = ISNULL(ImageLogoLocationFilenamePath, ''),
             @ImageLogo_Company = ISNULL(ImageLogoFilename, ''),
             @IsShowPOSReceiptLogo = ISNULL(IsShowPOSReceiptLogo, 0),
             @IsRemoveBoldText = ISNULL(IsRemoveBoldText, 0),
             @IsShowHeader = ISNULL(IsShowHeader, 1),
             @IsShowFooter = ISNULL(IsShowFooter, 1),
             @POSReceiptFontSize = ISNULL(POSReceiptFontSize, '13px'),
             @Guid_Company = Guid
      FROM   vCompany
      WHERE  ID = @ID_Company

      DECLARE @style VARCHAR(MAX)= '                                  
  body{                                  
   margin: 0px;                                  
   padding: 0px;                                  
   font-family:  arial, sans-serif;                                  
   font-weight: normal;                                  
   font-style: normal;                                  
   font-size: ' + @POSReceiptFontSize
        + ';                                  
  }                                  
                                  
  .logo{                                  
                                  
   width: 120px;                                  
   margin-bottom: 10px;                                  
  }                                  
                                  
  .receipt-container{                                  
     width: 100vw;                              
  position: relative;                              
  left: 50%;                              
  right: 50%;                              
  margin-left: -50vw;                              
  margin-right: -50vw;                              
  }                                  
                                  
  .company-logo{                                  
   display: '
        + CASE
            WHEN len(lTrim(rTrim(@ImageLogo_Company))) > 0 THEN 'block'
            ELSE 'none'
          END
        + ';                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   margin-bottom: 12px;                                  
  }                                  
                             
  .company-name{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
  }                                  
                                  
  .company-address{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   font-size: 11px                                  
  }                                  
                                  
  .company-contactnum-container{                                  
   display: block;                                  
   text-align: center;                                  
   word-wrap: break-word;                             
   font-size: 11px                                  
  }                                  
                                  
  .company-contactnum{                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
                                  
  }                                  
                                  
  .float-left{                                  
   float: left;            
  }                                  
                                  
  .float-right{                                  
   float: right;                                  
  }                                  
                        
  .hide{                 
 display: none;                
  }                
                                     
  .clearfix {                                  
   overflow: auto;                                  
  }                       
                                  
  '
        + CASE
            WHEN @IsRemoveBoldText = 0 THEN '.bold{ font-weight: bold; } '
            ELSE ''
          END
        + '                               
                                  
  .display-block{                                  
  word-wrap: break-word;                              
 display: block;                                  
  }                                  
                                  
  .title{                                  
   text-align: center;                                  
   word-wrap: break-word;                                  
   display: block;                                  
   padding-top: 15px;                                  
   padding-bottom: 15px;                                  
  }                                  
 '

      /*Main Layout */
      SET @content = @content
                     + '<div class="receipt-container">'
      /*Billing Invoice Layout*/
      SET @content = @content
                     + '<div class="company-logo"><img src="'
                     + @ImageLogoLocationFilenamePath_Company
                     + '" class="logo"></div>'
      SET @content = @content + '<center><div class="">'
                     + @Name_Company + '</div>'
      SET @content = @content + '<div>' + @Address_Company + '</div>'
      SET @content = @content + '<div>' + @ContactNumber_Company
                     + '</div></center>'
      SET @content = @content
                     + '<div class="title bold">INVOICE</div>'

      IF( @IsWalkIn = 0 )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @Name_Client);
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Pet Name:', @Name_Patient);
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLayout('Bill To:', @WalkInCustomerName);
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('Invoice Date:', FORMAT(@Date_BillingInvoice, 'MM/dd/yyyy'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLayout('BI #:', @CustomCode_BillingInvoice);

      /*Billing Invoice Items*/
      SELECT @biItemslayout = @biItemslayout
                              + dbo.fGetPOSReceiptBillingInvoiceItemsLayout(Name_Item, FORMAT(Quantity, '#,#0'), FORMAT(Amount, '#,#0.00'))
      FROM   vBillingInvoice_Detail biDetail
      WHERE  biDetail.ID_BillingInvoice = @ID_BillingInvoice

      SET @content = @content + '<br/>' + @biItemslayout; /*Billing Invoice Items*/
      /*DEPOSIT*/
      IF( @ConfinementDepositAmount_BillingInvoice > 0 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">DEPOSIT</div>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Initial Billing', FORMAT(@InitialSubtotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Confinement Deposit', FORMAT(@ConfinementDepositAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Consumed Deposit', FORMAT(@ConsumedDepositAmount_BillingInvoice, '#,#0.00'));

            IF( @RemainingDepositAmount_BillingInvoice > 0 )
              BEGIN
                  SET @content = @content
                                 + dbo.fGetPOSReceipt2ColumnsLRLayout('Remaining Deposit', FORMAT(@RemainingDepositAmount_BillingInvoice, '#,#0.00'));
              END
        END

      /*TOTAL*/
      Declare @DiscountWord VARCHAR(MAX) = 'Disc.'

      IF @Guid_Company IN ( @cardinalpetclinic_Guid_Company )
        BEGIN
            SET @DiscountWord = 'Discount'
        END

      SET @content = @content
                     + '<div class="title bold">TOTAL</div>'

      ------------------------------------------------------------------------------------------------------------    
      IF @Guid_Company IN ( @cardinalpetclinic_Guid_Company )
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@GrossSubTotalAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Discount Less', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
            SET @content = @content + '<br/>'
        END
      ELSE
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Subtotal', FORMAT(@SubTotal_BillingInvoice, '#,#0.00'));
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('T. Item '+ @DiscountWord +' Amt.', FORMAT(@TotalItemDiscountAmount_BillingInvoice, '#,#0.00'));
        END

      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Rate', FORMAT(@DiscountRate_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout(@DiscountWord +' Amount', FORMAT(@DiscountAmount_BillingInvoice, '#,#0.00'));
      SET @content = @content
                     + dbo.fGetPOSReceipt2ColumnsLRLayout('Total Amount', FORMAT(@TotalAmount_BillingInvoice, '#,#0.00'));

      IF( @TotalChangeAmount ) > 0
        BEGIN
            SET @content = @content + '<br/>'
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Payment Amount', FORMAT(@TotalPaymentAmount, '#,#0.00'));
        END

      IF( @TotalChangeAmount ) > 0
        BEGIN
            SET @content = @content
                           + dbo.fGetPOSReceipt2ColumnsLRLayout('Change Amount', FORMAT(@TotalChangeAmount, '#,#0.00'));
        END

      IF( @IsShowFooter = 1 )
        BEGIN
            SET @content = @content
                           + '<div class="title bold">THIS IS NOT AN OFFICIAL RECEIPT</div>'
        END

      SET @content = @content + '<div class="title bold">'
                     + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm:ss tt')
                     + '</div>'
      /*END Main Layout */
      SET @content = @content + '</div>' + Char(10) + Char (13)

      SELECT '_'

      SELECT @Code_BillingInvoice title,
             @style               style,
             @content             content,
             @isAutoPrint         isAutoPrint,
             @ID_BillingInvoice   ID_BillingInvoice,
             @millisecondDelay    millisecondDelay
  END

GO

--TRUNCATE TABLE tCompanyCustomReportObjectValue
GO

DECLARE @Guid VARCHAR(MAX) = ''
DECLARE LedgerCursor CURSOR FOR
  SELECT Guid
  FROM   tCompany
  WHERE  GUID IN ( 'DD11E0A3-7281-4F31-9B85-2678FB6E0A16', '6C6B0D25-425B-48CD-956F-F9CABDD21A64', '0056C1C4-D0A2-40AC-9010-E384DC15FCE7' )

OPEN LedgerCursor;

FETCH NEXT FROM LedgerCursor INTO @Guid

WHILE @@FETCH_STATUS = 0
  BEGIN
      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReportNoDiscountPerItem',
        'txtReportTitle',
        'Text',
        'COMPUTATION'

      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReportNoDiscountPerItem',
        'txtBINumberLabel',
        'Text',
        '#:'

      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReportNoDiscountPerItem',
        'txtBIAddressLabel',
        'Text',
        'Address:'

      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReport',
        'txtReportTitle',
        'Text',
        'COMPUTATION'

      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReport',
        'txtBINumberLabel',
        'Text',
        '#:'

      EXEC dbo.pInsertCompanyCustomReportObjectValueByGuidCompany
        @Guid,
        'PatientBillingInvoiceReport',
        'txtBIAddressLabel',
        'Text',
        'Address:'

      FETCH NEXT FROM LedgerCursor INTO @Guid
  END

CLOSE LedgerCursor;

DEALLOCATE LedgerCursor;

GO

SELECT *
FROM   tCompanyCustomReportObjectValue

GO

EXEC dbo.pInsertSystemVersion
  'Company Custom Report Object Value'

GO

SELECT *
FROM   tSystemVersion
ORDER  BY DateCreated DESC 
