GO

IF OBJECT_ID('dbo.tCompanyCustomReportTitle', 'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tCompanyCustomReportTitle',
        0,
        0,
        null

      exec _pAddModelProperty
        'tCompanyCustomReportTitle',
        'Oid_Report',
        1
  END

GO

CREATE     OR
ALTER PROC pInsertcustomReportTitle(@ID_Company        INT,
                                    @Oid_Report        VARCHAR(MAX),
                                    @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_CompanyCustomReportTitle INT = 0

	  IF @ID_Company IS NULL return;

      IF(SELECT COUNT(*)
         FROM   dbo.tCompanyCustomReportTitle
         WHERE  Oid_Report = @Oid_Report
                AND ID_Company = @ID_Company) = 0
        BEGIN
            INSERT INTO [dbo].[tCompanyCustomReportTitle]
                        ([Name],
                         [IsActive],
                         [Oid_Report],
                         ID_Company)
            VALUES      (@customReportTitle,
                         1,
                         @Oid_Report,
                         @ID_Company)
        END

      SELECT @ID_CompanyCustomReportTitle = ID
      FROM   dbo.tCompanyCustomReportTitle
      WHERE  Oid_Report = @Oid_Report
             AND ID_Company = @ID_Company

      UPDATE dbo.tCompanyCustomReportTitle
      SET    Name = @customReportTitle
      WHERE  ID = @ID_CompanyCustomReportTitle
  END

GO

CREATE     OR
ALTER PROC pInsertcustomReportTitleByReportName(@ID_Company        INT,
                                                @Name_Report       VARCHAR(MAX),
                                                @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @Oid_Report VARCHAR(MAX) = '';

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @Name_Report

      EXEC dbo.pInsertcustomReportTitle
        @ID_Company,
        @Oid_Report,
        @customReportTitle
  END

GO

CREATE     OR
ALTER PROC pInsertcustomReportTitleByCompanyGuidReportName(@GUID_Company      VARCHAR(MAX),
                                                           @Name_Report       VARCHAR(MAX),
                                                           @customReportTitle VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company
             AND IsActive = 1
		
      EXEC dbo.pInsertcustomReportTitleByReportName
        @ID_Company,
        @Name_Report,
        @customReportTitle
  END

GO


exec pInsertcustomReportTitleByReportName
  1,
  'PatientBillingInvoiceReport',
  'Computation'

EXEC dbo.pInsertcustomReportTitleByCompanyGuidReportName
  '70CDA78A-9859-4466-AA6D-7701E061F3A9',
  'PatientBillingInvoiceReport',
  'Computation'
  
EXEC dbo.pInsertcustomReportTitleByCompanyGuidReportName
  'DD11E0A3-7281-4F31-9B85-2678FB6E0A16',
  'PatientBillingInvoiceReport',
  'Computation'


EXEC dbo.pInsertcustomReportTitleByCompanyGuidReportName
  '6C6B0D25-425B-48CD-956F-F9CABDD21A64',
  'PatientBillingInvoiceReport',
  'Computation'

  
EXEC dbo.pInsertcustomReportTitleByCompanyGuidReportName
  '0056C1C4-D0A2-40AC-9010-E384DC15FCE7',
  'PatientBillingInvoiceReport',
  'Computation'
 
GO

select *
FROM   [tCompanyCustomReportTitle] 


EXEC dbo.pInsertSystemVersion
  'Custom Report Title'

SELECT ID,
       Name,
       DateCreated
FROM   tSystemVersion
ORDER  BY DateCreated DESC,
          ID DESC 
