exec _pAddModelProperty
  'tItem_Supplier',
  'UnitCost',
  3

GO

CREATE OR
ALTER VIEW vzItemPriceComparisonPerSupplier
AS
  select isupplier.ID,
         isupplier.ID_Item,
         item.Name                     Name_Item,
         supplier.Name                 Name_Supplier,
         ISNULL(isupplier.UnitCost, 0) UnitCost_Item_Supplier,
         company.ID                    ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                  Name_Company,
         company.Address               Address_Company,
         company.ContactNumber         ContactNumber_Company,
         CASE
           WHEN LEN(ISNULL(company.Address, '')) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(ISNULL(company.ContactNumber, '')) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(company.Email, '')) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                         HeaderInfo_Company
  FROM   tItem_Supplier isupplier
         INNER JOIN tItem item
                 ON item.ID = isupplier.ID_Item
         INNER JOIN tSupplier supplier
                 ON isupplier.ID_Supplier = supplier.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company

GO

CREATE OR
ALTER PROC dbo.pGetItem @ID          INT = -1,
                        @ID_ItemType INT = NULL,
                        @ID_Session  INT = NULL
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT;

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session;

      SELECT '_',
             '' Item_Supplier;

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   itemType.Name Name_ItemType
            FROM   (SELECT -1           AS [ID],
                           NULL         AS [Code],
                           NULL         AS [Name],
                           1            AS [IsActive],
                           NULL         AS [ID_Company],
                           NULL         AS [Comment],
                           NULL         AS [DateCreated],
                           NULL         AS [DateModified],
                           NULL         AS [ID_CreatedBy],
                           NULL         AS [ID_LastModifiedBy],
                           @ID_ItemType ID_ItemType) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN dbo.tItemType itemType
                          ON itemType.ID = H.ID_ItemType;
        END;
      ELSE
        BEGIN
            SELECT H.*,
                   CASE
                     WHEN H.OtherInfo_DateExpiration IS NOT NULL THEN dbo.GetRemainingYearMonthDays(H.OtherInfo_DateExpiration, '', 'Expired')
                     ELSE ''
                   END                                                  RemainingBeforeExpired,
                   DATEDIFF(DAY, GETDATE(), H.OtherInfo_DateExpiration) RemainingDays
            FROM   vItem H
            WHERE  H.ID = @ID;
        END;

      SELECT *
      FROm   vItem_Supplier
      WHERE  ID_Item = @ID
      ORDER  BY Name_Supplier
  END;

GO

CREATE   OR
ALTER PROC pInsertUserRoleReport (@Name_UserRole VARCHAR(MAX),
                                  @Name_Report   VARCHAR(MAX))
AS
  BEGIN
      DECLARE @ID_UserRole INT = 0
      DECLARE @Oid_Report VARCHAR(MAX) = ''

      SELECT @Oid_Report = Oid
      FROM   _tReport
      where  Name = @Name_Report

      IF(SELECT COUNT(*)
         FROM   tUserRole
         where  Name = @Name_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole]
                        ([Name],
                         [IsActive],
                         [DateCreated],
                         [DateModified],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         [IsFullAccess])
            SELECT @Name_UserRole,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   0;
        END

      SELECT @ID_UserRole = ID
      FROM   tUserRole
      where  Name = @Name_UserRole

      if(SELECT COUNT(*)
         FROM   tUserRole_Reports
         where  ID_Report = @Oid_Report
                AND ID_UserRole = @ID_UserRole) = 0
        BEGIN
            INSERT INTO [dbo].[tUserRole_Reports]
                        ([Name],
                         [IsActive],
                         [ID_UserRole],
                         [ID_Report])
            SELECT @Name_Report,
                   1,
                   @ID_UserRole,
                   @Oid_Report
        END
  END

GO

if(select COUNT(*)
   FROM   _tReport
   WHERE  Name = 'ItemPriceComparisonPerSupplier') = 0
  BEGIN
      Update _tNavigation
      SET    [Route] = 'ReportItemPriceComparisonPerSupplier',
             Caption = 'Item Price Comparison'
      WHERE  Name = 'ItemPriceComparisonPerSupplier_Navigation'

      exec _pCreateReportView
        'ItemPriceComparisonPerSupplier',
        1;

      exec pInsertUserRoleReport
        'Administrator',
        'ItemPriceComparisonPerSupplier'

      exec pInsertUserRoleReport
        'Item Price Comparison Per Supplier Only',
        'ItemPriceComparisonPerSupplier'

      UPDATE _tNavigation
      SET    Route = 'ReportItemPriceComparisonPerSupplier',
             Caption = 'Item Price Comparison Per Supplier'
      WHERE  Name = 'ItemPriceComparisonPerSupplier_Navigation';
  END

GO 

EXEC dbo.pInsertSystemVersion
  'Item Price Comparison By Supplier'


SELECT ID,
       Name,
       DateCreated
FROM   tSystemVersion
ORDER  BY DateCreated DESC,
          ID DESC 
