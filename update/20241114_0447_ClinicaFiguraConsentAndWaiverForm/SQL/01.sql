IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'ClinicaVeterinaria-ConsentAndWaiverForm') = 0
  BEGIN
      exec _pCreateReportView
        'ClinicaVeterinaria-ConsentAndWaiverForm',
        NULL

      exec pAddModelReport
        44,
        'Patient',
        'ClinicaVeterinaria-ConsentAndWaiverForm'

      DECLARE @Oid_Report VARCHAR(MAX)

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = 'ClinicaVeterinaria-ConsentAndWaiverForm'

      UPDate dbo.[_tModelReport]
      SET    Name = 'Consent And Waiver Form'
      WHERE  [Oid_Report] = @Oid_Report
  END 
