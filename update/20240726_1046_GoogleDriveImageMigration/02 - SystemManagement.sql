USE [SystemManagement]

GO

IF NOT EXISTS (SELECT *
               FROM   sys.tables
               WHERE  schema_id = SCHEMA_ID('dbo')
                      AND name = 'tForUploadToDropbox')
  BEGIN
      CREATE TABLE [dbo].[tForUploadToDropbox]
        (
           [ID]                    [int] IDENTITY(1, 1) NOT NULL,
           [DatabaseName]          [varchar](max) NULL,
           [ID_Company]            [int] NULL,
           [Name_Company]          [varchar](max) NULL,
           [GUID_Company]          [varchar](max) NULL,
           [TableName]             [varchar](max) NULL,
           [Model]                 [varchar](max) NULL,
           [ID_CurrentObject]      [int] NULL,
           [ColumnName]            [varchar](max) NULL,
           [OriginalImageFileName] [varchar](max) NULL,
           [ImageFileName]         [varchar](max) NULL,
           [ImageFilePath]         [varchar](max) NULL,
           [IsUpload]              [bit] NULL,
           [FileID]                [varchar](max) NULL,
           [ImageUrl]              [varchar](max) NULL,
           [DateCreated]           [datetime] NULL,
           [DateUploaded]          [datetime] NULL,
           [Comment]               [varchar](max) NULL,
           [IsError]               [bit] NULL,
           [DateError]             [datetime] NULL,
           [ErrorMessage]          [varchar](max) NULL,
           CONSTRAINT [PK_tForUploadToDropbox] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
        )
      ON [PRIMARY]
      TEXTIMAGE_ON [PRIMARY];
  END

GO

CREATE   OR
ALTER PROC [dbo].[pGenerateForUploadToDropboxRecords]
AS
  BEGIN
      DECLARE @table TABLE
        (
           DatabaseName     VARCHAR(MAX),
           ID_Company       INT,
           Name_Company     VARCHAR(MAX),
           GUID_Company     VARCHAR(MAX),
           TableName        VARCHAR(MAX),
           Model            VARCHAR(MAX),
           ID_CurrentObject INT,
           ColumnName       VARCHAR(MAX),
           ImageFileName    VARCHAR(MAX),
           ImageFilePath    VARCHAR(MAX)
        )
      DECLARE @name VARCHAR(50) -- database name                 
      DECLARE @dateStart DateTime
      DECLARE @dateEnd DateTime
      DECLARE db_cursorpBackUpDatabase_VetCloudv3 CURSOR FOR
        SELECT name
        FROM   MASTER.dbo.sysdatabases
        WHERE  ( name LIKE 'db_vetcloudv3_oldserver' )
        Order  by crdate

      OPEN db_cursorpBackUpDatabase_VetCloudv3

      FETCH NEXT FROM db_cursorpBackUpDatabase_VetCloudv3 INTO @name

      WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @dateStart = GETDATE();

            DECLARE @sql VARCHAR(MAX)= '' + + 'SELECT ' + ' ID_Company, '
              + ' Name_Company, ' + ' GUID_Company, '
              + ' TableName, ' + ' Model, '
              + ' ID_CurrentObject, ' + ' ColumnName, '
              + ' ImageFileName, '
              + ' FolderName + ''/'' + ImageFileName ImageFilePath '
              + 'FROM ' + @name
              + '.dbo.fGetImageFileList(''''), ( SELECT ''Image'' FolderName) tbl '

            --print @sql      
            INSERT @table
                   (ID_Company,
                    Name_Company,
                    GUID_Company,
                    TableName,
                    Model,
                    ID_CurrentObject,
                    ColumnName,
                    ImageFileName,
                    ImageFilePath)
            exec(@sql);

            DELETE FROM @table
            WHERE  ImageFileName IN (SELECT FileID
                                     FROM   tForUploadToDropbox
                                     where  FileID IS NOT NULL)

            insert tForUploadToDropbox
                   (DatabaseName,
                    ID_Company,
                    Name_Company,
                    GUID_Company,
                    TableName,
                    Model,
                    ID_CurrentObject,
                    ColumnName,
                    OriginalImageFileName,
                    ImageFileName,
                    ImageFilePath,
                    DateCreated)
            select @name,
                   ID_Company,
                   Name_Company,
                   GUID_Company,
                   TableName,
                   Model,
                   ID_CurrentObject,
                   ColumnName,
                   ImageFileName OriginalImageFileName,
                   ImageFileName,
                   ImageFilePath,
                   GETDATE()
            from   (SELECT ID_Company,
                           Name_Company,
                           GUID_Company,
                           TableName,
                           Model,
                           ID_CurrentObject,
                           ColumnName,
                           ImageFileName,
                           ImageFilePath
                    FROM   @table
                    EXCEPT
                    SELECT ID_Company,
                           Name_Company,
                           GUID_Company,
                           TableName,
                           Model,
                           ID_CurrentObject,
                           ColumnName,
                           OriginalImageFileName,
                           ImageFilePath
                    FROM   tForUploadToDropbox) tbl

            FETCH NEXT FROM db_cursorpBackUpDatabase_VetCloudv3 INTO @name
        END

      CLOSE db_cursorpBackUpDatabase_VetCloudv3

      DEALLOCATE db_cursorpBackUpDatabase_VetCloudv3
  END

GO

CREATE   OR
ALTER PROC [dbo].[pGetPendingForUploadToDropbox]
as
  BEGIN
      DECLARE @PageNumber INT = 1;
      DECLARE @PageSize INT = 20000;

      SELECT a.*
      FROM   tForUploadToDropbox a
      WHERE  1 = 1
             AND IsNull(IsUpload, 0) = 0
             AND IsNull(IsError, 0) = 0
             AND ISNULL(ID_CurrentObject, 0) > 0
             AND LEN(ISNULL(GUID_Company, '')) > 0
      ORDER  BY CASE
                  WHEN TableName = 'tCompany' THEN 0
                  ELSE 1
                END,
                a.ID DESC
      OFFSET (@PageNumber - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY;
  END

GO 
