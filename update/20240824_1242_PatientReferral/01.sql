GO

IF OBJECT_ID('dbo.tPatientReferral', 'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tPatientReferral',
        1,
        null,
        null;

      exec _pAddModelProperty
        'tPatientReferral',
        'Date',
        5;

      exec _pAddModelProperty
        'tPatientReferral',
        'ID_Client',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'ID_Patient',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'AttendingPhysician_ID_Employee',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'ClientContactNumber',
        1;

      exec _pAddModelProperty
        'tPatientReferral',
        'Age',
        1;

      exec _pAddModelProperty
        'tPatientReferral',
        'ID_FilingStatus',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'History',
        1;

      exec _pAddModelProperty
        'tPatientReferral',
        'Treatment',
        1;

      exec _pAddModelProperty
        'tPatientReferral',
        'Concerns',
        1;

      exec _pAddModelProperty
        'tPatientReferral',
        'DateCanceled',
        5;

      exec _pAddModelProperty
        'tPatientReferral',
        'ID_CanceledBy',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'DateUndoCanceled',
        5;

      exec _pAddModelProperty
        'tPatientReferral',
        'ID_UndoCanceledBy',
        2;

      exec _pAddModelProperty
        'tPatientReferral',
        'ReferralTypeOthers',
        1;

      exec pInsertUserRoleModel
        'Pet Grooming Only',
        'tPatientReferral';

      exec pInsertUserRoleModel
        'Administrator',
        'tPatientReferral';

      exec pInsertUserRoleModel
        'All Admission',
        'tPatientReferral';

      IF(SELECT COUNT(*)
         FROm   vDocumentSeries
         where  Name = 'PatientReferral') = 0
        BEGIN
            exec pAddDocumentSeries
              'tPatientReferral',
              'RFR',
              1,
              6;
        END;

      Update _tNavigation
      SET    Caption = 'Referral',
             Route = 'PatientReferralList',
             ID_Parent = '24C522DA-EA51-439F-BE1B-F7D096BEEF51'
      WHERE  Name = 'PatientReferral_ListView';

      EXEC _pCreateAppModuleWithTable
        'tPatientReferral_ReferralType',
        NULL,
        1,
        NULL;

      EXEC _pAddModelProperty
        'tPatientReferral_ReferralType',
        'ID_PatientReferral',
        2;

      EXEC _pAddModelDetail
        'tPatientReferral_ReferralType',
        'ID_PatientReferral',
        'tPatientReferral',
        'tPatientReferral';

      EXEC _pAddModelProperty
        'tPatientReferral_ReferralType',
        'ID_ReferralType',
        2;

      exec _pCreateReportView
        'ReferralReport',
        NULL;
  END

GO

ALTER TABLE dbo.tPatientReferral
  ALTER COLUMN History VARCHAR(5000);

GO

ALTER TABLE dbo.tPatientReferral
  ALTER COLUMN Treatment VARCHAR(5000);

GO

ALTER TABLE dbo.tPatientReferral
  ALTER COLUMN Concerns VARCHAR(5000);

GO

ALTER TABLE dbo.tPatientReferral
  ALTER COLUMN ReferralTypeOthers VARCHAR(5000);

GO

IF OBJECT_ID('dbo.tReferralType', 'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tReferralType',
        1,
        NULL,
        NULL;
  END

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetVeterinaryHealthCertificateVaccinationOptionComment](@VaccinationOptionComment VARCHAR(MAX),
                                                                               @DateVaccinated           DateTime,
                                                                               @Name_Item                VARCHAR(MAX),
                                                                               @LotNumber                VARCHAR(MAX),
                                                                               @CaseType                 VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @DateVacinatedString VARCHAR(MAX) = ''

      IF @DateVaccinated IS NOT NULL
        SET @DateVacinatedString = FORMAT(@DateVaccinated, 'MM/dd/yyyy')

      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Date*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                             + @DateVacinatedString
                                                                                             + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Vaccination*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                                    + IsNULL(@Name_Item, '_______________')
                                                                                                    + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Serial/Lot Number*****/', '<u>&nbsp;&nbsp;&nbsp;'
                                                                                                          + IsNULL(@LotNumber, '_______________')
                                                                                                          + '&nbsp;&nbsp;&nbsp;</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****CaseType*****/', IsNULL(@CaseType, '&nbsp;&nbsp;&nbsp;rabies&nbsp;&nbsp;&nbsp;'))

      RETURN @VaccinationOptionComment
  END

GO

CREATE    OR
ALTER PROC pInsertReferralType(@referralType VARCHAR(MAX))
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReferralType
         WHERE  Name = @referralType) = 0
        BEGIN
            INSERT INTO [dbo].tReferralType
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@referralType,
                         1,
                         1)
        END
  END

GO

exec _pRefreshAllViews;

GO

CREATE OR
ALTER VIEW vPatientReferral
AS
  SELECT H.*,
         UC.Name           AS CreatedBy,
         UM.Name           AS LastModifiedBy,
         client.Name       Name_Client,
         patient.Name      Name_Patient,
         fs.Name           Name_FilingStatus,
         attendingVet.Name AttendingPhysician_Name_Employee
  FROM   tPatientReferral H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on client.ID = h.ID_Client
         LEFT JOIN tPatient patient
                on H.ID_Patient = patient.ID
         LEFT JOIN tEmployee attendingVet
                on H.AttendingPhysician_ID_Employee = attendingVet.ID
         LEFT JOIN tFilingStatus fs
                on H.ID_FilingStatus = fs.ID

GO

CREATE    OR
ALTER VIEW vPatientReferral_ListvIew
AS
  SELECT ID,
         Date,
         Code,
         ID_Client,
         ID_Patient,
         Name_Client,
         Name_Patient,
         ID_FilingStatus,
         Name_FilingStatus,
         ID_Company,
         AttendingPhysician_Name_Employee,
         History,
         Treatment,
         Concerns,
         LEN(ISNULL(History, ''))   Count_History,
         LEN(ISNULL(Treatment, '')) Count_Treatment,
         LEN(ISNULL(Concerns, ''))  Count_Concerns
  FROm   vPatientReferral

GO

CREATE      OR
ALTER VIEW dbo.vPatientReferral_AttendingVeterinarianDropdown
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Company,
         hed.Name_Position
  FROM   vEmployee hed
  WHERE  ( hed.ID_Position IN ( 10, 4, 5, 6,
                                13, 14, 15, 16,
                                3, 19, 20, 31 )
            OR ISNULL(hed.Name_Position, '') LIKE '%Veterinarian%'
            OR ISNULL(hed.Name_Position, '') LIKE '%Groomer%' )
         AND ISNULL(hed.IsActive, 0) = 1
         AND LOWER(hed.Name) NOT LIKE 'system%'
         AND LOWER(hed.Name) NOT LIKE 'sole%'

GO

CREATE OR
ALTER VIEW vPatientReferral_ReferralType
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         refType.Name Name_ReferralType
  FROM   tPatientReferral_ReferralType H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tReferralType refType
                ON H.ID_ReferralType = refType.ID

GO

CREATE OR
ALTER VIEW vPatientReferralReferralTypeCount
as
  SELECT ID_PatientReferral,
         ISNULL([1], 0) AS ReferralType_1,
         ISNULL([2], 0) AS ReferralType_2,
         ISNULL([3], 0) AS ReferralType_3,
         ISNULL([4], 0) AS ReferralType_4,
         ISNULL([5], 0) AS ReferralType_5,
         ISNULL([6], 0) AS ReferralType_6
  FROM   (SELECT ID_PatientReferral,
                 ID_ReferralType
          FROM   tPatientReferral_ReferralType) AS SourceTable
         PIVOT ( COUNT(ID_ReferralType)
               FOR ID_ReferralType IN ([1],
                                       [2],
                                       [3],
                                       [4],
                                       [5],
                                       [6]) ) AS PivotTable;

GO

CREATE OR
ALTER VIEW [dbo].[vzPatientReferralForm]
AS
  SELECT hed.ID,
         hed.Code,
         hed.Date,
         hed.History,
         hed.Treatment,
         hed.Concerns,
         client.Name                                   Name_Client,
         client.ContactNumber                          ContactNumber_Client,
         patient.Name                                  Name_Patient,
         patient.Species,
         patient.DateBirth,
         gender.Name                                   Name_Gender,
         dbo.fGetAge(patient.DateBirth, GETDATE(), '') Age_Patient,
         attendingPhysician.Name                       AttendingPhysician_Name_Employee,
         attendingPhysician.PRCLicenseNumber           AttendingPhysician_PRCLicenseNumber_Employee,
         referralTypeCount.ReferralType_1,
         referralTypeCount.ReferralType_2,
         referralTypeCount.ReferralType_3,
         referralTypeCount.ReferralType_4,
         referralTypeCount.ReferralType_5,
         referralTypeCount.ReferralType_6,
         hed.ReferralTypeOthers,
         hed.ID_Company,
         filingStatus.Name                             Name_FilingStatus,
         company.ImageLogoLocationFilenamePath,
         company.Name                                  Name_Company,
         company.Address                               Address_Company,
         company.ContactNumber                         ContactNumber_Company,
         CASE
           WHEN LEN(ISNULL(company.Address, '')) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(ISNULL(company.ContactNumber, '')) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(ISNULL(company.Email, '')) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                         HeaderInfo_Company
  FROM   tPatientReferral hed
         LEFT JOIN tClient client
                on hed.ID_Client = client.ID
         LEFT JOIN tPatient patient
                on hed.ID_Patient = patient.ID
         LEFT JOIN tEmployee attendingPhysician
                on hed.AttendingPhysician_ID_Employee = attendingPhysician.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
         LEFT JOIN dbo.tFilingStatus filingStatus
                ON filingStatus.ID = hed.ID_FilingStatus
         LEFT JOIN dbo.vPatientReferralReferralTypeCount referralTypeCount
                on hed.ID = referralTypeCount.ID_PatientReferral
         LEFT JOIN dbo.tGender gender
                on patient.ID_Gender = gender.ID

GO

CREATE OR
ALTER VIEW [dbo].[vzVeterinaryHealthClinicReport]
AS
  SELECT vetcert.ID,
         vetcert.Code,
         vetcert.Date,
         vetcert.DestinationAddress,
         vetcert. Color,
         vetcert. Weight,
         vetcert.ID_Item,
         vetcert.Name_Item,
         vetcert.LotNumber,
         patient.ID                                                                                                                                                      ID_Patient,
         patient.NAME                                                                                                                                                    Name_Patient,
         IsNull(Species, '')                                                                                                                                             Name_Species,
         IsNull(Name_Gender, '')                                                                                                                                         Name_Gender,
         patient.Microchip,
         IsNull(FORMAT(patient.DateBirth, 'MM/dd/yyyy'), '')                                                                                                             Birthdate,
         dbo.fGetAge(patient.DateBirth, CASE
                                          WHEN IsNull(patient.IsDeceased, 0) = 1 THEN patient.DateDeceased
                                          ELSE NULL
                                        END, 'N/A')                                                                                                                      Age_Patient,
         IsNull(client.NAME, '')                                                                                                                                         Name_Client,
         IsNull(client.Address, '')                                                                                                                                      Address_Client,
         IsNull(client.ContactNumber, '')                                                                                                                                ContactNumber_Client,
         IsNull(client.ContactNumber2, '')                                                                                                                               ContactNumber2_Client,
         IsNull(client.Email, '')                                                                                                                                        Email_Client,
         company.ID                                                                                                                                                      ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.NAME                                                                                                                                                    Name_Company,
         company.Address                                                                                                                                                 Address_Company,
         company.ContactNumber                                                                                                                                           ContactNumber_Company,
         CASE
           WHEN len(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN len(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN len(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                                                                                                           HeaderInfo_Company,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         _AttendingPhysician.TINNumber,
         _AttendingPhysician.PTR,
         _AttendingPhysician.PRCLicenseNumber,
         CASE
           WHEN _AttendingPhysician.DatePRCExpiration IS NOT NULL THEN FORMAT(_AttendingPhysician.DatePRCExpiration, 'MM/dd/yyyy')
           ELSE ''
         END                                                                                                                                                             DatePRCExpiration,
         CASE
           WHEN vetCert.DateVaccination IS NOT NULL THEN FORMAT(vetCert.DateVaccination, 'MM/dd/yyyy')
           ELSE '                '
         END                                                                                                                                                             DateVaccination,
         dbo.fGetVeterinaryHealthCertificateVaccinationOptionComment(vacOption.Comment, vetCert.DateVaccination, vetcert.Name_Item, vetcert.LotNumber, vetCert.CaseType) Comment_VaccinationOption,
         ISNULL(vetCert.ValidityDayCount, 0)                                                                                                                             ValidityDayCount,
         ISNULL(vetCert.CaseType, '')                                                                                                                                    CaseType,
         ID_VaccinationOption
  FROM   vVeterinaryHealthCertificate vetCert
         LEFT JOIN vPatient patient
                ON vetCert.ID_Patient = patient.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = patient.ID_Client
         LEFT JOIN dbo.vCompany company
                ON company.ID = vetCert.ID_Company
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = vetCert.AttendingPhysician_ID_Employee
         LEFT JOIN tVaccinationOption vacOption
                on vetCert.ID_VaccinationOption = vacOption.ID

GO

CREATE OR
ALTER PROC pGetPatientReferral @ID         INT = -1,
                               @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS PatientReferral_ReferralType

      DECLARE @ID_Company   INT,
              @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROm   vUser
      where  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL        AS [_],
                           -1          AS [ID],
                           '--New--'   AS [Code],
                           NULL        AS [Name],
                           GETDATE()   AS [Date],
                           1           AS [IsActive],
                           @ID_Company AS [ID_Company],
                           NULL        AS [Comment],
                           NULL        AS [DateCreated],
                           NULL        AS [DateModified],
                           NULL        AS [ID_CreatedBy],
                           NULL        AS [ID_LastModifiedBy],
                           1           AS ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vPatientReferral H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vPatientReferral_ReferralType
      WHERE  ID_PatientReferral = @ID
  END

GO

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_PatientReferral] (@ID_CurrentObject VARCHAR(10),
                                                      @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPatientReferral
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  m.TableName = 'tPatientReferral';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPatientReferral
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

GO

GO

CREATE   OR
ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientAppointmentRequest'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_ClientAppointmentRequest]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PayablePayment'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_PayablePayment]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Grooming'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Patient_Grooming]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'InactiveSMSSending'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_InactiveSMSSending]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Employee'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Employee
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE
        BEGIN
            DECLARE @StoredProcedureName VARCHAR(MAX) = '';
            DECLARE @sql VARCHAR(MAX) = '';

            SET @StoredProcedureName = 'pModel_AfterSaved_/*ModelName*/'
            SET @StoredProcedureName = REPLACE(@StoredProcedureName, '/*ModelName*/', @ModelName)

            IF EXISTS (SELECT 1
                       FROM   sys.objects
                       WHERE  type = 'P' -- 'P' stands for stored procedure
                              AND name = 'pModel_AfterSaved_')
              BEGIN
                  SET @sql = 'EXEC dbo./*StoredProcedureName*/ ''/*ID_CurrentObject*/'', ''/*IsNew*/''';
                  SET @sql = REPLACE(@sql, '/*StoredProcedureName*/', @StoredProcedureName)
                  SET @sql = REPLACE(@sql, '/*ID_CurrentObject*/', @ID_CurrentObject)
                  SET @sql = REPLACE(@sql, '/*IsNew*/', ISNULL(@IsNew, 'NULL'))

                  EXEC (@sql);
              END

            PRINT 1;
        END

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pCancelPatientReferral] (@IDs_PatientReferral typIntList READONLY,
                                           @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatientReferral
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tPatientReferral bi
                 INNER JOIN @IDs_PatientReferral ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE OR
ALTER PROC [dbo].[pUndoCancelPatientReferral] (@IDs_PatientReferral typIntList READONLY,
                                               @ID_UserSession      INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @IDs_Patient TYPINTLIST

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          UPDATE dbo.tPatientReferral
          SET    ID_FilingStatus = 1,
                 DateCanceled = NULL,
                 ID_CanceledBy = NULL,
                 DateUndoCanceled = GETDATE(),
                 ID_UndoCanceledBy = @ID_User
          FROM   dbo.tPatientReferral bi
                 INNER JOIN @IDs_PatientReferral ids
                         ON bi.ID = ids.ID;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO

CREATE OR ALTER PROC pModel_AfterSaved_PatientAppointment (@ID_CurrentObject VARCHAR(10),
                                                  @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Schedule INT = 0
  END

GO

exec pInsertReferralType
  'Case Transfer';

exec pInsertReferralType
  'Internal Medicine';

exec pInsertReferralType
  'Surgical Consult / Surgery';

exec pInsertReferralType
  'Dental Surgery';

exec pInsertReferralType
  'Dental Surgery / Prophylaxis';

exec pInsertReferralType
  'Others'; 


EXEC dbo.pInsertSystemVersion
  'Patient Referral'

SELECT ID,
       Name,
       DateCreated
FROM   tSystemVersion
ORDER  BY DateCreated DESC,
          ID DESC 