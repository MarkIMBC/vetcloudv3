GO

CREATE   OR
ALTER VIEW [dbo].[vzMedicalServiceListReport]
AS
  SELECT _soap.Date,
         _client.Name            Name_Client,
         _patient.Name           Name_Patient,
         _soapType.Name          Name_SOAPType,
         LTRIM(RTRIM(item.Name)) Name_Item,
         _fs.ID                  ID_FilingStatus,
         _fs.Name                Name_FilingStatus,
         _soap.ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name            Name_Company,
         company.Address         Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                   HeaderInfo_Company
  FROM   dbo.tPatient_SOAP _soap
         LEFT JOIN tPatient_SOAP_Treatment _prescription
                ON _soap.ID = _prescription.ID_Patient_SOAP
         LEFT JOIN tItem item
                on item.ID = _prescription.ID_Item
         LEFT JOIN tSOAPType _soapType
                on _soapType.ID = _soap.ID_SOAPType
         LEFT JOIN tClient _client
                on _client.ID = _soap.ID_Client
         LEFT JOIN tPatient _patient
                on _patient.ID = _soap.ID_Patient
         LEFT JOIN tFilingStatus _fs
                on _fs.ID = _soap.ID_FilingStatus
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
  WHERE  item.ID_ITEMTYpe = 1
  UNION ALL
  SELECT _soap.Date,
         _client.Name            Name_Client,
         _patient.Name           Name_Patient,
         'Wellness'              Name_SOAPType,
         LTRIM(RTRIM(item.Name)) Name_Item,
         _fs.ID                  ID_FilingStatus,
         _fs.Name                Name_FilingStatus,
         _soap.ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name            Name_Company,
         company.Address         Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                   HeaderInfo_Company
  FROM   dbo.tPatient_Wellness _soap
         LEFT JOIN tPatient_Wellness_Detail _prescription
                ON _soap.ID = _prescription.ID_Patient_Wellness
         LEFT JOIN tItem item
                on item.ID = _prescription.ID_Item
         LEFT JOIN tClient _client
                on _client.ID = _soap.ID_Client
         LEFT JOIN tPatient _patient
                on _patient.ID = _soap.ID_Patient
         LEFT JOIN tFilingStatus _fs
                on _fs.ID = _soap.ID_FilingStatus
         LEFT JOIN dbo.vCompany company
                ON company.ID = item.ID_Company
  WHERE  item.ID_ITEMTYpe = 1

GO

if(SELECT COUNT(*)
   FROM   _tNavigation
   WHERE  Name = 'MedicalListReport_Navigation') = 0
  BEGIN
      exec _pCreateReportView
        'MedicalServiceListReport',
        1

      exec pAddUserRoleReport
        'Administrator',
        'MedicalServiceListReport'

      UPDATE _tNavigation
      SEt    Route = 'ReportMedicalServiceList',
             Caption = 'Medical Service List Report'
      WHERE  Name = 'MedicalListReport_Navigation'
  END

GO

EXEC dbo.pInsertSystemVersion
  'Medical Service List Report'

exec dbo.pInsertSOAPType
  'Wellness' 
