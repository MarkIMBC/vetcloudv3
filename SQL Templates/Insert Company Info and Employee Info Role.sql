DECLARE @IDs_UserRole typIntlist

INSERT @IDs_UserRole
select MAX(_userrole.ID)
FROM   tUserRole _userrole
where  Name LIKE '%EMployee Info%'
        OR Name LIKE '%Company Info%'
GROUP BY Name

INSERT tUser_Roles
       (ID_User,
        Name,
        IsActive,
        ID_UserRole)
SELECT _user.ID,
       NULL,
       1,
       _userrole.ID
FROM   @IDs_UserRole _userrole,
       vActiveUser _user 
	Where _user.Name_Company like '%Pet Haven%'
