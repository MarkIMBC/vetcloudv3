GO

ALTER PROCEDURE [dbo].[_pCreateTable] @TableName VARCHAR(500)
AS
  BEGIN
      DECLARE @ModelName VARCHAR(200) = NULL

      SELECT @ModelName = ( CASE
                              WHEN SUBSTRING(@TableName, 1, 1) = '_' THEN SUBSTRING(@TableName, 3, LEN(@TableName))
                              ELSE SUBSTRING(@TableName, 2, LEN(@TableName))
                            END )

      EXEC ( 'CREATE TABLE ['+@TableName+'] (  
   [ID] [int] IDENTITY (1, 1) NOT NULL ,  
   [Code] [varchar] (50),  
   [Name] [varchar] (200),  
   [IsActive] [bit] NULL DEFAULT (1),  
   [ID_Company] [int] NULL ,  
   [Comment] [varchar] (MAX) NULL,  
   [DateCreated] [Datetime] NULL ,  
   [DateModified] [DateTime] NULL ,  
   [ID_CreatedBy] [int] NULL ,  
   [ID_LastModifiedBy] [int] NULL ,  
        CONSTRAINT [PK_'+@TableName+'] PRIMARY KEY  CLUSTERED   
        (  
        [ID]  
        )  ON [PRIMARY] ,  
        ) ON [PRIMARY]  
  --GO  
  
        CREATE  INDEX [IX_'+@TableName+'] ON [dbo].['+@TableName+']([Name]) ON [PRIMARY]  
  ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT [FK_' + @TableName + '_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])  
  --GO  
  ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT [FK_' + @TableName + '_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])  
  --GO  
  ALTER TABLE ' + @TableName + ' WITH CHECK ADD  CONSTRAINT [FK_' + @TableName + '_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID])  
  ' )

      EXEC('  
  CREATE TRIGGER [dbo].[rDateCreated_' + @TableName + '] ON [dbo].[' + @TableName + '] FOR INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.' + @TableName + ' SET DateCreated = GETDATE() WHERE ID = @ID  
  ')

      EXEC('  
  CREATE TRIGGER [dbo].[rDateModified_' + @TableName + '] ON [dbo].[' + @TableName + '] FOR UPDATE, INSERT  
   AS  
    DECLARE @ID INT  
    SELECT @ID = ID FROM Inserted  
    UPDATE dbo.' + @TableName + ' SET DateModified = GETDATE() WHERE ID = @ID  
  ')

      --  
      --  
      --  
      EXEC('CREATE VIEW v' + @ModelName + ' AS   
    SELECT   
     H.* ,  
     UC.Name AS CreatedBy,  
     UM.Name AS LastModifiedBy  
    FROM ' + @TableName + ' H  
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID   
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID')

      EXEC('CREATE VIEW vActive' + @ModelName + ' AS   
    SELECT   
     H.*  
    FROM v' + @ModelName + ' H  
    WHERE IsActive = 1')

      EXEC('CREATE VIEW vInactive' + @ModelName + ' AS   
    SELECT   
     H.*  
    FROM v' + @ModelName + ' H  
    WHERE ISNULL(IsActive, 0) = 1')

      --  
      --  
      --  
      RETURN
  --DECLARE @DEFAULT_VALUES VARCHAR(MAX) = 'SELECT ' + CHAR(13) + ' NULL AS [_]' + CHAR(13)  
  --SELECT  --NULL ,  
  -- @DEFAULT_VALUES = COALESCE(@DEFAULT_VALUES + ' , ', '') +   
  -- ( CASE   
  --  WHEN t.system_type_id = 56 AND c.name = 'ID' THEN ' -1' --2 --INT  
  --  WHEN t.system_type_id = 56 AND c.name <> 'ID' THEN 'NULL' --2 --INT  
  --  WHEN t.system_type_id = 167 THEN 'NULL' --1 --STRING  
  --  WHEN t.system_type_id = 104 AND c.name = 'IsActive' THEN '1' --BOOL  
  --  WHEN t.system_type_id = 104 THEN 'NULL' --BOOL  
  --  WHEN t.system_type_id = 106 THEN 'NULL' -- DECIMAL  
  --  WHEN t.system_type_id = 40 THEN 'NULL' --DATE  
  --  ELSE ' NULL'  
  -- END )  + ' AS [' + C.name + ']' + CHAR(13)  
  --FROM sys.columns c  
  --LEFT JOIN sys.types t ON c.system_type_id = t.system_type_id  
  --WHERE   OBJECT_NAME(object_id) = @TableName  
  --  
  --EXEC('CREATE PROCEDURE pGet' + @ModelName + ' @ID INT = -1, @ID_Session INT = NULL  
  -- AS  
  -- BEGIN  
  --  SELECT ''  
  --  
  --  IF (@ID = -1)  
  --   BEGIN  
  --    SELECT   
  --     H.*   
  --    FROM (   
  --     ' + @DEFAULT_VALUES + '  
  --    ) H  
  --   END  
  --  ELSE  
  --   BEGIN  
  --    SELECT   
  --     H.*   
  --    FROM v' + @ModelName   + ' H   
  --    WHERE H.ID = @ID  
  --   END  
  -- END')  
  END

GO

GO

DECLARE @tableName VARCHAR(MAX) -- database name 
DECLARE @messages TABLE
  (
     Message   VARCHAR(MAX),
     TableName VARCHAR(MAX),
     ModelName Varchar(MAX)
  )
DECLARE db_cursor CURSOR FOR
  SELECT TABLE_NAME
  FROM   INFORMATION_SCHEMA.TABLES
  WHERE  TABLE_TYPE = 'BASE TABLE'
  Order  by TABLE_NAME

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @tableName

WHILE @@FETCH_STATUS = 0
  BEGIN
      BEGIN TRY
          DECLARE @ModelName VARCHAR(MAX) = RIGHT(@tableName, LEN(@tableName) - 1)

          IF OBJECT_ID('v' + @ModelName, 'V') IS NULL
            BegIN
                EXEC('CREATE VIEW v' + @ModelName + ' AS   
						SELECT   
						 H.*  
						FROM t' + @ModelName + ' H');
            END

          IF OBJECT_ID('vActive' + @ModelName, 'V') IS NULL
            BegIN
                EXEC('CREATE VIEW vActive' + @ModelName + ' AS   
				SELECT   
				 H.*  
				FROM v' + @ModelName + ' H  
				WHERE IsActive = 1')
            END

          IF OBJECT_ID('vInactive' + @ModelName + '', 'V') IS NULL
            BegIN
                EXEC('CREATE VIEW vInactive' + @ModelName + ' AS   
				SELECT   
				 H.*  
				FROM v' + @ModelName + ' H  
				WHERE ISNULL(IsActive, 0) = 0')
            END
      END TRY
      BEGIN CATCH
          INSERT @messages
          SELECT ERROR_MESSAGE(),
                 @tableName,
                 @ModelName
      END CATCH

      FETCH NEXT FROM db_cursor INTO @tableName
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT *
FROM   @messages 
