DECLARE @table TABLE
  (
     ID_Company           INT,
     Name_Company         VARCHAR(MAX),
     Name_Client          VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     DateReturn           DATETIME,
     Name_Item            VARCHAR(MAX),
     Comment              VARCHAR(MAX),
     Message              VARCHAR(MAX),
     DateSending          DATETIME,
     DateCreated          DATETIME,
     ID_Reference         INT,
     Oid_Model            VARCHAR(MAX),
     Code                 VARCHAR(MAX),
     Name_Patient         VARCHAR(MAX)
  )
DECLARE @DateStart DATE = '2000-01-01'
DECLARE @DateEnd DATE = '2022-01-31'
DECLARE @IsSMSSent BIT = NULL
DECLARE @IDsCompanyString VARCHAR(MAX) = '183'
DECLARE @IDs_Company TYPINTLIST
DECLARE @DayBeforeInterval INT =1
DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''

SELECT @Patient_Vaccination_Schedule_ID_Model = Oid
FROM   _tModel
WHERE  TableName = 'tPatient_Vaccination_Schedule'

SELECT @Patient_SOAP_Plan_ID_Model = Oid
FROM   _tModel
WHERE  TableName = 'tPatient_SOAP_Plan'

SELECT @Patient_Wellness_Schedule_ID_Model = Oid
FROM   _tModel
WHERE  TableName = 'tPatient_Wellness_Schedule'

IF( len(Trim(@IDsCompanyString)) > 0 )
  BEGIN
      INSERT @IDs_Company
      SELECT Part
      FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
  END
ELSE
  BEGIN
      INSERT @IDs_Company
      SELECT ID_Company
      FROM   tCompany_SMSSetting cSMSSetting
             INNER JOIN tCompany c
                     on cSMSSetting.ID_Company = c.ID
      WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
             and c.IsActive = 1
  END

DECLARE @Success BIT = 1;
DECLARE @SMSSent TABLE
  (
     IsSMSSent BIT
  )

IF @IsSMSSent IS NULL
  INSERT @SMSSent
  VALUES (0),
         (1)
ELSE
  INSERT @SMSSent
  VALUES (@IsSMSSent)

/*SOAP PLAN */
INSERT @table
SELECT c.ID                                                                                                                                                                                   ID_Company,
       c.NAME                                                                                                                                                                                 Name_Company,
       client.NAME                                                                                                                                                                            Name_Client,
       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
       soapPlan.DateReturn,
       soapPlan.Name_Item,
       IsNull(patientSOAP.Comment, '')                                                                                                                                                        Comment,
       dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, soapPlan.Name_Item, IsNull(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
       CONVERT(DATE, DateAdd(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
       patientSOAP.DateCreated,
       soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
       @Patient_SOAP_Plan_ID_Model,
       patientSOAP.Code,
       patient.Name                                                                                                                                                                           Name_Patient
FROM   dbo.tPatient_SOAP patientSOAP
       LEFT JOIN dbo.tPatient patient
              ON patient.ID = patientSOAP.ID_Patient
       LEFT JOIN dbo.tClient client
              ON client.ID = patient.ID_Client
       LEFT JOIN tCompany c
              ON c.iD = patientSOAP.ID_Company
       INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
               ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
       INNER JOIN @IDs_Company idsCompany
               ON patientSOAP.ID_Company = idsCompany.ID
WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
       AND IsNull(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                             FROM   @SMSSent)
       AND IsNull(patientSOAP.ID_CLient, 0) > 0
       AND IsNull(patient.IsDeceased, 0) = 0
       AND CONVERT(DATE, soapPlan.DateReturn) BETWEEN @DateStart AND @DateEnd
ORDER  BY c.NAME

/*Patient Wellness Schedule*/
INSERT @table
SELECT DISTINCT c.ID                                                                                                                                                                                                                        ID_Company,
                c.NAME                                                                                                                                                                                                                      Name_Company,
                client.NAME                                                                                                                                                                                                                 Name_Client,
                dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                          ContactNumber_Client,
                wellDetSched.Date,
                IsNull(wellDetSched.Comment, ''),
                IsNull(wellDetSched.Comment, ''),
                dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, IsNull(client.NAME, ''), IsNull(c.ContactNumber, ''), IsNull(patient.NAME, ''), IsNull(wellDetSched.Comment, ''), IsNull(wellDetSched.Comment, ''), wellDetSched.Date) Message,
                CONVERT(DATE, DateAdd(DAY, -1, wellDetSched.Date))                                                                                                                                                                          DateSending,
                wellHed.DateCreated,
                wellDetSched.ID                                                                                                                                                                                                             ID_Patient_Wellness_Schedule,
                @Patient_Wellness_Schedule_ID_Model,
                wellHed.Code,
                patient.Name                                                                                                                                                                                                                Name_Patient
FROM   dbo.vPatient_Wellness_Schedule wellDetSched
       inner JOIN vPatient_Wellness wellHed
               ON wellHed.ID = wellDetSched.ID_Patient_Wellness
       LEFT JOIN dbo.tPatient patient
              ON patient.ID = wellHed.ID_Patient
       LEFT JOIN dbo.tClient client
              ON client.ID = wellHed.ID_Client
       LEFT JOIN tCompany c
              ON c.iD = wellHed.ID_Company
       INNER JOIN @IDs_Company idsCompany
               ON c.ID = idsCompany.ID
WHERE  wellHed.ID_FilingStatus IN ( 1, 3, 13 )
       AND IsNull(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                 FROM   @SMSSent)
       AND IsNull(wellHed.ID_CLient, 0) > 0
       AND IsNull(patient.IsDeceased, 0) = 0
       AND CONVERT(DATE, wellDetSched.Date) BETWEEN @DateStart AND @DateEnd
ORDER  BY c.NAME

/*Vaccination*/
DECLARE @IDs_Vaccination_ExistingPatientWellness TYPINTLIST

INSERT @IDs_Vaccination_ExistingPatientWellness
SELECT hed.ID_Patient_Vaccination
FROM   tPatient_Wellness hed
       INNER JOIN tCompany c
               ON c.iD = hed.ID_Company
WHERE  ID_Patient_Vaccination IS NOT NULL

INSERT @table
SELECT c.ID                                                                                                                                                                   ID_Company,
       c.NAME                                                                                                                                                                 Name_Company,
       client.NAME                                                                                                                                                            Name_Client,
       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
       vacSchedule.Date                                                                                                                                                       DateReturn,
       vac.Name_Item,
       IsNull(vac.Comment, '')                                                                                                                                                Comment,
       dbo.fGetSoaplAnMessage(c.NAME, c.SOAPPlanSMSMessage, client.NAME, IsNull(c.ContactNumber, ''), patient.NAME, vac.Name_Item, IsNull(vac.Comment, ''), vacSchedule.Date) Message,
       CONVERT(DATE, DateAdd(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
       vac.DateCreated,
       vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
       @Patient_Vaccination_Schedule_ID_Model,
       vac.Code, 
       patient.Name                                                                                                                                                           Name_Patient
FROM   dbo.vPatient_Vaccination vac
       LEFT JOIN dbo.tPatient patient
              ON patient.ID = vac.ID_Patient
       LEFT JOIN dbo.tClient client
              ON client.ID = patient.ID_Client
       LEFT JOIN tCompany c
              ON c.iD = vac.ID_Company
       INNER JOIN tPatient_Vaccination_Schedule vacSchedule
               ON vac.ID = vacSchedule.ID_Patient_Vaccination
       INNER JOIN @IDs_Company idsCompany
               ON vac.ID_Company = idsCompany.ID
WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
       AND IsNull(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                FROM   @SMSSent)
       AND IsNull(vac.ID_CLient, 0) > 0
       AND IsNull(patient.IsDeceased, 0) = 0
       AND CONVERT(Date, vacSchedule.Date) BETWEEN @DateStart AND @DateEnd
       AND vac.ID NOT IN (SELECT ID
                          FROM   @IDs_Vaccination_ExistingPatientWellness)

SELECT Name_Company,
		Code,
       CONVERT(Date,DateReturn) DateReturn,
       Name_Client,
       Name_Patient,
       ISNULL(Name_Item, '') Note, DateCreated
FROm   @table
ORDER  BY DateReturn ASC, 
          Name_Client ,  COde
