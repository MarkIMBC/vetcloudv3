exec _pAddModelProperty
  'tItem',
  'tempID',
  1

exec _pRefreshAllViews

IF OBJECT_ID(N'Temp-2022-09-14-Item', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-14-Item];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT 'Temp-2022-09-14-' + company.Guid + '-Item-'
       + Convert(Varchar(MAX), Item.ID)              tempID,
       item.Code,
       item.Name,
       item.IsActive,
       item.ID_ItemType,
       item.ID_ItemCategory,
       item.MinInventoryCount,
       item.MaxInventoryCount,
       item.UnitCost,
       item.UnitPrice,
       item.OtherInfo_DateExpiration,
       item.BarCode,
       item.CustomCode,
       item._tempSupplier,
       CASE
         WHEN LEN(ISNULL(Item.Comment, '')) > 0 THEN CHAR(13)
         ELSE ''
       END
       + 'Imported from Old Server' Comment,
       Item. ID_Company
INTO   [dbo].[Temp-2022-09-14-Item]
FROM   db_waterworksv1_server2.[dbo].tItem Item
       inner join db_waterworksv1_server2.[dbo].tCompany company
               on Item.ID_Company = company.ID
where  Item.ID >= 98272  
       AND company.GUID = '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

SELECT * FROM [dbo].[Temp-2022-09-14-Item]

INSERT @NotYetInserted_TempID
SELECT tempItem.tempID
FROm   [dbo].[Temp-2022-09-14-Item] tempItem

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tItem)

INSERT tItem
       (ID_Company,
        tempID,
        Code,
        Name,
        ID_ItemType,
        ID_ItemCategory,
        MinInventoryCount,
        MaxInventoryCount,
        UnitCost,
        UnitPrice,
        OtherInfo_DateExpiration,
        BarCode,
        CustomCode,
        _tempSupplier,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT tempporyItem.ID_Company,
       tempporyItem.tempID,
       tempporyItem.Code,
       tempporyItem.Name,
       tempporyItem.ID_ItemType,
       tempporyItem.ID_ItemCategory,
       tempporyItem.MinInventoryCount,
       tempporyItem.MaxInventoryCount,
       tempporyItem.UnitCost,
       tempporyItem.UnitPrice,
       tempporyItem.OtherInfo_DateExpiration,
       tempporyItem.BarCode,
       tempporyItem.CustomCode,
       tempporyItem._tempSupplier,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROm   [Temp-2022-09-14-Item] tempporyItem
       inner join @NotYetInserted_TempID temp
               on tempporyItem.tempID = temp.tempID 