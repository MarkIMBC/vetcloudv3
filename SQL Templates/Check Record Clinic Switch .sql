SELECT 'Client and Patient' Purpose,
       *
FROM   tClient client
       inner join tPatient patient
               on client.ID = patient.ID_Client
WHERE  client.ID_Company <> patient.ID_Company

SELECT 'confinement AND patient' Purpose,
       confinement.ID_Company,
       company1.Name,
       confinement.Code,
       patient.ID_Company,
       company2.Name     Name_patient,
       ID_patient
FROM   vPatient_Confinement confinement
       inner join tpatient patient
               on confinement.ID_patient = patient.ID
       inner join tCompany company1
               on company1.ID = confinement.ID_Company
       inner join tCompany company2
               on company2.ID = patient.ID_Company
WHERE  confinement.ID_Company <> patient.ID_Company

SELECT 'SOAP AND Client' Purpose,
       patientSOAP.ID_Company,
       company1.Name,
       patientSOAP.Code,
       client.ID_Company,
       company2.Name     Name_Client,
       ID_Client
FROM   vPatient_SOAP patientSOAP
       inner join tClient client
               on patientSOAP.ID_Client = client.ID
       inner join tCompany company1
               on company1.ID = patientSOAP.ID_Company
       inner join tCompany company2
               on company2.ID = client.ID_Company
WHERE  patientSOAP.ID_Company <> client.ID_Company

SELECT 'SOAP AND patient' Purpose,
       patientSOAP.ID_Company,
       company1.Name,
       patientSOAP.Code,
       patient.ID_Company,
       company2.Name,
       Name_patient,
       ID_patient
FROM   vPatient_SOAP patientSOAP
       inner join tpatient patient
               on patientSOAP.ID_patient = patient.ID
       inner join tCompany company1
               on company1.ID = patientSOAP.ID_Company
       inner join tCompany company2
               on company2.ID = patient.ID_Company
WHERE  patientSOAP.ID_Company <> patient.ID_Company 


SELECT 'Wellness AND Client' Purpose,
       patientSOAP.ID_Company,
       company1.Name,
       patientSOAP.Code,
       client.ID_Company,
       company2.Name     Name_Client,
       ID_Client
FROM   vPatient_Wellness patientSOAP
       inner join tClient client
               on patientSOAP.ID_Client = client.ID
       inner join tCompany company1
               on company1.ID = patientSOAP.ID_Company
       inner join tCompany company2
               on company2.ID = client.ID_Company
WHERE  patientSOAP.ID_Company <> client.ID_Company

SELECT 'Wellness AND Patient' Purpose,
       patientSOAP.Code,
       patientSOAP.ID_Company,
       patient.ID_Company,
       Name_Patient,
       ID_Patient
FROM   vPatient_Wellness patientSOAP
       inner join tPatient patient
               on patientSOAP.ID_Patient = patient.ID
WHERE  patientSOAP.ID_Company <> patient.ID_Company


SELECT 'confinement AND client' Purpose,
       confinement.ID_Company,
       company1.Name,
       confinement.Code,
	   confinement.DateCreated,
       client.ID_Company,
       company2.Name     Name_client,
       ID_client, Name_Client
FROM   vPatient_Confinement confinement
       inner join tclient client
               on confinement.ID_client = client.ID
       inner join tCompany company1
               on company1.ID = confinement.ID_Company
       inner join tCompany company2
               on company2.ID = client.ID_Company
WHERE  confinement.ID_Company <> client.ID_Company


SELECT 'Deposit and Patient Confinement Patient' Purpose,
       clientDeposit.ID,
       clientDeposit.ID_Company,
       clientDeposit.Code,
       clientDeposit.Comment,
       clientDeposit.Name_FilingStatus,
       clientDeposit.ID_Patient,
       patient.ID,
       clientDeposit.ID_Company,
       patient.ID_Company
FROM   vClientDeposit clientDeposit
       INNER JOIN tpatient patient
               on clientDeposit.ID_Patient = patient.ID
WHERE  clientDeposit.ID_Company <> patient.ID_Company 




SELECT patient.ID, Name_Client, patient.Name ,company.Name, patient.Comment FROm vPatient patient 
       inner join tCompany company
               on company.ID = patient.ID_Company
			    where Name_Client  LIKE '%Ocampo%' and patient. Name LIKE '%Sophie%' 
				Order by company.Name, patient.Name

SELECT * FROm tAuditTrail  WHERE ID_CurrentObject = 52569