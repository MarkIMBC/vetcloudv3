IF COL_LENGTH('tBillingInvoice_Patient', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice_Patient',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-03-28-BillingInvoice_Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-BillingInvoice_Patient]

GO

Declare @NotYetInserted_Temp_ID_BillingInvoice_Patient TABLE
  (
     Temp_ID_BillingInvoice_Patient VARCHAR(MAX)
  )

SELECT company.Guid                                       Main_Guid_Company,
       company.ID                                         Main_ID_Company,
       company.Name                                       Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-BillingInvoice-'
       + Convert(Varchar(MAX), BillingInvoice_.ID)        Temp_ID_BillingInvoice,
       'Temp-2022-03-28-' + company.Guid
       + '-BillingInvoice_Patient-'
       + Convert(Varchar(MAX), BillingInvoice_Patient.ID) Temp_ID_BillingInvoice_Patient,
       BillingInvoice_.Code                               Code_BillingInvoice,
       BillingInvoice_Patient.*
INTO   [dbo].[Temp-2022-03-28-BillingInvoice_Patient]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vBillingInvoice BillingInvoice_
       INNER JOIN db_waterworksv1_live_temp_20220328_113414.[dbo].vBillingInvoice_Patient BillingInvoice_Patient
               on BillingInvoice_.ID = BillingInvoice_Patient.ID_BillingInvoice
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on BillingInvoice_.ID_Company = company.ID
where  BillingInvoice_.DateCreated > '2022-03-28 10:00:00'
      -- AND ID_FilingStatus NOT IN ( 1, 4 )

INSERT @NotYetInserted_Temp_ID_BillingInvoice_Patient
SELECT Temp_ID_BillingInvoice_Patient
FROm   [dbo].[Temp-2022-03-28-BillingInvoice_Patient]

DELETE FROM @NotYetInserted_Temp_ID_BillingInvoice_Patient
WHERE  Temp_ID_BillingInvoice_Patient IN (SELECT tempID
                                          FROM   tBillingInvoice_Patient)

SELECT DISTINCT Main_ID_Company,
                Name_Company,
                Code_BillingInvoice
FROM   [Temp-2022-03-28-BillingInvoice_Patient]


SELECT 
tempBillingInvoice_Patient.Main_ID_Company, Code_BillingInvoice,
ISNULL(mainBillingInvoice.ID, mainBillingInvoice2.ID) Main_ID_BillingInvoice,
       mainBillingInvoice.ID,
       mainBillingInvoice2.ID,
       ISNULL(mainPatient.ID, mainPatient2.ID)               Main_ID_Patient,
       tempBillingInvoice_Patient.Temp_ID_BillingInvoice_Patient, Name_Patient
FROm   [dbo].[Temp-2022-03-28-BillingInvoice_Patient] tempBillingInvoice_Patient
       inner join @NotYetInserted_Temp_ID_BillingInvoice_Patient unInsertedTempBillingInvoice_Patient
               on tempBillingInvoice_Patient.Temp_ID_BillingInvoice_Patient = unInsertedTempBillingInvoice_Patient.Temp_ID_BillingInvoice_Patient
       ------------ Billing Invoice ----------------
       LEFT JOIN tBillingInvoice mainBillingInvoice
              on mainBillingInvoice.tempID = 'Temp-2022-03-28-'
                                             + tempBillingInvoice_Patient.Main_Guid_Company
                                             + '-BillingInvoice-'
                                             + Convert(Varchar(MAX), tempBillingInvoice_Patient.ID_BillingInvoice)
                 and mainBillingInvoice.ID_Company = tempBillingInvoice_Patient.Main_ID_Company
       LEFT JOIN tBillingInvoice mainBillingInvoice2
              on mainBillingInvoice2.ID = tempBillingInvoice_Patient.ID_BillingInvoice
                 and mainBillingInvoice2.ID_Company = tempBillingInvoice_Patient.Main_ID_Company
       ------------ Billing Invoice END --------------------------
       /*                                  */
       ------------ Patient ----------------
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-03-28-'
                                      + tempBillingInvoice_Patient.Main_Guid_Company
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), tempBillingInvoice_Patient.ID_Patient)
                 and mainPatient.ID_Company = tempBillingInvoice_Patient.Main_ID_Company
       LEFT JOIN tPatient mainPatient2
              on mainPatient2.ID = tempBillingInvoice_Patient.ID_Patient
                 and mainPatient2.ID_Company = tempBillingInvoice_Patient.Main_ID_Company
------------------- Patient END ----------------
/*                                  */
WHERE  1 = 1

GO

exec _pRefreshAllViews 
