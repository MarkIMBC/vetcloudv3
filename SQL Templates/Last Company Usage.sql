GO

CREATE OR
ALTER PROC pGenerateCompanyUsageByDateCovered(@DateStart DATE,
                                              @DateEND   DATE)
AS
  BEGIN
      DECLARE @Table as TAble
        (
           ID_Company   INT,
           DateModified Datetime
        )

      /*******************************************************************************************************/
      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPatient_SOAP head
             inner join vCompanyActive c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateApproved, head.DateCreated)
      FROm   tPatient_SOAP head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateApproved, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tPatient_SOAP head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCanceled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateDone, head.DateCreated)
      FROm   tPatient_SOAP head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateDone, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPatient head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tClient head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tBillingInvoice head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateApproved, head.DateCreated)
      FROm   tBillingInvoice head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateApproved, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tBillingInvoice head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCanceled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPatient_Confinement head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateDischarge, head.DateCreated)
      FROm   tPatient_Confinement head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateDischarge, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tPatient_Confinement head
             inner join tCompany c
                     on head.ID_Company = c.ID

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPatient_Wellness head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tPatient_Wellness head
             inner join tCompany c
                     on head.ID_Company = c.ID

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPurchaseOrder head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateApproved, head.DateCreated)
      FROm   tPurchaseOrder head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateApproved, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tPurchaseOrder head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCanceled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCancelled, head.DateCreated)
      FROm   tPurchaseOrder head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCancelled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCreated
      FROm   tReceivingReport head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateModified
      FROm   tReceivingReport head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateApproved
      FROm   tReceivingReport head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateApproved, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCanceled
      FROm   tReceivingReport head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCanceled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPaymentTransaction head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateApproved, head.DateCreated)
      FROm   tPaymentTransaction head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateApproved, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateCanceled, head.DateCreated)
      FROm   tPaymentTransaction head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCanceled, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tItem head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tPatientAppointment head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             ISNULL(head.DateModified, head.DateCreated)
      FROm   tSupplier head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT u.ID_Company,
             head.Date
      FROm   tAuditTrail head
             inner join vUser u
                     on u.ID = head.ID_User
             inner join tCompany c
                     on u.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.Date, head.Date)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCreated
      FROm   tInventoryTrail head
             inner join tCompany c
                     on head.ID_Company = c.id
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCreated
      FROm   tClient_CreditLogs head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCreated
      FROm   tPayable head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateModified
      FROm   tPayable head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      INSERT @Table
      SELECT head.ID_Company,
             head.DateCreated
      FROm   tPayablePayment head
             inner join tCompany c
                     on head.ID_Company = c.ID
      WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

      --SELECT c.ID,
      --       c.Name,
      --       c.Guid,
      --       Max(t.DateModified) DateModified
      --FROm   @Table t
      --       INNER JOIN vCompany c
      --               on t.ID_Company = c.ID
      --GROUP  by c.ID,
      --          c.Name,
      --          c.Guid
      --ORDER  BY Max(t.DateModified) DESC
      /*******************************************************************************************************/
      DECLARE @Dates Table
        (
           Date Date
        )

      INSERT @Dates
      SELECT Date
      FROM   dbo.fGetDatesByDateCoverage(@DateStart, @DateEnd)

      IF OBJECT_ID('tempdb..##DateCoverage') IS NOT NULL
        DROP TABLE ##DateCoverage

      IF OBJECT_ID('tempdb..##UsageDates') IS NOT NULL
        DROP TABLE ##UsageDates

      SELECT *
      INTO   ##UsageDates
      FROM   @Table

      SELECT *
      INTO   ##DateCoverage
      FROM   @Dates

      /*Company Usage Count Per Day*/
      DECLARE @CompanyUsageCountPerDay TABLE
        (
           ID_Company      INT,
           Name_Company    Varchar(MAX),
           Date            Date,
           ClinicUsedCount Int
        )
      DECLARE @dateStringsColumns VARCHAR(MAX) = ''
      DECLARE @dateStrings VARCHAR(MAX) = ''
      DECLARE @dateSumStrings VARCHAR(MAX) = ''
      DECLARE @sql VARCHAR(MAX) = ''

      SELECT @dateStringsColumns = @dateStringsColumns + 'ISNULL(['
                                   + FORMAT(Date, 'yyyy-MM-dd') + '], '''') ['
                                   + FORMAT(Date, 'yyyy-MM-dd (ddd)') + '] , '
      FROM   @Dates

      SELECT @dateStrings = @dateStrings + '[' + FORMAT(Date, 'yyyy-MM-dd')
                            + '], '
      FROM   @Dates

      SELECT @dateSumStrings = @dateSumStrings + 'CASE WHEN ['
                               + FORMAT(Date, 'yyyy-MM-dd')
                               + '] = ''Yes'' THEN 1 ELSE 0 END + '
      FROM   @Dates

      SET @dateSumStrings = '' + @dateSumStrings + '0 AS TotalCount'

      PRINT @dateSumStrings

      SET @dateStringsColumns = LEFT(@dateStringsColumns, LEN(@dateStringsColumns) - 1)
      SET @dateStrings = LEFT(@dateStrings, LEN(@dateStrings) - 1)
      SET @sql = '
SELECT  REPLACE(DB_NAME(), ''db_waterworksv1_'', '''') DbName,  Name_Company, '
                 + @dateStringsColumns + ', ' + @dateSumStrings
                 + '
FROM   (SELECT Distinct c.ID                          ID_Company,
                        c.IsActive,
                        c.Name                        Name_Company,
                        COnvert(date, t.DateModified) DateUsage,
                        ''Yes''                         IsUsed
        FROM   ##UsageDates t
               RIGHT join vCompanyActive c
                       on c.ID = t.ID_Company
               LEFT JOIN ##DateCoverage dateDaily
                       on COnvert(date, t.DateModified) = COnvert(date, dateDaily.Date)) AS SourceTable
       PIVOT ( MAX(IsUsed)
             FOR DateUsage IN ( '
                 + @dateStrings + ') ) AS PivotTable
		   ---WHERE Name_Company LIKE ''%Pet Family%''
Order  by IsActive DESC,
          Name_Company '

      PRINT @sql

      exec (@sql)
  END

GO 


--exec dbo.pGenerateCompanyUsageByDateCovered '2024-11-23', '2024-11-26'