exec pUpdateItemCurrentInventory

GO

DECLARE @ID_Company INT = 190

SELECT ID,
       Name,
       CurrentInventoryCount
FROm   tItem
where  ID_Company = @ID_Company
       AND IsActive = 1
       and ID_ItemType = 2

DECLARE @forImport TABLE
  (
     ID_Item   Int,
     Name_Item Varchar(MAX),
     Quantity  int
  )

INSERT @forImport
       (ID_Item,
        Name_Item,
        Quantity)
SELECT ID,
       Name,
       CurrentInventoryCount
FROM   tItem
where  IsActive = 1
       AND ID_Company = @ID_Company
       AND ID_ItemType = 2

Declare @adjustInventory typReceiveInventory
Declare @ID_UserSession int

SELECT @ID_UserSession = MAX(ID)
FROM   tUserSession
WHERE  ID_User = 10

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       ID_Item,
       item.CurrentInventoryCount,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Reset inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       0
FROM   @forImport import
       inner join tItem item
               on import.ID_Item = item.ID
WHERE  item.CurrentInventoryCount > 0
ORDER  BY ID_Item

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

exec pUpdateItemCurrentInventory

SELECT ID,
       Name,
       CurrentInventoryCount
FROm   tItem
where  ID_Company = @ID_Company
       AND IsActive = 1
       and ID_ItemType = 2 
