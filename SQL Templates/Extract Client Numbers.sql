DECLARE @GUID_Company VARCHAR(MAX) = 'F13C585D-2D21-401F-8B58-BCDAF68DE516'
DECLARE @SourceID_Client INT =303537
DECLARE @DestinationID_Client INT =212873

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT company.Name,
       client.Name,
       ISNULL(client.ContactNumber, '')  ContactNumber,
       ISNULL(client.ContactNumber2, '') ContactNumber2
FROm   tClient client
       inner join tCompany company
               on client.ID_Company = company.ID
WHERE  client.ID_Company = @ID_Company
       and client.IsActive = 1 
Order by client.Name