GO

if (SELECT COUNT(*)
    FROM   _tModel
    where  TableName = 'tSMSList') = 0
  begin
      DROP VIEW vActiveSMSList

      DROP VIEW vInactiveSMSList

      DROP VIEW vSMSList

      DROP TABLE tSMSList

      DROP PROC pGetSMSList
  END

GO

if OBJECT_ID('dbo.tSMSList') is null
  begin
      exec _pCreateAppModuleWithTable
        'tSMSList',
        1,
        NULL,
        NULL
  END

GO

DECLARE @Administrator_ID_UserRole INT = 0
DECLARE @AllAdmission_ID_UserRole INT = 0
DECLARE @SystemFolder_ID_Navigation VARCHAR(MAX) = ''
DECLARE @Oid_Model UniqueIdentifier
DECLARE @Table_Model VARCHAR(MAX) = 'tSMSList'

SELECT @Oid_Model = Oid
FROM   _tModel
where  TableName = @Table_Model

SELECT @Oid_Model

SELECT @Administrator_ID_UserRole = ID
FROM   tUserRole
WHERE  Name = 'Administrator'

SELECT @AllAdmission_ID_UserRole = ID
FROM   tUserRole
WHERE  Name = 'All Admission'

SELECT @SystemFolder_ID_Navigation = Oid
FROM   _tNavigation
WHERE  Name = 'SystemFolder'

Update _tNavigation
set    Caption = 'SMS',
       Route = 'SMSList',
       IsActive = 1,
       ID_Parent = @SystemFolder_ID_Navigation
where  Name = 'SMSList_ListView'

SELECT _userrole.Name
FROM   tUserRole_Detail _userRoleDetail
       INNER JOIN tUserRole _userrole
               on _userrole.ID = _userRoleDetail.ID_UserRole
       INNER JOIN _tModel _model
               on CONVERT(VARCHAR(MAX), _userRoleDetail.ID_Model) = CONVERT(VARCHAR(MAX), _model.Oid)
WHERE  _model.TableName = @Table_Model

IF(SELECT COUNT(*)
   FROM   tUserRole_Detail _userRoleDetail
          INNER JOIN _tModel _model
                  on CONVERT(VARCHAR(MAX), _userRoleDetail.ID_Model) = CONVERT(VARCHAR(MAX), _model.Oid)
   WHERE  _model.TableName = @Table_Model) = 0
  begin
      SELECT @Administrator_ID_UserRole,
             @Table_Model

      INSERT INTO [dbo].[tUserRole_Detail]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_UserRole],
                   [ID_Model],
                   [IsView],
                   [IsCreate],
                   [IsEdit],
                   [IsDelete],
                   [IsDeny],
                   [SeqNo])
      VALUES      (NULL,
                   '',
                   1,
                   @Administrator_ID_UserRole,
                   @Oid_Model,
                   1,
                   1,
                   1,
                   1,
                   0,
                   0 )

      INSERT INTO [dbo].[tUserRole_Detail]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_UserRole],
                   [ID_Model],
                   [IsView],
                   [IsCreate],
                   [IsEdit],
                   [IsDelete],
                   [IsDeny],
                   [SeqNo])
      VALUES      (NULL,
                   '',
                   1,
                   @AllAdmission_ID_UserRole,
                   @Oid_Model,
                   1,
                   1,
                   1,
                   1,
                   0,
                   0 )
  END

Update tUserRole_Detail
SET    IsDeny = 0
FROM   tUserRole_Detail _userRoleDetail
       LEFT JOIN _tModel _model
              on CONVERT(VARCHAR(MAX), _userRoleDetail.ID_Model) = CONVERT(VARCHAR(MAX), _model.Oid)
WHERE  _model.TableName = @Table_Model

DELETE FROM tUserRole_Detail
where  ID IN (SELECT _userRoleDetail.ID
              FROM   tUserRole_Detail _userRoleDetail
                     LEFT JOIN _tModel _model
                            on CONVERT(VARCHAR(MAX), _userRoleDetail.ID_Model) = CONVERT(VARCHAR(MAX), _model.Oid)
              WHERE  _model.TableName IS NULL)

GO 
