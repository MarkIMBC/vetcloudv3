IF COL_LENGTH('tPurchaseOrder_Detail', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPurchaseOrder_Detail',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-03-28-PurchaseOrder_Detail', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-PurchaseOrder_Detail]

GO

Declare @NotYetInserted_Temp_ID_PurchaseOrder_Detail TABLE
  (
     Temp_ID_PurchaseOrder_Detail VARCHAR(MAX)
  )

SELECT company.Guid                                     Main_Guid_Company,
       company.ID                                       Main_ID_Company,
       company.Name                                     Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-PurchaseOrder-'
       + Convert(Varchar(MAX), PurchaseOrder_.ID)       Temp_ID_PurchaseOrder,
       'Temp-2022-03-28-' + company.Guid
       + '-PurchaseOrder_Detail-'
       + Convert(Varchar(MAX), PurchaseOrder_Detail.ID) Temp_ID_PurchaseOrder_Detail,
       PurchaseOrder_Detail.*
INTO   [dbo].[Temp-2022-03-28-PurchaseOrder_Detail]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vPurchaseOrder PurchaseOrder_
       INNER JOIN db_waterworksv1_live_temp_20220328_113414.[dbo].tPurchaseOrder_Detail PurchaseOrder_Detail
               on PurchaseOrder_.ID = PurchaseOrder_Detail.ID_PurchaseOrder
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on PurchaseOrder_.ID_Company = company.ID
where  PurchaseOrder_.DateCreated > '2022-03-28 10:00:00'

INSERT @NotYetInserted_Temp_ID_PurchaseOrder_Detail
SELECT tempPurchaseOrder.Temp_ID_PurchaseOrder_Detail
FROm   [dbo].[Temp-2022-03-28-PurchaseOrder_Detail] tempPurchaseOrder

DELETE FROM @NotYetInserted_Temp_ID_PurchaseOrder_Detail
WHERE  Temp_ID_PurchaseOrder_Detail IN (SELECT tempID
                                        FROM   tPurchaseOrder_Detail)

INSERT tPurchaseOrder_Detail
       (ID_PurchaseOrder,
        ID_Item,
        Quantity,
        VATAmount,
        GrossAmount,
        NetAmount,
        ID_UOM,
        DiscountAmount,
        UnitCost,
        Balance,
        UnitPrice,
        Amount,
        RemainingQuantity,
        tempID)
SELECT ISNULL(mainPurchaseOrder.ID, mainPurchaseOrder2.ID) Main_ID_PurchaseOrder,
       ISNULL(mainItem.ID, mainItem2.ID)                   Main_ID_Item,
       tempPurchaseOrder_Detail.Quantity,
       tempPurchaseOrder_Detail.VATAmount,
       tempPurchaseOrder_Detail.GrossAmount,
       tempPurchaseOrder_Detail.NetAmount,
       tempPurchaseOrder_Detail.ID_UOM,
       tempPurchaseOrder_Detail.DiscountAmount,
       tempPurchaseOrder_Detail.UnitCost,
       tempPurchaseOrder_Detail.Balance,
       tempPurchaseOrder_Detail.UnitPrice,
       tempPurchaseOrder_Detail.Amount,
       tempPurchaseOrder_Detail.RemainingQuantity,
       tempPurchaseOrder_Detail.Temp_ID_PurchaseOrder_Detail
FROm   [dbo].[Temp-2022-03-28-PurchaseOrder_Detail] tempPurchaseOrder_Detail
       inner join @NotYetInserted_Temp_ID_PurchaseOrder_Detail unInsertedTempPurchaseOrder_Detail
               on tempPurchaseOrder_Detail.Temp_ID_PurchaseOrder_Detail = unInsertedTempPurchaseOrder_Detail.Temp_ID_PurchaseOrder_Detail
       ------------ Purchase Order ----------------
       LEFT JOIN tPurchaseOrder mainPurchaseOrder
              on mainPurchaseOrder.tempID = 'Temp-2022-03-28-'
                                            + tempPurchaseOrder_Detail.Main_Guid_Company
                                            + '-PurchaseOrder-'
                                            + Convert(Varchar(MAX), tempPurchaseOrder_Detail.ID_PurchaseOrder)
                 and mainPurchaseOrder.ID_Company = tempPurchaseOrder_Detail.Main_ID_Company
       LEFT JOIN tPurchaseOrder mainPurchaseOrder2
              on mainPurchaseOrder2.ID = tempPurchaseOrder_Detail.ID_PurchaseOrder
                 and mainPurchaseOrder2.ID_Company = tempPurchaseOrder_Detail.Main_ID_Company
       ------------ Purchase Order END --------------------------
       /*                                  */
       ------------ Item ----------------
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-03-28-'
                                   + tempPurchaseOrder_Detail.Main_Guid_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempPurchaseOrder_Detail.ID_Item)
                 and mainItem.ID_Company = tempPurchaseOrder_Detail.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempPurchaseOrder_Detail.ID_Item
                 and mainItem2.ID_Company = tempPurchaseOrder_Detail.Main_ID_Company
------------ Item END ----------------
/*                                  */
WHERE  1 = 1

GO

EXEC _pRefreshAllViews 
