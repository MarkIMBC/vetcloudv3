GO

CREATE OR
ALTER FUNCTION _temp_fGetPractoClientName(@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      DECLARE @string VARCHAR(MAX) = ''
      DECLARE @charIndex INT = 0

      SET @string = @Value
      SET @charIndex = CHARINDEX('-', @string)
      SET @string = LTRIM(RTRIM(LEFT(@string, @charIndex)))

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('[', @Value)
            SET @string = LTRIM(RTRIM(LEFT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('=', @Value)
            SET @string = LTRIM(RTRIM(LEFT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('(', @Value)
            SET @string = LTRIM(RTRIM(LEFT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX('{', @Value)
            SET @string = LTRIM(RTRIM(LEFT(@Value, @charIndex)))
        END

      if( LEN(@string) = 0 )
        BEGIN
            SET @charIndex = CHARINDEX(']', @Value)
            SET @string = LTRIM(RTRIM(LEFT(@Value, @charIndex)))
        END

      SET @string = REPLACE(@string, '-', '')
      SET @string = REPLACE(@string, '[', '')
      SET @string = REPLACE(@string, '=', '')
      SET @string = REPLACE(@string, '(', '')
      SET @string = REPLACE(@string, '{', '')
      SET @string = REPLACE(@string, ']', '')

      RETURN @string
  END

GO

/*
	DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154
	PetValleySorsogon-Patients
	PetValleySorsogon-ClinicalNotes
*/
UPDATE dbo.vetsaveers_clientpatient_practo
SET    [Patient Number] = '''Temp' + LEFT(newID(), 4) + ''''
WHERE  ISNULL([Patient Number], '') = ''

DECLARE @forImportPatient TABLE
  (
     CustomCode_Patient VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     ContactNumber      VARCHAR(MAX),
     ContactNumber2     VARCHAR(MAX),
     EmailAddress       VARCHAR(MAX),
     ID_Gender          INT,
     Address_Client     VARCHAR(MAX),
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )

INSERT @forImportPatient
SELECT [Patient Number],
       REPLACE(REPLACE(REPLACE(REPLACE(dbo.fGetRemovedBegEndSQuatationString([Patient Name]), '- ', ' - '), ' -', ' - '), '  ', ' '), '  ', ' '),
       [Mobile Number],
       [Contact Number]
       + CASE
           WHEN LEN([Secondary Mobile]) > 0 THEN ' ' + [Secondary Mobile]
           ELSE ''
         END,
       [Email Address],
       CASE
         WHEN [Gender] = 'M' THEN 1
         ELSE
           CASE
             WHEN [Gender] = 'F' THEN 2
             ELSE NULL
           END
       END,
       [Address]
       + CASE
           WHEN LEN([Locality]) > 0 THEN ' '
           ELSE ''
         END
       + [Locality]
       + CASE
           WHEN LEN([City]) > 0 THEN ' '
           ELSE ''
         END
       + [City],
       CASE
         WHEN LEN([Date of Birth]) > 0 THEN TRY_CONVERT(DateTime, [Date of Birth])
         ELSE NULL
       END,
       [Groups],
       + CASE
           WHEN LEN([Remarks]) > 0 THEN 'Remarks' + CHAR(13) + CHAR(10) + ' ' + [Remarks]
           ELSE ''
         END
       +
       + CASE
           WHEN LEN([Medical History]) > 0 THEN CHAR(13) + CHAR(10) + 'Medical History'
                                                + CHAR(13) + CHAR(10) + ' ' + [Medical History]
           ELSE ''
         END
       + CASE
           WHEN LEN([Patient Notes]) > 0 THEN CHAR(13) + CHAR(10) + 'Patient Notes' + CHAR(13)
                                              + CHAR(10) + ' ' + [Patient Notes]
           ELSE ''
         END
       + CHAR(13) + CHAR(10) + 'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   (SELECT dbo.fGetRemovedBegEndSQuatationString([Patient Number])                                        [Patient Number],
               REPLACE(REPLACE(dbo.fGetRemovedBegEndSQuatationString([Patient Name]), '-', ' - '), '  ', ' ') [Patient Name],
               dbo.fGetRemovedBegEndSQuatationString([Mobile Number])                                         [Mobile Number],
               dbo.fGetRemovedBegEndSQuatationString([Contact Number])                                        [Contact Number],
               dbo.fGetRemovedBegEndSQuatationString([Email Address])                                         [Email Address],
               dbo.fGetRemovedBegEndSQuatationString([Secondary Mobile])                                      [Secondary Mobile],
               dbo.fGetRemovedBegEndSQuatationString([Gender])                                                [Gender],
               dbo.fGetRemovedBegEndSQuatationString([Address])                                               [Address],
               dbo.fGetRemovedBegEndSQuatationString([Locality])                                              [Locality],
               dbo.fGetRemovedBegEndSQuatationString([City])                                                  [City],
               dbo.fGetRemovedBegEndSQuatationString([National Id])                                           [National Id],
               dbo.fGetRemovedBegEndSQuatationString([Date of Birth])                                         [Date of Birth],
               dbo.fGetRemovedBegEndSQuatationString([Remarks])                                               [Remarks],
               dbo.fGetRemovedBegEndSQuatationString([Medical History])                                       [Medical History],
               dbo.fGetRemovedBegEndSQuatationString([Groups])                                                [Groups],
               dbo.fGetRemovedBegEndSQuatationString([Patient Notes])                                         [Patient Notes]
        FROM   [dbo].vetsaveers_clientpatient_practo) tbl

DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154

INSERT tPatient
       (ID_Company,
        ID_Client,
        CustomCode,
        Name,
        FirstName,
        Comment,
        Species,
        ID_Gender,
        DateBirth,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy)
SELECT @ID_Company_VetSaverSanIsidroAntipoloBranch,
       NULL,
       CustomCode_Patient,
       Name_Patient,
       dbo._temp_fGetPractoClientName(Name_Patient),
       Comment,
       Species,
       ID_Gender,
       DateBirth,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @forImportPatient

SELECT *
FROM   tPatient
where  ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
Order  By Name
/*


DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154

DELETE
FROM   tPatient
where  ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch  

*/
