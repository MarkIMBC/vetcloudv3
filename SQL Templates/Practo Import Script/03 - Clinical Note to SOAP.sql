/*
	@ID_Company_VetSaverSanIsidroAntipoloBranch 
	PetValleySorsogon-Patients
	PetValleySorsogon-ClinicalNotes
*/
DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154

UPDATE dbo.[PetValleySorsogon-Patients]
SET    [Patient Number] = '''Temp' + LEFT(newID(), 4) + ''''
WHERE  ISNULL([Patient Number], '') = ''

DECLARE @forImportSOAPRecord TABLE
  (
     Date                    DATETIME,
     CustomCode_Patient      VARCHAR(MAX),
     Name_Patient            VARCHAR(MAX),
     Name_AttendingPhysician VARCHAR(MAX),
     Comment                 VARCHAR(MAX),
     History                 VARCHAR(MAX),
     ClinicalExamination     VARCHAR(MAX),
     Diagnosis               VARCHAR(MAX),
     Treatment               VARCHAR(MAX)
  )

INSERT @forImportSOAPRecord
SELECT CONVERT(DateTIME, Date) Date,
       PatientNumber,
       PatientName,
       Doctor,
       CASE
         WHEN LEN(ISNULL(Revised, '')) > 0 THEN 'Revised' + CHAR(13) + CHAR(10) + Revised
         ELSE ''
       END,
       [complaints],
       CASE
         WHEN LEN(ISNULL([observations], '')) > 0 THEN 'Observations' + CHAR(13) + CHAR(10)
                                                       + [observations]
         ELSE ''
       END
       + CASE
           WHEN LEN(ISNULL([investigations], '')) > 0 THEN 'Investigations' + CHAR(13) + CHAR(10)
                                                           + [investigations]
           ELSE ''
         END,
       [diagnoses],
       [treatmentnotes]
FROM   (select dbo.fGetRemovedBegEndSQuatationString([Date])           Date,
               dbo.fGetRemovedBegEndSQuatationString([Patient Number]) PatientNumber,
               dbo.fGetRemovedBegEndSQuatationString([Patient Name])   PatientName,
               dbo.fGetRemovedBegEndSQuatationString([Doctor])         Doctor,
               dbo.fGetRemovedBegEndSQuatationString([Description])    Description,
               dbo.fGetRemovedBegEndSQuatationString([Revised])        Revised,
               dbo.fGetRemovedBegEndSQuatationString([Type])           Type
        FROm   dbo.[PetValleySorsogon-ClinicalNotes]) _clinicalNotes
       PIVOT ( MAX( _clinicalNotes.[Description] )
             FOR [Type] IN ( [treatmentnotes],
                             [complaints],
                             [investigations],
                             [observations],
                             [diagnoses] ) ) AS PivotTable
Order  by Date,
          PatientNumber,
          PatientName

INSERT INTO [dbo].[tEmployee]
            ([IsActive],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [DateCreated],
             [DateModified],
             [ID_Position],
             [ID_EmployeeStatus],
             [FirstName],
             [Name],
             [ID_Company])
SELECT DISTINCT 1,
                1,
                1,
                GetDAte(),
                GetDate(),
                10,
                1,
                Name_AttendingPhysician,
                Name_AttendingPhysician,
                @ID_Company_VetSaverSanIsidroAntipoloBranch 
FROM   @forImportSOAPRecord
INSERT INTO [dbo].[tPatient_SOAP]
            ([ID_Company],
             [Date],
             [Name],
             [ID_Patient],
             [AttendingPhysician_ID_Employee],
             [ID_SOAPType],
             [ID_FilingStatus],
             [History],
             [ClinicalExamination],
             [Diagnosis],
             [Treatment],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [Comment])
SELECT @ID_Company_VetSaverSanIsidroAntipoloBranch,
       import.Date,
       import.Name_Patient,
       patient.ID,
       employee.iD,
       7,
       1,
       History,
       ClinicalExamination,
       Diagnosis,
       Treatment,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported from Practo Export Files'
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @forImportSOAPRecord import
       inner JOIN tPatient patient
               on LTRIM(RTRIM(import.CustomCode_Patient)) = patient.CustomCode
       LEFT JOIN tEmployee employee
              on employee.Name = import.Name_AttendingPhysician
WHERE  patient.ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
       and patient.IsActive = 1
       AND employee.ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch

/* Not Match on Patient CUstom COde */
SELECT *
FROM   @forImportSOAPRecord import
WHERE  LEN(CustomCode_Patient) = 0 
