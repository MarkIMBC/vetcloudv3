DECLARE @IDs_CurrentObject typIntList

INSERT @IDs_CurrentObject
SELECT TOP 10 ID
FROM   tBillingInvoice hed WITH (NOLOCK)
WHERE  ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
ORDER  BY ID DESC

SELECT hed.ID,
       hed.Code,
       Date,
       DateRunAfterSavedProcess,
       RunAfterSavedProcess_ID_FilingStatus
FROM   tBillingInvoice hed WITH (NOLOCK)
       INNER JOIN @IDs_CurrentObject ids
               on hed.ID = ids.ID

exec pReRun_AfterSaved_Process_BillingInvoice_By_IDs
  @IDs_CurrentObject

GO

DECLARE @IDs_CurrentObject typIntList

INSERT @IDs_CurrentObject
SELECT TOP 10 ID
FROM   tPatient_SOAP hed WITH (NOLOCK)
WHERE  ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
ORDER  BY ID DESC

SELECT hed.ID,
       hed.Code,
       Date,
       DateRunAfterSavedProcess,
       RunAfterSavedProcess_ID_FilingStatus
FROM   tPatient_SOAP hed WITH (NOLOCK)
       INNER JOIN @IDs_CurrentObject ids
               on hed.ID = ids.ID

exec pReRun_AfterSaved_Process_Patient_SOAP_By_IDs
  @IDs_CurrentObject

GO

exec pUpdateItemCurrentInventory 
