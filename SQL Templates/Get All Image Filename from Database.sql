GO

CREATE OR
ALTER PROC pGetCompanyImageFileNames(@IDsCompanyString varchar(MAX))
AS
  BEGIN
      DECLARE @IDS_Company typIntList
      DECLARE @IsSuccess BIT = 1
	  DECLARE @IsFolderized BIT = 1

      if( LEN(@IDsCompanyString) > 0 )
        BEGIN
            INSERT @IDS_Company
            SELECT Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDS_Company
            SELECT ID
            FROM   tCompany
            WHERE  IsActive = 1
        END

      DECLARE @ImagePaths TABLE
        (
           ID_Company    VARCHAR(MAX),
           ImageFilePath VARCHAR(MAX)
        )

      /* Company */
      INSERT @ImagePaths
      SELECT company.ID,
             ImageFile
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )
      UNIOn ALL
      SELECT company.ID,
             ImageHeaderFilename
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageHeaderFilename, '') <> '' )
      UNIOn ALL
      SELECT company.ID,
             ImageLogoFilename
      FROm   tCompany company
             CROSS APPLY(SELECT ImageHeaderFilename
                         UNION ALL
                         SELECT ImageLogoFilename) img (ImageFile)
             inner join @IDS_Company ids
                     on company.ID = ids.ID
      WHERE  ( ISNULL(ImageLogoFilename, '') <> '' )

      /*Employee*/
      INSERT @ImagePaths
      SELECT ID_Company,
             ImageFile
      FROm   tEmployee emp
             inner join @IDS_Company ids
                     on emp.ID_Company = ids.ID
      WHERE  ( ISNULL(ImageFile, '') <> '' )

      /*Patient*/
      INSERT @ImagePaths
      SELECT ID_Company,
             ProfileImageFile
      FROm   tPatient patient
             inner join @IDS_Company ids
                     on patient.ID_Company = ids.ID
      WHERE  ( ISNULL(ProfileImageFile, '') <> '' )

      /* SOAP */
      INSERT @ImagePaths
      SELECT ID_Company,
             FilePath
      FROM   tPatient_SOAP ps
             CROSS APPLY (SELECT '01',
                                 LabImageRowIndex01,
                                 LabImageFilePath01,
                                 LabImageRemark01
                          UNION ALL
                          SELECT '02',
                                 LabImageRowIndex02,
                                 LabImageFilePath02,
                                 LabImageRemark02
                          UNION ALL
                          SELECT '03',
                                 LabImageRowIndex03,
                                 LabImageFilePath03,
                                 LabImageRemark03
                          UNION ALL
                          SELECT '04',
                                 LabImageRowIndex04,
                                 LabImageFilePath04,
                                 LabImageRemark04
                          UNION ALL
                          SELECT '05',
                                 LabImageRowIndex05,
                                 LabImageFilePath05,
                                 LabImageRemark05
                          UNION ALL
                          SELECT '06',
                                 LabImageRowIndex06,
                                 LabImageFilePath06,
                                 LabImageRemark06
                          UNION ALL
                          SELECT '07',
                                 LabImageRowIndex07,
                                 LabImageFilePath07,
                                 LabImageRemark07
                          UNION ALL
                          SELECT '08',
                                 LabImageRowIndex08,
                                 LabImageFilePath08,
                                 LabImageRemark08
                          UNION ALL
                          SELECT '09',
                                 LabImageRowIndex09,
                                 LabImageFilePath09,
                                 LabImageRemark09
                          UNION ALL
                          SELECT '10',
                                 LabImageRowIndex10,
                                 LabImageFilePath10,
                                 LabImageRemark10
                          UNION ALL
                          SELECT '11',
                                 LabImageRowIndex11,
                                 LabImageFilePath11,
                                 LabImageRemark11
                          UNION ALL
                          SELECT '12',
                                 LabImageRowIndex12,
                                 LabImageFilePath12,
                                 LabImageRemark12
                          UNION ALL
                          SELECT '13',
                                 LabImageRowIndex13,
                                 LabImageFilePath13,
                                 LabImageRemark13
                          UNION ALL
                          SELECT '14',
                                 LabImageRowIndex14,
                                 LabImageFilePath14,
                                 LabImageRemark14
                          UNION ALL
                          SELECT '15',
                                 LabImageRowIndex15,
                                 LabImageFilePath15,
                                 LabImageRemark15) c (ImageNo, ImageRowIndex, FilePath, Remark)
             INNER JOIN @IDS_Company idsCompany
                     on idsCompany.ID = ps.ID_Company
      WHERE  ( ISNULL(FilePath, '') <> '' )

      SELECT '_',
             ''                   AS Company,
             'Company.ID_Company' AS ImagePaths

      select @IsSuccess IsSuccess, @IsFolderized IsFolderized

      SELECT DISTINCT CONVERT(INT, imgPath.ID_Company) ID,
                      c.Name                           Name,
                      COUNT(*)                         TotalImagePath
      FROm   @ImagePaths imgPath
             iNNER JOIN tCompany c
                     on imgPath.ID_Company = c.ID
      GROUP  BY CONVERT(INT, imgPath.ID_Company),
                c.Name
      ORDER  BY c.Name

      SELECT CONVERT(INT, imgPath.ID_Company) ID_Company,
             c.Name                           Name_Company,
             imgPath.ImageFilePath            Filename
      FROm   @ImagePaths imgPath
             iNNER JOIN tCompany c
                     on imgPath.ID_Company = c.ID
      ORDER  BY c.Name,
                imgPath.ImageFilePath
  END

GO 
