GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO

SELECT *
Into   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice
FROm   (SELECT DISTINCt company.Name,
                        ID_Patient_Confinement,
                        conf.Date
        FROM   vPatient_Confinement_ItemsServices confItem
               Inner join tPatient_Confinement conf
                       on conf.ID = ID_Patient_Confinement
               Inner join vCompanyActive company
                       on company.ID = conf.ID_Company
               INNER JOIN tItem item
                       on confItem.ID_Item = item.ID
        WHERE  ISNULL(confItem.UnitPrice, 0) = 0
               and item.UnitPrice > 0
               and ( confItem.ID_Patient_SOAP_Prescription IS NOT NULL
                      OR confItem.ID_Patient_SOAP_Treatment IS NOT NULL )
               and ID_FilingStatus = 14
        UNION ALL
        SELECT company.Name,
               ID_Patient_Confinement,
               billing.Date
        FROM   tBillingInvoice billing
               Inner join vBillingInvoice_Detail billingItem
                       on billing.ID = billingItem.ID_BillingInvoice
               Inner join vCompanyActive company
                       on company.ID = billing.ID_Company
               INNER JOIN tItem item
                       on billingItem.ID_Item = item.ID
        WHERE  ISNULL(billingItem.UnitPrice, 0) = 0
               and item.UnitPrice > 0
               AND ID_Patient_Confinement_ItemsServices IS NOT NULL
               AND ID_FilingStatus = 1) tbl
WHERE  YEAR(Date) = 2022
ORDER  BY Name, Date DESC

GO

SELECT *
FROM   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO

Create  OR
ALTER PROC ____pReUpdateCOnfinementRecordIUNitPrice (@ID_Patient_Confinement INT)
as
    DECLARE @IDs_Patient_Confinement_ItemsServices typIntList
    DECLARE @IDs_Patient_SOAP_Treatment typIntList
    DECLARE @IDs_Patient_SOAP_Prescription typIntList

    INSERT @IDs_Patient_Confinement_ItemsServices
    SELECT ID
    FROm   vPatient_Confinement_ItemsServices
    WHERE  ID_Patient_Confinement = @ID_Patient_Confinement

    INSERT @IDs_Patient_SOAP_Treatment
    SELECT ID_Patient_SOAP_Treatment
    FROm   vPatient_Confinement_ItemsServices
    WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
           AND ID_Patient_SOAP_Treatment IS NOT NULL

    INSERT @IDs_Patient_SOAP_Prescription
    SELECT ID_Patient_SOAP_Prescription
    FROm   vPatient_Confinement_ItemsServices
    WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
           AND ID_Patient_SOAP_Prescription IS NOT NULL

    Update tPatient_SOAP_Prescription
    SET    UnitPrice = item.UnitPrice
    FROM   tPatient_SOAP_Prescription soapTreatment
           INNER JOIN @IDs_Patient_SOAP_Prescription ids
                   on soapTreatment.ID = ids.ID
           INNER JOIN tItem item
                   on soapTreatment.ID_Item = item.ID
    WHERE  ISNULL(soapTreatment.UnitPrice, 0) = 0

    Update tPatient_SOAP_Prescription
    SET    UnitCost = item.UnitCost
    FROM   tPatient_SOAP_Prescription soapPrescription
           INNER JOIN @IDs_Patient_SOAP_Prescription ids
                   on soapPrescription.ID = ids.ID
           INNER JOIN tItem item
                   on soapPrescription.ID_Item = item.ID
    WHERE  ISNULL(soapPrescription.UnitCost, 0) = 0

    Update tPatient_SOAP_Treatment
    SET    UnitPrice = item.UnitPrice
    FROM   tPatient_SOAP_Treatment soapTreatment
           INNER JOIN @IDs_Patient_SOAP_Treatment ids
                   on soapTreatment.ID = ids.ID
           INNER JOIN tItem item
                   on soapTreatment.ID_Item = item.ID
    WHERE  ISNULL(soapTreatment.UnitPrice, 0) = 0

    Update tPatient_SOAP_Treatment
    SET    UnitCost = item.UnitCost
    FROM   tPatient_SOAP_Treatment soapTreatment
           INNER JOIN @IDs_Patient_SOAP_Treatment ids
                   on soapTreatment.ID = ids.ID
           INNER JOIN tItem item
                   on soapTreatment.ID_Item = item.ID
    WHERE  ISNULL(soapTreatment.UnitCost, 0) = 0

    Update vPatient_Confinement_ItemsServices
    SET    UnitPrice = item.UnitPrice
    FROM   vPatient_Confinement_ItemsServices confItem
           INNER JOIN tItem item
                   on confItem.ID_Item = item.ID
    WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
           and ISNULL(confItem.UnitPrice, 0) = 0

    Update vPatient_Confinement_ItemsServices
    SET    UnitCost = item.UnitCost
    FROM   vPatient_Confinement_ItemsServices confItem
           INNER JOIN tItem item
                   on confItem.ID_Item = item.ID
    WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
           and ISNULL(confItem.UnitCost, 0) = 0

    Update tBillingInvoice_Detail
    SET    UnitPrice = item.UnitPrice
    FROM   tBillingInvoice_Detail biDetail
           inner join tBillingInvoice billing
                   on biDetail.ID_BillingInvoice = billing.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemID
                   on confItemID.ID = biDetail.ID_Patient_Confinement_ItemsServices
           INNER JOIN tItem item
                   on item.ID = biDetail.ID_Item
                      ANd ID_FilingStatus = 1
                      and ISNULL(biDetail.UnitPrice, 0) = 0

    Update tBillingInvoice_Detail
    SET    UnitCost = item.UnitCost
    FROM   tBillingInvoice_Detail biDetail
           inner join tBillingInvoice billing
                   on biDetail.ID_BillingInvoice = billing.ID
           INNER JOIN @IDs_Patient_Confinement_ItemsServices confItemID
                   on confItemID.ID = biDetail.ID_Patient_Confinement_ItemsServices
           INNER JOIN tItem item
                   on item.ID = biDetail.ID_Item
                      ANd ID_FilingStatus = 1
                      and ISNULL(biDetail.UnitCost, 0) = 0

GO

DECLARE @ID_Patient_Confinement INT
DECLARE db_cursor____pReUpdateCOnfinementRecordIUNitPrice CURSOR FOR
  SELECT Distinct ID_Patient_Confinement
  FROM   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice
  WHERE  name NOT IN ( 'master', 'model', 'msdb', 'tempdb' )

OPEN db_cursor____pReUpdateCOnfinementRecordIUNitPrice

FETCH NEXT FROM db_cursor____pReUpdateCOnfinementRecordIUNitPrice INTO @ID_Patient_Confinement

WHILE @@FETCH_STATUS = 0
  BEGIN
      exec ____pReUpdateCOnfinementRecordIUNitPrice
        @ID_Patient_Confinement

      FETCH NEXT FROM db_cursor____pReUpdateCOnfinementRecordIUNitPrice INTO @ID_Patient_Confinement
  END

CLOSE db_cursor____pReUpdateCOnfinementRecordIUNitPrice

DEALLOCATE db_cursor____pReUpdateCOnfinementRecordIUNitPrice

GO

DROP PROC ____pReUpdateCOnfinementRecordIUNitPrice

GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO 
