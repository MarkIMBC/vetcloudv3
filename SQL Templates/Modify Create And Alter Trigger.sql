DECLARE @ErrorMessage TABLE
  (
     [Procedure] VARCHAR(MAX),
     [Message]   VARCHAR(MAX)
  )
DECLARE @tableName VARCHAR(50) -- database name 
DECLARE db_cursor CURSOR FOR
  SELECT TableName
  FROM   _tModel

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @tableName

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @sqlCreated NVARCHAR(MAX) = N'
		CREATE OR ALTER TRIGGER [dbo].[rDateCreated_/*TableName*/]
		ON [dbo].[/*TableName*/]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo./*TableName*/
			SET    DateCreated = GETDATE()
			FROM   dbo./*TableName*/ hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;'
      DECLARE @sqlModified NVARCHAR(MAX) = N'
		CREATE OR ALTER TRIGGER [dbo].[rDateModified_/*TableName*/]
		ON [dbo].[/*TableName*/]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo./*TableName*/
			SET    DateModified = GETDATE()
			FROM   dbo./*TableName*/ hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;'

      SET @sqlCreated= REPLACE(@sqlCreated, '/*TableName*/', @tableName)
      SET @sqlModified= REPLACE(@sqlModified, '/*TableName*/', @tableName)

      BEGIN TRY
          exec (@sqlCreated)
      END TRY
      BEGIN CATCH
          INSERT @ErrorMessage
          VALUES(ERROR_PROCEDURE(),
                 ERROR_MESSAGE())
      END CATCH

      BEGIN TRY
          exec (@sqlModified)
      END TRY
      BEGIN CATCH
          INSERT @ErrorMessage
          VALUES(ERROR_PROCEDURE(),
                 ERROR_MESSAGE())
      END CATCH

      FETCH NEXT FROM db_cursor INTO @tableName
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT *
FROM   @ErrorMessage 
