IF OBJECT_ID(N'AuditTrail-2022-03-28-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[AuditTrail-2022-03-28-Patient];

GO

IF OBJECT_ID(N'Modified-Record-2022-03-28-Patient', N'U') IS NOT NULL
  DROP TABLE [dbo].[Modified-Record-2022-03-28-Patient];

GO

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [AuditTrail-2022-03-28-Patient]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Model = 'Patient'
       and Description LIKE '%modified%'
       AND Date >= '2022-03-28 11:00:00'
Order  by Date DESC

SELECT mainPatient.*
INTO   [Modified-Record-2022-03-28-Patient]
FROm   db_waterworksv1_live_temp_20220328_113414.dbo.tPatient mainPatient
       inner join [AuditTrail-2022-03-28-Patient] auditTrailPatient
               on mainPatient.ID = auditTrailPatient.ID_CurrentObject
                  and mainPatient.ID_Company = auditTrailPatient.ID_Company

SELECT mainPatient.DateModified,
       temporaryPatient.DateModified,
       mainPatient.Name,
       temporaryPatient.Name,
       mainPatient.DateBirth,
       temporaryPatient.DateBirth,
       mainPatient.Species,
       temporaryPatient.Species,
       mainPatient.IsNeutered,
       temporaryPatient.IsNeutered,
       mainPatient.IsDeceased,
       temporaryPatient.IsDeceased,
       mainPatient.AnimalWellness,
       temporaryPatient.AnimalWellness,
       mainPatient.DateDeceased,
       temporaryPatient.DateDeceased,
       mainPatient.DateLastVisited,
       temporaryPatient.DateLastVisited,
       mainPatient.ProfileImageFile,
       temporaryPatient.ProfileImageFile,
       mainPatient.CustomCode,
       temporaryPatient.CustomCode,
       mainPatient.WaitingStatus_ID_FilingStatus,
       temporaryPatient.WaitingStatus_ID_FilingStatus,
       mainPatient.Comment,
       temporaryPatient.Comment
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company

Update tPatient SET    Name = temporaryPatient.Name FROm   dbo.tPatient mainPatient inner join [Modified-Record-2022-03-28-Patient] temporaryPatient on mainPatient.ID = temporaryPatient.ID and mainPatient.ID_Company = temporaryPatient.ID_Company WHERE mainPatient.Name <> temporaryPatient.Name
Update tPatient
SET    DateBirth = temporaryPatient.DateBirth
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.DateBirth <> temporaryPatient.DateBirth

Update tPatient
SET    Species = temporaryPatient.Species
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.Species <> temporaryPatient.Species

Update tPatient
SET    IsNeutered = temporaryPatient.IsNeutered
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.IsNeutered <> temporaryPatient.IsNeutered

Update tPatient
SET    IsDeceased = temporaryPatient.IsDeceased
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.IsDeceased <> temporaryPatient.IsDeceased

Update tPatient
SET    AnimalWellness = temporaryPatient.AnimalWellness
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.AnimalWellness <> temporaryPatient.AnimalWellness

Update tPatient
SET    DateDeceased = temporaryPatient.DateDeceased
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.DateDeceased <> temporaryPatient.DateDeceased

Update tPatient
SET    DateLastVisited = temporaryPatient.DateLastVisited
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.DateLastVisited <> temporaryPatient.DateLastVisited

Update tPatient
SET    ProfileImageFile = temporaryPatient.ProfileImageFile
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.ProfileImageFile <> temporaryPatient.ProfileImageFile

Update tPatient
SET    CustomCode = temporaryPatient.CustomCode
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.CustomCode <> temporaryPatient.CustomCode

Update tPatient
SET    WaitingStatus_ID_FilingStatus = temporaryPatient.WaitingStatus_ID_FilingStatus
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.WaitingStatus_ID_FilingStatus <> temporaryPatient.WaitingStatus_ID_FilingStatus

Update tPatient
SET    Comment = temporaryPatient.Comment
FROm   dbo.tPatient mainPatient
       inner join [Modified-Record-2022-03-28-Patient] temporaryPatient
               on mainPatient.ID = temporaryPatient.ID
                  and mainPatient.ID_Company = temporaryPatient.ID_Company
WHERE  mainPatient.Comment <> temporaryPatient.Comment 
