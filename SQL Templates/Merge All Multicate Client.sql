EXEC dbo._solo_pBackUpDatabase

GO

DEclare @GUID_Company VARCHAR(MAX) = 'EBA615BD-3702-40C4-BC76-033C0F28DD10'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Source Company does not exist.', 1;
  END

SELECT Name Source_Company,
       *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @record TABLE
  (
     Min_ID_Client INT,
     Name_Client   VARCHAR(MAX),
     Code_Client   VARCHAR(MAX),
     ID_Company    INT
  )

INSERT @record
       (Min_ID_Client,
        Name_Client,
        ID_Company)
select MIN(ID),
       Name,
       ID_Company
FROM   vClient
WHERE  ID_Company = @ID_Company
GROUP  BY Name,
          ID_Company
HAVING COUNT(*) > 1

UPDATE @record
SET    Code_Client = c.Code
FROM   @record rec
       INNER JOIn tClient c
               on rec.Min_ID_Client = c.ID

SELECT rec.Min_ID_Client,
       rec.Code_Client,
       rec.Name_Client,
       a.ID,
       a.Name
FROm   vActiveClient a
       INNER JOin @record rec
               on a.Name = rec.Name_Client
                  AND a.ID_Company = rec.ID_Company
                  and a.ID <> rec.Min_ID_Client
ORDER  BY rec.Name_Client

DECLARE @Min_Code_Client VARCHAR(MAX)
DECLARE @Code_Client VARCHAR(MAX)
DECLARE @ID_Company_Source INT
DECLARE dbCursor CURSOR FOR
  SELECT rec.Code_Client,
         a.Code, rec.ID_Company
  FROm   vActiveClient a
         INNER JOin @record rec
                 on a.Name = rec.Name_Client
                    AND a.ID_Company = rec.ID_Company
                    and a.ID <> rec.Min_ID_Client
  ORDER  BY rec.Name_Client

OPEN dbCursor

FETCH NEXT FROM dbCursor INTO @Min_Code_Client, @Code_Client, @ID_Company_Source

WHILE @@FETCH_STATUS = 0
  BEGIN
      EXEC dbo.pMergeClientRecordByCompanyID
        @ID_Company_Source,
        @Code_Client,
        @Min_Code_Client

      FETCH NEXT FROM dbCursor INTO @Min_Code_Client, @Code_Client, @ID_Company_Source
  END

CLOSE dbCursor

DEALLOCATE dbCursor 
