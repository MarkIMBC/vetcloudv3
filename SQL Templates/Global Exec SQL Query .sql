GO

DECLARE @name VARCHAR(MAX) -- database name       
DECLARE @dateStart DateTime
DECLARE @dateEnd DateTime
DECLARE @filterString VARCHAR(MAX) = 'a'
DECLARE @sql VARCHAR(MAX) = 'SELECT ID, Name, ''' + @name
  + ''' DatabaseName FROM ' + @name
  + '.dbo.vEmployee WHERE Name LIKE ''%'
  + @filterString + '%'' '
DECLARE @table TABLE
  (
     ID           INT,
     Name         VARCHAR(MAX),
     DatabaseName VARCHAR(MAX)
  )
DECLARE db_cursorppGetCompanyActive_VetCloudv3 CURSOR FOR
  SELECT name
  FROM   MASTER.dbo.sysdatabases
  WHERE  ( name LIKE 'db_waterworksv1_server_'
            OR name LIKE 'db_waterworksv1_%' )

OPEN db_cursorppGetCompanyActive_VetCloudv3

FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dateStart = GETDATE();

      BEGIN TRY
          INSERT @table
          exec(@sql)

          SET @dateEnd = GETDATE();
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SET @dateEnd = GETDATE();
          SELECT @msg
      END CATCH

      FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name
  END

CLOSE db_cursorppGetCompanyActive_VetCloudv3

DEALLOCATE db_cursorppGetCompanyActive_VetCloudv3

SELECT *
FROM   @table
Order  by Name 
