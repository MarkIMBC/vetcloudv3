DECLARE @Source_ID_Company INT = 84
DECLARE @Destination_ID_Company INT = 263

INSERT INTO [dbo].[tItem]
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [MinInventoryCount],
             [MaxInventoryCount],
             [UnitCost],
             [UnitPrice],
             [CurrentInventoryCount],
             [Old_item_id],
             [Old_procedure_id],
             [OtherInfo_DateExpiration],
             [ID_InventoryStatus])
SELECT [Name],
       [IsActive],
       @Destination_ID_Company,
       [Comment],
       GETDATE(),
       GETDATE(),
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [ID_ItemType],
       [ID_ItemCategory],
       [MinInventoryCount],
       [MaxInventoryCount],
       [UnitCost],
       [UnitPrice],
       [CurrentInventoryCount],
       [Old_item_id],
       [Old_procedure_id],
       [OtherInfo_DateExpiration],
       [ID_InventoryStatus]
FROM   [dbo].[tItem]
WHERE  ID_Company = @Source_ID_Company
       AND IsActive = 1

GO 
