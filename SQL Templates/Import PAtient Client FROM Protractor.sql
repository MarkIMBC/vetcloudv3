DECLARE @PetVALLEY_ID_Company INT = 113
DECLARE @PatientInfo TABLE
  (
     Name_Client   VARCHAR(MAX),
     Name_Patient  VARCHAR(MAX),
     Code_Patient  VARCHAR(MAX),
     Email         VARCHAR(MAX),
     ContactNumber VARCHAR(MAX),
     DateBirth     DateTime,
     ID_Gender     INT
  )

INSERT @PatientInfo
SELECT REPLACE(dbo.fGetTitleCase(LTRIM(RTRIM(REPLACE(Name_Client, '-', '')))), '  ', ' '),
       dbo.fGetTitleCase(LTRIM(RTRIM(Name_Patient))),
       LTRIM(RTRIM(patient_number)),
       LTRIM(RTRIM(primary_email)),
       LTRIM(RTRIM(ContactNumber)),
       date_of_birth,
       ID_Gender
FROM   (select SUBSTRING(ISNULL(name, ''), CHARINDEX('-', ISNULL(name, '')), len(ISNULL(name, ''))) Name_Client,
               LTRIM(RTRIM(REPLACE(LEFT(name, CHARINDEX('-', ISNULL(name, ''))), '-', '')))         Name_Patient,
               name,
               CHARINDEX('-', ISNULL(name, ''))                                                     [dashindex],
               patient_number,
               primary_email,
               REPLACE(CONVERT(VARCHAR(MAX), CONVERT(MONEY, primary_mobile)), '.00', '')            ContactNumber,
               date_of_birth,
               CASE
                 WHEN gender = 'M' THEN 1
                 ELSE
                   CASE
                     WHEN gender = 'F' THEN 2
                     ELSE NULL
                   END
               END                                                                                  ID_Gender
        FRom   [dbo].[Animal Shelter Veterinary Clinic - Taguig - Patients]) tbl
where  dashindex > 0

DELETE FROM @PatientInfo
WHERE  LEN(Name_Client) = 0
 

INSERT INTO [dbo].[tClient]
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ContactNumber],
             [Email])
SELECT DISTINCT Name_Client,
       1,
       @PetVALLEY_ID_Company,
       'Import',
       GETDATE(),
       GETDATE(),
       1,
       1,
       MAX(ContactNumber),
       MAX(Email)
FROM   @PatientInfo
GROUP BY Name_Client
ORDER  BY Name_Client 

INSERT INTO [dbo].[tPatient]
            (ID_Client,
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [DateBirth],
             [Species],
             [CustomCode])
SELECT client.ID ID_Client,
       Name_Patient,
       1,
       @PetVALLEY_ID_Company,
       'Import',
       GETDATE(),
       GETDATE(),
       1,
       1,
       import.DateBirth,
       '',
       Code_Patient
FROM   @PatientInfo import
       inner join tClient client
               on import.Name_Client = client.Name
WHERE  ID_Company = @PetVALLEY_ID_Company
       and client.Comment = 'Import' 
