declare @IDs_Patient_SOAP TYPINTLIST
declare @_IDs_BillingInvoice TYPINTLIST

INSERT @_IDs_BillingInvoice
SELECT hed.ID
FROM   tBillingInvoice hed INNER JOIN vActiveCompany c on hed.ID_Company = c.ID
WHERE  ISNULL(hed.ID_FilingStatus, 0) NOT IN ( 0, 4 )
       AND ( ISNULL(hed.ID_Patient_Confinement, 0) > 0
              OR ISNULL(hed.ID_Patient_SOAP, 0) > 0
              OR ISNULL(hed.ID_Patient_Grooming, 0) > 0
              OR ISNULL(hed.ID_Patient_Wellness, 0) > 0 )

INSERT @IDs_Patient_SOAP
SELECT DISTINCT ID_Patient_SOAP
FROM   tBillingInvoice bi
       inner join @_IDs_BillingInvoice ids
               on bi.ID = ids.ID

EXEC pUpdatePatient_SOAP_BillingStatus
  @IDs_Patient_SOAP

exec ppUpdatePatient_Wellness_BillingStatus_By_BIs
  @_IDs_BillingInvoice

exec ppUpdatePatient_Grooming_BillingStatus_By_BIs
  @_IDs_BillingInvoice 
