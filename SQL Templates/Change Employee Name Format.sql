
DECLARE @GUID_Company VARCHAR(MAX) = 'ACE352E0-1293-4416-AB07-B8A611F37F6C'
DECLARE @ID_Company INT

SELECT @ID_Company = ID FROM tCompany where Guid = @GUID_Company

SELECT * FROm tCompany where iD = @ID_Company

Update tEmployee
SET    Name = '/*FirstName*/ /*MiddleName*/ /*LastName*/'
WHERE  ID_Company = @ID_Company

Update tEmployee
SET    Name = REPLACE(Name, '/*FirstName*/', ISNULL(FirstName, ''))
WHERE  ID_Company = @ID_Company 

Update tEmployee
SET    Name = REPLACE(Name, '/*MiddleName*/',ISNULL( MiddleName, ''))
WHERE  ID_Company = @ID_Company 

Update tEmployee
SET    Name = REPLACE(Name, '/*LastName*/',ISNULL( LastName, ''))
WHERE  ID_Company = @ID_Company 

Update tEmployee
SET    Name = REPLACE(REPLACE(Name, '   ', ' '), '  ', ' ')
WHERE  ID_Company = @ID_Company 

Update vUser set Name = Name_Employee where ID_Company = @ID_Company
SELECT *  FROm vUser where ID_Company = @ID_Company