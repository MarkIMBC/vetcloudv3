GO

Use SystemManagement 

GO
DECLARE @DatabaseName VARCHAR(MAX)
DECLARE @ID_Reference VARCHAR(MAX)
DECLARE @Oid_Model VARCHAR(MAX)
DECLARE @RowIndex Int
DECLARE db_cursorppGetCompanyActive_VetCloudv3 CURSOR FOR
  SELECT  DatabaseName,
         ID_Reference,
         Oid_Model,  ROW_NUMBER() OVER(ORDER BY DatabaseName,
         ID_Reference,
         Oid_Model ASC) AS RowinDex
  FROM   MASTER.dbo.sysdatabases databases
         inner join ForImport.[dbo].[For_Appointment_2022-12-09-20221208_1744] import
                 on databases.name = import.DatabaseName
  WHERE  ( name LIKE 'db_waterworksv1_server_'
            OR name LIKE 'db_waterworksv1_%' )

OPEN db_cursorppGetCompanyActive_VetCloudv3

FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @DatabaseName, @ID_Reference, @Oid_Model, @RowIndex

WHILE @@FETCH_STATUS = 0
  BEGIN
      BEGIN TRY
          DECLARE @sql VARCHAR(MAX) = 'exec /*database*/.dbo.pNoteSOAPPlanAsSend /*ID_Reference*/, ''/*ID_Model*/'', 20'

          SET @sql = REPLACE(@sql, '/*database*/', @DatabaseName)
          SET @sql = REPLACE(@sql, '/*ID_Reference*/', @ID_Reference)
          SET @sql = REPLACE(@sql, '/*ID_Model*/', @Oid_Model)

		  exec (@sql)
          print( @sql + ' - Row Index ' + CONVERT(VARCHAR(MAX), @RowIndex) )
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SELECT @msg
      END CATCH

      FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @DatabaseName, @ID_Reference, @Oid_Model, @RowIndex
  END

CLOSE db_cursorppGetCompanyActive_VetCloudv3

DEALLOCATE db_cursorppGetCompanyActive_VetCloudv3 
