exec _pAddModelProperty
  'tBillingInvoice',
  'tempID',
  1

exec _pAddModelProperty
  'tBillingInvoice_Detail',
  'tempID',

  1
exec _pAddModelProperty
  'tBillingInvoice_Patient',
  'tempID',
  1
GO
exec _pRefreshAllViews

IF OBJECT_ID(N'Temp-2022-03-28-BillingInvoice', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-BillingInvoice];

GO

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )
Declare @NotYetInserted_tempBillingInvoiceDetailID TABLE
  (
     tempBillingInvoiceDetailID VARCHAR(MAX)
  )

SELECT company.Name                                            Name_Company,
       mainClient.tempID                                       tempIDClient,
       ISNULL(mainClient.ID, BillingInvoice.ID_Client)         Main_ID_Client,
       'Temp-2022-03-28-' + company.Guid
       + '-BillingInvoice-'
       + Convert(Varchar(MAX), BillingInvoice.ID)              tempID,
       'Temp-2022-03-28-' + company.Guid
       + '-BillingInvoice_Detail-'
       + Convert(Varchar(MAX), BillingInvoiceDetail.ID)        tempBillingInvoiceDetailID,
       BillingInvoice.*,
       BillingInvoiceDetail.ID                                 BillingInvoice_Detail_ID,
       ConVERT(VARCHAR(MAX), mainItem.ID ) + '||'
       + ConVERT(VARCHAR(MAX), BillingInvoiceDetail.ID_Item)   compareID,
       ConVERT(VARCHAR(MAX), mainItem.Name ) + '||'
       + ConVERT(VARCHAR(MAX), BillingInvoiceDetail.Name_Item) compareName,
       ISNULL(mainItem.ID, BillingInvoiceDetail.ID_Item)       BillingInvoice_Detail_Main_ID_Item,
       BillingInvoiceDetail.Quantity                           BillingInvoice_Detail_Quantity,
       BillingInvoiceDetail.Amount                             BillingInvoice_Detail_Amount,
       BillingInvoiceDetail.UnitPrice                          BillingInvoice_Detail_UnitPrice,
       BillingInvoiceDetail.DateExpiration                     BillingInvoice_Detail_DateExpiration,
       BillingInvoiceDetail.UnitCost                           BillingInvoice_Detail_UnitCost,
       BillingInvoiceDetail.DiscountAmount                     BillingInvoice_Detail_DiscountAmount,
       BillingInvoiceDetail.IsComputeDiscountRate              BillingInvoice_Detail_IsComputeDiscountRate,
       BillingInvoiceDetail.DiscountRate                       BillingInvoice_Detail_DiscountRate
INTO   [dbo].[Temp-2022-03-28-BillingInvoice]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vBillingInvoice BillingInvoice
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].vBillingInvoice_Detail BillingInvoiceDetail
               ON BillingInvoice.ID = BillingInvoiceDetail.ID_BillingInvoice
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on BillingInvoice.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-03-28-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), BillingInvoice.ID_Client)  and mainClient.ID_Company = BillingInvoice.ID_Company
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-03-28-' + company.Guid + '-Item-'
                                   + Convert(Varchar(MAX), BillingInvoiceDetail.ID_Item ) and mainItem.ID_Company = BillingInvoice.ID_Company
where  BillingInvoice.DateCreated > '2022-03-28 10:00:00'
 AND ID_FilingStatus NOT IN ( 1, 4 )


INSERT @NotYetInserted_TempID
SELECT tempBillingInvoice.tempID
FROm   [dbo].[Temp-2022-03-28-BillingInvoice] tempBillingInvoice

INSERT @NotYetInserted_tempBillingInvoiceDetailID
SELECT tempBillingInvoice.tempBillingInvoiceDetailID
FROm   [dbo].[Temp-2022-03-28-BillingInvoice] tempBillingInvoice

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tBillingInvoice)

DELETE FROM @NotYetInserted_tempBillingInvoiceDetailID
WHERE  tempBillingInvoiceDetailID IN (SELECT tempID
                                      FROM   tBillingInvoice_Detail)

INSERT tBillingInvoice
       (ID_Company,
        OtherReferenceNumber,
        Date,
        ID_Client,
        IsWalkIn,
        WalkInCustomerName,
        ID_TaxScheme,
        BillingAddress,
        VatAmount,
        DiscountAmount,
        GrossAmount,
        VatPercentage,
        NetAmount,
        ID_FilingStatus,
        ID_ApprovedBy,
        ID_CanceledBy,
        DateApproved,
        DateCanceled,
        Payment_ID_FilingStatus,
        RemainingAmount,
        Discount,
        DiscountRate,
        IsComputeDiscountRate,
        SubTotal,
        TotalAmount,
        ID_SOAPType,
        PatientNames,
        ConfinementDepositAmount,
        RemainingDepositAmount,
        ConsumedDepositAmount,
        InitialSubtotalAmount,
        ID_Patient_Wellness,
        TotalItemDiscountAmount,
        InitialConfinementDepositAmount,
        tempID,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT distinct tempporyBillingInvoice.ID_Company,
                CASE
                  WHEN LEN(ISNULL(tempporyBillingInvoice.Code, '')) > 0 THEN CHAR(13)
                  ELSE ''
                END
                + 'Imported from VetCloudTemp March 28, 2022'
                + Char(13) + ' Reference Code ('
                + tempporyBillingInvoice.Code + ')' OtherReferenceNumber,
                tempporyBillingInvoice.Date,
                tempporyBillingInvoice.Main_ID_Client,
                tempporyBillingInvoice.IsWalkIn,
                tempporyBillingInvoice.WalkInCustomerName,
                tempporyBillingInvoice.ID_TaxScheme,
                tempporyBillingInvoice.BillingAddress,
                tempporyBillingInvoice.VatAmount,
                tempporyBillingInvoice.DiscountAmount,
                tempporyBillingInvoice.GrossAmount,
                tempporyBillingInvoice.VatPercentage,
                tempporyBillingInvoice.NetAmount,
                tempporyBillingInvoice.ID_FilingStatus,
                tempporyBillingInvoice.ID_ApprovedBy,
                tempporyBillingInvoice.ID_CanceledBy,
                tempporyBillingInvoice.DateApproved,
                tempporyBillingInvoice.DateCanceled,
                tempporyBillingInvoice.Payment_ID_FilingStatus,
                tempporyBillingInvoice.RemainingAmount,
                tempporyBillingInvoice.Discount,
                tempporyBillingInvoice.DiscountRate,
                tempporyBillingInvoice.IsComputeDiscountRate,
                tempporyBillingInvoice.SubTotal,
                tempporyBillingInvoice.TotalAmount,
                tempporyBillingInvoice.ID_SOAPType,
                tempporyBillingInvoice.PatientNames,
                tempporyBillingInvoice.ConfinementDepositAmount,
                tempporyBillingInvoice.RemainingDepositAmount,
                tempporyBillingInvoice.ConsumedDepositAmount,
                tempporyBillingInvoice.InitialSubtotalAmount,
                tempporyBillingInvoice.ID_Patient_Wellness,
                tempporyBillingInvoice.TotalItemDiscountAmount,
                tempporyBillingInvoice.InitialConfinementDepositAmount,
                tempporyBillingInvoice.tempID,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1,
                CASE
                  WHEN LEN(ISNULL(tempporyBillingInvoice.Comment, '')) > 0 THEN CHAR(13)
                  ELSE ''
                END
                + 'Imported from VetCloudTemp March 28, 2022'
                + Char(13) + ' Reference Code ('
                + tempporyBillingInvoice.Code + ')' Comment
FROm   [Temp-2022-03-28-BillingInvoice] tempporyBillingInvoice
       inner join @NotYetInserted_TempID temp
               on tempporyBillingInvoice.tempID = temp.tempID
       LEFT join tClient mainClient
              on mainClient.ID = tempporyBillingInvoice.Main_ID_Client

INSERT tBillingInvoice_Detail
       (ID_BillingInvoice,
        tempID,
        ID_Item,
        Quantity,
        Amount,
        UnitPrice,
        DateExpiration,
        UnitCost,
        DiscountAmount,
        IsComputeDiscountRate,
        DiscountRate)
SELECT DISTINCT mainBillingInvoice.ID,
       tempInvoice.tempBillingInvoiceDetailID,
       BillingInvoice_Detail_Main_ID_Item,
       BillingInvoice_Detail_Quantity,
       BillingInvoice_Detail_Amount,
       BillingInvoice_Detail_UnitPrice,
       BillingInvoice_Detail_DateExpiration,
       BillingInvoice_Detail_UnitCost,
       BillingInvoice_Detail_DiscountAmount,
       BillingInvoice_Detail_IsComputeDiscountRate,
       BillingInvoice_Detail_DiscountRate
FROm   tBillingInvoice mainBillingInvoice
       inner join [Temp-2022-03-28-BillingInvoice] tempInvoice
               on mainBillingInvoice.tempID = tempInvoice.tempID
       inner join @NotYetInserted_tempBillingInvoiceDetailID tempBIDetail
               on tempBIDetail.tempBillingInvoiceDetailID = tempInvoice.tempBillingInvoiceDetailID

IF OBJECT_ID(N'Temp-2022-03-28-BillingInvoice', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-BillingInvoice];

GO

GO

CReate OR
ALTER PROC _tempGenerateBillingInvoice(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_BillingInvoice TABLE
        (
           RowID             INT,
           ID_BillingInvoice INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_BillingInvoice
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tBillingInvoice
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_BillingInvoice

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_BillingInvoice INT = 0

            SELECT @ID_BillingInvoice = ID_BillingInvoice
            FROM   @IDs_BillingInvoice
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_BillingInvoice]
              @ID_BillingInvoice,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for BillingInvoice IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateBillingInvoice
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateBillingInvoice

GO 
