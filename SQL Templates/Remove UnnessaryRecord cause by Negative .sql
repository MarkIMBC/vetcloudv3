

Update tPatient_SOAP SET ID_Patient = NULL where ID_Patient = -1
Update tPatient_SOAP SET ID_Client = NULL where ID_Client = -1


Update tBillingInvoice SET ID_Patient = NULL where ID_Patient = -1
Update tBillingInvoice SET ID_Client = NULL where ID_Client = -1


Update tPatient_Confinement SET ID_Patient = NULL where ID_Patient = -1
Update tPatient_Confinement SET ID_Client = NULL where ID_Client = -1

Update tPatient_Wellness SET ID_Patient = NULL where ID_Patient = -1
Update tPatient_Wellness SET ID_Client = NULL where ID_Client = -1

Update tBillingInvoice SET ID_Client = NULL where ID_Client = -1
Update tBillingInvoice SET ID_Patient = NULL where ID_Patient = -1

