SELECT DISTINCT ServerName,
                ID,
                Comment,
                Name_Company,
                date,
                Guid
FROM   (SELECT 'Old Server' ServerName,
               hed.*,
               GUID
        FROm   db_waterworksv1_live_dev.dbo.vInactiveSMSSending hed
               INNER JOIN db_waterworksv1_live_dev.dbo.vCompany c
                       on c.ID = hed.ID_Company
        UNION ALL
        SELECT 'Server 1' ServerName,
               hed.*,
               GUID
        FROm   db_waterworksv1_server1_dev.dbo.vInactiveSMSSending hed
               INNER JOIN db_waterworksv1_server1_dev.dbo.vCompany c
                       on c.ID = hed.ID_Company
        UNION ALL
        SELECT 'Server 2' ServerName,
               hed.*,
               GUID
        FROm   db_waterworksv1_server2_dev.dbo.vInactiveSMSSending hed
               INNER JOIN db_waterworksv1_server2_dev.dbo.vCompany c
                       on c.ID = hed.ID_Company
        UNION ALL
        SELECT 'Server 3' ServerName,
               hed.*,
               GUID
        FROm   db_waterworksv1_server3_dev.dbo.vInactiveSMSSending hed
               INNER JOIN db_waterworksv1_server3_dev.dbo.vCompany c
                       on c.ID = hed.ID_Company
        UNION ALL
        SELECT 'Server 4' ServerName,
               hed.*,
               GUID
        FROm   db_waterworksv1_server4_dev.dbo.vInactiveSMSSending hed
               INNER JOIN db_waterworksv1_server4_dev.dbo.vCompany c
                       on c.ID = hed.ID_Company) tbl
ORDER  BY Date,
          Name_Company ASC 
