GO

GO

CREATE OR
ALTER PROC dbo._solo_pBackUpDatabase @Comment VARCHAR(MAX) = NULL
AS
    DECLARE @name VARCHAR(50) -- database name            
    DECLARE @path VARCHAR(500) -- path for backup files            
    DECLARE @fileName VARCHAR(256) -- filename for backup            
    DECLARE @fileDate VARCHAR(20) -- used for file name          
    DECLARE @databaseName VARCHAR(100) = DB_NAME()

    SET @path = dbo.fGetBackUpPath()

    SELECT @fileDate = CONVERT(VARCHAR(20), GETDATE(), 112)
                       + REPLACE(CONVERT(VARCHAR(8), GETDATE(), 108), ':', '')

    SET @fileName = @path + @name + '_' + @fileDate + '.BAK'

    DECLARE db_cursor CURSOR FOR
      SELECT name
      FROM   master.dbo.sysdatabases
      WHERE  name = DB_NAME()

    OPEN db_cursor

    FETCH NEXT FROM db_cursor INTO @name

    WHILE @@FETCH_STATUS = 0
      BEGIN
          SET @fileName = @path + @name + '_' + @fileDate + '.BAK'

          --DBCC SHRINKDATABASE(@name)          
          BACKUP DATABASE @name TO DISK = @fileName

          FETCH NEXT FROM db_cursor INTO @name
      END

    CLOSE db_cursor

    DEALLOCATE db_cursor

GO

CREATE OR
ALTER PROC pReOrganizeRebuildTableIndex
AS
  BEGIN
      DECLARE @fragmentTable TABLE
        (
           RowIndex         INT,
           TableName        [nvarchar](258) NULL,
           objectid         [int] NULL,
           indexid          [int] NULL,
           partitionnum_old [int] NULL,
           partitionnum_new [int] NULL,
           frag_old         [float] NULL,
           frag_new         [float] NULL,
           page_count_old   [bigint] NULL,
           page_count_new   [bigint] NULL
        )
      DECLARE @reorg_frag_thresh float
      DECLARE @rebuild_frag_thresh float
      DECLARE @fill_factor tinyint
      DECLARE @report_only bit
      DECLARE @page_count_thresh smallint

      SET @reorg_frag_thresh = 10.0
      SET @rebuild_frag_thresh = 30.0
      SET @fill_factor = 0
      SET @report_only = 0
      SET @page_count_thresh = 1

      INSERT @fragmentTable
             (RowIndex,
              TableName,
              objectid,
              indexid,
              partitionnum_old,
              frag_old,
              page_count_old)
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY (SELECT 1)),
             QUOTENAME(o.[name])              TableName,
             s.[object_id]                    AS objectid,
             s.[index_id]                     AS indexid,
             s.[partition_number]             AS partitionnum,
             s.[avg_fragmentation_in_percent] AS frag,
             s.[page_count]                   AS page_count
      FROM   sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, 'LIMITED') s
             INNER JOIN sys.objects AS o WITH (NOLOCK)
                     on s.object_id = o.object_id
      WHERE  o.[name] LIKE '_t%'
              OR o.[name] LIKE 't%'
      ORDER  BY QUOTENAME(o.[name]) ASC

      -- *********************************************************************************************
      DECLARE @objectid int
      DECLARE @indexid int
      DECLARE @partitioncount bigint
      DECLARE @schemaname nvarchar(130)
      DECLARE @objectname nvarchar(130)
      DECLARE @indexname nvarchar(130)
      DECLARE @partitionnum bigint
      DECLARE @partitions bigint
      DECLARE @frag float
      DECLARE @page_count int
      DECLARE @command nvarchar(4000)
      DECLARE @intentions nvarchar(4000)
      DECLARE @table_var TABLE
        (
           objectid     int,
           indexid      int,
           partitionnum int,
           frag         float,
           page_count   int
        )

      INSERT INTO @table_var
      SELECT [object_id]                    AS objectid,
             [index_id]                     AS indexid,
             [partition_number]             AS partitionnum,
             [avg_fragmentation_in_percent] AS frag,
             [page_count]                   AS page_count
      FROM   sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, 'LIMITED')
      WHERE  [avg_fragmentation_in_percent] > @reorg_frag_thresh
             AND page_count > @page_count_thresh
             AND index_id > 0

      DECLARE partitions CURSOR FOR
        SELECT *
        FROM   @table_var

      OPEN partitions

      WHILE ( 1 = 1 )
        BEGIN
            FETCH NEXT FROM partitions INTO @objectid,
                                            @indexid,
                                            @partitionnum,
                                            @frag,
                                            @page_count

            IF @@FETCH_STATUS < 0
              BREAK

            SELECT @objectname = QUOTENAME(o.[name]),
                   @schemaname = QUOTENAME(s.[name])
            FROM   sys.objects AS o WITH (NOLOCK)
                   JOIN sys.schemas AS s WITH (NOLOCK)
                     ON s.[schema_id] = o.[schema_id]
            WHERE  o.[object_id] = @objectid

            SELECT @indexname = QUOTENAME([name])
            FROM   sys.indexes WITH (NOLOCK)
            WHERE  [object_id] = @objectid
                   AND [index_id] = @indexid

            SELECT @partitioncount = count (*)
            FROM   sys.partitions WITH (NOLOCK)
            WHERE  [object_id] = @objectid
                   AND [index_id] = @indexid

            SET @intentions = @schemaname + N'.' + @objectname + N'.' + @indexname
                              + N':' + CHAR(13) + CHAR(10)
            SET @intentions = REPLACE(SPACE(LEN(@intentions)), ' ', '=')
                              + CHAR(13) + CHAR(10) + @intentions
            SET @intentions = @intentions + N' FRAGMENTATION: '
                              + CAST(@frag AS nvarchar) + N'%' + CHAR(13)
                              + CHAR(10) + N' PAGE COUNT: '
                              + CAST(@page_count AS nvarchar) + CHAR(13)
                              + CHAR(10)

            IF @frag < @rebuild_frag_thresh
              BEGIN
                  SET @intentions = @intentions + N' OPERATION: REORGANIZE'
                                    + CHAR(13) + CHAR(10)
                  SET @command = N'ALTER INDEX ' + @indexname + N' ON '
                                 + @schemaname + N'.' + @objectname
                                 + N' REORGANIZE; ' + N' UPDATE STATISTICS '
                                 + @schemaname + N'.' + @objectname + N' ' + @indexname
                                 + ';'
              END

            IF @frag >= @rebuild_frag_thresh
              BEGIN
                  SET @intentions = @intentions + N' OPERATION: REBUILD'
                                    + CHAR(13) + CHAR(10)
                  SET @command = N'ALTER INDEX ' + @indexname + N' ON '
                                 + @schemaname + N'.' + @objectname + N' REBUILD'
              END

            IF @partitioncount > 1
              BEGIN
                  SET @intentions = @intentions + N' PARTITION: '
                                    + CAST(@partitionnum AS nvarchar(10))
                                    + CHAR(13) + CHAR(10)
                  SET @command = @command + N' PARTITION='
                                 + CAST(@partitionnum AS nvarchar(10))
              END

            IF @frag >= @rebuild_frag_thresh
               AND @fill_factor > 0
               AND @fill_factor < 100
              BEGIN
                  SET @intentions = @intentions + N' FILL FACTOR: '
                                    + CAST(@fill_factor AS nvarchar) + CHAR(13)
                                    + CHAR(10)
                  SET @command = @command + N' WITH (FILLFACTOR = '
                                 + CAST(@fill_factor AS nvarchar) + ')'
              END

            IF @report_only = 0
              BEGIN
                  SET @intentions = @intentions + N' EXECUTING: ' + @command

                  PRINT @intentions

                  EXEC (@command)
              END
            ELSE
              BEGIN
                  PRINT @intentions
              END

            PRINT @command
        END

      CLOSE partitions

      DEALLOCATE partitions

      /********************************************************************************************************************/
      Update @fragmentTable
      SET    partitionnum_new = tbl.partitionnum,
             frag_new = tbl.frag,
             page_count_new = tbl.page_count
      FROM   @fragmentTable frag
             INNER JOIN (SELECT ROW_NUMBER()
                                  OVER(
                                    ORDER BY (SELECT 1) )        RowIndex,
                                QUOTENAME(o.[name])              TableName,
                                s.[object_id]                    AS objectid,
                                s.[index_id]                     AS indexid,
                                s.[partition_number]             AS partitionnum,
                                s.[avg_fragmentation_in_percent] AS frag,
                                s.[page_count]                   AS page_count
                         FROM   sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, 'LIMITED') s
                                INNER JOIN sys.objects AS o WITH (NOLOCK)
                                        on s.object_id = o.object_id
                         WHERE  o.[name] LIKE '_t%'
                                 OR o.[name] LIKE 't%') tbl
                     on frag.RowIndex = tbl.RowIndex

      SELECT *
      FROM   @fragmentTable
      where  partitionnum_new <> partitionnum_old
      Order  by tablename
  END

GO

exec pReOrganizeRebuildTableIndex

GO

exec _solo_pBackUpDatabase

GO 
