GO

CREATE OR
ALTER PROC pReschedulePatientSOAPPatientWellnessRecords (@GUID_Company VARCHAR(MAX),
                                                         @DateFrom     DateTime,
                                                         @DateTo       DateTime)
as
  begin
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   vCompanyActive
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------------------------------
      SELECT sched.ID_Patient_SOAP,
             hed.Code,
             hed.Name_Client,
             hed.Name_Patient,
             Name_Item,
             sched.DateReturn,
             Convert(DateTime, FORMAT(@DateTo, 'yyyy-MM-dd') + ' '
                               + FORMAT(sched.DateReturn, 'HH:mm')) DateReschedule,
             IsSentSMS,
             DateSent
      FROm   vPatient_SOAP_Plan sched
             INNER JOIN vPatient_SOAP hed
                     on hed.ID = sched.ID_Patient_SOAP
      WHERE  hed.ID_Company = @ID_Company
             AND CONVERT(Date, sched.DateReturn) = CONVERT(Date, @DateFrom)

      SELECT sched.ID_Patient_Wellness,
             hed.Code,
             hed.Name_Client,
             hed.Name_Patient,
             sched.Comment,
             sched.Date,
             Convert(DateTime, FORMAT(@DateTo, 'yyyy-MM-dd') + ' '
                               + FORMAT(sched.Date, 'HH:mm')) DateReschedule,
             IsSentSMS,
             DateSent
      FROm   vPatient_Wellness_Schedule sched
             INNER JOIN vPatient_Wellness hed
                     on hed.ID = sched.ID_Patient_Wellness
      WHERE  hed.ID_Company = @ID_Company
             AND CONVERT(Date, sched.Date) = CONVERT(Date, @DateFrom)

      Update tPatient_SOAP_Plan
      SET    DateReturn = Convert(DateTime, FORMAT(@DateTo, 'yyyy-MM-dd') + ' '
                                            + FORMAT(sched.DateReturn, 'HH:mm')),
             IsSentSMS = 0,
             DateSent = null
      FROm   tPatient_SOAP_Plan sched
             INNER JOIN vPatient_SOAP hed
                     on hed.ID = sched.ID_Patient_SOAP
      WHERE  hed.ID_Company = @ID_Company
             AND CONVERT(Date, DateReturn) = CONVERT(Date, @DateFrom)

      Update tPatient_Wellness_Schedule
      SET    Date = Convert(DateTime, FORMAT(@DateTo, 'yyyy-MM-dd') + ' '
                                      + FORMAT(sched.Date, 'HH:mm')),
             IsSentSMS = 0,
             DateSent = null
      FROm   tPatient_Wellness_Schedule sched
             INNER JOIN vPatient_Wellness hed
                     on hed.ID = sched.ID_Patient_Wellness
      WHERE  hed.ID_Company = @ID_Company
             AND CONVERT(Date, sched.Date) = CONVERT(Date, @DateFrom)
  END

GO

BEGIN TRANSACTION

exec pReschedulePatientSOAPPatientWellnessRecords
  '6965C9C8-3C12-4D28-858E-707DB4BF144C',
  '2023-03-19',
  '2023-03-20'

ROLLBACK TRANSACTION 
