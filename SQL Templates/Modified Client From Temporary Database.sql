IF OBJECT_ID(N'AuditTrail-2022-03-28-Client', N'U') IS NOT NULL
  DROP TABLE [dbo].[AuditTrail-2022-03-28-Client];

GO

IF OBJECT_ID(N'Modified-Record-2022-03-28-Client', N'U') IS NOT NULL
  DROP TABLE [dbo].[Modified-Record-2022-03-28-Client];

GO

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [AuditTrail-2022-03-28-Client]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Model = 'Client'
       and Description LIKE '%modified%'
       AND Date >= '2022-03-28 11:00:00'
Order  by Date DESC

SELECT mainclient.*
INTO   [Modified-Record-2022-03-28-Client]
FROm   db_waterworksv1_live_temp_20220328_113414.dbo.tClient mainclient
       inner join [AuditTrail-2022-03-28-Client] auditTrailClient
               on mainclient.ID = auditTrailClient.ID_CurrentObject
                  and mainclient.ID_Company = auditTrailClient.ID_Company

Update tClient
SET    Name = temporaryClient.Name,
       Comment = temporaryClient.Comment,
       ContactNumber = temporaryClient.ContactNumber,
       Email = temporaryClient.Email,
       Address = temporaryClient.Address,
       ContactNumber2 = temporaryClient.ContactNumber2,
       DateLastVisited = temporaryClient.DateLastVisited,
       CurrentCreditAmount = temporaryClient.CurrentCreditAmount,
       CustomCode = temporaryClient.CustomCode
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company

Update tClient
SET    Name = temporaryClient.Name
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.Name <> temporaryClient.Name

Update tClient
SET    Comment = temporaryClient.Comment
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.Comment <> temporaryClient.Comment

Update tClient
SET    ContactNumber = temporaryClient.ContactNumber
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.ContactNumber <> temporaryClient.ContactNumber

Update tClient
SET    Email = temporaryClient.Email
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.Email <> temporaryClient.Email

Update tClient
SET    Address = temporaryClient.Address
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.Address <> temporaryClient.Address

Update tClient
SET    ContactNumber2 = temporaryClient.ContactNumber2
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.ContactNumber2 <> temporaryClient.ContactNumber2

Update tClient
SET    DateLastVisited = temporaryClient.DateLastVisited
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.DateLastVisited <> temporaryClient.DateLastVisited

Update tClient
SET    CurrentCreditAmount = temporaryClient.CurrentCreditAmount
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.CurrentCreditAmount <> temporaryClient.CurrentCreditAmount

Update tClient
SET    CustomCode = temporaryClient.CustomCode
FROm   dbo.tClient mainclient
       inner join [Modified-Record-2022-03-28-Client] temporaryClient
               on mainclient.ID = temporaryClient.ID
                  and mainclient.ID_Company = temporaryClient.ID_Company
WHERE  mainclient.CustomCode <> temporaryClient.CustomCode 
