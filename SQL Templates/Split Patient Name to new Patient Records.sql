DECLARE @ID_Company INT
DECLARE @ID_Patient INT
DECLARE @ID_Client INT
DECLARE @Patient_Name VARCHAR(MAX) = ''
DECLARE @ID_Gender INT
DECLARE @Species VARCHAR(MAX) = ''
DECLARE @GUID_Patient VARCHAR(MAX) = ''
DECLARE @record TABLE
  (
     ID_Company   INT,
     ID_Patient   INT,
     ID_Client    INT,
     Patient_Name VARCHAR(MAX),
     ID_Gender    INT,
     Species      VARCHAR(MAX),
     tempID       VARCHAR(MAX),
     Comment      VARCHAR(MAX)
  )
DECLARE dbCursor CURSOR FOR
  select ID_Company,
         ID,
         ID_Client,
         Name,
         ID_Gender,
         Species,
         tempID
  FROM   vPatient
  WHERE  ID_Company = 302
         AND Name LIKE '%,%'

OPEN dbCursor

FETCH NEXT FROM dbCursor INTO @ID_Company,
                              @ID_Patient,
                              @ID_Client,
                              @Patient_Name,
                              @ID_Gender,
                              @Species,
                              @GUID_Patient

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT INTO @record
                  (ID_Company,
                   ID_Patient,
                   ID_Client,
                   Patient_Name,
                   ID_Gender,
                   Species,
                   tempID,
                   Comment)
      SELECT @ID_Company,
             CASE
               WHEN ID = 1 THEN @ID_Patient
               ELSE NULL
             END,
             @ID_Client,
             LTRIM(RTRIM(Part)),
             @ID_Gender,
             @Species,
             @GUID_Patient + '-From ID-'
             + CONVERT(varchar(MAX), @ID_Patient) + '-'
             + CONVERT(varchar(MAX), ID),
             'From ID-'
             + CONVERT(varchar(MAX), @ID_Patient) + '-'
             + CONVERT(varchar(MAX), ID)
      FROM   dbo.fGetSplitString(@Patient_Name, ', ')

      FETCH NEXT FROM dbCursor INTO @ID_Company,
                                    @ID_Patient,
                                    @ID_Client,
                                    @Patient_Name,
                                    @ID_Gender,
                                    @Species,
                                    @GUID_Patient
  END

CLOSE dbCursor

DEALLOCATE dbCursor


SELECT * FROm @record

INSERT INTO tPatient
            (ID_Company,
             ID_Client,
             Name,
             ID_Gender,
             Species,
             tempID,
             Comment,
             IsActive,
             ID_CreatedBy,
             DateCreated)
SELECT ID_Company,
       ID_Client,
       Patient_Name,
       ID_Gender,
       Species,
       tempID,
       Comment,
       1,
       10,
       GETDATE()
FROm   @record
WHERE  ID_Patient IS NULL

UPDATE tPatient
SET    Name = rec.Patient_Name
FROM   tPatient patient
       INNER JOIN @record rec
               on patient.ID = rec.ID_Patient
                  and patient.ID_Company = rec.ID_Company
WHERE  ID_Patient IS NOT NULL 
