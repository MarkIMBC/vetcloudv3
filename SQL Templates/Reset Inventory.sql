DEclare @GUID_Company VARCHAR(MAX) = '60B46DDB-E2EA-4904-A98D-8F3467D2640F'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset Inventory',
       ID,
       CurrentInventoryCount,
       0.00,
       OtherInfo_DateExpiration,
       NULL,
       3,
       @ID_Company,
       'Reset Inventory on '
       + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt'),
       CASE
         WHEN CurrentInventoryCount > 0 then 0
         ELSE 1
       END
FROM   tItem
where  ID_Company = @ID_Company
       AND ISNULL(CurrentInventoryCount, 0) > 0

Declare @ID_UserSession INT

select TOP 1 @ID_UserSession = ID
FRom   tUsersession
where  ID_User = 1
ORDER  BY ID DESC

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession

--------
Update tPatient_Confinement
set    IsActive = 0
where  ID_Company = @ID_Company 
