GO

ALTER VIEW [dbo].[vPurchaseOrder_Detail]
as
  SELECT DET.*,
         I.Name                   AS Item,
         UOM.Name                 AS UOM,
         I.Name                   AS Name_Item,
         UOM.Name                 AS Name_UOM,
         purchaseOrder.ID_Company Main_ID_Company
  FROM   tPurchaseOrder_Detail DET
         LEFT JOIN tItem I
                ON DET.ID_Item = I.ID
         LEFT JOIN tUnitOfMeasure UOM
                ON DET.ID_UOM = UOM.ID
         LEFT JOIN vPurchaseOrder purchaseOrder
                on purchaseOrder.ID = DET.ID_PurchaseOrder

GO

IF COL_LENGTH('tReceivingReport_Detail', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tReceivingReport_Detail',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-03-28-ReceivingReport_Detail', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-ReceivingReport_Detail]

GO

Declare @NotYetInserted_Temp_ID_ReceivingReport_Detail TABLE
  (
     Temp_ID_ReceivingReport_Detail VARCHAR(MAX)
  )

SELECT company.Guid                                       Main_Guid_Company,
       company.ID                                         Main_ID_Company,
       company.Name                                       Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-ReceivingReport-'
       + Convert(Varchar(MAX), ReceivingReport_.ID)       Temp_ID_ReceivingReport,
       'Temp-2022-03-28-' + company.Guid
       + '-ReceivingReport_Detail-'
       + Convert(Varchar(MAX), ReceivingReport_Detail.ID) Temp_ID_ReceivingReport_Detail,
       ReceivingReport_.Code                              Code_ReceivingReport,
       ReceivingReport_Detail.*
INTO   [dbo].[Temp-2022-03-28-ReceivingReport_Detail]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vReceivingReport ReceivingReport_
       INNER JOIN db_waterworksv1_live_temp_20220328_113414.[dbo].tReceivingReport_Detail ReceivingReport_Detail
               on ReceivingReport_.ID = ReceivingReport_Detail.ID_ReceivingReport
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on ReceivingReport_.ID_Company = company.ID
where  ReceivingReport_.DateCreated > '2022-03-28 10:00:00'

INSERT @NotYetInserted_Temp_ID_ReceivingReport_Detail
SELECT tempReceivingReport.Temp_ID_ReceivingReport_Detail
FROm   [dbo].[Temp-2022-03-28-ReceivingReport_Detail] tempReceivingReport

DELETE FROM @NotYetInserted_Temp_ID_ReceivingReport_Detail
WHERE  Temp_ID_ReceivingReport_Detail IN (SELECT tempID
                                          FROM   tReceivingReport_Detail)

INSERT tReceivingReport_Detail
       (tempID,
        ID_ReceivingReport,
        ID_Item,
        ID_PurchaseOrder_Detail,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Quantity,
        Amount,
        UnitPrice)
SELECT tempReceivingReport_Detail.Temp_ID_ReceivingReport_Detail,
       ISNULL(mainReceivingReport.ID, mainReceivingReport2.ID)           Main_ID_ReceivingReport,
       ISNULL(mainItem.ID, mainItem2.ID)                                 Main_ID_Item,
       ISNULL(mainPurchaseOrder_Detail.ID, mainPurchaseOrder_Detail2.ID) Main_ID_PurchaseOrder_Detail,
       + 'Imported from VetCloudTemp March 28, 2022'
       + ' Reference Code ('
       + tempReceivingReport_Detail.Code_ReceivingReport
       + ')'
       + CASE
           WHEN LEN(ISNULL(tempReceivingReport_Detail.Comment, '')) > 0 THEN CHAR(13)
                                                                             + tempReceivingReport_Detail.Comment
           ELSE ''
         END                                                             Comment,
       tempReceivingReport_Detail.DateCreated,
       tempReceivingReport_Detail.DateModified,
       tempReceivingReport_Detail.ID_CreatedBy,
       tempReceivingReport_Detail.ID_LastModifiedBy,
       tempReceivingReport_Detail.Quantity,
       tempReceivingReport_Detail.Amount,
       tempReceivingReport_Detail.UnitPrice
FROm   [dbo].[Temp-2022-03-28-ReceivingReport_Detail] tempReceivingReport_Detail
       inner join @NotYetInserted_Temp_ID_ReceivingReport_Detail unInsertedTempReceivingReport_Detail
               on tempReceivingReport_Detail.Temp_ID_ReceivingReport_Detail = unInsertedTempReceivingReport_Detail.Temp_ID_ReceivingReport_Detail
       ------------ RECEIVING REPORT ----------------
       LEFT JOIN tReceivingReport mainReceivingReport
              on mainReceivingReport.tempID = 'Temp-2022-03-28-'
                                              + tempReceivingReport_Detail.Main_Guid_Company
                                              + '-ReceivingReport-'
                                              + Convert(Varchar(MAX), tempReceivingReport_Detail.ID_ReceivingReport)
                 and mainReceivingReport.ID_Company = tempReceivingReport_Detail.Main_ID_Company
       LEFT JOIN tReceivingReport mainReceivingReport2
              on mainReceivingReport2.ID = tempReceivingReport_Detail.ID_ReceivingReport
                 and mainReceivingReport2.ID_Company = tempReceivingReport_Detail.Main_ID_Company
       ------------ RECEIVING REPORT END --------------------------
       ------------ Purchase Order Detail ----------------
       LEFT JOIN vPurchaseOrder_Detail mainPurchaseOrder_Detail
              on mainPurchaseOrder_Detail.tempID = 'Temp-2022-03-28-'
                                                   + tempReceivingReport_Detail.Main_Guid_Company
                                                   + '-PurchaseOrder_Detail-'
                                                   + Convert(Varchar(MAX), tempReceivingReport_Detail.ID_PurchaseOrder_Detail)
                 and mainPurchaseOrder_Detail.Main_ID_Company = tempReceivingReport_Detail.Main_ID_Company
       LEFT JOIN vPurchaseOrder_Detail mainPurchaseOrder_Detail2
              on mainPurchaseOrder_Detail2.ID = tempReceivingReport_Detail.ID_PurchaseOrder_Detail
                 and mainPurchaseOrder_Detail2.Main_ID_Company = tempReceivingReport_Detail.Main_ID_Company
       ------------  Purchase Order Detail END --------------------------
       /*                                  */
       ------------ Item ----------------
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-03-28-'
                                   + tempReceivingReport_Detail.Main_Guid_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempReceivingReport_Detail.ID_Item)
                 and mainItem.ID_Company = tempReceivingReport_Detail.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempReceivingReport_Detail.ID_Item
                 and mainItem2.ID_Company = tempReceivingReport_Detail.Main_ID_Company
------------ Item END ----------------
/*                                  */
WHERE  1 = 1

GO

EXEC _pRefreshAllViews

select TOP 1 *
FROm   tReceivingReport_Detail 
