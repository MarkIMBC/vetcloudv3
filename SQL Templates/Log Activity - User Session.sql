DECLARE @IDs_User TYPINTLIST
DECLARE @DateStart DATE = '2021-12-01'
DECLARE @DateEnd DATE = '2021-12-31'

INSERT @IDs_User
SELECT ID
FROM   vUser
WHERE  ID_Company = 149

SELECT _userSession.DateCreated [Date Login],
       Name_Employee
FROM   tUserSession _userSession
       INNER JOIN @IDs_User ids
               ON ids.ID = _userSession.ID_User
       INNER JOIN vUser _user 
               ON _user.ID = ids.ID
WHERE  CONVERT(DATE, _userSession.DateCreated) BETWEEN @DateStart AND @DateEnd 
