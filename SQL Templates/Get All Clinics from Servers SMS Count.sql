SELECT tbl.*
FROM   (SELECT company.Name,
               MaxSMSCountPerDay,
			   Guid,
               'Old Server' ServerName
        FROM   db_waterworksv1_live_dev.dbo. vCompanyActive company
               LEFT join db_waterworksv1_live_dev.dbo.tCompany_SMSSetting smsSetting
                       on company.ID = smsSetting.ID_Company
        UNION ALL
        SELECT company.Name,
               MaxSMSCountPerDay,
			   Guid,
               'Server 1' ServerName
        FROM   db_waterworksv1_server1_dev.dbo. vCompanyActive company
               LEFT join db_waterworksv1_server1_dev.dbo.tCompany_SMSSetting smsSetting
                       on company.ID = smsSetting.ID_Company
        UNION ALL
        SELECT company.Name,
               MaxSMSCountPerDay,
			   Guid,
               'Server 2' ServerName
        FROM   db_waterworksv1_server2_dev.dbo. vCompanyActive company
               inner join db_waterworksv1_server2_dev.dbo.tCompany_SMSSetting smsSetting
                       on company.ID = smsSetting.ID_Company
        UNION ALL
        SELECT company.Name,
               MaxSMSCountPerDay,
			   Guid,
               'Server 3' ServerName
        FROM   db_waterworksv1_server3_dev.dbo. vCompanyActive company
               LEFT join db_waterworksv1_server3_dev.dbo.tCompany_SMSSetting smsSetting
                       on company.ID = smsSetting.ID_Company
        UNION ALL
        SELECT company.Name,
               MaxSMSCountPerDay,
			   Guid,
               'Server 4' ServerName
        FROM   db_waterworksv1_server4_dev.dbo. vCompanyActive company
               LEFT join db_waterworksv1_server4_dev.dbo.tCompany_SMSSetting smsSetting
                       on company.ID = smsSetting.ID_Company) tbl
order  by Name 