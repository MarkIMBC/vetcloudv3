select TOP 100000  _user.Name_Company,
       _user.Name_Employee,
       Date,
       Description, Model, ID_CurrentObject
from   vAuditTrail _auditTrail
       LEFT JOIN tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN vUser _user
              on _user.ID = _auditTrail.ID_User
	--WHERE Model = 'Item'
	Order by Date DESC
		 
SELECT Date,
       Name_Company,
       Count(*) UserCount,
       Sum(AuditTrailCount)   AuditTrailCount
FROM   (select ID_Company,
               Name_Company,
               CONVERT(DATE, Date) Date,
               _user.ID            ID_User,
               _user.Name          Name_User,
               COUNT(*)            AuditTrailCount
        FROm   tAuditTrail _audit
               inner join vUser _user
                       on _audit.ID_User = _user.ID
        WHERE  CONVERT(DATE, Date) =  CONVERT(DATE, GETDATE()) and ID_Company = 250
        GROUP  BY ID_Company,
                  Name_Company,
                  _user.ID,
                  _user.Name,
                  CONVERT(DATE, Date)) tbl
GROUP  BY Date DESC,
          Name_Company
