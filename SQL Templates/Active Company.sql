GO
CREATE   OR
ALTER PROC pInsertCompany_IsActiveLog(@ID_Company INT,
                                      @IsActive   bit,
                                      @Date       Datetime)
as
  BEGIN
      INSERT INTO [dbo].tCompany_IsActiveLog
                  ([Name],
                   [IsActive],
                   ID_Company,
                   Date)
      VALUES      ('',
                   @IsActive,
                   @ID_Company,
                   @Date)
  END

GO


CREATE OR
ALTER PROC pActiveCompany(@GUID_Company VARCHAR(MAX))
as
  begin
      IF(SELECT Count(*)
         FROM   vCompanyInactive
         WHERE  Guid = @GUID_Company) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      SELECT *
      FROm   tCompany
      WHERE  GUID = @GUID_Company

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      ----------------------------------------------------------------
      if OBJECT_ID('dbo.tCompany_IsActiveLog') is null
        BEGIN
            exec _pCreateAppModuleWithTable
              'tCompany_IsActiveLog',
              1,
              1,
              NULL

            exec _pAddModelProperty
              'tCompany_IsActiveLog',
              'Date',
              5

            exec _pRefreshAllViews
        END

      Update tCompany
      SET    IsActive = 1
      FROM   tCompany company
      WHERE  IsActive = 0
             and ID = @ID_Company


      DECLARE @Date Datetime = getDAte()

      exec pInsertCompany_IsActiveLog
        @ID_Company,
        1,
        @Date
  END

GO

exec pActiveCompany 'E788C1B2-6229-4F9F-8F7F-1AB92E9B8215'
