Update db_waterworksv1_live_dev.dbo.tPatient_SOAP_Plan
SET    IsSentSMS = 1,
       DateSent = soapPlanSent.DateSent
FROm   db_waterworksv1_live_dev.dbo.tPatient_SOAP_Plan soapPlanLive
       inner join db_waterworksv1_live_old.dbo.tPatient_SOAP_Plan soapPlanSent
               on soapPlanLive.ID = soapPlanSent.ID
WHERE  Convert(Date, soapPlanSent.DateSent) = '2022-03-29'
       and soapPlanSent.IsSentSMS = 1

Update db_waterworksv1_live_dev.dbo.tPatient_Wellness_Schedule
SET    IsSentSMS = 1,
       DateSent = wellnessSchedOld.DateSent
FROm   db_waterworksv1_live_dev.dbo.tPatient_Wellness_Schedule wellnesSchedLive
       inner join db_waterworksv1_live_old.dbo.tPatient_Wellness_Schedule wellnessSchedOld
               on wellnesSchedLive.ID = wellnessSchedOld.ID
WHERE  Convert(Date, wellnessSchedOld.DateSent) = '2022-03-29'
       and wellnessSchedOld.IsSentSMS = 1 
