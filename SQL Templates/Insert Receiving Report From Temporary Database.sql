IF COL_LENGTH('tReceivingReport', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tReceivingReport',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF COL_LENGTH('tEmployee', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tEmployee',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF OBJECT_ID(N'Temp-2022-03-28-AuditTrail-ReceivingReport-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-AuditTrail-ReceivingReport-Added]

GO

IF OBJECT_ID(N'Temp-2022-03-28-ReceivingReport', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-ReceivingReport]

GO

Declare @NotYetInserted_Temp_ID_ReceivingReport TABLE
  (
     Temp_ID_ReceivingReport VARCHAR(MAX)
  )

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [dbo].[Temp-2022-03-28-AuditTrail-ReceivingReport-Added]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel _model
              on _auditTrail.ID_Model = _model.Oid
WHERE  Date >= '2022-03-28 11:00:00'
       AND Description like '%added%'
       AND _model.TableName = 'tReceivingReport'
Order  by Date

SELECT company.ID                                   Main_ID_Company,
       company.Guid                                 Main_GUID_Company,
       company.Name                                 Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-ReceivingReport-'
       + Convert(Varchar(MAX), ReceivingReport_.ID) Temp_ID_ReceivingReport,
       ReceivingReport_.*
INTO   [dbo].[Temp-2022-03-28-ReceivingReport]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vReceivingReport ReceivingReport_
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on ReceivingReport_.ID_Company = company.ID
where  ReceivingReport_.DateCreated > '2022-03-28 10:00:00'

INSERT @NotYetInserted_Temp_ID_ReceivingReport
SELECT tempReceivingReport.Temp_ID_ReceivingReport
FROm   [dbo].[Temp-2022-03-28-ReceivingReport] tempReceivingReport

DELETE FROM @NotYetInserted_Temp_ID_ReceivingReport
WHERE  Temp_ID_ReceivingReport IN (SELECT tempID
                                   FROM   tReceivingReport)

INSERT tReceivingReport
       (ID_Company,
        tempID,
        ID_PurchaseOrder,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_FilingStatus,
        ID_Supplier,
        ID_TaxScheme,
        GrossAmount,
        VatAmount,
        NetAmount,
        ID_ApprovedBy,
        DateApproved,
        Date,
        ID_CanceledBy,
        DateCanceled,
        ServingStatus_ID_FilingStatus,
        DiscountRate,
        DiscountAmount,
        IsComputeDiscountRate,
        SubTotal,
        TotalAmount)
SELECT tempReceivingReport.ID_Company,
       tempReceivingReport.Temp_ID_ReceivingReport,
       ISNULL(mainPurchaseOrder.ID, mainPurchaseOrder2.ID) Main_ID_PurchaseOrder,
       + 'Imported from VetCloudTemp March 28, 2022'
       + ' Reference Code ('
       + tempReceivingReport.Code + ')'
       + CASE
           WHEN LEN(ISNULL(tempReceivingReport.Comment, '')) > 0 THEN CHAR(13) + tempReceivingReport.Comment
           ELSE ''
         END                                               Comment,
       tempReceivingReport.DateCreated,
       tempReceivingReport.DateModified,
       tempReceivingReport.ID_CreatedBy,
       tempReceivingReport.ID_LastModifiedBy,
       tempReceivingReport.ID_FilingStatus,
       tempReceivingReport.ID_Supplier,
       tempReceivingReport.ID_TaxScheme,
       tempReceivingReport.GrossAmount,
       tempReceivingReport.VatAmount,
       tempReceivingReport.NetAmount,
       tempReceivingReport.ID_ApprovedBy,
       tempReceivingReport.DateApproved,
       tempReceivingReport.Date,
       tempReceivingReport.ID_CanceledBy,
       tempReceivingReport.DateCanceled,
       tempReceivingReport.ServingStatus_ID_FilingStatus,
       tempReceivingReport.DiscountRate,
       tempReceivingReport.DiscountAmount,
       tempReceivingReport.IsComputeDiscountRate,
       tempReceivingReport.SubTotal,
       tempReceivingReport.TotalAmount
FROm   [dbo].[Temp-2022-03-28-ReceivingReport] tempReceivingReport
       inner join @NotYetInserted_Temp_ID_ReceivingReport unInsertedTempReceivingReport
               on tempReceivingReport.Temp_ID_ReceivingReport = unInsertedTempReceivingReport.Temp_ID_ReceivingReport
       ------------ Purchase Order ----------------
       LEFT JOIN tPurchaseOrder mainPurchaseOrder
              on mainPurchaseOrder.tempID = 'Temp-2022-03-28-'
                                            + tempReceivingReport.Main_GUID_Company
                                            + '-PurchaseOrder-'
                                            + Convert(Varchar(MAX), tempReceivingReport.ID_PurchaseOrder)
                 and mainPurchaseOrder.ID_Company = tempReceivingReport.Main_ID_Company
       LEFT JOIN tPurchaseOrder mainPurchaseOrder2
              on mainPurchaseOrder2.ID = tempReceivingReport.ID_PurchaseOrder
                 and mainPurchaseOrder2.ID_Company = tempReceivingReport.Main_ID_Company

------------ Purchase Order END --------------------------
GO

exec _pRefreshAllViews

GO

GO

CReate OR
ALTER PROC _tempGenerateReceivingReport(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_ReceivingReport TABLE
        (
           RowID              INT,
           ID_ReceivingReport INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_ReceivingReport
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tReceivingReport
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_ReceivingReport

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_ReceivingReport INT = 0

            SELECT @ID_ReceivingReport = ID_ReceivingReport
            FROM   @IDs_ReceivingReport
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_ReceivingReport]
              @ID_ReceivingReport,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for ReceivingReport IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany
Order  by Name

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateReceivingReport
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateReceivingReport

GO 
