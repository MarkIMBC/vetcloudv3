IF COL_LENGTH('tPatient_SOAP_Prescription', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP_Prescription',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-09-14-Patient_SOAP_Prescription', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-14-Patient_SOAP_Prescription]

GO

Declare @NotYetInserted_Temp_ID_Patient_SOAP_Prescription TABLE
  (
     Temp_ID_Patient_SOAP_Prescription VARCHAR(MAX)
  )

SELECT company.Guid                                          Main_Guid_Company,
       company.ID                                            Main_ID_Company,
       company.Name                                          Name_Company,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP-'
       + Convert(Varchar(MAX), Patient_SOAP_.ID)             Temp_ID_Patient_SOAP,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP_Prescription-'
       + Convert(Varchar(MAX), Patient_SOAP_Prescription.ID) Temp_ID_Patient_SOAP_Prescription,
	   Patient_SOAP_.Code Code_Patient_SOAP,
       Patient_SOAP_Prescription.*
INTO   [dbo].[Temp-2022-09-14-Patient_SOAP_Prescription]
FROM   _______db_waterworksv1_oldserver_20220914164457.[dbo].vPatient_SOAP Patient_SOAP_
       INNER JOIN _______db_waterworksv1_oldserver_20220914164457.[dbo].tPatient_SOAP_Prescription Patient_SOAP_Prescription
               on Patient_SOAP_.ID = Patient_SOAP_Prescription.ID_Patient_SOAP
       inner join _______db_waterworksv1_oldserver_20220914164457.[dbo].tCompany company
               on Patient_SOAP_.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-09-14-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient_SOAP_.ID_Client)
                 and mainClient.ID_Company = Patient_SOAP_.ID_Company
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-09-14-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), Patient_SOAP_.ID_Patient)
                 and mainPatient.ID_Company = Patient_SOAP_.ID_Company
where  Patient_SOAP_.DateCreated > '2022-08-30 16:42:32.770' and company.GUID =  '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

INSERT @NotYetInserted_Temp_ID_Patient_SOAP_Prescription
SELECT tempPatient_SOAP.Temp_ID_Patient_SOAP_Prescription
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Prescription] tempPatient_SOAP

DELETE FROM @NotYetInserted_Temp_ID_Patient_SOAP_Prescription
WHERE  Temp_ID_Patient_SOAP_Prescription IN (SELECT tempID
                                             FROM   tPatient_SOAP_Prescription)

SELECT DISTINCT Main_ID_Company, Name_Company, Code_Patient_SOAP FROM [Temp-2022-09-14-Patient_SOAP_Prescription]

INSERT tPatient_SOAP_Prescription
       (ID_Patient_SOAP,
        ID_Item,
        Quantity,
        UnitCost,
        DateExpiration,
        UnitPrice,
        IsCharged,
		Comment,
        TempID
		)
SELECT ISNULL(mainPatient_SOAP.ID, mainPatient_SOAP2.ID) Main_ID_Patient_SOAP,
       ISNULL(mainItem.ID, mainItem2.ID)                 Main_ID_Item,
       tempPatient_SOAP_Prescription.Quantity,
       tempPatient_SOAP_Prescription.UnitCost,
       tempPatient_SOAP_Prescription.DateExpiration,
       tempPatient_SOAP_Prescription.UnitPrice,
       tempPatient_SOAP_Prescription.IsCharged,
	   tempPatient_SOAP_Prescription.Comment,
       tempPatient_SOAP_Prescription.Temp_ID_Patient_SOAP_Prescription
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Prescription] tempPatient_SOAP_Prescription
       inner join @NotYetInserted_Temp_ID_Patient_SOAP_Prescription unInsertedTempPatient_SOAP_Prescription
               on tempPatient_SOAP_Prescription.Temp_ID_Patient_SOAP_Prescription = unInsertedTempPatient_SOAP_Prescription.Temp_ID_Patient_SOAP_Prescription
       ------------ SOAP ----------------
       LEFT JOIN tPatient_SOAP mainPatient_SOAP
              on mainPatient_SOAP.tempID = 'Temp-2022-09-14-'
                                           + tempPatient_SOAP_Prescription.Main_Guid_Company
                                           + '-Patient_SOAP-'
                                           + Convert(Varchar(MAX), tempPatient_SOAP_Prescription.ID_Patient_SOAP)
                 and mainPatient_SOAP.ID_Company = tempPatient_SOAP_Prescription.Main_ID_Company
       LEFT JOIN tPatient_SOAP mainPatient_SOAP2
              on mainPatient_SOAP2.ID = tempPatient_SOAP_Prescription.ID_Patient_SOAP
                 and mainPatient_SOAP2.ID_Company = tempPatient_SOAP_Prescription.Main_ID_Company
       ------------ SOAP END --------------------------
       /*                                  */
       ------------ Item ----------------
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-09-14-'
                                   + tempPatient_SOAP_Prescription.Main_Guid_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempPatient_SOAP_Prescription.ID_Item)
                 and mainItem.ID_Company = tempPatient_SOAP_Prescription.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempPatient_SOAP_Prescription.ID_Item
                 and mainItem2.ID_Company = tempPatient_SOAP_Prescription.Main_ID_Company
------------ Item END ----------------
/*                                  */
WHERE  1 = 1
GO

exec _pRefreshAllViews


 SELECT * FROM [dbo].[Temp-2022-09-14-Patient_SOAP_Prescription]