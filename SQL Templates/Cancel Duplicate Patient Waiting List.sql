 Declare @IDs_PatientWaitingList typINTLIST
 
 INSERT @IDs_PatientWaitingList
 SELECT MIN(ID) ID 
  FROM   vPatientWaitingList  
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )  
  Group BY 
	  ID_Company,  
         Name_Client,  
         Name_Patient,  
         ID_Client,  
         ID_Patient,  
         WaitingStatus_ID_FilingStatus,  
         BillingInvoice_ID_FilingStatus,  
         WaitingStatus_Name_FilingStatus,  
         ISNULL(BillingInvoice_Name_FilingStatus, '---') 
		 HAVING COUNT(*) > 1

		 exec pUpdatePatientToWaitingListStatus @IDs_PatientWaitingList, 4,  53236
