IF OBJECT_ID('tempdb..#Latest') IS NOT NULL
  DROP TABLE #Latest

GO

IF OBJECT_ID('tempdb..#Old') IS NOT NULL
  DROP TABLE #Old

GO

DECLARE @Record TABLE
  (
     [ID_Company]   varchar(1000),
     [Name_Company] varchar(1000),
     [Message]      varchar(1000)
  )

/***********************  LATEST  *******************************/
SELECT *
INTO   #Latest
FROM   ForImport.[dbo].[For Appointment 2022-10-17-20221017_0936]

/*************************  OLD  ********************************/
SELECT *
INTO   #Old
FROM   ForImport.[dbo].[For Appointment 2022-10-17-20221016_1137]

/****************************************************************/
INSERT @Record
SELECT [ID_Company],
       [Name_Company],
       [Message]
FROM   #Latest
EXCEPT
SELECT [ID_Company],
       [Name_Company],
       [Message]
FROM   #Old

SELECT latest.*
FROM   #Latest latest
       inner join @Record record
               on latest.[Name_Company] = record.[Name_Company]
                  AND REPLACE(latest.[Message], CHAR(13), '') = REPLACE(record.[Message], CHAR(13), '')
Order  by Name_Company,
          [Name_Client],
          Message

IF OBJECT_ID('tempdb..#Latest') IS NOT NULL
  DROP TABLE #Latest

GO

IF OBJECT_ID('tempdb..#Old') IS NOT NULL
  DROP TABLE #Old

GO 
