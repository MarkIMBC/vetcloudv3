Declare @table Table
  (
     ID_Patient_SOAP      INT,
     ID_Patient_SOAP_Plan INT,
     ID_SOAP_SMSStatus    INT
  )
Declare @Client Table
  (
     Client        VARCHAR(MAX),
     ContactNumber VARCHAR(MAX)
  )

INSERT @table
select soapPlan.ID_Patient_SOAP,
       soapPlan.ID,
       MAX(soapSMS.ID) ID_SOAP_SMSStatus
FROM   tPatient_SOAP hed
       inner join tPatient_SOAP_Plan soapPlan
               on hed.ID = soapPlan.ID_Patient_SOAP
       Left JOIN tPatient_SOAP_SMSStatus soapSMS
              ON soapSMS.ID_Patient_SOAP = soapPlan.ID
where  hed.ID_Company = 44
       and CONVERT(Date, soapPlan.DateReturn) BETWEEN '2021-10-01' AND '2021-10-15'
GROUP  BY soapPlan.ID_Patient_SOAP,
          soapPlan.ID,
          soapPlan.IsSentSMS

INSERT @Client
SELECT DISTINCT client.Name,
                ISNULL(client.ContactNumber, '')
FROM   @table hed
       inner JOIN vPatient_SOAP soap
               on soap.ID = hed.ID_Patient_SOAP
       inner join vPatient_SOAP_Plan soapPlan
               on soapPlan.ID = hed.ID_Patient_SOAP_Plan
       INNER JOIN tCompany c
               on c.ID = soap.ID_Company
       inner join tClient client
               on client.ID = soap.ID_Client

INSERT @Client
SELECT Distinct Name_Client,
                client.ContactNumber
FROM   vPatient_VAccination hed
       inner join tPatient_Vaccination_Schedule vacSched
               on hed.ID = vacSched.ID_Patient_Vaccination
       inner join tClient client
               on client.ID = hed.ID_Client
where  ID_FilingStatus = 1
       AND hed.ID_Company = 44
       and CONVERT(Date, vacSched.Date) BETWEEN '2021-10-01' AND '2021-10-15'

SELECT DISTINCT *
FROM   @Client
ORDER  BY Client 
