GO
Use db_waterworksv1_companynamelowercase

GO


DECLARE @ID_Company INT
DECLARE @GUID_Company VARCHAR(MAX)

SELECT @ID_Company = ID, @GUID_Company = GUID
FROM   tCompany
WHERE  LOWER(REPLACE(Name, ' ', '')) = 'companynamelowercase'

/* Set Currrent Clinic as Active and others are inactive */
Update tCompany
SET    IsActive = 1
where  GUID IN ( '1118F106-718B-406B-932A-A7C1C05796AE', @GUID_Company )

Update tCompany
SET    IsActive = 0
where  GUID NOt IN ( '1118F106-718B-406B-932A-A7C1C05796AE', @GUID_Company )

sELECT *
FROm   vCompanyActive
Order  by Name

GO

CREATE  OR
ALTER FUNCTION dbo.fGetAPILink()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = '';

      SET @result = 'https://companynamelowercaseapi.veterinarycloudsystem.com'

      return @result
  END

GO

CREATE OR
ALTER FUNCTION dbo.fGetBackUpPath()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = '';

      SET @result = 'C:\inetpub\wwwroot\VetCloudv3-companynamelowercase\database\'

      return @result
  END

GO 
