DECLARE @ItemCategory TABLE
  (
     Name VARCHAR(MAX)
  )

INSERT @ItemCategory
VALUES ('Promotions'),
       ('Wellness'),
       ('Pharmacy NRx'),
       ('Pharmacy Rx'),
       ('Misc')

INSERT INTO [dbo].[tItemCategory]
            ([Name],
             [ID_ItemType],
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT Name,
       2,
       1,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @ItemCategory

GO 
