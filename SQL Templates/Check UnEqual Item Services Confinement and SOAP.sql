DECLARE @soapItems TABLE
  (
     ID_Patient_Confinement INT,
     Count                  INT
  )
DECLARE @confItems TABLE
  (
     ID_Patient_Confinement INT,
     Count                  INT
  )

INSERT @soapItems
SELECT ID_Patient_Confinement,
       --    ID_Patient_SOAP,
       SUM(Count) Count
FROM   (select soapTreatment.ID_Patient_SOAP,
               soap_.ID_Patient_Confinement,
               Count(*) Count
        FROM   tPatient_SOAP soap_
               INNER JOIN tPatient_SOAP_Treatment soapTreatment
                       on soapTreatment.ID_Patient_SOAP = soap_.ID
               inner join tPatient_Confinement confinement
                       on soap_.ID_Patient_Confinement = confinement.ID
               INNER JOIN vCompanyActive c
                       on soap_.ID_Company = c.ID
        where  confinement.ID_FilingStatus NOT IN ( 4 )
               and soap_.ID_FilingStatus NOT IN ( 4 )
        GROUP  BY soapTreatment.ID_Patient_SOAP,
                  soap_.ID_Patient_Confinement
        UNION ALL
        select soapPrescription.ID_Patient_SOAP,
               soap_.ID_Patient_Confinement,
               Count(*) Count
        FROM   tPatient_SOAP soap_
               INNER JOIN tPatient_SOAP_Prescription soapPrescription
                       on soapPrescription.ID_Patient_SOAP = soap_.ID
               inner join tPatient_Confinement confinement
                       on soap_.ID_Patient_Confinement = confinement.ID
               INNER JOIN vCompanyActive c
                       on soap_.ID_Company = c.ID
        where  confinement.ID_FilingStatus NOT IN ( 4 )
               and soap_.ID_FilingStatus NOT IN ( 4 ) ANd IsCharged = 1
        GROUP  BY soapPrescription.ID_Patient_SOAP,
                  soap_.ID_Patient_Confinement) tbl
GROUP  BY ID_Patient_Confinement

INSERT @confItems
SELECT ID_Patient_Confinement,
       COUNT(*)
FROM   tPatient_Confinement_ItemsServices confinementItemServices
       inner join tPatient_Confinement confinement
               on confinement.ID = confinementItemServices.ID_Patient_Confinement
where  confinement.ID_FilingStatus NOT IN ( 4 )
GROUP  BY ID_Patient_Confinement

SELECT confRecord.ID,
       confRecord.Date,
       confRecord.Code,
       soap_.Count SOAPCount,
       confine.Count ConfinementCount,
       c.Name,
       confRecord.Name_FilingStatus,
       confRecord.DateCreated,
       DateDischarge
FROM   @confItems confine
       inner join @soapItems soap_
               on confine.ID_Patient_Confinement = soap_.ID_Patient_Confinement
       Inner join vPatient_Confinement confRecord
               on confRecord.ID = confine.ID_Patient_Confinement
       INNER JOIN vCompanyActive c
               on confRecord.ID_Company = c.ID
WHERE  soap_.Count > confine.Count
Order  by Date DESC 
