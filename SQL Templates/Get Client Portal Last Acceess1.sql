SELECT _userUsession.DateCreated,
       _company.Name Clinic,
       _client.Name
FROM   tUSerSession _userUsession
       inner join tUser _user
               on _userUsession.ID_User = _user.ID
       inner join tClient _client
               on _client.ID_User = _user.ID
       inner join tCompany _company
               on _company.ID = _client.ID_Company
Order  by _userUsession.DateCreated,
          _company.Name,
          _client.Name 
