GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO
SELECT *
Into   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice
FROm   (SELECT DISTINCt company.Name,
                        ID_Patient_Confinement,
                        conf.Date
        FROM   vPatient_Confinement_ItemsServices confItem
               Inner join tPatient_Confinement conf
                       on conf.ID = ID_Patient_Confinement
               Inner join vCompanyActive company
                       on company.ID = conf.ID_Company
               INNER JOIN tItem item
                       on confItem.ID_Item = item.ID
        WHERE  ISNULL(confItem.UnitPrice, 0) = 0
               and item.UnitPrice > 0
               and ( confItem.ID_Patient_SOAP_Prescription IS NOT NULL
                      OR confItem.ID_Patient_SOAP_Treatment IS NOT NULL )
               and ID_FilingStatus = 14
        UNION ALL
        SELECT company.Name,
               ID_Patient_Confinement,
               billing.Date
        FROM   tBillingInvoice billing
               Inner join vBillingInvoice_Detail billingItem
                       on billing.Iwa
ORDER  BY Name, Date DESC

GO

SELECT *
FROM   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO
GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO