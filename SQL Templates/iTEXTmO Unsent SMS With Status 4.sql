SELECT company.Name,
       soapPlan.ID                                                                                                                                                                                                                      ID_Patiend_SOAP_Plan,
       soap.Code,
       DateReturn,
	   DateCreatedMax,
       Name_Client,
       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                               ContactNumber,
       dbo.fGetSOAPLANMessage(company.Name, company.SOAPPlanSMSMessage, Name_Client, ISNULL(company.ContactNumber, ''), Name_Patient, ISNULL(Name_Item, ''), ISNULL(soapPlan.Comment, ''), DateReturn)                                  Message,
       dbo.fGetITextMessageCreditCount(dbo.fGetSOAPLANMessage(company.Name, company.SOAPPlanSMSMessage, Name_Client, ISNULL(company.ContactNumber, ''), Name_Patient, ISNULL(Name_Item, ''), ISNULL(soapPlan.Comment, ''), DateReturn)) ItextMoCredit
FROM   (SELECT CONVERT(Date, DateCreated) DateCreated,
               MAX(DateCreated)           DateCreatedMax,
               ID_Patient_SOAP,
               iTextMo_Status,
               Count(*)                   SentCount
        FROM   tPatient_SOAP_SMSStatus
        WHERE  iTextMo_Status = 4
        GROUP  BY CONVERT(Date, DateCreated),
                  ID_Patient_SOAP,
                  iTextMo_Status) smsStatus
       INNER JOIN vPatient_SOAP_Plan soapPlan
               on smsStatus.ID_Patient_SOAP = soapPlan.ID
       INNER JOIN vPatient_SOAP soap
               on soap.ID = soapPlan.ID_Patient_SOAP
       INNER JOIN vClient client
               on soap.ID_Client = client.ID
       INNER JOIN tCompany company
               on company.ID = client.ID_Company
WHERE  CONVERT(Date, DateReturn)  IN ( '2022-07-08', '2022-07-07')
       AND CONVERT(Date,  smsStatus.DateCreatedMax) = '2022-07-08'
       and smsStatus.iTextMo_Status = 4
	   	        and ISNULL(IsSentSMS, 0) = 0
       AND ID_FilingStatus NOT IN ( 4 )
ORDER  BY smsStatus.DateCreated DESC,
          Name_Client

SELECT company.Name,
       ID_Patient_Wellness_Schedule,
       wellness.Code,
       wellnessSchedule.Date,
	   DateCreatedMax,
       Name_Client,
       dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                                           ContactNumber,
       dbo.fGetSOAPLANMessage(company.Name, company.SOAPPlanSMSMessage, Name_Client, isnull(company.ContactNumber, ''), Name_Patient, ISNULL('', ''), ISNULL(wellnessSchedule.Comment, ''), wellnessSchedule.Date)                                  Message,
       dbo.fGetITextMessageCreditCount(dbo.fGetSOAPLANMessage(company.Name, company.SOAPPlanSMSMessage, Name_Client, isnull(company.ContactNumber, ''), Name_Patient, ISNULL('', ''), ISNULL(wellnessSchedule.Comment, ''), wellnessSchedule.Date)) ItextMoCredit
FROM   (SELECT CONVERT(Date, DateCreated) DateCreated,
               ID_Patient_Wellness_Schedule,
               iTextMo_Status,
               MAX(DateCreated)           DateCreatedMax,
               Count(*)                   SentCount
        FROM   tPatient_Wellness_Schedule_SMSStatus
        WHERE  iTextMo_Status =4
        GROUP  BY CONVERT(Date, DateCreated),
                  ID_Patient_Wellness_Schedule,
                  iTextMo_Status) smsStatus
       INNER JOIN vPatient_Wellness_Schedule wellnessSchedule
               on smsStatus.ID_Patient_Wellness_Schedule = wellnessSchedule.ID
       INNER JOIN vPatient_Wellness wellness
               on wellness.ID = wellnessSchedule.ID_Patient_Wellness
       INNER JOIN vClient client
               on wellness.ID_Client = client.ID
       INNER JOIN tCompany company
               on company.ID = client.ID_Company
WHERE  CONVERT(Date, wellnessSchedule.Date) IN ( '2022-07-08', '2022-07-07')
       AND CONVERT(Date,  smsStatus.DateCreatedMax) = '2022-07-08'
       and smsStatus.iTextMo_Status =4
	        and ISNULL(IsSentSMS, 0) = 0
       AND ID_FilingStatus NOT IN ( 4 )
ORDER  BY smsStatus.DateCreated DESC,
          Name_Client 
