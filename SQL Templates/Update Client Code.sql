GO

CReate OR
ALTER PROC pAutoGenerateItemCode(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_Item TABLE
        (
           RowID      INT,
           ID_Item INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_Item
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tItem
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_Item

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   vCompanyActive
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_Item INT = 0

            SELECT @ID_Item = ID_Item
            FROM   @IDs_Item
            WHERE  RowID = @currentCounter

            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_ItemType INT;
            DECLARE @Services_ID_ItemType INT = 1
            DECLARE @Inventorialble_ID_ItemType INT = 2

            SELECT @ID_Company = ID_Company, @ID_ItemType = ID_ItemType
            FROM   dbo.tItem WITH (NOLOCK)
            WHERE  ID = @ID_Item;

            if( @ID_ItemType = @Inventorialble_ID_ItemType )  
              BEGIN  
                  SELECT @Oid_Model = m.Oid  
                  FROM   dbo._tModel m  
                  WHERE  m.TableName = 'tItem';  
              END  
            ELSE if( @ID_ItemType = @Services_ID_ItemType )  
              BEGIN  
                  SELECT @Oid_Model = m.Oid  
                  FROM   dbo._tModel m  
                  WHERE  m.TableName = 'tItemService';  
              END  
  
            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL); 

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tItem
            SET    Code = @Code
            WHERE  ID = @ID_Item;

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for Item IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   vCompanyActive

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec pAutoGenerateItemCode
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC pAutoGenerateItemCode

GO 
