DECLARE @Transfer Table
  (
     Source_Code_Patient      Varchar(MAX),
     Source_Code_Client       Varchar(MAX),
     Destination_Code_Patient Varchar(MAX),
     Destination_Code_Client  Varchar(MAX)
  )
DECLARE @Guid_Company Varchar(MAX) = '36B26761-B361-4912-87C4-1387C432B407'
DECLARE @Source_Code_Patient Varchar(MAX)= 'PNT-41484'
DECLARE @Source_Code_Client Varchar(MAX) = 'CNT-30085'

INSERT @Transfer
       (Source_Code_Patient,
        Source_Code_Client,
        Destination_Code_Patient,
        Destination_Code_Client)
VALUES ('PNT-61606',
        'CNT-33113',
        @Source_Code_Patient,
        @Source_Code_Client),
       ('PNT-62477',
        'CNT-33765',
        @Source_Code_Patient,
        @Source_Code_Client)

BEGIN TRANSACTION

DECLARE @Destination_Code_Patient Varchar(MAX)
DECLARE @Destination_Code_Client Varchar(MAX)
DECLARE db_cursor CURSOR FOR
  SELECT Source_Code_Patient,
         Source_Code_Client,
         Destination_Code_Patient,
         Destination_Code_Client
  FROM   @Transfer

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Source_Code_Patient, @Source_Code_Client, @Destination_Code_Patient, @Destination_Code_Client

WHILE @@FETCH_STATUS = 0
  BEGIN
      exec pTransferPetToNewClientByCompany
        @Guid_Company,
        @Source_Code_Patient,
        @Source_Code_Client,
        @Destination_Code_Client

      exec pMergePatientRecordByCompany
        @Guid_Company,
        @Source_Code_Patient,
        @Destination_Code_Patient

      FETCH NEXT FROM db_cursor INTO @Source_Code_Patient, @Source_Code_Client, @Destination_Code_Patient, @Destination_Code_Client
  END

CLOSE db_cursor

DEALLOCATE db_cursor

ROLLBACK TRANSACTION 
