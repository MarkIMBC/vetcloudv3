exec _pAddModelProperty
  'tUser',
  'OldPassword',
  1

GO

EXEC _pRefreshAllViews

GO

Update vUser SET OldPassword = Password
FROM   vUser _User
       inner join tCompany company
               on _User.ID_Company = company.ID
WHERE  company.IsActive = 0 

Update vUser SET Password = LEFT(NEWID(), 4)
FROM   vUser _User
       inner join tCompany company
               on _User.ID_Company = company.ID
WHERE  company.IsActive = 0 

SELECT * FROM   vUser _User
       inner join tCompany company
               on _User.ID_Company = company.ID
WHERE  company.IsActive = 1 