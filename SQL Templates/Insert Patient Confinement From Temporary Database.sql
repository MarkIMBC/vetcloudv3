exec _pAddModelProperty
  'tPatient_Confinement',
  'tempID',
  1

GO

exec _pAddModelProperty
  'tPatient_Confinement_Patient',
  'tempID',
  1

GO

exec _pRefreshAllViews

GO

IF OBJECT_ID(N'Temp-2022-03-28-AuditTrail', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-AuditTrail]

GO

IF OBJECT_ID(N'Temp-2022-03-28-Patient_Confinement-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-Patient_Confinement-Added]

GO

IF OBJECT_ID(N'Temp-2022-03-28-Patient_Confinement_Patient-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-Patient_Confinement_Patient-Added]

GO

IF OBJECT_ID(N'Temp-2022-03-28-Patient_Confinement-Modified', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-Patient_Confinement-Modified]

GO

Declare @NotYetInserted_TempID_Patient_Confinement TABLE
  (
     TempID_Patient_Confinement VARCHAR(MAX)
  )
Declare @NotYetInserted_TempID_Patient_Confinement_Patient TABLE
  (
     TempID_Patient_Confinement_Patient VARCHAR(MAX)
  )

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [dbo].[Temp-2022-03-28-AuditTrail]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Date >= '2022-03-28 11:00:00'
       AND model = 'Confinement'

SELECT DISTINCT company.Name                                        Name_Company,
                'Temp-2022-03-28-' + company.Guid
                + '-Patient_Confinement-'
                + Convert(Varchar(MAX), patientConfinement.ID)      temp_ID_Patient_Confinement,
                ISNULL(mainClient.ID, patientConfinement.ID_Client) Main_ID_Client,
                patientConfinement.*
INTO   [dbo].[Temp-2022-03-28-Patient_Confinement-Added]
FROM   db_waterworksv1_live_temp_20220328_113414.dbo.vPatient_Confinement patientConfinement
       LEFT join db_waterworksv1_live_temp_20220328_113414.dbo.tPatient_Confinement_ItemsServices patientConfinementItemServices
              on patientConfinement.ID = patientConfinementItemServices.ID_Patient_Confinement
       inner join [Temp-2022-03-28-AuditTrail] auditTrailCurrentObject
               on auditTrailCurrentObject.ID_CurrentObject = patientConfinement.ID
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on patientConfinement.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-03-28-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), patientConfinement.ID_Client)
WHERE  Description LIKE '%added%'

SELECT DISTINCT company.ID                                                   Main_ID_Company,
                company.Name                                                 Name_Company,
                'Temp-2022-03-28-' + company.Guid
                + '-Patient_Confinement-'
                + Convert(Varchar(MAX), patientConfinement.ID)               temp_ID_Patient_Confinement,
                'Temp-2022-03-28-' + company.Guid
                + '-Patient_Confinement_Patient-'
                + Convert(Varchar(MAX), patientConfinementPatient.ID)        temp_ID_Patient_Confinement_Patient,
                ISNULL(mainPatient.ID, patientConfinementPatient.ID_Patient) Main_ID_Patient,
                patientConfinementPatient.*
INTO   [dbo].[Temp-2022-03-28-Patient_Confinement_Patient-Added]
FROM   db_waterworksv1_live_temp_20220328_113414.dbo.vPatient_Confinement patientConfinement
       inner join db_waterworksv1_live_temp_20220328_113414.dbo.vPatient_Confinement_Patient patientConfinementPatient
               on patientConfinement.ID = patientConfinementPatient.ID_Patient_Confinement
       inner join [Temp-2022-03-28-AuditTrail] auditTrailCurrentObject
               on auditTrailCurrentObject.ID_CurrentObject = patientConfinement.ID
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on patientConfinement.ID_Company = company.ID
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-03-28-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), patientConfinementPatient.ID_Patient)
                 and mainPatient.ID_Company = patientConfinement.ID_Company
WHERE  Description LIKE '%added%'

INSERT @NotYetInserted_TempID_Patient_Confinement
SELECT temp_ID_Patient_Confinement
FROM   [Temp-2022-03-28-Patient_Confinement-Added]

DELETE FROM @NotYetInserted_TempID_Patient_Confinement
WHERE  TempID_Patient_Confinement IN (SELECT tempID
                                      FROM   tPatient_Confinement
                                      where  tempID is not null)

INSERT @NotYetInserted_TempID_Patient_Confinement_Patient
SELECT temp_ID_Patient_Confinement_Patient
FROM   [Temp-2022-03-28-Patient_Confinement_Patient-Added]

DELETE FROM @NotYetInserted_TempID_Patient_Confinement_Patient
WHERE  TempID_Patient_Confinement_Patient IN (SELECT tempID
                                              FROM   tPatient_Confinement_Patient
                                              where  tempID is not null)

INSERT tPatient_Confinement
       (ID_Company,
        tempID,
        Date,
        DateDischarge,
        ID_Client,
        ID_FilingStatus,
        ID_DischargeBy,
        DateCanceled,
        ID_CanceledBy,
        BillingInvoice_ID_FilingStatus,
        SubTotal,
        TotalAmount,
        ID_Patient_SOAP,
        PatientNames,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT tempPaymentTransactionAdded.ID_Company,
       tempPaymentTransactionAdded.temp_ID_Patient_Confinement,
       tempPaymentTransactionAdded.Date,
       tempPaymentTransactionAdded.DateDischarge,
       tempPaymentTransactionAdded.Main_ID_Client,
       tempPaymentTransactionAdded.ID_FilingStatus,
       tempPaymentTransactionAdded.ID_DischargeBy,
       tempPaymentTransactionAdded.DateCanceled,
       tempPaymentTransactionAdded.ID_CanceledBy,
       tempPaymentTransactionAdded.BillingInvoice_ID_FilingStatus,
       tempPaymentTransactionAdded.SubTotal,
       tempPaymentTransactionAdded.TotalAmount,
       tempPaymentTransactionAdded.ID_Patient_SOAP,
       tempPaymentTransactionAdded.PatientNames,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported from VetCloudTemp March 28, 2022'
       + CHAR(13) + ''''
       + tempPaymentTransactionAdded.Comment + '''' Comment
FROM   dbo.[Temp-2022-03-28-Patient_Confinement-Added] tempPaymentTransactionAdded
       inner join @NotYetInserted_TempID_Patient_Confinement temp
               on tempPaymentTransactionAdded.temp_ID_Patient_Confinement = temp.TempID_Patient_Confinement

INSERT tPatient_Confinement_Patient
       (tempID,
        ID_Patient_Confinement,
        ID_Patient,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT Distinct Added_Patient_Confinement_Patient.temp_ID_Patient_Confinement_Patient,
                mainPatientConfinement.ID Main_ID_Patient_Confinement,
                Added_Patient_Confinement_Patient.Main_ID_Patient,
                1,
                GETDATE(),
                GETDATE(),
                1,
                1,
                'Imported from VetCloudTemp March 28, 2022'
FROm   [dbo].[Temp-2022-03-28-Patient_Confinement_Patient-Added] Added_Patient_Confinement_Patient
       inner join tPatient_Confinement mainPatientConfinement
               on mainPatientConfinement.tempID = Added_Patient_Confinement_Patient.temp_ID_Patient_Confinement
       inner join @NotYetInserted_TempID_Patient_Confinement_Patient temp
               on Added_Patient_Confinement_Patient.temp_ID_Patient_Confinement_Patient = temp.TempID_Patient_Confinement_Patient 
