GO

GO

Create Or
alter PRoc temp_pInsertPatientWellness(@ID_Company                     int,
                                       @ID_Client                      int,
                                       @ID_Patient                     INT,
                                       @AttendingPhysician_ID_Employee INT)
as
  begin
      DECLARE @ID_Patient_Wellness INT = 0

      INSERT INTO [dbo].[tPatient_Wellness]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Date],
                   [ID_Client],
                   [ID_Patient],
                   [AttendingPhysician_ID_Employee],
                   ID_FilingStatus)
      VALUES      (1,
                   @ID_Company,
                   GEtdate(),
                   GETDate(),
                   1,
                   1,
                   GETDATE(),
                   @ID_Client,
                   @ID_Patient,
                   @AttendingPhysician_ID_Employee,
                   1)

      SET @ID_Patient_Wellness = @@IDENTITY

      INSERT INTO [dbo].[tPatient_Wellness_Schedule]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   [Date])
      VALUES      (1,
                   @ID_Company,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ID_Patient_Wellness,
                   '2023-03-17')
  END

GO

GO

DECLARE @Source_Guid_Company VARCHAR(MAX) ='2C824598-B78D-4EE7-AB61-C85AA337D8CC'
DECLARE @ContactNumber VARCHAR(MAX) =''
DEClare @ID_Company int = 275
DECLARE db_cursor CURSOR FOR
  select Distinct ContactNumber
  FROM   dbo.fGetClientMobileNumberByCompany(@Source_Guid_Company)

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ContactNumber

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Client INt = 0
      DECLARE @ID_Patient INt = 0

      INSERT tClient
             (Name,
              IsActive,
              ID_CreatedBy,
              ID_LastModifiedBy,
              DateCreated,
              DateModified,
              ID_Company,
              ContactNumber)
      VALUES(@ContactNumber,
             1,
             1,
             1,
             GETDAte(),
             GETDATE(),
             @ID_Company,
             @ContactNumber)

      SET @ID_Client = @@IDENTITY

      INSERT tPatient
             (Name,
              IsActive,
              ID_CreatedBy,
              ID_LastModifiedBy,
              DateCreated,
              DateModified,
              ID_Company,
              ID_Client)
      VALUES(@ContactNumber,
             1,
             1,
             1,
             GETDAte(),
             GETDATE(),
             @ID_Company,
             @ID_Client)

      SET @ID_Patient = @@IDENTITY

      exec temp_pInsertPatientWellness
        @ID_Company,
        @ID_Client,
        @ID_Patient,
        NULL

      FETCH NEXT FROM db_cursor INTO @ContactNumber
  END

CLOSE db_cursor

DEALLOCATE db_cursor

exec pGetForSendSOAPPlan_OneDayBeforeAppointment 
