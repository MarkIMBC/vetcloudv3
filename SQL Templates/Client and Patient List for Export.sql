DEclare @GUID_Company VARCHAR(MAX) = '804E6163-18D4-484D-8981-9EB2E1910925'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   vCompanyActive
WHERE  Guid = @GUID_Company

/*Services*/
SELECT @Name_Company                 Clinic,
       ISNULL(Code, '')              Code,
       Name,
       ISNULL(Name_ItemCategory, '') Name_ItemCategory,
       ISNULL(UnitCost, 0)           UnitCost,
       ISNULL(UnitPrice, 0)          UnitPrice,
       ISNULL(Comment, '')           Comment
FROM   vItem
WHERE  ID_Company = @ID_Company
       AND IsActive = 1
       AND ID_ItemType = 1

SELECT c.Name                                Clinic,
       ISNULL(client.Code, '')               Code,
       LTRIM(RTRIM(ISNULL(client.Name, ''))) Name,
       ISNULL(client.Address, '')            Address,
       ISNULL(client.Email, '')              Email,
       ISNULL(client.ContactNumber, '')      ContactNumber,
       ISNULL(client.ContactNumber2, '')     ContactNumber2
FROM   vClient client
       inner join vCompanyActive c
               on client.ID_Company = c.ID
WHERE  ID_Company = @ID_Company
       AND client.IsActive = 1
ORDER  BY LTRIM(RTRIM(client.Name))

SELECT c.Name                                                 Clinic,
       Client.Code                                            ClientCode,
       LTRIM(RTRIM(ISNULL(patient.Name_Client, '')))          Client,
       CASE
         WHEN patient.Old_patient_id IS NOT NULL THEN '(from Legacy)'
         ELSE ISNULL(patient.Code, '')
       END                                                    PatientCode,
       patient.Name                                           [Pet Name],
       ISNULL(patient.Species, '')                            [Breed - Species],
       ISNULL(patient.Name_Gender, '')                        Gender,
       ISNULL(FORMAT(patient.DateBirth, 'yyyy-MM-dd'), '')    DateBirth,
       CASE
         WHEN ISNULL(patient.IsNeutered, 0) = 1 THEN 'YES'
         ELSE 'NO'
       END                                                    Neutered,
       CASE
         WHEN ISNULL(patient.IsDeceased, 0) = 1 THEN 'YES'
         ELSE 'NO'
       END                                                    Deceased,
       ISNULL(FORMAT(patient.DateDeceased, 'yyyy-MM-dd'), '') DateDeceased,
       ISNULL(patient.AnimalWellness, '')                     AnimalWellness
FROM   vPatient patient
       INNER JOIN TClient client
               on client.ID = patient.ID_Client
       inner join vCompanyActive c
               on client.ID_Company = c.ID
WHERE  patient.ID_Company = @ID_Company
       AND patient.IsActive = 1
ORDER  BY LTRIM(RTRIM(Name_Client)),
          patient.Name

SELECT c.Name,
       _soap.Date,
       _soap. Code,
       Name_SOAPType,
       AttendingPhysician_Name_Employee AttendingPersonel,
       Name_Client,
       Name_Patient,
       History,
       ClinicalExamination,
       Interpretation,
       Diagnosis,
       Treatment,
       ClientCommunication,
       Prescription,
       Name_FilingStatus
FROM   vPatient_SOAP_ListView _soap
       inner join vCompanyActive c
               on _soap.ID_Company = c.ID
WHERE  _soap.ID_Company = @ID_Company
Order  by _soap.ID DESC 
