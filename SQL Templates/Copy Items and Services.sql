DEclare @SOurce_GUID_Company VARCHAR(MAX) = '4041DA5C-942F-4207-A222-65BBAF1EEB6B'
DEclare @Destination_GUID_Company VARCHAR(MAX) = '34F985EF-7091-43DB-9FBE-3131FF1FDE7A'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @SOurce_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Source Company does not exist.', 1;
  END

SELECT Name Source_Company,
       *
FROm   tCompany
WHERE  GUID = @SOurce_GUID_Company

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @Destination_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Destination Company does not exist.', 1;
  END

SELECT Name Destination_Company,
       *
FROm   tCompany
WHERE  GUID = @Destination_GUID_Company

DECLARE @Source_ID_Company INT
DECLARE @Destination_ID_Company INT
DECLARE @Source_Name_Company VARCHAR(MAX)
DECLARE @Destination_Name_Company VARCHAR(MAX)

SELECt @Source_ID_Company = ID,
       @Source_Name_Company = Name
FROm   tCompany
WHERE  Guid = @SOurce_GUID_Company

SELECt @Destination_ID_Company = ID,
       @Destination_Name_Company = Name
FROm   tCompany
WHERE  Guid = @Destination_GUID_Company

----------------------------------------------------------------------------------
INSERT INTO [dbo].[tItem]
            (
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [MinInventoryCount],
             [MaxInventoryCount],
             [UnitCost],
             [UnitPrice],
             [CurrentInventoryCount],
             [Old_item_id],
             [Old_procedure_id],
             [OtherInfo_DateExpiration],
             [ID_InventoryStatus],
             [BarCode],
             [CustomCode],
             [_tempSupplier],
             [tempID],
             [SKUCode],
             [SKU],
             [UnitName],
             [StockNumber],
             [StockLocation],
             [ProjectName],
             [PONumber],
             [DateDelivered])
SELECT 
       [Name],
       [IsActive],
       @Destination_ID_Company,
       'From-' + @SOurce_GUID_Company + '-' + 'Item-'
       + CONVERT(varchar(MAX), ID),
       [DateCreated],
       [DateModified],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [ID_ItemType],
       [ID_ItemCategory],
       [MinInventoryCount],
       [MaxInventoryCount],
       [UnitCost],
       [UnitPrice],
       [CurrentInventoryCount],
       [Old_item_id],
       [Old_procedure_id],
       [OtherInfo_DateExpiration],
       [ID_InventoryStatus],
       [BarCode],
       [CustomCode],
       [_tempSupplier],
       CONVERT(VARCHAR(MAX), NEWID()),
       [SKUCode],
       [SKU],
       [UnitName],
       [StockNumber],
       [StockLocation],
       [ProjectName],
       [PONumber],
       [DateDelivered]
FROM   tItem
WHERE  ID_Company = @Source_ID_Company
       AND IsActive = 1

GO 
exec pUpdateItemCurrentInventory