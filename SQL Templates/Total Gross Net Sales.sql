Declare @DateStart Date = '2022-01-01'
Declare @DateEnd Date= '2022-01-31'

select ID_Company,
       Name_Company,
       SUM(GrossSales)  TotalGrossSales, SUM(NetCost) TotalNetSales
FROm   vzSalesIncomentReport sr
       inner join vCompanyActive cact
               on sr.ID_Company = cact.ID
WHERE  CONVERT(Date, Date_BillingInvoice) BETWEEN @DateStart AND @DateEnd 
GROUP BY ID_Company,
       Name_Company

