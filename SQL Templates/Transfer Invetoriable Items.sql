GO

IF EXISTS(SELECT [name]
          FROM   tempdb.sys.tables
          WHERE  [name] like '#TransferInventoriableService%')
  BEGIN
      DROP TABLE tempdb.dbo.#TransferInventoriableService
  END;

GO

DEclare @SOurce_GUID_Company VARCHAR(MAX) = 'DF396BC2-5751-4B1C-9393-D39B4F5CB07D'
DEclare @Destination_GUID_Company VARCHAR(MAX) = '704E0F51-F417-4599-83D5-182235C75158'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @SOurce_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Source Company does not exist.', 1;
  END

SELECT Name Source_Company,
       *
FROm   tCompany
WHERE  GUID = @SOurce_GUID_Company

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @Destination_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Destination Company does not exist.', 1;
  END

SELECT Name Destination_Company,
       *
FROm   tCompany
WHERE  GUID = @Destination_GUID_Company

----------------------------------------------------------------
DECLARE @Source_ID_Company INT
DECLARE @Destination_ID_Company INT
DECLARE @Source_Name_Company VARCHAR(MAX)
DECLARE @Destination_Name_Company VARCHAR(MAX)
DECLARE @Service_ID_ItemType INT = 1
DECLARE @Inventoriable_ID_ItemType INT = 2
DECLARE @ItemTypes typIntList

INsert @ItemTypes
VALUES(@Service_ID_ItemType),
       (@Inventoriable_ID_ItemType)

SELECt @Source_ID_Company = ID,
       @Source_Name_Company = Name
FROm   tCompany
WHERE  Guid = @SOurce_GUID_Company

SELECt @Destination_ID_Company = ID,
       @Destination_Name_Company = Name
FROm   tCompany
WHERE  Guid = @Destination_GUID_Company

SELECT DISTINCT item.[Code],
                item.[Name],
                item.[IsActive],
                @Destination_ID_Company Destination_ID_Company,
                + 'Imported from ' + @Source_Name_Company
                + ' as of '
                + FORMAT(GETDATE(), 'MMMM dd yyyy hh:mm tt')
                + CASE
                    WHEN LEN(ISNULL(item.Comment, '')) > 0 THEN CHAR(13) + item.Comment
                    ELSE ''
                  END                   Comment,
                GETDATE()               DateCreated,
                GETDATE()               DateModified,
                1                       [ID_CreatedBy],
                1                       [ID_LastModifiedBy],
                item.[ID_ItemType],
                item.[ID_ItemCategory],
                item.[MinInventoryCount],
                item.[MaxInventoryCount],
                item.[UnitCost],
                item.[UnitPrice],
                item.[CurrentInventoryCount],
                item.[Old_item_id],
                item.[Old_procedure_id],
                item.[OtherInfo_DateExpiration],
                item.[ID_InventoryStatus]
INTO   #TransferInventoriableService
FROM   [dbo].[tItem] item
       INNER JOIN @ItemTypes itemType
               on item.ID_ItemType = itemType.ID
WHERE  ID_Company = @Source_ID_Company
       and IsActive = 1;

INSERT tItem
       (Name,
        IsActive,
        ID_Company,
        Comment,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        ID_ItemType,
        ID_ItemCategory,
        MinInventoryCount,
        MaxInventoryCount,
        UnitCost,
        UnitPrice,
        CurrentInventoryCount,
        Old_item_id,
        Old_procedure_id,
        OtherInfo_DateExpiration,
        ID_InventoryStatus)
SELECT Name,
       IsActive,
       Destination_ID_Company,
       Comment,
       DateCreated,
       DateModified,
       ID_CreatedBy,
       ID_LastModifiedBy,
       ID_ItemType,
       ID_ItemCategory,
       MinInventoryCount,
       MaxInventoryCount,
       UnitCost,
       UnitPrice,
       CurrentInventoryCount,
       Old_item_id,
       Old_procedure_id,
       OtherInfo_DateExpiration,
       ID_InventoryStatus
FROm   #TransferInventoriableService

SELECT type.Name Type,
       Count(*)  Count
FROm   #TransferInventoriableService import
       LEFT JOIN tItemType type
              on type.ID = import.ID_ItemType
GROUP  BY type.Name

SELECT *
FROM   tItem
WHERE  ID_Company = @Destination_ID_Company

GO

IF EXISTS(SELECT [name]
          FROM   tempdb.sys.tables
          WHERE  [name] like '#TransferInventoriableService%')
  BEGIN
      DROP TABLE tempdb.dbo.#TransferInventoriableService
  END;

GO
--exec pUpdateItemCurrentInventory 
