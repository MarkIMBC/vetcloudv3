GO

USE SystemManagement

GO

DECLARE @Start DATE = '2024-10-01'
DECLARE @END DATE = GETDATE()
DECLARE @createTempTableCompanyUsageSQL VARCHAR(MAX) = ''

SET @createTempTableCompanyUsageSQL = dbo.fGetCreateTempTableCompanyUsageSQL(@Start, @END);

IF OBJECT_ID('tempdb..##tCompanyPivotUsage') IS NOT NULL
  DROP TABLE ##tCompanyPivotUsage;

EXEC (@createTempTableCompanyUsageSQL);

DECLARE @name VARCHAR(MAX) -- database name     
DECLARE @dateStart DateTime
DECLARE @dateEnd DateTime
DECLARE @table TABLE
  (
     ID           INT,
     Name         VARCHAR(MAX),
     IsActive     BIT,
     DateCreated  DateTime,
     Guid         VARCHAR(MAX),
     DatabaseName VARCHAR(MAX)
  )
DECLARE db_cursorppGetCompanyActive_VetCloudv3 CURSOR FOR
  SELECT name
  FROM   MASTER.dbo.sysdatabases
  WHERE  ( name LIKE 'db_vetcloudv3_server_'
            OR name LIKE 'db_vetcloudv3_%' ) AND name NOT IN ('db_vetcloudv3_test')

OPEN db_cursorppGetCompanyActive_VetCloudv3

FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dateStart = GETDATE();

      BEGIN TRY;
          DECLARE @sql NVARCHAR(MAX) = 'exec ' + @name
            + '.dbo.pGenerateCompanyUsageByDateCovered '''
            + FORMAT(@Start, 'yyyy-MM-dd') + ''', '''
            + FORMAT(@END, 'yyyy-MM-dd') + '''';

          INSERT ##tCompanyPivotUsage
          exec(@sql);

          SET @dateEnd = GETDATE();
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SET @dateEnd = GETDATE();
          SELECT @msg + ' in ' + @name
      END CATCH

      FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name
  END

CLOSE db_cursorppGetCompanyActive_VetCloudv3

DEALLOCATE db_cursorppGetCompanyActive_VetCloudv3

SELECT *
FROM   ##tCompanyPivotUsage

IF OBJECT_ID('tempdb..##tCompanyPivotUsage') IS NOT NULL
  DROP TABLE ##tCompanyPivotUsage 
