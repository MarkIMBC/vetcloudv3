DECLARE @ID_Patient_Source INT = 442636
DECLARE @ID_Patient_Destination INT = 345655
DECLARE @ID_Company INT = 168
DECLARE @ID_Client_Destination INT = 0
DECLARE @Name_Company VARCHAR(MAX) = ''
DECLARE @msg VARCHAR(MAX) = ''

SELECT @Name_Company = Name
FROM   tCompany
WHERE  ID = @ID_Company

IF(SELECT COUNT(*)
   FROM   tPatient
   Where  ID_Company = @ID_Company
          AND ID = @ID_Patient_Source) = 0
  BEGIN;
      SET @msg = 'Patient Source not exist in '
                 + @Name_Company;

      THROW 51000, @msg, 1;
  END

IF(SELECT COUNT(*)
   FROM   tPatient
   Where  ID_Company = @ID_Company
          AND ID = @ID_Patient_Destination) = 0
  BEGIN;
      SET @msg = 'Patient Destination not exist in '
                 + @Name_Company;

      THROW 51000, @msg, 1;
  END

SELECT @ID_Client_Destination = ID_Client
FROM   tPatient
WHERE  ID = @ID_Patient_Destination

UPDATE tPatient_SOAP
SET    ID_Patient = @ID_Patient_Destination
WHERE  ID_Patient = @ID_Patient_Source

UPDATE tVeterinaryHealthCertificate
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination
WHERE  ID_Patient = @ID_Patient_Source

UPDATE tPatient_Wellness
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination
WHERE  ID_Patient = @ID_Patient_Source

UPDATE tPatient_Vaccination
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination
WHERE  ID_Patient = @ID_Patient_Source

GO