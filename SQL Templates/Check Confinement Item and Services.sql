DECLARE @ID_Patient_Confinement INT = 10365
DECLARE @ID_BillingInvoice INT

select Code,
       DateCreated
FROm   vPatient_Confinement
where  iD = @ID_Patient_Confinement

SELECT @ID_BillingInvoice = ID
FROM   vBillingInvoice
where  ID_Patient_Confinement = @ID_Patient_Confinement
       AND ID_FilingStatus NOT IN ( 4 )

SELECT Name_Item,
       Quantity
FROM   vPatient_Confinement_ItemsServices confineOItems
WHERE  confineOItems.ID_Patient_Confinement = @ID_Patient_Confinement

SELECT tbl.*
FROM   (SELECT Name_Item,
               Quantity,
               IsCharged
        FROM   vPatient_SOAP _soap
               inner join vPatient_SOAP_Prescription prescrition
                       on _soap.ID = prescrition.ID_Patient_SOAP
        WHERE  _soap.ID_Patient_Confinement = @ID_Patient_Confinement
               and ID_FilingStatus NOT IN ( 4 )
        UNION ALL
        SELECT Name_Item,
               Quantity,
               1 IsCharged
        FROM   vPatient_SOAP _soap
               inner join vPatient_SOAP_Treatment prescrition
                       on _soap.ID = prescrition.ID_Patient_SOAP
        WHERE  _soap.ID_Patient_Confinement = @ID_Patient_Confinement
               and ID_FilingStatus NOT IN ( 4 )) tbl

SELECT ID,
       Name_Item,
       Quantity,
       ID_Patient_Confinement_ItemsServices,
       DateCreated
FROM   vBillingInvoice_Detail
WHERE  ID_BillingInvoice = @ID_BillingInvoice
Order  by Name_Item 
