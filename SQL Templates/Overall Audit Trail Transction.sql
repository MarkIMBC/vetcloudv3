DECLARE @IDsCompanyString VARCHAR(MAX) = '1'
DECLARE @IDs_Company TypIntList

IF( len(Trim(@IDsCompanyString)) > 0 )
  BEGIN
      INSERT @IDs_Company
      SELECT Part
      FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
  END
ELSE
  BEGIN
      INSERT @IDs_Company
      SELECT ID_Company
      FROM   tCompany_SMSSetting cSMSSetting
             INNER JOIN tCompany c
                     on cSMSSetting.ID_Company = c.ID
      WHERE  IsNull(cSMSSetting.IsActive, 0) = 1
             and c.IsActive = 1
  END

DECLARE @TableAuditTrail as TAble
  (
     ID_Company   INT,
     Name_User    VARCHAR(MAX),
     Description  VARCHAR(MAX),
     DateModified Datetime
  )
DECLARE @Table as TAble
  (
     ID_Company   INT,
     DateModified Datetime
  )

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.Name_CreatedBy,
       head.Code + ' has been created.',
       head.DateCreated
FROm   vPatient_SOAP head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.Name_LastModifiedBy,
       head.Code + ' has been modified.',
       head.DateModified
FROm   vPatient_SOAP head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.Name_ApprovedBy,
       head.Code + ' has been approved.',
       head.DateApproved
FROm   vPatient_SOAP head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateApproved IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.Name_CanceledBy,
       head.Code + ' has been canceled.',
       head.DateCanceled
FROm   vPatient_SOAP head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCanceled IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.Name_LastModifiedBy,
       head.Code + ' has been doned.',
       head.DateDone
FROm   vPatient_SOAP head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateDone IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CreatedBy,
       head.Name + ' has been created.',
       head.DateCreated
FROm   vPatient head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.LastModifiedBy,
       head.Name + ' has been modified.',
       head.DateModified
FROm   vPatient head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL


INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CreatedBy,
       head.Name + ' has been created.',
       head.DateCreated
FROm   vClient head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.LastModifiedBy,
       head.Name + ' has been modified.',
       head.DateModified
FROm   vClient head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CreatedBy_Name_User,
       head.Code + ' has been created.',
       head.DateCreated
FROm   vBillingInvoice head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.LastModifiedBy_Name_User,
       head.Code + ' has been modified.',
       head.DateModified
FROm   vBillingInvoice head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.ApprovedBy_Name_User,
       head.Code + ' has been approved.',
       head.DateApproved
FROm   vBillingInvoice head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateApproved IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CanceledBy_Name_User,
       head.Code + ' has been cancelled.',
       head.DateCanceled
FROm   vBillingInvoice head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCanceled IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CreatedBy,
       head.Code + ' has been created.',
       head.DateCreated
FROm   vPatient_Confinement head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.LastModifiedBy,
       head.Code + ' has been modified.',
       head.DateModified
FROm   vPatient_Confinement head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.DischargeBy,
       head.Code + ' has been discharged.',
       head.DateDischarge
FROm   vPatient_Confinement head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateDischarge IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CanceledBy,
       head.Code + ' has been canceled.',
       head.DateCanceled
FROm   vPatient_Confinement head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCanceled IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CreatedBy,
       head.Code + ' has been created.',
       head.DateCreated
FROm   vPatient_Wellness head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCreated IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.LastModifiedBy,
       head.Code + ' has been modified.',
       head.DateModified
FROm   vPatient_Wellness head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateModified IS NOT NULL

INSERT @TableAuditTrail
SELECT head.ID_Company,
       head.CanceledBy,
       head.Code + ' has been canceled.',
       head.DateCanceled
FROm   vPatient_Wellness head
       inner join vCompanyActive c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id
WHERE  head.DateCanceled IS NOT NULL








INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tPurchaseOrder head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateApproved, head.DateCreated)
FROm   tPurchaseOrder head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateCanceled, head.DateCreated)
FROm   tPurchaseOrder head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateCancelled, head.DateCreated)
FROm   tPurchaseOrder head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCreated
FROm   tReceivingReport head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateModified
FROm   tReceivingReport head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateApproved
FROm   tReceivingReport head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCanceled
FROm   tReceivingReport head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tPaymentTransaction head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateApproved, head.DateCreated)
FROm   tPaymentTransaction head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateCanceled, head.DateCreated)
FROm   tPaymentTransaction head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tItem head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tPatientAppointment head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tSupplier head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT u.ID_Company,
       head.Date
FROm   tAuditTrail head
       inner join vUser u
               on u.ID = head.ID_User
       inner join tCompany c
               on u.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCreated
FROm   tInventoryTrail head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCreated
FROm   tClient_CreditLogs head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCreated
FROm   tPayable head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateModified
FROm   tPayable head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

INSERT @Table
SELECT head.ID_Company,
       head.DateCreated
FROm   tPayablePayment head
       inner join tCompany c
               on head.ID_Company = c.ID
       Inner join @IDs_Company ids
               on ids.ID = c.id

SELECT *
FROm   @TableAuditTrail ORder by DateModified

SELECT c.Name,
       Max(t.DateModified) DateModified
FROm   @Table t
       INNER JOIN vCompanyActive c
               on t.ID_Company = c.ID
GROUP  by Name
ORDER  BY Max(t.DateModified) DESC 
