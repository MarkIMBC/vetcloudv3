IF OBJECT_ID(N'Temp-2022-03-28-tPaymentTransaction', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-tPaymentTransaction];

GO

exec _pAddModelProperty
  'tPaymentTransaction',
  'tempID',
  1

exec _pAddModelProperty
  'tPaymentTransaction',
  'tempID',
  1

Declare @NotYetInserted_TempID TABLE
  (
     tempID VARCHAR(MAX)
  )

SELECT company.Name                                                             Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-PaymentTransaction-'
       + Convert(Varchar(MAX), paymentTransaction.ID)                           tempPaymentTransactionID,
       ISNULL(mainbillingInvoice.ID, paymentTransaction.ID_BillingInvoice)      Main_ID_BillingInvoice,
       ConVERT(VARCHAR(MAX), ISNULL( mainbillingInvoice.ID, 0) )
       + '||'
       + ConVERT(VARCHAR(MAX), ISNULL(paymentTransaction.ID_BillingInvoice, 0)) compareID_BillingInvoice,
       paymentTransaction.*
INTO   [dbo].[Temp-2022-03-28-tPaymentTransaction]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vPaymentTransaction paymentTransaction
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on paymentTransaction.ID_Company = company.ID
       LEFT JOIN tBillingInvoice mainbillingInvoice
              on mainbillingInvoice.tempID = 'Temp-2022-03-28-' + company.Guid
                                             + '-BillingInvoice-'
                                             + Convert(Varchar(MAX), paymentTransaction.ID_BillingInvoice )
                 AND mainbillingInvoice.ID_Company = company.ID
where  paymentTransaction.DateCreated > '2022-03-28 10:00:00'
       and paymentTransaction.ID_FilingStatus = 3

	   
INSERT @NotYetInserted_TempID
SELECT temptPaymentTransaction.tempPaymentTransactionID
FROm   [dbo].[Temp-2022-03-28-tPaymentTransaction] temptPaymentTransaction

DELETE FROM @NotYetInserted_TempID
WHERE  tempID IN (SELECT tempID
                  FROM   tPaymentTransaction where ID_FilingStatus = 3)

INSERT tPaymentTransaction
       (ID_Company,
        ID_BillingInvoice,
        Date,
        ID_TaxScheme,
        GrossAmount,
        VatAmount,
        NetAmount,
        ID_FilingStatus,
        ID_PaymentMethod,
        CashAmount,
        CheckAmount,
        CheckNumber,
        PayableAmount,
        PaymentAmount,
        ChangeAmount,
        DateApproved,
        ID_ApprovedBy,
        DateCanceled,
        ID_CanceledBy,
        CardAmount,
        GCashAmount,
        ID_CardType,
        CardHolderName,
        ReferenceTransactionNumber,
        CardNumber,
        RemainingAmount,
        CreditAmount,
        tempID,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy, Comment)
SELECT tempPaymenTransaction.ID_Company,
       tempPaymenTransaction.Main_ID_BillingInvoice,
       tempPaymenTransaction.Date,
       tempPaymenTransaction.ID_TaxScheme,
       tempPaymenTransaction.GrossAmount,
       tempPaymenTransaction.VatAmount,
       tempPaymenTransaction.NetAmount,
       tempPaymenTransaction.ID_FilingStatus,
       tempPaymenTransaction.ID_PaymentMethod,
       tempPaymenTransaction.CashAmount,
       tempPaymenTransaction.CheckAmount,
       tempPaymenTransaction.CheckNumber,
       tempPaymenTransaction.PayableAmount,
       tempPaymenTransaction.PaymentAmount,
       tempPaymenTransaction.ChangeAmount,
       tempPaymenTransaction.DateApproved,
       tempPaymenTransaction.ID_ApprovedBy,
       tempPaymenTransaction.DateCanceled,
       tempPaymenTransaction.ID_CanceledBy,
       tempPaymenTransaction.CardAmount,
       tempPaymenTransaction.GCashAmount,
       tempPaymenTransaction.ID_CardType,
       tempPaymenTransaction.CardHolderName,
       tempPaymenTransaction.ReferenceTransactionNumber,
       tempPaymenTransaction.CardNumber,
       tempPaymenTransaction.RemainingAmount,
       tempPaymenTransaction.CreditAmount,
       tempPaymenTransaction.tempPaymentTransactionID,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported from VetCloudTemp March 28, 2022'
       + CHAR(13) 
       + '''' + tempPaymenTransaction.Comment  + '''' Comment
FROm   [dbo].[Temp-2022-03-28-tPaymentTransaction] tempPaymenTransaction
       inner join @NotYetInserted_TempID unInsertedTempPaymentTransaction
               on tempPaymenTransaction.tempPaymentTransactionID = unInsertedTempPaymentTransaction.tempID 
GO

GO

CReate OR
ALTER PROC _tempGeneratePaymentTransaction(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_PaymentTransaction TABLE
        (
           RowID      INT,
           ID_PaymentTransaction INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_PaymentTransaction
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tPaymentTransaction
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_PaymentTransaction

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_PaymentTransaction INT = 0

            SELECT @ID_PaymentTransaction = ID_PaymentTransaction
            FROM   @IDs_PaymentTransaction
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_PaymentTransaction]
              @ID_PaymentTransaction,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for PaymentTransaction IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany order by Name

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGeneratePaymentTransaction
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGeneratePaymentTransaction

GO 


SELECT * FROM tFilingStatus 