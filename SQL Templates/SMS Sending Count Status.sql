SELECT ITxtMoStatus.ID, ITxtMoStatus.Name, COUNT(*)  SOAPSMSCount
FROM   tPatient_SOAP_SMSStatus smsStatus
       INNER JOIN tITextMoAPISMSStatus ITxtMoStatus
               ON smsStatus.iTextMo_Status = ITxtMoStatus.Code
WHERE  CONVERT(DATE, smsStatus.DateCreated) = '2021-12-18' 
GROUP BY ITxtMoStatus.ID, ITxtMoStatus.Name

SELECT ITxtMoStatus.ID, ITxtMoStatus.Name, COUNT(*)  WellnessSMSCount
FROM   tPatient_Wellness_Schedule_SMSStatus smsStatus
       INNER JOIN tITextMoAPISMSStatus ITxtMoStatus
               ON smsStatus.iTextMo_Status = ITxtMoStatus.Code
WHERE  CONVERT(DATE, smsStatus.DateCreated) = '2021-12-18' 
GROUP BY ITxtMoStatus.ID, ITxtMoStatus.Name
