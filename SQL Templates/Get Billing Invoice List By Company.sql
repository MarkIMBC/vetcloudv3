DEclare @GUID_Company VARCHAR(MAX) = '77F3B4CF-6521-4CCF-AA71-2A1FC83EFF81'

----------------------------------------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

--------------------------------------------------------------------------------------------------
/**************************************** Code Here *********************************************/
--------------------------------------------------------------------------------------------------
SELECT @Name_Company                                       Name_Company,
       bihed.Code,
       bihed.Date,
       ISNULL(bihed.Name_Client, bihed.WalkInCustomerName) Client,
       ISNULL(bihed.PatientNames, '')                      Patient,
       ISNULL(bihed.AttendingPhysician_Name_Employee, '')  AttendingPhysician_Name_Employee,
       bihed.Status,
       bihed.TotalAmount,
       bihed.RemainingAmount,
       CASE
         WHEN bihed.DateApproved IS NULL THEN ''
         ELSE FORMAT(bihed.DateApproved, 'yyyy-MM-dd HH:mm:ss')
       END                                                 DateApproved,
       ISNULL(bihed.ApprovedBy_Name_User, '')              ApprovedBy_Name_User,
       CASE
         WHEN bihed.DateCanceled IS NULL THEN ''
         ELSE FORMAT(bihed.DateCanceled, 'yyyy-MM-dd HH:mm:ss')
       END                                                 DateCanceled,
       ISNULL(bihed.CanceledBy_Name_User, '')              CanceledBy_Name_User
FROM   vBillingInvoice bihed
where  bihed.ID_Company = @ID_Company
       AND CONVERT(Date, bihed.Date) BETWEEN '2023-04-23' AND '2023-04-24'
ORDER  BY bihed.Code 
