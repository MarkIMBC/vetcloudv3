DECLARE @ID_Patient_Confinement INT = 6011
DECLARE @IDs_Patient_SOAP_Treatment typIntList
DECLARE @IDs_Patient_SOAP_Prescription typIntList

INSERT @IDs_Patient_SOAP_Treatment
SELECT ID_Patient_SOAP_Treatment
FROm   vPatient_Confinement_ItemsServices
WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
       AND ID_Patient_SOAP_Treatment IS NOT NULL

INSERT @IDs_Patient_SOAP_Prescription
SELECT ID_Patient_SOAP_Prescription
FROm   vPatient_Confinement_ItemsServices
WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
       AND ID_Patient_SOAP_Prescription IS NOT NULL

Update tPatient_SOAP_Prescription SET UnitPrice = item.UnitPrice
FROM   tPatient_SOAP_Prescription soapTreatment
       INNER JOIN @IDs_Patient_SOAP_Prescription ids
               on soapTreatment.ID = ids.ID
       INNER JOIN tItem item
               on soapTreatment.ID_Item = item.ID
WHERE  ISNULL(soapTreatment.UnitPrice, 0) = 0 

Update tPatient_SOAP_Prescription SET UnitCost = item.UnitCost
FROM   tPatient_SOAP_Prescription soapPrescription
       INNER JOIN @IDs_Patient_SOAP_Prescription ids
               on soapPrescription.ID = ids.ID
       INNER JOIN tItem item
               on soapPrescription.ID_Item = item.ID
WHERE  ISNULL(soapPrescription.UnitCost, 0) = 0 

Update tPatient_SOAP_Treatment SET UnitPrice = item.UnitPrice
FROM   tPatient_SOAP_Treatment soapTreatment
       INNER JOIN @IDs_Patient_SOAP_Treatment ids
               on soapTreatment.ID = ids.ID
       INNER JOIN tItem item
               on soapTreatment.ID_Item = item.ID
WHERE  ISNULL(soapTreatment.UnitPrice, 0) = 0 

Update tPatient_SOAP_Treatment SET UnitCost = item.UnitCost
FROM   tPatient_SOAP_Treatment soapTreatment
       INNER JOIN @IDs_Patient_SOAP_Treatment ids
               on soapTreatment.ID = ids.ID
       INNER JOIN tItem item
               on soapTreatment.ID_Item = item.ID
WHERE  ISNULL(soapTreatment.UnitCost, 0) = 0 

Update vPatient_Confinement_ItemsServices
SET    UnitPrice = item.UnitPrice
FROM   vPatient_Confinement_ItemsServices confItem
       INNER JOIN tItem item
               on confItem.ID_Item = item.ID
WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
       and ISNULL(confItem.UnitPrice, 0)  = 0 

Update vPatient_Confinement_ItemsServices
SET    UnitCost = item.UnitCost
FROM   vPatient_Confinement_ItemsServices confItem
       INNER JOIN tItem item
               on confItem.ID_Item = item.ID
WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
       and ISNULL(confItem.UnitCost, 0)  = 0 
