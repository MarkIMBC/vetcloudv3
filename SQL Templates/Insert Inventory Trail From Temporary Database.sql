IF COL_LENGTH('tInventoryTrail', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tInventoryTrail',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF COL_LENGTH('tEmployee', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tEmployee',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF OBJECT_ID(N'Temp-2022-03-28-AuditTrail-InventoryTrail-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-AuditTrail-InventoryTrail-Added]

GO

IF OBJECT_ID(N'Temp-2022-03-28-InventoryTrail', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-InventoryTrail]

GO

Declare @NotYetInserted_Temp_ID_InventoryTrail TABLE
  (
     Temp_ID_InventoryTrail VARCHAR(MAX)
  )

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [dbo].[Temp-2022-03-28-AuditTrail-InventoryTrail-Added]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel _model
              on _auditTrail.ID_Model = _model.Oid
WHERE  Date >= '2022-03-28 11:00:00'
       AND Description like '%added%'
       AND _model.TableName = 'tInventoryTrail'
Order  by Date

SELECT company.ID                                  Main_ID_Company,
       company.Guid                                Guid_ID_Company,
       company.Name                                Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-InventoryTrail-'
       + Convert(Varchar(MAX), InventoryTrail_.ID) Temp_ID_InventoryTrail,
       InventoryTrail_.*,
       billingInvoice.ID                           ID_BillingInvoice,
       billingInvoice.Code                         Code_BillingInvoice,
       ReceivingReport.ID                          ID_ReceivingReport,
       ReceivingReport.Code                        Code_ReceivingReport
INTO   [dbo].[Temp-2022-03-28-InventoryTrail]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vInventoryTrail InventoryTrail_
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on InventoryTrail_.ID_Company = company.ID
       --
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.[dbo].tBillingInvoice billingInvoice
              on InventoryTrail_.Code = billingInvoice.Code
                 and billingInvoice.ID_Company = company.ID
       --			  
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.[dbo].tReceivingReport ReceivingReport
              on InventoryTrail_.Code = ReceivingReport.Code
                 and ReceivingReport.ID_Company = company.ID
--
where  InventoryTrail_.DateCreated > '2022-03-28 10:00:00'
 
order  by InventoryTrail_.Code

INSERT @NotYetInserted_Temp_ID_InventoryTrail
SELECT tempInventoryTrail.Temp_ID_InventoryTrail
FROm   [dbo].[Temp-2022-03-28-InventoryTrail] tempInventoryTrail

DELETE FROM @NotYetInserted_Temp_ID_InventoryTrail
WHERE  Temp_ID_InventoryTrail IN (SELECT tempID
                                  FROM   tInventoryTrail)
SELECT * FROM @NotYetInserted_Temp_ID_InventoryTrail
INSERT tInventoryTrail
       (ID_Company,
        tempID,
        ID_Item,
        Code,
        Comment,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Quantity,
        UnitPrice,
        ID_FilingStatus,
        Date,
        DateExpired,
        BatchNo)
SELECT tempInventoryTrail.ID_Company,
       tempInventoryTrail.Temp_ID_InventoryTrail,
       ISNULL(mainItem.ID, mainItem2.ID) Main_ID_Item,
       CASE
         WHEN tempInventoryTrail.ID_ReceivingReport IS NOT NULL THEN ISNULL(mainReceivingReport.Code, mainReceivingReport2.Code)
         ELSE
           CASE
             WHEN tempInventoryTrail.ID_BillingInvoice IS NOT NULL THEN ISNULL(mainBillingInvoice.Code, mainBillingInvoice2.Code)
             ELSE tempInventoryTrail.Code
           END
       END                                   Main_Code_InventoryTrail,
       + 'Imported from VetCloudTemp March 28, 2022'
       + ' Reference Code ('
       + tempInventoryTrail.Code + ')'
       + CASE
           WHEN LEN(ISNULL(tempInventoryTrail.Comment, '')) > 0 THEN CHAR(13) + tempInventoryTrail.Comment
           ELSE ''
         END                                 Comment,
       tempInventoryTrail.IsActive,
       tempInventoryTrail.DateCreated,
       tempInventoryTrail.DateModified,
       tempInventoryTrail.ID_CreatedBy,
       tempInventoryTrail.ID_LastModifiedBy,
       tempInventoryTrail.Quantity,
       tempInventoryTrail.UnitPrice,
       tempInventoryTrail.ID_FilingStatus,
       tempInventoryTrail.Date,
       tempInventoryTrail.DateExpired,
       tempInventoryTrail.BatchNo
FROm   [dbo].[Temp-2022-03-28-InventoryTrail] tempInventoryTrail
       inner join @NotYetInserted_Temp_ID_InventoryTrail unInsertedTempInventoryTrail
               on tempInventoryTrail.Temp_ID_InventoryTrail = unInsertedTempInventoryTrail.Temp_ID_InventoryTrail
       --
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-03-28-'
                                   + tempInventoryTrail.Guid_ID_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempInventoryTrail.ID_Item)
                 and mainItem.ID_Company = tempInventoryTrail.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempInventoryTrail.ID_Item
                 and mainItem2.ID_Company = tempInventoryTrail.Main_ID_Company
       --
       LEFT JOIN tBillingInvoice mainBillingInvoice
              on mainBillingInvoice.tempID = 'Temp-2022-03-28-'
                                             + tempInventoryTrail.Guid_ID_Company
                                             + '-BillingInvoice-'
                                             + Convert(Varchar(MAX), tempInventoryTrail.ID_BillingInvoice)
                 and mainBillingInvoice.ID_Company = tempInventoryTrail.Main_ID_Company
       LEFT JOIN tBillingInvoice mainBillingInvoice2
              on mainBillingInvoice2.ID = tempInventoryTrail.ID_BillingInvoice
                 and mainBillingInvoice2.ID_Company = tempInventoryTrail.Main_ID_Company
       --
       LEFT JOIN tReceivingReport mainReceivingReport
              on mainReceivingReport.tempID = 'Temp-2022-03-28-'
                                              + tempInventoryTrail.Guid_ID_Company
                                              + '-ReceivingReport-'
                                              + Convert(Varchar(MAX), tempInventoryTrail.ID_ReceivingReport)
                 and mainReceivingReport.ID_Company = tempInventoryTrail.Main_ID_Company
       LEFT JOIN tReceivingReport mainReceivingReport2
              on mainReceivingReport2.ID = tempInventoryTrail.ID_ReceivingReport
                 and mainReceivingReport2.ID_Company = tempInventoryTrail.Main_ID_Company

GO

exec _pRefreshAllViews

GO

exec pUpdateItemCurrentInventory

SELECT *
FROm   [Temp-2022-03-28-InventoryTrail]
order  by Code

--SELECT * FROM vUser WHERE ID_Company = 32


SELECT * FROm vInventoryTrail WHERE ID_Item = 63309 order by Date DESC