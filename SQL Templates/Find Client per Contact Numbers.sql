DECLARE @ClientContactNumber TABLE
  (
     Name_Company   VARCHAR(MAX),
     Name_Client    VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX)
  )

INSERT @ClientContactNumber
SELECT Distinct *
FROM   (SELECT company.Name Name_Company,
               client.Name  Name_Client,
               client.ContactNumber,
               client.ContactNumber2
        FROM   db_waterworksv1_live_dev.dbo.tClient client
               inner join db_waterworksv1_live_dev.dbo.vCompanyActive company
                       on client.ID_Company = company.ID
        WHERE  client.IsActive = 1
        UNION ALL
        SELECT company.Name Name_Company,
               client.Name  Name_Client,
               client.ContactNumber,
               client.ContactNumber2
        FROM   db_waterworksv1_server1_dev.dbo.tClient client
               inner join db_waterworksv1_server1_dev.dbo.vCompanyActive company
                       on client.ID_Company = company.ID
        WHERE  client.IsActive = 1
        UNION ALL
        SELECT company.Name Name_Company,
               client.Name  Name_Client,
               client.ContactNumber,
               client.ContactNumber2
        FROM   db_waterworksv1_server2_dev.dbo.tClient client
               inner join db_waterworksv1_server2_dev.dbo.vCompanyActive company
                       on client.ID_Company = company.ID
        WHERE  client.IsActive = 1
        UNION ALL
        SELECT company.Name Name_Company,
               client.Name  Name_Client,
               client.ContactNumber,
               client.ContactNumber2
        FROM   db_waterworksv1_server3_dev.dbo.tClient client
               inner join db_waterworksv1_server3_dev.dbo.vCompanyActive company
                       on client.ID_Company = company.ID
        WHERE  client.IsActive = 1) tbl

--SELECT distinct [|],
--                [|__message_id],
--                [|__originator],
--                numbers.Name_Company,
--                numbers.Name_Client,
--                [|__message],
--                [|__timestamp]
--FROm   [Replies] replies
--       LEFT JOIn @ClientContactNumber numbers
--              on dbo.fGetMobileNumbers(numbers.ContactNumber, numbers.ContactNumber2) = replies.[|__originator]
--			  WHERE  LEN(dbo.fGetMobileNumbers(numbers.ContactNumber, numbers.ContactNumber2) ) > 0
--Order  by [|__message_id] ASC 
SELECT *
FROM   @ClientContactNumber
