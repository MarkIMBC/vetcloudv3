DECLARE @ContactNumber VARCHAR(MAX) = '09177140303'


SELECT Distinct * FROM (
SELECT company.Name Name_Company,
       client.Name Name_Client,
       client.ContactNumber,
       client.ContactNumber2
FROM   db_waterworksv1_server1.dbo.tClient client
       inner join db_waterworksv1_server1.dbo.vCompanyActive company
               on client.ID_Company = company.ID
WHERE  db_waterworksv1_server1.dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2) LIKE  '%' + @ContactNumber + '%'
UNION ALL
SELECT company.Name Name_Company,
       client.Name Name_Client,
       client.ContactNumber,
       client.ContactNumber2
FROM   db_waterworksv1_server2.dbo.tClient client
       inner join db_waterworksv1_server2.dbo.vCompanyActive company
               on client.ID_Company = company.ID
WHERE  db_waterworksv1_server1.dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2) LIKE '%' + @ContactNumber + '%'
UNION ALL
SELECT company.Name Name_Company,
       client.Name Name_Client,
       client.ContactNumber,
       client.ContactNumber2
FROM   db_waterworksv1_server3.dbo.tClient client
       inner join db_waterworksv1_server3.dbo.vCompanyActive company
               on client.ID_Company = company.ID
WHERE  db_waterworksv1_server1.dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2) LIKE '%' + @ContactNumber + '%'
UNION ALL
SELECT company.Name Name_Company,
       client.Name Name_Client,
       client.ContactNumber,
       client.ContactNumber2
FROM   db_waterworksv1_WaitingListReference_202201300000.dbo.tClient client
       inner join db_waterworksv1_WaitingListReference_202201300000.dbo.vCompanyActive company
               on client.ID_Company = company.ID
WHERE  db_waterworksv1_server1.dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2) LIKE '%' + @ContactNumber + '%' 
) tbl