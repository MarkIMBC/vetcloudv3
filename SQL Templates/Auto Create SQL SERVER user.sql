USE master

GO

CREATE OR
ALTER PROCEDURE dbo.pCreateUserInDatabase @LoginName    NVARCHAR(128),-- Login for the new user
                                          @Password     NVARCHAR(128),-- Password for the login
                                          @DatabaseName NVARCHAR(128),-- Database where the user will be created
                                          @UserName     NVARCHAR(128),-- Name of the user to be created in the database
                                          @RoleName     NVARCHAR(128) = 'db_datareader' -- Default role, can be changed to db_datawriter, db_owner, etc.
AS
  BEGIN
      -- Step 1: Check if the login already exists
      IF NOT EXISTS (SELECT *
                     FROM   sys.server_principals
                     WHERE  name = @LoginName)
        BEGIN
            -- Step 2: Create a new login
            EXEC sp_addlogin
              @LoginName,
              @Password;

            PRINT 'Login created successfully.';
        END
      ELSE
        BEGIN
            PRINT 'Login already exists.';
        END

      -- Step 3: Switch to the specified database
      EXEC('USE [' + @DatabaseName + '];');

      -- Step 4: Check if the user already exists in the database
      IF NOT EXISTS (SELECT *
                     FROM   sys.database_principals
                     WHERE  name = @UserName)
        BEGIN
            -- Step 5: Create the user in the database and associate with the login
            EXEC('CREATE USER [' + @UserName + '] FOR LOGIN [' + @LoginName + '];');

            PRINT 'User created successfully.';
        END
      ELSE
        BEGIN
            PRINT 'User already exists in the database.';
        END

      -- Step 6: Add user to specified role in the database
      EXEC('ALTER ROLE [' + @RoleName + '] ADD MEMBER [' + @UserName + '];');

      PRINT 'User added to role: ' + @RoleName;
  END;

GO

DECLARE @i INT = 1;
DECLARE @sql NVARCHAR(MAX);

WHILE @i <= 1
  BEGIN
      DECLARE @LoginName NVARCHAR(128) = 'Zam2MRBCUser' + CONVERT(nvarchar(MAX), @i)
      DECLARE @Password NVARCHAR(128) = '12345'
      DECLARE @DatabaseName NVARCHAR(128)= 'Zam2MRBC'
      DECLARE @UserName NVARCHAR(128) = 'Zam2MRBCUser'
      DECLARE @RoleName NVARCHAR(128)= 'db_datawriter'; -- Optional, defaults to 'db_datareader'
	  ------
      EXEC dbo.CreateUserInDatabase
        @LoginName,
        @Password,
        @DatabaseName,
        @UserName,
        @RoleName

      SET @i = @i + 1;
  END 
