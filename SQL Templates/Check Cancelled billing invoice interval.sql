SELECT ID,
       Date,
       Code,
       Name_Client,
       PatientNames,
       Name_FilingStatus,
       DateCreated,
       CreatedBy_Name_User, DateApproved,
       ApprovedBy_Name_User,
       DateCanceled,
       CanceledBy_Name_User,
       Convert(time, Convert(Datetime, Datediff(ms, ISNULL(DateApproved, DateCreated), DateCanceled) / 86400000.0)) 
	   [TimeElapse after Cancel]
FROM   vBillingInvoice
where  Name_Client = 'Megan Ege'
       AND ID_Company = 265 
