DECLARE @ID_User INT = 109996 --ID ng Administrator user na nawalan ng access
--
INSERT INTO [dbo].[tUser_Roles]
            ([IsActive],
             [ID_User],
             [ID_UserRole])
SELECT 1 IsActive,
       @ID_User,
       ID_UserRole
FROM   (SELECT ID_UserRole
        FROM   tUser_Roles
        where  ID_User = 10
               AND ID_UserRole = 12
        EXCEPT
        SELECT ID_UserRole
        FROM   tUser_Roles
        WHERE  ID_User = @ID_User) tbl 
