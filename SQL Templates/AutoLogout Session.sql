

DECLARE @ID_User INT = 10


Update tUserSession
SET    IsActive = 0,
       Comment = 'Auto log out as of '
                 + FORMAT(GETDATE(), 'yyyy-MM-dd hh:mm tt')
FROm   tUserSession _session
       INNER JOIN vUser _user
               on _session.ID_User = _user.ID
where  _user.ID_Company = 1
       and _session.IsActive = 1 
