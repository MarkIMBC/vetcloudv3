DECLARE @Guid_Company Varchar(MAX) = '64EC3DA8-8A29-49EE-8682-81A8DE30B0CC'
DECLARE @Client TABLE
  (
     Source_Code_Client      VARCHAR(MAX),
     Destination_Code_Client VARCHAR(MAX)
  )

INSERT @Client
       (Source_Code_Client,
        Destination_Code_Client)
VALUES ('CNT-01547',
        'CNT-03334'),
       ('CNT-03222',
        'CNT-01517'),
       ('CNT-05604',
        'CNT-04705'),
       ('CNT-05528',
        'CNT-04395'),
       ('CNT-03474',
        'CNT-02203'),
       ('CNT-00395',
        'CNT-01386'),
       ('CNT-05642',
        'CNT-04216'),
       ('CNT-04482',
        'CNT-05294'),
       ('CNT-00866',
        'CNT-05294'),
       ('CNT-03429',
        'CNT-05159'),
       ('CNT-01474',
        'CNT-03378'),
       ('CNT-04884',
        'CNT-03977')

Declare @Source_Code_Client VARCHAR(MAX)
Declare @Destination_Code_Client VARCHAR(MAX)
DECLARE db_cursor CURSOR FOR
  SELECT Source_Code_Client,
         Destination_Code_Client
  FROM   @Client

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Source_Code_Client, @Destination_Code_Client

WHILE @@FETCH_STATUS = 0
  BEGIN
      exec pMergeClientRecordByCompany
        @Guid_Company,
        @Source_Code_Client,
        @Destination_Code_Client

      FETCH NEXT FROM db_cursor INTO @Source_Code_Client, @Destination_Code_Client
  END

CLOSE db_cursor

DEALLOCATE db_cursor 
