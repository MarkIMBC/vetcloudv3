SELECT hed.ID,
       hed.Description,
       hed.Date,
       det.Name,
       OldValue,
       NewValue,
       ModelProperty
FROM   tAuditTrail hed
       inner join tAuditTrail_Detail det
               on hed.id = det.ID_AuditTrail
WHERE  hed.ID_CurrentObject = '473684' 
