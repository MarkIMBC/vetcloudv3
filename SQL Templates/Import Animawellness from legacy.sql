DECLARE @ID_Company_PasswomeSpace INT = 51
DECLARE @PatientAnimalAwareness TABLE
  (
     ID_Patient      INT,
     AnimalAwareness VARCHAR(MAX)
  )
DECLARE @ID_Patient INT = 0
DECLARE @PatientName VARCHAR(MAX) = ''
DECLARE @Date_AnimalAwareness DateTime
DECLARE @Activity_AnimalAwareness VARCHAR(MAX) = ''
DECLARE @Drugs_AnimalAwareness VARCHAR(MAX) = ''
DECLARE @SerialNumber_AnimalAwareness VARCHAR(MAX) = ''
DECLARE @expiry_AnimalAwareness DateTime
DECLARE @details_AnimalAwareness VARCHAR(MAX) = ''
DECLARE @Done_AnimalAwareness VARCHAR(MAX) = ''
DECLARE @DoneDate_AnimalAwareness DateTime
DECLARE Location_Cursor CURSOR FOR
  select patient.ID,
                patient.Name,
                animalawareness.date,
                activity,
                drugs,
                ISNULL(serial_number, ''),
                expiry,
                details,
                done,
                done_date
  FROM   [dbo].['pawsomespace-animalwellness$'] animalawareness
         INNER JOIN tPatient patient
                 on animalawareness.pet_id = patient.Old_patient_id
  WHERE  ID_Company = @ID_Company_PasswomeSpace
  ORDER  BY patient.Name,
            animalawareness.animal_wellness_id

OPEN Location_Cursor;

FETCH NEXT FROM Location_Cursor INTO @ID_Patient,
                                     @PatientName,
                                     @Date_AnimalAwareness,
                                     @Activity_AnimalAwareness,
                                     @Drugs_AnimalAwareness,
                                     @SerialNumber_AnimalAwareness,
                                     @expiry_AnimalAwareness,
                                     @details_AnimalAwareness,
                                     @Done_AnimalAwareness,
                                     @DoneDate_AnimalAwareness;

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @Count INT = 0

      SELECT @Count = COUNT(*)
      FROM   @PatientAnimalAwareness
      WHERE  ID_Patient = @ID_Patient

      IF( @Count = 0 )
        BEGIN
            INSERT @PatientAnimalAwareness
            VALUES(@ID_Patient,
                   '')
        END

      DECLARE @AnimalAwareness VARCHAR(MAX) = ''

      SET @AnimalAwareness = 'Date: ' + ' '
                             + ISNULL(FORMAT(@Date_AnimalAwareness, 'MM/dd/yyyy'),'') + CHAR(13) 
							 + 'Activity: ' + ISNULL(@Activity_AnimalAwareness, '') + CHAR(13) 
							 + 'Drug: ' + ISNULL(@Drugs_AnimalAwareness, '') + 
										 CASE WHEN LEN(ISNULL(@SerialNumber_AnimalAwareness, '')) > 0 THEN ' SerialNo: ' + ISNULL(@SerialNumber_AnimalAwareness, '') ELSE '' END +  
										 CASE WHEN LEN(ISNULL(FORMAT(@expiry_AnimalAwareness, 'MM/dd/yyyy'), '')) > 0 THEN ' Expiry Date: ' + ISNULL(FORMAT(@expiry_AnimalAwareness, 'MM/dd/yyyy'), '') ELSE '' END +  CHAR(13) +
							 CASE WHEN LEN(ISNULL(@Done_AnimalAwareness, 0)) = 1 THEN 'Status: Done ' ELSE '' END +  ISNULL(FORMAT(@DoneDate_AnimalAwareness, 'MM/dd/yyyy'), '') +
										 CHAR(13) +  CHAR(13)

		UPDATE @PatientAnimalAwareness
		SET AnimalAwareness = AnimalAwareness + @AnimalAwareness
		WHERE ID_Patient = @ID_Patient

		print @AnimalAwareness
      FETCH NEXT FROM Location_Cursor INTO @ID_Patient,
                                           @PatientName,
                                           @Date_AnimalAwareness,
                                           @Activity_AnimalAwareness,
                                           @Drugs_AnimalAwareness,
                                           @SerialNumber_AnimalAwareness,
                                           @expiry_AnimalAwareness,
                                           @details_AnimalAwareness,
                                           @Done_AnimalAwareness,
                                           @DoneDate_AnimalAwareness;
  END;

CLOSE Location_Cursor;

DEALLOCATE Location_Cursor;

SELECT patient.name
FROM tPatient patient INNER JOIN
	@PatientAnimalAwareness patientanimalawareness
	ON patient.ID = patientanimalawareness.ID_Patient


UPDATE tPatient 
SET AnimalWellness = patientanimalawareness.AnimalAwareness + CHAR(13) + CHAR(13) + ISNULL(AnimalWellness, '')
FROM tPatient patient INNER JOIN
	@PatientAnimalAwareness patientanimalawareness
	ON patient.ID = patientanimalawareness.ID_Patient

GO 

