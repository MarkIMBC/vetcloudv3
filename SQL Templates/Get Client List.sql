DEclare @GUID_Company VARCHAR(MAX) = 'D0F3355F-D95F-43E9-A22C-C3D67E857C88'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
select @Name_Company Name_Company,
       DateCreated,
       Code,
       Name,
       Email,
       Address,
       ContactNumber,
       ContactNumber2,
       CreatedBy,
       LastModifiedBy,
       DateLastVisited,
       Comment
FROM   vActiveClient
where  ID_Company = @ID_Company
       AND CONVERT(Date, DateCreated) BETWEEN CONVERT(Date, DATEADD(MONTH, -5, GETDATE())) AND CONVERT(Date, GETDATE())
Order  by DateCreated,
          Name 
