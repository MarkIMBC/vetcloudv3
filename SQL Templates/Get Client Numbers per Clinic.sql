DEclare @GUID_Company VARCHAR(MAX) = '3050A529-927C-4FB8-B5DF-8FD935C65962'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

SELECT companyActive.Name              Name_Company,
       client.Name,
       ISNULL(client.ContactNumber, 0) ContactNumber,
       case
         WHEN ISNULL(client.ContactNumber, 0) = ISNULL(client.ContactNumber2, 0) THEN ISNULL(client.ContactNumber2, 0)
         ELSE ''
       END                             ContactNumber2
FROm   tClient client
       inner join vCompanyActive companyActive
               on client.ID_Company = companyActive.ID
where  ID_Company = @ID_Company
       and client.IsActive = 1
ORDER  BY client.Name 
