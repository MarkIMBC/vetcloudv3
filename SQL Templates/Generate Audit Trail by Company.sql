DECLARE @GUID_Company VARCHAR(MAX) = '2EADB952-41AD-4CFC-B49F-0CE9AEA6172A'

-----------------------------------------------------------------
IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @table TABLE
  (
     Name_Company  VARCHAR(MAX),
     Date          Datetime,
     Name_Employee VARCHAR(MAX),
     Description   VARCHAR(MAX),
     PropertyGroup   VARCHAR(MAX),
     FieldName   VARCHAR(MAX),
     OldValue   VARCHAR(MAX),
     NewValue   VARCHAR(MAX),
	 AuditTrailType  VARCHAR(MAX)
  )

INSERT @table
select _user.Name_Company,
       _audit.Date,
       Name_Employee,
       _audit.Description, 
	   _auditDet.ModelProperty, _auditDet.Name, CONVERT(varchar(MAX), OldValue), CONVERT(varchar(MAX), NewValue), Name_AuditTrailType
FROM   vAuditTrail _audit
		INNER JOIN vAuditTrail_Detail _auditDet on _audit.ID = _auditDet.ID_AuditTrail
       inner join vUser _user
               on _audit.ID_User = _user.ID
where  _user.ID_Company = @ID_Company
Order  by _audit.Date

INSERT @table
SELECT @Name_Company,
       DateCanceled,
       CanceledBy_Name_User,
       bi.Code + ' - ' + Name_FilingStatus, '', '', '', '', ''
FROm   vBillingInvoice bi
where  ID_Company = @ID_Company
       and ID_FilingStatus IN ( 4 )

INSERT @table
SELECT @Name_Company,
       DateApproved,
       ApprovedBy_Name_User,
       bi.Code + ' - ' + Name_FilingStatus, '', '', '', '', ''
FROm   vBillingInvoice bi
where  ID_Company = @ID_Company
       and ID_FilingStatus IN ( 3 )

	   
INSERT @table
SELECT @Name_Company,
       DateApproved,
       ApprovedBy_Name_User,
       bi.Code + ' - ' + Name_FilingStatus, '', '', '', '', ''
FROm   vPaymentTransaction bi
where  ID_Company = @ID_Company
       and ID_FilingStatus IN ( 3 )

INSERT @table
SELECT @Name_Company,
       DateApproved,
       ApprovedBy_Name_User,
       bi.Code + ' - ' + Name_FilingStatus, '', '', '', '', ''
FROm   vPaymentTransaction bi
where  ID_Company = @ID_Company
       and ID_FilingStatus IN ( 3 )


UPDATE @table SET FieldName = REPLACE(FieldName, 'ID_', '')

SELECT *
FROM   @table
WHERE CONVERT(Date, Date) >= '2024-10-01'
Order  by Date DESC 
