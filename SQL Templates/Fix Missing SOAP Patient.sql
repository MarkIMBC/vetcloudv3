DECLARE @SOAPPatientIDFromAuditTrail TABLE
  (
     ID_Patient_SOAP INT,
     ID_Patient      INT
  )
DECLARE @IDs_AuditTrail typIntList

INSERT @IDs_AuditTrail
SELECT MAX(auditTrail.ID) ID
FROm   tAuditTrail auditTrail
       inner join _tModel model
               on model.Oid = auditTrail.ID_Model
where  model.TableName = 'tPatient_SOAP'
GROUP  BY auditTrail.ID_CurrentObject

INSERT @SOAPPatientIDFromAuditTrail
SELECT DISTINCT auditTrail.ID_CurrentObject,
                TRY_CONVERT(varchar(MAX), NewValue)
FROm   tAuditTrail auditTrail
       inner join tAuditTrail_Detail auDetail
               on auditTrail.ID = auDetail.ID_AuditTrail
       inner join _tModel model
               on model.Oid = auditTrail.ID_Model
       Inner join @IDs_AuditTrail ids
               on auditTrail.ID = ids.ID
where  model.TableName = 'tPatient_SOAP'
       and auDetail.Name = 'ID_Patient'

INSERT @SOAPPatientIDFromAuditTrail
SELECT _soap.ID,
       patientConfinementOnePet.ID_Patient
FROM   tpatient_Confinement con
       INNER JOIN (SELECT MAX(ID_Patient_Confinement) ID_Patient_Confinement,
                          ID_Patient
                   FROM   tPatient_Confinement_Patient
                   GROUP  BY ID_Patient
                   HAVING Count(*) = 1) patientConfinementOnePet
               on con.ID = patientConfinementOnePet.ID_Patient_Confinement
       Inner join tPatient_SOAP _soap
               ON _soap.ID_Patient_Confinement = con.ID
WHERE  _soap.ID_Patient IS NULL

SELECT _soap.ID,
       _soap.DateCreated,
       _soap.Code,
       _soap.ID_Company,
       _soap.ID_Patient,
       soapPatientAuditTrail.ID_Patient
FROm   tPatient_SOAP _soap
       inner join @SOAPPatientIDFromAuditTrail soapPatientAuditTrail
               on _soap.ID = soapPatientAuditTrail.ID_Patient_SOAP
       INNER JOIN tPatient patient
               on patient.ID = soapPatientAuditTrail.ID_Patient
WHERE  _soap.ID_Patient IS NULL
       and _soap.ID_Company = patient.ID_Company

Update tPatient_SOAP
SET    ID_Patient = soapPatientAuditTrail.ID_Patient
FROm   tPatient_SOAP _soap
       inner join @SOAPPatientIDFromAuditTrail soapPatientAuditTrail
               on _soap.ID = soapPatientAuditTrail.ID_Patient_SOAP
       INNER JOIN tPatient patient
               on patient.ID = soapPatientAuditTrail.ID_Patient
WHERE  _soap.ID_Patient IS NULL
       and _soap.ID_Company = patient.ID_Company 
