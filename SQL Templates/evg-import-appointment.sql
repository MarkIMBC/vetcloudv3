DECLARE @ID_Patient_Vaccination INT
DECLARE @EVGJuicoPetCarCenter_ID_Company INT = 183
DECLARE @appointment_id INT
DECLARE @client_id INT
DECLARE @pet_id INT
DECLARE @start DateTime
DECLARE @end DateTime
DECLARE @reason VARCHAR(MAX) = ''
DECLARE @vName VARCHAR(MAX) = ''
DECLARE import_cursor CURSOR FOR
  SELECT appointment_id,
         start,
         [end],
         client_id,
         pet_id,
         reason,
         vName
  FROM   evgjuicopetcarecenter_latest_appointment
  WHERE  ISNULL(appointment_id, 0) NOT IN (SELECT DISTINCT ISNULL(old_appointment_id, 0)
                          FROM   [tPatient_Wellness]
                          WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company)

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @appointment_id,
                                   @start,
                                   @end,
                                   @client_id,
                                   @pet_id,
                                   @reason,
                                   @vName

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Patient_Wellness INT = 0
      DECLARE @ID_Client INT = 0
      DECLARE @ID_Patient INT = 0

      SELECT @ID_Client = ID
      FROm   tClient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_client_id = @client_id

      SELECT @ID_Patient = ID
      FROm   tPatient
      WHERE  ID_Company = @EVGJuicoPetCarCenter_ID_Company
             AND Old_patient_id = @pet_id

      INSERT INTO [dbo].[tPatient_Wellness]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Date],
                   [ID_Client],
                   [ID_Patient],
                   [ID_FilingStatus],
                   [AttendingPhysician],
                   old_appointment_id)
      VALUES      ( 1,
                    @EVGJuicoPetCarCenter_ID_Company,
                    NULL,
                    GETDATE(),
                    GETDATE(),
                    1,
                    1,
                    @start,
                    @ID_Client,
                    @ID_Patient,
                    1,
                    @vName,
                    @appointment_id )

      SET @ID_Patient_Wellness = @@IDENTITY

      INSERT INTO [dbo].[tPatient_Wellness_Schedule]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   [Date])
      VALUES      (1,
                   @EVGJuicoPetCarCenter_ID_Company,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ID_Patient_Wellness,
                   @start )

      INSERT INTO [dbo].[tPatient_Wellness_Detail]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   CustomItem)
      VALUES      (1,
                   @EVGJuicoPetCarCenter_ID_Company,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ID_Patient_Wellness,
                   @reason)

      exec pModel_AfterSaved_Patient_Wellness
        @ID_Patient_Wellness,
        1

      FETCH NEXT FROM import_cursor INTO @appointment_id,
                                         @start,
                                         @end,
                                         @client_id,
                                         @pet_id,
                                         @reason,
                                         @vName
  END

CLOSE import_cursor;

DEALLOCATE import_cursor; 
