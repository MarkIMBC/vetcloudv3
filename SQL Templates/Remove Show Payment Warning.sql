GO

DECLARE @GUID_Company VARCHAR(MAX) = '7C0A3310-E268-4712-8E7A-7DB0D065790C'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

Update tCompany
set    IsShowPaymentLabel = 0,
       IsShowPaymentWarningLabel = 0
WHERE  ID = @ID_Company

SELECT Name,
       IsShowPaymentWarningLabel
FROm   tCompany
WHERE  ID = @ID_Company 
