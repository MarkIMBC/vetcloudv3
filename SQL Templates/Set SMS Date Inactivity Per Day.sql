DEclare @GUID_Company VARCHAR(MAX) = '8E45BE06-B8EB-4729-873C-A4B53190AB44'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------

DECLARE @Dates TABLE
  (
     DateInActivity Date
  )

INSERT @Dates
SELECT Date
FROM   dbo.fGetDatesByDateCoverage( '2023-02-27', '2023-02-27')
  

select *,
       FORMAT(CAST(DateInActivity AS DATE), 'ddd')
FROM   @Dates

INSERT INTO [dbo].[tInactiveSMSSending]
            ([Date],
             [ID_Company],
             [IsActive],
             [Comment],
             DateCreated,
             DateModified)
SELECT DateInActivity,
       company.ID,
       1,
       'HMR Veterinary - No SMS Sending Feb 27, 2023',
       GetDate(),
       GetDate()
FROM   vCompanyActive company,
       @Dates
where  GUid = @GUID_Company
Order  BY company.Name 

SELECT ID_Company, Date, Comment FROM tInactiveSMSSending  where ID_Company = @ID_Company Order by ID DESC