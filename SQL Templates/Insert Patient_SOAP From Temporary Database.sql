IF COL_LENGTH('tPatient_SOAP', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP',
        'tempID',
        1
  END
  exec _pRefreshAllViews
GO
IF COL_LENGTH('tEmployee', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tEmployee',
        'tempID',
        1
  END
  exec _pRefreshAllViews
GO

IF OBJECT_ID(N'Temp-2022-08-07-AuditTrail-Patient_SOAP-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-08-07-AuditTrail-Patient_SOAP-Added]

GO

IF OBJECT_ID(N'Temp-2022-08-07-Patient_SOAP', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-08-07-Patient_SOAP]

GO

Declare @NotYetInserted_Temp_ID_Patient_SOAP TABLE
  (
     Temp_ID_Patient_SOAP VARCHAR(MAX)
  )

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [dbo].[Temp-2022-08-07-AuditTrail-Patient_SOAP-Added]
from   db_waterworksv1_server2.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_server2.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_server2.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel _model
              on _auditTrail.ID_Model = _model.Oid
WHERE  Date >= '2022-07-15 09:34:51.037'
       AND Description like '%added%'
       AND _model.TableName = 'tPatient_SOAP' and GUID = '2EEB5DFE-075E-4EB2-B2D8-9160836F014F'
Order  by Date

SELECT company.ID                                Main_ID_Company,
       company.Guid                              Guid_ID_Company,
       company.Name                              Name_Company,
       'Temp-2022-08-07-' + company.Guid
       + '-Patient_SOAP-'
       + Convert(Varchar(MAX), Patient_SOAP_.ID) Temp_ID_Patient_SOAP,
       Patient_SOAP_.*
INTO   [dbo].[Temp-2022-08-07-Patient_SOAP]
FROM   db_waterworksv1_server2.[dbo].vPatient_SOAP Patient_SOAP_
       inner join db_waterworksv1_server2.[dbo].tCompany company
               on Patient_SOAP_.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-08-07-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient_SOAP_.ID_Client)
                 and mainClient.ID_Company = Patient_SOAP_.ID_Company
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-08-07-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), Patient_SOAP_.ID_Patient)
                 and mainPatient.ID_Company = Patient_SOAP_.ID_Company
where  Patient_SOAP_.DateCreated > '2022-07-14 16:36:01.523' and company.GUID = '2EEB5DFE-075E-4EB2-B2D8-9160836F014F'

INSERT @NotYetInserted_Temp_ID_Patient_SOAP
SELECT tempPatient_SOAP.Temp_ID_Patient_SOAP
FROm   [dbo].[Temp-2022-08-07-Patient_SOAP] tempPatient_SOAP

DELETE FROM @NotYetInserted_Temp_ID_Patient_SOAP
WHERE  Temp_ID_Patient_SOAP IN (SELECT tempID
                                FROM   tPatient_SOAP)

INSERT tPatient_SOAP
       (ID_Company,
        tempID,
        ID_Client,
        ID_Patient,
        AttendingPhysician_ID_Employee,
        Subjective,
        Objective,
        Assessment,
        Prescription,
        Planning,
        Date,
        ID_SOAPType,
        ID_ApprovedBy,
        ID_CanceledBy,
        DateApproved,
        DateCanceled,
        ID_FilingStatus,
        Old_soap_id,
        LabImageFilePath01,
        LabImageFilePath02,
        LabImageFilePath03,
        LabImageFilePath04,
        LabImageFilePath05,
        LabImageFilePath06,
        LabImageFilePath07,
        LabImageFilePath08,
        LabImageFilePath09,
        LabImageFilePath10,
        LabImageFilePath11,
        LabImageFilePath12,
        LabImageFilePath13,
        LabImageFilePath14,
        LabImageFilePath15,
        LabImageRemark01,
        LabImageRemark02,
        LabImageRemark03,
        LabImageRemark04,
        LabImageRemark05,
        LabImageRemark06,
        LabImageRemark07,
        LabImageRemark08,
        LabImageRemark09,
        LabImageRemark10,
        LabImageRemark11,
        LabImageRemark12,
        LabImageRemark13,
        LabImageRemark14,
        LabImageRemark15,
        LabImageRowIndex01,
        LabImageRowIndex02,
        LabImageRowIndex03,
        LabImageRowIndex04,
        LabImageRowIndex05,
        LabImageRowIndex06,
        LabImageRowIndex07,
        LabImageRowIndex08,
        LabImageRowIndex09,
        LabImageRowIndex10,
        LabImageRowIndex11,
        LabImageRowIndex12,
        LabImageRowIndex13,
        LabImageRowIndex14,
        LabImageRowIndex15,
        History,
        Diagnosis,
        Treatment,
        ClientCommunication,
        ClinicalExamination,
        Interpretation,
        DateDone,
        ID_DoneBy,
        CaseType,
        BillingInvoice_ID_FilingStatus,
        _temp,
        _tempID,
        old_history_id,
        Comment)
SELECT tempPatient_SOAP.ID_Company,
	   tempPatient_SOAP.Temp_ID_Patient_SOAP,
       ISNULL(mainClient.ID, mainClient2.ID)                         Main_ID_Client,
       ISNULL(mainPatient.ID, mainPatient2.ID)                       Main_ID_Patient,
       ISNULL(mainAttendingPhysician.ID, mainAttendingPhysician2.ID) Main_AttendingPhysician_ID_Employee,
       tempPatient_SOAP.Subjective,
       tempPatient_SOAP.Objective,
       tempPatient_SOAP.Assessment,
       tempPatient_SOAP.Prescription,
       tempPatient_SOAP.Planning,
       tempPatient_SOAP.Date,
       tempPatient_SOAP.ID_SOAPType,
       tempPatient_SOAP.ID_ApprovedBy,
       tempPatient_SOAP.ID_CanceledBy,
       tempPatient_SOAP.DateApproved,
       tempPatient_SOAP.DateCanceled,
       tempPatient_SOAP.ID_FilingStatus,
       tempPatient_SOAP.Old_soap_id,
       tempPatient_SOAP.LabImageFilePath01,
       tempPatient_SOAP.LabImageFilePath02,
       tempPatient_SOAP.LabImageFilePath03,
       tempPatient_SOAP.LabImageFilePath04,
       tempPatient_SOAP.LabImageFilePath05,
       tempPatient_SOAP.LabImageFilePath06,
       tempPatient_SOAP.LabImageFilePath07,
       tempPatient_SOAP.LabImageFilePath08,
       tempPatient_SOAP.LabImageFilePath09,
       tempPatient_SOAP.LabImageFilePath10,
       tempPatient_SOAP.LabImageFilePath11,
       tempPatient_SOAP.LabImageFilePath12,
       tempPatient_SOAP.LabImageFilePath13,
       tempPatient_SOAP.LabImageFilePath14,
       tempPatient_SOAP.LabImageFilePath15,
       tempPatient_SOAP.LabImageRemark01,
       tempPatient_SOAP.LabImageRemark02,
       tempPatient_SOAP.LabImageRemark03,
       tempPatient_SOAP.LabImageRemark04,
       tempPatient_SOAP.LabImageRemark05,
       tempPatient_SOAP.LabImageRemark06,
       tempPatient_SOAP.LabImageRemark07,
       tempPatient_SOAP.LabImageRemark08,
       tempPatient_SOAP.LabImageRemark09,
       tempPatient_SOAP.LabImageRemark10,
       tempPatient_SOAP.LabImageRemark11,
       tempPatient_SOAP.LabImageRemark12,
       tempPatient_SOAP.LabImageRemark13,
       tempPatient_SOAP.LabImageRemark14,
       tempPatient_SOAP.LabImageRemark15,
       tempPatient_SOAP.LabImageRowIndex01,
       tempPatient_SOAP.LabImageRowIndex02,
       tempPatient_SOAP.LabImageRowIndex03,
       tempPatient_SOAP.LabImageRowIndex04,
       tempPatient_SOAP.LabImageRowIndex05,
       tempPatient_SOAP.LabImageRowIndex06,
       tempPatient_SOAP.LabImageRowIndex07,
       tempPatient_SOAP.LabImageRowIndex08,
       tempPatient_SOAP.LabImageRowIndex09,
       tempPatient_SOAP.LabImageRowIndex10,
       tempPatient_SOAP.LabImageRowIndex11,
       tempPatient_SOAP.LabImageRowIndex12,
       tempPatient_SOAP.LabImageRowIndex13,
       tempPatient_SOAP.LabImageRowIndex14,
       tempPatient_SOAP.LabImageRowIndex15,
       tempPatient_SOAP.History,
       tempPatient_SOAP.Diagnosis,
       tempPatient_SOAP.Treatment,
       tempPatient_SOAP.ClientCommunication,
       tempPatient_SOAP.ClinicalExamination,
       tempPatient_SOAP.Interpretation,
       tempPatient_SOAP.DateDone,
       tempPatient_SOAP.ID_DoneBy,
       tempPatient_SOAP.CaseType,
       tempPatient_SOAP.BillingInvoice_ID_FilingStatus,
       tempPatient_SOAP._temp,
       tempPatient_SOAP._tempID,
       tempPatient_SOAP.old_history_id,
       + 'Imported from VetCloudTemp August 27, 2022'
       + ' Reference Code (' + tempPatient_SOAP.Code
       + ')'
       + CASE
           WHEN LEN(ISNULL(tempPatient_SOAP.Comment, '')) > 0 THEN CHAR(13) + tempPatient_SOAP.Comment
           ELSE ''
         END                                                         Comment
FROm   [dbo].[Temp-2022-08-07-Patient_SOAP] tempPatient_SOAP
       inner join @NotYetInserted_Temp_ID_Patient_SOAP unInsertedTempPatient_SOAP
               on tempPatient_SOAP.Temp_ID_Patient_SOAP = unInsertedTempPatient_SOAP.Temp_ID_Patient_SOAP
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-08-07-'
                                     + tempPatient_SOAP.Guid_ID_Company
                                     + '-Client-'
                                     + Convert(Varchar(MAX), tempPatient_SOAP.ID_Client)
                 and mainClient.ID_Company = tempPatient_SOAP.ID_Company
       LEFT JOIN tClient mainClient2
              on mainClient2.ID = tempPatient_SOAP.ID_Client
                 and mainClient2.ID_Company = tempPatient_SOAP.ID_Company
       ------------ Patient ----------------
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-08-07-'
                                      + tempPatient_SOAP.Guid_ID_Company
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), tempPatient_SOAP.ID_Patient)
                 and mainPatient.ID_Company = tempPatient_SOAP.ID_Company
       LEFT JOIN tPatient mainPatient2
              on mainPatient2.ID = tempPatient_SOAP.ID_Patient
                 and mainPatient2.ID_Company = tempPatient_SOAP.ID_Company
       ------------ Patient END ----------------
       /*                                        */
       -----------  Attending Personell ---------------
       LEFT JOIN tEmployee mainAttendingPhysician
              on mainAttendingPhysician.tempID = 'Temp-2022-08-07-'
                                                 + tempPatient_SOAP.Guid_ID_Company
                                                 + '-Employee-'
                                                 + Convert(Varchar(MAX), tempPatient_SOAP.AttendingPhysician_ID_Employee)
                 and mainAttendingPhysician.ID_Company = tempPatient_SOAP.ID_Company
       LEFT JOIN tEmployee mainAttendingPhysician2
              on mainAttendingPhysician2.ID = tempPatient_SOAP.AttendingPhysician_ID_Employee
                 and mainAttendingPhysician2.ID_Company = tempPatient_SOAP.ID_Company

-----------  Attending Personell END ---------------
GO

exec _pRefreshAllViews 


SELECT * FROm [Temp-2022-08-07-AuditTrail-Patient_SOAP-Added] order by Name_Company