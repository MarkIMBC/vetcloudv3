GO

Print 'TOTAL RESET Vetcloud Table will started after 1 minute'

GO

WAITFOR DELAY '00:01:00';

GO

TRUNCATE TABLE dbo.tAppointmentStatusLog

TRUNCATE TABLE dbo.tAppointmentSchedule

TRUNCATE TABLE dbo.tAppointmentReschduleLog

TRUNCATE TABLE dbo.tAppointmentRequest

GO

Print 'Appointment StatusLog Truncated'

Print 'Appointment Schedule Truncated'

Print 'Appointment ReschduleLog Truncated'

Print 'Appointment Request Truncated'

GO

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  DROP CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail]

GO

TRUNCATE TABLE dbo.tAuditTrail_Detail

TRUNCATE TABLE dbo.tAuditTrail

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  WITH NOCHECK ADD CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail] FOREIGN KEY([ID_AuditTrail]) REFERENCES [dbo].[tAuditTrail] ([ID]) ON DELETE CASCADE

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  CHECK CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail]

GO

Print 'Audit Trail Truncated'

GO

ALTER TABLE [dbo].[tBillingInvoice]
  DROP CONSTRAINT [FK_tBillingInvoice_ID_Company]

GO

ALTER TABLE [dbo].[tBillingInvoice]
  DROP CONSTRAINT [FK_tBillingInvoice_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tBillingInvoice]
  DROP CONSTRAINT [FK_tBillingInvoice_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  DROP CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  DROP CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tBillingInvoice] DROP CONSTRAINT [PK_tBillingInvoice] WITH ( ONLINE = OFF )
GO

TRUNCATE TABLE tBillingInvoice_SMSPayableRemider_SMSStatus

TRUNCATE TABLE tBillingInvoice_SMSPayableRemider

TRUNCATE TABLE tBillingInvoice_Patient

TRUNCATE TABLE tBillingInvoice_Detail

TRUNCATE TABLE tBillingInvoice

GO

ALTER TABLE [dbo].[tBillingInvoice]
  ADD CONSTRAINT [PK_tBillingInvoice] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tBillingInvoice]
  WITH CHECK ADD CONSTRAINT [FK_tBillingInvoice_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice]
  CHECK CONSTRAINT [FK_tBillingInvoice_ID_Company]

GO

ALTER TABLE [dbo].[tBillingInvoice]
  WITH CHECK ADD CONSTRAINT [FK_tBillingInvoice_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice]
  CHECK CONSTRAINT [FK_tBillingInvoice_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tBillingInvoice]
  WITH CHECK ADD CONSTRAINT [FK_tBillingInvoice_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice]
  CHECK CONSTRAINT [FK_tBillingInvoice_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  WITH CHECK ADD CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice]) REFERENCES [dbo].[tBillingInvoice] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  CHECK CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  WITH CHECK ADD CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice]) REFERENCES [dbo].[tBillingInvoice] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  CHECK CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice]

GO

Print 'Billing Invoice Truncated'

GO

GO

ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]
  DROP CONSTRAINT [FKtClientAppointmentRequest_Patient_ID_ClientAppointmentRequest]

GO

TRUNCATE TABLE dbo.tClientWithdraw

TRUNCATE TABLE dbo.tClientFeedback

TRUNCATE TABLE dbo.tClientDeposit

TRUNCATE TABLE dbo.tClientAppointmentRequest_Patient

TRUNCATE TABLE dbo.tClientAppointmentRequest

TRUNCATE TABLE dbo.tClient_CreditLogs

TRUNCATE TABLE dbo.tClient

GO

ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]
  WITH CHECK ADD CONSTRAINT [FKtClientAppointmentRequest_Patient_ID_ClientAppointmentRequest] FOREIGN KEY([ID_ClientAppointmentRequest]) REFERENCES [dbo].[tClientAppointmentRequest] ([ID])

GO

ALTER TABLE [dbo].[tClientAppointmentRequest_Patient]
  CHECK CONSTRAINT [FKtClientAppointmentRequest_Patient_ID_ClientAppointmentRequest]

GO

Print 'Client Truncated'

GO

GO

TRUNCATE TABLE dbo._tDatabaseMemoryLog

GO

Print 'Databas Memory Log Truncated'

GO

TRUNCATE TABLE dbo.tFileUpload

GO

Print 'File Upload Truncated'

GO

TRUNCATE TABLE dbo.tInventoryTrail_DeletedRecord

TRUNCATE TABLE dbo.tInventoryTrail

GO

Print 'Inventory Trail Truncated'

GO

GO

ALTER TABLE [dbo].[tItem_Supplier]
  DROP CONSTRAINT [FKtItem_Supplier_ID_Item]

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  DROP CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item]

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  DROP CONSTRAINT [FKtPurchaseOrder_Detail_ID_PurchaseOrder]

GO

ALTER TABLE [dbo].[tItem] DROP CONSTRAINT [PK_tItem] WITH ( ONLINE = OFF )
GO

TRUNCATE TABLE dbo.tItem_UnitPriceLog

TRUNCATE TABLE dbo.tItem_UnitCostLog

TRUNCATE TABLE dbo.tPurchaseOrder_Detail

TRUNCATE TABLE dbo.tPurchaseOrder

TRUNCATE TABLE dbo.tItem_Supplier

TRUNCATE TABLE dbo.tItem

GO

ALTER TABLE [dbo].[tItem]
  ADD CONSTRAINT [PK_tItem] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tItem_Supplier]
  WITH CHECK ADD CONSTRAINT [FKtItem_Supplier_ID_Item] FOREIGN KEY([ID_Item]) REFERENCES [dbo].[tItem] ([ID])

GO

ALTER TABLE [dbo].[tItem_Supplier]
  CHECK CONSTRAINT [FKtItem_Supplier_ID_Item]

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  WITH CHECK ADD CONSTRAINT [FKtPurchaseOrder_Detail_ID_PurchaseOrder] FOREIGN KEY([ID_PurchaseOrder]) REFERENCES [dbo].[tPurchaseOrder] ([ID])

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  CHECK CONSTRAINT [FKtPurchaseOrder_Detail_ID_PurchaseOrder]

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  WITH CHECK ADD CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item] FOREIGN KEY([ID_Item]) REFERENCES [dbo].[tItem] ([ID])

GO

ALTER TABLE [dbo].[tPurchaseOrder_Detail]
  CHECK CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item]

GO

Print 'Item with Purchase Order Truncated'

GO

GO

ALTER TABLE [dbo].[tPatient_DentalExamination]
  DROP CONSTRAINT [FKtPatient_DentalExamination_ID_Patient]

GO

ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation]
  DROP CONSTRAINT [FKtPatient_SOAP_RegularConsoltation_ID_Patient]

GO

ALTER TABLE [dbo].[tPatient_History]
  DROP CONSTRAINT [FKtPatient_History_ID_Patient]

GO

ALTER TABLE [dbo].[tPatient] DROP CONSTRAINT [PK_tPatient] WITH ( ONLINE = OFF )
GO

TRUNCATE TABLE dbo.tPatient_BirthDateSMSGreetingLog

TRUNCATE TABLE tPatient_History

TRUNCATE TABLE tPatient_DentalExamination_MedicalHistory

TRUNCATE TABLE tPatient_DentalExamination

TRUNCATE TABLE dbo.tPatient

GO

ALTER TABLE [dbo].[tPatient]
  ADD CONSTRAINT [PK_tPatient] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tPatient_DentalExamination]
  WITH CHECK ADD CONSTRAINT [FKtPatient_DentalExamination_ID_Patient] FOREIGN KEY([ID_Patient]) REFERENCES [dbo].[tPatient] ([ID])

GO

ALTER TABLE [dbo].[tPatient_DentalExamination]
  CHECK CONSTRAINT [FKtPatient_DentalExamination_ID_Patient]

GO

ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation]
  WITH CHECK ADD CONSTRAINT [FKtPatient_SOAP_RegularConsoltation_ID_Patient] FOREIGN KEY([ID_Patient]) REFERENCES [dbo].[tPatient] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP_RegularConsoltation]
  CHECK CONSTRAINT [FKtPatient_SOAP_RegularConsoltation_ID_Patient]

GO

ALTER TABLE [dbo].[tPatient_History]
  WITH CHECK ADD CONSTRAINT [FKtPatient_History_ID_Patient] FOREIGN KEY([ID_Patient]) REFERENCES [dbo].[tPatient] ([ID])

GO

ALTER TABLE [dbo].[tPatient_History]
  CHECK CONSTRAINT [FKtPatient_History_ID_Patient]

GO

Print 'Patient Truncated'

GO

GO

TRUNCATE TABLE dbo.tPatientAppointment_SMSStatus

TRUNCATE TABLE dbo.tPatientAppointment

GO

Print 'Patient Appointment Truncated'

GO

GO

ALTER TABLE [dbo].[tPatient_Vaccination_Schedule]
  DROP CONSTRAINT [FKtPatient_Vaccination_Schedule_ID_Patient_Vaccination]

GO

TRUNCATE TABLE tPatient_Vaccination_Schedule_SMSStatus

TRUNCATE TABLE tPatient_Vaccination_Schedule

TRUNCATE TABLE tPatient_Vaccination

GO

ALTER TABLE [dbo].[tPatient_Vaccination_Schedule]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Vaccination_Schedule_ID_Patient_Vaccination] FOREIGN KEY([ID_Patient_Vaccination]) REFERENCES [dbo].[tPatient_Vaccination] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Vaccination_Schedule]
  CHECK CONSTRAINT [FKtPatient_Vaccination_Schedule_ID_Patient_Vaccination]

GO

Print 'Patient Vaccination Truncated'

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  DROP CONSTRAINT [FK_tPatient_Confinement_ID_Company]

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  DROP CONSTRAINT [FK_tPatient_Confinement_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  DROP CONSTRAINT [FK_tPatient_Confinement_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]
  DROP CONSTRAINT [FKtPatient_Confinement_ItemsServices_ID_Patient_Confinement]

GO

ALTER TABLE [dbo].[tPatient_Confinement_Patient]
  DROP CONSTRAINT [FKtPatient_Confinement_Patient_ID_Patient_Confinement]

GO

ALTER TABLE [dbo].[tPatient_Confinement] DROP CONSTRAINT [PK_tPatient_Confinement] WITH ( ONLINE = OFF )
GO

TRUNCATE TABLE dbo.tPatient_Confinement_Patient

TRUNCATE TABLE dbo.tPatient_Confinement_ItemsServices

TRUNCATE TABLE dbo.tPatient_Confinement

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  ADD CONSTRAINT [PK_tPatient_Confinement] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tPatient_Confinement_Patient]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Confinement_Patient_ID_Patient_Confinement] FOREIGN KEY([ID_Patient_Confinement]) REFERENCES [dbo].[tPatient_Confinement] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Confinement_Patient]
  CHECK CONSTRAINT [FKtPatient_Confinement_Patient_ID_Patient_Confinement]

GO

ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Confinement_ItemsServices_ID_Patient_Confinement] FOREIGN KEY([ID_Patient_Confinement]) REFERENCES [dbo].[tPatient_Confinement] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Confinement_ItemsServices]
  CHECK CONSTRAINT [FKtPatient_Confinement_ItemsServices_ID_Patient_Confinement]

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Confinement_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  CHECK CONSTRAINT [FK_tPatient_Confinement_ID_Company]

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Confinement_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  CHECK CONSTRAINT [FK_tPatient_Confinement_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Confinement_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Confinement]
  CHECK CONSTRAINT [FK_tPatient_Confinement_ID_LastModifiedBy]

GO

Print 'Patient Confinement Truncated'

GO

ALTER TABLE [dbo].[tPatient_Grooming_Detail]
  DROP CONSTRAINT [FKtPatient_Grooming_Detail_ID_Patient_Grooming]

GO

TRUNCATE TABLE dbo.tPatient_Grooming_Detail

TRUNCATE TABLE dbo.tPatient_Grooming

GO

ALTER TABLE [dbo].[tPatient_Grooming_Detail]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Grooming_Detail_ID_Patient_Grooming] FOREIGN KEY([ID_Patient_Grooming]) REFERENCES [dbo].[tPatient_Grooming] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Grooming_Detail]
  CHECK CONSTRAINT [FKtPatient_Grooming_Detail_ID_Patient_Grooming]

GO

Print 'Patient Grooming Truncated'

GO

GO

ALTER TABLE [dbo].[tPatient_Wellness_Schedule]
  DROP CONSTRAINT [FKtPatient_Wellness_Schedule_ID_Patient_Wellness]

GO

ALTER TABLE [dbo].[tPatient_Wellness_Detail]
  DROP CONSTRAINT [FKtPatient_Wellness_Detail_ID_Patient_Wellness]

GO

ALTER TABLE [dbo].[tPatient_Wellness] DROP CONSTRAINT [PK_tPatient_Wellness] WITH ( ONLINE = OFF )
GO

ALTER TABLE [dbo].[tPatient_Wellness]
  DROP CONSTRAINT [FK_tPatient_Wellness_ID_Company]

GO

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  DROP CONSTRAINT [FK_tPatient_Wellness_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  DROP CONSTRAINT [FK_tPatient_Wellness_ID_CreatedBy]

GO

TRUNCATE TABLE tPatient_Wellness_Schedule_SMSStatus

TRUNCATE TABLE tPatient_Wellness_Schedule

TRUNCATE TABLE tPatient_Wellness_Detail

TRUNCATE TABLE tPatient_Wellness

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  ADD CONSTRAINT [PK_tPatient_Wellness] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tPatient_Wellness_Schedule]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Wellness_Schedule_ID_Patient_Wellness] FOREIGN KEY([ID_Patient_Wellness]) REFERENCES [dbo].[tPatient_Wellness] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Wellness_Schedule]
  CHECK CONSTRAINT [FKtPatient_Wellness_Schedule_ID_Patient_Wellness]

GO

ALTER TABLE [dbo].[tPatient_Wellness_Detail]
  WITH CHECK ADD CONSTRAINT [FKtPatient_Wellness_Detail_ID_Patient_Wellness] FOREIGN KEY([ID_Patient_Wellness]) REFERENCES [dbo].[tPatient_Wellness] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Wellness_Detail]
  CHECK CONSTRAINT [FKtPatient_Wellness_Detail_ID_Patient_Wellness]

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Wellness_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  CHECK CONSTRAINT [FK_tPatient_Wellness_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Wellness_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  CHECK CONSTRAINT [FK_tPatient_Wellness_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_Wellness_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID])

GO

ALTER TABLE [dbo].[tPatient_Wellness]
  CHECK CONSTRAINT [FK_tPatient_Wellness_ID_Company]

GO

Print 'Patient Wellness Truncated'

GO

TRUNCATE TABLE tPatientWaitingListCanceled

TRUNCATE TABLE tPatientWaitingList_SMSStatus

TRUNCATE TABLE tPatientWaitingList_Logs

TRUNCATE TABLE tPatientWaitingList

Print 'Patient Waiting List Truncated'

GO

ALTER TABLE [dbo].[tPatient_SOAP_Prescription]
  DROP CONSTRAINT [FKtPatient_SOAP_Prescription_ID_Patient_SOAP]

GO

ALTER TABLE [dbo].[tPatient_SOAP_Plan]
  DROP CONSTRAINT [FKtPatient_SOAP_Plan_ID_Patient_SOAP]

GO

ALTER TABLE [dbo].[tPatient_SOAP_Treatment]
  DROP CONSTRAINT [FKtPatient_SOAP_Treatment_ID_Patient_SOAP]

GO

ALTER TABLE [dbo].[tPatient_SOAP] DROP CONSTRAINT [PK_tPatient_SOAP] WITH ( ONLINE = OFF )
GO

ALTER TABLE [dbo].[tPatient_SOAP]
  DROP CONSTRAINT [FK_tPatient_SOAP_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  DROP CONSTRAINT [FK_tPatient_SOAP_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  DROP CONSTRAINT [FK_tPatient_SOAP_ID_Company]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  DROP CONSTRAINT [DF__tPatient___IsAct__61CE5A37]

GO

GO

TRUNCATE TABLE tPatient_SOAP_Treatment

TRUNCATE TABLE tPatient_SOAP_SMSStatus

TRUNCATE TABLE tPatient_SOAP_RegularConsoltation

TRUNCATE TABLE tPatient_SOAP_Prescription

TRUNCATE TABLE tPatient_SOAP_Plan

TRUNCATE TABLE tPatient_SOAP

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  ADD CONSTRAINT [PK_tPatient_SOAP] PRIMARY KEY CLUSTERED ( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_SOAP_ID_Company] FOREIGN KEY([ID_Company]) REFERENCES [dbo].[tCompany] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  CHECK CONSTRAINT [FK_tPatient_SOAP_ID_Company]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_SOAP_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  CHECK CONSTRAINT [FK_tPatient_SOAP_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  WITH CHECK ADD CONSTRAINT [FK_tPatient_SOAP_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  CHECK CONSTRAINT [FK_tPatient_SOAP_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tPatient_SOAP]
  ADD CONSTRAINT [DF__tPatient___IsAct__61CE5A37] DEFAULT ((1)) FOR [IsActive]

GO

ALTER TABLE [dbo].[tPatient_SOAP_Treatment]
  WITH CHECK ADD CONSTRAINT [FKtPatient_SOAP_Treatment_ID_Patient_SOAP] FOREIGN KEY([ID_Patient_SOAP]) REFERENCES [dbo].[tPatient_SOAP] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP_Treatment]
  CHECK CONSTRAINT [FKtPatient_SOAP_Treatment_ID_Patient_SOAP]

GO

ALTER TABLE [dbo].[tPatient_SOAP_Plan]
  WITH CHECK ADD CONSTRAINT [FKtPatient_SOAP_Plan_ID_Patient_SOAP] FOREIGN KEY([ID_Patient_SOAP]) REFERENCES [dbo].[tPatient_SOAP] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP_Plan]
  CHECK CONSTRAINT [FKtPatient_SOAP_Plan_ID_Patient_SOAP]

GO

ALTER TABLE [dbo].[tPatient_SOAP_Prescription]
  WITH CHECK ADD CONSTRAINT [FKtPatient_SOAP_Prescription_ID_Patient_SOAP] FOREIGN KEY([ID_Patient_SOAP]) REFERENCES [dbo].[tPatient_SOAP] ([ID])

GO

ALTER TABLE [dbo].[tPatient_SOAP_Prescription]
  CHECK CONSTRAINT [FKtPatient_SOAP_Prescription_ID_Patient_SOAP]

GO

Print 'Patient SOAP Truncated'

GO

ALTER TABLE [dbo].[tPayable_Detail]
  DROP CONSTRAINT [FKtPayable_Detail_ID_Payable]

GO

TRUNCATE TABLE dbo.tPayablePayment

TRUNCATE TABLE dbo.tPayable_Detail

TRUNCATE TABLE dbo.tPayable

GO

ALTER TABLE [dbo].[tPayable_Detail]
  WITH CHECK ADD CONSTRAINT [FKtPayable_Detail_ID_Payable] FOREIGN KEY([ID_Payable]) REFERENCES [dbo].[tPayable] ([ID])

GO

ALTER TABLE [dbo].[tPayable_Detail]
  CHECK CONSTRAINT [FKtPayable_Detail_ID_Payable]

GO

Print 'Payable Truncated'

GO

GO

TRUNCATE TABLE dbo.tPaymentTransaction

GO

Print 'Payment Transaction Truncated'

GO

GO

ALTER TABLE [dbo].[tReceivingReport_Detail]
  DROP CONSTRAINT [FKtReceivingReport_Detail_ID_ReceivingReport]

GO

TRUNCATE TABLE dbo.tReceivingReport_Detail

TRUNCATE TABLE dbo.tReceivingReport

GO

ALTER TABLE [dbo].[tReceivingReport_Detail]
  WITH CHECK ADD CONSTRAINT [FKtReceivingReport_Detail_ID_ReceivingReport] FOREIGN KEY([ID_ReceivingReport]) REFERENCES [dbo].[tReceivingReport] ([ID])

GO

ALTER TABLE [dbo].[tReceivingReport_Detail]
  CHECK CONSTRAINT [FKtReceivingReport_Detail_ID_ReceivingReport]

GO

Print 'Receiving Report Truncated'

GO

GO

TRUNCATE TABLE tRecordValueTransferLogs

GO

Print 'Record Value Transfer Logs Truncated'

GO

GO

GO

ALTER TABLE [dbo].[tPurchaseOrder]
  DROP CONSTRAINT [FKtPurchaseOrder_ID_Supplier]

GO

TRUNCATE TABLE dbo.tSupplier

GO

ALTER TABLE [dbo].[tPurchaseOrder]
  WITH CHECK ADD CONSTRAINT [FKtPurchaseOrder_ID_Supplier] FOREIGN KEY([ID_Supplier]) REFERENCES [dbo].[tSupplier] ([ID])

GO

ALTER TABLE [dbo].[tPurchaseOrder]
  CHECK CONSTRAINT [FKtPurchaseOrder_ID_Supplier]

GO

GO

Print 'Supplier Truncated'

GO

GO

TRUNCATE TABLE dbo.tTextBlast_Client_SMSStatus

TRUNCATE TABLE dbo.tTextBlast_Client

GO

Print 'Text Blast Truncated'

GO

TRUNCATE TABLE dbo.tUserSession

Print 'User Session Truncated'

GO

GO

TRUNCATE TABLE dbo.tVeterinaryHealthCertificate

GO

Print 'Veterinary Health Certificate Truncated'

GO

exec pReOrganizeRebuildTableIndex

GO

exec _pRefreshAllViews

GO

-- Create a temporary table to store results
CREATE TABLE #TableRecordCounts
  (
     TableName   NVARCHAR(256),
     RecordCount BIGINT
  );

DECLARE @TableName NVARCHAR(256);
DECLARE @SchemaName NVARCHAR(256);
DECLARE @SQL NVARCHAR(MAX);
-- Cursor to iterate through all tables
DECLARE TableCursor CURSOR FOR
  SELECT s.name AS SchemaName,
         t.name AS TableName
  FROM   sys.tables t
         JOIN sys.schemas s
           ON t.schema_id = s.schema_id;

OPEN TableCursor;

FETCH NEXT FROM TableCursor INTO @SchemaName, @TableName;

WHILE @@FETCH_STATUS = 0
  BEGIN
      -- Build the query for each table
      SET @SQL = 'INSERT INTO #TableRecordCounts (TableName, RecordCount)
                SELECT ''' + @SchemaName + '.'
                 + @TableName + ''', COUNT(*) 
                FROM '
                 + QUOTENAME(@SchemaName) + '.'
                 + QUOTENAME(@TableName);

      -- Execute the query
      EXEC sp_executesql
        @SQL;

      FETCH NEXT FROM TableCursor INTO @SchemaName, @TableName;
  END;

CLOSE TableCursor;

DEALLOCATE TableCursor;

-- Retrieve results
SELECT *
FROM   #TableRecordCounts
ORDER  BY TableName DESC;

-- Retrieve results
SELECT *
FROM   #TableRecordCounts
ORDER  BY RecordCount DESC;

-- Drop the temporary table
DROP TABLE #TableRecordCounts;

GO 
