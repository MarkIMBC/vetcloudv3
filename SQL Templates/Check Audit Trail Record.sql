DECLARE @ID_CurrentObject INT = 276734
DECLARE @ID_AuditTrail INT = 324117

SELECT ID, 
       ID_CurrentObject,
       DateCreated,
       Description,
       Model,
       h.[User],
       AuditType
FROm   vAuditTrail h
where  ID_CurrentObject = @ID_CurrentObject

SELECT ID,
       Date,
       Code,
	   ID_Company,
       DateCreated,
       DateModified,
       DateApproved,
       DateCanceled
FROM   tBillingInvoice
where  ID = @ID_CurrentObject 

SELECT ID_AuditTrail,
       Name,
       OldValue,
       NewValue,
       ModelProperty,
       Name_AuditTrailType
FROm   vAuditTrail_Detail
where  ID_AuditTrail = @ID_AuditTrail  AND Name = 'ID_Item'
Order by ID
