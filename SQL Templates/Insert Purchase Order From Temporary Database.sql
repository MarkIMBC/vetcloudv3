IF COL_LENGTH('tPurchaseOrder', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPurchaseOrder',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF COL_LENGTH('tEmployee', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tEmployee',
        'tempID',
        1
  END

exec _pRefreshAllViews

GO

IF OBJECT_ID(N'Temp-2022-03-28-AuditTrail-PurchaseOrder-Added', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-AuditTrail-PurchaseOrder-Added]

GO

IF OBJECT_ID(N'Temp-2022-03-28-PurchaseOrder', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-PurchaseOrder]

GO

Declare @NotYetInserted_Temp_ID_PurchaseOrder TABLE
  (
     Temp_ID_PurchaseOrder VARCHAR(MAX)
  )

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [dbo].[Temp-2022-03-28-AuditTrail-PurchaseOrder-Added]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
       LEFT JOIN _tModel _model
              on _auditTrail.ID_Model = _model.Oid
WHERE  Date >= '2022-03-28 11:00:00'
       AND Description like '%added%'
       AND _model.TableName = 'tPurchaseOrder'
Order  by Date

SELECT company.ID                                 Main_ID_Company,
       company.Guid                               Guid_ID_Company,
       company.Name                               Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-PurchaseOrder-'
       + Convert(Varchar(MAX), PurchaseOrder_.ID) Temp_ID_PurchaseOrder,
       PurchaseOrder_.*
INTO   [dbo].[Temp-2022-03-28-PurchaseOrder]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vPurchaseOrder PurchaseOrder_
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on PurchaseOrder_.ID_Company = company.ID
where  PurchaseOrder_.DateCreated > '2022-03-28 10:00:00'

INSERT @NotYetInserted_Temp_ID_PurchaseOrder
SELECT tempPurchaseOrder.Temp_ID_PurchaseOrder
FROm   [dbo].[Temp-2022-03-28-PurchaseOrder] tempPurchaseOrder

DELETE FROM @NotYetInserted_Temp_ID_PurchaseOrder
WHERE  Temp_ID_PurchaseOrder IN (SELECT tempID
                                 FROM   tPurchaseOrder)

INSERT tPurchaseOrder
       (ID_Company,
        tempID,
        ID_Supplier,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        TotalQuantity,
        TotalVAT,
        TotalGrossAmount,
        TotalNetAmount,
        TotalVatAmount,
        DocumentDate,
        ID_FilingStatus,
        ID_SubmittedBy,
        DateSubmitted,
        ID_TaxScheme,
        DateApproved,
        ID_ApprovedBy,
        DateCancelled,
        ID_CancelledBy,
        GrossAmount,
        VatAmount,
        NetAmount,
        Date,
        ID_CanceledBy,
        DateCanceled,
        ServingStatus_ID_FilingStatus,
        DiscountRate,
        DiscountAmount,
        IsComputeDiscountRate,
        SubTotal,
        TotalAmount,
        Comment)
SELECT tempPurchaseOrder.ID_Company,
       tempPurchaseOrder.Temp_ID_PurchaseOrder,
       tempPurchaseOrder.ID_Supplier,
       tempPurchaseOrder.DateCreated,
       tempPurchaseOrder.DateModified,
       tempPurchaseOrder.ID_CreatedBy,
       tempPurchaseOrder.ID_LastModifiedBy,
       tempPurchaseOrder.TotalQuantity,
       tempPurchaseOrder.TotalVAT,
       tempPurchaseOrder.TotalGrossAmount,
       tempPurchaseOrder.TotalNetAmount,
       tempPurchaseOrder.TotalVatAmount,
       tempPurchaseOrder.DocumentDate,
       tempPurchaseOrder.ID_FilingStatus,
       tempPurchaseOrder.ID_SubmittedBy,
       tempPurchaseOrder.DateSubmitted,
       tempPurchaseOrder.ID_TaxScheme,
       tempPurchaseOrder.DateApproved,
       tempPurchaseOrder.ID_ApprovedBy,
       tempPurchaseOrder.DateCancelled,
       tempPurchaseOrder.ID_CancelledBy,
       tempPurchaseOrder.GrossAmount,
       tempPurchaseOrder.VatAmount,
       tempPurchaseOrder.NetAmount,
       tempPurchaseOrder.Date,
       tempPurchaseOrder.ID_CanceledBy,
       tempPurchaseOrder.DateCanceled,
       tempPurchaseOrder.ServingStatus_ID_FilingStatus,
       tempPurchaseOrder.DiscountRate,
       tempPurchaseOrder.DiscountAmount,
       tempPurchaseOrder.IsComputeDiscountRate,
       tempPurchaseOrder.SubTotal,
       tempPurchaseOrder.TotalAmount,
       + 'Imported from VetCloudTemp March 28, 2022'
       + ' Reference Code ('
       + tempPurchaseOrder.Code + ')'
       + CASE
           WHEN LEN(ISNULL(tempPurchaseOrder.Comment, '')) > 0 THEN CHAR(13) + tempPurchaseOrder.Comment
           ELSE ''
         END Comment
FROm   [dbo].[Temp-2022-03-28-PurchaseOrder] tempPurchaseOrder
       inner join @NotYetInserted_Temp_ID_PurchaseOrder unInsertedTempPurchaseOrder
               on tempPurchaseOrder.Temp_ID_PurchaseOrder = unInsertedTempPurchaseOrder.Temp_ID_PurchaseOrder

GO

exec _pRefreshAllViews

GO

GO

CReate OR
ALTER PROC _tempGeneratePurchaseOrder(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_PurchaseOrder TABLE
        (
           RowID            INT,
           ID_PurchaseOrder INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_PurchaseOrder
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tPurchaseOrder
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_PurchaseOrder

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_PurchaseOrder INT = 0

            SELECT @ID_PurchaseOrder = ID_PurchaseOrder
            FROM   @IDs_PurchaseOrder
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_PurchaseOrder]
              @ID_PurchaseOrder,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for PurchaseOrder IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany
Order  by Name

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGeneratePurchaseOrder
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGeneratePurchaseOrder

GO 
