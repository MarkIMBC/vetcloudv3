GO

--exec _pBackUpDatabase

GO

ALTER TABLE [dbo].[tPurchaseOrder]
  DROP CONSTRAINT [FKtPurchaseOrder_ID_Supplier]

GO

ALTER TABLE [dbo].[tInventoryTrail]
  DROP CONSTRAINT [FK_tInventoryTrail_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tEmployee]
  DROP CONSTRAINT [FKtEmployee_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tUser]
  DROP CONSTRAINT [FKtUser_ID_Employee]

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  DROP CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  DROP CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice]

--ALTER TABLE [dbo].[tPurchaseOrder_Detail] DROP CONSTRAINT [FKtPurchaseOrder_Detail_ID_Item]
--GO
DECLARE @IDs_COmpany typIntList

INSERT @IDs_COmpany
SELECT ID
FROm   vCompanyInactive

--- Item Cost and Item Unit Price -------
TRUNCATE TABLE [dbo].[tItem_UnitCostLog]

TRUNCATE TABLE [dbo].[tItem_UnitPriceLog]

------------------------ Document Series ----------------------------
DECLARE @IDs_DocumentSeries typIntlist

INSERT @IDs_DocumentSeries
SELECT hed.ID
FROM   tDocumentSeries hed
       inner join @IDs_COmpany ids_c
               on hed.ID_Company = ids_c.ID

DELETE FROM tDocumentSeries
where  ID IN (SELECT ID
              FROM   @IDs_DocumentSeries)

------------------------ Document Series ----------------------------
DECLARE @IDs_Supplier typIntlist

INSERT @IDs_Supplier
SELECT hed.ID
FROM   tSupplier hed
       inner join @IDs_COmpany ids_c
               on hed.ID_Company = ids_c.ID

DELETE FROM tSupplier
where  ID IN (SELECT ID
              FROM   @IDs_Supplier)

------------- ReceivingReport -----------------
DECLARE @IDs_ReceivingReport typIntList

INSERT @IDs_ReceivingReport
SELECT _po.ID
FROm   tReceivingReport _po
       inner join @IDs_COmpany idsCompany
               on _po.ID_Company = idsCompany.ID

DELETE FROM tReceivingReport_Detail
WHERE  ID_ReceivingReport IN (SELECT ID
                              FROM   @IDs_ReceivingReport)

DELETE FROM tReceivingReport
WHERE  ID IN (SELECT ID
              FROM   @IDs_ReceivingReport)

------------- Purchase Order -----------------
DECLARE @IDs_PurchaseOrder typIntList

INSERT @IDs_PurchaseOrder
SELECT _po.ID
FROm   tPurchaseOrder _po
       inner join @IDs_COmpany idsCompany
               on _po.ID_Company = idsCompany.ID

DELETE FROM tPurchaseOrder_Detail
WHERE  ID_PurchaseOrder IN (SELECT ID
                            FROM   @IDs_PurchaseOrder)

DELETE FROM tPurchaseOrder
WHERE  ID IN (SELECT ID
              FROM   @IDs_PurchaseOrder)

------------- Patient Confinement -----------------
DECLARE @IDs_Patient_Confinement typIntList

INSERT @IDs_Patient_Confinement
SELECT hed.ID
FROm   tPatient_Confinement hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tPatient_Confinement_ItemsServices
WHERE  ID_Patient_Confinement IN (SELECT ID
                                  FROM   @IDs_Patient_Confinement)

DELETE FROM tPatient_Confinement_Patient
WHERE  ID_Patient_Confinement IN (SELECT ID
                                  FROM   @IDs_Patient_Confinement)

DELETE FROM tPatient_Confinement
WHERE  ID IN (SELECT ID
              FROM   @IDs_Patient_Confinement)

------------- SOAP -----------------
DECLARE @IDs_SOAP typIntList

INSERT @IDs_SOAP
SELECT _soap.ID
FROm   tPatient_SOAP _soap
       inner join @IDs_COmpany idsCompany
               on _soap.ID_Company = idsCompany.ID

DELETE FROM tPatient_SOAP_Prescription
WHERE  ID_Patient_SOAP IN (SELECT ID
                           FROM   @IDs_SOAP)

DELETE FROM tPatient_SOAP_Treatment
WHERE  ID_Patient_SOAP IN (SELECT ID
                           FROM   @IDs_SOAP)

DELETE FROM tPatient_SOAP_Plan
WHERE  ID_Patient_SOAP IN (SELECT ID
                           FROM   @IDs_SOAP)

DELETE FROM tPatient_SOAP
WHERE  ID IN (SELECT ID
              FROM   @IDs_SOAP)

------------------ Patient Wellness ----------------------------------
DECLARE @IDs_Patient_Wellness typIntList

INSERT @IDs_Patient_Wellness
SELECT hed.ID
FROm   tPatient_Wellness hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tPatient_Wellness_Schedule
WHERE  ID_Patient_Wellness IN (SELECT ID
                               FROM   @IDs_Patient_Wellness)

DELETE FROM tPatient_Wellness_Detail
WHERE  ID_Patient_Wellness IN (SELECT ID
                               FROM   @IDs_Patient_Wellness)

DELETE FROM tPatient_Wellness
WHERE  ID IN (SELECT ID
              FROM   @IDs_Patient_Wellness)

------------------ Sav ----------------------------------
DECLARE @IDs_Patient typIntList

INSERT @IDs_Patient
SELECT hed.ID
FROm   tPatient hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tPatient
WHERE  ID IN (SELECT ID
              FROM   @IDs_Patient)

---------------------------------Payment Transaction-----------------
DECLARE @IDs_PaymentTransaction typIntList

INSERT @IDs_PaymentTransaction
SELECT hed.ID
FROm   tPaymentTransaction hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tPaymentTransaction
WHERE  ID IN (SELECT ID
              FROM   @IDs_PaymentTransaction)

----------------------------- Billing INvoice -----------------
DECLARE @IDs_BillingInvoice typIntList

INSERT @IDs_BillingInvoice
SELECT hed.ID
FROm   tBillingInvoice hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tBillingInvoice_Detail
WHERE  ID_BillingInvoice IN (SELECT ID
                             FROM   @IDs_BillingInvoice)

DELETE FROM tBillingInvoice_Patient
WHERE  ID_BillingInvoice IN (SELECT ID
                             FROM   @IDs_BillingInvoice)

DELETE FROM tBillingInvoice
WHERE  ID IN (SELECT ID
              FROM   @IDs_BillingInvoice)

----------------------------------------------------
DECLARE @IDs_Client typIntList
DECLARE @IDs_Client_User typIntList

INSERT @IDs_Client
SELECT client.ID
FROm   tClient client
       inner join @IDs_COmpany idsCompany
               on client.ID_Company = idsCompany.ID

Insert @IDs_Client_User
SELECT client.ID_User
FROm   tClient client
       inner join @IDs_COmpany idsCompany
               on client.ID_Company = idsCompany.ID

DELETE FROM tClient
WHERE  ID IN (SELECT ID
              FROM   @IDs_Client)

SELECT *
FROM   @IDs_Client_User

DELETE tUser
WHERE  ID IN (SELECT ID
              FROm   @IDs_Client_User)
       AND ID_employee IS NULL

----------------------------------------------------
INSERT @IDs_Patient
SELECT hed.ID
FROm   tPatient hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tPatient
WHERE  ID IN (SELECT ID
              FROM   @IDs_Patient)

----------------------------------------------------
DECLARE @IDs_InventoryTrail typIntlIst

INSERT @IDs_InventoryTrail
SELECT hed.ID
FROm   tInventoryTrail hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tInventoryTrail
WHERE  ID IN (SELECT ID
              FROM   @IDs_InventoryTrail)

----------------------------------------------------
DECLARE @IDs_Item typIntList

INSERT @IDs_Item
SELECT hed.ID
FROm   tItem hed
       inner join @IDs_COmpany idsCompany
               on hed.ID_Company = idsCompany.ID

DELETE FROM tInventoryTrail
WHERE  ID_Item IN (SELECT ID
                   FROM   @IDs_Item)

DELETE FROM tItem_Supplier
WHERE  ID_Item IN (SELECT ID
                   FROM   @IDs_Item)

DELETE FROM tItem
WHERE  ID IN (SELECT ID
              FROM   @IDs_Item)

------------------ Sav ----------------------------------
DECLARE @IDs_AuditTrail typIntList

INSERT @IDs_AuditTrail
SELECT hed.ID
FROm   tAuditTrail hed
       inner join vUSer _user
               on _user.ID = hed.ID_User
       inner join @IDs_COmpany idsCompany
               on _user.ID_Company = idsCompany.ID

DELETE FROM tAuditTrail_Detail
WHERE  ID_AuditTrail IN (SELECT ID
                         FROM   @IDs_AuditTrail)

DELETE FROM tAuditTrail
WHERE  ID IN (SELECT ID
              FROM   @IDs_AuditTrail)

------------------------ User ----------------------------
DECLARE @IDs_User typIntlist

INSERT @IDs_User
SELECT _user.ID
FROM   vUser _user
       inner join @IDs_COmpany ids_c
               on _user.ID_Company = ids_c.ID

DELETE FROM tUser
where  ID IN (SELECT ID
              FROM   @IDs_User)

------------------------ Employee ----------------------------
DECLARE @IDs_Employee typIntlist

INSERT @IDs_Employee
SELECT hed.ID
FROM   tEmployee hed
       inner join @IDs_COmpany ids_c
               on hed.ID_Company = ids_c.ID

DELETE FROM tEmployee
where  ID IN (SELECT ID
              FROM   @IDs_Employee)

-------------------------------------------------------------
GO

ALTER TABLE [dbo].[tPurchaseOrder]
  WITH CHECK ADD CONSTRAINT [FKtPurchaseOrder_ID_Supplier] FOREIGN KEY([ID_Supplier]) REFERENCES [dbo].[tSupplier] ([ID])

GO

ALTER TABLE [dbo].[tPurchaseOrder]
  CHECK CONSTRAINT [FKtPurchaseOrder_ID_Supplier]

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  WITH CHECK ADD CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice]) REFERENCES [dbo].[tBillingInvoice] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice_Detail]
  CHECK CONSTRAINT [FKtBillingInvoice_Detail_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  WITH CHECK ADD CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice] FOREIGN KEY([ID_BillingInvoice]) REFERENCES [dbo].[tBillingInvoice] ([ID])

GO

ALTER TABLE [dbo].[tBillingInvoice_Patient]
  CHECK CONSTRAINT [FKtBillingInvoice_Patient_ID_BillingInvoice]

GO

ALTER TABLE [dbo].[tEmployee]
  WITH NOCHECK ADD CONSTRAINT [FKtEmployee_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tEmployee]
  CHECK CONSTRAINT [FKtEmployee_ID_LastModifiedBy]

GO

ALTER TABLE [dbo].[tInventoryTrail]
  WITH CHECK ADD CONSTRAINT [FK_tInventoryTrail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])

GO

ALTER TABLE [dbo].[tInventoryTrail]
  CHECK CONSTRAINT [FK_tInventoryTrail_ID_CreatedBy]

GO

ALTER TABLE [dbo].[tUser]
  WITH NOCHECK ADD CONSTRAINT [FKtUser_ID_Employee] FOREIGN KEY([ID_Employee]) REFERENCES [dbo].[tEmployee] ([ID])

GO

ALTER TABLE [dbo].[tUser]
  CHECK CONSTRAINT [FKtUser_ID_Employee]

GO 

TRUNCATE TABLE tUserSession