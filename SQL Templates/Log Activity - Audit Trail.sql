DECLARE @IDs_User TYPINTLIST
DECLARE @DateStart DATE = '2020-10-01'
DECLARE @DateEnd DATE = '2022-10-31'

INSERT @IDs_User
SELECT ID
FROM   vUser
WHERE  ID_Company = 246

SELECT _audit.Date,
       ISNULL(_model.Caption, _model.Name) Model,
       _user.Name_Employee                 Name,
       _audit.Description,
       auditDet.ModelProperty,
       auditDet.Name,
       OldValue,
       NewValue
FROM   vAuditTrail _audit
       INNER JOIN @IDs_User idsUser
               ON _audit.ID_User = idsUser.ID
       INNER JOIN tAuditTrail_Detail auditDet
               ON auditDet.ID_AuditTrail = _audit.ID
       INNER JOIN _tModel _model
               ON _model.Oid = _audit.ID_Model
       INNER JOIN vUser _user
               ON _user.ID = idsUser.ID
WHERE  CONVERT(DATE, _audit.Date) BETWEEN @DateStart AND @DateEnd
       and Convert(VARCHAR(MAX), auditDet.OldValue) = '470315'
       AND auditDet.Name = 'ID_Patient'
ORDER  BY _audit.Date DESC,
          ModelProperty,
          auditDet.Name 
