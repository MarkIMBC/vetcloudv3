IF OBJECT_ID(N'AuditTrail-2022-03-28-item', N'U') IS NOT NULL
  DROP TABLE [dbo].[AuditTrail-2022-03-28-item];

GO

IF OBJECT_ID(N'Modified-Record-2022-03-28-item', N'U') IS NOT NULL
  DROP TABLE [dbo].[Modified-Record-2022-03-28-item];

GO

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
INTO   [AuditTrail-2022-03-28-item]
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Model = 'item'
       and Description LIKE '%modified%'
       AND Date >= '2022-03-28 11:00:00'
Order  by Date DESC

SELECT mainitem.*
INTO   [Modified-Record-2022-03-28-item]
FROm   db_waterworksv1_live_temp_20220328_113414.dbo.titem mainitem
       inner join [AuditTrail-2022-03-28-item] auditTrailitem
               on mainitem.ID = auditTrailitem.ID_CurrentObject
                  and mainitem.ID_Company = auditTrailitem.ID_Company

SELECT mainitem.Name,
       temporaryitem.Name,
       mainitem.ID_ItemType,
       temporaryitem.ID_ItemType,
       mainitem.ID_ItemCategory,
       temporaryitem.ID_ItemCategory,
       mainitem.MinInventoryCount,
       temporaryitem.MinInventoryCount,
       mainitem.MaxInventoryCount,
       temporaryitem.MaxInventoryCount,
       mainitem.UnitCost,
       temporaryitem.UnitCost,
       mainitem.UnitPrice,
       temporaryitem.UnitPrice,
       mainitem.OtherInfo_DateExpiration,
       temporaryitem.OtherInfo_DateExpiration,
       mainitem.BarCode,
       temporaryitem.BarCode,
       mainitem.CustomCode,
       temporaryitem.CustomCode,
       mainitem._tempSupplier,
       temporaryitem._tempSupplier
FROm   dbo.titem mainitem
       inner join [Modified-Record-2022-03-28-item] temporaryitem
               on mainitem.ID = temporaryitem.ID
                  and mainitem.ID_Company = temporaryitem.ID_Company

Update tItem
SET    Name = temporaryItem.Name
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.Name <> temporaryItem.Name

Update tItem
SET    ID_ItemType = temporaryItem.ID_ItemType
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.ID_ItemType <> temporaryItem.ID_ItemType

Update tItem
SET    ID_ItemCategory = temporaryItem.ID_ItemCategory
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.ID_ItemCategory <> temporaryItem.ID_ItemCategory

Update tItem
SET    MinInventoryCount = temporaryItem.MinInventoryCount
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.MinInventoryCount <> temporaryItem.MinInventoryCount

Update tItem
SET    MaxInventoryCount = temporaryItem.MaxInventoryCount
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.MaxInventoryCount <> temporaryItem.MaxInventoryCount

Update tItem
SET    UnitCost = temporaryItem.UnitCost
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.UnitCost <> temporaryItem.UnitCost

Update tItem
SET    UnitPrice = temporaryItem.UnitPrice
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.UnitPrice <> temporaryItem.UnitPrice

Update tItem
SET    OtherInfo_DateExpiration = temporaryItem.OtherInfo_DateExpiration
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.OtherInfo_DateExpiration <> temporaryItem.OtherInfo_DateExpiration

Update tItem
SET    BarCode = temporaryItem.BarCode
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.BarCode <> temporaryItem.BarCode

Update tItem
SET    CustomCode = temporaryItem.CustomCode
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem.CustomCode <> temporaryItem.CustomCode

Update tItem
SET    _tempSupplier = temporaryItem._tempSupplier
FROm   dbo.tItem mainItem
       inner join [Modified-Record-2022-03-28-Item] temporaryItem
               on mainItem.ID = temporaryItem.ID
                  and mainItem.ID_Company = temporaryItem.ID_Company
WHERE  mainItem._tempSupplier <> temporaryItem._tempSupplier 
