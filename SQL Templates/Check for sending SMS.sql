DECLARE @DateForSending Date = '2024-07-03';

WITH CTE
     as (Select c.Name,
                DateAdd(day, -1, CONVERT(Date, _soap.DateStart)) DateForSending,
                'Appointment' Code,
                _soap.Name_Client,
                _soap.Name_Patient,
                _soap.DateStart,
                ''                                               Name_Item,
                _soap.Comment,
                IsSentSMS,
                DateSent
         FROM   vPatientAppointment _soap
                INNER JOIN vCompanyActive c
                        on c.ID = _soap.ID_Company
         WHERE  DateAdd(day, -1, CONVERT(Date, _soap.DateStart)) = @DateForSending
         UNION ALL
         Select c.Name,
                DateAdd(day, -1, CONVERT(Date, _plan.DateReturn)) DateForSending,
                _soap.Code,
                _soap.Name_Client,
                _soap.Name_Patient,
                _plan.DateReturn,
                Name_Item,
                _plan.Comment,
                IsSentSMS,
                DateSent
         FROM   vPatient_SOAP _soap
                INNER JOIN vPatient_SOAP_Plan _plan
                        on _soap.ID = _plan.ID_Patient_SOAP
                INNER JOIN vCompanyActive c
                        on c.ID = _soap.ID_Company
         WHERE  DateAdd(day, -1, CONVERT(Date, _plan.DateReturn)) = @DateForSending
         UNION ALL
         select c.Name,
                DateAdd(day, -1, CONVERT(Date, _plan.Date)) DateForSending,
                _soap.Code,
                _soap.Name_Client,
                _soap.Name_Patient,
                _plan.Date,
                '',
                _plan.Comment,
                IsSentSMS,
                DateSent
         FROM   vPatient_Wellness _soap
                INNER JOIN vPatient_Wellness_Schedule _plan
                        on _soap.ID = _plan.ID_Patient_Wellness
                INNER JOIN vCompanyActive c
                        on c.ID = _soap.ID_Company
         WHERE  DateAdd(day, -1, CONVERT(Date, _plan.Date)) = @DateForSending)


--For Unsent
SELECT *
FROM   CTE
WHERE  IsSentSMS = 1 -- uncomment ito kapag need mo lumabas ang mga nasent na sms
Order  by Name,
          DateForSending,
          Name_Client,
          Name_Patient 
