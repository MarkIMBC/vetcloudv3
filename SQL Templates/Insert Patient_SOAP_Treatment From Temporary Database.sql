IF COL_LENGTH('tPatient_SOAP_Treatment', 'tempID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP_Treatment',
        'tempID',
        1
  END

GO

IF OBJECT_ID(N'Temp-2022-09-14-Patient_SOAP_Treatment', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-09-14-Patient_SOAP_Treatment]

GO

Declare @NotYetInserted_Temp_ID_Patient_SOAP_Treatment TABLE
  (
     Temp_ID_Patient_SOAP_Treatment VARCHAR(MAX)
  )

SELECT company.Guid                                       Main_Guid_Company,
       company.ID                                         Main_ID_Company,
       company.Name                                       Name_Company,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP-'
       + Convert(Varchar(MAX), Patient_SOAP_.ID)          Temp_ID_Patient_SOAP,
       'Temp-2022-09-14-' + company.Guid
       + '-Patient_SOAP_Treatment-'
       + Convert(Varchar(MAX), Patient_SOAP_Treatment.ID) Temp_ID_Patient_SOAP_Treatment,
       Patient_SOAP_.Code                                 Code_Patient_SOAP,
       Patient_SOAP_Treatment.*
INTO   [dbo].[Temp-2022-09-14-Patient_SOAP_Treatment]
FROM   db_waterworksv1_server2.[dbo].vPatient_SOAP Patient_SOAP_
       INNER JOIN db_waterworksv1_server2.[dbo].tPatient_SOAP_Treatment Patient_SOAP_Treatment
               on Patient_SOAP_.ID = Patient_SOAP_Treatment.ID_Patient_SOAP
       inner join db_waterworksv1_server2.[dbo].tCompany company
               on Patient_SOAP_.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-09-14-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient_SOAP_.ID_Client)
                 and mainClient.ID_Company = Patient_SOAP_.ID_Company
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-09-14-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), Patient_SOAP_.ID_Patient)
                 and mainPatient.ID_Company = Patient_SOAP_.ID_Company
where  Patient_SOAP_.DateCreated > '2022-08-30 16:42:32.770' and company.GUID =  '56CD6D30-4877-47AA-9BFC-C407A7A532D3'

select * FROM [Temp-2022-09-14-Patient_SOAP_Treatment]

INSERT @NotYetInserted_Temp_ID_Patient_SOAP_Treatment
SELECT tempPatient_SOAP.Temp_ID_Patient_SOAP_Treatment
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Treatment] tempPatient_SOAP

DELETE FROM @NotYetInserted_Temp_ID_Patient_SOAP_Treatment
WHERE  Temp_ID_Patient_SOAP_Treatment IN (SELECT tempID
                                          FROM   tPatient_SOAP_Treatment)

SELECT DISTINCT Main_ID_Company,
                Name_Company,
                Code_Patient_SOAP
FROM   [Temp-2022-09-14-Patient_SOAP_Treatment]

INSERT tPatient_SOAP_Treatment
       (ID_Patient_SOAP,
        ID_Item,
        Quantity,
        UnitPrice,
        UnitCost,
        DateExpiration,
        Comment,
        tempID)
SELECT ISNULL(mainPatient_SOAP.ID, mainPatient_SOAP2.ID) Main_ID_Patient_SOAP,
       ISNULL(mainItem.ID, mainItem2.ID)                 Main_ID_Item,
       tempPatient_SOAP_Treatment.Quantity,
       tempPatient_SOAP_Treatment.UnitPrice,
       tempPatient_SOAP_Treatment.UnitCost,
       tempPatient_SOAP_Treatment.DateExpiration,
       tempPatient_SOAP_Treatment.Comment,
       tempPatient_SOAP_Treatment.Temp_ID_Patient_SOAP_Treatment
FROm   [dbo].[Temp-2022-09-14-Patient_SOAP_Treatment] tempPatient_SOAP_Treatment
       inner join @NotYetInserted_Temp_ID_Patient_SOAP_Treatment unInsertedTempPatient_SOAP_Treatment
               on tempPatient_SOAP_Treatment.Temp_ID_Patient_SOAP_Treatment = unInsertedTempPatient_SOAP_Treatment.Temp_ID_Patient_SOAP_Treatment
       ------------ SOAP ----------------
       LEFT JOIN tPatient_SOAP mainPatient_SOAP
              on mainPatient_SOAP.tempID = 'Temp-2022-09-14-'
                                           + tempPatient_SOAP_Treatment.Main_Guid_Company
                                           + '-Patient_SOAP-'
                                           + Convert(Varchar(MAX), tempPatient_SOAP_Treatment.ID_Patient_SOAP)
                 and mainPatient_SOAP.ID_Company = tempPatient_SOAP_Treatment.Main_ID_Company
       LEFT JOIN tPatient_SOAP mainPatient_SOAP2
              on mainPatient_SOAP2.ID = tempPatient_SOAP_Treatment.ID_Patient_SOAP
                 and mainPatient_SOAP2.ID_Company = tempPatient_SOAP_Treatment.Main_ID_Company
       ------------ SOAP END --------------------------
       /*                                  */
       ------------ Item ----------------
       LEFT JOIN tItem mainItem
              on mainItem.tempID = 'Temp-2022-09-14-'
                                   + tempPatient_SOAP_Treatment.Main_Guid_Company
                                   + '-Item-'
                                   + Convert(Varchar(MAX), tempPatient_SOAP_Treatment.ID_Item)
                 and mainItem.ID_Company = tempPatient_SOAP_Treatment.Main_ID_Company
       LEFT JOIN tItem mainItem2
              on mainItem2.ID = tempPatient_SOAP_Treatment.ID_Item
                 and mainItem2.ID_Company = tempPatient_SOAP_Treatment.Main_ID_Company
------------ Item END ----------------
/*                                  */
WHERE  1 = 1

GO


exec _pRefreshAllViews 
