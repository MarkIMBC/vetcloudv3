GO

GO

CREATE      OR
ALTER PROC pInsertEmployee(@LastName_Employee  VARCHAR(MAX),
                           @FirstName_Employee VARCHAR(MAX),
                           @NamePosition       VARCHAR(MAX),
                           @ID_Company         INT)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tEmployee
         WHERE  LastName = @LastName_Employee
                AND FirstName = @FirstName_Employee
                AND ID_Company = @ID_Company) = 0
        BEGIN
            DECLARE @ID_Position INT = 0

            exec pInsertPosition
              @NamePosition,
              1

            SELECT @ID_Position = ID
            FROM   tPosition
            where  name = @NamePosition

            INSERT INTO [dbo].[tEmployee]
                        ([IsActive],
                         [ID_CreatedBy],
                         [ID_LastModifiedBy],
                         ID_Company,
                         [DateCreated],
                         [DateModified],
                         [ID_Position],
                         [LastName],
                         [FirstName],
                         [tempID])
            SELECT 1,
                   1,
                   1,
                   @ID_Company,
                   GETDATE(),
                   GETDATE(),
                   @ID_Position,
                   @LastName_Employee,
                   @FirstName_Employee,
                   NEWID()
        END
  END

GO

GO

DECLARE @user TABLE
  (
     [ID_Company]    INT,
     [GUID_Company]  varchar(MAX),
     [Name_Company]  varchar(MAX),
     [LastName]      varchar(MAX),
     [FirstName]     varchar(MAX),
     [Name_Position] varchar(MAX),
     ID_Employee     INT,
     ID_User         INT,
     RowIndex        int IDENTITY(1, 1) PRIMARY KEY
  )
DECLARE @RowIndex INT = 0
DECLARE @ID_Company INT = 0
DECLARE @GUID_Company varchar(MAX) = ''
DECLARE @Name_Company varchar(MAX) = ''
DECLARE @LastName varchar(MAX) = ''
DECLARE @FirstName varchar(MAX) = ''
DECLARE @Name_Position varchar(MAX) = ''

INSERT @user
       ([ID_Company],
        [GUID_Company],
        [Name_Company],
        [LastName],
        [FirstName],
        [Name_Position])
select _company.ID,
       _petlinkUser.GUID_Company,
       _petlinkUser.Name_Company,
       [LastName],
       [FirstName],
       [Name_Position]
FROM   ForImport.[dbo].[20230513_1123_ImportPetlinkUser] _petlinkUser
       inner join vCompanyActive _company
               on _petlinkUser.GUID_Company = _company.Guid

DECLARE db_cursor CURSOR FOR
  SELECT [ID_Company],
         [GUID_Company],
         [Name_Company],
         [LastName],
         [FirstName],
         [Name_Position],
         RowIndex
  FROM   @user

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_Company,
                               @GUID_Company,
                               @Name_Company,
                               @LastName,
                               @FirstName,
                               @Name_Position,
                               @RowIndex

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Employee INT = 0
      DECLARE @ID_User INT = 0
      DECLARE @ID_UserRole INT = 0

      exec pInsertEmployee
        @LastName,
        @FirstName,
        @Name_Position,
        @ID_Company

      SELECT @ID_Employee = ID
      FROM   tEmployee
      WHERE  LastName = @LastName
             AND FirstName = @FirstName
             AND ID_Company = @ID_Company
             AND IsActive = 1

      IF(select COUNT(*)
         FROM   tUser
         where  ID_Employee = @ID_Employee) = 0
        BEGIN
            SET @FirstName = TRIM(@FirstName)
            SET @FirstName = REPLACE(@FirstName, ' ', '')
            SET @FirstName = REPLACE(@FirstName, ' ', '')

            exec pCreateUser
              @ID_Company,
              @ID_Employee,
              @FirstName
        END

      SELECT @ID_User = ID
      FROM   tUser
      where  ID_Employee = @ID_Employee

      IF(SELECT COUNT(*)
         FROM   tUser_Roles
         WHERE  ID_User = @ID_User
                and ID_UserRole = 29) = 0
        BEGIN
            INSERT INTO [dbo].[tUser_Roles]
                        ([IsActive],
                         [ID_User],
                         [ID_UserRole])
            SELECT 1,
                   @ID_User,
                   29
        END

      Update @user
      SET    ID_Employee = @ID_Employee,
             ID_User = @ID_User
      WHERE  RowIndex = @RowIndex

      FETCH NEXT FROM db_cursor INTO @ID_Company,
                                     @GUID_Company,
                                     @Name_Company,
                                     @LastName,
                                     @FirstName,
                                     @Name_Position,
                                     @RowIndex
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT ISNULL(temp.Name_Company, _user.Name_Company) Name_Company,
       LastName,
       FirstName,
       Name_Position,
       _user.Username,
       _user.Password
FROM   @user temp
       LEFT join vUser _user
              on temp.ID_User = _user.ID 
