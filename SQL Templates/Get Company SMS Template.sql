DECLARE @name VARCHAR(MAX) -- database name       
DECLARE @dateStart DateTime
DECLARE @dateEnd DateTime
DECLARE @table TABLE
  (
     ID           INT,
     Name         VARCHAR(MAX),
     IsActive     BIT,
     DateCreated  DateTime,
     Guid         VARCHAR(MAX),
	 SOAPPlanSMSMessage  VARCHAR(MAX),
     DatabaseName VARCHAR(MAX)
  )
DECLARE db_cursorppGetCompanyActive_VetCloudv3 CURSOR FOR
  SELECT name
  FROM   MASTER.dbo.sysdatabases
  WHERE  ( name LIKE 'db_vetcloudv3_server_'
            OR name LIKE 'db_vetcloudv3_%' )

OPEN db_cursorppGetCompanyActive_VetCloudv3

FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dateStart = GETDATE();

      BEGIN TRY
          INSERT @table
          exec( 'SELECT ID, Name, IsActive, DateCreated, GUID, SOAPPlanSMSMessage, '''+ @name +''' DatabaseName FROM ' + @name +'.dbo.vCompanyActive')

          SET @dateEnd = GETDATE();
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SET @dateEnd = GETDATE();
          SELECT @msg
      END CATCH

      FETCH NEXT FROM db_cursorppGetCompanyActive_VetCloudv3 INTO @name
  END

CLOSE db_cursorppGetCompanyActive_VetCloudv3

DEALLOCATE db_cursorppGetCompanyActive_VetCloudv3

SELECT DatabaseName,
       COunt(*) TotalActiveCompany
FROM   @table
GRoup  by DatabaseName

SELECT DISTINCT *
FROM   @table
Order  by Name 
