DEclare @SOurce_GUID_Company VARCHAR(MAX) = 'C604D48E-9608-4567-9EFE-82EF88DCC962'
DEclare @Destination_GUID_Company VARCHAR(MAX) = '294BDCDE-5A58-41F6-A6E2-EECAB9344212'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @SOurce_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Source Company does not exist.', 1;
  END

SELECT Name Source_Company,
       *
FROm   tCompany
WHERE  GUID = @SOurce_GUID_Company

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @Destination_GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Destination Company does not exist.', 1;
  END

SELECT Name Destination_Company,
       *
FROm   tCompany
WHERE  GUID = @Destination_GUID_Company

DECLARE @Source_ID_Company INT
DECLARE @Destination_ID_Company INT
DECLARE @Source_Name_Company VARCHAR(MAX)
DECLARE @Destination_Name_Company VARCHAR(MAX)

SELECt @Source_ID_Company = ID,
       @Source_Name_Company = Name
FROm   tCompany
WHERE  Guid = @SOurce_GUID_Company

SELECt @Destination_ID_Company = ID,
       @Destination_Name_Company = Name
FROm   tCompany
WHERE  Guid = @Destination_GUID_Company

-----------------------------------------------------
DECLARE @import TABLE
  (
     [ID]                            [int],
     [Name]                          [varchar](200) NULL,
     [IsActive]                      [bit] NULL,
     [Comment]                       [varchar](max) NULL,
     [DateCreated]                   [datetime] NULL,
     [DateModified]                  [datetime] NULL,
     [ID_CreatedBy]                  [int] NULL,
     [ID_LastModifiedBy]             [int] NULL,
     [FirstName]                     [varchar](300) NULL,
     [LastName]                      [varchar](300) NULL,
     [MiddleName]                    [varchar](300) NULL,
     [ID_Gender]                     [int] NULL,
     [Email]                         [varchar](300) NULL,
     [DateBirth]                     [datetime] NULL,
     [FullAddress]                   [varchar](300) NULL,
     [ID_Country]                    [int] NULL,
     [ContactNumber]                 [varchar](300) NULL,
     [ID_Company]                    [int] NULL,
     [Species]                       [varchar](300) NULL,
     [Source_ID_Client]              [int] NULL,
     [ID_Client]                     [int] NULL,
     [tempID_Client]                 [varchar](300) NULL,
     [Name_Client]                   [varchar](300),
     [IsNeutered]                    [bit] NULL,
     [IsDeceased]                    [bit] NULL,
     [Old_patient_id]                [int] NULL,
     [AnimalWellness]                [varchar](7000) NULL,
     [DateDeceased]                  [datetime] NULL,
     [DateLastVisited]               [datetime] NULL,
     [CurrentCreditAmount]           [decimal](18, 4) NULL,
     [ProfileImageFile]              [varchar](300) NULL,
     [CustomCode]                    [varchar](300) NULL,
     [WaitingStatus_ID_FilingStatus] [int] NULL,
     [Microchip]                     [varchar](300) NULL,
     [tempID]                        [varchar](300) NULL,
     [Color]                         [varchar](300) NULL,
     [Idiosyncrasies]                [varchar](300) NULL
  )

INSERT INTO @import
            ([Name],
             [IsActive],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [FirstName],
             [LastName],
             [MiddleName],
             [ID_Gender],
             [Email],
             [DateBirth],
             [FullAddress],
             [ID_Country],
             [ContactNumber],
             [ID_Company],
             [Species],
             [Source_ID_Client],
             [Name_Client],
             [tempID_Client],
             [IsNeutered],
             [IsDeceased],
             [Old_patient_id],
             [AnimalWellness],
             [DateDeceased],
             [DateLastVisited],
             [CurrentCreditAmount],
             [ProfileImageFile],
             [CustomCode],
             [WaitingStatus_ID_FilingStatus],
             [Microchip],
             [tempID],
             [Color],
             [Idiosyncrasies])
SELECT patient. [Name],
       patient. [IsActive],
       patient. [Comment],
       patient. [DateCreated],
       patient. [DateModified],
       patient. [ID_CreatedBy],
       patient. [ID_LastModifiedBy],
       patient. [FirstName],
       patient. [LastName],
       patient. [MiddleName],
       patient. [ID_Gender],
       patient. [Email],
       patient. [DateBirth],
       patient. [FullAddress],
       patient. [ID_Country],
       patient. [ContactNumber],
       @Destination_ID_Company,
       patient. [Species],
       patient. ID_Client [Source_ID_Client],
       [Name_Client],
       'From-' + @SOurce_GUID_Company + '-' + 'Client-'
       + CONVERT(varchar(MAX), patient.ID_Client ),
       patient. [IsNeutered],
       patient. [IsDeceased],
       patient. [Old_patient_id],
       patient. [AnimalWellness],
       patient. [DateDeceased],
       patient. [DateLastVisited],
       patient. [CurrentCreditAmount],
       patient. [ProfileImageFile],
       patient. [CustomCode],
       patient. [WaitingStatus_ID_FilingStatus],
       patient. [Microchip],
       'From-' + @SOurce_GUID_Company + '-' + 'Patient-'
       + CONVERT(varchar(MAX), patient.ID ),
       patient. [Color],
       patient. [Idiosyncrasies]
FROM   vPatient patient
Where  patient.ID_Company = @Source_ID_Company
       and IsActive = 1

Update @import
SET    ID = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.tempID = patient.tempID
where  patient.ID_Company = @Destination_ID_Company and patient.IsActive = 1

Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.tempID_Client = client.tempID
where  client.ID_Company = @Destination_ID_Company and client.IsActive = 1

INSERT INTO [dbo].[tPatient]
            ([Name],
             [IsActive],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [FirstName],
             [LastName],
             [MiddleName],
             [ID_Gender],
             [Email],
             [DateBirth],
             [FullAddress],
             [ID_Country],
             [ContactNumber],
             [ID_Company],
             [Species],
             [IsNeutered],
             [IsDeceased],
             [Old_patient_id],
             [AnimalWellness],
             [DateDeceased],
             [DateLastVisited],
             [CurrentCreditAmount],
             [ProfileImageFile],
             [CustomCode],
             [WaitingStatus_ID_FilingStatus],
             [Microchip],
             [tempID],
             [Color],
             [Idiosyncrasies])
SELECT [Name],
       [IsActive],
       [Comment],
       [DateCreated],
       [DateModified],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [FirstName],
       [LastName],
       [MiddleName],
       [ID_Gender],
       [Email],
       [DateBirth],
       [FullAddress],
       [ID_Country],
       [ContactNumber],
       [ID_Company],
       [Species],
       [IsNeutered],
       [IsDeceased],
       [Old_patient_id],
       [AnimalWellness],
       [DateDeceased],
       [DateLastVisited],
       [CurrentCreditAmount],
       [ProfileImageFile],
       [CustomCode],
       [WaitingStatus_ID_FilingStatus],
       [Microchip],
       [tempID],
       [Color],
       [Idiosyncrasies]
FROM   @import
where  iD IS NULL

Update @import
SET    ID = patient.ID
FROM   @import import
       inner join tPatient patient
               on import.tempID = patient.tempID
where  patient.ID_Company = @Destination_ID_Company and patient.IsActive = 1

INSERT INTO [dbo].[tClient]
            ([Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ContactNumber],
             [Email],
             [Address],
             [ContactNumber2],
             [Old_client_id],
             [tempID],
             [DateLastVisited],
             [CurrentCreditAmount],
             [CustomCode])
SELECT Distinct client.[Name],
       client.[IsActive],
       @Destination_ID_Company,
       client.[Comment],
       client.[DateCreated],
       client.[DateModified],
       client.[ID_CreatedBy],
       client.[ID_LastModifiedBy],
       client.[ContactNumber],
       client.[Email],
       client.[Address],
       client.[ContactNumber2],
       client.[Old_client_id],
       import.tempID_Client,
       client.[DateLastVisited],
       client.[CurrentCreditAmount],
       client.[CustomCode]
FROM   @import import
       inner join tClient client
               on import.Source_ID_Client = client.ID
where  client.ID_Company = @Source_ID_Company
       and client.IsActive = 1
       and import.ID_Client IS NULL

Update @import
SET    ID_Client = client.ID
FROM   @import import
       inner join tClient client
               on import.tempID_Client = client.tempID
where  client.ID_Company = @Destination_ID_Company and client.IsActive = 1

SELECT ID_Client,
       Name_Client,
       ID   ID_Patient,
       Name Name_Patient
FROM   @import
Order by Name_Client

GO 
