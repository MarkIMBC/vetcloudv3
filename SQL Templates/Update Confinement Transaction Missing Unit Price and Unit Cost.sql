GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO

SELECT *
Into   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice
FROm   (SELECT DISTINCt company.Name,
                        ID_Patient_Confinement,
                        conf.Date
        FROM   vPatient_Confinement_ItemsServices confItem
               Inner join tPatient_Confinement conf
                       on conf.ID = ID_Patient_Confinement
               Inner join vCompanyActive company
                       on company.ID = conf.ID_Company
               INNER JOIN tItem item
                       on confItem.ID_Item = item.ID
        WHERE  confItem.UnitPrice IS NULL
               and item.UnitPrice > 0
               and ( ISNULL(confItem.ID_Patient_SOAP_Prescription, 0) > 0
                      OR ISNULL(confItem.ID_Patient_SOAP_Treatment, 0) > 0 )
               and ID_FilingStatus IN ( 14, 15 )
        UNION ALL
        SELECT company.Name,
               ID_Patient_Confinement,
               billing.Date
        FROM   tBillingInvoice billing
               Inner join vBillingInvoice_Detail billingItem
                       on billing.ID = billingItem.ID_BillingInvoice
               Inner join vCompanyActive company
                       on company.ID = billing.ID_Company
               INNER JOIN tItem item
                       on billingItem.ID_Item = item.ID
        WHERE  ISNULL(billingItem.UnitPrice, 0) = 0
               and item.UnitPrice > 0
               AND ID_Patient_Confinement_ItemsServices IS NOT NULL
               AND ID_FilingStatus = 1) tbl
WHERE  YEAR(Date) = 2022
ORDER  BY Name,
          Date DESC

GO

SELECT *
FROM   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO

DECLARE @IDs_Patient_Confinement TYPINTLIST

INSERT @IDs_Patient_Confinement
SELECT Distinct ID_Patient_Confinement
FROM   #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice
WHERE  name NOT IN ( 'master', 'model', 'msdb', 'tempdb' )

--exec pModel_AfterSaved_Patient_Confinement_Computation
--  @IDs_Patient_Confinement
--EXEC dbo.pUpdateBillingInvoiceItemsByPatientConfinement
--  @IDs_Patient_Confinement
GO

IF OBJECT_ID('tempdb..#PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice') IS NOT NULL
  DROP TABLE #PatientConfinementtoFixpReUpdateCOnfinementRecordIUNitPrice

GO 
