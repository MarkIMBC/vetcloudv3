  GO
CREATE OR ALTER PROC [dbo].[_temp_pGetSendSOAPPlan](@DateSent      Date,  
                                    @IsSMSSent Bit = NULL)  
AS  
  BEGIN  
      DECLARE @Success BIT = 1;  
      DECLARE @SMSSent Table  
        (  
           IsSMSSent bit  
        )  
  
      if @IsSMSSent IS NULL  
        INSERT @SMSSent  
        VALUES (0),  
               (1)  
      ELSE  
        INSERT @SMSSent  
        VALUES (@IsSMSSent)  
  
      SET @DateSent = ISNULL(@DateSent, GETDATE())  
  
      SELECT '_',  
             '' AS summary,  
             '' AS records;  
  
      Declare @record TABLE  
        (  
           ID_Company           INT,  
           Name_Company         VARCHAR(MAX),  
           Name_Client          VARCHAR(MAX),  
           ContactNumber_Client VARCHAR(MAX),  
           DateReturn           DATETime,  
           Name_Item            VARCHAR(MAX),  
           Comment              VARCHAR(MAX),  
           Message              VARCHAR(MAX),  
           DateSending          DATETime,  
		      DateSent          DATETime  ,
           ID_Patient_SOAP_Plan INT,  
           DateCreated          DATETime   
		
        )  
  
      INSERT @record  
      SELECT c.ID                                                                                                                                                                                   ID_Company,  
             c.Name                                                                                                                                                                                 Name_Company,  
             client.Name                                                                                                                                                                            Name_Client,  
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,  
             soapPlan.DateReturn,  
             soapPlan.Name_Item,  
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,  
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, ISNULL(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,  
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,  
			 DateSent,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,  
             patientSOAP.DateCreated  
      FROM   dbo.tPatient_SOAP patientSOAP  
             LEFT JOIN dbo.tPatient patient  
                    ON patient.ID = patientSOAP.ID_Patient  
             LEFT JOIN dbo.tClient client  
                    ON client.ID = patient.ID_Client  
             LEFT JOIN tCompany c  
                    ON c.iD = patientSOAP.ID_Company  
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan  
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID  
      WHERE  ISNULL(soapPlan.IsSentSMS, 0) IN (1)  
             AND  ( CONVERT(DATE,soapPlan.DateSent) = CONVERT(DATE, @DateSent) )  

      ORDER  BY c.Name  
  
      SELECT @Success Success;  
  
      SELECT 
             tbl.Name_Company,  
             Count(*)                          Count,  
             SUM(CASE  
                   WHEN LEN(tbl.Message) <= 160 THEN 1  
                   ELSE  
                     CASE  
                       WHEN LEN(tbl.Message) <= 306 THEN 2  
                       ELSE  
                         CASE  
                           WHEN LEN(tbl.Message) <= 459 THEN 3  
                           ELSE 4  
                         END  
                     END  
                 END)                          ConsumedSMSCredit  
      FROM   (SELECT *  
              FROM   @record) tbl  
      GROUP  BY 
                Name_Company  
      Order  BY 
                Name_Company  
  END  
  
SELECT *  
FROM   @record  
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,  
          Name_Company,  
          Name_Client  
  GO
exec  [_temp_pGetSendSOAPPlan]  '2021-07-23', 1
