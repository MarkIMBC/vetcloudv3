GO

exec _solo_pBackUpDatabase

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  DROP CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail]

GO

ALTER TABLE [dbo].[tAuditTrail]
  DROP CONSTRAINT [DF_tAuditTrail_IsActive]

GO

TRUNCATE TABLE tAuditTrail_Detail

TRUNCATE TABLE tAuditTrail

GO

ALTER TABLE [dbo].[tAuditTrail]
  ADD CONSTRAINT [DF_tAuditTrail_IsActive] DEFAULT ((1)) FOR [IsActive]

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  WITH NOCHECK ADD CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail] FOREIGN KEY([ID_AuditTrail]) REFERENCES [dbo].[tAuditTrail] ([ID]) ON DELETE CASCADE

GO

ALTER TABLE [dbo].[tAuditTrail_Detail]
  CHECK CONSTRAINT [FKtAuditTrail_Detail_ID_AuditTrail]

GO

SELECT *
FROM   tAuditTrail

SELECT *
FROM   tAuditTrail_Detail

GO

 
exec pReOrganizeRebuildTableIndex