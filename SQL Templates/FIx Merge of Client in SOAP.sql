BEGIN TRANSACTION

DECLARE @GUID_Company VARCHAR(MAX) = 'C2BB5272-3DF0-4869-A1A3-A34CBF5C8808'
DECLARE @Source_Code_Client VARCHAR(MAX) = 'CNT-02269';
DECLARE @Destiantion_Code_Client VARCHAR(MAX) = 'CNT-03228';

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
DECLARE @ID_Client_Destination INT = 0

SELECT @ID_Client_Destination = ID
FROM   tClient
where  ID_Company = @ID_Company
       and Code = @Destiantion_Code_Client

exec pMergeClientRecordByCompany
  @GUID_Company,
  @Source_Code_Client,
  @Destiantion_Code_Client

Update tPatient_SOAP
SET    ID_Client = patient.ID_Client
FROM   tPatient_SOAP _soap
       inner join vPatient patient
               on _soap.ID_Patient = patient.ID
where  _soap.ID_Client <> patient.ID_Client
       AND patient.ID_Client = @ID_Client_Destination

ROLLBACK TRANSACTION 
