DECLARE @table TABLE
  (
     ServerName         VARCHAR(MAX),
     ID                 INT,
     Name               VARCHAR(MAX),
     Guid               VARCHAR(MAX),
     DateCreated        Datetime,
     SOAPPlanSMSMessage VARCHAR(MAX)
  )

INSERT @table
SELECT DISTINCT ServerName,
                ID,
                Name,
                Guid,
                DateCreated,
                SOAPPlanSMSMessage
FROM   (SELECT 'Old Server' ServerName,
               *
        FROm   db_waterworksv1_live_dev.dbo.vCompanyActive
        UNION ALL
        SELECT 'Server 1' ServerName,
               *
        FROm   db_waterworksv1_server1_dev.dbo.vCompanyActive
        UNION ALL
        SELECT 'Server 2' ServerName,
               *
        FROm   db_waterworksv1_server2_dev.dbo.vCompanyActive
        UNION ALL
        SELECT 'Server 3' ServerName,
               *
        FROm   db_waterworksv1_server3_dev.dbo.vCompanyActive
        UNION ALL
        SELECT 'Server 4' ServerName,
               *
        FROm   db_waterworksv1_server4_dev.dbo.vCompanyActive) tbl
ORDER  BY Name ASC

SELECT *
FROM   @table
WHERE  Name IN (SELECT Name
                FROM   @table
                GROUP  BY Name
                HAVING COUNT(*) > 1)

SELECT ServerName,
       ID,
       Name,
       Len(SOAPPlanSMSMessage)                             CharacterLength,
       dbo.fGetITextMessageCreditCount(SOAPPlanSMSMessage) DefaultCreditCount,
       Guid
FROM   @table
Order  By Name

select company.Name,  MaxSMSCountPerDay
FROM   vCompanyActive company
       inner join tCompany_SMSSetting comSetting
               on company.ID = comSetting.ID_Company 
			   ORDER BY company.Name



/*

SELECT * FROm vUser WHERE Guid = '73A8BD51-932F-47E4-A84C-174E8D4364C2'

*/