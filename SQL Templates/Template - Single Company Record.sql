DEclare @GUID_Company VARCHAR(MAX) = 'FDBEEA27-9FCA-47E1-A139-26243011AC56'

----------------------------------------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

--------------------------------------------------------------------------------------------------
/**************************************** Code Here *********************************************/
--------------------------------------------------------------------------------------------------
SELECT where1.ID_Company,
       where1.ImageLogoLocationFilenamePath,
       where1.Name_Company,
       where1.Address_Company,
       where1.HeaderInfo_Company,
       where1.ID_Client,
       where1.Name_Client,
       where1.ID_Item,
       where1.Name_Item,
       where1.Date_BillingInvoice,
       where1.TotalQuantity,
       where1.UnitCost,
       where1.UnitPrice,
       where1.GrossSales,
       where1.NetCost,
       totalDiscountPerBillDate.TotalDiscountAmountPerDate,
       AttendingPhysician_ID_Employee,
       AttendingPhysician_Name_Employee
FROM   vzSalesIncomentReport where1
       LEFT JOIN (SELECT tbl.ID_Company,
                         tbl.Date_BillingInvoice,
                         SUM(ISNULL(tbl.DiscountAmount, 0)) TotalDiscountAmountPerDate
                  FROM   (SELECT where2.ID_Company,
                                          Convert(Date, where2.Date_BillingInvoice) Date_BillingInvoice,
                                          where2.DiscountAmount
                          FROM  vzSalesIncomentReport_DetailedDiscount where2
                         /*@WHERE2*/
                         ) tbl
                  GROUP  BY ID_Company,
                            Date_BillingInvoice) totalDiscountPerBillDate
              ON where1.ID_Company = totalDiscountPerBillDate.ID_Company
                 AND CONVERT(DATE, where1.Date_BillingInvoice) = totalDiscountPerBillDate.Date_BillingInvoice
/*@WHERE1*/
