GO

IF COL_LENGTH('dbo.tAuditTrail', 'RunAfterSavedProcess_ID_FilingStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tAuditTrail',
        'RunAfterSavedProcess_ID_FilingStatus',
        2
  END

GO

IF COL_LENGTH('dbo.tPatient_SOAP', 'RunAfterSavedProcess_ID_FilingStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP',
        'RunAfterSavedProcess_ID_FilingStatus',
        2
  END

GO

IF COL_LENGTH('dbo.tPatient_SOAP', 'DateRunAfterSavedProcess') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tPatient_SOAP',
        'DateRunAfterSavedProcess',
        5
  END

GO

IF COL_LENGTH('dbo.tBillingInvoice', 'RunAfterSavedProcess_ID_FilingStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice',
        'RunAfterSavedProcess_ID_FilingStatus',
        2
  END

GO

IF COL_LENGTH('dbo.tBillingInvoice', 'DateRunAfterSavedProcess') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tBillingInvoice',
        'DateRunAfterSavedProcess',
        5
  END

GO

EXEC _pRefreshAllViews

GO

CREATE OR
ALTER proc pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs(@IDs_CurrentObject typIntList ReadOnly)
as
  BEGIN
      DECLARE @TranName VARCHAR(MAX);
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
      DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

      SET @TranName = 'pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs-'
                      + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                BEGIN TRANSACTION @TranName;

                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID

                DECLARE @IDs_Patient_SOAP TYPINTLIST

                INSERT @IDs_Patient_SOAP
                SELECT ID
                FROM   @IDs_CurrentObject

                EXEC dbo.pUpdatPatientConfinementItemsServicesByPatientSOAP
                  @IDs_Patient_SOAP

                ----------------------------------------------
                --exec pRemoveMulticatePatientConfinementFromSOAP
                --  @IDs_Patient_SOAP

                --------------------------------------------
                DECLARE @IDs_Patient TYPINTLIST

                INSERT @IDs_Patient
                SELECT patient.ID
                FROM   tPatient_SOAP soap
                       INNER JOIN tPatient patient
                               ON patient.ID = soap.ID_Patient
                       INNER JOIN @IDs_CurrentObject ids
                               ON soap.ID = ids.ID

                EXEC dbo.PupDatePatientsLastVisitedDate
                  @IDs_Patient

                --------------------------------------------
                EXEC _pCancelPatient_Confinement_temp

                exec pUpdatePatient_SOAP_BillingStatus
                  @IDs_Patient_SOAP

                Update tPatient_SOAP
                SET    DateRunAfterSavedProcess = GETDATE(),
                       RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID

                COMMIT TRANSACTION @TranName
            END TRY
            BEGIN CATCH
                ROLLBACK TRANSACTION @TranName;
                Update tPatient_SOAP
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tPatient_SOAP _soap
                       INNER JOIN @IDs_CurrentObject ids
                               ON _soap.ID = ids.ID
            END CATCH
        END
  END

GO

CREATE OR
ALTER proc pReRun_AfterSaved_Process_BillingInvoice_By_IDs(@IDs_CurrentObject typIntList ReadOnly)
AS
  BEGIN
      IF(SELECT COUNT(*)
         FROM   @IDs_CurrentObject) > 0
        BEGIN
            BEGIN TRY
                DECLARE @TranName VARCHAR(MAX);
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Ongoing INT = 9
                DECLARE @RunAfterSavedProcess_ID_FilingStatus_Done INT = 13

                SET @TranName = 'pReRun_AfterSaved_Process_BillingInvoice_By_IDs-'
                                + FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss');

                BEGIN TRANSACTION @TranName;

                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Ongoing
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID

                IF(SELECT COUNT(*)
                   FROM   @IDs_CurrentObject) > 0
                  BEGIN
                      DECLARE @IDs_BillingINvoice TYPINTLIST
                      /*Update Confinement Bill Status*/
                      DECLARE @IDs_Patient_Confinement TYPINTLIST

                      INSERT @IDs_Patient_Confinement
                      SELECT DISTINCT ID_Patient_Confinement
                      FROM   tBillingInvoice bi
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      EXEC PupDatePatient_ConfineMen_BillIngStatus
                        @IDs_Patient_Confinement

                      /*Update SOAP Bill Status*/
                      DECLARE @IDs_Patient_SOAP TYPINTLIST

                      INSERT @IDs_Patient_SOAP
                      SELECT DISTINCT ID_Patient_SOAP
                      FROM   tBillingInvoice bi
                             inner join @IDs_CurrentObject ids
                                     on bi.ID = ids.ID

                      exec dbo.pModel_AfterSaved_BillingInvoice_Computation
                        @IDs_BillingInvoice

                      EXEC pUpdatePatient_SOAP_BillingStatus
                        @IDs_Patient_SOAP

                      Update tBillingInvoice
                      SET    DateRunAfterSavedProcess = GETDATE(),
                             RunAfterSavedProcess_ID_FilingStatus = @RunAfterSavedProcess_ID_FilingStatus_Done
                      FROm   tBillingInvoice bi
                             INNER JOIN @IDs_CurrentObject ids
                                     ON bi.ID = ids.ID
                  END

                COMMIT TRANSACTION @TranName
            END TRY
            BEGIN CATCH
                ROLLBACK TRANSACTION @TranName;
                Update tBillingInvoice
                SET    RunAfterSavedProcess_ID_FilingStatus = NULL
                FROm   tBillingInvoice bi
                       INNER JOIN @IDs_CurrentObject ids
                               ON bi.ID = ids.ID
            END CATCH
        END
  END

GO

ALTER proc pReRun_AfterSaved_Process_Patient_SOAP
AS
  BEGIN
      DECLARE @IDs_CurrentObject typIntList

      INSERT @IDs_CurrentObject
      SELECT TOP 5 _soap.ID
      FROm   tPatient_SOAP _soap
             INNER JOIN vCompanyActive c
                     on _soap.ID_Company = c.ID
                        and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
                        AND CONVERT(Date, _soap.DateModified) = CONVERT(Date, GETDATE())
      Order  by _soap.DateModified DESC

      exec pReRun_AfterSaved_Process_Patient_SOAP_BY_IDs
        @IDs_CurrentObject
  END

GO

ALTER proc pReRun_AfterSaved_Process_BillingInvoice
AS
  BEGIN
      DECLARE @IDs_CurrentObject typIntList

      INSERT @IDs_CurrentObject
      SELECT TOP 5 bi.ID
      FROm   tBillingInvoice bi
             INNER JOIN vCompanyActive c
                     on bi.ID_Company = c.ID
                        and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) NOT IN ( 13 )
                        AND CONVERT(Date, bi.DateModified) = CONVERT(Date, GETDATE())
      Order  by bi.DateModified DESC

      exec pReRun_AfterSaved_Process_BillingInvoice_By_IDs
        @IDs_CurrentObject
  END

GO

exec pReRun_AfterSaved_Process_Patient_SOAP

GO

exec pReRun_AfterSaved_Process_BillingInvoice

GO

DECLARE @IDs_Patient_SOAP typIntList
DECLARE @IDs_BillingInvoice typIntList

INSERT @IDs_Patient_SOAP
SELECT DISTINCT ID_CurrentObject
FROm   tAuditTrail auditTrail
       inner join _tModel model
               on model.Oid = auditTrail.ID_Model
WHERE  TableName = 'tPatient_SOAP'
       -- AND CoNVERT(Date, Date) = CONVERT(Date, GETDATE())
       and ISNULL(auditTrail.IsRunAfterSavedProcess, 0) = 1

INSERT @IDs_BillingInvoice
SELECT DISTINCT ID_CurrentObject
FROm   tAuditTrail auditTrail
       inner join _tModel model
               on model.Oid = auditTrail.ID_Model
WHERE  TableName = 'tBillingInvoice'
       -- AND CoNVERT(Date, Date) = CONVERT(Date, GETDATE())
       and ISNULL(auditTrail.IsRunAfterSavedProcess, 0) = 1

Update tPatient_SOAP
SET    RunAfterSavedProcess_ID_FilingStatus = 13
FROM   tPatient_SOAP _soap
       inner join @IDs_Patient_SOAP ids
               on _soap.ID = ids.ID
WHERE  _soap.DateRunAfterSavedProcess IS NULL
       and _soap.RunAfterSavedProcess_ID_FilingStatus IS NULL

Update tBillingInvoice
SET    RunAfterSavedProcess_ID_FilingStatus = 13
FROM   tBillingInvoice bi
       inner join @IDs_BillingInvoice ids
               on bi.ID = ids.ID
WHERE  bi.DateRunAfterSavedProcess IS NULL
       and bi.RunAfterSavedProcess_ID_FilingStatus IS NULL

GO

SELECT RunAfterSavedProcess_ID_FilingStatus,
       COUNT(*)
FROm   tPatient_SOAP _soap
       INNER JOIN vCompanyActive c
               on _soap.ID_Company = c.ID
                  --and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) = 0
                  AND CONVERT(Date, _soap.DateModified) = CONVERT(Date, GETDATE())
GROUP  BY RunAfterSavedProcess_ID_FilingStatus

SELECT RunAfterSavedProcess_ID_FilingStatus,
       COUNT(*)
FROm   tBillingInvoice _soap
       INNER JOIN vCompanyActive c
               on _soap.ID_Company = c.ID
                  --and ISNULL(RunAfterSavedProcess_ID_FilingStatus, 0) = 0
                  AND CONVERT(Date, _soap.DateModified) = CONVERT(Date, GETDATE())
GROUP  BY RunAfterSavedProcess_ID_FilingStatus

--SELECT RunAfterSavedProcess_ID_FilingStatus,
--       Count(*)
--FROm   tAuditTrail auditTrail
--       inner join _tModel model
--               on model.Oid = auditTrail.ID_Model
--WHERE  TableName = 'tPatient_SOAP'
--       AND CoNVERT(Date, Date) = CONVERT(Date, GETDATE())
----and ISNULL(auditTrail.IsRunAfterSavedProcess, 0) = 0
--GROUP  BY RunAfterSavedProcess_ID_FilingStatus
--SELECT RunAfterSavedProcess_ID_FilingStatus,
--       Count(*)
--FROm   tAuditTrail auditTrail
--       inner join _tModel model
--               on model.Oid = auditTrail.ID_Model
--WHERE  TableName = 'tBillingInvoice'
--       AND CoNVERT(Date, Date) = CONVERT(Date, GETDATE())
----and ISNULL(auditTrail.IsRunAfterSavedProcess, 0) = 0
--GROUP  BY RunAfterSavedProcess_ID_FilingStatus
GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  name = 'pReRun_AfterSaved_Process_BillingInvoice_By_AuditTrailID')
  BEGIN
      DROP PROC pReRun_AfterSaved_Process_BillingInvoice_By_AuditTrailID
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  name = 'pReRun_AfterSaved_Process_BillingInvoice_By_AuditTrailID')
  BEGIN
      DROP PROC pReRun_AfterSaved_Process_BillingInvoice_By_AuditTrailID
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  name = 'pReRun_AfterSaved_Process_Patient_SOAP_By_AuditTrailID')
  BEGIN
      DROP PROC pReRun_AfterSaved_Process_Patient_SOAP_By_AuditTrailID
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  name = 'pReRun_AfterSaved_Process_Patient_SOAP_By_AuditTrailIDs')
  BEGIN
      DROP PROC pReRun_AfterSaved_Process_Patient_SOAP_By_AuditTrailIDs
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  name = '___Temp_pReRun_AfterSaved_Process_Patient_SOAP')
  BEGIN
      DROP PROC ___Temp_pReRun_AfterSaved_Process_Patient_SOAP
  END

GO 
