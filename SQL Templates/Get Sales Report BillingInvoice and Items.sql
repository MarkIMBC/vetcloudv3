DEclare @GUID_Company VARCHAR(MAX) = 'FDBEEA27-9FCA-47E1-A139-26243011AC56'

----------------------------------------------------------------------------------------------------
IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT
DECLARE @Name_Company VARCHAR(MAX)

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

--------------------------------------------------------------------------------------------------
/**************************************** Code Here *********************************************/
--------------------------------------------------------------------------------------------------
SELECT company.Name                                                      Name_Company,
       biHed.Code,
       biHed.Date,
       lTrim(rTrim(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
       lTrim(rTrim(Name_Item))                                           Name_Item,
       CONVERT(DATE, biHed.Date)                                         Date_BillingInvoice,
       Sum(Quantity)                                                     TotalQuantity,
       ISNULL(UnitCost, 0)                                               UnitCost,
       ISNULL(UnitPrice, 0)                                              UnitPrice,
       ISNULL(UnitPrice, 0) * Sum(Quantity)                              GrossSales,
       ( ISNULL(UnitPrice, 0) - ISNULL(UnitCost, 0) ) * Sum(Quantity)    NetCost
FROM   vBillingInvoice biHed
       INNER JOIN vCompany company
               ON company.ID = biHed.ID_Company
       INNER JOIN vBillingInvoice_Detail biDetail
               ON biHed.ID = biDetail.ID_BillingInvoice
WHERE  biHed.ID_FilingStatus IN ( 3 )
       AND company.ID = @ID_Company
GROUP  BY biHed.ID_Company,
          biHed.Code,
          biHed.Date,
          company.Name,
          biHed.Name_Client,
          biHed.WalkInCustomerName,
          ID_Item,
          Name_Item,
          UnitCost,
          UnitPrice

SELECT biHed.Date,
       biHed.Code,
       lTrim(rTrim(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
       DiscountAmount,
       TotalItemDiscountAmount
FROM   vBillingInvoice biHed
       INNER JOIN vCompany company
               ON company.ID = biHed.ID_Company
WHERE  biHed.ID_FilingStatus IN ( 3 )
       AND ( ISNULL(biHed.DiscountAmount, 0) > 0
              OR ISNULL(biHed.TotalItemDiscountAmount, 0) > 0 )
       AND company.ID = @ID_Company 
