GO

USE SystemManagement

GO

GO

IF NOT EXISTS (SELECT *
               FROM   sys.objects
               WHERE  object_id = OBJECT_ID(N'dbo.tForUploadToDropbox')
                      AND type in ( N'U' ))
  BEGIN
  --DROP TABLE tForUploadToDropbox
      CREATE TABLE [dbo].tForUploadToDropbox
        (
           ID               [int] IDENTITY(1, 1) NOT NULL,
           DatabaseName     [varchar](max) NULL,
           Model            [varchar](max) NULL,
           TableName        [varchar](max) NULL,
           ID_Company       [int] NULL,
           ColumnName       [varchar](max) NULL,
           ID_CurrentObject INT,
           Name_Company     [varchar](max) NULL,
           ImageFilePath    [varchar](max) NULL,
           IsUpload         BIT NULL,
           DropBoxUrl       [varchar](max) NULL
        );
		
		
		Alter Table  [dbo].tForUploadToDropbox
		ADD  DropBoxThumbnailUrl       [varchar](max) NULL;

		Alter Table  [dbo].tForUploadToDropbox
		ADD  Comment       [varchar](max) NULL;

  END

GO

IF NOT EXISTS (SELECT *
               FROM   sys.objects
               WHERE  object_id = OBJECT_ID(N'dbo.tSystemFolder')
                      AND type in ( N'U' ))
  BEGIN
      CREATE TABLE [dbo].tSystemFolder
        (
           ID           [int] IDENTITY(1, 1) NOT NULL,
           DatabaseName [varchar](max) NULL,
           FolderPath   [varchar](max) NULL
        );
  END

GO

CREATE OR
ALTER PROC pAssignedSystemFolderPath(@databaseName VARCHAR(MAX),
                                     @folderPath   VARCHAR(MAX))
AS
  BEGIN
      INSERT dbo.tSystemFolder
             (DatabaseName)
      SELECT name
      FROM   MASTER.dbo.sysdatabases
      WHERE  ( name LIKE 'db_vetcloudv3_%'
                OR Name = 'db_ProvincialVet_test' )
      EXCEPT
      SELECT DatabaseName
      FROm   tSystemFolder

      UPDATE tSystemFolder
      SET    FolderPath = @folderPath
      WHERE  DatabaseName = @databaseName
  END

GO

CREATE OR
ALTER PROC pGenerateForUploadToDropboxRecords
AS
  BEGIN
      DECLARE @table TABLE
        (
           [Model]          [varchar](max) NULL,
           [TableName]      [varchar](max) NULL,
           [ID_Company]     [int] NULL,
           [Name_Company]   [varchar](max) NULL,
           ColumnName       [varchar](max) NULL,
           ID_CurrentObject INT,
           [ImageFilePath]  [varchar](max) NULL,
           [IsUpload]       [int] NULL,
           [DropBoxUrl]     [varchar](MAX) NULL
        )
      DECLARE @name VARCHAR(50) -- database name       
      DECLARE @dateStart DateTime
      DECLARE @dateEnd DateTime
      DECLARE db_cursorpBackUpDatabase_VetCloudv3 CURSOR FOR
        SELECT name
        FROM   MASTER.dbo.sysdatabases
        WHERE  ( name LIKE 'db_vetcloudv3_%'
                  OR Name = 'db_ProvincialVet_test' )
        Order  by crdate

      OPEN db_cursorpBackUpDatabase_VetCloudv3

      FETCH NEXT FROM db_cursorpBackUpDatabase_VetCloudv3 INTO @name

      WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @dateStart = GETDATE();

            DECLARE @sql VARCHAR(MAX)= 'SELECT  Model, TableName,	ID_Company,	 ColumnName, ID_CurrentObject, Name_Company, FolderName + ''/'' + ImageFilePath ImageFilePath FROM '
              + @name
              + '.dbo.fGetCompanyImageFileNames(''''), ( SELECT ''Image'' FolderName  UNION ALL SELECT ''Thumbnail'' FolderName) tbl '

            print @sql

            INSERT @table
                   (Model,
                    TableName,
                    ID_Company,
                    ColumnName,
                    ID_CurrentObject,
                    Name_Company,
                    ImageFilePath)
            exec(@sql);

            insert tForUploadToDropbox
                   (DatabaseName,
                    Model,
                    TableName,
                    ID_Company,
                    ColumnName,
                    ID_CurrentObject,
                    Name_Company,
                    ImageFilePath)
            select @name,
                   Model,
                   TableName,
                   ID_Company,
                   ColumnName,
                   ID_CurrentObject,
                   Name_Company,
                   ImageFilePath
            from   (SELECT Model,
                           TableName,
                           ID_Company,
                           ColumnName,
                           ID_CurrentObject,
                           Name_Company,
                           ImageFilePath
                    FROM   @table
                    WHERE  ID_Company NOT IN ( 1 )
                    EXCEPT
                    SELECT Model,
                           TableName,
                           ID_Company,
                           ColumnName,
                           ID_CurrentObject,
                           Name_Company,
                           ImageFilePath
                    FROM   tForUploadToDropbox) tbl

            FETCH NEXT FROM db_cursorpBackUpDatabase_VetCloudv3 INTO @name
        END

      CLOSE db_cursorpBackUpDatabase_VetCloudv3

      DEALLOCATE db_cursorpBackUpDatabase_VetCloudv3
  END

GO

CREATE OR
ALTER PROC pGetPendingForUploadToDropbox
as
  BEGIN
      DECLARE @PageNumber INT = 1;
      DECLARE @PageSize INT = 500;

      SELECT a.ID,
             a.DatabaseName,
             a.Model,
             a.TableName,
             a.ID_Company,
             a.ColumnName,
             a.ID_CurrentObject,
             a.Name_Company,
             ISNULL(b.FolderPath, '') + '/api/wwwroot/Content/' + a.ImageFilePath ImageFilePath,
             a.IsUpload,
             a.DropBoxUrl
      FROM   tForUploadToDropbox a
             INNER JOIN tSystemFolder b
                     on a.DatabaseName = b.DatabaseName
      WHERE  1 = 1
             AND IsNull(IsUpload, 0) = 0
      ORDER  BY a.ID DESC
      OFFSET (@PageNumber - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY;
  END

GO

SELECT name
FROM   MASTER.dbo.sysdatabases
WHERE  ( name LIKE 'db_vetcloudv3_%'
          OR Name = 'db_ProvincialVet_test' )
Order  by crdate


exec pAssignedSystemFolderPath 'db_vetcloudv3_oldserver', 'C:/inetpub/wwwroot/VetCloudv3-OldServer'
exec pAssignedSystemFolderPath 'db_vetcloudv3_cardinalpetclinic', 'C:/inetpub/wwwroot/VetCloudv3-CardinalPetClinic'
exec pAssignedSystemFolderPath 'db_vetcloudv3_bathandbarkvetclinic', 'C:/inetpub/wwwroot/VetCloudv3-BathandBarkVetClinic'
exec pAssignedSystemFolderPath 'db_vetcloudv3_elkingchayclinic', 'C:/inetpub/wwwroot/VetCloudv3-ElKingChayClinic'
exec pAssignedSystemFolderPath 'db_vetcloudv3_thecatclinic', 'C:/inetpub/wwwroot/VetCloudv3-TheCatClinic'
exec pAssignedSystemFolderPath 'db_vetcloudv3_venveterinaryclinic', 'C:/inetpub/wwwroot/VetCloudv3-VenVeterinaryClinic'

GO
UPDATE tForUploadToDropbox SET IsUpload = null
go
--exec pGenerateForUploadToDropboxRecords
exec pGetPendingForUploadToDropbox
--Update tForUploadToDropbox SET IsUpload = NULL, DateExecuted= NULL, Comment = NULL
select TOP 10  *
FROm   tForUploadToDropbox WITH (NOLOCK)
WHERE  IsUpload = 1
ORDER BY 1 DESC

GO

select TOP 10  *
FROm   tForUploadToDropbox WITH (NOLOCK)
WHERE  IsUpload = 0


