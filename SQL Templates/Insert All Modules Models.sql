DECLARE @IDs_UserRole typIntList
DECLARE @Guids_Model TAble
  (
     GUID_Model VARCHAR(MAX)
  )
DECLARE @Guids_Report TAble
  (
     GUID_Report VARCHAR(MAX)
  )

INSERT @IDs_UserRole
select ID
FROM   tUserRole
WHERE  Name IN ( 'All Inventory Reports', 'All Sales Related Reports', 'All Admission', 'All Inventory Modules',
                 'All Sales Module', 'All Human Resource', 'All Administrator Modules', 'Help Desk Only' )

IF(SELECT COUNT(*)
   FROM   tUserRole
   WHERE  Name = 'All Modules') = 0
  BEGIN
      INSERT INTO [dbo].[tUserRole]
                  ([Name],
                   [IsActive],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ( 'All Modules',
                    1,
                    GETDATE(),
                    GETDATE(),
                    1,
                    1)
  END

declare @ID_UserRole INT

SELECT @ID_UserRole = ID
FROM   tUserRole
WHERE  Name = 'All Modules'

INSERT @Guids_Model
SELECT _userRoleDetail.[ID_Model]
FROM   tUserRole _userRole
       inner join @IDs_UserRole ids
               on _userRole.ID = ids.ID
       INNER JOIN tUserRole_Detail _userRoleDetail
               on _userRole.ID = _userRoleDetail.ID_UserRole

DELETE FROM @Guids_Model
WHERE  GUID_Model IN (SELECT GUID_Model
                      FROM   tUserRole_Detail
                      WHERE  ID_UserRole = @ID_UserRole)

INSERT @Guids_Report
SELECT _userRoleReport.ID_Report
FROM   tUserRole _userRole
       inner join @IDs_UserRole ids
               on _userRole.ID = ids.ID
       INNER JOIN tUserRole_Reports _userRoleReport
               on _userRole.ID = _userRoleReport.ID_UserRole

DELETE FROM @Guids_Report
WHERE  GUID_Report IN (SELECT GUID_Report
                       FROM   tUserRole_Reports
                       WHERE  ID_UserRole = @ID_UserRole)

INSERT INTO [dbo].[tUserRole_Detail]
            ([Name],
             [IsActive],
             [ID_UserRole],
             [ID_Model],
             [IsView],
             [IsCreate],
             [IsEdit],
             [IsDelete],
             [IsDeny],
             [SeqNo])
SELECT _userRoleDetail.[Name],
       _userRoleDetail.[IsActive],
       @ID_UserRole,
       _userRoleDetail.[ID_Model],
       _userRoleDetail.[IsView],
       _userRoleDetail.[IsCreate],
       _userRoleDetail.[IsEdit],
       _userRoleDetail.[IsDelete],
       _userRoleDetail.[IsDeny],
       _userRoleDetail.[SeqNo]
FROM   tUserRole _userRole
       inner join @IDs_UserRole ids
               on _userRole.ID = ids.ID
       INNER JOIN tUserRole_Detail _userRoleDetail
               on _userRole.ID = _userRoleDetail.ID_UserRole
WHERE  ID_Model IN (SELECT GUID_Model
                    FROm   @Guids_Model)

INSERT INTO [dbo].[tUserRole_Reports]
            ([Code],
             [Name],
             [IsActive],
             [Comment],
             [ID_UserRole],
             [ID_Report])
SELECT _userRoleReport.[Code],
       _userRoleReport.[Name],
       _userRoleReport.[IsActive],
       _userRoleReport.[Comment],
       @ID_UserRole,
       _userRoleReport.[ID_Report]
FROM   tUserRole _userRole
       inner join @IDs_UserRole ids
               on _userRole.ID = ids.ID
       INNER JOIN tUserRole_Reports _userRoleReport
               on _userRole.ID = _userRoleReport.ID_UserRole
WHERE  [ID_Report] IN (SELECT GUID_Report
                       FROm   @Guids_Report)

SELECT *
FROM   tUserRole
where  ID = @ID_UserRole

SELECT *
FROM   tUserRole_Detail
where  ID_UserRole = @ID_UserRole

SELECT *
FROM   tUserRole_Reports
where  ID_UserRole = @ID_UserRole

GO 
