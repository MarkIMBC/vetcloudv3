DECLARE @temp TABLE
  (
     Name        VARCHAR(MAX),
     DateCreated DATETIME,
     GUID        VARCHAR(MAX)
  )

INSERT @temp
SELECT Name,
       DateCreated,
       GUID
FROM   tCompany
WHERE  CONVERT(date, DateCreated) <= '2022-12-01'
ORDER  BY DateCreated

DELETE FROM @temp
WHERE  Name IN ( 'Clinica Veterinaria', 'Gimenez Veterinary Clinic', 'Reyhan Animal Care Clinic', 'Vet Cloud',
                 'CDO Pet Doctor', 'Pet Point Animal Clinic - San Felipe Branch' )

DELETE FROM @temp
WHERE  GUID IN (SELECT GUID
                FROM   vCompanyActive)

SELECT *
FROM   @temp

DECLARE @ImagePaths TABLE
  (
     ID_Company    INT,
     Name_Company  VARCHAR(MAX),
     Name_Model    VARCHAR(MAX),
     ImageFilePath VARCHAR(MAX)
  )
DECLARE @Guid VARCHAR(MAX)
DECLARE db_cursor CURSOR FOR
  SELECT top 10 GUID
  FROM   @temp

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Guid

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT @ImagePaths
      select *
      FROM   dbo.fGetCompanyImagePathByGUID(@Guid)

      FETCH NEXT FROM db_cursor INTO @Guid
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT DISTiNCT *
INTO   temp_ImagePaths_202301291515
FROm   @ImagePaths 
