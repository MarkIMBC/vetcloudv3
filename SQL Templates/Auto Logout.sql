DECLARE @ID_Company INT = 1

UPDATE tUserSession SET IsActive = 0
FROM tUserSession _u INNER JOIN vCompanyActiveUser _cu on _u.ID_User = _cu.ID_User
WHERE _cu.ID_Company= @ID_Company