DECLARE @GUID_Company VARCHAR(MAX) = 'FDBEEA27-9FCA-47E1-A139-26243011AC56'
DECLARE @Name_Company VARCHAR(MAX) = ''

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID,
       @Name_Company = Name
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------
SELECT @Name_Company Name_Company,
       Code,
       Name
       + CASE
           WHEN ISNULL(IsActive, 0) = 0 THEN ' (Deleted)'
           ELSE ''
         END         Name,
       Name_ItemCategory,
       UnitCost      BuyingPrice,
       UnitPrice     SellingPrice,
       CurrentInventoryCount,
       Name_InventoryStatus,
       OtherInfo_DateExpiration
FROM   vItem head
WHERE  head.ID_Company = @ID_Company
       and head.ID_ItemType = 2
Order  by ISNULL(IsActive, 0) DESC,
          Name

SELECT @Name_Company Name_Company,
       Name
       + CASE
           WHEN ISNULL(IsActive, 0) = 0 THEN ' (Deleted)'
           ELSE ''
         END         Name,
       Name_ItemCategory,
       UnitCost      BuyingPrice,
       UnitPrice     ServicePrice
FROM   vItem head
WHERE  ID_Company = @ID_Company
       and ID_ItemType = 1
Order  by ISNULL(IsActive, 0) DESC,
          Name

DECLARE @ClientPatients TABLE
  (
     RowIndex         INT IDENTITY(1, 1),
     [ClientPatient]  [varchar](MAX) NULL,
     Name_Company     [varchar](MAX) NULL,
     [ClientCode]     [varchar](50) NULL,
     [Client]         [varchar](2010) NULL,
     [ContactNumber]  [varchar](max) NULL,
     [ContactNumber2] [varchar](max) NULL,
     [Email]          [varchar](max) NULL,
     [Address]        [varchar](max) NULL,
     [Code]           [varchar](50) NULL,
     [Patient]        [varchar](MAX) NULL,
     [DateBirth]      [datetime] NULL,
     [Name_Gender]    [varchar](max) NULL,
     [Species]        [varchar](max) NULL,
     [IsNeutered]     [varchar](1) NOT NULL,
     [IsDeceased]     [varchar](1) NOT NULL,
     [Microchip]      [varchar](max) NULL,
     [Comment]        [varchar](max) NULL
  )

INSERt @ClientPatients
       (Name_Company,
        [ClientCode],
        [Client],
        [ContactNumber],
        [ContactNumber2],
        [Email],
        [Address],
        [Code],
        [ClientPatient],
        [Patient],
        [DateBirth],
        [Name_Gender],
        [Species],
        [IsNeutered],
        [IsDeceased],
        [Microchip],
        [Comment])
SELECT c.Name,
       client.Code ClientCode,
       client.Name
       + CASE
           WHEN ISNULL(client.IsActive, 0) = 0 THEN ' (Deleted)'
           ELSE ''
         END       Client,
       client.ContactNumber,
       client.ContactNumber2,
       client.Email,
       client.Address,
       patient.Code,
       client.Name + ' - ' + patient.Name
       + CASE
           WHEN ISNULL(client.IsActive, 0) = 0 THEN ' (Deleted)'
           ELSE ''
         END       Patient,
       patient.Name
       + CASE
           WHEN ISNULL(client.IsActive, 0) = 0 THEN ' (Deleted)'
           ELSE ''
         END       Patient,
       patient.DateBirth,
       Name_Gender,
       Species,
       CASE
         WHEN IsNeutered = 1 THEN 'Y'
         ELSE 'N'
       END         IsNeutered,
       CASE
         WHEN IsDeceased = 1 THEN 'Y'
         ELSE 'N'
       END         IsDeceased,
       patient.Microchip,
       patient.Comment
FROM   vClient client
       inner join vPatient patient
               on client.ID = patient.ID_Client
       INNER JOIN tCompany c
               on c.ID = client.ID_Company
WHERE  client.ID_Company = @ID_Company
       and client.IsActive = 1
--AND CONVERT(Date, patient.DateCreated) BETWEEN @DateStart AND @DateEnd
Order  by client.Name,
          patient.Name

SELECT Name_Company,
       [ClientCode],
       dbo.fGetCleanedStringForExtratedRecord([Client]),
       [ContactNumber],
       [ContactNumber2],
       [Email],
       dbo.fGetCleanedStringForExtratedRecord([Address]),
       [Code],
       dbo.fGetCleanedStringForExtratedRecord([Patient]),
       [DateBirth],
       [Name_Gender],
       [Species],
       [IsNeutered],
       [IsDeceased],
       [Microchip],
       dbo.fGetCleanedStringForExtratedRecord([Comment])
FROM   @ClientPatients
ORDER  BY RowIndex,
          ClientCode

DECLARE @TotalRecordMulticate INT = 0

SELECT @TotalRecordMulticate = Count(*)
FROM   (SELECT Max(RowIndex) MAXRowindex
        FROm   @ClientPatients
        WHERE  LEN(ISNULL(ClientCode, '')) > 0
        GROUP  BY ClientCode
        HAVING COUNT(*) > 1) tbl

WHILE ( @TotalRecordMulticate > 0 )
  BEGIN
      Update @ClientPatients
      SET    [ClientCode] = '',
             [Client] = '',
             [ContactNumber] = '',
             [ContactNumber2] = '',
             [Email] = '',
             [Address] = ''
      WHERE  RowIndex IN (SELECT Max(RowIndex)
                          FROm   @ClientPatients
                          WHERE  LEN(ISNULL(ClientCode, '')) > 0
                          GROUP  BY ClientCode
                          HAVING COUNT(*) > 1)

      SELECT @TotalRecordMulticate = Count(*)
      FROM   (SELECT Max(RowIndex) MAXRowindex
              FROm   @ClientPatients
              WHERE  LEN(ISNULL(ClientCode, '')) > 0
              GROUP  BY ClientCode
              HAVING COUNT(*) > 1) tbl
  END

Update @ClientPatients
SET    [ClientCode] = '',
       [Client] = '',
       [ContactNumber] = '',
       [ContactNumber2] = '',
       [Email] = '',
       [Address] = ''
WHERE  RowIndex IN (SELECT Max(RowIndex)
                    FROm   @ClientPatients
                    GROUP  BY ClientCode
                    HAVING COUNT(*) > 1)

/*Patient SOAP*/
SELECT @Name_Company                                               Name_Company,
       Date,
       Code,
       Name_SOAPType                                               [Type],
       AttendingPhysician_Name_Employee                            [Attnd. Personnel],
       Name_Client                                                 Client,
       Name_Patient                                                Patient,
       dbo.fGetCleanedStringForExtratedRecord(History)             [Primary Complaint / History],
       dbo.fGetCleanedStringForExtratedRecord(ClinicalExamination) ClinicalExamination,
       dbo.fGetCleanedStringForExtratedRecord(head.Interpretation) [Laboratory Interpretation],
       dbo.fGetCleanedStringForExtratedRecord(head.Diagnosis)      Diagnosis,
       dbo.fGetCleanedStringForExtratedRecord(head.Treatment)      Treatment,
       dbo.fGetCleanedStringForExtratedRecord(head.Prescription)   Prescription,
       Name_FilingStatus                                           Status
FROm   vPatient_SOAP head
WHERE  ID_Company = @ID_Company
--AND CONVERT(Date, head.DateCreated) BETWEEN @DateStart AND @DateEnd
Order  by Date

SELECT @Name_Company                    Name_Company,
       Date,
       hed.Code,
       AttendingPhysician_Name_Employee [Attnd. Personnel],
       Name_Client                      Client,
       Name_Patient                     Patient,
       Name_FilingStatus                Status,
       Name_Item,
       det. Comment
FROm   vPatient_Wellness hed
       inner join vPatient_Wellness_Detail det
               on det.ID_Patient_Wellness = hed.ID
WHERE  hed.ID_Company = @ID_Company
--AND CONVERT(Date, hed.DateCreated) BETWEEN @DateStart AND @DateEnd
Order  by Date 
