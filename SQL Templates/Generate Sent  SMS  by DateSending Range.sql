DEclare @DateStart DateTime = '2022-06-01'
DEclare @DateEnd DateTime = '2022-06-15'
DEclare @Date DateTime
DECLARE @Dates Table
  (
     Date Date
  )

INSERT @Dates
SELECT Date
FROM   dbo.fGetDatesByDateCoverage(@DateStart, @DateEnd)

DECLARE @table TABLE
  (
     Name_Company         VARCHAR(MAX),
     DateSending          DateTime,
     Name_Client          VARCHAR(MAX),
     ContactNumber_Client VARCHAR(MAX),
     Name_Item            VARCHAR(MAX),
     Comment              VARCHAR(MAX),
     Message              VARCHAR(MAX)
  )
DECLARE db_cursor CURSOR FOR
  SELECT Date
  FROM   @Dates
  Order  by Date

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @Date

WHILE @@FETCH_STATUS = 0
  BEGIN
      insert @table
      select Name_Company,
             DateSending,
             Name_Client,
             ContactNumber_Client,
             Name_Item,
             Comment,
             Message
      FROm   dbo.fGetSendSoapPlan(@Date, 1, '')

      FETCH NEXT FROM db_cursor INTO @Date
  END

CLOSE db_cursor

DEALLOCATE db_cursor

SELECT *,
       dbo.fGetITextMessageCreditCount(Message)
FROm   @table 
