DECLARE @Confinement TABLE
  (Name_Company    VARCHAR(MAX),
     ID_Company    INT,
     DateDischarge Date
  )


INSERT @Confinement
select c.Name Name_Company,
       confinement.ID_Company,
       CONVERT(Date, confinement.DateDischarge) 
FROm   tPatient_Confinement confinement
       inner join vCompanyActive c
               on c.ID = confinement.ID_Company
Where  ID_DischargeBy IS NOT NULL 




SELECT ID_Company, Name_Company,
       [2022-02-01],
       [2022-02-02],
       [2022-02-03],
       [2022-02-04],
       [2022-02-05],
       [2022-02-06],
       [2022-02-07],
       [2022-02-08],
       [2022-02-09],
       [2022-02-10],
       [2022-02-11],
       [2022-02-12],
       [2022-02-13],
       [2022-02-14],
       [2022-02-15],
       [2022-02-16],
       [2022-02-17],
       [2022-02-18],
       [2022-02-19],
       [2022-02-20],
       [2022-02-21],
       [2022-02-22],
       [2022-02-23],
       [2022-02-24],
       [2022-02-25],
       [2022-02-26],
       [2022-02-27],
       [2022-02-28]
FROM   (SELECT Distinct  ID_Company,
                                Name_Company,
                        DateDischarge
        FROM  @Confinement) AS SourceTable
       PIVOT ( Count(DateDischarge)
             FOR DateDischarge IN ( [2022-02-01],
                                [2022-02-02],
                                [2022-02-03],
                                [2022-02-04],
                                [2022-02-05],
                                [2022-02-06],
                                [2022-02-07],
                                [2022-02-08],
                                [2022-02-09],
                                [2022-02-10],
                                [2022-02-11],
                                [2022-02-12],
                                [2022-02-13],
                                [2022-02-14],
                                [2022-02-15],
                                [2022-02-16],
                                [2022-02-17],
                                [2022-02-18],
                                [2022-02-19],
                                [2022-02-20],
                                [2022-02-21],
                                [2022-02-22],
                                [2022-02-23],
                                [2022-02-24],
                                [2022-02-25],
                                [2022-02-26],
                                [2022-02-27],
                                [2022-02-28] ) ) AS PivotTable
Order  by
          Name_Company 



GO


SELECT ID_Company,CONVERT(Date, DateDischarge), Count(*)  FROM tPatient_Confinement 
Where  DateDischarge IS NOT NULL 
GROUP BY ID_Company,CONVERT(Date, DateDischarge)


select  CONVERT(Date, confinement.DateDischarge) DateDischarge, c.Name Name_Company,
Count(*)  DischargeCount
FROm   tPatient_Confinement confinement
       inner join vCompanyActive c
               on c.ID = confinement.ID_Company
Where  ID_FilingStatus  IN (15)
GROUP BY c.Name, 
       confinement.ID_Company,
       CONVERT(Date, confinement.DateDischarge) 
ORDER BY CONVERT(Date, confinement.DateDischarge) DESC, c.Name


