Declare @Guid_Company TABLE
  (
     GUID VARCHAR(MAX)
  )
Declare @DateInactiveSendingSchedule DATETIME = '2022-04-16 00:00:00.000'
Declare @Comment VARCHAR(MAX) = 'Clinic Close at June 23.'

INSERT @Guid_Company
SELECT GUID
FROM   vCOmpanyActive
WHERE  GUID IN ( '3050A529-927C-4FB8-B5DF-8FD935C65962' )

DELETE FROM [tInactiveSMSSending]
WHERE  Date = @DateInactiveSendingSchedule

DELETE guids
FROM   @Guid_Company guids
       inner join vCompanyActive company
               on company.GUID = guids.GUID
       INNER JOIN tInactiveSMSSending inactiveSending
               on company.ID = inactiveSending.ID_Company
where  inactiveSending.Date = @DateInactiveSendingSchedule

INSERT INTO [dbo].[tInactiveSMSSending]
            ([Date],
             [ID_Company],
             [IsActive],
             [Comment],
             DateCreated,
             DateModified)
SELECT @DateInactiveSendingSchedule,
       company.ID,
       1,
       @Comment,
       GetDate(),
       GetDate()
FROM   vCompanyActive company
       inner join @Guid_Company guids
               on company.GUID = guids.GUID
Order  BY company.Name

SELECT Comment,
       ID_Company,
       Name_Company,
       Date DateInactiveSending,
       IsActive
FROm   vInactiveSMSSending inactiveSending
where  inactiveSending.Date = @DateInactiveSendingSchedule
       AND Date = @DateInactiveSendingSchedule
Order  by Name 
