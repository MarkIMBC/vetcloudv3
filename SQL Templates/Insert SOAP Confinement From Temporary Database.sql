GO

exec _pAddModelProperty
  'tPatient_SOAP',
  'tempID',
  1

GO

exec _pAddModelProperty
  'tPatient_SOAP_Prescription',
  'tempID',
  1

exec _pAddModelProperty
  'tPatient_SOAP_Prescription',
  'tempID',
  1

GO

exec _pAddModelProperty
  'tPatient_SOAP_Plan',
  'tempID',
  1

GO

EXEC _pRefreshAllViews

GO

IF OBJECT_ID(N'Temp-2022-03-28-AuditTrail-SOAP', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-AuditTrail-SOAP]

GO

IF OBJECT_ID(N'Temp-2022-03-28-Patient_SOAP', N'U') IS NOT NULL
  DROP TABLE [dbo].[Temp-2022-03-28-Patient_SOAP]

GO

select _user.Name_Company,
       _user.Name_Employee,
       _user.ID_Company,
       Date,
       Description,
       Model,
       ID_CurrentObject
from   db_waterworksv1_live_temp_20220328_113414.dbo.vAuditTrail _auditTrail
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.tAuditTrailType auditType
              on _auditTrail.ID_AuditType = auditType.ID
       LEFT JOIN db_waterworksv1_live_temp_20220328_113414.dbo.vUser _user
              on _user.ID = _auditTrail.ID_User
WHERE  Date >= '2022-03-28 11:00:00'
       AND Description like '%added%'
       AND model = 'SOAP'
Order  by Date

SELECT company.Name                                    Name_Company,
       'Temp-2022-03-28-' + company.Guid
       + '-Patient_SOAP-'
       + Convert(Varchar(MAX), Patient_SOAP.ID)        Temp_ID_Patient_SOAP,
       ISNULL(mainClient.ID, Patient_SOAP.ID_Client)   Main_ID_Client,
       ISNULL(mainPatient.ID, Patient_SOAP.ID_Patient) Main_ID_Patient,
       Patient_SOAP.*
INTO   [dbo].[Temp-2022-03-28-Patient_SOAP]
FROM   db_waterworksv1_live_temp_20220328_113414.[dbo].vPatient_SOAP Patient_SOAP
       inner join db_waterworksv1_live_temp_20220328_113414.[dbo].tCompany company
               on Patient_SOAP.ID_Company = company.ID
       LEFT JOIN tClient mainClient
              on mainClient.tempID = 'Temp-2022-03-28-' + company.Guid + '-Client-'
                                     + Convert(Varchar(MAX), Patient_SOAP.ID_Client)
                 and mainClient.ID_Company = Patient_SOAP.ID_Company
       LEFT JOIN tPatient mainPatient
              on mainPatient.tempID = 'Temp-2022-03-28-' + company.Guid
                                      + '-Patient-'
                                      + Convert(Varchar(MAX), Patient_SOAP.ID_Patient)
                 and mainPatient.ID_Company = Patient_SOAP.ID_Company
where  Patient_SOAP.DateCreated > '2022-03-28 10:00:00'

Declare @NotYetInserted_Temp_ID_Patient_SOAP TABLE
  (
     tempID VARCHAR(MAX)
  )

INSERT @NotYetInserted_Temp_ID_Patient_SOAP
SELECT tempPatient_SOAP.Temp_ID_Patient_SOAP
FROm   [dbo].[Temp-2022-03-28-Patient_SOAP] tempPatient_SOAP

DELETE FROM @NotYetInserted_Temp_ID_Patient_SOAP
WHERE  tempID IN (SELECT tempID
                  FROM   tPatient_SOAP) 
