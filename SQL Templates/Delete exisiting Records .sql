DELETE FROM tAuditTrail_Detail

DELETE FROM tAuditTrail

DELETE FROM tInventoryTrail

DELETE FROM dbo.tInventoryTrail_DeletedRecord

DELETE FROM dbo.tPatient_Vaccination_Schedule_SMSStatus

DELETE FROM dbo.tPatient_Vaccination_Schedule

DELETE FROM dbo.tPatient_Vaccination

DELETE FROM dbo.tClientDeposit

DELETE FROM dbo.tPayablePayment

DELETE FROM dbo.tPayable_Detail

DELETE FROM dbo.tPayable

DELETE FROM dbo.tPatient_Vaccination_Schedule_SMSStatus

DELETE FROM dbo.tPatient_BirthDateSMSGreetingLog

DELETE FROM tPatient_SOAP_Treatment

DELETE FROM tPatient_SOAP_Prescription

DELETE FROM tPatient_SOAP_Plan

DELETE FROM tPatient_SOAP_SMSStatus

DELETE FROM tPatient_SOAP_RegularConsoltation

DELETE FROM tPatient_SOAP

DELETE FROM [dbo].[tPatientWaitingList_Logs]

DELETE FROM [dbo].[tPatientWaitingList]

DELETE FROM [dbo].[tPatientAppointment]

DELETE FROM [dbo].[tUserSession]

DELETE FROM [dbo].[tUserSession]
where  ID_User IN (SELECT ID
                   FROM   vUser
                   WHERE  ID_Company NOT IN ( 1 ))

DELETE FROM [dbo].[tPatient_Confinement_ItemsServices]

DELETE FROM [dbo].[tPatient_Confinement_Patient]

DELETE FROM [dbo].[tPatient_Confinement]

DELETE FROM [dbo].[tFileUpload]

DELETE FROM [dbo].[tAppointmentStatusLog]

DELETE FROM tRecordValueTransferLogs

DELETE FROM [dbo].[tPurchaseOrder_Detail]

DELETE FROM [dbo].[tPurchaseOrder]

DELETE FROM [dbo].[tReceivingReport_Detail]

DELETE FROM [dbo].[tReceivingReport]

DELETE FROM [dbo].tDocumentSeries
where  ID_Company NOT IN ( 1 )

TRUNCATE TABLE tClientFeedback

DELETE FROM tPatient_Wellness_Schedule_SMSStatus

DELETE FROM tPatient_Wellness_Schedule

DELETE FROM tPatient_Wellness_Detail

DELETE FROM tPatient_Wellness

DELETE FROM [dbo].[_tDatabaseMemoryLog]

DELETE FROM tPaymentTransaction

DELETE FROM tBillingInvoice_Patient

DELETE FROM tBillingInvoice_Detail

DELETE FROM tBillingInvoice

DELETE FROM [tClientWithdraw ]

DELETE FROM [tClientWithdraw ]

DELETE FROM tClient_CreditLogs

DELETE FROM tPatient_DentalExamination

DELETE FROM tPatient_History

DELETE FROM tPatient

DELETE FROM tClient

if OBJECT_ID('dbo.[TuktukanClient]') is NOT null
  BEGIN
      DROP TABLE dbo.[TuktukanClient]
  END

if OBJECT_ID('dbo.PetLink_Services') is NOT null
  BEGIN
      DROP TABLE dbo.PetLink_Services
  END

if OBJECT_ID('dbo.PetLinkCafe_Items') is NOT null
  BEGIN
      DROP TABLE dbo.PetLinkCafe_Items
  END

if OBJECT_ID('dbo.temp_vPatient_SOAP_ListView') is NOT null
  BEGIN
      DROP TABLE dbo.temp_vPatient_SOAP_ListView
  END

if OBJECT_ID('dbo.TuktukanFinal') is NOT null
  BEGIN
      DROP TABLE dbo.TuktukanFinal
  END

if OBJECT_ID('dbo.PetLinkAcacia_Items') is NOT null
  BEGIN
      DROP TABLE dbo.PetLinkAcacia_Items
  END

-------------------------------------------------------------------------
GO

Update tEmployee
SET    ID_LastModifiedBy = 10,
       ID_CreatedBy = 10

DELETE FROM [dbo].tEmployee
where  ID_Company NOT IN ( 1 )

Update tClientAppointmentRequest
SET    ID_LastModifiedBy = 10,
       ID_CreatedBy = 10

Update tClientAppointmentRequest_Patient
SET    ID_LastModifiedBy = 10,
       ID_CreatedBy = 10

Update [vUser]
SET    ID_Employee = 1
WHERE  ID_Company NOT IN ( 1 )

DECLARE @Count INT =0

SELECT @Count = COUNT(*)
FROM   tUser

WHILE ( @Count > 0 )
  BEGIN
      SELECT @Count = COUNT(*)
      FROM   tUser

      pRINT @Count

      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)

		
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)


					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)
					
      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 3 ID
                    FROM   vUser
                    WHERE  ID_Employee IS NULL)


      DELETE FROM [dbo].[tUser]
      where  ID IN (SELECT TOP 2 ID
                    FROM   vUser
                    WHERE  ID_Company NOT IN ( 1 ))
  END 
GO