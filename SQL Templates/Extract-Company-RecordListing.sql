DECLare @ID_Company INT = 192

SELECt com.Name,
       client.Code,
       client.Name,
       client.ContactNumber,
       client.ContactNumber2,
       client.Address,
       client.CurrentCreditAmount
FROm   vClient client
       INNER Join tCompany com
               on client.ID_Company = com.ID
WHERE  client.IsActive = 1
       AND ID_Company = @ID_Company
Order  By client.Name

SELECt com.Name,
       client.Code   [Client Code],
       client.Name   [Client],
       p.Code,
       p.Name,
       p.CustomCode,
       p.Microchip,
       CASE
         WHEN p.IsNeutered = 1 THEN 'Yes'
         ELSE ''
       END           IsNeutered,
       CASE
         WHEN p.IsDeceased = 1 THEN 'Yes'
         ELSE ''
       END           IsDeceased,
       p.Species     [Breed - Species],
       p.Name_Gender [Gender],
       p.DateBirth
FROm   vPatient p
       inner join vClient client
               on p.ID_Client = client.ID
       INNER Join tCompany com
               on client.ID_Company = com.ID
WHERE  p.IsActive = 1
       AND p.ID_Company = @ID_Company
Order  By p.Name

SELECT com.Name,
       item.ID,
       item.Name,
       item.Name_ItemCategory [Category],
       item.UnitCost          [Buying Price],
       item.UnitPrice         [Selling Price],
       item.CurrentInventoryCount,
       item.Name_InventoryStatus,
       item.OtherInfo_DateExpiration
FROm   vItem item
       INNER Join tCompany com
               on item.ID_Company = com.ID
WHERE  item.IsActive = 1
       AND item.ID_Company = @ID_Company
       AND ID_ItemType = 2
Order  By item.Name

SELECT com.Name,
       item.ID,
       item.Name,
       item.Name_ItemCategory [Category],
       item.UnitCost          [Buying Price],
       item.UnitPrice         [Service Price]
FROm   vItem item
       INNER Join tCompany com
               on item.ID_Company = com.ID
WHERE  item.IsActive = 1
       AND item.ID_Company = @ID_Company
       AND ID_ItemType = 1
Order  By item.Name

SELECT com.Name,
       Name_Item,
       invtTrail.Code,
       Date,
       Quantity,
       Name_FilingStatus
FROm   vInventoryTrail invtTrail
       inner join tItem item
               on invtTrail.ID_Item = item.ID
       INNER Join tCompany com
               on item.ID_Company = com.ID
where  item.ID_Company = @ID_Company
Order  By Name_Item

SELECT com.Name,
       soap.Date,
       soap.Code,
       soap.Name_SOAPType,
       soap.Name_Client,
       soap.Name_Patient,
       soap.History,
       soap.ClinicalExamination,
       soap.Interpretation,
       soap.Diagnosis,
       soap.ClientCommunication
FROM   vPatient_SOAP soap
       INNER Join tCompany com
               on soap.ID_Company = com.ID
where  ID_Company = @ID_Company 
